@IsTest
public class CS_ProductUtilityTest  {
	testMethod static void testProductUtility(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(false, 'basketName', opp);
        INSERT basket;

        cscfga__Product_Definition__c pd1 = CS_TestDataFactory.generateProductDefinition(true, 'Mobile Voice');
        

        cscfga__Product_Definition__c pd2 = CS_TestDataFactory.generateProductDefinition(true, 'User Group');
        

        cscfga__Product_Configuration__c pc1 = CS_TestDataFactory.generateProductConfiguration(false, 'Special Conditions', basket);
        pc1.cscfga__Product_Definition__c = pd1.Id;
        pc1.cscfga__Configuration_Status__c = 'Valid';
        insert pc1;

        cscfga__Product_Configuration__c pc2 = CS_TestDataFactory.generateProductConfiguration(false, 'Special Conditions', basket);
        pc2.cscfga__Product_Definition__c = pd2.Id;
        pc2.cscfga__Configuration_Status__c = 'Valid';
        insert pc2;
        
        List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>{pc1, pc2};

        List<cscfga__Attribute_Definition__c> tmpAtrDefList = CS_TestDataFactory.generateAttributeDefinitions(true, 
            new List<String>{'Full Co Terminus', 'Averaging Co Terminus', 'Disconnect Allowance', 'Competitive Clause',
            'Total Revenue Guarantee', 'Total Contract Guarantee', 'Unlocking Fees', 'Competitive Benchmarking', 'Payment Terms', 
            'Crown Commercial Service'}, 
            pd1);

        List<cscfga__Attribute__c> attrs = CS_TestDataFactory.generateAttributesForConfiguration(false, tmpAtrDefList, pc1);
        attrs.addAll(CS_TestDataFactory.generateAttributesForConfiguration(false, tmpAtrDefList, pc2));
        for(cscfga__Attribute__c a : attrs){
        	a.cscfga__Is_Line_Item__c = true;
        	a.cscfga__Price__c = 10;
        }
        INSERT attrs;

        Map<Id, cscfga__Product_Basket__c> basketMap = new Map<Id, cscfga__Product_Basket__c>{ basket.Id => basket };
        Map<Id, cscfga__Product_Configuration__c> configsMap = new Map<Id, cscfga__Product_Configuration__c>{
        	pc1.Id => pc1,
        	pc2.Id => pc2
        };

        CS_TestDataFactory.generatePLReportConfigRecords();
        
        Test.startTest();

        CS_ProductUtility.CreateOLIs(new Set<String>{basket.Id});
        try{
        	CS_ProductUtility.DeleteHardOLIs(new Set<String>{basket.Id});
        }
        catch(Exception exc){}

        Test.stopTest();
	}
}