public class OCSJsonController {
   	public static final String ResourceName = 'OCS_JSON';
   	
   	public static OSCJSON getJsonSchema() {
		return OSCJSON.parse(CS_Utils.loadStaticResource(ResourceName).toString());
	}
	
	public static void ProcessJSON(string basketId){
	    OSCJSON oscJsonSchema = getJsonSchema();
	    
	    System.debug('oscJsonSchema>>'+oscJsonSchema);
	    
	    Map<String,List<OCSJsonUtility.ProductConfigDataWrapper>> pcdataSet = OCSJsonUtility.getproductConfigrationData(basketId);
	    
	    System.debug('pcdataSet>>'+JSON.serializePretty(pcdataSet));
	    
	}
}