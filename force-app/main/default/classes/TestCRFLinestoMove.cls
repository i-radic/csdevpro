@isTest(seeAllData=TRUE)

    private class TestCRFLinestoMove{
    
    static testMethod void myUnitTest(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    Opportunity o = new Opportunity();
    o.Name='TEST';
    o.stageName = 'FOS Stage';
    o.CloseDate = system.today();
    insert o;

        
	Flow_CRF__c Flowcrf = new Flow_CRF__c();    
    Flowcrf.Opportunity__c = o.Id;
    Flowcrf.recordTypeId=[Select Id from RecordType where DeveloperName=:'BusinessMovers'].Id;
    Flowcrf.Contact__c = c.Id;
    Flowcrf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert Flowcrf;
    
    CRF__c crf = new CRF__c();    
    crf.Opportunity__c = o.Id;
    crf.recordTypeId=[Select Id from RecordType where DeveloperName=:'Movers'].Id;
    crf.Contact__c = c.Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;
        
        
    Lines_to_Move__c FlowLM = new Lines_to_Move__c();
    FlowLM.Related_to_CRF__c = crf.Id;
    FlowLM.Related_to_Flow_CRF__c = Flowcrf.Id;    
    FlowLM.Name = '1';
    FlowLM.Keep_Number__c='No';
    FlowLM.RCF_or_CNI_or_Transfer_to_VOIP__c = 'None Required';
    insert FlowLM;    

    Apexpages.Standardcontroller sc = new Apexpages.Standardcontroller(FlowLM);
    CRFLinestoMove controller = new CRFLinestoMove(sc);
    controller.OnStart(); 
        
    Flow_CRF__c Flowcrf1 = new Flow_CRF__c();    
    Flowcrf1.Opportunity__c = o.Id;
    Flowcrf1.recordTypeId=[Select Id from RecordType where DeveloperName=:'BusinessMovers'].Id;
    Flowcrf1.Contact__c = c.Id;
    Flowcrf1.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert Flowcrf1;    
        
    CRF__c crf1 = new CRF__c();    
    crf1.Opportunity__c = o.Id;
    crf1.recordTypeId=[Select Id from RecordType where DeveloperName=:'Business_Mover'].Id;
    crf1.Contact__c = c.Id;
    crf1.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf1;
        
        
    Lines_to_Move__c FlowLM1 = new Lines_to_Move__c();
    FlowLM1.Related_to_CRF__c = crf1.Id;
    FlowLM1.Related_to_Flow_CRF__c = Flowcrf1.Id;    
    FlowLM1.Name = '1';
    FlowLM1.Keep_Number__c='No';
    FlowLM1.RCF_or_CNI_or_Transfer_to_VOIP__c = 'None Required';
    insert FlowLM1;    

    Apexpages.Standardcontroller sc1 = new Apexpages.Standardcontroller(FlowLM1);
    CRFLinestoMove controller1 = new CRFLinestoMove(sc1);
    controller1.OnStart(); 
        
    Flow_CRF__c Flowcrf2 = new Flow_CRF__c();    
    Flowcrf2.Opportunity__c = o.Id;
    Flowcrf2.recordTypeId=[Select Id from RecordType where DeveloperName=:'MoverswithexistingBB'].Id;
    Flowcrf2.Contact__c = c.Id;
    Flowcrf2.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert Flowcrf2;    
        
    CRF__c crf2 = new CRF__c();    
    crf2.Opportunity__c = o.Id;
    crf2.recordTypeId=[Select Id from RecordType where DeveloperName=:'Mover_with_existing_BB'].Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf2;
        
        
    Lines_to_Move__c FlowLM2 = new Lines_to_Move__c();
    FlowLM2.Related_to_CRF__c = crf2.Id;
    FlowLM2.Related_to_Flow_CRF__c = Flowcrf2.Id;    
    FlowLM2.Name = '1';
    FlowLM2.Keep_Number__c='No';
    FlowLM2.RCF_or_CNI_or_Transfer_to_VOIP__c = 'None Required';
    insert FlowLM2;    

    Apexpages.Standardcontroller sc2 = new Apexpages.Standardcontroller(FlowLM2);
    CRFLinestoMove controller2 = new CRFLinestoMove(sc2);
    controller2.OnStart();     
    }
    
}