public class LEMAccountTeamContactsList {
	
	private final LEM_Review__c lr;
	private LEM_Review__c lrBTLB;
	private List<Account_Team_Contact_BTLB__c> LEMATContacts;   
	private String sortDirection = 'ASC';   
	private String sortExp = 'name';
		
	
	public String sortExpression {
		get {
			return sortExp;     
		}     
		set {
			//if the column is clicked on then switch between Ascending and Descending modes       
			if (value == sortExp)         
				sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';       
			else         
				sortDirection = 'ASC';       
			sortExp = value;     
		}   
	}
	
	public String getSortDirection() {    
		//if not column is selected     
		if (sortExpression == null || sortExpression == '')      
			return 'ASC';    
		else     
			return sortDirection; 
	} 
	
	public void setSortDirection(String value) {
		sortDirection = value; 
	}
		
    public LEMAccountTeamContactsList(ApexPages.StandardController stdController) {
        this.lr = (LEM_Review__c)stdController.getRecord();
        this.lrBTLB = [Select l.Id, l.BTLB_Master_Object__c From LEM_Review__c l where l.Id =: lr.Id];
        ViewData();
    }
    
    public List<Account_Team_Contact_BTLB__c> getLEMATContacts() {       
    	return LEMATContacts;   
    }   
    
    public Id getLrId() {
		return lr.Id;
	}
    
    public PageReference ViewData() {
    	if(lrBTLB.BTLB_Master_Object__c != null) {
	    	string recId = ApexPages.currentPage().getParameters().get('id');       
	    	//build the full sort expression       
	    	string sortFullExp = sortExpression  + ' ' + sortDirection;             
	    	//query the database based on the sort expression       
	    	LEMATContacts = Database.query('Select a.email__c, a.Tel_No__c, a.SystemModstamp, a.Specialist_Role__c, a.Specialist_Name__c, a.Name__c, a.Name, a.Mobile__c, a.LastModifiedDate, a.LastModifiedById, a.IsDeleted, a.Id, a.ExtId__c, a.CreatedDate, a.CreatedById, a.BTLB__c From Account_Team_Contact_BTLB__c a where a.BTLB__c=\'' + lrBTLB.BTLB_Master_Object__c + '\' order by ' + sortFullExp + ' limit 1000');
    	}
    	return null;   
    }
}