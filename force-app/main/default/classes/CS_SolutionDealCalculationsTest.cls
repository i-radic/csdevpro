@IsTest
public with sharing class CS_SolutionDealCalculationsTest {
    static cscfga__Product_Basket__c cvBasket;
    static cscfga__Product_Basket__c fmBasket;

    static void createFMBasket() {
        CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        fmBasket = new cscfga__Product_Basket__c(
                Name = 'Test Basket',
                csbb__Synchronised_With_Opportunity__c = true,
                csordtelcoa__Synchronised_with_Opportunity__c = true,
                Synchronised_with_Opportunity__c = true,
                Total_Cost__c = 90,
                Number_of_Users__c = 30,
                Tenure__c = 12,
                FM_Data_ARPU__c = 3,
                FM_Voice_ARPU__c = 5,
                FM_Data_ARPU_Margin__c = 2,
                FM_Voice_ARPU_Margin__c = 1,
                Rolling_Air_Time__c = 0,
                Staged_Air_Time__c = 0
        );
        insert fmBasket;

        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
                Name = 'Future Mobile',
                cscfga__Description__c = 'Test'
        );

        insert pd;

        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
                Name = 'test config',
                cscfga__Product_Basket__c = fmBasket.Id,
                Tenure__c = 24,
                Number_of_users__c = 100,
                Mobile_Voice_One_Off_Cost__c = 60.3,
                Mobile_Voice_Recurring_Cost__c = 90.80,
                GM_Upfront_Cost__c = 77.00,
                GM_Recurring_Cost__c = 33.00,
                Rolling_Air_Time__c = 40.0,
                Staged_Air_Time__c = 78.0,
                Tech_Fund__c = 93,
                SpecCon_Full_co_terminus__c = true,
                SpecCon_Averaging_co_terminus__c = true,
                SpecCon_Disconnection_Allowance__c = true,
                SpecCon_Competitive_Clause__c = true,
                SpecCon_Total_Revenue_Guarantee__c = true,
                SpecCon_Total_Contract_Guarantee__c = true,
                SPecCon_Unlocking_Fees__c = true,
                SpecCon_Competitive_Benchmarking__c = true,
                Calculations_Product_Group__c = 'Future Mobile'
        );
        insert config;

        for (cscfga__Attribute__c att :
                CS_TestDataFactory.generateAttributesForConfiguration(true, CS_TestDataFactory.generateAttributeDefinitions(true, new List<String>{'cscfga__One_Off_Charge__c', 'cscfga__Recurring_Charge__c'}, pd), config)
        ) {
            att.cscfga__Is_Line_Item__c = true;
            update att;
        }

        CS_Approval_Level__c resignApprovalLevel = new CS_Approval_Level__c(
                Subs_From__c = 0,
                Subs_To__c = 999999,
                KPI__c = 'Total',
                Band_To__c = 0,
                Band_From__c = 99999,
                Is_Active__c = true,
                Type__c = 'Re-sign'
        );


        CS_Approval_Level__c acquisitionApprovalLevel = new CS_Approval_Level__c(
                Subs_From__c = 0,
                Subs_To__c = 999999,
                KPI__c = 'Total',
                Band_To__c = 0,
                Band_From__c = 99999,
                Is_Active__c = true,
                Type__c = 'Acquisition'
        );
        insert resignApprovalLevel;
        insert acquisitionApprovalLevel;
    }

    static void createCVBasket() {
        CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        cvBasket = new cscfga__Product_Basket__c(
                Name = 'Test Basket',
                csbb__Synchronised_With_Opportunity__c = true,
                csordtelcoa__Synchronised_with_Opportunity__c = true,
                Synchronised_with_Opportunity__c = true,
                Total_Cost__c = 90,
                Number_of_Users__c = 30,
                Tenure__c = 12,
                FM_Data_ARPU__c = 3,
                FM_Voice_ARPU__c = 5,
                FM_Data_ARPU_Margin__c = 2,
                FM_Voice_ARPU_Margin__c = 1,
                Rolling_Air_Time__c = 0,
                Staged_Air_Time__c = 0
        );
        insert cvBasket;

        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
                Name = 'Future Mobile',
                cscfga__Description__c = 'Test'
        );

        insert pd;

        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
                Name = 'test config',
                cscfga__Product_Basket__c = cvBasket.Id,
                Tenure__c = 24,
                Number_of_users__c = 100,
                Mobile_Voice_One_Off_Cost__c = 60.3,
                Mobile_Voice_Recurring_Cost__c = 90.80,
                GM_Upfront_Cost__c = 77.00,
                GM_Recurring_Cost__c = 33.00,
                Rolling_Air_Time__c = 40.0,
                Staged_Air_Time__c = 78.0,
                Tech_Fund__c = 93,
                SpecCon_Full_co_terminus__c = true,
                SpecCon_Averaging_co_terminus__c = true,
                SpecCon_Disconnection_Allowance__c = true,
                SpecCon_Competitive_Clause__c = true,
                SpecCon_Total_Revenue_Guarantee__c = true,
                SpecCon_Total_Contract_Guarantee__c = true,
                SPecCon_Unlocking_Fees__c = true,
                SpecCon_Competitive_Benchmarking__c = true,
                Calculations_Product_Group__c = 'Future Mobile'
        );
        insert config;

        for (cscfga__Attribute__c att :
                CS_TestDataFactory.generateAttributesForConfiguration(true, CS_TestDataFactory.generateAttributeDefinitions(true, new List<String>{'cscfga__One_Off_Charge__c', 'cscfga__Recurring_Charge__c'}, pd), config)
        ) {
            att.cscfga__Is_Line_Item__c = true;
            update att;
        }

        CS_Approval_Level__c resignApprovalLevel = new CS_Approval_Level__c(
                Subs_From__c = 0,
                Subs_To__c = 999999,
                KPI__c = 'Total',
                Band_To__c = 0,
                Band_From__c = 99999,
                Is_Active__c = true,
                Type__c = 'Re-sign'
        );


        CS_Approval_Level__c acquisitionApprovalLevel = new CS_Approval_Level__c(
                Subs_From__c = 0,
                Subs_To__c = 999999,
                KPI__c = 'Total',
                Band_To__c = 0,
                Band_From__c = 99999,
                Is_Active__c = true,
                Type__c = 'Acquisition'
        );
        insert resignApprovalLevel;
        insert acquisitionApprovalLevel;
    }

     static testMethod void unitTest1() {
         createCVBasket();
         cvBasket.ReSign__c = true;
         update cvBasket;
         Test.startTest();
         CS_SolutionDealCalculations basket = new CS_SolutionDealCalculations(cvBasket);
         basket.calculateBasketAttributes();
         basket.calculateBasketApprovals();
         Test.stopTest();
    }

    static testMethod void unitTest2() {
        createFMBasket();
        fmBasket.ReSign__c = false;
        update fmBasket;
        Test.startTest();
        CS_SolutionDealCalculations basket = new CS_SolutionDealCalculations(fmBasket);
        basket.calculateBasketAttributes();
        basket.calculateBasketApprovals();
        Test.stopTest();
    }
}