@isTest
private class SetAgreementStatusesOnOpp_Test
{

    static Account acc = null;
    static Contact contact = null;
    static Opportunity opp = null;     
    static testMethod void unitTestStatusDraft() 
    {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        
        prepareTestData();
        Map<Id,echosign_dev1__SIGN_Agreement__c> oppsMap = new Map<Id,echosign_dev1__SIGN_Agreement__c>();
        // echosign_dev1__SIGN_Agreement__c eco = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id,echosign_dev1__Status__c = 'Draft',Name='EE Business Agreement');
        echosign_dev1__SIGN_Agreement__c eco  = prepareTestAgreeementData('Draft');
        insert eco;
        oppsMap.put(opp.id, eco);
        update opp;
        
        List<Opportunity> oppList= [ SELECT Id, EEBA_Status__c
                       FROM Opportunity
                       WHERE Id = :opp.Id ];
        System.AssertEquals('No Agreement Generated', oppList[0].EEBA_Status__c);
       // echosign_dev1__SIGN_Agreement__c eco1 = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id , echosign_dev1__Status__c = 'Signed',Name='Credit Check');
       // insert eco1;
      
       
      
    }
    static testMethod void unitTestStatusOutforSignature() 
    {
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        prepareTestData();
        Map<Id,echosign_dev1__SIGN_Agreement__c> oppsMap = new Map<Id,echosign_dev1__SIGN_Agreement__c>();
        // echosign_dev1__SIGN_Agreement__c eco = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id,echosign_dev1__Status__c = 'Draft',Name='EE Business Agreement');
        echosign_dev1__SIGN_Agreement__c eco  = prepareTestAgreeementData('Out for Signature');
        insert eco;
        oppsMap.put(opp.id, eco);
        update opp;
        
        List<Opportunity> oppList= [ SELECT Id, EEBA_Status__c
                       FROM Opportunity
                       WHERE Id = :opp.Id ];
        System.AssertEquals('Out for Signature', oppList[0].EEBA_Status__c);
       // echosign_dev1__SIGN_Agreement__c eco1 = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id , echosign_dev1__Status__c = 'Signed',Name='Credit Check');
       // insert eco1;
      
       
      
    }
    static testMethod void unitTestStatusCounterSignature() 
    {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        
        prepareTestData();
        Map<Id,echosign_dev1__SIGN_Agreement__c> oppsMap = new Map<Id,echosign_dev1__SIGN_Agreement__c>();
        // echosign_dev1__SIGN_Agreement__c eco = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id,echosign_dev1__Status__c = 'Draft',Name='EE Business Agreement');
        echosign_dev1__SIGN_Agreement__c eco  = prepareTestAgreeementData('Waiting for Counter-Signature');
        insert eco;
        oppsMap.put(opp.id, eco);
        update opp;
        
        List<Opportunity> oppList= [ SELECT Id, EEBA_Status__c
                       FROM Opportunity
                       WHERE Id = :opp.Id ];
        System.AssertEquals('Waiting for Counter-Signature', oppList[0].EEBA_Status__c);
        
      
    }
    static testMethod void unitTestExpired() 
    {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        
        prepareTestData();
        Map<Id,echosign_dev1__SIGN_Agreement__c> oppsMap = new Map<Id,echosign_dev1__SIGN_Agreement__c>();
        // echosign_dev1__SIGN_Agreement__c eco = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id,echosign_dev1__Status__c = 'Draft',Name='EE Business Agreement');
        echosign_dev1__SIGN_Agreement__c eco  = prepareTestAgreeementData('Expired');
        insert eco;
        oppsMap.put(opp.id, eco);
        update opp;
        
        List<Opportunity> oppList= [ SELECT Id, EEBA_Status__c
                       FROM Opportunity
                       WHERE Id = :opp.Id ];
        System.AssertEquals('Expired', oppList[0].EEBA_Status__c);
        echosign_dev1__SIGN_Agreement__c eco1 = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id , echosign_dev1__Status__c = 'Signed',Name='Credit Check');
        insert eco1;
      
       
      
    }
    
    
    static testMethod void unitTestStatusSigned() 
    {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        
        prepareTestData();
        Map<Id,echosign_dev1__SIGN_Agreement__c> oppsMap = new Map<Id,echosign_dev1__SIGN_Agreement__c>();
        // echosign_dev1__SIGN_Agreement__c eco = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id,echosign_dev1__Status__c = 'Draft',Name='EE Business Agreement');
        echosign_dev1__SIGN_Agreement__c eco  = prepareTestAgreeementData('Signed');
        insert eco;
        oppsMap.put(opp.id, eco);
        update opp;
        
        List<Opportunity> oppList= [ SELECT Id, EEBA_Status__c
                       FROM Opportunity
                       WHERE Id = :opp.Id ];
        System.AssertEquals('Fully Signed', oppList[0].EEBA_Status__c);
       
      
    }
    static testMethod void unitTestCreditCheckStatusSigned() 
    {	
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        prepareTestData();
        echosign_dev1__SIGN_Agreement__c eco1 = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id , echosign_dev1__Status__c = 'Signed',Name='Credit Check');
        insert eco1;
        update opp;
        List<Opportunity> oppList= [ SELECT Id, CreditCheck_Status__c
                       FROM Opportunity
                       WHERE Id = :opp.Id ];
        System.AssertEquals('Fully Signed', oppList[0].CreditCheck_Status__c);
    }
    private static void prepareTestData()
    {	
       User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         } 
       acc = new Account();
       acc.Name = 'TestEchosign';
       insert acc;
       
       contact=new Contact(FirstName='John', LastName='Bloggs', AccountId=acc.id, Email='John.Bloggs@abc.com');
       insert contact;
       opp  = new Opportunity();
       Opp.Name ='Test Opportunity';
       Opp.StageName ='Decision';
       Opp.CloseDate = Date.today() + 28;
       Opp.AccountId = acc.id;
       
       insert opp;
          
     }
     private static echosign_dev1__SIGN_Agreement__c prepareTestAgreeementData(String status)
     {
     
         echosign_dev1__SIGN_Agreement__c eco = new echosign_dev1__SIGN_Agreement__c(echosign_dev1__Opportunity__c = opp.id,echosign_dev1__Status__c = status,Name='EE Business Agreement');
         return eco;
     }

}