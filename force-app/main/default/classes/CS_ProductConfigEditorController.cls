public class CS_ProductConfigEditorController {
    private static final String DEFAULT_SECTION_NAME = 'General Information';

    public class AttributeTableRowItem {
        public cscfga.Attribute attribute { get; set; }
        public Integer sectionIndex { get; set; }
        public String sectionName { get; set; }
        public Integer row { get { return (Integer)attribute.getDefinition().cscfga__Row__c; } }
        public Integer column { get { return (Integer)attribute.getDefinition().cscfga__Column__c; } }
    }

    public class AttributeTableRow {
        public cscfga.Attribute leftAttribute { get; set; }
        public cscfga.Attribute rightAttribute { get; set; }
        public Integer sectionIndex { get; set; }
        public String sectionName { get; set; }
        public Integer row { get; set; }
    }

    public String columnsForDisplay { get; set; }

    public List<String> displayColumnList {
        get { return String.isBlank(columnsForDisplay) ? new List<String>() : columnsForDisplay.split(','); }
    }

    private cscfga.API_1.ApiSession editorApiSession { get; set; }

    public List<cscfga.Attribute> hiddenAttributes { get; private set; }

    public String prodDefName {
        get { return prodDefName; }
        set {
            prodDefName = value;
            prodDef = String.isBlank(value) ? null : getProductDefinition(value);
        }
    }

    public cscfga__Product_Definition__c prodDef { get; set; }

    public Id prodConfigId {
        get { return prodConfigId; }
        set {
            prodConfigId = value;
            prodConfig = (value == null) ? null : getProductConfiguration(value);
            prodBasketId = (prodConfig == null) ? null : prodConfig.cscfga__Product_Basket__c;
            editorApiSession = getCurrentApiSession(prodConfig, prodDef);
        }
    }

    public cscfga__Product_Configuration__c prodConfig { get; set; }

    public Id prodConfigIdToDelete { get; set; }

    public List<cscfga__Product_Configuration__c> productConfigList {
        get { return getProductConfigurations(prodDef, prodBasketId); }
        set;
    }

    public Id prodBasketId { get; set; }

    public Boolean isEditable { get; set; }

    public String prodConfEditorTitle {
        get { return prodConfigId == null ? 'New ' + prodDefName : 'Currently editing: [' + prodConfig.Name + ']'; }
    }

    public String saveButtonText {
        get { return prodConfigId == null ? 'Save New ' + prodDefName : 'Save Changes'; }
    }

    public String cancelButtonText {
        get { return prodConfigId == null ? 'Clear Editor' : 'Cancel Changes'; }
    }

    public List<List<AttributeTableRow>> attributeTable {
        get {
            List<AttributeTableRowItem> attList = getAttributeMap(prodDef, prodConfig);
            return generateAttributeTable(attList);
        }

        private set;
    }

    public CS_ProductConfigEditorController() {
        hiddenAttributes = new List<cscfga.Attribute>();
    }

    private cscfga.API_1.ApiSession getCurrentApiSession(cscfga__Product_Configuration__c pc, cscfga__Product_Definition__c pd) {
        cscfga.API_1.ApiSession retval;

        if (pc != null) {
            retval = cscfga.Api_1.getApiSession(pc);
            //cscfga.ProductConfiguratorController.currentInstance = session.getController();
        }
        else if (pd != null) {
            retval = cscfga.Api_1.getApiSession(pd);
            //cscfga.ProductConfiguratorController.currentInstance = session.getController();
            retVal.getController().selectConfig();
            
        }
        else {
            retval = null;
        }

        return retval;
    }

    private List<AttributeTableRowItem> getAttributeMap(cscfga__Product_Definition__c prodDef, cscfga__Product_Configuration__c prodConfig) {
        List<AttributeTableRowItem> retval = new List<AttributeTableRowItem>();
        hiddenAttributes = new List<cscfga.Attribute>();

        if (editorApiSession == null)
            editorApiSession = getCurrentApiSession(prodConfig, prodDef);

        if (editorApiSession == null)
            return retval;

        cscfga.ProductConfiguration pc = editorApiSession.getRootConfiguration();
        List<cscfga.Attribute> attributes = pc.getAttributes();

        for (cscfga.Attribute att : attributes) {
            cscfga__Attribute_Definition__c attDef = att.getDefinition();
            att.presentationType = attDef.cscfga__Type__c;

            if (attDef.cscfga__Row__c != null && attDef.cscfga__Column__c != null) {
                AttributeTableRowItem atri = new AttributeTableRowItem();
                atri.attribute = att;
                retval.add(atri);
            }
            else {
                hiddenAttributes.add(att);
            }
        }

        Set<Id> ssIdList = new Set<Id>();
        for (AttributeTableRowItem atri : retval) {
            cscfga__Attribute_Definition__c attDef = atri.attribute.getDefinition();

            if (attDef.cscfga__Screen_Section__c != null)
                ssIdList.add(attDef.cscfga__Screen_Section__c);
        }

        Map<Id, cscfga__Screen_Section__c> ssMap = new Map<Id, cscfga__Screen_Section__c>([
            SELECT Id, Name, cscfga__Index__c, cscfga__Label__c
            FROM cscfga__Screen_Section__c
            WHERE Id IN :ssIdList
        ]);

        for (AttributeTableRowItem atri : retval) {
            cscfga__Attribute_Definition__c attDef = atri.attribute.getDefinition();

            if (attDef.cscfga__Screen_Section__c == null || !ssMap.containsKey(attDef.cscfga__Screen_Section__c)) {
                atri.sectionIndex = -1;
                atri.sectionName = DEFAULT_SECTION_NAME;
            }
            else {
                atri.sectionIndex = (Integer)ssMap.get(attDef.cscfga__Screen_Section__c).cscfga__Index__c;
                atri.sectionName = ssMap.get(attDef.cscfga__Screen_Section__c).Name;
            }
        }

        return retval;
    }

    private List<List<AttributeTableRow>> generateAttributeTable(List<AttributeTableRowItem> attributeList) {
        List<List<AttributeTableRow>> retval = new List<List<AttributeTableRow>>();

        Map<Integer, Map<Integer, AttributeTableRow>> tableSections = new Map<Integer, Map<Integer, AttributeTableRow>>();

        for (AttributeTableRowItem atri : attributeList) {
            Map<Integer, AttributeTableRow> atrMap = tableSections.get(atri.sectionIndex);
            if (atrMap == null) {
                atrMap = new Map<Integer, AttributeTableRow>();
                tableSections.put(atri.sectionIndex, atrMap);
            }

            AttributeTableRow atr;

            if (atrMap.containsKey(atri.row)) {
                atr = atrMap.get(atri.row);
            }
            else {
                atr = new AttributeTableRow();
                atr.sectionName = atri.sectionName;
                atr.sectionIndex = atri.sectionIndex;
                atr.row = atri.row;

                atrMap.put(atr.row, atr);
            }

            if (atri.column == 0) {
                atr.leftAttribute = atri.attribute;
                //if (atr.rightAttribute == null)
                //  atr.rightAttribute = new cscfga.Attribute(null, null, null);
            }
            else {
                atr.rightAttribute = atri.attribute;
                //if (atr.leftAttribute == null)
                //  atr.leftAttribute = new cscfga.Attribute(null, null, null);
            }
        }

        List<Integer> sortedSectionIndices = new List<Integer>(tableSections.keySet());
        sortedSectionIndices.sort();

        for (Integer sectIdx : sortedSectionIndices) {
            Map<Integer, AttributeTableRow> sectionMap = tableSections.get(sectIdx);
            List<AttributeTableRow> sortedSection = new List<AttributeTableRow>();

            List<Integer> sortedRowIndices = new List<Integer>(sectionMap.keySet());
            sortedRowIndices.sort();

            for (Integer rowIdx : sortedRowIndices)
                sortedSection.add(sectionMap.get(rowIdx));

            retval.add(sortedSection);
        }

        return retval;
    }

    public void edit() {
        editorApiSession = getCurrentApiSession(prodConfig, prodDef);
    }

    public PageReference saveProdConfig() {
        cscfga.Api_1.ApiSession apiSess = editorApiSession;

        cscfga.ProductConfiguration pc = apiSess.getConfiguration();

        if (prodConfig == null) {
            pc.getSObject().cscfga__Product_Basket__c = prodBasketId;
            //pc.setOppId(prodBasketId);
        }

        apiSess.updateConfig();
        cscfga.ValidationResult vr = apiSess.validateConfiguration();

        if (vr.getIsValid()) {
            apiSess.persistConfiguration(false);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Record Saved. Product Configuration Id = [' + pc.getSObject().Id + ']'));
        }
        else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid configuration.'));
            for (cscfga.FieldMessage fm : vr.getAllErrors())
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, formatFieldMessage(fm)));

            return null;
        }

/*
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'prodConfigId = ' + prodConfigId));
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'prodConfig.Id = ' + (prodConfig != null ? prodConfig.Id : null)));
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'pc.getSObject().Id = ' + pc.getSObject().Id));
*/

        if (prodConfig == null && prodBasketId != null) {
            cscfga__Product_Configuration__c newPC = pc.getSObject();
    
            if (newPC.cscfga__Product_Basket__c != prodBasketId) {
                cscfga__Product_Basket__c pbToDelete = new cscfga__Product_Basket__c(Id = newPC.cscfga__Product_Basket__c);
                newPC.cscfga__Product_Basket__c = prodBasketId;
    
                update newPC;
                delete pbToDelete;
            }
        }

        prodConfigId = null;
        editorApiSession = getCurrentApiSession(null, prodDef);

        return getPageRefWithParams();
        //return null;
    }

    public PageReference clearEditor() {
        prodConfigId = null;
        editorApiSession = getCurrentApiSession(null, prodDef);

        return getPageRefWithParams();
    }

    public PageReference deleteOpenProdConfig() {
        return deleteProdConfig(prodConfigId);
    }

    public PageReference deleteProdConfig() {
        return deleteProdConfig(prodConfigIdToDelete);
    }

    private PageReference deleteProdConfig(Id prodConfigToDelete) {
        if (prodConfigId == prodConfigToDelete)
            prodConfigId = null;

        delete new cscfga__Product_Configuration__c(Id = prodConfigToDelete);
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Record deleted. Product Configuration Id = [' + prodConfigToDelete + ']'));

        return getPageRefWithParams();
    }

    private cscfga__Product_Definition__c getProductDefinition(String prodDefName) {
        cscfga__Product_Definition__c productDefinition = [
            SELECT Id, Name
            FROM cscfga__Product_Definition__c
            WHERE
                Name = :prodDefName AND
                cscfga__Active__c = true
                LIMIT 1
        ];

        return productDefinition;
    }

    private cscfga__Product_Configuration__c getProductConfiguration(Id prodConfId) {
        cscfga__Product_Configuration__c retval = [
            SELECT
                Id, Name,
                cscfga__Product_Bundle__c,
                cscfga__Product_Definition__c,
                cscfga__Configuration_Offer__c,
                cscfga__Product_Basket__c,
                cscfga__Screen_Flow__r.cscfga__Landing_Page_Reference__c
            FROM cscfga__Product_Configuration__c
            WHERE Id = :prodConfId
        ];

        return retval;
    }

    private List<cscfga__Product_Configuration__c> getProductConfigurations(cscfga__Product_Definition__c productDefinition, Id productBasketId) {
        Id productDefinitionId = productDefinition.Id;

        String query = 'SELECT Id, Name';
        if (displayColumnList != null && displayColumnList.size() > 0)
            query += ', ' + String.join(displayColumnList, ', ') + ' ';

        query += ' FROM cscfga__Product_Configuration__c ';
        query += 'WHERE cscfga__Product_Definition__c = :productDefinitionId';
        if (productBasketId != null)
            query += ' AND cscfga__Product_Basket__c = :productBasketId';

        return Database.query(query);
    }

    @TestVisible
    private String formatFieldMessage(cscfga.FieldMessage fm) {
        List<String> fillers = new List<String>{
            fm.fieldReference.substring(0, fm.fieldReference.length() - 2).replace('_', ' '),
            fm.message
        };

        return String.format('Error on field [{0}]: {1}', fillers);
    }

    private PageReference getPageRefWithParams() {
        PageReference retval = ApexPages.currentPage();
        // retval.setRedirect(true);

        return retval;
    }
}