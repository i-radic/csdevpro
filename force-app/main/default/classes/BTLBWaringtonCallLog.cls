public with sharing class BTLBWaringtonCallLog {
    
    @future(callout=true)
    public static void getWarringtonCallLogData(string ids) {
        BTLBWaringtonCallLog callLog = new BTLBWaringtonCallLog();
        
        try {
            SalesforceCRMService.BTLBEventInteraction eventRequest = new SalesforceCRMService.BTLBEventInteraction();
            eventRequest = callLog.crmEvent(ids);
            SalesforceCRMService.CreateEventInteraction(eventRequest);            
        }
        catch (System.CalloutException ex){
            system.debug('Callout Exception occured:' + ex); 
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Callout Exception!' + ex));
        }
        catch (Exception exep) {
            system.debug('Exception:' + exep);
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Exception!' + exep));           
        }
    }
    
    private SalesforceCRMService.BTLBEventInteraction crmEvent(string eveId) {
        SalesforceCRMService.BTLBEventInteraction btlbEvent = new SalesforceCRMService.BTLBEventInteraction();
        List<BTLB_Warrington_Sales_Log__c> btlbWarringtonCallLog = new List<BTLB_Warrington_Sales_Log__c>();
        BTLB_Warrington_Sales_Log__c interacId= new BTLB_Warrington_Sales_Log__c();
        try {

            btlbWarringtonCallLog = [select LastModifiedBy.EIN__c, LastModifiedBy.Phone, LastModifiedById, Account__r.CUG__c,Account__c, Name, Call_Source__c from BTLB_Warrington_Sales_Log__c where id =: eveId];
            interacId.Interaction_Integration_Id__c = eveId;
            btlbEvent.CUG = btlbWarringtonCallLog[0].Account__r.CUG__c;
            btlbEvent.EventId = btlbWarringtonCallLog[0].Name;
            btlbEvent.RecordId = btlbWarringtonCallLog[0].Name;
            btlbEvent.agentEIN = btlbWarringtonCallLog[0].LastModifiedBy.EIN__c;
            //btlbEvent.interactionIntegrationId = warring.Interaction_Integration_Id__c;
            btlbEvent.interactionIntegrationId = interacId.Interaction_Integration_Id__c ;
            return btlbEvent;
        }
        catch(Exception e) {
            system.debug('exception:' + e);
            return null;
        }
    }
    
    /* -- singhd62
    @future(callout=true)
    public static void getWarringtonCallLogData(string ids) {
        BTLBWaringtonCallLog callLog = new BTLBWaringtonCallLog();
        SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();
        service.timeout_x = 60000;
        try {
            SalesforceServicesCRM.BTLBEventInteraction eventResponse = new SalesforceServicesCRM.BTLBEventInteraction();
            eventResponse = callLog.crmEvent(ids);
            service.CreateEventInteraction('','',eventResponse);            
        }
        catch (System.CalloutException ex){
            system.debug('Callout Exception occured:' + ex); 
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Callout Exception!' + ex));
        }
        catch (Exception exep) {
            system.debug('Exception:' + exep);
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Exception!' + exep));           
        }
    }
    
    
    private SalesforceServicesCRM.BTLBEventInteraction crmEvent(string eveId) {
        SalesforceServicesCRM.BTLBEventInteraction btlbEvent = new SalesforceServicesCRM.BTLBEventInteraction();
        List<BTLB_Warrington_Sales_Log__c> btlbWarringtonCallLog = new List<BTLB_Warrington_Sales_Log__c>();
        BTLB_Warrington_Sales_Log__c interacId= new BTLB_Warrington_Sales_Log__c();
        try {

            btlbWarringtonCallLog = [select LastModifiedBy.EIN__c, LastModifiedBy.Phone, LastModifiedById, Account__r.CUG__c,Account__c, Name, Call_Source__c from BTLB_Warrington_Sales_Log__c where id =: eveId];
            interacId.Interaction_Integration_Id__c = eveId;
            btlbEvent.CUG = btlbWarringtonCallLog[0].Account__r.CUG__c;
            btlbEvent.EventId = btlbWarringtonCallLog[0].Name;
            btlbEvent.RecordId = btlbWarringtonCallLog[0].Name;
            btlbEvent.agentEIN = btlbWarringtonCallLog[0].LastModifiedBy.EIN__c;
            //btlbEvent.interactionIntegrationId = warring.Interaction_Integration_Id__c;
            btlbEvent.interactionIntegrationId = interacId.Interaction_Integration_Id__c ;
            return btlbEvent;
        }
        catch(Exception e) {
            system.debug('exception:' + e);
            return null;
        }
    }
    */
}