/**
 * Clone User Group page controller
 *
 * @author Kristijan
 */
public with sharing class CS_CloneUserGroupController {

    private String basketId;
    private String configurationId;
    List<cscfga__Product_Configuration__c> childConfigurations;

    /**
     * Class constructor
     */
    public CS_CloneUserGroupController() {
        if (ApexPages.currentPage().getParameters().get('basketId') != null) {
            basketId = ApexPages.currentPage().getParameters().get('basketId');
        }
        if (ApexPages.currentPage().getParameters().get('configurationId') != null) {
            configurationId = ApexPages.currentPage().getParameters().get('configurationId');
        }
    }

    /**
     * Returns the list of eligible configurations for clone
     * 
     * @param String configurationId root config Id
     * @return list of configurations
     */
    public List<cscfga__Product_Configuration__c> getChildConfigurations() {
        
        // get only valid configurations
        if (childConfigurations == null) {
             String productDefinitionName = '';
            //get user profile
            Id profileId=userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            system.debug('ProfileName'+profileName);
			if(ProfileName == 'SME BTLB Sales' || ProfileName == 'SME System Admin' || ProfileName == 'SME Setup (Doxford)' || ProfileName.contains('BTLB')){
                productDefinitionName = 'User Group SME';
            }else{
                productDefinitionName = 'User Group';
            }
    
            childConfigurations = [
                    select Id, Name 
                    from cscfga__Product_Configuration__c
                    where cscfga__Parent_Configuration__c = :configurationId
                    and cscfga__Configuration_Status__c = 'Valid'
                    and cscfga__Product_Definition__r.Name = :productDefinitionName
                ];
        }
        
        return childConfigurations;
    }

    /**
     * Initiate clone
     * 
     * @param List<String> configurationIds configurations to clone
     * @param String configurationId root config Id
     * @param String basketId 
     */
    @RemoteAction
    public static String cloneConfigurations(List<String> configurationIds, String basketId, String configurationId) {
        String status = '';
        try { 
            CS_ConfigurationCopyHelper.cloneConfiguration(configurationIds, basketId, configurationId); 
        } catch (Exception e) {
            status = 'Error - ' + e.getMessage();
        }
        return status;
    }
    
    /**
     * Check if cloning is finished
     * 
     * @param String configurationId root config Id
     * @return String boolean value (true or false)
     */
    @RemoteAction
    public static String checkIfCloneCompleted(String configurationId) {
        String retVal = 'in_progress';
        try {
            cscfga__Product_Configuration__c rootConfiguration = [
                select id, Clone_In_Progress__c
                from cscfga__Product_Configuration__c
                where id = :configurationId
            ];
            if (!rootConfiguration.Clone_In_Progress__c) {
                retVal = 'completed';
            }
        } catch (Exception e) {
            retVal = 'Error + ' + e.getMessage();
        }
        return retVal;
    }
}