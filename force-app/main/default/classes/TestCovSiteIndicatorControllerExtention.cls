@isTest
public class TestCovSiteIndicatorControllerExtention
{
    static testMethod void testController() 
    {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
        }
        Opportunity opp = new Opportunity();
        opp.Name ='TestOpp1';
        opp.StageName='Changes Over Time';
        opp.CloseDate = Date.Today();
        insert opp;
        Coverage_Check__c cc = new Coverage_Check__c ();
        cc.Opportunity__c = opp.id;
        insert cc;
        Coverage_Check_Site__c  ccs = new Coverage_Check_Site__c ();
        ccs.I_understand2__c = TRUE;
        ccs.Location_Name2__c = 'Location';
        ccs.Post_Code__c = 'LS10 1LJ';
        ccs.CER_Reason__c ='No coverage confirmed on site';  // Coverage
        ccs.Site_Contact_Name__c ='John Smith';
        ccs.Telephone_Number__c ='123456';
        ccs.Floorplan_Attached__c = true;
        ccs.No_of_Buildings_Requiring_Enhancements__c = 5;
        ccs.No_of_floors_in_this_building__c = 6;
        ccs.Which_floors_require_enhancement__c ='2 and 3rd';
        ccs.Coverage_Check__c = cc.id;
        insert ccs; 
        String response='{\"links\" : [{\"rel\" : \"self\",\"href\" : \"/rest/v1/coverage/locations/ls10+1lj\",'+
            '\"method\" : \"get\"}],\"locations\": [{\"title\" : \"LS10 1LJ\",\"lat\" : \"53.79088\",\"lng\" :'+
            ' \"-1.5338569\",\"links\" : [{\"rel\" : \"self\",\"href\" : \"/rest/v1/coverage/locations/ls10+1lj\"'+
            ',\"method\" : \"get\"}], \"coverage\" : [ {\"type\" : \"2G\", \"strength\" : \"5\"}, {\"type\" : \"3G\",'+
            ' \"strength\" : \"5\"}, {\"type\" : \"4G\", \"strength\" : \"4\", \"comingsoon\" : \"false\"}, {\"type\" :'+
            '\"ee_single_flat\", \"strength\" : \"0\"}]}],\"lastUpdated\": \"17 March 2014 12:00 AM\"}'; 
        buildTestController(ccs).forTest(response); 
        Coverage_Check_Site__c  ccs1 = new Coverage_Check_Site__c ();
        ccs1.I_understand2__c = TRUE;
        ccs1.Location_Name2__c = 'Location1';
        ccs1.Post_Code__c = '';
        ccs1.CER_Reason__c ='No coverage confirmed on site';
        ccs1.Site_Contact_Name__c ='John Smith';
        ccs1.Telephone_Number__c ='123456';
        ccs1.Floorplan_Attached__c = true;
        ccs1.No_of_Buildings_Requiring_Enhancements__c = 5;
        ccs1.No_of_floors_in_this_building__c = 6;
        ccs1.Which_floors_require_enhancement__c ='2 and 3rd';
        ccs1.Coverage_Check__c = cc.id;
        insert ccs1;          
        response=''; 
        buildTestController(ccs1).forTest(response); 
        
        Coverage_Check_Site__c  ccs2 = new Coverage_Check_Site__c ();
        ccs2.I_understand2__c = TRUE;
        ccs2.Location_Name2__c = 'Location2';
        ccs2.Post_Code__c = 'cheeseLane';
        ccs2.CER_Reason__c ='No coverage confirmed on site';
        ccs2.Site_Contact_Name__c ='John Smith';
        ccs2.Telephone_Number__c ='123456';
        ccs2.Floorplan_Attached__c = true;
        ccs2.No_of_Buildings_Requiring_Enhancements__c = 5;
        ccs2.No_of_floors_in_this_building__c = 6;
        ccs2.Which_floors_require_enhancement__c ='2 and 3rd';
        ccs2.Coverage_Check__c = cc.id;
        insert ccs2;         
        response='{"links" : [{"rel" : "self","href" : "/rest/v1/coverage/locations/cheeselane","method" : "get"}]'+
            ',"locations": [{"title" : "","lat" : "50.66004","lng" : "-2.5367255","links" : [{"rel" : "self","href" : '+
            '"/rest/v1/coverage/locations/Cheese+Lane,+Weymouth,+Dorset+DT3,+UK","method" : "get"}]},{"title" : "","lat" :'+
            '"51.727646","lng" : "-0.0761939","links" : [{"rel" : "self","href" : "/rest/v1/coverage/locations/'+
            'Bread+and+Cheese+Lane,+Cheshunt,+Waltham+Cross,+Hertfordshire+EN7,+UK","method" : "get"}]},'+
            ' {"title" : "EX10 8QY","lat" : "50.681713","lng" : "-3.2475858","links" :'+
            '  [{"rel" : "self","href" : "/rest/v1/coverage/locations/Cheese+Lane,+Sidmouth,+Devon+EX10+8QY,+UK","method" :'+
            '   "get"}]},{"title" : "BS2 0JJ","lat" : "51.454205","lng" : "-2.585196","links" : [{"rel" : "self","href" :'+
            '   "/rest/v1/coverage/locations/Cheese+Lane,+Bristol,+City+of+Bristol+BS2+0JJ,+UK","method" : "get"}]}],'+
            '    "lastUpdated": "17 March 2014 12:00 AM"}'; 
        buildTestController(ccs2).forTest(response); 
        
        Coverage_Check_Site__c  ccs3 = new Coverage_Check_Site__c ();
        ccs3.I_understand2__c = TRUE;
        ccs3.Location_Name2__c = 'Location3';
        ccs3.Post_Code__c = 'ss5 5yy';
        ccs3.CER_Reason__c ='No coverage confirmed on site';
        ccs3.Telephone_Number__c ='123456';
        ccs3.Floorplan_Attached__c = true;
        ccs3.No_of_Buildings_Requiring_Enhancements__c = 5;
        ccs3.No_of_floors_in_this_building__c = 6;
        ccs3.Which_floors_require_enhancement__c ='2 and 3rd';
        ccs3.Coverage_Check__c = cc.id;
        insert ccs3;            
        response='{"links" : [{"rel" : "self","href" : "/rest/v1/coverage/locations/ss5+5yy","method" : "get"}]'+
            ',"locations": [{"title" : "SS55YY","lat" : "51.60228","lng" : "0.6564715","links" : '+
            '[{"rel" : "self","href" : "/rest/v1/coverage/locations/ss5+5yy","method" : "get"}]null}],'+
            '"lastUpdated": "20 March 2014 12:00 AM"}'; 
        buildTestController(ccs3).forTest(response);                 
        testParse();   
    }
    static void testParse() 
    {
        String json = '{\"links\" : [{\"rel\" : \"self\",\"href\" : \"/rest/v1/coverage/locations/ls101lj\",\"method\" :'+
            ' \"get\"}],\"locations\": [{\"title\" : \"LS10 1LJ\",\"lat\" : \"53.789864\",\"lng\" : \"-1.5338647\",\"links\" :'+
            ' [{\"rel\" : \"self\",\"href\" : \"/rest/v1/coverage/locations/ls101lj\",\"method\" : \"get\"}], \"coverage\" :'+
            ' [ {\"type\" : \"2G\", \"strength\" : \"5\"}, {\"type\" : \"3G\", \"strength\" : \"5\"}, {\"type\" :'+
            ' \"4G\", \"strength\" : \"4\", \"comingsoon\" : \"false\"}, {\"type\" : \"ee_single_flat\", \"strength\" :'+
            ' \"0\"}]}],\"lastUpdated\": \"17 March 2014 12:00 AM\"}';
        CoverageIndicatorJSONParser obj = CoverageIndicatorJSONParser.parse(json);
        //CoverageIndicatorJSONParser.Locations loc = obj.locations[0];
        System.assertEquals(1,  obj.locations.size());
        List<CoverageIndicatorJSONParser.Coverage> coverage =obj.locations[0].coverage;
        System.assertEquals(4,  coverage.size());
        System.assertEquals('2G',  coverage[0].type);
        //obj.Coverage = obj.Coverage();
        System.assert(obj != null);
    }
    static CovSiteIndicatorControllerExtention buildTestController(Coverage_Check_Site__c ccs)
    {
        PageReference pageRef = Page.CoverageCheckCoverageIndicator;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', ccs.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ccs);
        CovSiteIndicatorControllerExtention controllerExt = new CovSiteIndicatorControllerExtention(sc);
        return controllerExt ;        
        
    }
}