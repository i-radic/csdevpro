@isTest
private class Test_BatchPortfolioAreaSnapshotOwner {

    static testMethod void testOne() {
    	Profile p = [select id from profile where name='System Administrator'];
        
        User u1 = new User(alias = 'pfa1', email='pfa@bt.it',IsActive=true, 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, timezonesidkey='Europe/London', 
            username='pfa@bt.it', EIN__c='pfa12345');
        insert u1;    
        
    	BTLB_Master__c bm = new BTLB_Master__c(BTLB_Name_ExtLink__c = 'BTLB_TEST_1', Account_Owner__c = u1.Id);
    	insert bm;
    	
    	Forecasting_Portfolio_Area_Snapshot__c fpa = new Forecasting_Portfolio_Area_Snapshot__c(BTLB_Name__c = 'BTLB_TEST_1');
    	insert fpa;
    	
    	fpa = [select id, OwnerId from Forecasting_Portfolio_Area_Snapshot__c where id = :fpa.id];
    	System.assertNotEquals(fpa.OwnerId, u1.Id);    	
    	
        Test.startTest(); 
		string q = 'select id, BTLB_Name__c, OwnerId from Forecasting_Portfolio_Area_Snapshot__c where BTLB_Name__c = \'BTLB_TEST_1\' limit 10';
        BatchPortfolioAreaSnapshotOwner b = new BatchPortfolioAreaSnapshotOwner(q);
        ID batchprocessid = Database.executeBatch(b,200);
        Test.stopTest();
        
        fpa = [select id, OwnerId from Forecasting_Portfolio_Area_Snapshot__c where id = :fpa.id];
    	System.assertEquals(fpa.OwnerId, u1.Id);
    }
}