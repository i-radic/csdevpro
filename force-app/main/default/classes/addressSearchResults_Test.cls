@isTest(SeeAllData=true)
private class addressSearchResults_Test 
{    
    static testMethod void runPositiveTestCases() 
    {        
        Test_Factory.SetProperty('IsTest', 'yes');        
        Contact contact = new Contact();        
        addressSearchResults addSearchResults = new addressSearchResults(contact);        
        addSearchResults.refferalPage = 'new';        
        addSearchResults.postcode = 'GU195QT';        
        addSearchResults.getTest();        
        system.debug('getTest');        
        //addSearchResults.getAddresses();        
        //system.debug('getAddresses');        
        addSearchResults.getSelected();        
        system.debug('getSelected');        
        addSearchResults.getselectedAddress();         
        system.debug('getselectedAddress');                
        addSearchResults.CreateAddress();        
        system.debug('CreateAddress');        
        addSearchResults.ReDirect();        
        system.debug('ReDirect');         
        addSearchResults.Back();        
        system.debug('Back');
        addSearchResults.pullAddresses();
        system.debug('pullAddresses');
        
		      
     }         
    static testMethod void runPositiveTestCases2() 
    {        
        Test_Factory.SetProperty('IsTest', 'yes');        
        Contact contact = new Contact();        
        addressSearchResults addSearchResults = new addressSearchResults(contact);        
        addSearchResults.refferalPage = 'update';        
        addSearchResults.postcode = 'GU195QT';        
        addSearchResults.getTest();        
        system.debug('getTest');        
        //addSearchResults.getAddresses();        
        //system.debug('getAddresses');        
        addSearchResults.getSelected();        
        system.debug('getSelected');        
        addSearchResults.getselectedAddress();         
        system.debug('getselectedAddress');                
        addSearchResults.CreateAddress();        
        system.debug('CreateAddress');        
        addSearchResults.ReDirect();        
        system.debug('ReDirect');        
        addSearchResults.Back();        
        system.debug('Back'); 
        addSearchResults.pullAddresses();
        system.debug('pullAddresses');
    }    
    static testMethod void runPositiveTestCases3() 
    {        
        Test_Factory.SetProperty('IsTest', 'yes');        
        Contact contact = new Contact();        
        addressSearchResults addSearchResults = new addressSearchResults(contact);        
        addSearchResults.refferalPage = 'detail';        
        addSearchResults.postcode = 'GU195QT';        
        addSearchResults.getTest();        
        system.debug('getTest');        
        //addSearchResults.getAddresses();        
        //system.debug('getAddresses');        
        addSearchResults.getSelected();        
        system.debug('getSelected');        
        addSearchResults.getselectedAddress();         
        system.debug('getselectedAddress');                
        addSearchResults.CreateAddress();        
        system.debug('CreateAddress');        
        addSearchResults.ReDirect();        
        system.debug('ReDirect');        
        addSearchResults.Back();        
        system.debug('Back');  
        addSearchResults.pullAddresses();
        system.debug('pullAddresses');
    }
    static testMethod void runPositiveTestCases4() {
               
        Contact contact = new Contact();
        ApexPages.StandardController con = new ApexPages.StandardController(contact); 
        addressSearchResults addSearchResults = new addressSearchResults(con);
        
        Test_Factory.SetProperty('IsTest', 'No');
        addSearchResults.Back();        
        system.debug('Back');
        addSearchResults.ReDirect();        
        system.debug('ReDirect');
       
    }
    
}