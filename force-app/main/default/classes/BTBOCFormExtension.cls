/*
Client      : BT Business
Author      : Krupakar Reddy J
Date        : 07/02/2013 (dd/mm/yyyy)
Description : This class is a result of custom functionality Orders Re-Issued.
*/
public class BTBOCFormExtension {
	// Variables Required
    string BOId = '';
    string SP_PROFILEID = '00e200000015KNMAA2';
    string SPM_PROFILEID = '00e200000015KgOAAU';
    
    Boolean RecordLockError, SubmitLockError;
        
    public Boolean getRecLockError() {Return RecordLockError;}
    
    public Boolean getSaveLockError() {Return SubmitLockError;}

    public BTLB_Order_Cancellation_Form__c BOCF {get;set;}
    public BTB_Order_Line__c BO {get;set;}

    
	/*
		Contructor To Pull Required Records And Initialize Variables
	*/
    public BTBOCFormExtension(ApexPages.StandardController controller) {
        BOCF = (BTLB_Order_Cancellation_Form__c)controller.getRecord();
        BOId = Apexpages.currentPage().getParameters().get('BTBOrderLineId');
        BO = [Select Cancellation_Current_Status__c, BTB_CSS_ORDER_NO__c, CreatedDate, BTB_ORDER__r.OV_ORDER_NUM__c, BTB_ORDER__r.CreatedDate, BTB_ORDER__r.OwnerId, CUSTOMER_REQ_DATE__c, NOTES__c, CUST_AGREED_DT__c, COMPLETION_DATE__c, CANCELLATION_REASON__c, Currently_Actioned_By__c, Currently_Actioned_Time__c, 
        		(Select Product_Name_on_CSS__c, PROD_GROUP__c From BTB_Order_Line_Products__r where Acquisition_Defence_Type__c = 'Y'), 
        		(Select Telephone_Number_OV__c, Second_Actioned_Date_Time__c, Second_Actioned_By__c, 
        			Second_Actioned_By_EIN__c, Sales_Channel__c, ReIssue_RequestedBy__c, Order_Raised_By__c,Q8_Do_you_wish_to_complain__c, 
					Q7_Do_you_want_us_to_reissue_the_order__c, Q6_Have_you_informed_your_current_supp__c, 
					Q5a_Did_you_contact_BT_to_cancel__c, Q5_Did_your_current_supplier_refer_you__c, 
					Q4a_What_is_the_length_of_your_notice__c, Q4_What_is_the_remaining_term_notice_p__c, 
					Q3a_Who_initiated_the_contact_when_dis__c, Q3_What_was_the_reason_for_agreeing_to__c, 
					Q2_Did_you_agree_to_the_cancellation_o__c, Q1_Did_you_know_that_your_order_to_ret__c, 
					Product_Ordered__c, Position_in_Company__c,	OwnerId, Order_raised_by_EIN__c, Order_Created_By_Name__c, 
					Order_Create_Date__c, Ofcom_Consent__c, OUC__c, Number_of_lines_involved__c, Number_of_Employees__c, Notes__c, 
					Name_of_Contact__c, Name, Modified_Yesterday__c, ModifiedDate_Day_of_the_Week__c, Losing_Supplier_Name__c, 
					Location_OV__c, Incident_Notes__c, Id, Form_Submitted_Date_Time__c, Form_Submitted_By__c, Form_Submitted_By_EIN__c, 
					Form_Created_By_Name__c, Form_Created_By_EIN__c, Form_Completion_Date__c, First_Actioned_Date_Time__c, 
					First_Actioned_By__c, First_Actioned_By_EIN__c, Customer_Telephone_Number__c, Customer_Required_By_Date__c, 
					Customer_Contacted__c, Customer_Agreed_Date__c, Contact_Name_OV__c, 
					Consent_CP__c, Company_Name_OV__c, Cancellation_reason__c, Cancellation_Date__c, Call_Centre__c, CSS_Order_Number__c, CSS_Order_Created_Date__c, Broadband_Included__c, BT_Order_Number__c, BTB_Order_Line__c, Agent_Extension__c, Agent_Contact_Telephone_Number__c, Address__c From BTB_Order_Cancellation_Forms__r)
            From BTB_Order_Line__c where Id = :BOId];

        User U = [Select EIN__C, Department, Division, Phone, Extension, OUC__c From User where Id = :UserInfo.getUserId()];
        User Own = [Select Id, EIN__C, Name, Department From User where Id = :BO.BTB_ORDER__r.OwnerId];

        //Below section to display form entries from the queried Cancellation Form record
        If (BO.BTB_Order_Cancellation_Forms__r.size() > 0) {
            BOCF = BO.BTB_Order_Cancellation_Forms__r[0];
        } else {
            BOCF.BT_Order_Number__c = BO.BTB_ORDER__r.OV_ORDER_NUM__c;
            BOCF.Order_Create_Date__c = BO.BTB_ORDER__r.CreatedDate;
            BOCF.CSS_Order_Number__c = BO.BTB_CSS_ORDER_NO__c;
            BOCF.CSS_Order_Created_Date__c = BO.CreatedDate;
            BOCF.Order_raised_by_EIN__c = Own.EIN__c;
            BOCF.Order_Raised_By__c = Own.Id;
            BOCF.Order_Created_By_Name__c = Own.Name;
            BOCF.Location_OV__c = Own.Department;

            BOCF.Customer_Required_By_Date__c = BO.CUSTOMER_REQ_DATE__c;
            BOCF.Customer_Agreed_Date__c = BO.CUST_AGREED_DT__c;
            BOCF.Cancellation_Date__c = BO.COMPLETION_DATE__c;
            BOCF.Cancellation_reason__c = BO.CANCELLATION_REASON__c;
            If (BO.BTB_Order_Line_Products__r.size() == 1) {
                BOCF.Product_Ordered__c = BO.BTB_Order_Line_Products__r[0].Product_Name_on_CSS__c;
                If (BO.BTB_Order_Line_Products__r[0].PROD_GROUP__c == 'BB Winback')
                    BOCF.Broadband_Included__c = 'Yes';
                Else
                    BOCF.Broadband_Included__c = 'No';
            }
        }

        BOCF.Form_Completion_Date__c = system.today();
        BOCF.Form_Created_By_Name__c = UserInfo.getName();
        BOCF.Form_Created_By_EIN__c = U.EIN__C;
        BOCF.Call_Centre__c = U.Department;
        BOCF.Sales_Channel__c = U.Division;
        BOCF.Agent_Contact_Telephone_Number__c = U.Phone;
        BOCF.Agent_Extension__c = U.Extension;
        BOCF.OUC__c = U.OUC__c;
    }

	/*
		Record Lock Time Calculator. 35 Minutes Lock Period Raise Validation Error
	*/
	public double CalcLockTime() {
        long dt1 = BO.Currently_Actioned_Time__c.getTime() / 1000 / 60;
        long dt2 = datetime.now().getTime() / 1000 / 60;
        double d = dt2 - dt1;
        Return d;
    }
	
	/*
		Record Lock Functionality
	*/
    public void RecordLocking() {
        if (BO.Currently_Actioned_By__c == null) {
            BO.Currently_Actioned_By__c = UserInfo.getName();
            BO.Currently_Actioned_Time__c = datetime.now();
            Update BO;
        } else if (BO.Currently_Actioned_By__c != null) {
	            if (BO.Currently_Actioned_By__c != UserInfo.getName()) {
	                long dt1 = BO.Currently_Actioned_Time__c.getTime() / 1000 / 60;
	                long dt2 = datetime.now().getTime() / 1000 / 60;
	                double dcalc = CalcLockTime();
	                if (dcalc < 35) {
	                    RecordLockError = True;
	                } else
	                    if (dcalc > 35) {
	                    BO.Currently_Actioned_By__c = UserInfo.getName();
	                    BO.Currently_Actioned_Time__c = datetime.now();
	                    Update BO;
	                }
	            } else  if (BO.Currently_Actioned_By__c == UserInfo.getName())
                		BO.Currently_Actioned_Time__c = datetime.now();
            Update BO;
        }
    }

	/*
		Postback Functionality To Perform Ajax
	*/
    public void postback() {
		//Method Used Only For Doing Postbacks For Ajax Calls          
    }

    /*
		Final Form Saving Method
	*/
    public PageReference SubmitForm() {
       try {
       		   pageReference CForm = new pageReference('/' + BOId);			       
		       double dcalcsave = CalcLockTime();
			   if (dcalcsave > 35) {
		           SubmitLockError = True;
		           CForm = null;
		       }
			   else if (dcalcsave < 35) {
		       		BOCF.BTB_Order_Line__c=BOId;
		       		if(BOCF.Customer_Contacted__c != 'Yes') {
			           if(BOCF.Customer_Contacted__c=='No')
			             BO.Cancellation_Current_Status__c='Pending';
			           else 
			             BO.Cancellation_Current_Status__c='Email';  
		
			             BOCF.Name_of_Contact__c=null;
			             BOCF.Position_in_Company__c=null;
			             BOCF.Address__c=null;
			             BOCF.Number_of_Employees__c=null;
		       		}
		       		else if(BOCF.Customer_Contacted__c=='Yes') {
		           
			              if(BOCF.Q7_Do_you_want_us_to_reissue_the_order__c == 'YES') {
			                BOCF.ReIssue_RequestedBy__c = UserInfo.getUserId();
			                BO.Cancellation_Current_Status__c='Re-Issue';
			              }
			              else if(BOCF.Q7_Do_you_want_us_to_reissue_the_order__c == 'Order Re-Issued') {
			              	BOCF.Order_ReIssued_By__c = UserInfo.getUserId();
			                BO.Cancellation_Current_Status__c='Completed';
			              }
			              else {
			                BO.Cancellation_Current_Status__c='Completed';
			              }
				          BOCF.Form_Submitted_By__c=BOCF.Form_Created_By_Name__c;
				          BOCF.Form_Submitted_By_EIN__c=BOCF.Form_Created_By_EIN__c;
				          BOCF.Form_Submitted_Date_Time__c=system.now();
		        	}
		
		            if(BOCF.First_Actioned_By__c==null){
			             BOCF.First_Actioned_By__c=BOCF.Form_Created_By_Name__c;
			             BOCF.First_Actioned_By_EIN__c=BOCF.Form_Created_By_EIN__c;
			             BOCF.First_Actioned_Date_Time__c=system.now();
		            }
		            else if(BOCF.Second_Actioned_By__c==null){
			             BOCF.Second_Actioned_By__c=BOCF.Form_Created_By_Name__c;
			             BOCF.Second_Actioned_By_EIN__c=BOCF.Form_Created_By_EIN__c;
			             BOCF.Second_Actioned_Date_Time__c=system.now();            
		            }	
		             BO.Currently_Actioned_By__c='';
		             BO.Currently_Actioned_Time__c=null;
		             BO.First_Actioned_By__c=BOCF.First_Actioned_By__c;
		             BO.Second_Actioned_By__c=BOCF.Second_Actioned_By__c;
		             BO.Form_Submitted_By__c=BOCF.Form_Submitted_By__c;
		             BOCF.Customer_Contacted__c=BOCF.Customer_Contacted__c;
		             BO.NOTES__c = BOCF.Notes__c;
			         Upsert BOCF;  
			         Update BO;
		        }
		        return CForm;
       }
       catch(exception e) {
        	return null;
       }
    }
    
    /*
		Pull Customer Contact Field Values To Create Radio Button List
	*/
    public List < SelectOption > getCustomer_Contacted() {
        Schema.sObjectType sobject_type = BTLB_Order_Cancellation_Form__c.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map < String,Schema.SObjectField > field_map = sobject_describe.fields.getMap();
        List < Schema.PicklistEntry > pick_list_values = field_map.get('Customer_Contacted__c').getDescribe().getPickListValues();
        List < selectOption > options = new List < selectOption > ();
        for (Schema.PicklistEntry a: pick_list_values) {
            options.add(new selectOption(a.getLabel(), a.getValue()));
        }
        return options;
    }
    /*
		Pull Re-Issue Order Field Values Based On Profile
	*/
    public List < SelectOption > getReIssueOrder() {
        Schema.sObjectType sobject_type = BTLB_Order_Cancellation_Form__c.getSObjectType();
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe();
        Map<String,Schema.SObjectField> field_map = sobject_describe.fields.getMap();
        List<Schema.PicklistEntry> pick_list_values = field_map.get('Q7_Do_you_want_us_to_reissue_the_order__c').getDescribe().getPickListValues();
        List<selectOption> options = new List < selectOption > ();
        options.add(new SelectOption('','--None--'));
        for (Schema.PicklistEntry a: pick_list_values) {
        	system.debug('Label : ' + a.getLabel() + ' Value : ' + a.getValue());
        	if(a.isActive() && accept(a)) {	
               options.add(new selectOption(a.getLabel(), a.getValue()));
        	}
        }
        return options;
    }
    
    /*
		Accept Picklist Values Based on Profile
	*/
    private Boolean accept(PicklistEntry e) {
    	system.debug('User Profile : ' + UserInfo.getProfileId() + ' Current Status : ' + BO.Cancellation_Current_Status__c);
	    if ((SP_PROFILEID == UserInfo.getProfileId() || SPM_PROFILEID == UserInfo.getProfileId()) && BO.Cancellation_Current_Status__c == 'Re-Issue') {
	        // Only show the entries not in the hide set
	        Set<String> hide = new Set<String>{
	        	'',
	            'YES',
	            'NO',
	            'Order Re-Issued'
	            };
	        return hide.contains(e.getValue());
	    } else {
	        // Show all entries
	        Set<String> hide = new Set<String>{
	        	'',
	            'YES',
	            'NO'	            
	            };
	        return hide.contains(e.getValue());
	    }
	}
    
}