public class DisableDeleteLinkOnOpportunity{

    public DisableDeleteLinkOnOpportunity(){}
    
    public DisableDeleteLinkOnOpportunity(ApexPages.StandardController con){
    }
    
    public PageReference disableOpportunityDelete()
    {        
        PageReference pr = new PageReference('/'+ ApexPages.currentPage().getParameters().get('retURL'));
        pr.setRedirect(true);
        return pr;
    }
}