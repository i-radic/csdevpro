global class BTSport_EmailStore implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        try {
            Attachment[] newAttachment = new Attachment[0];
            string RefNo = email.subject.mid(email.subject.indexOf('BTSport-', 0),15);
            System.debug('ADJ :RefNo ' + RefNo );  
            list <BT_Sport__c> relatedCase = [select id from BT_Sport__c where name = :RefNo limit 1]; 
            newAttachment.add(new Attachment(
                name = 'To:'+ email.toAddresses + ': '+ email.subject + '.html',
                body = Blob.valueof(email.htmlBody),
                ParentID = relatedCase[0].id));
            insert newAttachment;    
            System.debug('Nehttp://marketplace.eclipse.org/marketplace-client-intro?mpc_install=1336w Task Object: ' + newAttachment );   
        }
        catch (QueryException e) {
            System.debug('Query Issue: ' + e);
        }
        result.success = true; 
        return result;
    }
}