@isTest
public class CustomButtonBTPLAcceleratorTest  {
	@IsTest
	public static void testPerformActionGoodProfile() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account testAcc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', testAcc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', testOpp);

		CustomButtonBTPLAccelerator accelerator = new CustomButtonBTPLAccelerator();

		Test.startTest();
		String action = accelerator.performAction(basket.Id);
		String check = '{"status":"ok","redirectURL":"/apex/cspl__PLReportNew?id=' + basket.Id + '&npvMode=false"}';
		System.assertEquals(check, action);
		Test.stopTest();

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testPerformActionBadProfile() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account testAcc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', testAcc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', testOpp);

		Profile testProfile = [SELECT Id FROM Profile WHERE Name='BTLB: Standard Team'];
		User testUser = new User(Alias = 'testUser', Email='testuser@testorg.hr', 
			EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
			LocaleSidKey='en_US', ProfileId = testProfile.Id, TimeZoneSidKey='America/Los_Angeles', UserName='testuser@testorg.hr', EIN__c = '123456789');

		CustomButtonBTPLAccelerator accelerator = new CustomButtonBTPLAccelerator();

		Test.startTest();
		System.runAs(testUser) {
			String action = accelerator.performAction(basket.Id);
			String check = '{"status":"error","title":"Error","text":"Your user profile cannot access profit and loss report."}';
			System.assertEquals(check, action);	
		}
		Test.stopTest();

		notriggers.Flag__c = false;
		DELETE notriggers;
	}
}