@IsTest
public class CS_DiscountRedirectController_test {
    public testMethod static void DiscRedirectTest(){
        try{
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        PageReference pageRef = Page.CS_DiscountRedirect;
        test.setCurrentPageReference(pageRef);
        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BTnet');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BTnet';
        pc.Volume__c = 10;
        INSERT pc;
        cspl__Report_Configuration__c reportCOnfig = new cspl__Report_Configuration__c(Name = 'configFields');
        reportCOnfig.cspl__value__c = '["Name","cscfga__One_Off_Charge__c","cscfga__Recurring_Charge__c","cspl__Month_Term__c","Calculations_Product_Group__c","Product_Definition_Name__c","Care_and_Chargable_Service_Revenue__c","Other_Revenue__c","Year_1_Other_Revenue__c","BT_Technology_Fund__c","Mobile_Voice_One_Off_Charge__c","Mobile_Voice_Recurring_Charge__c","SMS_Recurring_Charge__c","SMS_One_Off_Charge__c","Data_One_Off_Charge__c","Data_Recurring_Charge__c","Other_One_Off_Charge__c","Other_Recurring_Charge__c","Fixed_One_Off_Charge__c","Fixed_Recurring_Charge__c","Incoming_Recurring_Charge__c","Incoming_Recurring_Cost__c","Mobile_Voice_One_Off_Cost__c","Mobile_Voice_Recurring_Cost__c","Line_Rental__c","SMS_One_Off_Cost__c","SMS_Recurring_Cost__c","Data_One_Off_Cost__c","Data_Recurring_Cost__c","Other_One_Off_Cost__c","Other_Recurring_Cost__c","Fixed_One_Off_Cost__c","Fixed_Recurring_Cost__c","Device_One_Off_Cost__c","Device_Recurring_Cost__c","Device_One_Off_Charge__c","Device_One_Off_Charge_Discounted__c","Device_Recurring_Charge__c","Sales_Commision_One_Off_Cost__c","OPEX_Recurring_Cost__c","Service_Recurring_Cost__c","Rolling_Air_Time__c","Staged_Air_Time__c","Tech_Fund__c","Buy_out_cheque__c","Unconditional_cheque__c","cscfga__Contract_Term__c","Data_Costs__c","Year_1_Care_and_Chargable_Service_Costs__c","Care_and_Chargable_Service_Costs__c","Net_device_profit_cost__c","Product_Name__c","Total_Investment_Pot__c","Investment__c","Voice_Revenue_inc_SMS__c","Data_Revenue__c","cscfga__recurring_charge_product_discount_value__c","cscfga__one_off_charge_product_discount_value__c","Incoming_Revenue__c","Voice_Costs_inc_SMS__c","Other_Costs__c","Year_1_Other_Costs__c","Year_1_Incremental_Overheads__c","Incremental_Overheads__c","Volume__c","Year_1_Other_Overheads__c","Other_Overheads__c","BT_Total_Commission__c","Data_Costs_One_Off__c","Voice_Costs_inc_SMS_One_Off__c","Mobile_Voice_Recurring_Charge_Discounted__c","Mobile_Voice_One_Off_Charge_Discounted__c","SMS_Recurring_Charge_Discounted__c","SMS_One_Off_Charge_Discounted__c","Data_Recurring_Charge_Discounted__c","Data_One_Off_Charge_Discounted__c","Other_Recurring_Charge_Discounted__c","Other_One_Off_Charge_Discounted__c","Fixed_Recurring_Charge_Discounted__c","Fixed_One_Off_Charge_Discounted__c","Airtime_Fund__c","NPV_Tech_Fund__c","Tech_Fund_Total__c","Tech_Fund__c","BTOP_Tech_Fund__c","Year_1_Site_And_Sub_Install_Cost__c","Site_And_Sub_Install_Cost__c","Year_1_Site_And_Sub_Install__c","Site_And_Sub_Install__c","NPV_Monthly_Cost__c","NPV_Monthly_Revenue__c","NPV_Upfront_Cost__c","NPV_Upfront_Revenue__c","BT_Technology_Fund_Neg__c","NPV_Monthly_Revenue_Disc__c","NPV_Upfront_Revenue_Disc__c","GM_Recurring_Cost__c","GM_Upfront_Cost__c","cscfga__Quantity__c","Total_GM_Recurring_Cost__c","Total_GM_Upfront_Cost__c","PC_Total_Recurring_Charge__c","PC_Total_One_Off_Charge__c"]';
        INSERT reportConfig;
      //  CS_CustomRedirectCSTest.createTestData();
        pageRef.getParameters().put('basketId',basket.id);
        
            CS_CR_Basket_Calculations__c setting2 = new CS_CR_Basket_Calculations__c(
                Sync_Basket_Size_Limit__c = 1
            );
            insert setting2;
            CS_DiscountRedirectController DiscRedir= new CS_DiscountRedirectController();
            DiscRedir.redirectToPage();
            DiscRedir.doDiscountCalculations();
        }
        Catch(exception e){}
    }
    
   public testMethod static void DiscRedirectTest2(){
        try{
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        PageReference pageRef = Page.CS_DiscountRedirect;
        test.setCurrentPageReference(pageRef);
        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BTnet');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BTnet';
       
        pc.Volume__c = 10;
        INSERT pc;
        pageRef.getParameters().put('basketId',basket.id);
       
            CS_CR_Basket_Calculations__c setting2 = new CS_CR_Basket_Calculations__c(
                Sync_Basket_Size_Limit__c = -1
            );
            //insert setting2;
            CS_DiscountRedirectController DiscRedir= new CS_DiscountRedirectController();
            DiscRedir.doDiscountCalculations();
            DiscRedir.redirectToPage();           
        }
        Catch(exception e){}
    }
    
    public testMethod static void DiscRedirectTest3(){
        try{
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        PageReference pageRef = Page.CS_DiscountRedirect;
        test.setCurrentPageReference(pageRef);
        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BTnet');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        //pc.cscfga__Product_Family__c = 'BTnet';
        pc.Calculations_Product_Group__c = 'Cloud Voice';
        pc.Volume__c = 10;
        INSERT pc;
        cspl__Report_Configuration__c reportCOnfig = new cspl__Report_Configuration__c(Name = 'configFields');
        reportCOnfig.cspl__value__c = '["Name","cscfga__One_Off_Charge__c","cscfga__Recurring_Charge__c","cspl__Month_Term__c","Calculations_Product_Group__c","Product_Definition_Name__c","Care_and_Chargable_Service_Revenue__c","Other_Revenue__c","Year_1_Other_Revenue__c","BT_Technology_Fund__c","Mobile_Voice_One_Off_Charge__c","Mobile_Voice_Recurring_Charge__c","SMS_Recurring_Charge__c","SMS_One_Off_Charge__c","Data_One_Off_Charge__c","Data_Recurring_Charge__c","Other_One_Off_Charge__c","Other_Recurring_Charge__c","Fixed_One_Off_Charge__c","Fixed_Recurring_Charge__c","Incoming_Recurring_Charge__c","Incoming_Recurring_Cost__c","Mobile_Voice_One_Off_Cost__c","Mobile_Voice_Recurring_Cost__c","Line_Rental__c","SMS_One_Off_Cost__c","SMS_Recurring_Cost__c","Data_One_Off_Cost__c","Data_Recurring_Cost__c","Other_One_Off_Cost__c","Other_Recurring_Cost__c","Fixed_One_Off_Cost__c","Fixed_Recurring_Cost__c","Device_One_Off_Cost__c","Device_Recurring_Cost__c","Device_One_Off_Charge__c","Device_One_Off_Charge_Discounted__c","Device_Recurring_Charge__c","Sales_Commision_One_Off_Cost__c","OPEX_Recurring_Cost__c","Service_Recurring_Cost__c","Rolling_Air_Time__c","Staged_Air_Time__c","Tech_Fund__c","Buy_out_cheque__c","Unconditional_cheque__c","cscfga__Contract_Term__c","Data_Costs__c","Year_1_Care_and_Chargable_Service_Costs__c","Care_and_Chargable_Service_Costs__c","Net_device_profit_cost__c","Product_Name__c","Total_Investment_Pot__c","Investment__c","Voice_Revenue_inc_SMS__c","Data_Revenue__c","cscfga__recurring_charge_product_discount_value__c","cscfga__one_off_charge_product_discount_value__c","Incoming_Revenue__c","Voice_Costs_inc_SMS__c","Other_Costs__c","Year_1_Other_Costs__c","Year_1_Incremental_Overheads__c","Incremental_Overheads__c","Volume__c","Year_1_Other_Overheads__c","Other_Overheads__c","BT_Total_Commission__c","Data_Costs_One_Off__c","Voice_Costs_inc_SMS_One_Off__c","Mobile_Voice_Recurring_Charge_Discounted__c","Mobile_Voice_One_Off_Charge_Discounted__c","SMS_Recurring_Charge_Discounted__c","SMS_One_Off_Charge_Discounted__c","Data_Recurring_Charge_Discounted__c","Data_One_Off_Charge_Discounted__c","Other_Recurring_Charge_Discounted__c","Other_One_Off_Charge_Discounted__c","Fixed_Recurring_Charge_Discounted__c","Fixed_One_Off_Charge_Discounted__c","Airtime_Fund__c","NPV_Tech_Fund__c","Tech_Fund_Total__c","Tech_Fund__c","BTOP_Tech_Fund__c","Year_1_Site_And_Sub_Install_Cost__c","Site_And_Sub_Install_Cost__c","Year_1_Site_And_Sub_Install__c","Site_And_Sub_Install__c","NPV_Monthly_Cost__c","NPV_Monthly_Revenue__c","NPV_Upfront_Cost__c","NPV_Upfront_Revenue__c","BT_Technology_Fund_Neg__c","NPV_Monthly_Revenue_Disc__c","NPV_Upfront_Revenue_Disc__c","GM_Recurring_Cost__c","GM_Upfront_Cost__c","cscfga__Quantity__c","Total_GM_Recurring_Cost__c","Total_GM_Upfront_Cost__c","PC_Total_Recurring_Charge__c","PC_Total_One_Off_Charge__c"]';
        INSERT reportConfig;
        pageRef.getParameters().put('basketId',basket.id);
       
            CS_CR_Basket_Calculations__c setting2 = new CS_CR_Basket_Calculations__c(
                Sync_Basket_Size_Limit__c = 1
            );
            //insert setting2;
            Test.startTest();
                CS_DiscountRedirectController DiscRedir= new CS_DiscountRedirectController();
                DiscRedir.doDiscountCalculations();
                DiscRedir.redirectToPage();   
            Test.stopTest();
        }
        Catch(exception e){}
    }
    

    public testMethod static void DiscRedirectTest4(){
        try{
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        PageReference pageRef = Page.CS_DiscountRedirect;
        test.setCurrentPageReference(pageRef);
        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BTnet');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        //pc.cscfga__Product_Family__c = 'BTnet';
        pc.Calculations_Product_Group__c = 'BT Net';
        pc.Volume__c = 10;
        INSERT pc;
        cspl__Report_Configuration__c reportCOnfig = new cspl__Report_Configuration__c(Name = 'configFields');
        reportCOnfig.cspl__value__c = '["Name","cscfga__One_Off_Charge__c","cscfga__Recurring_Charge__c","cspl__Month_Term__c","Calculations_Product_Group__c","Product_Definition_Name__c","Care_and_Chargable_Service_Revenue__c","Other_Revenue__c","Year_1_Other_Revenue__c","BT_Technology_Fund__c","Mobile_Voice_One_Off_Charge__c","Mobile_Voice_Recurring_Charge__c","SMS_Recurring_Charge__c","SMS_One_Off_Charge__c","Data_One_Off_Charge__c","Data_Recurring_Charge__c","Other_One_Off_Charge__c","Other_Recurring_Charge__c","Fixed_One_Off_Charge__c","Fixed_Recurring_Charge__c","Incoming_Recurring_Charge__c","Incoming_Recurring_Cost__c","Mobile_Voice_One_Off_Cost__c","Mobile_Voice_Recurring_Cost__c","Line_Rental__c","SMS_One_Off_Cost__c","SMS_Recurring_Cost__c","Data_One_Off_Cost__c","Data_Recurring_Cost__c","Other_One_Off_Cost__c","Other_Recurring_Cost__c","Fixed_One_Off_Cost__c","Fixed_Recurring_Cost__c","Device_One_Off_Cost__c","Device_Recurring_Cost__c","Device_One_Off_Charge__c","Device_One_Off_Charge_Discounted__c","Device_Recurring_Charge__c","Sales_Commision_One_Off_Cost__c","OPEX_Recurring_Cost__c","Service_Recurring_Cost__c","Rolling_Air_Time__c","Staged_Air_Time__c","Tech_Fund__c","Buy_out_cheque__c","Unconditional_cheque__c","cscfga__Contract_Term__c","Data_Costs__c","Year_1_Care_and_Chargable_Service_Costs__c","Care_and_Chargable_Service_Costs__c","Net_device_profit_cost__c","Product_Name__c","Total_Investment_Pot__c","Investment__c","Voice_Revenue_inc_SMS__c","Data_Revenue__c","cscfga__recurring_charge_product_discount_value__c","cscfga__one_off_charge_product_discount_value__c","Incoming_Revenue__c","Voice_Costs_inc_SMS__c","Other_Costs__c","Year_1_Other_Costs__c","Year_1_Incremental_Overheads__c","Incremental_Overheads__c","Volume__c","Year_1_Other_Overheads__c","Other_Overheads__c","BT_Total_Commission__c","Data_Costs_One_Off__c","Voice_Costs_inc_SMS_One_Off__c","Mobile_Voice_Recurring_Charge_Discounted__c","Mobile_Voice_One_Off_Charge_Discounted__c","SMS_Recurring_Charge_Discounted__c","SMS_One_Off_Charge_Discounted__c","Data_Recurring_Charge_Discounted__c","Data_One_Off_Charge_Discounted__c","Other_Recurring_Charge_Discounted__c","Other_One_Off_Charge_Discounted__c","Fixed_Recurring_Charge_Discounted__c","Fixed_One_Off_Charge_Discounted__c","Airtime_Fund__c","NPV_Tech_Fund__c","Tech_Fund_Total__c","Tech_Fund__c","BTOP_Tech_Fund__c","Year_1_Site_And_Sub_Install_Cost__c","Site_And_Sub_Install_Cost__c","Year_1_Site_And_Sub_Install__c","Site_And_Sub_Install__c","NPV_Monthly_Cost__c","NPV_Monthly_Revenue__c","NPV_Upfront_Cost__c","NPV_Upfront_Revenue__c","BT_Technology_Fund_Neg__c","NPV_Monthly_Revenue_Disc__c","NPV_Upfront_Revenue_Disc__c","GM_Recurring_Cost__c","GM_Upfront_Cost__c","cscfga__Quantity__c","Total_GM_Recurring_Cost__c","Total_GM_Upfront_Cost__c","PC_Total_Recurring_Charge__c","PC_Total_One_Off_Charge__c"]';
        INSERT reportConfig;
        pageRef.getParameters().put('basketId',basket.id);
       
            CS_CR_Basket_Calculations__c setting2 = new CS_CR_Basket_Calculations__c(
                Sync_Basket_Size_Limit__c = 1
            );
            //insert setting2;
            Test.startTest();
                CS_DiscountRedirectController DiscRedir= new CS_DiscountRedirectController();
                DiscRedir.doDiscountCalculations();
                DiscRedir.redirectToPage();   
            Test.stopTest();
        }
        Catch(exception e){}
    }
    
    public testMethod static void DiscRedirectTest5(){
        try{
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        PageReference pageRef = Page.CS_DiscountRedirect;
        test.setCurrentPageReference(pageRef);
        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BTnet');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        //pc.cscfga__Product_Family__c = 'BTnet';
        pc.Calculations_Product_Group__c = 'BT Net';
        pc.Volume__c = 10;
        INSERT pc;
        cspl__Report_Configuration__c reportCOnfig = new cspl__Report_Configuration__c(Name = 'configFields');
        reportCOnfig.cspl__value__c = '["Name","cscfga__One_Off_Charge__c","cscfga__Recurring_Charge__c","cspl__Month_Term__c","Calculations_Product_Group__c","Product_Definition_Name__c","Care_and_Chargable_Service_Revenue__c","Other_Revenue__c","Year_1_Other_Revenue__c","BT_Technology_Fund__c","Mobile_Voice_One_Off_Charge__c","Mobile_Voice_Recurring_Charge__c","SMS_Recurring_Charge__c","SMS_One_Off_Charge__c","Data_One_Off_Charge__c","Data_Recurring_Charge__c","Other_One_Off_Charge__c","Other_Recurring_Charge__c","Fixed_One_Off_Charge__c","Fixed_Recurring_Charge__c","Incoming_Recurring_Charge__c","Incoming_Recurring_Cost__c","Mobile_Voice_One_Off_Cost__c","Mobile_Voice_Recurring_Cost__c","Line_Rental__c","SMS_One_Off_Cost__c","SMS_Recurring_Cost__c","Data_One_Off_Cost__c","Data_Recurring_Cost__c","Other_One_Off_Cost__c","Other_Recurring_Cost__c","Fixed_One_Off_Cost__c","Fixed_Recurring_Cost__c","Device_One_Off_Cost__c","Device_Recurring_Cost__c","Device_One_Off_Charge__c","Device_One_Off_Charge_Discounted__c","Device_Recurring_Charge__c","Sales_Commision_One_Off_Cost__c","OPEX_Recurring_Cost__c","Service_Recurring_Cost__c","Rolling_Air_Time__c","Staged_Air_Time__c","Tech_Fund__c","Buy_out_cheque__c","Unconditional_cheque__c","cscfga__Contract_Term__c","Data_Costs__c","Year_1_Care_and_Chargable_Service_Costs__c","Care_and_Chargable_Service_Costs__c","Net_device_profit_cost__c","Product_Name__c","Total_Investment_Pot__c","Investment__c","Voice_Revenue_inc_SMS__c","Data_Revenue__c","cscfga__recurring_charge_product_discount_value__c","cscfga__one_off_charge_product_discount_value__c","Incoming_Revenue__c","Voice_Costs_inc_SMS__c","Other_Costs__c","Year_1_Other_Costs__c","Year_1_Incremental_Overheads__c","Incremental_Overheads__c","Volume__c","Year_1_Other_Overheads__c","Other_Overheads__c","BT_Total_Commission__c","Data_Costs_One_Off__c","Voice_Costs_inc_SMS_One_Off__c","Mobile_Voice_Recurring_Charge_Discounted__c","Mobile_Voice_One_Off_Charge_Discounted__c","SMS_Recurring_Charge_Discounted__c","SMS_One_Off_Charge_Discounted__c","Data_Recurring_Charge_Discounted__c","Data_One_Off_Charge_Discounted__c","Other_Recurring_Charge_Discounted__c","Other_One_Off_Charge_Discounted__c","Fixed_Recurring_Charge_Discounted__c","Fixed_One_Off_Charge_Discounted__c","Airtime_Fund__c","NPV_Tech_Fund__c","Tech_Fund_Total__c","Tech_Fund__c","BTOP_Tech_Fund__c","Year_1_Site_And_Sub_Install_Cost__c","Site_And_Sub_Install_Cost__c","Year_1_Site_And_Sub_Install__c","Site_And_Sub_Install__c","NPV_Monthly_Cost__c","NPV_Monthly_Revenue__c","NPV_Upfront_Cost__c","NPV_Upfront_Revenue__c","BT_Technology_Fund_Neg__c","NPV_Monthly_Revenue_Disc__c","NPV_Upfront_Revenue_Disc__c","GM_Recurring_Cost__c","GM_Upfront_Cost__c","cscfga__Quantity__c","Total_GM_Recurring_Cost__c","Total_GM_Upfront_Cost__c","PC_Total_Recurring_Charge__c","PC_Total_One_Off_Charge__c"]';
        INSERT reportConfig;
        pageRef.getParameters().put('basketId',basket.id);
       
            CS_CR_Basket_Calculations__c setting2 = new CS_CR_Basket_Calculations__c(
                Sync_Basket_Size_Limit__c = 20
            );
            insert setting2;
            Test.startTest();
                CS_DiscountRedirectController DiscRedir= new CS_DiscountRedirectController();
                DiscRedir.doDiscountCalculations();
                DiscRedir.redirectToPage();   
            Test.stopTest();
        }
        Catch(exception e){}
    }
    
}