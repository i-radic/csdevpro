@IsTest(SeeAllData=true)
public class CS_CloudVoice_LineCheckCtrl_Test { 

	static Account account {get; set;}
	static Order_Address__c orderAddress {get; set;}

	static void SetVariables(){
		account = Test_Factory.CreateAccount();     
        account.CUG__c = 'cugCV1';
        account.OwnerId = UserInfo.GetUserId() ;
        Database.SaveResult aR = Database.insert(account);

		orderAddress = new Order_Address__c(Account__c = account.id, 
											Building_Name__c = 'TEST', 
											Country__c = 'TEST', 
											County__c = 'TEST', 
											Locality__c = 'TEST', 
											NAD__c = 'TEST', 
											Number__c = 'TEST', 
											PO_Box__c = 'TEST', 
											Post_Code__c = 'TEST', 
											Post_Town__c = 'TEST', 
											Street__c = 'TEST', 
											Sub_Building__c = 'TEST');
		insert orderAddress;
	}

	static testMethod void UnitTest() {
		SetVariables(); 
		PageReference page = Page.CS_CloudVoice_SpeedTestEmail;
		Test.setCurrentPage(page);	
		ApexPages.currentPage().getParameters().put('addressId', orderAddress.id);
		CS_CloudVoice_LineCheckCtrl ctrl = new CS_CloudVoice_LineCheckCtrl();
        ctrl.lineResults();
	}

	static testMethod void UnitTest2() {
		PageReference page = Page.CS_CloudVoice_SpeedTestEmail;
		Test.setCurrentPage(page);	
		CS_CloudVoice_LineCheckCtrl ctrl = new CS_CloudVoice_LineCheckCtrl();
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        string errordescription = 'ERROR';
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>1</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setStatusCode(200);
        ctrl.httpRespTest = res;  
        ctrl.lineChange = '35';
        ctrl.pcChange ='';
        ctrl.nameNumChange ='';
        ctrl.fullAdd = '';
        ctrl.checkType = 'tel';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseforLineCheckerCTRL());
        ctrl.lineResults();        
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>1</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>2</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>3</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>4</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>5</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>6</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>7</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>8</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>9</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>10</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>11</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>12</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>13</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>14</ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');        
        res.setStatusCode(200);
        ctrl.httpRespTest = res;  
        ctrl.lineChange = '35';
        ctrl.pcChange ='';
        ctrl.nameNumChange ='';
        ctrl.fullAdd = '';
        ctrl.checkType = 'pc';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseforLineCheckerCTRL());
        ctrl.lineResults();
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><ERRORDETAILS><ERRORID>0<MAX>1<ADDRESS_DETAILS><Test>123<SUBPREMISES>Test</SUBPREMISES></Test>Test</ADDRESS_DETAILS><ADDRESS_MATCHED>0FALSE900</ADDRESS_MATCHED></MAX></ERRORID><ERRORCODE>ABC</ERRORCODE><ERRORTEXT>GET</ERRORTEXT><RESULTANT>UQWEI</RESULTANT></ERRORDETAILS>');
        res.setStatusCode(200);
        ctrl.httpRespTest = res;  
        ctrl.lineChange = '35';
        ctrl.pcChange ='';
        ctrl.nameNumChange ='';
        ctrl.fullAdd = '';
        ctrl.checkType = 'tel';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseforLineCheckerCTRL());
        ctrl.lineResults();
        ctrl.lineChange = '';
        ctrl.pcChange ='99';
        ctrl.nameNumChange ='76';
        ctrl.fullAdd = '35~36~76~56~16~78~97~12';
        ctrl.checkType = 'full';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseforLineCheckerCTRL());
        ctrl.lineResults();
	}
}