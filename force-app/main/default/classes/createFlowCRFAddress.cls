public with sharing class createFlowCRFAddress{

    public Flow_Address_Details__c addressDet{get; set;} 
    public string refferalPage;
    public string selectedAddr1 = ApexPages.currentPage().getParameters().get('AddType1');  
    
    public createFlowCRFAddress(ApexPages.StandardController stdController) {

         addressDet = (Flow_Address_Details__c)stdController.getRecord();
    }
    
    public createFlowCRFAddress(Flow_Address_Details__c addressDet_edit){
         addressDet = addressDet_edit;
    }
    
    public PageReference OnLoad() { 
         refferalPage = ApexPages.currentPage().getParameters().get('refPage');
        if(selectedAddr1=='ExistingAddress'){
         addressDet.Building_Name__c = '';
         addressDet.Sub_Building__c = '';
         addressDet.Number__c = '';
         addressDet.PO_Box__c = '';
         addressDet.street__c= '';
         addressDet.Locality__c = '';
         addressDet.Post_Town__c = '';
         addressDet.County__c = '';
         addressDet.Country__c = 'UK';
         }
         
         if(selectedAddr1=='CeaseAddress'){
          addressDet.Building_Name__c = '';
         addressDet.Sub_Building__c = '';
         addressDet.Number__c = '';
         addressDet.PO_Box__c = '';
         addressDet.street__c= '';
         addressDet.Locality__c = '';
         addressDet.Post_Town__c = '';
         addressDet.County__c = '';
         addressDet.Country__c = 'UK';
         }
         
         
         
         if(selectedAddr1=='CorrespondenceAddress'){
             addressDet.Building_Name__c = '';
             addressDet.Sub_Building__c = '';
             addressDet.Number__c = '';
             addressDet.PO_Box__c = '';
             addressDet.street__c= '';
             addressDet.Locality__c = '';
             addressDet.Post_Town__c = '';
             addressDet.County__c = '';
             addressDet.Country__c = 'UK';
         }
         if(selectedAddr1=='DeliveryAddress'){
          addressDet.Building_Name__c = '';
             addressDet.Sub_Building__c = '';
             addressDet.Number__c = '';
             addressDet.PO_Box__c = '';
             addressDet.street__c= '';
             addressDet.Locality__c = '';
             addressDet.Post_Town__c = '';
             addressDet.County__c = '';
             addressDet.Country__c = 'UK';
         }
         if(selectedAddr1=='BillingAddress'){
         addressDet.Building_Name__c = '';
             addressDet.Sub_Building__c = '';
             addressDet.Number__c = '';
             addressDet.PO_Box__c = '';
             addressDet.street__c= '';
             addressDet.Locality__c = '';
             addressDet.Post_Town__c = '';
             addressDet.County__c = '';
             addressDet.Country__c = 'UK';
         }
         
         
         
         if(selectedAddr1=='ProvideAddress'){
          addressDet.Building_Name__c = '';
             addressDet.Sub_Building__c = '';
             addressDet.Number__c = '';
             addressDet.PO_Box__c = '';
             addressDet.street__c= '';
             addressDet.Locality__c = '';
             addressDet.Post_Town__c = '';
             addressDet.County__c = '';
             addressDet.Country__c = 'UK';
         }
         if(selectedAddr1=='AlternateBillingAddress'){
          addressDet.Building_Name__c = '';
             addressDet.Sub_Building__c = '';
             addressDet.Number__c = '';
             addressDet.PO_Box__c = '';
             addressDet.street__c= '';
             addressDet.Locality__c = '';
             addressDet.Post_Town__c = '';
             addressDet.County__c = '';
             addressDet.Country__c = 'UK';
         }  
         return null;         
   }
   
   public PageReference Save() {
       If (ValidateMandatoryFields() != true){
            return null;
       }      
       try {                    
           string NADKey;
           if(Test_Factory.GetProperty('IsTest') == 'yes') {
               NADKey = 'test';
           }           
           else {
               NADKey = SalesforceCRMService.CreateNADAddress(crmAddress());
           }                
           if (NADKey == '' || 
               NADKey == null){                  
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The NAD service has rejected the request, please enter a valid address!'));                               
               return null;
           } 
           else {
               addressDet.Flow_CRF_address_Id__c = NADKey;
               if(Test_Factory.GetProperty('IsTest') == 'yes') {                         
               }
               else {
                   upsert addressDet;                 
               }    
               return ReDirect();                          
           }                  
       }
       catch (System.CalloutException ex){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There appears to be problem connecting to the NAD service, please try again!'));
           return null;
       }
   }
   
   /* -- singhd62
   public PageReference Save() {
       If (ValidateMandatoryFields() != true){
            return null;
       }      
       SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();
       service.timeout_x = 60000;
       try {                    
           string NADKey;
           if(Test_Factory.GetProperty('IsTest') == 'yes') {
               NADKey = 'test';
           }           
           else {
               NADKey = service.CreateNADAddress('', '', crmAddress());
           }                
           if (NADKey == '' ||
               NADKey == null){                  
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The NAD service has rejected the request, please enter a valid address!'));                               
               return null;
           } 
           else {
               addressDet.CRF_address_Id__c = NADKey;
               if(Test_Factory.GetProperty('IsTest') == 'yes') {                         
               }
               else {
                   upsert addressDet;                 
               }    
               return ReDirect();                          
           }                  
       }
       catch (System.CalloutException ex){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There appears to be problem connecting to the NAD service, please try again!'));
           return null;
       }
   }
   */
   public PageReference Cancel(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
        if (refferalPage == 'new'){
            PageReference PageRef = new PageReference('/apex/FlowCRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
                       
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }    
            return PageRef;                       
        }
        if (refferalPage == 'update'){
            PageReference PageRef = new PageReference('/apex/FlowCRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }   
            return PageRef;    
        }
        if (refferalPage == 'detail'){
            PageReference PageRef = new PageReference('/'+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }      
            return PageRef;  
        }
        return null;
    }
    /* singhd62
    public SalesforceServicesCRM.Address crmAddress(){
    
        SalesforceServicesCRM.Address crmAddress = new SalesforceServicesCRM.Address();
        if(selectedAddr1=='ExistingAddress'){
        crmAddress.Name = addressDet.Existing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Existing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Existing_Thoroughfare_No_Street_Number__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Existing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Existing_Locality__c;
        crmAddress.Town = addressDet.Existing_Post_Town__c;
        crmAddress.County = addressDet.Existing_County__c; 
        crmAddress.Country = addressDet.Existing_Country__c; 
        crmAddress.Postcode = addressDet.Existing_Post_Code__c;
        }
        
        if(selectedAddr1=='CeaseAddress'){
        crmAddress.Name = addressDet.Cease_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Cease_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Cease_Thoroughfare_Number_Street_Number__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Cease_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Cease_Locality__c;
        crmAddress.Town = addressDet.Cease_Post_Town__c;
        crmAddress.County = addressDet.Cease_County__c; 
        crmAddress.Country = addressDet.Cease_Country__c; 
        crmAddress.Postcode = addressDet.Cease_Post_Code__c;
        }
        
        
        
        
        if(selectedAddr1=='CorrespondenceAddress'){
        crmAddress.Name = addressDet.Legal_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Legal_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.LegalThoroughfare_Number_StreetNumber__c;
        crmAddress.POBox = addressDet.Legal_PO_Box__c;
        crmAddress.Street = addressDet.Legal_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Legal_Locality__c;
        crmAddress.Town = addressDet.Legal_Post_Town__c;
        crmAddress.County = addressDet.Legal_County__c; 
        crmAddress.Country = addressDet.Legal_Country__c; 
        crmAddress.Postcode = addressDet.Legal_Post_Code__c;
        }
        if(selectedAddr1=='DeliveryAddress'){
        crmAddress.Name = addressDet.Provide_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Provide_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Provide_Thoroughfare_Number_Street_Numb__c;
        crmAddress.POBox = addressDet.Provide_PO_Box__c;
        crmAddress.Street = addressDet.Provide_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Provide_Locality__c;
        crmAddress.Town = addressDet.Provide_Post_Town__c;
        crmAddress.County = addressDet.Provide_County__c; 
        crmAddress.Country = addressDet.Provide_Country__c; 
        crmAddress.Postcode = addressDet.Provide_Post_Code__c;
        }
        if(selectedAddr1=='BillingAddress'){
        crmAddress.Name = addressDet.Billing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Billing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Billing_Thoroughfare_Number_Street_Numb__c;
        crmAddress.POBox = addressDet.Billing_PO_Box__c;
        crmAddress.Street = addressDet.Billing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Billing_Locality__c;
        crmAddress.Town = addressDet.Billing_Post_Town__c;
        crmAddress.County = addressDet.Billing_County__c; 
        crmAddress.Country = addressDet.Billing_Country__c; 
        crmAddress.Postcode = addressDet.Billing_Post_Code__c;
        }
        
        
        
        if(selectedAddr1=='ProvideAddress'){
        crmAddress.Name = addressDet.Provide_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Provide_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Provide_Thoroughfare_Number_Street_Numb__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Provide_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Provide_Locality__c;
        crmAddress.Town = addressDet.Provide_Post_Town__c;
        crmAddress.County = addressDet.Provide_County__c; 
        crmAddress.Country = addressDet.Provide_Country__c; 
        crmAddress.Postcode = addressDet.Provide_Post_Code__c;
        }
        if(selectedAddr1=='AlternateBillingAddress'){    
        crmAddress.Name = addressDet.Billing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Billing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Billing_Thoroughfare_Number_Street_Numb__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Billing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Billing_Locality__c;
        crmAddress.Town = addressDet.Billing_Post_Town__c;
        crmAddress.County = addressDet.Billing_County__c; 
        crmAddress.Country = addressDet.Billing_Country__c; 
        crmAddress.Postcode = addressDet.Billing_Post_Code__c;
        }
        return crmAddress; 
    }
    */
    
    public SalesforceCRMService.CRMAddress crmAddress(){
    
        SalesforceCRMService.CRMAddress crmAddress = new SalesforceCRMService.CRMAddress();
        if(selectedAddr1=='ExistingAddress'){
        crmAddress.Name = addressDet.Building_Name__c;
        crmAddress.SubBuilding = addressDet.Sub_Building__c;
        crmAddress.BuildingNumber = addressDet.Number__c;
        crmAddress.POBox = addressDet.PO_Box__c;
        crmAddress.Street = addressDet.street__c;
        crmAddress.Locality = addressDet.Locality__c;
        crmAddress.Town = addressDet.Post_Town__c;
        crmAddress.County = addressDet.County__c; 
        crmAddress.Country = addressDet.Country__c; 
        crmAddress.Postcode = addressDet.Post_Code__c;
        }
        
        if(selectedAddr1=='CeaseAddress'){
        crmAddress.Name = addressDet.Building_Name__c;
        crmAddress.SubBuilding = addressDet.Sub_Building__c;
        crmAddress.BuildingNumber = addressDet.Number__c;
        crmAddress.POBox = addressDet.PO_Box__c;
        crmAddress.Street = addressDet.street__c;
        crmAddress.Locality = addressDet.Locality__c;
        crmAddress.Town = addressDet.Post_Town__c;
        crmAddress.County = addressDet.County__c; 
        crmAddress.Country = addressDet.Country__c; 
        crmAddress.Postcode = addressDet.Post_Code__c;
        }
        
        
        
        
        if(selectedAddr1=='CorrespondenceAddress'){
        crmAddress.Name = addressDet.Building_Name__c;
        crmAddress.SubBuilding = addressDet.Sub_Building__c;
        crmAddress.BuildingNumber = addressDet.Number__c;
        crmAddress.POBox = addressDet.PO_Box__c;
        crmAddress.Street = addressDet.street__c;
        crmAddress.Locality = addressDet.Locality__c;
        crmAddress.Town = addressDet.Post_Town__c;
        crmAddress.County = addressDet.County__c; 
        crmAddress.Country = addressDet.Country__c; 
        crmAddress.Postcode = addressDet.Post_Code__c;
        }
        
        if(selectedAddr1=='DeliveryAddress'){
        crmAddress.Name = addressDet.Building_Name__c;
        crmAddress.SubBuilding = addressDet.Sub_Building__c;
        crmAddress.BuildingNumber = addressDet.Number__c;
        crmAddress.POBox = addressDet.PO_Box__c;
        crmAddress.Street = addressDet.street__c;
        crmAddress.Locality = addressDet.Locality__c;
        crmAddress.Town = addressDet.Post_Town__c;
        crmAddress.County = addressDet.County__c; 
        crmAddress.Country = addressDet.Country__c; 
        crmAddress.Postcode = addressDet.Post_Code__c;
        }
        
        if(selectedAddr1=='BillingAddress'){
        crmAddress.Name = addressDet.Building_Name__c;
        crmAddress.SubBuilding = addressDet.Sub_Building__c;
        crmAddress.BuildingNumber = addressDet.Number__c;
        crmAddress.POBox = addressDet.PO_Box__c;
        crmAddress.Street = addressDet.street__c;
        crmAddress.Locality = addressDet.Locality__c;
        crmAddress.Town = addressDet.Post_Town__c;
        crmAddress.County = addressDet.County__c; 
        crmAddress.Country = addressDet.Country__c; 
        crmAddress.Postcode = addressDet.Post_Code__c;
        }
        
        
        
        if(selectedAddr1=='ProvideAddress'){
        crmAddress.Name = addressDet.Building_Name__c;
        crmAddress.SubBuilding = addressDet.Sub_Building__c;
        crmAddress.BuildingNumber = addressDet.Number__c;
        crmAddress.POBox = addressDet.PO_Box__c;
        crmAddress.Street = addressDet.street__c;
        crmAddress.Locality = addressDet.Locality__c;
        crmAddress.Town = addressDet.Post_Town__c;
        crmAddress.County = addressDet.County__c; 
        crmAddress.Country = addressDet.Country__c; 
        crmAddress.Postcode = addressDet.Post_Code__c;
        }
        
        if(selectedAddr1=='AlternateBillingAddress'){    
        crmAddress.Name = addressDet.Building_Name__c;
        crmAddress.SubBuilding = addressDet.Sub_Building__c;
        crmAddress.BuildingNumber = addressDet.Number__c;
        crmAddress.POBox = addressDet.PO_Box__c;
        crmAddress.Street = addressDet.street__c;
        crmAddress.Locality = addressDet.Locality__c;
        crmAddress.Town = addressDet.Post_Town__c;
        crmAddress.County = addressDet.County__c; 
        crmAddress.Country = addressDet.Country__c; 
        crmAddress.Postcode = addressDet.Post_Code__c;
        }
        return crmAddress; 
    }
    
    private boolean ValidateMandatoryFields() {
    
    if(selectedAddr1=='ExistingAddress'){
        if (addressDet.street__c == '' ||
            addressDet.street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Town__c == '' ||
            addressDet.Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Code__c == '' ||
            addressDet.Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.County__c == '' ||
            addressDet.County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Country__c == '' ||
            addressDet.Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    
    
    if(selectedAddr1=='CeaseAddress'){
        if (addressDet.street__c == '' ||
            addressDet.street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Town__c == '' ||
            addressDet.Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Code__c == '' ||
            addressDet.Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.County__c == '' ||
            addressDet.County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Country__c == '' ||
            addressDet.Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    
    
    
    
    if(selectedAddr1=='CorrespondenceAddress'){
        if (addressDet.street__c == '' ||
            addressDet.street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Town__c == '' ||
            addressDet.Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Code__c == '' ||
            addressDet.Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.County__c == '' ||
            addressDet.County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Country__c == '' ||
            addressDet.Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    if(selectedAddr1=='DeliveryAddress'){
        if (addressDet.street__c == '' ||
            addressDet.street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Town__c == '' ||
            addressDet.Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Code__c == '' ||
            addressDet.Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.County__c == '' ||
            addressDet.County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Country__c == '' ||
            addressDet.Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    if(selectedAddr1=='BillingAddress'){
        if (addressDet.street__c == '' ||
            addressDet.street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Town__c == '' ||
            addressDet.Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Code__c == '' ||
            addressDet.Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.County__c == '' ||
            addressDet.County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Country__c == '' ||
            addressDet.Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    
    
    
    if(selectedAddr1=='ProvideAddress'){
        if (addressDet.street__c == '' ||
            addressDet.street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Town__c == '' ||
            addressDet.Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Code__c == '' ||
            addressDet.Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.County__c == '' ||
            addressDet.County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Country__c == '' ||
            addressDet.Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }        
    if(selectedAddr1=='AlternateBillingAddress'){
        if (addressDet.street__c == '' ||
            addressDet.street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Town__c == '' ||
            addressDet.Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Post_Code__c == '' ||
            addressDet.Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.County__c == '' ||
            addressDet.County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Country__c == '' ||
            addressDet.Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
        return true;
    }
}