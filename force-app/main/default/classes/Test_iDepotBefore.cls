@isTest
private class Test_iDepotBefore {

static testMethod void runPositiveTestCases() {

Date TestDate1 = date.today();
Date TestDate2 = date.today()+1;

 // set user to non Admin / Dataloader     
     Profile prof = [SELECT Id FROM Profile WHERE Name = 'BTLB: Standard Team']; 
     
     User u1 = new User();
        u1.Username = 'test98745@bt.com';
        u1.Ein__c = '987654321';
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.ProfileId = prof.Id;
        u1.TimeZoneSidKey = 'Europe/London'; 
        u1.LocaleSidKey = 'en_GB';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'en_US';
        u1.Apex_Trigger_Account__c = false; 
        u1.Run_Apex_Triggers__c = false;
        insert u1;

     User u2 = new User();
        u2.Username = 'test98745ddd@bt.com';
        u2.Ein__c = '98765dddd';
        u2.LastName = 'TestLastname';
        u2.FirstName = 'TestFirstname';
        u2.MobilePhone = '07918672032';
        u2.Phone = '02085878834';
        u2.Title='What i do';
        u2.OUC__c = 'DKW';
        u2.Manager_EIN__c = '123456789';
        u2.Email = 'no.reply@bt.com';
        u2.Alias = 'boatid01';
        u2.ProfileId = prof.Id;
        u2.TimeZoneSidKey = 'Europe/London'; 
        u2.LocaleSidKey = 'en_GB';
        u2.EmailEncodingKey = 'ISO-8859-1';
        u2.LanguageLocaleKey = 'en_US';
        u2.Apex_Trigger_Account__c = false; 
        u2.Run_Apex_Triggers__c = false;
        insert u2;
                        
        Account acc1= Test_Factory.CreateAccountForDummy();
        insert acc1;

        Account acc2= Test_Factory.CreateAccountForDummy();
        insert acc2;        
        
        Opportunity opp1 = Test_Factory.CreateOpportunity(acc1.id);
        opp1.closedate = system.today();
        Database.SaveResult opptResult1 = Database.insert(opp1);

        Opportunity opp2 = Test_Factory.CreateOpportunity(acc2.id);
        opp2.closedate = system.today();
        Database.SaveResult opptResult2 = Database.insert(opp2);        
                 
        Contact con1 = Test_Factory.CreateContact();
        con1.CUG__c = acc1.CUG__c;
        con1.Contact_Post_Code__c = 'WR5 3RL';
        insert con1; 
             
        Contact con2 = Test_Factory.CreateContact();
        con2.CUG__c = acc2.CUG__c;
        con2.Contact_Post_Code__c = 'WR5 3RL';
        insert con2;                 
                
iDepot__c LS1 = new iDepot__c(); 
//validation of lookups
LS1.Account__c = acc1.id;
LS1.WSX_Opportunity__c = opp1.id;
LS1.WSX_Customer_Contact_Name__c=con1.id;
LS1.Assigned_to__c= u1.id;
LS1.WSX_Assigned_to_B2B__c= u1.id;
LS1.WSX_Assigned_to_Specialist__c= u1.id;
LS1.WSX_BTLB_Contact_Name__c= u1.id;


//VOL ref for validation
LS1.Order_Reference__c = 'VOL011-123456789';
LS1.Commission_Due__c=10;
LS1.Outstanding_Commission_Due__c=10;
LS1.WSX_Annual_Maintenance_Charge__c=10;
LS1.WSX_Annual_Rental_Charge__c=10;
LS1.WSX_Connection_Charge__c=10;
LS1.WSX_Verified_Annual_Maintenance_Charge__c=10;
LS1.WSX_Verified_Annual_Rental_Charge__c=10;
LS1.WSX_Verified_Connection_Charge__c=10;
LS1.Cobra_Payment__c=TestDate1;
LS1.Date_Scheduled__c=TestDate1;
LS1.From__c=TestDate1;
LS1.To__c=TestDate1;
LS1.WSX_Commissions_Month__c=TestDate1;
LS1.WSX_Contract_signed__c=TestDate1;
LS1.WSX_Next_Update__c=TestDate1;
LS1.WSX_Internal_Notes__c='T 12 3 3';
LS1.WSX_Quantity__c=10;
LS1.WSX_Verified_Quantity__c=10;
LS1.Product_Area__c='T3';
LS1.Status__c='T3';
LS1.Sub_Status__c='T3';
LS1.Type_of_Payment__c='T3';
LS1.Year_Covered__c='T3';
LS1.WSX_Contact_Details__c='T3';
LS1.WSX_Product__c='T3';
LS1.WSX_Product_Category__c='T3';
LS1.WSX_Status__c='T3';
LS1.WSX_Sub_Status__c='T3';
LS1.WSX_Term_Years__c='T3';
LS1.WSX_Verified_on_Scars__c='T3';
LS1.WSX_Verified_Term__c='T3';
LS1.WSX_Route_To_Market__c='T3';
insert LS1;

//check for setting new data to null
LS1.Priority_Case__c=True;
LS1.WSX_Deleted__c=True;
LS1.WSX_Finance__c=True;
LS1.Commission_Due__c=null;
LS1.Outstanding_Commission_Due__c=null;
LS1.WSX_Annual_Maintenance_Charge__c=null;
LS1.WSX_Annual_Rental_Charge__c=null;
LS1.WSX_Connection_Charge__c=null;
LS1.WSX_Verified_Annual_Maintenance_Charge__c=null;
LS1.WSX_Verified_Annual_Rental_Charge__c=null;
LS1.WSX_Verified_Connection_Charge__c=null;
LS1.Cobra_Payment__c=null;
LS1.Date_Scheduled__c=null;
LS1.From__c=null;
LS1.To__c=null;
LS1.WSX_Commissions_Month__c=null;
LS1.WSX_Contract_signed__c=null;
LS1.WSX_Next_Update__c=null;
LS1.WSX_Quantity__c=null;
LS1.WSX_Verified_Quantity__c=null;
LS1.Commission_Due__c=null;
LS1.Outstanding_Commission_Due__c=null;
LS1.WSX_Annual_Maintenance_Charge__c=null;
LS1.WSX_Annual_Rental_Charge__c=null;
LS1.WSX_Connection_Charge__c=null;
LS1.WSX_Verified_Annual_Maintenance_Charge__c=null;
LS1.WSX_Verified_Annual_Rental_Charge__c=null;
LS1.WSX_Verified_Connection_Charge__c=null;
update LS1;

//check changing data from null
LS1.Priority_Case__c=False;
LS1.WSX_Deleted__c=False;
LS1.WSX_Finance__c=False;
LS1.Commission_Due__c=20;
LS1.Outstanding_Commission_Due__c=20;
LS1.WSX_Annual_Maintenance_Charge__c=20;
LS1.WSX_Annual_Rental_Charge__c=20;
LS1.WSX_Connection_Charge__c=20;
LS1.WSX_Verified_Annual_Maintenance_Charge__c=20;
LS1.WSX_Verified_Annual_Rental_Charge__c=20;
LS1.WSX_Verified_Connection_Charge__c=20;
LS1.Cobra_Payment__c=TestDate2;
LS1.Date_Scheduled__c=TestDate2;
LS1.From__c=TestDate2;
LS1.To__c=TestDate2;
LS1.WSX_Commissions_Month__c=TestDate2;
LS1.WSX_Contract_signed__c=TestDate2;
LS1.WSX_Next_Update__c=TestDate2;
LS1.WSX_Internal_Notes__c='T 12 3 fsdfsdf3';
LS1.WSX_Quantity__c=20;
LS1.WSX_Verified_Quantity__c=20;
LS1.Product_Area__c='T1';
LS1.Status__c='T1';
LS1.Sub_Status__c='T1';
LS1.Type_of_Payment__c='T1';
LS1.Year_Covered__c='T1';
LS1.WSX_Contact_Details__c='T1';
LS1.WSX_Product__c='T1';
LS1.WSX_Product_Category__c='T1';
LS1.WSX_Status__c='T1';
LS1.WSX_Sub_Status__c='T1';
LS1.WSX_Term_Years__c='T1';
LS1.WSX_Verified_on_Scars__c='T1';
LS1.WSX_Verified_Term__c='T1';
LS1.WSX_Route_To_Market__c='T1';
update LS1;

// test lookup changes
LS1.Account__c = acc2.id;
LS1.WSX_Opportunity__c = opp2.id;
LS1.WSX_Customer_Contact_Name__c=con2.id;
LS1.Assigned_to__c= u2.id;
LS1.WSX_Assigned_to_B2B__c= u2.id;
LS1.WSX_Assigned_to_Specialist__c= u2.id;
LS1.WSX_BTLB_Contact_Name__c= u2.id;
update LS1;


}

}