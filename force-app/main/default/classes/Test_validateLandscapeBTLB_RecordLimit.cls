@isTest

private class Test_validateLandscapeBTLB_RecordLimit{
    static testMethod void runPositiveTestCases(){
        Test_Factory.SetProperty('IsTest', 'yes');

        Test_Factory.CreateAccountForDummy();

        String SACCODE='TESTSAC';   
        String sacname = 'TestName';   
        String LOB='FIELDM';   
        
        Account testAcc = Test_Factory.CreateAccount();
        testAcc.name = 'TESTCODE';
        testAcc.sac_code__c=SACCODE;
        testAcc.Sector_code__c='CORP';
        testAcc.LOB_Code__c=LOB;
        Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc});

        Account testAcc2 = Test_Factory.CreateAccount();
        testAcc2.name = 'TESTCODE 2';
        testAcc2.sac_code__c=SACCODE;
        testAcc2.Sector_code__c='CORP';
        testAcc2.LOB_Code__c=LOB;
        Database.SaveResult[] accResult2 = Database.insert(new Account[] {testAcc2});

        Landscape_BTLB__c landscape = Test_Factory.CreateLandscapeBTLB();
        landscape.Customer__c = accResult[0].getId();
        insert landscape;

        Landscape_BTLB__c landscape2 = Test_Factory.CreateLandscapeBTLB();
        landscape2.Customer__c = accResult[0].getId();
        try {
            insert landscape2;
        }
        catch (Exception e) {
        }

        PageReference pageRef = Page.validateLandscapeBTLB_RecordLimit;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('retURL', '/' + accResult2[0].getId());

        ApexPages.StandardController stc = new ApexPages.StandardController(landscape );

        validateLandscapeBTLB_RecordLimit limiter = Test_Factory.CreateValidateLandscapeBTLB_RecordLimit(stc);

        validateLandscapeBTLB_RecordLimit limiter2 = Test_Factory.CreateValidateLandscapeBTLB_RecordLimit(null);

        limiter.account = Test_Factory.CreateAccount(accResult[0].getId());
        limiter.landscape = landscape;
        system.debug('TESTJB 2:' + accResult[0].getId());
        Account tt = [select id, name from account where id = :accResult[0].getId()];
        limiter.OnLoad(accResult[0].getId());
        limiter.OnLoad();
    }
}