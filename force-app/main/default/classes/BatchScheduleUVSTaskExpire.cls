global class BatchScheduleUVSTaskExpire implements Schedulable{
    global void execute(SchedulableContext sc) {
        Date myDate = (Date.today()- 7);
        String sDate = String.valueOf(myDate);
        BatchTaskExpire b = new BatchTaskExpire('SELECT Id FROM Task WHERE isClosed = false  and Qtr_End_Date__c < '+ sDate +'  AND Task_Category__c = \'UVS Defence Portal\'');
        database.executebatch(b, 200);
    }
}