public with sharing class TestNIBRNIER {
 public static boolean isNIBRNIERData = false; 
 public static Pricebook2 stdPb ;
 public static Account account;
 public static Opportunity opp;
 public static Product2 p, p2;
 public static Group newGroup;
 public static PricebookEntry pbe, pbe2;
 public static OpportunityLineItem oli;
 
    static{
        
        //Get the standard pricebook
        stdPb = [select Id from Pricebook2 where isStandard=true limit 1];
        
        //Create an Account
        account = new Account(name='Test Account 1');
        insert account;
        
        //Create an Opportunity
        //ROS - 24/01/2013 Opportunity field cleanse
        //opp = new Opportunity(name='Test Opp1',NIBR_Year_2009_10__c=1,type='New Business',stagename='Created',closedate=system.today(),accountid=account.id);
        opp = new Opportunity(name='Test Opp1',type='New Business',stagename='Created',closedate=system.today(),accountid=account.id);
        
        insert opp; 
        
        String thisSupplier = 'MainSupplier';
        
        //Create Product 1
        p = new Product2(name='TestProd1', Description = 'test product',Family ='ICT',Part_Number__c='TET2401418', supplier__c = thisSupplier);
        insert p;               
        
        //Create Product 2
        p2 = new Product2(name='TestProd1', Description = 'test product',Family ='ICT',Part_Number__c='TST2401418', supplier__c = thisSupplier);
        insert p2;
        
        newGroup = new Group(name=thisSupplier);
        insert newGroup;              
        
        //Create pricebook entry 1
        pbe =  new PricebookEntry(pricebook2id = stdPb.id, product2id = p.id, unitprice=1.0, isActive=true);
        insert pbe;
        
        //Create pricebook entry 1
        pbe2 =  new PricebookEntry(pricebook2id = stdPb.id, product2id = p2.id, unitprice=1.0, isActive=true);
        insert pbe2;
        
        //Insert opportunity line item 1
        oli = new OpportunityLineItem (OpportunityId = opp.Id, description = 'opp1',PricebookEntryId = pbe.Id, Quantity = 1, UnitPrice = 20000,category__c = 'Main',contract_term__c='24 Months',billing_cycle__c='Monthly',service_ready_date__c = system.today().addmonths(1),first_bill_date__c = system.today().addmonths(2),initial_cost__c = 4000);
        system.debug('JBRUNONVCE before one:' + NIBRandNIERCalculator.runopplineonce);
        insert oli;
        system.debug('JBRUNONVCEafter one:' + NIBRandNIERCalculator.runopplineonce);
        //Insert opportunity line item 2
        
    }
    
    public static void createNIBRNIERTestData(){
        
            try{
                
                
                List <Product_Revenue__c> listProdRev = [Select name from Product_Revenue__c where opportunity__c = :opp.Id];
                update listProdRev;
                
                
                OpportunityLineItem oli2 = new OpportunityLineItem (OpportunityId = opp.Id, description = 'opp2', PricebookEntryId = pbe2.Id, Quantity = 1, UnitPrice = 20000,category__c = 'Standard',contract_term__c='24 Months',billing_cycle__c='Monthly',service_ready_date__c = system.today().addmonths(1),first_bill_date__c = system.today().addmonths(2),initial_cost__c = 5000);
                NIBRandNIERCalculator.runopplineonce=false;
                insert oli2;
                system.debug('JBRUNONVCE after two:' + NIBRandNIERCalculator.runopplineonce);
                
                //NIBRandNIERCalculator.runopplineonce=false;
                delete oli2;
                
                system.debug('JBRUNONVCEafter delete:' + NIBRandNIERCalculator.runopplineonce);
                NIBRandNIERCalculator.runopplineonce=false;
                delete oli;
                
                
             }catch(Exception e){
                System.debug( 'NIBNIER TEST EXCEPTION' + e );
            }   
        
            TestNIBRNIER.isNIBRNIERData =true;
            
    }
    
    static testmethod void testSalesJourneyCreateAndUpdate(){
      
          //Setup the Sales Journey Test Data
          ict_sales_journey__c ictrec = new ict_sales_journey__c();
          
          System.debug('Starting testSalesJourneyCreateandUpdate');
        
          Test.startTest();
          if( !TestNIBRNIER.isNIBRNIERData) TestNIBRNIER.createNIBRNIERTestData();
        
          
         Test.stoptest();
    }  
}