global with sharing class CustomButtonSubmitForApprovalCorp implements csdiscounts.ICustomButton{ 
	private static String CONTROLLER_NAME = 'CustomButtonSubmitForApprovalCorp';

	global String performAction(String basketId){
		List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
		Id currentUser = UserInfo.getUserId();

		try {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval');
            req1.setObjectId(basketId);
            req1.setSubmitterId(currentUser);
            requests.add(req1);
            
            Approval.ProcessResult[] processResults = Approval.process(requests, true);
		}
		catch (System.DmlException e) {
			System.debug(LoggingLevel.INFO, 'CustomButtonSubmitForApproval.Exception = ' + e.getMessage());
			return '{ "message":"All good", "status":200}';
		}

		return '{ "message":"All good", "status":200}';
	}
}