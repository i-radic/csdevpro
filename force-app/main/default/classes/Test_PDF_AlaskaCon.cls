/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
                    
private class Test_PDF_AlaskaCon{

    static testMethod void TestAlaskaPDF() {
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     	
    Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B3B2', email = 'B2B17cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2B123Profile13167cr@bt.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;

        Account accnt = Test_Factory.CreateAccount();
        accnt.name = 'TESTCODE 3';
        accnt.sac_code__c = 'test1SAC';
        accnt.Sector_code__c = 'CORP';
        accnt.LOB_Code__c = 'LOB';
        accnt.OwnerId = u1.Id;
        accnt.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(accnt);
        
        Opportunity O = new Opportunity();
        O.StageName = 'Created';
        O.Name = 'Test_Flow_Opp';
        O.CloseDate = Date.Today()+1;
        O.AccountId = accnt.id;
        Insert O;
        
        Flow_CRF__c CRF = new Flow_CRF__c();
        CRF.Account__c = accnt.id;
        CRF.Opportunity__c = O.id;
        insert CRF;
        
        Flow_Address_Details__c AddressDetails = new Flow_Address_Details__c();
        
        AddressDetails.Alternate_Contact_Number__c = '+0012365478';
        AddressDetails.Flow_CRF__c = CRF.id;
        insert AddressDetails;
        
        ApexPages.StandardController sc = new ApexPages.StandardController(CRF);
        PDF_AlaskaCon AlaskaPDFExt = new PDF_AlaskaCon(sc);
        }
    }
}