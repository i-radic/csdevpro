public with sharing class CRFLinestoMove {

public Lines_to_Move__c LM{get;set;}

    public CRFLinestoMove(ApexPages.StandardController stdcontroller) {
     this.LM= (Lines_to_Move__c)stdController.getRecord();
    }
    
    Public PageReference OnStart(){    
    List<CRF__c> crf = [Select Id,Name,RecordTypeId from CRF__c where Id=:LM.Related_to_CRF__c];
    List<Flow_CRF__c> Flowcrf = [Select Id,Name,RecordTypeId from Flow_CRF__c where Id=:LM.Related_to_Flow_CRF__c];
    if(crf.size()>0){
    Id crfId= crf[0].Id;
    String crfName= crf[0].Name;
    Id crfRTId= crf[0].RecordTypeId;
        
    String crfRTName = [Select DeveloperName from RecordType where Id=:crfRTId].DeveloperName;
    String LMMoversRTId = [Select Id from RecordType where DeveloperName=:'LinestoMove_Movers'].Id;
    String LMMoverswithBBRTId = [Select Id from RecordType where DeveloperName=:'LinestoMove_MoversWithBB'].Id;  
    String LMBusMoverRTId = [Select Id from RecordType where DeveloperName=:'Business_Movers'].Id;    
    
    if(crfRTName == 'Movers'){ 
    /* https://cs4.salesforce.com/a1z/e?CF00N20000002oGvG={!CRF__c.Name}&CF00N20000002oGvG_lkid={!CRF__c.Id}&RecordType=01220000000AGAX&ent=01I200000007TPA&saveURL=/{!CRF__c.Id}&retURL=/{!CRF__c.Id} */    
    PageReference pageRef = new PageReference( '/a1z/e?'+'CF00N20000002oGvG='+crfName+'&CF00N20000002oGvG_lkid='+crfId+'&RecordType='+LMMoversRTId+'&ent=01I200000007TPA&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1' );
    Return pageRef;
    }
    
    else if(crfRTName == 'Mover_with_existing_BB'){    
    PageReference pageRef = new PageReference( '/a1z/e?'+'CF00N20000002oGvG='+crfName+'&CF00N20000002oGvG_lkid='+crfId+'&RecordType='+LMMoverswithBBRTId+'&ent=01I200000007TPA&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1' );
    Return pageRef;
    }
    
    else if(crfRTName == 'Business_Mover'){    
    PageReference pageRef = new PageReference( '/a1z/e?'+'CF00N20000002oGvG='+crfName+'&CF00N20000002oGvG_lkid='+crfId+'&RecordType='+LMBusMoverRTId+'&ent=01I200000007TPA&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1' );
    Return pageRef;
    }
    
    else return null;
    
    }
    if(Flowcrf.size()>0){
    Id crfId= Flowcrf[0].Id;
    String crfName= Flowcrf[0].Name;
    Id crfRTId= Flowcrf[0].RecordTypeId;
        
    String crfRTName = [Select DeveloperName from RecordType where Id=:crfRTId].DeveloperName;
    String LMMoversRTId = [Select Id from RecordType where DeveloperName=:'LinestoMove_Movers'].Id;
    String LMMoverswithBBRTId = [Select Id from RecordType where DeveloperName=:'LinestoMove_MoversWithBB'].Id;  
    String LMBusMoverRTId = [Select Id from RecordType where DeveloperName=:'Business_Movers'].Id;    
    
    if(crfRTName == 'Movers'){ 
    /* https://cs4.salesforce.com/a1z/e?CF00N20000002oGvG={!CRF__c.Name}&CF00N20000002oGvG_lkid={!CRF__c.Id}&RecordType=01220000000AGAX&ent=01I200000007TPA&saveURL=/{!CRF__c.Id}&retURL=/{!CRF__c.Id} */    
    PageReference pageRef = new PageReference( '/a1z/e?'+'CF00N20000009AkeT='+crfName+'&CF00N20000009AkeT_lkid='+crfId+'&RecordType='+LMMoversRTId+'&ent=01I200000007TPA&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1' );
    Return pageRef;
    }
    
    else if(crfRTName == 'MoverswithexistingBB'){    
    PageReference pageRef = new PageReference( '/a1z/e?'+'CF00N20000009AkeT='+crfName+'&CF00N20000009AkeT_lkid='+crfId+'&RecordType='+LMMoverswithBBRTId+'&ent=01I200000007TPA&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1' );
    Return pageRef;
    }
    
    else if(crfRTName == 'BusinessMovers'){    
    PageReference pageRef = new PageReference( '/a1z/e?'+'CF00N20000009AkeT='+crfName+'&CF00N20000009AkeT_lkid='+crfId+'&RecordType='+LMBusMoverRTId+'&ent=01I200000007TPA&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1' );
    Return pageRef;
    }
    
    else return null;
    
    }
    else return null;
    }
}