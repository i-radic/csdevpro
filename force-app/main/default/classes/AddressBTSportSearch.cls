public with sharing class AddressBTSportSearch {

    public BT_Sport__c bt_sport;  
    public Boolean test= true;
    public string refferalPage = ApexPages.currentPage().getParameters().get('Id');
    public string postcode = ApexPages.currentPage().getParameters().get('Pc');
    List<Addresswrapper> ResultsList = new List<Addresswrapper>();
    List<SalesforceCRMService.CRMAddress> selectedAddress = new List<SalesforceCRMService.CRMAddress>(); //singhd62
    
    public Boolean getTest(){
        return test;
    }
    
    public AddressBTSportSearch (BT_Sport__c bt_sport_edit){
         bt_sport = bt_sport_edit;
    }
    
    public PageReference CreateAddress(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }
        PageReference PageRef = new PageReference('/apex/createAddress?id='+bt_sport__c.Id+'&refPage='+refferalPage);
        PageRef.setRedirect(true);      
        return PageRef;                 
    }
    
    public PageReference Back(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
       
            PageReference PageRef = new PageReference('/'+ bt_sport.Id);
            PageRef.setRedirect(true);
                       
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }    
            return PageRef;                       
     
        
         return null;
    }
     
    public AddressBTSportSearch (ApexPages.StandardController stdController) {

         bt_sport = (BT_Sport__c)stdController.getRecord();
    }

    public List<Addresswrapper> getAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceCRMService.CRMAddressList NADResponse = null;
                    
            
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceCRMService.CRMAddressList();
            NADResponse.Addresses = new SalesforceCRMService.CRMArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceCRMService.CRMAddress[0];
                    
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            system.debug('adddddddd'+NADResponse.Addresses.Address);
            
            return null;               
        }
        else{
            try {
                NADResponse = SalesforceCRMService.GetNADAddress(postCode);
            }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;
            }                                              
        }                                       
            
        for(SalesforceCRMService.CRMAddress sfsAddress : NADResponse.Addresses.Address){
            
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();
                    
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
                
            ResultsList.add(new Addresswrapper(a));                                                         
        }
        if(ResultsList.size()>1){        
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please click back button and enter manually.'));          
             return null;
        }
    }    
    /*
    public List<Addresswrapper> getAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceServicesCRM.AddressList NADResponse = null;
        SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();            
            
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceServicesCRM.AddressList();
            NADResponse.Addresses = new SalesforceServicesCRM.ArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceServicesCRM.Address[0];
                    
            SalesforceServicesCRM.Address a = new SalesforceServicesCRM.Address();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                service.timeout_x = 60000;
                NADResponse = Endpoints.SFCRMGateway().GetNADAddress('', '', postCode);
            }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;
            }                                              
        }                                       
            
        for(SalesforceServicesCRM.Address sfsAddress : NADResponse.Addresses.Address){
            
            Address a = new Address();
                    
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
                
            ResultsList.add(new Addresswrapper(a));                                                         
        }
        if(ResultsList.size()>1){        
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please click back button and enter manually.'));          
             return null;
        }
    }
    */
    public PageReference getSelected()
    {
        selectedAddress.clear();
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();
            
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
                
            Addresswrapper wrapper = new Addresswrapper(a);
            wrapper.selected = true;
            ResultsList.add(wrapper);                        
        }        
        
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                
                    bt_sport.Building_Name__c = accwrapper.acc.Name;
                    bt_sport.Building_Number__c = accwrapper.acc.BuildingNumber;
                    bt_sport.Street__c = accwrapper.acc.Street;
                    bt_sport.Town__c = accwrapper.acc.Town;
                    bt_sport.County__c= accwrapper.acc.County;
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update bt_sport;
                        Return ReDirect();
                    }    
        }    
        return null;
    }
    
    public List<SalesforceCRMService.CRMAddress> getselectedAddress()
    {
        if(selectedAddress.size()>0){
            return selectedAddress;
        }
        else
        return null;
    }    
    
    public with sharing class Addresswrapper
    {
        public SalesforceCRMService.CRMAddress acc {get; set;}
        public Boolean selected {get; set;}
        public Addresswrapper(SalesforceCRMService.CRMAddress a)
        {
            acc = a;
            selected = false;
        }
    }
}