Global class ConvertAsOrder {
    /* Author:Teja CR10002::CREATED::02/03/2018:16:55 */
    Webservice static ID getOpportunity(Id opp_id) {
        System.debug('Input ID is : '+opp_id);
        List<Opportunity> input_id_result = [select id,CloseDate,AccountId,Opportunity_Id__c,Customer_Signature_Date__c,OwnerId from Opportunity where id =: opp_id];
        List<Order__c> Order_List = new List<Order__c>();
        for(Opportunity opp : input_id_result) {
            Order__c ord = new Order__c();
            ord.Customer_Signature_date__c = opp.Customer_Signature_Date__c;
            ord.Opportunity_ID__c = opp.Id;
            ord.OpportunityID__c = opp.Opportunity_Id__c;
            ord.Account_Name__c = opp.AccountId;
			ord.Account_Director_Name__c = opp.OwnerId;
            Order_List.add(ord);
        }
        try {
        	insert Order_List;
            return Order_List.get(0).id;
        } catch(DmlException DE) {
        	System.debug(DE.getMessage());
            return null;
        }
    }
}