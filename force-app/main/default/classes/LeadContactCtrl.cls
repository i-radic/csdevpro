public class LeadContactCtrl{ 

private ApexPages.StandardController stdCtrl {get; set;}
  
public List<Contact> conts {get; set;}
public List<Account> accs{get; set;}
public List<Contact> cons{get; set;}
   
public ID LeadId = ApexPages.currentPage().getParameters().get('id');

public Boolean refreshPage {get; set;}

public LeadContactCtrl(ApexPages.StandardController std) {   
    stdCtrl=std;
    setupContacts();
    viewAccs();
    viewCons();  
}


List<Lead> acc = [SELECT Account__c, Contact__c FROM Lead WHERE Id = :LeadId LIMIT 1];
public string leadacc = acc[0].Account__c;
public string leadcon = acc[0].Contact__c;
public Integer countAcc{get; set;}
private void setupContacts() {
    if(leadacc != Null){
        conts=[SELECT Id, Name, Email, Phone, Account_Manager_Contact_Flag__c, Job_Function__c, Job_Title__c, Contact_Key_Decision_Maker__c, 
        Target_Contact__c, Last_Updated_Date__c, LastModifiedDate FROM Contact 
        WHERE AccountId = :leadacc AND Status__c = 'Active' 
        ORDER BY Target_Contact__c DESC, Account_Manager_Contact_Flag__c, Contact_Key_Decision_Maker__c, Last_Updated_Date__c DESC];
    }
}

public String selectedID {get;set;} 

public void viewAccs(){
    List<Lead> l= [Select Id, Name, Company, Account__c From Lead Where Id = :LeadID LIMIT 1];
    if(l[0].Account__c == Null){
        Integer lenName = l[0].Company.length();
        if(lenName > 5){
            lenName = 5;
        }    
        String strName = l[0].Company;
        strName = strName.replaceFirst('the ','');strName = strName.replaceFirst('The ','');
        String strName1 = '%'+ strName + '%';
        String strName2 = '%'+ strName.substring(0,lenName)+ '%';
        String strName3 = '%'+ strName.substringBefore(' ')+ '%';
        
        List<Account> accs1a= new List<Account>([Select Id, Owner.Name, Name, LastModifiedDate, PostCode__c, Sub_Sector__c, Phone__c From Account Where Name Like :strName ORDER BY PostCode__c LIMIT 30 ]);
        List<Account> accs1b= new List<Account>([Select Id, Owner.Name, Name, LastModifiedDate, PostCode__c, Sub_Sector__c, Phone__c From Account Where Name Like :strName1 ORDER BY PostCode__c LIMIT 50 ]);
        //List<Account> accs2= new List<Account>([Select Id, Owner.Name, Name, LastModifiedDate, PostCode__c, Sub_Sector__c, Phone__c From Account Where Name Like :strName2 ORDER BY PostCode__c LIMIT 20 ]);
        //List<Account> accs3= new List<Account>([Select Id, Owner.Name, Name, LastModifiedDate, PostCode__c, Sub_Sector__c, Phone__c From Account Where Name Like :strName3 ORDER BY PostCode__c LIMIT 20 ]);
        List<Account> MasterList= new List<Account>();
        MasterList.addall(accs1a);
        MasterList.addall(accs1b);
        //MasterList.addall(accs2);
        //MasterList.addall(accs3);
            
        Set<Account> myset = new Set<Account>();
        List<Account> result = new List<Account>();
        for (Account s : MasterList) {
          if (myset.add(s)) {
            result.add(s);
          }
        }
        accs = result;
        countAcc = accs.size();
    }        
}    

public void viewCons(){
    List<Lead> l= [Select Account__c From Lead Where Id = :LeadID LIMIT 1];
    if(l[0].Account__c != Null){
        //cons= [Select Id, Name, LastModifiedDate From Contact Where AccountId = :l[0].Account__c ];
        cons = [SELECT Id, Name, Email, Phone, Account_Manager_Contact_Flag__c, Job_Function__c, Job_Title__c, Contact_Key_Decision_Maker__c, 
        Target_Contact__c, Last_Updated_Date__c, LastModifiedDate FROM Contact 
        WHERE AccountId = :leadacc AND Status__c = 'Active' 
        ORDER BY Name, Last_Updated_Date__c DESC];
    }
}
public PageReference selAcc(){  
    List<Lead> l = [SELECT Account__c FROM Lead WHERE Id = :LeadID LIMIT 1];
    system.debug('JMM Lead: ' + selectedID);
    l[0].Account__c= selectedID;
    update l;
    refreshPage=true;
    //viewAccs();
    return null;
}  
public PageReference selCon(){
    List<Lead> l = [SELECT Contact__c FROM Lead WHERE Id = :LeadID LIMIT 1];
    system.debug('JMM Lead: ' + selectedID);
    l[0].Contact__c= selectedID;
    update l;
    refreshPage=true;
    return null;
}    
public PageReference save(){  
    List<Lead> l = [SELECT Contact__c FROM Lead WHERE Id = :LeadId LIMIT 1];
    system.debug('JMM Lead: ' + selectedID);
    l[0].Contact__c= selectedID;//'003P000000XjJvZ';
    update l;
    refreshPage=true;
    setupContacts();
    return null;
    
    //PageReference jmmPage = new PageReference('/apex/LeadContact?'+LeadId+'&refreshPage=true');
    //system.debug('JMM Lead: ' + jmmPage );
    //return jmmPage;
}

}