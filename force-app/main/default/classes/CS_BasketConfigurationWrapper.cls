public with sharing class CS_BasketConfigurationWrapper {
	public Id basketId;
	public cscfga__Product_Basket__c basket;
	public Set<cscfga__Product_Configuration__c> prodConfigSet;
	public Map<String, Set<cscfga__Product_Configuration__c>> basketProductFamilyMap;
	public Map<String, Decimal> productFamilyThresholdsMap;
	public Map<String, Decimal> productFamilyNPVThresholdsMap;
	public Map<String, Decimal> productFamilyDilutedThresholdsMap;

	public CS_BasketConfigurationWrapper(cscfga__Product_Basket__c b){
		basketId = b.Id;
		basket = b;
		prodConfigSet = new Set<cscfga__Product_Configuration__c>();
		basketProductFamilyMap = new Map<String, Set<cscfga__Product_Configuration__c>>();
		productFamilyThresholdsMap = new Map<String, Decimal>();
		productFamilyNPVThresholdsMap = new Map<String, Decimal>();
		productFamilyDilutedThresholdsMap = new Map<String, Decimal>();
	}
}