global class CS_CustomLookupProductType extends cscfga.ALookupSearch{

    public override String getRequiredAttributes(){
        return '["Basket ID", "Account Postcode"]';
    }

    public override Object[] doLookupSearch(Map<String, String>
            searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset,
            Integer pageLimit){

        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
            
        String basketId = searchFields.get('Basket ID');
        String postcode = searchFields.get('Account Postcode');
        List<String> definitionsList = new List<String>{'BT Mobile Sharer', 'BT Mobile Flex'};
        String filter = '';
        String searchTerm = searchFields.get('searchValue');
        if(searchTerm != ''){
            filter = ' AND Name LIKE \'%' + searchTerm + '%\' ';
        }

        List<cscfga__Product_Configuration__c> pcs = [SELECT Id, Name, Volume__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId AND Product_Definition_Name__c IN :definitionsList];
        List<BT_Extra_Product_Type__c> ProductTypes = Database.query('SELECT ' + CS_Utils.getSobjectFields('BT_Extra_Product_Type__c') + ' FROM BT_Extra_Product_Type__c WHERE Product_Definition_Name__c = \'BT Mobile\' ' + filter + 'ORDER BY Name ASC LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset');
        
        // If Basket doesnt contain Sharer or Flex, or Account post code doesnt start with BT remove NI Extra
        if(pcs.size() == 0 || !postcode.startsWith('BT')){
            for(Integer i = 0; i < ProductTypes.size(); i++){
                if(ProductTypes.get(i).Name.equals('NI Extra')){
                    ProductTypes.remove(i);
                }
            }
        }

        return ProductTypes;
    }
}