@isTest
private class CampaignAssignExtension_Test {
    
    static Campaign c;
    static Campaign c1;
    static Task t1;
    static Task t2;
    static User uUser;
    static User uUser1;
    static Contact contact;
    static Account a;
    
    static String t1Id;
    static String t2Id;
    
    static{
        //Data setup - start
        
        //Create User and when Campaign Update check their X Day rule value is updated
//Profile pProfile = [select id from profile where name='BTLB: Standard User'];
        Profile pProfile = [select id from profile where name='System Administrator'];
        User thisUser = [select id from User where id=:userinfo.getUserid()];
	     System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        uUser = new User(alias = 'tst2', email='tst2@btlocalbusiness.co.uk', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = pProfile.Id, timezonesidkey='Europe/London', username='tst2@testemail.com', EIN__c='tst2');
        
        uUser1 = new User(alias = 'tst3', email='tst3@btlocalbusiness.co.uk', 
            emailencodingkey='UTF-8', lastname='Testing3', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = pProfile.Id, timezonesidkey='Europe/London', username='tst3@testemail.com', EIN__c='tst3');
            
        insert uUser;   
        insert uUser1;
        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@btlocalbusiness.co.uk',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
         
        a = Test_Factory.CreateAccount();
        a.name = 'TESTCODE 2';
        a.sac_code__c = 'testSAC';
        a.Sector_code__c = 'CORP';
        a.LOB_Code__c = 'LOB';
        a.OwnerId = u1.Id;
        a.CUG__c='CugTest';
        insert a;
        contact = Test_Factory.CreateContact();
        contact.Phone = '01234 567890';
        contact.AccountId = a.Id;
        insert contact;
        
        List<Campaign_Call_Status__c> ccsToInsert = new List<Campaign_Call_Status__c>();
        Campaign_Call_Status__c ccs1 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status1', Responded__c = true, Default__c=true);
        Campaign_Call_Status__c ccs2 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status2', Responded__c = false, Default__c=false);
        ccsToInsert.add(ccs1);
        ccsToInsert.add(ccs2);
        insert ccsToInsert;
        Date enddate = date.today() + 1;
        c = new Campaign(Name='TestCampaign', Type='Telemarketing', Status='Planned', 
            Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, OwnerId=uUser.Id, isActive=true, Data_Source__c='Self Generated');
        
        insert c;
        
        enddate = date.today() + 2;
        c1 = new Campaign(Name='TestCampaign2', Type='Telemarketing', Status='Planned', 
            Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 5, EndDate = enddate, OwnerId=uUser.Id, isActive=true,Data_Source__c='Self Generated');
        insert c1;
  
        //RecordType rt = [select id from RecordType where Id=:StaticVariables.campaign_task_record_type];
        RecordType rt = [select id from RecordType where DeveloperName='Campaign_Task'];
        System.assert(rt!= null);
        
        List<Task> insertTask = new List<Task>();
        t1 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c.Id, WhoId=contact.Id, Type='Call', RecordTypeId=rt.Id);
        insertTask.add(t1);
        t2 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c1.Id, WhoId=contact.Id, Type='Call', RecordTypeId=rt.Id);
        insertTask.add(t2);
        insert insertTask;
        
        t1 = [select id, X_Day_Rule_Copy__c, activitydate from Task where WhatId=:c.Id];
        t1Id = t1.Id;
        System.assert(t1 != null && t1.X_Day_Rule_Copy__c == 3 && t1.activitydate == date.today() + 1);
        t2 = [select id, X_Day_Rule_Copy__c, activitydate, WhatId, Status from Task where WhatId=:c1.Id];
        t2Id = t2.Id;
        System.assert(t2.X_Day_Rule_Copy__c == 5 && t2.activitydate == date.today() + 2);
        //Data Setup - END
             }
    }
    
    
    /*static testMethod void fromContactListTest1() {
        c.IsActive = true;
        update c;
        uUser.Current_Campaign_Id__c = null;
        update uUser;
        
        Test.startTest(); 
        System.runAs(uUser) {
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 1 - No campaign Id selected (not on User record)######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean bPass = false;
            for(Integer i=0; i <msgs.size();i++){
                if(msgs[i].getDetail()!= null && msgs[i].getDetail()=='You do not have a Campaign assigned, please select a Campaign.'){
                    bPass=true;
                }
            }
            System.assert(bPass);
        }
        Test.stopTest();
    }
    
    static testMethod void fromContactListTest2() {
            
        c.IsActive = false;
        update c;
        uUser.Current_Campaign_Id__c = c.Id;
        update uUser;
        
        Test.startTest();   
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 2 - No campaign Id selected (not on User record)######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean bPass = false;
            for(Integer i=0; i <msgs.size();i++){
                if(msgs[i].getDetail()!= null && msgs[i].getDetail()=='Your selected Campaign does not exist or is INACTIVE.'){
                    bPass=true;
                }
            }
            System.assert(bPass);
        }
        Test.stopTest();
    }
    
     static testMethod void fromContactListTest3() {
        
          
        c.IsActive = true;
        update c;
        uUser.Current_Campaign_Id__c = c.Id;
        update uUser;
        a.Last_Account_Call_Date__c = date.today()-1;
        update a;
        
        Test.startTest(); 
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 3 - X day rule violation - Account Last called date was YESTERDAY.  X Day on campaign [3] days ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean bPass = false;
            for(Integer i=0; i <msgs.size();i++){
                if(msgs[i].getDetail()!= null && msgs[i].getDetail()=='The selected Contact should not be contacted as they have already been contacted in the last [3] days. Please select another Contact.'){
                    bPass=true;
                }
            }
            System.assert(bPass);
        }
        Test.stopTest();
    }*/
    
    static testMethod void fromContactListTest4() {
        
        
        c.IsActive = true;
        update c;
        /*uUser.Current_Campaign_Id__c = c.Id;
        update uUser;*/
        a.Last_Account_Call_Date__c = null;
        a.Call_Back_Time__c = datetime.now().addHours(1);
        update a;
        
        Test.startTest();   
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 4 - Call back violation -  Account called with outcome of CALLBACK.  We haven't yet reached allowable call back time. ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean bPass = false;
            for(Integer i=0; i <msgs.size();i++){
                if(msgs[i].getDetail()!= null && msgs[i].getDetail()=='Contact is marked for a Call back but they where called < 3 hours ago so should not be contacted, please select another contact.'){
                    bPass=true;
                }
            }
            System.assert(bPass);
        }
        Test.stopTest();
    }
    
    static testMethod void fromContactListTest5() {
        
           
        c.IsActive = true;
        update c;
        /*uUser.Current_Campaign_Id__c = c.Id;
        update uUser;*/
        a.Last_Account_Call_Date__c = null;
        a.Call_Back_Time__c = null;
        a.Assigned_User__c = uUser1.Id;
        update a;
        
        Test.startTest();
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 5 - Account already assigned to another agent. i.e. they are in the processs of calling a contact on this account already.  ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean bPass = false;
            for(Integer i=0; i <msgs.size();i++){
                if(msgs[i].getDetail()!= null && msgs[i].getDetail()=='Contact already assigned to another user, please select another contact.'){
                    bPass=true;
                }
            }
            System.assert(bPass);
        }
        Test.stopTest();
    }
    
    static testMethod void fromContactListTest6() {
        
           
        c.IsActive = true;
        update c;
        /*uUser.Current_Campaign_Id__c = c.Id;
        update uUser;*/
        a.Last_Account_Call_Date__c = null;
        a.Call_Back_Time__c = null;
        a.Assigned_User__c = null;
        update a;
        contact.DoNotCall = true;
        update contact;
        
        Test.startTest();
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 6 - Contact is marked as DO NOT CALL.  ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean bPass = false;
            for(Integer i=0; i <msgs.size();i++){
                if(msgs[i].getDetail()!= null && msgs[i].getDetail()=='Contact has elected not to be called, please select another contact.'){
                    bPass=true;
                }
            }
            System.assert(bPass);
        }
        Test.stopTest();
    }
    
    /*static testMethod void fromContactListTest7() {
        
          
        c.IsActive = true;
        update c;
        uUser.Current_Campaign_Id__c = c.Id;
        update uUser;
        a.Last_Account_Call_Date__c = null;
        a.Call_Back_Time__c = null;
        a.Assigned_User__c = null;
        update a;
        contact.DoNotCall = false;
        update contact;
        
        Test.startTest(); 
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 7 - Contact is not a campaign member of selection Campaign  ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
            ApexPages.Message[] msgs = ApexPages.getMessages();
            Boolean bPass = false;
            for(Integer i=0; i <msgs.size();i++){
                if(msgs[i].getDetail()!= null && msgs[i].getDetail().contains('is not a Campaign Member of Campaign: [TestCampaign] or is marked as Unable to Contact.  Please choose another Campaign.')){
                    bPass=true;
                }
            }
            System.assert(bPass);
        }
        Test.stopTest();
    }*/
    
    static testMethod void fromContactListTest8() {
        
           
        c.IsActive = true;
        update c;
        /*uUser.Current_Campaign_Id__c = c.Id;
        update uUser;*/
        a.Last_Account_Call_Date__c = null;
        a.Call_Back_Time__c = null;
        a.Assigned_User__c = null;
        update a;
        contact.DoNotCall = false;
        update contact;
        CampaignMember cm = new CampaignMember(CampaignId=c.Id, ContactId=contact.Id);
        insert cm;
        
        //PRE CHECKS
        a = [select Id, Assigned_User_Datetime__c, Assigned_User__c from Account where Id=:a.Id];
        System.assert(a.Assigned_User_Datetime__c == null && a.Assigned_User__c == null);    
        t1 = [select id, Assigned_User_Id__c, Assigned_User_Datetime__c from Task where Id=:t1Id];
        t2 = [select id, Assigned_User_Id__c, Assigned_User_Datetime__c from Task where Id=:t2Id];
        System.assert(t1.Assigned_User_Id__c == null);
        System.assert(t1.Assigned_User_Datetime__c == null);
        System.assert(t2.Assigned_User_Id__c == null);
        System.assert(t2.Assigned_User_Datetime__c == null);
        
        Test.startTest();
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 8 - Success.  The contact select abides to checks and therefore the Account is assigned to THIS running user and all associated Tasks of the Account are assigned to the User.  ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.performAssignment();
        }
        Test.stopTest();
        
        //POST CHECKS
        a = [select Id, Assigned_User_Datetime__c, Assigned_User__c from Account where Id=:a.Id];
        System.assert(a.Assigned_User_Datetime__c != null && a.Assigned_User__c == uUser.Id);    
        t1 = [select id, Assigned_User_Id__c, Assigned_User_Datetime__c from Task where Id=:t1Id];
        t2 = [select id, Assigned_User_Id__c, Assigned_User_Datetime__c from Task where Id=:t2Id];
        System.assert(t1.Assigned_User_Id__c == uUser.Id);
        System.assert(t1.Assigned_User_Datetime__c != null);
        System.assert(t2.Assigned_User_Id__c == uUser.Id);
        System.assert(t2.Assigned_User_Datetime__c != null);
        
        
    }
    /*
    static testMethod void fromContactListTest9() {
        
        uUser.Current_Campaign_Id__c = null;
        uUser.Current_Campaign_X_Day_Rule__c = null;
        update uUser;

        Test.startTest(); 
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 9 - Test ChangeCampaign Method updates User record  ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
                        
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.setCampaignId('12345678');
            controller.campaignXDayRule = 55;
            System.assert(uUser.Current_Campaign_Id__c == null);
            System.assert(uUser.Current_Campaign_X_Day_Rule__c == null);
            
            controller.selectCampaign();    
        }
        Test.stopTest();
        User userTest9 = [select Id, Current_Campaign_Id__c,Current_Campaign_X_Day_Rule__c from User where Id=:uUser.Id];          
        System.assert(userTest9.Current_Campaign_Id__c == '12345678');
        System.assert(userTest9.Current_Campaign_X_Day_Rule__c == 55);
    }
    
    static testMethod void fromContactListTest10() {

        Test.startTest(); 
        System.runAs(uUser) {       
            PageReference pageRef = Page.CampaignAssign;
            Test.setCurrentPage(pageRef);
            List<Contact> contactList = [SELECT Id FROM Contact where Id=:contact.Id];
            ApexPages.StandardSetController ssc = new ApexPages.StandardSetController(contactList);

            //####### TEST 10 - Coverage only  test method.  There are no asserts as SOSL searches in a test method always return 0 results. ######
            ApexPages.currentPage().getParameters().put('id', contact.Id);
            ApexPages.currentPage().getParameters().put('SelectCampaign', 'false');
                        
            CampaignAssignExtension controller = new CampaignAssignExtension(ssc);
            controller.campaign = 'TestCampaign2';
            List<Campaign> lCampaignResults = controller.matches;
        }
        Test.stopTest();    
    }    */
}