/**************************************************************************************************************************************************
Class Name : EmailPreviewForCustomerHandbook
Test Class Name : Test_EmailPreviewForCustomerHandbook
Email Template : CustomerHandbookEmailTemplate
VisualForce Component : CustomerHandbookBTEmail
Description : To Send eContact Card Email.
Version : V0.1
Created By Author Name : BALAJI MS
Date : 13/02/2018
Modified Date : 
*************************************************************************************************************************************************/
public class EmailPreviewForCustomerHandbook {
    
    /********************************** Declaration Part Starts ********************************************************************************/
    
    public Id ContactRecId{get;set;}
    public Id UserRecordId{get;set;}
    Public Contact ConRec {get;set;}
    Public User UsrRec {get;set;}
    Public Map<String, List<eContactCardDetails__c>> CustomSettingMap {get; set;}
    Public Boolean BillingFlag {get; set;}
    Public Boolean ServiceFlag {get; set;}
    Public Boolean FaultsFlag {get; set;}
    Public Boolean OrdersFlag {get; set;}
    Public Boolean DefaultFlag {get; set;}
    //Public Id RecordVFpage {get;set;}
    Public String ManagerPhn {get;set;}
    Public String AccOwnerPhn {get;set;}
    
    /********************************** Declaration Part Ends *********************************************************************************/
    
    /********************************** Logic Part Starts ************************************************************************************/
    
    public void getData(){
        
        /*********************************** Memory Allocation Starts ************************************************************************/
        
        ConRec = new Contact();
        UsrRec = new User();
        CustomSettingMap = new Map<String, List<eContactCardDetails__c>>();
        BillingFlag = False;
        ServiceFlag = False;
        FaultsFlag = False;
        OrdersFlag = False;
        DefaultFlag = False;
        AccOwnerPhn = '';
        ManagerPhn = '';
        String BrandValue = ''; 
        
        /*********************************** Memory Allocation Ends ************************************************************************/
        
        /*********************************** Main Logic Starts ****************************************************************************/
        
        //Assigning Id for Processing
        Id RecordId = (ContactRecId != Null ? ContactRecId : UserRecordId);
        /*if(RecordId == NULL){
            RecordId = apexpages.currentpage().getparameters().get('Id');
        }*/
        
        //Main Logic begins
        if(RecordId != Null){
            
            //Query to get values from Contact object based on Id we got
            ConRec = [Select Id, Name, Account.Owner.Name, Account.Owner.Phone, Account.Owner.Email, Account.OwnerId, Account.Brand__c, Account.Product__c from Contact where Id =: RecordId];
            
            //Query to get Manager details for the corresponding Account Owner
            UsrRec = [Select Id, Name, Manager.Name, Manager.Email, Manager.Phone from User where Id =: ConRec.Account.OwnerId];
            
            /************************************************ Logic to Phrase Phone Number Begins ***********************************************/
            
            //Logic to replace the Country code of Phone Number with 0
            
            //For Account Owner Phone Number
            if(ConRec.Account.Owner.Phone != Null && ConRec.Account.Owner.Phone.StartsWith('+')){
                
                Integer lengthofString = ConRec.Account.Owner.Phone.length();
                
                if(ConRec.Account.Owner.Phone.substring(3,4) == ' ')
                    AccOwnerPhn = String.valueOf(0+ConRec.Account.Owner.Phone.substring(4,lengthofString)); 
                else
                    AccOwnerPhn = String.valueOf(0+ConRec.Account.Owner.Phone.substring(3,lengthofString));
                
            }
            else
                AccOwnerPhn = ConRec.Account.Owner.Phone;
            
            //For Account Owner's Manager Phone Number
            if(UsrRec.Manager.Phone != Null && UsrRec.Manager.Phone.StartsWith('+')){
                
                Integer lengthofStringManager = UsrRec.Manager.Phone.length();
                
                if(UsrRec.Manager.Phone.substring(3,4) == ' ')
                    ManagerPhn = String.valueOf(0+UsrRec.Manager.Phone.substring(4,lengthofStringManager));
                else
                    ManagerPhn = String.valueOf(0+UsrRec.Manager.Phone.substring(3,lengthofStringManager));
                
            }
            else
                ManagerPhn = UsrRec.Manager.Phone;
            
            /************************************************ Logic to Phrase Phone Number Ends ***********************************************/
            
            //Assigning Account Brand to temporary String for Querying purpose
            if(ConRec.Account.Brand__c == 'BT')
                BrandValue = 'BT';
            else if(ConRec.Account.Brand__c == 'EE')
                BrandValue = 'EE';
            
            /******************************* Logic if Product in Account was selected Begins **************************************************************/
            
            if(ConRec.Account.Product__c != Null){
                
                //Query to get Values from Custom Settings based on the MultiSelect picklist values (Product__c) from Account object
                for(eContactCardDetails__c eContactCard : [Select Id, Area__c, Brand__c, Email__c, End_User__c, Phone__c, Product__c, Supports__c, Website__c from eContactCardDetails__c where (End_User__c =: ConRec.Account.Product__c.split(';') or End_User__c='Default') and Brand__c =: BrandValue Order By Product__c Asc]){
                    
                    //Storing values in Map
                    if(CustomSettingMap.containsKey(eContactCard.Area__c))
                        CustomSettingMap.get(eContactCard.Area__c).add(eContactCard);
                    else
                        CustomSettingMap.put(eContactCard.Area__c,new list<eContactCardDetails__c>{eContactCard});
                    
                    //Enabling Boolean Flag to display in Component
                    if(eContactCard.Area__c == 'Billing')
                        BillingFlag = True;
                    if(eContactCard.Area__c == 'Service')
                        ServiceFlag = True;
                    if(eContactCard.Area__c == 'Faults')
                        FaultsFlag = True;
                    if(eContactCard.Area__c == 'Orders')
                        OrdersFlag = True;
                }
            }
            
            /******************************* Logic if Product in Account was selected Ends **************************************************************/
            
            /******************************* Logic if Product in Account was Not selected Begins **************************************************************/
            
            else{
                
                //Flag for displaying default values output Panel in 'BT' Section
                DefaultFlag = True;
                
                //Query to get Values from Custom Settings
                for(eContactCardDetails__c eContactCard : [Select Id, Area__c, Brand__c, Email__c, End_User__c, Phone__c, Product__c, Supports__c, Website__c from eContactCardDetails__c where End_User__c='Default' and Brand__c =: BrandValue]){
                    
                    //Storing values in Map
                    if(CustomSettingMap.containsKey(eContactCard.Area__c))
                        CustomSettingMap.get(eContactCard.Area__c).add(eContactCard);
                    else
                        CustomSettingMap.put(eContactCard.Area__c,new list<eContactCardDetails__c>{eContactCard});
                    
                    //Enabling Boolean Flag to display in Component
                    if(eContactCard.Area__c == 'Billing')
                        BillingFlag = True;
                    if(eContactCard.Area__c == 'Service')
                        ServiceFlag = True;
                    if(eContactCard.Area__c == 'Faults')
                        FaultsFlag = True;
                    if(eContactCard.Area__c == 'Orders')
                        OrdersFlag = True;
                }
            }
            
            /******************************* Logic if Product in Account was Not selected Ends **************************************************************/
            
        }
        //Main Logic Ends
        
        /*********************************** Main Logic Ends  *******************************************************************************/
        
    }
}