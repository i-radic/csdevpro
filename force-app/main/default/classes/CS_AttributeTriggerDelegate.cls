/**
 * Trigger delegate implementation for Attribute trigger logic
 */
public with sharing class CS_AttributeTriggerDelegate extends CS_TriggerHandler.DelegateBase {
	List<cscfga__Attribute__c> discountAttributes = new List<cscfga__Attribute__c>();
	Set<Id> configIds = new Set<Id>();
	
    // do any preparation here – bulk loading of data etc
    public override void prepareBefore() {

    }
    
    // do any preparation here - bulk loading of data etc
    public override void prepareAfter() {

    }
    
    // Apply before insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeInsert(sObject o) {
    	cscfga__Attribute__c att = (cscfga__Attribute__c) o;

        CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
        String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;
        if(att.Product_Group__c != null && calculateProductGroup.contains(att.Product_Group__c)) {
            return;
        }

    	if(att.cscfga__Attribute_Definition__c == null) {
        	discountAttributes.add(att);
        	configIds.add(att.cscfga__Product_Configuration__c);
    	}
    }
    
    // Apply before update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeUpdate(sObject old, sObject o) {

    }
    
    // Apply before delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeDelete(sObject o) {
        
    }

    // Apply after insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterInsert(sObject o) {
        
    }

    // Apply after update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUpdate(sObject old, sObject o) {

    }
    // Apply after delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterDelete(sObject o) {
        
    }

    // Apply after undelete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUndelete(sObject o) {
        
    }

    // finish logic - process stored records and perform any dml action
    public override void finish() {
		if(!discountAttributes.isEmpty() && !configIds.isEmpty()) {
			Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>([Select Id, Name From cscfga__Product_Configuration__c Where Id = :configIds]);
			for(cscfga__Attribute__c att : discountAttributes) {
				att.cscfga__Line_Item_Description__c = configs.get(att.cscfga__Product_Configuration__c).Name;
				if(att.cscfga__Recurring__c) {
					att.cscfga__Line_Item_Description__c += ' Recurring Discount';
				}
				else {
					att.cscfga__Line_Item_Description__c += ' One Off Discount';
				}
			}
		}
    }
}