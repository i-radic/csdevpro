/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_DeDateProxy {

    static testMethod void updateNextDueDateOnOpp() {
        //Create an Account
        Account account = new Account(name='Test Account 1');
        insert account;
                
        //Create an Opportunity
        // ROS - 24/01/2013. Opportunity data cleanse
        //Opportunity opp = new Opportunity(name='Test Opp1',NIBR_Year_2009_10__c=1,NIBR_Year_2010_11__c=2,NIBR_Year_2011_12__c=3,type='New Business',stagename='Created',closedate=system.today(),accountid=account.id);
        Opportunity opp = new Opportunity(name='Test Opp1',type='New Business',stagename='Created',closedate=system.today(),accountid=account.id);
        insert opp;
        
        Task tsk = new Task(whatid=opp.id, subject='Test Task', priority='Normal', ActivityDate=Date.today()+2, Description='Required');
        insert tsk;
        
        Task tsk2 = new Task(whatid=opp.id, subject='Test Task', priority='Normal', ActivityDate=Date.today()+1);
        insert tsk2;
        
        tsk.Status='Completed';
        update tsk; 
    }
}