@isTest
private class Test_LandscapeBTLBBefore {

static testMethod void runPositiveTestCases() {

Date TestDate = date.today();
Date TestDate2 = date.today()+1;

        Account acc1 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc1.Sector__c = 'other';
        acc1.LOB_Code__c = 'TEST';
        acc1.SAC_Code__c = 'JMMO1231';
        acc1.LE_Code__c = null;
        acc1.AM_EIN__c = '803268119';
        acc1.postcode__c = 'WR5 3RL';
        acc1.Base_Team_Assign_Date__c = Date.today();
        insert acc1;
        
Landscape_BTLB__c LS1 = new Landscape_BTLB__c(); 
LS1.Customer__c = acc1.id;
LS1.Approx_when_did_your_biz_start_trading__c=TestDate ;
LS1.Calls_contract_expiry_date__c=TestDate ;
LS1.Contract_expiry_for_calls_over_internet__c=TestDate ;
LS1.Finance_contract_expiry_date__c=TestDate ;
LS1.ISP_contract_expiry_date__c=TestDate ;
LS1.IT_support_contract_renewal_date__c=TestDate ;
LS1.Lines_contract_expiry_date__c=TestDate ;
LS1.Mobile_contract_expiry_date__c=TestDate ;
LS1.Network_contract_renewal_date__c=TestDate ;
LS1.Phone_system_maint_contract_expiry_date__c=TestDate ;
LS1.Are_the_PC_s_networked_or_stand_alone__c='TestData1' ;
LS1.Are_the_sites_networked_together__c='TestData1' ;
LS1.Are_you_planning_to_move_premises__c='TestData1' ;
LS1.Confirm_no_of_lines_your_company_has__c='TestData1' ;
LS1.Do_you_have_a_server__c='TestData1' ;
LS1.Do_you_have_a_website__c='TestData1' ;
LS1.Do_you_sell_online__c='TestData1' ;
LS1.Do_you_use_BT_for_all_your_calls__c='TestData1' ;
LS1.Do_you_use_BT_for_all_your_lines__c='TestData1' ;
LS1.Does_your_company_have_Broadband__c='TestData1' ;
LS1.Has_a_call_contract_expiry_date__c='TestData1' ;
LS1.Has_a_contract_expiry_date_for_maint__c='TestData1' ;
LS1.Has_a_mobile_contract_renewal_date__c='TestData1' ;
LS1.Has_a_network_contract_renewal_date__c='TestData1' ;
LS1.Has_an_expiry_date_for_ISP_contract__c='TestData1' ;
LS1.Has_an_IT_Support_contract_renewal_date__c='TestData1' ;
LS1.Has_expiry_date_for_finance_contract__c='TestData1' ;
LS1.Has_finance_on_existing_system__c='TestData1' ;
LS1.Has_internet_calls_supp_contract_expiry__c='TestData1' ;
LS1.Has_line_contract_expiry_date__c='TestData1' ;
LS1.How_many_are_Laptops_Notebooks__c='TestData1' ;
LS1.How_many_employees_are_not_office_based__c='TestData1' ;
LS1.How_many_employees_in_your_company__c='TestData1' ;
LS1.How_many_sites_abroad__c='TestData1' ;
LS1.How_many_sites_in_the_UK__c='TestData1' ;
LS1.How_many_years_old_is_your_phone_system__c='TestData1' ;
LS1.How_much_spent_annually_on_calls__c='TestData1' ;
LS1.If_not_BT_who_do_you_use_for_calls__c='TestData1' ;
LS1.If_not_BT_who_do_you_use_for_your_lines__c='TestData1' ;
LS1.No_mobile_handsets_used_for_biz_purposes__c='TestData1' ;
LS1.No_of_company_PC_s_including_Apple_Mac_s__c='TestData1' ;
LS1.Use_company_mob_to_send_receive_emails__c='TestData1' ;
LS1.Which_supplier_for_calls_over_internet__c='TestData1' ;
LS1.Who_is_your_business_mobile_supplier__c='TestData1' ;
LS1.Who_is_your_internet_service_provider__c='TestData1' ;
LS1.Who_maintains_your_phone_switch_system__c='TestData1' ;
LS1.Who_manages_your_network__c='TestData1' ;
LS1.Who_manufactured_your_phone_switch_sys__c='TestData1' ;
LS1.Who_provides_your_IT_support__c='TestData1' ;
LS1.Who_supplies_your_network_LAN__c='TestData1' ;

insert LS1;

//check for setting new date to null
LS1.Approx_when_did_your_biz_start_trading__c= null;
LS1.Calls_contract_expiry_date__c=null;
LS1.Contract_expiry_for_calls_over_internet__c=null;
LS1.Finance_contract_expiry_date__c=null;
LS1.ISP_contract_expiry_date__c=null;
LS1.IT_support_contract_renewal_date__c=null;
LS1.Lines_contract_expiry_date__c=null;
LS1.Mobile_contract_expiry_date__c=null;
LS1.Network_contract_renewal_date__c=null;
LS1.Phone_system_maint_contract_expiry_date__c=null;

update LS1;

//check changing date from null
LS1.Approx_when_did_your_biz_start_trading__c=TestDate2 ;
LS1.Calls_contract_expiry_date__c=TestDate2 ;
LS1.Contract_expiry_for_calls_over_internet__c=TestDate2 ;
LS1.Finance_contract_expiry_date__c=TestDate2 ;
LS1.ISP_contract_expiry_date__c=TestDate2 ;
LS1.IT_support_contract_renewal_date__c=TestDate2 ;
LS1.Lines_contract_expiry_date__c=TestDate2 ;
LS1.Mobile_contract_expiry_date__c=TestDate2 ;
LS1.Network_contract_renewal_date__c=TestDate2 ;
LS1.Phone_system_maint_contract_expiry_date__c=TestDate2 ;
LS1.Are_the_PC_s_networked_or_stand_alone__c='TestData2' ;
LS1.Are_the_sites_networked_together__c='TestData2' ;
LS1.Are_you_planning_to_move_premises__c='TestData2' ;
LS1.Confirm_no_of_lines_your_company_has__c='TestData2' ;
LS1.Do_you_have_a_server__c='TestData2' ;
LS1.Do_you_have_a_website__c='TestData2' ;
LS1.Do_you_sell_online__c='TestData2' ;
LS1.Do_you_use_BT_for_all_your_calls__c='TestData2' ;
LS1.Do_you_use_BT_for_all_your_lines__c='TestData2' ;
LS1.Does_your_company_have_Broadband__c='TestData2' ;
LS1.Has_a_call_contract_expiry_date__c='TestData2' ;
LS1.Has_a_contract_expiry_date_for_maint__c='TestData2' ;
LS1.Has_a_mobile_contract_renewal_date__c='TestData2' ;
LS1.Has_a_network_contract_renewal_date__c='TestData2' ;
LS1.Has_an_expiry_date_for_ISP_contract__c='TestData2' ;
LS1.Has_an_IT_Support_contract_renewal_date__c='TestData2' ;
LS1.Has_expiry_date_for_finance_contract__c='TestData2' ;
LS1.Has_finance_on_existing_system__c='TestData2' ;
LS1.Has_internet_calls_supp_contract_expiry__c='TestData2' ;
LS1.Has_line_contract_expiry_date__c='TestData2' ;
LS1.How_many_are_Laptops_Notebooks__c='TestData2' ;
LS1.How_many_employees_are_not_office_based__c='TestData2' ;
LS1.How_many_employees_in_your_company__c='TestData2' ;
LS1.How_many_sites_abroad__c='TestData2' ;
LS1.How_many_sites_in_the_UK__c='TestData2' ;
LS1.How_many_years_old_is_your_phone_system__c='TestData2' ;
LS1.How_much_spent_annually_on_calls__c='TestData2' ;
LS1.If_not_BT_who_do_you_use_for_calls__c='TestData2' ;
LS1.If_not_BT_who_do_you_use_for_your_lines__c='TestData2' ;
LS1.No_mobile_handsets_used_for_biz_purposes__c='TestData2' ;
LS1.No_of_company_PC_s_including_Apple_Mac_s__c='TestData2' ;
LS1.Use_company_mob_to_send_receive_emails__c='TestData2' ;
LS1.Which_supplier_for_calls_over_internet__c='TestData2' ;
LS1.Who_is_your_business_mobile_supplier__c='TestData2' ;
LS1.Who_is_your_internet_service_provider__c='TestData2' ;
LS1.Who_maintains_your_phone_switch_system__c='TestData2' ;
LS1.Who_manages_your_network__c='TestData2' ;
LS1.Who_manufactured_your_phone_switch_sys__c='TestData2' ;
LS1.Who_provides_your_IT_support__c='TestData2' ;
LS1.Who_supplies_your_network_LAN__c='TestData2' ;

update LS1;


}

}