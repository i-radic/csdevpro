public with sharing class ViewProjectStagesController {
    public ApexPages.StandardController controller;
    public list<Phase__c> stages;
    public boolean projectHasStages { get; set; }
    public static String errorMessage{get; set;}
    
    public ViewProjectStagesController(ApexPages.StandardController controller) {
        this.controller = controller;
        this.setProjectStages();
    }
    
    public void setProjectStages() {
        stages = [SELECT Name, Milestones__c, RAG_Status__c, DB_Received_Date__c, Hot_Staging_Date__c, Hot_Staging_Reference__c, Handover_Date__c, Porting_Start_Date__c, Porting_End_Date__c 
                  FROM Phase__c 
                  WHERE Project__c = :controller.getId()
                  ORDER BY CreatedDate];
        if (stages == null || stages.size() < 1) {
            projectHasStages = false;
        } else {
            projectHasStages = true;
        }
    }
    
    public list<Phase__c> getProjectStages() {
        return stages;
    }
    
    public Pagereference save() {
        errorMessage = null;
            
        try {
            update stages;
            Apexpages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Stages Updated Successfully'));
            
            
        } catch (Dmlexception e) {
            errorMessage = 'Problem whilst saving, please ensure you have entered all the required data for stage ' + [Select Name FROM Phase__c WHERE Id = :e.getDmlId(0)].Name + '. ' + e.getMessage().substring(e.getMessage().indexOf(',')+1);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,errorMessage));
            return null;
        }
        
        return null;
    }
    
    public pageReference cancel() {
        errorMessage = null;
        this.setProjectStages();
        return null;
    }
}