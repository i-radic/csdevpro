@isTest
private class Test_CampaignBefore {

    static testMethod void testCampaignBefore(){     
        Campaign c, c1, c2, c3, c4;
        //  parent
        c = new Campaign(Name='TestCampaignPARENT', Campaign_Type__c='Marketing', Type='Telemarketing', X_Day_Rule__c = 3,
         Status='In Progress X', Description='Some description X', Chapter__c='Broadband X', StartDate = System.today() - 3, EndDate = System.today() + 2,  isActive=true, Data_Source__c='Self Generated');
        insert c;
        //  child
        c1 = new Campaign(Name='TestCampaignCHILD1', Campaign_Type__c='Marketing', Type='Telemarketing', X_Day_Rule__c = 3, isActive=true, ParentId = c.Id, Data_Source__c='Self Generated');
        insert c1;       
        //  another marketing Campaign (not a child)
        c2 = new Campaign(Name='TestCampaign', Campaign_Type__c='Marketing', Type='Telemarketing', X_Day_Rule__c = 3, isActive=true, Data_Source__c='Self Generated');
        insert c2;   
        //  none Marketing Campaign parent
        c3 = new Campaign(Name='TestCampaign', Campaign_Type__c='BTLB', Type='Telemarketing', X_Day_Rule__c = 3, isActive=true, Data_Source__c='Self Generated');
        insert c3;   
        //  none Marketing Campaign child
        c4 = new Campaign(Name='TestCampaign', Campaign_Type__c='BTLB', Type='Telemarketing', X_Day_Rule__c = 3, isActive=true, ParentId = c3.Id, Data_Source__c='Self Generated');
        insert c4;
        
        //  Step 1 - Check child campaign on insert had parent info. replicated
        c1 = [select Id, Status, Description, Chapter__c, StartDate, EndDate from Campaign where Id = :c1.Id];
        c2 = [select Id, Status, Description, Chapter__c, StartDate, EndDate from Campaign where Id = :c2.Id];
        System.assert(c1.Status =='In Progress X' && c1.Description == 'Some description X' && c1.Chapter__c == 'Broadband X' && c1.StartDate == System.today() - 3 && c1.EndDate == System.today() + 2);
        //  this campaign is not a child so should not reflect the parent
        System.assert(c2.Status !='In Progress X' && c2.Description != 'Some description X' && c2.Chapter__c != 'Broadband X' && c2.StartDate != System.today() - 3 && c2.EndDate != System.today() + 2);
        
        //  Step 2 - Check child campaign is updated if parent is amended
        c = [select Status, Description, Chapter__c, StartDate, EndDate from Campaign where Id = :c.Id];
        c.Status = 'In Progress Y';
        c.Description = 'Some description Y';
        c.Chapter__c = 'Broadband Y';
        c.StartDate = System.today() + 1; 
        c.EndDate = System.today() + 3;
        update c;
        c1 = [select Status, Description, Chapter__c, StartDate, EndDate from Campaign where Id = :c1.Id];
        System.assert(c1.Status =='In Progress Y' && c1.Description == 'Some description Y' && c1.Chapter__c == 'Broadband Y' && c1.StartDate == System.today() + 1 && c1.EndDate == System.today() + 3);
        
        // Step 3 - Check child is deleted if parent is deleted - Marketing
        List<Campaign> childCampaign = [Select Id from Campaign where Id = : c1.Id];
        System.assert(!childCampaign.isEmpty());
        delete c;
        childCampaign = [Select Id from Campaign where Id = : c1.Id];
        System.assert(childCampaign.isEmpty());
        
        // Step 4 - Check child is NOT deleted if parent is deleted - None Marketing
        childCampaign = [Select Id from Campaign where Id = : c4.Id];
        System.assert(!childCampaign.isEmpty());
        delete c3;
        childCampaign = [Select Id from Campaign where Id = : c4.Id];
        System.assert(!childCampaign.isEmpty());
    }
}