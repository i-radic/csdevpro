@IsTest
public class CS_Utl_SObjectTest  {
	@IsTest
	public static void testUtlSobject() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Account');

		Test.startTest();
		String checkName = CS_Utl_SObject.getFieldValue(acc, 'Name');
		Test.stopTest();

		System.assertEquals('Test Account', checkName);
	}

	@IsTest
	public static void testUtlSobjectWithDotSeparator() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(true, 'Test PC', basket);

		pc = [SELECT Id, Name, cscfga__Product_Basket__r.Name FROM cscfga__Product_Configuration__c WHERE Id = :pc.Id];

		Test.startTest();
		String checkName = CS_Utl_SObject.getFieldValue(pc, 'cscfga__Product_Basket__r.Name');
		Test.stopTest();

		System.assertEquals('Test Basket', checkName);
		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testUtlSobjectNull() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Test.startTest();
		String checkName = CS_Utl_SObject.getFieldValue(null, 'Name');
		Test.stopTest();
		System.assertEquals(null, checkName);
	}

	@IsTest
	public static void testUtlSobjectException() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Account');

		Test.startTest();
		String checkName = CS_Utl_SObject.getFieldValue(acc, 'cscfga__Product_Basket__r.Name');
		Test.stopTest();
		System.assertEquals(null, checkName);
	}
}