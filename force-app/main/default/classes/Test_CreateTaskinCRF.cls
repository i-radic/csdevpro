@isTest

    private class Test_CreateTaskinCRF{
    
    static testMethod void myUnitTest(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Mover_with_existing_BB'].Id;
    crf.Opportunity__c = o.Id;
    crf.Back_Office_Status__c='Created';
    crf.Contact__c = c.id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;
    
    crf.Back_Office_Status__c='Pending BB';
    crf.Pending_BB_Date__c=System.today();
    update crf;
    
    crf.Pending_BB_Date__c=System.today()+8;
    update crf;
    
    crf.Back_Office_Status__c='Order Issued';
    update crf;
    
    }
    
    static testMethod void myUnitTest1(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Mover_with_existing_BB'].Id;
    crf.Opportunity__c = o.Id;
    crf.Back_Office_Status__c='Created';
    crf.Contact__c = c.id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;   
    
    crf.CRF_Status__c='PSTN APPOINTED';
    crf.PSTN_Appt_Date__c=System.today();
    update crf;
    

    }
    
    static testMethod void myUnitTest2(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Mover_with_existing_BB'].Id;
    crf.Opportunity__c = o.Id;
    crf.Back_Office_Status__c='Created';
    crf.Contact__c = c.id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    crf.CRF_Status__c='PSTN APPOINTED';
    crf.PSTN_Appt_Date__c=System.today();
    insert crf;   
    
    Id loggedinUser = UserInfo.getUserId();
    
    Date dToday = System.today();
    Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
    dt=dt.addHours(8);
 
    Task t = new Task();
    t.ownerId=loggedinUser;  
    t.WhatId=crf.Id;
    t.subject='PSTN Appt Date';
    t.ActivityDate=dToday;
    t.IsReminderSet=true;
    t.ReminderDateTime=dt;
    t.Status='Created';
    t.RecordTypeId=[Select Id from RecordType where DeveloperName=:'CRF_Task'].Id;
    insert t; 
    
    
 
    crf.CRF_Status__c='PSTN APPOINTED';
    date myDate = date.newInstance(2011, 2, 17);
    crf.PSTN_Appt_Date__c=mydate.addDays(2);
    update crf;
    

    }
    
    static testMethod void myUnitTest3(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Mover_with_existing_BB'].Id;
    crf.Opportunity__c = o.Id;
    crf.Back_Office_Status__c='Created';
    crf.Contact__c = c.id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;   
    
    crf.CRF_Status__c='BROADBAND ISSUED';
    crf.BB_Comp_Date__c=System.today();
    update crf;

    }
    
    static testMethod void myUnitTest4(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Mover_with_existing_BB'].Id;
    crf.Opportunity__c = o.Id;
    crf.Back_Office_Status__c='Created';
    crf.Contact__c = c.id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    crf.CRF_Status__c='BROADBAND ISSUED';
    crf.BB_Comp_Date__c=System.today();
    insert crf;   
    
    Id loggedinUser = UserInfo.getUserId();
    
    Date dToday = System.today();
    Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
    dt=dt.addHours(8);
 
    Task t = new Task();
    t.ownerId=loggedinUser;  
    t.WhatId=crf.Id;
    t.subject='BB Comp Date';
    t.ActivityDate=dToday;
    t.IsReminderSet=true;
    t.ReminderDateTime=dt;
    t.Status='Created';
    t.RecordTypeId=[Select Id from RecordType where DeveloperName=:'CRF_Task'].Id;
    insert t; 
       
    crf.CRF_Status__c='BROADBAND ISSUED';
    date myDate = date.newInstance(2011, 2, 17);
    crf.BB_Comp_Date__c=mydate.addDays(2);
    update crf;
    }
    
    static testMethod void myUnitTest5(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Mover_with_existing_BB'].Id;
    crf.Opportunity__c = o.Id;
    crf.Back_Office_Status__c='Created';
    crf.Contact__c = c.id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;   
    
    Id loggedinUser = UserInfo.getUserId();
    
    Date dToday = System.today();
    Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
    dt=dt.addHours(8);
 
    Task t = new Task();
    t.ownerId=loggedinUser;  
    t.WhatId=crf.Id;
    t.subject='BB Comp Date';
    t.ActivityDate=dToday;
    t.IsReminderSet=true;
    t.ReminderDateTime=dt;
    t.Status='Created';
    t.RecordTypeId=[Select Id from RecordType where DeveloperName=:'CRF_Task'].Id;
    insert t; 
    
    
    crf.CRF_Status__c='ORDER DELIVERED';
    update crf;
    }
    
}