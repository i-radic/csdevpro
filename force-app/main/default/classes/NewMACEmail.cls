//CR SF8521

/*
Modification History
    Date           Author                      Description
    03/10/2014     Sridevi Bayyapuneedi        Created methods "sendEmail" to display the email 
                                               template "MAC Order Confirmation Email" in the VF page,to send the email and track the email sent.
*/

public with sharing class NewMACEmail{
   
    public string msg='';
    public Contact custContact{ get; set; }
    public Id cotId{get;set;}
    public Id contactId{ get; set; }
    public String tempEmail;
    public PageReference pageRef;
    
    private Set<String> excludeEmailAddr = new Set<String>{'noemail@address.com','donotcontact@bt.co.uk','er.er@er.co.uk','er.er@er.com'};
         

    public NewMACEmail(ApexPages.StandardController Controller) {
             
       cotId=Apexpages.currentPage().getParameters().get('cotid');
       contactId = [Select Contact__c from CustomerOptions__c where Id=:cotId].Contact__c;
       custContact=[Select Email,Name,Mac_code__c from Contact where Id=:contactId ];
       tempEmail= custContact.Email; 
             
    }    
     
    public PageReference sendEmail (){
                
         try{
         
             
             Pattern p = Pattern.compile( '([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3}))');
             Matcher m = p.matcher(custContact.Email);

             if(!excludeEmailAddr.contains(custContact.Email) && custContact.Email != null && custContact.Email != '' && m.matches()) {
                 
                 List<CustomerOptions__c> coList = [Select MAC_eMail_Status2__c from CustomerOptions__c where Id=:cotId];
                 coList[0].MAC_eMail_Status2__c = 1;
                 update coList;
                 
                 EmailTemplate emailTemp = [select Id from EmailTemplate where Name=:'MAC Order Confirmation Email Template'];
                 
                 msg='Email sent successfully.';
                 
                 if(tempEmail == custContact.Email){
                 //system.debug('same email');
                 pageRef= new PageReference('/_ui/core/email/author/EmailAuthor?p2_lkid='+contactId+'&cotIdVal='+cotId+'&retURL='+EncodingUtil.urlEncode('/apex/New_MAC_Transfer_Page2?cotid='+cotId+'&msg='+msg,'UTF-8')+'&rtype=003&template_id='+emailTemp.Id+'&p26=noreply.btbusiness@bt.com&p5=&save=1');
                 }
                 else{
                 //system.debug('different email');
                 pageRef= new PageReference('/_ui/core/email/author/EmailAuthor?p24='+custContact.Email+'&cotIdVal='+cotId+'&retURL='+EncodingUtil.urlEncode('/apex/New_MAC_Transfer_Page2?cotid='+cotId+'&msg='+msg,'UTF-8')+'&rtype=003&template_id='+emailTemp.Id+'&p26=noreply.btbusiness@bt.com&p5=&save=1');
                 }
                 pageRef.setRedirect(true);
                 return pageRef;
                                
            } else {
                msg = null ;
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid email address for the customer.'));
                Apexpages.currentPage().getParameters().put('msg', msg);
                return null;
            }
    
       }catch(Exception e){
         //ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
         //Apexpages.currentPage().getParameters().put('msg', msg);
         return null;
       }
    } 
    
  /*     
    public string getcallnew(){  
       return msg;
    } 
    
    
    public String emailcontent(){
    
    PageReference p = ApexPages.CurrentPage();
    Blob b = p.getContent();
    tes  
    }
   
   public void trackEmailSent(){
    
    RecordType recordType = [SELECT Id FROM RecordType WHERE Name = 'Standard' AND SobjectType = 'Task'];
    
    Task emailTask = new Task();
    emailTask.WhoId = ContactId;
    //emailTask.WhatId = cotId;
    emailTask.OwnerId = UserInfo.getUserId();
    emailTask.Subject = 'MAC Order Confirmation Email';
    emailTask.RecordType = recordType; 
    emailTask.Type = 'Email';
    emailTask.ActivityDate = Date.today();
    emailTask.Description = 'MAC Order Confirmation Email Notification was sent to '+custContact.Name+'to the Email Address '+custContact.Email;
    emailTask.Priority = 'Normal';
    emailTask.Status = 'Completed';
    
    insert emailTask;
    
    }
    */
    
    
    /* 
    public List<BTLB_Customer_Option_Product__c> getMacCodes(){  
        List<BTLB_Customer_Option_Product__c> ls_cop=[select MAC_Code__c,Telephone_Number__c, BTLB_Customer_Option_record__c from BTLB_Customer_Option_Product__c where BTLB_Customer_Option_record__c=:cotid1 and Order_Type__c='mac'];
        return ls_cop;          
    }
  
    public String getMACtable(List<BTLB_Customer_Option_Product__c> lst)  {
       String str = '' ;
       for(BTLB_Customer_Option_Product__c cs : lst)
       {
           str += '<tr><td>'+ cs.Telephone_Number__c +'</td>'+'<td>'+ cs.MAC_Code__c +'</td>'+'</tr>' ;
       }
       str = str.replace('null' , '') ;
       String finalStr = '' ;
       finalStr = '<table border="1" cellpadding="5" cellspacing="0"><td> Telephone Number </td><td> MAC Code</td> ' + str +'</table>' ;
       return finalStr ;
    }
    
   
   
   
    public Void showTemplate(){
        
         e = new EmailTemplate();
         e = [select Id,HtmlValue,subject from EmailTemplate where Name='MAC Order Confirmation Email'];
         emailBody = e.HtmlValue.replace('{!Contact.FirstName}',Cont.Name);
         emailBody= emailBody.replace('DisplayTable',getMACtable(getMACCodes()) );
    
    }
    */
  
   /*  
   public void actionDo(){
        try{
            // CR4796
            if(!excludeEmailAddr.contains(cont.Email) && cont.Email != null) {
                
                Id fromaddr=[select Id,Address from OrgWideEmailAddress where DisplayName=:'BT Business No Reply'].Id;  
                String EmailTemplateSubject= 'MAC Order Confirmation';
        
                String tempbody='';
                
                String topbody = '' ;
                topbody = getTopBody();
                String tablebody = '' ;
                tablebody = getMACtable(getMACCodes()) ;
                String sign=getSign();  
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();     
                mail.setHtmlBody('Dear ' + cont.Name + ',' + topbody + tablebody + sign);
                mail.setSubject(EmailTemplateSubject);   
                mail.setToAddresses(new String[]{cont.Email});
                //mail.setBccAddresses(new String[]{senderemail});
                mail.setOrgWideEmailAddressId(fromaddr);
                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                msg='Email sent successfully.';
        
                List<CustomerOptions__c> coList = [Select MAC_eMail_Status2__c from CustomerOptions__c where Id=:cotid1];
                coList[0].MAC_eMail_Status2__c=1;
                update coList[0];
            } else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid email address for the customer.'));
            }
    
       }catch(Exception e){
        ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid email address for the customer.'));
       } 
        }
        
   
  
    public string getSign(){
       String sn='<br/>Yours Sincerely,<br/><br/>BT Business.<br/>';
       return sn;
    }
  
    public String getactionBody(){
        String topbody = '' ;
        topbody = getTopBody();
        String tablebody = '' ;
        tablebody = getMACtable(getMACCodes()) ;
        String sign=getSign(); 
        String tempbody='Dear ' + cont.Name + ',' + topbody + tablebody + sign;
        return  tempbody;    
    }
    
     public String getTopBody() {
        String str='';
        str='<br/><br/>We’re sorry that you’re leaving us.<br/><br/>As requested, please find your Broadband Migration Authorisation Code (MAC) this code provides authorisation to your chosen Service Provider to migrate your Broadband Service.<br/><br/>It is valid for 30 days from today. Just to remind you that there may be early termination charges (if you are part way through your contract) but if you would like to take the opportunity to discuss these charges with the Customer Option’s Team, please contact us on 0800 800 152.<br/><br/>We’d like to take this opportunity to thank you for your custom and if there is anything that we can do to help or that you would like to discuss please do not hesitate to contact us – We’re here to listen.<br/><br/>We hope that you would consider coming back to BT for your Broadband service in the near future.<br/><br/>Your MAC(s):<br/><br/>';
        return str;
    }
    
    */
    
   
  
  }