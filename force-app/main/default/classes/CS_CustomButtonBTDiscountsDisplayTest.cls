@isTest
public class CS_CustomButtonBTDiscountsDisplayTest  {
    private static CustomButtonBTDiscountsDisplay controller;
    private static cscfga__Product_Basket__c testBasket;
    private static cscfga__Product_Configuration__c productConfig;
    private static Map<String, Object> result = new Map<String, Object>();

    private static void crateTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        controller = new CustomButtonBTDiscountsDisplay();

        Account testAcc = new Account
            ( Name = 'TestAccount'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
            , AccountId = testAcc.Id
            , CloseDate = System.today()
            , StageName = 'Closed Won'
            , TotalOpportunityQuantity = 0 );
        insert testOpp;
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;

        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
                
        createProductConfigurations(testBasket.Id);
    }

    private static void createProductConfigurations(Id basketId){
        cscfga__Product_Definition__c prodDefNew =
            new cscfga__Product_Definition__c
                ( Name = 'Mobile Sharer'
                , cscfga__Description__c = 'Test helper' );
        insert prodDefNew;
        
        List<cscfga__Attribute_Definition__c> attDefList = new List<cscfga__Attribute_Definition__c>();
        attDefList.add(new cscfga__Attribute_Definition__c(Name='Quantity', cscfga__Product_Definition__c = prodDefNew.Id));
        attDefList.add(new cscfga__Attribute_Definition__c(Name='Retail Price', cscfga__Product_Definition__c = prodDefNew.Id));
        insert attDefList;

        cscfga__Product_Configuration__c config =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = basketId
                , cscfga__Product_Definition__c = prodDefNew.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12 );
        insert config;
        productConfig = config;
    }

    @isTest
    public static void testBasketValid() {
        crateTestData();
        
        Test.startTest();
        result = (Map<String, Object>) JSON.deserializeUntyped(controller.performAction(testBasket.Id));
        Test.stopTest();

        cscfga__Product_Configuration__c processedPC = [SELECT Id, cscfga__Configuration_Status__c FROM cscfga__Product_Configuration__c
                                                            WHERE cscfga__Product_Basket__c = :testBasket.Id];

        System.assertEquals('ok', result.get('status'));
        System.assertEquals('Valid', processedPC.cscfga__Configuration_Status__c);
    }

    @isTest
    public static void testBasketNotValid() {
        crateTestData();
        String result2;

        cscfga__Product_Configuration__c aPC = [SELECT Id, cscfga__Configuration_Status__c FROM cscfga__Product_Configuration__c];
        aPC.cscfga__Configuration_Status__c = 'Incomplete';
        update aPC;

        Test.startTest();
        result = (Map<String, Object>) JSON.deserializeUntyped(controller.performAction(testBasket.Id));
        Test.stopTest();

        cscfga__Product_Configuration__c processedPC = [SELECT Id, cscfga__Configuration_Status__c FROM cscfga__Product_Configuration__c
                                                            WHERE cscfga__Product_Basket__c = :testBasket.Id];

        System.assertEquals('error', result.get('status'));
        System.assertNotEquals('Valid', processedPC.cscfga__Configuration_Status__c);
    }
}