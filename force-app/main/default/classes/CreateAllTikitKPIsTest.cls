@isTest(seeAllData=false)


private class CreateAllTikitKPIsTest {



    static testMethod void myUnitTest() 
    {

       StaticVariables.setAccountAfterDontRun(true);
       StaticVariables.setAccountBeforeDontRun(true);
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
	     System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
         }
        
        Account acc1 = Test_Factory.CreateAccount();     
        acc1.Sub_Sector__c = 'TIKIT';  
        acc1.Sector__c = 'Corporate';
       Database.SaveResult accountResult = Database.insert(acc1);
      
        Opportunity opp1 = Test_Factory.CreateOpportunity(accountResult.getid());
        opp1.stageName='Won';
        opp1.closeDate= date.today();
        opp1.probability=100;
        Database.SaveResult opptResult = Database.insert(opp1);  
        test.starttest();
        String CRON_EXP = '0 0 12 * * ? 2050';
        System.schedule('testBasicScheduledApex', CRON_EXP, new CreateAllTikitKPIs());
        
        test.stopTest();
        
    }
    
    static testMethod void myUnitTest2() 
    {
               StaticVariables.setAccountAfterDontRun(true);
       StaticVariables.setAccountBeforeDontRun(true);
         User thisUser = [select id from User where id=:userinfo.getUserid()];
	     System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
         }
        Account acc1 = Test_Factory.CreateAccount();   
        acc1.Sub_Sector__c = 'TIKIT';     
        acc1.Sector__c = 'Corporate';   
        Database.SaveResult accountResult = Database.insert(acc1);
      
        Opportunity opp1 = Test_Factory.CreateOpportunity(accountResult.getid());
        Database.SaveResult opptResult = Database.insert(opp1);  
        opp1.stageName='Won';
        opp1.closeDate= date.today();
        opp1.probability=100;
        update opp1;
        test.starttest();
        String CRON_EXP = '0 0 12 * * ? 2050';
        System.schedule('testBasicScheduledApex2', CRON_EXP, new CreateAllTikitKPIs2());
        
        test.stopTest();
        
    }
    
    static testMethod void myUnitTest3() 
    {
               StaticVariables.setAccountAfterDontRun(true);
       StaticVariables.setAccountBeforeDontRun(true);
        User thisUser = [select id from User where id=:userinfo.getUserid()];
	     System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
         }
       Account acc1 = Test_Factory.CreateAccount();        
        acc1.Sub_Sector__c = 'TIKIT';   
        acc1.Sector__c = 'Corporate';
        Database.SaveResult accountResult = Database.insert(acc1);

      
        Opportunity opp1 = Test_Factory.CreateOpportunity(accountResult.getid());
        Database.SaveResult opptResult = Database.insert(opp1);  
        opp1.stageName='Won';
        opp1.closeDate= date.today();
        opp1.probability=100;
        update opp1;
        test.starttest();
        String CRON_EXP = '0 0 12 * * ? 2050';
        System.schedule('testBasicScheduledApex3', CRON_EXP, new CreateAllTikitKPIs3());
        
        test.stopTest();
        
    }

}