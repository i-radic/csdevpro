global class BatchLS2L_Schedule03 implements Schedulable{
    
global void execute(SchedulableContext sc) {
    StaticVariables.setUserDontRun(true);       
    BatchLS2L_03CountLeads lCountCL = new BatchLS2L_03CountLeads();
    lCountCL.query = 'SELECT Id, OwnerId FROM Lead WHERE CreatedDate = TODAY AND RecordTypeId = \'01220000000ANXt\'';
    ID batchprocessid = Database.executeBatch(lCountCL);                               
}
}