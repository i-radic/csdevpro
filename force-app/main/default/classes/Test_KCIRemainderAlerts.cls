@isTest

    private class Test_KCIRemainderAlerts{
    
    static TestMethod void myUnitTest(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        KCI__c kci = new KCI__c();
        kci.Name='VOL011-00000000000';        
        kci.Correct_One_View_Queue__c='BTB';
        
        kci.Validation_Call_KCI_1_Attempt_1__c='Not Successful';
        kci.Validation_Call_KCI_1_Attempt_2__c='Not Successful';
        insert kci;
        
        
        kci.Validation_Call_KCI_1_Attempt_1__c='Test';
        update kci;
        
        kci.Validation_Call_KCI_1_Attempt_1__c='Not Successful';
        update kci;
        
        kci.Validation_Call_KCI_1_Attempt_2__c='Test';
        update kci;
        
        kci.Validation_Call_KCI_1_Attempt_2__c='Not Successful';
        update kci;
        
        kci.Confirmation_Call_KCI_1_Attempt_1__c='Not Successful';
        update kci;
        
        kci.Confirmation_Call_KCI_1_Attempt_2__c='Not Successful';
        update kci;
  
        
    }
    
    static TestMethod void myUnitTest1(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        KCI__c kci = new KCI__c();
        kci.Name='VOL011-00000000000';
        kci.Correct_One_View_Queue__c='BTB';

        kci.Confirmation_Call_KCI_1_Attempt_1__c='Call Back Promised';
        kci.Confirmation_Call_KCI_1_Attempt_2__c='Call Back Promised';
        kci.Confirmation_Call_KCI_1_Attempt_3__c='Call Back Promised';

        kci.Confirmation_Call_Back_Date_1__c=System.Now();
        kci.Confirmation_Call_Back_Date_2__c=System.Now();
        kci.Confirmation_Call_Back_Date_3__c=System.Now();
        insert kci;
    
    }
    
    static TestMethod void myUnitTest11(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        KCI__c kci = new KCI__c();
        kci.Name='VOL011-00000000000';        
        kci.Correct_One_View_Queue__c='BTB';
        
        kci.Validation_Call_KCI_1_Attempt_1__c='Call Back Promised';
        kci.Validation_Call_KCI_1_Attempt_2__c='Call Back Promised';
        kci.Validation_Call_KCI_1_Attempt_3__c='Call Back Promised';
        
        kci.Validation_Call_Back_Date_1__c=System.Now();
        kci.Validation_Call_Back_Date_2__c=System.Now();
        kci.Validation_Call_Back_Date_3__c=System.Now();

        insert kci;
    
    }
    
    static TestMethod void myUnitTest2(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        KCI__c kci = new KCI__c();
        kci.Name='VOL011-00000000003';
        kci.Correct_One_View_Queue__c='BTB';
        
        kci.Validation_Call_KCI_1_Attempt_1__c='Test';
        kci.Validation_Call_KCI_1_Attempt_2__c='Test';
        kci.Validation_Call_KCI_1_Attempt_3__c='Test';
        insert kci;
        
        kci.Validation_Call_KCI_1_Attempt_1__c='Call Back Promised';
        kci.Validation_Call_KCI_1_Attempt_2__c='Call Back Promised';
        kci.Validation_Call_KCI_1_Attempt_3__c='Call Back Promised';
        kci.Validation_Call_Back_Date_1__c=System.Now();
        kci.Validation_Call_Back_Date_2__c=System.Now();
        kci.Validation_Call_Back_Date_3__c=System.Now();
        update kci;
        
    }
    
    static TestMethod void myUnitTest3(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        KCI__c kci = new KCI__c();
        kci.Name='VOL011-00000000003';
        kci.Correct_One_View_Queue__c='BTB';
        
        kci.Confirmation_Call_KCI_1_Attempt_1__c='Not Successful';
        kci.Confirmation_Call_KCI_1_Attempt_2__c='Not Successful';
        kci.Confirmation_Call_KCI_1_Attempt_3__c='Not Successful';
        insert kci;
    
        kci.Confirmation_Call_KCI_1_Attempt_1__c='Call Back Promised';
        kci.Confirmation_Call_KCI_1_Attempt_2__c='Call Back Promised';
        kci.Confirmation_Call_KCI_1_Attempt_3__c='Call Back Promised';
        kci.Confirmation_Call_Back_Date_1__c=System.Now();
        kci.Confirmation_Call_Back_Date_2__c=System.Now();
        kci.Confirmation_Call_Back_Date_3__c=System.Now();
        update kci;
        
    }
}