/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2013-06-24 14:10:00 
 *	@description:
 *	    test for OpportunityTeamMemberHandler
 *	
 *	Version History :   
 *		
 */
@isTest
public without sharing class OpportunityTeamMemberHandlerTest {
	static Id SYS_ADMIN_PROFILE_ID = MockUtils.getProfile('System Administrator').Id;
    
    Static testMethod void TriggerDisabled (){
         TriggerDeactivating__c dt = new TriggerDeactivating__c();
            dt.Opportunity__c = True;
            insert dt;
    }
	/**
	 * #806 - when new OpportunityTeamMember is added this user shall start following Opportunity
	 */
	static testMethod void testMemberAddAndRemove () {
		List<User> users = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID ,'EIN__c'=> '522226815'}, 1, true);
        List<User> users2 = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID ,'EIN__c'=> '522226325'}, 1, true); 
		List<Opportunity> opps = new List<Opportunity>();
        
                
		System.runAs(new User(Id = UserInfo.getUserId())) {
			//2 members for opp1 and 1 for opp2
			//opps[0] -> users[0], users[2]
			//opps[1] -> users[1]
			TriggerDisabled();
			opps = Mock.many('Opportunity', 2, true); 
			List<OpportunityTeamMember> members = Mock.many('OpportunityTeamMember', new Map<String, Object>{
															'OpportunityId' => Mock.toIds(opps), 
															'UserId' => Mock.toIds(users)
                                                            },
															users.size(), true); 
            
         Database.delete([select Id from OpportunityTeamMember where UserId =: users[0].Id ]);
            System.assertEquals(0, [select count() from EntitySubscription where subscriberId in: Mock.toIds(users) and parentId =: opps[0].Id], 
							'Expected 1 follower of Opportunity opps[0]');
         
		}
		
		
        
	}
	/**
	 * #806 - when new OpportunityTeamMember is added this user shall start following Opportunity 
	 *		unless this user is already following the Opportunity
	 */
	static testMethod void testDuplicateFollower () {
		List<User> users = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C'=> '454689797'}, 1, true); 
		List<Opportunity> opps = new List<Opportunity>();
		
		System.runAs(new User(Id = UserInfo.getUserId())) {
			//2 members for opp1 and 1 for opp2
			//opps[0] -> users[0], users[2] 
			//opps[1] -> users[1]
			TriggerDisabled ();
            opps = Mock.many('Opportunity', 2, true); 
            Database.insert(new EntitySubscription(subscriberId = users[0].Id, parentId = opps[0].Id));
            List<OpportunityTeamMember> members = Mock.many('OpportunityTeamMember', new Map<String, Object>{
															'OpportunityId' => Mock.toIds(opps), 
															'UserId' => Mock.toIds(users)},
															users.size(), true); 
			//if OpportunityTeamMemberHandler allowed duplicate followers then line above would fail with DUPLICATE_... exception
			Update  members;
		}
		//check that all users are now following respective opps
		System.assertEquals(1, [select count() from EntitySubscription where subscriberId in: Mock.toIds(users) and parentId =: opps[0].Id], 
							'Expected 2 followers of Opportunity opps[0]');
		
		//System.assertEquals(1, [select count() from EntitySubscription where subscriberId in: Mock.toIds(users) and parentId =: opps[1].Id], 
							//'Expected 1 follower of Opportunity opps[1]');
		
       	

	}
}