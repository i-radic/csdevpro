/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FlowBroadbandCRFController_Test {

    static testMethod void myUnitTest() {
       Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User thisUser = [select id from User where id=:userinfo.getUserid()];
		System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2B2Profile13167cr@bt.it',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;

        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 2';
        A.sac_code__c = 'testSAC';
        A.Sector_code__c = 'CORP';
        A.LOB_Code__c = 'LOB';
        A.OwnerId = u1.Id;
        A.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(A);
        
        Contact Con = new Contact();
        Con.FirstName = 'Test_FirstName';
        Con.LastName = 'Test_LastName';
        Con.Email = 'Email@bt.it';
        Con.AccountId = A.id;
        Insert Con;
        
        Opportunity O = new Opportunity();
        O.StageName = 'Created';
        O.Name = 'Test_Flow_Opp';
        O.CloseDate = Date.Today()+1;
        O.AccountId = A.id;
        Insert O;
        
        
        OpportunityContactRole OppConRole = new OpportunityContactRole();
        OppConRole.OpportunityId = O.id;
        OppConRole.ContactId = con.id;
        OppConRole.Role = 'Business User';
        insert OppConRole;
        
        Flow_CRF__c CRF = new Flow_CRF__c();
        CRF.Account__c = A.id;
        CRF.Opportunity__c = O.id;
        insert CRF;
        
        Flow_Address_Details__c AddressDetails = new Flow_Address_Details__c();
        
        AddressDetails.Alternate_Contact_Number__c = '+0012365478';
        AddressDetails.Flow_CRF__c = CRF.id;
        insert AddressDetails;
        
        CRF__c C = new CRF__c();
        C.Name = 'test';
        C.Opportunity__c = O.id;
        insert C;
        
       	Test.setCurrentPageReference(new PageReference('Page.FlowBroadbandCRF'));
        String VFOId = System.currentPageReference().getParameters().put('CF00N20000002oGtq', 'CF00N20000002oGtq');
		        
        ApexPages.StandardController sc = new ApexPages.StandardController(CRF);
        FlowBroadbandCRFController TestFlowBroadbandCRF = new FlowBroadbandCRFController(sc);       	
        TestFlowBroadbandCRF.getBroadbandID(); 		     
        TestFlowBroadbandCRF.getBroadbandCRF();
       
        }
    }
}