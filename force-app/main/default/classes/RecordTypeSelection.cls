public class RecordTypeSelection {

    String recordTypes, opptyName, opptyLookup, retURLValue, jmmGrossACV;
    List < RecordType > RC = [Select Id, Name, SOBJECTTYPE From RecordType where SOBJECTTYPE = 'BookToBill__c'];
    Map < string, string > RCMap = new Map < string, string > ();
    Set < String > stCompardIDs;
    Boolean ProfVar;
    

    public RecordTypeSelection(ApexPages.StandardController controller) {
        //00N20000002oN6E--> oppty lookup field id in BooktoBill object 
        opptyName = Apexpages.currentPage().getParameters().get('CF00N20000002oN6E');
        opptyLookup = Apexpages.currentPage().getParameters().get('CF00N20000002oN6E_lkid');
        system.debug('Opp ID: '+opptyLookup);
        jmmGrossACV = Apexpages.currentPage().getParameters().get('00NP0000000QzZA');
        retURLValue = Apexpages.currentPage().getParameters().get('retURL');
        ProfVar = false;
    }

    public List < SelectOption > getItems() {
        String ProfileId = UserInfo.getProfileId();
        List < Profile > p = [Select name from Profile where id = : ProfileId];
        List < SelectOption > options = new List < SelectOption > ();
        //CR11203
        List <Opportunity> Corpsector = [select Account_Sector__c from Opportunity where id =: opptyLookup and Account_Sector__c = 'Corporate'];
        if(CorpSector.size()>0) {
            ProfVar = true;
        }
        
        if (p.size() > 0) {

            if (p[0].name.contains('Standard User') || (p[0].name.contains('Mid-Market') && ProfVar )) {
                options.add(new SelectOption('Calls And Lines Acquisition', 'Calls And Lines Acquisition'));
                options.add(new SelectOption('Calls And Lines Defence', 'Calls And Lines Defence'));
                //options.add(new SelectOption('Mobile MVNO', 'Mobile MVNO'));
                //options.add(new SelectOption('Mobile Workforce Solutions', 'Mobile Workforce Solutions'));
                
            } else if (p[0].name.contains('Sales Manager')) {
                options.add(new SelectOption('Calls And Lines Acquisition', 'Calls And Lines Acquisition'));
                options.add(new SelectOption('Calls And Lines Defence', 'Calls And Lines Defence'));
                options.add(new SelectOption('Revenue Assurance Audit', 'Revenue Assurance Audit'));
                options.add(new SelectOption('Revenue Assurance ICT', 'Revenue Assurance ICT'));
                options.add(new SelectOption('Revenue Assurance UVS', 'Revenue Assurance UVS'));
                //options.add(new SelectOption('Mobile MVNO', 'Mobile MVNO'));
                //options.add(new SelectOption('Mobile Workforce Solutions', 'Mobile Workforce Solutions'));
                
            } else if (p[0].name.contains('BookToBill Team')) {
                options.add(new SelectOption('Calls And Lines Acquisition', 'Calls And Lines Acquisition'));
                options.add(new SelectOption('Calls And Lines Defence', 'Calls And Lines Defence'));
                options.add(new SelectOption('ICT Book to Bill', 'ICT Book to Bill'));
                options.add(new SelectOption('Revenue Assurance Audit', 'Revenue Assurance Audit'));
                options.add(new SelectOption('Revenue Assurance ICT', 'Revenue Assurance ICT'));
                options.add(new SelectOption('Revenue Assurance UVS', 'Revenue Assurance UVS'));
                //options.add(new SelectOption('Mobile MVNO', 'Mobile MVNO'));
                //options.add(new SelectOption('Mobile Workforce Solutions', 'Mobile Workforce Solutions'));
                
            } else if (p[0].name.contains('BookToBill WinBack')) {
                options.add(new SelectOption('Calls And Lines Acquisition', 'Calls And Lines Acquisition'));
                options.add(new SelectOption('Calls And Lines Defence', 'Calls And Lines Defence'));
                //options.add(new SelectOption('Mobile MVNO', 'Mobile MVNO'));
                //options.add(new SelectOption('Mobile Workforce Solutions', 'Mobile Workforce Solutions'));
                
            }else if (p[0].name.contains('Major and Public Sales')) {
                options.add(new SelectOption('Calls And Lines Acquisition', 'Calls And Lines Acquisition'));
                options.add(new SelectOption('Calls And Lines Defence', 'Calls And Lines Defence'));  
                
            }else if (p[0].name.contains('Reporting Team')) {
                options.add(new SelectOption('--Master--', 'Calls And Lines Acquisition'));               
            }
            
            //Testing purpose
            else if (p[0].name.contains('Admin')) {
                options.add(new SelectOption('Calls And Lines Acquisition', 'Calls And Lines Acquisition'));
                options.add(new SelectOption('Calls And Lines Defence', 'Calls And Lines Defence'));
                options.add(new SelectOption('ICT Book to Bill', 'ICT Book to Bill'));
                options.add(new SelectOption('Revenue Assurance Audit', 'Revenue Assurance Audit'));
                options.add(new SelectOption('Revenue Assurance ICT', 'Revenue Assurance ICT'));
                options.add(new SelectOption('Revenue Assurance UVS', 'Revenue Assurance UVS'));
                //options.add(new SelectOption('Mobile MVNO', 'Mobile MVNO'));
                //options.add(new SelectOption('Mobile Workforce Solutions', 'Mobile Workforce Solutions'));
                
            }
        }
        return options;

    }

    public String getrecordTypes() {
        return recordTypes;
    }

    public void setrecordTypes(String recordTypes) {
        this.recordTypes = recordTypes;
    }

    public PageReference Continue1() {

        //try {
            List < OpportunityLineItem > OLI = new List < OpportunityLineItem > ();
            Opportunity Oppty = new Opportunity();
            Map < String, Decimal > ProdQuantity = new Map < String, Decimal > ();
            Decimal pstnCount = 0;
            Id UserId = UserInfo.getUserId();
            stCompardIDs = new Set < String > ();

            for (RecordType R: RC) {
                RCMap.put(R.Name, R.Id);
                if (R.Name.contains(recordTypes)) stCompardIDs.add(R.Id);
            }
            system.debug('Compare Ids : '+stCompardIDs);
            system.debug('Opp ID2: '+opptyLookup);  
            if (opptyLookup == Null) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'At this point of time You cannot create a new Book to Bill record, Please go back to opportunity'));
                return null;
            }
             if ([Select Id From BookToBill__c where RecordTypeId IN: stCompardIDs and Opportunity__c = : opptyLookup].size() > 0) {

                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Only one type of "' + recordTypes + '" Book to Bill Record can be created for an opportunity.'));
                return null;
            }
                
            if([Select Id, StageName From Opportunity where Id = : opptyLookup LIMIT 1].StageName != 'Won'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Book to Bill Record can be created only on "Won" opportunity.'));
                return null;
              }
           
            else {
                Oppty = [Select sum_of_REVENUE_SOV__c, ACV_Calc__c, Amount, Net_ACV__c From Opportunity Where Id = : opptyLookup];

                OLI = [Select Qty__c, ProdCode__c, ProdName__c, Category__c, TotalPrice From OpportunityLineItem where OpportunityId = : opptyLookup and Sales_Type__c = 'Acquisition'
                        and ProdName__c in ('PSTN - Analogue Exchange Line', 'PSTN Bespoke Line', 'PSTN Bespoke Line - with Hosted Voice', 'Featureline CORPORATE', 'Featureline COMPACT', 'Featureline (Not Compact or Corporate)', 'OnePlan Lines - Not Long Term Contracted', 'ISDN2', 'ISDN30', 'International Direct Connect')];

                List < OpportunityLineItem > OLI1 = [Select Qty__c,ProdCode__c,Product_Family__c, ProdName__c,First_Bill_Date__c, Category__c, TotalPrice, FFA_Supplier__c,ACV_Calc__c,Sales_Type__c From OpportunityLineItem where OpportunityId = : opptyLookup];

                ProdQuantity.put('PSTN', 0);
                ProdQuantity.put('International Direct Connect', 0);
                ProdQuantity.put('ISDN2', 0);
                ProdQuantity.put('ISDN30', 0);

                double GrossValue = 0.00;
                double forecastAnnualGrowth= 0.00;
                Date firstBillDate;
                string st;
                String FFASupplier; //for ffaSupplier field id ' 00NW0000000Qmuo'
                for (OpportunityLineItem O: OLI1) {system.debug('loop1:'+O.Category__c);
                    if (O.Category__c == 'Main' ) {system.debug('in main'+ O.ProdCode__c+'and'+O.Product_Family__c );
                        If( O.ProdCode__c != 'toDLAWL' && ( O.Product_Family__c == 'UVM Calls, OnePlan' || O.Product_Family__c == 'UVM Calls, Business Plan' || O.Product_Family__c == 'UVM Calls, Customer Commitment ' )){
                            system.debug('@@@' + GrossValue);
                            GrossValue += O.ACV_Calc__c;
                        }                           
                        st = String.valueOf(O.First_Bill_Date__c);
                        system.debug('Gross Value out::' + GrossValue);
                        FFASupplier = O.FFA_Supplier__c;
                    }
                                     
                       If( O.ProdCode__c != 'toDLAWL' && ( O.Product_Family__c == 'UVM Calls, OnePlan' || O.Product_Family__c == 'UVM Calls, Business Plan' || O.Product_Family__c == 'UVM Calls, Customer Commitment ' ) && O.Sales_Type__c == 'Acquisition'){
                            system.debug('@@@' + forecastAnnualGrowth+'#####'+O.Sales_Type__c);
                            forecastAnnualGrowth += O.ACV_Calc__c;
                       }
                  
                }
                if(st != null){
                    List<string> slist = st.split('-',4);  
                    st = slist[2]+'/'+slist[1]+'/'+slist[0];                             
                }
                //firstBillDate = Date.parse(slist[2]+'/'+slist[1]+'/'+slist[0]);
                // CR3943

                Integer volVoiceNew = 0;
                Integer volBlackberryNew = 0;
                Integer volDataNew = 0;
                Integer voliPhoneNew = 0;
                Integer volMobileNew = 0;
                Integer productQty = 0; // used for populating in Mob Wforce Automation Record type selection
                
                for (OpportunityLineItem lineItem: OLI1) {
                    
                    productQty += (Integer) lineItem.Qty__c;

                    if (lineItem.ProdCode__c == 'MVNRV' || lineItem.ProdCode__c == 'MVDBO' || lineItem.ProdCode__c == 'TTMON' || lineItem.ProdCode__c == 'TTOFN' || lineItem.ProdCode__c == 'TTSOL'|| lineItem.ProdCode__c == 'MVNEE') {
                        volVoiceNew += (Integer) lineItem.Qty__c;                        
                    } else if (lineItem.ProdCode__c == 'BBNEW' || lineItem.ProdCode__c == 'TTBBR') {
                        volBlackberryNew += (Integer) lineItem.Qty__c;
                    } else if (lineItem.ProdCode__c == 'BTBMB' || lineItem.ProdCode__c == 'MDEXN' || lineItem.ProdCode__c == 'TAB04'|| lineItem.ProdCode__c == 'MBNEE' || lineItem.ProdCode__c == 'MBINE') {
                        volDataNew += (Integer) lineItem.Qty__c;
                    } else if (lineItem.ProdCode__c == 'IPHON' || lineItem.ProdCode__c == 'TTIPN') {
                        voliPhoneNew += (Integer) lineItem.Qty__c;
                    }
                    
                    volMobileNew = volVoiceNew + volBlackberryNew + volDataNew + voliPhoneNew;
                }

                // END
                
                
                for (OpportunityLineItem O: OLI) {
                    if (O.ProdName__c == 'PSTN - Analogue Exchange Line' || O.ProdName__c == 'PSTN Bespoke Line' || O.ProdName__c == 'PSTN Bespoke Line - with Hosted Voice' || O.ProdName__c == 'Featureline CORPORATE' || O.ProdName__c == 'Featureline COMPACT' || O.ProdName__c == 'Featureline (Not Compact or Corporate)' || O.ProdName__c == 'OnePlan Lines - Not Long Term Contracted') {
                        ProdQuantity.put('PSTN', ProdQuantity.get('PSTN') + O.Qty__c);
                    } else {
                        ProdQuantity.put(O.ProdName__c, O.Qty__c);
                    }
                }                
                
                //ProdQuantity.put('PSTN',pstnCount);


                Id RCId = RCMap.get(recordTypes);
                PageReference pageRef;
                
                system.debug('Record Type Name :'+recordTypes);

                //00N20000002oN6E--> oppty master detail field in BooktoBill object
                //01I200000007Tsv-->BooktoBill object id
                //00N20000002oN6w-->Isdn2 acquired lines field Id
                //00N20000002oN7S-->Isdn30 acquired lines field Id
                //00N20000002oN8N-->PSTN acquired lines field Id
                //00N20000002oN6q-->IDC acquired lines field Id
                //00N20000002oN6m-->Forecast Annual Growth field Id
                //00N20000002oNMC-->Claimed Gross ACV
                //00N20000002oYAs-->Net ACV
                //00N20000002p17i-->Net ACV Confirmed 
                //a0H should be updated: keep std button in book2bill related list and click on new

                String urlEncodedTimestamp = EncodingUtil.urlEncode(opptyName, 'UTF-8');



                if (recordTypes == 'Calls And Lines Acquisition' || recordTypes == 'Calls And Lines Defence') {
                    pageRef = new PageReference('/a0H/e?' + 'CF00N20000002oN6E=' + urlEncodedTimestamp + '&CF00N20000002oN6E_lkid=' + opptyLookup + '&00N20000002oNMC=' + Oppty.ACV_Calc__c + '&00N20000002oYAs=' + GrossValue + '&retURL=' + opptyLookup + '&RecordType=' + RCId + '&ent=01I200000007Tsv' + '&00N20000002oN6w=' + ProdQuantity.get('ISDN2').intValue() + '&00N20000002oN7S=' + ProdQuantity.get('ISDN30').intValue() + '&00N20000002oN8N=' + ProdQuantity.get('PSTN').intValue() + '&00N20000002oN6q=' + ProdQuantity.get('International Direct Connect').intValue() + '&00N20000002oN6m=' + forecastAnnualGrowth + '&00N20000003itRK=' + Oppty.ACV_Calc__c+ '&nooverride=1'); //Oppty.Net_ACV__c
                } else if (recordTypes == 'Mobile MVNO') {
                    pageRef = new PageReference('/a0H/e?' + 'CF00N20000002oN6E=' + urlEncodedTimestamp + '&CF00N20000002oN6E_lkid=' + opptyLookup + '&00N20000003itRq='+ volVoiceNew + '&00N20000003itRo='+ volBlackberryNew +'&00N20000003itRp='+ volDataNew +'&00N20000003itRr='+ voliPhoneNew +'&00N20000003itRl='+ volMobileNew +'&00N20000003itRk='+ volMobileNew +'&retURL=' + opptyLookup + '&RecordType=' + RCId + '&ent=01I200000007Tsv' + '&nooverride=1');
                } else if (recordTypes == 'Mobile Workforce Solutions') {
                    pageRef = new PageReference('/a0H/e?' + 'CF00N20000002oN6E=' + urlEncodedTimestamp + '&CF00N20000002oN6E_lkid=' + opptyLookup + '&00N20000003itRc='+ oppty.Amount + '&00N20000003itRd='+ oppty.Amount + '&00N20000003iuEh='+ FFASupplier +'&00N20000003itRT='+ productQty + '&00N20000003itRH='+ st + '&00N20000003itRI='+ st + '&RecordType=' + RCId + '&ent=01I200000007Tsv' + '&nooverride=1');
                }else {
                    pageRef = new PageReference('/a0H/e?' + 'CF00N20000002oN6E=' + urlEncodedTimestamp + '&CF00N20000002oN6E_lkid=' + opptyLookup + '&00N20000002oNMC=' + Oppty.ACV_Calc__c + '&00N20000002oYAs=' + Oppty.Net_ACV__c + '&00N20000002p17i=' + Oppty.Net_ACV__c + '&retURL=' + opptyLookup + '&RecordType=' + RCId + '&ent=01I200000007Tsv' + '&nooverride=1');
                }

                Return pageRef;
            }
        /*} Catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'At this point of time You cannot create a new Book to Bill record. Please return to the Opportunity to create a new Book To Bill record'));
            return null;
        }*/

    }

    Public PageReference Cancel1() {
        PageReference CancelPageRef;
        if (opptyLookup != Null) CancelPageRef = new PageReference('/' + opptyLookup);
        Else
        CancelPageRef = new PageReference('/' + 'home/home.jsp');
        Return CancelPageRef;
    }
}