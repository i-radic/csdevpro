@isTest
private class Test_updateFlowCRFAddress {

    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
        addressDet.Type__c = 'Cease Address';
        
        addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();      
          
    }
     
     static testMethod void runPositiveTestCases11() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        addressDet.Type__c='Cease Address';  
        if(addressDet.Type__c=='Cease Address'){
        String AType='CeaseAddress';}
        
         addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
 		ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();  
    }

    static testMethod void runPositiveTestCases1() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
         addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
        ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();     
    }
    static testMethod void runPositiveTestCases2() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
       addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
        ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();    
        }
    
        static testMethod void runPositiveTestCases5() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
         addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
        ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();         
          
    }
    static testMethod void runPositiveTestCases6() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
        addressDet.Type__c = 'Provide Address';
        
         addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
         ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();   
          
    }
    static testMethod void runPositiveTestCases7() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
        addressDet.Type__c = 'Alternate Billing Address';
        
        addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
         ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();     
          
    }
    static testMethod void runPositiveTestCases8() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
        addressDet.Type__c = 'Billing Address';
        
        addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
        ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();                
          
    }
    static testMethod void runPositiveTestCases9() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
        addressDet.Type__c = 'Delivery Address';
        
        addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
 		ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();     
              
          
    }
    static testMethod void runPositiveTestCases10() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
        addressDet.Type__c = 'Correspondence Address';
        
        addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
         ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();       
        
    }
    
    static testMethod void runPositiveTestCases15() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();  
        addressDet.Type__c = 'Existing Address';
        
        addressDet.Country__c = '';
        addressDet.County__c = null;
        addressDet.Street__c = '';
        addressDet.Post_Code__c = null;
        addressDet.Post_Town__c = '';                
        addressDet.Locality__c = '';
        addressDet.Building_Name__c = '';
        addressDet.Sub_Building__c = '';       
        addressDet.Number__c = '';
        
        
        
 		ApexPages.StandardController sc= new ApexPages.StandardController(addressDet);
        updateFlowCRFAddress updateadd = new updateFlowCRFAddress(sc);
        updateadd.AddressSearch();         
        updateadd.saveAndReturn(); 
        updateadd.NextPage();     
      
    }
        
}