@isTest
private class Test_ISDNController{

    static testMethod void runPositiveTestCases() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        FLOW_CRF__c f = new FLOW_CRF__c();
        ISDN_CRF__c isdn = new ISDN_CRF__c();
        channels__c channel = new channels__c();
        opportunity o = new opportunity();
        Id MoversId;
        Id NSOId;
        Id BroadbandId;
        Id DDId;
        Id AXId;
        Id WLR3Id;
        Id ResignId;
        Id NPId;
        Id NCId;
        Id ISDNId;
        Id NCPId;
        List<RecordType> RTList = [Select Id,DeveloperName from RecordType where SobjectType=:'FLOW_CRF__c'];
        if(RTList.size()>0){
            for(RecordType RT:RTList){
                if(RT.DeveloperName=='Movers')
                    MoversId = RT.Id;
                else if(RT.DeveloperName=='AX_Form')
                    AXId = RT.Id;
                else if(RT.DeveloperName=='ISDN30')
                    ISDNId = RT.Id;
            }
        }
        o.name='test';
        o.StageName='FOS_Stage';
        insert o;
        f.Opportunity__c = o.Id;
        f.RecordTypeId = ISDNId;
        f.Working_Type__c = 'Test;Test1;';
        f.Telephone_Numbers_DDI_range__c = '01234567890 ; 01234567891;' ;
        f.Exist_No_to_convert_to_ISDN__c = 'Test; Test1; Test2;' ;
        f.CLIP_A18025__c = 'Test;Test1;' ;
        f.CLIR_A18026__c = 'Test;Test1;' ;
        f.Divert_on_E_F_A18017__c = 'Test;Test1;' ;
        f.Divert_no_reply_A18018__c  = 'Test;Test1;' ;
        f.Divert_all_calls_A18016__c = 'Test;Test1;' ;
        f.Other_CSS_Order_Codes_1__c = 'Test;Test1;' ;
        f.Other_CSS_Order_Codes_2__c = 'Test;Test1;' ;
        f.Other_CSS_Order_Codes_3__c = 'Test;Test1;' ;
        insert f;
        isdn.Related_to_FLOW_CRF__c = f.Id;
        isdn.Room_No_where_NTE_to_be_fitted__c = '111';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '1';
        isdn.Is_this_a_new_customer_to_BT__c = 'Yes';
        isdn.Tel_no_A_c_no__c = '1111';
        isdn.DASS_2__c = '111';
        isdn.ISDN_30e__c = '2';
        isdn.Total_No_of_DDI_Channels_required__c = '5';
        isdn.Total_No_of_non_DDI_Channels_required__c = '5';
        isdn.VP_Number__c = '14';
        isdn.No_of_DDI_s__c = '15';
        isdn.Room_No_where_NTE_to_be_fitted__c = '123';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '123';
        isdn.New_BLDG_No_BT_Lines_Or_Services__c = '123';
        isdn.Is_this_a_new_customer_to_BT__c = 'Yes';
        isdn.Dual_Parenting__c = false;
        isdn.LOP_ref__c = '123';
        isdn.Secondary_exchange_name_and_code__c = '123';
        isdn.Diverse_Routing__c = false;
		isdn.LOP_ref_DR__c = '123';
        isdn.Remote_exchange_name_and_code_DR__c = '123';
		isdn.One_Bill__c = false;
		isdn.Discount_Plan__c = false;
		isdn.Which_Discount_Plan__c = '123';
		isdn.Site_Assurance__c = true;
		isdn.Option1_ACCD__c = true;
		isdn.Option2__c = true;
        insert isdn;
        channel.Related_to_FLOW_CRF__c = f.Id;
        insert channel;
        
        Flow_Address_Details__c addr = new Flow_Address_Details__c();
        addr.Flow_CRF__c = f.Id;
        insert addr;

        PageReference PageRef = Page.ISDN_FlowCRF_Edit;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('type','new'); //opptyAccntId
        ApexPages.currentPage().getParameters().put('opptyId',o.Id);
        ApexPages.currentPage().getParameters().put('type','new');
        ApexPages.currentPage().getParameters().put('id',f.Id);
        ApexPages.currentPage().getParameters().put('channelId',null);
        ApexPages.currentPage().getParameters().put('isdncrfId',isdn.Id);

        ApexPages.StandardController ctrl = new ApexPages.StandardController(f);
      	ISDNFlowCRFEditController isdnedit = new ISDNFlowCRFEditController(ctrl);
        isdnedit.InsertOnload();
        isdnedit.Submit();
 		isdnedit.cancel1();
        isdnedit.getWorkingType();
        isdnedit.getCommonValues();
        isdnedit.getStandbyBattery();
        isdnedit.getServiceType();
        isdnedit.validatefields();
        
        ISDNFlowViewController view = new ISDNFlowViewController(ctrl);
        view.OnLoad();
        view.ShowTextAreaValues();
        view.getWorkingType();
        view.gettotalchannels();
        view.EditPage();
        
        

		f = new FLOW_CRF__c();
        isdn = new ISDN_CRF__c();
        channel = new channels__c();
		f.Opportunity__c = o.Id;
        f.RecordTypeId = ISDNId;
        insert f;
        isdn.Related_to_FLOW_CRF__c = f.Id;
        isdn.Room_No_where_NTE_to_be_fitted__c = '111';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '1';
        isdn.Is_this_a_new_customer_to_BT__c = 'Yes';
        isdn.Tel_no_A_c_no__c = '1111';
        isdn.DASS_2__c = '111';
        isdn.ISDN_30e__c = '2';
        isdn.Total_No_of_DDI_Channels_required__c = '5';
        isdn.Total_No_of_non_DDI_Channels_required__c = '5';
        isdn.VP_Number__c = '14';
        isdn.No_of_DDI_s__c = '15';
        isdn.Room_No_where_NTE_to_be_fitted__c = '';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '';
        isdn.Is_this_a_new_customer_to_BT__c = '-None-';
        isdn.Dual_Parenting__c = true;
        isdn.LOP_ref__c = '';
        isdn.Secondary_exchange_name_and_code__c = '';
        isdn.Diverse_Routing__c = true;
		isdn.LOP_ref_DR__c = '';
        isdn.Remote_exchange_name_and_code_DR__c = '';
		isdn.One_Bill__c = true;
		isdn.Discount_Plan__c = true;
		isdn.Which_Discount_Plan__c = '';
		isdn.Site_Assurance__c = true;
		isdn.Option1_ACCD__c = false;
		isdn.Option2__c = false;
        insert isdn;
        channel.Related_to_FLOW_CRF__c = f.Id;
        insert channel;

        PageReference PageRef2 = Page.ISDN_FlowCRF_Edit;
        test.setCurrentPage(PageRef2);
        ApexPages.currentPage().getParameters().put('id',f.Id);
        ApexPages.currentPage().getParameters().put('channelId',channel.Id);
        ApexPages.currentPage().getParameters().put('isdncrfId',isdn.Id);

        ApexPages.StandardController ctrl2 = new ApexPages.StandardController(f);
        ISDNFlowCRFEditController isdnedit2 = new ISDNFlowCRFEditController(ctrl2);

        isdnedit2.Submit();
        isdnedit2.cancel1();
    }
}