global class BatchScheduleOpportunityForecastQuarter implements Schedulable{
	global void execute(SchedulableContext sc) {
		Date todaysDate = Date.today();
		Integer month = todaysDate.month();
		Integer day = todaysDate.day();
		if(day == 1 && (month == 1 || month == 4 || month == 7 || month == 10)){   //only want this job to run at the beginning of the quarter - schedule apex can only be schedule weekly or monthly hence this check is needed
		    Date myDate = (Date.today()- 3);
		    String sDate = String.valueOf(myDate);
		    String qString = 'select Close_Date_Fiscal_Quarter__c, zCurrent_Fiscal_Quarter__c,Net_ACV__c,SOV_GM__C, zCurrent_Quarter_Forecast_ACV__c, ACV_Calc__c, zCurrent_Quarter_Forecast_NIBR__c, NIBR_Next_Year__c, zCurrent_Quarter_Forecast_Vol__c, zCurrent_Quarter_Forecast_CY_NIBR__c, NIBR_Current_Year__c, Current_Quarter_Forecast_Amount__c, Amount, ForecastUpdatedQuarter__c from opportunity where zToProcessInForecastBatchQuarter__c = \'PROCESS\'and ( ForecastUpdatedQuarter__c < '+ sDate +' or ForecastUpdatedQuarter__c  = null) ';
		    batchOpportunityForecastQuarter b = new batchOpportunityForecastQuarter(qString);
		    database.executebatch(b, 50);
		}
	}
}