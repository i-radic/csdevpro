public with sharing class updateCRFAddress{
    public Address_Details__c addressDet; 
    public String retUrl;
    
    public Address_Details__c PickListvalues{get;set;}
    
    
    public updateCRFAddress(ApexPages.StandardController controller) {
    addressDet = (Address_Details__c)controller.getRecord();
    }
    
    
    public PageReference NextPage(){
    return Page.CRFAddress;
    
    }
    /* With AX fields */
    
    public PageReference saveAndReturn(){
    if(
    
    ((addressDet.Address_Type_NSO__c=='Provide Address')&& (addressDet.Provide_Post_Code__c != null) && (addressDet.Provide_Sub_Premises__c!= null || addressDet.Provide_Premises_Building_Name__c!= null || addressDet.Provide_Thoroughfare_Number_Street_Numb__c!= null || addressDet.Provide_Thoroughfare_Name_Street_Name__c!= null || addressDet.Provide_Post_Town__c!= null || addressDet.Provide_County__c!= null) )
    
    ||((addressDet.Address_Type_NSO__c=='Alternate Billing Address')&& (addressDet.Billing_Post_Code__c != null)&& (addressDet.Billing_County__c!= null || addressDet.Billing_Post_Town__c!= null || addressDet.Billing_Premises_Building_Name__c!= null || addressDet.Billing_Sub_Premises__c!= null || addressDet.Billing_Thoroughfare_Name_Street_Name__c!= null || addressDet.Billing_Thoroughfare_Number_Street_Numb__c!= null))
    
    ||((addressDet.Address_Type_NSO__c=='Existing Address')&& (addressDet.Existing_Post_Code__c != null) && (addressDet.Existing_County__c!= null || addressDet.Existing_Post_Town__c!= null || addressDet.Existing_Premises_Building_Name__c!= null || addressDet.Existing_Sub_Premises__c!= null || addressDet.Existing_Thoroughfare_Name_Street_Name__c!= null || addressDet.Existing_Thoroughfare_No_Street_Number__c!= null))
    
    ||((addressDet.AX_Address_Type__c=='Delivery Address')&& (addressDet.Provide_Post_Code__c != null)&& (addressDet.Provide_Sub_Premises__c!= null || addressDet.Provide_Premises_Building_Name__c!= null || addressDet.Provide_Thoroughfare_Number_Street_Numb__c!= null || addressDet.Provide_Thoroughfare_Name_Street_Name__c!= null || addressDet.Provide_Post_Town__c!= null || addressDet.Provide_County__c!= null))
    
    ||((addressDet.AX_Address_Type__c=='Correspondence Address')&& (addressDet.Legal_Post_Code__c != null) && (addressDet.Legal_County__c!= null || addressDet.Legal_Post_Town__c!= null || addressDet.Legal_Premises_Building_Name__c!= null || addressDet.Legal_Sub_Premises__c!= null || addressDet.Legal_Thoroughfare_Name_Street_Name__c!= null || addressDet.LegalThoroughfare_Number_StreetNumber__c!= null))
    
    ||((addressDet.AX_Address_Type__c=='Billing Address')&& (addressDet.Billing_Post_Code__c != null) && (addressDet.Billing_County__c!= null || addressDet.Billing_Post_Town__c!= null || addressDet.Billing_Premises_Building_Name__c!= null || addressDet.Billing_Sub_Premises__c!= null || addressDet.Billing_Thoroughfare_Name_Street_Name__c!= null || addressDet.Billing_Thoroughfare_Number_Street_Numb__c!= null))
    
    ||((addressDet.Address_Type__c=='Cease Address')&& (addressDet.Cease_Post_Code__c != null) && (addressDet.Cease_County__c!= null || addressDet.Cease_Post_Town__c!= null || addressDet.Cease_Premises_Building_Name__c!= null || addressDet.Cease_Sub_Premises__c!= null || addressDet.Cease_Thoroughfare_Name_Street_Name__c!= null || addressDet.Cease_Thoroughfare_Number_Street_Number__c!= null))
    
    ||((addressDet.Address_Type__c=='Provide Address')&& (addressDet.Provide_Post_Code__c != null) && (addressDet.Provide_Sub_Premises__c!= null || addressDet.Provide_Premises_Building_Name__c!= null || addressDet.Provide_Thoroughfare_Number_Street_Numb__c!= null || addressDet.Provide_Thoroughfare_Name_Street_Name__c!= null || addressDet.Provide_Post_Town__c!= null || addressDet.Provide_County__c!= null))
    
    ||((addressDet.Address_Type__c=='Alternate Billing Address')&& (addressDet.Billing_Post_Code__c != null) && (addressDet.Billing_County__c!= null || addressDet.Billing_Post_Town__c!= null || addressDet.Billing_Premises_Building_Name__c!= null || addressDet.Billing_Sub_Premises__c!= null || addressDet.Billing_Thoroughfare_Name_Street_Name__c!= null || addressDet.Billing_Thoroughfare_Number_Street_Numb__c!= null))
    
    )
    {
        if((addressDet.AX_Address_Type__c=='Delivery Address')||(addressDet.AX_Address_Type__c=='Correspondence Address')||(addressDet.AX_Address_Type__c=='Billing Address'))
        {
            addressDet.recordtypeId=[SELECT Id FROM RecordType WHERE DeveloperName=:'AX_Address'].Id;
        }
        else if((addressDet.Address_Type__c=='Cease Address')||(addressDet.Address_Type__c=='Alternate Billing Address')||(addressDet.Address_Type__c=='Provide Address'))
        {
            addressDet.recordtypeId=[SELECT Id FROM RecordType WHERE DeveloperName=:'Address_Type'].Id;
        }
        else if((addressDet.Address_Type_NSO__c=='Existing Address')||(addressDet.Address_Type_NSO__c=='Alternate Billing Address')||(addressDet.Address_Type_NSO__c=='Provide Address'))
        {
            addressDet.recordtypeId=[SELECT Id FROM RecordType WHERE DeveloperName=:'NSO_Address_Type'].Id;    
        }
        upsert addressDet;
        PageReference PageRef = new PageReference('/'+addressDet.Related_to_CRF__c);                                  
        
        PageRef.setRedirect(true);
        
        return PageRef;
    }
    else
    {
        ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode and click on Address Search !!'));    
        return null;
    }
    }
    
    
    /* Without AX fields*/
    /*
    public PageReference saveAndReturn(){
    if(((addressDet.Address_Type_NSO__c=='Provide Address')&& (addressDet.Provide_Post_Code__c != null)&& (addressDet.Provide_County__c!=null))||((addressDet.Address_Type_NSO__c=='Alternate Billing Address')&& (addressDet.Billing_Post_Code__c != null)&& (addressDet.Billing_County__c!=null))||((addressDet.Address_Type_NSO__c=='Existing Address')&& (addressDet.Existing_Post_Code__c != null)&& (addressDet.Existing_County__c!=null))||((addressDet.Address_Type__c=='Cease Address')&& (addressDet.Cease_Post_Code__c != null)&& (addressDet.Cease_County__c!=null))||((addressDet.Address_Type__c=='Provide Address')&& (addressDet.Provide_Post_Code__c != null)&& (addressDet.provide_County__c!=null))||((addressDet.Address_Type__c=='Alternate Billing Address')&& (addressDet.Billing_Post_Code__c != null)&& (addressDet.Billing_County__c!=null)))
    {
        if((addressDet.Address_Type__c=='Cease Address')||(addressDet.Address_Type__c=='Alternate Billing Address')||(addressDet.Address_Type__c=='Provide Address'))
        {
            addressDet.recordtypeId=[SELECT Id FROM RecordType WHERE DeveloperName=:'Address_Type'].Id;
        }
        else if((addressDet.Address_Type_NSO__c=='Existing Address')||(addressDet.Address_Type_NSO__c=='Alternate Billing Address')||(addressDet.Address_Type_NSO__c=='Provide Address'))
        {
            addressDet.recordtypeId=[SELECT Id FROM RecordType WHERE DeveloperName=:'NSO_Address_Type'].Id;    
        }
        upsert addressDet;
        PageReference PageRef = new PageReference('/'+addressDet.Related_to_CRF__c);                                  
        
        PageRef.setRedirect(true);
        
        return PageRef;
    }
    else
    {
        ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode and click on Address Search !!'));    
        return null;
    }
    }
    
   */ 
       
    
    
    public PageReference cancelAndReturn(){    
    
    PageReference PageRef = new PageReference('/'+addressDet.Related_to_CRF__c);    
    PageRef.setRedirect(true);
    System.debug('valueb'+addressDet.Related_to_CRF__c);
    return PageRef;
    
    
    }

    
    public updateCRFAddress(Address_Details__c address_edit){
         addressDet = address_edit;
    }
    public PageReference OnLoad() { 
         retUrl = ApexPages.currentPage().getParameters().get('retURL'); 
         String id = ApexPages.currentPage().getParameters().get('id');
         
         if(Test_Factory.GetProperty('IsTest') == 'yes'){
            id = 'test';
         }         
         return null;
    }
     /*public PageReference OnLoad(string accountId){
        return null;    
    }*/
    public PageReference AddressSearch() {
        String integrationId;
       
       String AType;
       if(addressDet.Address_Type__c=='Cease Address'){
      AType='CeaseAddress';
      System.debug('valueab1'+AType);
      }
      if((addressDet.Address_Type__c=='Provide Address')||(addressDet.Address_Type_NSO__c=='Provide Address')){
      
      AType='ProvideAddress';
      System.debug('valueab2'+AType);
      }
      if((addressDet.Address_Type__c=='Alternate Billing Address')||(addressDet.Address_Type_NSO__c=='Alternate Billing Address'))
      AType='AlternateBillingAddress';
      
      if(addressDet.AX_Address_Type__c=='Billing Address')
      AType='BillingAddress';
      if(addressDet.AX_Address_Type__c=='Correspondence Address')
      AType='CorrespondenceAddress';
      if(addressDet.AX_Address_Type__c=='Delivery Address')
      AType='DeliveryAddress';
      
      if(addressDet.Address_Type_NSO__c=='Existing Address')
      Atype='ExistingAddress';
      
      
      if(AType=='ExistingAddress'){
        try {
        System.debug('valueab'+AType);
        
        System.debug('valueab3'+AType);
            if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Existing_Post_Code__c = 'TEST';
            }   
                                       
            if (addressDet.Existing_Post_Code__c != null){
            System.debug('valueab4'+AType);
                string pCode = addressDet.Existing_Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/addressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Existing_Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            } 
            }
            catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
           
            }
            }
      
      
      
      
      if(AType=='CorrespondenceAddress'){
        try {
        System.debug('valueab'+AType);
        
        System.debug('valueab3'+AType);
            if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Legal_Post_Code__c = 'TEST';
            }   
                                       
            if (addressDet.Legal_Post_Code__c != null){
            System.debug('valueab4'+AType);
                string pCode = addressDet.Legal_Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/addressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Legal_Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            } 
            }
            catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
           
            }
            }
        
        
        
        if(AType=='BillingAddress'){
        try {
        System.debug('valueab'+AType);
        
        System.debug('valueab3'+AType);
            if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Billing_Post_Code__c = 'TEST';
            }   
                                       
            if (addressDet.Billing_Post_Code__c != null){
            System.debug('valueab4'+AType);
                string pCode = addressDet.Billing_Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/addressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Billing_Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            } 
            }
            catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
           
            }
            }
            
         /*if(AType=='DeliveryAddress'){
        try {
        System.debug('valueab'+AType);
        
        System.debug('valueab3'+AType);
            if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Provide_Post_Code__c = 'TEST';
            }   
                                       
            if (addressDet.Provide_Post_Code__c != null){
            System.debug('valueab4'+AType);
                string pCode = addressDet.Provide_Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/addressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Provide_Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            } 
            }
            catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
           
            }
            }  */
            
        if(AType=='CeaseAddress'){
        try {
        System.debug('valueab'+AType);
        
        System.debug('valueab3'+AType);
            if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Cease_Post_Code__c = 'TEST';
            }   
                                       
            if (addressDet.Cease_Post_Code__c != null){
            System.debug('valueab4'+AType);
                string pCode = addressDet.Cease_Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/addressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Cease_Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            } 
            }
            catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
           
            }
            }
            else if(AType.contains('ProvideAddress')||AType.contains('DeliveryAddress')){
            try{
            System.debug('valueab5'+AType);
                if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Provide_Post_Code__c = 'TEST';
            }   
             
            System.debug('valueabc'+addressDet.Provide_Post_Code__c);                                       
            if ((addressDet.Provide_Post_Code__c != null)){
                System.debug('valueabc'+AType);
                string pCode = addressDet.Provide_Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/addressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Provide_Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            }
            }
            catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
        }   
            }
            
            else{
            try{
                if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Billing_Post_Code__c = 'TEST';
            }   
                                       
            if ((addressDet.Billing_Post_Code__c != null)){
                string pCode = addressDet.Billing_Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/addressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Billing_Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            }
          }
          catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
        }                 
        }  
               

}
}