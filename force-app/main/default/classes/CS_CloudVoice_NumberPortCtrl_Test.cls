@isTest(SeeAllData=true)

private class CS_CloudVoice_NumberPortCtrl_Test {

	static Account account {get; set;}
	static Contact contact {get; set;}
	static cscfga__Product_Basket__c prodBasket {get; set;}

	static void SetVariables(){
		account = Test_Factory.CreateAccount();     
        account.CUG__c = 'cugCV1';
        account.OwnerId = UserInfo.GetUserId() ;
        Database.SaveResult aR = Database.insert(account);     
                     
        contact = Test_Factory.CreateContact();
        contact.Cug__c = 'cugCV1';
		contact.FirstName = 'TEST';
		contact.LastName = 'TEST';
        contact.Contact_Post_Code__c = 'WR5 3RL';
        contact.Email = 'test@email.com';
        insert contact;       	

		prodBasket = new cscfga__Product_Basket__c(csbb__Account__c = account.Id, Customer_Contact__c = contact.Id);
		insert prodBasket;
	}

	static testMethod void UnitTest() {
		SetVariables(); 
		PageReference page = Page.CS_CloudVoice_NumberPort;
		Test.setCurrentPage(page);		
		ApexPages.currentPage().getParameters().put('basketId', prodBasket.id);
		ApexPages.currentPage().getParameters().put('numstoport', '10');
		ApexPages.currentPage().getParameters().put('retURL', 'TEST');
		CS_CloudVoice_NumberPortCtrl ctrl = new CS_CloudVoice_NumberPortCtrl();
		
		List<SelectOption> addressList = ctrl.getSiteAddressList();
		List<Number_Port_Reference__c> rlist = ctrl.getCPs();
		ctrl.newNumberPort();
        ctrl.numberPortList.get(0).PVT_Ref__c = 'Test123';
        ctrl.newNumberPort.PVT_Ref__c = 'Test123';
		ctrl.addNumberPort();
		ctrl.selectedIndex = 1;
		ctrl.cloneNumberPort();
		ctrl.csvFileBody = Blob.valueof('Order_Address__c,Range_Holder__c,Losing_CP__c,Billing_A_C_Number__c,Main_Number__c,Number_Type__c,Number_Start__c,Number_End__c,Associated_Numbers__c,Service_Usage__c,PVT_Ref__c,LOA_Status__c'+'\n'+'a6V3E0000004T6sUAE,BT,BT,001,01145767776,Range,01145767776,01145767777,01145767776,Phone,TEST,Draft');
		ctrl.importCSVFile();
        ctrl.deleteNumberPort();
		ctrl.saveInlineChanges();
		ctrl.closeButtonClick();
	}
}