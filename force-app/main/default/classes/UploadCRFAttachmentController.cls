public class UploadCRFAttachmentController {
    
    public String selectedType {get;set;}
    public Boolean isActive {get;set;}
    public String description {get;set;}
    private BTNet_Customer_Details__c BTNetCustomerDtl {get;set;} 
    public CRF_Attachment__c eccAttachment {get;set;} 
    public String fileName {get;set;}
    public String crfId {get;set;}
    public Blob fileBody {get;set;}
    
    public UploadCRFAttachmentController(ApexPages.StandardController controller) { 
        this.BTNetCustomerDtl = (BTNet_Customer_Details__c)controller.getRecord();
        System.debug('BTNetCustomerDt>>l'+BTNetCustomerDtl);
        crfId = ApexPages.currentPage().getParameters().get('crfId');
        eccAttachment = new CRF_Attachment__c();
        List<CRF_Attachment__c> eccAttachmentlist = [select id,Name,Customer_Requirements_Form__c, description__c, active__c,Attachment_Type__c from CRF_Attachment__c where Attachment_Type__c='BTNet CRM ECC Mapping' and Customer_Requirements_Form__c= :crfId ];
        if(eccAttachmentlist!=null && !eccAttachmentlist.isEmpty()){
            eccAttachment=eccAttachmentlist[0];
        }else{
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                  'No ECC Attached'));  
        }
    }   
    
    // creates a new CRF_Attachment__c record
    @TestVisible
    private Database.SaveResult saveCustomAttachment() {
        CRF_Attachment__c obj = new CRF_Attachment__c();
        obj.Customer_Requirements_Form__c = BTNetCustomerDtl.Id; 
        obj.description__c = description;
        obj.Attachment_Type__c = selectedType;
        obj.active__c = true;
        // fill out cust obj fields
        return Database.insert(obj);
    }
    
    // create an actual Attachment record with the CRF_Attachment__c as parent
    @testVisible
    private Database.SaveResult saveStandardAttachment(Id parentId) {
        Database.SaveResult result;
        
        Attachment attachment = new Attachment();
        attachment.body = this.fileBody;
        attachment.name = this.fileName;
        attachment.parentId = parentId;
        // inser the attahcment
        result = Database.insert(attachment);
        // reset the file for the view state
        fileBody = Blob.valueOf(' ');
        return result;
    }
    
    /**
    * Upload process is:
    *  1. Insert new CRF_Attachment__c record
    *  2. Insert new Attachment with the new CRF_Attachment__c record as parent
    *  3. Update the CRF_Attachment__c record with the ID of the new Attachment
    **/
    public PageReference processUpload() {
        try {
            
            List<Attachment> existingattachmentlist = [select id from Attachment where parentId = :eccAttachment.id];
            if(!existingattachmentlist.isEmpty()){
                delete existingattachmentlist;
            }
            /*Database.SaveResult customAttachmentResult = saveCustomAttachment();
        
            if (customAttachmentResult == null || !customAttachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                  'Could not save attachment.'));
                return null;
            }
            */
            Database.SaveResult attachmentResult = saveStandardAttachment(eccAttachment.id);
        
            if (attachmentResult == null || !attachmentResult.isSuccess()) {
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                  'Could not save attachment.'));            
                return null;
            } else {
                // update the custom attachment record with some attachment info
                //CRF_Attachment__c customAttachment = [select id from CRF_Attachment__c where id = :eccAttachment.id];
                eccAttachment.name = this.fileName;
                eccAttachment.Attachment__c = attachmentResult.getId();
                update eccAttachment;
            }
        
        } catch (Exception e) {
            ApexPages.AddMessages(e);
            return null;
        }
        
        return new PageReference('/'+crfId);
    }
    
    public PageReference back() {
        return new PageReference('/'+crfId);
    }     

}