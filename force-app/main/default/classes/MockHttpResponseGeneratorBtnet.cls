global class MockHttpResponseGeneratorBtnet implements HttpCalloutMock {

        // Implement this interface method   
     global HTTPResponse respond(HTTPRequest req) {
        system.debug('rrrrrrrr'+req);
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('<?xml version="1.0" encoding="UTF-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"><soap:Body><ns2:checkProductAvailabilityReturn xmlns:ns2="http://capabilities.nat.bt.com/xsd/ManageProductAvailability/2007/04/30" xmlns="http://wsi.nat.bt.com/2005/06/StandardHeader/" xmlns:ns4="http://capabilities.nat.bt.com/xsd/ManageProductAvailability/2007/04/30/CCM/Standards/TestResponse" xmlns:ns3="http://capabilities.nat.bt.com/xsd/ManageProductAvailability/2007/04/30/CCM/CoreClasses/Specifications"><standardHeader><e2e><E2EDATA>1=4jjy3v9cqg,10=4jjy3v9cqo,7= BTnet Quick Quo,6=PCK002069,4= BTnet Quick Quote,15=WS-RoBTESB,9=rbtesb67,8=UNKNOWN,1.1=APP10865:4jjy3v9cqg:</E2EDATA></e2e><serviceState><stateCode>OK</stateCode><errorCode>0000</errorCode><errorDesc>Informational</errorDesc><errorText>Request Successful</errorText></serviceState><serviceAddressing><from>http://ccm.intra.bt.com/COPAL</from><to><address>http://ccm.intra.bt.com/ManageProductAvailability</address><contextItemList/></to><messageId>3807</messageId><action/></serviceAddressing></standardHeader><ns2:productAvailabilityResponsePayload><ns2:resultStatus>GREEN</ns2:resultStatus><ns2:resultText>GetECC for:MR/RIN</ns2:resultText><ns2:productAvailabilityResult><ns4:productAvailabilityInstanceResult><ns4:instance><ns3:instanceCharacteristic><ns3:name>IndicativeOrderCategory</ns3:name><ns3:value>2.1</ns3:value></ns3:instanceCharacteristic><ns3:instanceCharacteristic><ns3:name>IndicativeECCTariff</ns3:name><ns3:value>Out of Tariff</ns3:value></ns3:instanceCharacteristic><ns3:instanceCharacteristic><ns3:name>IndicativeECCOutSideThreshold</ns3:name><ns3:value>GBP 2239 to 3359</ns3:value></ns3:instanceCharacteristic><ns3:instanceCharacteristic><ns3:name>ProductName</ns3:name><ns3:value>EAD</ns3:value></ns3:instanceCharacteristic></ns4:instance><ns4:productAvailabilityCheckResult><ns4:reasonCode/><ns4:reasonText/><ns4:type>NL</ns4:type><ns4:status>GREEN</ns4:status><ns4:responseTrace/></ns4:productAvailabilityCheckResult></ns4:productAvailabilityInstanceResult></ns2:productAvailabilityResult></ns2:productAvailabilityResponsePayload></ns2:checkProductAvailabilityReturn></soap:Body></soap:Envelope>');
         res.setStatusCode(200);
        return res; 
    }
        
    
    }