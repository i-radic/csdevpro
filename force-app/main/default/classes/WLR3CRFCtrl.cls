public with sharing class WLR3CRFCtrl {

    public WLR3CRFCtrl(ApexPages.StandardController controller) {

    }
    public string SLID = System.currentPageReference().getParameters().get('id');
  
public Numbers_to_be_Transferred__c newProd {
  get {
      if (newProd == null)
        newProd = new Numbers_to_be_Transferred__c();
      return newProd ;
    }
  set;
  }

public List<Numbers_to_be_Transferred__c> getProdList() {
      return [SELECT ID,Name,Number_to_be_transferred__c,Related_to_CRF__c FROM Numbers_to_be_Transferred__c WHERE Related_to_CRF__c = :SLID ORDER BY CreatedDate DESC];
}

public PageReference addProd() {
    newProd.Related_to_CRF__c = SLID ; // the record the file is attached to
    

    try {
      insert newProd;
    } catch (DMLException e) {
      ApexPages.addMessages(e);
    } finally {
      newProd= new Numbers_to_be_Transferred__c(); 
    }

    return null;
}

}