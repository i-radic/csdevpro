public class LeadProcessDispatcherController{
    
  // Private variables
    private final ApexPages.StandardController controller;
    private string profName;
    private final String msStatus;
    
  // Public variables
    public String subTitle {get; set;}
    public Lead moLead {get; set;}
    public final Id pIdlead {get;set;}
    public string proc {get;set;}

    
   /*
    * @name         LeadProcessDispatcherController
    * @description  Constructor initialization
    * @author       P Goodey
    * @date         June 2014
    * @see      WR 00070519
    * @UPDATED      Dec 2018 G James  Removed use of pageRef to allow for mobile navigation 
    */ 
    public LeadProcessDispatcherController(ApexPages.StandardController stdController) {

    // Set the Controller Record Id field
        this.controller = stdController;
        pIdlead = controller.getRecord().Id;

        // Retrieve the Lead fields
        moLead = [SELECT Id, Name, Status, Lead_Status_Reason__c, Comments__c, Date_Diff__c, OwnerId, Account__c, Lead_Profile__c, Public_Sector_Type__c FROM Lead WHERE Id=:pIdlead LIMIT 1]; 
            
        // Get and set the status from the querystring parameter
        msStatus = ApexPages.currentPage().getParameters().get('status');
        moLead.Status = msStatus;
        profName = moLead.Lead_Profile__c;
        
    }
    

   /*
    * @name         processLead
    * @description  Process the Lead according to the specified parameters
    * @author       P Goodey
    * @date         June 2014
    * @see      WR 00070519
    * @UPDATED      Dec 2018 G James  Removed use of pageRef to allow for mobile navigation
    */ 
    public Void processLead() {

        //Mobile - If statement transferred from JavaScript button to only perform action if profile is not Public Sector or it is and Sector Type is not null
        if( ('{!Lead.Lead_Profile__c}'.indexOf('Public Sector') == -1) || ( '{!Lead.Lead_Profile__c}'.indexOf('Public Sector') != -1 && '{!Lead.Public_Sector_Type__c}' != '') ){
        
            // Set the default Page Reference for the return to the Lead
            //PageReference pageRef = new ApexPages.StandardController(moLead).view();
        
            //Mobile - get process sent from URL button for JavaScript in VF page
            proc = ApexPages.currentPage().getParameters().get('process');
        
            // Get the process parameter
            String lsProcess = ApexPages.currentPage().getParameters().get('process');
               
            // Autosave Process
            if(lsProcess == 'autosave') {
        
                // Get the Status Parameter
                String lsStatus = ApexPages.currentPage().getParameters().get('status');
                    
                // Set the Status
                moLead.Status = lsStatus;
                        
                // Update the Lead
                update moLead;
                        
                // set the page reference to the Lead page and clear viewstate 
                //pageRef.setRedirect(true);
                        
            }else if(lsProcess == 'redirect') {
                  
                // Set the subTitle text to show the new Status as per button text
                subTitle = msStatus;
                        
                // Set the return page reference
                //pageRef = System.Page.LeadProcessEntry;
                                    
                // Do not clear the viewstate (we are calling the page with this same controller)
                //pageRef.setRedirect(false);
                                       
            }else if(lsProcess == 'auto_assign') {
              
                List<Account> lstCompany = new List<Account>();    
              
                lstCompany = [SELECT OwnerId FROM Account WHERE Id = :moLead.Account__c LIMIT 1];    
                moLead.OwnerId = lstCompany[0].OwnerId;    
                moLead.Status = 'Unqualified';
             
                // Update the Lead
                update moLead;
                    
                // set the page reference to the Lead page and clear viewstate 
                //pageRef.setRedirect(true);
                System.debug('GLJ Auto Assign should have occurred');
                  
            }
            
            // Return the page reference
            //return pageRef;
        }
    }
    

   /*
    * @name         onSave
    * @description  This is called from the vf page when a user presses the "Save" button
    * @author       P Goodey
    * @date         June 2014
    * @see       WR 00070519
    * @UPDATED      Dec 2018 G James  Removed use of pageRef to allow for mobile navigation
    */     
    public void onSave(){
    
        // Set the default Page Reference for the return to the Lead
    //PageReference pageRef = new ApexPages.StandardController(moLead).view();
    
        // Save the Lead
        try{
      update moLead;
        }
        catch(Exception e){
            ApexPages.addMessages(e);
            //return null;
        }  
           
        // After save return the user to the Lead
        //return pageRef;
        
    }    
    
    
   /*
    * @name         resetStatus
    * @description  This is called from the vf page when a user changes the Status picklist
    * @author       P Goodey
    * @date         June 2014
    * @see       WR 00070519
    */     
    public void resetStatus(){
    
        // Set the original Status
        moLead.Status = msStatus;
        
    }  
    

    
}