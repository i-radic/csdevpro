public with sharing class iDepotController {
public List<iDepot__c> searchResults {get;set;}
public string CheckStatus = null;
public Map<Id,String> iDepMap = new Map<Id, String>();

    public iDepotController(ApexPages.StandardController controller) {

    }

public string FeedbackID = System.currentPageReference().getParameters().get('id');

 
public Attachment attachment {
  get {
      if (attachment == null)
      attachment = new Attachment();
      return attachment;
    }
  set;
  }

public Note note {
  get {
      if (note == null)
        note = new Note();
      return note ;
    }
  set;
  }

public List<Attachment> getAtt() {    
      return [SELECT ID, Name, Description, Body, BodyLength, CreatedByID, CreatedBy.Name, CreatedDate FROM Attachment WHERE ParentID = :FeedbackID  ORDER BY CreatedDate DESC ];
  }
public List<Note> getNoteList() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :FeedbackID ORDER BY CreatedDate DESC];
  }

public PageReference upload() {
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = FeedbackID ; // the record the file is attached to
    attachment.IsPrivate = false;
    if (attachment.Description == '') {
        attachment.adderror('Please enter a Title for the attachment'); 
        return null;
    }
    else {
    note.Body= 'CAT FAILED. Rejection Comments: ' + note.body;
    } 
    try {
      insert attachment;
    } 
    catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } 
    finally {
      attachment = new Attachment(); 
      attachment.body  = null;
 
      For(iDepot__c jmRec: [SELECT ID, Attachment__c FROM iDepot__c WHERE ID = :FeedbackID]){
       iDepMap.put(jmRec.Id, 'False');
       jmRec.Attachment__c = False;
       update jmRec;
       jmRec.Attachment__c = True;
       update jmRec;     
      } 
    }

    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
    
  }
  
public PageReference addNote() {
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = FeedbackID ; // the record the file is attached to
    //note.IsPrivate = false;
    note.Title= 'Feedback Note';
    Profile checkProfile;
    checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];
    
     
    try {
      insert Note;
      List<iDepot__c> IDep=[Select WSX_Next_Update__c,Notes__c,RecordTypeId From iDepot__c where Id=:FeedbackID];
      Id RcrdId=[Select Id From RecordType where Name='Wessex' and sobjecttype='iDepot__c'].Id;
      if(checkProfile.Name=='BTLB: Support Team'&&IDep.size()==1 && IDep[0].RecordTypeId==RcrdId)
      IDep[0].WSX_Next_Update__c=system.today()+30;
      
      if(IDep[0].RecordTypeId==RcrdId)
      IDep[0].Notes__c = Note.body;
      Update IDep[0];
      
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
}
 
}