@isTest
private class CS_TestUtil{
    static testMethod void testFastMethods(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Decimal sum = CS_Util.sum(new List<Decimal> { 1, 2, 3 });
        System.assertEquals(6, sum);
        System.assertNotEquals(CS_Util.utf8BOM(), null);
        //System.assertNotEquals(CS_Util.getConfigurationFieldsFromMetadata(), null);
    	System.assertEquals(0, CS_Util.prepare(null));
        System.assertEquals(34, CS_Util.prepare(34.00));
        System.assertEquals(34, CS_Util.decimalValue(34.00));
    	System.assertEquals(0, CS_Util.decimalValue(null));
    }
    
    static testMethod void testConfiguraitons(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
        cspl__PLReportSettings__c setting = new cspl__PLReportSettings__c(
            cspl__Contract_Term_Field__c = 'cscfga__Contract_Term__c',
            cspl__API_Field_name__c = 'Calculations_Product_Group__c'
        );
        insert setting;

        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'basketName', opp);

        cscfga__Product_Configuration__c pc1 = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);
        cscfga__Product_Configuration__c pc2 = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);
        cscfga__Product_Configuration__c pc3 = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);

        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');
        pc1.cscfga__Product_Definition__c = pd.Id;
        pc1.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc1.Calculations_Product_Group__c = 'BT Mobile';
        pc2.cscfga__Product_Definition__c = pd.Id;
        pc2.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc2.Calculations_Product_Group__c = 'BT Mobile';
        pc3.cscfga__Product_Definition__c = pd.Id;
        pc3.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc3.Calculations_Product_Group__c = 'BT Mobile';

        List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();
        productConfigurations.add(pc1);
        productConfigurations.add(pc2);
        productConfigurations.add(pc3);

        INSERT productConfigurations;

        List<cscfga__Attribute_Definition__c> attDefs = CS_TestDataFactory.generateAttributeDefinitions(true, new List<String>{'TestAtt'}, pd);
    
        
        List<cscfga__attribute__c> atts = new List<cscfga__attribute__c> {
            new cscfga__attribute__c(cscfga__Product_Configuration__c = pc1.Id, Name = 'TestAtt', cscfga__Value__c = 'TestValue',
            cscfga__Attribute_Definition__c = attDefs[0].Id),
            new cscfga__attribute__c(cscfga__Product_Configuration__c = pc2.Id, Name = 'TestAtt', cscfga__Value__c = 'TestValue',
            cscfga__Attribute_Definition__c = attDefs[0].Id),
            new cscfga__attribute__c(cscfga__Product_Configuration__c = pc3.Id, Name = 'TestAtt', cscfga__Value__c = 'TestValue',
            cscfga__Attribute_Definition__c = attDefs[0].Id)
        };
        insert atts;
        
        String CONTROLLER_NAME = 'test';

        CS_TestDataFactory.generatePLReportConfigRecords();

        BT_User_Group__c userGroup = CS_TestDataFactory.generateBTUserGroup(true, 'group');
        BT_User_Group_Member__c userGroupMember = CS_TestDataFactory.generateBTUserGroupMember(false, 'member');
        userGroupMember.BT_User_Group__c = userGroup.Id;
        INSERT userGroupMember;

        CS_Approval_Conditions__c ac = CS_TestDataFactory.generateApprovalCondition(true);
        CS_Approval_Paths__c ap = CS_TestDataFactory.generateApprovalPath(true, ac, userGroup);

        Test.startTest();

        Map<Id, cscfga__Product_Configuration__c> mapa = CS_Util.fillConfigurationsMap(productConfigurations, CONTROLLER_NAME);
        System.assertNotEquals(mapa, null);

        Map<Id, cscfga__Product_Configuration__c> productConfigurationsMap = CS_Util.getConfigurationsInBasket(basket.Id, CONTROLLER_NAME);
        
        try{
            CS_Util.upsertReportConfigurationAttachment(productConfigurationsMap.keySet());
        }
        catch(Exception exc){

        }
        
        Set<Id> basketIdsSet = CS_Util.fillBasketIdSet(productConfigurationsMap, CONTROLLER_NAME);
        System.assertNotEquals(basketIdsSet, null);

        Map<String, Map<String, Object>> productFamilyJSONMap = CS_Util.getProductFamiliesJSONMap(CONTROLLER_NAME); 
        System.assertNotEquals(productFamilyJSONMap, null);
        
        try{
            CS_Util.updateBasketTotals(productConfigurationsMap, basketIdsSet, null);
        }
        catch(Exception exc){

        }
        
        Map<Id, CS_BasketConfigurationWrapper> basketConfigsMap = CS_Util.fillBasketConfigsMap(basketIdsSet); 
        System.assertNotEquals(basketConfigsMap, null);

        CS_Util.fillProductConfigurationsSetInBasketConfigsMap(productConfigurationsMap, basketConfigsMap, productFamilyJSONMap);
        
        List<BT_User_Group_Member__c> userGroupMemberList = CS_Util.getUserGroupMember(CONTROLLER_NAME); 
        System.assertNotEquals(userGroupMemberList, null);

        List<CS_Approval_Paths__c> approvalPathList = CS_Util.getApprovalPaths(userGroupMemberList, CONTROLLER_NAME); 
        System.assertNotEquals(approvalPathList, null);

        List<CS_Approval_Conditions__c> approvalConditionsList = CS_Util.getApprovalConditions(approvalPathList, CONTROLLER_NAME); 
        System.assertNotEquals(approvalConditionsList, null);

        basketConfigsMap = CS_Util.calculateThresholdsForFamilies(basketConfigsMap); 
        System.assertNotEquals(basketConfigsMap, null);
        
        Map<Id, cscfga__Product_Configuration__c> configsToUpdate = CS_Util.checkApprovalProcessThresholds(basketConfigsMap, productFamilyJSONMap, approvalPathList, approvalConditionsList); //soql 7
        System.assertNotEquals(configsToUpdate, null);

        //Map<String, List<String>> definitionHierarchyMap = CS_Util.getProductDefinitionHierarchyJSONResourceMap();
        //System.assertNotEquals(definitionHierarchyMap, null);

        try{
            CS_Util.upsertReportConfigurationAttachment(mapa.keySet()); 
        }
        catch(Exception exc){

        }
        
        basketConfigsMap = CS_Util.calculateNPVThresholdsForFamilies(basketConfigsMap);
        System.assertNotEquals(basketConfigsMap, null);

        basketConfigsMap = CS_Util.calculateDilutedRevenueThresholdsForFamilies(basketConfigsMap);
        System.assertNotEquals(basketConfigsMap, null);

        Test.stopTest();
    }
}