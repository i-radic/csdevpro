//------------------------------------------------------------------------------
// CSV Table
//------------------------------------------------------------------------------
@isTest
private class CS_TestCSVTable
{
    static testMethod void createCSVString()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        List<CS_IRow> rows = new List<CS_IRow>();
        rows.add
            ( new CS_StringRow(new List<String>{'First; Name', 'Last\nName'}) );
        rows.add
            ( new CS_StringRow(new List<String>{'Jur"o', 'Mecko'}) );
        rows.add
            ( new CS_StringRow(new List<String>{null, ''}) );
        CS_ITable csvTable = new CS_CSVTable(rows, ';', '\n');
        System.assertEquals(CS_Util.utf8BOM() + '"First; Name";"Last\nName"\n"Jur""o";Mecko\n;\n', csvTable.generate());
        csvTable = new CS_CSVTable(rows, ',', '\r');
        System.assertEquals(CS_Util.utf8BOM() + 'First; Name,"Last\nName"\r"Jur""o",Mecko\r,\r', csvTable.generate());
        csvTable = new CS_CSVTable(rows, ',', '\r', true);
        System.assertEquals(CS_Util.utf8BOM() + '"First; Name","Last\nName"\r"Jur""o","Mecko"\r,\r', csvTable.generate());
    }
}