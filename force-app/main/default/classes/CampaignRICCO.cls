//RICCO Dialler File Extract Process

global class CampaignRICCO {

  @future (callout=true) 
  WebService static void GenerateDiallerExtract(String CampaignId) {
      
      Http http = new Http();
      HttpRequest req = new HttpRequest();
      
      // Gotham endpoint
      //req.setEndpoint('https://eric-gotham-salesforceservices.bt.com/SF.RICCO/DiallerExtractProcess.asmx/Start?CampaignId='+CampaignId);
      
      // Production endpoint
      req.setEndpoint('https://salesforceservices.bt.com/SF.RICCO/DiallerExtractProcess.asmx/Start?CampaignId='+CampaignId);
      
      req.setMethod('GET');
      if (!system.Test.isRunningTest()){
      	HttpResponse res = http.send(req);    	
      	// Log the XML content    
      	System.debug(res.getBody());
      }   
  }
}