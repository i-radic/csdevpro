/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FlowCRFCtrl_Test {

    static testMethod void myUnitTest(){
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
            }

        Account accnt = Test_Factory.CreateAccount();
        accnt.name = 'TESTCODE 2';
        accnt.sac_code__c = 'testSAC';
        accnt.Sector_code__c = 'CORP';
        accnt.LOB_Code__c = 'LOB';
        accnt.OwnerId = thisUser.Id;
        accnt.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(accnt);
        
        Opportunity O = new Opportunity();
        O.StageName = 'Created';
        O.Name = 'Test_Flow_Opp';
        O.CloseDate = Date.Today()+1;
        O.AccountId = accnt.id;
        Insert O;
        
        Flow_CRF__c CRF = new Flow_CRF__c();
        CRF.Account__c = accnt.id;
        CRF.Opportunity__c = O.id;
        insert CRF;
        
        Apexpages.Standardcontroller sc = new Apexpages.Standardcontroller(CRF);
        FlowCRFCtrl cntrl= new FlowCRFCtrl (sc);    
        ApexPages.currentPage().getParameters().put('Id',CRF.Id);
        Provide_Line_Details__c pld = cntrl.newProd; 
        cntrl.getProdList();
        cntrl.addProd();
    
    }

}