@isTest
Private class EmailTemBSClassicSLProduct_Test{
static testMethod void MyUnitTest() {
    
     Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        
		System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;        
        User uM = new User();
        uM.Username = '999999000123@bt.com';
        uM.Ein__c = 'NI_def';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = profileId;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});
     
    Account acc1 = Test_factory.createAccount();
         acc1.cug__c = 'CUGtest1Cug';
         acc1.OwnerId = uM.Id;
         acc1.Sub_Sector__c = 'Stratford Gold Customers';
         acc1.Sector__c = 'NI Enterprise';
         acc1.Account_Owner_Department__c = 'BT Ireland';
      
    insert acc1;
        
     BS_Classic_Sales_log__c bsClassic=new BS_Classic_Sales_log__c();
        bsClassic.Call_Outcome__c='Quote Sent';
        bsClassic.Account__c=acc1.Id;
        insert bsClassic;
    
    BSCLassicEmail__c blsEmail=new BSCLassicEmail__c();
        blsEmail.email_code__c='LN10';
        blsEmail.type__c='email'; 
        blsEmail.Email_Text__c = 'Morning appointments are usually between 09:00 and 13:00 and afternoon appointments';
        insert blsEmail;
       
        BSCLassicEmail__c blsFooter=new BSCLassicEmail__c();
        blsFooter.email_code__c='LN20';
        blsFooter.type__c='footer'; 
        blsFooter.Email_Text__c = 'footer statement';
        insert blsFooter;
    
    BS_Classic_SL_Product__c bls=new BS_Classic_SL_Product__c();
        bls.Product__c='PSTN 2 Year Resign';
        bls.Sales_Log__c=bsClassic.id;
        bls.Product__c='PSTN 2 Year Resign';
        bls.Product_Type2__c='Lines';
        bls.Quantity__c=1;
        bls.Sales_Log__c=bsClassic.id;
        bls.email_statements__c = 'LN10Test';
        insert bls;
    
 BS_Classic_SL_Product__c bls1=new BS_Classic_SL_Product__c();
        bls1.Product__c='PSTN 2 Year Resign';
        bls1.Sales_Log__c=bsClassic.id;
        bls1.Product__c='PSTN 2 Year Resign';
        bls1.Product_Type2__c='Lines';
        bls1.Quantity__c=1;
        bls1.Sales_Log__c=bsClassic.id;
        bls1.email_statements__c = 'LN20Test';
        insert bls1;    
    

    EmailTemBSClassicSLProduct controller = new EmailTemBSClassicSLProduct();
     controller.SLProductid = bsClassic.id ;
    controller.getBSClassicSLProduct();
    controller.getEmailFooter();
    
}
}
}