@isTest

private class Test_FeedbackController{
    static testMethod void runPositiveTestCases() {
        // test general feebdack      
        Feedback__c fbk = new Feedback__c (Feedback_Title__c = 'Test Feedback', Requested_Priority__c = 'low', Type__c = 'Issue',Description__c = '' ); 
        Database.Saveresult fbkResult = Database.insert(fbk);    
        //additional validation on CR
        Feedback__c fbk1 = new Feedback__c (Feedback_Title__c = 'Test Feedback', Requested_Priority__c = 'low', RecordTypeID = '01220000000PlHa', Type__c = 'Change Request', Description__c = 'Testing',Reason__c = 'Teast Reason' ); 
        Database.Saveresult fbkResult1 = Database.insert(fbk1);    
        Feedback__c fbk2 = new Feedback__c (Feedback_Title__c = 'Test Feedback', Status__c = 'Failed CAT', Requested_Priority__c = 'low', RecordTypeID = '01220000000PlHa', Type__c = 'Change Request', Description__c = 'Testing',Reason__c = 'Teast Reason' ); 
        Database.Saveresult fbkResult2 = Database.insert(fbk2);                
        // test attachment
        Blob attachBody = Blob.valueOf('attachment body');
        Attachment attach = new Attachment (OwnerId = UserInfo.getUserId(), ParentId = fbk1.ID, isPrivate = false, body = attachBody, Name = 'test');
        Database.Saveresult attachmentResult = Database.insert(attach);    
        Boolean failed = false;
        Feedback__c fbkStatus;
        try {
        fbkStatus= [SELECT ID, Status__c FROM Feedback__c WHERE ID = :fbk1.ID LIMIT 1];
        fbkStatus.Status__c = 'Failed CAT';

        update fbkStatus;
        } catch (Exception e) {
          failed = true;
        }

        
        // invoke controller actions 
        //System.currentPageReference().getParameters().put('id',fbkStatus.id);               
        FeedbackController controller = new FeedbackController(null);
        controller.addNote();  
        controller.upload();  
        controller.CATcancel();
        controller.CATno();
        controller.CATyes();
        controller.getAtt();
        controller.getNoteList();
        
        Blob attachBody1 = Blob.valueOf('attachment body');
        Attachment attach1 = new Attachment (OwnerId = UserInfo.getUserId(), ParentId = fbk.ID, isPrivate = false, body = attachBody1, Name = 'test');
        Database.Saveresult attachmentResult1 = Database.insert(attach1);    
        Boolean failed1 = false;
        Feedback__c fbkStatus1;
        try {
         fbkStatus1= [SELECT ID, Status__c FROM Feedback__c WHERE ID = :fbk.ID LIMIT 1];
        fbkStatus1.Status__c = 'Failed CAT';

        update fbkStatus1;
        } catch (Exception e) {
          failed1 = true;
        }

        
        // invoke controller actions 
        System.currentPageReference().getParameters().put('id',fbkStatus1.id);              
        FeedbackController controller1 = new FeedbackController(null);
        controller1.addNote();  
        controller1.upload();  
        controller1.CATcancel();
        controller1.CATno();
        controller1.CATyes();
        controller1.getAtt();
        controller1.getNoteList();
    }    
}