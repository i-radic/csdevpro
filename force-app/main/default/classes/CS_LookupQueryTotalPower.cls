global class CS_LookupQueryTotalPower implements cssmgnt.RemoteActionDataProvider   {

   global Map<String, Object> getData(Map<String, Object> inputMap) {
        Map<String, Decimal> returnMap = new Map<String, Decimal>(); 
        try{
            List<cspmb__Price_Item__c> priceItemList= [select id,Power__c from cspmb__Price_Item__c where id in :inputMap.keyset() ];
            if(!priceItemList.isEmpty()){
                decimal sum=0;
                for(cspmb__Price_Item__c oneitem : priceItemList) {
                    sum+=oneitem.Power__c;
                }
                returnMap.put ('TotalPower', sum);
                returnMap.put ('DataFound', 1);
            }else{
                 returnMap.put ('DataFound', 0);
            }
            
        }catch(Exception e){
            
        }
        return returnMap;
    }    
    
}