global with sharing class CustomButtonSendToService extends csbb.CustomButtonExt{
    public String performAction (String basketId) {
        
        String newUrl = CustomButtonSendToService.SendToService(basketId);
        String successMsg = 'Opportunity Stage is moved to Order Validation';
        //return '{"status":"ok","redirectURL":"' + newUrl + '"}';
        return '{"status":"ok","redirectURL":"' + newUrl+'","text":"'+ successMsg + '"}';

    } 
    
    public static String SendToService(String basketId){
    
        cscfga__Product_Basket__c pb = Database.query('SELECT Id, Name, cscfga__Opportunity__r.id FROM cscfga__Product_Basket__c WHERE Id=\'' + basketId + '\'');
        opportunity opp;  
        if(pb != null){      
            opp = Database.query('SELECT Id, Sent_to_Service__c,Sales_Stage_Detail__c FROM opportunity WHERE Id=\'' + pb.cscfga__Opportunity__r.id + '\'');
        }

        if(opp != null){
            opp.Sales_Stage_Detail__c = 'Order Validation';
            update opp;
        }
        
        
        return 'apex/BasketbuilderApp?id=' + basketId;
        
    }

}