@isTest
private class Test_BatchOpportunityForecastQuarter {
	
	static testMethod void testOne() {
		Account account = Test_Factory.CreateAccount();    
		insert account;
		Opportunity opp = Test_Factory.CreateOpportunity(account.Id);
		opp.CloseDate = system.today();
		insert opp;
        
		string q = 'select Close_Date_Fiscal_Quarter__c, zCurrent_Fiscal_Quarter__c, Net_ACV__c,SOV_GM__c,zCurrent_Quarter_Forecast_ACV__c, ACV_Calc__c, zCurrent_Quarter_Forecast_NIBR__c, NIBR_Next_Year__c, zCurrent_Quarter_Forecast_Vol__c, zCurrent_Quarter_Forecast_CY_NIBR__c, NIBR_Current_Year__c, Current_Quarter_Forecast_Amount__c, Amount, ForecastUpdatedQuarter__c from opportunity where zToProcessInForecastBatchQuarter__c = \'PROCESS\' and id=\'' + opp.Id+  '\' limit 10';
        batchOpportunityForecastQuarter b = new batchOpportunityForecastQuarter(q);
        
        Test.startTest();
        database.executeBatch(b, 1);
        Test.stopTest();
        
        //check results
        Opportunity opp1 = [select zCurrent_Quarter_Forecast_ACV__c, Net_ACV__c,SOV_GM__c,zCurrent_Quarter_Forecast_NIBR__c, zCurrent_Quarter_Forecast_Vol__c,zCurrent_Quarter_Forecast_CY_NIBR__c,Current_Quarter_Forecast_Amount__c, ForecastUpdatedQuarter__c, ACV_Calc__c, NIBR_Next_Year__c, NIBR_Current_Year__c, Amount from Opportunity where id = :opp.Id];
        System.assertNotEquals(opp1.ForecastUpdatedQuarter__c, null);
    }
    
    static testMethod void testSchedule(){
    	BatchScheduleOpportunityForecastQuarter s = new BatchScheduleOpportunityForecastQuarter();
        String sch = '0 0 8 13 2 ? 2050';
        String jobId = system.schedule('One Time Schedule', sch, s);
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        // Verify the next time the job will run
		System.assertEquals('2050-02-13 08:00:00', String.valueOf(ct.NextFireTime));
    }
}