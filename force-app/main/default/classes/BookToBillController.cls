public with sharing class BookToBillController {
public List<Feedback__c> searchResults {get;set;}
public string CheckStatus = null;
    public BookToBillController (ApexPages.StandardController controller) {

    }

public string FeedbackID = System.currentPageReference().getParameters().get('id');
 
      
public Attachment attachment {
  get {
      if (attachment == null)
      attachment = new Attachment();
      return attachment;
    }
  set;
  }
  
public Note note {
  get {
      if (note == null)
        note = new Note();
      return note ;
    }
  set;
  }

public List<Attachment> getAtt() {    
      return [SELECT ID, Name, Description, BodyLength, CreatedBy.Name, CreatedDate FROM Attachment WHERE ParentID = :FeedbackID  ORDER BY CreatedDate DESC ];
  }

public List<Note> getNoteList() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :FeedbackID ORDER BY CreatedDate DESC];
  }


public PageReference upload() {
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = FeedbackID ; // the record the file is attached to
    attachment.IsPrivate = false;
    if (attachment.Description == '') {
        attachment.adderror('Please enter a Title for the attachment'); 
        return null;
    }

    try {
      insert attachment;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      attachment = new Attachment(); 
      attachment.body  = null;
    }
 
    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
    
  }
  
public PageReference addNote() {
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = FeedbackID ; // the record the file is attached to
    //note.IsPrivate = false;
    note.Title= 'Book2Bill Note';
 
    try {
      insert Note;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
}
 
}