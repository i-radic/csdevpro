global class BatchCampaignSplitEloqua implements Database.Batchable<sobject>{
    /***********************************************************************                                                          
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION           AUTHOR           DATE            DETAIL                        FEATURES
    1.0        Venkata Srinivas M   26/10/2015      INITIAL DEVELOPMENT           Initial Build: 
    ***********************************************************************/
     
    
    public String query;
    public String filter;
    global BatchCampaignSplitEloqua(String q)
    {
        query=q;
        //filter=st;
    }
    global database.querylocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        
        //Declarations
        Map<String,BTLB_Master__c> sac_Codes= new Map<String,BTLB_Master__c>();
        
        //Retreived Campaign Members from the Passed Query
        List<CampaignMember> campMems=(List<CampaignMember>) scope;        
        system.debug('coming campMems'+campMems.size());
        
        Set<Id> campIds= new Set<Id>();
        Map<Id,Id> conIds= new Map<Id,Id>();
        Map<Id,Contact> accLBs= new Map<Id,Contact>();
        Map<String,Campaign> finalCampIds= new Map<String,Campaign>();
        List<Campaign> camList= new List<Campaign>();
        List<Campaign> parentCamps= new List<Campaign>();
        List<CampaignMember> updateList= new List<CampaignMember>();
        List<CampaignMember> deleteList= new List<CampaignMember>();  
        Map<CampaignMember,Campaign> campMemswithCamps= new Map<CampaignMember,Campaign>();
        Map<String,BTLB_Market_SAC__c> sac_Codes1= new Map<String,BTLB_Market_SAC__c>();
        CampaignMember newMember;
        Campaign newCampaign;
        String st='';
        Integer count1=0,count2=0;
        
        //sac codes along with BTLB Name from BTLB Master Object
        for(BTLB_Master__c s:[Select Id,Account_Owner_Department__c,Account_Owner__c from BTLB_Master__c])
        {
            sac_Codes.put(s.Account_Owner_Department__c,s);
        }
        system.debug('SAC Codes size::'+sac_Codes.size());             
        
        //conntact and Campaign Ids from the Campaign Members
        for(CampaignMember cm:campMems)
        {
            campIds.add(cm.CampaignId);
            conIds.put(cm.Id,cm.contactId);
        }        
        system.debug('Entered CampIds size::'+campIds.size());
        
        //Department of the Campaign Owner
        if(campIds.size()>0)
        {
        for(Campaign c:[Select Id,parentid,owner.Department,parent.Name from Campaign where parentid IN: campIds])
        {
            if(c.parentId!=null || c.parentId!='')
            {
                finalCampIds.put(c.owner.Department+':'+c.parent.Name.substring(0,c.parent.Name.length()-1),c);
            }
            else
            {
                finalCampIds.put(c.owner.Department,c);
            }
        }
        }        
        system.debug('Retrieved CampIds size::'+finalCampIds.size()+'  '+finalCampIds);
        
        //Department and Division of the Account from the contacts
        for(Contact cc:[Select Id,Account.Account_Owner_Department__c,Account.Account_Owner_Division__c from Contact where Id IN: conIds.Values()])
        {
            accLBs.put(cc.Id,cc);
        }
        
        for(CampaignMember cm:campMems)
        {  
            system.debug('********'+accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c+'------'+accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Division__c=='BTLB');
            
            count1++;
            if(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c!=null && accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Division__c=='BTLB')
            {
                count2++;
                if(finalCampIds.size()>0)
                {
                    if(finalCampIds.containsKey(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c+':'+cm.Campaign.Name.substring(0,cm.Campaign.Name.length()-1)))
                    {
                        if(finalCampIds.get(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c+':'+cm.Campaign.Name.substring(0,cm.Campaign.Name.length()-1)).parentid==cm.CampaignId)
                        {                               
                            campMemswithCamps.put(cm,finalCampIds.get(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c+':'+cm.Campaign.Name.substring(0,cm.Campaign.Name.length()-1)));
                        }
                        else
                        {
                            newCampaign= new Campaign();
                            newCampaign.parentid= cm.CampaignId;
                            st=accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c +':'+ cm.Campaign.Name;
                            if(st.length()<80){
                                newCampaign.Name= accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c +':'+ cm.Campaign.Name;
                            }
                            else
                            {
                                newCampaign.Name=st.left(79);
                                newCampaign.Name=newCampaign.Name+'~';
                            }
                            newCampaign.Campaign_Type__c='BTLB Split';
                            newCampaign.Campaign_Owner_Department__c=accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c;
                            if(sac_Codes.containsKey(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c)){
                                newCampaign.ownerId=sac_Codes.get(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c).Account_Owner__c;
                            }
                            newCampaign.Campaign_Split_Status__c='Child Campaign-No Need of Splitting';
                            camList.add(newCampaign);
                            campMemswithCamps.put(cm,newCampaign);
                            finalCampIds.put(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c+':'+cm.Campaign.Name.substring(0,cm.Campaign.Name.length()-1),newCampaign);
                        }
                    }
                    else
                    {
                        newCampaign= new Campaign();
                        newCampaign.parentid= cm.CampaignId;
                        st=accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c +':'+ cm.Campaign.Name;
                        if(st.length()<80){
                            newCampaign.Name= accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c +':'+ cm.Campaign.Name;
                        }
                        else
                        {
                            newCampaign.Name=st.left(79);
                            newCampaign.Name=newCampaign.Name+'~';
                        }
                        newCampaign.Campaign_Type__c='BTLB Split';
                        newCampaign.Campaign_Owner_Department__c=accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c;
                        if(sac_Codes.containsKey(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c)){
                            newCampaign.ownerId=sac_Codes.get(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c).Account_Owner__c;
                        }    
                        newCampaign.Campaign_Split_Status__c='Child Campaign-No Need of Splitting';
                        camList.add(newCampaign);
                        campMemswithCamps.put(cm,newCampaign);
                        finalCampIds.put(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c+':'+cm.Campaign.Name.substring(0,cm.Campaign.Name.length()-1),newCampaign);
                    }
                }
                else
                {
                    newCampaign= new Campaign();
                    newCampaign.parentid= cm.CampaignId;
                    system.debug('******'+ cm.contact.SAC_Code__c);
                    system.debug('Values--->'+accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c); 
                    st=accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c +':'+ cm.Campaign.Name;                     
                    if(st.length()<80){
                        newCampaign.Name= accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c +':'+ cm.Campaign.Name;
                    }
                    else
                    {
                        newCampaign.Name=st.left(79);
                        newCampaign.Name=newCampaign.Name+'~';
                    }
                    newCampaign.Campaign_Type__c='BTLB Split';
                    newCampaign.Campaign_Owner_Department__c=accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c;
                    if(sac_Codes.containsKey(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c)){
                        newCampaign.ownerId=sac_Codes.get(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c).Account_Owner__c;
                    }
                    newCampaign.Campaign_Split_Status__c='Child Campaign-No Need of Splitting';
                    camList.add(newCampaign);
                    campMemswithCamps.put(cm,newCampaign);
                    finalCampIds.put(accLBs.get(conIds.get(cm.Id)).Account.Account_Owner_Department__c+':'+cm.Campaign.Name.substring(0,cm.Campaign.Name.length()-1),newCampaign);
                }
            }
            st='';
        }
        
        system.debug('withBTLB--'+count2+'withoutBTLB::'+count1);
        system.debug('camList--->'+camList.size());
        Insert camList;
        system.debug('campMems---->'+campMemswithCamps.size());
        
        for(CampaignMember cm:campMemswithCamps.KeySet())
        {
            newMember= new CampaignMember();
            system.debug('CampainId----------'+campMemswithCamps.get(cm).Id);
            newMember.campaignId=campMemswithCamps.get(cm).Id;
            newMember.contactId=cm.contactId;
            newMember.Status=cm.Status;
            newMember.Unsuccessful_Call_Attempts__c=cm.Unsuccessful_Call_Attempts__c;
            newMember.Campaign_Member_Notes__c=cm.Campaign_Member_Notes__c;
            updateList.add(newMember);
            deleteList.add(cm);
        }
        system.debug('list Deleted---->'+deleteList.size()+'    '+deleteList);
        delete deleteList;
        system.debug('List Inserted'+updateList.size()+'    '+updateList);
        Insert updateList;
        
        
        //change the Campaign Split Status of Parent Campaign to 'Done Successfully'
        if(campIds.size()>0)
        {
        for(Campaign c:[Select Id,parentid,owner.Department,parent.Name,Campaign_Split_Status__c from Campaign where Id IN: campIds])
        {
           c.Campaign_Split_Status__c='Done Successfully'; 
           parentCamps.add(c);
        }
        } 
        update ParentCamps;
        
     }
     
     global void finish(Database.BatchableContext BC){
        
        system.debug('Done');  
    }
}