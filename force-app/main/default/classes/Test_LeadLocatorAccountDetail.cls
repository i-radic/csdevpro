@isTest
private class Test_LeadLocatorAccountDetail{
    
    static testMethod void myUnitTestLL() {
        
        Account ALL=new Account();
        ALL.Name='Test LL';
        ALL.SAC_Code__c='SACLL11';
        Insert ALL;
        
        Account ALLl=new Account();
        ALLl.Name='Test LLl';
        ALLl.SAC_Code__c='SACLL111';
        Insert ALLl;
        
        List<User> UserList = new List<User>();
        List<Lead_Locator__c> LeadLocatorList = new List<Lead_Locator__c>();
            
        Profile LLProfile1 = [select id from profile where name='Field: Standard User'];
        Profile LLProfile11 = [select id from profile where name='Corporate: Admin user'];
        User LLUser1=new User(alias ='LL1',email='LL1cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL1',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile13167cr@bt.com',EIN__c='LL11');        
        UserList.add(LLUser1);   
        
        User LLUser2=new User(alias ='LL2',email='LL2cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL2',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile22cr@bt.com',EIN__c='LL22');        
        UserList.add(LLUser2);
        
        User LLUser3=new User(alias ='LL3',email='LL3cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL3',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile33cr@bt.com',EIN__c='LL33');        
        UserList.add(LLUser3);
        
        User LLUser4=new User(alias ='LL4',email='LL4cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL4',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile44cr@bt.com',EIN__c='LL44');        
        UserList.add(LLUser4);
        
        User LLUser5=new User(alias ='LL5',email='LL5cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL5',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile55cr@bt.com',EIN__c='LL55');        
        UserList.add(LLUser5);
        
        User LLUser6=new User(alias ='LL46',email='LL46cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL46',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile466cr@bt.com',EIN__c='LL464');        
        UserList.add(LLUser6);
        
        User LLUser7=new User(alias ='LL57',email='LL57cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL57',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile557cr@bt.com',EIN__c='LL575');        
        UserList.add(LLUser7);
        
        User LLUser8=new User(alias ='LL578',email='LL578cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL578',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile11.Id, timezonesidkey='Europe/London',username='LLProfile5587cr@bt.com',EIN__c='LL5758');        
        UserList.add(LLUser8);
        
        Insert UserList;      
        
        Lead_Locator__c LL1=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='EngageIT SP',User__c=LLUser1.Id); 
        LeadLocatorList.add(LL1);
        
        Lead_Locator__c LL2=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='UVS Specialist',User__c=LLUser2.Id); 
        LeadLocatorList.add(LL2);
        
        Lead_Locator__c LL3=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Service Manager',User__c=LLUser3.Id); 
        LeadLocatorList.add(LL3);
        
        Lead_Locator__c LL4=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Business Manager',User__c=LLUser4.Id); 
        LeadLocatorList.add(LL4);
        
        Lead_Locator__c LL5=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Sales Manager (Primary)',User__c=LLUser5.Id); 
        LeadLocatorList.add(LL5);       
        
        Lead_Locator__c LL6=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Credit Support',User__c=LLUser5.Id); 
        LeadLocatorList.add(LL6);
        
        Lead_Locator__c LL7=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Desk Based Sales Manager',User__c=LLUser6.Id); 
        LeadLocatorList.add(LL7);
        
        Lead_Locator__c LL8=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Desk Based Account Manager',User__c=LLUser7.Id); 
        LeadLocatorList.add(LL8);
        
        Lead_Locator__c LL9=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Service Relationship Managers',User__c=LLUser8.Id); 
        LeadLocatorList.add(LL9);
        
        Lead_Locator__c LL10=new Lead_Locator__c(Account__c=ALL.Id,Person_Role__c='Business Direct Specialist',User__c=LLUser8.Id); 
        LeadLocatorList.add(LL10);
        
        Insert LeadLocatorList;
        
        Apexpages.Standardcontroller sc = new Apexpages.Standardcontroller(ALL);
        LeadLocatorAccountDetail LLAD=new LeadLocatorAccountDetail(sc);
        LLAD.getLeadLocsOne();
        LLAD.getLeadLocsTwo();
        LLAD.NewLeadLocator();
        LLAD.EditLeadLocator();
        //LLAD.DelLeadLocator();
        LLAD.getNButton();
        LLAD.getELink();
        
        Delete LL2;
        Delete LL3;
        Delete LL4;         
        Delete LL5;
        Delete LL6; 
        Delete LL7;
        Delete LL8;
        Delete LL10; 
        try{
            system.runAs(LLUser8){
            	Delete LL9;
        }
        }Catch(Exception Ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains('You are not allowed to delete this record') ? true : false;
            System.assertEquals(expectedExceptionThrown, True);            
        }        
        
    }
}