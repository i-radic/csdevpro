global class CS_DiscountBasketSaveObserver implements csdiscounts.IBasketObserver {
	
	public CS_DiscountBasketSaveObserver() {}

	private static String CONTROLLER_NAME = 'CS_DiscountBasketSaveObserver';

	String returnMessage = '';

	public String execute(String basketId) {

		if (CS_ProductBasketService.containsCalculateProductGroup(basketId)) {
			return returnMessage;
		}

		try {
			Map<Id, cscfga__Product_Configuration__c> configs = CS_Util.getConfigurationsInBasket(basketId, CONTROLLER_NAME);
			cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
			
			returnMessage = CS_ProductConfigurationService.calculateApprovals(configs, basket, CONTROLLER_NAME, true);
			
			update CS_ProductBasketService.desyncBasket(new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket}).values();
        }
		catch(Exception exc){
			logger('csexception = ' + exc);
		}
		
		logger('returnMessage = ' + returnMessage);
		return returnMessage;
	}

    @TestVisible private void logger(String msg){
    	system.debug(LoggingLevel.WARN, 'CS_DiscountBasketSaveObserver.' + msg);
    }
}