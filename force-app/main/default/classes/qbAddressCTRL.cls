public class qbAddressCTRL{ 

    public Order_Address__c orderAddress;  
    public Boolean test= true;
    public string refferalPage = ApexPages.currentPage().getParameters().get('refPage');
    public string qbId = ApexPages.currentPage().getParameters().get('qbId'); 
    public string postcode = ApexPages.currentPage().getParameters().get('pc');
    public string accId = ApexPages.currentPage().getParameters().get('AccId');
    public string aType = ApexPages.currentPage().getParameters().get('aType');
    
    List<Addresswrapper> ResultsList = new List<Addresswrapper>();
    List<Address> selectedAddress = new List<Address>(); 

    public string createdAddrId {get; Set;}

    public qbAddressCTRL(ApexPages.StandardController controller) {
        orderAddress = (Order_Address__c)controller.getRecord();
    }

    public Static String aj{
        get {         
            aj = qbOrderCTRL.j;
            return aj;
        }
        set;
    } 
    
    //-----------------
     public Boolean getTest(){
        return test;
    }
    
    public qbAddressCTRL(Order_Address__c orderAddress_edit){
         //orderAddress = orderAddress_edit;
    }
    
    //public PageReference CreateAddress(){                                     
        //if(Test_Factory.GetProperty('IsTest') == 'yes') {
            //return null;
        //}
        //PageReference PageRef = new PageReference('/apex/qbSite?id='+Order_Address__c.Id+'&refPage='+refferalPage);
        //PageRef.setRedirect(true);      
        //return PageRef;                 
    //}

    public PageReference Create() {       
        If (!ValidateMandatoryFields())  return null;
        
        try {                         
            string NADKey;
               
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                NADKey = 'test';
            }           
            else {                          
                NADKey = SalesforceCRMService.CreateNADAddress(crmAddress());               
            }  
            system.debug('mpk NADKey  '+NADKey +'Test_Factory.GetProperty :'+Test_Factory.GetProperty('IsTest'));              
            if (string.isNotBlank(NADKey)){ 
                system.debug('mpk accId  '+accId);
                orderAddress.Account__c = accId; 
                orderAddress.NAD__c = NADKey;
                if(Test_Factory.GetProperty('IsTest') != 'yes') {
                    system.debug('mpk upsert orderAddress  '+orderAddress);
                    upsert orderAddress;                    
                    updateAddressIdOnSite(orderAddress.Id);
                    System.debug('mpk orderAddress.Id= '+orderAddress.Id);    
                }
            } 
            else {                
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The NAD service has rejected the request, please enter a valid address!'));                
            }                   
        }
        catch (System.CalloutException ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There appears to be problem connecting to the NAD service, please try again!'));            
        }
        return null;                      
   }
    @TestVisible	
    private void updateAddressIdOnSite(string orderAddressId)
    {
        if (string.isNotBlank(orderAddressId))
            {
                system.debug('qbId==='+qbId);
                Order_Site__c orderSite = [SELECT Id,Address_Delivery__c,Address_Installation__c FROM Order_Site__c WHERE Id = :qbId];
                if (aType.equals('del'))
                    orderSite.Address_Delivery__c = orderAddressId;
                else
                    orderSite.Address_Installation__c = orderAddressId;                     
                update orderSite;

                System.debug('mpk orderAddress.Id= '+orderSite.Id);
            }           
    }
    
    @TestVisible
    private boolean ValidateMandatoryFields() {
        System.debug('mpk ValidateMandatoryFields entered');
        if (string.isBlank(orderAddress.Street__c) || string.isBlank(orderAddress.Post_Town__c) ||string.isBlank(orderAddress.Post_Code__c) ||
            string.isBlank(orderAddress.County__c) || string.isBlank(orderAddress.Country__c))
        {
            System.debug('mpk ValidateMandatoryFields false');
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please check and enter all the mandatory fields.'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        return true;
    }

    
    public SalesforceCRMService.CRMAddress crmAddress(){ 
        SalesforceCRMService.CRMAddress crmAddress = new SalesforceCRMService.CRMAddress(); 
		crmAddress.BuildingName = orderAddress.Building_Name__c;
        crmAddress.Name = orderAddress.Building_Name__c;
        crmAddress.SubBuilding = orderAddress.Sub_Building__c;
        crmAddress.BuildingNumber = orderAddress.Number__c;
        crmAddress.POBox = orderAddress.PO_Box__c;
        crmAddress.Street = orderAddress.Street__c;
        crmAddress.Locality = orderAddress.Locality__c;
        crmAddress.Town = orderAddress.Post_Town__c;
        crmAddress.County = orderAddress.County__c; 
        crmAddress.Country = orderAddress.Country__c; 
        crmAddress.Postcode = orderAddress.Post_Code__c;
        System.debug('mpk crmAddress '+crmAddress);    
        return crmAddress; 
    }
	
	/*CRMAddress:[BuildingName=null, BuildingNumber=B123, Country=UK, County=Lanarkshire, DoubleDependentLocality=null, 
	ExchangeGroupCode=null, IdentifierId=null, IdentifierName=null, IdentifierValue=null, Locality=locality, Name=TestBuilding, 
	POBox=PO box, PostCode=G2 8LA, Street=York Street, SubBuilding=subb, Town=Glasgow]
	*/


    public PageReference Back(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
        //if (refferalPage == 'new'){
            //PageReference PageRef = new PageReference('/apex/qbSite?id='+Order_Address__c.Id);
            //PageRef.setRedirect(true);
                       
            //if(Test_Factory.GetProperty('IsTest') == 'yes') {
                //return null;
            //}    
            //return PageRef;                       
        //}
        //if (refferalPage == 'update'){
            //PageReference PageRef = new PageReference('/apex/qbSite?id='+Order_Address__c.Id);
            //PageRef.setRedirect(true);
            //if(Test_Factory.GetProperty('IsTest') == 'yes') {
                //return null;
            //}   
            //return PageRef;    
        //}
        //if (refferalPage == 'detail'){
            //PageReference PageRef = new PageReference('/'+Order_Address__c.Id);
            //PageRef.setRedirect(true);
            //if(Test_Factory.GetProperty('IsTest') == 'yes') {
                //return null;
            //}      
            //return PageRef;  
        //}
        //return null;

        PageReference PageRef = new PageReference('/apex/qbSite?id='+qbId);
        PageRef.setRedirect(true);
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return PageRef;  
        
        
    }
    
    public List<Addresswrapper> pullAddr {
        get {     
            return pullAddresses();
        }        
    }  
    
    public List<Addresswrapper> pullAddresses()
    {                      
        ResultsList.Clear();
        SalesforceCRMService.CRMAddressList NADResponse = null; 
            
        if(Test_Factory.GetProperty('IsTest') == 'yes') {            
            NADResponse = new SalesforceCRMService.CRMAddressList();
            NADResponse.Addresses = new SalesforceCRMService.CRMArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceCRMService.CRMAddress[0];
                    
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                system.debug('POSTCODE : '+postCode);
                NADResponse = SalesforceCRMService.GetNADAddress(postCode,false);
                system.debug('nadresponse ' + NADResponse);
              
                if(NADResponse == null || NADResponse.Addresses == null) {
                    return null;
               }               
           }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;
            }                                              
        }    
        for(SalesforceCRMService.CRMAddress sfsAddress : NADResponse.Addresses.Address){
            
            system.debug('LLLLLLLLL '+sfsAddress );
            
            Address a = new Address();                    
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
            ResultsList.add(new Addresswrapper(a)); 
        }
        if(ResultsList.size()>=1){
            system.debug('ResultsList :'+ResultsList);
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please \'Create Address\'.'));          
             return null;
        }
    }
    
    public PageReference getSelected()
    {
        selectedAddress.clear();
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {            
            Address a = new Address();            
            //a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
                
            Addresswrapper wrapper = new Addresswrapper(a);
            wrapper.selected = true;
            ResultsList.add(wrapper);                        
        }        
        
        for(Addresswrapper accwrapper : ResultsList){
            if(accwrapper.selected == true){
                orderAddress = new Order_Address__c (); 
                orderAddress.Account__c = accId;             
                orderAddress.Street__c = accwrapper.acc.Street;
                orderAddress.County__c = accwrapper.acc.County;
                orderAddress.Post_Code__c = accwrapper.acc.PostCode;
                orderAddress.Country__c = accwrapper.acc.Country;  
                               
                //Address Key#########
                orderAddress.NAD__c = accwrapper.acc.IdentifierValue;
                //####################                    
                    
                orderAddress.Building_Name__c = accwrapper.acc.Name;
                orderAddress.PO_Box__c = accwrapper.acc.POBox;
                orderAddress.Number__c = accwrapper.acc.BuildingNumber;
                orderAddress.Locality__c = accwrapper.acc.Locality;
                orderAddress.Post_Code__c = accwrapper.acc.PostCode;
                orderAddress.Post_Town__c = accwrapper.acc.Town;
                orderAddress.Sub_Building__c = accwrapper.acc.SubBuilding;                                         
                    
                if(Test_Factory.GetProperty('IsTest') == 'yes') {
                    return null;                      
                }
                else {
                    try {
						System.debug('orderAddresssssss = '+orderAddress);                
						insert orderAddress;                    
						updateAddressIdOnSite(orderAddress.Id);

						System.debug('mpk selected orderAddress.Id'+orderAddress.Id);
						Return null;//ReDirect();
					}
					catch(System.DmlException dmlEx) {
						System.debug('mpk address: '+dmlEx.getMessage());
						ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'You have tried to add an address already in the list - Please use existing address'));
						return null;
					}      
                }    
            } 
        }   
        return null;
    }
    
    public List<Address> getselectedAddress()
    {
        if(selectedAddress.size()>0)
            return selectedAddress;
        else
            return null;
    }    
    
    public with sharing class Addresswrapper
    {
        public Address acc {get; set;}
        public Boolean selected {get; set;}
        public Addresswrapper(Address a)
        {
            acc = a;
            selected = false;
        }
    }
 
    public class Address {
        public String IdentifierId {get; set;}
        public String IdentifierName {get; set;}
        public String IdentifierValue {get; set;}        
        public String Country {get; set;}
        public String County {get; set;}
        public String Name {get; set;}
        public String POBox {get; set;}
        public String BuildingNumber {get; set;}
        public String Street {get; set;}
        public String Locality {get; set;}
        public String DoubleDependentLocality {get; set;}
        public String PostCode {get; set;}
        public String Town {get; set;}
        public String SubBuilding {get; set;}
    } 





		/*

	public Void btnetToAdd(){     
        // ######################################  BTnet products to add into back and insert into product object table  ######################################   
        productsToAdd = ''; 
        string bTnetVariant = siteRec.BTnet_Variant__c;
        Boolean bTnetRouter = siteRec.BTnet_BT_Managed_Router__c;
        string bTnetTerm = siteRec.BTnet_Contract_Term__c;
        string bTnetTermSTR  = (bTnetTerm != '' ? bTnetTerm.split(' ')[0] : '0');
        integer bTnetTermVal  = Integer.valueOf(bTnetTermSTR);
        
        string bTnetRegionPRI = siteRec.BTnet_Pricing_Region__c;
        string bTnetBearerPRI = siteRec.BTnet_Bearer_Speed__c;        
        string bTnetBandwidthPRI = siteRec.BTnet_Port_Speed__c != '' ? siteRec.BTnet_Port_Speed__c.split(' ')[0]:''; //btnetPortSpeed
        integer bTnetBandwidthValPRI = Integer.valueOf(bTnetBandwidthPRI);
        Decimal bTnetDistancePRI = siteRec.BTnet_Distance__c;
        Decimal bTnetInitialPRI = siteRec.BTnet_Initial_Price__c;
        Decimal bTnetRecurringPRI = siteRec.BTnet_Recurring_Price__c;
        Decimal bTnetWinPrice = siteRec.BTnet_Win_Price__c != null ? siteRec.BTnet_Win_Price__c : 0;
        
        List<QuoteBuilder_BTnet_Pricing__c> priceBTnet_PRI, priceBTnet_SEC =null;
        
        Decimal TotalRev_SEC=0,TotalBRRev_SEC=0,TotalBWRev_SEC=0,TotalCost_SEC=0,TotalBWRev_OFFER=0;
        Decimal TotalRev_PRIM=0,TotalBRRev_PRIM=0,TotalBWRev_PRIM=0,TotalCost_PRIM=0;
        Decimal StdEBIT, CalcEBIT, StdRental, TotalWinRev, calcRev, OfferPrice, TotalRevCalculated = 0; 
        Decimal connCharge_PRI,conncharge_SEC=0 ;
        Decimal TotalRev, TotalCost, TotalConnCharge,calcRevPRI,calcRevSEC = 0;
        Decimal stdDistRentalPRI,stdBRRentalPRI,stdBWRentalPRI=0;
        Decimal stdDistRental,stdBRRental,stdBWRental=0;
        Boolean BDUK_Contract = false;	//TODO: Andrew said that this can be ignored for now
        Decimal WES_OFFER = 2;
        Decimal LA_OFFER = 3;
        //bTnetBearerPRI = bTnetBearerPRI.contains('Offer') ? bTnetBearerPRI.split(' ')[0]+' '+bTnetBearerPRI.split(' ')[1] : bTnetBearerPRI;
        system.debug('mpk Variant__c=\''+bTnetVariant+'\' AND Router__c =\''+bTnetRouter+'\' AND Contract_Term__c=\''+bTnetTerm+'\' AND Pricing_Region__c =\''+bTnetRegionPRI+'\' AND Bearer__c =\''+bTnetBearerPRI+'\' AND Bandwidth__c=\''+bTnetBandwidthPRI+'--'+btnetPortSpeed);
        
        //Primary
        priceBTnet_PRI = [SELECT  Id, Name, Bandwidth_Variable__c,BDUK__c,Bearer_Variable__c,Connection_Cost__c,Fixed_Bandwidth_Cost__c,
                          Fixed_Bandwidth_Cost_Overhead__c,Fixed_Bearer_Cost__c,Fixed_Bearer_Cost_Distance__c,Fixed_Bearer_Cost_KM_or_Pair__c,
                          Fixed_Bearer_Price__c,Fixed_Bearer_Price_Distance__c,Fixed_Bearer_Price_KM_or_Pair__c,LA__c,WES__c
                          FROM QuoteBuilder_BTnet_Pricing__c
                          WHERE Variant__c=:bTnetVariant AND Router__c =:bTnetRouter AND Contract_Term__c=:bTnetTerm AND 
                          Pricing_Region__c =:bTnetRegionPRI AND Bearer__c =:bTnetBearerPRI AND BDUK__c=:BDUK_Contract AND
                          (Bandwidth__c='' OR Bandwidth__c=:bTnetBandwidthPRI) ORDER BY Bandwidth__c];
//Primary Line
        if(priceBTnet_PRI.size() > 0) //should get 2 Rec(BR & BW) if BDUK_Contract = false else only one BR
        {
            system.debug('mpk priceBTnet- '+priceBTnet_PRI[0].Id);
            Decimal fixedBRPrice_PRI = 0;
            Decimal fixedBRDistPrice_PRI = 0;
            Decimal BWPrice_PRI = 0;
            Decimal fixedBRCost_PRI = 0;
            Decimal fixedBRDistCost_PRI = 0;            
            
            fixedBRPrice_PRI = (BDUK_Contract == true) ? priceBTnet_PRI[0].Fixed_Bearer_Price__c+3000 : priceBTnet_PRI[0].Fixed_Bearer_Price__c;	
            fixedBRCost_PRI = priceBTnet_PRI[0].Fixed_Bearer_Cost__c;            
            connCharge_PRI = priceBTnet_PRI[0].Connection_Cost__c;
            
            if(bTnetDistancePRI>0){
                fixedBRDistPrice_PRI = priceBTnet_PRI[0].Fixed_Bearer_Price_Distance__c + (bTnetDistancePRI * priceBTnet_PRI[0].Fixed_Bearer_Price_KM_or_Pair__c);	
                fixedBRDistCost_PRI = priceBTnet_PRI[0].Fixed_Bearer_Cost_Distance__c + (bTnetDistancePRI * priceBTnet_PRI[0].Fixed_Bearer_Cost_KM_or_Pair__c);
                
                if(bTnetTerm == '1 Year' && bTnetBearerPRI =='100 Mbps (Offer)')
                    BWPrice_PRI= WES_OFFER;
                else
                    BWPrice_PRI= priceBTnet_PRI[1].WES__c;  //TODO: BAD coding, will change it.                   
            }
            else{
                if(bTnetTerm == '1 Year' && bTnetBearerPRI =='100 Mbps (Offer)')
                    BWPrice_PRI= LA_OFFER;
                else
                    BWPrice_PRI= priceBTnet_PRI[1].LA__c; //TODO: BAD coding, will change it.  
            }
            system.debug('mpk fixedBRDistPrice_PRI = '+fixedBRDistPrice_PRI);
            system.debug('mpk fixedBRDistCost_PRI = '+fixedBRDistCost_PRI);
            system.debug('mpk BWPrice_PRI = '+BWPrice_PRI);
            
            //TotalBRRev_PRIM=BO4 =(BB4+IF(L4>0,BC4,0)+(L4*BD4))
            TotalBRRev_PRIM = fixedBRPrice_PRI + fixedBRDistPrice_PRI;
            
            //TotalBWRev_PRIM=BP4 =+(LEFT(H4,1)*IF(AND(H4<>"1 Yr",K4="100MO"),IF(L4>0,BU4,BT4)*J4,IF(L4>0,BM4,BL4)*J4))				
            TotalBWRev_PRIM = bTnetTermVal * BWPrice_PRI * bTnetBandwidthValPRI;
            
            //TotalRev_PRIM=BQ4=BO4+BP4
            TotalRev_PRIM = TotalBRRev_PRIM + TotalBWRev_PRIM;            
            
            //TotalCost_PRIM=BR4 =BE4+IF(L4>0,BF4,0)+(BG4*L4)+(BH4*BO4)+(BI4*(LEFT(H4,1)*IF(L4>0,BM4,BL4)*J4))+BJ4+(J4*BK4)
            TotalCost_PRIM = fixedBRCost_PRI + fixedBRDistCost_PRI;
            TotalCost_PRIM += (priceBTnet_PRI[0].Bearer_Variable__c * TotalBRRev_PRIM);
            TotalCost_PRIM += (priceBTnet_PRI[0].Bandwidth_Variable__c * TotalBWRev_PRIM);
            TotalCost_PRIM += priceBTnet_PRI[0].Fixed_Bandwidth_Cost_Overhead__c + (bTnetBandwidthValPRI * priceBTnet_PRI[0].Fixed_Bandwidth_Cost__c);
            
            stdDistRentalPRI= (bTnetDistancePRI * priceBTnet_PRI[0].Fixed_Bearer_Price_KM_or_Pair__c)/bTnetTermVal; //DU4=L4*BD4/LEFT($H4,1)            
            stdBRRentalPRI = ((TotalBRRev_PRIM-connCharge_PRI)/bTnetTermVal)-stdDistRentalPRI; //DT4=((BO4-BN4)/LEFT($H4,1))-DU4
            if(bTnetBearerPRI =='100 Mbps (Offer)')//DV4=IF(AND(K4="100MO"),S4-SUM(DT4:DU4),(BV4)/LEFT($H4,1))
            	stdBWRentalPRI = ((TotalRev_PRIM-connCharge_PRI)/bTnetTermVal)-(stdBRRentalPRI+stdDistRentalPRI); 
            else
                stdBWRentalPRI = TotalBWRev_PRIM/bTnetTermVal;
            
            calcRevPRI = PriceFind(bTnetBandwidthValPRI,bTnetBearerPRI,stdBRRentalPRI,stdDistRentalPRI,stdBWRentalPRI,bTnetWinPrice,'TOT'); //EQ4
            
            //TotalRev = TotalRev_PRIM; // TotalBRRev_PRIM + (bTnetTermVal * (bTnetDistancePRI>0 ? priceBTnet_PRI[0].WES__c:priceBTnet_PRI[0].LA__c) * bTnetBandwidthPRI) ;	
            //TotalCost = TotalCost_PRIM;
        }
//Secondary Line
        if(bTnetVariant=='Loadbalancing' || bTnetVariant=='Failover' || bTnetVariant=='Backup')	
        {
            string bTnetRegionSEC = siteRec.BTnet_Pricing_Region_Secondary__c;
            string bTnetBearerSEC = siteRec.BTnet_Bearer_Speed_Secondary__c;        
            string bTnetBandwidthSEC = siteRec.BTnet_Port_Speed_Secondary__c != '' ? siteRec.BTnet_Port_Speed_Secondary__c.split(' ')[0]:'';//btnetPortSpeed2
            integer bTnetBandwidthValSEC = Integer.valueOf(bTnetBandwidthSEC);
            Decimal bTnetDistanceSEC = siteRec.BTnet_Distance_Secondary__c;
            Decimal bTnetInitialSEC = siteRec.BTnet_Initial_Price_Secondary__c;
            Decimal bTnetRecurringSEC = siteRec.BTnet_Recurring_Price_Secondary__c;
            
            system.debug('mpk Variant__c=\''+bTnetVariant+'\' AND Router__c ='+bTnetRouter+' AND Contract_Term__c=\''+bTnetTerm+'\' AND Pricing_Region__c =\''+bTnetRegionSEC+'\' AND Bearer__c =\''+bTnetBearerSEC+'\' AND Bandwidth__c=\''+bTnetBandwidthSEC+'--'+btnetPortSpeed);
            
            priceBTnet_SEC = [SELECT  Id, Name, Bandwidth_Variable__c,BDUK__c,Bearer_Variable__c,Connection_Cost__c,Fixed_Bandwidth_Cost__c,
                              Fixed_Bandwidth_Cost_Overhead__c,Fixed_Bearer_Cost__c,Fixed_Bearer_Cost_Distance__c,Fixed_Bearer_Cost_KM_or_Pair__c,
                              Fixed_Bearer_Price__c,Fixed_Bearer_Price_Distance__c,Fixed_Bearer_Price_KM_or_Pair__c,LA__c,WES__c
                              FROM QuoteBuilder_BTnet_Pricing__c
                              WHERE Variant__c=:bTnetVariant AND Router__c =:bTnetRouter AND Contract_Term__c=:bTnetTerm AND 
                              Pricing_Region__c =:bTnetRegionSEC AND Bearer__c =:bTnetBearerSEC AND BDUK__c=:BDUK_Contract AND
                              (Bandwidth__c='' OR Bandwidth__c=:bTnetBandwidthSEC) ORDER BY Bandwidth__c];
            
            if(priceBTnet_SEC.size() > 0) //should get 2 Rec(BR & BW) if BDUK_Contract = false else only one BR
            {
                system.debug('mpk Secondary-'+priceBTnet_SEC[0].Id);
                Decimal fixedBRPrice = 0;
                Decimal fixedBRDistPrice = 0;
                Decimal BWPrice = 0;
                Decimal fixedBRCost = 0;
                Decimal fixedBRDistCost = 0;
                
                fixedBRPrice = priceBTnet_SEC[0].Fixed_Bearer_Price__c;	
            	fixedBRCost = priceBTnet_SEC[0].Fixed_Bearer_Cost__c;
                conncharge_SEC = priceBTnet_SEC[0].Connection_Cost__c;
                
                if(bTnetDistanceSEC>0){
                    fixedBRDistPrice = priceBTnet_SEC[0].Fixed_Bearer_Price_Distance__c + (bTnetDistanceSEC * priceBTnet_SEC[0].Fixed_Bearer_Price_KM_or_Pair__c);
                    fixedBRDistCost = priceBTnet_SEC[0].Fixed_Bearer_Cost_Distance__c + (bTnetDistancePRI * priceBTnet_SEC[0].Fixed_Bearer_Cost_KM_or_Pair__c);
                    if(bTnetBearerSEC =='100 Mbps (Offer)')
                        BWPrice= WES_OFFER;
                    else
                        BWPrice= priceBTnet_SEC[1].WES__c;  //TODO: BAD coding, will change it.                   
                }
                else{
                    if(bTnetBearerSEC =='100 Mbps (Offer)')
                        BWPrice= LA_OFFER;
                    else
                        BWPrice= priceBTnet_SEC[1].LA__c; //TODO: BAD coding, will change it.  
                }
                system.debug('mpk fixedBRDistPrice = '+fixedBRDistPrice);
                system.debug('mpk fixedBRDistCost = '+fixedBRDistCost);
                system.debug('mpk BWPrice = '+BWPrice);
                
                TotalBRRev_SEC = fixedBRPrice + fixedBRDistPrice;
                
                if(bTnetVariant == 'Loadbalancing'){
                    //TotalBWRev_SEC = bTnetTermVal * (bTnetBearerSEC=='100MO')? (bTnetDistanceSEC>0? WES_OFFER:LA_OFFER):((bTnetDistanceSEC > 0 ? priceBTnet_SEC[0].WES__c : priceBTnet_SEC[0].LA__c) * bTnetBandwidthValSEC) ;
                    TotalBWRev_SEC = bTnetTermVal * BWPrice * bTnetBandwidthValSEC ;
                }
                
                TotalRev_SEC= TotalBRRev_SEC + TotalBWRev_SEC;	
                
                if(bTnetVariant != 'Backup' && bTnetVariant != 'Failover'){
                    TotalBWRev_OFFER = bTnetTermVal * BWPrice * bTnetBandwidthValSEC;
                }

                //TotalCost_SEC=CQ4 =CD4+IF(P4>0,CE4,0)+(CF4*P4)+(CG4*CN4)+(CH4*CU4)+CI4+(N4*CJ4)
                TotalCost_SEC = fixedBRCost + fixedBRDistCost;
                TotalCost_SEC += priceBTnet_SEC[0].Bearer_Variable__c * TotalBRRev_SEC;
                TotalCost_SEC += priceBTnet_SEC[0].Bandwidth_Variable__c *TotalBWRev_OFFER;
                TotalCost_SEC += priceBTnet_SEC[0].Fixed_Bandwidth_Cost_Overhead__c + (bTnetBandwidthValSEC * priceBTnet_SEC[0].Fixed_Bandwidth_Cost__c);
                
                stdDistRental= (bTnetDistancePRI * priceBTnet_PRI[0].Fixed_Bearer_Price_KM_or_Pair__c)/bTnetTermVal; //DU4=L4*BD4/LEFT($H4,1)            
            	stdBRRental = ((TotalBRRev_PRIM-connCharge_PRI)/bTnetTermVal)-stdDistRental; //DT4=((BO4-BN4)/LEFT($H4,1))-DU4
            	if(bTnetBearerSEC =='100 Mbps (Offer)')//DV4=IF(AND(K4="100MO"),S4-SUM(DT4:DU4),(BV4)/LEFT($H4,1))
                    stdBWRental = ((TotalRev_SEC-connCharge_PRI)/bTnetTermVal)-(stdBRRental+stdDistRental); 
                else
                    stdBWRental = TotalBWRev_OFFER/bTnetTermVal;
                
                calcRevSEC = PriceFind(bTnetBandwidthValSEC,bTnetBearerSEC,stdBRRental,stdDistRental,stdBWRental,bTnetWinPrice,'TOT'); //EQ4
            
                //TotalCost = TotalCost_SEC;
                //TotalRev = TotalRev_SEC;
            }			
        }
//Secondary Line End


		TotalRev = TotalRev_PRIM + TotalRev_SEC;

        TotalCost = TotalCost_PRIM + TotalCost_SEC;
		
        system.debug('mpk TotalConnCharge = '+connCharge_PRI+' + '+connCharge_SEC);
        TotalConnCharge = connCharge_PRI + connCharge_SEC;
        
        TotalWinRev = (bTnetWinPrice > 0) ? (bTnetWinPrice * bTnetTermVal) + TotalConnCharge : 0;        
        
        //calcRev(ER4) =((EQ4+DZ4)*LEFT(H4,1))+CM4+BN4
        calcRev = (calcRevPRI + calcRevSEC) * bTnetTermVal + TotalConnCharge;
        
		//TotalRev(DH4) =IF(ER4="",DF4,ER4)   --   (calcRev='')? TotalWinRev : calcRev;
        TotalRevCalculated = (calcRev > 0) ? calcRev : TotalWinRev;
        
        system.debug('mpk TotalRev = '+TotalRev+' + '+TotalBWRev_OFFER+' - '+TotalBRRev_SEC+' - '+TotalCost);
        system.debug('mpk TotalRevCalculated = '+TotalRevCalculated);
        system.debug('mpk StdEBIT = '+StdEBIT);
 
        StdEBIT	= (TotalRev+TotalBWRev_OFFER-TotalBRRev_SEC-TotalCost) / (TotalRev+TotalBWRev_OFFER-TotalBRRev_SEC) * 100;
        //AB4=IF(OR($AA$1="N",$AW4="N"),"",(DH4-$CW4)/DH4)
        CalcEBIT = (TotalRevCalculated-TotalCost)/TotalRevCalculated * 100;
        StdRental = (TotalRev- TotalConnCharge) / bTnetTermVal;
        
        OfferPrice = (calcRevPRI==0 && calcRevSEC==0)? bTnetWinPrice : (calcRevPRI+calcRevSEC);
            
        siteRec.BTnet_Standard_EBIT__c = StdEBIT.SetScale(2, System.RoundingMode.HALF_UP);
        siteRec.BTnet_Calculated_EBIT__c = CalcEBIT.SetScale(2, System.RoundingMode.HALF_UP);
        siteRec.BTnet_Actual_Commission__c=0;
        siterec.BTnet_Commission_Multiplier__c=0;
        siteRec.BTnet_Original_Commision__c = 0;
        siteRec.BTnet_Recurring_Price__c = StdRental;
        siteRec.BTnet_Offer_Price__c = OfferPrice;
        siteRec.BTnet_BR_Bid_Code_Primary__c = btnetBR_Code;
        siteRec.BTnet_BW_Bid_Code_Primary__c = btnetBW_Code;
        //siteRec.BTnet_BR_Bid_Code_Secondary__c = OfferPrice;
        //siteRec.BTnet_BW_Bid_Code_Secondary__c = OfferPrice;
    }
    public Static Decimal PriceFind(integer bandWidthVal, String bearer, Decimal stdBR, Decimal stdDist, Decimal stdBW, Decimal matchPrice, String type1)
    {        
        Decimal BWM = stdBW;
		Decimal BRM = stdBR;		
        Decimal VAR1 = 1000000;
        Decimal var = 0;
     	system.debug('mpk bandWidthVal = '+bandWidthVal+'bearer = '+bearer+'stdBR = '+stdBR+'stdDist = '+stdDist+'stdBW = '+stdBW+'matchPrice = '+matchPrice+'type1 = '+type1);
        
        matchPrice = matchPrice - stdDist;
        
        if(stdBW + stdBR + stdDist != matchPrice) {
            List<QuoteBuilder_BTnet_Discount__c> bRPriceCodes = [Select id, Bearer_Price__c, BR_Code__c from QuoteBuilder_BTnet_Discount__c 
                                                                  where (Bearer_Speed__c =:bearer OR Bearer_Speed__c='All') AND Bandwidth_Price_Mbps__c =null];
            List<QuoteBuilder_BTnet_Discount__c> bWPriceCodes = [Select id, Bandwidth_Price_Mbps__c, BW_Code__c from QuoteBuilder_BTnet_Discount__c 
                                                                  where (Bearer_Speed__c =:bearer OR Bearer_Speed__c='All') AND Bearer_Price__c =null];
            for (QuoteBuilder_BTnet_Discount__c br :bRPriceCodes){
                for(QuoteBuilder_BTnet_Discount__c bw :bWPriceCodes){
                    //bw.Bandwidth_Price_Mbps__c *bandWidthVal;
                    var = (bw.Bandwidth_Price_Mbps__c*bandWidthVal) + br.Bearer_Price__c - matchPrice;           
                    if(stdBW + stdBR + stdDist < matchPrice)  var = var * -1;
                    
                    if(var < VAR1 && var >= 0){
                        BWM = bw.Bandwidth_Price_Mbps__c*bandWidthVal;
                        BRM = br.Bearer_Price__c;
                        VAR1 = var;
                        btnetBW_Code = bw.BW_Code__c;
                        btnetBR_Code = br.BR_Code__c;
                    }
                    if(VAR1 == 0) break;
                }
            }
        }

        if(BWM == stdBW) btnetBW_Code = '';
        if(BRM == stdBR) btnetBR_Code = '';
        
        Decimal TOT = BWM + BRM + stdDist;
        
        system.debug('mpk BWM='+BWM+' BRM='+BRM+'stdDist='+stdDist);
        system.debug('mpk btnetBW_Code='+btnetBW_Code+' btnetBR_Code='+btnetBR_Code);
        
        If(type1 == 'BW') return BWM;        
        If(type1 == 'BR') return BRM;
		        
        return TOT;
       
    }

	
	// BTnet new code - mpk//

	//Std EBIT 
	Integer TotalRev_SEC,TotalBRRev_SEC,TotalBWRev_SEC,FixedSecBRPrice;
	Integer PrimaryTotalRev,TotalBRRev_PRIM,TotalBWRev_PRIM;
	integer Distance_PRIM,BearerPriceDistance_PRIM, PricebyKMorPair_PRIM,Bearer_PRIM;
	Integer StdEBIT = 0;
	Integer TotalRev = 0;
	Integer TotalCost=0;
	integer Distance_SEC,BearerPriceDistanceSEC, PricebyKMorPairSEC;

	//			fetch from object;
	//PricebyKMorPairSEC,FixedSecBRPrice,BearerPriceDistanceSEC,WES_OFFER,LA_OFFER,WES_SEC,LA_SEC,WES_PRIM,LA_PRIM,FixedBRPrice,FixedCost_PRIM,OneOffDistCost_PRIM,OneOffDistCost_SEC
	//DistorPrCost_PRIM,DistorPrCost_SEC,VariableMultiplierBR_PRIM,VariableMultiplierBR_SEC,FixedCostOverheadBW_PRIM,FixedBWCost_PRIM,FixedBWCost_SEC,FixedCost_SEC

	//		user inputs
	//Term,Bandwidth_SEC,Bandwidth_PRIM, Bearer_PRIM,Bearer_SEC, BDUK_Contract,Distance_PRIM,Distance_SEC	
	FixedBRPrice += (BDUK_Contract=='YES')?3000:0;


	TotalBRRev_SEC = FixedSecBRPrice + ((Distance_SEC>0)?BearerPriceDistanceSEC:0) +(Distance_SEC*PricebyKMorPairSEC); //=(CA4+IF(P4>0,CB4,0)+(P4*CC4))
	//TotalBWRev_SEC=CO4= IF(OR(E4="BACKUP",E4="FAILOVER"),0,+(LEFT(H4,1)*IF(O4="100MO",IF(P4>0,CT4,CS4),IF(P4>0,CL4,CK4))*N4))
	if(Variant__C == 'BACKUP' OR Variant__C == 'FAILOVER')
		TotalBWRev_SEC = 0;
	else{
		TotalBWRev_SEC += Term * (Bearer_SEC='100MO')? (Distance_SEC>0? WES_OFFER:LA_OFFER):((Distance_SEC>0?WES_SEC:LA_SEC) * Bandwidth_SEC) ;
	}
	
	//TotalRev_SEC=CP4 =CN4+CO4
	TotalRev_SEC= TotalBRRev_SEC + TotalBWRev_SEC;		

	//TotalBWRev_OFFER=CU4=IF(OR(E4="BACKUP",E4="FAILOVER"),0,+(LEFT(H4,1)*IF(O4="100MO",IF(P4>0,CL4,CK4),IF(P4>0,CL4,CK4))*N4))
	if(Variant__C == 'BACKUP' OR Variant__C == 'FAILOVER')
		TotalBWRev_OFFER = 0;
	else{
		TotalBWRev_OFFER = Term * ((Bearer_SEC='100MO')? (Distance_SEC>0? WES_SEC:LA_SEC):((Distance_SEC>0?WES_SEC:LA_SEC) * Bandwidth_SEC));
	}

	//TotalBRRev_PRIM=BO4 =(BB4+IF(L4>0,BC4,0)+(L4*BD4))
	TotalBRRev_PRIM = FixedBRPrice + (Distance_PRIM>0 ? BearerPriceDistance_PRIM : 0) + (Distance_PRIM*PricebyKMorPair_PRIM);	
	//TotalBWRev_PRIM=BP4 =+(LEFT(H4,1)*IF(AND(H4<>"1 Yr",K4="100MO"),IF(L4>0,BU4,BT4)*J4,IF(L4>0,BM4,BL4)*J4))
	TotalBWRev_PRIM = Term * (Term == '1 year' && Bearer_PRIM =='100MO')? ((Distance_PRIM>0? WES_OFFER:LA_OFFER)*Bandwidth_PRIM):((Distance_PRIM>0?WES_PRIM:LA_PRIM)*Bandwidth_PRIM) ;
	//TotalRev_PRIM=bq4=BO4+BP4
	TotalRev_PRIM = TotalBRRev_PRIM + TotalBWRev_PRIM;


	//TotalCost_SEC=CQ4 =CD4+IF(P4>0,CE4,0)+(CF4*P4)+(CG4*CN4)+(CH4*CU4)+CI4+(N4*CJ4)
	TotalCost_SEC +=FixedCost_SEC+(Distance_SEC > 0 ? OneOffDistCost_SEC:0);
	TotalCost_SEC +=(DistorPrCost_SEC*Distance_SEC) + (VariableMultiplierBR_SEC * TotalBRRev_SEC);
	TotalCost_SEC += (VariableMultiplierBW_SEC *TotalBWRev_OFFER;
	TotalCost_SEC += FixedCostOverheadBW_SEC + (Bandwidth_SEC * FixedBWCost_SEC)
		 
	//TotalCost_PRIM=BR4 =BE4+IF(L4>0,BF4,0)+(BG4*L4)+(BH4*BO4)+(BI4*(LEFT(H4,1)*IF(L4>0,BM4,BL4)*J4))+BJ4+(J4*BK4)
	TotalCost_PRIM = FixedCost_PRIM+(Distance_PRIM > 0 ? OneOffDistCost_PRIM:0);
	TotalCost_PRIM += (DistorPrCost_PRIM*Distance_PRIM) + (VariableMultiplierBR_PRIM * TotalBRRev_PRIM);
	TotalCost_PRIM += (VariableMultiplierBW_PRIM *(Term * (Distance_PRIM > 0 ? WES_PRIM:LA_PRIM)*Bandwidth_PRIM);
	TotalCost_PRIM += FixedCostOverheadBW_PRIM + (Bandwidth_PRIM * FixedBWCost_PRIM)

	//TotalCost=CW4= =IF(E4="STANDARD",0,CQ4)+BR4
	if(Variant__C!="STANDARD") 
		TotalCost = TotalCost_SEC;
	else
		TotalCost = TotalCost_PRIM;

	//TotalRev=CV4 =IF(E4="STANDARD",0,CP4)+BO4+(LEFT(H4,1)*IF(L4>0,BM4,BL4)*J4)
	if(Variant__C!="STANDARD") 
		TotalRev = TotalRev_SEC;
	else
		TotalRev = TotalRev_PRIM; // TotalBRRev_PRIM + (Term * (Distance_PRIM>0 ? WES_PRIM:LA_PRIM) * Bandwidth_PRIM) ;	

	//StdEBIT = AA4 = IF(OR($AA$1="N",$AW4="N"),"",(CV4+CU4-CO4-$CW4)/(CV4+CU4-CO4))
	StdEBIT	= (TotalRev+TotalBWRev_OFFER-TotalBRRev_SEC-TotalCost) / TotalRev+TotalBWRev_OFFER-TotalBRRev_SEC);		
	
	// BTnet new code END - mpk//

	*/
	   
}