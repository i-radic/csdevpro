@isTest(seeAllData=true)
private class Test_DisableDeleteLinkOnOpportunity {        
static testMethod void myUnitTest(){
//Get recordtype 'Leads_from_Service' 
   
Set<String> rtset=new Set<String>();
rtset.add('Leads_from_Service' );
rtset.add('Standard');
       //List<RecordType> rt = [select Id,name from RecordType where DeveloperName ='Leads_from_Service' and SobjectType = 'Opportunity'];
       
List<RecordType> rt = [select Id,name,DeveloperName from RecordType where DeveloperName in :rtset and SobjectType = 'Opportunity'];
Id lfsid, stdid;
for(RecordType r:rt){
if(r.DeveloperName=='Leads_from_Service')
lfsid=r.id;
else if(r.DeveloperName=='Standard')
stdid=r.id;
}
     User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId='00e20000001MX7z',
                           timezonesidkey = 'Europe/London', username = 'B2B3Profile13167cr@testemail.com',
                           EIN__c = '012365'
                           );
        insert u1;

        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 3';
        A.sac_code__c = 'testSAC1';
        A.Sector_code__c = 'CORP1';
        A.LOB_Code__c = 'LOB1';
        A.OwnerId = u1.Id;
        A.CUG__c='CugTest1';
        Database.SaveResult accountResult = Database.insert(A);
             
   //Create LFS Oppty      
        Opportunity Oppy1= new Opportunity();
        Oppy1.Name = 'tst_oppty';
        //Oppy1.NIBR_Year_2009_10__c = 1;
        //Oppy1.NIBR_Year_2010_11__c = 1;
        //Oppy1.NIBR_Year_2011_12__c = 1;
        Oppy1.StageName = 'Created';
        Oppy1.Sales_Stage_Detail__c = 'Appointment Made';
        Oppy1.AccountId = A.id;
        Oppy1.CloseDate = system.today();
       //Oppy1.RecordTypeId = rt[0].id;
        Oppy1.RecordTypeId = lfsid;       
       Oppy1.postcode__c = 'WR5 3RL';
       Oppy1.Customer_Contact_Number__c = '000111222333';
       oppy1.Email_Address__c = 'test.Alan@bt.com';
       oppy1.Customer_Type__c = 'BT Business';
       oppy1.OwnerId=UserInfo.getUserId();
       oppy1.Product__c = 'ISDN';
       oppy1.Sub_Category__c='New Order';
       oppy1.Stage_Reason__c='test reason';
       oppy1.Description='aaaaaaaaaaa';
       
//Create Std Oppty      
        Opportunity Oppy2= new Opportunity();
        Oppy2.Name = 'tst_std_oppty';
        //Oppy2.NIBR_Year_2009_10__c = 1;
        //Oppy2.NIBR_Year_2010_11__c = 1;
        //Oppy2.NIBR_Year_2011_12__c = 1;
        Oppy2.StageName = 'Assigned';
        //Oppy2.Sales_Stage_Detail__c = 'Appointment Made';
        Oppy2.CloseDate = system.today();
       //Oppy2.RecordTypeId = rt1[0].id;
        Oppy2.RecordTypeId = stdid;       
       Oppy2.postcode__c = 'WR5 3RL';
       Oppy2.Customer_Contact_Number__c = '000111222333';
       oppy2.Email_Address__c = 'test.Alan@bt.com';
       oppy2.Customer_Type__c = 'Test';
       oppy2.OwnerId=UserInfo.getUserId();
       oppy2.Product__c = 'ISDN';
       //oppy2.Sub_Category__c='New Order';
       //oppy2.Stage_Reason__c='test reason';
       oppy2.Description='aaaaaaaaaaa';
       //oppy2.Mobile_product__c='Test';
       oppy2.Switch_Size__c='Test';
       oppy2.Sub_Category__c='';       
       Oppy2.Sales_Stage_Detail__c = '';
       oppy2.Stage_Reason__c='';

      insert oppy1; 
      
      
      DisableDeleteLinkOnOpportunity obj=new DisableDeleteLinkOnOpportunity();
       ApexPages.StandardController stc = new ApexPages.StandardController(oppy1);
       DisableDeleteLinkOnOpportunity obj1=new DisableDeleteLinkOnOpportunity(stc);
       PageReference pg=obj1.disableOpportunityDelete();
}
}