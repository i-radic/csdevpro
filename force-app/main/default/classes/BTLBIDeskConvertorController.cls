/*

Date                 Author                  Comment
 * 27/05/2015        Sridevi Bayyapuneedi    Made changes as per CR7582

*/
public with sharing class BTLBIDeskConvertorController {
    
    public List<String> newUrls{ get; set; }
    public String URLtoConvert { get; set; }
    //public String ConvertURL { get; set; }
   
    String newURL;
    String newURLPilot;
    
    public PageReference ConvertURL() {
        newUrls = getConvertURL();
        return null;
    }
 
    public List<String> getConvertURL() {
    String WorkingURL = URLtoConvert.trim();
    system.debug('$$$ '+WorkingURL ); 
    String URLvar = '';
    String URLpage = '';
    String URLsub = '';
    String URLroot = '';
    String URLport = '';
    
    //remove commands if exist
    
    Integer URLLength = WorkingURL.length();
    system.debug('@@@lll '+URLLength );
    Integer URLStart = 0;
    
    if(URLLength != 0){
    
    If (WorkingURL.substring(0,7) == 'http://') {
        URLStart = 7;
    }
    If (WorkingURL.substring(0,8) == 'https://') {
        URLStart = 8;
    } 
    system.debug('#### '+WorkingURL.substring(URLLength-1));   
    /*If (WorkingURL.substring(URLLength-1) == '/') {
        URLLength = URLLength-1;
    }
    Else {
        URLLength = URLLength;
    } */   
    
    WorkingURL = WorkingURL.substring(URLStart,URLLength );
    
    //set new length
    URLLength = WorkingURL.length();
   
    //pick out the variable (after any ?)
    If(WorkingURL.indexOf('?',0) > 0) { 
        URLvar = WorkingURL.substring(WorkingURL.indexOf('?',0),URLLength );
    }
    Else {
        URLvar = '';
    }

    //determine if there is a port
    If(WorkingURL.IndexOf(':') > 0) { 
       // URLport = WorkingURL.substring(WorkingURL.IndexOf(':')+1,URLLength);
          URLport = WorkingURL.substring(WorkingURL.IndexOf(':')+1,WorkingURL.indexOf('/'));
    }
    Else {
        URLport = '';
    }    
           
    //determine page , is anything after last /
    If(WorkingURL.lastIndexOf('/') > 0) { 
    
        If(WorkingURL.indexOf('?',0)>0){
        WorkingURL= WorkingURL.substring(0,WorkingURL.indexOf('?',0));
        }
        URLpage = WorkingURL.substring(WorkingURL.lastIndexOf('/')+1,URLLength-URLVar.length());
    }
    Else {
        URLpage= '';
    }   
    
    //determine subdirectories
    If(WorkingURL.IndexOf('/', 1) > 0) { 
        URLsub = WorkingURL.substring(WorkingURL.IndexOf('/')+1,WorkingURL.lastIndexOf('/')+1);
    }
    Else {
        URLsub = '';
    }  


            
    //determine root
    If(WorkingURL.IndexOf('/', 1) > 0) { 
        URLroot = WorkingURL.substring(0,WorkingURL.IndexOf('/'));
    }
    Else {
        URLroot = WorkingURL;
    }   
    If(WorkingURL.IndexOf(':', 0) > 0) { 
        URLroot = WorkingURL.substring(0,WorkingURL.IndexOf(':'));
    }
       

              
    
    //WorkingURL = ;
    If(URLport.length() > 0) {
    URLport = ',Port=' + URLport;
    }
        newURL = 'https://mia.bt.com/' + URLsub + ',DanaInfo=' + URLroot + URLport + '+' + URLpage + URLvar ;             
        newURLPilot = 'https://idesk.bt.com/' + URLsub + ',DanaInfo=' + URLroot + URLport + '+' + URLpage + URLvar ;
        //newURL = newURL + WorkingURL;
        //newURL = 'URLvar :' + URLvar + ' URLpage  :' + URLpage  + ' URLsub  :' + URLsub + ' URLroot :' + URLroot + ' URLport :' + URLport ;
        
        List<String> newURLs = new List<String>();
        newURLs.add(newURL);
        newURLs.add(newURLPilot);
        return newURLs;
    
    }
  
    return null;
    }
}