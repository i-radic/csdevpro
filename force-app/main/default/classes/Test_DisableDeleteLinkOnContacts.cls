@isTest

private class Test_DisableDeleteLinkOnContacts{
    testmethod static void Test_DisableDeleteLinkOnContacts(){
        DisableDeleteLinkOnAccountOppty d = null;
        d = new DisableDeleteLinkOnAccountOppty();
        d = new DisableDeleteLinkOnAccountOppty(null);
        d.disableDelete();
        
        DisableDeleteLinkOnContacts d1 = null;
        d1 = new DisableDeleteLinkOnContacts();
        d1 = new DisableDeleteLinkOnContacts(null);
        d1.disableDelete();
    }
}