/*
Client      : BT
Author      : Krupakar J
Date        : 17/05/2012 (dd/mm/yyyy)
Description : This class will handle trigger logic for LEM Review objects.
*/

public class LEMReviewBeforeHandler {
	
	// Boolean Variable To Handle Recursion For LEM Review Before Insert
    public static Boolean LEMREVIEW_BEFORE_INSERT_PROCESSING_COMPLETE  = false;
    
    // Boolean Variable To Handle Recursion For LEM Review Before Update
    public static Boolean LEMREVIEW_BEFORE_UPDATE_PROCESSING_COMPLETE  = false;
    
    // Boolean Variable To Handle Recursion LEM Review Before Delete
    public static Boolean LEMREVIEW_BEFORE_DELETE_PROCESSING_COMPLETE  = false;
	
	// Boolean Variable To Handle Recursion LEM Review Period Before Insert
    public static Boolean LEMREVIEWPERIOD_BEFORE_INSERT_PROCESSING_COMPLETE  = false;
	
	private Set<String> LEMIds = null;
	
	private Set<String> BTLBIds = null;
	
	private Set<String> BTLBPSMNames = null;
	
	private Map<String,String> mLEMReviewBTLBIDs = null;
	
	private Map<String,String> mBTLBPSMNames = null;
	
	private Map<String,String> mBTLBRDNames = null;
	
	// Variables Used In LEM Review Period Before Trigger	
	private Set<String> LEMPREV = null;
	
	private Map<String,LEM_Review_Period__c> mLEMreviewPeriods = null;
	
	// Handler Code LEM Review Before (Insert)
    public void handleLEMReviewBeforeInserts(List<LEM_Review__c> newLEMReviews)
    {
        // Prevent Recursion
        if (LEMREVIEW_BEFORE_INSERT_PROCESSING_COMPLETE)
        {
            return;
        }
        else
        {
            LEMREVIEW_BEFORE_INSERT_PROCESSING_COMPLETE = true;
        }
        
        populateRDandPSMName(newLEMReviews);
    }
    
    // Handler Code LEM Review Before (Update)
    public void handleLEMReviewBeforeUpdates(List<LEM_Review__c> newLEMReviews, List<LEM_Review__c> oldLEMReviews)
    {
        // Prevent Recursion
        if (LEMREVIEW_BEFORE_UPDATE_PROCESSING_COMPLETE)
        {
            return;
        }
        else
        {
            LEMREVIEW_BEFORE_UPDATE_PROCESSING_COMPLETE = true;
        }
        
        populateRDandPSMName(newLEMReviews);
    }
    
    // Method to populate RD and PSM names on LEM Review
    public void populateRDandPSMName(List<LEM_Review__c> LEMReviews) {
    	// Initialize all SETs and MAPs for Usage.
    	LEMIds = new Set<String>();
    	
    	BTLBIds = new Set<String>();
    	
    	BTLBPSMNames = new Set<String>();
    	
    	mLEMReviewBTLBIDs = new Map<String,String>();
    	
    	mBTLBPSMNames = new Map<String,String>();
    	
    	mBTLBRDNames = new Map<String,String>();
    	
    	//Loop Through To Get LEM Review ID's
    	for(LEM_Review__c lr : LEMReviews) {
    		LEMIds.add(lr.Id);
    	}
    	
    	//Loop Through and Query To Get LEM Referenced BTLB ID's
    	for(LEM_Review__c lr : [SELECT l.Name, l.Id, l.BTLB_Master_Object__c FROM LEM_Review__c l WHERE l.Id IN : LEMIds]) {
    		BTLBIds.add(lr.BTLB_Master_Object__c);
    		mLEMReviewBTLBIDs.put(lr.Id, lr.BTLB_Master_Object__c);
    	}
    	
    	//Loop Through and Query To Get All Account Team Contacts BTLB PSM ID's
    	for(Account_Team_Contact_BTLB__c atc : [SELECT a.Specialist_Role__c, a.Specialist_Name__c, a.Name, a.Id, a.BTLB__c FROM Account_Team_Contact_BTLB__c a WHERE a.BTLB__c IN : BTLBIds AND a.Specialist_Role__c = 'PSM']) {
    		BTLBPSMNames.add(atc.Specialist_Name__c);
    		mBTLBPSMNames.put(atc.BTLB__c, atc.Specialist_Name__c);
    	}
    	
    	//Loop Through and Query To Get All Account Team Contacts BTLB RD Names
    	for(User u : [SELECT u.Name, u.Manager_Name__c, u.ManagerId, u.Id FROM User u WHERE u.Name IN : BTLBPSMNames]) {
    		mBTLBRDNames.put(u.Name, u.Manager_Name__c);
    	}
    	
    	//Loop Through and Update PSM and RD Names
    	for(LEM_Review__c lr : LEMReviews) {
    		lr.PSM_Name__c = mBTLBPSMNames.containsKey(mLEMReviewBTLBIDs.get(lr.Id)) ? mBTLBPSMNames.get(mLEMReviewBTLBIDs.get(lr.Id)) : '';
    		lr.RD_Name__c = mBTLBRDNames.containsKey(mBTLBPSMNames.get(mLEMReviewBTLBIDs.get(lr.Id))) ?  mBTLBRDNames.get(mBTLBPSMNames.get(mLEMReviewBTLBIDs.get(lr.Id))) : '';
    	}
    }
    
    // Handler Code LEM Review Period Before (Insert)
    public void handleLEMReviewPeriodBeforeInserts(List<LEM_Review_Period__c> newLEMReviewPeriods)
    {
        // Prevent Recursion
        if (LEMREVIEWPERIOD_BEFORE_INSERT_PROCESSING_COMPLETE)
        {
            return;
        }
        else
        {
            LEMREVIEWPERIOD_BEFORE_INSERT_PROCESSING_COMPLETE = true;
        }
        populatePreviousForecast(newLEMReviewPeriods);
    }
	
	 // Method to populate Previous Forecast Values for 30,60 and 90 Day Reviews. 
    public void populatePreviousForecast(List<LEM_Review_Period__c> newLEMReviewPeriods) {
    	LEMPREV = new Set<String>();
    	mLEMreviewPeriods = new Map<String,LEM_Review_Period__c>();
    	for(LEM_Review_Period__c lrp : newLEMReviewPeriods) {
    		if(lrp.Period__c == '60 Day') {
    			LEMPREV.add(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-30 Day');
    		}
    		else if(lrp.Period__c == '90 Day') {
    			LEMPREV.add(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-60 Day');
    		}
    		else if(lrp.Period__c == '30 Day' && (lrp.Quarter__c == 'Quarter 2' || lrp.Quarter__c == 'Quarter 3' || lrp.Quarter__c == 'Quarter 4')) {
    			String Quarter = '';
    			if(lrp.Quarter__c == 'Quarter 2') Quarter = 'Quarter 1';
    			else if(lrp.Quarter__c == 'Quarter 3') Quarter = 'Quarter 2';
    			else if(lrp.Quarter__c == 'Quarter 4') Quarter = 'Quarter 3';
    			LEMPREV.add(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + Quarter + '-90 Day');
    		}
    	}
    	system.debug('Value of LEMPREV :' + LEMPREV);
    	for(LEM_Review_Period__c lrp :[SELECT l.zCheck__c, l.iNET_Next_30_Day_Forecast__c, l.WLR_Lines_Next_30_Day_Forecast__c, l.CalWlr_Next_30_Day_Forecast__c, l.CL_Next_30_Day_Forecast__c, l.EduIT_Next_30_Day_Forecast__c, l.OP_Next_30_Day_Forecast__c, l.Switch_Volume_Next_30_Day_Forecast__c, l.Switch_Value_Next_30_Day_Forecast__c, l.Quarter__c, l.Period__c, l.Mobile_Next_30_Day_Forecast__c, l.LEM_Review__c, l.Id, l.Data_Networks_Next_30_Day_Forecast__c, l.Calls_Next_30_Day_Forecast__c, l.Broadband_Next_30_Day_Forecast__c FROM LEM_Review_Period__c l WHERE l.zCheck__c IN : LEMPREV]) {
    		mLEMreviewPeriods.put(lrp.zCheck__c, lrp);
    		system.debug('Value of LRP :' + lrp);
    	}
    	system.debug('Value of mLEMreviewPeriods :' + mLEMreviewPeriods);
    	for(LEM_Review_Period__c lrp : newLEMReviewPeriods) {
    		if(lrp.Period__c == '30 Day' && lrp.Quarter__c == 'Quarter 1') {
    			lrp.Broadband_Previous_30_Day_Forecast__c = 0;
    			lrp.Calls_Previous_30_Day_Forecast__c = 0;
                lrp.CalWlr_Previous_30_Day_Forecast__c =0;
                lrp.CL_Previous_30_Day_Forecast__c =0;
                lrp.EduIT_Previous_30_Day_Forecast__c =0;
                lrp.OP_Previous_30_Day_Forecast__c =0;
    			lrp.Data_Networks_Previous_30_Day_Forecast__c = 0;
    			lrp.iNET_Previous_30_Day_Forecast__c = 0;
    			lrp.Mobile_Previous_30_Day_Forecast__c = 0;
    			lrp.Switch_Value_Previous_30_Day_Forecast__c = 0;
    			lrp.Switch_Volume_Previous_30_Day_Forecast__c = 0;
    			lrp.WLR_Lines_Previous_30_Day_Forecast__c = 0;
    			system.debug('Value of LRP 30:' + lrp);
    		}
    		if(lrp.Period__c == '30 Day' && (lrp.Quarter__c == 'Quarter 2' || lrp.Quarter__c == 'Quarter 3' || lrp.Quarter__c == 'Quarter 4')) {
    			String Quarter = '';
    			if(lrp.Quarter__c == 'Quarter 2') Quarter = 'Quarter 1';
    			else if(lrp.Quarter__c == 'Quarter 3') Quarter = 'Quarter 2';
    			else if(lrp.Quarter__c == 'Quarter 4') Quarter = 'Quarter 3';
    			system.debug('Value of LRP 30 Q2,Q3,Q4:' + lrp);
    			LEM_Review_Period__c LEM90 = mLEMreviewPeriods.containsKey(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + Quarter + '-90 Day') ? mLEMreviewPeriods.get(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + Quarter + '-90 Day') : null;
    			system.debug('Value of LEM90 :' + LEM90);
    			if(LEM90 != null) {
	    			lrp.Broadband_Previous_30_Day_Forecast__c = LEM90.Broadband_Next_30_Day_Forecast__c;
	    			lrp.Calls_Previous_30_Day_Forecast__c = LEM90.Calls_Next_30_Day_Forecast__c;
                    lrp.CalWlr_Previous_30_Day_Forecast__c = LEM90.CalWlr_Next_30_Day_Forecast__c;
                	lrp.CL_Previous_30_Day_Forecast__c = LEM90.CL_Next_30_Day_Forecast__c;
                	lrp.EduIT_Previous_30_Day_Forecast__c = LEM90.EduIT_Next_30_Day_Forecast__c;
                	lrp.OP_Previous_30_Day_Forecast__c = LEM90.OP_Next_30_Day_Forecast__c;
	    			lrp.Data_Networks_Previous_30_Day_Forecast__c = LEM90.Data_Networks_Next_30_Day_Forecast__c;
	    			lrp.iNET_Previous_30_Day_Forecast__c = LEM90.iNET_Next_30_Day_Forecast__c;
	    			lrp.Mobile_Previous_30_Day_Forecast__c = LEM90.Mobile_Next_30_Day_Forecast__c;
	    			lrp.Switch_Value_Previous_30_Day_Forecast__c = LEM90.Switch_Value_Next_30_Day_Forecast__c;
	    			lrp.Switch_Volume_Previous_30_Day_Forecast__c = LEM90.Switch_Volume_Next_30_Day_Forecast__c;
	    			lrp.WLR_Lines_Previous_30_Day_Forecast__c = LEM90.WLR_Lines_Next_30_Day_Forecast__c;
	    			system.debug('Value of LRP 30 Q2,Q3,Q4::' + lrp);
	    			system.debug('Value of LRP M30 Q2,Q3,Q4::' + mLEMreviewPeriods.get(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-30 Day'));
    			}
    		}
    		else if(lrp.Period__c == '60 Day') {
    			LEM_Review_Period__c LEM30 = mLEMreviewPeriods.containsKey(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-30 Day') ? mLEMreviewPeriods.get(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-30 Day') : null;
    			if(LEM30 != null) {
	    			lrp.Broadband_Previous_30_Day_Forecast__c = LEM30.Broadband_Next_30_Day_Forecast__c;
	    			lrp.Calls_Previous_30_Day_Forecast__c = LEM30.Calls_Next_30_Day_Forecast__c;
                    lrp.CalWlr_Previous_30_Day_Forecast__c = LEM30.CalWlr_Next_30_Day_Forecast__c;
                	lrp.CL_Previous_30_Day_Forecast__c = LEM30.CL_Next_30_Day_Forecast__c;
                	lrp.EduIT_Previous_30_Day_Forecast__c = LEM30.EduIT_Next_30_Day_Forecast__c;
                	lrp.OP_Previous_30_Day_Forecast__c = LEM30.OP_Next_30_Day_Forecast__c;
	    			lrp.Data_Networks_Previous_30_Day_Forecast__c = LEM30.Data_Networks_Next_30_Day_Forecast__c;
	    			lrp.iNET_Previous_30_Day_Forecast__c = LEM30.iNET_Next_30_Day_Forecast__c;
	    			lrp.Mobile_Previous_30_Day_Forecast__c = LEM30.Mobile_Next_30_Day_Forecast__c;
	    			lrp.Switch_Value_Previous_30_Day_Forecast__c = LEM30.Switch_Value_Next_30_Day_Forecast__c;
	    			lrp.Switch_Volume_Previous_30_Day_Forecast__c = LEM30.Switch_Volume_Next_30_Day_Forecast__c;
	    			lrp.WLR_Lines_Previous_30_Day_Forecast__c = LEM30.WLR_Lines_Next_30_Day_Forecast__c;
	    			system.debug('Value of LRP 60:' + lrp);
	    			system.debug('Value of LRP M60:' + mLEMreviewPeriods.get(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-30 Day'));
    			}
    		}
    		else {
    			LEM_Review_Period__c LEM60 = mLEMreviewPeriods.containsKey(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-60 Day') ? mLEMreviewPeriods.get(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-60 Day') : null;
    			if(LEM60 != null) {
	    			lrp.Broadband_Previous_30_Day_Forecast__c = LEM60.Broadband_Next_30_Day_Forecast__c;
	    			lrp.Calls_Previous_30_Day_Forecast__c = LEM60.Calls_Next_30_Day_Forecast__c;
                    lrp.CalWlr_Previous_30_Day_Forecast__c = LEM60.CalWlr_Next_30_Day_Forecast__c;
                	lrp.CL_Previous_30_Day_Forecast__c = LEM60.CL_Next_30_Day_Forecast__c;
                	lrp.EduIT_Previous_30_Day_Forecast__c = LEM60.EduIT_Next_30_Day_Forecast__c;
                	lrp.OP_Previous_30_Day_Forecast__c = LEM60.OP_Next_30_Day_Forecast__c;
	    			lrp.Data_Networks_Previous_30_Day_Forecast__c = LEM60.Data_Networks_Next_30_Day_Forecast__c;
	    			lrp.iNET_Previous_30_Day_Forecast__c = LEM60.iNET_Next_30_Day_Forecast__c;
	    			lrp.Mobile_Previous_30_Day_Forecast__c = LEM60.Mobile_Next_30_Day_Forecast__c;
	    			lrp.Switch_Value_Previous_30_Day_Forecast__c = LEM60.Switch_Value_Next_30_Day_Forecast__c;
	    			lrp.Switch_Volume_Previous_30_Day_Forecast__c = LEM60.Switch_Volume_Next_30_Day_Forecast__c;
	    			lrp.WLR_Lines_Previous_30_Day_Forecast__c = LEM60.WLR_Lines_Next_30_Day_Forecast__c;
	    			system.debug('Value of LRP 90:' + lrp);
	    			system.debug('Value of LRP M90:' + mLEMreviewPeriods.get(String.valueOf(lrp.LEM_Review__c).substring(0,15) + ':' + lrp.Quarter__c + '-60 Day'));
    			}
    		}
    	}
    	
    }
}