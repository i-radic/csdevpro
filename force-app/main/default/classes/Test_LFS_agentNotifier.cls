@isTest(seeAllData=true)
private class Test_LFS_agentNotifier{

  static testMethod void myUnitTest() {
  
   //Get recordtype 'Leads_from_Service' 
   
//Set<String> rtset=new Set<String>();
//rtset.add('Leads_from_Service' );
//rtset.add('Standard');
      String StandardId='01220000000PjDu';
      String LFSId='01220000000ABFB';
       //List<RecordType> rt = [select Id,name from RecordType where DeveloperName ='Leads_from_Service' and SobjectType = 'Opportunity'];
       
//List<RecordType> rt = [select Id,name,DeveloperName from RecordType where DeveloperName in :rtset and SobjectType = 'Opportunity'];
//Id lfsid, stdid;
/*for(RecordType r:rt){
if(r.DeveloperName=='Leads_from_Service')
lfsid=r.id;
else if(r.DeveloperName=='Standard')
stdid=r.id;
}*/
   
   User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId='00e20000001MX7z',
                           timezonesidkey = 'Europe/London', username = 'B2B3Profile13167cr@bt.com',
                           EIN__c = '012365'
                           );
        insert u1;

        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 3';
        A.sac_code__c = 'testSAC1';
        A.Sector_code__c = 'CORP1';
        A.LOB_Code__c = 'LOB1';
        A.OwnerId = u1.Id;
        A.CUG__c='CugTest1';
        Database.SaveResult accountResult = Database.insert(A);
        
        
        //Create LFS Oppty      
        Opportunity Oppy1= new Opportunity();
        Oppy1.Name = 'tst_oppty';
        Oppy1.AccountId = A.id;
        //Oppy1.NIBR_Year_2009_10__c = 1;
        //Oppy1.NIBR_Year_2010_11__c = 1;
        //Oppy1.NIBR_Year_2011_12__c = 1;
        Oppy1.StageName = 'Created';
        Oppy1.Sales_Stage_Detail__c = 'Appointment Made';
        Oppy1.CloseDate = system.today();
       //Oppy1.RecordTypeId = rt[0].id;
        Oppy1.RecordTypeId = LFSId;//lfsid;       
       Oppy1.postcode__c = 'WR5 3RL';
       Oppy1.Customer_Contact_Number__c = '000111222333';
       oppy1.Email_Address__c = 'test.Alan@bt.com';
       oppy1.Customer_Type__c = 'BT Business';
       oppy1.OwnerId=UserInfo.getUserId();
       oppy1.Product__c = 'ISDN';
       oppy1.Sub_Category__c='New Order';
       oppy1.Stage_Reason__c='test reason';
       oppy1.Description='aaaaaaaaaaa';
      oppy1.Mobile_product__c = 'OA';
       
//Create Std Oppty      
        Opportunity Oppy2= new Opportunity();
        Oppy2.Name = 'tst_std_oppty';
        Oppy2.AccountId = A.id;
        //Oppy2.NIBR_Year_2009_10__c = 1;
        //Oppy2.NIBR_Year_2010_11__c = 1;
        //Oppy2.NIBR_Year_2011_12__c = 1;
        Oppy2.StageName = 'Cancelled';
        //Oppy2.Sales_Stage_Detail__c = 'Appointment Made';
        Oppy2.CloseDate = system.today();
       //Oppy2.RecordTypeId = rt1[0].id;
        Oppy2.RecordTypeId = StandardId;//stdid;       
       Oppy2.postcode__c = 'WR5 3RL';
       Oppy2.Customer_Contact_Number__c = '000111222333';
       oppy2.Email_Address__c = 'test.Alan@bt.com';
       oppy2.Customer_Type__c = 'Test';
       oppy2.OwnerId=UserInfo.getUserId();
       oppy2.Product__c = 'ISDN';
       //oppy2.Sub_Category__c='New Order';
       //oppy2.Stage_Reason__c='test reason';
       oppy2.Description='aaaaaaaaaaa';
       //oppy2.Mobile_product__c='Test';
       oppy2.Switch_Size__c='Test';
       oppy2.Sub_Category__c='';       
       Oppy2.Sales_Stage_Detail__c = '';
       oppy2.Stage_Reason__c='';

      insert oppy1; 
      insert oppy2;
     
           
    //Create Note on oppty  
      Note oppnote=new Note();
       oppnote.ParentId=oppy1.Id;
       oppnote.Title='Test Note';
       oppnote.IsPrivate=False;
       oppnote.Body='Test Note Body';
       oppnote.OwnerId=UserInfo.getUserId();
       
       insert oppnote;
       
    //Create Order
       Opportunity_Submitted_Order__c order=new Opportunity_Submitted_Order__c();
       order.Opportunity__c=oppy1.Id;
       order.Order_Reference__c='VOL011-112233445566';
       order.SUBMITTED_BY_EIN__C='123456789';
       order.SUBMITTED_COUNT__C=1;
       
       
       insert order;
       
     //Create Product  
        Product2 p2 = new Product2();
        p2.IsActive = true;
        p2.Name = 'Test Product';
        p2.ProductCode = 'testprod';
        
        insert p2;
        
      List<Pricebook2> standardPB = [select id from Pricebook2 where isStandard=true];
           
      //Create Standard Price Book Entry
      PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = '01s20000000HXVd', Product2Id = p2.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
           
      insert standardPrice;
      
      //Create Price Book
           
           PriceBook2 pb=new PriceBook2();
           pb.name='BTLB';
           
           insert pb;
    //Create Price Book Entry
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = p2.id;
        pbe.Pricebook2Id = pb.id;
        pbe.IsActive = true;
        pbe.UnitPrice = 100;
        pbe.USESTANDARDPRICE=false;

        insert pbe;
      
      //Create Oppty Line Item      
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = oppy1.id;
        oli.PricebookEntryId = pbe.id;
        oli.UnitPrice = 4100;
        oli.Quantity = 1;    
        oli.category__c = 'Main';
        oli.contract_term__c='24 Months';
        oli.billing_cycle__c='Monthly';
        oli.service_ready_date__c = system.today().addmonths(1);
        oli.first_bill_date__c = system.today().addmonths(2);
        oli.initial_cost__c = 4000;
        
      test.startTest();
        insert oli;
        //test.stopTest();
        
       
        
        List<Opportunity> opp = [select id,RecordTypeId, Opportunity_Id__c, AccountId, Customer_Contact__c, Customer_Contact_Number__c, Email_Address__c from Opportunity where Id =:oppy1.id];
        
         LFS_agentNotifier getOpties = new LFS_agentNotifier();
         getOpties.oppyId = opp[0].Opportunity_Id__c;
         getOpties.getNotificationText();
         String strBody = getOpties.mailBody;
         
         
        Oppy1.StageName = 'Lost';   
        oppy1.Sub_Category__c ='';
     
        update oppy1;
        
         LFS_agentNotifier getOpties3 = new LFS_agentNotifier();
         getOpties3.oppyId = opp[0].Opportunity_Id__c;
       
         //Test.startTest();
         getOpties3.getNotificationText();
         //Test.stopTest();
    
        // String strBody3 = getOpties3.mailBody;     
   
      
      //Create Oppty Line Item      
        OpportunityLineItem oli1 = new OpportunityLineItem();
        oli1.OpportunityId = oppy2.id;
        oli1.PricebookEntryId = pbe.id;
        oli1.UnitPrice = 4100;
        oli1.Quantity = 1;    
        oli1.category__c = 'Main';
        oli1.contract_term__c='24 Months';
        oli1.billing_cycle__c='Monthly';
        oli1.service_ready_date__c = system.today().addmonths(1);
        oli1.first_bill_date__c = system.today().addmonths(2);
        oli1.initial_cost__c = 4000;
        
      //test.startTest();
        insert oli1;
      test.stopTest();
        
       List<Opportunity> opp2 = [select id,RecordTypeId, Opportunity_Id__c, AccountId, Customer_Contact__c, Customer_Contact_Number__c, Email_Address__c from Opportunity where Id =:oppy2.id];
        
      LFS_agentNotifier getOpties4 = new LFS_agentNotifier();
      getOpties4.oppyId = opp2[0].Opportunity_Id__c;
      //test.starttest();
      getOpties4.getNotificationText();
      Oppy2.Sales_Stage_Detail__c = 'Appointment Made';
      oppy2.Stage_Reason__c = 'test reason';
      update oppy2;
      getOpties4.getNotificationText();
       oppy2.StageName = 'Assigned';
      update oppy2;
      getOpties4.getNotificationText();
      oppy2.StageName = 'Won';
      update oppy2;
      getOpties4.getNotificationText();
      //test.stopTest();
      String strBody4 = getOpties4.mailBody; 
      
      
  }
  }