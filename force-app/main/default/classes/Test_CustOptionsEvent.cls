@isTest
private class Test_CustOptionsEvent {
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        CustomerOptions__c testcallLog = new CustomerOptions__c();
        testcallLog.Account__c = '0012000000kNdyRAAS';
        testcallLog.Telephone_Number__c = '0123456789';
        testcallLog.Account_Number__c = 'ab22222222';
        testcallLog.Call_Originator__c = 'BTLB';
        
        testcallLog.Reason_for_alleged_mis_sale__c = 'Did not order it';
        testcallLog.EIN_of_the_agent_who_placed_the_order__c = 604218245;
        testcallLog.Site__c = 'BTLB';
        testcallLog.Line_Manager_of_above__c = '604218245';

        testcallLog.Order_placed_date__c = DateTime.now();
        testcallLog.Free_Notes__c = 'Test Event';
        

        insert testcallLog;
            
    }
}