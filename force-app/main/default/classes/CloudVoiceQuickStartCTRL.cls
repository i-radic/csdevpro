/*######################################################################################
PURPOSE
Controller for the RC quick start journey in a single page

HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
public without sharing class CloudVoiceQuickStartCTRL{

    public CloudVoiceQuickStartCTRL(ApexPages.StandardController controller) { }
    public CloudVoiceQuickStartCTRL() {}

    public Integer licenses {get; set;}  
    public Integer Qty {get; set;}
    public Integer physicalPhones {get; set;}
    public Decimal delCharge {get; set;}  

    public String runningCost { get;set; }
    public String licenseType {get; set;} 
    public String term {get; set;} 
    public String phone{get; set;} 
    public String product {get; set;}  
    public String speed {get; set;}  
    public String accPrice {get; set;}  
    public String bundleSTD {get; set;}  
    public String newLAN {get; set;}  
    public String newSC {get; set;}  
    public String telNum {get; set;}  
    public String cvSiteId {get; set;}  
    public String u999Contact {get; set;}
    public String u999Phone {get; set;}
    public String u999Email {get; set;}  
    public String locationCode {get; set;}  
    public String selectedProducts {get; set;}
    public String productsToAddCSVRC {get; set;}
    public String multiAddress {get; set;}
    public String multiAddressCSV {get; set;}
    public String rcMultiAddress {get; set;}
    public String rcAccessChoice {get; set;}
    public String rcAccessRef {get; set;}
    public String rcLineRef {get; set;}
    public String rcAddNumber {get; set;}
    public String rcMultiSTD {get; set;}
    public String rcMainDirEntry {get; set;}
    public String rcDirEntry {get; set;}
    public String custCRD {get; set;}
    public String setCRD {get; set;}
    public String rcTestId {get; set;}
    public String rcMOS {get; set;}
    public String rcPort {get; set;}
    public String rcMax {get; set;}
    public String rcCompetitor {get; set;}
    public String rcMinRate {get; set;}
    public String rcMaxRate {get; set;}
    public String rcBAC {get; set;}
    public String rcAccNum {get; set;}
    public String rcSortCode {get; set;}
    public String connID {get; set;}
    public String delID {get; set;}
    public String lineID {get; set;}
    public String countPhones {get; set;}
    public String rcTerm{get; set;}
    public String oppId{get; set;}
    public String emailSent{get; set;}

    public Boolean zeroConnection{get; set;}
    public Boolean rcDD{get; set;}
    public Boolean detailMissing{get; set;}

    public Date crdDate {get; set;}    
    public Date sampleDateField {get; set;}
    
    public List<prodClass> rawResultsList {get; set;}
    public List<String[]> matchIdList {get; set;}
    public List<prodClass> groupList {get; set;}
    public List<String> tableRowList {get; set;}
    
    public List<prodClass> groupList12 {get; set;}
    public List<prodClass> groupListVol12 {get; set;}
    public List<String> tableRowList12 {get; set;}
    public List<String> tableRowListVol12 {get; set;}
    public List<prodClass> groupList24 {get; set;}
    public List<prodClass> groupListVol24 {get; set;}
    public List<String> tableRowList24 {get; set;}
    public List<String> tableRowListVol24 {get; set;}
    public List<prodClass> groupList36 {get; set;}
    public List<prodClass> groupListVol36 {get; set;}
    public List<String> tableRowList36 {get; set;}
    public List<String> tableRowListVol36 {get; set;}
    public List<prodClass> groupList60 {get; set;}
    public List<prodClass> groupListVol60 {get; set;}
    public List<String> tableRowList60 {get; set;}
    public List<String> tableRowListVol60 {get; set;}

    private List<SelectOption> selOptions; 
    
    public string oId = System.currentPageReference().getParameters().get('Id');
    public string obfId = System.currentPageReference().getParameters().get('results');     
    public string selProduct = System.currentPageReference().getParameters().get('selProduct');    
    public string urlCVSiteId = System.currentPageReference().getParameters().get('cvID'); 
    public string conId = System.currentPageReference().getParameters().get('conId'); 

    public PageReference refresh() {
        return null;
    }   
    public String getAccName(){
        String aName = System.currentPageReference().getParameters().get('accName');
        return aName;
    }  
    public List<SelectOption> getLicense() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('', 'Select'));
      selOptions.add(new SelectOption('1', '1'));
      selOptions.add(new SelectOption('2', '2'));
      selOptions.add(new SelectOption('3', '3'));
      selOptions.add(new SelectOption('4', '4'));
      selOptions.add(new SelectOption('5', '5'));
      selOptions.add(new SelectOption('6', '6'));
      selOptions.add(new SelectOption('7', '7'));
      selOptions.add(new SelectOption('8', '8'));
      selOptions.add(new SelectOption('9', '9'));
      selOptions.add(new SelectOption('10', '10'));
      selOptions.add(new SelectOption('11', '11'));
      selOptions.add(new SelectOption('12', '12'));
      selOptions.add(new SelectOption('13', '13'));
      selOptions.add(new SelectOption('14', '14'));
      selOptions.add(new SelectOption('15', '15'));
      selOptions.add(new SelectOption('16', '16'));
      selOptions.add(new SelectOption('17', '17'));
      selOptions.add(new SelectOption('18', '18'));
      selOptions.add(new SelectOption('19', '19'));
      selOptions.add(new SelectOption('20', '20'));
      selOptions.add(new SelectOption('21', '21'));
      selOptions.add(new SelectOption('22', '22'));
      selOptions.add(new SelectOption('23', '23'));
      selOptions.add(new SelectOption('24', '24'));
      selOptions.add(new SelectOption('25', '25'));
      return selOptions;
    } 
    public List<CSS_Products__c> getallCPErc() {
        List<CSS_Products__c> CSSprodList = [Select Id, Product_Name__c, Description__c, ShopLink__c, Unit_Cost__c, Discountable__c,Connection_Charge__c,A_Code__c 
        FROM CSS_Products__c 
        WHERE Active__c = True AND Type__c = 'CPE' AND Grouping__c = 'Phone' AND Provider__c = 'Cloud Phone' AND (Unit__c <> 'Discounted' OR Unit__c = 'Both')  ORDER BY Sort_Order__c ASC LIMIT 200];
        return CSSprodList ;
    }
    public List<CSS_Products__c> getallCPErcALT() {
        List<CSS_Products__c> CSSprodList = [Select Id, Product_Name__c, Description__c, ShopLink__c, Unit_Cost__c, Discountable__c,Connection_Charge__c,A_Code__c 
        FROM CSS_Products__c 
        WHERE Active__c = True AND Type__c = 'CPE' AND Grouping__c = 'Phone' AND Provider__c = 'Cloud Phone' AND (Unit__c = 'Discounted' OR Unit__c = 'Both') ORDER BY Sort_Order__c ASC LIMIT 200];
        return CSSprodList ;
    }
    public List<CSS_Products__c> getallCPErcPics() {
        List<CSS_Products__c> CSSprodList = [Select Id, Product_Name__c, Description__c, ShopLink__c, Unit_Cost__c, Discountable__c,Connection_Charge__c,A_Code__c  FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'CPE' AND ShopLink__c <>'' AND Provider__c = 'Cloud Phone' ORDER BY Sort_Order__c ASC LIMIT 200];
        return CSSprodList ;
    }

    public List<Competitors_Suppliers_Manufacturers__c> getCompetitors() {
        List<Competitors_Suppliers_Manufacturers__c> comps = [Select Id, Name FROM Competitors_Suppliers_Manufacturers__c WHERE Opportunity__c = TRUE ORDER BY Name];
        return comps ;
    }

    public List<SelectOption> getYesNo() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('', 'Select'));
      selOptions.add(new SelectOption('No', 'No'));
      selOptions.add(new SelectOption('Yes', 'Yes'));
      return selOptions;
    } 
    public List<SelectOption> getYesNoDef() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('No', 'No'));
      selOptions.add(new SelectOption('Yes', 'Yes'));
      return selOptions;
    }  
    public List<SelectOption> getYesNoMask() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('No', 'Same as main number'));
      selOptions.add(new SelectOption('Yes', 'Different dialling codes'));
      return selOptions;
    }  
    public List<SelectOption> getLAN() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('', 'Select'));
      selOptions.add(new SelectOption('No', 'No'));
      selOptions.add(new SelectOption('Yes', 'Yes'));
      selOptions.add(new SelectOption('YesG', 'Yes Gigabit'));
      return selOptions;
    }     
    public List<SelectOption> getSC() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('', 'Select'));
      selOptions.add(new SelectOption('No', 'No'));
      selOptions.add(new SelectOption('Yes', 'Yes'));
      return selOptions;
    }     
    public List<SelectOption> getdirListing() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('No Listing', 'No Listing'));
      selOptions.add(new SelectOption('Free Listing', 'Free Listing'));
      selOptions.add(new SelectOption('Paid Listing', 'Paid Listing'));  
      return selOptions;
    }
    public List<SelectOption> getconTermCP() {  
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('','Please Select'));
        selOptions.add(new SelectOption('12 months','12 months'));
        selOptions.add(new SelectOption('24 months','24 months'));
        selOptions.add(new SelectOption('36 months','36 months'));
        selOptions.add(new SelectOption('60 months','60 months'));
        return selOptions;
    }
    public List<Number_Port_Reference__c> getSTDs() {
        List<Number_Port_Reference__c> rlist = [Select Id, Type__c,Reference__c,Description__c,Area__c FROM Number_Port_Reference__c WHERE Type__c = 'STD' ORDER BY Reference__c LIMIT 1000];
        return rlist ;
    }    
    public void createCrossTabData() {
      crossTabData('12','standard');
      crossTabData('12','volume');
      crossTabData('24','standard');
      crossTabData('24','volume');
      crossTabData('36','standard');
      crossTabData('36','volume');
      crossTabData('60','standard');
      crossTabData('60','volume');
    }  

    public void crossTabData(String term, String type) {
        List<AggregateResult> aggRes = new list<AggregateResult>();
    
        if(type == 'standard'){
            aggRes = [Select Unit__c, Grouping__c, GROUPING(Unit__c) gProd, GROUPING(Grouping__c)gGroup, MIN(Unit_Cost__c) minCost, MAX(Unit_Cost__c) maxCost, SUM(Unit_Cost__c) sumCost, COUNT(id) cnt, MIN(id) prodId
            FROM CSS_Products__c 
            WHERE Active__c = True AND Type__c = 'License' AND Term__c = :term+' months' AND Provider__c = 'Cloud Phone' AND Sub_Grouping__c <> 'Volume' 
            GROUP BY CUBE(Unit__c, Grouping__c)
            ORDER BY MIN(Sort_Order__c)];
        }else if(type == 'volume'){
            aggRes = [Select Unit__c, Grouping__c, GROUPING(Unit__c) gProd, GROUPING(Grouping__c)gGroup, MIN(Unit_Cost__c) minCost, MAX(Unit_Cost__c) maxCost, SUM(Unit_Cost__c) sumCost, COUNT(id) cnt, MIN(id) prodId
            FROM CSS_Products__c 
            WHERE Active__c = True AND Type__c = 'License' AND Term__c = :term+' months' AND Provider__c = 'Cloud Phone' AND (Sub_Grouping__c = 'Volume' OR Sub_Grouping__c = 'Both')
            GROUP BY CUBE(Unit__c, Grouping__c)
            ORDER BY MIN(Sort_Order__c)];
        }

        groupList = new list<prodClass>();
        list<prodClass> prodList = new list<prodClass>();
        rawResultsList = new list<prodClass>();
        matchIdList = new list<String[]>();
        tableRowList = new list<String>();
      
        for (AggregateResult ar: aggRes) {
            prodClass objProdClass = new prodClass(ar);  
            if(objProdClass.gProd == 0 && objProdClass.gGroup == 1){
                prodList.add(objProdClass);
            }
            if(objProdClass.gProd == 1 && objProdClass.gGroup == 0){
                groupList.add(objProdClass);
            }    
            if(objProdClass.gProd == 0 && objProdClass.gGroup == 0){
                rawResultsList.add(objProdClass);
            }           
        }
        for (prodClass prod: prodList){
            for (prodClass grp: groupList){
                matchIdList.add(new string[] { grp.grouping, prod.prodName });
            }
        }
        String tableRowHTML = null; 
        Boolean matched = false;
        Integer groupCount = 0;
        for (String[] matchId: matchIdList){        
            for (prodClass prod: rawResultsList){
                string temp = matchId[0] + matchId[1];
                string link = '\'' + prod.prodId +'\'';
                string price = '\'' + prod.minCost +'\'';
                //--------Match
                if(groupCount == groupList.size()){
                    tableRowList.add('<tr>' + tableRowHTML + '</tr>');
                    tableRowHTML = null;
                    groupCount = 0;
                }
                if(tableRowHTML == null && temp == prod.grouping + prod.prodName){
                    tableRowHTML = '<td width="125">'+ prod.prodName + '</td><td align="center" onClick="selLicenseType('+link+');setPrice('+price+')" id="'+prod.prodId+'">£' + prod.minCost + '</td>'; 
                    matched = true;
                }
                else if(temp == prod.grouping + prod.prodName){
                    tableRowHTML = tableRowHTML + '<td align="center" onClick="selLicenseType('+link+');setPrice('+price+')" id="'+prod.prodId+'">£'+ prod.minCost + '</td>';
                    matched = true;
                }
            }
            //--------No Match
            if(!matched){
                if(groupCount == groupList.size()){
                    tableRowList.add('<tr>' + tableRowHTML + '</tr>');
                    tableRowHTML = null;
                    groupCount = 0;
                }
                if(tableRowHTML == null){
                    tableRowHTML = '<td width="125">'+ matchId[1] + '</td><td></td>'; 
                }
                else{
                    tableRowHTML = tableRowHTML + '<td></td>';
                }  
            }           
            groupCount++;   
            matched = false;    
        }
        tableRowList.add('<tr>' + tableRowHTML + '</tr>');

        if(type == 'standard'){
            if(term == '12'){
            tableRowList12 = tableRowList;      
            groupList12 = groupList;        
            }else if(term == '24'){
            tableRowList24 = tableRowList;          
            groupList24 = groupList;    
            }else if(term == '36'){
            tableRowList36 = tableRowList;          
            groupList36 = groupList;    
            }else if(term == '60'){
            tableRowList60 = tableRowList;          
            groupList60 = groupList;    
            }
        }else if(type == 'volume'){
            if(term == '12'){
            tableRowListVol12 = tableRowList;       
            groupListVol12 = groupList;         
            }else if(term == '24'){
            tableRowListVol24 = tableRowList;       
            groupListVol24 = groupList;     
            }else if(term == '36'){
            tableRowListVol36 = tableRowList;       
            groupListVol36 = groupList;     
            }else if(term == '60'){
            tableRowListVol60 = tableRowList;   
            groupListVol60 = groupList;         
            }
        }

    } 
    public List<CSS_Products__c> getCharges(){
      List<CSS_Products__c> connCharge = [SELECT Id, Product_Name__c,Connection_Charge__c,Type__c,Grouping__c,Unit_Cost__c ,Start__c,End__c FROM CSS_Products__c WHERE (Type__c = 'Connection' OR (Type__c = 'CPE' AND Grouping__c = 'Delivery')) AND Provider__c = 'Cloud Phone' AND Active__c = True];
      return connCharge;
    }      
    public List<CSS_Products__c> getLineCharges(){
      List<CSS_Products__c> connCharge = [SELECT Id, Product_Name__c,Connection_Charge__c,Type__c,Grouping__c,Unit_Cost__c ,Start__c,End__c FROM CSS_Products__c WHERE Type__c = 'Line' AND Provider__c = 'Cloud Phone' AND Active__c = True];
      return connCharge;
    }       
    public class prodClass {  
        public Id prodId { get;set; }
        public String prodName { get;set; }  
        public String grouping { get;set; }
        public Integer gProd { get;set; }  
        public Integer gGroup { get;set; }
        public Decimal minCost { get;set; }
        public Decimal maxCost { get;set; }
        public Decimal sumCost { get;set; }
    
        public prodClass(AggregateResult ar) {   
            prodId = (Id)ar.get('prodId');
            prodName = (String)ar.get('Unit__c');  
            grouping = (String)ar.get('Grouping__c');
            gProd = (Integer)ar.get('gProd');  
            gGroup = (Integer)ar.get('gGroup');  
            minCost = (Decimal)ar.get('minCost');
            maxCost = (Decimal)ar.get('maxCost');
            sumCost = (Decimal)ar.get('sumCost');

            //--------Hack to order grouping    
            if(gProd == 1 && gGroup == 0){
                if(grouping == '500 mins'){
                    grouping = 'PAYG';
                }
                else if(grouping == 'Unlimited'){
                    grouping = '500 mins';
                }
                else if(grouping == 'PAYG'){
                    grouping = 'Unlimited';
                }
            }
        }  
    }      
    public Opportunity newOpp {
        get {
        if (newOpp == null)
            newOpp = new Opportunity();
            return newOpp ;
        }
        set;
    }         
    public Cloud_Voice__c newCV {
        get {
        if (newCV == null)
            newCV = new Cloud_Voice__c();
            return newCV ;
        }
        set;
    }      
    public Cloud_Voice_Site__c newSite {
        get {
        if (newSite == null)
            newSite = new Cloud_Voice_Site__c();
            return newSite ;
        }
        set;
    }             
    public Cloud_Voice_Site_Product__c newProd {
        get {
        if (newProd == null)
            newProd = new Cloud_Voice_Site_Product__c();
            return newProd ;
        }
        set;
    }   
        
    public PageReference createBundle() { 
        if (crdDate <= date.today()) {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select a date in the future.'));
          return null;
        }
        if (productsToAddCSVRC == '') {
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add a Qty'));
          return null;
        }
        system.debug('JMM start: ');
        //set to stop opp triggeres running
        StaticVariables.setCloudVoiceDontRun(true);
        //stop rollup summaries
        HelperRollUpSummary.setCVSiteStop(true);
        
        Cloud_Voice_Site__c cvId = [SELECT CV__c, CV__r.Opportunity__c FROM Cloud_Voice_Site__c WHERE Id = :cvSiteId LIMIT 1];

        CSS_Products__c css = [SELECT ID, Term__c, A_Code__c FROM CSS_Products__c WHERE Id = :licenseType LIMIT 1];
        CSS_Products__c rcMainLine = [SELECT ID, Term__c, A_Code__c FROM CSS_Products__c WHERE A_Code__c = 'RC0001' LIMIT 1];
        List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();

        if(rcCompetitor != '' && rcCompetitor != Null){
			List<Competitors_Suppliers_Manufacturers__c> rcCompID = [SELECT ID FROM Competitors_Suppliers_Manufacturers__c WHERE Name = :rcCompetitor LIMIT 1];
			if(rcCompID.size() > 0){
				newOpp.Main_Competitor__c = rcCompID[0].Id;
			}
        }
        newOpp.ID = cvId.CV__r.Opportunity__c;
        newOpp.zCPMissingDetails__c = detailMissing; //set flag for missing deatils  
        update newOpp;

        //update CV record
system.debug('JMM cvId:'+cvID.CV__c);
system.debug('JMM rcMultiAddress:'+rcMultiAddress);        
        newCV.Id = cvId.CV__c;
        newCV.SharerPlanID__c = 'Cloud Phone';
        newCV.InternationalSharerPlanID__c = 'Cloud Phone';
        newCV.ConnectionChargeID__c = 'Cloud Phone';
        newCV.zLicenseType__c = css.A_Code__c;
        newCV.zBandwidthID__c = rcTestId;
        newCV.zBandwidthMosScore__c = rcMOS;
        newCV.zBandwidthSimultaneousUsers__c = rcMax;     
        newCV.Contract_Term__c = rcTerm;
        newCV.Bank_Sort_Code__c = rcSortCode;
        newCV.Bank_Account_Number__c = rcAccNum;
        newCV.Billing_Account_Number__c = rcBAC;
        newCV.Opt_out_of_Direct_Debit__c = rcDD;
        newCV.zZeroConnection__c = zeroConnection;
        
        update newCV;
        system.debug('JMM newCV:'+newCV );

        //update  CV site
        newSite.Id = cvSiteId;
        newSite.Customer_Required_Date__c = crdDate;
        if(rcMultiAddress == 'Yes'){
          newSite.CPE_Multiple_Delivery_Addresses__c = True;
        }
        newSite.Access_product_decision__c = rcAccessChoice;
        newSite.Access_Order_Reference__c = rcAccessRef;
        newSite.Access_Service_Id__c = rcLineRef;
        newSite.Access_Data_Traffic__c = rcTestId +' MOS: '+ rcMOS;
        newSite.Access_Concurrent_Calls__c = rcMax;
        newSite.Number_Port_CP__c = rcPort;
        //newSite.CV__r.Opportunity__r.Main_Competitor__c = rcCompetitor;
        update newSite;

        //add license
        newProd.Cloud_Voice_Site__c = cvSiteId;
        newProd.Quantity__c = licenses ;
        newProd.SFCSSProdId__c = licenseType;
        prodList.Add(newProd); 

        List<String> csvList = productsToAddCSVRC.Split(',');
        system.debug('JMM csvList : ' + csvList );
        for(String s : csvList){
          String[] csvVals = s.Split(':');           
          //newProd = new Cloud_Voice_Site_Product__c();
          if(rcMultiAddress == 'Yes' || rcMultiSTD == 'Yes' || rcDirEntry == 'Yes'){
            for(Integer i = 0; i < Decimal.valueOf(csvVals[1]); i++){
              newProd = new Cloud_Voice_Site_Product__c();
              newProd.Cloud_Voice_Site__c = cvSiteId; 
              newProd.Quantity__c = 1;
              newProd.SFCSSProdId__c = csvVals[0];     
              if(Decimal.valueOf(csvVals[1]) > 0){
                  prodList.Add(newProd);  
              }
            }  
          }else{
            newProd = new Cloud_Voice_Site_Product__c();
            newProd.Cloud_Voice_Site__c = cvSiteId; 
            newProd.Quantity__c = Decimal.valueOf(csvVals[1]);
            newProd.SFCSSProdId__c = csvVals[0];     
            if(Decimal.valueOf(csvVals[1]) > 0){
                prodList.Add(newProd);  
            }
          }  
        }
        //add main number detail
        newProd = new Cloud_Voice_Site_Product__c();
        newProd.Cloud_Voice_Site__c = cvSiteId; 
        newProd.Quantity__c = 1;
        newProd.SFCSSProdId__c = rcMainLine.Id;  
        //newProd.Number_Info__c =  bundleSTD ;
        newProd.rcSTD__c =  bundleSTD;
        newProd.rcDirectory__c =  rcMainDirEntry;        
        prodList.Add(newProd);     
        //add connection charge
        if(!zeroConnection){
          newProd = new Cloud_Voice_Site_Product__c();
          newProd.Cloud_Voice_Site__c = cvSiteId; 
          newProd.Quantity__c = 1;
          newProd.SFCSSProdId__c = connId;  
          prodList.Add(newProd);  
        }
        //add delivery charge
        if(physicalPhones >0){
          newProd = new Cloud_Voice_Site_Product__c();
          newProd.Cloud_Voice_Site__c = cvSiteId; 
          newProd.Quantity__c = 1;
          newProd.SFCSSProdId__c = delID;  
          newProd.Connection_Charge__c = delCharge;  
          newProd.Unit_Cost__c = null;  
          newProd.Unit__c =  'DeliveryX' ;
          prodList.Add(newProd);  
        }
        system.debug('JMM prodList: ' + prodList);
        if(prodList.Size() >= 1){
          try {
            insert prodList;
          } 
          catch (DMLException e) {
            ApexPages.addMessages(e);
          }       
        }
   
        //add numbers
          Decimal addLineNum = 0;
          if(rcAddNumber !=''){
              addLineNum =Decimal.valueof(rcAddNumber);
          }
          if(addLineNum > 0){
            CSS_Products__c line= [SELECT ID, End__c FROM CSS_Products__c WHERE Type__c = 'Line' AND Grouping__c = '' AND Provider__c = 'Cloud Phone' ORDER BY Sort_Order__c ASC LIMIT 1];
            newProd = Null;
            newProd.Number_Info__c =  'New: ' + bundleSTD ;
            newProd.Locked_Edit__c = True;
            newProd.Cloud_Voice_Site__c = cvSiteId;
            newProd.Quantity__c = addLineNum;
            newProd.SFCSSProdId__c = line.Id;
            HelperRollUpSummary.setCVSiteStop(false);
            insert newProd;
          }
        HelperRollUpSummary.setCVSiteStop(false);
        StaticVariables.setCloudVoiceDontRun(false);

        //update opp with amounts
        
        if(!Test.isRunningTest()){
          Cloud_Voice__c updCV = [SELECT Id, Opportunity__c,zOpportunityLineItem__c,zOpportunityLineItemLease__c,Total_1st_Year_Charges__c,Total_Selected_Finance__c,Contract_Term__c,Opportunity__r.Account_Sector__c,Total_Recurring_Charges__c FROM Cloud_Voice__c WHERE Id = :cvID.CV__c LIMIT 1];        
  
          OpportunityLineItem oliT = [SELECT UnitPrice FROM OpportunityLineItem WHERE Id = :updCV.zOpportunityLineItem__c];
          OpportunityLineItem oliL = [SELECT UnitPrice FROM OpportunityLineItem WHERE Id = :updCV.zOpportunityLineItemLease__c];

          Decimal oppTotal = (updCV.Total_1st_Year_Charges__c-updCV.Total_Selected_Finance__c);
          Integer conTerm = Integer.valueOf(updCV.Contract_Term__c.substring(0,2));
          String sector = updCV.Opportunity__r.Account_Sector__c;
          if(sector != Null){
              sector = sector.toUpperCase();
          }
          system.debug('JMM conTerm:'+conTerm);
          if(conTerm > 12 && sector == 'BT LOCAL BUSINESS'){
              oppTotal = oppTotal + (updCV.Total_Recurring_Charges__c*(conTerm-12));
          }
          oliT.UnitPrice = oppTotal;
          oliL.UnitPrice = updCV.Total_Selected_Finance__c;

          update oliT;
          update oliL;
        }
        //return to opp/header/site
        PageReference retPg = Null;
        if(rcmultiAddress == 'Yes' || rcMultiSTD == 'Yes' || rcDirEntry == 'Yes'){
          retPg = page.CloudVoiceMultiAddress;
          retPg.getParameters().put('cvID',cvSiteId);
          retPg.getParameters().put('accName',getaccName());
          retPg.getParameters().put('std',bundleSTD);
          retPg.getParameters().put('multiAdd',rcMultiAddress);
          retPg.getParameters().put('multiSTD',rcMultiSTD);
          retPg.getParameters().put('dEntry',rcDirEntry);
        }else{
          retPg = new PageReference('/'+cvID.CV__c);
        }
        return retPg.setRedirect(true); 
    }
    public List<Cloud_Voice__C> getResult(){
        String clearID = obfId.substring(9,27);
        system.debug('JMM Id: '+clearId);
        List<Cloud_Voice__c> cvRec = [SELECT ID, Name, Total_Initial_Charges__c, Total_Recurring_Charges__c FROM Cloud_Voice__c WHERE Id = :clearID];
        return cvRec;
    }

    public List<Cloud_Voice_Site_Product__c> getProducts(){
      return [SELECT Id, Product_Name__c,Number_Info__c, Cloud_Voice_Site__r.Delivery_address__c, Cloud_Voice_Site__r.Site_contact_address__c, Image__c,A_Code__c,Type__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :urlCVSiteId AND Type__c='CPE' AND Grouping__c = 'Phone'];
    }    
    public List<Cloud_Voice_Site__c> getSiteAddress(){
      //List<Cloud_Voice_Site__c> recs = [SELECT Id, Delivery_address__c, Site_contact_address__c FROM Cloud_Voice_Site__c WHERE Id = :urlCVSiteId];
      //return recs;
      return [SELECT Id, Delivery_address__c, Site_contact_address__c FROM Cloud_Voice_Site__c WHERE Id = :urlCVSiteId];
    } 

    public PageReference addMultiAddress(){   
system.debug('jmm test urlCVSiteId:'+urlCVSiteId);
      Cloud_Voice_Site__c cvId = [SELECT CV__c FROM Cloud_Voice_Site__c WHERE Id = :urlCVSiteId LIMIT 1];
      List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();
      List<Cloud_Voice_Site_Product__c> prodList2 = new List<Cloud_Voice_Site_Product__c>();
      List<String> csvList = multiAddressCSV.Split(';');
      Set<Id> prodIds = New Set<Id>();

      for(String s : csvList){
        newProd = new Cloud_Voice_Site_Product__c();
        String[] csvVals = s.Split(':');       
        newProd.Id = csvVals[0];    
        newProd.Number_Info__c = csvVals[1]; 
        newProd.rcSTD__c = csvVals[2]; 
        newProd.rcDirectory__c = csvVals[3]; 
        prodList.Add(newProd); 
        prodIds.Add(csvVals[0]); 
      }    
      update prodList;

      //re-summarise products
      List<AggregateResult> aggRes = new list<AggregateResult>();

      aggRes = [Select SFCSSProdId__c, Number_Info__c, rcSTD__c, rcDirectory__c, GROUPING(SFCSSProdId__c) gProd, GROUPING(Number_Info__c)gAdd, SUM(Quantity__c)totQty, Min(Product_Name__c)pName
      FROM Cloud_Voice_Site_Product__c 
      //WHERE Cloud_Voice_Site__c = :urlCVSiteId
      WHERE Id In :prodIds
      GROUP BY SFCSSProdId__c, Number_Info__c, rcSTD__c, rcDirectory__c
      ORDER BY MIN(Product_Name__c)];
      
      for(AggregateResult ar: aggRes){
        newProd = new Cloud_Voice_Site_Product__c();
        newProd.Cloud_Voice_Site__c = urlCVSiteId;
        newProd.SFCSSProdId__c = (String)ar.get('SFCSSProdId__c');         
        newProd.Quantity__c = (Decimal)ar.get('totQty'); 
        newProd.Number_Info__c =  (String)ar.get('Number_Info__c') ;
        newProd.rcSTD__c =  (String)ar.get('rcSTD__c') ;
        newProd.rcDirectory__c =  (String)ar.get('rcDirectory__c') ;
        prodList2.Add(newProd); 
      }
      HelperRollUpSummary.setCVSiteStop(false);
      insert prodList2;
      delete prodList;

      PageReference retPg = new PageReference('/'+cvId.CV__c);
      return retPg;
    } 
       
    public PageReference passToLB(){
      Cloud_Voice_Site__c cvId = [SELECT CV__c, CV__r.Opportunity__c FROM Cloud_Voice_Site__c WHERE Id = :cvSiteId LIMIT 1];
          
      system.debug('jmm licenseType: ' +licenseType);
      string strDesc = 'Number of users: ' + licenses;
      //Opportunity updOpp = [SELECT Id FROM Opportunity WHERE Id = :oId];
      if(licenseType !=''){
        CSS_Products__c css = [SELECT ID, Term__c, A_Code__c,Product_Name__c FROM CSS_Products__c WHERE Id = :licenseType LIMIT 1];
        strDesc = strDesc +  '\nLicense: ' + css.Product_Name__c;
      }
      strDesc = strDesc +  '\n' + multiAddress;

      newOpp.ID = cvId.CV__r.Opportunity__c;  
      newOpp.Auto_Assign_Owner__c = True;
      newOpp.Description = strDesc;
      update newOpp;

      PageReference retPg = Null;
      retPg = new PageReference('/'+ cvId.CV__r.Opportunity__c);
      return retPg.setRedirect(true);
    }   
    public PageReference cancelOrder(){
      Cloud_Voice_Site__c cvRec = [SELECT Site_Contact__c, CV__r.Opportunity__r.Id FROM Cloud_Voice_Site__c WHERE Id = :urlCVSiteId];
      Opportunity oppId = [SELECT Id FROM Opportunity WHERE Id = :cvRec.CV__r.Opportunity__r.Id];
      
      delete oppId;

      PageReference retPg = Null;
      retPg = new PageReference('/'+ cvRec.Site_Contact__c);
      return retPg.setRedirect(true);
    }

    public PageReference sendEmail(){
      Contact con = [SELECT Name, Email FROM Contact WHERE Id = :conId];
      
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

      String[] toAddresses = new String[] {con.Email};
      mail.setToAddresses(toAddresses);
      mail.setOrgWideEmailAddressId('0D220000000CbZl');

      mail.setUseSignature(false);

      mail.setSubject('BT Cloud Phone Bandwidth Check FAO:' + con.Name);

      mail.setPlainTextBody('THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY\n\n'
        +'Dear '+con.Name+'\n'
        +'Please navigate to the link below to run the BT Cloud Phone bandwidth checker from the desired network access line.\n'
        +'http://setup-bt.ringcentral.com/sa/bandwidth.html\n'
        +'When complete let the BT Sales Person know the results.\n\n'
        +'Regards\n'
        +'BT Cloud Phone Team');

      mail.setHtmlBody('<span style="font-family:Arial;font-size:13px">'
        +'<span style="font-weight:bold;color:red;">THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY<br/><br/></span>'
        +'Dear '+con.Name+'<br/>'
        +'Please navigate to the link below to run the BT Cloud Phone bandwidth checker from the desired network access line.<br/>'
        +'<a href="http://setup-bt.ringcentral.com/sa/bandwidth.html">Run Bandwidth Tool</a><br/>'
        +'When complete let the BT Sales Person know the results.<br/><br/>'
        +'Regards<br/>'
        +'BT Cloud Phone Team'
        +'</span>');      

      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
      emailSent = 'Email sent to ' + con.Email;
      return null;
    }
}