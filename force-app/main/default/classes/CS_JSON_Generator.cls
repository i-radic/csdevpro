public class CS_JSON_Generator {
    public static final String ResourceName = 'JSONExport';

    public Set<String> setOfProductActivitiesToExtract = new Set<String>();

    public CS_JSON_Schema schema {get; set;}

    public Id id {get; set;}

    public csbtcl_Temporary_Data_Store_CS_JSON__c dataStore {get; set;}

    public List<String> nadKeysList = new List<String>();

    public Map<Id, cscfga__Product_Configuration__c> productConfigurationsByIdsMap;
    public Map<String, Order_Address__c> quoteBuilderAddressesMap = new Map<String, Order_Address__c>();
    public Map<String, List<Number_Port_Line_CPQ__c>> nadNumberPortLinesMap = new Map<String, List<Number_Port_Line_CPQ__c>>();
    public List<Attachment> productConfigurationsAttachmentsList = new List<Attachment>();
	
	public CS_JSON_Generator(Id id) {
		this.schema = getSchema();
		this.id = id;
		this.dataStore = new csbtcl_Temporary_Data_Store_CS_JSON__c(
			Opportunity__c = id,
			JSON_Generated_Date_Time__c = System.now()
		);
	}

    public void generateJSON() {
		
        cscfga__Product_Basket__c basket = getBasketLastSyncedWithOpportunity();

		if(basket == null) {
			return;
		}

        Attachment attachment = new Attachment();
        attachment.Name = 'testJSON';

        //JSON structure is defined here in OCSProject
        CS_CreateDownstreamJSONCSVPayload.OCSProject ocsProject = new CS_CreateDownstreamJSONCSVPayload.OCSProject();
        CS_CreateDownstreamJSONCSVPayload.ProjectInformation projectInformation = new CS_CreateDownstreamJSONCSVPayload.ProjectInformation();

        projectInformation.OpportunityID = id;

        ocsProject.ProjectInformation = projectInformation;
        ocsProject.BasketInformation = getBasketInfo(basket);
       
        productConfigurationsByIdsMap = getAllProductConfigurationsForBasket(basket.Id);
        getNumberPortLinesForBasket(basket.Id);

        if (productConfigurationsByIdsMap.values() != null) {
            nadKeysList = getAllProductConfigurationNADKeysForBasket();

            List<Order_Address__c> orderAddresses = getAllOrderAddressesWithNAD();

            if (orderAddresses != null) {
                for (Order_Address__c orderAddress :  orderAddresses) {
                    quoteBuilderAddressesMap.put(orderAddress.NAD__c, orderAddress);
                }
            }
        }

        ocsProject.Sites = getAllSitesForBasket(quoteBuilderAddressesMap);
        ocsProject.CompanyProducts = getCompanyProductsForBasket();

        string query = 'SELECT Id, ParentId FROM Attachment WHERE ParentId=\'0067Y00000AUXJWQA5\'';

        List<Attachment> attachmentsList = new List<Attachment>();
        attachmentsList = Database.query(query);

        delete attachmentsList;

        String downstreamJSON = JSON.serializePretty(ocsProject);
        attachment.Body = Blob.valueOf(downstreamJSON);
        attachment.ParentId = ocsProject.BasketInformation.OpportunityID;

        insert attachment;
    }

    public cscfga__Product_Basket__c getBasketLastSyncedWithOpportunity() {
        cscfga__Product_Basket__c basket = [SELECT Id,
        Name,
        cscfga__Opportunity__c,
        csbb__Account__c,
        Customer_Contact__c,
        LastModifiedDate,
        csbb__Account__r.Name,
        Customer_Contact__r.Name,
        Customer_Contact__r.Email,
        Customer_Contact__r.Phone,
        Customer_Contact__r.MobilePhone,
        cscfga__total_contract_value__c,
        cscfga__Opportunity__r.Owner.Name,
        cscfga__Opportunity__r.Owner.Email,
        cscfga__Opportunity__r.Owner.EIN__c,
        cscfga__Opportunity__r.Owner.Phone,
        cscfga__Opportunity__r.Account_Sector__c,
        cscfga__Opportunity__r.AssociatedLE__c,
        csbb__Account__r.SAC_Code__c,
        csbb__Account__r.Account_Owner_Department__c,
        csbb__Account__r.Sector__c,
        csbb__Account__r.Sub_Sector__c,
        csbb__Account__r.CUG__c,
        cscfga__Opportunity__r.Name,
        cscfga__Opportunity__r.Campaign.Name,
        cscfga__Opportunity__r.CloseDate,
        Total_Subscribers__c,
        Basket_Reference__c,
        cscfga__Opportunity__r.StageName,
        cscfga__Opportunity__r.Opportunity_Id__c,
        cscfga__Opportunity__r.Description,
        cscfga__Opportunity__r.Amount,
        cscfga__Opportunity__r.IsClosed__c,
        cscfga__Opportunity__r.Closed_By__c,
        cscfga__Opportunity__r.Account_Le__c,
        cscfga__Opportunity__r.SOV_nonSub__c,
        cscfga__Opportunity__r.Closing_Channel__c,
        cscfga__Opportunity__r.AssociatedLE__r.Registered_Address__c,
        cscfga__Opportunity__r.SEC_Code__c,
        csbb__Account__r.zAccountMarker__c
        FROM cscfga__Product_Basket__c
        WHERE cscfga__Opportunity__c = :id
        AND csbb__Synchronised_With_Opportunity__c = true
        ORDER BY LastModifiedDate DESC
        LIMIT 1];

        return basket;
    }

    public CS_CreateDownstreamJSONCSVPayload.Basket getBasketInfo(cscfga__Product_Basket__c basket) {

        CS_CreateDownstreamJSONCSVPayload.Basket basketInfo = new CS_CreateDownstreamJSONCSVPayload.Basket();

		basketInfo.OpportunityID = basket.cscfga__Opportunity__c;
        basketInfo.AccountID = basket.csbb__Account__c;
        basketInfo.ContactID = basket.Customer_Contact__c;
        basketInfo.HeaderRef = basket.Id;
        basketInfo.AccountName = basket.csbb__Account__r.Name;
        basketInfo.CustomerContact = basket.Customer_Contact__r.Name;
        basketInfo.CustomerContactEmail = basket.Customer_Contact__r.Email;
        basketInfo.CustomerContactTelephone = basket.Customer_Contact__r.Phone;
        basketInfo.CustomerContactMobile = basket.Customer_Contact__r.MobilePhone;
        basketInfo.TotalCost = String.valueOf(basket.cscfga__total_contract_value__c);
        basketInfo.SalesName = basket.cscfga__Opportunity__r.Owner.Name;
        basketInfo.SalesEmail = basket.cscfga__Opportunity__r.Owner.Email;
        basketInfo.SalesUIN = basket.cscfga__Opportunity__r.Owner.EIN__c;
        basketInfo.SalesTelephone = basket.cscfga__Opportunity__r.Owner.Phone;
        basketInfo.BasketURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + basket.Id;
        basketInfo.BTLBSector = basket.cscfga__Opportunity__r.Account_Sector__c;
        basketInfo.AccountLECode = basket.cscfga__Opportunity__r.AssociatedLE__c;
        basketInfo.AccountSACCode = basket.csbb__Account__r.SAC_Code__c;
        basketInfo.AccountCUG = basket.csbb__Account__r.CUG__c;
        basketInfo.OpportunityName = basket.cscfga__Opportunity__r.Name;
        basketInfo.CampaignName = basket.cscfga__Opportunity__r.Campaign.Name;
        basketInfo.isFinanced = false;
        basketInfo.BasketName = basket.name;
        basketInfo.OpportunityURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' + basket.cscfga__Opportunity__c;
        basketInfo.TotalSubscribers = basket.Total_Subscribers__c;
        basketInfo.BasketReference = basket.Basket_Reference__c;
        basketInfo.OpportunityIDAutoNumber = basket.cscfga__Opportunity__r.Opportunity_Id__c;
        basketInfo.OpportunityDescription = basket.cscfga__Opportunity__r.Description;
        basketInfo.OpportunityAmount = basket.cscfga__Opportunity__r.Amount;
        basketInfo.IsClosed = basket.cscfga__Opportunity__r.IsClosed__c;
        basketInfo.ClosedBy = basket.cscfga__Opportunity__r.Closed_By__c;
        basketInfo.AccountLE = basket.cscfga__Opportunity__r.Account_Le__c;
        basketInfo.SOV = basket.cscfga__Opportunity__r.SOV_nonSub__c;
        basketInfo.ClosingChannel = basket.cscfga__Opportunity__r.Closing_Channel__c;
        basketInfo.AssociatedLEsRegisteredAddress = basket.cscfga__Opportunity__r.AssociatedLE__r.Registered_Address__c;
        basketInfo.SECCode = basket.cscfga__Opportunity__r.SEC_Code__c;

        return basketInfo;
	}

    public List<CS_CreateDownstreamJSONCSVPayload.Site> getAllSitesForBasket(Map<String, Order_Address__c> quoteBuilderAddressesMap) {

        List<CS_CreateDownstreamJSONCSVPayload.Site> sitesList = new List<CS_CreateDownstreamJSONCSVPayload.Site>();

        for (String nadKey : quoteBuilderAddressesMap.keySet()) {
            CS_CreateDownstreamJSONCSVPayload.Site site = new CS_CreateDownstreamJSONCSVPayload.Site();
            site.SiteProducts = new List<CS_CreateDownstreamJSONCSVPayload.Product>();
            site.NumberDetails = new List<CS_CreateDownstreamJSONCSVPayload.NumberDetail>();

            Order_Address__c orderAddress = quoteBuilderAddressesMap.get(nadKey);

            site.SiteReference = orderAddress.NAD__c;
            site.InstallationSiteAddress = orderAddress.Display_Name__c;
            site.InstallationBuildingName = orderAddress.Building_Name__c;
            site.InstallationNumber = orderAddress.Number__c;
            site.InstallationLocality = orderAddress.Locality__c;
            site.InstallationStreet = orderAddress.Street__c;
            site.InstallationStreet2 = orderAddress.Street_2__c;
            site.InstallationPostTown = orderAddress.Post_Town__c;
            site.InstallationPostCode = orderAddress.Post_Code__c;
            site.InstallationCountry = orderAddress.Country__c;

            for (cscfga__Product_Configuration__c pc : productConfigurationsByIdsMap.values()) {
                if (pc.Product_Definition_Name__c == 'Site Access') {
                    Order_Address__c deliveryOrderAddress = quoteBuilderAddressesMap.get(pc.DeliveryNADKey__c);
    
                    site.DeliverySiteAddress = deliveryOrderAddress.Display_Name__c;
                    site.DeliveryBuildingName = deliveryOrderAddress.Building_Name__c;
                    site.DeliveryNumber = deliveryOrderAddress.Number__c;
                    site.DeliveryLocality = deliveryOrderAddress.Locality__c;
                    site.DeliveryStreet = deliveryOrderAddress.Street__c;
                    site.DeliveryStreet2 = deliveryOrderAddress.Street_2__c;
                    site.DeliveryPostTown = deliveryOrderAddress.Post_Town__c;
                    site.DeliveryPostCode = deliveryOrderAddress.Post_Code__c;
                    site.DeliveryCountry = deliveryOrderAddress.Country__c;
                    
                    site.SiteName = pc.Name;
                    site.CRD = String.valueOf(pc.Customer_Required_Date__c);
                    site.SiteProducts = getSiteProducts(pc.Name);
                }
            }
            sitesList.add(site);

            if (nadNumberPortLinesMap.get(nadKey) != null) {
                for (Number_Port_Line_CPQ__c portLine : nadNumberPortLinesMap.get(nadKey)) {
                    CS_CreateDownstreamJSONCSVPayload.NumberDetail numberPort = new CS_CreateDownstreamJSONCSVPayload.NumberDetail();
    
                    numberPort.AssociatedNumbers = portLine.Associated_Numbers__c;
                    numberPort.BillingACNumber = portLine.Billing_A_C_Number__c;
                    numberPort.LosingCP = portLine.Losing_CP__c;
                    numberPort.MainNumber = portLine.Main_Number__c;
                    numberPort.NumberEnd = portLine.Number_End__c;
                    numberPort.NumberStart = portLine.Number_Start__c;
                    numberPort.PVTRef = portLine.PVT_Ref__c;
                    numberPort.RangeHolder = portLine.Range_Holder__c;
                    numberPort.ServiceUsage = portLine.Service_Usage__c;
                    numberPort.AddToBTDirectory = portLine.Add_to_BT_Directory__c;
                    numberPort.NumberType = portLine.Number_Type__c;
    
                    site.NumberDetails.add(numberPort);
                }
            }
        }

        return sitesList;

    }

    public CS_JSON_Schema getSchema() {
		return (CS_JSON_Schema) JSON.deserialize(CS_Utils.loadStaticResource(ResourceName).toString(), CS_JSON_Schema.class);
	}

    public Map<Id, cscfga__Product_Configuration__c> getAllProductConfigurationsForBasket(string productBasketId) {
        Map<Id, cscfga__Product_Configuration__c> productConfigurationsByIdsMap;

        productConfigurationsByIdsMap = new Map<Id, cscfga__Product_Configuration__c>([
                    SELECT
                            Id,
                            Name,
                            Change_Type__c,
                            cscfga__Serial_Number__c,
                            cscfga__Quantity__c,
                            cscfga__One_Off_Charge__c,
                            cscfga__Recurring_Charge__c,
                            cscfga__Contract_Term__c,
                            cscfga__Product_Family__c,
                            Product_Definition_Name__c,
                            Module__c,
                            Grouping__c,
                            Financed__c,
                            Customer_Required_Date__c,
                            cscfga__total_contract_value__c,
                            cscfga__total_recurring_charge__c,
                            cscfga__total_one_off_charge__c,
                            cscfga__one_off_charge_product_discount_value__c,
                            cscfga__recurring_charge_product_discount_value__c,
                            Product_Code__c,
                            cscfga__Description__c,
                            Site_NAD_Key__c,
                            Site_Name__c,
                            BT_Net_Primary__c,
                            BT_Net_Secondary__c,
                            DeliveryNADKey__c,
                            Add_On_Quantity__c,
                            cscfga__Product_Basket__c,
                            cscfga__Parent_Configuration__c
                    FROM cscfga__Product_Configuration__c
                    WHERE cscfga__Product_Basket__c = :productBasketId
            ]);

        return productConfigurationsByIdsMap;
    }


    public List<String> getAllProductConfigurationNADKeysForBasket() {
        List<String> nadKeysList = new List<String>();

        for (cscfga__Product_Configuration__c pc : productConfigurationsByIdsMap.values()) {
            if (pc.Site_NAD_Key__c != null && pc.Site_NAD_Key__c != '') {
                if (!nadKeysList.contains(pc.Site_NAD_Key__c)) {
                    nadKeysList.add(pc.Site_NAD_Key__c);
                }
            }
        }

        return nadKeysList;
	}

    public List<Order_Address__c> getAllOrderAddressesWithNAD() {
        List<Order_Address__c> orderAddresses = [SELECT
                    NAD__c,
                    Display_Name__c,
                    Building_Name__c,
                    Number__c,
                    Locality__c,
                    Street__c,
                    Street_2__c,
                    Post_Town__c,
                    Post_Code__c,
                    Country__c
            FROM  Order_Address__c
            WHERE NAD__c IN :nadKeysList
        ];
        
        return orderAddresses;
    }

    public void getNumberPortLinesForBasket(string productBasketId) {
        List<Number_Port_Line_CPQ__c> numberPortLines = [SELECT
                    Associated_Numbers__c,
                    Billing_A_C_Number__c,
                    Losing_CP__c,
                    Main_Number__c,
                    Number_End__c,
                    Number_Start__c,
                    Number_Type__c,
                    Product_Basket__c,
                    PVT_Ref__c,
                    Range_Holder__c,
                    Service_Usage__c,
                    Order_Address__r.NAD__c,
                    Add_to_BT_Directory__c
            FROM Number_Port_Line_CPQ__c
            WHERE Product_Basket__c = :productBasketId
        ];

        if (numberPortLines != null) {
            for (Number_Port_Line_CPQ__c portLine : numberPortLines) {
                List<Number_Port_Line_CPQ__c> portLines = nadNumberPortLinesMap.get(portLine.Order_Address__r.NAD__c);
                if (portLines == null) {
                    nadNumberPortLinesMap.put(portLine.Order_Address__r.NAD__c, new List<Number_Port_Line_CPQ__c>{portLine});
                } else {
                    portLines.add(portLine);
                }
            }
        }
    }

    public List<CS_CreateDownstreamJSONCSVPayload.Product> getCompanyProductsForBasket() {
        List<CS_CreateDownstreamJSONCSVPayload.Product> companyProductsList =  new List<CS_CreateDownstreamJSONCSVPayload.Product>();
        for (cscfga__Product_Configuration__c pc : productConfigurationsByIdsMap.values()) {
            if (pc.Name == 'Cloud Voice Service') {
                CS_CreateDownstreamJSONCSVPayload.Product companyProduct = new CS_CreateDownstreamJSONCSVPayload.Product();
                companyProduct.AddOns = new List<CS_CreateDownstreamJSONCSVPayload.Product>();
                companyProduct.ProductActivities = new List<CS_CreateDownstreamJSONCSVPayload.AttributeDetail>();
                companyProduct.productActivities = getAllAttributesStoredAsJSONForPC(pc.Id);

                companyProduct.AddOns = getAllAddonsForPC(pc);

                companyProduct.ChangeType = pc.Change_Type__c;
                companyProduct.ProductName = pc.cscfga__Description__c;
                companyProduct.SerialNumber = pc.cscfga__Serial_Number__c;
                companyProduct.Quantity = Integer.valueOf(pc.cscfga__Quantity__c);
                companyProduct.OneOffCharge = pc.cscfga__One_Off_Charge__c;
                companyProduct.RecurringCharge = pc.cscfga__Recurring_Charge__c;
                companyProduct.ContractTerm = Integer.valueOf(pc.cscfga__Contract_Term__c);
                companyProduct.ProductFamily = pc.cscfga__Product_Family__c;
                companyProduct.ProductDefinitionName = pc.Product_Definition_Name__c;
                companyProduct.Module = pc.Module__c;
                companyProduct.Grouping = pc.Grouping__c;
                companyProduct.Financed = pc.Financed__c;
                companyProduct.TotalContractValue = pc.cscfga__total_contract_value__c;
                companyProduct.TotalRecurringCharge = pc.cscfga__total_recurring_charge__c;
                companyProduct.TotalOneOffCharge = pc.cscfga__total_one_off_charge__c;
                companyProduct.OneOffChargeProductDiscountValue = pc.cscfga__one_off_charge_product_discount_value__c;
                companyProduct.RecurringChargeProductDiscountValue = pc.cscfga__recurring_charge_product_discount_value__c;
                companyProduct.ProductCode = pc.Product_Code__c;
                if (pc.Product_Code__c != null) {
                    companyProduct.isProduct = true;
                } else {
                    companyProduct.isProduct = false;
                }

                companyProductsList.add(companyProduct);
            }
        }

        return companyProductsList;
	}

    public List<CS_CreateDownstreamJSONCSVPayload.Product> getAllAddonsForPC(cscfga__Product_Configuration__c parentPC) {
        List<CS_CreateDownstreamJSONCSVPayload.Product> addOnsList = new List<CS_CreateDownstreamJSONCSVPayload.Product>();

        for (cscfga__Product_Configuration__c childPC : productConfigurationsByIdsMap.values()) {
            if (childPC.cscfga__Parent_Configuration__c == parentPC.Id) {
                CS_CreateDownstreamJSONCSVPayload.Product addOn = new CS_CreateDownstreamJSONCSVPayload.Product();
                addOn.ChangeType = childPC.Change_Type__c;
                addOn.ProductName = childPC.cscfga__Description__c;
                addOn.SerialNumber = childPC.cscfga__Serial_Number__c;
                addOn.Quantity = Integer.valueOf(childPC.Add_On_Quantity__c);
                addOn.OneOffCharge = childPC.cscfga__One_Off_Charge__c;
                addOn.RecurringCharge = childPC.cscfga__Recurring_Charge__c;
                addOn.ContractTerm = Integer.valueOf(childPC.cscfga__Contract_Term__c);
                addOn.ProductFamily = childPC.cscfga__Product_Family__c;
                addOn.ProductDefinitionName = childPC.Product_Definition_Name__c;
                addOn.Module = childPC.Module__c;
                addOn.Grouping = childPC.Grouping__c;
                addOn.Financed = childPC.Financed__c;
                addOn.TotalContractValue = childPC.cscfga__total_contract_value__c;
                addOn.TotalRecurringCharge = childPC.cscfga__total_recurring_charge__c;
                addOn.TotalOneOffCharge = childPC.cscfga__total_one_off_charge__c;
                addOn.OneOffChargeProductDiscountValue = childPC.cscfga__one_off_charge_product_discount_value__c;
                addOn.RecurringChargeProductDiscountValue = childPC.cscfga__recurring_charge_product_discount_value__c;
                addOn.ProductCode = childPC.Product_Code__c;
                if (childPC.Product_Code__c != null) {
                    addOn.isProduct = true;
                } else {
                    addOn.isProduct = false;
                }

                addOnsList.add(addOn);
            }
        }
        return addOnsList;
    }

    public List<Attachment> getAllAttachmentsForPC(Id PcId) {
        List<Attachment> productConfigurationsAttachmentsList = [SELECT
                    Id,
                    Name,
                    Body,
                    ParentId
            FROM  Attachment
            WHERE ParentId = :PcId
        ];

        return productConfigurationsAttachmentsList;
    }

    public List<CS_CreateDownstreamJSONCSVPayload.Product> getSiteProducts(string siteName) {
        List<CS_CreateDownstreamJSONCSVPayload.Product> siteProductsList = new List<CS_CreateDownstreamJSONCSVPayload.Product>();

        for (cscfga__Product_Configuration__c pc : productConfigurationsByIdsMap.values()) {
            CS_CreateDownstreamJSONCSVPayload.Product product = new CS_CreateDownstreamJSONCSVPayload.Product();

            if (pc.name.startsWith(siteName)) {
                if (pc.Product_Definition_Name__c != 'BTnet') {
                    product.ChangeType = pc.Change_Type__c;
                    product.ProductName = pc.cscfga__Description__c;
                    product.SerialNumber = pc.cscfga__Serial_Number__c;
                    product.Quantity = Integer.valueOf(pc.cscfga__Quantity__c);
                    product.OneOffCharge = pc.cscfga__One_Off_Charge__c;
                    product.RecurringCharge = pc.cscfga__Recurring_Charge__c;
                    product.ContractTerm = Integer.valueOf(pc.cscfga__Contract_Term__c);
                    product.ProductFamily = pc.cscfga__Product_Family__c;
                    product.ProductDefinitionName = pc.Product_Definition_Name__c;
                    product.Module = pc.Module__c;
                    product.Grouping = pc.Grouping__c;
                    product.Financed = pc.Financed__c;
                    product.TotalContractValue = pc.cscfga__total_contract_value__c;
                    product.TotalRecurringCharge = pc.cscfga__total_recurring_charge__c;
                    product.TotalOneOffCharge = pc.cscfga__total_one_off_charge__c;
                    product.OneOffChargeProductDiscountValue = pc.cscfga__one_off_charge_product_discount_value__c;
                    product.RecurringChargeProductDiscountValue = pc.cscfga__recurring_charge_product_discount_value__c;
                    product.ProductCode = pc.Product_Code__c;
                    if (pc.Product_Code__c != null) {
                        product.isProduct = true;
                    } else {
                        product.isProduct = false;
                    }

                    List<CS_CreateDownstreamJSONCSVPayload.Product> addOnsList = new List<CS_CreateDownstreamJSONCSVPayload.Product>();
                    addOnsList = getAllAddonsForPC(pc);
                    product.AddOns = addOnsList;
                }
                siteProductsList.add(product);
            }
        }

        return siteProductsList;
    }

    public List<CS_CreateDownstreamJSONCSVPayload.AttributeDetail> getAllAttributesStoredAsJSONForPC(Id productConfigurationId) {
        List<CS_CreateDownstreamJSONCSVPayload.AttributeDetail> ProductActivitiesList = new List<CS_CreateDownstreamJSONCSVPayload.AttributeDetail>();

        List<Attachment> productConfigurationsAttachmentsList = getAllAttachmentsForPC(productConfigurationId);

        if (productConfigurationsAttachmentsList == null || productConfigurationsAttachmentsList.size() == 0) {
            return null;
        }

        string attributesAsJSONString = JSON.serialize(productConfigurationsAttachmentsList[0].Body.toString());

        List<String> listOfAttributes = attributesAsJSONString.split('"attributes"');

        if (listOfAttributes == null || listOfAttributes.size() == 0) {
            return null;
        }

        List<String> listOfAttributesSplitted = listOfAttributes[0].split('attributes');

        if (listOfAttributesSplitted == null || listOfAttributesSplitted.size() == 0) {
            return null;
        }

        for (string currentAttribute : listOfAttributesSplitted) {
            integer nameIndex = currentAttribute.indexOf('Name');
            integer valueIndex = currentAttribute.indexOf('Value', nameIndex); 

            if (nameIndex != -1 && valueIndex != -1) {
                string attributeName = currentAttribute.substring(nameIndex).substringBetween(':\\\"', '\\\"');
                string attributeValue = currentAttribute.substring(valueIndex).substringBetween(':\\\"', '\\\"');
                CS_CreateDownstreamJSONCSVPayload.AttributeDetail attributeEntry = new CS_CreateDownstreamJSONCSVPayload.AttributeDetail(attributeName, attributeValue);

                ProductActivitiesList.add(attributeEntry);
            }
        }

        return ProductActivitiesList;
    }

    //used to populate the set of product activities (order enrichement attributes) to be extracted from JSONs attached to product configuration
    public void populateSetOfProductActivitiesToExtract() {
        setOfProductActivitiesToExtract.add('Numbers to Port');
        setOfProductActivitiesToExtract.add('New Numbers Required');
        setOfProductActivitiesToExtract.add('Call Analytics Package');
        setOfProductActivitiesToExtract.add('Total Numbers');
        setOfProductActivitiesToExtract.add('Additional Numbers Required');
        setOfProductActivitiesToExtract.add('Minimum Numbers Required');
        setOfProductActivitiesToExtract.add('Care Level');
        setOfProductActivitiesToExtract.add('Contract Term');
        setOfProductActivitiesToExtract.add('Opt Out Paperless');
        setOfProductActivitiesToExtract.add('Opt Out One Bill');
        setOfProductActivitiesToExtract.add('Opt Out Direct Debit');
        setOfProductActivitiesToExtract.add('VP Billing Account Ref');
        setOfProductActivitiesToExtract.add('VP Billing Account');
        setOfProductActivitiesToExtract.add('BAC Account');
        setOfProductActivitiesToExtract.add('Billing Account');
    }

}