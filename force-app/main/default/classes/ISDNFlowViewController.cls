public with sharing class ISDNFlowViewController {
 public ISDN_CRF__c ISDN {get;set;}
    public Id crfId;
    public String RecordTypeName;
    public String CRFRTId;
    public Channels__c channel {get;set;}
    List<ISDN_CRF__c> lstISDN;
    public List<Flow_CRF__c> FlowCRF {get;set;}
    public List<Flow_Address_Details__c> lstProvideAddress {get;set;}
    public List<Flow_Address_Details__c> lstCeaseAddress {get;set;}
    public List<Flow_Address_Details__c> lstBillingAddress {get;set;}
    
    
    public ISDNFlowViewController(ApexPages.StandardController controller) {
        crfId=Apexpages.currentPage().getParameters().get('id');
        
        FlowCRF = new List<Flow_CRF__c> ();
        
        FlowCRF = [select Id,Telephone_Numbers_DDI_range__c,CLIP_A18025__c,CLIR_A18026__c,Other_CSS_Order_Codes_1__c,Other_CSS_Order_Codes_2__c,
                   Other_CSS_Order_Codes_3__c,Divert_no_reply_A18018__c,Divert_all_calls_A18016__c,Divert_on_E_F_A18017__c,Working_Type__c,Exist_No_to_convert_to_ISDN__c
                   From Flow_CRF__c where Id=: CrfId Limit 1]; 
        
        ISDN = new ISDN_CRF__c();
        lstISDN = new List<ISDN_CRF__c>();
        
        
        
        lstISDN = [Select Id, Bearer_Location__c, New_BLDG_No_BT_Lines_Or_Services__c, Alternate_Bearer__c, Alternative_Routing_check_1__c, Alternative_Routing_check_2__c,
                    Customer_Name_ISDN__c, Customer_Name_ISDN_DP__c, Diverse_Routing_check_1__c, Diverse_Routing_check_2__c, Diverse_Routing_DP__c, Fax_ISDN__c, 
                    Fax_ISDN_DP__c, LOP_ref__c, LOP_ref_DR__c, Named_Contacts_and_contact_numbers_1__c, Named_Contacts_and_contact_numbers_2__c, 
                    Named_Contacts_and_contact_numbers_3__c, Password__c, Remote_exchange_name_and_code_DR__c, Sales_Agent__c, Secondary_exchange_name_and_code__c, 
                    Tel__c, Telephone_No_ISDN__c, Telephone_No_ISDN_DP__c, Additional_services_to_be_provided__c, ADSL_CRD__c, Alternative_Routing__c, Bus_Con_Option__c, 
                    Circuit_Number__c, Contract_term__c, Conversions__c, Customer_accepts_if_6_digits_provided__c, Customer_Contact__c, Customer_Contact_Number__c, 
                    Customer_Name_DASS2__c, Customer_Name__c, Cust_Provide_Power_from_Switch__c, DASS_2__c, DDI_Range_or_Single_numbers_1__c, DDI_Range_or_Single_numbers_2__c,
                    DDI_Range_or_Single_numbers_3__c, DDI_Range_or_Single_numbers_4__c, DDI_Range_or_Single_numbers_5__c, DDI_Range_or_Single_numbers_10__c, 
                    DDI_Range_or_Single_numbers_6__c, DDI_Range_or_Single_numbers_7__c, DDI_Range_or_Single_numbers_8__c, DDI_Range_or_Single_numbers_9__c, 
                    Directory_Entry__c, Directory_No_digits_to_forward_by_n_w__c, Directory_No_digits_to_forward_to_switch__c, Discount_Plan__c, Diverse_Routing__c, 
                    Divert_to_A__c,Dual_Parenting__c, Floor_No_where_NTE_to_be_fitted__c, From_ISDN_1__c, From_ISDN_2__c,Divert_to_B__c,Divert_to_C__c,  
                    Is_a_post_commission_test_required__c, ISDN_30e__c, Is_this_a_new_customer_to_BT__c, Itemised_Billing__c, Main_Bearer_cct__c, 
                    Main_or_DDI_Number__c, Maintenance_Type__c, No_of_DDI_s__c, Notes__c, NOU_contact_details__c, Number_of_Channels_if_applicable1__c, Number_of_Channels_if_applicable2__c,  Number_Porting__c, One_Bill__c, Option1_ACCD__c, Option2__c, 
                    Order_type__c, Order_Type_2__c, Other_Associated_ISDN_30_Circuit_Numbers__c, Out_of_Hours_1__c, Out_of_Hours_2__c, Range_1_1__c, Range_1_2__c, 
                    Range_2_1__c, Range_2_2__c, Range_3_1__c, Range_3_2__c, Range_4_1__c, Range_4_2__c, Related_to_Flow_CRF__c, Room_No_where_NTE_to_be_fitted__c, 
                    Service_Centre__c, Service_Type__c, Serving_Tel_Exch__c, Site_Assurance__c, Site_Contact__c, Site_Contact_Tel_No__c, Site_Name__c, 
                    Special_Requirements__c, Standby__c, Standby_Bearer_cct__c, Standby_Power__c, Supplementary_services_required__c, Switch_Supplier__c, 
                    Telephone_Number__c, Telephone_Number_or_DDI_range__c,Tel_No__c, Tel_no_A_c_no__c,To_ISDN_1__c, To_ISDN_2__c,Total_Channels__c,   
                    Total_No_of_DDI_Channels_required__c, Total_No_of_non_DDI_Channels_required__c,Transmission_Medium__c,Type_of_Forwarding_1__c,  
                    Type_of_Forwarding_2__c, Type_of_ISPBX__c, VP_Number__c, Which_Discount_Plan__c,Type_of_ISPBX_Known__c,Supplier_of_ISPBX_if_known__c,  
                    DDI_Range_or_Single_numbers__c from ISDN_CRF__c where Related_to_Flow_CRF__c = : crfId limit 1];
                    
        lstProvideAddress = new List<Flow_Address_Details__c>();
        lstCeaseAddress = new List<Flow_Address_Details__c>();
        lstBillingAddress = new List<Flow_Address_Details__c>();

        lstProvideAddress = [SELECT Alternate_Contact_Number__c, Building_Name__c, Contact_Email__c, Contact_Name__c, Contact_Number__c, Country__c, County__c, CreatedById, 
                            CreatedDate, IsDeleted, Name, Flow_CRF__c, LastModifiedById, LastModifiedDate, Locality__c, Number__c, OwnerId, PO_Box__c, Post_Code__c, 
                            Post_Town__c, Preferred_Contact__c, ConnectionReceivedId, Id, ConnectionSentId, Street_2__c, Sub_Building__c, SystemModstamp, Type__c, 
                            street__c FROM Flow_Address_Details__c WHERE Flow_CRF__c=: crfId AND Type__c = 'Provide Address'];
                            
        lstCeaseAddress = [SELECT Alternate_Contact_Number__c, Building_Name__c, Contact_Email__c, Contact_Name__c, Contact_Number__c, Country__c, County__c, CreatedById, 
                            CreatedDate, IsDeleted, Name, Flow_CRF__c, LastModifiedById, LastModifiedDate, Locality__c, Number__c, OwnerId, PO_Box__c, Post_Code__c, 
                            Post_Town__c, Preferred_Contact__c, ConnectionReceivedId, Id, ConnectionSentId, Street_2__c, Sub_Building__c, SystemModstamp, Type__c, 
                            street__c FROM Flow_Address_Details__c WHERE Flow_CRF__c=: crfId AND Type__c = 'Cease Address'];
                            
        lstBillingAddress = [SELECT Alternate_Contact_Number__c, Building_Name__c, Contact_Email__c, Contact_Name__c, Contact_Number__c, Country__c, County__c, CreatedById, 
                            CreatedDate, IsDeleted, Name, Flow_CRF__c, LastModifiedById, LastModifiedDate, Locality__c, Number__c, OwnerId, PO_Box__c, Post_Code__c, 
                            Post_Town__c, Preferred_Contact__c, ConnectionReceivedId, Id, ConnectionSentId, Street_2__c, Sub_Building__c, SystemModstamp, Type__c, 
                            street__c FROM Flow_Address_Details__c WHERE Flow_CRF__c=: crfId AND Type__c = 'Alternate Billing Address'];

          
                    
        if(lstISDN.size() > 0) {
            ISDN = lstISDN[0];
            ShowTextAreaValues();
        }
        
        
    }
    public Integer gettotalchannels(){
        Integer int1;
        Integer int2;
        if(ISDN.Total_No_of_DDI_Channels_required__c != null)
            int1 = Integer.ValueOf(ISDN.Total_No_of_DDI_Channels_required__c);
        else
            int1 = 0;
        if(ISDN.Total_No_of_non_DDI_Channels_required__c != null)
            int2 = Integer.valueOf(ISDN.Total_No_of_non_DDI_Channels_required__c);
        else
            int2 = 0;
        return (int1+int2);
    }
    
    public List<string> TelephoneNumber{get;set;}
    public List<string> WorkingType{get;set;}
    public List<string> convert_to_ISDN{get;set;}
    public List<string> CLIP{get;set;}
    public List<string> CLIR{get;set;}
    public List<string> Divert_all_calls{get;set;}
    public List<string> Divert_on_EF{get;set;}
    public List<string> Divert_no_reply{get;set;}
    public List<string> Order_Codes1{get;set;}
    public List<string> Order_Codes2{get;set;}
    public List<string> Order_Codes3{get;set;}
    
    
    
    public List<SelectOption> getWorkingType() {
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('--None--','--None--'));
    options.add(new SelectOption('B/W','B/W'));
    options.add(new SelectOption('I/C','I/C'));
    options.add(new SelectOption('O/G','O/G'));
    return options;
    }
    
    public void ShowTextAreaValues(){
    
    TelephoneNumber = new List<string>();
    WorkingType = new List<string>();
    convert_to_ISDN = new List<string>();
    CLIP = new List<string>();
    CLIR = new List<string>();
    Divert_all_calls = new List<string>();
    Divert_on_EF = new List<string>();
    Divert_no_reply = new List<string>();
    Order_Codes1 = new List<string>();
    Order_Codes2 = new List<string>();
    Order_Codes3  = new List<string>();
    
    
    
    if(FlowCRF.size() > 0){    
    TelephoneNumber = FlowCRF[0].Telephone_Numbers_DDI_range__c.split(';');
    WorkingType = FlowCRF[0].Working_Type__c.split(';');  
    
    
    convert_to_ISDN = FlowCRF[0].Exist_No_to_convert_to_ISDN__c.split(';'); 
    CLIP  =  FlowCRF[0].CLIP_A18025__c.split(';'); 
    CLIR  = FlowCRF[0].CLIR_A18026__c.split(';');    
    Divert_on_EF = FlowCRF[0].Divert_on_E_F_A18017__c.split(';');
    Divert_all_calls = FlowCRF[0].Divert_all_calls_A18016__c.split(';');
    Divert_no_reply =  FlowCRF[0].Divert_no_reply_A18018__c.split(';');
    Order_Codes1 = FlowCRF[0].Other_CSS_Order_Codes_1__c.split(';');
    Order_Codes2 = FlowCRF[0].Other_CSS_Order_Codes_2__c.split(';');
    Order_Codes3 = FlowCRF[0].Other_CSS_Order_Codes_3__c.split(';');
    
    
    } 
     
    ISDNListWrapper =  new List<ISDNWrapper>();
        integer i = 0;
        
        Map<Integer,String> Tel = new Map<Integer,String>();
        Map<Integer,String> Work_T = new Map<Integer,String>();
        Map<Integer,String> Convert_T = new Map<Integer,String>();
        Map<Integer,String> CL_IP = new Map<Integer,String>();
        Map<Integer,String> CL_IR = new Map<Integer,String>();        
        Map<Integer,String> Divert_E = new Map<Integer,String>();
        Map<Integer,String> Divert_C = new Map<Integer,String>();
        Map<Integer,String> Divert_R = new Map<Integer,String>();
        Map<Integer,String> Order1 = new Map<Integer,String>();
        Map<Integer,String> Order2 = new Map<Integer,String>();
        Map<Integer,String> Order3  = new Map<Integer,String>();
        
        
        for(String S : TelephoneNumber ){                       
        Tel.put(i,S);  
        i++;  
       }
       i = 0;
       for(String W : WorkingType ){
       
        Work_T.put(i,W);        
        i++;    
       }
       i = 0;
       for(String C : convert_to_ISDN){
       
        Convert_T.put(i,C);        
        i++;    
       }
       i=0;
       for(String CL : CLIP  ){
       
        CL_IP.put(i,CL);        
        i++;    
       }
       i=0;
       for(String CR : CLIR){
       
        CL_IR.put(i,CR );        
        i++;    
       }
       i=0;
       for(String W : Divert_on_EF){
       
        Divert_E.put(i,W);        
        i++;    
       }
       i=0;
       for(String W : Divert_all_calls){
       
        Divert_C.put(i,W);        
        i++;    
       }
       i=0;
       for(String W : Divert_no_reply){
       
        Divert_R.put(i,W);        
        i++;    
       }
       i=0;
       for(String W : Order_Codes1){
       
        Order1.put(i,W);        
        i++;    
       }
       i=0;
       for(String W : Order_Codes2){
       
        Order2.put(i,W);        
        i++;    
       }
       i=0;
       for(String W : Order_Codes3){
       
        Order3.put(i,W);        
        i++;    
       }
       
       
       integer Serial_No = 1;
       for(integer j=0;j<=29;j++){
            ISDNListWrapper.add(new ISDNWrapper(Tel.get(j),Work_T.get(j),Convert_T.get(j),CL_IP.get(j),CL_IR.get(j),Divert_E.get(j),Divert_C.get(j),Divert_R.get(j),Order1.get(j),Order2.get(j),Order3.get(j),Serial_No));
            Serial_No ++; 
            
        }
       
          // ISDN Option1  Inner Class 
         
         List<string>  Op1L_Telephone_number = new List<string>();
         List<string>  Op1L_DivertA = new List<string>();
         List<string>  Op1L_DivertB = new List<string>();
         List<string>  Op1L_DivertC = new List<string>();
         
         Map<Integer,String> Op1Map_Telephone_number = new Map<Integer,String>();
         Map<Integer,String> Op1Map_DivertA = new Map<Integer,String>();
         Map<Integer,String> Op1Map_DivertB = new Map<Integer,String>();
         Map<Integer,String> Op1Map_DivertC = new Map<Integer,String>(); 
                 
         if(LstISDN.size() >0){
            if(LstISDN[0].Telephone_number__c != '' && LstISDN[0].Telephone_number__c != null)   
            Op1L_Telephone_number = LstISDN[0].Telephone_number__c.split(';');
            
            if(LstISDN[0].Divert_to_A__c != '' && LstISDN[0].Divert_to_A__c != null)   
            Op1L_DivertA = LstISDN[0].Divert_to_A__c.split(';');
            
            if(LstISDN[0].Divert_to_B__c != '' && LstISDN[0].Divert_to_B__c != null)   
            Op1L_DivertB = LstISDN[0].Divert_to_B__c.split(';');
            
            if(LstISDN[0].Divert_to_C__c != '' && LstISDN[0].Divert_to_C__c != null)   
            Op1L_DivertC = LstISDN[0].Divert_to_C__c.split(';'); 
             
         }    
             i = 0;
             
             for(String Op1Tel : Op1L_Telephone_number){ 
             if(Op1Tel  == '@')
             Op1Map_Telephone_number.put(i,'');
             else                      
             Op1Map_Telephone_number.put(i,Op1Tel );  
             i++;  
           }
           i = 0;
           for(String OpDivA : Op1L_DivertA){ 
             if(OpDivA == '@')
             Op1Map_DivertA.put(i,'');
             else                      
             Op1Map_DivertA.put(i,OpDivA );  
             i++;  
           }
           i = 0;
           for(String OpDivB : Op1L_DivertB){ 
             if(OpDivB == '@')
             Op1Map_DivertB.put(i,'');
             else                      
             Op1Map_DivertB.put(i,OpDivB);  
             i++;  
           }
           i = 0;
           for(String OpDivC : Op1L_DivertC){ 
             if(OpDivC == '@')
             Op1Map_DivertC.put(i,'');
             else                      
             Op1Map_DivertC.put(i,OpDivC);  
             i++;  
           } 
              
         
         
      
        ISDNList_Option1_ACCD_Wrapper = new List<ISDN_Option1_ACCD_Wrapper>();
        
        for(integer jj=0;jj<=5;jj++){
            
            system.debug('Op1Map_DivertB.get(jj) ----------------'+Op1Map_DivertB.get(jj)+'YYYYYYYYYYYYYYYYYY'+Op1Map_DivertC.get(jj));
            
            ISDNList_Option1_ACCD_Wrapper.add(new ISDN_Option1_ACCD_Wrapper(Op1Map_Telephone_number.get(jj),Op1Map_DivertA.get(jj),Op1Map_DivertB.get(jj),Op1Map_DivertC.get(jj)));                    
        
            system.debug('ISDNList_Option1_ACCD_Wrapper **************'+ISDNList_Option1_ACCD_Wrapper);
        }
        
        
        List<string>  Op2L_MainBearer = new List<string>();
         List<string>  Op2L_NumberOfChannels = new List<string>();
         List<string>  Op2L_TelNumberDDI = new List<string>();
         List<string>  Op2L_StandbyBearer = new List<string>();
         List<string>  Op2L_NumberOfChannels2 = new List<string>();
         
         Map<Integer,String> Op2Map_MainBearer = new Map<Integer,String>();
         Map<Integer,String> Op2Map_NumberOfChannels1 = new Map<Integer,String>();
         Map<Integer,String> Op2Map_TelNumberDDI = new Map<Integer,String>();
         Map<Integer,String> Op2Map_StandbyBearer = new Map<Integer,String>();
         Map<Integer,String> Op2Map_NumberOfChannels2 = new Map<Integer,String>(); 
                 
         if(LstISDN.size() >0){
            if(LstISDN[0].Main_Bearer_cct__c != '' && LstISDN[0].Main_Bearer_cct__c != null)   
            Op2L_MainBearer = LstISDN[0].Main_Bearer_cct__c.split(';');
            
            if(LstISDN[0].Number_of_Channels_if_applicable1__c != '' && LstISDN[0].Number_of_Channels_if_applicable1__c != null)   
            Op2L_NumberOfChannels = LstISDN[0].Number_of_Channels_if_applicable1__c.split(';');

            if(LstISDN[0].Telephone_Number_or_DDI_range__c != '' && LstISDN[0].Telephone_Number_or_DDI_range__c != null)   
            Op2L_TelNumberDDI = LstISDN[0].Telephone_Number_or_DDI_range__c.split(';'); 
            
            if(LstISDN[0].Standby_Bearer_cct__c != '' && LstISDN[0].Standby_Bearer_cct__c != null)   
            Op2L_StandbyBearer = LstISDN[0].Standby_Bearer_cct__c.split(';');
            
            if(LstISDN[0].Number_of_Channels_if_applicable2__c != '' && LstISDN[0].Number_of_Channels_if_applicable2__c != null)   
            Op2L_NumberOfChannels2 = LstISDN[0].Number_of_Channels_if_applicable2__c.split(';'); 
             
             
             i = 0;
             
             for(String Op1MB : Op2L_MainBearer){ 
             if(Op1MB == '@')
             Op2Map_MainBearer.put(i,'');
             else                      
             Op2Map_MainBearer.put(i,Op1MB );  
             i++;  
           }
           i = 0;
           for(String OpNoC1 : Op2L_NumberOfChannels){ 
             if(OpNoC1 == '@')
             Op2Map_NumberOfChannels1.put(i,'');
             else                      
             Op2Map_NumberOfChannels1.put(i,OpNoC1 );  
             i++;  
           }
           i = 0;
           for(String OpTelDDI : Op2L_TelNumberDDI){ 
             if(OpTelDDI == '@')
             Op2Map_TelNumberDDI.put(i,'');
             else                      
             Op2Map_TelNumberDDI.put(i,OpTelDDI );  
             i++;  
           }
           i = 0;
           for(String OpSB : Op2L_StandbyBearer){ 
             if(OpSB == '@')
             Op2Map_StandbyBearer.put(i,'');
             else                      
             Op2Map_StandbyBearer.put(i,OpSB );  
             i++;  
           } 
           i = 0;
           for(String OpNOC2 : Op2L_NumberOfChannels2){ 
             if(OpNOC2 == '@')
             Op2Map_NumberOfChannels2.put(i,'');
             else                      
             Op2Map_NumberOfChannels2.put(i,OpNOC2 );  
             i++;  
           }
              
         }
         
         
         ISDNList_Option2_ACCD_Wrapper = new List<ISDN_Option2_ACCD_Wrapper>();
         for(integer MM=0;MM<=9;MM++){
            ISDNList_Option2_ACCD_Wrapper.add(new ISDN_Option2_ACCD_Wrapper(Op2Map_MainBearer.get(MM),Op2Map_NumberOfChannels1.get(MM),Op2Map_TelNumberDDI.get(MM),Op2Map_StandbyBearer.get(MM),Op2Map_NumberOfChannels2.get(MM)));                    
        }
        
        
        ISDN30eList = new List<ISDN_30e>();
        List<string>  ISDN30eDDIList  = new List<string>();
        Map<Integer,String> ISDN30eDDIMap   = new Map<Integer,String>();
        
        if(lstISDN.size() >0){
            if(lstISDN[0].DDI_Range_or_Single_numbers__c != '' && lstISDN[0].DDI_Range_or_Single_numbers__c != null)   
            ISDN30eDDIList = lstISDN[0].DDI_Range_or_Single_numbers__c.split(';');
            
            i = 0;
             
             for(String TelDDI: ISDN30eDDIList){ 
             if(TelDDI == '@')
             ISDN30eDDIMap.put(i,'');
             else                      
             ISDN30eDDIMap.put(i,TelDDI);  
             i++;  
           }
            
        }
        
        
         ISDN30eList = new List<ISDN_30e>();
         for(integer MM=0;MM<=4;MM++){
         ISDN30eList.add(new ISDN_30e(ISDN30eDDIMap.get(MM)));
         }
    
    }
    
    public List<ISDNWrapper> ISDNListWrapper{get;set;}
    public Class ISDNWrapper{
    
        public List<String>  Telephone_Number{get;set;}
        public List<String>  WorkingType {get;set;}
        public String  Telephone{get;set;}
        public String  Work{get;set;}
        public String convert_to_ISDN{get;set;}
        public String CLIP{get;set;}
        public String CLIR{get;set;}
        public String Divert_all_calls {get;set;}
        public String Divert_on_EF{get;set;}
        public string Divert_no_reply{get;set;}
        public string Order_Codes1{get;set;}
        public string Order_Codes2{get;set;}
        public string Order_Codes3{get;set;}
        public integer SerialNo{get;set;}
        
        public ISDNWrapper (String T,string W,String CTI, string CLP,String CLR,string DAC,string DOE,string DNR,string OC1,string OC2,string OC3,integer S_No){
            Telephone = T;
            Work = W;
            convert_to_ISDN = CTI;
            CLIP = CLP;
            CLIR = CLR;
            Divert_all_calls = DAC;
            Divert_on_EF = DOE;
            Divert_no_reply = DNR;
            Order_Codes1 = OC1;
            Order_Codes2=OC2;
            Order_Codes3 = OC3;
            SerialNo = S_No;
            
            
            
            
        }    
    }
    public List<ISDN_Option1_ACCD_Wrapper> ISDNList_Option1_ACCD_Wrapper{get;set;}
    public class ISDN_Option1_ACCD_Wrapper{

        public String Op1Telephonenumber{get;set;}
        public String Op1DivertA{get;set;}
        public String Op1DivertB{get;set;}
        public String Op1DivertC{get;set;}
        
        
        public ISDN_Option1_ACCD_Wrapper(String Op1T,string OP1DA,String OP1DB, string OP1DC){

           Op1Telephonenumber = Op1T;
           Op1DivertA = OP1DA;
           Op1DivertB = OP1DB;
           Op1DivertC= OP1DC; 
                   
        }
    }
    
    public List<ISDN_Option2_ACCD_Wrapper> ISDNList_Option2_ACCD_Wrapper{get;set;}
    public class ISDN_Option2_ACCD_Wrapper{

        public String Op2MAinBearer{get;set;}
        public String Op2NumberOfChannels1{get;set;}
        public String Op2TelNumberDDI{get;set;}
        public String Op2StandbyBearer{get;set;}
        public String Op2NumberOfChannels2{get;set;}
        
        
        public ISDN_Option2_ACCD_Wrapper(String Op2MB,string OP2NOC,String OP2TDDI, string OP2SB,string Op2NOC2){

           Op2MAinBearer= Op2MB;
           Op2NumberOfChannels1= OP2NOC;
           Op2TelNumberDDI= OP2TDDI;
           Op2StandbyBearer= OP2SB; 
           Op2NumberOfChannels2=Op2NOC2;
                   
        }
    }
    
        public List<ISDN_30e> ISDN30eList{get;set;}
        public class ISDN_30e{
    
            public String Tel_DDI1{get;set;}                
            
            public ISDN_30e(String DDI1){
    
               Tel_DDI1= DDI1;
               
              
               
                       
            }
        }
    
     public PageReference OnLoad(){
        System.debug('Entered On Load-->');
        System.debug('crfId-->'+crfId);
        CRFRTId = [Select RecordTypeId from Flow_CRF__c where Id=:crfId].RecordTypeId;
        RecordTypeName = [Select DeveloperName from RecordType where Id=:CRFRTId].DeveloperName;
        System.debug('RecordTypeName-->'+RecordTypeName );
        if(RecordTypeName!='ISDN30'){
            PageReference RD = new PageReference('/'+crfId+'?nooverride=1');
            RD.setRedirect(true);
            System.debug('crfIdInside-->'+crfId);
            System.debug('URL-->'+RD);
            return RD;
        }
        return null;
    }
    
    
    public PageReference EditPage(){
        PageReference crfPage = new PageReference('/apex/ISDN_FlowCRF_Edit?id='+crfId);
        return crfPage;
    }
   
}