global with sharing class CustomButtonUnlock extends csbb.CustomButtonExt {

    public String performAction (String basketId) {
        
        String newUrl = CustomButtonUnlock.UnlockBasket(basketId);
        
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    } 
    
    public static String UnlockBasket(String basketId){
    
        cscfga__Product_Basket__c pb = Database.query('SELECT Id, Name, isBasketReadonly__c FROM cscfga__Product_Basket__c WHERE Id=\'' + basketId + '\'');
		String status = '';
        
        if(pb != null){
            pb.isBasketReadonly__c = false;
            pb.csbb__Accessibility_Status__c = '';
            pb.BT_Basket_Approval_Status__c = '';
            update pb;
            status = '&basketUnlocked';
        }
        
        return 'apex/BasketbuilderApp?id=' + basketId + status;
        
    }
}