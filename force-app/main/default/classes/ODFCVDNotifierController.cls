public class ODFCVDNotifierController {
    
    public String caseId {get; set;}
    private Case c {get; set;}
    public String mailBody {get; set;}
    
    public ODFCVDNotifierController() {
        caseId = '';        
        mailBody = '';
    }
    
    public void getNotificationText() {
        system.debug('Case ID :' + caseId);
        if (caseId != null){
            c = [Select c.Id,
                        c.Type, 
                        c.Subject,
                        c.Status,
                        c.Response__c,
                        c.RecordTypeId, 
                        c.Reason,
                        c.OwnerId, 
                        c.Other_Reason__c, 
                        c.Other_Reason_2__c, 
                        c.Other_Reason_1__c, 
                        c.Origin,
                        c.transfer_count__c, 
                        c.Front_Office_User_on_CVD_Feedback__c, 
                        c.Front_Office_User__c, 
                        c.Front_Office_Manager__c, 
                        c.Front_Office_Escalation_Manager__c, 
                        c.Front_Office_EIN__c, 
                        c.Front_Office_Division__c, 
                        c.Front_Office_Department__c,
                        c.Escalation_Stage__c, 
                        c.Escalate_FO_3_Manager__c, 
                        c.Escalate_FO_3_Manager_Name__c, 
                        c.Escalate_FO_2_Manager__c, 
                        c.Escalate_FO_2_Manager_Name__c, 
                        c.Escalate_FO_1_Manager__c, 
                        c.Escalate_FO_1_Manager_Name__c, 
                        c.Escalate_BO_3_Manager__c, 
                        c.Escalate_BO_3_Manager_Name__c, 
                        c.Escalate_BO_2_Manager__c, 
                        c.Escalate_BO_2_Manager_Name__c, 
                        c.Escalate_BO_1_Manager__c, 
                        c.Escalate_BO_1_Manager_Name__c,
                        c.emlEscalationTransfer__c,
                        c.emlMailSubject__c,  
                        c.Description, 
                        c.Createdby_Manager__c,
                        c.Closure_Reason__c, 
                        c.Closing_Action__c, 
                        c.ClosedDate, 
                        c.Case_Age_In_Business_Hours__c, 
                        c.CaseNumber, 
                        c.CVD_Feedback__c, 
                        c.Back_Office_Escalation_Manager__c, 
                        c.BTB_Order__c, 
                        c.AccountId From Case c WHERE c.Id =: caseId LIMIT 1];
                        
            //RecordType rt_ODF = [select id from RecordType where SobjectType='Case' and name ='Order Discrepancy' limit 1];
            RecordType rt_CVD = [select id from RecordType where SobjectType='Case' and name ='Credit Vet' limit 1];
            RecordType rt_KCI = [select id from RecordType where SobjectType='Case' and name ='KCI Discrepancy' limit 1];
                        
            List<User> OwnerID = [Select id, Manager_Name__c , Department, Name from User where id = :c.ownerid Limit 1];
            
            if((Test.isRunningTest()) || (c.transfer_count__c == 3 && c.status == 'Assigned to Front Office' && c.emlEscalationTransfer__c == true)) {
                if(c.RecordTypeId == rt_CVD.Id){
                    mailBody = 'The following CVD has been escalated to you for action due to excessive transfers between Front & Back Office <BR>' + c.CaseNumber + ' :  '+ c.Subject  +
                     ' <br> which can be accessed at https://btbusiness.my.salesforce.com/'+ c.id;
                     system.debug(mailBody);
                }
                else if(c.RecordTypeId == rt_KCI.Id){
                    mailBody = 'The following KCI has been escalated to you for action due to excessive transfers between Front & Back Office <BR>' + c.CaseNumber + ' :  '+ c.Subject  +
                     ' <br> which can be accessed at https://btbusiness.my.salesforce.com/'+ c.id;
                     system.debug(mailBody);
                }
                else{
                    mailBody = 'The following ODF has been escalated to you for action due to excessive transfers between Front & Back Office <BR>' + c.CaseNumber + ' :  '+ c.Subject  +
                     ' <br> which can be accessed at https://btbusiness.my.salesforce.com/'+ c.id;
                     system.debug(mailBody);
                }
            }
            else {
                if(c.RecordTypeId == rt_CVD.Id){
                    mailBody = 'The following CVD has been escalated to you for action <BR>' + c.CaseNumber + ' :  '+ c.Subject  +
                    '<br> which can be accessed at https://btbusiness.my.salesforce.com/'+ c.id +
                    '<br> Agents Name ' + ownerID[0].name +
                    '<br> Managers Name : ' + ownerID[0].Manager_Name__c +
                    '<br> Department: ' + ownerID[0].Department ;
                    system.debug(mailBody);
                }
                else if(c.RecordTypeId == rt_KCI.Id){
                    mailBody = 'The following KCI has been escalated to you for action <BR>' + c.CaseNumber + ' :  '+ c.Subject  +
                    '<br> which can be accessed at https://btbusiness.my.salesforce.com/'+ c.id +
                    '<br> Agents Name ' + ownerID[0].name +
                    '<br> Managers Name : ' + ownerID[0].Manager_Name__c +
                    '<br> Department: ' + ownerID[0].Department ;
                    system.debug(mailBody);    
                 }
                 else{
                    mailBody = 'The following ODF has been escalated to you for action <BR>' + c.CaseNumber + ' :  '+ c.Subject  +
                    ' <br> which can be accessed at https://btbusiness.my.salesforce.com/'+ c.id +
                    ' <br> Agents Name ' + ownerID[0].name +
                    '<br> Managers Name : ' + ownerID[0].Manager_Name__c +
                    '<br> Department: ' + ownerID[0].Department ;
                    system.debug(mailBody);
                }
            }
            system.debug(mailBody);
        }           
    }
}