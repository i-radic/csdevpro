global class BatchLS2L_06ZeroLeads implements Database.Batchable<sobject>{
/*
###################################################################################################
To calculate the number of leads created by owner by a user and post chatter message or email
account owners from being made inactive automatically.

24/01/12    John McGovern    Intial Build

###################################################################################################
*/

public String query;
public Set<id> uIds = new Set<id>();
global Map<Id,Decimal> umap = new Map<Id, Decimal>();
List<User> users = new List<User>();

global BatchLS2L_06ZeroLeads() {
    
}

global database.querylocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    //set leads to zero 
    List<User> users = new List<User>();      
    for(sObject s : scope) {
    User u = (User)s;
        u.zLeadsOwned__c = 0;
        users.add(u);
    }
    StaticVariables.setUserDontRun(true);
    update users;    

}

global void finish(Database.BatchableContext BC){  
    //abort batch
    BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
    system.debug('jmm getOrgDefaults :'+BatchSchedule__c.getOrgDefaults());
    //List<BatchSchedule__c> blist = [SELECT scheduled_id__c, scheduled_id2__c, scheduled_id3__c FROM BatchSchedule__c];
    //system.debug('jmm getOrgDefaults :'+blist );
    try{
        if (b.scheduled_id2__c != '' && b.scheduled_id2__c != null){
            system.abortJob(b.scheduled_id2__c);
        }
    } catch (Exception e){
        // let it go b/c the job isn't scheduled
    }    
    try{
        if (b.scheduled_id3__c != '' && b.scheduled_id3__c != null){
            system.abortJob(b.scheduled_id3__c);
        }
    } catch (Exception e){
        // let it go b/c the job isn't scheduled
    }    
    try{
        if (b.scheduled_id4__c != '' && b.scheduled_id4__c != null){
            system.abortJob(b.scheduled_id4__c);
        }
    } catch (Exception e){
        // let it go b/c the job isn't scheduled
    }    
    try{
        if (b.scheduled_id5__c != '' && b.scheduled_id5__c != null){
            system.abortJob(b.scheduled_id5__c);
        }
    } catch (Exception e){
        // let it go b/c the job isn't scheduled
    }    
    try{
        if (b.scheduled_id6__c != '' && b.scheduled_id6__c != null){
            system.abortJob(b.scheduled_id6__c);
        }
    } catch (Exception e){
        // let it go b/c the job isn't scheduled
    }    
}

}