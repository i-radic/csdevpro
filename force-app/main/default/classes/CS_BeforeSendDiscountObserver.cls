global class CS_BeforeSendDiscountObserver implements csdiscounts.IDiscountConfigurationResultObserver {
	public CS_BeforeSendDiscountObserver () {
	}

	public csdiscounts.ConfigurationQueryResult execute(Id basketId, csdiscounts.ConfigurationQueryResult configurationQueryResult) {

		if (CS_ProductBasketService.containsCalculateProductGroup(basketId)) {
			return configurationQueryResult;
		}

		//mpk CR12610
		System.debug('mpk CS_BeforeSendDiscountObserver class');
		//System.debug('mpk configurationQueryResult  '+ configurationQueryResult);
		//System.debug('mpk ConfigurationObjectDetails details: '+ configurationQueryResult.objectDetails);

		try{
			for(csdiscounts.ConfigurationQueryResult.ConfigurationObjectDetails obj : configurationQueryResult.objectDetails) {
				System.debug('********'+configurationQueryResult.objectDetails);
				Integer volume = 1;
				if(obj.discounts != null) {
					if(obj.objectFields != null) {
						for(csdiscounts.FieldDetails fd : obj.objectFields) {
							system.debug('fd- '+fd.name+' = '+fd.value);
							if(fd.name == 'Volume__c' && String.isNotBlank(fd.value)) {
								//system.debug('fd- '+fd.name+' = '+fd.value);
								volume = Integer.valueOf(fd.value);
							}
						}
					}
					for(csdiscounts.ConfigurationQueryResult.Discount discount : obj.discounts) {
						/* mpk CR12610
                          if(discount.type == 'absolute' && discount.amount != null) {
                            discount.amount = discount.amount / volume;
                            }*/
						for(csdiscounts.ConfigurationQueryResult.Discount memDiscount : discount.memberDiscounts) {
							System.debug('mpk memDiscount:' + memDiscount);
							System.debug('mpk memDiscount.amount BEFORE:' + memDiscount.amount);
							if(memDiscount.type == 'absolute' && memDiscount.amount != null) {
								memDiscount.amount = (memDiscount.amount / volume);
							}
							System.debug('mpk memDiscount.amount AFTER:' + memDiscount.amount);
						}
					}
				}
			}
		}
		catch(Exception ex)
		{
			System.Debug('DiscountLoadObserver.ConfigurationQueryResult Exception occurred' + ex.getMessage());
			return null;
		}

		return configurationQueryResult;
	}
}