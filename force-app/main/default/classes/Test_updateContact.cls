@isTest
private class Test_updateContact {

     static testMethod void runPositiveTestCases() {
        Account A = new Account(); 
         A.Name = 'test';
         insert A;
         
        Contact contact = new Contact();  
        contact.Contact_Address_Street__c = '';
        contact.Address_County__c = '';
        contact.Contact_Post_Code__c = '';
        contact.Address_Country__c = '';
        contact.Contact_Post_Town__c = '';                
        contact.Contact_Address_Street__c = '';
        contact.Address_County__c = '';
        contact.Contact_Post_Code__c = '';
        contact.Address_Country__c = '';
        contact.Contact_Post_Town__c = '';       
        contact.Active__c = '';
        contact.Comment__c = '';
        contact.Type__c = '';
        contact.Salutation = '';
        contact.FirstName = '';
        contact.MiddleName__c = '';
        contact.LastName = '';
        contact.AliasName__c = '';
        contact.ContactSource__c = '';
        contact.JobSeniority__c = '';
        contact.Profession__c = '';
        contact.Birthdate = null;
        contact.MotherMaidenName__c = '';
        contact.Status__c = 'InActive';
        contact.Job_Title__c = '';
        contact.Key_Decision_Maker__c = '';
        contact.Fax = '';
        contact.Organization__c = '';
        contact.Department = '';
        contact.Preferred_Contact_Channel__c = '';
        contact.Secondary_Preferred_Contact_Channel__c = '';
        contact.Email_Id__c = '';       
        contact.Email_Id__c = '';
        contact.Email = 'test@bt.com';
        contact.Email_2nd__c = 'test@bt.com';
        contact.Email_3rd__c = 'test@bt.com'; 
        contact.Email_Consent__c = '';
        contact.Email_Consent_Date__c = null;
        contact.Mobile_Id__c = '';
        contact.MobilePhone = '';
        contact.Mobile_Consent__c = 'Not Asked';
        contact.Mobile_Consent_Date__c = null;    
        contact.Phone_Id__c = '';
        contact.Phone = '3';
        contact.Phone_Consent__c = '';
        contact.Phone_Consent_Date__c = null;
        contact.Fax_Id__c = '';
        contact.Fax = '';
        contact.Fax_Consent__c = '';
        contact.Fax_Consent_Date__c = null;
        contact.Contact_Address_Number__c = '';
        contact.Address_Id__c = '0000';
        contact.Contact_Address_Street__c = '';
        contact.Contact_Locality__c = '';
        contact.Contact_Locality__c = '';
        contact.Address_County__c = '';
        contact.Address_Country__c = '';
        contact.Contact_Post_Code__c = '';
        contact.Contact_Post_Town__c = '';
        contact.Contact_Sector__c = '';
        contact.Address_Name__c = '';
        contact.Contact_Zip_Code__c = '';
        contact.Address_Consent__c = '';
        contact.Address_Consent_Date__c = null;  
        contact.Do_Not_Survey_Customer_Request__c=true;
        contact.Voice2__c = '4';
        contact.Voice3__c = '5'; 
         
        ApexPages.StandardController sc = new ApexPages.StandardController(contact);
        updateContact updateCont = new updateContact(sc);
        updateContact updateCont2 = new updateContact(contact); 
         
        Test_Factory.SetProperty('IsTest', 'Yes');  
        updateCont.contact.Contact_Address_Street__c = '';
        updateCont.contact.Address_County__c = '';
        updateCont.contact.AccountID = A.id;
        updateCont.contact.Contact_Post_Code__c = '';
        updateCont.contact.Address_Country__c = '';
        updateCont.contact.Contact_Post_Town__c = '';                
        updateCont.contact.Contact_Address_Street__c = '';
        updateCont.contact.Address_County__c = '';
        updateCont.contact.Contact_Post_Code__c = '';
        updateCont.contact.Address_Country__c = '';
        updateCont.contact.Contact_Post_Town__c = '';       
        updateCont.contact.Active__c = '';
        updateCont.contact.Comment__c = '';
        updateCont.contact.Type__c = '';
        updateCont.contact.Salutation = '';
        updateCont.contact.FirstName = '';
        updateCont.contact.MiddleName__c = '';
        updateCont.contact.LastName = '';
        updateCont.contact.AliasName__c = '';
        updateCont.contact.ContactSource__c = '';
        updateCont.contact.JobSeniority__c = '';
        updateCont.contact.Profession__c = '';
        updateCont.contact.Birthdate = Date.today()-1;
        updateCont.contact.MotherMaidenName__c = '';
        updateCont.contact.Status__c = 'Active';
        updateCont.contact.Job_Title__c = '';
        updateCont.contact.Key_Decision_Maker__c = '';
        updateCont.contact.Fax = '';
        updateCont.contact.Organization__c = '';
        updateCont.contact.Department = '';
        updateCont.contact.Preferred_Contact_Channel__c = 'Email';
        updateCont.contact.Secondary_Preferred_Contact_Channel__c = '';
        updateCont.contact.Email_Id__c = '';       
        updateCont.contact.Email = 'DoNotContact@btco.uk';
        updateCont.contact.Email_Consent__c = 'Yes';
        updateCont.contact.Email_2nd_Id__c = '';       
        updateCont.contact.Email_2nd__c = 'DoNotContact@btco.uk';
        updateCont.contact.Email_3rd_Id__c = '';       
        updateCont.contact.Email_3rd__c = 'DoNotContact@btco.uk';
        updateCont.contact.Email_Consent_Date__c = null;
        updateCont.contact.Mobile_Id__c = '';
        updateCont.contact.MobilePhone = '';
        updateCont.contact.Mobile_Consent__c = 'No';
        updateCont.contact.Mobile_Consent_Date__c = null;    
        updateCont.contact.Phone_Id__c = '';
        updateCont.contact.Phone = '3';
        updateCont.contact.Phone_Consent__c = '';
        updateCont.contact.Phone_Consent_Date__c = null;        
        updateCont.contact.Fax_Id__c = '';
        updateCont.contact.Fax = '';
        updateCont.contact.Fax_Consent__c = '';
        updateCont.contact.Fax_Consent_Date__c = null;
        updateCont.contact.Contact_Address_Number__c = '';
        updateCont.contact.Address_Id__c = '0000';
        updateCont.contact.Contact_Address_Street__c = '';
        updateCont.contact.Contact_Locality__c = '';
        updateCont.contact.Contact_Locality__c = '';
        updateCont.contact.Address_County__c = '';
        updateCont.contact.Address_Country__c = '';
        updateCont.contact.Contact_Post_Code__c = 'DN3 6GB';
        updateCont.contact.Contact_Post_Town__c = '';
        updateCont.contact.Contact_Sector__c = '';
        updateCont.contact.Address_Name__c = '';
        updateCont.contact.Contact_Zip_Code__c = '';
        updateCont.contact.Address_Consent__c = '';
        updateCont.contact.Address_Consent_Date__c = null;
        updateCont.contact.Contact_Status_Flag__c = TRUE;
        updateCont.contact.Do_Not_Survey_Customer_Request__c=true;
        updateCont.contact.Customer_Experience_Client_Contact__c =true;
        updateCont.contact.Customer_Experience_Client_Owner__c = true;
        updateCont.contact.Voice2__c = '4';
        updateCont.contact.Voice3__c = '5'; 
        updateCont.contact.voice_preferred__c = 'Voice 2';
        updateCont.contact.Email_preferred__c = 'Email 3';
        updateCont.contact.Automated_Phone_calls_Consent__c = 'Not Asked'; 
        updateCont.contact.Profiling_Consent__c = 'Not Asked';
        updateCont.contact.Digital_Marketing_Consent__c = 'Not Asked';
        updateCont.crmContactTest(); 
        updateCont.OnLoad();
        updateCont.AddressSearch();
        //updateCont.cancel();
         
    
         
        updateCont.OnLoad('test');
        updateCont.IsDummyEmail('DoNotContact@btco.uk');
        updateCont.ValidateVoice('+44','Mobile');
        updateCont.ValidatePhone('+44');
        updateCont.save();         
        updateCont.contact.voice_preferred__c = 'Voice 3';
        updateCont.contact.Email_preferred__c = 'Email 3';
        updateCont.crmContactTest(); 
        updateCont.OnLoad();
        updateCont.AddressSearch();
        //updateCont.cancel();
         
    
         
        updateCont.OnLoad('test');
        updateCont.IsDummyEmail('DoNotContact@btco.uk');
        updateCont.ValidateVoice('+440000000000','Mobile'); 
        updateCont.ValidatePhone('+440000000000');
        updateCont.save(); 
        
        updateCont.contact.Email = '';
        updateCont.contact.Phone = '';
        updateCont.contact.voice_preferred__c = 'Voice 2';
        updateCont.contact.Email_preferred__c = 'Email 3';
        
        updateCont.save();
        
        updateCont.contact.Email = '';
        updateCont.contact.Phone = '';
        updateCont.contact.voice_preferred__c = 'Voice 3';
        updateCont.contact.Email_preferred__c = 'Email 2';
        updateCont.contact.Preferred_Contact_Channel__c = 'Voice';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = '';
        updateCont.contact.voice2__c ='';
        updateCont.contact.voice_preferred__c = 'Voice 2';
        updateCont.contact.Email_preferred__c = 'Email 3';
		updateCont.contact.Preferred_Contact_Channel__c = 'SMS';        
        updateCont.save(); 
        
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = 'test@test.com';
        updateCont.contact.Email_3rd__c = '';
        updateCont.contact.voice2__c ='';
        updateCont.contact.voice_preferred__c = 'Voice 2';
        updateCont.contact.Email_preferred__c = 'Email 2';
		updateCont.contact.Preferred_Contact_Channel__c = 'Postal';        
        updateCont.save(); 
        
        
        updateCont.contact.voice2__c ='4';
        updateCont.contact.Phone ='03098765409';
        updateCont.contact.Voice3__c ='';
        updateCont.Contact.Voice_Type_3__c = 'Mobile';
        updateCont.contact.voice_preferred__c = 'Voice 2';
               
        updateCont.save(); 
        
        updateCont.contact.voice2__c ='+440897654321';
        updateCont.contact.Phone ='';
        updateCont.Contact.Voice_Type_1__c = 'Mobile';
        updateCont.contact.Voice3__c ='5';
        updateCont.contact.voice_preferred__c = 'Voice 3';
               
        updateCont.save(); 
        
        updateCont.contact.Email_2nd__c = '';
        updateCont.contact.Email = '';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.voice2__c ='';
        updateCont.contact.Phone ='';
        updateCont.Contact.Voice_Type_2__c = 'Mobile';
        updateCont.contact.Voice3__c ='3';
        updateCont.contact.voice_preferred__c = 'Voice 1';                           
        updateCont.save(); 
        
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = '';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.Email_preferred__c = 'Email 2';    
        updateCont.save(); 
         
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = 'test@test.com';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.voice2__c ='1';
        updateCont.contact.Phone ='2';
        updateCont.contact.Voice3__c ='3';
        updateCont.contact.voice_preferred__c = 'Voice 3'; 
        updateCont.contact.Email_preferred__c = 'Email 3';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = 'test@test.com';
        updateCont.contact.Email_3rd__c = '';
        updateCont.contact.voice2__c ='1';
        updateCont.contact.Phone ='2';
        updateCont.contact.Voice3__c ='';
        updateCont.contact.voice_preferred__c = 'Voice 3'; 
        updateCont.contact.Email_preferred__c = 'Email 3';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = '';
        updateCont.contact.Email = 'test@test.com';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.voice2__c ='';
        updateCont.contact.Phone ='2';
        updateCont.contact.Voice3__c ='3';
        updateCont.contact.voice_preferred__c = 'Voice 2'; 
        updateCont.contact.Email_preferred__c = 'Email 2';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = '';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.voice2__c ='2';
        updateCont.contact.Phone ='';
        updateCont.contact.Voice3__c ='3';
        updateCont.contact.voice_preferred__c = 'Voice 1'; 
        updateCont.contact.Email_preferred__c = 'Email 1';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = '';
        updateCont.contact.Email = 'test@test.com';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.voice2__c ='';
        updateCont.contact.Phone ='2';
        updateCont.contact.Voice3__c ='3';
        updateCont.contact.voice_preferred__c = ''; 
        updateCont.contact.Email_preferred__c = '';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = 'test@test.com';
        updateCont.contact.Email_3rd__c = '';
        updateCont.contact.voice2__c ='1';
        updateCont.contact.Phone ='+44(0)2';
        updateCont.contact.Voice3__c ='';
        updateCont.contact.voice_preferred__c = ''; 
        updateCont.contact.Email_preferred__c = '';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = '';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.voice2__c ='+44(0)8';
        updateCont.contact.Phone ='';
        updateCont.contact.Voice3__c ='04';
        updateCont.contact.voice_preferred__c = ''; 
        updateCont.contact.Email_preferred__c = '';
         
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = 'test@test.com';
        updateCont.contact.Email = 'test@test.com';
        updateCont.contact.Email_3rd__c = 'test@test.com';
        updateCont.contact.voice2__c ='1';
        updateCont.contact.Phone ='+44010';
        updateCont.contact.Voice3__c ='2';
        updateCont.contact.voice_preferred__c = ''; 
        updateCont.contact.Email_preferred__c = '';
        updateCont.save();
        
        updateCont.contact.Email_2nd__c = '';
        updateCont.contact.Email = '';
        updateCont.contact.Email_3rd__c = '';
        updateCont.contact.Email_preferred__c = '';
        updateCont.contact.Email_Consent__c = 'Yes';
        updateCont.contact.Preferred_Contact_Channel__c = 'Email';
        updateCont.save();
        
        
        updateCont.contact.voice2__c ='';
        updateCont.contact.Phone ='';
        updateCont.contact.Voice3__c ='';
        updateCont.contact.Preferred_Contact_Channel__c = 'Voice';        
        updateCont.contact.Status__c = '';
        updateCont.contact.Phone_consent__c ='Yes';
        updateCont.contact.Mobile_consent__c ='Yes';
        updateCont.contact.voice_type_1__c ='';
        updateCont.Contact.voice_type_2__c = '';
        updateCont.Contact.voice_type_3__c = ''; 
        updateCont.save();
        
        updateCont.contact.Contact_Post_Code__c ='';
        updateCont.contact.Address_Id__c ='A000';
        updateCont.Contact.Mobile_Consent__c = 'Not Asked';
        updateCont.Contact.status__c = 'InActive';
        updateCont.save();
        
        
        
        
         
        
               
        
         
    }
}