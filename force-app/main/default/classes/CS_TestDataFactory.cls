/*******************************************************************************************************************************************
* @author         Matija Gulic
* @description    This class is providing a set of methods generating test data that can be used in the different test classes
* @date           2017-09-15
* @group          Test Classes
*******************************************************************************************************************************************/

@isTest
public class CS_TestDataFactory
{
    public static Account generateAccount(Boolean executeDML, String accountName){
        Account acct = new Account(Name = accountName);
        if (executeDML)
            insert acct;
        return acct;
    }

    public static Contact generateContact(Boolean executeDML, String contactName) {
        Contact contact = new Contact (SAC_code__c = 'tst_sac', lastname = 'tst_developer',
            FirstName = contactName, Salutation = 'Mr', MobilePhone = '07755648877', Phone = '02555818000');
        if (executeDML)
            insert contact;
        return contact;
    }

    public static Associated_LEs__c generateAssociatedLES(Boolean executeDML, String name, Account acc){
        Associated_LEs__c ales = new Associated_LEs__c(Name = name, Account__c = acc.Id);
        if (executeDML)
            insert ales;
        return ales;
    }

    public static Opportunity generateOpportunity(Boolean executeDML, String oppName, Account acc) {
        Opportunity opp = new Opportunity(Name = oppName, AccountID = acc.ID, StageName = 'Prospecting',
            CloseDate = date.today(), NextStep = 'Test', Next_Action_Date__c = date.today(), TotalOpportunityQuantity = 0);
        if (executeDML)
            insert opp;
        return opp;
    }

    public static cscfga__Product_Basket__c generateProductBasket(Boolean executeDML, String basketName, Opportunity opp){
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(Name = basketName, cscfga__Opportunity__c = opp.Id, FM_Voice_ARPU__c = 2.0);
        if (executeDML)
            insert pb;
        return pb;
    }
    
    public static cscfga__Product_Configuration__c generateProductConfiguration(Boolean executeDML, String configName, cscfga__Product_Basket__c pb){
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(Name = configName, cscfga__Product_Basket__c = pb.Id, 
            cscfga__Contract_Term_Period__c = 12, cscfga__Contract_Term__c = 12 );
        if (executeDML)
            insert pc;
        return pc;
    }

    public static cscfga__Product_Category__c generateProductCategory(Boolean executeDML, String categoryName){
        cscfga__Product_Category__c prodCategory = new cscfga__Product_Category__c(Name = categoryName, cscfga__Active__c = true);
        if (executeDML)
            insert prodCategory;
        return prodCategory;
    }

    public static cscfga__Configuration_Offer__c generateOffer(Boolean executeDML, String offerName){
        cscfga__Configuration_Offer__c ofr = new cscfga__Configuration_Offer__c(Name = offerName, cscfga__Active__c = true);
        if (executeDML)
            insert ofr;
        return ofr;
    }
    
    public static cspmb__Price_Item__c generatePriceItem(Boolean executeDML, String itemName){
        cspmb__Price_Item__c pi = new cspmb__Price_Item__c(Name = itemName, cspmb__Recurring_Charge__c = 100, 
            cspmb__One_Off_Charge__c = 50, Minimum_One_Off_Price__c = 10, Minimum_Recurring_Price__c = 10);
        if (executeDML)
            insert pi;
        return pi;
    }
    
    public static void generateAttributePriceItemRelationship(Boolean executeDML, cscfga__Attribute__c attr, Id priceItemId){
        attr.cscfga__Value__c = priceItemId;
        if(executeDML)
            update attr;
    }
    
    public static cspmb__Add_On_Price_Item__c generateAddOnPriceItem(Boolean executeDML, String itemName){
        cspmb__Add_On_Price_Item__c api = new cspmb__Add_On_Price_Item__c(Name = itemName, cspmb__Recurring_Charge__c = 100, 
            cspmb__One_Off_Charge__c = 50, Minimum_One_Off_Price__c = 10, Minimum_Recurring_Price__c = 10);
        if (executeDML)
            insert api;
        return api;
    }
    
    public static cspmb__Price_Item_Add_On_Price_Item_Association__c generatePriceItem(Boolean executeDML, cspmb__Add_On_Price_Item__c api, cspmb__Price_Item__c pi){
        cspmb__Price_Item_Add_On_Price_Item_Association__c aopipia = new cspmb__Price_Item_Add_On_Price_Item_Association__c(cspmb__Add_On_Price_Item__c = api.Id, 
            cspmb__Price_Item__c = pi.Id);
        if (executeDML)
            insert aopipia;
        return aopipia;
    }
    
    public static cscfga__Product_Definition__c generateProductDefinition(Boolean executeDML, String definitionName){
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c( Name = definitionName, cscfga__Description__c = definitionName );
        if (executeDML)
            insert prodDef;
        return prodDef;
    }
    
    public static csbb__Product_Configuration_Request__c generateProductConfigRequest(Boolean executeDML, Id prodCategory, Id prodConfig){
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c( csbb__Product_Category__c = prodCategory,
            csbb__Product_Configuration__c = prodConfig); 
        if (executeDML)
            insert pcr;
        return pcr;
    }
    
    public static cscfga__Product_Definition__c generateProductDefinitionResign(Boolean executeDML, String definitionName,Boolean custoResign){
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c( Name = definitionName, cscfga__Description__c = definitionName,Custom_Resign_Process__c = custoResign);
        if (executeDML)
            insert prodDef;
        return prodDef;
    }

    public static cscfga__Offer_Category_Association__c generateCategoryDefinitionAssoc(Boolean executeDML, cscfga__Product_Category__c prodCategory, cscfga__Configuration_Offer__c offer){
        cscfga__Offer_Category_Association__c oca = new cscfga__Offer_Category_Association__c(cscfga__Configuration_Offer__c = offer.ID, cscfga__Product_Category__c = prodCategory.ID);
        if (executeDML)
            insert oca;
        return oca;
    }

    public static List<cscfga__Attribute_Definition__c> generateAttributeDefinitions(Boolean executeDML, List<String> attributeNameList, cscfga__Product_Definition__c prodDef){
        List<cscfga__Attribute_Definition__c> attributeDefinitionList = new List<cscfga__Attribute_Definition__c>();
        for(String attrName : attributeNameList){
            if(attrName == 'Plan' || attrName == 'Device')
                attributeDefinitionList.add(new cscfga__Attribute_Definition__c(Name = attrName, cscfga__Type__c = 'User Input', 
                    cscfga__Default_Value__c = '20', cscfga__Data_Type__c = 'Decimal', cscfga__Product_Definition__c = prodDef.ID));
            else
                attributeDefinitionList.add(new cscfga__Attribute_Definition__c(Name = attrName, cscfga__Type__c = 'User Input', 
                    cscfga__Default_Value__c = '20', cscfga__Data_Type__c = 'String', cscfga__Product_Definition__c = prodDef.ID));
        }
        if (executeDML)
            insert attributeDefinitionList;
        return attributeDefinitionList;
    }

    public static List<cscfga__Attribute__c> generateAttributesForConfiguration(Boolean executeDML, List<cscfga__Attribute_Definition__c> attributeNameList, cscfga__Product_Configuration__c prodConfig){
        List<cscfga__Attribute__c> attributesList = new List<cscfga__Attribute__c>();
        for(cscfga__Attribute_Definition__c attrName : attributeNameList){
            attributesList.add(new cscfga__Attribute__c(Name = attrName.Name, cscfga__Value__c = '20', cscfga__Attribute_Definition__c = attrName.ID,
                cscfga__Product_Configuration__c = prodConfig.ID, cscfga__is_active__c = true));
        }
        if (executeDML)
            insert attributesList;
        return attributesList;
    }

    public static List<CS_Report_Page_Calculation__c> generateReportPageSettings(Boolean executeDML, List<String> attributeNameList){
        List<CS_Report_Page_Calculation__c> reportPageSettingList = new List<CS_Report_Page_Calculation__c>();
        for(String atrName : attributeNameList){
            reportPageSettingList.add(new CS_Report_Page_Calculation__c(Name = atrName, Calculation_Formula__c = atrName, Highlight_calculation_row__c = true,
               Page_Name__c = 'CS_ProfitAndLossReport', Sequence__c = 10, Total_Formula__c = atrName));
        }
        if (executeDML)
            insert reportPageSettingList;
        return reportPageSettingList;
    }

    public static CS_Report_Page_Settings__c generateReportPageCustomSetting(Boolean executeDML){
        CS_Report_Page_Settings__c csrps = new CS_Report_Page_Settings__c(Calculation_Header_Name__c = 'Name', Column_Header_Name_Pattern__c = '{Calculations_Product_Group__c}', 
            Default_View_Mode__c = 'Split Field', PC_Split_Field_Name__c = 'Calculations_Product_Group__c', Total_Header_Name__c = 'Total' );
        if (executeDML)
            insert csrps;
        return csrps;
    }
    
    public static Usage_Profile__c generateUsageProfile(Boolean executeDML, Account acc) {
        Associated_LEs__c ale = generateAssociatedLES(true, 'test', acc);
        Usage_Profile__c up = new Usage_Profile__c(
            Active__c = true,
            Account__c = acc.Id,
            Expected_SMS__c = 20,
            Expected_Voice__c = 10,
            Expected_Data__c = 20,
            Calls_to_other_UK_mobile_networks__c = 20,
            Total_Number_of_Connections__c = 2,
            Data_Only_Connections__c = 2,
            AssociatedLE__c = ale.Id,
            EOLBillDays__c = '300',
            Landline__c = 10
        );
        if (executeDML)
            insert up;
        return up;
    }
    public static csbtcl_Temporary_Data_Store_CS_JSON__c generateTemporaryStoreJSON(Boolean executeDML,Id oppId,Boolean isProcessed){
        csbtcl_Temporary_Data_Store_CS_JSON__c jsonRec = new csbtcl_Temporary_Data_Store_CS_JSON__c(
            Processed__c = isProcessed,
            Opportunity__c = oppId);
         if (executeDML)
            insert jsonRec;
        return jsonRec;
    }

    public static OLI_Sync__c generateOLISync(Boolean executeDML){
        OLI_Sync__c oliSyncSetting = new OLI_Sync__c(Product_Configuration_Level__c = true, Sum_One_Off_And_Recurring__c = false, Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        if(executeDML)
            insert oliSyncSetting;
        return oliSyncSetting;
    }
    
    public static CS_Solution_Calculations__c generateSolutionCalculation(Boolean executeDML){
        CS_Solution_Calculations__c solCalc = new CS_Solution_Calculations__c(Skip_Old_Calculations_Product_Group__c = 'Cloud Voice, Future Mobile');
        if(executeDML)
            insert solCalc;
        return solCalc;
    }

    public static cspl__Report_Configuration__c generateReportConfiguration(Boolean executeDML, String name, String value){
        cspl__Report_Configuration__c rc = new cspl__Report_Configuration__c(Name = name, cspl__value__c = value);
        if(executeDML)
            insert rc;
        return rc;
    }

    public static void generatePLReportConfigRecords(){
        String reportConfig = '[{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Voice Revenue (inc SMS)","type":"BT Mobile,EE SME,BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"10","operation":"","rowDetails":[{"fieldName":"Voice_Revenue_inc_SMS__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Data Revenue","type":"BT Mobile,EE SME,BT Mobile Broadband,BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"20","operation":"","rowDetails":[{"fieldName":"Data_Revenue__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Care and Chargable Services Revenue","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"30","operation":"","rowDetails":[{"fieldName":"Care_and_Chargable_Service_Revenue__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Site & Sub Install Costs BTOP One Off (hide later)","type":"BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"35","operation":"","rowDetails":[{"fieldName":"Year_1_Site_And_Sub_Install__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Site & Sub Install Costs BTOP Recurring (hide later)","type":"BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"36","operation":"","rowDetails":[{"fieldName":"Site_And_Sub_Install__c","isRecurring":"yes"}]},{"calcField":"SiteAndInstall","showInUI":"no","isBold":"no","rowName":"Site & Sub Install Costs BT Mobile","rowOrder":"37","operation":"SiteAndInstall","operationNumbers":""},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Site & Sub Install Costs","rowOrder":"40","operation":"sum","operationNumbers":"35,36,37"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Other Revenue One Off","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"50","operation":"","rowDetails":[{"fieldName":"Year_1_Other_Revenue__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Other Revenue Recurring","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"60","operation":"","rowDetails":[{"fieldName":"Other_Revenue__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Other Revenue","rowOrder":"70","operation":"sum","operationNumbers":"50,60"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"TOTAL BILLABLE REVENUE One Off","rowOrder":"80","operation":"sum","operationNumbers":"35,50"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"TOTAL BILLABLE REVENUE Recurring","rowOrder":"90","operation":"sum","operationNumbers":"10,20,30,36,60"},{"calcField":"","showInUI":"yes","isBold":"yes","rowName":"TOTAL BILLABLE REVENUE","rowOrder":"100","operation":"sum","operationNumbers":"80,90"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Incoming Revenue","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"yes","isCost":"no","rowOrder":"110","operation":"","rowDetails":[{"fieldName":"Incoming_Revenue__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"no","isBold":"yes","rowName":"TOTAL REVENUE One Off","rowOrder":"118","operation":"sum","operationNumbers":"80"},{"calcField":"","showInUI":"no","isBold":"yes","rowName":"TOTAL REVENUE Recurring","rowOrder":"119","operation":"sum","operationNumbers":"90,110"},{"calcField":"","showInUI":"yes","isBold":"yes","rowName":"TOTAL REVENUE","rowOrder":"120","operation":"sum","operationNumbers":"118,119"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Airtime Fund Basket Hidden","type":"BT Mobile,BT Mobile Broadband,EE SME","isCharge":"no","isCost":"no","rowOrder":"125","operation":"","rowDetails":[{"basketFieldName":"Airtime_Fund__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Airtime Fund BTOP Hidden","type":"BT OnePhone","isCharge":"no","isCost":"no","rowOrder":"126","operation":"","rowDetails":[{"fieldName":"Airtime_Fund__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Airtime Fund SME Hidden","type":"EE SME","isCharge":"no","isCost":"no","rowOrder":"127","operation":"","rowDetails":[{"fieldName":"Other_One_Off_Cost__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"yes","rowName":"Airtime Fund Hidden","rowOrder":"130","operation":"sum","operationNumbers":"125,126,127"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Tech Fund BT Mobile Hidden","type":"BT Mobile,BT Mobile Broadband,EE SME","isCharge":"no","isCost":"no","rowOrder":"140","operation":"","rowDetails":[{"basketFieldName":"BT_Technology_Fund_Calculated__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Tech Fund EE SME Hidden","type":"EE SME","isCharge":"no","isCost":"no","rowOrder":"150","operation":"","rowDetails":[{"fieldName":"Investment__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Tech Fund BTOP Hidden","type":"BT OnePhone","isCharge":"no","isCost":"no","rowOrder":"155","operation":"","rowDetails":[{"fieldName":"BTOP_Tech_Fund__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Tech Fund Hidden","rowOrder":"160","operation":"sum","operationNumbers":"140,150,155"},{"calcField":"","showInUI":"no","isBold":"no","type":"BT Mobile,BT Mobile Broadband,EE SME","rowName":"Hardware Extra Hidden","isCharge":"no","isCost":"no","rowOrder":"170","operation":"","rowDetails":[{"basketFieldName":"Total_Hardware_Extra__c","monthFieldBasket":"Contract_Term_Period__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"no","isBold":"yes","rowName":"CUSTOMER INVESTMENT One Off","rowOrder":"180","operation":"sum","operationNumbers":"130,160"},{"calcField":"","showInUI":"no","isBold":"yes","rowName":"CUSTOMER INVESTMENT Recurring","rowOrder":"190","operation":"sum","operationNumbers":"170"},{"calcField":"","showInUI":"yes","isBold":"yes","rowName":"CUSTOMER INVESTMENT","rowOrder":"200","operation":"sum","operationNumbers":"180,190"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Airtime Fund","rowOrder":"210","operation":"sum","operationNumbers":"125,126,127"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Tech Fund","rowOrder":"220","operation":"sum","operationNumbers":"140,150,155"},{"calcField":"","showInUI":"yes","isBold":"no","type":"BT Mobile,BT Mobile Broadband,EE SME","rowName":"Hardware Extra","isCharge":"no","isCost":"no","rowOrder":"230","operation":"","rowDetails":[{"basketFieldName":"Total_Hardware_Extra__c","monthFieldBasket":"Contract_Term_Period__c","isRecurring":"yes"}]},{"calcField":"EmptyYears","showInUI":"yes","isBold":"no","rowName":"Cheque","rowOrder":"240","operation":"EmptyYears","operationNumbers":""},{"calcField":"","showInUI":"no","isBold":"yes","rowName":"Revenue Net of Investment One Off","rowOrder":"250","operation":"sub","operationNumbers":"118,180"},{"calcField":"","showInUI":"no","isBold":"yes","rowName":"Revenue Net of Investment Recurring (hide later)","rowOrder":"260","operation":"sub","operationNumbers":"119,190"},{"calcField":"","showInUI":"yes","isBold":"yes","rowName":"Revenue Net of Investment","rowOrder":"270","operation":"sum","operationNumbers":"250,260"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Voice Costs (inc SMS)","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"280","operation":"","rowDetails":[{"fieldName":"Voice_Costs_inc_SMS__c","isRecurring":"yes"},{"fieldName":"Voice_Costs_inc_SMS_One_Off__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Data Costs One Off (hidden)","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"285","operation":"","rowDetails":[{"fieldName":"Data_Costs_One_Off__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Data Costs Recurring (hidden)","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"286","operation":"","rowDetails":[{"fieldName":"Data_Costs__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Data Costs","rowOrder":"290","operation":"sum","operationNumbers":"285,286"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Site & Sub Install (COST) BTOP One Off (hidden)","type":"BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"295","operation":"","rowDetails":[{"fieldName":"Year_1_Site_And_Sub_Install_Cost__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Site & Sub Install (COST) BTOP Recurring (hidden)","type":"BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"296","operation":"","rowDetails":[{"fieldName":"Site_And_Sub_Install_Cost__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Site & Sub Install (COST)","rowOrder":"300","operation":"sum","operationNumbers":"295,296"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Care and Chargable Service Costs One Off (hidden)","type":"BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"305","operation":"","rowDetails":[{"fieldName":"Year_1_Care_and_Chargable_Service_Costs__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Care and Chargable Service Costs Recurring (hidden)","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"306","operation":"","rowDetails":[{"fieldName":"Care_and_Chargable_Service_Costs__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Care and Chargable Service Costs","rowOrder":"310","operation":"sum","operationNumbers":"305,306"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Net Device Profit/Cost","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"320","operation":"","rowDetails":[{"fieldName":"Net_device_profit_cost__c","isRecurring":"no"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Sales Overhead - Commission","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"330","operation":"","rowDetails":[{"fieldName":"BT_Total_Commission__c","isRecurring":"no"},{"fieldName":"Sales_Commision_One_Off_Cost__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Other Costs One Off","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"340","operation":"","rowDetails":[{"fieldName":"Year_1_Other_Costs__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Other Costs Recurring","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"yes","rowOrder":"350","operation":"","rowDetails":[{"fieldName":"Other_Costs__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Other Costs","rowOrder":"360","operation":"sum","operationNumbers":"340,350"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Total Costs One Off ","rowOrder":"370","operation":"sum","operationNumbers":"285,295,305,320,330,340"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Total Costs Recurring","rowOrder":"380","operation":"sum","operationNumbers":"280,286,296,306,350"},{"calcField":"","showInUI":"yes","isBold":"yes","rowName":"Total Costs","rowOrder":"390","operation":"sum","operationNumbers":"370,380"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Gross Margin One Off","rowOrder":"400","operation":"sub","operationNumbers":"250,370"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Gross Margin Recurring","rowOrder":"410","operation":"sub","operationNumbers":"260,380"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Gross Margin","rowOrder":"420","operation":"sum","operationNumbers":"400,410"},{"calcField":"MarginPercent","showInUI":"yes","isBold":"no","rowName":"GM %","rowOrder":"430","operation":"MarginPercent","operationNumber":""},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Incremental Overheads One Off","type":"BT Mobile,BT Mobile Broadband,EE SME","isCharge":"no","isCost":"no","rowOrder":"440","operation":"","rowDetails":[{"fieldName":"Year_1_Incremental_Overheads__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Incremental Overheads Recurring","type":"BT Mobile,BT Mobile Broadband,EE SME,BT OnePhone","isCharge":"no","isCost":"no","rowOrder":"450","operation":"","rowDetails":[{"fieldName":"Incremental_Overheads__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Incremental Overheads","rowOrder":"460","operation":"sum","operationNumbers":"440,450"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Incremental EBITDA One Off","rowOrder":"470","operation":"sub","operationNumbers":"400,440"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Incremental EBITDA Recurring","rowOrder":"480","operation":"sub","operationNumbers":"410,450"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Incremental EBITDA","rowOrder":"490","operation":"sum","operationNumbers":"470,480"},{"calcField":"IncrementalEBITDAPrecent","showInUI":"yes","isBold":"no","rowName":"Incremental EBITDA%","rowOrder":"500","operation":"IncrementalEBITDAPrecent","operationNumbers":""},{"calcField":"IncrNpvPerSub","showInUI":"yes","isBold":"no","rowName":"Incremental NPV per Sub","rowOrder":"510","operation":"IncrNpvPerSub","operationNumbers":""},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Other Overheads One Off","type":"BT Mobile,BT Mobile Broadband,EE SME","isCharge":"no","isCost":"no","rowOrder":"520","operation":"","rowDetails":[{"fieldName":"Year_1_Other_Overheads__c","isRecurring":"no"}]},{"calcField":"","showInUI":"no","isBold":"no","rowName":"Other Overheads Recurring","type":"BT Mobile,BT Mobile Broadband,EE SME","isCharge":"no","isCost":"no","rowOrder":"530","operation":"","rowDetails":[{"fieldName":"Other_Overheads__c","isRecurring":"yes"}]},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"Other Overheads","rowOrder":"540","operation":"sum","operationNumbers":"520,530"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"EBITDA One Off","rowOrder":"550","operation":"sub","operationNumbers":"470,520"},{"calcField":"","showInUI":"no","isBold":"no","rowName":"EBITDA Recurring","rowOrder":"560","operation":"sub","operationNumbers":"480,530"},{"calcField":"","showInUI":"yes","isBold":"no","rowName":"EBITDA","rowOrder":"570","operation":"sum","operationNumbers":"550,560"},{"calcField":"EBITDAPercent","showInUI":"yes","isBold":"no","rowName":"EBITDA%","rowOrder":"580","operation":"EBITDAPercent","operationNumbers":""},{"calcField":"NPVPerSub","showInUI":"yes","isBold":"no","rowName":"NPV per Sub","rowOrder":"590","operation":"NPVPerSub","operationNumbers":""}]';
        cspl__Report_Configuration__c rc = CS_TestDataFactory.generateReportConfiguration(true, 'rowConfig', reportConfig);
        
        reportConfig = '["Name","cscfga__One_Off_Charge__c","cscfga__Recurring_Charge__c","cspl__Month_Term__c","Calculations_Product_Group__c","Product_Definition_Name__c","Care_and_Chargable_Service_Revenue__c","Other_Revenue__c","Year_1_Other_Revenue__c","BT_Technology_Fund__c","Mobile_Voice_One_Off_Charge__c","Mobile_Voice_Recurring_Charge__c","SMS_Recurring_Charge__c","SMS_One_Off_Charge__c","Data_One_Off_Charge__c","Data_Recurring_Charge__c","Other_One_Off_Charge__c","Other_Recurring_Charge__c","Fixed_One_Off_Charge__c","Fixed_Recurring_Charge__c","Incoming_Recurring_Charge__c","Incoming_Recurring_Cost__c","Mobile_Voice_One_Off_Cost__c","Mobile_Voice_Recurring_Cost__c","Line_Rental__c","SMS_One_Off_Cost__c","SMS_Recurring_Cost__c","Data_One_Off_Cost__c","Data_Recurring_Cost__c","Other_One_Off_Cost__c","Other_Recurring_Cost__c","Fixed_One_Off_Cost__c","Fixed_Recurring_Cost__c","Device_One_Off_Cost__c","Device_Recurring_Cost__c","Device_One_Off_Charge__c","Device_Recurring_Charge__c","Sales_Commision_One_Off_Cost__c","OPEX_Recurring_Cost__c","Service_Recurring_Cost__c","Rolling_Air_Time__c","Staged_Air_Time__c","Tech_Fund__c","Buy_out_cheque__c","Unconditional_cheque__c","cscfga__Contract_Term__c","Data_Costs__c","Year_1_Care_and_Chargable_Service_Costs__c","Care_and_Chargable_Service_Costs__c","Net_device_profit_cost__c","Product_Name__c","Total_Investment_Pot__c","Investment__c","Voice_Revenue_inc_SMS__c","Data_Revenue__c","cscfga__recurring_charge_product_discount_value__c","cscfga__one_off_charge_product_discount_value__c","Incoming_Revenue__c","Voice_Costs_inc_SMS__c","Other_Costs__c","Year_1_Other_Costs__c","Year_1_Incremental_Overheads__c","Incremental_Overheads__c","Volume__c","Year_1_Other_Overheads__c","Other_Overheads__c","BT_Total_Commission__c","Data_Costs_One_Off__c","Voice_Costs_inc_SMS_One_Off__c","Mobile_Voice_Recurring_Charge_Discounted__c","Mobile_Voice_One_Off_Charge_Discounted__c","SMS_Recurring_Charge_Discounted__c","SMS_One_Off_Charge_Discounted__c","Data_Recurring_Charge_Discounted__c","Data_One_Off_Charge_Discounted__c","Other_Recurring_Charge_Discounted__c","Other_One_Off_Charge_Discounted__c","Fixed_Recurring_Charge_Discounted__c","Fixed_One_Off_Charge_Discounted__c","Airtime_Fund__c","Tech_Fund__c","Site_And_Sub_Install__c","Year_1_Site_And_Sub_Install__c","Site_And_Sub_Install_Cost__c","Year_1_Site_And_Sub_Install_Cost__c","BTOP_Tech_Fund__c"]';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'configFields', reportConfig);

        reportConfig = '{"cumsum", "sum", "sub", "add", "mul", "div"}';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'configRecord', reportConfig);

        reportConfig = '{"":{"monthTermField":"Month_Term__c", "contractTermField":"cscfga__Contract_Term__c"}}';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'configSettings', reportConfig);

        reportConfig = '[{"isNegative" : "yes" }]';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'costConfig', reportConfig);

        reportConfig = '[{"showInUI":"no"}]';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'grossMarginField', reportConfig);

        reportConfig = '[{"showInUI":"no"}]';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'hideChart', reportConfig);

        reportConfig = '[{"fieldName":"cscfga__Total_Price__c","sequence":2,"showInUI":"yes"},{"fieldName":"cscfga__total_contract_value__c","sequence":1,"showInUI":"yes"},{"fieldName":"Airtime_Fund__c","sequence":3,"showInUI":"no"},{"fieldName":"Payment_Terms_Cost__c","sequence":4,"showInUI":"no"},{"fieldName":"Tenure__c","sequence":5,"showInUI":"no"},{"fieldName":"Competitive_Clause_Formula__c","sequence":6,"showInUI":"no"},{"fieldName":"Disconnection_Allowance_Formula__c","sequence":7,"showInUI":"no"},{"fieldName":"Unlocking_Fees_Formula__c","sequence":8,"showInUI":"no"},{"fieldName":"Full_Co_Terminus_Formula__c","sequence":9,"showInUI":"no"},{"fieldName":"BT_Technology_Fund_Calculated__c","sequence":10,"showInUI":"no"},{"fieldName":"User_Profile_Name__c","sequence":11,"showInUI":"no"},{"fieldName":"Total_Buyout_Cost__c ","sequence":12,"showInUI":"no"},{"fieldName":"Total_Hardware_Extra__c","sequence":13,"showInUI":"no"},{"fieldName":"Contract_Term_Period__c","sequence":14,"showInUI":"no"},{"fieldName":"Total_Subscribers__c","sequence":15,"showInUI":"no"}]';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'recordConfig', reportConfig);

        reportConfig = '[{"name":"HideExportButton","value":"no"},{"name":"ExportHiddenRows","value":"no"}]';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'exportButton', reportConfig);

        reportConfig = '[{"showInUI":"no"}]';
        rc = CS_TestDataFactory.generateReportConfiguration(true, 'grossMarginField', reportConfig);
    }

    public static BT_User_Group__c generateBTUserGroup(Boolean executeDML, String name){
        BT_User_Group__c ug = new BT_User_Group__c(Name = name, Active__c = true);
        if(executeDML)
            insert ug;
        return ug;
    }

    public static BT_User_Group_Member__c generateBTUserGroupMember(Boolean executeDML, String name){
        BT_User_Group_Member__c ug = new BT_User_Group_Member__c(Name = name, User__c = UserInfo.getUserId());
        if(executeDML)
            insert ug;
        return ug;
    }

    public static CS_Approval_Conditions__c generateApprovalCondition(Boolean executeDML){
        CS_Approval_Conditions__c ac = new CS_Approval_Conditions__c(
            Is_Default__c = true,
            KPI_Level_3_value_from__c = 34, 
            KPI_Level_2_value_to__c = 34.01, 
            KPI_Level_2_value_from__c = 39, 
            KPI_Level_1_value_to__c = 39.01,
            KPI_Level_1_value_from__c = 44, 
            KPI__c = 'GM%',
            Product_Family__c = 'BT Mobile',
            Product__c = 'BT Mobile Sharer',
            Name = 'BT Mobile Sharer'
        );
        if(executeDML)
            insert ac;
        return ac;
    }

    public static CS_Approval_Paths__c generateApprovalPath(Boolean executeDML, CS_Approval_Conditions__c ac, BT_User_Group__c ug){
        CS_Approval_Paths__c ap = new CS_Approval_Paths__c(
            Level_1_Approver__c = UserInfo.getUserId(),
            Level_2_Approver__c = UserInfo.getUserId(),
            Level_3_Approver__c = UserInfo.getUserId(),
            BT_User_Group__c = ug.Id,
            CS_Approval_Conditions__c = ac.Id,
            Name = ac.Name + ' - ' + ug.Name
        );
        if(executeDML)
            insert ap;
        return ap;
    }

    public static void setupNoTriggersFlag(){
        No_Triggers__c notriggers =  new No_Triggers__c();
        notriggers.Flag__c = true;
        INSERT notriggers;
    }

    public static Product2 createProduct2(Boolean executeDML, string Name, string ProductFamily){
        Product2 tmpProduct2 = new Product2();
        tmpProduct2.Family=ProductFamily;
        tmpProduct2.Name = Name;
        tmpProduct2.IsActive = true;

        if(executeDML)
            insert tmpProduct2;
        
        return tmpProduct2;
    }

    public static BT_One_Phone__c generateBTOnePhone(Boolean executeDML, Opportunity opp){
        BT_One_Phone__c btOnePhone = new BT_One_Phone__c(Opportunity__c = opp.id, Existing_Customer_Resign__c = 'N',
                OnePhone_Product_Type__c = 'BT One Phone Office', Volume_of_Data_SIMs__c = 2, Contract_Duration__c = '24 Months', 
                All_key_Customer_Locations_Coverage__c = 'Checked OK', Contract_Value__c = 10000);
        if (executeDML)
            insert btOnePhone;
        return btOnePhone;
    }

    public static BT_One_Phone_Site__c generateBTOnePhoneSite(Boolean executeDML, BT_One_Phone__c btOnePhone, Contact contact){
        BT_One_Phone_Site__c btOnePhoneSite = new BT_One_Phone_Site__c(No_Onsite_Network__c = true, BT_One_Phone__c = btOnePhone.Id, 
        Site_Telephone_Number_Landline__c = '01234 567890', Number_of_Users__c = 4, Number_Concurrent_Calls_Expected_at_Site__c = 4, 
        Number_of_Users_in_Parallel_Hunt_Group__c = '5 or Less', mobilecoverageResults__c = 'no on-site network', 
        Coverage_3G_Checker_Results__c = 'Excellent Indoor & Outdoor', Customer_Contact__c = contact.Id, Building_Name__c = 'Building 1', 
        Building_Number__c = '2', Street__c = 'Street', Post_Code__c = '10000', County__c = 'County', Town__c = 'Town', 
        Additional_Site_Connection_Charge__c = 11111, Private_Voice_Extra_One_Off_Charge__c = 22222, Private_Voice_Extra_Monthly_Charge__c = 33333,
        Private_Data_Extra_One_Off_Charge__c = 44444, Private_Data_Extra_Monthly_Charge__c = 55555, Additional_SIP_Network_Charge__c = 66666, 
        On_Site_SIP_Network_Ongoing_Charge__c = 77777, Additional_OnSite_Mobile_Network_charges__c = 88888, 
        Addl_Onsite_MobileNetwork_Ongoing_Charge__c = 99999);
        if (executeDML)
            insert btOnePhoneSite;
        return btOnePhoneSite;
    }    

    public static void insertTriggerDeactivatingSetting() {
       TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
        insert settings;
    }

}