public with sharing class createAddress{

    public Contact contact{get; set;} 
    public string refferalPage;
       
    public createAddress(ApexPages.StandardController stdController) {

         contact = (Contact)stdController.getRecord();
    }
    
    public createAddress(Contact contact_edit){
         contact = contact_edit;
    }
    
    public PageReference OnLoad() { 
         refferalPage = ApexPages.currentPage().getParameters().get('refPage');
         Contact.Contact_Building__c = '';
         Contact.Contact_Sub_Building__c = '';
         Contact.Contact_Address_Number__c = '';
         Contact.Address_POBox__c = '';
         Contact.Contact_Address_Street__c = '';
         Contact.Contact_Locality__c = '';
         Contact.Contact_Post_Town__c = '';
         Contact.Address_County__c = '';
         Contact.Address_Country__c = 'United Kingdom';
           
         return null;            
   }
   
   public PageReference Save() {
       
       If (ValidateMandatoryFields() != true){
            return null;
       }      
       //SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway(); 
       //service.timeout_x = 60000;
       

           try {                         
               string NADKey;
               
               if(Test_Factory.GetProperty('IsTest') == 'yes') {
                   NADKey = 'test';
               }           
               else {
                   //NADKey = service.CreateNADAddress('', '', crmAddress()); // singhd62
                   NADKey = SalesforceCRMService.CreateNADAddress(crmAddress()); // singhd62
               }  
               system.debug('IIIIIIIIIIIIIIIIIII NADKey  '+NADKey +'Test_Factory.GetProperty :'+Test_Factory.GetProperty('IsTest'));              
               if (NADKey == '' ||
                   NADKey == null){                  
                   //contact.Address_Id__c = 'X05000243785';
                   //upsert contact;               
                   //return ReDirect();
                   ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The NAD service has rejected the request, please enter a valid address!'));                               
                   return null;
               } 
               else {
                   contact.Address_Id__c = NADKey;
                   if(Test_Factory.GetProperty('IsTest') == 'yes') {
                         
                   }
                   else {
                       upsert contact;                 
                   }    
                   return ReDirect();                          
               }                   
           }
           catch (System.CalloutException ex){
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There appears to be problem connecting to the NAD service, please try again!'));
               return null;
           }

   }
   
   public PageReference Cancel(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
        if (refferalPage == 'new'){
            PageReference PageRef = new PageReference('/apex/createcontact?id='+contact.Id);
            PageRef.setRedirect(true);
                       
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }    
            return PageRef;                       
        }
        if (refferalPage == 'update'){
            PageReference PageRef = new PageReference('/apex/updatecontact?id='+contact.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }   
            return PageRef;    
        }
        if (refferalPage == 'detail'){
            PageReference PageRef = new PageReference('/'+contact.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }      
            return PageRef;  
        }
        return null;
    }
    
    public SalesforceCRMService.CRMAddress crmAddress(){ //singhd62
        SalesforceCRMService.CRMAddress crmAddress = new SalesforceCRMService.CRMAddress(); //singhd62
        crmAddress.Name = contact.Contact_Building__c;
        crmAddress.SubBuilding = contact.Contact_Sub_Building__c;
        crmAddress.BuildingNumber = contact.Contact_Address_Number__c;
        crmAddress.POBox = contact.Address_POBox__c;
        crmAddress.Street = contact.Contact_Address_Street__c;
        crmAddress.Locality = contact.Contact_Locality__c;
        crmAddress.Town = contact.Contact_Post_Town__c;
        crmAddress.County = contact.Address_County__c; 
        crmAddress.Country = contact.Address_Country__c; 
        crmAddress.Postcode = contact.Contact_Post_Code__c;    
        return crmAddress; 
    }
    
    private boolean ValidateMandatoryFields() {
        if (contact.Contact_Address_Street__c == '' ||
            contact.Contact_Address_Street__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (contact.Contact_Post_Town__c == '' ||
            contact.Contact_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (contact.Contact_Post_Code__c == '' ||
            contact.Contact_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (contact.Address_County__c == '' ||
            contact.Address_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (contact.Address_Country__c == '' ||
            contact.Address_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        return true;
    }
}