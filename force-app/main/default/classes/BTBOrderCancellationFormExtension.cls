Public class BTBOrderCancellationFormExtension{

String BOId;
Boolean DisplayAHD,CFClosed=True,CFError,RecordLockError,SubmitLockError;

  BTLB_Order_Cancellation_Form__c BOCF=new BTLB_Order_Cancellation_Form__c();
  public BTLB_Order_Cancellation_Form__c getBTOCF() {return BOCF;}
  public void setBTOCF(BTLB_Order_Cancellation_Form__c value) {BOCF= value;}
  BTB_Order_Line__c BO;
  
  public double CalcLockTime(){
         long dt1=BO.Currently_Actioned_Time__c.getTime()/1000 / 60;
         long dt2=datetime.now().getTime()/1000 / 60;
         double d = dt2 - dt1;
         Return d;
  
  }

  public BTBOrderCancellationFormExtension(ApexPages.StandardController controller) {   
    Try{
    Boolean CF=False;
    
    BOId=Apexpages.currentPage().getParameters().get('BTBOrderLineId');
    BO=[Select Cancellation_Current_Status__c,BTB_CSS_ORDER_NO__c,CreatedDate,BTB_ORDER__r.OV_ORDER_NUM__c,BTB_ORDER__r.CreatedDate,BTB_ORDER__r.OwnerId,CUSTOMER_REQ_DATE__c,NOTES__c,CUST_AGREED_DT__c,COMPLETION_DATE__c,CANCELLATION_REASON__c,Currently_Actioned_By__c,Currently_Actioned_Time__c, 
                          (Select Product_Name_on_CSS__c,PROD_GROUP__c From BTB_Order_Line_Products__r where Acquisition_Defence_Type__c='Y'),
                          (Select BT_Order_Number__c, Order_Create_Date__c, CSS_Order_Number__c, CSS_Order_Created_Date__c, Order_raised_by_EIN__c, Order_Created_By_Name__c, Location_OV__c, Company_Name_OV__c, Contact_Name_OV__c, Telephone_Number_OV__c, Customer_Required_By_Date__c, Customer_Agreed_Date__c, Cancellation_Date__c, Broadband_Included__c, Product_Ordered__c, Number_of_lines_involved__c, Cancellation_reason__c, Customer_Contacted__c, Customer_Telephone_Number__c,Notes__c,
                                  First_Actioned_By__c,First_Actioned_By_EIN__c,First_Actioned_Date_Time__c,Second_Actioned_By__c,Second_Actioned_By_EIN__c,Second_Actioned_Date_Time__c,Form_Submitted_By__c,Form_Submitted_By_EIN__c,Form_Submitted_Date_Time__c From BTB_Order_Cancellation_Forms__r) 
                           From BTB_Order_Line__c where Id=:BOId FOR Update];  
    User U=[Select EIN__C,Department,Division,Phone,Extension,OUC__c From User where Id=:UserInfo.getUserId()];
    User Own=[Select EIN__C,Name,Department From User where Id=:BO.BTB_ORDER__r.OwnerId];
        
    If(BO.BTB_Order_Cancellation_Forms__r.size()!=0){
    BOCF=BO.BTB_Order_Cancellation_Forms__r[0];
    CF=True;
    }
    
    If(BO.Cancellation_Current_Status__c=='Completed'){
      CFClosed=False;
      CFError=True;
      }
    BOCF.Form_Completion_Date__c=system.today();
    BOCF.Form_Created_By_Name__c=UserInfo.getName();
    BOCF.Form_Created_By_EIN__c=U.EIN__C;
    BOCF.Call_Centre__c=U.Department;
    BOCF.Sales_Channel__c=U.Division;
    BOCF.Agent_Contact_Telephone_Number__c=U.Phone;
    BOCF.Agent_Extension__c=U.Extension;
    BOCF.OUC__c=U.OUC__c;
    
    //Below section to display form entries from the queried Cancellation Form record
    If(!CF)
    {
    BOCF.BT_Order_Number__c=BO.BTB_ORDER__r.OV_ORDER_NUM__c;
    BOCF.Order_Create_Date__c=BO.BTB_ORDER__r.CreatedDate;
    BOCF.CSS_Order_Number__c=BO.BTB_CSS_ORDER_NO__c;
    BOCF.CSS_Order_Created_Date__c=BO.CreatedDate;
    BOCF.Order_raised_by_EIN__c=Own.EIN__c;
    BOCF.Order_Created_By_Name__c=Own.Name;
    BOCF.Location_OV__c=Own.Department;
    
    BOCF.Customer_Required_By_Date__c=BO.CUSTOMER_REQ_DATE__c;
    BOCF.Customer_Agreed_Date__c=BO.CUST_AGREED_DT__c;
    BOCF.Cancellation_Date__c=BO.COMPLETION_DATE__c;
    BOCF.Cancellation_reason__c=BO.CANCELLATION_REASON__c;
    If(BO.BTB_Order_Line_Products__r.size()==1){
    BOCF.Product_Ordered__c=BO.BTB_Order_Line_Products__r[0].Product_Name_on_CSS__c;
      If(BO.BTB_Order_Line_Products__r[0].PROD_GROUP__c=='BB Winback')
        BOCF.Broadband_Included__c='Yes';
      Else
         BOCF.Broadband_Included__c='No'; 
      }
     } 
    
    
        If(BOCF.Customer_Contacted__c!=NULL)
        customerContacted=BOCF.Customer_Contacted__c;  
    }
    Catch(Exception e){}
     
  }
  
  public PageReference RecordLocking(){
    If(BO.Currently_Actioned_By__c==Null){
     BO.Currently_Actioned_By__c=UserInfo.getName();
     BO.Currently_Actioned_Time__c=datetime.now();
     Update BO;
    }
    Else If(BO.Currently_Actioned_By__c!=Null){
      If(BO.Currently_Actioned_By__c!=UserInfo.getName()){
         long dt1=BO.Currently_Actioned_Time__c.getTime()/1000 / 60;
         long dt2=datetime.now().getTime()/1000 / 60;
         double dcalc = CalcLockTime();
         If(dcalc<35){
         RecordLockError=True;       
         CFClosed=False;
         }
         Else If(dcalc>35){
         BO.Currently_Actioned_By__c=UserInfo.getName();
         BO.Currently_Actioned_Time__c=datetime.now();
         Update BO;
        }
      }
      Else If(BO.Currently_Actioned_By__c==UserInfo.getName())
        BO.Currently_Actioned_Time__c=datetime.now();
        Update BO;
    }
   
  Return Null;
  }
  
   public PageReference incrementCounter() {
     //If(customerContacted=='Yes')
     If(customerContacted=='Yes')
     DisplayAHD=True;
     Else 
     DisplayAHD=False;
    Return Null;
   }
   
   Public Boolean getDispPageSectionError(){Return CFError;}
   Public Boolean getRecLockError(){Return RecordLockError;}
   Public Boolean getSaveLockError(){Return SubmitLockError;}
   Public Boolean getDispPageSection(){Return CFClosed;}
   Public Boolean getDispAhdSection(){Return DisplayAHD;}
   
   //Save code starts here

   Public pageReference SubmitForm(){
   System.Debug('Debug 0: Initialization ---'+DisplayErr);
    Try
     {
     double dcalcsave = CalcLockTime();
     
     If(dcalcsave>35){
     SubmitLockError=True;
     CFClosed=False;
     Return Null;  
     }
     
     Else If(dcalcsave<35){ 
         DisplayErr=False;
         System.Debug('Debug 1: Initialization ---'+DisplayErr);
         If(BOCF.Company_Name_OV__c==''|| BOCF.Contact_Name_OV__c=='' || BOCF.Telephone_Number_OV__c==NULL|| BOCF.Number_of_lines_involved__c==NULL       
                 || customerContacted==NULL ||BOCF.Customer_Telephone_Number__c==NULL)  {                 
          DisplayErr=True;
          System.Debug('Debug 2: IF 1 ---'+DisplayErr);
          }
          
          
     

         IF(customerContacted=='Yes'){  
           If (BOCF.Name_of_Contact__c==NULL||BOCF.Position_in_Company__c==NULL||BOCF.Address__c==NULL||BOCF.Number_of_Employees__c==NULL || Q1=='None' || BOCF.Losing_Supplier_Name__c==NULL){
               DisplayErr=True;
               System.Debug('Debug 3: First IF 2 ---'+DisplayErr);
              }    
           Else If(Q1=='YES'||Q1=='NO'){        
               If(Q1=='YES'&&Q2=='None'){
                   DisplayErr=True;      
                   System.Debug('Debug 4: ---'+DisplayErr);
                   }    
               Else If(Q1=='NO'||Q2=='NO'){
                    If(Q6=='None'||Q7=='None'||Q8=='None'){
                        DisplayErr=True;    
                        System.Debug('Debug 5: ---'+DisplayErr);      
                        }
                }
               Else If(Q3=='None'){    
                  DisplayErr=True;
                  System.Debug('Debug 6: ---'+DisplayErr);
                  }
               Else If(Q3!='None'&& Q3!='Move of Premises' && Q3!='Refused to Supply Information' && Q3!='Did not place order with BT' && Q3!='Unhappy with BT Service/Advice' && Q3!='Order Placed With Wrong CRD' && Q3!='Admin Fee Raised By CP'){
                  If((Q3=='In Contract'||Q3=='HTT Charges too high')&& Q4=='None') {
                       DisplayErr=True;
                       System.Debug('Debug 7: ---'+DisplayErr);
                       }
                  Else If((Q3=='SP Made Better Offer')&& Q3a=='None') {  
                       DisplayErr=True;
                       System.Debug('Debug 8: ---'+DisplayErr);
                       }
                  Else If((Q3=='Notice Period Required')&& NPLenth==0)  { 
                       DisplayErr=True;                   
                       System.Debug('Debug 9: ---'+DisplayErr);
                       }
                 If(Q5=='None'||Q5a=='None') {
                 DisplayErr=True;
                 System.Debug('Debug 10: ---'+DisplayErr);
                 }
               }
             }
           }
          
        System.Debug('Debug 11: ---'+DisplayErr);

        If(DisplayErr==False){
           BOCF.BTB_Order_Line__c=BOId;
           BOCF.Q1_Did_you_know_that_your_order_to_ret__c=Q1;
           BOCF.Q2_Did_you_agree_to_the_cancellation_o__c=Q2;
           BOCF.Q3_What_was_the_reason_for_agreeing_to__c=Q3;
           BOCF.Q3a_Who_initiated_the_contact_when_dis__c=Q3a;
           BOCF.Q4_What_is_the_remaining_term_notice_p__c=Q4;
           BOCF.Q4a_What_is_the_length_of_your_notice__c=NPLenth;
           BOCF.Q5_Did_your_current_supplier_refer_you__c=Q5;
           BOCF.Q5a_Did_you_contact_BT_to_cancel__c=Q5a;
           BOCF.Q6_Have_you_informed_your_current_supp__c=Q6;
           BOCF.Q7_Do_you_want_us_to_reissue_the_order__c=Q7;
           BOCF.Q8_Do_you_wish_to_complain__c=Q8;
               
           
           If(customerContacted!='Yes'){
           If(customerContacted=='No')
             BO.Cancellation_Current_Status__c='Pending';
           Else If(customerContacted=='Email')
             BO.Cancellation_Current_Status__c='Email';  
             
             BOCF.Name_of_Contact__c=Null;
             BOCF.Position_in_Company__c=Null;
             BOCF.Address__c=Null;
             BOCF.Number_of_Employees__c=Null;
             }
           Else If(customerContacted=='Yes'){
             BO.Cancellation_Current_Status__c='Completed';
             BOCF.Form_Submitted_By__c=BOCF.Form_Created_By_Name__c;
             BOCF.Form_Submitted_By_EIN__c=BOCF.Form_Created_By_EIN__c;
             BOCF.Form_Submitted_Date_Time__c=system.now();
            }

            If(BOCF.First_Actioned_By__c==Null){
             BOCF.First_Actioned_By__c=BOCF.Form_Created_By_Name__c;
             BOCF.First_Actioned_By_EIN__c=BOCF.Form_Created_By_EIN__c;
             BOCF.First_Actioned_Date_Time__c=system.now();
            }
            Else If(BOCF.Second_Actioned_By__c==Null){
             BOCF.Second_Actioned_By__c=BOCF.Form_Created_By_Name__c;
             BOCF.Second_Actioned_By_EIN__c=BOCF.Form_Created_By_EIN__c;
             BOCF.Second_Actioned_Date_Time__c=system.now();            
            }
             
             BO.Currently_Actioned_By__c='';
             BO.Currently_Actioned_Time__c=NULL;
             BO.First_Actioned_By__c=BOCF.First_Actioned_By__c;
             BO.Second_Actioned_By__c=BOCF.Second_Actioned_By__c;
             BO.Form_Submitted_By__c=BOCF.Form_Submitted_By__c;
             BOCF.Customer_Contacted__c=customerContacted;
             BO.NOTES__c = BOCF.Notes__c;
           Upsert BOCF;  
           Update BO;  
         }      
        If(BOCF.Id!=NULL && DisplayErr==False){
           pageReference CForm=new pageReference('/'+BOId);
           Return CForm;  
         }
        Else
        Return Null;  
       }  
       Else
        Return Null;      
       }
     Catch(Exception e){
        Return Null;
     }
   }
   
   //Save code ends here

   Public String Q1 {get; set;} 
   public List<SelectOption> getQ1Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('YES','YES'));
            options.add(new SelectOption('NO','NO'));
            options.add(new SelectOption('Move of Premises/Cease Line','Move of Premises/Cease Line'));
            options.add(new SelectOption('Did not place order with BT','Did not place order with BT'));
            options.add(new SelectOption('Customer is unaware of order',' Customer is unaware of order'));
            return options;
        }
            
      
      Public String Q2 {get; set;} 
      public List<SelectOption> getQ2Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('YES','YES'));
            options.add(new SelectOption('NO','NO'));
            options.add(new SelectOption('Refused to Supply Information',' Refused to Supply Information'));
            return options;
        }
      
      Public String Q3 {get; set;} 
      public List<SelectOption> getQ3Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('Move of Premises','Move of Premises'));
            options.add(new SelectOption('Refused to Supply Information','Refused to Supply Information'));
            options.add(new SelectOption('Did not place order with BT','Did not place order with BT'));
            options.add(new SelectOption('Unhappy with BT Service/Advice','Unhappy with BT Service/Advice'));
            options.add(new SelectOption('Order Placed With Wrong CRD','Order Placed With Wrong CRD'));
            options.add(new SelectOption('Admin Fee Raised By CP','Admin Fee Raised By CP'));
            options.add(new SelectOption('Notice Period Required','Notice Period Required'));
            options.add(new SelectOption('In Contract','In Contract'));
            options.add(new SelectOption('HTT Charges too high','HTT Charges too high'));
            options.add(new SelectOption('SP Made Better Offer','SP Made Better Offer'));
            options.add(new SelectOption('Changed Mind','Changed Mind'));
            return options;
        }  
       
      Public String Q3a {get; set;} 
      public List<SelectOption> getQ3aItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('SP','SP'));
            options.add(new SelectOption('Customer','Customer'));            
            return options;
        }   
        
      Public String Q4 {get; set;} 
      public List<SelectOption> getQ4Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('1-3 Months','1-3 Months'));
            options.add(new SelectOption('4-6 Months','4-6 Months'));    
            options.add(new SelectOption('7-12 Months','7-12 Months')); 
            options.add(new SelectOption('13-24 Months','13-24 Months')); 
            options.add(new SelectOption('Other','Other')); 
            options.add(new SelectOption('Out of Contract','Out of Contract')); 
            options.add(new SelectOption('Unknown','Unknown'));         
            return options;
        }  
        
      Public String Q5 {get; set;} 
      public List<SelectOption> getQ5Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('YES','YES'));
            options.add(new SelectOption('NO','NO'));               
            return options;
        }      
        
      Public String Q5a {get; set;} 
      public List<SelectOption> getQ5aItems() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('YES','YES'));
            options.add(new SelectOption('NO','NO'));               
            return options;
        }      
        
      Public String Q6 {get; set;} 
      public List<SelectOption> getQ6Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('YES','YES'));
            options.add(new SelectOption('NO','NO'));                     
            return options;
        }     
        
      Public String Q7 {get; set;} 
      public List<SelectOption> getQ7Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('YES','YES'));
            options.add(new SelectOption('NO','NO'));                       
            return options;
        }    
        
      Public String Q8 {get; set;} 
      public List<SelectOption> getQ8Items() {
            List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('None','-- None --'));
            options.add(new SelectOption('YES','YES'));
            options.add(new SelectOption('NO','NO'));                      
            return options;
        }           
   
   Public Integer NPLenth{get; set;}      
   Boolean DisplayQ2,DisplayQ3,DisplayQ3a,DisplayQ4,DisplayQ4a,DisplayQ5,DisplayQ678,DisplayQ9,DisplayErr=False;  
 
   
   public PageReference incrementQ1() {
   Q2='None';
   Q3='None';
   Q3a='None';
   Q4='None';
   NPLenth=NULL;
   Q5='None';
   Q5a='None';
   Q6='None';
   Q7='None';
   Q8='None';
     If(Q1=='Yes'){
       DisplayQ2=True;
       DisplayQ678=False;
       }
     Else
        If(Q1=='No'){
         DisplayQ678=True;
         DisplayQ2=False;
         DisplayQ3=False;
         DisplayQ3a=False;
         DisplayQ4=False;
         DisplayQ4a=False;
         DisplayQ5=False;
         }
        Else{
        DisplayQ678=False;
        DisplayQ2=False;
        DisplayQ3=False;
         DisplayQ3a=False;
         DisplayQ4=False;
         DisplayQ4a=False;
         DisplayQ5=False;
        }
        Return Null;
        }
        
   public PageReference incrementQ2() {
   Q3='None';
   Q3a='None';
   Q4='None';
   NPLenth=NULL;
   Q5='None';
   Q5a='None';
   Q6='None';
   Q7='None';
   Q8='None';
     If(Q2=='Yes'){
       DisplayQ3=True;
       DisplayQ678=False;
       }
     Else
        If(Q2=='No'){
         DisplayQ678=True;
         DisplayQ3=False;
         DisplayQ3a=False;
         DisplayQ4=False;
         DisplayQ4a=False;
         DisplayQ5=False;
         }
        Else{
        DisplayQ678=False;
        DisplayQ3=False;
        }
        Return Null;
        }    
        
     public PageReference incrementQ3() {
     Q3a='None';
     Q4='None';
     NPLenth=NULL;
     Q5='None';
     Q5a='None';
     Q6='None';
     Q7='None';
     Q8='None';
     DisplayQ5=False;
     DisplayQ3a=False;
     DisplayQ4=False;
     DisplayQ4a=False;
     
       If(Q3=='Changed Mind')
        DisplayQ5=True;
        
       Else If(Q3=='Notice Period Required'){ 
         DisplayQ4a=True;
         DisplayQ5=True;
         }
       Else If(Q3=='SP Made Better Offer'){ 
         DisplayQ3a=True;
         DisplayQ5=True;
         }
       Else If(Q3=='In Contract'||Q3=='HTT Charges too high'){ 
         DisplayQ4=True;
         DisplayQ5=True;
         }  
         
        Return Null;
        }   
   public PageReference incrementQ8() {
   DisplayQ9=False;
     If(Q8=='YES')
       DisplayQ9=True; 
     Return Null;  
    }           
   
   Public Boolean getDispQ2Section(){Return DisplayQ2;}  
   Public Boolean getDispQ3Section(){Return DisplayQ3;}
   Public Boolean getDispQ3aSection(){Return DisplayQ3a;}
   Public Boolean getDispQ4Section(){Return DisplayQ4;}
   Public Boolean getDispQ4aSection(){Return DisplayQ4a;}
   Public Boolean getDispQ5Section(){Return DisplayQ5;}
   Public Boolean getDispQ678Section(){Return DisplayQ678;}   
   Public Boolean getDispQ9Section(){Return DisplayQ9;}
   Public Boolean getDispErrSection(){Return DisplayErr;}
   
   String customerContacted = null;
   public List<SelectOption> getcustomerContactedItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('No','No')); 
        options.add(new SelectOption('Yes','Yes')); 
        options.add(new SelectOption('Email','Email')); 
        return options; 
    }
                   
    public String getcustomerContacted () {

        return customerContacted ;
    }
                    
    public void setcustomerContacted (String customerContacted ) { this.customerContacted = customerContacted ; } 
}