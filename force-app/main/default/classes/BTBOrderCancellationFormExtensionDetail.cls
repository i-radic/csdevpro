Public class BTBOrderCancellationFormExtensionDetail{

BTLB_Order_Cancellation_Form__c BOCF=new BTLB_Order_Cancellation_Form__c();
BTLB_Order_Cancellation_Form__c BOCFRec=new BTLB_Order_Cancellation_Form__c();
public BTLB_Order_Cancellation_Form__c getBTOCF() {return BOCFRec;}

 public BTBOrderCancellationFormExtensionDetail(ApexPages.StandardController controller) {   
 
 this.BOCF= (BTLB_Order_Cancellation_Form__c)controller.getRecord();
 BOCFRec=[Select Address__c,Broadband_Included__c,BTB_Order_Line__c,Call_Centre__c,Cancellation_Date__c,Cancellation_reason__c,Consent_CP__c,Agent_Contact_Telephone_Number__c,CSS_Order_Created_Date__c,CSS_Order_Number__c,Customer_Agreed_Date__c,Customer_Contacted__c,Customer_Required_By_Date__c,Customer_Telephone_Number__c,Agent_Extension__c,Form_Completion_Date__c,Form_Created_By_EIN__c,Form_Created_By_Name__c,Incident_Notes__c,Name_of_Contact__c,Losing_Supplier_Name__c,Number_of_Employees__c,
           Number_of_lines_involved__c,Ofcom_Consent__c,OUC__c,Location_OV__c,Company_Name_OV__c,Contact_Name_OV__c,
           Order_raised_by_EIN__c,Order_Created_By_Name__c,Order_Create_Date__c,BT_Order_Number__c,
           Telephone_Number_OV__c,Position_in_Company__c,Product_Ordered__c,
           Q1_Did_you_know_that_your_order_to_ret__c,Q2_Did_you_agree_to_the_cancellation_o__c,
           Q3a_Who_initiated_the_contact_when_dis__c,Q3_What_was_the_reason_for_agreeing_to__c,
           Q4a_What_is_the_length_of_your_notice__c,Q4_What_is_the_remaining_term_notice_p__c,
           Q5a_Did_you_contact_BT_to_cancel__c,Q5_Did_your_current_supplier_refer_you__c,
           Q6_Have_you_informed_your_current_supp__c,Q7_Do_you_want_us_to_reissue_the_order__c,
           Q8_Do_you_wish_to_complain__c,Sales_Channel__c,
           First_Actioned_By__c,First_Actioned_By_EIN__c,First_Actioned_Date_Time__c,
           Second_Actioned_By__c,Second_Actioned_By_EIN__c,Second_Actioned_Date_Time__c,
           Form_Submitted_By__c,Form_Submitted_By_EIN__c,Form_Submitted_Date_Time__c
           From BTLB_Order_Cancellation_Form__c where Id=:BOCF.Id];
 }   

}