global class RPActionClassPriceItem extends cscfga.ALookupSearch{
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
      List<cspmb__Price_Item__c>
      priceItems = [SELECT Id, Name, Minimum_Recurring_Price__c, Minimum_One_Off_Price__c, Discountable__c, BT_Hardware_Fund__c,
        BT_Technology_Fund__c, cspmb__Recurring_Charge__c, cspmb__Recurring_Cost__c
       FROM cspmb__Price_Item__c WHERE cspmb__Is_Active__c = true AND Record_Type_Name__c = 'BT Broadband Plan'];
      return priceItems;
    }

    public override String getRequiredAttributes(){
      return '[]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
      final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
      final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
      Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
        List<cspmb__Price_Item__c>
        priceItems = [
          SELECT Id, Name, Minimum_Recurring_Price__c, Minimum_One_Off_Price__c, Discountable__c, BT_Hardware_Fund__c,
        BT_Technology_Fund__c, cspmb__Recurring_Charge__c, cspmb__Recurring_Cost__c FROM cspmb__Price_Item__c WHERE cspmb__Is_Active__c = true AND 
        Record_Type_Name__c = 'BT Broadband Plan' ORDER BY Sequence__c asc
        LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT
        OFFSET :recordOffset
        ];
        return priceItems ;
    }
}