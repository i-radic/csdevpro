/*
###########################################################################
# File..................: TrialLineItemHelperTest.cls
# Version...............: 1
# Created by............: Martin Eley
# Created Date..........: 23rd August 2012
# Last Modified by......: Varun Pokhriyal
# Last Modified Date....: 4th April 2013
# Description...........: Test Class                         
# VF page...............:             
# Change Log:               
# Apex Class............: TrialLineItemHelper.cls
# Apex Trigger....... ..: TrialLineItemTrigger.cls
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#   
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
#
###########################################################################
*/
@isTest
(SeeAllData=True)
private class TrialLineItemHelperTest {
     
     static Account acc1 = null;
     static Contact contact1 = null;
     static Opportunity opp1 = null;
     static Trial__c aTrial = null;
     
     static SKU__c aTabletSKU = null;
     static Stock_Item__c aTabletSKUStockItem1 = null;
     static Stock_Item__c aTabletSKUStockItem2 = null;
     static Trial_Line_Item__c aTabletTrialLineItem = null;
     
     static SKU__c aNonTabletSKU = null;
     static Stock_Item__c aNonTabletSKUStockItem1 = null;
     static Stock_Item__c aNonTabletSKUStockItem2 = null;
     static Trial_Line_Item__c aNonTabletTrialLineItem = null;
     
     static List<SKU__c> skuLst = null;
     
     private static void prepareTestData(){
         
         List<Stock_Item__c> StockItems= new List<Stock_Item__c>();
         skuLst = new List<SKU__c>();
         
         acc1 = Test_Utils.createAccount('Test Account');
         contact1 = Test_Utils.createContact(null, null, acc1.Id, null);
         opp1 = Test_Utils.createOpp('Test Opportunity', 'Test Stage', Date.today() + 28, acc1.Id);
         aTrial =  Test_Utils.createTrial(opp1, contact1);
         
         aTabletSKU = Test_Utils.populateSKU('Test Tablet SKU1', 'Test Tablet SKU1', 99.99 , true, 'Micro', 'Device');
         aNonTabletSKU = Test_Utils.populateSKU('Test Tablet SKU2', 'Test Tablet SKU2', 11.99 , false, 'Nano', 'Device');
         skuLst.add(aTabletSKU);
         skuLst.add(aNonTabletSKU);
         insert(skuLst);
          
         aTabletSKUStockItem1 = Test_Utils.populateStockItem(skuLst[0], 'Active', 'Tablet1','07010101010');
         aTabletSKUStockItem2 = Test_Utils.populateStockItem(skuLst[0], 'Active', 'Tablet2','07020202020');
         
         aNonTabletSKUStockItem1 = Test_Utils.populateStockItem(skuLst[1], 'Active', 'NonTablet1','07030303030');
         aNonTabletSKUStockItem2 = Test_Utils.populateStockItem(skuLst[1], 'Active', 'NonTablet2','07040404040');
         
         StockItems.add(aTabletSKUStockItem1);
         StockItems.add(aTabletSKUStockItem2);
         StockItems.add(aNonTabletSKUStockItem1);
         StockItems.add(aNonTabletSKUStockItem2);
         insert StockItems;
         
         //Add line items to trial
        aTabletTrialLineItem = Test_Utils.createTrialLineItem(aTrial.Id, skuLst[0].Id, 1);
        aNonTabletTrialLineItem = Test_Utils.createTrialLineItem(aTrial.Id, skuLst[1].Id, 2);
        
     }
    
    static testMethod void testMethod1(){
        
        Test.startTest();
        prepareTestData();
        
        //Assert
        aTabletTrialLineItem = [SELECT Tablet_Device__c
            FROM Trial_Line_Item__c
            WHERE Id = :aTabletTrialLineItem.Id];
            
        aNonTabletTrialLineItem = [SELECT Tablet_Device__c
            FROM Trial_Line_Item__c
            WHERE Id = :aNonTabletTrialLineItem.Id];
            
        System.assertEquals(TRUE, aTabletTrialLineItem.Tablet_Device__c);
        System.assertEquals(FALSE, aNonTabletTrialLineItem.Tablet_Device__c);
        Test.stopTest();
    
    }   
}