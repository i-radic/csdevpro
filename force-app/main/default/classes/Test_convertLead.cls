@isTest
public class Test_convertLead {
    static testmethod void TestConvert(){
        TriggerDeactivating__c dt = new TriggerDeactivating__c();
        dt.Account__c = True;
        insert dt;
        
        Account acc=new Account(name='testacc');
        insert acc;
        Account acc1=new Account(name='testacc1');
        insert acc1;
        Contact con=new Contact(lastname='testcon',accountid=acc.Id);
        insert con;
        Lead ld=new Lead();
        ld.RecordtypeId=[Select Id from RecordType where sObjectType='Lead' and DeveloperName='Default'].Id;
        ld.FirstName= 'TestLead';
        ld.LastName = 'TestLead2';
        ld.Company ='Test Acc';
        ld.Status ='Not Started';
        ld.LeadSource ='EE';
        ld.Account__c=acc.id;
        ld.Contact__c=con.id;
        Insert ld;
        string str =  ld.id;
        convertLead.convertLeadMethod(str);
        
        
        Lead ld1=new Lead();
        ld1.RecordtypeId=[Select Id from RecordType where sObjectType='Lead' and DeveloperName='Default'].Id;
        ld1.FirstName= 'TestLeadEE1';
        ld1.LastName = 'TestLeadEE2';
        ld1.Company ='Test Acc';
        ld1.Status ='Not Started';
        ld1.LeadSource ='Astute';
        ld1.Account__c=acc.id;
        ld1.Contact__c=con.id;
        Insert ld1;
        
    }
    
}