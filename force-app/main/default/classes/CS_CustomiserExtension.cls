/**
 * Interface class to allow dynamic class instantiation
 *
 * NO LONGER USED
 * 
 */
global interface CS_CustomiserExtension {
	/**
	 * Returns additional html to be used on configurator page
	 *
	 * @param ctrl
	 * @return String 
	 */
	String getTopComponentHtml(cscfga.ProductConfiguratorController ctrl);

}