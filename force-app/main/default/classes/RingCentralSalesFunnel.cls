public class RingCentralSalesFunnel {
    public Cloud_Voice__c cvRec;
    public Cloud_Voice__c cvRec1;
    public String sign {get; set;}
    public String customerFirstName {get; set;}
    public String customerLastName {get; set;}
    public String customerEmail {get; set;}
    public String customerContactNumber {get; set;}
    public String customerCompanyName {get; set;}
    public String oppID {get; set;}
    public String cug {get; set;}
    public String salesAgentID {get; set;}
    public String lbID {get; set;}
    public String numberUsers {get; set;}
    public String editionType {get; set;}
    public String testID {get; set;}
    public String mosScore {get; set;}
    public String simultaneousUsers {get; set;}
    public String bandwidthResult {get; set;}
    public String accountActivationDateDD {get; set;}
    public String accountActivationDateMM {get; set;}
    public String accountActivationDateYYYY {get; set;}
    public String accountActivationDate {get; set;}
    public String contractEndDateDD {get; set;}
    public String contractEndDateMM {get; set;}
    public String contractEndDateYYYY {get; set;}
    public String contractEndDate {get; set;}
    public String initialPurchaseCreditLimit {get; set;}
    public String recurringMonthlyCreditLimit {get; set;}
    public String referenceNoteCreditChange {get; set;}
    public String shippingAddressCity {get; set;}
    public String shippingAddressCounty {get; set;}
    public String shippingAddressLine1 {get; set;}
    public String shippingAddressLine2 {get; set;}
    public String shippingAddressPostCode {get; set;}
    public String shippingAddress {get; set;}
    public String rcDealID {get; set;}  
    public String CV = System.currentPageReference().getParameters().get('id');
    public String url {get; set;}


    public RingCentralSalesFunnel(ApexPages.StandardController controller) {
        this.cvRec = [SELECT ID, RCDeal_ID__c, zLicenseType__c, zBandwidthID__c, zBandwidthMosScore__c, zBandwidthSimultaneousUsers__c, zInitialPurchaseCreditLimit__c, zRecurringMonthlyCreditLimit__c, zReferenceNoteCreditChange__c, Opportunity__r.Account.Name, Total_Licenses__c, Opportunity__r.Account_CUG__c, Opportunity__r.Account.AccountClonedID__c, Opportunity__r.Opportunity_Id__c, Opportunity__r.Owner__r.EIN__c, Opportunity__r.Owner__r.Department, Cloud_Phone_Main_Number__c,Cloud_Phone_Account_ID__c FROM Cloud_Voice__c WHERE ID =: CV];
        this.cvRec1= (Cloud_Voice__c)controller.getRecord();
    }

    public void SetTestHarnessValues(){
        //--------RC Security Validation
        Blob var1 = Blob.valueOf('12345qwerty');
        Blob var2 = Blob.valueOf('12345qwerty|');
        Blob signature = System.Crypto.signWithCertificate('RSA-SHA256', var1, 'SalesFunnel');
        sign = EncodingUtil.base64Encode(var2) + EncodingUtil.base64Encode(signature);
    }    

    public void SetValues(){
        Cloud_Voice_Site__c site = getCVSites()[0];
        //--------RC Security Validation
        Blob var1 = Blob.valueOf('12345qwerty');
        Blob var2 = Blob.valueOf('12345qwerty|');
        Blob signature = System.Crypto.signWithCertificate('RSA-SHA256', var1, 'SalesFunnel');
        sign = EncodingUtil.base64Encode(var2) + EncodingUtil.base64Encode(signature);
        //--------Endpoint URL
        RingCentralURL__c RSUrl = null;
        if (Test.isRunningTest()){   
            RSUrl = [select Name, Username__c, Password__c, Url__c, Partner_Login_Url__c from RingCentralURL__c where Name = 'testRCurl'];
        }else{
            RSUrl = [select Name, Username__c, Password__c, Url__c from RingCentralURL__c where Name = 'SalesFunnel'];
        }
        url = String.valueOf(RSUrl.Url__c);
        //--------User
        salesAgentID = cvRec.Opportunity__r.Owner__r.EIN__c;
        //--------Contact
        Contact contact = [SELECT ID, Contact_Post_Code__c, Address_County__c, Contact_Post_Town__c, Contact_Address_Street__c, Contact_Address_Number__c, FirstName, LastName, Email, Email2__c, Phone FROM Contact WHERE ID =: site.Site_contact__c];
        customerFirstName = contact.FirstName;
        customerLastName = contact.LastName;
        customerEmail = contact.Email2__c;
        customerContactNumber = contact.Phone;
        //--------Other details
        if (cvRec.Opportunity__r.Account.Name.length() > 64){
            customerCompanyName = cvRec.Opportunity__r.Account.Name.SubString(0, 64);
        }
        else{
            customerCompanyName = cvRec.Opportunity__r.Account.Name;
        } 
        oppID = cvRec.Opportunity__r.Opportunity_Id__c;
        cug = cvRec.Opportunity__r.Account_CUG__c;
        lbID = cvRec.Opportunity__r.Owner__r.Department;
        //--------CUG fix for Business Partner Sales (BPS)
        if(cug.startsWith('001')){
            cug = cvRec.Opportunity__r.Account.AccountClonedID__c;
        }
        //--------Product
        for (Integer i = 0; i < site.Site_Products__r.Size(); i++) { 
            if(site.Site_Products__r[i].Type__c == 'License'){
                numberUsers = String.valueOf(site.Site_Products__r[i].Quantity__c);
                //-------- Contract End Date
                try{
                    Date endDate = site.Customer_Required_Date__c.addMonths(Integer.valueOf(site.Site_Products__r[i].Term__c.substring(0,2)));
                    contractEndDateDD = String.valueOf(endDate.Day());
                    contractEndDateMM = String.valueOf(endDate.Month());
                    contractEndDateYYYY = String.valueOf(endDate.Year());
                }catch(exception e){}    
            }
        }
        //--------Account Activation Date
        try{
            accountActivationDateDD = String.valueOf(site.Customer_Required_Date__c.Day());
            accountActivationDateMM = String.valueOf(site.Customer_Required_Date__c.Month());
            accountActivationDateYYYY = String.valueOf(site.Customer_Required_Date__c.Year());
        }catch(exception e){}
        editionType = cvRec.zLicenseType__c;
        //--------Bandwidth
        testID = cvRec.zBandwidthID__c;
        mosScore = cvRec.zBandwidthMosScore__c;
        simultaneousUsers = cvRec.zBandwidthSimultaneousUsers__c;
        //-------- Credit - change to new default values and catch inflight missing
    Decimal iVal = site.Total_Site_One_Off_Cost__c;
    Decimal rVal = site.Total_Site_Recurring_Cost__c;
    Decimal tVal = iVal + rVal;
        //changed to remove check on existing values   
        if(tVal > 2500){
            initialPurchaseCreditLimit = String.valueOf(tVal);
        }else{            
            initialPurchaseCreditLimit = '2500';
        }
        if(rVal > 850){
            recurringMonthlyCreditLimit = String.valueOf(rVal*1.10);
        }else{            
            recurringMonthlyCreditLimit = '850';   
        }
    referenceNoteCreditChange = 'Default Values';  
        //--------Shipping Address
        shippingAddressLine1 = contact.Contact_Address_Number__c;
        shippingAddressLine2 = contact.Contact_Address_Street__c;
        shippingAddressCity = contact.Contact_Post_Town__c;
        shippingAddressCounty = contact.Address_County__c;
        shippingAddressPostCode = contact.Contact_Post_Code__c;
        rcDealID = cvRec.RCDeal_ID__c;
    }

    public List<Cloud_Voice_Site__c> getCVSites() {
        List<Cloud_Voice_Site__c> CVSites = [SELECT ID, Site_name__c, Site_Contact__r.Name, Site_Contact__r.Phone, Site_Contact__r.Email, Site_Contact__r.Email2__c, Site_address__c, Total_Site_Recurring_Cost__c, Total_Site_One_Off_Cost__c, Total_Site_Licenses__c, Users__c, 
        Structured_Cabling_Summary__c,LAN_Summary__c, Billing_address__c, Delivery_address__c,Access_Concurrent_Calls__c,Access_Codec__c,Access_Codec_Name__c,Site_Discounts_Initial__c,Site_Discounts_Recurring__c,Access_product_decision__c, Access_product_price__c,
        Access_Sale__c,Access_Cease__c,Access_Data_Traffic__c,Access_Max_UL__c,Access_Speed_UL__c,Access_Product__c,Access_Hub_Upgrade__c,Access_firewall__c,CPE_Multiple_Delivery_Addresses__c,Total_CPE__c,
        Server_Administrator__c,Total_CV_Basic_Licenses__c ,Total_CV_Connect_Licenses__c,Total_CV_Collaborate_Licenses__c,Total_Lines__c,Total_New_Lines__c,LAN_solution_provider__c, zCablingOCS__c,zCPEInstall__c ,
        Existing_LAN_Support_PoE__c,LAN_Peripheral_Device_Ports__c,Customer_Required_Date__c,Power__c,Structured_cabling_provider__c,LAN_Max_Ports__c,SC_Installation_Date__c,zCVConfig__c, Total_Discount__c,Total_Site_1st_Year_Cost__c, RecordType.Name,zAccessProvision__c, Total_Selected_Finance__c ,
            (SELECT ID, rcSTD__c, rcDirectory__c, Product_name__c, Quantity__c, Unit_Cost__c, Total_Recurring_Cost__c, Total_One_Off_Cost__c, Cloud_Voice_Site__c, Cloud_Voice_Site__r.Site_name__c, Type__c,Grouping__c,Image__c,Purchasing_Option__c,Discount_Initial__c, Number_Info__c,Finance_Option__c, Term__c FROM Site_Products__r ORDER BY Type__c, Product_Name__c) 
        FROM Cloud_Voice_Site__c WHERE CV__c = :CV ORDER BY Site_Name__c];
        return CVSites;
    }

    public PageReference FinishButton(){
        update cvRec1;
        PageReference retPg = new PageReference('/' + cvRec.ID);
        return retpg.setRedirect(true);
    }
}