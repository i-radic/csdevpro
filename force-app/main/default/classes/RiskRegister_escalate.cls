public class RiskRegister_escalate {

  public Risk_Register__c risk = null; 
  public id loggedinID = UserInfo.getUserId();  
  
  public PageReference escalate() {
 
    
   if (this.risk != null) {
      Id riskId = risk.Id; 
            
      List<Risk_Register__c> risklist = [select Id, Risk_Escalated_To__c, UVS_SM__c, UVS_DGM__c, UVS_GM__c from Risk_Register__c where Id = :riskId];
      risk = risklist[0];
 
if ((risk.Risk_Escalated_To__c == risk.UVS_SM__c) && (risk.Risk_Escalated_To__c == loggedinID)){
risk.Risk_Escalated_To__c = risk.UVS_DGM__c;
update risk; 
//Create task to advise of escalation
Task task1 = new Task();
        task1.OwnerId = risk.Risk_Escalated_To__c;
        task1.ActivityDate = (Date.today()+7);
        task1.ReminderDateTime = datetime.newInstance(Date.today().year(),Date.today().month(),Date.today().day(),8,0,0);
        task1.ISREMINDERSET = TRUE;
        task1.Target_Completion_Date__c = (Date.today()+ 7);
        task1.Subject = 'UVS Risk Register Escalation to UVS DGM';
        task1.WhatId = risk.Id;
        task1.Task_Category__c = 'UVS Risk Register Escalation';
        insert task1;
 
}
if ((risk.Risk_Escalated_To__c == risk.UVS_DGM__c) && (risk.Risk_Escalated_To__c == loggedinID)){
risk.Risk_Escalated_To__c = risk.UVS_GM__c;
update risk; 
//Create task to advise of escalation
Task task1 = new Task();
        task1.OwnerId = risk.Risk_Escalated_To__c;
        task1.ActivityDate = (Date.today()+7);
        task1.ReminderDateTime = datetime.newInstance(Date.today().year(),Date.today().month(),Date.today().day(),8,0,0);
        task1.ISREMINDERSET = TRUE;
        task1.Target_Completion_Date__c = (Date.today()+ 7);
        task1.Subject = 'UVS Risk Register Escalation to UVS GM';
        task1.WhatId = risk.Id;
        task1.Task_Category__c = 'UVS Risk Register Escalation';
        insert task1;

}
 
   
// go back to original page
            PageReference pr = new PageReference('/'+riskId);
            pr.setRedirect(true);
         return pr;
    }
        return null;
    }
   public RiskRegister_escalate (ApexPages.StandardController stdController) {
   risk = (Risk_Register__c)stdController.getRecord();
            }
    public RiskRegister_escalate () {
    }
  
}