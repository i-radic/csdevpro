/**************************************************************************************************************************************************
    Class Name : ServiceImprovementPlanTriggerHandler
    Test Class Name : Test_CustomerExpClientplanBefore
    Description : Code to Insert and Update Customer Experience client plan records. 
    Version : V0.1
    Created By Author Name : BALAJI MS
    Date : 08/09/2017
 *************************************************************************************************************************************************/

public class ServiceImprovementPlanTriggerHandler {
    
    public static Boolean BeforeUpdateRecursionChecker = True;
    
    public static void BeforeInsertTriggerHandler(List<Service_Improvement_Plan__c> ServiceImprovementList){
        
        ServiceImprovementPlanTriggerHelper.BeforeInsertTrigger(ServiceImprovementList);
        
    }
    
    public static void BeforeUpdateTriggerHandler(Map<Id, Service_Improvement_Plan__c> NewServicePlanMap, Map<Id, Service_Improvement_Plan__c> OldServicePlanMap){
       
        ServiceImprovementPlanTriggerHelper.BeforeUpdateTrigger(NewServicePlanMap, OldServicePlanMap);
        
    }
    
}