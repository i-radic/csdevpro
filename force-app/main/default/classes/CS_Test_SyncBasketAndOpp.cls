@isTest
private class CS_Test_SyncBasketAndOpp {
    private static cscfga__Product_Basket__c testBasket;
    
    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        // create account
        Account testAcc = new Account();
        testAcc.Name = 'TestAccount';
        testAcc.NumberOfEmployees = 1;
        insert testAcc; 
        
        // there is no possiblity to create standard pricebook2 in test so seeAllData must be set to true
         Id pricebookId = Test.getStandardPricebookId();
        
        // create opportunity
        Opportunity testOpp = new Opportunity();    
        testOpp.Name = 'Online Order: ' + testAcc.Name + ':'+datetime.now();
        testOpp.AccountId = testAcc.Id;
        testOpp.CloseDate = System.today();
        testOpp.StageName = 'Changes Over Time';   //@TODO - This should be a configuration
        testOpp.TotalOpportunityQuantity = 0; // added this as validation rule was complaining it was missing
        testOpp.pricebook2Id = pricebookId; // needed for sync
        insert testOpp;

        // create baskets
        testBasket = new cscfga__Product_Basket__c();
        Datetime d = system.now();
        String strDatetime = d.format('yyyy-MM-dd HH:mm:ss');
        testBasket.Name = 'Test Order ' + strDatetime;
        testBasket.cscfga__Opportunity__c = testOpp.Id;
        insert testBasket;
        
        cscfga__Product_Basket__c newTestBasket = new cscfga__Product_Basket__c();
        Datetime dNew = system.now();
        String strDatetimeNew = dNew.format('yyyy-MM-dd HH:mm:ss');
        newTestBasket.Name = 'Test Order New' + strDatetimeNew;
        newTestBasket.cscfga__Opportunity__c = testOpp.Id;
        insert newTestBasket;
        
        // create product definition
        String offerId = null;
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c(Name='Landline Test', cscfga__Description__c = 'Test helper');
        insert prodDef;
        
        // create configuration
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c();
        config.cscfga__Product_Basket__c = testBasket.Id;
        config.cscfga__Product_Definition__c = prodDef.Id;
        if (offerId != null)
            config.cscfga__Configuration_Offer__c = offerId;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.cscfga__Unit_Price__c = 10;
        config.cscfga__Quantity__c = 1;
        config.cscfga__Recurrence_Frequency__c = 12;
        insert config;
    }

    private static testMethod void testBasketSync() {
        CreateTestData();
        
        Test.StartTest();
        
        CS_SyncBasketAndOpp.SyncBasket(testBasket.Id);
        List<cscfga__Product_Basket__c> ProductBasket = [select Id, cscfga__Opportunity__c, Synchronised_with_Opportunity__c
            from cscfga__Product_Basket__c
            where cscfga__Opportunity__c = :testBasket.cscfga__Opportunity__c AND Synchronised_with_Opportunity__c = true];
        //System.assert(ProductBasket.size() == 1);
        //System.assert(ProductBasket.get(0).Id == testBasket.Id);
        
        Test.StopTest();
    }
}