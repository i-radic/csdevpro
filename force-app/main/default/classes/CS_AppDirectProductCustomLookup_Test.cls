@isTest
private class CS_AppDirectProductCustomLookup_Test {

    @isTest
    private static void getRequiredAttributes_Test() {
        String result;
        Test.startTest();
            CS_AppDirectProductCustomLookup appDirectProductLookup = new CS_AppDirectProductCustomLookup();
            result = appDirectProductLookup.getRequiredAttributes();
        Test.stopTest();
        System.assertEquals('["BrandId","Brand","Group Name"]', result);
    }
    
     @isTest
    private static void doLookupSearch_Test() {
        Object[] result;
        cspmb__Price_Item__c appDirectBrand = CS_TestDataFactory.generatePriceItem(TRUE, 'Test App Direct Brand');
        cspmb__Add_On_Price_Item__c appDirectProduct = CS_TestDataFactory.generateAddOnPriceItem(FALSE, 'Test App Direct Product');
        appDirectProduct.cspmb__Is_Active__c = TRUE;
        appDirectProduct.cspmb__Contract_Term__c = '12';
        INSERT appDirectProduct;
        cspmb__Price_Item_Add_On_Price_Item_Association__c priceItemAddOnAssoc = CS_TestDataFactory.generatePriceItem(FALSE, appDirectProduct, appDirectBrand);
        priceItemAddOnAssoc.Module__c = 'Test App Direct Brand';
        priceItemAddOnAssoc.cspmb__Group__c = 'Microsoft 365 | Business';
        INSERT priceItemAddOnAssoc;
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('BrandId', appDirectBrand.Id);
        searchFields.put('Brand', appDirectBrand.Name);
        searchFields.put('Group Name', priceItemAddOnAssoc.cspmb__Group__c);
        Test.startTest();
            CS_AppDirectProductCustomLookup appDirectProductLookup = new CS_AppDirectProductCustomLookup();
            result = appDirectProductLookup.doLookupSearch(searchFields, '', NULL, 10, 10);
        Test.stopTest();
        System.assertEquals(1, result.size());
    }
}