public with sharing class createCRFAddress{

    public Address_Details__c addressDet{get; set;} 
    public string refferalPage;
    public string selectedAddr1 = ApexPages.currentPage().getParameters().get('AddType1');  
    
    public createCRFAddress(ApexPages.StandardController stdController) {

         addressDet = (Address_Details__c)stdController.getRecord();
    }
    
    public createCRFAddress(Address_Details__c addressDet_edit){
         addressDet = addressDet_edit;
    }
    
    public PageReference OnLoad() { 
         refferalPage = ApexPages.currentPage().getParameters().get('refPage');
         if(selectedAddr1=='ExistingAddress'){
         addressDet.Existing_Premises_Building_Name__c = '';
         addressDet.Existing_Sub_Premises__c = '';
         addressDet.Existing_Thoroughfare_No_Street_Number__c = '';
        // Address_Details__c.Address_POBox__c = '';
         addressDet.Existing_Thoroughfare_Name_Street_Name__c = '';
         addressDet.Existing_Locality__c = '';
         addressDet.Existing_Post_Town__c = '';
         addressDet.Existing_County__c = '';
         addressDet.Existing_Country__c = 'UK';
         }
         
         if(selectedAddr1=='CeaseAddress'){
         addressDet.Cease_Premises_Building_Name__c = '';
         addressDet.Cease_Sub_Premises__c = '';
         addressDet.Cease_Thoroughfare_Number_Street_Number__c = '';
        // Address_Details__c.Address_POBox__c = '';
         addressDet.Cease_Thoroughfare_Name_Street_Name__c = '';
         addressDet.Cease_Locality__c = '';
         addressDet.Cease_Post_Town__c = '';
         addressDet.Cease_County__c = '';
         addressDet.Cease_Country__c = 'UK';
         }
         
         
         
         if(selectedAddr1=='CorrespondenceAddress'){
         addressDet.Legal_Premises_Building_Name__c = '';
         addressDet.Legal_Sub_Premises__c = '';
         addressDet.LegalThoroughfare_Number_StreetNumber__c = '';
         addressDet.Legal_Thoroughfare_Name_Street_Name__c = '';
         addressDet.Legal_Locality__c = '';
         addressDet.Legal_Post_Town__c = '';
         addressDet.Legal_County__c = '';
         addressDet.Legal_Country__c = 'UK';
         addressDet.Legal_PO_Box__c = '';
         }
         if(selectedAddr1=='DeliveryAddress'){
         addressDet.Provide_Premises_Building_Name__c = '';
         addressDet.Provide_Sub_Premises__c = '';
         addressDet.Provide_Thoroughfare_Number_Street_Numb__c = '';
         addressDet.Provide_Thoroughfare_Name_Street_Name__c = '';
         addressDet.Provide_Locality__c = '';
         addressDet.Provide_Post_Town__c = '';
         addressDet.Provide_County__c = '';
         addressDet.Provide_Country__c = 'UK';
         addressDet.Provide_PO_Box__c = '';
         }
         if(selectedAddr1=='BillingAddress'){
         addressDet.Billing_Premises_Building_Name__c = '';
         addressDet.Billing_Sub_Premises__c = '';
         addressDet.Billing_Thoroughfare_Number_Street_Numb__c = '';
         addressDet.Billing_Thoroughfare_Name_Street_Name__c = '';
         addressDet.Billing_Locality__c = '';
         addressDet.Billing_Post_Town__c = '';
         addressDet.Billing_County__c = '';
         addressDet.Billing_Country__c = 'UK';
         addressDet.Billing_PO_Box__c = '';
         }
         
         
         
         if(selectedAddr1=='ProvideAddress'){
         addressDet.Provide_Premises_Building_Name__c = '';
         addressDet.Provide_Sub_Premises__c = '';
         addressDet.Provide_Thoroughfare_Number_Street_Numb__c = '';
        // Address_Details__c.Address_POBox__c = '';
         addressDet.Provide_Thoroughfare_Name_Street_Name__c = '';
         addressDet.Provide_Locality__c = '';
         addressDet.Provide_Post_Town__c = '';
         addressDet.Provide_County__c = '';
         addressDet.Provide_Country__c = 'UK';
         }
         if(selectedAddr1=='AlternateBillingAddress'){
         addressDet.Billing_Premises_Building_Name__c = '';
         addressDet.Billing_Sub_Premises__c = '';
         addressDet.Billing_Thoroughfare_Number_Street_Numb__c = '';
        // Address_Details__c.Address_POBox__c = '';
         addressDet.Billing_Thoroughfare_Name_Street_Name__c = '';
         addressDet.Billing_Locality__c = '';
         addressDet.Billing_Post_Town__c = '';
         addressDet.Billing_County__c = '';
         addressDet.Billing_Country__c = 'UK';
         } 
         return null;         
   }
   
   public PageReference Save() {
       If (ValidateMandatoryFields() != true){
            return null;
       }      
       try {                    
           string NADKey;
           if(Test_Factory.GetProperty('IsTest') == 'yes') {
               NADKey = 'test';
           }           
           else {
               NADKey = SalesforceCRMService.CreateNADAddress(crmAddress());
           }                
           if (NADKey == '' || 
               NADKey == null){                  
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The NAD service has rejected the request, please enter a valid address!'));                               
               return null;
           } 
           else {
               addressDet.CRF_address_Id__c = NADKey;
               if(Test_Factory.GetProperty('IsTest') == 'yes') {                         
               }
               else {
                   upsert addressDet;                 
               }    
               return ReDirect();                          
           }                  
       }
       catch (System.CalloutException ex){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There appears to be problem connecting to the NAD service, please try again!'));
           return null;
       }
   }
   
   /* -- singhd62
   public PageReference Save() {
       If (ValidateMandatoryFields() != true){
            return null;
       }      
       SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();
       service.timeout_x = 60000;
       try {                    
           string NADKey;
           if(Test_Factory.GetProperty('IsTest') == 'yes') {
               NADKey = 'test';
           }           
           else {
               NADKey = service.CreateNADAddress('', '', crmAddress());
           }                
           if (NADKey == '' ||
               NADKey == null){                  
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'The NAD service has rejected the request, please enter a valid address!'));                               
               return null;
           } 
           else {
               addressDet.CRF_address_Id__c = NADKey;
               if(Test_Factory.GetProperty('IsTest') == 'yes') {                         
               }
               else {
                   upsert addressDet;                 
               }    
               return ReDirect();                          
           }                  
       }
       catch (System.CalloutException ex){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There appears to be problem connecting to the NAD service, please try again!'));
           return null;
       }
   }
   */
   public PageReference Cancel(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
        if (refferalPage == 'new'){
            PageReference PageRef = new PageReference('/apex/CRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
                       
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }    
            return PageRef;                       
        }
        if (refferalPage == 'update'){
            PageReference PageRef = new PageReference('/apex/CRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }   
            return PageRef;    
        }
        if (refferalPage == 'detail'){
            PageReference PageRef = new PageReference('/'+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }      
            return PageRef;  
        }
        return null;
    }
    /* singhd62
    public SalesforceServicesCRM.Address crmAddress(){
    
        SalesforceServicesCRM.Address crmAddress = new SalesforceServicesCRM.Address();
        if(selectedAddr1=='ExistingAddress'){
        crmAddress.Name = addressDet.Existing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Existing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Existing_Thoroughfare_No_Street_Number__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Existing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Existing_Locality__c;
        crmAddress.Town = addressDet.Existing_Post_Town__c;
        crmAddress.County = addressDet.Existing_County__c; 
        crmAddress.Country = addressDet.Existing_Country__c; 
        crmAddress.Postcode = addressDet.Existing_Post_Code__c;
        }
        
        if(selectedAddr1=='CeaseAddress'){
        crmAddress.Name = addressDet.Cease_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Cease_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Cease_Thoroughfare_Number_Street_Number__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Cease_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Cease_Locality__c;
        crmAddress.Town = addressDet.Cease_Post_Town__c;
        crmAddress.County = addressDet.Cease_County__c; 
        crmAddress.Country = addressDet.Cease_Country__c; 
        crmAddress.Postcode = addressDet.Cease_Post_Code__c;
        }
        
        
        
        
        if(selectedAddr1=='CorrespondenceAddress'){
        crmAddress.Name = addressDet.Legal_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Legal_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.LegalThoroughfare_Number_StreetNumber__c;
        crmAddress.POBox = addressDet.Legal_PO_Box__c;
        crmAddress.Street = addressDet.Legal_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Legal_Locality__c;
        crmAddress.Town = addressDet.Legal_Post_Town__c;
        crmAddress.County = addressDet.Legal_County__c; 
        crmAddress.Country = addressDet.Legal_Country__c; 
        crmAddress.Postcode = addressDet.Legal_Post_Code__c;
        }
        if(selectedAddr1=='DeliveryAddress'){
        crmAddress.Name = addressDet.Provide_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Provide_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Provide_Thoroughfare_Number_Street_Numb__c;
        crmAddress.POBox = addressDet.Provide_PO_Box__c;
        crmAddress.Street = addressDet.Provide_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Provide_Locality__c;
        crmAddress.Town = addressDet.Provide_Post_Town__c;
        crmAddress.County = addressDet.Provide_County__c; 
        crmAddress.Country = addressDet.Provide_Country__c; 
        crmAddress.Postcode = addressDet.Provide_Post_Code__c;
        }
        if(selectedAddr1=='BillingAddress'){
        crmAddress.Name = addressDet.Billing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Billing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Billing_Thoroughfare_Number_Street_Numb__c;
        crmAddress.POBox = addressDet.Billing_PO_Box__c;
        crmAddress.Street = addressDet.Billing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Billing_Locality__c;
        crmAddress.Town = addressDet.Billing_Post_Town__c;
        crmAddress.County = addressDet.Billing_County__c; 
        crmAddress.Country = addressDet.Billing_Country__c; 
        crmAddress.Postcode = addressDet.Billing_Post_Code__c;
        }
        
        
        
        if(selectedAddr1=='ProvideAddress'){
        crmAddress.Name = addressDet.Provide_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Provide_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Provide_Thoroughfare_Number_Street_Numb__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Provide_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Provide_Locality__c;
        crmAddress.Town = addressDet.Provide_Post_Town__c;
        crmAddress.County = addressDet.Provide_County__c; 
        crmAddress.Country = addressDet.Provide_Country__c; 
        crmAddress.Postcode = addressDet.Provide_Post_Code__c;
        }
        if(selectedAddr1=='AlternateBillingAddress'){    
        crmAddress.Name = addressDet.Billing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Billing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Billing_Thoroughfare_Number_Street_Numb__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Billing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Billing_Locality__c;
        crmAddress.Town = addressDet.Billing_Post_Town__c;
        crmAddress.County = addressDet.Billing_County__c; 
        crmAddress.Country = addressDet.Billing_Country__c; 
        crmAddress.Postcode = addressDet.Billing_Post_Code__c;
        }
        return crmAddress; 
    }
    */
    
    public SalesforceCRMService.CRMAddress crmAddress(){
    
        SalesforceCRMService.CRMAddress crmAddress = new SalesforceCRMService.CRMAddress();
        if(selectedAddr1=='ExistingAddress'){
        crmAddress.Name = addressDet.Existing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Existing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Existing_Thoroughfare_No_Street_Number__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Existing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Existing_Locality__c;
        crmAddress.Town = addressDet.Existing_Post_Town__c;
        crmAddress.County = addressDet.Existing_County__c; 
        crmAddress.Country = addressDet.Existing_Country__c; 
        crmAddress.Postcode = addressDet.Existing_Post_Code__c;
        }
        
        if(selectedAddr1=='CeaseAddress'){
        crmAddress.Name = addressDet.Cease_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Cease_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Cease_Thoroughfare_Number_Street_Number__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Cease_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Cease_Locality__c;
        crmAddress.Town = addressDet.Cease_Post_Town__c;
        crmAddress.County = addressDet.Cease_County__c; 
        crmAddress.Country = addressDet.Cease_Country__c; 
        crmAddress.Postcode = addressDet.Cease_Post_Code__c;
        }
        
        
        
        
        if(selectedAddr1=='CorrespondenceAddress'){
        crmAddress.Name = addressDet.Legal_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Legal_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.LegalThoroughfare_Number_StreetNumber__c;
        crmAddress.POBox = addressDet.Legal_PO_Box__c;
        crmAddress.Street = addressDet.Legal_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Legal_Locality__c;
        crmAddress.Town = addressDet.Legal_Post_Town__c;
        crmAddress.County = addressDet.Legal_County__c; 
        crmAddress.Country = addressDet.Legal_Country__c; 
        crmAddress.Postcode = addressDet.Legal_Post_Code__c;
        }
        if(selectedAddr1=='DeliveryAddress'){
        crmAddress.Name = addressDet.Provide_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Provide_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Provide_Thoroughfare_Number_Street_Numb__c;
        crmAddress.POBox = addressDet.Provide_PO_Box__c;
        crmAddress.Street = addressDet.Provide_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Provide_Locality__c;
        crmAddress.Town = addressDet.Provide_Post_Town__c;
        crmAddress.County = addressDet.Provide_County__c; 
        crmAddress.Country = addressDet.Provide_Country__c; 
        crmAddress.Postcode = addressDet.Provide_Post_Code__c;
        }
        if(selectedAddr1=='BillingAddress'){
        crmAddress.Name = addressDet.Billing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Billing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Billing_Thoroughfare_Number_Street_Numb__c;
        crmAddress.POBox = addressDet.Billing_PO_Box__c;
        crmAddress.Street = addressDet.Billing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Billing_Locality__c;
        crmAddress.Town = addressDet.Billing_Post_Town__c;
        crmAddress.County = addressDet.Billing_County__c; 
        crmAddress.Country = addressDet.Billing_Country__c; 
        crmAddress.Postcode = addressDet.Billing_Post_Code__c;
        }
        
        
        
        if(selectedAddr1=='ProvideAddress'){
        crmAddress.Name = addressDet.Provide_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Provide_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Provide_Thoroughfare_Number_Street_Numb__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Provide_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Provide_Locality__c;
        crmAddress.Town = addressDet.Provide_Post_Town__c;
        crmAddress.County = addressDet.Provide_County__c; 
        crmAddress.Country = addressDet.Provide_Country__c; 
        crmAddress.Postcode = addressDet.Provide_Post_Code__c;
        }
        if(selectedAddr1=='AlternateBillingAddress'){    
        crmAddress.Name = addressDet.Billing_Premises_Building_Name__c;
        crmAddress.SubBuilding = addressDet.Billing_Sub_Premises__c;
        crmAddress.BuildingNumber = addressDet.Billing_Thoroughfare_Number_Street_Numb__c;
        //crmAddress.POBox = addressDet.Address_POBox__c;
        crmAddress.Street = addressDet.Billing_Thoroughfare_Name_Street_Name__c;
        crmAddress.Locality = addressDet.Billing_Locality__c;
        crmAddress.Town = addressDet.Billing_Post_Town__c;
        crmAddress.County = addressDet.Billing_County__c; 
        crmAddress.Country = addressDet.Billing_Country__c; 
        crmAddress.Postcode = addressDet.Billing_Post_Code__c;
        }
        return crmAddress; 
    }
    
    private boolean ValidateMandatoryFields() {
    
    if(selectedAddr1=='ExistingAddress'){
        if (addressDet.Existing_Thoroughfare_Name_Street_Name__c == '' ||
            addressDet.Existing_Thoroughfare_Name_Street_Name__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Existing_Post_Town__c == '' ||
            addressDet.Existing_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Existing_Post_Code__c == '' ||
            addressDet.Existing_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Existing_County__c == '' ||
            addressDet.Existing_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Existing_Country__c == '' ||
            addressDet.Existing_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    
    
    if(selectedAddr1=='CeaseAddress'){
        if (addressDet.Cease_Thoroughfare_Name_Street_Name__c == '' ||
            addressDet.Cease_Thoroughfare_Name_Street_Name__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Cease_Post_Town__c == '' ||
            addressDet.Cease_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Cease_Post_Code__c == '' ||
            addressDet.Cease_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Cease_County__c == '' ||
            addressDet.Cease_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Cease_Country__c == '' ||
            addressDet.Cease_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    
    
    
    
    if(selectedAddr1=='CorrespondenceAddress'){
        if (addressDet.Legal_Thoroughfare_Name_Street_Name__c == '' ||
            addressDet.Legal_Thoroughfare_Name_Street_Name__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Legal_Post_Town__c == '' ||
            addressDet.Legal_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Legal_Post_Code__c == '' ||
            addressDet.Legal_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Legal_County__c == '' ||
            addressDet.Legal_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Legal_Country__c == '' ||
            addressDet.Legal_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    if(selectedAddr1=='DeliveryAddress'){
        if (addressDet.Provide_Thoroughfare_Name_Street_Name__c == '' ||
            addressDet.Provide_Thoroughfare_Name_Street_Name__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_Post_Town__c == '' ||
            addressDet.Provide_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_Post_Code__c == '' ||
            addressDet.Provide_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_County__c == '' ||
            addressDet.Provide_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_Country__c == '' ||
            addressDet.Provide_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    if(selectedAddr1=='BillingAddress'){
        if (addressDet.Billing_Thoroughfare_Name_Street_Name__c == '' ||
            addressDet.Billing_Thoroughfare_Name_Street_Name__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_Post_Town__c == '' ||
            addressDet.Billing_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_Post_Code__c == '' ||
            addressDet.Billing_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_County__c == '' ||
            addressDet.Billing_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_Country__c == '' ||
            addressDet.Billing_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
    
    
    
    if(selectedAddr1=='ProvideAddress'){
        if (addressDet.Provide_Thoroughfare_Name_Street_Name__c == '' ||
            addressDet.Provide_Thoroughfare_Name_Street_Name__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_Post_Town__c == '' ||
            addressDet.Provide_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_Post_Code__c == '' ||
            addressDet.Provide_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_County__c == '' ||
            addressDet.Provide_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Provide_Country__c == '' ||
            addressDet.Provide_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }        
    if(selectedAddr1=='AlternateBillingAddress'){
        if (addressDet.Billing_Thoroughfare_Name_Street_Name__c == '' ||
            addressDet.Billing_Thoroughfare_Name_Street_Name__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Street!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_Post_Town__c == '' ||
            addressDet.Billing_Post_Town__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Town!'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_Post_Code__c == '' ||
            addressDet.Billing_Post_Code__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Postcode'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_County__c == '' ||
            addressDet.Billing_County__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter County'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
        if (addressDet.Billing_Country__c == '' ||
            addressDet.Billing_Country__c == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter Country'));
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }
        }
    }
        return true;
    }
}