@isTest
    global class MockHttpResponseforLineCheckerCTRL implements HttpCalloutMock {
        // Implement this interface method   
     global HTTPResponse respond(HTTPRequest req) { 
         
        // System.assertEquals('https://www.wsd-rtref.robt.bt.co.uk:53080/get/dev/Salesforce/customers/0123456.xml?v=10.0', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        string errordescription = 'ERROR';
         
        res.setBody('<?xml version=\'1.0\' encoding=\'utf-8\'?>');
        res.setStatusCode(200);
        return res;      
    }
}