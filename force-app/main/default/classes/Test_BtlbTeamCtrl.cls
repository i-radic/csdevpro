@isTest

    private class Test_BtlbTeamCtrl{
    
    static testMethod void myUnitTest(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Account a = Test_Factory.CreateAccount();
    a.LE_CODE__c = 'T-99991';
    a.Base_Team_Assign_Date__c=System.today();
    a.BTLB_Common_Name__c='TestSupport';
    insert a;
    
    
    
    Profile p= [select id from profile where name='Field: Standard User'];
    
    User u;
    u= new User(alias='asd',email='testing@bt.it',EIN__c='87fgter',emailencodingkey='UTF-8',lastname='Testingasd', languagelocalekey='en_US',        
                localesidkey='en_US',isActive=True,isActive2__c='True',Department='TestSupport', profileid = p.Id, timezonesidkey='Europe/London', username='testingasd@bt.com');
                
    insert u;
        
    BTLB_Master__c BM = new BTLB_Master__c();
    BM.Name='TestSupport';
    BM.BTLB_Name_ExtLink__c='TestSupport';
    BM.Email_Signature__c='Regards,Test Support Team';
    BM.Company_Name__c='Test Company';
    BM.Company_Registration_No__c='12345';
    BM.Company_Registered_Address__c='Address';
    insert BM;
    
    Account_Team_Contact_BTLB__c atcb= new Account_Team_Contact_BTLB__c();
    atcb.BTLB__c=bm.Id;
    atcb.ExtId__c='test';
    insert atcb;
        
    
    //PageReference PageRef = Page.BTLBAccountContacts;
    //test.setCurrentPage(PageRef);
    ApexPages.currentPage().getParameters().put('id',a.Id);
    BtlbTeamCtrl btc=new BtlbTeamCtrl(new ApexPages.StandardController(a));
    btc.getSpecialist();
    
    }
    }