public class CaseSiteCTRL {

	public Case caseRec;
	public String caseNo {get; set;}
	public String caseId = System.currentPageReference().getParameters().get('id');

    public CaseSiteCTRL(ApexPages.StandardController controller) { 
			this.caseRec = (Case)controller.getRecord();		
    }

	public PageReference OnLoad() { 
        system.debug('*****CaseId'+caseId);
		if(caseId != null){ 
			Case c = [Select Id, Include_In_Updates_Email__c, CaseNumber From Case Where Id =: caseId];
			caseNo = c.CaseNumber;
			CreateTeamMember(c.Id, c.Include_In_Updates_Email__c);
		}
		caseRec.RecordTypeId = GetRecordTypeId('People / User Request');
		caseRec.OwnerId = GetQueueId('User Admin');
		return null;
	}
	
	private void CreateTeamMember(Id cId, String usrEmail) {
		try {
			List<User> users = [Select Id, Email From User Where Email =: usrEmail LIMIT 10];
			if (users.size() == 1) {
				CaseTeamMember caseTeamMember = new CaseTeamMember();
				caseTeamMember.MemberId = users[0].Id;
				caseTeamMember.ParentId = cId;
				caseTeamMember.TeamRoleId = '0B720000000Cao1CAC';
				insert caseTeamMember;
			}
		} catch(exception e){}
	}

	private Id GetRecordTypeId(String recordTypeName) {
		RecordType recordType = [Select ID, Name From RecordType Where sObjectType = 'Case' And RecordType.Name =: recordTypeName];	
		return recordType.Id;
	}

	private Id GetQueueId(String queueName) {
		Group grp = [Select Id From Group Where Type = 'Queue' And Name =: queueName];	
		return grp.id;
	}

	private void SetIncludeInUpdatesEmail() {
		try {
			User user = [Select EIN__c, Email From User Where EIN__c =: caseRec.Creator_EIN_Site__c];
			caseRec.Include_In_Updates_Email__c = user.Email;
		} catch(exception e){}
	}

	public PageReference Submit() {
		// VALIDATIONS	
		//system.debug('caseRec.Creator_EIN_Site__c.length()'+caseRec.Creator_EIN_Site__c.length());
		if (caseRec.Creator_EIN_Site__c == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Case Creator EIN is blank, please check and try again.'));
			return null;           
        }else if(caseRec.Creator_EIN_Site__c != null && caseRec.Creator_EIN_Site__c.length() != 9){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Case Creator EIN should be 9 digits, please check and try again.'));
			return null;	    
            }                    
		else {
			SetIncludeInUpdatesEmail();
		}
        if (caseRec.Creator_OUC_Site__c == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Case Creator OUC is blank, please check and try again.'));
			return null; 
		}
		
		if (caseRec.Subject == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Subject is blank, please check and try again.'));
			return null; 
		}
		if (caseRec.Description == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Description is blank, please check and try again.'));
			return null; 
		}
        if (caseRec.Category__c == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Category is blank, please check and try again.'));
			return null; 
		}
		/**if (caseRec.SF_Instance__c == null) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'SF Instance is blank, please check and try again.'));
			return null; 
		}*/
		insert caseRec;
		PageReference retPg = new PageReference('/apex/CaseUserRequestSiteThanks?id=' + caseRec.Id);
		retpg.setRedirect(true);
		return retPg;
	}
    
    public List<selectOption> getCategory(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        options.add(new SelectOption('New User Request','New User Request'));
        options.add(new SelectOption('Login/Access Issues','Login/Access Issues'));
        return options;
    }
}