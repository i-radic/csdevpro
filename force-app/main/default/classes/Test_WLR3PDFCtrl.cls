@isTest

private class Test_WLR3PDFCtrl{

    static testMethod void myUnitTest() {
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='WLR3_Authorisation'].Id;
    crf.Opportunity__c = o.Id;  
    crf.Contact__c = c.Id;     
    insert crf;
    
    
    Numbers_to_be_Transferred__c NT = new Numbers_to_be_Transferred__c();
    NT.related_to_CRF__c=crf.Id;
    NT.Number_to_be_transferred__c='0123654789';
    insert NT;
    
    WLR3PDFCtrl myController = new WLR3PDFCtrl (new ApexPages.StandardController(crf));
    myController = new WLR3PDFCtrl(new ApexPages.StandardController(crf));
    
    myController.getNumbersList();
    }

}