public with sharing class FlowCRFAXController {
    
    public Flow_CRF__c crf{get;set;}
    public FlowCRFAXController(ApexPages.StandardController controller) {
         this.crf = (Flow_CRF__c)controller.getRecord();    
    }
    
    public List<Provide_Line_Details__c> getProvideLines() {                       
    List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,
                                                  Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,
                                                  Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,
                                                  Serial_No__c from Provide_Line_Details__c where 
                                                  related_to_Flow_CRF__c=:crf.Id ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){                                                                 
        return ProvideLines;                                             
    }
    else return null;
    }
             
    public List<Provide_Line_Details__c> getProvideLines1() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,
                                                  Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,
                                                  Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,
                                                  Serial_No__c from Provide_Line_Details__c where 
                                                  related_to_Flow_CRF__c=:crf.Id AND 
                                                  (Serial_No__c>10 AND Serial_No__c<=20) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */ return null;
     }
     
    public List<Provide_Line_Details__c> getProvideLines2() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,
                                                  Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,
                                                  Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,
                                                  Serial_No__c from Provide_Line_Details__c where 
                                                  related_to_Flow_CRF__c=:crf.Id AND 
                                                  (Serial_No__c>20 AND Serial_No__c<=30) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else*/ return null;
    }
    public List<Provide_Line_Details__c> getProvideLines3() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,
                                                  Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,
                                                  Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,
                                                  Serial_No__c from Provide_Line_Details__c where 
                                                  related_to_Flow_CRF__c=:crf.Id AND 
                                                  (Serial_No__c>30 AND Serial_No__c<=40) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else  */return null;
    }
    public List<Provide_Line_Details__c> getProvideLines4() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,
                                                  Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,
                                                  Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,
                                                  Serial_No__c from Provide_Line_Details__c where 
                                                  related_to_Flow_CRF__c=:crf.Id AND 
                                                  (Serial_No__c>40 AND Serial_No__c<=50) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */return null;
    }
    public List<Provide_Line_Details__c> getProvideLines5() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,
                                                  Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,
                                                  Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,
                                                  Serial_No__c from Provide_Line_Details__c where 
                                                  related_to_Flow_CRF__c=:crf.Id AND 
                                                  (Serial_No__c>50 AND Serial_No__c<=60) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */return null;
    }
    public List<Provide_Line_Details__c> getProvideLines6() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,
                                                  Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,
                                                  Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,
                                                  Serial_No__c from Provide_Line_Details__c where
                                                  related_to_Flow_CRF__c=:crf.Id 
                                                  AND (Serial_No__c>60 AND Serial_No__c<=70) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */return null;
    }
    public List<Provide_Line_Details__c> getProvideLines7() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,Serial_No__c from Provide_Line_Details__c where related_to_CRF__c=:crf.Id AND (Serial_No__c>70 AND Serial_No__c<=80) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */return null;
    }
    public List<Provide_Line_Details__c> getProvideLines8() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,Serial_No__c from Provide_Line_Details__c where related_to_CRF__c=:crf.Id AND (Serial_No__c>80 AND Serial_No__c<=90) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */return null;
    }
    public List<Provide_Line_Details__c> getProvideLines9() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,Serial_No__c from Provide_Line_Details__c where related_to_CRF__c=:crf.Id AND (Serial_No__c>90 AND Serial_No__c<=100) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */return null;
    }
    public List<Provide_Line_Details__c> getProvideLines10() {                        
    /*List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,Serial_No__c from Provide_Line_Details__c where related_to_CRF__c=:crf.Id AND (Serial_No__c>100 AND Serial_No__c<=110) ORDER BY Serial_No__c];
    if(ProvideLines.size()>0){
         return ProvideLines;                                             
    }
    else */return null;
    } 
     boolean show;   
    public boolean getShowTable(){
    if(getProvideLines()== null)
        show = false;
    else
        show=true;
    return show;
    } 
    public boolean getShowTable1(){
    if(getProvideLines1()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable2(){
    if(getProvideLines2()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable3(){
    if(getProvideLines3()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable4(){
    if(getProvideLines4()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable5(){
    if(getProvideLines5()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable6(){
    if(getProvideLines6()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable7(){
    if(getProvideLines7()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable8(){
    if(getProvideLines8()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable9(){
    if(getProvideLines9()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public boolean getShowTable10(){
    if(getProvideLines10()== null)
        show = false;
    else
        show=true;
    return show;
    }
    public String getDivision(){
         if(getProvideLines()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision1(){
         if(getProvideLines1()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision2(){
         if(getProvideLines2()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision3(){
         if(getProvideLines3()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision4(){
         if(getProvideLines4()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision5(){
         if(getProvideLines5()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision6(){
         if(getProvideLines6()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision7(){
         if(getProvideLines7()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision8(){
         if(getProvideLines8()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
     public String getDivision9(){
         if(getProvideLines9()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
  public String getDivision10(){
         if(getProvideLines10()== null)
             return null;
         else
             return '<div style="page-break-before:always;" />';
     }
}