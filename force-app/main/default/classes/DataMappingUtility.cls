public class DataMappingUtility {
    
    public static Map<String, String> CreateParametersValueMap(Set<String> objectIdSet, String TransactionType)
    {
        Map <String, String> OutputParamMap = new Map<String, String>();
        
        Map<String, String> ParamNameSobjectMap = new Map<String, String>();
        Map<String, String> ParamNameFieldNameMap = new Map<String, String>();
        Map<String, String> ParamNameAttNameMap = new Map<String, String>();
        Map<String, String> ParamNameHardCodedValueMap = new Map<String, String>();
        
        Set<String> ParamNameCustomMappingSet = new Set<String>();
        
        Set<String> sobjectSet = new Set<String>();
        System.debug('TransactionType>>>>'+TransactionType);
        
        List<Data_Mapping__c> dataMappingList = new list<Data_Mapping__c>();
        dataMappingList = [select Active__c,Parameter_Name__c,Calculation_Logic__c,Column_Sequence__c,SFDC_Object_Name__c,Display_Column_1__c,
                            Display_Column_2__c,SFDC_Field_Name__c,Hardcoded_Value__c,Transaction_Type__c
                             from Data_Mapping__c where
                            Active__c = true and Transaction_Type__c includes (:TransactionType)];
                            
        if(!dataMappingList.isEmpty()){
            for (Data_Mapping__c dataMappingObj : dataMappingList){
                System.debug('dataMappingObj.Display_Column_2__c>>'+dataMappingObj.Display_Column_2__c);
                System.debug('dataMappingObj.Hardcoded_Value__c>>'+dataMappingObj.Display_Column_1__c);     
                System.debug('dataMappingObj.Calculation_Logic__c>>'+dataMappingObj.Calculation_Logic__c);
                 
                if((dataMappingObj.Calculation_Logic__c == 'SFDC Object')){
                    System.debug('dataMappingObj.Display_Column_2__c>>'+dataMappingObj.Display_Column_2__c);
                    System.debug('dataMappingObj.Parameter_Name__c>>'+dataMappingObj.Parameter_Name__c);
                    ParamNameSobjectMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Object_Name__c.tolowercase());
                    ParamNameFieldNameMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Field_Name__c.tolowercase());
                    sobjectSet.add((dataMappingObj.SFDC_Object_Name__c).toLowerCase());
                }else if((dataMappingObj.Calculation_Logic__c == 'SFDC Object Custom Mapping')){
                    ParamNameSobjectMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Object_Name__c.tolowercase());
                    ParamNameFieldNameMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Field_Name__c.tolowercase());
                    sobjectSet.add((dataMappingObj.SFDC_Object_Name__c).toLowerCase());
                    ParamNameCustomMappingSet.add((dataMappingObj.Parameter_Name__c).toLowerCase());
                }
                else if((dataMappingObj.Calculation_Logic__c == 'Configuration Attribute')){
                    // if we need to fatch value from attribute sfdc field name should be excact attribute name
                    ParamNameAttNameMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Field_Name__c.tolowercase());
                }
                else if (dataMappingObj.Calculation_Logic__c == 'Hardcoded'){
                    ParamNameHardCodedValueMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.Hardcoded_Value__c);
                }
            }
        }
        OutputParamMap= GetSFDCFiledValues(objectIdSet,ParamNameSobjectMap, ParamNameFieldNameMap, sobjectSet);
        OutputParamMap.putAll(ParamNameHardCodedValueMap);
        // get data for Custom mapping
        System.Debug('OutputParamMap111'+JSON.serializePretty(OutputParamMap));
        System.Debug('ParamNameCustomMappingSet'+ParamNameCustomMappingSet);
        OutputParamMap= GetCustomMappingData(OutputParamMap,ParamNameCustomMappingSet);
		System.Debug('OutputParamMap222'+JSON.serializePretty(OutputParamMap));
		// 
		if(!ParamNameAttNameMap.isEmpty()){
			Map<String,String> outputParamMapforAtt = GetPCAttributeValues(objectIdSet,ParamNameAttNameMap, sobjectSet);
			if(!outputParamMapforAtt.isEmpty()){
			    OutputParamMap.putAll(outputParamMapforAtt);
			}
		}
		
        return OutputParamMap;
    }
    
    Public Static Map<String, String> GetCustomMappingData( Map<String, String> OutputParamMap, Set<String> ParamNameCustomMappingSet)
    {
        Map<String, String> CustomMappingParamValueMap = new Map<String, String>();
        try{
             List<Data_Mapping__c> dataMappingList = new list<Data_Mapping__c>();
             dataMappingList = [select Active__c,Parameter_Name__c,Hardcoded_Value__c,Transaction_Type__c
                             from Data_Mapping__c where
                            Active__c = true and Transaction_Type__c includes ('Custom Mapping')];
            for (Data_Mapping__c dataMappingObj : dataMappingList){ 
                CustomMappingParamValueMap.put(dataMappingObj.Parameter_Name__c.toLowerCase(),dataMappingObj.Hardcoded_Value__c);
            }
            for(String CustomMappingParam : ParamNameCustomMappingSet){
                if(OutputParamMap.containsKey(CustomMappingParam)){
                    String customMappingParamValue = OutputParamMap.get(CustomMappingParam);
                    System.Debug('customMappingParamValue'+JSON.serializePretty(customMappingParamValue));
                    System.Debug('CustomMappingParamValueMap'+JSON.serializePretty(CustomMappingParamValueMap));
                    System.Debug('CustomMappingParamValueMap Check '+CustomMappingParamValueMap.containsKey(customMappingParamValue.toLowerCase()));
                    if(CustomMappingParamValueMap.containsKey(customMappingParamValue.toLowerCase())){
                        OutputParamMap.put(CustomMappingParam,CustomMappingParamValueMap.get(customMappingParamValue.toLowerCase()));
                    }
                }
            }
            
        }catch(Exception e){
            
        }
        return OutputParamMap;
    }
    Public Static Map<String, String> GetPCAttributeValues(Set<String> ObjectIdList, Map<String, String> ParamNameAttNameMap, Set<String> sobjectSet)
    {
		String pcId='';
		Map<String, String> paramNameValueMap = new Map<String, String>();
		Map<String, String> attNameValueMap = new Map<String, String>();
		try{
			if(!ObjectIdList.isEmpty()){
				 for(String objId : ObjectIdList){
					  if(String.isnotblank(objId) && !(objId.equalsignorecase('null')))
					  {
						String sObjectName = (Id.valueOF(objId).getSObjectType().getDescribe().getName()).tolowercase();
						System.debug('sObjectName>>'+sObjectName);
						if(sObjectName.equalsignorecase('cscfga__Product_Configuration__c')){
							pcId = objId;
							break;
						}
					  }
				 }
			}
			if(String.isNotBlank(pcId)){
				List<cscfga__Attribute__c> attbuiteDataList = [SELECT cscfga__Display_Value__c,cscfga__Value__c,Name FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c = :pcId and name in : ParamNameAttNameMap.values()];
				if(!attbuiteDataList.isEmpty()){
					System.Debug('attbuiteDataList.isEmpty()>>'+attbuiteDataList);
					for(cscfga__Attribute__c att : attbuiteDataList){						
						attNameValueMap.put(String.valueOF(att.Name).toLowerCase(),att.cscfga__Value__c);
					}
				}
				
				if(!attNameValueMap.isEmpty()){
					System.Debug('attNameValueMap>>'+attNameValueMap);
					for(String paramName :ParamNameAttNameMap.keySet()){
						paramNameValueMap.put(paramName,attNameValueMap.get(ParamNameAttNameMap.get(paramName)));
					}
				}
				
			}
			
		}catch(Exception e){
			
		}	
		System.Debug('paramNameValueMap>>'+paramNameValueMap);
		return paramNameValueMap;
	}
    
     /*This method returns the value of attribues refered to in Object Types of different Record Ids 
    Inputs - 1. ObjectIdList - Comma separated list of different record Ids from which paramute values have to be fetched
    2. ParamNameSobjectMap - Map of paramute Name and Object Name from which the value has to be fetched
    3. ParamNameFieldNameMap - Map of paramute Names and Field Names of sObject
    4. sobjectSet - Set of sobjects for different attributes*/
    Public Static Map<String, String> GetSFDCFiledValues(Set<String> ObjectIdList, Map<String, String> ParamNameSobjectMap, Map<String, String> ParamNameFieldNameMap, Set<String> sobjectSet)
    {
      Map<String, String> sobjectfieldnamesMap = new Map<String, String>();
      Map<String, Set<String>> sobjectfieldnamesSetMap = new Map<String, Set<String>>();
      Map<String, String> paramNameValueMap = new Map<String, String>();
      Map<String, sobject> sObjectNamesobjectMap = new Map<String, sobject>();
      list<String> FinalObjectIdList = new list<String>();
      try
      {
          System.debug('ParamNameSobjectMap>>'+ParamNameSobjectMap);
          System.debug('ParamNameFieldNameMap>>'+ParamNameFieldNameMap);
          System.debug('sobjectSet>>'+sobjectSet);
          for (String param : ParamNameFieldNameMap.keySet())
          {
              System.debug('param>>'+param);
              String fieldsnames;
              Set<String> fieldsnameSet = new Set<String>();
              if(sobjectfieldnamesSetMap.containsKey(ParamNameSobjectMap.get(param.tolowercase())))
              {
                  fieldsnameSet = sobjectfieldnamesSetMap.get(ParamNameSobjectMap.get(param.tolowercase()));
                  System.debug('fieldname>>'+ParamNameFieldNameMap.get(param.tolowercase()));
                  fieldsnameSet.add(ParamNameFieldNameMap.get(param.tolowercase()));
              }
              else
              {
                  fieldsnameSet.add(ParamNameFieldNameMap.get(param.tolowercase()));
              }

              sobjectfieldnamesSetMap.put(ParamNameSobjectMap.get(param).tolowercase(), fieldsnameSet);
          } 
          System.debug('sobjectfieldnamesSetMap>>'+sobjectfieldnamesSetMap);
          System.debug('ObjectIdList>>'+ObjectIdList);
          if(!ObjectIdList.isEmpty()){
              for(String objId : ObjectIdList){
                  if(String.isnotblank(objId) && !(objId.equalsignorecase('null')))
                  {
                	  System.debug('inside if for rec');
                      FinalObjectIdList.add(objId);
                  }
              }
          }
          System.debug('FinalObjectIdList>>'+FinalObjectIdList);
          for (id RecordId : FinalObjectIdList)
          {
              System.debug('RecordId>>'+RecordId);
              String sObjectName = (RecordId.getSObjectType().getDescribe().getName()).tolowercase();
              System.debug('sObjectName>>'+sObjectName);
              String soqlquery, soqlfields;
              if (sobjectfieldnamesSetMap.get(sObjectName.toLowerCase()) != null)
              {
                  soqlfields = convertSettoString(sobjectfieldnamesSetMap.get(sObjectName.toLowerCase()));
                  System.debug('soqlfields>>'+soqlfields); 
              }    
              
              if ((soqlfields != null) && ((RecordId != null) || (String.isNotBlank(RecordId))))
              {
                  System.debug('RecordId>>'+RecordId);
                  soqlquery = 'select '+soqlfields+' from '+sObjectName+' where id = \''+RecordId+'\'';
                  System.debug('soqlquery>>'+soqlquery);
                  list<sobject> QueryResult = Database.query(soqlquery);
                  System.debug('QueryResult>>'+QueryResult);
                  System.debug('QueryResult.size>>'+QueryResult.size());
                  if (QueryResult.size() > 0)
                  {
                      sObjectNamesobjectMap.put(sObjectName.toLowerCase(), QueryResult[0]);
                  }    
              }    
          }
          ////System.debug('sObjectNamesobjectMap>>'+sObjectNamesobjectMap);
          for (String param : ParamNameFieldNameMap.keySet())
          {
              ////System.debug('param>>'+param);
              sobject sObjectRecord;
              sObjectRecord = sObjectNamesobjectMap.get(ParamNameSobjectMap.get(param.toLowerCase()));
              if (sObjectRecord != null)
              {
                  //System.debug('param.toLowerCase>>'+param.toLowerCase()+'         '+ParamNameFieldNameMap.get(param)+'     '+returnFieldValue(sObjectRecord, ParamNameFieldNameMap.get(param)));
                  //System.debug('ParamNameFieldNameMap.get(param)>>'+ParamNameFieldNameMap.get(param));
                  paramNameValueMap.put(param.toLowerCase(), returnFieldValue(sObjectRecord, ParamNameFieldNameMap.get(param)));
              }    
          }
          //System.debug('paramNameValueMap>>'+paramNameValueMap);
      }
      catch (exception e)
      {
          
      }
      return paramNameValueMap;
    }  
  
    public static String convertlisttoString(list<String> StringList)  {
        String commaSepratedList='';
        for(String str : StringList){
           commaSepratedList += str + ',' ;
        }
        // remove last additional comma from String
        commaSepratedList = commaSepratedList.subString(0,commaSepratedList.length()-1);
        return commaSepratedList;
    }  
    
    public static String convertSettoString(Set<String> StringList){
        String commaSepratedList='';
        for(String str : StringList){
            commaSepratedList += str + ',' ;
        }
         
        // remove last additional comma from String
        commaSepratedList = commaSepratedList.subString(0,commaSepratedList.length()-1);
        return commaSepratedList;
    }
    public static String returnFieldValue(sObject sobj, String fieldName){
        try{
            //System.debug('fieldName:::::::::::::::::::::::::::::::'+fieldName);
            if(fieldName.contains('.')){
                List<String> fieldsList = fieldName.split('\\.');
                String fieldValue = '';
                if(fieldsList.size() == 6){
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]).getSObject(fieldsList[4]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]).getSObject(fieldsList[4]).get(fieldsList[5]));
                }
                else if(fieldsList.size() == 5){
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]).get(fieldsList[4]));
                }
                else if(fieldsList.size() == 4){
                    //System.debug('fieldsList[0]>>'+fieldsList[0]+'llll'+sobj.getSObject(fieldsList[0]));
                    //System.debug('fieldsList[1]>>'+fieldsList[0]+'llll'+sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]));
                    //System.debug('fieldsList[2]>>'+fieldsList[0]+'llll'+sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]));
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).get(fieldsList[3]));
                }
                else if(fieldsList.size() == 3){
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).get(fieldsList[2]));
                }
                else if(fieldsList.size() == 2){
                    if(sobj.getSObject(fieldsList[0]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).get(fieldsList[1]));
                }
                return fieldValue != null ? fieldValue : '';
            } else {
                return sobj.get(fieldName) != null ? String.valueOf(sobj.get(fieldName)) : null;
            }
        } catch(Exception ex){
            //System.debug('Exception Message: '+ ex.getMessage());
            return 'Default Value';
        }
    }  

}