public without sharing class Page_Redirect {
    
    public pagereference redirectEditPage()
    {
        String oppId=Apexpages.currentPage().getParameters().get('oppId');
        String accId=Apexpages.currentPage().getParameters().get('accId');
        String returnTo=Apexpages.currentPage().getParameters().get('category');
        
        Opportunity opp= [Select Name from Opportunity where Id=: oppId];
        Account acc= [Select Name from Account where Id=: accId];
        
        string OpportunityName = opp.Name;
        string AccountName = acc.Name;
        
        if(OpportunityName.contains('&')){
        
            OpportunityName = OpportunityName.replace('&','%26');
        
        }
        if(AccountName.contains('&')){
        
            AccountName = AccountName.replace('&','%26');
        
        }
                
        if(returnTo=='AX')
        {
            PageReference pg= new PageReference('/a2w/e?CF00N200000098SzU='+OpportunityName+'&CF00N200000098SzU_lkid='+oppId+'&retURL=/'+OppId+'&CF00N200000099nt7='+AccountName+'&CF00N200000099nt7_lkid='+accId+'&nooverride=1&RecordType=01220000000AxDL');
            pg.setRedirect(true);
            return pg;
        }
        if(returnTo=='movers')
        {
            PageReference pg= new PageReference('/a2w/e?CF00N200000098SzU='+OpportunityName+'&CF00N200000098SzU_lkid='+oppId+'&retURL=/'+OppId+'&CF00N200000099nt7='+AccountName+'&CF00N200000099nt7_lkid='+accId+'&nooverride=1&RecordType=01220000000AxDG');
            pg.setRedirect(true);
            return pg;
        }
        if(returnTo=='moverswithBB')
        {
            PageReference pg= new PageReference('/a2w/e?CF00N200000098SzU='+OpportunityName+'&CF00N200000098SzU_lkid='+oppId+'&retURL=/'+OppId+'&CF00N200000099nt7='+AccountName+'&CF00N200000099nt7_lkid='+accId+'&nooverride=1&RecordType=01220000000AxDB');
            pg.setRedirect(true);
            return pg;
        }
        return null;
        
    }

}