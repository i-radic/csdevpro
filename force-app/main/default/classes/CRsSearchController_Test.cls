@isTest
private class CRsSearchController_Test{
static testMethod void runPositiveTestCases(){
    Change_Request__c fbk = new Change_Request__c (Feedback_Title__c = 'Test Feedback',Status__c='Logged',Salesforce_Org__c='Openzone',Requested_Priority__c = 'low',Description__c = 'Testing',Type__c = 'Change Request' ); 
    insert fbk;
    CRsSearchController controller = new CRsSearchController();
     ApexPages.currentPage().getParameters().put('name','Test');
    ApexPages.currentPage().getParameters().put('Status',fbk.Status__c);
    ApexPages.currentPage().getParameters().put('Salesforce_Org',fbk.Salesforce_Org__c);
    
    String cr  = controller.cr;
    String debugSoql = controller.debugSoql;
    String debugSoql2 = controller.debugSoql2;
    List<String> StatusList = New List<String>();
    StatusList  = controller.Status;
     List<String> SalesOrg = New List<String>();
    SalesOrg = controller.Salesforce_Org;
    controller.toggleSort();
    controller.runQuery();
    controller.runDetail();
    controller.runSearch();
}
}