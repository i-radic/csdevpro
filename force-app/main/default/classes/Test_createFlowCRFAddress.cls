@isTest
private class Test_createFlowCRFAddress {

    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        
        addressDet.Type__c = 'CeaseAddress';
        PageReference PageRef = Page.createFlowCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Type__c);
        
        
        createFlowCRFAddress createAddr = new createFlowCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases2() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        
        addressDet.Type__c = 'AlternateBillingAddress';
        PageReference PageRef = Page.createFlowCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Type__c);
        
        createFlowCRFAddress createAddr = new createFlowCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases3() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        
        addressDet.Type__c = 'ProvideAddress';
        
        PageReference PageRef = Page.createFlowCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Type__c);
        
        createFlowCRFAddress createAddr = new createFlowCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases4() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        
        addressDet.Type__c = 'DeliveryAddress';
        
        PageReference PageRef = Page.createFlowCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Type__c);
        
        createFlowCRFAddress createAddr = new createFlowCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases5() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        
        addressDet.Type__c = 'BillingAddress';
        
        PageReference PageRef = Page.createFlowCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Type__c);
        
        createFlowCRFAddress createAddr = new createFlowCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases6() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        
        addressDet.Type__c = 'CorrespondenceAddress';
        
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Type__c);
        
        createFlowCRFAddress createAddr = new createFlowCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases7() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        
        addressDet.Type__c = 'ExistingAddress';
        
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Type__c);
        
        createFlowCRFAddress createAddr = new createFlowCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
}