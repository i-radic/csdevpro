public class ProductUtility 
{

    public static void CreateOLIs(set<string> setProductBasketId)
    {
        /*
        --select all attributes which are line items
        --check if combinations of Product Family and Line Item Description exist in Product2
        --for the ones which do exist - get Product2.Id
        --for the others - insert and get Product2.Id
        
        --select standard price book
        
        --check if there is an existing combination of PriceBook and Product2 in PriceBookEntry
        --if it doesn't exist - create and get PriceBookEntry.Id
        --if it does exist - get PriceBookEntry.Id
        
        -- enter all PriceBookEntries into OLIs
        */
        
        map<string,string> mapProductBasketIdPriceBookId = new AssignnPriceBookToProductBasketImpl().AssignPriceBook(setProductBasketId);
        system.debug('****mapProductBasketIdPriceBookId=' + mapProductBasketIdPriceBookId);
        system.debug('****setProductBasketId=' + setProductBasketId);
        
        list<cscfga__Attribute__c> lstAttribute = [select Id, cscfga__is_active__c, cscfga__Is_Line_Item__c, cscfga__Line_Item_Description__c, cscfga__Line_Item_Sequence__c, 
            cscfga__Price__c, cscfga__List_Price__c, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c, cscfga__Product_Configuration__r.Name,
            cscfga__Product_Configuration__r.cscfga__Product_Family__c, Name , cscfga__Recurring__c, cscfga__Attribute_Definition__r.cscfga__Line_Item_Sequence__c, cscfga__Product_Configuration__r.cscfga__Contract_Term__c,
            cscfga__Product_Configuration__r.Volume__c, cscfga__Product_Configuration__r.cscfga__Recurrence_Frequency__c
            from cscfga__Attribute__c
            where cscfga__Is_Line_Item__c=true and cscfga__is_active__c=true and 
            cscfga__Product_Configuration__r.cscfga__Product_Basket__c in : mapProductBasketIdPriceBookId.keyset()];
            
        system.debug('****lstAttribute=' + lstAttribute);
        
        //this is the map where the keys are: ProductFamily and ProductName (LineItemDescription) 
        map<string,map<string,Product>> mapProductFamilymapProduct = CreateProducts2(lstAttribute);
        system.debug('****mapProductFamilymapProduct=' + mapProductFamilymapProduct);
        
        //this is the map where the keys are: PriceBookId and Product2Id
        map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry = MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId,lstAttribute,mapProductFamilymapProduct);
        system.debug('****MakePriceBookPBEntriesMap mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        //this function is void because it just modifies mapPriceBookIdmapPBEntry, so no need for return
        CreatePriceBookEntries(mapPriceBookIdmapPBEntry);
        system.debug('****CreatePriceBookEntries mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        //this function takes all structures created before and generates OLIs
        CreateOLIs(mapProductBasketIdPriceBookId,lstAttribute,mapProductFamilymapProduct,mapPriceBookIdmapPBEntry);
        
    }
    
    private static void CreateOLIs(map<string,string> mapProductBasketIdPriceBookId, list<cscfga__Attribute__c> lstAttribute,
        map<string,map<string,Product>> mapProductFamilymapProduct, map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry)
    {
        Boolean productConfLevel=false;
        Boolean SumOneOffAndRecurring=false;
        OpportunityLineItem maxValueOppLineItem;
        
        OLI_Sync__c OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
        
        if (OLISync != null)
        {
            productConfLevel=OLISync.Product_Configuration_Level__c;    
            SumOneOffAndRecurring=OLISync.Sum_One_Off_And_Recurring__c;
        }
        
        system.debug('****OLISync=' + OLISync);
        system.debug('****productConfLevel=' + productConfLevel);
        system.debug('****SumOneOffAndRecurring=' + SumOneOffAndRecurring);
        
        
        list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>(); 
        map<Id,OpportunityLineItem> mapPCIdOLI = new map<Id,OpportunityLineItem>(); 
        OpportunityLineItem tmpOpportunityLineItem;
        
        set<string> setProductBasketId = new set<string>();
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            setProductBasketId.add(tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c);
        }
        
        map<Id,cscfga__Product_Basket__c> mapProductBasket = new map<Id,cscfga__Product_Basket__c>([select Id,cscfga__Opportunity__c 
            from cscfga__Product_Basket__c where Id in : setProductBasketId]); 
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            //system.debug('****tmpProductFamily=' + tmpProductFamily);
            
            //string tmpLineItemDescription = ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
            //string tmpLineItemDescription = tmpAttribute.cscfga__Line_Item_Description__c;
            string tmpLineItemDescription = GetOLILineItemDescription(tmpAttribute);
            //system.debug('****tmpLineItemDescription=' + tmpLineItemDescription);
            
            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            //system.debug('****tmpProductBasketId=' + tmpProductBasketId);
            
            string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
            //system.debug('****tmpPriceBookId=' + tmpPriceBookId);         
            
            double tmpUnitPrice = tmpAttribute.cscfga__Price__c;
            //system.debug('****tmpUnitPrice=' + tmpUnitPrice);
            
            map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
            //system.debug('****mapProduct=' + mapProduct);
            
            Product tmpProduct = mapProduct.get(tmpLineItemDescription);
            //system.debug('****tmpProduct=' + tmpProduct);
            
            string tmpProduct2Id = tmpProduct.Prod2.Id;
            //system.debug('****tmpProduct2Id=' + tmpProduct2Id);
            
            map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);
            //system.debug('****mapPBEntry=' + mapPBEntry);
            
            PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
            //system.debug('****tmpPBEntry=' + tmpPBEntry);
            
            string tmpPriceBookEntryId = tmpPBEntry.PBE.Id;
            //system.debug('****tmpPriceBookEntryId=' + tmpPriceBookEntryId);
            
            string tmpOpportunityId=mapProductBasket.get(tmpProductBasketId).cscfga__Opportunity__c;
            //system.debug('****tmpOpportunityId=' + tmpOpportunityId);
            
            system.debug('****productConfLevel***=' + productConfLevel);
            if (productConfLevel)
            {
                if (mapPCIdOLI.containsKey(tmpAttribute.cscfga__Product_Configuration__c))
                {
                    tmpOpportunityLineItem = mapPCIdOLI.get(tmpAttribute.cscfga__Product_Configuration__c);
                }
                else
                {
                    tmpOpportunityLineItem = new OpportunityLineItem(); 
                    tmpOpportunityLineItem.UnitPrice=0;
                    tmpOpportunityLineItem.One_Off_Price__c=0;
                    tmpOpportunityLineItem.Recurring_Price__c=0;
                    mapPCIdOLI.put(tmpAttribute.cscfga__Product_Configuration__c,tmpOpportunityLineItem);
                }
            }
            else
            {
                tmpOpportunityLineItem = new OpportunityLineItem(); 
                tmpOpportunityLineItem.UnitPrice=0;
                tmpOpportunityLineItem.One_Off_Price__c=0;
                tmpOpportunityLineItem.Recurring_Price__c=0;
                lstOLI.add(tmpOpportunityLineItem);
                //mapPCIdOLI.put(tmpAttribute.cscfga__Product_Configuration__c,tmpOpportunityLineItem);
            }
            
            Decimal quantity = tmpAttribute.cscfga__Product_Configuration__r.Volume__c != null && tmpAttribute.cscfga__Product_Configuration__r.Volume__c != 0 ? tmpAttribute.cscfga__Product_Configuration__r.Volume__c : 1;
            system.debug('mpk '+tmpAttribute.cscfga__Product_Configuration__r.cscfga__Contract_Term__c+' - '+tmpAttribute.cscfga__Product_Configuration__r.cscfga__Recurrence_Frequency__c);
            Decimal recurrence = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Contract_Term__c / 12 * tmpAttribute.cscfga__Product_Configuration__r.cscfga__Recurrence_Frequency__c;
            tmpOpportunityLineItem.cscfga__Attribute__c=tmpAttribute.Id;
            tmpOpportunityLineItem.OpportunityId=tmpOpportunityId;
            tmpOpportunityLineItem.PricebookEntryId=tmpPriceBookEntryId;
            tmpOpportunityLineItem.Quantity=quantity; 
            tmpOpportunityLineItem.Qty__c=quantity;
            //tmpOpportunityLineItem.Description=tmpAttribute.cscfga__Line_Item_Description__c;
            tmpOpportunityLineItem.Description=tmpLineItemDescription;
            tmpOpportunityLineItem.Category__c = 'Additional';
            tmpOpportunityLineItem.BigMachines_Quote__c = 'CPQ';
            
            if (tmpAttribute.cscfga__Price__c!=null)
            {
                
                if (tmpAttribute.cscfga__Recurring__c)
                    tmpOpportunityLineItem.Recurring_Price__c+=(tmpAttribute.cscfga__Price__c / quantity);
                else
                    tmpOpportunityLineItem.One_Off_Price__c+=(tmpAttribute.cscfga__Price__c / quantity);
                
                if (SumOneOffAndRecurring)
                {
                    tmpOpportunityLineItem.UnitPrice+=(tmpAttribute.cscfga__Price__c / quantity);
                }
                else
                {
                    if (tmpAttribute.cscfga__Recurring__c)
                        tmpOpportunityLineItem.UnitPrice+= (tmpAttribute.cscfga__Price__c / quantity * recurrence);
                    else tmpOpportunityLineItem.UnitPrice+=(tmpAttribute.cscfga__Price__c / quantity);
                }
            }           
            
            if(maxValueOppLineItem == null || maxValueOppLineItem.UnitPrice < tmpOpportunityLineItem.UnitPrice) {
            	maxValueOppLineItem = tmpOpportunityLineItem;
            }
        }
        
        if(maxValueOppLineItem != null) {
        	maxValueOppLineItem.Category__c = 'Main';
        }
        
        //if (lstOLI.size()>0) insert lstOLI;
        if (mapPCIdOLI.size()>0) insert mapPCIdOLI.values();
        else if (lstOLI.size()>0) insert lstOLI;
    }
    
    
    private static map<string,map<string,PBEntry>> MakePriceBookPBEntriesMap(map<string,string> mapProductBasketIdPriceBookId,list<cscfga__Attribute__c> lstAttribute, 
        map<string,map<string,Product>> mapProductFamilymapProduct)
    {
        list<PBEntry> lstPBEntry = new list<PBEntry>();
        map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry = new map<string,map<string,PBEntry>>(); 
        
        //system.debug('****mapProductBasketIdPriceBookId=' + mapProductBasketIdPriceBookId);
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            //system.debug('****tmpProductFamily=' + tmpProductFamily);
            
            //string tmpLineItemDescription = ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
            //string tmpLineItemDescription = tmpAttribute.cscfga__Line_Item_Description__c;
            string tmpLineItemDescription = GetOLILineItemDescription(tmpAttribute);
            //system.debug('****tmpLineItemDescription=' + tmpLineItemDescription);
            
            string tmpProductBasketId = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
            //system.debug('****tmpProductBasketId=' + tmpProductBasketId);
            
            if (mapProductFamilymapProduct.containsKey(tmpProductFamily))
            {
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                
                if (mapProduct.containsKey(tmpLineItemDescription))
                {
                    Product tmpProduct = mapProduct.get(tmpLineItemDescription);
                    //system.debug('****tmpProduct=' + tmpProduct);
                    
                    string tmpProduct2Id = tmpProduct.Prod2.Id;
                    //system.debug('****tmpProduct2Id=' + tmpProduct2Id);
                    
                    string tmpPriceBookId = mapProductBasketIdPriceBookId.get(tmpProductBasketId);
                    //system.debug('****tmpPriceBookId=' + tmpPriceBookId);
                    
                    PBEntry tmpPBEntry = new PBEntry();
                    tmpPBEntry.PriceBookId=tmpPriceBookId;
                    tmpPBEntry.Product2Id=tmpProduct2Id;
                    
                    if (tmpAttribute.cscfga__Price__c==null)
                        tmpPBEntry.UnitPrice=0;
                    else
                        tmpPBEntry.UnitPrice=tmpAttribute.cscfga__Price__c;
            
                    tmpPBEntry.Name = tmpLineItemDescription;
                    
                    if (mapPriceBookIdmapPBEntry.containsKey(tmpPriceBookId))
                    {
                        map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPriceBookId);
                        if (!mapPBEntry.containsKey(tmpProduct2Id))
                        {
                            mapPBEntry.put(tmpProduct2Id,tmpPBEntry);
                        }
                    }
                    else
                    {
                        map<string,PBEntry> mapPBEntry = new map<string,PBEntry>();
                        mapPBEntry.put(tmpProduct2Id,tmpPBEntry);
                        mapPriceBookIdmapPBEntry.put(tmpPriceBookId,mapPBEntry);
                    }
                    
                }
                
            }
        }
        
        //system.debug('****mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        return mapPriceBookIdmapPBEntry;
    }
    
    
    
    private static void CreatePriceBookEntries(map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry)
    {
        //system.debug('***mapPriceBookIdmapPBEntry=' + mapPriceBookIdmapPBEntry);
        
        set<string> setProduct2Id = new set<string>();
        
        list<PricebookEntry> lstPricebookEntryInsert = new list<PricebookEntry>(); 
        
        for (map<string,PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values())
        {
            for (PBEntry tmpPBEntry : mapPBEntry.values())
            {
                setProduct2Id.add(tmpPBEntry.Product2Id);
            }
        }
        
        //system.debug('***setProduct2Id=' + setProduct2Id);
        
        if (setProduct2Id.size()>0)
        {
            map<Id,PricebookEntry> mapPricebookEntry = new map<Id,PricebookEntry> ([select Id, IsActive, Name, Pricebook2Id, Product2Id, UnitPrice
                from PricebookEntry
                where Product2Id in : setProduct2Id]);
                
            
            //system.debug('***mapPricebookEntry=' + mapPricebookEntry);
            
            for (PricebookEntry tmpPricebookEntry : mapPricebookEntry.values())
            {
                string tmpPricebook2Id = tmpPricebookEntry.Pricebook2Id;
                string tmpProduct2Id = tmpPricebookEntry.Product2Id;
                
                //system.debug('***tmpPricebook2Id=' + tmpPricebook2Id);
                //system.debug('***tmpProduct2Id=' + tmpProduct2Id);
                
                if (mapPriceBookIdmapPBEntry.containsKey(tmpPricebook2Id))
                {
                    map<string,PBEntry> mapPBEntry = mapPriceBookIdmapPBEntry.get(tmpPricebook2Id);
                    if ((mapPBEntry.containsKey(tmpProduct2Id)))
                    {
                        PBEntry tmpPBEntry = mapPBEntry.get(tmpProduct2Id);
                        tmpPBEntry.PBEntryId=tmpPricebookEntry.Id;
                    }
                }
            }
            
            for (map<string,PBEntry> mapPBEntry : mapPriceBookIdmapPBEntry.values())
            {
                for (PBEntry tmpPBEntry : mapPBEntry.values())
                {
                    
                    //system.debug('***tmpPBEntry=' + tmpPBEntry);
                    
                    if ((tmpPBEntry.PBEntryId=='') || (tmpPBEntry.PBEntryId==null)) 
                    {
                        PricebookEntry tmpPricebookEntry = new PricebookEntry();
                        tmpPricebookEntry.IsActive=true;
                        //tmpPricebookEntry.Name=tmpPBEntry.Name;
                        tmpPricebookEntry.Pricebook2Id=tmpPBEntry.PriceBookId;
                        tmpPricebookEntry.Product2Id=tmpPBEntry.Product2Id;
                        tmpPricebookEntry.UnitPrice=tmpPBEntry.UnitPrice;
                        
                        tmpPBEntry.PBE=tmpPricebookEntry;
                        
                        lstPricebookEntryInsert.add(tmpPricebookEntry);
                    }   
                    else
                    {
                        PricebookEntry tmpPricebookEntry = mapPricebookEntry.get(tmpPBEntry.PBEntryId);
                        tmpPBEntry.PBE=tmpPricebookEntry;
                    }   
                }
            }
            
        }
        
        if (lstPricebookEntryInsert.size()>0) insert lstPricebookEntryInsert;
        
    }
    
    /*
    private static string RemoveAmpresand(string pLineItemDescription)
    {
        //example: Connect ZZP Internet Start &amp; Play Start
        
        if (pLineItemDescription!=null)
        {
            if (pLineItemDescription.contains('&amp;'))
                pLineItemDescription=pLineItemDescription.replace('&amp;','&');
        }
        else
            pLineItemDescription='';
                
        return pLineItemDescription;
    }   
    */
    
    private static map<string,map<string,Product>> CreateProducts2(list<cscfga__Attribute__c> lstAttribute)
    {
        
        //system.debug('***lstAttribute=' + lstAttribute);
        
        map<string,map<string,Product>> mapProductFamilymapProduct = new map<string,map<string,Product>>(); 
        
        for (cscfga__Attribute__c tmpAttribute : lstAttribute)
        {
            string tmpProductFamily = tmpAttribute.cscfga__Product_Configuration__r.cscfga__Product_Family__c;
            //string tmpLineItemDescription = ProductUtility.RemoveAmpresand(tmpAttribute.cscfga__Line_Item_Description__c);
            //string tmpLineItemDescription = tmpAttribute.cscfga__Line_Item_Description__c;
            string tmpLineItemDescription = GetOLILineItemDescription(tmpAttribute);
        
            //system.debug('***tmpAttribute.cscfga__Line_Item_Description__c=' + tmpAttribute.cscfga__Line_Item_Description__c);
            //system.debug('***tmpProductFamily=' + tmpProductFamily);
            //system.debug('***tmpLineItemDescription=' + tmpLineItemDescription);
            
        
            if (mapProductFamilymapProduct.containsKey(tmpProductFamily))
            {
                //system.debug('***contains tmpProductFamily=' + tmpProductFamily);
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                //system.debug('***mapProduct=' + mapProduct);
                if (!mapProduct.containsKey(tmpLineItemDescription))
                {
                    //system.debug('***does not contain tmpLineItemDescription=' + tmpLineItemDescription);
                    Product tmpProduct = new Product();
                    tmpProduct.ProductFamily=tmpProductFamily;
                    tmpProduct.LiniItemDescription=tmpLineItemDescription;
                    mapProduct.put(tmpLineItemDescription,tmpProduct);
                }
            }
            else
            {
                //system.debug('***does not contain tmpProductFamily=' + tmpProductFamily);
                map<string,Product> mapProduct = new map<string,Product>();
                Product tmpProduct = new Product();
                tmpProduct.ProductFamily=tmpProductFamily;
                tmpProduct.LiniItemDescription=tmpLineItemDescription;
                mapProduct.put(tmpLineItemDescription,tmpProduct);
                mapProductFamilymapProduct.put(tmpProductFamily,mapProduct);
            }
            
        }
        
        //system.debug('***mapProductFamilymapProduct=' + mapProductFamilymapProduct);
        
        if (mapProductFamilymapProduct.size()>0)
        {
            map<Id,Product2> mapProduct2 = new map<Id,Product2>([select Family, Id, IsActive, Name 
                from Product2
                where IsActive=true and Family in : mapProductFamilymapProduct.keySet()]);
            List<Product2> csProduct2 = [Select id, name, productcode, part_number__c from product2 where part_number__c like 'CS-%' order by part_number__c desc limit 1];
            Integer partNumber = csProduct2.isEmpty() ? 1 : Integer.valueOf(csProduct2.get(0).part_number__c.replace('CS-', '')) + 1;
            
            for (Product2 tmpProduct2 : mapProduct2.values())
            {
                if (mapProductFamilymapProduct.containsKey(tmpProduct2.Family))
                {
                    map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProduct2.Family);
                    if (mapProduct.containsKey(tmpProduct2.Name))
                    {
                        Product tmpProduct = mapProduct.get(tmpProduct2.Name);
                        tmpProduct.Product2Id = tmpProduct2.Id;
                    }
                }
            }
            
            list<Product2> lstProduct2Insert = new list<Product2>();
            
            for (string tmpProductFamily : mapProductFamilymapProduct.keySet())
            {
                //system.debug('****tmpProductFamily=' + tmpProductFamily);
                
                map<string,Product> mapProduct = mapProductFamilymapProduct.get(tmpProductFamily);
                
                //system.debug('****mapProduct=' + mapProduct);
                
                for (Product tmpProduct : mapProduct.values())
                {
                    //system.debug('****tmpProduct=' + tmpProduct);
                    if ((tmpProduct.Product2Id=='') || (tmpProduct.Product2Id==null)) 
                    {
                        Product2 tmpProduct2 = new Product2();
                        tmpProduct2.Family=tmpProductFamily;
                        tmpProduct2.Name = tmpProduct.LiniItemDescription;
                        tmpProduct2.IsActive = true;
                        tmpProduct.Prod2 = tmpProduct2;
                        tmpProduct2.Part_Number__c = 'CS-' + String.valueOf(partNumber).leftPad(7, '0');
                        partNumber = partNumber + 1;
                        
                        lstProduct2Insert.add(tmpProduct2);
                    }
                    else
                    {
                        Product2 tmpProduct2 = mapProduct2.get(tmpProduct.Product2Id);
                        tmpProduct.Prod2 = tmpProduct2;
                    }
                }
            }
            
            //system.debug('****before lstProduct2Insert=' + lstProduct2Insert);
            if (lstProduct2Insert.size()>0) insert lstProduct2Insert;
            //system.debug('****lstProduct2Insert=' + lstProduct2Insert);
        }       
        
        
        //system.debug('****mapProductFamilymapProduct=' + mapProductFamilymapProduct);
        return mapProductFamilymapProduct;
    }
    
    
    public static void DeleteHardOLIs(set<string> setProductBasketId)
    {
    	map<string,string> mapProductBasketIdPriceBookId = new AssignnPriceBookToProductBasketImpl().AssignPriceBook(setProductBasketId);
        list<cscfga__Product_Basket__c> lstPB = [select Id, cscfga__Opportunity__c from cscfga__Product_Basket__c 
            where Id in : setProductBasketId];
            
        set<Id> setOpportunityId = new set<Id>();
        
        for(cscfga__Product_Basket__c tmpPB : lstPB)
        {
            setOpportunityId.add(tmpPB.cscfga__Opportunity__c);
        }
        
        if (setOpportunityId.size()>0)
        {
        	List<Opportunity> opps = [Select Id, PriceBook2Id, (Select Id From OpportunityLineItems) From Opportunity Where Id IN :setOpportunityId];
            list<OpportunityLineItem> lstOLI = new list<OpportunityLineItem>();
            List<Opportunity> oppToUpdate = new List<Opportunity>();
            for(Opportunity opp : opps) {
            	if(!opp.OpportunityLineItems.isEmpty()) lstOLI.addAll(opp.OpportunityLineItems);
            	if(opp.PriceBook2Id != AssignnPriceBookToProductBasketImpl.StandardPriceBookId) {
            		opp.PriceBook2Id = AssignnPriceBookToProductBasketImpl.StandardPriceBookId;
            		oppToUpdate.add(opp);
            	}
            }
            if (lstOLI.size()>0) delete lstOLI;
            if(!oppToUpdate.isEmpty()) update oppToUpdate;
        }
    }

    public static string CSVFromsetId(set<Id> setId)
    {
        string CSV='';
        for (Id tmpId : setId)
        {
            CSV+='\'' + tmpId + '\','; 
        }

        if (CSV.length()>0) CSV=CSV.left(CSV.length()-1);

        return CSV;
    }

    public static string GetSyncField()
    {
        OLI_Sync__c OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
        System.debug('OLISync : ' + OLISync + '---OLISync.Synchronisation_field__c : ' + OLISync.Synchronisation_field__c);
        return OLISync.Synchronisation_field__c;
    }
    
    private static string GetOLILineItemDescription(cscfga__Attribute__c tmpAttribute)
    {
        Boolean productConfLevel=false;
        string OLIDescription;
        
        OLI_Sync__c OLISync = OLI_Sync__c.getInstance(UserInfo.getUserId());
        
        if (OLISync != null)
            productConfLevel=OLISync.Product_Configuration_Level__c;    
        
        
        if (productConfLevel)
            OLIDescription=tmpAttribute.cscfga__Product_Configuration__r.Name;
        else
            OLIDescription=tmpAttribute.cscfga__Line_Item_Description__c;
            
        return OLIDescription;
        
    }
        
    private class Product
    {
        public string ProductFamily {get;set;}
        public string LiniItemDescription {get;set;}
        public string Product2Id {get;set;}
        public Product2 Prod2 {get;set;}
    }
    
    
    private class PBEntry
    {
        public string PBEntryId {get;set;}
        public string PriceBookId {get;set;}
        public string Product2Id {get;set;}
        public double UnitPrice {get;set;}
        public string Name {get;set;}
        public PriceBookEntry PBE {get;set;}
        
    }
}