@isTest
private class Test_EventContactId {


    static testMethod void testCreateUpdateEventContactId(){
    	
     	// Contact c1 = Test_Factory.CreateContact();	
     	// insert c1;
     	
        Account testAcc = Test_Factory.CreateAccount();
        testAcc.name += 'C';
        testAcc.sac_code__c += 'C';
        testAcc.Sector_code__c='CORP';
        testAcc.LOB_Code__c='FIELDM';
        Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc});
        
        
        Contact TestContact = Test_Factory.CreateContact();
        TestContact.AccountId = accResult[0].getid();
        insert TestContact;
        
        Event e1 = Test_Factory.CreateEvent(TestContact.Id);
        insert e1; 
                  
        // Update Event
        e1.reminder_days__c = 10;
        update e1;
    }
}