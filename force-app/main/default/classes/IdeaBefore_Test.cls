/**
Testing Idea before and creating CR where required - John McGovern - 11/01/12
Testing when new Idea posted email generation & posting on Chatter groups BTLB & COrp - Phanidra Mangipudi - July 5th 2013 
*/
@isTest
private class IdeaBefore_Test {
	
	List < Change_Request__c > newCR = new List < Change_Request__c > ();                  

    static testMethod void myUnitTest() { 
        
        
        Idea a = new Idea(Title='Test Idea', Status= 'Test Idea', Body='Test Idea', Categories='BTLB' ,CommunityId='09a20000000LUaW');//
        insert a;        
        Idea a1 = new Idea(Title='Test Idea', Status= 'Test Idea', Body='Test Idea', Categories='Corporate guys',CommunityId='09a20000000LUaW');// 
        insert a1;        
        a.Promote_to_CR__c = True;                
        a.Status ='Not Yet Reviewed';
        update a;
        a.Status ='Under Consideration';       
        a.Categories = 'Business Sales';
        update a;       
        a.Status ='Not Yet Reviewed';
        update a;
        a.Status ='Under Consideration';               
        a.Categories = 'corp';
        update a;        
        a.Status ='Delivered';
        a.Categories = 'Business';
        update a;       
        a.Categories = 'Business Sales';
        update a; 

    }    
    
}