global class CS_PLCustomDP implements cspl.DataProvider {

    String familyName = 'Total';

    global CS_PLCustomDP(){
        
    }
    
    global List<cspl.Source> getData(Map<String,Object> params){
        String basketId = (String)params.get('basketId');
        List<String> pcIdList = (List<String>)params.get('configIds');
        List<cscfga__product_configuration__c> pcList = getProductConfigurations(pcIdList);
        List<cscfga__Product_Basket__c > baskets = [SELECT Id, cscfga__Opportunity__c, PL_Family_Group__c FROM cscfga__Product_Basket__c WHERE id = :basketId LIMIT 1];
        if ((baskets != null) && !baskets.isEmpty()) {
            familyName = baskets[0].PL_Family_Group__c;
        }

        List<cspl.Source> data = prepareData(pcList);
        return data;
    }
    
    private List<cscfga__product_configuration__c> getProductConfigurations(List<Id> pcIdList){
        List<cscfga__product_configuration__c> pcList = [SELECT Id, Name, cscfga__contract_term__c, cscfga__Product_Family__c,
                                                            PL_Mapping_Cost__c,
                                                            PL_Mapping_Revenue__c,
                                                            cscfga__total_recurring_charge__c,
                                                            cscfga__total_one_off_charge__c,
                                                            Solution_Total_One_Off_Cost__c,
                                                            Solution_Total_Recurring_Cost__c,
                                                            Tech_Fund__c,
                                                            Staged_Air_Time__c,
                                                            cspl__Month_Term__c,
                                                            Rolling_Air_Time__c
                                                         FROM cscfga__Product_Configuration__c WHERE id in :pcIdList ORDER BY cscfga__contract_term__c DESC NULLS LAST];
        return pcList;
    }
    
    private List<cspl.Source> prepareData(List<cscfga__product_configuration__c> pcList){
        List<cspl.Source> data = new List<cspl.Source>();
        Integer longestTerm = 1;
        if (pcList != null && pcList.size()>0){
            for (cscfga__product_configuration__c pc : pcList){
                if (pc.cscfga__contract_term__c != null && pc.cscfga__contract_term__c > longestTerm){
                    longestTerm = Integer.valueOf(pc.cscfga__contract_term__c);
                }
                data.addAll(createSourcesFromPC(pc));
            }
        }
        return data;
    }
    
    private List<cspl.Source> createSourcesFromPC(cscfga__product_configuration__c pc){
        List<cspl.Source> sourceDataList = new List<cspl.Source>();
        Integer pcContractTerm = pc.cscfga__contract_term__c != null ? (Integer)pc.cscfga__contract_term__c : 1;
        pcContractTerm = pcContractTerm + 12;
        String pcFamily = pc.cscfga__Product_Family__c;
        
        if (familyName.equals('Total') || familyName == pcFamily) {
            Decimal totalOneOff = pc.cscfga__total_one_off_charge__c != null ? pc.cscfga__total_one_off_charge__c : 0;
            Decimal totalRecurring = pc.cscfga__total_recurring_charge__c != null ? pc.cscfga__total_recurring_charge__c : 0;
            Decimal totalCostOneOff = pc.Solution_Total_One_Off_Cost__c != null ? pc.Solution_Total_One_Off_Cost__c : 0;
            Decimal totalCostReccuring = pc.Solution_Total_Recurring_Cost__c != null ? pc.Solution_Total_Recurring_Cost__c : 0;
            String pLMappingRevenue = pc.PL_Mapping_Revenue__c != null ? pc.PL_Mapping_Revenue__c : '';
            String pLMappingCost = pc.PL_Mapping_Cost__c != null ? pc.PL_Mapping_Cost__c : '';
            Decimal techFund = pc.Tech_Fund__c != null ? pc.Tech_Fund__c : 0;
            Decimal stagedAirTime = pc.Staged_Air_Time__c != null ? pc.Staged_Air_Time__c : 0;
            Decimal stagedAirTimeMonth = pc.cspl__Month_Term__c != null ? pc.cspl__Month_Term__c : 0;
            Decimal rollingAirTime = pc.Rolling_Air_Time__c != null ? pc.Rolling_Air_Time__c : 0;
            
            if (pLMappingRevenue != '') {
                sourceDataList.add(generateSource(pLMappingRevenue, pcContractTerm, 0, totalOneOff, 12, totalRecurring));
            }
            if (pLMappingCost != '') {
                if (totalCostReccuring != 0) {
                    totalCostReccuring = totalCostReccuring / pc.cscfga__contract_term__c;
                }
                sourceDataList.add(generateSource(pLMappingCost, pcContractTerm, 0, totalCostOneOff, 12, totalCostReccuring));
            }
            
            if (techFund != 0) {
                sourceDataList.add(generateSource('equipmentR2', pcContractTerm, 0, 0, 12, techFund));
            }
            
            if (stagedAirTime != 0) {
                Integer monthTermCalc = 11 + Integer.valueOf(stagedAirTimeMonth);
                sourceDataList.add(generateSource('equipmentR1', pcContractTerm, monthTermCalc, stagedAirTime, 12, 0));
            }
            
            if (rollingAirTime != 0) {
                Decimal totalVal = rollingAirTime * pc.cscfga__contract_term__c;
                sourceDataList.add(generateSource('equipmentR1', pcContractTerm, 0, 0, 12, totalVal));
            }
            
            //check what to do with this fields
            //sourceDataList.add(generateSource('incrementalOpex', pcContractTerm, 0, incrementalOpexOneOff, 12, incrementalOpexRecc));
            //sourceDataList.add(generateSource('directCapex', pcContractTerm, 0, directCapexOneOff, 12, directCapexRecc));
            //sourceDataList.add(generateSource('depreciation', pcContractTerm, 0, depreciationOneOff, 12, depreciationRecc));
        }
        
        return sourceDataList;
    }
    
    private cspl.Source generateSource(String typeIn, Integer pcContractTermIn, Integer oneOffOffsetIn, Decimal oneOffAmountIn, Integer recurringOffsetIn, Decimal recurringAmountIn){
        cspl.Source.OneOff oneOff = null;
        cspl.Source.Recurring recurring = null;
       
        cspl.Source sourceData = new cspl.Source();
       
        sourceData.duration = pcContractTermIn;
        sourceData.durationPeriod = '12';
        sourceData.type = typeIn;
       
        oneOff = new cspl.Source.oneOff();
        oneOff.offset = oneOffOffsetIn;
        oneOff.amount = oneOffAmountIn.SetScale(2);
       
        recurring = new cspl.Source.Recurring();
        recurring.offset = recurringOffsetIn;
        recurring.amount = recurringAmountIn.SetScale(2);
       
        sourceData.oneOff = oneOff;
        sourceData.recurring = recurring;
       
        return sourceData;
    }
}