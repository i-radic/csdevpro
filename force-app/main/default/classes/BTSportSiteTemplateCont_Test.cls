@isTest(SeeAllData=true)
private class BTSportSiteTemplateCont_Test {
    
    static testMethod void myTest()
    {   
        RecordType rt = [select id from RecordType where SobjectType='BT_Sport_Site__c' and DeveloperName ='Season3' limit 1];//mpk
        
        //Creation of Account
        StaticVariables.setAccountDontRun(False);  
        Account acc1 = Test_Factory.CreateAccount(); // a SAC Level Account to link to
        acc1.OwnerId = UserInfo.getUserId();
        /*acc1.Sector__c = 'BT Local Business';
acc1.LOB_Code__c = 'lob999';
acc1.SAC_Code__c = 'aSac999';
acc1.LE_Code__c = '';
acc1.AM_EIN__c = '802537216';
acc1.Sub_Sector__c = 'ssTest';*/
        Database.SaveResult accountResult = Database.insert(acc1);        
        
        //Creation of Contact
        Contact con1 = Test_Factory.CreateContact();
        con1.Cug__c = 'cugAlan1';
        con1.Contact_Post_Code__c = 'WR5 3RL';       
        insert con1;  
        
        //Creation of Opportunity
        
        Opportunity oppS2S = Test_Factory.CreateOpportunity(accountResult.getid());
        oppS2s.Name='Test_BTSportSite';
        oppS2S.closedate = system.today();
        oppS2S.Order_Reference__c='ABCD1234';
        oppS2S.StageName='Won';
        Database.SaveResult opptResult = Database.insert(oppS2S);
        
        //Creation of OpportunityContactRole
        
        OpportunityContactRole OppContRole= new OpportunityContactRole();
        OppContRole.ContactId=con1.Id;//'003g000000KKx17';
        OppContRole.OpportunityId=oppS2S.id;//'006g0000006uVC5';
        insert OppContRole;
        
        //Insertion of BTSportCL Object        
        BT_Sport_CL__c SportCL= new BT_Sport_CL__c();
        SportCL.Opportunity__c=oppS2S.id;//'006g0000006uVC5';
        insert SportCL;
        
        //Insertion of BTSportPricing Object
        
        BT_Sport_Pricing__c sportPrice= new BT_Sport_Pricing__c();
        sportPrice.Discount_Period_End_Date__c=Date.newInstance(2016,08,31);
        sportPrice.Discount_Period_Months__c=3;
        sportPrice.Discount_Amount__c=10;
        sportPrice.Discount_Name__C='35 percent off for 3 months';
        sportPrice.Entry_Type__c='Discount';
        sportPrice.Start_Date__c=Date.newInstance(2015,04,10);
        sportPrice.End_Date__c=Date.newInstance(2020,05,12);
        sportPrice.Discount_Period__c=1;
        sportPrice.Price_Type__c = '3RV';
        insert sportPrice;
        
        //Insertion of BTSportSite Object
        
        BT_Sport_Site__c sportSite= new BT_Sport_Site__c();
        sportSite.RecordTypeID=rt.id;//'01220000000cklV'; mpk
        sportSite.Contract_Type__c='12 month contract';
        sportSite.Band__c='Band A - £0 - £5,000';
        sportSite.BT_Sport_CL__c=SportCL.Id;
        sportSite.Order_Date__c=System.today();
        sportSite.Building_Number__c='123';
        sportSite.County__c='Worcs';
        sportSite.Post_Code__c='WR11 5QR';
        sportSite.Discounts__c='';
        sportSite.Street__c='arrow lane';
        sportSite.Town__c='evesham';
        sportSite.Site_Type_S2__c='BT Sport Pack Pub (GB)';
        sportSite.Site_Type__c='BT Sport Total Pub (GB)';
        sportSite.Discounts_S2__c='';
        sportSite.Site_Display_Name__c='Order1';
        insert sportSite;
        system.debug('***********'+sportSite.Discount_Period_Months_S2__c);
        
        Test.StartTest();        
        //Calling of webservice class BTSportSiteAttCont 
        //BTSportSiteAttCont.createAttachment(sportSite.id, 'Order');
        
        //Calling of BTSportSiteTemplateCont class 
        
        BTSportSiteTemplateCont bts= new BTSportSiteTemplateCont();
        ApexPages.currentPage().getParameters().put('Id',sportSite.id);
        ApexPages.currentPage().getParameters().put('type','Order');        
        
        //Site Name as Quote
        sportSite.PMF_Effective_Date__c=System.today();
        sportSite.Product__c='BT Sport 1';
        sportSite.Site_Display_Name__c='Quote1'; 
        sportSite.Discount_Period_Months__c=3;
        //update sportSite;        
        
        //Calling of webservice class BTSportSiteAttCont         
        BTSportSiteAttCont.createAttachment(sportSite.id, 'Quote');
        
        
        //Order Date and PMF Effective Date as NULL
        sportSite.Order_Date__c=null;
        sportSite.PMF_Effective_Date__c=null;
        sportPrice.Discount_Period_Months__c=null;
        sportSite.Discount_Period_Months_S2__c=null;
        sportSite.Discount_Period_End_Date_S2__c=Date.newInstance(2015,12,1);
        sportSite.Site_Display_Name__c=NULL;      
        //    update sportSite;       
        
        //   BTSportSiteAttCont.createAttachment(sportSite.id, 'Quote');        
        
        //Order Date greater than 30th June
        sportSite.PMF_Effective_Date__c=null;
        sportSite.Order_Date__c=Date.newInstance(2015,7,1);
        sportSite.Site_Display_Name__c=NULL;
        //   update sportSite;        
        
        //BTSportSiteAttCont.createAttachment(sportSite.id, 'Order');
        
        Test.stopTest();
        
    }
    
}