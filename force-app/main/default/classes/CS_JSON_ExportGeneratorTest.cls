@IsTest
public class CS_JSON_ExportGeneratorTest  {
	private static CS_JSON_Schema jsonSchema;
	private static csbtcl_Temporary_Data_Store_CS_JSON__c tempDataStore;
	private static Account acc;
	private static Opportunity opp;
	private static cscfga__Product_Basket__c basket;

	//done

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		jsonSchema = (CS_JSON_Schema) JSON.deserialize(CS_JSONExportTestFactory.getJSON(), CS_JSON_Schema.class);
		acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		tempDataStore = CS_TestDataFactory.generateTemporaryStoreJSON(true, opp.Id, false);
	}

	@IsTest
	public static void testExportGenerator() {
		createTestData();

		Test.startTest();
		CS_JSON_ExportGenerator controller = new CS_JSON_ExportGenerator(opp.Id);
		controller.Id = opp.Id;
		controller.dataStore = tempDataStore;
		controller.schema = jsonSchema;
		Test.stopTest();

		System.assertNotEquals(null, controller);
		System.assertEquals(opp.Id, controller.dataStore.Opportunity__c);
	}

	@IsTest
	public static void testGenerate() {
		createTestData();

		cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinitionResign(true, 'BT Mobile', true);

		basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
		pc.cscfga__Configuration_Status__c = 'Valid';
		pc.cscfga__Product_Definition__c = prodDef.Id;
		INSERT pc;
        
        CloudPhone_RC_Deal_Management__c rcMan = new CloudPhone_RC_Deal_Management__c(
            Opportunity__c = opp.Id,
            RCDeal_ID__c = '123'
        );
		insert rcMan;
        
		basket.csbb__Synchronised_With_Opportunity__c = true;
		UPDATE basket;

		Test.startTest();
		CS_JSON_ExportGenerator controller = new CS_JSON_ExportGenerator(opp.Id);
		try{
			controller.generate();
		}
		catch(Exception exc){}
		Test.stopTest();

		List<Attachment> processedAtts = [SELECT Id, Body, Name
								 FROM Attachment
								 WHERE ParentId = :controller.dataStore.Id];

		System.assertNotEquals(null, controller);
	}

	@IsTest
	public static void testGenerateUnsyncedBasket() {
		createTestData();
		basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);

		Test.startTest();
		CS_JSON_ExportGenerator controller = new CS_JSON_ExportGenerator(opp.Id);
		controller.generate();
		Test.stopTest();

		List<Attachment> processedAtts = [SELECT	Id, Body, Name
								 FROM Attachment
								 WHERE ParentId = :controller.dataStore.Id];
		
		System.assertNotEquals(null, controller);
		System.assert(processedAtts.size() == 0); 
	}

	@IsTest
	public static void testCopyImplementationForm() {
		createTestData();
		basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);

		Test.startTest();

		CS_JSON_ExportGenerator controller = new CS_JSON_ExportGenerator(opp.Id);
		List<Attachment> copyImplementationForm = controller.copyImplementationForm(basket, basket.Id);
		
		Test.stopTest();
	}
}