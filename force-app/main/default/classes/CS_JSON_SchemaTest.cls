@IsTest
public class CS_JSON_SchemaTest  {
	private static CS_JSON_Schema jsonSchema;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		jsonSchema = new CS_JSON_Schema();
		jsonSchema = (CS_JSON_Schema) JSON.deserialize(CS_JSONExportTestFactory.getJSON(), CS_JSON_Schema.class);
	}

	private static testMethod void getJSONSection() {
		createTestData();
		CS_JSON_Schema.CS_JSON_Section jsonSection = new CS_JSON_Schema.CS_JSON_Section();
		jsonSection = jsonSchema.getSection('Basket Information');
		System.assertNotEquals(null, jsonSection);
	}

	private static testMethod void getJSONSetting() {
		createTestData();
		CS_JSON_Schema.CS_JSON_Section jsonSection = jsonSchema.getSection('Basket Information');
		CS_JSON_Schema.CS_JSON_Setting jsonSetting = new CS_JSON_Schema.CS_JSON_Setting();
		jsonSetting = jsonSchema.getSetting('companyProducts');
		String setting = jsonSection.setting;
		String service = jsonSetting.service;
		String filter = jsonSetting.filter;
		CS_JSON_Schema.CS_JSON_Setting jsonSettingInstance = jsonSection.settingInstance;
		System.assertNotEquals(null, jsonSetting);
	}

	private static testMethod void getJSONSettingDefinition() {
		createTestData();
		CS_JSON_Schema.CS_JSON_Setting jsonSetting = jsonSchema.getSetting('companyProducts');
		CS_JSON_Schema.CS_JSON_Object jsonDefintionObject = jsonSetting.getDefinition('BT Mobile Sharer');
		System.assertNotEquals(null, jsonDefintionObject);
	}

	private static testMethod void getJSONField() {
		createTestData();
		CS_JSON_Schema.CS_JSON_Field jsonFieldInit = new CS_JSON_Schema.CS_JSON_Field();
		CS_JSON_Schema.CS_JSON_Setting jsonSetting = jsonSchema.getSetting('companyProducts');
		CS_JSON_Schema.CS_JSON_Object jsonObject = new CS_JSON_Schema.CS_JSON_Object();
		jsonObject = jsonSetting.getDefinition('BT Mobile Sharer');
		String jsonObjectType = jsonObject.type;
		String filter = jsonObject.filter;
		String grouping = jsonObject.grouping;
		String relatedList = jsonObject.relatedList;
		List<CS_JSON_Schema.CS_JSON_Field> attributes = jsonObject.attributes;
		List<CS_JSON_Schema.CS_JSON_Field> fieldList = jsonObject.fields;

		for(CS_JSON_Schema.CS_JSON_Field jsonField : fieldList) {
			String label = jsonField.label;
			jsonField.getType();
			jsonField.getAttributeName();
			jsonField.getAttributeField();
		}

		System.assertNotEquals(null, attributes);
		System.assertNotEquals(null, fieldList);
	}
	
}