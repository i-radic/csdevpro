public class CoverageBulkCheckerSPGJSONParser {

   public class Coverage {
        public String type;
        public Integer strength;
        public Boolean comingsoon;
    }

    public class Links {
        public String rel;
        public String href;
        public String method;
    }

    public CoverageResource coverageResource;

    public class Locations {
        public Links links;
        public String title;
        public Double lat;
        public Double lng;
        public List<Coverage> coverage;
    }

    public class CoverageResource {
        public Links links;
        public Locations locations;
        public String lastUpdated;
    }

    
    public static CoverageBulkCheckerSPGJSONParser parse(String json) {
        return (CoverageBulkCheckerSPGJSONParser ) System.JSON.deserialize(json, CoverageBulkCheckerSPGJSONParser .class);
    }
    
   
}