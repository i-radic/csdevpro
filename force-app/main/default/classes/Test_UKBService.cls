@istest

private class Test_UKBService {
    
   static testmethod void GetAccountSummary(){
        Test_Factory.SetProperty('istest', 'yes');
        Account account = Test_Factory.CreateAccount();
        account.CUG__c = 'ukbtest';
       account.Name='test';
        insert account;
        Opportunity oppty = new Opportunity();
       	oppty.AccountId=account.Id;
       	oppty.Name='test';
        oppty.StageName='Created';
       	oppty.Product_Family__c='BT FINANCE';
        oppty.CloseDate=Date.today()+1;
        insert oppty;
        
       Landscape_BTLB__c landscape_BTLB = new Landscape_BTLB__c();
       landscape_BTLB.Customer__c=account.Id;
       insert landscape_BTLB;
       
       Landscape_DBAM__c landscape_DBAM = new Landscape_DBAM__c();
       landscape_DBAM.Account__c=account.Id;
       insert landscape_DBAM;
       
       Task task= new Task();
       task.Subject='Send Letter';
       task.Status='Not Started';
       task.Priority='Normal';
       task.Assigned_User_Id__c='Venkata Srinivas';
       task.WhatId=account.Id;
       insert task;
       
       Event event= new Event();
       event.WhatId=account.Id;
       event.Subject='Send Letter/Quote';
       event.Assigned_User_Id__c='Venkata Srinivas';
       event.StartDateTime=system.now();
       event.EndDateTime=system.now()+10;
       insert event;
       
       ADP__c adp= new ADP__c();
       adp.Customer__c=account.Id;
       insert adp;
       
       ADP_Landscape__c adpLandscape= new ADP_Landscape__c();
       adpLandscape.ADP__c=adp.Id;
       adpLandscape.Customer__c=account.Id;
       insert adpLandscape;   
       
       Campaign campaign=new Campaign();
       campaign.Name='test';
       insert campaign;
       
       Contact contact=new Contact();
       contact.Salutation='Mr';
       contact.FirstName='test';
       contact.LastName='test';
       contact.AccountId=account.Id;
       contact.Status__c='Active';
       contact.Job_Function__c='ADMIN';
       contact.Job_Title__c='ADMINISTRATOR';
       contact.Preferred_Contact_Channel__c='Email';
       contact.Phone='12345';
       contact.Phone_Consent__c='Yes';
       contact.Phone_Consent_Date__c=system.now();
       contact.Email='test@tcs.com';
       contact.Contact_Post_Code__c='521521';
       contact.Email_Consent__c='yes';
       contact.Email_Consent_Date__c=system.now();
       contact.Address_Consent__c='yes';
       contact.Address_Consent_Date__c=system.now();
       insert contact;
       
           
       CampaignMember campaignMember = new CampaignMember();
       campaignMember.ContactId=contact.Id;
       campaignMember.Account__c=account.Id;
       campaignMember.CampaignId=campaign.Id;
       campaignMember.Status='Callback';
       insert campaignMember;
         
        UKBService.LandscapeSummary landscapeSummary = new UKBService.LandscapeSummary();
        UKBService.CampaignSummary campaignSummary = new UKBService.CampaignSummary();
        UKBService.ActivitySummary activitySummary = new UKBService.ActivitySummary();
        
       	UKBService.AccountSummary acs = UKBService.GetAccountSummary(account.CUG__c);
    }
    
    static testmethod void CreateOpporunity() {
        Test_Factory.SetProperty('istest', 'yes');
        Account account = Test_Factory.CreateAccount(); 
        account.CUG__c = 'ukbtest';
        insert account;
        
        UKBService.OpportunitySummary o = UKBService.CreateOpporunity(
            account.CUG__c,
            'description',
            'Created', 
            '603860148',
            'ukb Test Oppty',
            Date.today());   	
    }
    

}