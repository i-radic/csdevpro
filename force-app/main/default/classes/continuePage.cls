public class continuePage {

    public iDepot__c iDepot;
    
    public continuePage(ApexPages.StandardController controller) {
        iDepot = (iDepot__c) controller.getRecord();
    }
    
    public PageReference cancel() {
        Pagereference ref = new Pagereference('/a08/o');
        return ref;
    }
  
    public PageReference cont(){
        
        System.debug('Am in cont()');
        String retUrl; 
        Pagereference returnPage;
        
        System.debug('Am retUrl : '+retUrl);
        
        if( iDepot.RecordTypeId == '01220000000AJjv') {
            retUrl = 'a08/e?retURL=/a08/o&RecordType='+iDepot.RecordTypeId+'&CF00N20000003hyWy='+userinfo.getName()+'&CF00N20000003hyWy_lkid='+Userinfo.getUserId()+'&ent=01I200000007Thh&nooverride=1';
            returnPage = new Pagereference('/'+retUrl);
        } else {            
            retUrl = 'a08/e?retURL=/a08/o&RecordType='+iDepot.RecordTypeId+'&ent=01I200000007Thh&nooverride=1';
            returnPage = new Pagereference('/'+retUrl); 
        }
        
        returnPage.setRedirect(true);
        return returnPage;
    }

}