/**
 * Used for the Product Basket new custom button 'P&L'
 * Button opens the P&L VF page.
 *
 * @author Cloudsense
 */
global with sharing class CustomButtonPLNEW extends csbb.CustomButtonExt {

  public String performAction(String basketId) {
    String action = '';
    action = '{"status":"ok","redirectURL":"/apex/c__CS_PLFamilySelect?basketId=' + basketId + '"}';
    
    return action;
  }
}