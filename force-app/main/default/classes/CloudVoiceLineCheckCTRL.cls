/*######################################################################################
PURPOSE
Used to make the call out to wholesale dsl checker to return BB availability

HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
public class CloudVoiceLineCheckCTRL{

public CloudVoiceLineCheckCTRL(CloudVoiceQuickStartCTRL controller) { }
//public CloudVoiceLineCheckCTRL(ipcCTRL controller) { }
public CloudVoiceLineCheckCTRL() {}
public CloudVoiceLineCheckCTRL(ApexPages.StandardController controller) {}

public HttpResponse httpRespTest {get; set;} 

public List<SelectOption> selOptions {get;set;}

public String cvsId = System.currentPageReference().getParameters().get('id');    
public String lineChange = System.currentPageReference().getParameters().get('line');    
public String pcChange = System.currentPageReference().getParameters().get('pc');      
public String nameNumChange = System.currentPageReference().getParameters().get('namenum');
public String fullAdd = System.currentPageReference().getParameters().get('full');     
public String users = System.currentPageReference().getParameters().get('users');      
public String calls = System.currentPageReference().getParameters().get('calls');      
public String data = System.currentPageReference().getParameters().get('data');      
public String codec = System.currentPageReference().getParameters().get('codec');     

public String source {
    set ;
    get { 
        source = System.currentPageReference().getParameters().get('source');
        return source;
    }
}
/*
public String line {
    set ;
    get { 
        line = lineChange;
        return line;
    }
}
*/

public String qCompany {get; set;}  
public String qContact {get; set;}  
public String qContactTel {get; set;}  
public String qContactEmail {get; set;} 
public String multiAddress {get; set;} 
public String jmAddress = '';
public String jmSubPrem = '';
public String jmPrem = '';
public String jmSt = '';
public String jmLoc = '';
public String jmTown = '';
public String jmDist = '';

public String tErrText {get; set;}
public String tMax {get; set;}
public String tADSL2 {get; set;}
public String tWBC {get; set;}
public String tFTTC {get; set;}
public String tFTTP {get; set;}
public String tFOD {get; set;}
public String tdlMax {get; set;}
public String tdlADSL2 {get; set;}
public String tdlWBC {get; set;}
public String tdlFTTC {get; set;}
public String tdlFTTP {get; set;}
public String tdlFOD {get; set;}
public String jmPC {get; set;}
public String jmNum {get; set;}

public Boolean showAddresses{get; set;}

public String lineCheck(string checkNumber, string chkType){
    //setup wholesale checker details
    String uname = 'btbsfdc';
    String pword = 'Btwhs1';
    String sysVer = '39';

    String telNum = checkNumber; 
    String action = chkType; 
    String epURL = Null;
    showAddresses = False; 

    String MAX = Null; String ADSL2 = Null; String WBC = Null; String FTTC = Null; String FTTP = Null; String FOD = Null;
    String dlMAX = Null; String dlADSL2 = Null; String dlWBC = Null; String dlFTTC = Null; String dlFTTP = Null; String dlFOD = Null;
    
    Http h = new Http();
    HttpRequest req = new HttpRequest();

    if(action == 'tel'){
        req.setEndpoint('https://www.dslchecker.bt.com/adsl/adslchecker_xml.telno?username='+uname+'&password='+pword+'&version='+sysVer+'&input='+telNum);
    }
    if(action == 'pc'){
        telNum = telNum.replace(' ','%20');
        nameNumChange = nameNumChange.replace(' ','%20');
        req.setEndpoint('https://www.dslchecker.bt.com/adsl/adslchecker_xml.address?username='+uname+'&password='+pword+'&version='+sysVer+'&ThoroughfareNumber='+nameNumChange+'&PremiseName='+nameNumChange+'&PostCode='+telNum);
    }

    if(action == 'full'){
        showAddresses = False; 
        String[] lAddress = telnum.Split('~');
        jmSubPrem = lAddress[0];
        jmPrem = lAddress[1];
        jmNum = lAddress[2];
        jmSt = lAddress[3];
        jmLoc = lAddress[4];
        jmTown = lAddress[5];
        jmPC = lAddress[6];
        jmDist = lAddress[7];

        epURL = 'SubPremises='+jmSubPrem+'&PremiseName='+jmPrem+'&ThoroughfareNumber='+jmNum+'&ThoroughfareName='+jmSt+'&Locality='+jmLoc+'&Posttown='+jmTown+'&Postcode='+jmPC+'&DistrictID='+jmDist;

        epURL = epURL.replace(' ','%20');
        req.setEndpoint('https://www.dslchecker.bt.com/adsl/adslchecker_xml.address?username='+uname+'&password='+pword+'&version='+sysVer+'&'+epURL);
    }
    req.setMethod('GET');
    req.setTimeout(120000);

    String errText = Null;
    String addPass = Null;
    HttpResponse res;

    if(!Test.isRunningTest()){
        for (Integer i = 1; i < 100; i++) {
            try {
                res = h.send(req);
                i = 100;
            } catch (System.CalloutException e) {
                System.debug('JMM error: ' + e + ' retry: '+ i);
            } 
        }   
    }
    else{
        res = httpRespTest;
    }

    Dom.Document doc = res.getBodyDocument();   
    Dom.XMLNode access = doc.getRootElement();
    //return error messages
    String chkError= access.getChildElement('ERRORID', null).getText();
    if(action == 'tel'){
        if(chkError == '1'){errText = 'Telephone number not found in the BT database or the number belongs to a LLU operator';
        }else if(chkError == '2'){errText = 'Postcode not found in database';
        }else if(chkError == '3'){errText = 'Telephone number is invalid. Telephone numbers must be 10-11 characters in length and start with either 01 or 02';
        }else if(chkError == '4'){errText = 'Postcode is invalid. Postcodes must be between 5 and 7 characters in length';
        }else if(chkError == '5'){errText = 'Telephone Number Exported to an Other Licensed Operator therefore we cannot qualify this line';
        }else if(chkError == '6'){errText = 'Telephone Number Ceased, therefore the telephone number no longer exists';
        }else if(chkError == '7'){errText = 'Error processing request. Please try again later';
        }else if(chkError == '8'){errText = 'Authentication failed. Username and password combination incorrect';
        }else if(chkError == '9'){errText = 'Not executed via HTTPS';
        }else if(chkError == '99'){errText = 'Description of MAC Validation Request Failure ';
        }
    }
    if(action == 'pc' || action == 'full'){
        if(chkError == '1'){errText = 'Address Not Found (Unmatched Address)';
        }else if(chkError == '2'){errText = 'Too Many Addresses Found';
        }else if(chkError == '3'){errText = 'Qualification not available for this address';
        }else if(chkError == '4'){errText = 'Mandatory Postcode field not entered';
        }else if(chkError == '5'){errText = 'Mandatory Thoroughfare Number or Premise Name not entered';
        }else if(chkError == '6'){errText = 'Insufficient Address Elements';
        }else if(chkError == '7'){errText = 'Conflict between Locality, Posttown and Postcode';
        }else if(chkError == '8'){errText = 'Posttown or Locality not unique';
        }else if(chkError == '9'){errText = 'An Error Has Occurred';
        }else if(chkError == '10'){errText = 'Authentication failed. Username and password combination incorrect';
        }else if(chkError == '11'){errText = 'Not executed via HTTPS. The XML version of the checker must be sent over HTTPS as it returns customer information';
        }else if(chkError == '12'){errText = 'Incomplete information available';
        }else if(chkError == '13'){errText = 'The address does not map to Gold NAD key, please proceed with postcode qualification';
        }else if(chkError == '14'){errText = 'Invalid NAD Key Format';
        }
    }    
    //address list
    selOptions= new List<SelectOption>();
    selOptions.add(new SelectOption('','-- Select Correct Address --'));

    for(Dom.XMLNode child : access.getChildElements()) {
        for(Dom.XMLNode c1: child.getChildElements()) {
            if(child.getName() == 'ADDRESS_DETAILS' && c1.getName() =='ADDRESS_MATCHED' && c1.getText().substring(1,6) == 'FALSE' && chkError =='0' ){
system.debug('JMM chkError: '+chkError);
            showAddresses = True;
            }
            for(Dom.XMLNode c2: c1.getChildElements()) {
                for(Dom.XMLNode c3: c2.getChildElements()) {    
                    jmAddress = '';
                    if(c3.getName() =='SUBPREMISES'){jmSubPrem = c3.getText();}
                    if(c3.getName() =='PREMISENAME'){jmPrem = c3.getText();}
                    if(c3.getName() =='THOROUGHFARENUMBER'){jmNum = c3.getText();}
                    if(c3.getName() =='THOROUGHFARENAME'){jmSt = c3.getText();}
                    if(c3.getName() =='LOCALITY'){jmLoc = c3.getText();}
                    if(c3.getName() =='POSTTOWN'){jmTown = c3.getText();}
                    if(c3.getName() =='POSTCODE'){jmPC = c3.getText();}
                    if(c3.getName() =='DISTRICTID'){jmDist = c3.getText();}
                    if(jmSubPrem !=''){jmAddress=jmSubPrem+' ';}
                    if(jmPrem !=''){jmAddress=jmAddress+jmPrem+' ';}
                    if(jmNum !=''){jmAddress=jmAddress+jmNum+' ';}
                    if(jmSt !=''){jmAddress=jmAddress+jmSt+' ';}
                    if(jmLoc !=''){jmAddress=jmAddress+jmLoc+' ';}
                    if(jmTown !=''){jmAddress=jmAddress+jmTown+' ';}
                    if(jmPC !=''){jmAddress=jmAddress+jmPC+' ';}
                    //if(jmDist !=''){jmAddress=jmAddress+jmDist+' ';}
                }
                selOptions.add(new SelectOption(jmSubPrem+'~'+jmPrem+'~'+jmNum+'~'+jmSt+'~'+jmLoc+'~'+jmTown+'~'+jmPC+'~'+jmDist, jmAddress));
                selOptions.sort();
            }            
        }
    }

    if(chkError == '0'){
        String retText = Null;
        MAX = 'Unavailable';ADSL2= 'Unavailable';WBC= 'Unavailable';FTTC= 'Unavailable';FTTP= 'Unavailable';FOD= 'Unavailable';
        dlMAX = '-';dlADSL2= '-';dlWBC= '-';dlFTTC= '-';dlFTTP= '-';dlFOD= '-';
        for(Dom.XMLNode child : access.getChildElements()) {
            //availabiity
            for(Dom.XMLNode c1: child .getChildElements()) {
               if(child.getName() == 'MAX' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){MAX = 'Available';}
               if(child.getName() == 'ADSL2' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){ADSL2= 'Available';}
               if(child.getName() == 'WBC' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){WBC= 'Available';}
               if(child.getName() == 'WBCFTTC' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){FTTC= 'Available';}
               if(child.getName() == 'WBCFTTP' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){FTTP= 'Available';}
               if(child.getName() == 'FTTPONDEMAND' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){FOD= 'Available';}
            }
            //speeds
            for(Dom.XMLNode c1: child .getChildElements()) {
               if(child.getName() == 'WBC' && c1.getName() =='ANNEXMUPSPEED' && c1.getText()!=''){WBC= c1.getText();}
               if(child.getName() == 'WBCFTTC' && c1.getName() =='UPSPEED' && c1.getText()!=''){FTTC= c1.getText();}
               if(child.getName() == 'WBCFTTP' && c1.getName() =='UPSPEED' && c1.getText()!=''){FTTP= c1.getText();}
               if(child.getName() == 'FTTPONDEMAND' && c1.getName() =='UPSPEED' && c1.getText()!=''){FOD= c1.getText();}

               if(child.getName() == 'MAX' && c1.getName() =='SPEED' && c1.getText()!=''){dlMAX= c1.getText();}
               if(child.getName() == 'ADSL2' && c1.getName() =='SPEED' && c1.getText()!=''){dlADSL2= c1.getText();}
               if(child.getName() == 'WBC' && c1.getName() =='SPEED' && c1.getText()!=''){dlWBC= c1.getText();}
               if(child.getName() == 'WBCFTTC' && c1.getName() =='DOWNSPEED' && c1.getText()!=''){dlFTTC= c1.getText();}
               if(child.getName() == 'WBCFTTP' && c1.getName() =='DOWNSPEED' && c1.getText()!=''){dlFTTP= c1.getText();}
               if(child.getName() == 'FTTPONDEMAND' && c1.getName() =='DOWNSPEED' && c1.getText()!=''){dlFOD= c1.getText();}
            }          
        } 
    }else{
        MAX = Null; ADSL2 = Null; WBC = Null; FTTC = Null; FTTP = Null; FOD = Null;
    }
    return MAX+','+ADSL2+','+WBC+','+FTTC+','+FTTP+','+FOD+','+dlMAX+','+dlADSL2+','+dlWBC+','+dlFTTC+','+dlFTTP+','+dlFOD+','+errText;
        

}
public PageReference lineResults() {
    String telNum = Null; 
    String chkType = Null; 
    String MAX = Null; String ADSL2 = Null; String WBC = Null; String FTTC = Null; String FTTP = Null; String FOD = Null;
    String dlMAX = Null; String dlADSL2 = Null; String dlWBC = Null; String dlFTTC = Null; String dlFTTP = Null; String dlFOD = Null;

    List<Cloud_Voice_Site__c> cvs = [SELECT ID, Access_Service_Id__c,LineCheckNumber__c,LineCheckPostCode__c,LineCheckRerun__c FROM Cloud_Voice_Site__c WHERE Id = :cvsId LIMIT 1];

    if(cvs[0].LineCheckRerun__c == False && lineChange == Null && pcChange == Null && fullAdd == Null){
        return null;
    }
    if(cvs[0].LineCheckRerun__c == True && lineChange == Null && !Test.isRunningTest()){
        telNum = cvs[0].LineCheckNumber__c;
    }else if(lineChange != Null){
        telNum = lineChange;
        chkType = 'tel'; 
        pcChange = Null;
        nameNumChange = Null;
    }else if(pcChange != Null){
        telNum = pcChange;
        chkType = 'pc'; 
        lineChange = Null;
    }else if(fullAdd != Null){
        telNum = fullAdd;
        chkType = 'full';
        pcChange = '1'+jmPC;    
        nameNumChange = jmNum;          
    }

    String tRes = lineCheck(telNum, chkType);
    tErrText = Null;

    List<String> tSplit = tRes.split(',',-1);
    tMax = tSplit[0];
    tADSL2 = tSplit[1];
    tWBC = tSplit[2];
    tFTTC = tSplit[3];
    tFTTP = tSplit[4];
    tFOD = tSplit[5];
    tdlMax = tSplit[6];
    tdlADSL2 = tSplit[7];
    tdlWBC = tSplit[8];
    tdlFTTC = tSplit[9];
    tdlFTTP = tSplit[10];
    tdlFOD = tSplit[11];
    tErrText = tSplit[12];

    cvs[0].LineCheckMAX__c = tMax;
    cvs[0].LineCheckADSL2__c = tADSL2;
    cvs[0].LineCheckWBC__c = tWBC;
    cvs[0].LineCheckFTTC__c = tFTTC;
    cvs[0].LineCheckFTTP__c = tFTTP;
    cvs[0].LineCheckFOD__c = tFOD;

    cvs[0].LineCheckdlMAX__c = tdlMax;
    cvs[0].LineCheckdlADSL2__c = tdlADSL2;
    cvs[0].LineCheckdlWBC__c = tdlWBC;
    cvs[0].LineCheckdlFTTC__c = tdlFTTC;
    cvs[0].LineCheckdlFTTP__c = tdlFTTP;
    cvs[0].LineCheckdlFOD__c = tdlFOD; 

    cvs[0].LineCheckError__c= tErrText;
    cvs[0].LineCheckRerun__c = False; //reset flag to stop rerunning    
    cvs[0].LineCheckNumber__c = lineChange;  
    cvs[0].LineCheckPostCode__c = jmPC;//pcChange;    
    cvs[0].LineCheckNameNum__c = jmNum;//nameNumChange;  

    //store passed in parameters
    cvs[0].Access_Users__c = users;
    cvs[0].Access_Concurrent_Calls__c = calls;
    cvs[0].Access_Data_Traffic__c = data;
    cvs[0].Access_Codec__c = codec;

    update cvs;

    return Null; 
}
/* 
public PageReference refresh() {
    PageReference retPg = Null;
    retPg = page.CloudVoiceAccessNew;
    retPg.getParameters().put('mod','Acc2');
    retPg.getParameters().put('Id',cvsId);
    return retpg.setRedirect(true);
}
*/
}