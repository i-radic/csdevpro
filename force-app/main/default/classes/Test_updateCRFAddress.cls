@isTest
private class Test_updateCRFAddress {

    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.Address_Type__c = 'Cease Address';
        
        addressDet.Cease_Country__c = '';
        addressDet.Cease_County__c = null;
        addressDet.Cease_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Cease_Post_Code__c = null;
        addressDet.Cease_Post_Town__c = '';                
        addressDet.Cease_Exchange_Group_Code__c = '';
        addressDet.Cease_Locality__c = '';
        addressDet.Cease_Location_Description__c = '';
        addressDet.Cease_Premises_Building_Name__c = '';
        addressDet.Cease_Sub_Premises__c = '';       
        addressDet.Cease_Thoroughfare_Name_Street_Name_Co__c = '';
        addressDet.Cease_Thoroughfare_Number_Street_Number__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
         
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
     
     static testMethod void runPositiveTestCases11() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.Address_Type__c='Cease Address';  
        if(addressDet.Address_Type__c=='Cease Address'){
        String AType='CeaseAddress';}
        
        addressDet.Cease_Country__c = '';
        addressDet.Cease_County__c = '';
        addressDet.Cease_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Cease_Post_Code__c = null;
        addressDet.Cease_Post_Town__c = '';                
        addressDet.Cease_Exchange_Group_Code__c = '';
        addressDet.Cease_Locality__c = '';
        addressDet.Cease_Location_Description__c = '';
        addressDet.Cease_Premises_Building_Name__c = '';
        addressDet.Cease_Sub_Premises__c = '';       
        addressDet.Cease_Thoroughfare_Name_Street_Name_Co__c = '';
        addressDet.Cease_Thoroughfare_Number_Street_Number__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
         
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }

    static testMethod void runPositiveTestCases1() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.Address_Type__c = 'Provide Address';
        addressDet.Provide_Country__c = '';
        addressDet.Provide_County__c = null;
        addressDet.Provide_Exchange_Group_Code__c = '';
        addressDet.Provide_Locality__c = '';
        addressDet.Provide_Location_Description__c = '';
        addressDet.Provide_Post_Code__c = null;
        addressDet.Provide_Post_Town__c = '';
        addressDet.Provide_Premises_Building_Name__c = '';
        addressDet.Provide_Sub_Premises__c = '';
        addressDet.Provide_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Provide_Thoroughfare_Street_Name_Cont__c = '';
        addressDet.Provide_Thoroughfare_Number_Street_Numb__c = '';
     updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
         
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn(); 
    
    }
    static testMethod void runPositiveTestCases2() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.Address_Type__c = 'Alternate Billing Address';
        addressDet.Billing_Country__c = '';
        addressDet.Billing_County__c = null;
        addressDet.Billing_Exchange_Group_Code__c = '';
        addressDet.Billing_Locality__c = '';
        addressDet.Billing_Location_Description__c = '';
        addressDet.Billing_Post_Code__c = null;
        addressDet.Billing_Post_Town__c = '';
        addressDet.Billing_Premises_Building_Name__c = '';       
        addressDet.Billing_Sub_Premises__c = '';
        addressDet.Billing_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Billing_Thoroughfare_StreetName_Cont__c = '';
        addressDet.Billing_Thoroughfare_Number_Street_Numb__c = '';
         updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
         
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn(); 
        }
        static testMethod void runPositiveTestCases5() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.Address_Type__c = 'Cease Address';
        
        addressDet.Cease_Country__c = '';
        addressDet.Cease_County__c = 'xyz';
        addressDet.Cease_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Cease_Post_Code__c = 'xxx xxx';
        addressDet.Cease_Post_Town__c = '';                
        addressDet.Cease_Exchange_Group_Code__c = '';
        addressDet.Cease_Locality__c = '';
        addressDet.Cease_Location_Description__c = '';
        addressDet.Cease_Premises_Building_Name__c = '';
        addressDet.Cease_Sub_Premises__c = '';       
        addressDet.Cease_Thoroughfare_Name_Street_Name_Co__c = '';
        addressDet.Cease_Thoroughfare_Number_Street_Number__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
         
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
    static testMethod void runPositiveTestCases6() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.Address_Type__c = 'Provide Address';
        
        addressDet.Cease_Country__c = '';
        addressDet.Cease_County__c = 'xyz';
        addressDet.Cease_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Cease_Post_Code__c = 'xxx xxx';
        addressDet.Cease_Post_Town__c = '';                
        addressDet.Cease_Exchange_Group_Code__c = '';
        addressDet.Cease_Locality__c = '';
        addressDet.Cease_Location_Description__c = '';
        addressDet.Cease_Premises_Building_Name__c = '';
        addressDet.Cease_Sub_Premises__c = '';       
        addressDet.Cease_Thoroughfare_Name_Street_Name_Co__c = '';
        addressDet.Cease_Thoroughfare_Number_Street_Number__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
         
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
    static testMethod void runPositiveTestCases7() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.Address_Type__c = 'Alternate Billing Address';
        
        addressDet.Cease_Country__c = '';
        addressDet.Cease_County__c = 'xyz';
        addressDet.Cease_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Cease_Post_Code__c = 'xxx xxx';
        addressDet.Cease_Post_Town__c = '';                
        addressDet.Cease_Exchange_Group_Code__c = '';
        addressDet.Cease_Locality__c = '';
        addressDet.Cease_Location_Description__c = '';
        addressDet.Cease_Premises_Building_Name__c = '';
        addressDet.Cease_Sub_Premises__c = '';       
        addressDet.Cease_Thoroughfare_Name_Street_Name_Co__c = '';
        addressDet.Cease_Thoroughfare_Number_Street_Number__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
        updateadd.NextPage(); 
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
    static testMethod void runPositiveTestCases8() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.AX_Address_Type__c = 'Billing Address';
        
        addressDet.Billing_Country__c = '';
        addressDet.Billing_County__c = 'xyz';
        addressDet.Billing_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Billing_Post_Code__c = 'xxx xxx';
        addressDet.Billing_Post_Town__c = '';                
        addressDet.Billing_Exchange_Group_Code__c = '';
        addressDet.Billing_Locality__c = '';
        addressDet.Billing_Location_Description__c = '';
        addressDet.Billing_Premises_Building_Name__c = '';
        addressDet.Billing_Sub_Premises__c = '';       
        addressDet.Billing_Thoroughfare_StreetName_Cont__c = '';
        addressDet.Billing_Thoroughfare_Number_Street_Numb__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
        updateadd.NextPage(); 
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
    static testMethod void runPositiveTestCases9() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.AX_Address_Type__c = 'Delivery Address';
        
        addressDet.Provide_Country__c = '';
        addressDet.Provide_County__c = 'xyz';
        addressDet.Provide_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Provide_Post_Code__c = 'xxx xxx';
        addressDet.Provide_Post_Town__c = '';                
        addressDet.Provide_Exchange_Group_Code__c = '';
        addressDet.Provide_Locality__c = '';
        addressDet.Provide_Location_Description__c = '';
        addressDet.Provide_Premises_Building_Name__c = '';
        addressDet.Provide_Sub_Premises__c = '';       
        addressDet.Provide_Thoroughfare_Street_Name_Cont__c = '';
        addressDet.Provide_Thoroughfare_Number_Street_Numb__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
        updateadd.NextPage(); 
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
    static testMethod void runPositiveTestCases10() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.AX_Address_Type__c = 'Correspondence Address';
        
        addressDet.Legal_Country__c = '';
        addressDet.Legal_County__c = 'xyz';
        addressDet.Legal_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Legal_Post_Code__c = 'xxx xxx';
        addressDet.Legal_Post_Town__c = '';                
        addressDet.Legal_Locality__c = '';
        addressDet.Legal_Premises_Building_Name__c = '';
        addressDet.Legal_Sub_Premises__c = '';       
        addressDet.LegalThoroughfare_Number_StreetNumber__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
        updateadd.NextPage(); 
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
    
    static testMethod void runPositiveTestCases15() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();  
        addressDet.Address_Type_NSO__c = 'Existing Address';
        
        addressDet.Existing_Country__c = '';
        addressDet.Existing_County__c = 'xyz';
        addressDet.Existing_Thoroughfare_Name_Street_Name__c = '';
        addressDet.Existing_Post_Code__c = 'xxx xxx';
        addressDet.Existing_Post_Town__c = '';                
        addressDet.Existing_Exchange_Group_Code__c = '';
        addressDet.Existing_Locality__c = '';
        addressDet.Existing_Location_Description__c = '';
        addressDet.Existing_Premises_Building_Name__c = '';
        addressDet.Existing_Sub_Premises__c = '';       
        addressDet.ExistingThoroughfareName_StreetName_Cont__c = '';
        addressDet.Existing_Thoroughfare_No_Street_Number__c = '';
        
        
        
        updateCRFAddress updateadd = new updateCRFAddress(addressDet);
        updateadd.AddressSearch(); 
        updateadd.NextPage(); 
        updateadd.OnLoad();
        updateadd.saveAndReturn(); 
        updateadd.cancelAndReturn();      
          
    }
        
}