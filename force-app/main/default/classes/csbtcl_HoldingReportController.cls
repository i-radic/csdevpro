public with sharing class csbtcl_HoldingReportController {
  public Id     basketId      {get;set;}
  public Id     pcId        {get;set;}
  public String   csvFilename     {get;set;}
  public String   resignOption    {get;set;}
  public String   resignDesription {get;set;}
  public Boolean  showResignOption  {get;set;} 
  public String   applicableETCs    {get;set;}

  public transient String         csvAsString   {get;set;}
  public transient List<String>   csvFileLines  {get;set;}
  public transient Blob           csvFileBody   {get;set;}
  private cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c();
  private String    attachmentNamePlaceholder = 'bt_holdingReport_{0}.json';
  public ApexPages.StandardController stdCntrlr {get; set;}

  public csbtcl_HoldingReportController(ApexPages.StandardController controller) {
    stdCntrlr = controller;
    csvFileLines = new List<String>();
    basketId  =   ApexPages.currentPage().getParameters().get('basketId');
    pcId    =   ApexPages.currentPage().getParameters().get('pcId');
    pc = [SELECT name,cscfga__product_definition__r.name,Ave_Days_Remaining_Months__c FROM cscfga__Product_Configuration__c WHERE ID = :pcId];
    showResignOption = false;
  } 

  public csbtcl_HoldingReportController() {
    csvFileLines = new List<String>();
    basketId = ApexPages.currentPage().getParameters().get('basketId');
  }

  public void importCSVFile() {
    Boolean isCsvFile = false;
    Attachment holdingReportJSON = new Attachment();
    holdingReportJSON.parentId  = basketId;
    holdingReportJSON.name    = String.format( attachmentNamePlaceholder, new String[] { basketId } );

    try {
      isCsvFile = String.isNotBlank( csvFileName ) && csvFilename.endsWith( '.csv' );

      if( isCsvFile ) {
        csvAsString = csvFileBody.toString();
        csvFileLines = csvAsString.split('\n');
        csvFileLines.remove(0);

        String jsonHoldingReport = csbtcl_CS_Holding_Report_JSON_Generator.getJSONHoldingReport( csvFileLines, pc.name, pcId );

        if(jsonHoldingReport == Label.BT_ResignHurdleNotMet ){
          ApexPages.Message signHurdleNotMet = new ApexPages.Message(ApexPages.severity.ERROR, Label.BT_ResignHurdleNotMet);
          ApexPages.addMessage( signHurdleNotMet );
        }
        else{
          Blob blobValue = Blob.valueOf( jsonHoldingReport );
          holdingReportJSON.body = blobValue;
          insert holdingReportJSON;
          pc = [SELECT name,cscfga__product_definition__r.name,Ave_Days_Remaining_Months__c FROM cscfga__Product_Configuration__c WHERE ID = :pcId];    
          showResignOption = true;
        }
      } else {
        ApexPages.Message wrongFileExtensionMsg = new ApexPages.Message(ApexPages.severity.ERROR,'Incorrect File type uploaded, please ensure that file is of CSV file type');
        ApexPages.addMessage( wrongFileExtensionMsg );
      }
    } catch( DMLException dmlExc){
      System.debug(LoggingLevel.ERROR, 'Error Occurred while trying to insert attachment - ' + dmlExc.getMessage() );
      System.debug(LoggingLevel.ERROR, dmlExc.getStackTraceString() );
    } catch (Exception e) {
      System.debug(LoggingLevel.ERROR, e.getStackTraceString() );
      ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing data. Please make sure the input csv file is correct');
      ApexPages.addMessage(errorMessage);
    }  
  }

  public List<SelectOption> getResignTypeOptions() {
    List<SelectOption> options = new List<SelectOption>();
    List<BT_Resign_Option_Settings__c> resignOptionList= BT_Resign_Option_Settings__c.getall().values();
    options.add(new SelectOption('','--None--'));
    for(BT_Resign_Option_Settings__c rOP : resignOptionList){
        system.debug('***pc.Ave_Days_Remaining_Months__c'+pc.Ave_Days_Remaining_Months__c);
        if(pc.Ave_Days_Remaining_Months__c > rOP.Minimum_Remaining_Months__c && pc.Ave_Days_Remaining_Months__c <= rOP.Maximum_Remaining_Months__c){
            options.add(new SelectOption(rOP.name,rOP.name));  
        }
    }  
    return options;
  }
  
  public void getResignDescription() {
      system.debug('**resignOption'+resignOption);
      if(resignOption != null){system.debug('**resignOption'+resignOption);
        BT_Resign_Option_Settings__c resignDesc = BT_Resign_Option_Settings__c.getValues(resignOption);
        if(resignDesc != null){
            resignDesription = resignDesc.Description__c;
        }
      }
  }

  public PageReference saveDataAndgoToBasket() {
    if(resignOption == Label.BT_ETCSHurdleValue && (applicableETCs == '' || (applicableETCs != '' && Decimal.valueOf(applicableETCs) < 100))){
      ApexPages.Message wrongETCSNumber = new ApexPages.Message(ApexPages.severity.ERROR, Label.BT_ResignETCTooLow);
      ApexPages.addMessage( wrongETCSNumber );
      return null;
    }
    else{
      cscfga__Product_Configuration__c pc;
      if(applicableETCs != '' && Decimal.valueOf(applicableETCs) != null)
        pc = new cscfga__Product_Configuration__c( Id = pcId, Resign_Option__c = resignOption,Applicable_ETC__c = Decimal.valueOf(applicableETCs));
      else
        pc = new cscfga__Product_Configuration__c( Id = pcId, Resign_Option__c = resignOption);
      update pc;
      return new PageReference(Label.BTSFOrganizationLink + '/' + basketId);
    }
  }
}