@IsTest
public class CS_TSCObserverTest  {

	testMethod static void testTSCObserver1() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account acc = CS_TestDataFactory.generateAccount(false, 'Test Acc');
		acc.Sector__c = 'UNALLOCATED';
		insert acc;
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'BT Mobile Sharer');
		cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(false, 'BT Mobile Sharer');
        testOffer.Product_Definitions__c = '[BT Mobile Sharer]';
        insert testOffer;
        
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile Sharer', basket);
        pc.cscfga__Product_Definition__c = prodDef.Id;
        pc.cscfga__Configuration_Offer__c = testOffer.ID;
        insert pc;
        
        cscfga__Product_Configuration__c pc2 = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile Sharer', basket);
        pc2.cscfga__Product_Definition__c = prodDef.Id;
        pc2.cscfga__Configuration_Offer__c = testOffer.ID;
        insert pc2;
		
		cscfga__Offer_Category_Association__c oca = CS_TestDataFactory.generateCategoryDefinitionAssoc(true, prodCategory, testOffer);
		csbb__Product_Configuration_Request__c pcRequest = CS_TestDataFactory.generateProductConfigRequest(true, prodCategory.Id, pc.Id);
		csbb__Product_Configuration_Request__c pcRequest2 = CS_TestDataFactory.generateProductConfigRequest(true, prodCategory.Id, pc2.Id);
    
		CS_TSCObserver o = new CS_TSCObserver();
		o.checkConfigsInBasket(pcRequest2, pc2);
		
		try{
			o.execute(null, null);
		}
		catch(Exception exc){}
	}
}