@isTest
private class Testriskregistercontroller {
    
    static testMethod void firstTest() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }
        Date dt = System.Today();
         Account acc1 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc1.Sector__c = 'other';
        acc1.LOB_Code__c = 'TEST';
        acc1.SAC_Code__c = 'JMMO1231';
        acc1.LE_Code__c = null;
        acc1.AM_EIN__c = '803268119';
        acc1.postcode__c = 'WR5 3RL';
        acc1.Base_Team_Assign_Date__c = Date.today();
        insert acc1;
        Risk_Register_DBAM__c r = new Risk_Register_DBAM__c();
        r.Account__c = acc1.id;
        r.Main_Risk_Reason__c = 'Product';
        r.At_Risk_Rating__c = '6. Won’t lock in';
        r.Status__c = 'Open';
        r.Why_Are_We_At_Risk__c = 'test';
        r.Risk_Deadline__c = dt;
        r.What_is_the_Competitor_Offering__c = 'test';
        r.What_is_BT_Customer_Engagement_Like__c = 'TEST';
        r.How_Do_We_Retain_Win_the_Business__c = 'test';
        r.Additional_Opportunities__c = 'test';
        Insert r;
        ApexPages.currentPage().getParameters().put('Id',r.Id);
        ApexPages.StandardController stc = new ApexPages.StandardController(r);
        riskregistercontroller rc = new riskregistercontroller(stc);
        rc.getContracts();
        
    }
}