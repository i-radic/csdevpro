global class BatchLS2L_Schedule01 implements Schedulable{
        
global void execute(SchedulableContext sc) {
    Date LEM= System.Today() - 90;
    String sLEM = String.valueOf(LEM);  
    
    //LEM Landscape data matchin criteria to create a lead, some of the logic is in the landscape object formula fields returning true.
    String qString = 'SELECT Id, Customer__c, Customer__r.Name, Customer__r.OwnerId, Customer__r.Owner.Department, Customer__r.Sector__c';
    qString = qString + ' ,xLeads_Flag_Calls__c, xLeads_Flag_Lines__c, xLeads_Flag_BB__c, xLeads_Flag_Mobile__c, xLeads_Flag_Switch__c, xLeads_Flag_LAN__c, xLeads_Flag_WAN__c';
    qString = qString + ' ,If_not_BT_who_do_you_use_for_calls__c, Calls_contract_expiry_date__c';
    qString = qString + ' FROM Landscape_BTLB__c';
    qString = qString + ' WHERE (Customer__r.xLead_Last_Created__c < ' + sLEM + ' OR Customer__r.xLead_Last_Created__c = Null)';
    qString = qString + ' AND (xLeads_Flag_Calls__c = \'True\'';
    qString = qString + ' OR xLeads_Flag_Lines__c = \'True\'';
    qString = qString + ' OR xLeads_Flag_BB__c = \'True\'';
    qString = qString + ' OR xLeads_Flag_Mobile__c = \'True\'';
    qString = qString + ' OR xLeads_Flag_Switch__c = \'True\'';
    qString = qString + ' OR xLeads_Flag_LAN__c = \'True\''; 
    qString = qString + ' OR xLeads_Flag_WAN__c = \'True\')';
    BatchLS2L_01LEM b = new BatchLS2L_01LEM(qString);
    database.executebatch(b, 1);
}    
}