global with sharing class CS_ServiceSpecification {

    public String status;
    public String guid;
    public String startDate;
    public String name;
    public String description;
    public String version;
    public String replacedSpecification;
    public String identifier;
    public String endDate;
    public String code;
    public String instanceId;
    public String productConfigurationId;
    public List<SimpleAttribute> simpleAttributes;
    public Map<String, List<ComplexAttribute>> complexAttributes;
    public List<SimpleAttribute> additionalAttributes;
    public String serviceId;

    public CS_ServiceSpecification() {

    }

    public class SimpleAttribute {
        public String name;
        public String value;

        public SimpleAttribute(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    public class ComplexAttribute {
        public String productConfigurationId;
        public List<SimpleAttribute> simpleAttributes;

        public ComplexAttribute() {

        }
    }
}