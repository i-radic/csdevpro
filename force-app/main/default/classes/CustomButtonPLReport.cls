global with sharing class CustomButtonPLReport extends csbb.CustomButtonExt {

    public String performAction (String basketId) {
    	String newUrl = '/apex/c__CS_PLReport?id='+basketId;
        
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    } 
}