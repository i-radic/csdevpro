public with sharing class NIBRandNIERCalculator {
 /**********************************************************************
     Name:  NIBRandNIERCalculator.class()
     Copyright © 2010  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    This class contains two methods, getprodrevlist and refreshrevenuesummary
    getprodrevlist takes and opportunity line item and an additional boolean parameter
    and returns a list of product revenue records with either NIBR or NIER calculations
    
    refreshrevenuesummary takes an opportunity id and refreshes the revenue summary 
    table. This aggregates all of the product revenue records by fiscal year to give total
    NIBR NIER values for a particular fiscal year
                                                          
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
    1.0 -    Greg Scarrtott    22/04/2010        INITIAL DEVELOPMENT              Initial Build: 
    
    ***********************************************************************/
     
    public static boolean isrun =false;
    public static boolean runopplineonce =false; 
    
    public static list<product_revenue__c> getprodrevlist(opportunitylineitem opplineitem,Boolean isNibr){
        Decimal newunitprice = 0;
        newunitprice.setscale(20);
        Decimal newinitialprice = 0;
        newinitialprice.setscale(20);
        
        //Constants aligned to the Opportunity Line Item Category field
        //**************************************************************
        
        final string CATEGORY_MAIN='Main';
        final string CATEGORY_STANDARD='Additional';
        final string CATEGORY_CROSSSELL='Attachment';
        
        Product_Revenue__c prodrevrec = new product_revenue__c();
    
        List<Product_Revenue__c> prodrevenuelist = new list<Product_Revenue__c>(); 
        Decimal monthlypayment = 0;
        monthlypayment.setScale(20);
        
        Boolean bStop = false;
        Boolean addmonthsstop = false;
        Boolean bFirstYear = True;
        Integer nCounter = 0;
        
        Integer fiscalyear = 0;
        Decimal FiscalYearsRevenue =0;
        FiscalYearsRevenue.setScale(20);
        Date firstBillDate;
        Date currentDate;
        Date nextfiscalyearstart;
        Integer monthcount = 0;
        
            
        if (opplineitem.Initial_Cost__c==null){
            opplineitem.Initial_Cost__c=0;
        }
        //If the bill/service start dates have been populated on the Opportunity , then we need to use these.
        //Override what has been selected on the line item
        
        
        //what is the monthly bill ?
        
        //Make the price negative if it is a substitute product
        //A workflow rule also does this and changes the unit-price value
        //of the line item. I have chosen not to do that here to save 
        //doing a DML update on the opportunity line item.
        
        system.debug('GCompare'+opplineitem.category__c + CATEGORY_MAIN + CATEGORY_STANDARD + CATEGORY_CROSSSELL);
        If (opplineitem.category__c == CATEGORY_MAIN||opplineitem.category__c == CATEGORY_STANDARD||opplineitem.category__c == CATEGORY_CROSSSELL){
            system.debug('Constworked');
            newunitprice = opplineitem.unitprice;   
        }else{
            //Negate the price and add the initial cost to the amount.
            newunitprice = (opplineitem.unitprice.abs() * -1) ;
        }
        
        
        If (opplineitem.category__c == CATEGORY_MAIN||opplineitem.category__c == CATEGORY_STANDARD||opplineitem.category__c == CATEGORY_CROSSSELL){
                monthlypayment = ((newunitprice - opplineitem.Initial_Cost__c).divide(opplineitem.Contract_Term_Months__c, 20,system.roundingmode.up)); 
        }else{
                monthlypayment = ((newunitprice + opplineitem.Initial_Cost__c).divide(opplineitem.Contract_Term_Months__c, 20,system.roundingmode.up)); 
        }
                    
        system.debug('Monthly payment is ' + newunitprice + '-' +opplineitem.Initial_Cost__c + '/' +opplineitem.Contract_Term_Months__c );
        system.debug('Coming to ' + monthlypayment);
        
            //Which month is the last bill ? Take the first bill date and add the contract term
            firstBillDate = opplineitem.First_Bill_Date__c; 
            
            system.debug('GSProb'+opplineitem);
            system.debug('GSParentBillDate' + firstbilldate);
              
            Date lastBillDate = firstBillDate.addmonths(opplineitem.contract_term_months__c.intvalue());
            
            
            If(isNibr){
                 currentdate = opplineitem.First_Bill_Date__c;   
            }Else{
                 currentdate = opplineitem.service_ready_date__c;    
            }
               
           
           While (!bstop){
                //Initialise vars
                addmonthsstop = false;
                FiscalYearsRevenue = 0;
                newinitialprice = 0;
                newunitprice=0;
                system.debug('GOV:new prod rev rec' );
                
                prodrevrec = new product_revenue__c();
                //What is this fiscal year and month ?
                //Set it on our new product revenue record
                system.debug('currentdate' + currentdate + ' nextfiscalstart' + nextfiscalyearstart);
                
                If(currentdate.month() > 3){
                    prodrevrec.fiscal_year_commencing__c = date.newInstance(currentdate.year(),4,1);
                }else{
                    prodrevrec.fiscal_year_commencing__c = date.newInstance(currentdate.year() -1,4,1);
                }
                
                //What is the start of the next fiscal year ?
                If (nextfiscalyearstart ==null){
                        nextfiscalyearstart = prodrevrec.fiscal_year_commencing__c.addyears(1);
                }
                
                //Lets keep adding months and bill ammounts until we get to the next fiscal year
                
                While (!addmonthsstop){
                //system.debug('GOV:adding month' );
                
                    //Move the month on by bill cycle
                    If(opplineitem.Billing_Cycle__c=='Monthly'){
                        FiscalYearsRevenue = FiscalYearsRevenue + monthlypayment;
                        monthcount = monthcount+1;
                    }else if(opplineitem.Billing_Cycle__c=='Quarterly'){
                        FiscalYearsRevenue = FiscalYearsRevenue + (monthlypayment*3);
                        monthcount = monthcount + 3;
                    }else if(opplineitem.Billing_Cycle__c=='Annually'){
                        FiscalYearsRevenue = FiscalYearsRevenue + (monthlypayment*12);
                        monthcount = monthcount + 12;
                    }
                    
                    
                    If(opplineitem.Billing_Cycle__c=='Monthly'){
                        currentdate = currentdate.addmonths(1);
                    }else if(opplineitem.Billing_Cycle__c=='Quarterly'){
                        currentdate = currentdate.addmonths(3);
                    }else if(opplineitem.Billing_Cycle__c=='Annually'){
                        currentdate = currentdate.addmonths(12);
                    }
                
                    //system.debug('afterex'+currentdate);
                    //system.debug('monthcount' + monthcount + 'dbtermmonths '+opplineitem.contract_term_months__c);
                    
                    //Has the current date gone into the next fiscal year ?
                    If(currentdate >= nextfiscalyearstart||(monthcount > opplineitem.contract_term_months__c-1) || opplineitem.Billing_Cycle__c == 'Outright Sale' ){ // added by dilip - outright sale
                        addmonthsstop = true;
                    }
               }
                
                //Are we at the end of the contract?
                
                if(monthcount > (opplineitem.contract_term_months__c-1) ){
                    
                    //Test - correct the revenue figure, it will 
                    bStop =true;
                    monthcount =0;
                    
                    prodrevrec.product__c = opplineitem.ProdId__c;
                    prodrevrec.Oppproductid__c = opplineitem.id;
                    
                    If(bFirstYear == true){
                        //Negate the initial cost of this is a substitue product
                        If (opplineitem.category__c == CATEGORY_MAIN||opplineitem.category__c == CATEGORY_STANDARD||opplineitem.category__c == CATEGORY_CROSSSELL){
                            FiscalYearsRevenue=FiscalYearsRevenue+opplineitem.initial_cost__c;
                        }else{
                            FiscalYearsRevenue=FiscalYearsRevenue-opplineitem.initial_cost__c;
                        }
                        system.debug('FirstYearCalc' + FiscalYearsRevenue+ '  ' + opplineitem.initial_cost__c );
                        bFirstYear=false;
                    }
                    If(isNibr==true){
                        prodrevrec.Billed_Revenue_Auto__c = FiscalYearsRevenue;
                    }else{
                        prodrevrec.earned_Revenue_Auto__c = FiscalYearsRevenue;
                    }
                    
                    prodrevrec.Opportunity__c = opplineitem.OpportunityId;
                    prodrevrec.Category__c = opplineitem.Category__c;
                    prodrevenuelist.add(prodrevrec);
                    
                    system.debug('Done with contract'+currentdate);
                    //Move the next fiscal year on
                    nextfiscalyearstart = nextfiscalyearstart.addYears(1);
                
                //Or just this fiscal year ?
                    
                }else{
                    //Store record and move on
                    prodrevrec.product__c = opplineitem.ProdId__c;
                    prodrevrec.Oppproductid__c = opplineitem.id;
                    
                    If(bFirstYear == true){
                    If (opplineitem.category__c == CATEGORY_MAIN||opplineitem.category__c == CATEGORY_STANDARD||opplineitem.category__c == CATEGORY_CROSSSELL){
                            FiscalYearsRevenue=FiscalYearsRevenue+opplineitem.initial_cost__c;
                        }else{
                            FiscalYearsRevenue=FiscalYearsRevenue-opplineitem.initial_cost__c;
                        }
                        bFirstYear=false;
                        system.debug('FirstYearCalc' + FiscalYearsRevenue+ '  ' + opplineitem.initial_cost__c );
                        
                    }
                    
                    If(isNibr==true){
                        prodrevrec.Billed_Revenue_Auto__c = FiscalYearsRevenue;
                    }else{
                        prodrevrec.earned_Revenue_Auto__c = FiscalYearsRevenue;
                    }
                    prodrevrec.Opportunity__c = opplineitem.OpportunityId;
                    prodrevrec.Category__c = opplineitem.Category__c; 
                    prodrevenuelist.add(prodrevrec);
                    
                    //Move the next fiscal year on
                    nextfiscalyearstart = nextfiscalyearstart.addYears(1);
                    
                    if(opplineitem.Billing_Cycle__c == 'Outright Sale' ){ // added by dilip - outright sale
                        bStop = true;
                    }
               }
            }
        
        
        return prodrevenuelist;
    }
    
    
    public void refreshrevenuesummary(id opportunityid){
        //Flush the old records
        system.debug('gotcalledwith'+opportunityid);
        
        List<Revenue_summary__c> deleterecs = new List<Revenue_summary__c>();
        deleterecs = [select id from Revenue_summary__c rs where rs.opportunity__c = :opportunityid];
        delete deleterecs; 
        
        //Get all of the current product revenue records for this opportunity
        List<product_revenue__c> prodrevrecs = [select id,opportunity__c,billed_revenue__c,earned_revenue__c,fiscal_year__c,fiscal_year_commencing__c,product__c from product_revenue__c where opportunity__c = :opportunityid order by fiscal_year_commencing__c];
        List<Revenue_summary__c> revsummarylist = new List<Revenue_summary__c>();
        
        //Loop around the records and create a new summary record when the fiscal year changes
        Date currentfiscalyear;
        Revenue_Summary__c newrevsumrec;
        
        for(product_revenue__c pr : prodrevrecs){
            system.debug('looping DM'+ pr.Id);
                system.debug('looping'+ pr.Fiscal_Year_commencing__c);
                If (pr.fiscal_year_commencing__c !=currentfiscalyear){
                    system.debug('foundnewdate'+pr.Product__c);
                    if (newrevsumrec !=null){
                        revsummarylist.add(newrevsumrec);
                    }
                    //create a new summary record
                    newrevsumrec = new Revenue_Summary__C();
                    
                    currentfiscalyear = pr.fiscal_year_commencing__c;
                    newrevsumrec.Opportunity__c = pr.Opportunity__c;
                    newrevsumrec.fiscal_year_commencing__c = pr.fiscal_year_commencing__c;
                    newrevsumrec.nibr__c = pr.billed_revenue__c;
                    system.debug('adding billed' +pr.billed_revenue__c);
                    
                    newrevsumrec.nier__c = pr.earned_revenue__c;
                    
                }else{
                    //Keep adding to the total
                    if(newrevsumrec != null){
                    if(newrevsumrec.nibr__c!=null){
                        system.debug('adding billed' +pr.billed_revenue__c);
                        
                        //DM
                        system.debug('XXXXX' +pr.billed_revenue__c + ', ' + newrevsumrec.nibr__c + ', ' + pr.Id);
                        
                        newrevsumrec.nibr__c = newrevsumrec.nibr__c + pr.billed_revenue__c;
                    }else{
                        system.debug('adding billed' +pr.billed_revenue__c);
                        newrevsumrec.nibr__c = pr.billed_revenue__c;
                    }
                    
                    if(newrevsumrec.nier__c!=null ){
                        if (pr.earned_revenue__c !=null){
                            newrevsumrec.nier__c = newrevsumrec.nier__c + pr.earned_revenue__c;
                        }
                    }else{
                        	newrevsumrec.nier__c = pr.earned_revenue__c;    
                    	}
                	}
                }    
        }
        //Update last record if list ran out
        revsummarylist.add(newrevsumrec);
        
        if(revsummarylist.size()>0 && newrevsumrec !=null){
            insert revsummarylist;
        }
    } 
}