/**
 * Used for the Product Basket new custom button 'P&L'
 * Button opens the P&L VF page.
 *
 * @author Cloudsense
 */
global with sharing class CustomButtonBTPLAccelerator extends csbb.CustomButtonExt {

	public static final String errorMsg = '{"status":"error","title":"Error","text":"' + Label.BT_NoConfigurationsInBasket + '"}';
	public static final String userProfileErrorMsg = '{"status":"error","title":"Error","text":"' + Label.BT_UserProfileRestriction + '"}';

	public String performAction(String basketId) {
		String action = '';
		String profileList = Label.BT_ProfitAndLossRestrictedProfiles;
		String userProfile = [
			SELECT Name 
			FROM profile 
			WHERE Id = :Userinfo.getProfileId()
		].Name;

		if(profileList.contains(userProfile))
			action = userProfileErrorMsg;
		else {
            Map<Id, cscfga__Product_Configuration__c> configs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{new cscfga__Product_Basket__c(Id = basketId)});
            cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
			CS_ProductBasketService.prepareTechFund(basket, configs, 'Tech_Fund__c');
            
            CS_Util.upsertReportConfigurationAttachment(configs.keySet());
			action = '{"status":"ok","redirectURL":"/apex/cspl__PLReportNew?id=' + basketId + '&npvMode=false"}';
		}
		
		return action;
	}
}