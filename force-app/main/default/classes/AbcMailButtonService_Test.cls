@isTest
private class AbcMailButtonService_Test {
    static testMethod void UnitTest() {
        AbcMailButtonService abc = new AbcMailButtonService ();
       
        User B2BUser1,B2BUser2;
        
        Profile B2BProfile = [select id from profile where name = 'BTLB: RDs and PSMs'];
        B2BUser2 = new User(alias = 'B2B2', email = 'B2B132cr@bt.it',
        emailencodingkey = 'UTF-8', lastname = 'Testing22222 B2B1', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile.Id, timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr2222@testemail.com',
        EIN__c = 'B2B5cr');
        insert B2BUser2;
        
        B2BUser1 = new User(alias = 'B2B1', email = 'B2B13cr@bt.it',
        emailencodingkey = 'UTF-8', lastname = 'Testing B2B1', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile.Id, timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
        EIN__c = 'B2B3cr',ManagerId =B2BUser2.Id);
        
        insert B2BUser1;
        System.runAs(B2BUser1) {
        iDepot__c iDep = new iDepot__c(Order_Reference__c = 'VOL011-12',Business_Plan_Type__c = 'BT Business Complete',
                                       Date_Plan_Submitted__c= Date.today(),Type_of_Order__c ='Defence',Committed_Call_Spend__c = 800,
                                       Which_attribute_are_you_amending__c = 'Commitment',OwnerId = B2BUser1.Id );

        insert(iDep);
        test.startTest();
        AbcMailButtonService.SendEmailNotification(iDep.Id);
        test.stopTest();
        }     
    }
}