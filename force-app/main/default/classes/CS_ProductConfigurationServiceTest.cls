@IsTest
public class CS_ProductConfigurationServiceTest  {
	private static Account acc;
	private static Opportunity opp;
	private static cscfga__Product_Basket__c basket;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
	}

	@IsTest
	public static void testUpdateDiscounts() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();

		/*CS_DiscountJSON dJSON = new CS_DiscountJSON();
		CS_DiscountJSON.Discount disc = new CS_DiscountJSON.Discount();
		disc.type = 'absolute';
		disc.amount = 99;
		disc.description = 'Test Discount';
		disc.discountCharge = 'oneOff';
		disc.source = 'Test Source';

		dJSON.discounts = new List<CS_DiscountJSON.Discount>();

		dJSON.discounts.add(disc);

		String jsonString = JSON.serialize(dJSON);*/

		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
		pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Manual -Infinity","discountCharge":"__PRODUCT__","description":"test discount description","amount":"15","version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
		pc.Volume__c = 10;
		INSERT pc;

		cscfga__Product_Configuration__c oldPc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC Old', basket);
		oldPc.Volume__c = 5;
		INSERT oldPc;

		Test.startTest();
		CS_ProductConfigurationService.updateDiscounts(pc, oldPc);
		Test.stopTest();

		CS_DiscountJSON newdJSON = (CS_DiscountJSON) JSON.deserialize(pc.cscfga__discounts__c,  CS_DiscountJSON.class);
		//CS_DiscountJSON.Discount newDisc = newdJSON.discounts.get(0);

		//Boolean check = newDisc.amount == disc.amount / oldPc.Volume__c * pc.Volume__c;
		//System.assert(check);

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testDeleteProdConfigTotals() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();

		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(true, 'Test PC', basket);

		Basket_Totals__c basketTotals = new Basket_Totals__c(Name = 'Test Basket Totals');
		INSERT basketTotals;

		Product_Configuration_Totals__c prodConfigTotal = new Product_Configuration_Totals__c( Name = 'Test Prod Config Total', Product_Configuration__c = pc.Id, Basket_Totals__c = basketTotals.Id);
		INSERT prodConfigTotal;

		Test.startTest();
		CS_ProductConfigurationService.deleteProdConfigTotals(new List<cscfga__Product_Configuration__c>{pc});
		Test.stopTest();

		List<Product_Configuration_Totals__c> prodConfigTotalList = [SELECT Id, Name, Product_Configuration__c FROM Product_Configuration_Totals__c WHERE Product_Configuration__c = :pc.Id];

		System.assert(prodConfigTotalList.size() == 0);

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testCreateProductConfigurationTotals() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();
		cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'Test Product Name');
		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
		pc.cscfga__Product_Definition__c = prodDef.Id;
		pc.Product_Type__c = 'Test Product Type';
		pc.Product_Name__c = 'Test Product Name';
		pc.Volume__c = 10;
		pc.Voice_Subscribers__c = 2;
		pc.Data_Subscribers__c = 3;
		INSERT pc;

		Basket_Totals__c basketTotals = new Basket_Totals__c(Name = 'Test Basket Totals');
		INSERT basketTotals;

		Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>{pc.Id => pc};

		Test.startTest();
		CS_ProductConfigurationService.createProductConfigurationTotals(configs);
		Test.stopTest();

		List<Product_Configuration_Totals__c> prodConfigTotalList = [SELECT Id, Name, Product_Configuration__c FROM Product_Configuration_Totals__c WHERE Product_Configuration__c = :pc.Id];
		System.assertEquals(pc.Id, prodConfigTotalList[0].Product_Configuration__c);

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testUpdateCreditFunds() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();
		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
		pc.Tenure__c = 5;
		pc.cspl__Month_Term__c = 12;
		pc.cscfga__Configuration_Status__c = 'Valid';
		INSERT pc;
		
		basket = [SELECT Id, Name, (SELECT Id, Name, Tenure__c, cspl__Month_Term__c, cscfga__Configuration_Status__c, cscfga__Product_Basket__c FROM cscfga__Product_Configurations__r) FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
		Map<Id, cscfga__Product_Basket__c> baskets = new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket};

		Test.startTest();
		Map<Id, cscfga__Product_Configuration__c> pcMap = CS_ProductConfigurationService.updateCreditFunds(baskets);
		Test.stopTest();

		System.assertEquals('Incomplete', pcMap.get(pc.Id).cscfga__Configuration_Status__c);

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	//@IsTest
	//public static void testCalculateApprovals() {
		//No_Triggers__c notriggers =  new No_Triggers__c();
		//notriggers.Flag__c = true;
		//INSERT notriggers;

		//createTestData();
		//cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(true, 'Test PC', basket);
		//Map<Id, cscfga__Product_Configuration__c> productConfigurationsMap = new Map<Id, cscfga__Product_Configuration__c> {pc.Id => pc};
		//String CONTROLLER_NAME = 'Test';
		//Boolean refreshAtt = false;

		//Test.startTest();
		//String returnMessage = CS_ProductConfigurationService.calculateApprovals(productConfigurationsMap,basket, CONTROLLER_NAME, refreshAtt);
		//Test.stopTest();
		//System.debug('sasa:: returnMessage: ' + returnMessage);

		//notriggers.Flag__c = false;
		//DELETE notriggers;
	//}

	@IsTest
	public static void testUpdateUsageProfileCalcs() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();
		Usage_Profile__c usageProfile = CS_TestDataFactory.generateUsageProfile(false, acc);
		usageProfile.Product_Basket__c = basket.Id;
		INSERT usageProfile;
		usageProfile = [SELECT 
							Id, 
							Name, 
							Account__c, 
							Product_Basket__c, 
							Data_Only_Connections__c, 
							Total_Number_of_Connections__c, 
							Expected_Data__c,
							Voice_Roaming_EU__c,
							Active__c,
							Expected_SMS__c,
							Expected_Voice__c,
							Calls_to_other_UK_mobile_networks__c,
							AssociatedLE__c,
							Other_BT_Minutes_Total__c,
							Expected_Data_per_User__c,
							Domestic_Text__c,
							UK_Fixed_Line_Numbers__c,
							FreePhone_BT__c,
							IDD_Zone_1__c,
							IDD_Zone_2__c,
							IDD_Zone_3__c,
							IDD_Zone_4__c,
							IDD_Zone_5__c,
							IDD_Zone_6__c,
							Voice_Roaming_Zone_3_RoW__c,
							Voice_Roaming_Zone_4_USA_Canada__c,
							Voice_Roaming_Zone_5__c,
							Voice_Roaming_Zone_6_RoW__c,
							Own_Account_Fixed_Line_Numbers__c,
							Own_Account_Mobiles__c,
							Other_BT_Mobiles__c,
							Other_UK_Mobile_Operators__c,
							Non_Geographic__c,
							Roaming_Inter_Country__c,
							Voicemail__c
						FROM Usage_Profile__c WHERE Id = :usageProfile.Id];

		List<String> rateCardLineNames = new List<String>{
			'Data Usage (MB)',
			'Domestic Text',
			'UK Fixed Line Numbers',
			'FreePhone',
			'IDD Zone 1',
			'IDD Zone 2',
			'IDD Zone 3',
			'IDD Zone 4',
			'IDD Zone 5',
			'IDD Zone 6',
			'Voice Roaming - EU',
			'Voice Roaming Zone 3',
			'Voice Roaming Zone 4',
			'Voice Roaming Zone 5',
			'Voice Roaming Zone 6',
			'Own Account Fixed Line Numbers',
			'Own Account Mobiles',
			'Other BT Mobiles',
			'Other UK Mobile Operators',
			'Non Geographic',
			'Roaming Inter-Country',
			'Voicemail'
		};

		cspmb__Rate_Card__c rateCard = new cspmb__Rate_Card__c(name = 'Test Rate Card', cspmb__Is_Active__c = true, cspmb__Product_Definition_Name__c = 'BT Mobile Flex');
		INSERT rateCard;

		List<cspmb__Rate_Card_Line__c> rateCardLines = new List<cspmb__Rate_Card_Line__c>();
		for (Integer i = 0; i < rateCardLineNames.size(); i++){
			cspmb__Rate_Card_Line__c rateCardLine = new cspmb__Rate_Card_Line__c(name = rateCardLineNames[i], cspmb__rate_card__c = rateCard.Id, quote_price__c = 1500.05, 
				quote_cost__c = 100.50, quote_price_percentage_discount_value__c = 5.5);
			rateCardLines.add(rateCardLine);
		}
		INSERT rateCardLines;

		Set<Id> rclIds = new Set<Id>();
		for(cspmb__Rate_Card_Line__c rcl : rateCardLines) {
			rclIds.add(rcl.Id);
		}
		rateCardLines = [SELECT id, name, cspmb__rate_card__c, quote_price__c, quote_cost__c, quote_price_percentage_discount_value__c FROM cspmb__Rate_Card_Line__c WHERE id IN :rclIds];

		RateCardWrapper rateCardWrapper = new RateCardWrapper();
		rateCardWrapper.rateCard = rateCard;
		rateCardWrapper.rateCardLines = new List<cspmb__Rate_Card_Line__c>();
		for(cspmb__Rate_Card_Line__c rcl : rateCardLines) {
			rateCardWrapper.rateCardLines.add(rcl);
		}

		Map<String,Object> jsonRCL = new Map<String,Object>{'rateCard' => rateCardWrapper.rateCard, 'rateCardLines' => rateCardWrapper.rateCardLines };
		String jsonString = JSON.serialize(jsonRCL);

		//field names need converting to lowercase, otherwise they are automatically saved as camel case, causing a null pointer exception in main class
		jsonString = jsonString.replace('Name', 'name');
		jsonString = jsonString.replace('Quote_Price__c', 'quote_price__c');
		jsonString = jsonString.replace('Quote_Cost__c', 'quote_cost__c');
		jsonString = jsonString.replace('Quote_Price_Percentage_Discount_Value__c', 'quote_price_percentage_discount_value__c');

		System.debug(LoggingLevel.INFO, 'sasa:: jsonString: ' + jsonString);

		List<String> attributeNameList = new List<String>{
			'RateCardLine',
			'Usage Profile',
			'Volume'
		};

		cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Flex');

		List<cscfga__Attribute_Definition__c> attDefsToInsert = new List<cscfga__Attribute_Definition__c>();
		cscfga__Attribute_Definition__c attDefRateCardLine = new cscfga__Attribute_Definition__c(Name = 'RateCardLine', cscfga__Product_Definition__c = prodDef.Id, cscfga__Type__c = 'User Input', cscfga__Data_Type__c = 'String');
		cscfga__Attribute_Definition__c attDefUsageProfile = new cscfga__Attribute_Definition__c(Name = 'Usage Profile', cscfga__Product_Definition__c = prodDef.Id, cscfga__Type__c = 'Lookup', cscfga__Data_Type__c = 'String', cscfga__Default_Value__c = usageProfile.Id);
		cscfga__Attribute_Definition__c attDefVolume = new cscfga__Attribute_Definition__c(Name = 'Volume', cscfga__Product_Definition__c = prodDef.Id, cscfga__Type__c = 'User Input', cscfga__Data_Type__c = 'Decimal', cscfga__configuration_output_mapping__c = 'Volume__c');
		attDefsToInsert.add(attDefRateCardLine);
		attDefsToInsert.add(attDefUsageProfile);
		attDefsToInsert.add(attDefVolume);
		INSERT attDefsToInsert;

		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
		pc.cscfga__Product_Definition__c = prodDef.Id;
		INSERT pc;
		pc = [SELECT Id, Name, cscfga__Product_Definition__c, cscfga__Key__c, Usage_Profile_Data_Cost__c, Usage_Profile_Data_Revenue__c, Usage_Profile_Voice_Cost__c, Usage_Profile_Voice_Revenue__c FROM cscfga__Product_Configuration__c WHERE Id = :pc.Id];

		//System.assertEquals(null, pc.Usage_Profile_Voice_Revenue__c);

		List<cscfga__Attribute__c> attributesList = new List<cscfga__Attribute__c>();

		cscfga__Attribute__c attRateCardLine = new cscfga__Attribute__c(Name = 'RateCardLine', cscfga__Value__c = jsonString, cscfga__Attribute_Definition__c = attDefRateCardLine.Id,
                cscfga__Product_Configuration__c = pc.Id, cscfga__is_active__c = true);
		cscfga__Attribute__c attUsageProfile = new cscfga__Attribute__c(Name = 'Usage Profile', cscfga__Value__c = usageProfile.Id, cscfga__Attribute_Definition__c = attDefUsageProfile.Id,
                cscfga__Product_Configuration__c = pc.Id, cscfga__is_active__c = true);
		cscfga__Attribute__c attVolume = new cscfga__Attribute__c(Name = 'Volume', cscfga__Value__c = '99', cscfga__Attribute_Definition__c = attDefVolume.Id,
                cscfga__Product_Configuration__c = pc.Id, cscfga__is_active__c = true);
		attributesList.add(attRateCardLine);
		attributesList.add(attUsageProfile);
		attributesList.add(attVolume);
		INSERT attributesList;


		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c> {pc};
		Map<Id, cscfga__Product_Definition__c> definitions = new Map<Id, cscfga__Product_Definition__c> {prodDef.Id => prodDef};
		Map<Id, cscfga__Attribute_Definition__c> attDefs = new Map<Id, cscfga__Attribute_Definition__c>();
		for (cscfga__Attribute_Definition__c attDef : attDefsToInsert) {
			attDefs.put(attDef.Id, attDef);
		}

		Test.startTest();
		CS_ProductConfigurationService.updateUsageProfileCalcs(attributesList, configs, definitions, attDefs);
		Test.stopTest();

		System.assertNotEquals(null, pc.Usage_Profile_Voice_Revenue__c);

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testGetRevenueForCallType() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();

		decimal quotePrice = 10;
		decimal quoteCost = 5;

		Usage_Profile__c usageProfile = CS_TestDataFactory.generateUsageProfile(false, acc);
		usageProfile.Product_Basket__c = basket.Id;
		INSERT usageProfile;

		usageProfile = [SELECT 
							Id, 
							Name, 
							Account__c, 
							Product_Basket__c, 
							Data_Only_Connections__c, 
							Total_Number_of_Connections__c, 
							Expected_Data__c,
							Voice_Roaming_EU__c,
							Active__c,
							Expected_SMS__c,
							Expected_Voice__c,
							Calls_to_other_UK_mobile_networks__c,
							AssociatedLE__c,
							Other_BT_Minutes_Total__c,
							Expected_Data_per_User__c,
							Domestic_Text__c,
							UK_Fixed_Line_Numbers__c,
							FreePhone_BT__c,
							IDD_Zone_1__c,
							IDD_Zone_2__c,
							IDD_Zone_3__c,
							IDD_Zone_4__c,
							IDD_Zone_5__c,
							IDD_Zone_6__c,
							Voice_Roaming_Zone_3_RoW__c,
							Voice_Roaming_Zone_4_USA_Canada__c,
							Voice_Roaming_Zone_5__c,
							Voice_Roaming_Zone_6_RoW__c,
							Own_Account_Fixed_Line_Numbers__c,
							Own_Account_Mobiles__c,
							Other_BT_Mobiles__c,
							Other_UK_Mobile_Operators__c,
							Non_Geographic__c,
							Roaming_Inter_Country__c,
							Voicemail__c
						FROM Usage_Profile__c WHERE Id = :usageProfile.Id];

		List<String> rateLineNameList = new List<String>{
			'Data Usage (MB)',
			'Domestic Text',
			'UK Fixed Line Numbers',
			'FreePhone',
			'IDD Zone 1',
			'IDD Zone 2',
			'IDD Zone 3',
			'IDD Zone 4',
			'IDD Zone 5',
			'IDD Zone 6',
			'Voice Roaming - EU',
			'Voice Roaming Zone 3',
			'Voice Roaming Zone 4',
			'Voice Roaming Zone 5',
			'Voice Roaming Zone 6',
			'Own Account Fixed Line Numbers',
			'Own Account Mobiles',
			'Other BT Mobiles',
			'Other UK Mobile Operators',
			'Non Geographic',
			'Roaming Inter-Country',
			'Voicemail'
		};

		Map<String, CS_ProductConfigurationService.WrapperRevenueCost> wrcMap = new Map<String, CS_ProductConfigurationService.WrapperRevenueCost>();
		Test.startTest();
		for (Integer i = 0; i < rateLineNameList.size(); i++){
			CS_ProductConfigurationService.WrapperRevenueCost wrc = CS_ProductConfigurationService.GetRevenueForCallType(rateLineNameList[i], quotePrice, quoteCost, usageProfile);
			wrcMap.put(rateLineNameList[i], wrc);
		}
		Test.stopTest();

		System.assertEquals(0.5, wrcMap.get('Data Usage (MB)').revenue);
		System.assertEquals(25, wrcMap.get('Data Usage (MB)').cost);

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	private class RateCardWrapper {
		public cspmb__rate_card__c rateCard;
		public List<cspmb__Rate_Card_Line__c> rateCardLines;
	}
}