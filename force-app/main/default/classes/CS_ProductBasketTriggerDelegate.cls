public with sharing class CS_ProductBasketTriggerDelegate extends CS_TriggerHandler.DelegateBase {
    Map<Id, String> customerAppleIDMap;
    Map<Id, Opportunity> opportunityMap;
    Map<Id, Boolean> hasUsageProfile;
    Map<Id, Id> activeUsageProfile;
    Map<Id, Attachment> usageProfileAttachments;
    List<Opportunity> approvedOpportunities;
    List<Attachment> attachmentsToInsert;
    List<Approval.ProcessSubmitRequest> requests;

    // do any preparation here - bulk loading of data etc
    public override void prepareBefore() {
        activeUsageProfile = new Map<Id, Id>();
        hasUsageProfile = new Map<Id, Boolean>();
        usageProfileAttachments = new Map<Id, Attachment>();
        attachmentsToInsert = new List<Attachment>();
        customerAppleIDMap = new Map<Id, String>();
        Set<Id> associatedLEs = new Set<Id>();
        requests = new List<Approval.ProcessSubmitRequest>();
        approvedOpportunities = new List<Opportunity>();


        if (trigger.isInsert) {
            Map<id,id> basketCustomerContactMap = new Map<id,id>();
            List<cscfga__Product_Basket__c> baskets = (List<cscfga__Product_Basket__c>) Trigger.new;
            Set<Id> opporIds = new Set<Id>();
            List<cscfga__Product_Basket__c> newBasketList = (List<cscfga__Product_Basket__c>) Trigger.new;
            for (cscfga__Product_Basket__c basket : newBasketList) {
                if (basket.cscfga__Opportunity__c != null)
                    opporIds.add(basket.cscfga__Opportunity__c);
            }
            System.debug(LoggingLevel.WARN, '@CS_ProductBasketTriggerDelegate.prepareBefore.oppContactRoles');
            List<OpportunityContactRole> oppContactRoles = [
                    SELECT id, IsPrimary, ContactId, OpportunityId
                    FROM OpportunityContactRole
                    WHERE IsPrimary = true AND OpportunityId in:opporIds
            ];

            if(oppContactRoles.size() > 0 && oppContactRoles != null){
                for(OpportunityContactRole cr : oppContactRoles){
                    basketCustomerContactMap.put(cr.OpportunityId,cr.ContactId);
                }
            }
            for (cscfga__Product_Basket__c basket : baskets) {
                if(basketCustomerContactMap.get(basket.cscfga__Opportunity__c) != null)
                    basket.Customer_Contact__c = basketCustomerContactMap.get(basket.cscfga__Opportunity__c);
            }

            List<Basket_Totals__c> basketTotalsList = new List<Basket_Totals__c>();
            Set<Id> oppIds = new Set<Id>();
            Set<Id> accountIds = new Set<Id>();

            for(cscfga__Product_Basket__c basket : baskets){
                Basket_Totals__c bt = new Basket_Totals__c();
                basketTotalsList.add(bt);
                basket.Default_Usage_Profile_ID__c = Label.bt_default_usage_profile_id;
                if (basket.cscfga__Opportunity__c != null)
                    oppIds.add(basket.cscfga__Opportunity__c);
                hasUsageProfile.put(basket.cscfga__Opportunity__c, false);
            }

            System.debug(LoggingLevel.WARN, '@CS_ProductBasketTriggerDelegate.prepareBefore.basketTotalsList');

            INSERT basketTotalsList;

            for(Integer i = 0; i < baskets.size(); i++){
                baskets[i].Basket_Totals__c = basketTotalsList[i].id;
            }

            if (!oppIds.isEmpty()) {
                System.debug(LoggingLevel.WARN, '@CS_ProductBasketTriggerDelegate.prepareBefore.opps');
                List<Opportunity> opps = [
                        SELECT Id, AccountId, AssociatedLE__c
                        FROM Opportunity
                        WHERE id IN :oppIds
                ];
                opportunityMap = new Map<Id, Opportunity>(opps);

                for (Opportunity opp : opps) {
                    accountIds.add(opp.AccountId);
                    associatedLEs.add(opp.AssociatedLE__c);
                }

                if (!accountIds.isEmpty()) {
                    System.debug(LoggingLevel.WARN, '@CS_ProductBasketTriggerDelegate.prepareBefore.usageProfiles');

                    List<Usage_Profile__c> usageProfiles = [Select Id, Active__c, Account__c, AssociatedLE__c
                    from Usage_Profile__c
                    where AssociatedLE__c in :associatedLEs
                    and Active__c = true
                    ];

                    System.debug(LoggingLevel.WARN, '@CS_ProductBasketTriggerDelegate.prepareBefore.accountsOpportunity');

                    List<Account> accountsOpportunity = [
                            SELECT Id, Customer_Apple_Id__c
                            FROM Account
                            WHERE Id IN :accountIds
                    ];
                    System.debug(LoggingLevel.WARN, '@CS_ProductBasketTriggerDelegate.prepareBefore.accountsOpportunity AFTER');


                    Map<Id, boolean> accountUsageProfile = new Map<Id, boolean>();
                    // Map of Account id and Usage Profile Id
                    Map<Id, Id> accountActiveUsageProfileId = new Map<Id, Id>();
                    if (!usageProfiles.isEmpty()) {
                        for (Usage_Profile__c up : usageProfiles) {
                            accountUsageProfile.put(up.AssociatedLE__c, true);
                            // Populate the map of Account Id and Active Usage Profile Id
                            accountActiveUsageProfileId.put(up.AssociatedLE__c, up.Id);
                        }
                        for (Opportunity opp : opps) {
                            if(opp.AssociatedLE__c != null) {
                                if (accountUsageProfile.get(opp.AssociatedLE__c) != null) {
                                    hasUsageProfile.put(opp.Id, true);
                                    // Populate the map of Opp Id and Active Usage Profile Id
                                    activeUsageProfile.put(opp.Id, accountActiveUsageProfileId.get(opp.AssociatedLE__c));
                                }
                            }
                        }
                    }

                    Map<Id, String> accountCustomerAppleId = new Map<Id, String>();
                    if (!accountsOpportunity.isEmpty()) {
                        for (Account acc : accountsOpportunity) {
                            accountCustomerAppleId.put(acc.Id, acc.Customer_Apple_Id__c);
                        }
                        for (Opportunity opp : opps) {
                            if (accountCustomerAppleId.get(opp.AccountId) != null)
                                customerAppleIDMap.put(opp.Id, accountCustomerAppleId.get(opp.AccountId));
                        }
                    }
                }
            }
        }

        if(Trigger.isUpdate) {
            for (cscfga__Product_Basket__c bskt : (List<cscfga__Product_Basket__C>) Trigger.new) {
                if(bskt.BT_Basket_Approval_Status__c == 'In approval process')
                    bskt.Incremental_EBITDA_Old__c = bskt.Incremental_EBITDA_Percentage__c;
            }
        }
        System.debug(LoggingLevel.WARN, '@CS_ProductBasketTriggerDelegate.prepareBefore END');
    }

    // do any preparation here - bulk loading of data etc
    public override void prepareAfter() {
        activeUsageProfile = new Map<Id, Id>();
        hasUsageProfile = new Map<Id, Boolean>();
        usageProfileAttachments = new Map<Id, Attachment>();
        attachmentsToInsert = new List<Attachment>();
        requests = new List<Approval.ProcessSubmitRequest>();
        approvedOpportunities = new List<Opportunity>();

        if (trigger.isInsert) {
            Set<Id> oppIds = new Set<Id>();
            List<cscfga__Product_Basket__c> baskets = (List<cscfga__Product_Basket__c>) Trigger.new;
            for (cscfga__Product_Basket__c basket : baskets) {
                if (basket.cscfga__Opportunity__c != null) {
                    oppIds.add(basket.cscfga__Opportunity__c);
                    hasUsageProfile.put(basket.cscfga__Opportunity__c, false);
                }
            }
            if (!oppIds.isEmpty()) {
                Set<Id> associatedLEs = new Set<Id>();
                List<Opportunity> opps = [Select Id, AccountId ,Account_SAC__c, AssociatedLE__c
                from Opportunity
                where id in :oppIds
                ];
                for (Opportunity opp : opps) {
                    associatedLEs.add(opp.AssociatedLE__c);
                }
                if (!associatedLEs.isEmpty()) {
                    List<Usage_Profile__c> usageProfiles = Database.query(
                            'select ' + CS_Utils.geCustomtSobjectFields('Usage_Profile__c') +
                                    ' from Usage_Profile__c' +
                                    ' where Active__c = true' +
                                    ' and AssociatedLE__c in :associatedLEs'
                    );
                    Map<Id, Attachment> accountUsageProfile = new Map<Id, Attachment>();
                    if (!usageProfiles.isEmpty()) {
                        for (Usage_Profile__c up : usageProfiles) {
                            Attachment att = new Attachment(
                                    Name = 'UsageProfile',
                                    Body = Blob.valueOf(JSON.serializePretty(new List<Usage_Profile__c>{up}))
                            );
                            accountUsageProfile.put(up.AssociatedLE__c, att);
                        }
                        for (Opportunity opp : opps) {
                            //New changes to associate the Default Usage Profile for partner accounts
                            System.debug('Account_SAC__c===='+opp.Account_SAC__c);
                            if (opp.AssociatedLE__c != null && (accountUsageProfile.get(opp.AssociatedLE__c) != null) && (opp.Account_SAC__c!= 'Non Managed Operational Account'))
                            {
                                System.debug('INSERT USAGE PROFILE FROM ACCOUNT');
                                usageProfileAttachments.put(opp.Id, accountUsageProfile.get(opp.AssociatedLE__c));
                            }
                            else
                            {
                                System.debug('DO NOT INSERT USAGE PROFILE FROM ACCOUNT');

                                List<Usage_Profile__c> defaultUsageProfile = Database.query(
                                        'select ' + CS_Utils.geCustomtSobjectFields('Usage_Profile__c') +
                                                ' from Usage_Profile__c' +
                                                ' where Active__c = true' +
                                                ' and Name = \'A\'  LIMIT 1 '
                                );

                                System.debug('defaultUsageProfile '+defaultUsageProfile .size());
                                if (defaultUsageProfile.size() > 0)
                                {
                                    System.debug('Defined defaultUsageProfile '+defaultUsageProfile[0].id);
                                    Attachment att1 = new Attachment(
                                            Name = 'UsageProfile',
                                            Body = Blob.valueOf(JSON.serializePretty(new List<Usage_Profile__c>{defaultUsageProfile[0] })));
                                    usageProfileAttachments.put(opp.Id, att1);
                                }
                            }
                        }
                    }
                }
            }
        }
       
    }

    // Apply before insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeInsert(sObject o) {
        cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c) o;

        if(opportunityMap != null && opportunityMap.containsKey(basket.cscfga__Opportunity__c)) {
            basket.csbb__Account__c = opportunityMap.get(basket.cscfga__Opportunity__c).AccountId;
        }

        // Assign the Customer Apple ID
        if(customerAppleIDMap.get(basket.cscfga__Opportunity__c) != null)
            basket.Customer_Apple_Id__c = customerAppleIDMap.get(basket.cscfga__Opportunity__c);

        if (hasUsageProfile.get(basket.cscfga__Opportunity__c) != null) {
            basket.Usage_Profile__c = hasUsageProfile.get(basket.cscfga__Opportunity__c);
            // Assign the active Usage Profile Id
            basket.Active_Usage_Profile__c = activeUsageProfile.get(basket.cscfga__Opportunity__c);
        } else {
            basket.Usage_Profile__c = false;
            // Clear the active Usage Profile Id
            basket.Active_Usage_Profile__c = null;
        }

        CS_ProductBasketService.calculateBasketTotals(new Map<Id, cscfga__Product_Configuration__c>(), basket);
        basket.Total_Charges_NPV__c = 0;
        basket.Total_Cost_NPV__c = 0;
    }

    // Apply before update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    // updates totals and thunderhead fields on basket
    public override void beforeUpdate(sObject old, sObject o) {
        cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c) o;

        if (CS_ProductBasketService.containsCalculateProductGroup(basket.Id)) {
            return;
        }

        if(basket.SOV_GM_V2__c != null && basket.SOV_GM_V2__c < basket.NPV_GOM_Before_Commission2__c * 1.5 ) {
            basket.SOV_GM_Before_Commission_V2__c = basket.SOV_GM_V2__c;
        }
        else {
            basket.SOV_GM_Before_Commission_V2__c = basket.NPV_GOM_Before_Commission2__c * 1.5;
        }

        if(basket.Hardware_Charges__c != null && basket.NPV_Tech_Fund__c != null) {
            if(basket.Hardware_Charges__c > -basket.NPV_Tech_Fund__c) {
                basket.TechFundDevicesv2__c = basket.NPV_Tech_Fund__c;
            }
            else {
                basket.TechFundDevicesv2__c = ((-basket.NPV_Tech_Fund__c -basket.Hardware_Charges__c)*0.9 + basket.Hardware_Charges__c)*-1;
            }
        }

        if(basket.BT_Flag__c != null && basket.EE_Flag__c != null) {
            if(basket.BT_Flag__c > 0 && basket.EE_Flag__c > 0) {
                basket.EE_BT_Flag__c = 1;
            }
            else {
                basket.EE_BT_Flag__c = 0;
            }
        }
         if(basket.Product_Definitions__c!=null && basket.Product_Definitions__c.contains('Future Mobile')){ 
            basket.Implementation_Form_Valid__c=true;
        }
    }

    // Apply before delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeDelete(sObject o) {

    }

    // Apply after insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterInsert(sObject o) {
        cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c) o;
        if (usageProfileAttachments.get((Id) o.get('cscfga__Opportunity__c')) != null) {
            Attachment att = usageProfileAttachments.get((Id) o.get('cscfga__Opportunity__c'));
            att.ParentId = (Id) o.get('Id');
            attachmentsToInsert.add(att);
        }
    }

    // Apply after update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUpdate(sObject old, sObject o) {
        cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c) o;
        cscfga__Product_Basket__c oldBasket = (cscfga__Product_Basket__c) old;

        if (CS_ProductBasketService.containsCalculateProductGroup(basket.Id)) {
            return;
        }

        if (basket.Approval_Status__c == 'Commercially Approved' && oldBasket.Approval_Status__c != 'Commercially Approved') {
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval');
            req1.setObjectId(basket.Id);
            requests.add(req1);
        }
        else if(basket.Approval_Status__c == 'Approved' && oldBasket.Approval_Status__c != 'Approved') {
            approvedOpportunities.add(new Opportunity(Id = basket.cscfga__Opportunity__c, Deal_Approved_in_CloudSense__c = 'Yes'));
        }
        if(basket.cscfga__Products_In_Basket__c == ''){ //mpk CR12500
            basket.PromoOffer__c=false;
        }
       
    }

    // Apply after delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterDelete(sObject o) {

    }

    // Apply after undelete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUndelete(sObject o) {

    }

    // finish logic - process stored records and perform any dml action
    // flags and logic for custom special conditions eligibility rule
    public override void finish() {
        if (!requests.isEmpty()) {
            Approval.ProcessResult[] processResults = Approval.process(requests, true);
        }
        if (!attachmentsToInsert.isEmpty()) {
            insert attachmentsToInsert;
        }
        if(!approvedOpportunities.isEmpty()) {
            update approvedOpportunities;
        }
    }
}