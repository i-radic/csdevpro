/**********************************************************************
     Name:  Test_Bulk_Campaign_Load.class()
     Copyright © 2010  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    This class contains unit tests for validating the behavior of Apex classes
    for the Bulk Campaign Load functionality.
 
    Unit tests are class methods that verify whether a particular piece
    of code is working properly. Unit test methods take no arguments,
    commit no data to the database, and are flagged with the testMethod
    keyword in the method definition.
    
    All test methods in an organization are executed whenever Apex code is deployed
    to a production organization to confirm correctness, ensure code
    coverage, and prevent regressions. All Apex classes are
    required to have at least 75% code coverage in order to be deployed
    to a production organization. In addition, all triggers must have some code coverage.
      
    The @isTest class annotation indicates this class only contains test
    methods. Classes defined with the @isTest annotation do not count against
    the organization size limit for all Apex scripts.
     
    See the Apex Language Reference for more information about Testing and Code Coverage.
 
    This test class contains two methods, for testing BTLB and Corporate.
    
    Testing Batch Apex requires that the executeBatch method is called directly.
    In order words it is not possible to call a class which in turn calls the batch apex.
    Because batch Apex runs asynchronously it most also be surrounded with Test.startTest() and
    Test.stopTest() statements.
    
    Another condition on testing batch apex is that the execute method can only be called once
    from a single test.
    
    Becuase the batch apex process functions differently for BTLB and Corporate, i have split the testing
    of the two into separate methods and call the execute method once from each.
        
 
                                                      
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
    1.0 -    Greg Scarrott    2/05/2010        INITIAL DEVELOPMENT              Initial Build: 
    
    ***********************************************************************/
    
    
@isTest
private class Test_Bulk_Campaign_Load {

    static testMethod void bulkCampaignUnitTestBTLB() {
       //Create a BTLB account first.
       // myTestFactory = new Test_Factory();
       Account thisTestAccount = Test_factory.createAccount();
       insert thisTestAccount;
       
       Contact Con=new Contact (firstname='ABC',lastname='DEF');
       insert Con;
       String le_code = thisTestAccount.LE_Code__c;
       
       //Create a blob record from my string
       Blob myBlob = Blob.valueof(le_code); 
       
       //Create a campaign record
       Campaign btlbCampaign = new Campaign(name='BTLBCampaignTest001',isActive=true,X_Day_Rule__c=1,campaign_type__c='Marketing', Data_Source__c='Self Generated');
       insert btlbCampaign;
       
       Campaign btlbchildCampaign=new Campaign(name='BTLBCampaignTest002',isActive=true,X_Day_Rule__c=1,campaign_type__c='Marketing', Data_Source__c='Self Generated',parentId=btlbCampaign.Id);
       insert btlbchildCampaign;
       
       campaignmember CM=new campaignmember(Campaignid=btlbchildCampaign.id,Contactid=Con.id);
       insert CM;  
       //Create a VF Page to use in the test
      
       PageReference p = Page.BulkCampaignCreate;
        
       // Set the GEO ID as the context for use by the controller. */
       p.getParameters().put('id',btlbCampaign.id);
       Test.startTest();
        
       Test.setCurrentPage(p); 

        //Create a new Resource plan controller
        
       CreateBulkCampaigns bulkCampController = new CreateBulkCampaigns();
        
       bulkCampController.setFileBody(myBlob);
       Pagereference myPage = bulkCampController.docampaigncreate();
        
       //Test batch apex
        
        Bulk_Campaign_Run__c newcampaignrun;
        newcampaignrun = bulkCampController.bulkcamprun;
        
        BatchCampaignCreate docampaigns = new BatchCampaignCreate();
    
        system.debug('GSSELECT Id, legalentityid__c,sac_code__c FROM bulkcampaignstaging__c where related_bulk_campaign_batch__c = \'' + newcampaignrun.Bulk_Campaign_Batch__c + '\'');
    
        docampaigns.query='SELECT Id, le_or_sac_code__c,ein__c,lead_locator_role__c,campaign_member_notes__c,Corp_System_Url__c,Corp_System_Text__c,Corp_Subject__c,Corp_Reminder_Days__c,Corp_Primary_Campaign_Source__c,Corp_Due_Date__c,Corp_Details__c,Corp_Collateral_Url__c,Corp_Collateral_Text__c FROM bulkcampaignstaging__c where related_bulk_campaign_batch__c = \'' + newcampaignrun.Bulk_Campaign_Batch__c + '\'';
    
        docampaigns.runrecord = newcampaignrun;
        ID batchprocessid = Database.executeBatch(docampaigns,100);  
        Test.stopTest();
        
        //ASSERT SECTION
        //Now query the campaign member records for this campaign.
        
        //Query the BTLB child campaign
        Campaign childCampaign = [select id from campaign where parentid = :btlbCampaign.id limit 1];
        
        List<campaignmember> campMemList = new list<campaignmember>([select campaignid,contactid from campaignmember where campaignid=:childCampaign.id limit 1]);
        Contact testContact = [Select firstname,lastname from contact where id = :campMemlist[0].contactid];
        
        system.assertequals(campmemlist.size(),1);
        System.assertnotequals(btlbCampaign.isactive,false);
        System.assertequals(campMemlist[0].campaignid,childcampaign.id);
        System.assertnotequals(testcontact,null);
        System.assertnotequals(testcontact.firstname,null);
        System.assertnotequals(testcontact.lastname,null);
        System.assertequals(campMemlist[0].contactid,testcontact.id);
        
    }
    
     static testMethod void bulkCampaignUnitTestCorporate() {
      //Create a BTLB account first.
       Account thisTestAccount = Test_factory.createAccount();
       thisTestAccount.ownerid = UserInfo.getUserId();
       insert thisTestAccount; 
       String SAC_code = thisTestAccount.SAC_Code__c;
       
       //Create a blob record from my string
       Blob myBlob = Blob.valueof(SAC_code); 
       
       //Create a campaign record
       Campaign corpCampaign = new Campaign(name='CorpCampaignTest001',isActive=true,X_Day_Rule__c=1,campaign_type__c='Corporate', Data_Source__c='Self Generated');
       insert corpCampaign;
         
       //Create a VF Page to use in the test
      
       PageReference p = Page.BulkCampaignCreate;
        
       // Set the GEO ID as the context for use by the controller. */
       p.getParameters().put('id',corpCampaign.id);
       Test.startTest();
        
       Test.setCurrentPage(p); 

        //Create a new Resource plan controller
        
       CreateBulkCampaigns bulkCampController = new CreateBulkCampaigns();
       
       //Run test of no file being selected.
       Pagereference myPage = bulkCampController.docampaigncreate();
       
       //Now set the file for a positive test
       bulkCampController.setFileBody(myBlob);
       
       //Now set the filesize to high to test the validation
       bulkCampController.setFilesize(500000);
       myPage = bulkCampController.docampaigncreate();
     
       //Now run positive test
       bulkCampController.setFilesize(0);
       myPage = bulkCampController.docampaigncreate();
       
           
       //Test batch apex
        
        Bulk_Campaign_Run__c newcampaignrun;
        newcampaignrun = bulkCampController.bulkcamprun;
        
        BatchCampaignCreate docampaigns = new BatchCampaignCreate();
    
        system.debug('GSSELECT Id, legalentityid__c,sac_code__c FROM bulkcampaignstaging__c where related_bulk_campaign_batch__c = \'' + newcampaignrun.Bulk_Campaign_Batch__c + '\'');
    
        docampaigns.query='SELECT Id, le_or_sac_code__c,ein__c,lead_locator_role__c,campaign_member_notes__c,Corp_System_Url__c,Corp_System_Text__c,Corp_Subject__c,Corp_Reminder_Days__c,Corp_Primary_Campaign_Source__c,Corp_Due_Date__c,Corp_Details__c,Corp_Collateral_Url__c,Corp_Collateral_Text__c FROM bulkcampaignstaging__c where related_bulk_campaign_batch__c = \'' + newcampaignrun.Bulk_Campaign_Batch__c + '\'';
    
        docampaigns.runrecord = newcampaignrun;
        ID batchprocessid = Database.executeBatch(docampaigns,100);  
        system.assertNotEquals(batchprocessid,null);
        Test.stopTest();
        
        //ASSERT SECTION 
        
        //Now query the campaign member records for this campaign.
        List<campaignmember> campMemList = new list<campaignmember>([select contactid from campaignmember where campaignid=:corpCampaign.id]);
        system.assertequals(campmemlist.size(),1);
        System.assertnotequals(corpCampaign.isactive,false);
        
        Contact testContact = [Select firstname,lastname from contact where id = :campMemlist[0].contactid];
        System.assertnotequals(testcontact,null);
        
        Task thisTask = [Select subject,ownerid,whoid,whatid from Task where whatid = :corpCampaign.id];
        System.assertnotequals(thisTask,null);
//      System.assertequals(thisTask.ownerid,thisTestAccount.OwnerId); // auto account assignment means i had to comment this line out ADJ 30 Sep10
        System.assertequals(thisTask.whatid,corpCampaign.id);
        System.assertequals(thisTask.whoid,campMemlist[0].contactid);
        System.assertnotequals(testcontact.firstname,null);
        System.assertnotequals(testcontact.lastname,null);
        
        
        }
}