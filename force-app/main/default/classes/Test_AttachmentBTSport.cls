@isTest
private class Test_AttachmentBTSport {


    testmethod static void Test_FlagIsSetExist(){
        Test_Factory.setProperty('IsTest', 'yes');
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }

        StaticVariables.setContactIsRunBefore(False);             
        String BTSOnePhoneRecType = '01220000000AKzm';
        Date uDate = date.today().addDays(7);
        
        Opportunity newOppty = new Opportunity();
        newOppty.RecordTypeId = BTSOnePhoneRecType;
        newOppty.Name = 'BTSOnePhone';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});
        
        Contact con = Test_Factory.CreateContact();
        con.Cug__c = 'cugAlan1';
        con.Contact_Post_Code__c = 'WR5 3RL';
        insert con;
        
        BT_One_Phone__c BTOP = new BT_One_Phone__c();
        BTOP.Opportunity__c = oppResult[0].id;
        BTOP.Existing_Customer_Resign__c  = 'Y';
        Database.SaveResult[] BTOPResult = Database.insert(new BT_One_Phone__c [] {BTOP});  
        
        BT_One_Phone_Site__c bops = new BT_One_Phone_Site__c();
        bops.BT_One_Phone__c = BTOP.id;
        bops.Site_Telephone_Number_Landline__c = '01234 567890';
        bops.Site_Diagram_or_Sketch_Attached__c = true;
        bops.Number_of_Users__c = 10;
        bops.Number_of_users__c = 1;
        bops.Number_Concurrent_Calls_Expected_at_Site__c = 10;
        bops.Number_of_Users_in_Parallel_Hunt_Group__c = '5 or Less';
        bops.mobilecoverageResults__c = 'Good Outdoor Only';
        bops.Post_Code__c = 'w13 8qd';
		bops.Customer_Contact__c = con.id;
        bops.Contract_Duration__c = '24 Months';
        bops.Onsite_mobile_network__c=true;
        insert bops;
        
        
        Blob b = Blob.valueOf('Test Data');
        Attachment BTSAttachment = new Attachment();
        BTSAttachment.body = b;
        BTSAttachment.name = 'Test';
        BTSAttachment.ParentId = BTOPResult[0].Id;
        Database.SaveResult[] BTSAttachmentResult = Database.insert(new Attachment[] {BTSAttachment});  
        

                
          }
}