public class BSClassicSLProductEmail {

    public String relatedTo { get; set; }
    public  List<BS_Classic_SL_Product__c> listBSClassicProduct;
    public  List<BS_Classic_SL_Product__c> listBSClassicProductTable;
    public  List<BSCLassicEmail__c> listBSCLassicEmailStatements;
    public  List<BS_Classic_SL_Product__c> listBSClassicEmailfooter;
    public  List<BS_Classic_SL_Product__c> listBSClassicProductfooter;
    public  List<BS_Classic_SL_Product__c> listBSClassicProduct2;
    public  List<BSCLassicEmail__c>  EmailCodes;
 
    public List<BSCLassicEmail__c>  bSCLassicEmail{get;set;}
    public List<BSCLassicEmail__c>  bSCLassicEmailfooter{get;set;}
    public List<BSCLassicEmail__c> BSClassicSLProductEmail{get;set; }
    public string SLProductid{get;set;}
    
    public List<String> friendlyNames = new List<String>();
    public Integer prodNum = 0;
    public String temp ;
    public Map<String,String> emailCodeText = new Map<String,String>();
       
    
//Email Product Table    
    public List<BS_Classic_SL_Product__c> getBSClassicSLProductTable() {
    listBSClassicProductTable=[select Omit_From_Recap_Email_Table__c, id,email_statements__c,Name,Product__c,Product_Type2__c,Quantity__c, period__c, Friendly_name__c from BS_Classic_SL_Product__c where Sales_Log__c =: SLProductid  and email_statements__c <> '' and include_in_Recap_Email__c > 0 ORDER BY Product__c];
    
    if (listBSClassicProductTable.size() >0){ 
    return listBSClassicProductTable; 
    }
     else {
         return null;
     }
    }
     
// Email Statements
     

public String[]  getBSCLassicEmailStatements() {   

    Set<String> bscEmailUnique=new Set<String>();// change to list
    List <String> bscEmail=new List<String>();
    List <String> EmailText = new List<String>();
    Map<String, String> EmailCode = new Map<String, String>();
    
    listBSClassicProduct = [select Omit_From_Recap_Email_Table__c, id,email_statements__c,Name,Product__c,Product_Type2__c,Quantity__c, period__c, Friendly_name__c from BS_Classic_SL_Product__c where Sales_Log__c =: SLProductid  and email_statements__c <> '' ORDER BY Product__c];
    prodNum = 0;
    
    for(BS_Classic_SL_Product__c bsc :listBSClassicProduct){
        
        if(bsc.email_statements__c.contains('0002')){
            if(bsc.Friendly_name__c != null){
                friendlyNames.add(bsc.Friendly_name__c);
            }else{
                friendlyNames.add(bsc.Product__c);
            } 
        }
        do {
            
            if (bsc.email_statements__c.length() >= 4){
                if(bsc.email_statements__c.substring(0,4) == '0002'){
                    bscEmail.add('0002$'+prodNum); 
                    prodNum += 1;
                }
                else{
                    bscEmail.add(bsc.email_statements__c.substring(0,4)); 
                }
                bscEmailUnique.add(bsc.email_statements__c.substring(0,4));
            }
            
            if (bsc.email_statements__c.length() > 4){
                bsc.email_statements__c = bsc.email_statements__c.substring(4,bsc.email_statements__c.length());
            }
            else{
                bsc.email_statements__c= '';
            }
        }
        while  (bsc.email_statements__c.length() >= 4 );

    }

    system.debug('ADJ bscEmail list size '+ bscEmail.size());
    system.debug('@@@@@'+ friendlyNames);



    if (bscEmail.size() >0){ 
        
        system.debug('TEST12345'+bscEmail);
    // create an array of Email code to text lookup

        EmailCodes=[SELECT  email_code__c, Email_Text__c FROM BSCLassicEmail__c WHERE type__c ='email' and email_code__c in :bscEmailUnique ];
        if(EmailCodes.size() > 0){
            for (BSCLassicEmail__c ec :EmailCodes){
                emailCodeText.put(ec.email_code__c,ec.Email_Text__c);
            }
                    
        }
        
    // loop trough initial list & creat new list substituting email codes with email text

    system.debug('@@@@'+emailCodeText);
    prodNum = 0;
    for (String s : bscEmail) {
        if(s.contains('0002$')){
            EmailText.add(emailCodeText.get('0002')+' "'+ friendlyNames[prodNum]+'"');
            prodNum = prodNum + 1;
        }else{
            EmailText.add(emailCodeText.get(s));
        }
    }


    }

    return EmailText;

}




    
// Email footer    
    public List<BSCLassicEmail__c> getEmailFooter() {
        set <String> footer = new Set<String>();
         List<BS_Classic_SL_Product__c> footerCodes = [Select email_statements__c from BS_Classic_SL_Product__c where Sales_Log__c = :SLProductid and email_statements__c <> '' ];
             for (BS_Classic_SL_Product__c ft :footerCodes){
                         do {
                            if (ft.email_statements__c.length() >= 4){
                                footer.add(ft.email_statements__c.substring(0,4)); 
                            }
                                if (ft.email_statements__c.length() > 4){
                                ft.email_statements__c = ft.email_statements__c.substring(4,ft.email_statements__c.length());
                            }
                            else{
                                ft.email_statements__c = '';
                            }
                            }
                            while  (ft.email_statements__c.length() >= 4 );
                   }
            
                 if (footer.size() >0){  
                        return [SELECT  Email_Text__c,Text_Format__c FROM BSCLassicEmail__c WHERE type__c ='footer' and email_code__c in :footer ORDER BY email_code__c ];
                        }
                 else {
                    return null;
                 }
}
}