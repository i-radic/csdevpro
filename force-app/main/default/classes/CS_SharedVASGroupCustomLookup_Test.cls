@isTest
private class CS_SharedVASGroupCustomLookup_Test {

    @isTest
    private static void getRequiredAttributes_Test() {
        String result;
        Test.startTest();
            CS_SharedVASGroupCustomLookup sharedVASLookup = new CS_SharedVASGroupCustomLookup();
            result = sharedVASLookup.getRequiredAttributes();
        Test.stopTest();
        System.assertNotEquals('', result);
    }
    
     @isTest
    private static void doLookupSearch_Test() {
        Object[] result;
        cspmb__Price_Item__c servicePlan = CS_TestDataFactory.generatePriceItem(TRUE, 'Test Service Plan');
        cspmb__Add_On_Price_Item__c sharedVASAddOn = CS_TestDataFactory.generateAddOnPriceItem(FALSE, 'Test VAS');
        sharedVASAddOn.cspmb__Is_Active__c = TRUE;
        sharedVASAddOn.Minimum_Connections__c = 250;
        sharedVASAddOn.Maximum_Connections__c = 500;
        INSERT sharedVASAddOn;
        cspmb__Price_Item_Add_On_Price_Item_Association__c priceItemAddOnAssoc = CS_TestDataFactory.generatePriceItem(FALSE, sharedVASAddOn, servicePlan);
        priceItemAddOnAssoc.Module__c = 'Shared VAS';
        priceItemAddOnAssoc.cspmb__Group__c = 'Total Resource';
        priceItemAddOnAssoc.Add_On_Type__c = 'Annual';
        priceItemAddOnAssoc.cspmb__Group__c = 'Total Resource';
        INSERT priceItemAddOnAssoc;
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('Service Plan', servicePlan.Id);
        Test.startTest();
            CS_SharedVASGroupCustomLookup sharedVASLookup = new CS_SharedVASGroupCustomLookup();
            result = sharedVASLookup.doLookupSearch(searchFields, '', NULL, 10, 10);
        Test.stopTest();
        System.assertNotEquals(0, result.size());
    }
}