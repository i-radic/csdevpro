@isTest
private class TestCoverageCheckApprovalProcess{
    static testMethod void testCoverageCheckApproval(){
        Coverage_Check_Site__c ccs = getCoverageCheckSite();
        Coverage_Check_Site__c testCCS = [Select Location_Name2__c,Post_Code__c,CER_Reason__c,Site_Contact_Name__c,Telephone_Number__c,
                                     Floorplan_Attached__c,Building_Type__c,No_of_Buildings_Requiring_Enhancements__c,Which_floors_require_enhancement__c from Coverage_Check_Site__c where Id = :ccs.id ];
       String result = CoverageCheckApprovalProcess.submitAndProcessSalesRequest(testCCS.id);
       System.debug('Test result1===='+ result);
       System.assert(result.contains('Special Projects submission error'),result);
        Coverage_Check_Site__c ccs1 = getValidCoverageCheckSite();
        Coverage_Check_Site__c testCCS1 = [Select Location_Name2__c,Post_Code__c,CER_Reason__c,Site_Contact_Name__c,Telephone_Number__c,
                                     Floorplan_Attached__c,Which_floors_require_enhancement__c,Building_Type__c,No_of_Buildings_Requiring_Enhancements__c from Coverage_Check_Site__c where Id = :ccs1.id ];
       String result1 = CoverageCheckApprovalProcess.submitAndProcessSalesRequest(testCCS1.id);
       System.debug('Test result1===='+ result1);
       System.assert(result1.contains('Successfully submitted to Special projects.'),result1);
    }
    public static Coverage_Check_Site__c getCoverageCheckSite() 
    {
       TriggerDeactivateTest();
       Opportunity opp = new Opportunity();
       opp.Name ='TestOpp';
       opp.StageName='Changes Over Time';
       opp.CloseDate = Date.Today();
       insert opp;
       Coverage_Check__c cc = new Coverage_Check__c ();
       cc.Opportunity__c = opp.id;
       insert cc;
       Coverage_Check_Site__c  ccs = new Coverage_Check_Site__c ();
       ccs.Location_Name2__c = '';
       ccs.Post_Code__c = '';
       ccs.CER_Reason__c ='';
       ccs.Site_Contact_Name__c ='';  
       
       ccs.Telephone_Number__c ='';
       ccs.Floorplan_Attached__c = false;
       ccs.No_of_Buildings_Requiring_Enhancements__c = null;
       ccs.Which_floors_require_enhancement__c =null;
       ccs.Coverage_Check__c = cc.id;
       ccs.I_understand2__c = TRUE;
       insert ccs;      
       return ccs;
    }
    public static Coverage_Check_Site__c getValidCoverageCheckSite() 
    {
       TriggerDeactivateTest();
        Opportunity opp = new Opportunity();
       opp.Name ='TestOpp1';
       opp.StageName='Changes Over Time';
       opp.CloseDate = Date.Today();
       insert opp;
       Coverage_Check__c cc = new Coverage_Check__c ();
        cc.Opportunity__c = opp.id;
       insert cc;
       Coverage_Check_Site__c  ccs = new Coverage_Check_Site__c ();
       ccs.Location_Name2__c = 'Location';
       ccs.Post_Code__c = 'AA11';
       ccs.CER_Reason__c ='No coverage confirmed on site'; //Coverage
       ccs.Site_Contact_Name__c ='John Smith';
       ccs.Telephone_Number__c ='123456';
       ccs.Floorplan_Attached__c = true;
       ccs.No_of_Buildings_Requiring_Enhancements__c = 5;
       ccs.No_of_floors_in_this_building__c = 6;
       ccs.Which_floors_require_enhancement__c ='2 and 3rd';
       ccs.Coverage_Check__c = cc.id;
       ccs.I_understand2__c = TRUE;
       insert ccs;      
       return ccs;
    }
    static void TriggerDeactivateTest(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    }


}