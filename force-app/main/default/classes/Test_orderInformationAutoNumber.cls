@isTest

    private class Test_orderInformationAutoNumber{
    
    static testMethod void myUnitTest(){
    
    Opportunity o = new Opportunity();
    o.Name='TEST';
    o.stageName = 'FOS Stage';
    o.CloseDate = system.today();
    insert o;
    
    CRF__c crf = new CRF__c();    
    crf.Opportunity__c = o.Id;
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='NSO_BS'].Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;
    
    Order_Information__c LM = new Order_Information__c();
    LM.Related_To_CRF__c = crf.Id;
    LM.Name = '1';
    insert LM;
    
    Order_Information__c LM1 = new Order_Information__c();
    LM1.Related_To_CRF__c = crf.Id;
    LM1.Name = '1';
    insert LM1;
    
    Delete LM1;
    
    LM.Name='NewName';
    update LM;
    
    }
}