public with sharing class ContactActivityController {
    public ContactActivityController(ApexPages.StandardController controller) {
   
    }
public string conID = System.currentPageReference().getParameters().get('ID');
//public string AccID = '0012000000Tem2f'; 


List<Contact> ConAccID = [SELECT AccountId FROM Contact WHERE ID = :conID LIMIT 1 ];
Id accID = ConAccID[0].AccountId;

public List<Event> getAccEventHist() {    
    system.debug('Contact ID set to: ' + ConID ); 
    system.debug('Account ID set to: ' + accID);  
      return [Select e.WhoId, e.WhatId, e.Subject, e.Related_Opportunity_ID__c, e.Related_Campaign_Opportunity__c, e.LastModifiedDate, 
              e.Event_Status__c, e.EndDateTime, e.CreatedDate, e.OwnerId, e.AccountId
              FROM Event e 
              WHERE AccountID = :accID AND e.Event_Status__c <> 'Open'
              ORDER BY e.CreatedDate DESC];
  }

public List<Task> getAccTaskHist() {    
      return [Select t.WhoId, t.WhatId, t.Subject, t.LastModifiedDate, t.CreatedDate, t.Assigned_User_Id__c, t.ActivityDate, t.AccountId, t.OwnerId,
              t.Status
              FROM Task t 
              WHERE AccountID = :AccID  
              ORDER BY t.CreatedDate DESC];
  }

   
}