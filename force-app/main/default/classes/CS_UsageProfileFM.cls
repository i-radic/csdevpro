global class CS_UsageProfileFM implements cssmgnt.RemoteActionDataProvider{
    
    global Map<String, Object> getData(Map<String, Object> inputMap){
        Map<String, Object> returnMap = new Map<String, Object>();
        String accountId = String.valueOf(inputMap.get('Id'));
        try{
            List<Usage_Profile__c> usageProfileList = [select Id,Name,Account__r.Id,FM_Calls_to_EE__c,FM_Calls_to_other_UK_mobile_networks__c,
                  FM_IDD_SMS_Zone_A__c,FM_IDD_SMS_Zone_B__c,FM_IDD_SMS_Zone_C__c,FM_IDD_Zone_A__c,FM_IDD_Zone_B__c,
                  FM_IDD_Zone_C__c,FM_MMS__c,FM_MMS_Messaging__c,FM_Non_Geographic_Calls__c,FM_Receiving_call_in_Zone_A__c,
                  FM_Receiving_call_in_Zone_B__c,FM_Receiving_call_in_Zone_C__c,FM_Roaming_SMS_Zone_A__c,FM_Roaming_SMS_Zone_B__c,
                  FM_Roaming_SMS_Zone_C__c,FM_SMS_to_EE__c,FM_SMS_to_other_network__c,FM_UK_Landlines__c,FM_Voice_calls_back_to_UK_from_Zone_A__c,
                  FM_Voice_calls_back_to_UK_from_Zone_B__c,FM_Voice_calls_back_to_UK_from_Zone_C__c,FM_Voice_calls_to_Zone_A__c,
                  FM_Voice_calls_to_Zone_B__c,FM_Voice_calls_to_Zone_C__c,
                  Expected_Voice__c,Expected_Voice_per_User__c,Expected_SMS__c,Expected_SMS_per_User__c,
                  Expected_Data__c,Expected_Data_per_User__c,Early_Termination_Charge__c,
                  Total_IDD_and_ROW_Minutes__c,ROW_Roaming_Data_MB__c
                  from Usage_Profile__c where Account__r.Id = :AccountId and Active__c = true];
            if(!usageProfileList.isEmpty()){
                returnMap.put('DataFound', true);
                returnMap.put('UsageProfileId',usageProfileList[0].Id);
                returnMap.put('UsageProfileName',usageProfileList[0].Name);
                returnMap.put('FMCallstoEE',usageProfileList[0].FM_Calls_to_EE__c);
                returnMap.put('FMCallstootherUKmobilenetworks',usageProfileList[0].FM_Calls_to_other_UK_mobile_networks__c);
                returnMap.put('FMIDDSMSZoneA',usageProfileList[0].FM_IDD_SMS_Zone_A__c);
                returnMap.put('FMIDDSMSZoneB',usageProfileList[0].FM_IDD_SMS_Zone_B__c);
                returnMap.put('FMIDDSMSZoneC',usageProfileList[0].FM_IDD_SMS_Zone_C__c);
                returnMap.put('FMIDDZoneA',usageProfileList[0].FM_IDD_Zone_A__c);
                returnMap.put('FMIDDZoneB',usageProfileList[0].FM_IDD_Zone_B__c);
                returnMap.put('FMIDDZoneC',usageProfileList[0].FM_IDD_Zone_C__c);
                returnMap.put('FMMMS',usageProfileList[0].FM_MMS__c);
                returnMap.put('FMMMSMessaging',usageProfileList[0].FM_MMS_Messaging__c);
                returnMap.put('FMNonGeographicCalls',usageProfileList[0].FM_Non_Geographic_Calls__c);
                returnMap.put('FMReceivingcallinZoneA',usageProfileList[0].FM_Receiving_call_in_Zone_A__c);
                returnMap.put('FMReceivingcallinZoneB',usageProfileList[0].FM_Receiving_call_in_Zone_B__c);
                returnMap.put('FMReceivingcallinZoneC',usageProfileList[0].FM_Receiving_call_in_Zone_C__c);
                returnMap.put('FMRoamingSMSZoneA',usageProfileList[0].FM_Roaming_SMS_Zone_A__c);
                returnMap.put('FMRoamingSMSZoneB',usageProfileList[0].FM_Roaming_SMS_Zone_B__c);
                returnMap.put('FMRoamingSMSZoneC',usageProfileList[0].FM_Roaming_SMS_Zone_C__c);
                returnMap.put('FMSMStoEE',usageProfileList[0].FM_SMS_to_EE__c);
                returnMap.put('FMSMStoothernetwork',usageProfileList[0].FM_SMS_to_other_network__c);
                returnMap.put('FMUKLandlines',usageProfileList[0].FM_UK_Landlines__c);
                returnMap.put('FMVoicecallsbacktoUKfromZoneA',usageProfileList[0].FM_Voice_calls_back_to_UK_from_Zone_A__c);
                returnMap.put('FMVoicecallsbacktoUKfromZoneB',usageProfileList[0].FM_Voice_calls_back_to_UK_from_Zone_B__c);
                returnMap.put('FMVoicecallsbacktoUKfromZoneC',usageProfileList[0].FM_Voice_calls_back_to_UK_from_Zone_C__c);
                returnMap.put('FMVoicecallstoZoneA',usageProfileList[0].FM_Voice_calls_to_Zone_A__c);
                returnMap.put('FMVoicecallstoZoneB',usageProfileList[0].FM_Voice_calls_to_Zone_B__c);
                returnMap.put('FMVoicecallstoZoneC',usageProfileList[0].FM_Voice_calls_to_Zone_C__c);
                returnMap.put('ExpectedVoice',usageProfileList[0].Expected_Voice__c);
                returnMap.put('ExpectedVoiceperUser',usageProfileList[0].Expected_Voice_per_User__c);
                returnMap.put('ExpectedSMS',usageProfileList[0].Expected_SMS__c);
                returnMap.put('ExpectedSMSperUser',usageProfileList[0].Expected_SMS_per_User__c);
                returnMap.put('ExpectedData',usageProfileList[0].Expected_Data__c);
                returnMap.put('ExpectedDataperUser',usageProfileList[0].Expected_Data_per_User__c);
                returnMap.put('ETC', usageProfileList[0].Early_Termination_Charge__c);
                returnMap.put('ROW Voice Usage', usageProfileList[0].Total_IDD_and_ROW_Minutes__c);
                returnMap.put('ROW Data Usage', usageProfileList[0].ROW_Roaming_Data_MB__c);
            }
            else{
                returnMap.put('DataFound', false);
            }
        }catch(Exception e){}
        system.debug('returnMap ====='+returnMap);
        return returnMap;
    }

}