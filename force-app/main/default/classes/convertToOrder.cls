public with sharing class convertToOrder{

    public Opportunity oppty {get; set;}
    public string opptyId = ApexPages.currentPage().getParameters().get('id');
       
    public convertToOrder(ApexPages.StandardController stdController) {

        oppty = (Opportunity)stdController.getRecord();
    }
     
    public convertToOrder(Opportunity oppty_edit) {
        oppty = oppty_edit;
    }
    
    public PageReference OnLoad() {          
        try {
            oppty = [SELECT id, Opportunity_Id__c, Account_CUG__c FROM Opportunity WHERE id=:opptyId LIMIT 1];       
            PageReference PageRef = new PageReference('/' + oppty.id + '?oid=' + oppty.Opportunity_Id__c + '&cug=' + oppty.Account_CUG__c);
            PageRef.setRedirect(true);    
            return PageRef;
        }
        catch (Exception ex){
            System.Debug('####################' + ex);
            PageReference PageRef = new PageReference('/' + opptyId);
            PageRef.setRedirect(true);    
            return PageRef;
        }       
    }
}