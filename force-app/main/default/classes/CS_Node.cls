public class CS_Node
{
    private List<CS_Node> children;
    private CS_Node parent;
    private cscfga__Product_Configuration__c config;

    public CS_Node(cscfga__Product_Configuration__c config)
    {
        this.config = config;
        parent = null;
        children = new List<CS_Node>();
    }

    public void connectWithParent(CS_Node parent)
    {
        if (parent != null)
        {
            this.parent = parent;
            parent.children.add(this);
        }
    }

    public List<CS_Node> getChildren()
    {
        return children;
    }

    public CS_Node getParent()
    {
        return parent;
    }

    public cscfga__Product_Configuration__c getConfig()
    {
        return config;
    }

    public CS_Node duplicate()
    {
        return new CS_Node(config);
    }

    public abstract class Filter
    {
        public virtual Boolean filter(CS_Node node)
        {
            return true;
        }

        public Filter andOp(Filter filter)
        {
            return new FilterAnd(this, filter);
        }

        public Filter orOp(Filter filter)
        {
            return new FilterOr(this, filter);
        }

        public Filter notOp()
        {
            return new FilterNot(this);
        }
    }

    public class FilterAnd extends Filter
    {
        Filter first;
        Filter second;

        public FilterAnd(Filter first, Filter second)
        {
            this.first = first;
            this.second = second;
        }

        public override Boolean filter(CS_Node node)
        {
            return first.filter(node) && second.filter(node);
        }
    }

    public class FilterOr extends Filter
    {
        Filter first;
        Filter second;

        public FilterOr(Filter first, Filter second)
        {
            this.first = first;
            this.second = second;
        }

        public override Boolean filter(CS_Node node)
        {
            return first.filter(node) || second.filter(node);
        }
    }

    public class FilterNot extends Filter
    {
        Filter filter;

        public FilterNot(Filter filter)
        {
            this.filter = filter;
        }

        public override Boolean filter(CS_Node node)
        {
            return !filter.filter(node);
        }
    }

    public class FilterConfigId extends Filter
    {
        Id configId;

        public FilterConfigId(Id configId)
        {
            this.configId = configId;
        }

        public override Boolean filter(CS_Node node)
        {
            return (node.getConfig().Id == configId);
        }
    }

    public class FilterDefinitionNames extends Filter
    {
        Set<String> names;

        public FilterDefinitionNames(Set<String> names)
        {
            this.names = new Set<String>(names);
        }

        public override Boolean filter(CS_Node node)
        {
            return
                names.contains
                    (node.getConfig().cscfga__Product_Definition__r.Name);
        }
    }

    public class FilterParents extends Filter
    {
        public override Boolean filter(CS_Node node)
        {
            return (node.getParent() == null);
        }
    }

    public static List<CS_Node> filterList(List<CS_Node> nodes, Filter filter)
    {
        List<CS_Node> returnValue = new List<CS_Node>();
        for (CS_Node node : nodes)
        {
            if (filter.filter(node))
            {
                returnValue.add(node);
            }
        }
        return returnValue;
    }

    public static List<CS_Node> filterTree(List<CS_Node> nodes, Filter filter)
    {
        List<CS_Node> returnValue = new List<CS_Node>();
        for (CS_Node node : nodes)
        {
            if (filter.filter(node))
            {
                CS_Node parent = node.duplicate();
                returnValue.add(parent);
                List<CS_Node> children = filterTree(node.getChildren(), filter);

                for (CS_Node child : children)
                {
                    child.connectWithParent(parent);
                }
            }
        }
        return returnValue;
    }

    public interface FoldOp
    {
        Object fold(Object value, CS_Node node);
    }

    public class FoldSumExpression implements FoldOp
    {
        CS_Expression.Eval expressionTree;

        public FoldSumExpression(String expression)
        {
            this.expressionTree = CS_Expression.parseExpression(expression);
        }

        public Object fold(Object value, CS_Node node)
        {
            return (Decimal)value
                + CS_Util.decimalValue(expressionTree.eval(node.getConfig()));
        }
    }

    public class FoldAttributes implements FoldOp
    {
        Map<String, Set<String>> definitionAttributes;
        Map<String, Map<String, String>> attributesMap;

        public FoldAttributes
            ( Map<String, Set<String>> definitionAttributes
            , Map<String, Map<String, String>> attributesMap )
        {
            this.definitionAttributes = definitionAttributes;
            this.attributesMap = attributesMap;
        }

        public Object fold(Object value, CS_Node node)
        {
            Map<String, Map<String, Map<String, String>>> returnValue =
                (Map<String, Map<String, Map<String, String>>>)value;
            String definitionName =
                node.getConfig().cscfga__Product_Definition__r.Name;
            String configId = node.getConfig().Id;
            Set<String> attributeNames =
                this.definitionAttributes.get(definitionName);
            Map<String, String> attributes =
                this.attributesMap.get(configId) != null
                    ? this.attributesMap.get(configId)
                    : new Map<String, String>();
            Map<String, Map<String, String>> configurationMap =
                returnValue.get(definitionName) != null
                    ? returnValue.get(definitionName)
                    : new Map<String, Map<String, String>>();
            //
            // If we have attributes for this definition
            //
            if (attributeNames != null)
            {
                Map<String, String> returnAttributes =
                    new Map<String, String>();
                for (String attributeName : attributeNames)
                {
                    returnAttributes.put
                        ( attributeName
                        , attributes.get(attributeName) );
                }
                configurationMap.put(configId, returnAttributes);
                returnValue.put(definitionName, configurationMap);
            }
            return returnValue;
        }
    }

    public static Object foldTree(Object acc, List<CS_Node> nodes, FoldOp fold)
    {
        Object returnValue = acc;
        for (CS_Node node : nodes)
        {
            returnValue =
                fold.fold
                   ( foldTree(returnValue, node.getChildren(), fold)
                   , node );
        }
        return returnValue;
    }

    public static List<CS_Node> createTree(List<cscfga__Product_Configuration__c> configs)
    {
        Map<Id, CS_Node> nodeMap = new Map<Id, CS_Node>();
        for (cscfga__Product_Configuration__c config : configs)
        {
            nodeMap.put(config.Id, new CS_Node(config));
        }
        for (CS_Node node : nodeMap.values())
        {
            node.connectWithParent(nodeMap.get(node.getConfig().cscfga__Parent_Configuration__c));
        }

        return filterList(nodeMap.values(), new FilterParents());
    }
}