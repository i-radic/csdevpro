@isTest(SeeAllData=True)

//Can see all data as requires insertion of PRojects. Because we cannot insert Workspaces then we need to use this

private class ViewProjectStagesControllerTest {
    static testMethod void setProjectStagesTest() {
        User u = [Select id from user where firstname = 'Marc' and lastname = 'Francis'];
        
        System.runAs(u)
        {
            Opportunity opp = new Opportunity(name='unit test opp', 
                                          StageName = 'Prospecting', 
                                          CloseDate = Date.today(),
                                          NextStep = 'test',
                                          Next_Action_Date__c = Date.today(),
                                          Incumbent__c = 'Other',
                                          Customer_Status__c = 'New to Orange',
                                          Competitors__c = 'O2');
            insert opp;
            
            Project__c p = new Project__c();
            p.Project_Name__c = 'Unit Test Project';
            p.Opportunity__c = opp.Id;
            p.Notify_Project_Manager__c = FALSE;
            insert p;
            
            Phase__c stage1 = new Phase__c(project__c = p.Id, Name = 'Stage 1');
            Phase__c stage2 = new Phase__c(project__c = p.Id, Name = 'Stage 2');
            Phase__c stage3 = new Phase__c(project__c = p.Id, Name = 'Stage 3');
            Phase__c stage4 = new Phase__c(project__c = p.Id, Name = 'Stage 4');
            insert stage1;
            insert stage2;
            insert stage3;
            insert stage4;
            
            Apexpages.Standardcontroller stdControl = new Apexpages.Standardcontroller(p);
            ViewProjectStagesController vpc = new ViewProjectStagesController(stdControl);
            System.assert(vpc.projectHasStages);
            System.assert(vpc.getProjectStages().size() == 4);
            List<Phase__c> stages = vpc.getProjectStages();
            for (Phase__c stage : stages) {
                stage.Milestones__c = '1 DB Literature';
            }
            vpc.save();
            
            stages = vpc.getProjectStages();
            for (Phase__c stage : stages) {
                System.assert(stage.Milestones__c == '1 DB Literature');
            }
            for (Phase__c stage : stages) {
                stage.Milestones__c = 'Test Stage';
            }
            vpc.cancel();
            stages = vpc.getProjectStages();
            for (Phase__c stage : stages) {
                //System.assert(stage.Milestones__c == '1 DB Literature');
            }    
        }   
    }
}