@IsTest
public class CS_JSONService_ProductsTest extends CS_JSONService_Products {
	private static CS_JSONService_Products jsonServiceProducts;
	private static CS_JSON_Schema jsonSchema;
	private static CS_JSON_Schema.CS_JSON_Section jsonSection;
	private static CS_JSON_Schema.CS_JSON_Setting jsonSetting;
	private static CS_JSON_Schema.CS_JSON_Object jsonObject;
	private static Map<Id, cscfga__Product_Configuration__c> idConfigurationMap;

	private static testMethod void getValues() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		No_Triggers__c notriggers =  new No_Triggers__c();
        notriggers.Flag__c = true;
        INSERT notriggers;
		jsonServiceProducts = new CS_JSONService_Products();
		jsonSchema = (CS_JSON_Schema) JSON.deserialize(CS_JSONExportTestFactory.getJSON(), CS_JSON_Schema.class);
		jsonSection = jsonSchema.getSection('Basket Information');
		jsonSetting = jsonSchema.getSetting('companyProducts');
		jsonObject = jsonSetting.getDefinition('BT Mobile Sharer');

		List<Id> configIdList = new List<Id>();
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', acc);
		cscfga__Product_Basket__c pb = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true,'BT Mobile Sharer');
		cscfga__Product_Definition__c prodDefR = CS_TestDataFactory.generateProductDefinition(true,'Related');
		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(true, 'Test Config', pb);
		cscfga__Product_Configuration__c pcr = CS_TestDataFactory.generateProductConfiguration(true, 'Related Config', pb);
		pc.cscfga__Product_Definition__c = prodDef.Id;
		pcr.cscfga__Product_Definition__c = prodDefR.Id;
		pcr.cscfga__Parent_Configuration__c = pc.Id;
		update pc;
		update pcr;
		Map<Id, cscfga__Product_Configuration__c> mapPC = new Map<Id, cscfga__Product_Configuration__c>();
		mapPC.put(pc.Id, pc);

        Test.startTest();
        CS_JSONService_Products jsonProducts = new CS_JSONService_Products();
        jsonSetting.definitions = new List<CS_JSON_Schema.CS_JSON_Object>{jsonObject};
        jsonProducts.setting = jsonSetting;
        Object o = jsonProducts.getResult(pb.Id);
        Test.stopTest();

	}
}