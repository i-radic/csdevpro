public class CS_Calculation
{
    //--------------------------------------------------------------------------
    // Constants
    //--------------------------------------------------------------------------
  
    //public static final Decimal NPV_FACTOR = 1.1;
    //New calculation updated as per request from Finance
    public static final Decimal NPV_FACTOR = 1.104;
    public static final Decimal NO_NPV = 1.0;


    //--------------------------------------------------------------------------
    // Members
    //--------------------------------------------------------------------------
    List<Decimal> data;

    public List<Decimal> getData()
    {
       return new List<Decimal>(data);
    }
    

    
    //--------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------
    public CS_Calculation(List<Decimal> data)
    {
        this.data = data == null
            ? new List<Decimal>()
            : new List<Decimal>(data);
    }

    //--------------------------------------------------------------------------
    // Methods
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
    // Multiplication of two vectors.
    // All elements elements from two vectors are multiplied.
    // Size of the resulting vector is size of smaller vector.
    //--------------------------------------------------------------------------
    public CS_Calculation mul(CS_Calculation rightOperand)
    {
        Iterator<Decimal> leftIt = this.data.iterator();
        Iterator<Decimal> rightIt = rightOperand.data.iterator();

        CS_Calculation returnValue = new CS_Calculation(null);
        List<Decimal> data = new List<Decimal>();

        while (leftIt.hasNext() && rightIt.hasNext())
        {
            data.add(leftIt.next() * rightIt.next());
        }

        returnValue.data = data;
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Addition of two vectors.
    // All elements from two vctors are added.
    // Size of the resulting vector is size of bigger vector.
    //--------------------------------------------------------------------------
    public CS_Calculation add(CS_Calculation rightOperand)
    {
        Iterator<Decimal> leftIt = this.data.iterator();
        Iterator<Decimal> rightIt = rightOperand.data.iterator();

        CS_Calculation returnValue = new CS_Calculation(null);
        List<Decimal> data = new List<Decimal>();

        while (leftIt.hasNext() && rightIt.hasNext())
        {
            data.add(leftIt.next() + rightIt.next());
        }

        while (leftIt.hasNext())
        {
            data.add(leftIt.next());
        }

        while (rightIt.hasNext())
        {
            data.add(rightIt.next());
        }

        returnValue.data = data;
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Cumsum of vector
    //--------------------------------------------------------------------------
    public CS_Calculation cumsum()
    {
        CS_Calculation returnValue = new CS_Calculation(null);
        List<Decimal> data = new List<Decimal>();

        Decimal sum = 0;

        for (Decimal value : this.data)
        {
            sum += value;
            data.add(sum);
        }

        returnValue.data = data;
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Set scale of decimals in the vector
    //--------------------------------------------------------------------------
    public CS_Calculation setScale(Integer scale)
    {
        CS_Calculation returnValue = new CS_Calculation(null);
        List<Decimal> data = new List<Decimal>();

        for (Decimal value : this.data)
        {
            data.add(value.setScale(scale));
        }

        returnValue.data = data;
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Size of a vector
    //--------------------------------------------------------------------------
    public Integer size()
    {
        return data.size();
    }

    //--------------------------------------------------------------------------
    // Sum every year and return it in list
    //--------------------------------------------------------------------------
    public List<Decimal> sumOnYears()
    {
        List<Decimal> returnValue = new List<Decimal>();

        //
        // For first year we count 13 for one off charge on 0 month
        //
        Iterator<Decimal> dataIt = data.iterator();
        Integer counter = 13;
        Integer i       = 0;
        Decimal sum     = 0;
        while (dataIt.hasNext())
        {
            i++;
            sum += dataIt.next();
            if (i == counter)
            {
                returnValue.add(sum);
                counter = 12;
                i       = 0;
                sum     = 0;
            }
        }
        if (i > 0)
        {
            returnValue.add(sum);
        }

        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Change the sign of all elements
    //--------------------------------------------------------------------------
    public CS_Calculation neg()
    {
        CS_Calculation returnValue = new CS_Calculation(null);
        List<Decimal> data = new List<Decimal>();

        for (Decimal element : this.getData())
        {
            data.add(-element);
        }
        returnValue.data = data;

        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Split calculation by positive and negative values
    // Result are positive and negative calculations.
    //--------------------------------------------------------------------------
    public List<CS_Calculation> splitBySign()
    {
        CS_Calculation first = new CS_Calculation(null);
        CS_Calculation second = new CS_Calculation(null);
        List<Decimal> positive = new List<Decimal>();
        List<Decimal> negative = new List<Decimal>();

        for (Decimal value : this.data)
        {
            if (value >= 0)
            {
                positive.add(value);
                negative.add(0);
            }
            else
            {
                positive.add(0);
                negative.add(value);
            }
        }

        first.data = positive;
        second.data = negative;

        return new List<CS_Calculation>{ first, second };
    }

    //--------------------------------------------------------------------------
    // Craate vector with one off only
    //--------------------------------------------------------------------------
    public static CS_Calculation createOneOff(Decimal oneOff)
    {
        return createOneOffOnMonth(oneOff, 0);
    }

    //--------------------------------------------------------------------------
    // Create vector with one off on month. Before that month data is filled
    // with zeros.
    //--------------------------------------------------------------------------
    public static CS_Calculation createOneOffOnMonth
        ( Decimal oneOff
        , Integer month)
    {
        CS_Calculation returnValue = new CS_Calculation(null);

        List<Decimal> data = new List<Decimal>();

        for (;month > 0; month--)
        {
            data.add(0);
        }

        data.add(CS_Util.prepare(oneOff));

        returnValue.data = data;

        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Create vector from one off and recurring charge
    //--------------------------------------------------------------------------
    public static CS_Calculation createOneOffRecurring
        ( Decimal oneOff
        , Decimal recurring
        , Integer contractTerm )
    {
        CS_Calculation returnValue = new CS_Calculation(null);

        List<Decimal> data = new List<Decimal>();
        data.add(CS_Util.prepare(oneOff));
        for (Integer i = contractTerm; i > 0; i--)
        {
            data.add(CS_Util.prepare(recurring));
        }
        returnValue.data = data;

        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Create vector with NPV coefficients of given length
    //--------------------------------------------------------------------------
    public static CS_Calculation createNPVFactors(Integer length, boolean npvMode)
    {
        Decimal factor;
        if (npvMode) {
            factor = NPV_FACTOR;
        } else {
            factor = NO_NPV;
        }
        
        CS_Calculation returnValue = new CS_Calculation(null);
        Decimal value = 1;
        Decimal monthlyFactor = Math.exp(-Math.log(factor) / 12);

        List<Decimal> data = new List<Decimal>();

        data.add(1);

        for (Integer i = length; i > 1; i--)
        {
            data.add(value);
            value = value * monthlyFactor;
        }

        returnValue.data = data;
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Sum list of calculations
    //--------------------------------------------------------------------------
    public static CS_Calculation sum(List<CS_Calculation> calculations)
    {
        CS_Calculation returnValue = new CS_Calculation(null);
        for (CS_Calculation calculation : calculations)
        {
            returnValue = returnValue.add(calculation);
        }
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Sum list of calculation and return two calculations revenue and cost
    //--------------------------------------------------------------------------
    public static List<CS_Calculation> sumBySign
        ( List<CS_Calculation> calculations )
    {
        CS_Calculation positive = new CS_Calculation(null);
        CS_Calculation negative = new CS_Calculation(null);

        for (CS_Calculation calculation : calculations)
        {
            List<CS_Calculation> split = calculation.splitBySign();
            positive = positive.add(split[0]);
            negative = negative.add(split[1]);
        }

        return new List<CS_Calculation>{ positive, negative };
    }
}