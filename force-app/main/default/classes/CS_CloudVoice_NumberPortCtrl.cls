public class CS_CloudVoice_NumberPortCtrl {

	private Id basketId;
	public Integer numbersToPort { get; set; }
	public String retURL { get; set; }
	public List<Number_Port_Line_CPQ__c> numberPortList { get; set; }
	public Number_Port_Line_CPQ__c newNumberPort { get; set; }
	public Integer selectedIndex { get; set; }
	public Id selectedAddressValue { get; set; }
	public String errorMessage { get; set; }
	public Blob csvFileBody { get; set; }
	public string csvString { get; set; }
	public Contact contactObj { get; set; }
	public Account accountObj { get; set; }
	public class applicationException extends Exception {}

	public CS_CloudVoice_NumberPortCtrl() {
		basketId = ApexPages.currentPage().getParameters().get('basketId');
		numbersToPort = integer.valueof(ApexPages.currentPage().getParameters().get('numstoport'));
		retURL = EncodingUtil.urlDecode(ApexPages.currentPage().getParameters().get('returl'), 'UTF-8');
		cscfga__Product_Basket__c basket = [select id, Customer_Contact__c, csbb__Account__c from cscfga__Product_Basket__c where id = :basketId];
		try {
			contactObj = [select Id, Name, FirstName, LastName, Job_Title__c, Phone, Email from Contact where Id = :basket.Customer_Contact__c];
		}
		catch(Exception e) {
			throw new applicationException('Customer_Contact__c field on the basket is missing. Please fill it in before trying again');
		}

		accountObj = [select Id, Name, Company_Registration_Number__c, BillingStreet, BillingCity, BillingState, BillingPostalcode from Account where Id = :basket.csbb__Account__c];
		getNumberPortList();
		newNumberPort = new Number_Port_Line_CPQ__c(Product_Basket__c = basketId);
	}

	public void getNumberPortList() {
		numberPortList = [SELECT Id,
		                  Product_Basket__c,
		                  Losing_CP__c,
		                  Billing_A_C_Number__c,
		                  Number_Type__c,
		                  Main_Number__c,
						  Order_Address__r.Display_Name__c,
						  Range_Holder__c,
		                  Number_Start__c,
						  Number_End__c,
		                  Associated_Numbers__c,
		                  Service_Usage__c,
		                  //Temporary_Number_Required__c,
		                  PVT_Ref__c,
		                  //LOA_Status__c,
						  Add_to_BT_Directory__c FROM Number_Port_Line_CPQ__c Where Product_Basket__c = :basketId ORDER BY Losing_CP__c ASC];
	}

	public List<SelectOption> getSiteAddressList(){
		List<SelectOption> addressList = new  List<SelectOption>();
		for (Order_Address__c address : [select Id, Display_Name__c from Order_Address__c order where Account__c =: accountObj.Id]) {
			addressList.add(new SelectOption(address.Id, address.Display_Name__c));
		}
		return addressList;       
	}

	public List<Number_Port_Reference__c> getCPs() {
		List<Number_Port_Reference__c> rlist = [Select Id, Type__c, Reference__c, Description__c, Area__c, EstablishedIP__c FROM Number_Port_Reference__c WHERE Type__c = 'CP' ORDER BY Reference__c LIMIT 1000];
		return rlist;
	}

	Public PageReference newNumberPort() {
		Number_Port_Line_CPQ__c nplObj = new Number_Port_Line_CPQ__c(Product_Basket__c = basketId);
		saveInlineChanges();
		insert nplObj;
		getNumberPortList();
		return ApexPages.CurrentPage();
	}

	public Void addNumberPort() {
		if (numberPortList.Size() >= numbersToPort) {
			errorMessage = 'You have completed '+numberPortList.size()+' of '+numbersToPort+' number port required';
		}
		else if (newNumberPort.PVT_Ref__c == Null) {
			errorMessage = 'PVT Ref cannot be empty!';
		}
		else {
			newNumberPort.Order_Address__c = selectedAddressValue;	
				
			//-------Range Holder Filter
			String rh = newNumberPort.Range_Holder__c;
			try{
				newNumberPort.Range_Holder__c = rh.substring(0, rh.indexOf('-')) + '(CUPID' + rh.substring(rh.indexOf(':'), rh.length());
			}catch(Exception e){}
			try{
				rh = newNumberPort.Range_Holder__c.Replace('**Not Service Established**', '');
				newNumberPort.Range_Holder__c = rh;
			}catch(Exception e){}
			//-------Losing CP Filter
			String cp = newNumberPort.Losing_CP__c;
			try{
				newNumberPort.Losing_CP__c = cp.substring(0, cp.indexOf('-')) + '(CUPID' + cp.substring(cp.indexOf(':'), cp.length());
			}catch(Exception e){}
			try{
				cp = newNumberPort.Losing_CP__c.Replace('**Not Service Established**', '');
				newNumberPort.Losing_CP__c = cp;
			}catch(Exception e){}

			insert newNumberPort;
			getNumberPortList();
			newNumberPort = new Number_Port_Line_CPQ__c(Product_Basket__c = basketId);
			errorMessage = null;
		}
	}

	Public Void deleteNumberPort() {
		Integer indexVal = selectedIndex;
		if (numberPortList[indexVal - 1].Id != null) {
			saveInlineChanges();
			delete numberPortList[indexVal - 1];
			getNumberPortList();
			errorMessage = null;
		}
	}

	Public Void cloneNumberPort() {
		if (numberPortList.Size() >= numbersToPort) {
			errorMessage = 'You have completed '+numberPortList.size()+' of '+numbersToPort+' number port required';
		}
		else {
			Integer indexVal = selectedIndex;
			if (numberPortList[indexVal - 1].Id != null) {
				Number_Port_Line_CPQ__c clonedNPL = numberPortList[indexVal - 1].clone(false);
				saveInlineChanges();
				insert clonedNPL;
				getNumberPortList();
				errorMessage = null;
			}
		}
	}

	Public Void saveInlineChanges() {
		update numberPortList;
		errorMessage = null;
	}

	public void importCSVFile() {		
		try {
			List<Number_Port_Line_CPQ__c> numberPortImportList = new List<Number_Port_Line_CPQ__c>();
			String[] csvFileLines;
			csvString = csvFileBody.toString();
			csvFileLines = csvString.split('\n');

			for (Integer i = 1; i < csvFileLines.size(); i++) {
				String[] csvRecordData = csvFileLines[i].split(',');
				Number_Port_Line_CPQ__c nplObj = new Number_Port_Line_CPQ__c();
				nplObj.Product_Basket__c = basketId;
				nplObj.Order_Address__c = csvRecordData[0];
				nplObj.Range_Holder__c = csvRecordData[1];			
				nplObj.Losing_CP__c = csvRecordData[2];
				nplObj.Billing_A_C_Number__c = csvRecordData[3];
				nplObj.Main_Number__c = csvRecordData[4];
				nplObj.Number_Type__c = csvRecordData[5];
				nplObj.Number_Start__c = csvRecordData[6];
				nplObj.Number_End__c = csvRecordData[7];
				nplObj.Associated_Numbers__c = csvRecordData[8];
				nplObj.Service_Usage__c = csvRecordData[9];
				nplObj.PVT_Ref__c = csvRecordData[10];
				nplObj.Add_to_BT_Directory__c = Boolean.ValueOf(csvRecordData[11]);
				numberPortImportList.add(nplObj);
			}
			insert numberPortImportList;
			getNumberPortList();
			errorMessage = null;
		}
		catch(Exception e) {
			errorMessage = 'An error has occured while importing data, please ensure input CSV file is correct.';
		}
	}

	public PageReference closeButtonClick() {
        PageReference pageRef = new PageReference(retURL);
        return pageRef;
	}
}