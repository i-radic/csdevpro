public with sharing class FlowCRFCtrl {

    public FlowCRFCtrl(ApexPages.StandardController controller) {

    }
        public string SLID = System.currentPageReference().getParameters().get('id');
        
        public Provide_Line_Details__c newProd {
        get {
        if (newProd == null)
        newProd = new Provide_Line_Details__c();
        return newProd ;
        }
        set;
        }
        
        public List<Provide_Line_Details__c> getProdList() {
        return [SELECT ID, Name, Serial_No__c, Quantity__c, Description_AX__c, Initial_charges__c, Recurring_charges__c, Rental_Term__c, Product_code__c, Supply_Code__c, Initial_Charge_Code__c, Recurring_Charge_Code__c, Related_to_Flow_CRF__c FROM Provide_Line_Details__c WHERE Related_to_Flow_CRF__c = :SLID ORDER BY CreatedDate DESC];
        }
        
        public PageReference addProd() {
        newProd.Related_to_Flow_CRF__c = SLID ; // the record the file is attached to
        newProd.RecordTypeId = [Select Id from RecordType where DeveloperName='Provide_Lines_Details_AX'].Id;
        /*
        if (newProd.Product_Type2__c== null) {
        newProd.adderror('Please select the Product Type.'); 
        return null;
        }
        if (newProd.Product__c== null) {
        newProd.adderror('Please select the Product.'); 
        return null;
        }    
        if (newProd.Quantity__c == null) {
        newProd.adderror('Please enter the Quantity.'); 
        return null;
        }
        */
        try {
        insert newProd;
        } catch (DMLException e) {
        ApexPages.addMessages(e);
        } finally {
        newProd= new Provide_Line_Details__c(); 
        }
        
        return null;
        }
        



}