global class CS_LookupQueryForAccountDetails implements cssmgnt.RemoteActionDataProvider{
    
    global static Map<String, Object> getData(Map<String, Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>(); 
        String basketId = String.valueOf(inputMap.get('basketId'));
        
        try{
            List<cscfga__Product_Basket__c> productBasketList= [select id,Name,csbb__Account__c from cscfga__Product_Basket__c where id =:basketId];
            if(!productBasketList.isEmpty() && basketId!=null){
                returnMap.put ('AccountId', productBasketList[0].csbb__Account__c);
                returnMap.put ('DataFound', true);
            }else{
                 returnMap.put ('DataFound', false);
            }
            
        }catch(Exception e){
            
        }
        return returnMap;
    }
    
}