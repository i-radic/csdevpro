@isTest(SeeAllData=true)
private class Test_MACOrderConfirmationController{
    
    static testMethod void myUnitTest(){
    
    Test_Factory.SetProperty('IsTest', 'yes');
    
     Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
    
     User u1 = new User();
        u1.Username = '999999000@bt.com';
        u1.Ein__c = '999999000';
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '01234567890';
        u1.Phone = '01234567890';
        u1.Title='TestTitle';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.TIMEZONESIDKEY = 'Europe/London';
        u1.LOCALESIDKEY  = 'en_GB';
        u1.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u1.PROFILEID = profileId;
        u1.LANGUAGELOCALEKEY = 'en_US';       
        u1.email = 'no.reply@bt.com';
        insert u1;
    
    Account newAccount = new Account (name='XYZ Organization',OwnerId=u1.id);
    Database.SaveResult accountResult = Database.insert(newAccount);

    Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',AccountId=accountResult.id,Email='test.mail@test.com');
    Database.SaveResult contactResult = Database.insert(myContact);
   
    
    CustomerOptions__c cot=new CustomerOptions__c(Account__c=newAccount.Id, Telephone_Number__c='0123456789', Account_Number__c='AB12345678', Contact__c=contactResult.Id);
    Database.SaveResult cotResult = Database.insert(cot);
    
    BTLB_Customer_Option_Product__c cop=new BTLB_Customer_Option_Product__c(Order_Type__c='mac',BTLB_Customer_Option_record__c=cotResult.Id, Mac_code__c='ABCD12345678/EF12G',Telephone_Number__c='0123456789',Quantity__c=1);
    Database.SaveResult copResult = Database.insert(cop);
    
     
     
     MACOrderConfirmationController controller = new MACOrderConfirmationController();
     controller.custCotId = cotResult.Id;
     controller.getTemplateDet();
      
     
    }
     
}