/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2013-05-10 16:59:30 
 *	@description:
 *	    various utility methods useful in triggers
 *	
 */
public with sharing class TriggerUtils {
	public static final Map<String, String> SKIP_CAUSE_BY_KEY = new Map<String, String>();

	public static Boolean hasSkipReason(String key) {
		String cause = SKIP_CAUSE_BY_KEY.get(key);
		if (null != cause) {
			System.debug('ag§§ SKIP ' + key + ' due to:: ' + cause);
			return true;
		}
		return false;
	} 

	public static void setSkipReason(String key, String cause) {
		SKIP_CAUSE_BY_KEY.put(key, cause);
	}
	public static void removeSkipReason(String key) {
		SKIP_CAUSE_BY_KEY.remove(key);
	}

	/**
	 * usually used in unit tests
	 */
	public static void clearStatics() {
		SKIP_CAUSE_BY_KEY.clear();
	}

}