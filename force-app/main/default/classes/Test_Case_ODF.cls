@isTest
private class Test_Case_ODF {
    static RecordType rt;
    static Case c1;
    
    static{
    
        rt = [select id from RecordType where SobjectType='Case' and name ='Order Discrepancy' limit 1];
        //Create Users & hierachy
        List<Profile> profiles2 = [Select p.Id, p.Name from Profile p Where p.name = 'Service Provision'];
        string profileId2 = profiles2[0].Id;
        
        
        List<User> thisUser = [Select id, Run_Apex_Triggers__c, Manager_EIN__c from User where id = :UserInfo.getUserId() Limit 1];
        thisUser[0].Run_Apex_Triggers__c = False;
        update thisUser[0]; 
 

        User uM = new User();
          uM.Username = '999999000@bt.com';
          uM.Ein__c = '999999000';
          uM.LastName = 'TestLastname';
          uM.FirstName = 'TestFirstname';
          uM.MobilePhone = '07918672032';
          uM.Phone = '02085878834';
          uM.Title='What i do';
          uM.OUC__c = 'DKW';
          uM.Manager_EIN__c = '123456789';
          uM.Email = 'no.reply@bt.com';
          uM.Alias = 'boatid01';
          uM.TIMEZONESIDKEY = 'Europe/London';
          uM.LOCALESIDKEY  = 'en_GB';
          uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
          uM.PROFILEID = profileId2;
          uM.LANGUAGELOCALEKEY = 'en_US';
          //uGM.ISACTIVE = FALSE;
          uM.email = 'no.reply@bt.com';
          Database.SaveResult[] uMResult = Database.insert(new User [] {uM});
          //insert uM;


          User uGM = new User();
              uGM.Username = '999999001@bt.com';
              uGM.Ein__c = '999999001';
              uGM.LastName = 'TestLastname';
              uGM.FirstName = 'TestFirstname';
              uGM.MobilePhone = '07918672032';
              uGM.Phone = '02085878834';
              uGM.Title='What i do';
              uGM.OUC__c = 'DKW';
              uGM.Manager_EIN__c = '999999000';
              uGM.ManagerId = uMResult[0].id;
              uGM.Email = 'no.reply@bt.com';
              uGM.Alias = 'boatid01';
              uGM.TIMEZONESIDKEY = 'Europe/London';
              uGM.LOCALESIDKEY  = 'en_GB';
              uGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
              uGM.PROFILEID = profileId2;
              uGM.LANGUAGELOCALEKEY = 'en_US';
              //uGM.ISACTIVE = FALSE;
              uGM.email = 'no.reply@bt.com';
              Database.SaveResult[] uGMResult = Database.insert(new User [] {uGM});
              //insert uGM;
      
    
 
       User uDGM = new User();
          uDGM.Username = '999999002@bt.com';
          uDGM.Ein__c = '999999002';
          uDGM.LastName = 'TestLastname';
          uDGM.FirstName = 'TestFirstname';
          uDGM.MobilePhone = '07918672032';
          uDGM.Phone = '02085878834';
          uDGM.Title='What i do';
          uDGM.OUC__c = 'DKW';
          uDGM.Manager_EIN__c = '999999001';
          uDGM.ManagerId = uGMResult[0].id;
          uDGM.Email = 'no.reply@bt.com';
          uDGM.Alias = 'boatid01';
          uDGM.managerID = uGM.Id;
          uDGM.TIMEZONESIDKEY = 'Europe/London';
          uDGM.LOCALESIDKEY  = 'en_GB';
          uDGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
          uDGM.PROFILEID = profileId2;
          uDGM.LANGUAGELOCALEKEY = 'en_US';
          //uDGM.ISACTIVE = FALSE;
          uDGM.email = 'no.reply@bt.com';
          Database.SaveResult[] uDGMResult = Database.insert(new User [] {uDGM});
          //insert uDGM;
          
        User uSM = new User();
          uSM.Username = '999999003@bt.com';
          uSM.Ein__c = '999999003';
          uSM.LastName = 'TestLastname';
          uSM.FirstName = 'TestFirstname';
          uSM.MobilePhone = '07918672032';
          uSM.Phone = '02085878834';
          uSM.Title='What i do';
          uSM.OUC__c = 'DKW';
          uSM.Manager_EIN__c = '999999002';
          uSM.ManagerId = uDGMResult[0].id;
          uSM.Email = 'no.reply@bt.com';
          uSM.Alias = 'boatid01';
          uSM.managerID = uDGM.Id;
          uSM.TIMEZONESIDKEY = 'Europe/London';
          uSM.LOCALESIDKEY  = 'en_GB';
          uSM.EMAILENCODINGKEY = 'ISO-8859-1';                               
          uSM.PROFILEID = profileId2;
          uSM.LANGUAGELOCALEKEY = 'en_US';
          //uSM.ISACTIVE = FALSE;
          uSM.email = 'no.reply@bt.com';
        insert uSM;
      
        //link user running test to hierachy created above
        thisUser[0].ManagerId = uSM.ID;
        update thisUser[0]; 
        
        c1 = new case();
        c1.Vol_Reference__c = 'VOL011-12345678900';
        c1.Reason = 'test';
        c1.Front_Office_ein__c = '999999003';
        c1.RecordTypeId = rt.id;
        insert c1;
    }
    
    

	
    
   
    static testMethod void runPositiveTestCases() {
        Test.startTest();     
        //Order descrepency
        //test escalations
        c1.Escalation_Stage__c = 'Front Office Stage 1';
        c1.status = 'Assigned to Front Office';
        update c1;
        Test.stopTest();
    }

    static testMethod void runPositiveTestCases1(){
        Test.startTest();
        c1.Escalation_Stage__c = 'Front Office Stage 2';
        update c1;
        Test.stopTest();
    }
    
    static testMethod void runPositiveTestCases2(){
        Test.startTest();
        c1.Escalation_Stage__c = 'Front Office Stage 3';
        update c1;
        Test.stopTest();
    }
    static testMethod void runPositiveTestCases3(){
        Test.startTest();
        c1.Escalation_Stage__c = 'Back Office Stage 1';
        c1.status = 'Assigned to Back Office';
        c1.Response__c = 'Reply';
        update c1;
        c1.transfer_count__c = 2;
        c1.status = 'Assigned to Front Office';
        update c1;
        Test.stopTest();
    }
    static testMethod void runPositiveTestCases4(){
        Test.startTest();
        c1.Escalation_Stage__c = 'Back Office Stage 2';
        update c1;
        Test.stopTest();
    }
    static testMethod void runPositiveTestCases5(){
        Test.startTest();
        c1.Escalation_Stage__c = 'Back Office Stage 3';
        update c1;
        Test.stopTest();
    }
    
    static testMethod void runPositiveTestCases6(){
        Test.startTest();
        c1.Closure_Reason__c = 'some reason';
        c1.Closing_Action__c = 'some action';
        c1.status = 'Closed';
        update c1;
        Test.stopTest();
    }
}