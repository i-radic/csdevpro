public class createBulkTasksAccount {

        public Account account = null;
        public Integer diff = 0;
        public Integer aSize = 0;
        public Integer tSize = 0;
        public Integer aMSize = 0;
        public Integer tMSize = 0;
        public Integer prevActs = 0;
        public Integer currentActs = 0;
        public Integer testing = 0;
        public id loggedinID = UserInfo.getUserId();
        public string loggedinTeamFunction = null;
        public string taskRole = null;
        public string Sector = null;
        public string Lob = null;
        
        public PageReference createAccountTasks() {

            if (this.account != null) {
                    Id accountId = account.Id;

            List<Account> accounts = [select id, Sector_code__c, lob_code__c, SAC_Code__c, LE_Code__c from Account where Id = :accountId];
                    account = accounts[0];
                    Sector = account.Sector_code__c;
                    Lob = account.lob_code__c;

//get user team function to determin tasks to create
            List <AccountTeamMember> userRole = [Select a.AccountId, a.TeamMemberRole, a.UserId from AccountTeamMember a where a.UserId = :loggedinID and a.accountId = :account.id];
                    if (userRole.size() > 0) {
                            taskRole = userRole[0].TeamMemberRole;
                            }
                        
//get List of  existing open Tasks or are not due for presenting for completion again yet
                set <String> existTasks = new Set<String>();
                List<Task> existTasksQ = [Select Task_Id__c from Task where WhatId = :accountId and Task_Category__c = 'Account' and (isClosed = false or Minimum_Repeat_Date__c > :Date.today())];
                for (Task et :existTasksQ)
                existTasks.add(et.Task_Id__c);                        


if (Sector == 'BTLB' ){// get BTLB tasks else assume task are at Line of Business (LOB) level
// get list of tasks to be created for BTLB user
        List<Bulk_Tasks__c> tasks1 = [Select q.Reminder_Days__c, q.Minimum_Repeat_Time__c, q.Minimum_Lead_Time__c, q.Promote__c, q.Details__c, q.Description__c, q.CreatedDate, q.Task_Id__c, Collateral_text__c, Collateral_url__c, Activity_area__c, Activity_type__c, System_text__c, System_url__c, Task_Category__c 
        From Bulk_Tasks__c q 
        where Task_Category__c = 'Account' and Active_Task_Switch__c = true and sector_code__c = :Sector and USER_Type__c =:taskRole and Task_Id__c not in :existTasks];
                           aSize = tasks1.size();
//create tasks for identified BTLB accounts
                    List<Task> newTasks = new List<Task>();
                for (Bulk_Tasks__c qa : tasks1) {
                            Task nt = new Task();
                            nt.ActivityDate = (Date.today()+ qa.Minimum_Lead_Time__c.intValue());
                            nt.Target_Completion_Date__c = nt.ActivityDate;
                            nt.ReminderDateTime = datetime.newInstance((nt.ActivityDate - qa.Reminder_Days__c.intValue()).year(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).month(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).day(),8,0,0);
                            if (qa.reminder_days__c != null){
                                nt.ISREMINDERSET = TRUE;
                            }
                            nt.OwnerId = UserInfo.getUserId();
                            nt.Subject = qa.Description__c;
                            nt.WhatId = account.Id;
                            nt.Task_Id__c = qa.Task_Id__c;
                            nt.Promote__c = qa.Promote__c;
                            nt.Details__c = qa.Details__c;
                            nt.Collateral_text__c = qa.Collateral_text__c;
                            nt.Collateral_url__c = qa.Collateral_url__c;
                            nt.System_text__c = qa.System_text__c;
                            nt.System_url__c = qa.System_url__c;
                            nt.Task_Category__c = qa.Task_Category__c;
                            nt.SAC_Code__c = account.SAC_Code__c;
                            nt.LE_Code__c = account.LE_Code__c;
                             if (qa.Minimum_Repeat_Time__c != null){
                            nt.Minimum_Repeat_Date__c = (Date.today()+ qa.Minimum_Repeat_Time__c.intValue());
                             }
                             else{
                              nt.Minimum_Repeat_Date__c = (Date.today()-1);
                             }
                            newTasks.add(nt);
                            }
                    tSize = newTasks.size();
                    insert newTasks;
                    
 }
        else{ // get corporate tasks
       List<Bulk_Tasks__c> tasks2 = [Select q.Reminder_Days__c, q.Minimum_Repeat_Time__c, q.Minimum_Lead_Time__c, q.Promote__c, q.Details__c, q.Description__c, q.CreatedDate, q.Task_Id__c, Collateral_text__c, Collateral_url__c, Activity_area__c, Activity_type__c, System_text__c, System_url__c, Task_Category__c 
       From Bulk_Tasks__c q 
       where Task_Category__c = 'Account' and Active_Task_Switch__c = true and LOB_code__c = :lob and USER_Type__c =:taskRole and Task_Id__c not in :existTasks];
        aSize = tasks2.size();
//create tasks for identified non BTLB accounts
                    List<Task> newTasks = new List<Task>();
                for (Bulk_Tasks__c qa : tasks2) {
                            Task nt = new Task();
                            nt.ActivityDate = (Date.today()+ qa.Minimum_Lead_Time__c.intValue());
                            nt.Target_Completion_Date__c = nt.ActivityDate;
                            nt.ReminderDateTime = datetime.newInstance((nt.ActivityDate - qa.Reminder_Days__c.intValue()).year(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).month(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).day(),8,0,0);
                            if (qa.reminder_days__c != null){
                                nt.ISREMINDERSET = TRUE;
                            }
                            nt.OwnerId = UserInfo.getUserId();
                            nt.Subject = qa.Description__c;
                            nt.WhatId = account.Id;
                            nt.Task_Id__c = qa.Task_Id__c;
                            nt.Promote__c = qa.Promote__c;
                            nt.Details__c = qa.Details__c;
                            nt.Collateral_text__c = qa.Collateral_text__c;
                            nt.Collateral_url__c = qa.Collateral_url__c;
                            nt.System_text__c = qa.System_text__c;
                            nt.System_url__c = qa.System_url__c;
                            nt.Task_Category__c = qa.Task_Category__c;
                            nt.SAC_Code__c = account.SAC_Code__c;
                            nt.LE_Code__c = account.LE_Code__c;
                            if (qa.Minimum_Repeat_Time__c != null){
                            nt.Minimum_Repeat_Date__c = (Date.today()+ qa.Minimum_Repeat_Time__c.intValue());
                             }
                             else{
                              nt.Minimum_Repeat_Date__c = (Date.today()-1);
                             }
                            newTasks.add(nt);
                            }
                    tSize = newTasks.size();
                    insert newTasks;
     }

// go back to original page
                    PageReference pr = new PageReference('/'+accountId);
                    pr.setRedirect(true);

                    return pr;

                    }

            return null;
            }

        public createBulkTasksAccount (ApexPages.StandardController stdController) {
            account = (Account)stdController.getRecord();
            }

        public createBulkTasksAccount () {}

        }