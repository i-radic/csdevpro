@isTest
private class Test_IssueVote {

	static Issue__c iss;
	 
	static{
        iss = new Issue__c();
        insert iss;    
       
	}

    static testMethod void issTestAll() {
        //
        PageReference pageRef = Page.IssueVoteButton;
	    Test.setCurrentPage(pageRef);
	    ApexPages.StandardController mainIss = new ApexPages.StandardController(iss);

		IssueVote controller = new IssueVote(mainIss);
	    controller.doIssueVote();
	    
	    Integer rowsSelected = [select count() from issue_vote__c where issue__c = :iss.id];
	    System.assertEquals(rowsSelected, 1);
	    
	    mainIss = new ApexPages.StandardController(new Issue__c());
		controller = new IssueVote(mainIss);
	    controller.doIssueVote();
	    
    }
}