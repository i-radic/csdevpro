public class usageProfilesUploadResignPacks
{
    public string fileName{get;set;}
    public string attachmentName{get;set;}
    public Blob fileContent{get;set;}
    String[] filelines = new String[]{};
    Usage_Profile__c up;
    Resign_Request__c rrp;
    
    public static boolean isApexTest = false;
    public static String testResponse='';
    public Boolean refreshPage {get; set;}
    public Id recordId  {    get;set;    } 
    public String recordName  {    get;set;    }  
    public String uploadResponse { get;set;}

    
    public usageProfilesUploadResignPacks(ApexPages.StandardController stdController) 
    {
        recordId  = stdController.getRecord().Id;         
        if(  ApexPages.currentPage().getParameters().get('id') != null)
        {    
            rrp = [SELECT id,Account__c  FROM Resign_Request__c WHERE id = :ApexPages.currentPage().getParameters().get('id')];
            
        }
      
    }
    public PageReference uploadInputFile(String attachmentName)  
    {  
        PageReference pr;  
        if(fileName!= null && fileContent!= null)  
        {  
              Attachment myAttachment  = new Attachment();  
              myAttachment.Body = fileContent;  
              myAttachment.Name = attachmentName;  
              myAttachment.ContentType = 'text/csv;charset=UTF-8'; 
              myAttachment.ParentId = rrp.id;  
              insert myAttachment;               
              //pr = new PageReference('/' + myAttachment.Id);               
              //update up;              
        }  
        return null;  
    }      
    public Pagereference ReadFile()
    {  
        uploadInputFile(fileName);
        attachmentName= fileName; 
        try 
        {
            fileName=fileContent.toString();
            System.debug(fileName);
            filelines = fileName.split('\n');
            Decimal expectedVoiceTmp;
            Decimal expectedSMSTmp;
            System.debug('Number of rows in the csv file = ' + filelines.size());
            Usage_Profile__c usageProfile = new Usage_Profile__c();
            usageProfile.Account__c = rrp.Account__c;
         
        for (Integer i=1;i<filelines.size();i++)
        { 
                String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');            
                if (inputvalues[1].containsIgnoreCase('Total no.of voice connections'))
                {
                       usageProfile.Total_Number_of_Connections__c = convertStringInput(inputvalues[3]);                                   
                } 
                if (inputvalues[4].containsIgnoreCase('Company Name'))
                {
                       if(inputvalues[5].length() >50)
                           usageProfile.Name = inputvalues[5].substring(1,50);  
                       else 
                           usageProfile.Name = inputvalues[5];                                   
                } 
                if (inputvalues[1].containsIgnoreCase('Total no.of inactive voice connections'))
                {
                        usageProfile.Total_Number_of_Inactive_Connections__c = convertStringInput(inputvalues[3]);                                   
                } 
                if (inputvalues[1].equalsIgnoreCase('Max additional new connections or inactive re-signs'))
                {
                        //usageProfile.Total_Number_of_Inactive_Connections__c = Decimal.valueOf(inputvalues[3].trim());                                   
                } 
                if (inputvalues[7].equalsIgnoreCase('Data Only Connections'))
                {
                        usageProfile.Data_Only_Connections__c = convertStringInput(inputvalues[8]);                                     
                } 
                if (!inputvalues[7].equalsIgnoreCase('Data Only Connections') && inputvalues[8].equalsIgnoreCase('Data Only Connections'))
                {
                        usageProfile.Data_Only_Connections__c = convertStringInput(inputvalues[9]);                                     
                } 
                if (inputvalues[1].containsIgnoreCase('Expected Voice (Mins)'))
                {
                        expectedVoiceTmp = convertStringInput(inputvalues[2]);                                     
                } 
                if (inputvalues[4].containsIgnoreCase('Expected SMS (volume)'))
                {
                        expectedSMSTmp = convertStringInput(inputvalues[5]);                                     
                } 
                if (inputvalues[7].containsIgnoreCase('Expected Data (MB)'))
                {
                        //usageProfile .Expected_Data__c = Decimal.valueOf(inputvalues[8].trim());                                   
                } 
                if(inputvalues[1].containsIgnoreCase('Closed user group'))
                {
                      usageProfile .Closed_user_group__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].containsIgnoreCase('Closed user group text messaging'))
                  {
                       usageProfile .Closed_user_group_text_messaging__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('Data'))
                {
                   usageProfile.Data__c = removePercent(inputvalues[8].trim()) ;                  
                }           
                  if(inputvalues[1].containsIgnoreCase('Calls to EE'))
                {
                      usageProfile.Calls_to_EE__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].containsIgnoreCase('Text messaging to EE'))
                {
                       usageProfile.Text_message_to_EE__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('Roaming data in EU'))
                {
                    usageProfile.Roaming_data_in_EU__c = removePercent(inputvalues[8].trim()) ;                  
                }  
                  if(inputvalues[1].containsIgnoreCase('Calls to Orange'))
                {
                      usageProfile.Calls_to_Orange__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].containsIgnoreCase('Text messaging to Orange'))
                {
                       usageProfile.Text_messaging_to_Orange__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('Roaming data in ROW'))
                {
                    usageProfile.Roaming_data_in_ROW__c = removePercent(inputvalues[8].trim()) ;                  
                }   
                if(inputvalues[1].containsIgnoreCase('Calls to T-Mobile'))
                {
                      usageProfile.Calls_to_T_Mobile__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].containsIgnoreCase('Text messaging to T-Mobile'))
                {
                       usageProfile.Text_messaging_to_T_Mobile__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('Wifi'))
                {
                    usageProfile.Wifi__c = removePercent(inputvalues[8].trim()) ;                  
                }  
                 if(inputvalues[1].containsIgnoreCase('Calls to other UK mobile networks'))
                {
                      usageProfile.Calls_to_other_UK_mobile_networks__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].containsIgnoreCase('Text messaging to other UK mobile networks'))
                {
                       usageProfile.Text_messaging_to_other_UK_mobile__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('Wifi in EU'))
                {
                    usageProfile.Wifi_in_EU__c = removePercent(inputvalues[8].trim()) ;                  
                }         
                 if(inputvalues[1].containsIgnoreCase('Answer phone'))
                {
                      usageProfile.Answer_phone__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].containsIgnoreCase('Other text messaging'))
                {
                       usageProfile.Other_text_messaging__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('Wifi in ROW'))
                {
                    usageProfile.Wifi_in_ROW__c = removePercent(inputvalues[8].trim()) ;                  
                }      
                  if(inputvalues[1].containsIgnoreCase('Landline'))
                {
                      usageProfile.Landline__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].containsIgnoreCase('International numbers text messaging'))
                {
                       usageProfile.International_numbers_text_messag__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('MMS'))
                {
                    usageProfile.MMS__c = removePercent(inputvalues[8].trim()) ;                  
                }
                if(inputvalues[1].equalsIgnoreCase('Other'))
                {
                    usageProfile.Other__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[4].equalsIgnoreCase('Outgoing roaming text messaging'))
                {
                     usageProfile.Outgoing_roaming_text_messaging__c = removePercent(inputvalues[5].trim()) ;                   
                }
                if(inputvalues[7].equalsIgnoreCase('MMS roaming'))
                {
                    usageProfile.MMS_Roaming__c = removePercent(inputvalues[8].trim()) ;                  
                }    
                if(inputvalues[1].equalsIgnoreCase('International calls'))
                {
                    usageProfile.International_calls__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[1].equalsIgnoreCase('Outgoing roaming from EU'))
                {
                    usageProfile.Outgoing_roaming_from_EU__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[1].equalsIgnoreCase('Outgoing roaming from ROW'))
                {
                    usageProfile.Outgoing_roaming_from_ROW__c = removePercent(inputvalues[2].trim()) ;                   
                }
                if(inputvalues[1].equalsIgnoreCase('Incoming roaming in EU'))
                {
                    usageProfile.Incoming_roaming_in_EU__c = removePercent(inputvalues[2].trim()) ;                   
                }
                 if(inputvalues[1].equalsIgnoreCase('Incoming roaming in ROW'))
                {
                    usageProfile.Incoming_roaming_in_ROW__c = removePercent(inputvalues[2].trim()) ;                   
                }                 
            }
            insert usageProfile;  
            Usage_Profile__c insertedUsageProfile = [Select  Net_Active_Voice_Connections__c,Name from Usage_Profile__c where id= :usageProfile.id];
            if (insertedUsageProfile.Net_Active_Voice_Connections__c != null && expectedVoiceTmp !=null) 
            {
                insertedUsageProfile.Expected_Voice__c  =   expectedVoiceTmp*insertedUsageProfile.Net_Active_Voice_Connections__c;
            }
            if (insertedUsageProfile.Net_Active_Voice_Connections__c != null && expectedSMSTmp!=null) 
            {
                insertedUsageProfile.Expected_SMS__c  =   expectedSMSTmp*insertedUsageProfile.Net_Active_Voice_Connections__c;
            }     
               
                update insertedUsageProfile;               
                uploadResponse  = 'File read successfully.Please click the below link to go to the usage profile created.';
                recordId   =insertedUsageProfile.id;
                recordName   =insertedUsageProfile.Name;
                refreshPage = true;
               
         }
         catch(Exception e)
         {
             System.debug('Exception ='+e.getMessage());
             ApexPages.Message errorMsg= new ApexPages.Message(ApexPages.Severity.ERROR,'Error creating usage profile from the attached file.Please contact the system administrator.');
             ApexPages.addMessage(errorMsg); 
              
         }
            return null;
       
    }
    public Decimal removePercent(String inputData)
    {    
         Decimal inputDataNoPercent  = 0.0;
         System.debug('Input Data='+inputData);
         if(inputData!=null  &&inputData.indexOf('%') != -1)
             inputDataNoPercent = Decimal.valueOf(inputData.substring(0,inputData.indexOf('%')));
         else inputDataNoPercent =0.0;
         return inputDataNoPercent ;
    }
    public Decimal convertStringInput(String inputString)
    {    
         Decimal inputData = 0.0;
         System.debug('Input String='+inputString);
         if(inputString!=null  && inputString !='')
             inputData = Decimal.valueOf(inputString.trim());
         else inputData =0.0;
         return inputData ;
    }
   
   
}