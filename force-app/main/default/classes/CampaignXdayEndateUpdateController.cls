public without sharing class CampaignXdayEndateUpdateController {
	private String campaignId;
	
	public CampaignXdayEndateUpdateController() {
		System.debug('Initialise CampaignXdayEndateUpdateController...');
		for (String keyv : ApexPages.currentPage().getParameters().keySet()) {
      		System.debug ('currentPage().getParameters() : ' + keyv + ' : ' + ApexPages.currentPage().getParameters().get(keyv));
    	}
    	this.campaignId = ApexPages.currentPage().getParameters().get('id');
	}
	
	public PageReference runBatchJobs () {
		List<Campaign> c = [select Id, X_Day_Rule__c, EndDate from Campaign where Id = :campaignId];
		if(c == null || c.isEmpty()){
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 
				'Error: Could not find the campaign.');
	    	ApexPages.addMessage (myMsg);
	    	return null;
		}
		
		List<AsyncApexJob> jobs = [select Id from AsyncApexJob where status = 'Processing' OR status ='Queued'];
		
		//Only can have upto 5 running or queued apex jobs.
		if(jobs != null && jobs.size() <= 4){
			try{
				/*BatchUserCampaignUpdate bu = new BatchUserCampaignUpdate('SELECT Id FROM User WHERE isActive=true AND Current_Campaign_Id__c=\'' + c[0].Id + '\'', c[0].X_Day_Rule__c);
				database.executebatch(bu, 200);*/
				BatchTaskUpdate bt = new BatchTaskUpdate('SELECT Id FROM Task WHERE WhatId=\'' + c[0].Id + '\' AND isClosed=false AND Campaign_Task__c = \'TRUE\'', c[0].X_Day_Rule__c, c[0].EndDate);
				database.executebatch(bt, 200);
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, 'Mass update is running...  Please monitor batch progress via page (Setup>Monitoring>Apex Jobs)');
	    		ApexPages.addMessage (myMsg);
			}catch(Exception e){
				ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
	    		ApexPages.addMessage (myMsg);
			}
		}else{
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 
				'Too many batch jobs are already scheduled.  Please monitor the queue and retry later (Setup>Monitoring>Apex Jobs)');
	    	ApexPages.addMessage (myMsg);
		}
		  
        return null;
    }
}