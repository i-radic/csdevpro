@isTest
private class Test_TaskAfter {
/*
static Campaign c;
static Campaign c1;
static User uUser;
static Contact contact;
static Contact contact1;
static Contact contact2;
static Account a;
static Account a1;
static RecordType rt;
static CampaignMember cm;
static CampaignMember cm1;
static CampaignMember cm2;
static CampaignMember cm3;

static{
//Data setup - start
a = Test_Factory.CreateAccount();
insert a; 
//First contact
contact = Test_Factory.CreateContact();
contact.Phone = '01234 567890';
contact.AccountId = a.Id;
insert contact;

//Second contact
contact1 = Test_Factory.CreateContact();
contact1.Phone = '01234 123456';
contact1.AccountId = a.Id;
insert contact1;

///Create second account and third contact
a1 = Test_Factory.CreateAccount();
insert a1;
//Third contact
contact2 = Test_Factory.CreateContact();
contact2.Phone = '01234 888888';
contact2.AccountId = a1.Id;
insert contact2;

List<Campaign_Call_Status__c> ccsToInsert = new List<Campaign_Call_Status__c>();
Campaign_Call_Status__c ccs1 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Targeted', Responded__c = true, Default__c=true);
Campaign_Call_Status__c ccs2 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Sale Made', Responded__c = false, Default__c=false);
Campaign_Call_Status__c ccs3 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Opportunity Created', Responded__c = false, Default__c=false);
Campaign_Call_Status__c ccs4 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Unable to Contact', Responded__c = false, Default__c=false);
Campaign_Call_Status__c ccs5 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Callback', Responded__c = false, Default__c=false);
ccsToInsert.add(ccs1);
ccsToInsert.add(ccs2);
ccsToInsert.add(ccs3);
ccsToInsert.add(ccs4);
ccsToInsert.add(ccs5);
insert ccsToInsert;

//Insert Test
Date enddate = date.today() + 1;
c = new Campaign(Name='TestCampaign', Type='Telemarketing', Status='Planned', 
Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, isActive=true);
insert c;

c1 = new Campaign(Name='TestCampaign2', Type='Telemarketing', Status='Planned', 
Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, isActive=true);
insert c1;

cm = new CampaignMember(CampaignId=c.Id, ContactId=contact.Id, Unsuccessful_Call_Attempts__c = 1);
insert cm;

cm1 = new CampaignMember(CampaignId=c1.Id, ContactId=contact.Id, Status ='Targeted', Unsuccessful_Call_Attempts__c = 2);
insert cm1;

cm2 = new CampaignMember(CampaignId=c.Id, ContactId=contact1.Id, Unsuccessful_Call_Attempts__c = 1);
insert cm2;

cm3 = new CampaignMember(CampaignId=c1.Id, ContactId=contact2.Id, Unsuccessful_Call_Attempts__c = 2);
insert cm3;

//rt = [select id from RecordType where Id=:StaticVariables.campaign_task_record_type];
rt = [select id from RecordType where DeveloperName='Campaign_Task'];
System.assert(rt!= null);

//Create User and when Campaign Update check their X Day rule value is updated
Profile pProfile = [select id from profile where name='System Administrator'];
uUser = new User(alias = 'tst2', email='tst2@testemail.com', 
emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
localesidkey='en_US', profileid = pProfile.Id, timezonesidkey='Europe/London', username='tst2@testemail.com',
EIN__c='tst2');

insert uUser; 
//Data Setup - END
}

static testMethod void taskAfter1() {

//####### TEST 1 - Check none-campaign related Tasks with no whoid (contact) cause .adderror ######
Test.startTest();
Database.SaveResult sr;
//Should be allowed as not closed - even though no whoId
Task t1 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c.Id,Type='Call' );
sr = database.insert(t1, false);
StaticVariables.setTaskIsBeforeRun(false);
    
System.assert(sr.getId() != null);

//Should not be allowed - no who Id
Task t2 = new Task(RecordType = rt, Status = 'Completed', WhatId=c.Id, Type='Call', Call_Status__c='Sale Made');
sr = database.insert(t2, false);
System.assert(sr.getId() == null);
Test.stopTest();
}

static testMethod void taskAfter2() {

/*####### TEST 2 #########
* Check - related Tasks of the SAME Account have their Last Contact Date updated when another Task is completed successfully.
* Check - Account record last updated time is updated.
* Check - Campaign Member record updated - i.e. Status and X day block forumla. Unsuccessful call count reset.
*/
/*
//Data Setup
Task t1 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c.Id, WhoId=contact.Id, Type='Call', Call_Status__c=null);
insert t1; 
StaticVariables.setTaskIsBeforeRun(false);
    
Task t2 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c.Id, WhoId=contact1.Id, Type='Call', Call_Status__c=null);
insert t2;
StaticVariables.setTaskIsBeforeRun(false);
    
Task t3 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c1.Id, WhoId=contact.Id, Type='Call', Call_Status__c=null);
insert t3;
StaticVariables.setTaskIsBeforeRun(false);
    
Task t4 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c1.Id, WhoId=contact2.Id, Type='Call', Call_Status__c=null);
insert t4;
StaticVariables.setTaskIsBeforeRun(false);
    

//##### Pre-checks #####
t1 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t1.Id];
t2 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t2.Id];
t3 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t3.Id];
t4 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t4.Id];
System.assert(t1.Assigned_User_Id__c == null && t1.Assigned_User_Datetime__c == null 
&& t1.Last_Account_Call_Date__c == null && t1.Call_Succeeded__c == 'FALSE');
System.assert(t2.Assigned_User_Id__c == null && t2.Assigned_User_Datetime__c == null 
&& t2.Last_Account_Call_Date__c == null && t2.Call_Succeeded__c == 'FALSE');
System.assert(t3.Assigned_User_Id__c == null && t3.Assigned_User_Datetime__c == null 
&& t3.Last_Account_Call_Date__c == null && t3.Call_Succeeded__c == 'FALSE');
System.assert(t4.Assigned_User_Id__c == null && t4.Assigned_User_Datetime__c == null 
&& t4.Last_Account_Call_Date__c == null && t4.Call_Succeeded__c == 'FALSE');

//###Check account records###
Account acc = [select Id, Last_Account_Call_Date__c from Account where Id =: a.Id];
Account acc1 = [select Id, Last_Account_Call_Date__c from Account where Id =: a1.Id];
//System.assert(acc.Last_Account_Call_Date__c == null);
//System.assert(acc1.Last_Account_Call_Date__c == null);

//###Check Campaign Member records###

cm = [select Id, Status, X_Day_Blocked_Formula__c from CampaignMember where Id=:cm.Id];
//System.assert(cm.X_Day_Blocked_Formula__c == 0);
cm1 = [select Id, Status, Unsuccessful_Call_Attempts__c, X_Day_Blocked_Formula__c from CampaignMember where Id=:cm1.Id];
System.assert(cm1.status == 'Targeted'); 
System.assert(cm1.Unsuccessful_Call_Attempts__c == 2); 
//System.assert(cm1.X_Day_Blocked_Formula__c == 0);
cm2 = [select Id, Status, Unsuccessful_Call_Attempts__c, X_Day_Blocked_Formula__c from CampaignMember where Id=:cm2.Id];
System.assert(cm2.status == 'Targeted'); 
System.assert(cm2.Unsuccessful_Call_Attempts__c == 1); 
//System.assert(cm2.X_Day_Blocked_Formula__c == 0);
cm3 = [select Id, Status, Unsuccessful_Call_Attempts__c, X_Day_Blocked_Formula__c from CampaignMember where Id=:cm3.Id];
System.assert(cm3.status == 'Targeted'); 
System.assert(cm3.Unsuccessful_Call_Attempts__c == 2); 
//System.assert(cm3.X_Day_Blocked_Formula__c == 0);


Task taskt3Update = [select Id, Call_Status__c, Description from Task where Id =: t3.Id];
Test.startTest();
//Now complete a task - (i.e. a 'successful' call is made to a contact)
taskt3Update.Description = 'blaaa';
taskt3Update.Call_Status__c = 'Sale Made';
update taskt3Update;

Test.stopTest();

//####### Post-Checks #######
t1 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t1.Id];
t2 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t2.Id];
t3 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t3.Id];
t4 = [select Id, Assigned_User_Id__c, Assigned_User_Datetime__c, Last_Account_Call_Date__c, Call_Succeeded__c from Task where Id =: t4.Id];
System.assert(t1.Assigned_User_Id__c == null && t1.Assigned_User_Datetime__c == null 
&& t1.Last_Account_Call_Date__c == date.today() && t1.Call_Succeeded__c == 'FALSE');
System.assert(t2.Assigned_User_Id__c == null && t2.Assigned_User_Datetime__c == null 
&& t2.Last_Account_Call_Date__c == date.today() && t2.Call_Succeeded__c == 'FALSE');

//Event though this is the Task that has been completed we don't update Last Call Date as there is no need.
//The Task is closed (as Completed) so should not appear in peoples Open Task lists.
System.assert(t3.Assigned_User_Id__c == null && t3.Assigned_User_Datetime__c == null 
&& t3.Last_Account_Call_Date__c == null && t3.Call_Succeeded__c == 'TRUE');
//Should still have no last contact date. As different Account to completed call.
System.assert(t4.Assigned_User_Id__c == null && t4.Assigned_User_Datetime__c == null 
&& t4.Last_Account_Call_Date__c == null && t4.Call_Succeeded__c == 'FALSE');

//###Check account records###
acc = [select Id, Last_Account_Call_Date__c from Account where Id =: a.Id];
acc1 = [select Id, Last_Account_Call_Date__c from Account where Id =: a1.Id];
System.assert(acc.Last_Account_Call_Date__c == date.today());
//System.assert(acc1.Last_Account_Call_Date__c == null);

//###Check Campaign Member records###
//The CM should have his call status updated (Campaign the same as the completed Task/Call)
cm1 = [select Id, Status, Unsuccessful_Call_Attempts__c, X_Day_Blocked_Formula__c from CampaignMember where Id=:cm1.Id];
System.debug('--test2-->' + cm1.status + '_' + cm1.Unsuccessful_Call_Attempts__c + '_' + cm1.X_Day_Blocked_Formula__c);
System.assertEquals(cm1.status, 'Sale Made');
System.assertEquals(cm1.Unsuccessful_Call_Attempts__c, 0);
//System.assertEquals(cm1.X_Day_Blocked_Formula__c, 0);
cm = [select Id, Status, X_Day_Blocked_Formula__c from CampaignMember where Id=:cm.Id];
//The SAME Contact (with same account - obviously) but different Campaign should be blocked by X day rule
System.assert(cm.X_Day_Blocked_Formula__c == 1);
cm2 = [select Id, Status, Unsuccessful_Call_Attempts__c, X_Day_Blocked_Formula__c from CampaignMember where Id=:cm2.Id];
//This SECOND contact should be blocked as has the SAME acccount as previous Contact
System.assert(cm2.status == 'Targeted' && cm2.Unsuccessful_Call_Attempts__c == 1 && cm2.X_Day_Blocked_Formula__c == 1);
//This THIRD CM should NOT be blocked as has DIFFERENT acccount as previous Contact where successful call made.
System.assert(cm3.status == 'Targeted' && cm3.Unsuccessful_Call_Attempts__c == 2 && cm3.X_Day_Blocked_Formula__c == 0);
}

static testMethod void taskAfter3() {
//###Test 3 - Check Create Opportunity from Task ###
List<Opportunity> opps = [select Id from Opportunity where AccountId=:a.Id AND CampaignId=:c.Id];
System.assert(opps.isEmpty());
Test.startTest();
Task t1 = new Task(RecordType = rt, WhatId=c.Id, WhoId=contact.Id, Subject='Call', Type='Call', Call_Status__c='Opportunity Created', Description='blaaa');
insert t1;
Test.stopTest();
opps = [select Id from Opportunity where AccountId=:a.Id AND CampaignId=:c.Id];
System.assert(!opps.isEmpty() && opps.size() == 1);
}

static testMethod void tastAfter4(){
//###Test 4 CORP/DBAM - We do not set to do not call after 3 x failed attempts. (i.e. call outcome 'Unable to Contact at this time'))###
a.Sector_Code__c = 'CORP'; 
update a;
cm.Unsuccessful_Call_Attempts__c = 1;
update cm;
StaticVariables.setTaskIsBeforeRun(false);

Task t1 = new Task(RecordType = rt, WhatId=c.Id, WhoId=contact.Id, Type='Call', Call_Status__c=null);
insert t1;

//Pre-checks
List<Task> tasks = [select Id from Task where WhatId=:c.Id AND WhoId=:contact.Id AND isClosed = false];
System.assert(tasks.size() == 1);

CampaignMember test5CM = [select Id, Status, Unsuccessful_Call_Attempts__c, ContactId from CampaignMember where Id =:cm.Id];
System.assert(test5CM.status == 'Targeted');

// SECOND failed call attempt (i.e. 2 x attempts)
t1 = [select Id, Call_Status__c, Description from Task where Id =:t1.Id];
t1.Description = 'blaaaa';
t1.Call_Status__c='Unable to Contact at this time';
StaticVariables.setTaskIsBeforeRun(false);

update t1;

//Auto creation of Task for CORP/DBAM
tasks = [select Id,Call_Status__c, Description from Task where WhatId=:c.Id AND WhoId=:contact.Id AND isClosed = false];
System.assert(tasks.size() == 1);
test5CM = [select Id, Status, Unsuccessful_Call_Attempts__c from CampaignMember where Id =:cm.Id];
System.assert(test5CM.status == 'Callback');
System.assert(test5CM.Unsuccessful_Call_Attempts__c == 2);

// FINAL failed call attempt (i.e. 3 x attempts) - do not block the CM and do not reset unsuccesful call counter
StaticVariables.setTaskIsRun(false);
tasks[0].Call_Status__c='Unable to Contact at this time';
tasks[0].Description='blaaa';
StaticVariables.setTaskIsBeforeRun(false);

update tasks[0];
test5CM = [select Id, Status, Unsuccessful_Call_Attempts__c from CampaignMember where Id =:cm.Id];
system.debug('gs:cm'+test5CM);
System.assert(test5CM.status == 'Callback');
System.assertequals(test5CM.Unsuccessful_Call_Attempts__c,3);
}

static testMethod void taskAfter5(){
//###Test 5 - BTLB - Call outcome is a 'Unable to Contact at this time' AND account is BTLB (i.e. we set do nto call after 3 x failed attempts)###
a.Sector_Code__c = 'BTLB'; 
update a;
cm.Unsuccessful_Call_Attempts__c = 1;
update cm;

Task t1 = new Task(RecordType = rt, WhatId=c.Id, WhoId=contact.Id, Type='Call', Call_Status__c=null);
StaticVariables.setTaskIsBeforeRun(false);

insert t1;

//Pre-checks
List<Task> tasks = [select Id from Task where WhatId=:c.Id AND WhoId=:contact.Id AND isClosed = false];
System.assert(tasks.size() == 1);

CampaignMember test5CM = [select Id, Status, Unsuccessful_Call_Attempts__c from CampaignMember where Id =:cm.Id];
System.assert(test5CM.status == 'Targeted');

// SECOND failed call attempt (i.e. 2 x attempts)
StaticVariables.setTaskIsRun(false);
t1 = [select Id, Call_Status__c, Description from Task where Id =:t1.Id];
t1.Description = 'blaaa';
t1.Call_Status__c='Unable to Contact at this time';
StaticVariables.setTaskIsBeforeRun(false);

update t1;
t1 = [select Id, Call_Status__c, Call_Succeeded__c from Task where Id =:t1.Id];

System.assert(t1.Call_Succeeded__c == 'CALLBACK');

//No auto creation of Task for BTLB
tasks = [select Id from Task where WhatId=:c.Id AND WhoId=:contact.Id AND isClosed = false];
System.assert(tasks.size() == 0);
test5CM = [select Id, Status, Unsuccessful_Call_Attempts__c from CampaignMember where Id =:cm.Id];

System.assert(test5CM.status == 'Callback');
System.assert(test5CM.Unsuccessful_Call_Attempts__c == 2);

// FINAL failed call attempt (i.e. 3 x attempts)
StaticVariables.setTaskIsRun(false);
t1 = new Task(RecordType = rt, WhatId=c.Id, WhoId=contact.Id, Subject='Call', Type='Call', Call_Status__c='Unable to Contact at this time', Description='blaaa');
StaticVariables.setTaskIsBeforeRun(false);

insert t1;
test5CM = [select Id, Status, Unsuccessful_Call_Attempts__c from CampaignMember where Id =:cm.Id];
System.assert(test5CM.status == 'Unable to Contact');
//Reset to 0
System.assert(test5CM.Unsuccessful_Call_Attempts__c == 0);
}
**/
}