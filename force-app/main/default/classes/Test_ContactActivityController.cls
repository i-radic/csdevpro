@isTest

private class Test_ContactActivityController{
    static testMethod void runPositiveTestCases() {
        Id userId = UserInfo.getUserId();
        Account a = Test_Factory.CreateAccount();
        a.Name = 'Test';
        a.ownerId = userId ;
        a.Base_Team_Assign_Date__c=System.today();
        insert a;
        
        //Create Contact
        Contact contact1 = new Contact();
        contact1.Cug__c = 'test1Cug';
        contact1.Phone = '000111222333';
        contact1.Email = 'test.Alan@test.com';
        contact1.Contact_Post_Code__c = 'WR5 3RL';
        contact1.AccountId=a.Id;
        contact1.LastName = 'Test';
        
        insert contact1;
        
        //PageReference pageRef = Page.New_MAC_Transfer_Page2;
        //Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('ID',contact1.Id);
        
        ApexPages.StandardController stdcon = new ApexPages.StandardController(contact1);
       
        ContactActivityController ctr = new ContactActivityController(stdcon);
        ctr.conID=contact1.Id;
        List<Event> ev=ctr.getAccEventHist();
        List<Task> tk=ctr.getAccTaskHist();
        //Test_Factory.SetProperty('IsTest', 'yes');
        //Contact contact = new Contact();  
        //contact.Contact_Address_Street__c = '';
        //contact.Address_County__c = '';
        //contact.Contact_Post_Code__c = '';
        //contact.Address_Country__c = '';
        //contact.Contact_Post_Town__c = '';                
        //contact.Contact_Address_Street__c = '';
        //contact.Address_County__c = '';
        //contact.Contact_Post_Code__c = '';
        //contact.Address_Country__c = '';
        //contact.Contact_Post_Town__c = '';       
        //contact.Active__c = '';
        //contact.Comment__c = '';
        //contact.Type__c = '';
        //contact.Salutation = '';
        //contact.FirstName = '';
        //contact.MiddleName__c = '';
        //contact.LastName = '';
        //contact.AliasName__c = '';
        //contact.ContactSource__c = '';
        //contact.JobSeniority__c = '';
        //contact.Profession__c = '';
        //contact.Birthdate = null;
        //contact.MotherMaidenName__c = '';
        //contact.Status__c = '';
        //contact.Job_Title__c = '';
        //contact.Key_Decision_Maker__c = '';
        //contact.Fax = '';
        //contact.Organization__c = '';
        //contact.Department = '';
        //contact.Preferred_Contact_Channel__c = '';
        //contact.Secondary_Preferred_Contact_Channel__c = '';
        //contact.Email_Id__c = '';       
        //contact.Email_Id__c = '';
        //contact.Email = '';
        //contact.Email_Consent__c = '';
        //contact.Email_Consent_Date__c = null;
        //contact.Mobile_Id__c = '';
        //contact.MobilePhone = '';
        //contact.Mobile_Consent__c = '';
        //contact.Mobile_Consent_Date__c = null;    
        //contact.Phone_Id__c = '';
        //contact.Phone = '3';
        //contact.Phone_Consent__c = '';
        //contact.Phone_Consent_Date__c = null;
        //contact.Fax_Id__c = '';
        //contact.Fax = '';
        //contact.Fax_Consent__c = '';
        //contact.Fax_Consent_Date__c = null;
        //contact.Contact_Address_Number__c = '';
        //contact.Address_Id__c = '';
        //contact.Contact_Address_Street__c = '';
        //contact.Contact_Locality__c = '';
        //contact.Contact_Locality__c = '';
        //contact.Address_County__c = '';
        //contact.Address_Country__c = '';
        //contact.Contact_Post_Code__c = '';
        //contact.Contact_Post_Town__c = '';
        //contact.Contact_Sector__c = '';
        //contact.Address_Name__c = '';
        //contact.Contact_Zip_Code__c = '';
        //contact.Address_Consent__c = '';
        //contact.Address_Consent_Date__c = null;          
        //updateContact updateCont = new updateContact(contact);
        //updateCont.crmContactTest(); 
        //updateCont.OnLoad();
        //updateCont.save(); 
        //updateCont.cancel();      
        //updateCont.AddressSearch();   
    }
}