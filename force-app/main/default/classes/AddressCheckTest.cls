@isTest
private class AddressCheckTest {
	@isTest static void test_method_one() {
    	CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    	Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test basket', testOpp);
        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinitionResign(true, 'BT Mobile', true);
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);
		prodConfig.cscfga__Product_Definition__c = prodDef.Id;
		prodConfig.Calculations_Product_Group__c = 'Future Mobile';
		INSERT prodConfig;

		String returnStr = AddressCheck.getAddresses(basket.Id, 'test', 'test', 'test', 'test', 'test', 'test');
		System.assertNotEquals(returnStr, null);
	}
}