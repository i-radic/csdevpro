public with sharing class CS_Download_Baskets {

	public set<string> selectedBaskets;
	public string csvFile {get; set;}
	public string oppId;
	public list<Schema.FieldSetMember> basketFields;
	public CS_Download_Baskets(){
		basketFields = new list<Schema.FieldSetMember>();
        if(apexPages.currentPage().getParameters().get('oppId') != null){
	       	oppId = apexPages.currentPage().getParameters().get('oppId');
        }
	}
	public pageReference download(){
		string basketqry = 'select ';
		basketFields = sObjectType.cscfga__Product_Basket__c.FieldSets.cs_Fields_displayed_on_Basket_Compare.getFields();
        if(!basketFields.isEmpty()){
        	for(Schema.FieldSetMember fs : basketFields){
        		basketqry = basketqry + fs.getFieldPath() + ',';
        	}
        	
        	basketqry = basketqry + 'id,name from cscfga__Product_Basket__c where id=:selectedBaskets';
        	if(apexPages.currentPage().getParameters().get('selectedBasket') != null){
	        	string selectedBasket = apexPages.currentPage().getParameters().get('selectedBasket');
	        	system.debug('selectedBasket'+selectedBasket);
	        	selectedBaskets = new set<string>();
	        	selectedBasket = selectedBasket.subString(1,selectedBasket.length());
	        	
	        	system.debug('selectedBasket>>>'+selectedBasket);
	        	
	        	for(string s : selectedBasket.split(',')){
	        		system.debug('s>>'+s);
	        		s = s.trim();
	        		if(s.length() > 18){
	        			selectedBaskets.add(s.subString(0,18));
	        		}else{
	        			system.debug('s>>>1'+s);
	        			if(s.contains(']')){
	        				s = s.replace(']','');
	        				s = s.trim();
	        			}
	        			system.debug('s>>>2'+s);
	        			selectedBaskets.add(s);
	        		}
	        	}
	        }
	        system.debug('selectedBaskets'+selectedBaskets);
	        
        	list<cscfga__Product_Basket__c> baskets = new list<cscfga__Product_Basket__c>();
			baskets = database.query(basketqry);
        	csvFile = prepareBaskets(baskets);
        }
		return null;
	}
	
	public string prepareBaskets(list<cscfga__Product_Basket__c> basketsToCompare){
    	string columnNames = 'Name';
    	for(Schema.FieldSetMember fs : basketFields){
    			columnNames = columnNames + ','+ fs.getLabel();
    	}
    	
    	string rowInfo = ' ';
    	
    	for(cscfga__Product_Basket__c b : basketsToCompare){
    		rowInfo = rowInfo + '\n' + b.name;
    		for(Schema.FieldSetMember fs : basketFields){
    			rowInfo = rowInfo + ',' +checkNull(string.valueOf(b.get(fs.getFieldPath())));
    		}
    	}
    	string details = columnNames + rowInfo;
    	return details;
    }
    
    public string checkNull(string ip){
    	string ret = '';
    	if(ip != null && ip != ''){
    		if(ip.contains(',')){
    			ip = ip.replace(',',' ');
    		}
    		return ip;
    	}else{
    		return ret;
    	}
    }

}