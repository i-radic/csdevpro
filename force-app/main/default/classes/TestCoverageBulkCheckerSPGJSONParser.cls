@isTest
public class TestCoverageBulkCheckerSPGJSONParser {
      static testMethod void testparse() {
         
         String json = '{\"coverageResource\":'+
        '{\"links\":{\"rel\":\"self\",\"href\":\"\\/iif\\/coverage-v1\\/locations\\/coverage.json\\/LS10+1LJ\",\"method\":\"get\"},'+
        '\"locations\":'+
        '{\"links\":'+
        '{\"rel\":\"self\",\"href\":\"\\/iif\\/coverage-v1\\/locations/coverage.json/LS10+1LJ\",\"method\":\"get\"},'+
        '\"title\":\"LS101LJ\",\"lat\":53.79088,\"lng\":-1.5338569,'+
        ''+
        '\"coverage\":[{\"type\":\"2G\",\"strength\":5},'+
        '{\"type\":\"3G\",\"strength\":5},'+
        '{\"type\":\"4G\",\"strength\":4,\"comingsoon\":true},'+
        '{\"type\":\"ee_single_flat\",\"strength\":0}]},\"lastUpdated\":\"23 February 2015 12:00 AM\"}}';
          CoverageBulkCheckerSPGJSONParser obj = CoverageBulkCheckerSPGJSONParser.parse(json);
          List<CoverageBulkCheckerSPGJSONParser.Coverage> covList = obj.coverageResource.locations.coverage;
          CoverageBulkCheckerSPGJSONParser.Coverage cov = new CoverageBulkCheckerSPGJSONParser.Coverage();
          CoverageBulkCheckerSPGJSONParser.Links links = new CoverageBulkCheckerSPGJSONParser.Links();
          CoverageBulkCheckerSPGJSONParser.CoverageResource covResource= new CoverageBulkCheckerSPGJSONParser.CoverageResource ();
          CoverageBulkCheckerSPGJSONParser.Locations loc= new CoverageBulkCheckerSPGJSONParser.Locations ();
          CoverageBulkCheckerSPGJSONParser ccJSONParser   = new CoverageBulkCheckerSPGJSONParser();
          System.assertEquals(4,  covList.size());
          System.assertEquals('2G',  covList[0].Type);
          //obj.Coverage = obj.Coverage();
          System.assert(obj != null);       
      }
}