@isTest
public class CS_OEDataHandlerTest {

	private static Map<String, Object> createTestData(String name){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
		Map<String, Object> inputMap = new Map<String, Object>();
		inputMap.put('name', name);
		
		Account testAcc = new Account();
        testAcc.Name = 'TestBasket';
        testAcc.NumberOfEmployees = 10;
        insert testAcc;
        
        // create opportunity
        Opportunity testOpp = new Opportunity();    
        testOpp.Name = 'Test basket compare opp: ' + testAcc.Name + ':'+datetime.now();
        testOpp.AccountId = testAcc.Id;
        testOpp.CloseDate = System.today();
        testOpp.StageName = 'Closed Won';   //@TODO - This should be a configuration
        testOpp.TotalOpportunityQuantity = 0; // added this as validation rule was complaining it was missing
        insert testOpp;

		cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c(
            Name = 'BTOP Subscription Level Add-On',
            cscfga__Description__c = 'PD1 Desc'
        );
        insert pd1;

        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
            Name = 'BT Mobile Extras',
            cscfga__Description__c = 'PD1 Desc'
        );
        insert pd2;

		Basket_Totals__c bt = new Basket_Totals__c(
			Name = 'bt1'
		);
		insert bt;

		cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
			Name = 'Basket',
			Basket_Totals__c = bt.Id,
			cscfga__Opportunity__c = testOpp.Id
		);
		insert pb;

		cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
			Name = 'PC1 - Extra Product Type',
			cscfga__Product_Definition__c = pd1.Id,
			cscfga__Product_Basket__c = pb.Id,
			Volume__c = 2,
			cscfga__Configuration_Status__c = 'Valid'
		);
		insert pc1;

		cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
			Name = 'PC2 - BT Mobile Extra',
			cscfga__Product_Definition__c = pd2.Id,
			cscfga__Product_Basket__c = pb.Id,
			Volume__c = 2,
			cscfga__Configuration_Status__c = 'Valid'
		);
		insert pc2;

		BT_Extra_Product_Type__c t1 = new BT_Extra_Product_Type__c();
		t1.Name = 'Call Recording';
		t1.Extension_type__c = 'Subscriber extension';
		insert t1;

		BT_Extra_Product_Type__c t2 = new BT_Extra_Product_Type__c();
		t2.Name = 'Data Extras';
		t2.Extension_type__c = 'Subscriber extension';
		insert t2;

		BT_Extra_Product_Type__c t3 = new BT_Extra_Product_Type__c();
		t3.Name = 'Roaming Extras';
		t3.Extension_type__c = 'Subscriber extension';
		insert t3;

		List<cspmb__Price_Item__c> pis = new List<cspmb__Price_Item__c>();
		pis.add(new cspmb__Price_Item__c( Name = 'PC1 - Extra Product Type', Product_Type__c = t1.Id ));
		pis.add(new cspmb__Price_Item__c( Name = 'PC1 - Extra Product Type', Product_Type__c = t2.Id ));
		pis.add(new cspmb__Price_Item__c( Name = 'PC1 - Extra Product Type', Product_Type__c = t3.Id ));
		insert pis;


		inputMap.put('configId', pc1.Id);

		return inputMap;
	}

	private static testMethod void testBTMobile(){
		Test.startTest();
		CS_OEDataHandler cls = new CS_OEDataHandler();
		cls.execute(createTestData('BTMobile'));
		boolean checker = CS_OEDataHandler.hasSObjectField('Name', new cspmb__Price_Item__c());
		Test.stopTest();
	}
	private static testMethod void testBTMobileRelated(){
		Test.startTest();
		CS_OEDataHandler cls = new CS_OEDataHandler();
		cls.execute(createTestData('BTMobileRelated'));
		Test.stopTest();
	}
	private static testMethod void testBTOP(){
		Test.startTest();
		CS_OEDataHandler cls = new CS_OEDataHandler();
		cls.execute(createTestData('BTOP'));
		Test.stopTest();
	}
}