@isTest
private class accountView_Test {

    static testMethod void runPositiveTestCases() {
        
       Test_Factory.SetProperty('IsTest', 'yes');
       User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
             
        Account testAcc = Test_Factory.CreateAccount();
        //testAcc.AM_ein__c = u1.EIN__c;
       //testAcc.Sub_Sector__c = 'true';
        testAcc.OwnerId = thisUser.Id; 
        testAcc.CUG__c='CugTest';
        insert testAcc;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(testAcc);
        accountView accView = new accountView(stdController);
        test.startTest();
        accView.onStart();
        test.stopTest();
        
                        }                    

    
}