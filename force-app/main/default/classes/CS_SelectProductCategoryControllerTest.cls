@IsTest
public class CS_SelectProductCategoryControllerTest {

    public static Id basketTestId { get; set; }
    public static Id categoryTestId { get; set; }

    @IsTest
    public static void initTest() {
        setupTestData();
        
        CS_SelectProductCategoryController controller = new CS_SelectProductCategoryController();

        controller.basketId = basketTestId;
        controller.paramRetURL = '#';
        controller.containerType = 'basket';
        String str = controller.buttonClass;
        str = controller.cancelURL;
        str = controller.buttonStyle;
        str = controller.selcat_Cancel;
        PageReference pr = controller.onGoBack();
        pr = controller.onCancel();
        /*
    public string buttonClass { get { return 'btn'; } }
    public string cancelURL { 
    public string buttonStyle { get { return 'padding: 4px 3px; text-decoration: none'; } }
    public string selcat_Cancel { get { return 'Cancel'; } }
    public PageReference onGoBack() {
    public PageReference onCancel() {
        */
        test.startTest();
        
        controller.init();
        
        test.stopTest();
        
        System.assertEquals(3, controller.categoriesMap.size(), 'Invalid number of categories');
        System.assertEquals(3, controller.productsMap.size(), 'Invalid number of prodcuts');
        System.assertEquals(3, controller.offersMap.size(), 'Invalid number of offers');
    }

	@IsTest
    public static void getProductsByIdTest() {
        setupTestData();
        
        CS_SelectProductCategoryController controller = new CS_SelectProductCategoryController();

        controller.session = cscfga.SessionManager.getSessionInfo('', new cscfga__Product_Basket__c(id=basketTestId));
        controller.basketId = basketTestId;
        controller.paramRetURL = '#';
        controller.containerType = 'basket';

        test.startTest();
        
        controller.InitProducts(categoryTestId);
        
        test.stopTest();
        
        System.assertEquals(4, controller.productsMap.size(), 'Invalid number of prodcuts');
    }

	@IsTest
    public static void getOffersByIdTest() {
        setupTestData();
        
        CS_SelectProductCategoryController controller = new CS_SelectProductCategoryController();

        controller.session = cscfga.SessionManager.getSessionInfo('', new cscfga__Product_Basket__c(id=basketTestId));
        controller.basketId = basketTestId;
        controller.paramRetURL = '#';
        controller.containerType = 'basket';

        test.startTest();
        
        controller.InitOffers(categoryTestId);
        
        test.stopTest();
        
        System.assertEquals(4, controller.offersMap.size(), 'Invalid number of offers');
    }
    
    @IsTest
    public static void onGoToConfiguratorTest() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        cscfga__Product_Category__c category = new cscfga__Product_Category__c(Name='Category', cscfga__Browseable__c = true, cscfga__Active__c = true);
        cscfga__Product_Definition__c definition = new cscfga__Product_Definition__c(Name='Product', cscfga__Product_Category__c = category.Id, cscfga__Description__c = 'Product description');
                                                            
        insert category;
        insert definition;

        
        CS_SelectProductCategoryController controllerFirst = new CS_SelectProductCategoryController();

        controllerFirst.categoryId = category.Id;
        controllerFirst.definitionId = definition.Id;
        controllerFirst.containerType = 'basket';

        CS_SelectProductCategoryController controllerSecond = new CS_SelectProductCategoryController();

        controllerSecond.definitionId = definition.Id;
        controllerSecond.containerType = 'basket';

        test.startTest();
        
        controllerFirst.onGoToConfigurator();
        controllerSecond.onGoToConfigurator();
        
        test.stopTest();
        
        System.assertEquals('/apex/cscfga__ConfigureProduct?categoryId=' + category.Id + '&containerType=basket&definitionId=' + definition.Id + '&linkedId=%2F' + basketTestId + '&retURL=%2F' + basketTestId, 
                                controllerFirst.redirectTo, 'Invalid URL');
        System.assertEquals('/apex/cscfga__ConfigureProduct?containerType=basket&definitionId=' + definition.Id + '&linkedId=%2F' + basketTestId + '&retURL=%2F' + basketTestId, 
                                controllerSecond.redirectTo, 'Invalid URL');
    }
    
    @IsTest
    public static void onSelectProductCategoryTest() {
        setupTestData();
        
        CS_SelectProductCategoryController controller = new CS_SelectProductCategoryController();

        controller.session = cscfga.SessionManager.getSessionInfo('', new cscfga__Product_Basket__c(id=basketTestId));
        controller.basketId = basketTestId;
        controller.paramRetURL = '#';
        controller.containerType = 'basket';
        controller.categoryId = categoryTestId;

        test.startTest();
        
        controller.init();
        controller.onSelectProductCategory();
        
        test.stopTest();
        
        System.assertEquals(0, controller.categoriesMap.size(), 'Invalid data');
        System.assertEquals(0, controller.numCategories, 'Invalid data');
        System.assertEquals(4, controller.productsMap.size(), 'Invalid number of prodcuts');
        System.assertEquals(4, controller.offersMap.size(), 'Invalid number of offers');
    }

    private static void setupTestData() {
	    CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
        List<cscfga__Product_Category__c> categories = new List<cscfga__Product_Category__c>();
        List<cscfga__Product_Definition__c> products = new List<cscfga__Product_Definition__c>();
        List<cscfga__Configuration_Offer__c> offers = new List<cscfga__Configuration_Offer__c>();
        List<cscfga__Product_Definition__c> packages = new List<cscfga__Product_Definition__c>();
        
        List<cscfga__Offer_Category_Association__c> associations = new List<cscfga__Offer_Category_Association__c>();
        List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();
        
        Id CategoryId = null;

        Account acct = new Account(name='test account');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId=acct.Id,
                                            StageName='Prospecting',
                                            CloseDate=date.today(),
                                            NextStep='Test',
                                            Next_Action_Date__c=date.today(),name='test OpportunityCI',
                                            TotalOpportunityQuantity = 0);
        insert opp;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
            Name = 'Basket X',
            cscfga__Opportunity__c = opp.Id
        );
        
        insert basket;

        for (integer i = 0; i<3; i++) {
            categories.add(new cscfga__Product_Category__c(Name='Category__' + i, cscfga__Browseable__c = true, cscfga__Active__c = true));
        }
        
        insert categories;

        /*for (integer i = 0; i<3; i++) {
            productDefinitions.add(new cscfga__Product_Definition__c(Name = 'ProductDefinition__' + i, cscfga__Description__c = 'Product description ' + i, cscfga__Product_Category__c = categories[i].Id));
        }

        insert productDefinitions;*/
        
        for (integer i = 0; i<7; i++) {
            CategoryId = math.mod(i, 2) == 0 ? categories[0].Id : null;
            products.add(new cscfga__Product_Definition__c(Name='Product__' + i, 
                                                            cscfga__Product_Category__c = CategoryId, 
                                                            cscfga__Description__c = 'Product description ' + i, 
                                                            cscfga__Active__c = true, 
                                                            cscfga__isArchived__c = false, 
                                                            cscfga__is_shared_context_definition__c = false, 
                                                            cscfga__restrict_access__c = false));
        }
        
        insert products;
        
        for (integer i = 0; i<7; i++) {
            productConfigurations.add(new cscfga__Product_Configuration__c(Name='ProductDefinition__' + i, cscfga__Product_Basket__c = basket.Id, cscfga__Product_Definition__c = products[0].Id));
        }
        
        insert productConfigurations;
        
        for (integer i = 0; i<7; i++) {
            offers.add(new cscfga__Configuration_Offer__c(Name='Offer__' + i, 
                                                            cscfga__Description__c = 'Offer description ' + i,
                                                            cscfga__Active__c = true));
        }
        
        insert offers;
        
        for (integer i = 0; i<7; i++) {
            if (math.mod(i, 2) == 0) {
                cscfga__Offer_Category_Association__c association = new cscfga__Offer_Category_Association__c(cscfga__Configuration_Offer__c = offers[i].Id, cscfga__Product_Category__c = categories[0].Id);
                associations.add(association);
            }
        }
        
        insert associations;

        /*for (integer i = 0; i<7; i++) {
            CategoryId = math.mod(i, 2) == 0 ? categories[0].Id : null;
            packages.add(new cscfga__Product_Definition__c(Name='Package__' + i, 
                                                            cscfga__Product_Category__c = CategoryId, 
                                                            cscfga__Description__c = 'Package description ' + i, 
                                                            cscfga__Active__c = true,
                                                            cscfga__isArchived__c = false, 
                                                            cscfga__is_shared_context_definition__c = false, 
                                                            cscfga__restrict_access__c = false));
        }
        
        insert packages;*/
        
        basketTestId = basket.Id;
        categoryTestId = categories[0].Id;
    }
}