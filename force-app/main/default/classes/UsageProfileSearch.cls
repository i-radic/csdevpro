public class UsageProfileSearch {

    public static ApexPages.StandardController Gcontroller;
	public Opportunity thisRec {get;set;}

	public UsageProfileSearch() { }
	public UsageProfileSearch(ApexPages.StandardController controller) {
        Gcontroller = controller;
        /*if(!Test.isRunningTest()) {
			Gcontroller.addFields(new List<String>{'AccountId'}); 
		}*/
        this.thisRec= (Opportunity)Gcontroller.getRecord(); 
	}

	public String searchEOL{get; set;}
	public String textEOL{get; set;}
	public String recordEOL{get; set;}
	public String resultEOLText{get; set;}
	public String oppAccountId{get; set;}
	
	public Boolean resultEOL{get; set;}
	public Boolean linkedEOL{get; set;}
	
	public List<Usage_Profile__c> getEOLlist(){
		List<Usage_Profile__c> usageRecs2 = [SELECT Id, Account__c, Account__r.Name, EOLAccountName__c,Account__r.Id,  Usage_Profile_External_ID__c,EOLDateRun__c,EOLDateRange__c,EOLAgentName__c,EOLAgentEIN__c FROM Usage_Profile__c WHERE Account__c = :thisRec.AccountId ORDER BY CreatedDate Desc];
		return usageRecs2;
	}
	public Void searchEOL(){         
		List<Usage_Profile__c> usageRec = [SELECT Id, Account__c, Account__r.Name, EOLAccountName__c,Account__r.Id,  Usage_Profile_External_ID__c,EOLDateRun__c,EOLDateRange__c,EOLAgentName__c,EOLAgentEIN__c FROM Usage_Profile__c WHERE Usage_Profile_External_ID__c = :searchEOL LIMIT 1];
		resultEOL = false;
		textEOL = '<h2 style="color:#e8342c;">NOT FOUND<br/></h2>EOL Id <strong>'+searchEOL+'</strong> has not been found. Please check and try again.';
		
		if(usageRec.size() > 0){
			resultEOL = true;
			recordEOL = usageRec[0].Id;
			textEOL = '<table>';
			textEOL += '<tr><th>EOL:</th><td>'+usageRec[0].Usage_Profile_External_ID__c+'</td></tr>';
			textEOL += '<tr><th>EOL Account:</th><td>'+usageRec[0].EOLAccountName__c+'</td></tr>';
			textEOL += '<tr><th>Date Run:</th><td>'+usageRec[0].EOLDateRun__c+'</td></tr>';
			textEOL += '<tr><th>Date Range:</th><td>'+usageRec[0].EOLDateRange__c+'</td></tr>';
			textEOL += '<tr><th>Requested By:</th><td>'+usageRec[0].EOLAgentName__c+' ('+usageRec[0].EOLAgentEIN__c+')</td></tr>';
			textEOL += '</table>';

			if(usageRec[0].Account__c != null){
				if(usageRec[0].Account__c == thisRec.AccountId){
					linkedEOL = true;
					textEOL = 'This EOL is already linked to this account.';
					resultEOLText = '';
				}else{
					linkedEOL = true;
					resultEOLText = 'This EOL is linked to a different account <a href="/'+usageRec[0].Account__r.Id+'" target="_new1">'+usageRec[0].Account__r.Name+'</a>';
				}
			}else{
				linkedEOL = false;
			}
		}
    }

	public pageReference confirmLink(){ 
		Usage_Profile__c usageRec = [SELECT Id, Account__c FROM Usage_Profile__c WHERE Id = :recordEOL];
		usageRec.Account__c = thisRec.AccountId;
		update usageRec;
		linkedEOL = true;
		resultEOLText = 'This has now been linked.';
		
		PageReference retPg = new PageReference('/apex/UsageProfileSearch?id='+thisRec.Id);
		return retpg.setRedirect(true);   
	}
}