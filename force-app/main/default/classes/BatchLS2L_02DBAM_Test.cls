@isTest
private class BatchLS2L_02DBAM_Test{

  static testMethod void myUnitTest() {
      Test.startTest();
      User thisUser = [select id from User where id=:userinfo.getUserid()];
		System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
     
     	upsert settings TriggerDeactivating__c.Id;
        }      
      
      Account retval = new Account(id = null);        
      retval.Name = 'TESTACCOUNTESTACCOUNTTESTACCOUNTTESTACCOUNTTESTACCOUNTTESTACCOUNT1';        
      retval.LOB_Code__c = 'tst_lob';                
      retval.SAC_Code__c = 't_sac';        
      retval.LE_Code__c = 'tst_le6789';        
      retval.AM_EIN__c = '605398106';        
      retval.Sector_code__c = 'CORP';
      retval.OwnerId=thisuser.id;       
      retval.Base_Team_Assign_Date__c=System.today();
      insert retval;
      Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',AccountId=retval.id,Email='test.mail@test.com');
      insert myContact;
      
      Landscape_DBAM__c ld=new Landscape_DBAM__c();
      ld.Account__c=retval.Id;
      ld.Calls_contract_end_date__c=System.today()+59;
      ld.Mobile_End_Date__c=System.today()+59;
      ld.WAN_End_Date__c=System.today()+200;
      ld.Lines_contract_end_date__c=System.today()+59; 
      insert ld;
      
      String query = 'Select Id,Account__r.Name,Account__r.Owner.Department,xLeads_Flag_Calls__c,xLeads_Flag_Lines__c,xLeads_Flag_Mobile__c,xLeads_Flag_WAN__c From Landscape_DBAM__c Limit 200';
      BatchLS2L_02DBAM b=new BatchLS2L_02DBAM(query);
      Database.executeBatch(b,200);
      
      
           
      Test.stopTest();
      
  
  }
  }