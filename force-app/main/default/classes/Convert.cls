public class Convert{
    public static DateTime ConvertToDateTime(Date dateToConvert) {
        return DateTime.newInstance(dateToConvert.year(), dateToConvert.month(), dateToConvert.day());
    } 
}