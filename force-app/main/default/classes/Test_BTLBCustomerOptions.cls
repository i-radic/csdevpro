@isTest
private class Test_BTLBCustomerOptions {
    
     static testMethod void runPositiveTestCases() {
    	 CustomerOptions__c testCustOpn = new CustomerOptions__c();
         testCustOpn.Telephone_Number__c = '0123456789';
         testCustOpn.Account_Number__c = 'AB12345678';
         Insert testCustOpn;
         BTLBCustomerOptions.getCustomerOptionsData(testCustOpn.id);
     }
    static testMethod void runNegativeTestCases() {
      BTLBCustomerOptions.getCustomerOptionsData('Test');  
    }
}