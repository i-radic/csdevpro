/**
* ProjectHandler.cls
*
* Handles functionality for ProjectTrigger.trigger
*
* @author Gary Marsh (westbrook)
* @date 2013-06-07
*/
public without sharing class ProjectHandler {
    
    private static ProjectHandler handler;
    private static final Set<String> TRACKED_STAGES;
    
    private static final String STAGE_UPDATE = Label.Project_Stage_Update;
    private static final String HOLD_UPDATE = Label.Project_On_Hold_Update;
    	
    public static final String RT_TPS = 'Technical_Pre_Sales';
    public static final String RT_TD = 'Technical_Delivery';
    	
    private static final Set<String> RECORD_TYPES = new Set<String> {
    	RT_TPS,
    	RT_TD
    };
    
    static {
        TRACKED_STAGES = new Set<String> {
            'Awaiting Assignment to Tech. Delivery',
            'Project Assigned',
            'Customer Welcome Complete',
            'System Commissioned & Tested',
            'Handed Over to Inlife',
            'SOR Approved by Customer',
            'SOR Commercially Approved'
        };
    }
    
    private ProjectHandler(){}
    
    public static ProjectHandler getHandler() {
        if(null==handler) { handler = new ProjectHandler(); }
        return handler;
    }
    
    public void afterUpdate() {
        updateChatterForOpportunity((Map<Id, Project__c>)Trigger.oldMap, (List<Project__c>)Trigger.new);
    }
    
    /**
    * #808 - Inserts new chatter items for related opportunity on project if the
    * project is put on hold or stage is changed to any of the tracked stages
    */
    private void updateChatterForOpportunity(Map<Id, Project__c> oldMap, List<Project__c> recs) {
        if (TriggerUtils.hasSkipReason('ProjectHandler.updateChatterForOpportunity')) { return; }
        		
        //load Record Type map
		final Map<Id, String> rtMap = new Map<Id, String>();
		for (String rtName : RECORD_TYPES) {
			RecordType rt = MockUtils.getRecordType('Project__c', rtName);
			if (null != rt) {
				rtMap.put(rt.Id, rtName);
			}
		}
        
        List<Project__c> onHoldChanges = new List<Project__c>();
        List<Project__c> stageChanges = new List<Project__c>();
        List<Project__c> projectManagerChanges = new List<Project__c>();
        
        // check if tracked fields have changed and add it to affected records
        for(Project__c rec : recs) {
            if(hasOnHoldChanged(oldMap.get(rec.Id), rec)) { onHoldChanges.add(rec); }
            if(hasChangedToTrackedStage(oldMap.get(rec.Id), rec)) { stageChanges.add(rec); }
            if(hasProjectManagerChanged(oldMap.get(rec.Id), rec, rtMap)) { projectManagerChanges.add(rec); }
        }
        
        if(onHoldChanges.isEmpty() && stageChanges.isEmpty() && projectManagerChanges.isEmpty()) { return; }
        
        List<FeedItem> feedItems = new List<FeedItem>();
        
        // create new feed items for on hold changes
        for(Project__c rec : onHoldChanges) {
            feedItems.add(new FeedItem(
                ParentId = rec.Opportunity__c,
                Type = 'LinkPost',
                Title = rec.Project_Name__c,
                LinkUrl = '/' + rec.Id,
                Body = HOLD_UPDATE.replace('{!On_Hold}', getOnHoldStatus(rec))
            ));
        }
        
        // create new feed items for stage changes
        for(Project__c rec : stageChanges) {
            feedItems.add(new FeedItem(
                ParentId = rec.Opportunity__c,
                Type = 'LinkPost',
                Title = rec.Project_Name__c,
                LinkUrl = '/' + rec.Id,
                Body = STAGE_UPDATE.replace('{!Stage}', rec.Stage__c)
            ));
        }
        		
       	//Project {!Id} for {!Company_text} has been reassigned from {!Project_Manager_Old} to {!Project_Manager}.
        
        List<Project__c> allProjects = new List<Project__c>();
		allProjects.addAll(recs);
		allProjects.addAll(oldMap.values());
		Map<Id, User> projectManagerMap = getProjectManagerMap(allProjects);
        		
        for(Project__c rec : projectManagerChanges) {
        	String body = Label.Project_Reassignment;
        	body = body.replace('{!Name}', rec.Name);
        	body = body.replace('{!Company_text}', getNullAsString(rec.Company_text__c));
        	body = body.replace('{!Project_Manager_Old}', projectManagerMap.get(oldMap.get(rec.Id).Project_Manager__c).Name);
        	body = body.replace('{!Project_Manager}', projectManagerMap.get(rec.Project_Manager__c).Name);
        			
        	feedItems.add(new FeedItem(
        		ParentId = rec.Opportunity__c,
        		Type = 'LinkPost',
        		Title = 'Project reassigned',
        		LinkUrl = '/' + rec.Id,
        		Body = body
        	));
        }
        
        System.debug('~~No of feedItems: ' + feedItems.size());
        insert feedItems;
        TriggerUtils.setSkipReason('ProjectHandler.updateChatterForOpportunity', 'Feed items already added so skipping');
    }
    
    private Boolean hasOnHoldChanged(Project__c oldRec, Project__c newRec) {
        return oldRec.On_Hold__c != newRec.On_Hold__c;
    }
    
    private Boolean hasChangedToTrackedStage(Project__c oldRec, Project__c newRec) {
        return oldRec.Stage__c != newRec.Stage__c && 
        TRACKED_STAGES.contains(newRec.Stage__c);
    }
    	
    private Boolean hasProjectManagerChanged(Project__c oldRec, Project__c newRec, Map<Id, String> rtMap) {
    	String rtName = rtMap.get(newRec.RecordTypeId);
    	return oldRec.Project_Manager__c != newRec.Project_Manager__c &&
    	(rtName == RT_TPS || rtName == RT_TD);
    }  
    
    private String getOnHoldStatus(Project__c rec) {
        if(rec.On_Hold__c) { return 'now On Hold - the reason is: ' + rec.On_Hold_Reason__c; }
        else { return 'no longer On Hold'; }
    }
    	
    private Map<Id, User> getProjectManagerMap(List<Project__c> recs) {
		Set<Id> projectManagerIds = new Set<Id>();
		for(Project__c rec : recs) { projectManagerIds.add(rec.Project_Manager__c); }
		return new Map<Id, User>([select Id, Name from User where Id in :projectManagerIds]);
	}
	
	private String getNullAsString(String s) {
		return s == null ? 'null' : s;
	}
}