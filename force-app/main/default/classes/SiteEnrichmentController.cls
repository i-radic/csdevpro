public class SiteEnrichmentController{

	public static String t { get; set; }
    public static String v { get; set; }
    public static String o { get; set; }

	public SiteEnrichmentController(){
		t = System.currentPagereference().getParameters().get('t');
		v = System.currentPagereference().getParameters().get('v');
	}

	public PageReference onLoad(){
		Boolean isValid = validateRequest();
		if(!isValid){
            PageReference retURL = new PageReference('http://www.google.com'); // error page redirect
            retURL.setRedirect(true);
            return retURL;
        }
        return null;
	}

	public Boolean validateRequest(){
        CS_Site_PIN__c found = null;
        List<CS_Site_PIN__c> sps = [SELECT Id, Name, Timestamp__c, Hash__c, Target_Email__c, VHash__c, Order__c FROM CS_Site_PIN__c WHERE Timestamp__c = :EncodingUtil.base64Decode(t).toString()];
        for(CS_Site_PIN__c sp: sps){
            Blob bKey = EncodingUtil.base64Decode(sp.Hash__c);
            String vd = SiteUtilities.decryptString(bKey, EncodingUtil.base64Decode(v));
            if(vd == sp.VHash__c){
                found = sp;
                o = sp.Order__c;
            }
        }            
        return found == null ? false : true;
    }
    
    @RemoteAction
    public static Map<String, String> getSolution(Id orderId){
        List<csord__Service__c> ser = [SELECT Id, csordtelcoa__Product_Basket__c FROM csord__Service__c WHERE csord__Order__c =:orderId];
		List<csord__Solution__c> sol = [SELECT Id FROM csord__Solution__c WHERE cssdm__product_basket__c = :ser.get(0).csordtelcoa__Product_Basket__c];
        return new Map<String, String>{ SiteUtilities.BASKET => ser.get(0).csordtelcoa__Product_Basket__c, SiteUtilities.SOLUTION => sol.get(0).Id};
    }
	
}