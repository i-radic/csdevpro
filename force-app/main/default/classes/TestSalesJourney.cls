public with sharing class TestSalesJourney {
    public static boolean isSalesJourneyData = false; 
    public static String oppName = 'ZZ12TestOPP';
    
    public static void createSalesJourneyTestData(){
        //Create the Custom Settings Timeline
        SalesJourney__c sj = new SalesJourney__c(Lead_Time_weeks__c = 1,qualify_Time_weeks__c = 1,Sell_Time_weeks__c =30,
                            Propose_Time_weeks__c = 10,Closure_Time_weeks__c = 10,name = 'ICT Product1');
        insert sj;
        
            try{
                //Get the standard pricebook
                Pricebook2 stdPb = [select Id from Pricebook2 where isStandard=true limit 1];
                
                //Create an Account
                Account account = new Account(name='Test Account 1');
                insert account;
                
                //Create an Opportunity
                //ROS - 24/01/2013 Opportunity field cleanse
                //Opportunity opp = new Opportunity(name=oppName,NIBR_Year_2009_10__c=1,NIBR_Year_2010_11__c=2,NIBR_Year_2011_12__c=3,type='New Business',stagename='Created',closedate=system.today(),accountid=account.id);
                Opportunity opp = new Opportunity(name=oppName,type='New Business',stagename='Created',closedate=system.today(),accountid=account.id);
                
                insert opp; 
                
                //Create a Product
                Product2 p = new Product2(name='TestProd1', Description = 'test product',family ='ICT',Part_Number__c='2401418');
                insert p;               
                
                //Create pricebook entry
                PricebookEntry pbe =  new PricebookEntry(pricebook2id = stdPb.id, product2id = p.id, unitprice=1.0, isActive=true);
                insert pbe;
                
                //Insert opportunity line item
                OpportunityLineItem oli = new OpportunityLineItem (OpportunityId = opp.Id, description = 'sjopp',PricebookEntryId = pbe.Id, Quantity = 1, UnitPrice = 20000,category__c = 'Main',contract_term__c='12 Months',billing_cycle__c='Monthly',service_ready_date__c = system.today().addmonths(1),first_bill_date__c = system.today().addmonths(2),initial_cost__c = 3000);
                insert oli;
                
             }catch(Exception e){
                System.debug( e );
            }   
        
            TestSalesJourney.isSalesJourneyData =true;
                
            //Create a holiday record tomorrow
            Date tdate = date.newinstance(2010,02,22);
            
            holiday newholiday = new holiday(name='Holiday Tomorrow',activitydate =tdate);
            insert newholiday; 
            //Create another holiday day after tomorrow
            holiday newholidayweek = new holiday(name='Holiday Day After Tomorrow',activitydate =tdate.adddays(1));
            insert newholidayweek; 
        
            //Added by GS
            list<holiday> holidaylist = [select activitydate from holiday];
        
            system.debug('Holidaycount is ' + holidaylist.size());
    }
    
    static testmethod void testSalesJourneyCreateAndUpdate(){
      
          //Setup the Sales Journey Test Data
          ict_sales_journey__c ictrec = new ict_sales_journey__c();
          
          System.debug('Starting testSalesJourneyCreateandUpdate');
        
          Test.startTest();
          //if( !TestSalesJourney.isSalesJourneyData) 
          TestSalesJourney.createSalesJourneyTestData();
        
          List<Opportunity> testopps = [select id,name,(select id,name,lead_due_date__c,qualify_due_date__c,propose_due_date__c,sell_due_date__c,
                                closure_due_date__c from ict_sales_journeys__r) from opportunity where name = :oppName];
          
          if (testopps.isEmpty()){
              return;
          }
          
          Opportunity testopp = testopps.get(0);
          
          //Get the test Sales Journey record and update a due date.
          try {
          ictrec = testopp.ict_sales_journeys__r;
          }
          catch (Exception ex){
              return;           
          }
          ictrec.Lead_Due_Date__c = ictrec.Lead_Due_Date__c.adddays(1);
          update ictrec;
          ictrec.qualify_Due_Date__c = ictrec.qualify_Due_Date__c.adddays(2);
          update ictrec;
          ictrec.propose_Due_Date__c = ictrec.propose_Due_Date__c.adddays(3);
          update ictrec;
          system.debug('Did the propose update');
          ictrec.sell_Due_Date__c = ictrec.sell_due_Date__c.adddays(4);
          update ictrec;
          ictrec.closure_Due_Date__c = ictrec.closure_due_Date__c.adddays(5);
          update ictrec;
          system.debug('Updated ICT Sales Journey record');
          
          
          //Test the weekend functionality. Keep adding to lead due date until we hit a saturday
          Datetime dt = datetime.now();
          Boolean bcontinue = false; 
        
          //Find the next weekend date
          While (bcontinue == false){
                String weekday = dt.format('E'); 
                if (weekday == 'Sat'){
                        bcontinue = true;
                } 
                dt=dt.addDays(1);   
          }
          /*
          //Set the lead due date to the weekend date
          ictrec.Lead_Due_Date__c = ictrec.Lead_Due_Date__c.adddays(1);
          update ictrec;
          
          //Set the lead due date to the day before the holiday date
          Date tdate = date.newinstance(2010,02,19);
          update ictrec;
          
          ictrec.Lead_Due_Date__c = tdate.adddays(1);
          update ictrec;
          
          ictrec.Lead_Due_Date__c = tdate.adddays(2);
          update ictrec;
          */
          //Assert the creation of the Sales Journey record and all foruma fields + custom Opp fields
        
          //Get the timeline first
          SalesJourney__c sj = [select Lead_Time_weeks__c,qualify_Time_weeks__c,Sell_Time_weeks__c,
                            Propose_Time_weeks__c,Closure_Time_weeks__c,name from salesjourney__c where name ='ICT Product1'];
        
          System.assertequals(sj.lead_time_weeks__c,1);
          System.assertequals(sj.qualify_Time_weeks__c,1);
          System.assertequals(sj.Sell_Time_weeks__c,30);
          System.assertequals(sj.Propose_Time_weeks__c,10);
          System.assertequals(sj.Closure_Time_weeks__c,10);
         
          //Now get the opportunity record from the DB.
          
          System.assertequals(testopp.name,oppName);
      
          //Edit the Sales Journey record
          System.assertequals(ictrec.name,testopp.name); 
         // System.assertequals(ictrec.Closure_Due_Date__c,date.newinstance(2010,05,30)); 
         // System.assertequals(ictrec.Sell_Due_Date__c,date.newinstance(2010,05,01));
         // System.assertequals(ictrec.Propose_Due_Date__c,date.newinstance(2010,05,14)); 
         // System.assertequals(ictrec.Qualify_Due_Date__c,date.newinstance(2010,03,18)); 
         // System.assertequals(ictrec.Lead_Due_Date__c,date.newinstance(2010,02,21));
          
          //Assert all due date fields on Sales Journey.
            
          //Assert Opportunity fields once more.
         Test.stoptest();
    }  
 
}