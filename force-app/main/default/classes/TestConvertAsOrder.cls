@isTest(seeAllData=true)
private class TestConvertAsOrder {
    @isTest static void testcr10002() {
        Account a  = new Account(Name='TestAccount');
        insert a;
        Associated_LEs__c aa = new Associated_LEs__c(Name='TestAsc');
        aa.Account__c = a.id;
        insert aa;
        Opportunity opp = new Opportunity();
        opp.Name = 'Testopp';
        opp.AssociatedLE__c = aa.Id;
        opp.AccountId = a.id;
        opp.CloseDate = System.Date.today()+25;
        opp.StageName = 'Created';
        opp.Buying_Cycle_Stage__c = 'Decision';
        opp.Product_Family__c = 'HOSTED VOICE';
        insert opp;
        ConvertAsOrder.getOpportunity(opp.Id);
    }
}