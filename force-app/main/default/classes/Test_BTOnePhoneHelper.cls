/******************************************************************************************************
Name : Test_BTOnePhoneHelper 
Description : Test class of BTOnePhoneHelper

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    01/02/2016                 Praveen                                Class created         
*********************************************************************************************************/

@IsTest
public class Test_BTOnePhoneHelper {
    
    static testmethod void BTOnePhoneHelpertest()
    {   
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];         
        User u1 = new User(        
            alias = 'PRENELA', email = 'BTOP@bt.it',        
            emailencodingkey = 'UTF-8', lastname = 'Testing BTOP', 
            languagelocalekey = 'en_US',        
            localesidkey = 'en_US', ProfileId=prof.Id,
            timezonesidkey = 'Europe/London', username = 'BTOP@bt.it',
            EIN__c = '012345678'      
        );                                
        insert u1; 
        
        User u2 = new User(        
            alias = 'PRENELA', email = 'BTOPS@bt.it',        
            emailencodingkey = 'UTF-8', lastname = 'Testing BTOPS', 
            languagelocalekey = 'en_US',        
            localesidkey = 'en_US', ProfileId=prof.Id,
            timezonesidkey = 'Europe/London', username = 'BTOPS@bt.it',
            EIN__c = '012345679'      
        );                                
        insert u2; 
        
        Account acct = Test_Factory.CreateAccount();
        insert acct;
        
        Contact con = Test_Factory.CreateContact();
        insert con;
        
        Opportunity oppS2S = Test_Factory.CreateOpportunity(acct.id);
        insert oppS2S;
        
        BT_One_Phone__c bop = new BT_One_Phone__c();
        bop.Opportunity__c=oppS2S.Id;
        bop.BT_OnePhone_Sales_Specialist__c=u1.Id;        
        bop.Volume_of_Data_SIMs__c=123;
        bop.Contract_Duration__c='60 Months';
        bop.Status__c='Pre Clearance Check Completed';
        bop.All_key_Customer_Locations_Coverage__c='Checked OK';
        bop.Contract_Value__c=1.11;
        bop.Pre_Clearance_Limit__c = 10;
        bop.Credit_Worthiness_Date__c = DateTime.now();
        bop.Professional_Implementation__c = 'simple (2 wk)';
        bop.Existing_Customer_Resign__c  = 'Y'; 
         try{
        insert bop;
         }catch(Exception ex){}
        
        BT_One_Phone__c bop1 = new BT_One_Phone__c();
        bop1.Opportunity__c=oppS2S.Id;
        bop1.BT_OnePhone_Sales_Specialist__c=u1.Id;        
        bop1.Volume_of_Data_SIMs__c=123;
        bop1.Contract_Duration__c='60 Months';
        bop1.Status__c='Order Rejected';
        bop1.All_key_Customer_Locations_Coverage__c='Checked OK';
        bop1.Contract_Value__c=1.11;
        bop1.Pre_Clearance_Limit__c = 10;
        bop1.Credit_Worthiness_Date__c = DateTime.now();
        bop1.Professional_Implementation__c = 'simple (2 wk)';
        bop1.BTOP_PM_Hand_Over_Discussion_Call__c = True;
        bop1.Existing_Customer_Resign__c  = 'Y'; 
         try{
        insert bop1;
         }catch(Exception ex){
             
         }
        
        BT_One_Phone_Site__c bops = new BT_One_Phone_Site__c();
        bops.BT_One_Phone__c = bop.Id;
        bops.Onsite_mobile_network__c=true;
        bops.Site_Telephone_Number_Landline__c='01234567890';
        bops.Number_of_Buildings__c='1';
        bops.Number_of_Users__c=1;
        bops.Contract_Duration__c='60 Months';
        bops.Number_Concurrent_Calls_Expected_at_Site__c=8;
        bops.Number_of_Users_in_Parallel_Hunt_Group__c='5 or Less';
        bops.mobilecoverageResults__c='Excellent Indoor & Outdoor';
        bops.Coverage_3G_Checker_Results__c='Low Outdoor Only';
        bops.How_Many_Hunt_Groups__c=1;
        bops.Building_Number__c='test1';
        bops.Street__c='test2';
        bops.Customer_Contact__c= con.Id;
        bops.Town__c='tes';
        bops.County__c='test4';
        bops.Post_Code__c='te';
        bops.Site_Diagram_or_Sketch_Attached__c = true;
        insert bops;
        
        bop.Contract_Duration__c='24 Months';
        bop.BT_OnePhone_Sales_Specialist__c=u2.Id;
        bop.BTOP_PM_Hand_Over_Discussion_Call__c = True;
        bop.Status__c = 'Order Rejected';
        BTOnePhoneTriggerHandler.BTOnePhoneTriggerAfterHandleRecusion = True;
        BTOnePhoneTriggerHandler.BTOnePhoneTriggerBeforeHandleRecusion = True;
        update bop;
        
        bop.Status__c = 'Customer Installed';
        bop.Pre_clearance_amount_from_Pricing_Tool__c = 9;        
        bop.Number_of_Attachments__c = 1;
        bop.OnePhone_Product_Type__c = 'BT One Phone Professional';
        BTOnePhoneTriggerHandler.BTOnePhoneTriggerBeforeHandleRecusion = True;
        update bop;
        
        bop.Status__c = 'Site evaluation / survey requested';
        bop.Pre_clearance_amount_from_Pricing_Tool__c = 9;        
        bop.Number_of_Attachments__c = 1;
        bop.OnePhone_Product_Type__c = 'BT One Phone Professional';
        BTOnePhoneTriggerHandler.BTOnePhoneTriggerBeforeHandleRecusion = True;
        update bop;
         //CR10933
         bop.Status__c = 'Sales handover call completed';
        bop.Pre_clearance_amount_from_Pricing_Tool__c = 9;        
        bop.Number_of_Attachments__c = 1;
        bop.OnePhone_Product_Type__c = 'BT One Phone Professional';
        BTOnePhoneTriggerHandler.BTOnePhoneTriggerBeforeHandleRecusion = True;
        update bop;
        
    }
    }
}