global class scheduledAgedDebtDeleteBatch implements Schedulable{
 global void execute(SchedulableContext sc) {   
  AgedDebtDeleteBatch b = new AgedDebtDeleteBatch();
  database.executebatch(b);
 }
}