/*
###########################################################################
# File..................: StockItemsController 
# Created by............: Sridhar Aluru
# Created Date..........: 16-May-2016
# Last Modified by......:
# Last Modified Date....: 
# Description...........: Used to load Stock Items into Salesforce. 
# Change Log............:
# Test Class............: TestStockItemsController
###########################################################################
*/
public with sharing class StockItemsController {

    public String nameFile{get;set;}
    public Blob contentFile{get;set;}
  
  //  public Boolean processing { get; set; }
    public Boolean errorExists { get; set; }
    
    public String uploadStatusMsg { get; set; }
    public Integer transListSize { get; set; }
    
    public List<Stock_Item__c> stockResult;    
     
    public Boolean showtable {get;set;} 
    private string defaultSKUId;   
  //  private string defaultEmailGroup;  
   
    List<ID> skuIDList = new List <ID>();
    Set<ID> skuSet = new Set<ID>();
 
 
    public StockItemsController()
    {
        System.debug('------------inside controller-----------');
        errorExists = false;
        uploadStatusMsg ='';
        defaultSKUId = '';
     //   defaultEmailGroup = '';
        showtable = false;
        stockResult = new List<Stock_Item__c>();     
    }
   
    public PageReference readFile()
    { 
        //Check Custom settings has value and Get default SKU for invalid Stock Items else show message
        System_Defaults__c sysDefaults = System_Defaults__c.getInstance('Stock Item Default SKU');
        
        if (sysDefaults == null || sysDefaults.Record_Id__c == null)
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Custom setting for default SKU for linking stockitems with missing details not defined.'));
        else
            defaultSKUId = sysDefaults.Record_Id__c;      
        
        try
        {
            List<List<String>> filelines;
            filelines = parseCSV(contentFile.toString(),',',false);
           
            if(filelines.size()>=1000)
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'File Too Big. Max 1000 Records allowed to upload at one time'));
            else
            {
                populate_Transactions(filelines);      
                showtable =  true;          
            } 
            return null;
        }
        catch(Exception e)
        {
            errorExists= true;
            uploadStatusMsg += 'Error Reading File:'+e.getMessage();
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Error reading file, please check the file format and try again<br> File should not contain more than 1000 records.<br/> File Issue :<b>' + e + '</b>'));
        }
        return null;
    }
   
    
    private void populate_Transactions(List<List<String>> flinesList )
    {   
        //Parser
        StockItemParser parser = new StockItemParser ();
        stockResult = parser.parseFile(flinesList);
        system.debug(' Scores ---> ' + stockResult);
    }
   
    public List<Stock_Item__c> getStockItems()
    {
        system.debug(' Stock Items ---> ' + stockResult);
        return stockResult;
    }
   
    // parses the individual columns in files    
    private  List<List<String>> parseCSV(String contents,String sep,Boolean skipHeaders) {  
    
        List<List<String>> allFields = new List<List<String>>();  
    
         // we are not attempting to handle fields with a newline inside of them  
         // so, split on newline to get the spreadsheet rows  
        List<String> lines = new List<String>();  
         try {  
            system.debug('Before Splitting -----> ');
            
                 lines = contents.split('\n');  
         } catch (System.ListException e) {  
             System.debug('Limits exceeded?' + e.getMessage());  
        }  
        
        system.debug('Split Done -----> ');
        Integer num = 0;  
        for(String line : lines) {  
    
            // check for blank CSV lines (only commas)  
            //System.debug('*********'+line);
            if (line.replaceAll(',','').trim().length() == 0) continue;  
            if(line.trim().length()==0) continue;
            
            //Replace the Separator with ','
            line = line.replace(sep,',');
            
            List<String> fields = line.split(',');      
            List<String> cleanFields = new List<String>();  
    
            String compositeField;  
            Boolean makeCompositeField = false;  
    
            for(String field : fields) {  
                field = field.trim();
                 if (field.startsWith('"') && field.endsWith('"')) {  
                    cleanFields.add(field.replaceAll('"',''));  
                    makeCompositeField = false;
            } else if (field.startsWith('"')) {  
                    compositeField += ',' + field;  
                    makeCompositeField = true;
                    compositeField = field;  
                } else if (field.endsWith('"')) {  
                    compositeField += ',' + field;  
                    cleanFields.add(compositeField.replaceAll('"',''));  
                    makeCompositeField = false;  
                } else if (makeCompositeField) {  
                     compositeField +=  ',' + field;  
                 } else {  
                     cleanFields.add(field.replaceAll('"',''));  
                 }  
      
            }  
            allFields.add(cleanFields);  
         }  
         if (skipHeaders) allFields.remove(0);  
         return allFields;         
    } 
    
     
    //Saves all the transactions
    public PageReference uploadStockItems()
    {
        String saveUploadStatusMsg='';
        try{
            
            List<Stock_Item__c> allCorrectStockItems = new List<Stock_Item__c>(); 
            List<Stock_Item__c> allIncorrectStockItems = new List<Stock_Item__c>();                       
           
            for(Stock_Item__c ps : stockResult)
            {
                skuIDList.add(ps.SKU__c);            
            }                        

            for (SKU__c sku1 : [SELECT Id FROM SKU__c where Id in :skuIDList]) {
               skuSet.add(sku1.Id);
            }
       
            //Run Loop to link Blank store items to default SKU as mentioned in Custom Settings
            
            for(Stock_Item__c ps : stockResult)
            {
                if (ps.SKU__c == null || !skuSet.contains(ps.SKU__c))
                {
                    ps.SKU__c = defaultSKUId;
                    allIncorrectStockItems.add(ps);
                }

                else if (skuSet.contains(ps.SKU__c))
                {
                    System.debug('SKU already Exists>>>>>>>>>>>>>');
                    allCorrectStockItems.add(ps);             
                }   
            }
            
            //Insert others successfully
            Database.SaveResult[] sr = Database.insert(allCorrectStockItems,false);
            // Process the save results for insert.
          
           for(Integer i=0;i<sr.size();i++)
           {
                 if (sr.get(i).isSuccess()){
                    saveUploadStatusMsg += '<br/>Row successfully created with Salesforce Record Id :'+ sr.get(i).getId();
                }
                else if (!sr.get(i).isSuccess()){
                    // DML operation failed
                    Database.Error err = sr.get(i).getErrors().get(0);
                    saveUploadStatusMsg += '<br/>Error processing row :'+err.getMessage();  

                    allCorrectStockItems.get(i);//failed record from the list
                    system.debug('Failed Record --->'+allCorrectStockItems.get(i));
                    Stock_Item__c ps = allCorrectStockItems.get(i);               
                    ps.SKU__c = defaultSKUId;
                    allIncorrectStockItems.add(ps);
                }
            }
            
            saveUploadStatusMsg += '<br/><br/>File Successfully uploaded.';
            
            system.debug('Incorrect Records --->' + allIncorrectStockItems);
            insert allIncorrectStockItems;
            
            //Send email if any failed stock items found to trial request users
            System.debug('Incorrect Stock Items List Size>>>>>>>>>>>>>>>>>>>>'+allIncorrectStockItems.size());
            if(allIncorrectStockItems.size()>0)
            {                 
                 SendMail(allIncorrectStockItems);      
            }
            allIncorrectStockItems.clear();
        }
        catch (Exception e)
        {    
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template format or contact your system admin if problem persists. SFDC error: '+e.getMessage());
            ApexPages.addMessage(errormsg);
            return null;  
        }
         
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'<br/>Stock Items successfully uploaded. Email notification will be sent for results that failed to upload'));
        return null;    
    }
    
    public PageReference refresh()
    {       
        Pagereference ref = Page.Upload_StockItems;
        ref.setRedirect(true);
        return ref;   
    }  
    
    private void SendMail(List<Stock_Item__c> failedStockItems)
    {
        // First, reserve email capacity for the current Apex transaction to ensure 
        // that we won't exceed our daily email limits when sending email after 
        // the current transaction is committed. 
        
        Messaging.reserveSingleEmailCapacity(2);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
      //String[] toAddresses = new String[] {mailToAddresses}; 
        List<String> mailToAddresses = new List<String>();
        mailToAddresses.add('trial.request@ee.co.uk');
        mail.setToAddresses(mailToAddresses);
        mail.setSenderDisplayName('Sales Enablers');
        mail.setSubject('StoreItem failures');
        // Specify the text content of the email. 
        mail.setPlainTextBody('StoreItem Failures');
        string str='Dear User, <br/><br/>Few store Item results failed to upload and have been linked to default SKU defined in the system. Please check and verify them.';
        str=str+ '<br/><br/>Thanks <br/>Sales Enablers Team';
        mail.setHtmlBody(str);
        
        // Send the email you have created.
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }   
}