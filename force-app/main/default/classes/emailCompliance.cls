/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class emailCompliance implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();

 list <User> ui = [Select Id, EIN__c, Email_BCC__c, Division, Department, ProfileId, Email_Signature__c from User where  email = :email.fromAddress and isActive = true limit 1];
     if(ui.size()>0){
         String ProfName = [Select Name from Profile where Id=:ui[0].ProfileId].Name;
         String UserES = ui[0].Email_Signature__c;
         String EmailHTMLBody = email.htmlbody;
         String EmailTextBody = email.plainTextBody;
         
         if(ProfName=='BTLB: Standard Team' || ProfName=='BTLB: Standard MD'){
             
             if(ui[0].Email_BCC__c==true){
             //Send Email
             
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             //mail.setCCAddresses(new String[]{email.fromAddress}); //Sandbox
             mail.setBCCAddresses(new String[]{email.fromAddress}); // Live
             mail.setSaveAsActivity(False);
             mail.htmlbody=email.htmlbody;
             mail.plainTextBody=email.plainTextBody;
             mail.subject=email.subject;
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
             }
             System.debug('HTMLBody---->'+email.htmlbody);
             System.debug('plainTextBody---->'+email.plainTextBody);
             
             if(EmailHTMLBody.contains(UserES) || EmailTextBody.contains(UserES) ){
                 //do nothing             
             }
             else{
                
                EmailCompliance__c e = new EmailCompliance__c();
                //e.toAddress__c = email.toAddresses;
                e.emailSubject__c = email.subject;
                e.fromAddress__c = email.fromAddress;
                e.emailBodyHtml__c = email.htmlbody;
                e.emailBodyTxt__c = email.plainTextBody;
                e.sender__c = ui[0].Id;
                insert e;

             }
         }
     }


system.debug('eMail com fromAddress ' + email.fromAddress);

        return result;
    }
}