/*
 * Author:609701681
 * Name: DharmaTeja
 * Created as Part of CASE00400313
 * ApexClass: BTLB_Master_Helper
 * ApexTrigger: UpdateUserDepartment
*/
@isTest
public class Test_BTLB_Master_Helper {
    static testMethod void testBTLBIns() {
        BTLB_Master__c test_rec = new BTLB_Master__c(Name = 'BPS Scancom Distribution ltd',
                                                 BTLB_Name_ExtLink__c = 'Test Distribution Ltd',
                                                 Email_Signature__c = 'Test email signature');
       
        insert test_rec;
        test_rec.Email_Signature__c = 'Test signature 2 --@--';
        update test_rec;
        
    }
    static testMethod void testInsTriggerExec() {
        BTLB_Master__c test_rec2 = new BTLB_Master__c(Name = 'BPSScancomDistributionLTD',
                                                 BTLB_Name_ExtLink__c = 'TestDistributionLtd',
                                                 Email_Signature__c = 'Testemailsignature');
        insert test_rec2;
        BTLB_Master__c test_rec3 = new BTLB_Master__c(Name = 'BPSScancomDistributionLTD 1',
                                                 BTLB_Name_ExtLink__c = 'TestDistributionLtd 1',
                                                 Email_Signature__c = 'Testemailsignature');
        insert test_rec3;
    }
    static testMethod void testUpdTriggerExec() {
        BTLB_Master__c test_rec1 = new BTLB_Master__c(Name = 'BPSScancomDistributionLTD',
                                                 BTLB_Name_ExtLink__c = 'TestDistributionLtd',
                                                 Email_Signature__c = 'Testemailsignature');
        insert test_rec1;
        BTLB_Master__c test_rec2 = new BTLB_Master__c(Name = 'BPSScancomDistributionLTD 11',
                                                 BTLB_Name_ExtLink__c = 'TestDistributionLtd 2',
                                                 Email_Signature__c = 'Testemailsignature');
        insert test_rec2;
        //test_rec2.Name = test_rec1.Name;
        //update test_rec2;
    }
}