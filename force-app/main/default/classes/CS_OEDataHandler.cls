global class CS_OEDataHandler implements csoe.IRemoteAction {
    static final String COLUMN_NAME = 'MSISDN';
    global Map<String, Object> execute(Map<String, Object> inputMap){
        String name = (String)inputMap.get('name');
        String configId = (String)inputMap.get('configId');
        Map<String, Object> outputMap = new Map<String, Object>();
        cscfga__Product_Configuration__c config = [
        SELECT Id, cscfga__Product_Basket__c 
            FROM cscfga__Product_Configuration__c 
            WHERE Id = :configId LIMIT 1
        ];
        outputMap.put('basketId', config.cscfga__Product_Basket__c);

        if(name == 'BTMobile'){
            outputMap.putAll(getMobileHardwareFromBasket(config.cscfga__Product_Basket__c));    
            outputMap.putAll(getExtrasFromBasket(config.cscfga__Product_Basket__c));
            outputMap.putAll(getMobileBroadbandFromBasket(config.cscfga__Product_Basket__c));   
            outputMap.putAll(getFlexData(configId));
        }
        if(name == 'BTOP'){
            outputMap.putAll(getRelatedProductsFromBasketBTOP(config.cscfga__Product_Basket__c));
            outputMap.putAll(getAllConfigsFromBasketBTOP(config.cscfga__Product_Basket__c));
            outputMap.putAll(getDevicesFromBasket(config.cscfga__Product_Basket__c));
            outputMap.putAll(getSubscriptionsFromBasket(config.cscfga__Product_Basket__c));
            outputMap.putAll(getHQData(configId));
            outputMap.putAll(getAttachmentData(configId));
        }
        if(name == 'BTMobileRelated'){
            outputMap.putAll(getRelatedProductsFromBasketBTMobile(configId));
        }
        if(name == 'BTOPRelated'){
            
        }        
        return outputMap;
    }
    
    @TestVisible
    private static boolean hasSObjectField(String fieldName, SObject so){
        String s = JSON.serialize(so);
        // Deserialize it back into a key/value map
        Map<String,Object> obj = (Map<String,Object>) JSON.deserializeUntyped(s);
        // Build a set containing the fields present on our SObject
        Set<String> fieldsPresent = obj.keyset().clone();
        return fieldsPresent.contains(fieldName);
    }

    /* BTMOBILE start */
    private Map<String, Object> getMobileHardwareFromBasket(String basketId){
        Map<String, Object> outputMap = new Map<String, Object>();
        //  AND cscfga__Configuration_Status__c = 'Valid' - removed 
        List<cscfga__Product_Configuration__c> pc = [
            SELECT Id, Name, SIM_Type__c, Volume__c 
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Product_Basket__c = :basketId 
                AND Product_Definition_Name__c = 'BT Mobile Hardware' 
                AND (Product_Type__c = 'Tablet' OR Product_Type__c = 'Handset') 
                AND Name != 'BT Mobile Hardware'
        ];
        outputMap.put('mobileHardware', pc);
        return outputMap;
    }

    private Map<String, Object> getMobileBroadbandFromBasket(String basketId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<cscfga__Product_Configuration__c> pc = [
            SELECT Id, Name, Volume__c, Deal_Type__c 
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Product_Basket__c = :basketId 
                AND Product_Definition_Name__c = 'BT Mobile Broadband' 
                AND cscfga__Configuration_Status__c = 'Valid'
        ];
        outputMap.put('mobileBroadband', pc);
        return outputMap;
    }

    private Map<String, Object> getExtrasFromBasket(String basketId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<cscfga__Product_Configuration__c> extras = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> pcs = [
            SELECT Id, Name, Volume__c 
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Product_Basket__c = :basketId 
                AND Product_Definition_Name__c = 'BT Mobile Extras' 
                AND cscfga__Configuration_Status__c = 'Valid'
        ];
        
        List<String> extraProductTypeNames = new List<String>();
        for (cscfga__Product_Configuration__c pc : pcs){
            extraProductTypeNames.add(pc.Name.substring(0, pc.Name.indexOf('-') - 1));
        }

        List<BT_Extra_Product_Type__c> extraProductTypes = [
            SELECT Id, Name, Extension_type__c 
            FROM BT_Extra_Product_Type__c 
            WHERE Name IN :extraProductTypeNames
        ];
        
        for (cscfga__Product_Configuration__c pc : pcs){
            String name = pc.Name.substring(0, pc.Name.indexOf('-') - 1);
            String extType = getExtraProductType(extraProductTypes, name);
            if(extType.equals('Subscriber extension')){
                extras.add(pc);
            }
        }


        outputMap.put('extras', extras);
        return outputMap;
    }

    private String getExtraProductType(List<BT_Extra_Product_Type__c> eptlist, String name){
        for(BT_Extra_Product_Type__c ept : eptlist){
            if(name.equals(ept.Name) && hasSObjectField('Extension_type__c', ept)){
                return ept.Extension_type__c;
            }
        }
        return '';
    }

    private Map<String, Object> getRelatedProductsFromBasketBTMobile(String configId){
        Map<String, Object> outputMap = new Map<String, Object>();
        String basketId = '';
        List<cscfga__Product_Configuration__c> configs = [
            SELECT Id, Spend_Cap_Name__c, Spend_Cap_Billing_Code__c, Name, 
                    Volume__c, Product_Definition_Name__c, cscfga__Product_Basket__c,
                (SELECT Name, cscfga__Value__c 
                FROM cscfga__Attributes__r 
                WHERE Name = 'Connection type')            
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Parent_Configuration__c = :configId
        ];
        if(configs.size() > 0){
            outputMap.put('data', configs);
            basketId = configs.get(0).cscfga__Product_Basket__c;
        }
        else{
            cscfga__Product_Configuration__c coreConf = [
                SELECT Id, cscfga__Product_Basket__c 
                FROM cscfga__Product_Configuration__c 
                WHERE Id = :configId LIMIT 1
            ];
            basketId = coreConf.cscfga__Product_Basket__c;
        }
        List<cscfga__Product_Basket__c> baskets = [
            SELECT Id, Apple_Reseller_Id__c, Customer_Apple_Id__c 
            FROM cscfga__Product_Basket__c 
            WHERE Id = :basketId
        ];
        outputMap.put('basket', baskets);
        return outputMap;
    }

    private Map<String, Object> getFlexData(String configId){
        Map<String, Object> outputMap = new Map<String, Object>();

        List<cscfga__Product_Configuration__c> configs = [
            SELECT Id, Name, Spend_Cap_Name__c, Spend_Cap_Billing_Code__c, 
                Number_of_Ports__c, Number_of_Resigns__c, Number_of_New_Connections__c 
            FROM cscfga__Product_Configuration__c 
            WHERE ID = :configId AND Product_Definition_Name__c = 'BT Mobile Flex'
        ];
        outputMap.put('flex', configs);

        return outputMap;
    }
    /* BTMOBILE end */

    /* BTOP start */
    private Map<String, Object> getAllConfigsFromBasketBTOP(String basketId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<cscfga__Product_Configuration__c> pcs = [
            SELECT Id, Name, Volume__c 
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Product_Basket__c = :basketId 
        ];

        outputMap.put('configsFromBtopBasket', pcs);
        return outputMap;
    }

    private Map<String, Object> getRelatedProductsFromBasketBTOP(String basketId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<cscfga__Product_Configuration__c> dataExtras = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> roamingExtras = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> callRecordings = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> signalAssist = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> deviceExtras = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> blackberryExtras = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> simExtras = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> pcs = [
            SELECT Id, Name, Volume__c
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Product_Basket__c = :basketId 
                AND Product_Definition_Name__c = 'BTOP Subscription Level Add-On' 
                AND cscfga__Configuration_Status__c = 'Valid'
        ];
        
        List<String> extrasNames = new List<String>();
        List<Id> extrasIds = new List<Id>();        
        for(cscfga__Product_Configuration__c pc : pcs){
            extrasIds.add(pc.Id);
            extrasNames.add(pc.Name);
            if(pc.Name.contains('Call Recording'))
                callRecordings.add(pc);
        }

        List<cspmb__Price_Item__c> pis = [
            SELECT Id, Name, Product_Type_Name__c 
            FROM cspmb__Price_Item__c 
            WHERE Name IN :extrasNames
            //WHERE id IN :extrasIds
        ];

        for(cspmb__Price_Item__c pi : pis){
            //if(pi.Product_Type_Name__c == 'Call Recording')
            //    callRecordings.add(getPCbyPIName(pcs, pi.Name));
            if(pi.Product_Type_Name__c == 'Data Extras')
                dataExtras.addall(getPCbyPIName(pcs, pi.Name));
            if(pi.Product_Type_Name__c == 'Roaming Extras')
                roamingExtras.addall(getPCbyPIName(pcs, pi.Name));
            if(pi.Product_Type_Name__c == 'BT Signal Assist')
                signalAssist.addall(getPCbyPIName(pcs, pi.Name));
            if(pi.Product_Type_Name__c == 'Device Management Extras')
                deviceExtras.addall(getPCbyPIName(pcs, pi.Name));   
                //deviceExtras.add(getPCbyPIId(pcs, pi.id));   
            if(pi.Product_Type_Name__c == 'Blackberry Extras')
                blackberryExtras.addall(getPCbyPIName(pcs, pi.Name));
                //blackberryExtras.add(getPCbyPIId(pcs, pi.id));
            if(pi.Product_Type_Name__c == 'SIM Extras')
                simExtras.addall(getPCbyPIName(pcs, pi.Name));
        }        

        outputMap.put('dataExtras', dataExtras);
        outputMap.put('roamingExtras', roamingExtras);
        outputMap.put('callRecordings', callRecordings);
        outputMap.put('signalAssist', signalAssist);
        outputMap.put('deviceExtras', deviceExtras);
        outputMap.put('blackberryExtras', blackberryExtras);
        outputMap.put('simExtras', simExtras);
        outputMap.put('subsLevelAddon', extrasNames);
        return outputMap;
    }

    private list<cscfga__Product_Configuration__c> getPCbyPIName(List<cscfga__Product_Configuration__c> pcs, String name){
        list<cscfga__Product_Configuration__c> results = new list<cscfga__Product_Configuration__c>();
        for(cscfga__Product_Configuration__c pc : pcs){
            if(pc.Name == name)
                results.add(pc);
        }
        if (results.size() == 0){
                return null;
        }
        return results;
    }
    
    private cscfga__Product_Configuration__c getPCbyPIId(List<cscfga__Product_Configuration__c> pcs, id id){
        for(cscfga__Product_Configuration__c pc : pcs){
            if(pc.id == id)
                return pc;
        }
        return null;
    }

    private Map<String, Object> getDevicesFromBasket(String basketId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<cscfga__Product_Configuration__c> pcs = [
            SELECT Id, Name, Volume__c 
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Product_Basket__c = :basketId 
                AND Product_Definition_Name__c = 'BTOP Devices' 
                AND (Product_Type__c = 'SIM' OR Product_Type__c = 'Handset') 
                AND Name != 'BTOP Devices'
        ];
        outputMap.put('devices', pcs);
        return outputMap;
    }

    private Map<String, Object> getSubscriptionsFromBasket(String basketId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<cscfga__Product_Configuration__c> pcs = [
            SELECT Id, Product_Type__c, Volume__c 
            FROM cscfga__Product_Configuration__c 
            WHERE cscfga__Product_Basket__c = :basketId 
            AND Product_Definition_Name__c = 'BTOP User Subscription' 
            AND cscfga__Configuration_Status__c = 'Valid'
        ];
        outputMap.put('subs', pcs);
        return outputMap;
    }

    private Map<String, Object> getHQData(String configId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<cscfga__Product_Configuration__c> pcs = [
            SELECT Id, Deal_Type__c, Volume__c 
            FROM cscfga__Product_Configuration__c 
            WHERE Id = :configId AND cscfga__Configuration_Status__c = 'Valid'
        ];
        outputMap.put('hq', pcs);
        return outputMap;
    }

    private Map<String, Object> getAttachmentData(String configId){
        Map<String, Object> outputMap = new Map<String, Object>();
        List<Attachment> atts = [
            SELECT Id, Body, Name 
            FROM Attachment 
            WHERE ParentId = :configId AND Name = 'MSISDN'
        ];
        List<String> csv = new List<String>();
        for(Attachment a: atts){
            csv.add(a.Body.toString());
        }
        outputMap.put('csv', csv);
        return outputMap;
    }
    /* BTOP end*/
}