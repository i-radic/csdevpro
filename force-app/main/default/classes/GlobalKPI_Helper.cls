public with sharing class GlobalKPI_Helper 
{
    
    public static map<date, Period> quarterPeriodMap = new map<date, Period>();
    public static map<date, Period> yearPeriodMap = new map<date, Period>(); 
    
    public GlobalKPI_Helper()
    {
        Date fromDate   = date.today().addMonths(-36);
        Date toDate     = date.today().addMonths(36);
        for (Period p : [select StartDate, EndDate, Type from Period where startDate>=:fromDate and startDate<=:toDate and type in ('Quarter', 'Year') ])
        {
            if (p.Type=='Quarter')
            {
                quarterPeriodMap.put(p.StartDate, p);
            }
            else if (p.Type=='Year')
            {
                yearPeriodMap.put(p.StartDate, p);
            }
        }
    }
    
    public date getPeriodStartDate(string inPeriod, date inDate)
    {
        if (inPeriod=='Month')
        {
            return inDate.toStartOfMonth();
        }
        else if (inPeriod=='Fiscal Quarter')
        {
            return getFiscalQuarterStartDate(inDate);
        }
        else if (inPeriod=='Fiscal Year')
        {
            return getFiscalYearStartDate(inDate);
        }
        else 
        {
            return date.newInstance(2000, 1, 1);
        }
    } 
    
    private date getFiscalQuarterStartDate(date inDate)
    {
        for(date d : quarterPeriodMap.keySet())
        {
            if (inDate>=quarterPeriodMap.get(d).startDate    &&    inDate<=quarterPeriodMap.get(d).endDate)
            {
                return quarterPeriodMap.get(d).startDate;//
            }
        }
    return date.newInstance(2000, 1, 1);
    }
    
    private date getFiscalYearStartDate(date inDate)
    {
        for(date d : yearPeriodMap.keySet())
        {
            if (inDate>=yearPeriodMap.get(d).startDate    &&    inDate<=yearPeriodMap.get(d).endDate)
            {
                return yearPeriodMap.get(d).startDate;//
            }
        }
    return date.newInstance(2000, 1, 1);
    }

}