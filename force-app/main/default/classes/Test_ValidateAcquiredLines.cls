@isTest(seealldata=true)
private class Test_ValidateAcquiredLines {
    static testMethod void myUnitTest() {
      
    User B2BUser1;
    Profile B2BProfile1 = [select id from profile where name='Field: Standard User'];
    
        B2BUser1= new User(alias = 'B2B1', email='B2B13cr@bt.com', 
        emailencodingkey='UTF-8', lastname='Testing B2B1', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = B2BProfile1.Id, timezonesidkey='Europe/London', username='B2BProfile131cr@bt.com',
        EIN__c='B2B3cr');
        insert B2BUser1;
        
        system.runAs(B2BUser1){
        test.starttest();  
        String rtId;
        
        List < Profile > PList = [select Id From Profile where Name like '%Standard User%' or Name Like '%Sales Manager%'];
        
        Set<Id> ProfileSet = new Set<Id>();
            
        Set<Id> ICTRTSCalls = new set<Id>();
        
        for (Profile P: PList)
        ProfileSet.add(P.Id);
        
        
            Opportunity o=new Opportunity();
            o.Name='Book2Bill';
            o.StageName='Created';
            o.ownerid=B2BUser1.id;
            o.closedate=date.today();
            o.LFS_Static_Close_Date__c = date.today();
            o.Amount = 10.0;
            
            insert o;
           
        
            Product2 Pd7=new Product2();
            pd7.IsActive=TRUE;
            pd7.ProductCode='MVNRV';
            pd7.Name='PSTN - Analogue Exchange Line';
            pd7.Family='Network Services'; 
           // pd7.Part_Number__c = 'IPHO';
            insert pd7; 
        
            
            Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
            Pricebook2 PriceBook2 = new Pricebook2(IsActive=FALSE,Description='Custom Price Book',Name='BT');
            Insert PriceBook2;
        
            PriceBookEntry PBE8=new PricebookEntry();
            PBE8.Product2Id=pd7.id;
            PBE8.Pricebook2Id=[Select Id From PriceBook2 where Name='BT Corporate' ].Id;
            PBE8.Pricebook2Id = standardPB.Id;
            //pBE8.ProductCode='MVNRV';
            PBE8.UnitPrice=100.00;
            PBE8.IsActive=TRUE;
            
            insert PBE8;
   
        
            
            OpportunityLineItem oli4=new OpportunityLineItem();
              oli4.Qty__c=100;            
              oli4.ServiceDate=system.today();
              oli4.OpportunityId=o.Id;
              oli4.PricebookEntryId=PBE8.Id;           
              oli4.Sales_Type__c='Acquisition';
              oli4.UnitPrice = 0.00;
              oli4.Category__c = 'Main';  
              //oli4.ProdCode__c = PBE8.ProductCode;
              oli4.Product_Family__c = 'UVS One Plan';
              oli4.First_Bill_Date__c = System.today();
              oli4.First_Bill_Date__c = System.today();
              oli4.Service_Ready_Date__c = System.today();
              //insert oli4;
       
            List<OpportunityLineItem> OLI = new List<OpportunityLineItem>();
          
            OLI.add(oli4);
            insert OLI;
            
           
           
            BookToBill__c b=new BookToBill__c();
            
           // RecordType R = [select Id,Name from RecordType where DeveloperName  = 'Revenue_Assurance_Audit'];
            RecordType R = [select Id,Name from RecordType where id  = '01220000000AMUl'];
     
      //ICTRTSCalls.add(R.Id); 
            
            system.debug('LLL KJJJJ '+R);
            
            b.Progress_Comments__c='test1';
            b.Opportunity__c=o.Id;
            b.ISDN2_Acquired_Lines__C=0;
            b.ISDN30_Acquired_Lines__c=0;
            b.PSTN_Acquired_Lines__c=0;
            b.IDC_Acquired_Lines__c=0;
            b.ISDN30_LLU_Lines__c=0;
            b.ISDN2_LLU_Lines__c=0;
            b.pstn_LLU_Lines__c=0;
            b.IDC_Other__c=0;
            b.Forecast_Annual_Growth__c=0.00;
            b.Gross_Annual_Growth__c = 0.00;
            b.RecordTypeId = R.id;
            b.Company_Name_on_the_AX_Legal_Entity__c = '';
            b.Volume_iPhone_New__c = '0';
            b.Total_Mobile_New__c = 0.00;
            b.Volume_Data_New__c = '0';
            b.Volume_Blackberry_New__c = '0';
            b.Volume_Voice_New__c = '0';
            
            Map < String, Decimal > OpptyRevenueSOV = new Map < String, Decimal > ();
            OpptyRevenueSOV.put(o.id,0.00);
            
            Map < String, String > OpptyName = new Map < String, String > ();
            OpptyName.put(o.id,'BookToBill');
            
            rtId = '01220000000AMUl'; 
          
            insert b;
            test.stopTest();
            ICTRTSCalls.add(b.RecordTYPEID); 
           
            BookToBill__c UpdateB = [select Id,Status__c,RecordTYPEID from BookToBill__c where Id=:b.id];
            
            ICTRTSCalls.add(UpdateB.RecordTYPEID);            
            
            UpdateB.Status__c = 'Project in Progress';
            UpdateB.Progress_Comments__c = 'test';
            UpdateB.New_Measurement__c = 'Yes';
            UpdateB.Project_Status__c = 'With Oneplan Team to Implement';
           
            
            Update UpdateB;
            
                        
          //  test.stoptest();
            
            BookToBill__c bb=new BookToBill__c();
            
            RecordType R1 = [select Id,Name from RecordType where DeveloperName  = 'Mobile_Workforce_Solutions'];
          //  RecordType R1 = [select Id,Name from RecordType where id  = '01220000000AMV1'];
      
          //ICTRTSCalls.add(R.Id); 
            
            system.debug('LLL KJJJJ '+R);
            
            bb.Progress_Comments__c='test2';
            bb.Opportunity__c=o.Id;
            bb.ISDN2_Acquired_Lines__C=0;
            bb.ISDN30_Acquired_Lines__c=0;
            bb.PSTN_Acquired_Lines__c=0;
            bb.IDC_Acquired_Lines__c=0;
            bb.ISDN30_LLU_Lines__c=0;
            bb.ISDN2_LLU_Lines__c=0;
            bb.pstn_LLU_Lines__c=0;
            bb.IDC_Other__c=0;
            bb.Forecast_Annual_Growth__c=0.00;
            bb.Gross_Annual_Growth__c = 0.00;
            bb.RecordTypeId = R1.id;
            bb.Company_Name_on_the_AX_Legal_Entity__c = '';
            bb.Volume_iPhone_New__c = '0';
            bb.Total_Mobile_New__c = 0.00;
            bb.Volume_Data_New__c = '0';
            bb.Volume_Blackberry_New__c = '0';
            bb.Volume_Voice_New__c = '0';
            bb.Expected_1st_Bill_Date_original__c = system.today();
            bb.Revenue_SOV__c = 0.00;
            bb.Mobile_Workforce_Volume__c = '100';
            Date d= Date.today();
            bb.First_Bill_Date__c= d.addDays(12);
            
            //Map < String, Decimal > OpptyRevenueSOV = new Map < String, Decimal > ();
            OpptyRevenueSOV.put(o.id,0.00);
            
            //Map < String, String > OpptyName = new Map < String, String > ();
            OpptyName.put(o.id,'BookToBill');
            
            rtId = '01220000000AMV'; 
          
            insert bb;
            
            ICTRTSCalls.add(b.RecordTYPEID); 
            
            BookToBill__c UpdateBB = [select Id,Status__c,RecordTYPEID from BookToBill__c where Id=:bb.id];
            
            ICTRTSCalls.add(UpdateBB.RecordTYPEID);            
            
            UpdateBB.Status__c = 'Project in Progress';
            UpdateBB.Progress_Comments__c = 'test';
            UpdateBB.New_Measurement__c = 'Yes';
            UpdateBB.Project_Status__c = 'With Oneplan Team to Implement';
            
      //test.stoptest();
            
            RecordType rr= [Select Id from RecordType where Name='Calls And Lines Acquisition BTBTeam'];
            BookToBill__c b2B= new BookToBill__c();
            b2B.RecordTypeId=rr.Id;
            b2B.Opportunity__c=o.Id;
            
            insert b2B;
    }
    }
    }