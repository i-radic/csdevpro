/*  ======================================================
    History                                                            
    -------                                                            
VERSION  AUTHOR             DATE              DETAIL                                 FEATURES
1.00     Dan Measures       13/06/2011        batch class used to delete old Forecast Portfolio Area records
		 John McGovern		13/02/2012		  Touched to change last updated away from inactive user
*/
global class BatchDeleteForecastPortfolioArea implements Database.Batchable<SObject>{
	private String query;

	global BatchDeleteForecastPortfolioArea(String q){
        this.query = q;
    }
    
   	global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
   		List<Forecasting_Portfolio_Area__c> fpa_for_delete = new List<Forecasting_Portfolio_Area__c>();
		for(Sobject c : scope){
			Forecasting_Portfolio_Area__c fpa = (Forecasting_Portfolio_Area__c)c;
			fpa_for_delete.add(fpa);
		}
		if(!fpa_for_delete.isEmpty())
			delete fpa_for_delete;
    }
    
	global void finish(Database.BatchableContext BC){
    }
}