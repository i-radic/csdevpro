@isTest
private class Test_createCRFAddress {

    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        
        addressDet.Address_Type__c = 'CeaseAddress';
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Address_Type__c);
        
        
        createCRFAddress createAddr = new createCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases2() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        
        addressDet.Address_Type__c = 'AlternateBillingAddress';
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Address_Type__c);
        
        createCRFAddress createAddr = new createCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases3() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        
        addressDet.Address_Type__c = 'ProvideAddress';
        
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Address_Type__c);
        
        createCRFAddress createAddr = new createCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases4() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        
        addressDet.AX_Address_Type__c = 'DeliveryAddress';
        
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.AX_Address_Type__c);
        
        createCRFAddress createAddr = new createCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases5() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        
        addressDet.AX_Address_Type__c = 'BillingAddress';
        
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.AX_Address_Type__c);
        
        createCRFAddress createAddr = new createCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases6() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        
        addressDet.AX_Address_Type__c = 'CorrespondenceAddress';
        
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.AX_Address_Type__c);
        
        createCRFAddress createAddr = new createCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
    static testMethod void runPositiveTestCases7() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        
        addressDet.Address_Type_NSO__c = 'ExistingAddress';
        
        PageReference PageRef = Page.createCRFAddress;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType1',addressDet.Address_Type_NSO__c);
        
        createCRFAddress createAddr = new createCRFAddress(addressDet); 
        
        
        createAddr.OnLoad();
        createAddr.Save();
        createAddr.Cancel();
        createAddr.refferalPage = 'new';
        createAddr.ReDirect(); 
        createAddr.refferalPage = 'update';
        createAddr.ReDirect();
        createAddr.refferalPage = 'detail';
        createAddr.ReDirect();  
        createAddr.crmAddress();
    }
}