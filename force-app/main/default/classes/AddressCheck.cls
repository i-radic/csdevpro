global class AddressCheck {
    webservice static String getAddresses (String basketId, String buildingName, String buildingNumber, String street, String town, String county, String postcode) {
        //String basketId 
        System.debug('It is in!');
        System.debug('*** basketId ' + basketId);

        //Id basketId = 'a7O3E000000563b';

        cscfga__Product_Basket__c basket = [SELECT Id, Name, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId LIMIT 1];
        System.debug('**** basket ');
        System.debug(basket);
        
        String buildingNameCondition = (buildingName == '') ? '' : 'Building_Name__c like \'%' + buildingName + '%\'';
        String buildingNumberCondition = (buildingNumber == '') ? '' : 'Building_Number__c like \'%' + buildingNumber + '%\'';
        String streetCondition = (street == '') ? '' : 'Street__c like \'%' + street + '%\'';
        String townCondition = (town == '') ? '' : 'Town__c like \'%' + town + '%\'';
        String countyCondition = (county == '') ? '' : 'County__c like \'%' + county + '%\'';
        String postcodeCondition = (postcode == '') ? '' : 'Post_Code__c like \'%' + postcode + '%\'';
        String opportunityIdCondition = (String.valueOf(basket.cscfga__Opportunity__c) == '') ? '' : 'BT_One_Phone__r.Opportunity__c = \'' + basket.cscfga__Opportunity__c + '\'';
       
        String conditions = '';

        conditions += (conditions != '' && buildingNameCondition != '') ? ' and ' : '';
        conditions += buildingNameCondition;
        conditions += (conditions != '' && buildingNumberCondition != '') ? ' and ' : '';
        conditions += buildingNumberCondition;
        conditions += (conditions != '' && streetCondition != '') ? ' and ' : '';
        conditions += streetCondition;
        conditions += (conditions != '' && townCondition != '') ? ' and ' : '';
        conditions += townCondition;
        conditions += (conditions != '' && countyCondition != '') ? ' and ' : '';
        conditions += countyCondition;
        conditions += (conditions != '' && postcodeCondition != '') ? ' and ' : '';
        conditions += postcodeCondition;
        conditions += (conditions != '' && opportunityIdCondition != '') ? ' and ' : '';
        conditions += opportunityIdCondition;
      
        String whereConditions = (conditions == '') ? '' : ' where ' + conditions;
        String query = 'SELECT Id, Name, Building_Name__c, Building_Number__c, Street__c, Town__c, County__c, Post_Code__c, BT_One_Phone__r.Opportunity__c FROM BT_One_Phone_Site__c ' + whereConditions;
        System.debug('Query : ' + query);
        system.debug('Getting address');
        List<BT_One_Phone_Site__c> sitesForOpportunity = Database.query(query);

        // get BTOP address from PC
        List<cscfga__Product_Configuration__c> pcList = [SELECT Id, Name, BT_One_Phone_Site__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId AND Product_Definition_Name__c = 'BTOP Site'];

        Set<Id> usedAddresses = new Set<Id>();

        for(cscfga__Product_Configuration__c pc : pcList) {
            if(pc.BT_One_Phone_Site__c != null) {
                usedAddresses.add(pc.BT_One_Phone_Site__c);
            }
        }

        System.debug('usedAddresses ' + usedAddresses);

        // list of BT One Phone Sites to be sent
        List<BT_One_Phone_Site__c> sitesToBeShown = new List<BT_One_Phone_Site__c>();

        for(BT_One_Phone_Site__c site : sitesForOpportunity) {
            if(!usedAddresses.contains(site.Id)) {
                sitesToBeShown.add(site);
            }
        }

        System.debug('***sitesToBeShown ' + sitesToBeShown);

        return JSON.serialize(sitesToBeShown);
    } 
}