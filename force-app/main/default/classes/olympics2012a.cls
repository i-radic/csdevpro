global class olympics2012a implements Messaging.InboundEmailHandler {
global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope envelope){

Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();

Messaging.InboundEmail.TextAttachment[] tAttachments = email.textAttachments;
Messaging.InboundEmail.BinaryAttachment[] bAttachments = email.BinaryAttachments;
String contents='';

if(bAttachments !=null){
for(Messaging.InboundEmail.BinaryAttachment btt : bAttachments){
if(btt.filename.endsWith('.csv')){
contents = btt.body.toString();
}
}
}

else if(tAttachments !=null){
for(Messaging.InboundEmail.TextAttachment ttt : tAttachments){
if(ttt.filename.endsWith('.csv')){
contents = ttt.body; 

}
}
}
system.debug('ADJ Attachment file ' + contents);
//parse attachment & load records
Id RTId = [Select Id from RecordType where DeveloperName=:'Olympics'].Id;

//Backoffic2 Sandbox
//List<CRF__c> ListCRFName = [Select Name,Recently_Created_Olympics__c from CRF__c where RecordTypeId=:RTId AND Opportunity__c=:'006P0000003V4Nk' Order by CreatedDate desc limit 1];
    
//Live
List<CRF__c> ListCRFName = [Select Name,Recently_Created_Olympics__c from CRF__c where RecordTypeId=:RTId AND Opportunity__c=:'0062000000KB0YI' Order by CreatedDate desc limit 1];
    
    String CRFName;

        if(ListCRFName.isEmpty()){    
        CRFName='Olympics-0'; 
    }
    else{    
        CRFName=ListCRFName[0].Recently_Created_Olympics__c;
    }
    List<String> parts = CRFName.split('-');
    Integer CRFNameSplit = Integer.valueOf(parts[1]);
    
List<CRF__c> CRFList = new List<CRF__c>(); // create list to object

contents = contents.replaceAll('\n','\t');
contents = contents.replaceAll('\t\t','\t');
//get separate lines
String[] splitted = new CsvReader(contents, '\t').readLine();
Integer LinesToParse;
    if(splitted.size() == 1){
        LinesToParse = (splitted.size()-0);//if there is a header record splitted.size()-1 else -0
    }
    else{   //if there is a header -2 else -1
        LinesToParse = (splitted.size()-1);
    }            
Integer ParseLine = 0;//if there is a header record 1 else 0
system.debug('split------> ' + splitted.size());
system.debug('split------> ' + splitted);
system.debug('ADJ LinesToParse initial 1 ' + LinesToParse);
     while(LinesToParse >0){
           String[] SubSplitted = new CsvReader(splitted[ParseLine]).readLine();
           system.debug('subsplit------> ' + SubSplitted);
           system.debug('subsplit------> ' + SubSplitted.size());
           CRF__c crf = new CRF__c();
            CRFNameSplit++;
            crf.Name='Olympics-'+CRFNameSplit;
            crf.RecordTypeId=RTId;
            
            //crf.Opportunity__c='006P0000003V4Nk';//Backoffic2 Sandbox
            crf.Opportunity__c='0062000000KB0YI';//Live
            
            crf.Request_Type__c = SubSplitted[0];
            crf.Contact_email_address__c = SubSplitted[1];
            crf.Contact_number__c = SubSplitted[2];
            //crf.Contact_number__c = SubSplitted[2];                
            crf.Alterative_contact__c = SubSplitted[3];                
            crf.Order_ID__c= SubSplitted[4];                
            crf.Connect_email__c= SubSplitted[5];                
            crf.Tel_number_for_Broadband_Service__c= SubSplitted[6];                                
            crf.Customer_Name__c= SubSplitted[7];
            crf.Customer_Contact_Number__c= SubSplitted[8];
            crf.Address_Location_description__c= SubSplitted[9];
            crf.Sub_location_sub_premises__c=SubSplitted[10];
            crf.Building_Name__c= SubSplitted[11];
            crf.Number__c= SubSplitted[12];
            crf.Street__c= SubSplitted[13];                           
            crf.Post_Town__c= SubSplitted[14];
            crf.County__c= SubSplitted[15];
            crf.Post_Code__c= SubSplitted[16];
            crf.PSTN_CRD__c= SubSplitted[17];
            crf.Appointment_date__c= SubSplitted[18];
            crf.TRC__c= SubSplitted[19];
            crf.ADSL_CRD__c= SubSplitted[20];
            crf.IP_address__c= SubSplitted[21];
            crf.PSTN_Term_MIn_months__c= SubSplitted[22];
            //crf.Incoming_Call_Barring_CSS_code__c = SubSplitted[23];
            //crf.Outgoing_Call_Barring_CSS_code__c = SubSplitted[24];
            crf.Hazard_and_warning_notes__c= SubSplitted[23];
            crf.Exhange_Group_Code__c= SubSplitted[24];
            crf.Generic_Notes__c= SubSplitted[25];
            crf.Router_Required__c= SubSplitted[26];
            
            //Constant Values
            
            crf.Product_required__c = 'BT Business Total Broadband Office unlimited';
            //crf.CPE__c = 'Router Required (Static IP address also needed)';
            crf.Security_Phrase__c = 'When do the Olympic games start';
            crf.Security_Answer__c = '27th July 2012';
            crf.Preferred_contact_Method__c = 'Email';
            crf.Payment_method_for_olympics__c = 'One Bill VP 12153270';
            crf.Billing_Address_for_olympics__c = 'PP1040 Telephone Exchange, 18-20 Bath Road, Bournemouth, Dorset. BH1 2NR';
            crf.password__c = 'weLCome2012';
            crf.Company_Name_for_olympics__c = '2012 BT LOCOG';
            crf.Affinity_ID__c = 'NR9';
            crf.ADSL_Term_MIn_months__c = '12';
            crf.Incoming_Call_Barring_CSS_code__c = 'ICB for Main Line (First line) : A13508';
            crf.Outgoing_Call_Barring_CSS_code__c = 'OCB for Main Line (First line) : A13507';
            if(crf.Request_Type__c == 'LOCOG')
                crf.Css_code__c = 'B36718';
            if(crf.Request_Type__c == 'RateCard')
                crf.Css_code__c = 'B36719';
            
            if(crf.Request_Type__c == 'LOCOG')
                crf.Maintenance__c = 'Prompt care';
            if(crf.Request_Type__c == 'RateCard')
                crf.Maintenance__c = 'TotalCare';  
            
            String RouterRequired = crf.Router_Required__c;
                        
            if(RouterRequired.contains('Yes'))
                crf.CPE__c='Router Required (Static IP address NOT needed)';
            if(RouterRequired.contains('No'))
                crf.CPE__c='Router Not Required';
                  
            crf.Billing__c = 'Monthly';
            crf.Number_allocation__c = 'auto-allocation';
            crf.Directory_Entry__c = 'NQR';
            crf.CRF_Status__c = 'New Order Received';
            
                CRFList.add(crf); 
                LinesToParse = LinesToParse -1;
                ParseLine = ParseLine +1;
                system.debug('ADJ LinesToParse countDown' + LinesToParse);
    system.debug('ADJ splitted ' + splitted[LinesToParse]);
    system.debug('ADJ subSplitted 0 ' + subsplitted[0]);
    system.debug('ADJ subSplitted 1 ' + subsplitted[1]);
    system.debug('ADJ subSplitted 2 ' + subsplitted[2]);
}


             if(CRFList.size() > 0){
                 insert CRFList;
                 //Backoffic2 Sandbox
                 //List<CRF__c> newList = [Select Name from CRF__c where RecordTypeId=:RTId AND Opportunity__c=:'006P0000003V4Nk' Order by CreatedDate desc limit 1];
                 
                 //Live
                 List<CRF__c> newList = [Select Name from CRF__c where RecordTypeId=:RTId AND Opportunity__c=:'0062000000KB0YI' Order by CreatedDate desc limit 1];                 
                 
                 newList[0].Recently_Created_Olympics__c = 'Olympics-'+CRFNameSplit;
                 UPDATE newList[0];
             }    




return result;
}
}