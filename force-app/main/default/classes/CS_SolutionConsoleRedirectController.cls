public class CS_SolutionConsoleRedirectController {
    public PageReference redirectToSolutionConsole() {
        Id basketId = ApexPages.currentpage().getparameters().get('basketId');
        final String smNamespace = 'cssmgnt';
        String host = (Test.isRunningTest()) ? 'https://btbusiness--csdevpro--csbb.cs173.visual.force.com' : System.URL.getSalesforceBaseURL().getHost(); // get host uri
        List<String> hostArr = host.split('\\.'); // split by .
        List<String> namespaceArr = hostArr[0].split('--'); // split first entry by --
        namespaceArr[2] = smNamespace; // replace third item in list, represents package namespace
        String newNamespace = String.join(namespaceArr, '--'); // join new namespace
        hostArr[0] = newNamespace; // add new namspace to original array
        String URI = String.join(hostArr, '.'); // join updated array
        PageReference pageRef = new PageReference('https://' + URI + '/apex/sceditor?basketId=' + basketId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}