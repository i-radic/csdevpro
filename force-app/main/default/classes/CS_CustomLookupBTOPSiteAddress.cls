global class CS_CustomLookupBTOPSiteAddress extends cscfga.ALookupSearch{

    public override String getRequiredAttributes(){
        return '["Address", "Term", "Pricing fee option"]';
    }

    public override Object[] doLookupSearch(Map<String, String>
            searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset,
            Integer pageLimit){
            
        return doSearch(searchFields);
    }
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields,String productDefinitionID){
             
        return doSearch(searchFields);
    }

    private List<BT_One_Phone_Site__c> doSearch(Map<String, String> searchFields) {
        String address = searchFields.get('Address');
        Integer term = String.isNotBlank(searchFields.get('Term')) ? Integer.valueOf(searchFields.get('Term')) : 1;
        String pricingFeeOption = searchFields.get('Pricing fee option');
        
        System.debug(searchFields);
        System.debug(term);
            
        BT_One_Phone_Site__c btOnePhoneSite = [
            SELECT      Id,
                        Name,
                        Building_Name__c,
                        Building_Number__c,
                        Street__c,
                        Post_Code__c,
                        County__c,
                        Town__c,
                        Additional_Site_Connection_Charge__c,
                        Private_Voice_Extra_One_Off_Charge__c,
                        Private_Voice_Extra_Monthly_Charge__c,
                        Private_Data_Extra_One_Off_Charge__c,
                        Private_Data_Extra_Monthly_Charge__c,
                        Additional_SIP_Network_Charge__c,
                        On_Site_SIP_Network_Ongoing_Charge__c,
                        Additional_OnSite_Mobile_Network_charges__c,
                        Addl_Onsite_MobileNetwork_Ongoing_Charge__c
                FROM    BT_One_Phone_Site__c
                WHERE   id = :address
        ];

        btOnePhoneSite.Additional_Site_Connection_Charge__c = btOnePhoneSite.Additional_Site_Connection_Charge__c == null ? 0 : btOnePhoneSite.Additional_Site_Connection_Charge__c;
        btOnePhoneSite.Private_Voice_Extra_One_Off_Charge__c = btOnePhoneSite.Private_Voice_Extra_One_Off_Charge__c == null ? 0 : btOnePhoneSite.Private_Voice_Extra_One_Off_Charge__c;
        btOnePhoneSite.Private_Voice_Extra_Monthly_Charge__c = btOnePhoneSite.Private_Voice_Extra_Monthly_Charge__c == null ? 0 : btOnePhoneSite.Private_Voice_Extra_Monthly_Charge__c;
        btOnePhoneSite.Private_Data_Extra_One_Off_Charge__c = btOnePhoneSite.Private_Data_Extra_One_Off_Charge__c == null ? 0 : btOnePhoneSite.Private_Data_Extra_One_Off_Charge__c;
        btOnePhoneSite.Private_Data_Extra_Monthly_Charge__c = btOnePhoneSite.Private_Data_Extra_Monthly_Charge__c == null ? 0 : btOnePhoneSite.Private_Data_Extra_Monthly_Charge__c;
        btOnePhoneSite.Additional_SIP_Network_Charge__c = btOnePhoneSite.Additional_SIP_Network_Charge__c == null ? 0 : btOnePhoneSite.Additional_SIP_Network_Charge__c;
        btOnePhoneSite.On_Site_SIP_Network_Ongoing_Charge__c = btOnePhoneSite.On_Site_SIP_Network_Ongoing_Charge__c == null ? 0 : btOnePhoneSite.On_Site_SIP_Network_Ongoing_Charge__c;
        btOnePhoneSite.Additional_OnSite_Mobile_Network_charges__c = btOnePhoneSite.Additional_OnSite_Mobile_Network_charges__c == null ? 0 : btOnePhoneSite.Additional_OnSite_Mobile_Network_charges__c;
        btOnePhoneSite.Addl_Onsite_MobileNetwork_Ongoing_Charge__c = btOnePhoneSite.Addl_Onsite_MobileNetwork_Ongoing_Charge__c == null ? 0 : btOnePhoneSite.Addl_Onsite_MobileNetwork_Ongoing_Charge__c;       

        if(String.isNotBlank(pricingFeeOption) && pricingFeeOption.equals('Recurring over the term')) {
            btOnePhoneSite.Additional_Site_Connection_Charge__c = btOnePhoneSite.Additional_Site_Connection_Charge__c / term;
            btOnePhoneSite.Private_Voice_Extra_Monthly_Charge__c += (btOnePhoneSite.Private_Voice_Extra_One_Off_Charge__c / term);
            btOnePhoneSite.Private_Data_Extra_Monthly_Charge__c += (btOnePhoneSite.Private_Data_Extra_One_Off_Charge__c / term);
            btOnePhoneSite.On_Site_SIP_Network_Ongoing_Charge__c += (btOnePhoneSite.Additional_SIP_Network_Charge__c / term);
            btOnePhoneSite.Addl_Onsite_MobileNetwork_Ongoing_Charge__c += (btOnePhoneSite.Additional_OnSite_Mobile_Network_charges__c / term);
            btOnePhoneSite.Private_Voice_Extra_One_Off_Charge__c = 0;
            btOnePhoneSite.Private_Data_Extra_One_Off_Charge__c = 0;
            btOnePhoneSite.Additional_SIP_Network_Charge__c = 0;
            btOnePhoneSite.Additional_OnSite_Mobile_Network_charges__c = 0;
        }
        
        return new List<BT_One_Phone_Site__c>{btOnePhoneSite};
    }
}