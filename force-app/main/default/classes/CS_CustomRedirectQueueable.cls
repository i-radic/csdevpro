public class CS_CustomRedirectQueueable implements Queueable {
    public cscfga__Product_Basket__c basket;
    public Map<Id, cscfga__Product_Configuration__c> configs;
    public String module;

    public CS_CustomRedirectQueueable(cscfga__Product_Basket__c basket, Map<Id, cscfga__Product_Configuration__c> configs, String module) {
        this.basket = basket;
        this.configs = configs;
        this.module = module;
    }

    public void execute (QueueableContext context) {
        if(module.equalsIgnoreCase('Configration')){
            CS_ProductBasketService.prepareTechFund(basket, configs, 'Tech_Fund__c');
            try{CS_Util.upsertReportConfigurationAttachment(configs.keySet());}catch(Exception exc){}
            //CS_Util.upsertReportConfigurationAttachment(configs.keySet());
            try{CS_ProductBasketService.calculateBasketTotals(configs, basket);}catch(Exception exc){}
            CS_ProductBasketService.setSpecialConditions(configs, basket);
            update basket;
        }else if(module.equalsIgnoreCase('Discount')){
            CS_ProductConfigurationService.calculateApprovals(CS_Util.getConfigurationsInBasket(basket.Id, 'CustomButtonBTDiscounting'), null, 'CustomButtonBTDiscounting', true);
        }
   }
}