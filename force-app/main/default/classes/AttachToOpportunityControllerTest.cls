/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2013-06-17 13:51:13 
 *	@description:
 *	    unit tests for AttachToOpportunityController
 *	
 *	Version History :   
 *		
 */
@isTest
public without sharing class AttachToOpportunityControllerTest {
	/**
	 * #801 - add attachment to Opportunity
	 * check behaviour if Project Id is missing, or Opportunity is blank
	 */
	static testMethod void testMissingProject () {
        TriggerDeactivateTest();
		AttachToOpportunityController con = new AttachToOpportunityController(new ApexPages.StandardController(new Project__c()));
		System.assert(con.getHasErrors(), 'Expected error because project Id is undefined');
		
		List<Project__c> objs = Mock.many('Project__c', 1, true); 
		con = new AttachToOpportunityController(new ApexPages.StandardController(objs[0]));

		System.assert(!con.getShowForm(), 'Input form should not be shown when project is blank. ');

	}

	static testMethod void testNoAttachment () {
		TriggerDeactivateTest();
		List<Project__c> objs = Mock.many('Project__c', 1, true); 
		Project__c project = objs[0];

		AttachToOpportunityController con = new AttachToOpportunityController(new ApexPages.StandardController(project));
		System.assert(!con.getHasErrors(), 'Did not expected any errors here');

		PageReference ref = con.attach();
		System.assertEquals(null, ref, 'Did not expect to leave Attachment page because attachment is blank');
	}
	/**
	 * #801 - add attachment to Opportunity
	 * check happy path. Project and Opportuity are provided and Attachment as well as Doc Type are specified.
	 */
	static testMethod void testWithAttachment () {
		TriggerDeactivateTest();
		List<Project__c> objs = Mock.many('Project__c', 1, true); 
		Project__c project = objs[0];

		AttachToOpportunityController con = new AttachToOpportunityController(new ApexPages.StandardController(project));

		con.attachment.Name = 'test.txt';
		con.attachment.body = Blob.valueOf('some text');
		con.attachment.ContentType = 'text/plain';
		con.documentType = con.documentTypeList[1].getValue();//element 0 is NONE, so need to take next one
		System.assert(!con.getAllowUserDefinedDocType(), 'User defined doc type must NOT be allowed');
		PageReference ref = con.attach();
		System.assertNotEquals(null, ref, 'Attachment should have been saved. ' + ApexPages.getMessages());

	}
	/**
	 * #801-note-22 - user defined document type
	 * check happy path. Project and Opportuity are provided and Attachment as well as Doc Type are specified.
	 * However, attachment type is 'Other' and user defined doc type is supplied
	 */
	static testMethod void testWithAttachmentAndUserDefinedDocType() {
		TriggerDeactivateTest();
		List<Project__c> objs = Mock.many('Project__c', 1, true); 
		Project__c project = objs[0];

		AttachToOpportunityController con = new AttachToOpportunityController(new ApexPages.StandardController(project));

		con.attachment.Name = 'test.txt';
		con.attachment.body = Blob.valueOf('some text');
		con.attachment.ContentType = 'text/plain';
		con.documentType = System.Label.Document_Type_Other;
		System.assert(con.getAllowUserDefinedDocType(), 'User defined doc type must be allowed');

		con.userDefinedDocType = 'my custom type';
		PageReference ref = con.attach();
		System.assertNotEquals(null, ref, 'Attachment should have been saved. ' + ApexPages.getMessages());
		System.assertEquals(1, [select count() from Attachment where Name like 'my custom type%'], 'Expected attachment name to start with user defined doc type');

	}
	/**
	 * #801-note-22 - user defined document type
	 * check sad path. Project and Opportuity are provided and Attachment as well as Doc Type are specified.
	 * Document Type is 'Other' and user defined doc type is NOT supplied
	 */
	static testMethod void testWithAttachmentAndUserUnDefinedDocType() {
		TriggerDeactivateTest();
		List<Project__c> objs = Mock.many('Project__c', 1, true); 
		Project__c project = objs[0];

		AttachToOpportunityController con = new AttachToOpportunityController(new ApexPages.StandardController(project));

		con.attachment.Name = 'test.txt';
		con.attachment.body = Blob.valueOf('some text');
		con.attachment.ContentType = 'text/plain';
		con.documentType = System.Label.Document_Type_Other;
		System.assert(con.getAllowUserDefinedDocType(), 'User defined doc type must be allowed');
		PageReference ref = con.attach();
		System.assertEquals(null, ref, 'Attachment should NOT have been saved. ' + ApexPages.getMessages());
	}
    
    static void TriggerDeactivateTest(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    }
    
}