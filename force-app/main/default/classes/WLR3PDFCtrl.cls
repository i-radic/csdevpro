public with sharing class WLR3PDFCtrl {

private CRF__c crf;
 
    public WLR3PDFCtrl(ApexPages.StandardController stdcontroller) {
     this.crf = (CRF__c)stdController.getRecord();
    }
    
    public String getNumbersList() {
    List<Numbers_to_be_Transferred__c> NumbersList = [select Number_to_be_transferred__c from Numbers_to_be_Transferred__c where related_to_CRF__c=:crf.Id];
    String UpdatedList;

    if(NumbersList.size()>0){
        for(Numbers_to_be_Transferred__c forNL : NumbersList ){
           If(UpdatedList==Null){
             If(forNL.Number_to_be_transferred__c!=NULL)
             UpdatedList=forNL.Number_to_be_transferred__c;
             }
           Else{
            If(forNL.Number_to_be_transferred__c!=NULL)
            UpdatedList = UpdatedList+', '+forNL.Number_to_be_transferred__c;   
            }     
        }                                                             
        return UpdatedList;                                             
    }
    else return null;
    
    }

}