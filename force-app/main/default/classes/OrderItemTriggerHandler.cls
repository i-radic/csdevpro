public without sharing class OrderItemTriggerHandler {

	final private String CONST_STATUS_DRAFT	= 'Draft';
	final private String CONST_STATUS_COMPLETE	= 'Complete';
	final private String CONST_STATUS_CANCELLED	= 'Cancelled';
	
    // Member Variables
    private Map<Id, OrderItem> mMapOrderItemNew;
    private Map<Id, OrderItem> mMapOrderItemOld;
    private List<OrderItem> mListOrderItemNew;
    private List<OrderItem> mListOrderItemOld;
    private Integer miTriggerSize;
    private Boolean mbIsInsert;
    private Boolean mbIsUpdate;
    private Boolean mbIsBefore;
    private Boolean mbIsAfter;
    private Boolean mbIsDelete;

	public String documentType {get; set;}
	public List<SelectOption> documentTypeList {get; private set;}
	final Map<String, String> fieldByLabel = new Map<String, String>();

    // Class Variables
    private static Boolean cbIsExecuting;
    @TestVisible private static Boolean cbIsFirstRun = true;
    
    // Constructor
    public OrderItemTriggerHandler(  Map<Id, OrderItem> pMapOrderItemNew, Map<Id, OrderItem> pMapOrderItemOld, 
    List<OrderItem> pListOrderItemNew, List<OrderItem> pListOrderItemOld,
                                Integer piTriggerSize,
                                Boolean pbIsInsert, Boolean pbIsUpdate, Boolean pbIsBefore, Boolean pbIsAfter,
                                Boolean pbIsExecuting,
                                Boolean pbIsDelete ){

        // Assign Member Variables  
        mMapOrderItemNew = pMapOrderItemNew;
        mMapOrderItemOld = pMapOrderItemOld;
        mListOrderItemNew = pListOrderItemNew;
        mListOrderItemOld = pListOrderItemOld;
        miTriggerSize   = piTriggerSize;
        mbIsInsert      = pbIsInsert;
        mbIsUpdate      = pbIsUpdate;
        mbIsBefore      = pbIsBefore;
        mbIsAfter       = pbIsAfter;
        mbIsDelete		= pbIsDelete;

        // Assign Class Variables
        cbIsExecuting   = pbIsExecuting;    
                                    
    }


   /*
    * @name         processTrigger
    * @description  entry point for the OrderItem trigger to process the OrderItem trigger class
    * @author       P Goodey
    * @date         Mar 2014
    * @see 
    */
    public void processTrigger() {

        // Process After Trigger       
        if(mbIsAfter){

	        system.debug('**********' + ' processTrigger() mbIsAfter');

	        // Prevent the trigger from re-entrency
	        if(cbIsFirstRun){
	        	
	        	system.debug('**********' + ' processTrigger() cbIsFirstRun');
	        	
				if(!mbIsDelete){
        
					// Create 1-to-1 Order Requests for Order Items
					createOrderRequest();

/*						
					// Create Order Level
					createNegativeOrderLevel();
*/					
					// Create Order Level
					createNegativeOrderLevelDynamic();
					
					// Stamp the Order Request onto the Otrder Item when it's inserted
					if( mbIsInsert ){
						updateOrderItemWithOrderRequestLookup();
					}
				//}	
				
				}else{
					// Use the OrderItem External Id to find the Order Levels to be deleted
					deleteOrderLevel();
				}
				
				// Stamp the Order with Order Item Product Names
				setOrderProductNamesFromOrderItems();

		        // Set our static to prevent transaction re-entrancy
		        cbIsFirstRun = false;
				
	        }
	    // Before Trigger            
        }else{

			if(mbIsDelete){

				system.debug('**********' + ' processTrigger() mbIsDelete');
	
				// Use the OrderItem External Id to find the Order Requests to be deleted
				deleteOrderRequest();
				
				// Use the OrderItem External Id to find the Order Levels to be deleted
				//deleteOrderLevel();
	
			}
        }

    }


   /*
    * @name         setOrderProductNamesFromOrderItems
    * @description  This sets the Order Item Product Names onto the Order record (using a multi-select picklist field)
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */ 
    private void setOrderProductNamesFromOrderItems() {  


        system.debug('**********' + ' entering setOrderProductNamesFromOrderItems');

        // Get the Order Ids from the Order Items that are being affected and place in a set
        set<Id> lSetOrderIds = new set<Id>();
        
        // Check for insert, update or delete
        if(mbIsDelete){
        	// After Delete
	        for(OrderItem oi : mListOrderItemOld){
	            lSetOrderIds.add(oi.OrderId);
	        }

        	system.debug('**********' + ' in the mbIsDelete in setOrderProductNamesFromOrderItems');
	        
	               	
        }else{
        	// After Insert or Update
	        for(OrderItem oi : mListOrderItemNew){
	            lSetOrderIds.add(oi.OrderId);
	        }
        }

        // Store the Order Id link to Order Item string (multiselect picklist used to store and split using ";" char)
        Map<Id, String> lMapOrderIdOrderItemProducts = new Map <Id,String>();
        
        // Used to store the working Product Name values
        String lsProductNames = '';


        // Loop thru the all the Order Items (all Status) that are being updated / inserted / deleted     
        for(OrderItem oi : [  SELECT Id, OrderId, Order.Id, Order.Order_Item_Products__c, PriceBookEntry.Product2.Name
                              FROM OrderItem 
                              WHERE OrderId IN :lSetOrderIds
                              ORDER BY CreatedDate ]){

            // First time the Order is being handled
            if( !lMapOrderIdOrderItemProducts.containsKey(oi.OrderId) ){                   
                // Start building the Order Item Product Name string
                lsProductNames = oi.PriceBookEntry.Product2.Name;
                lMapOrderIdOrderItemProducts.put(oi.OrderId, lsProductNames);
            }
            else{
                // Append to the the Order Item Product Name string
                String lsProductNamesMultiSelect = lMapOrderIdOrderItemProducts.get(oi.OrderId);
                lsProductNames = oi.PriceBookEntry.Product2.Name;     
                lMapOrderIdOrderItemProducts.put(oi.OrderId, lsProductNamesMultiSelect + ';' + lsProductNames);
            }
        }
                       

        // Finally loop thru the Order and update the Products field        
        list<Order> lLstOrdersToUpdate = new List<Order>();
        for(Order o : [   SELECT Id, Order_Item_Products__c
                          FROM Order
                          WHERE Id IN :lSetOrderIds]){
            // Check if the Order Item Products are changing
/*
            if( o.Order_Item_Products__c != lMapOrderIdOrderItemProducts.get(o.Id) ){
                // Stamp the Order
                o.Order_Item_Products__c = lMapOrderIdOrderItemProducts.get(o.Id);
                // Add to the List to update
                lLstOrdersToUpdate.add(o);            
            }
*/            

            system.debug('**********' + ' in the order for loop');

            // Check if we are deleting (using after delete)
            if( mbIsDelete ){
            	// Check if all Order Items have been removed
	            if( lMapOrderIdOrderItemProducts.get(o.Id) == null || lMapOrderIdOrderItemProducts == null ){
	            	o.Order_Item_Products__c = null;
                	system.debug('**********' + ' is delete = true, order item product is null');
	            }else{
	                o.Order_Item_Products__c = lMapOrderIdOrderItemProducts.get(o.Id);
                	system.debug('**********' + ' is delete = true, order item product is not null');
	            }
                // Add to the List to update
                lLstOrdersToUpdate.add(o);            
	            
	    	}else{
	            if( o.Order_Item_Products__c != lMapOrderIdOrderItemProducts.get(o.Id) ){
	                // Stamp the Order
	                o.Order_Item_Products__c = lMapOrderIdOrderItemProducts.get(o.Id);
	                // Add to the List to update
	                lLstOrdersToUpdate.add(o);            
	            }
	    	}
        }

         
        // If there are Order to update then make it so
        if( !lLstOrdersToUpdate.isEmpty() ){
            // Update as a batch
            update lLstOrdersToUpdate;
        }

    }



   /*
    * @name         createOrderRequest
    * @description  Creates an Order Request for every Inserted OrderItem (using upsert)
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private void createOrderRequest(){

		List<Order_Request__c> lListOrderRequest = new List<Order_Request__c>();
        // loop through the new updated Order Item records
 
        for( OrderItem oi : [	SELECT Id, OrderId, PriceBookEntry.Product2.Name, Quantity, Order.EffectiveDate, Order.AccountId, 
        						Status__c, Order.OrderNumber, Category__c
        										FROM OrderItem
												WHERE Id IN: mMapOrderItemNew.keyset() ]){
        	Order_Request__c lOr = new Order_Request__c();

			// Check if there is a valid Cystom Setting for the Product Name - This sets the Order Request Record Type
			Id lIdOrderRequestRecordTypeId = getRecordTypeForOrderRequest(oi.PriceBookEntry.Product2.Name);
        	if(lIdOrderRequestRecordTypeId==null){
        		String lsError = 'Unable to retrieve Order Request RecordTypeId for ' + 
        			oi.PriceBookEntry.Product2.Name + ' - please check the OrderRequest_RecordTypeSettings Custom Setting';
        		oi.addError(lsError);
        	}else{
	        	lOr.RecordTypeId = lIdOrderRequestRecordTypeId;
        	}

			lOr.Order_Product__c 	= oi.Id;
        	lOr.Name				= oi.Order.OrderNumber + ' ' + oi.PriceBookEntry.Product2.Name;
        	lOr.Company__c			= oi.Order.AccountId;
        	lOr.System_Order_Item__c	= oi.Id;
        	lOr.Order_Quantity__c	= oi.Quantity;
        	lOr.Order__c 			= oi.OrderId;
        	lOr.Status__c			= oi.Status__c;
        	lOr.Category__c			= oi.Category__c;
        	
        	// Dec 2014 Add Requested By
        	lOr.Requested_By__c		= UserInfo.getUserId();
        	
        	lOr.System_DateTime__c	= system.now();
        	
        	lListOrderRequest.add(lOr);
		}
		
        
		// If there Order Requests to process
        if( !lListOrderRequest.isEmpty() ){
        	try{

				// Upsert the list of Order Requests using the Order Item as the external id key
        		upsert lListOrderRequest System_Order_item__c;        	

        	} catch (Exception e) {

		        // Raise an Error for the User	
				lListOrderRequest[0].addError( e.getMessage() );
			}
        }
    }



   /*
    * @name         createNegativeOrderLevelDynamic
    * @description  Creates an Order Level record (for Order Requests) for every Inserted OrderItem (using upsert)
    * @author       P Goodey
    * @date         Dec 2014
    * @see			WR
    */   
    private void createNegativeOrderLevelDynamic(){

		List<Order_Level__c> lListOrderLevel = new List<Order_Level__c>();
		Map<Id, Order_Request__c> lMapOrderItemIdOrderRequest = new Map<Id, Order_Request__c>();
		
        // loop through existing Order Request records to get the Id
        for( Order_Request__c lOr : [	SELECT Id, Order_Product__c, Status__c FROM Order_Request__c WHERE Order_Product__c IN: mMapOrderItemNew.keyset() ]){
        	lMapOrderItemIdOrderRequest.put(lOr.Order_Product__c, lOr);
		}	
		
        // loop through the new updated Order Item records
        for( OrderItem oi : [	SELECT Id, OrderId, PriceBookEntry.Product2.Name, Quantity, Order.AccountId, Order.Type, Status__c
        										FROM OrderItem
												WHERE Id IN: mMapOrderItemNew.keyset() ]){

	        Order_Level__c lOl = new Order_Level__c();
	        lOl.System_Order_Item__c		= oi.Id;
	        	
	        // The quantity is negative to reduce the available Quantities 
	        lOl.Quantity__c					= oi.Quantity * -1;
	        	
	        // Constructs the simplified picklist to be selected from the Company Implementation record
	        // Example: 'Welcome Day | Chargeable Service'
	        lOl.Product_Order_Type__c		= oi.PriceBookEntry.Product2.Name + ' | ' + oi.Order.Type;
	        lOl.Company__c 					= oi.Order.AccountId;
	        lOl.Order_Request__c			= lMapOrderItemIdOrderRequest.get(oi.Id).Id;

			// Check the status of the Order Item	        	
	        if( (oi.Status__c == CONST_STATUS_CANCELLED) || (oi.Status__c == null) || (oi.Status__c == '') ){
	        	// Dynamic Rework
	        	lOl.put('Is_Quantity_To_Be_Summed__c', false);
	        }
	        else{
	        	// Dynamic Rework
	        	lOl.put('Is_Quantity_To_Be_Summed__c', true);	        		
	        }
	        lListOrderLevel.add(lOl);
		}

		// If there Order Level records to process
        if( !lListOrderLevel.isEmpty() ){
        	try{
        		upsert lListOrderLevel System_Order_item__c;        	
        	} catch (Exception e) {
		        // Raise an Error for the User	
				lListOrderLevel[0].addError( e.getMessage() );
			}
        }
    }





   /*
    * @name         updateOrderItemWithOrderRequestLookup
    * @description  Update the OrderItem and stamps it with the Order Request (using External Id)
    * @author       P Goodey
    * @date         Dec 2014
    * @see			WR
    */
	private void updateOrderItemWithOrderRequestLookup(){

		// List of Inserted Order Items
		List<OrderItem> lListOrderItem = new List<OrderItem>();

		// Id map Key = Order Item Id, Value = Order Request Id
		Map<Id, Id> lMapOrderItemIdOrderRequest = new Map<Id, Id>();
				
		// loop through existing Order Request records to get the Id
        for( Order_Request__c lOr : [	SELECT Id, Order_Product__c, Status__c FROM Order_Request__c WHERE Order_Product__c IN: mMapOrderItemNew.keyset() ]){
        	lMapOrderItemIdOrderRequest.put(lOr.Order_Product__c, lOr.Id);
		}		

        // loop through the new inserted Order Item records
        for( OrderItem oi : [	SELECT Id, OrderId, Order_Request__c, Status__c FROM OrderItem WHERE Id IN: mMapOrderItemNew.keyset() ]){
        	oi.Order_Request__c = lMapOrderItemIdOrderRequest.get(oi.Id);
        	lListOrderItem.add(oi);
        }
        
        // Update Order Items
		update lListOrderItem;
		
	}


   /*
    * @name         deleteOrderLevel
    * @description  Deletes an Order Level for every Deleted OrderItem (using External Id)
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private void deleteOrderLevel(){

		List<Order_Level__c> lListOrderLevel = new List<Order_Level__c>();		
		Set<String> sIds = new Set<String>();
        // loop through the new updated Order Item records
        for( Order_Level__c lOl : [	SELECT Id, System_Order_Item__c	
        						FROM Order_Level__c
								WHERE System_Order_Item__c IN: mMapOrderItemOld.keyset() ]){
        	lListOrderLevel.add(lOl);
		}
		// If there are Order Requests to process
        if( !lListOrderLevel.isEmpty() ){
        	try{
        		delete lListOrderLevel;        	
        	} catch (Exception e) {
		        // Raise an Error
				System.debug('***** Error in deleteOrderLevel(): '  + e.getMessage());
			}
        }
    }



   /*
    * @name         deleteOrderRequest
    * @description  Deletes an Order Request for every Deleted OrderItem (using External Id)
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private void deleteOrderRequest(){

		List<Order_Request__c> lListOrderRequest = new List<Order_Request__c>();
		Set<String> sIds = new Set<String>();
        
        // loop through the new updated Order Item records
        for( Order_Request__c lOr : [	SELECT Id, System_Order_Item__c	
        						FROM Order_Request__c
								WHERE System_Order_Item__c IN: mMapOrderItemOld.keyset() ]){
        	lListOrderRequest.add(lOr);
		}

		// If there are Order Requests to process
        if( !lListOrderRequest.isEmpty() ){
        	
        	//lListOrderRequest[0].addError( 'not deleting' );
        	try{
        		delete lListOrderRequest;        	
        	} catch (Exception e) {
		        // Raise an Error
				System.debug('***** Error in deleteOrderLevel(): '  + e.getMessage());
			}
        }
    }



   /*
    * @name         getRecordTypeForOrderRequest
    * @description  Gets the Record Type Id for the Order Request based on the Order Item Product (which is set using a JSON custom label)
    * @param		Product Name    
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private Id getRecordTypeForOrderRequest(string psProductName){
		Id lIdRecordTypeId;
 		Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            if (orRecordType.Product_Name__c == psProductName) {
            	String lsRecordTypeLabel = orRecordType.Record_Type_Label__c;
            	if(lsRecordTypeLabel!=null){
            		Schema.RecordTypeInfo sRctInfo = Schema.SObjectType.Order_Request__c.RecordTypeInfosByName.get(lsRecordTypeLabel);
            		if(sRctInfo!=null){
            			lIdRecordTypeId = sRctInfo.RecordTypeId;
            		}
	            	break;            		
            	}
            }
        }
		return lIdRecordTypeId;
	}


	
}