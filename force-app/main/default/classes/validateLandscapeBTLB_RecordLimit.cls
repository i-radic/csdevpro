public with sharing class validateLandscapeBTLB_RecordLimit{

    public Account account;
    public Landscape_BTLB__c landscape;    
       
    public validateLandscapeBTLB_RecordLimit(ApexPages.StandardController stdController) {

         landscape = (Landscape_BTLB__c)stdController.getRecord();
    }
     
    public validateLandscapeBTLB_RecordLimit() {
    }
    
   
   public PageReference OnLoad() { 
         String retUrl = ApexPages.currentPage().getParameters().get('retURL'); 
        system.debug('ADJ' + retURL);
         return OnLoad(retUrl.substring(1));
    }
   
    
    
	public PageReference OnLoad(string accountId){
	
		accountId = accountId.substring(0,15);  
        Account account = [SELECT Id, Name FROM Account WHERE Id = :accountId];
                
        List<Landscape_BTLB__c> landscapes = [SELECT Id FROM Landscape_BTLB__c WHERE Customer__c = :accountId];
        
        if (landscapes.size() > 0) {            
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Only one landscape permitted per account!'));    
        }                          
        else { 
        	string AccountName = account.Name.replaceAll('&','%26'); 
        	                
            PageReference PageRef = new PageReference('/a0b/e?CF00N20000002Azej='+AccountName+'&CF00N20000002Azej_lkid='+account.Id+'&retURL=%2F'+account.Id+'&nooverride=0');                                                                   
            PageRef.setRedirect(true);
            return PageRef;                   
        }  
        return null;    
    }
}