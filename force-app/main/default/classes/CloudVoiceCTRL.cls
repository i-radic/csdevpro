/*######################################################################################
PURPOSE
Controller for the Cloud Voice override edit VF page.
Used for actions to save documents and make updates back to the opportunity.
Used for updating opp and line items with values from CV record.

HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
public with sharing class CloudVoiceCTRL{

    ApexPages.StandardController Gcontroller;
    public Cloud_Voice__c cvRec;
    public CloudVoiceCtrl () { }

    public CloudVoiceCTRL (ApexPages.StandardController controller) { 
        Gcontroller = controller;
        //controller.addFields(new List<String>{'Opportunity__r.Main_Competitor__c'});
        this.cvRec= (Cloud_Voice__c)controller.getRecord();
    }
    
    private List<SelectOption> selOptions; 
    public String objID {get; set;}
    public String selectedProducts {get; set;}
    public String selPortType {get; set;}
    public String CV= System.currentPageReference().getParameters().get('id');
    public String purchasing{get; set;}
    public String provider{get; set;}
    public String term{get; set;}
    public String charge{get; set;}
    public String productsToAddCSV {get; set;}
    public String discountsCSV {get; set;}
    public String CP {get; set;}   
    public String billingAcc{get;set;}
    public String nameNum{get; set;}
    public String street{get; set;}
    public String townCity{get; set;}
    public String postCode{get; set;}
    public String NEWnameNum{get; set;}
    public String NEWstreet{get; set;}
    public String NEWtownCity{get; set;}
    public String NEWpostCode{get; set;}
    public String selLineAction{get; set;}
    public String cCSV {get; set;}
    public String pCSV {get; set;}
    public String rCSV {get; set;}
    public String fullRange {get; set;}
    public String rangeHolder {get; set;}
    public String companyName {get; set;}
    public String accountNumber {get; set;}
    public String registeredAddress {get; set;}
    public String registeredNumber {get; set;}
    public String requestersName {get; set;}
    public String requestersJobTitle {get; set;}
    public String requestersContactDetails {get; set;}
    public String associatedNumbersP {get; set;}
    public String associatedNumbersC {get; set;}
    public String associatedNumbersR {get; set;}
    public String otherNumbersP {get; set;}
    public String otherNumbersC {get; set;}
    public String otherNumbersR {get; set;}
    public String addInfo {get; set;}
    public String repOrder {get; set;}
    public String prodTel {get; set;}
    public String cpCompetitor {get; set;}
    public String cpAccessRef {get; set;}
    public String cpLineRef {get; set;}
    public String cpAddress {get; set;}
    public String flexOld {get; set;}
    
    public Date cpCRD {get; set;}

    public Integer Qty2 {get; set;}
    public Long StartNum {get; set;}
    public Long FinishNum {get; set;}
    public Integer pStartNum {get; set;}
    public Integer pFinishNum {get; set;}
    public Integer rStartNum {get; set;}
    public Integer rFinishNum {get; set;}
    public Integer cStartNum {get; set;}
    public Integer cFinishNum {get; set;}
    public Integer totalInRange {get; set;}
    public Integer totalAllocated {get; set;}
    public Integer Qty {get; set;}   

    public Boolean quoteRef = False;
    public Boolean refreshPage {get; set;}
    public Boolean prodNonBT {get; set;}
    public Boolean prodBT {get; set;}
    public Boolean prodOther {get; set;}
    public Boolean detailMissing{get; set;}
    public Boolean authOld{get; set;}


    public PageReference OnLoad() { 
        try{
            objID = System.currentPageReference().getParameters().get('id');
            selectedProducts = System.currentPageReference().getParameters().get('prodId');
            purchasing= System.currentPageReference().getParameters().get('finance');
            provider= System.currentPageReference().getParameters().get('provider');        
            term= System.currentPageReference().getParameters().get('term');
            charge= System.currentPageReference().getParameters().get('charge');
        }
        catch(exception e){}
        if(selectedProducts != null){        
            apply();
            PageReference retPg = page.CloudVoiceFinance;
            retPg.getParameters().put('ID',objID);
            return retpg.setRedirect(true);             
        }else if (provider != Null){
            cvRec.Finance_Provider__c = provider;
            cvRec.Finance_Term__c = Integer.valueOf(term);
            cvRec.Finance_Payment__c = Decimal.valueOf(charge);   
            update cvRec; 
            oppUpdate(); //update the opp with finance selection and values
            PageReference retPg = page.CloudVoiceFinance;
            retPg.getParameters().put('ID',objID);
            return retpg.setRedirect(true);          
        }
        return null;
    }
    public List<SelectOption> getConnection() {  
        selOptions= new List<SelectOption>();
        for (CSS_Products__c rt : [Select Id, Product_Name__c, Product_Summary__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'Connection' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200]) {
            selOptions.add(new SelectOption(rt.Id, rt.Product_Summary__c));
        }
        return selOptions;
    }
    public List<SelectOption> getCallPlan() {  
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('', 'None'));
        for (CSS_Products__c rt : [Select Id, Product_Name__c, Product_Summary__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'Call Plan' AND Grouping__c = 'UK' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200]) {
            selOptions.add(new SelectOption(rt.Id, rt.Product_Summary__c));
        }
        return selOptions;
    }
    public List<SelectOption> getPAYGflex() {  
		selOptions= new List<SelectOption>();
		selOptions.add(new SelectOption('No Flex','No Flex'));
		selOptions.add(new SelectOption('A','A'));
		selOptions.add(new SelectOption('B','B'));
		selOptions.add(new SelectOption('C','C'));
		selOptions.add(new SelectOption('D','D'));
		selOptions.add(new SelectOption('E','E'));
		selOptions.add(new SelectOption('F','F'));
		selOptions.add(new SelectOption('G','G'));
		return selOptions;
    }
    public List<SelectOption> getIntCallPlan() {  
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('', 'None'));
        for (CSS_Products__c rt : [Select Id, Product_Name__c, Product_Summary__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'Call Plan' AND Grouping__c = 'International' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200]) {
            selOptions.add(new SelectOption(rt.Id, rt.Product_Summary__c));
        }
        return selOptions;
    }
    public List<CSS_Products__c> getTraining() {  
        List<CSS_Products__c> rt = [SELECT Id, Name, Product_Name__c, Description__c, ShopLink__c, Connection_Charge__c, Discountable__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'Training' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC];
        return rt;
    }
    public List<Number_Port_Reference__c> getCPs() {
        List<Number_Port_Reference__c> rlist = [Select Id, Type__c,Reference__c,Description__c,Area__c, EstablishedIP__c FROM Number_Port_Reference__c WHERE Type__c = 'CP' ORDER BY Reference__c LIMIT 1000 ];
        return rlist ;
    }   
    public PageReference updateCV(){
        if(cvRec.RecordType.DeveloperName != 'Cloud_Voice_Modify'){
            if(!system.Test.isRunningTest() && cvRec.Admin_username__c.contains(' ')){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Admin Username should not have spaces.'));
                return null;
            }
        }
        PageReference retPg = Null;
        if(cvRec.ConnectionChargeID__c != null){
            CSS_Products__c pC = [SELECT Id, Name, Product_Name__c, Connection_Charge__c, Unit_Cost__c FROM CSS_Products__c WHERE Id = :cvRec.ConnectionChargeID__c LIMIT 1];
            cvRec.Connection_Charge__c = pC.Name;
            cvRec.Connection_Initial_Charge__c = pC.Connection_Charge__c;
            cvRec.Connection_Recurring_Charge__c = pC.Unit_Cost__c;
        }
        if(cvRec.SharerPlanId__c != null){
            CSS_Products__c pS = [SELECT Id, Name, Product_Name__c, Connection_Charge__c, Unit_Cost__c FROM CSS_Products__c WHERE Id = :cvRec.SharerPlanId__c LIMIT 1];
            cvRec.Sharer_Plan__c = pS.Name;
            cvRec.SharerPlan_Initial_Charge__c = pS.Connection_Charge__c;
            cvRec.SharerPlan_Recurring_Charge__c = pS.Unit_Cost__c;
        }
        if(cvRec.InternationalSharerPlanID__c != null){
            CSS_Products__c pSI = [SELECT Id, Name, Product_Name__c, Connection_Charge__c, Unit_Cost__c FROM CSS_Products__c WHERE Id = :cvRec.InternationalSharerPlanID__c LIMIT 1];
            cvRec.International_Sharer_Plan__c = pSI.Name;
            cvRec.International_SharerPlan_Initial_Charge__c = pSI.Connection_Charge__c;
            cvRec.Intl_SharerPlan_Recurring_Charge__c = pSI.Unit_Cost__c;
        }
        if(cvRec.SharerPlanId__c == null){
            cvRec.Sharer_Plan__c = '';
            cvRec.SharerPlan_Initial_Charge__c = null;
            cvRec.SharerPlan_Recurring_Charge__c = null;
        }
        if(cvRec.InternationalSharerPlanID__c == null){
            cvRec.International_Sharer_Plan__c = '';
            cvRec.International_SharerPlan_Initial_Charge__c = null;
            cvRec.Intl_SharerPlan_Recurring_Charge__c = null;
        }
system.debug('JMM flex: '+cvRec.UK_Sharer_PAYG_Flex__c);
system.debug('JMM flexOld: '+flexOld);
system.debug('JMM authOld: '+authOld);
		if(cvRec.UK_Sharer_PAYG_Flex__c == 'No Flex'){
			cvRec.UK_Sharer_PAYG_Flex_Authorised__c = false;
		}
		if(flexOld != cvRec.UK_Sharer_PAYG_Flex__c && authOld == cvRec.UK_Sharer_PAYG_Flex_Authorised__c){
			cvRec.UK_Sharer_PAYG_Flex_Authorised__c = false;
		}
        update cvRec;

        oppUpdate(); //update the opp with financials

        retPg = new PageReference('/' + cvRec.Id);
        retpg.setRedirect(true);
        return retPg;
    }

    public PageReference updateCVandCVS(){
        Profile checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];  
        Cloud_Voice_Site__c cvs = [SELECT Id,Customer_Required_Date__c FROM Cloud_Voice_site__c WHERE CV__c = :cvRec.Id LIMIT 1];

        if(checkProfile.Name!='Service Provision'){
            if ((cvRec.Bank_Sort_Code__c == Null || cvRec.Bank_Account_Number__c == Null) && cvRec.Billing_Account_Number__c == Null ) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must enter both the Bank Account Number and Sort Code, or the Existing Billing Account.'));
                return null;
            }
            
            if (cpCRD <= System.Today() && cpCRD != Null && cvs.Customer_Required_Date__c != cpCRD) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must select a Customer Required Date greater than today.'));
                return null;
            }
            
            if (cpAccessRef != Null && cpAccessRef != '') {
                if (cpAccessRef.startsWith('VOL011-') == false && cpAccessRef.startsWith('VOL012-') == false && cpAccessRef.startsWith('BT') == false ) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'You must enter the Order Reference in the correct format. Starting VOL011- or VOL012- or BT'));
                    return null;
                }
            }
        }
        Opportunity opp = [SELECT ID,Main_Competitor__c FROM Opportunity WHERE Id = :cvRec.Opportunity__c LIMIT 1];
        
        if(cpCompetitor != '' && cpCompetitor != Null){
          Competitors_Suppliers_Manufacturers__c rcCompID = [SELECT ID FROM Competitors_Suppliers_Manufacturers__c WHERE Name = :cpCompetitor LIMIT 1];
          opp.Main_Competitor__c = rcCompID.Id;
        }
        opp.zCPMissingDetails__c = false; //set flag for missing deatils  
        update opp;

        cvs.Access_Order_Reference__c = cpAccessRef;
        cvs.Access_Service_Id__c = cpLineRef;
        cvs.Use_Contact_Address_for_Site__c = false;
        cvs.Use_Site_Address_for_Delivery__c = false;
        cvs.Delivery_address__c = cpAddress;
        if (cpCRD != Null) {
            cvs.Customer_Required_Date__c = cpCRD;
        }
        cvs.Site_address__c = cpAddress;

        update cvs;
        update cvRec;

        PageReference retPg = Null;
        retPg = new PageReference('/' + cvRec.Id);
        retpg.setRedirect(true);
        return retPg;
    }    

    public List<Cloud_Voice_Site__c> getCVSPFin() {
        List<Cloud_Voice_Site__c> CVSites = [SELECT ID, Site_name__c, Site_Contact__r.Name, Site_Contact__r.Phone, Site_Contact__r.Email, Site_address__c, Total_Site_Recurring_Cost__c, Total_Site_One_Off_Cost__c, Total_Site_Licenses__c, Users__c, 
        Structured_Cabling_Summary__c,LAN_Summary__c, Billing_address__c, Delivery_address__c,Access_Concurrent_Calls__c,Access_Codec__c,Access_Codec_Name__c,Site_Discounts_Initial__c,Site_Discounts_Recurring__c,
            (SELECT ID, Product_name__c, Quantity__c, Unit_Cost__c, Total_Recurring_Cost__c, Total_One_Off_Cost__c, Cloud_Voice_Site__c, Cloud_Voice_Site__r.Site_name__c, Type__c,Image__c, Purchasing_Option__c,Discount_Initial__c FROM Site_Products__r WHERE Finance_Option__c = True ORDER BY Type__c, Product_Name__c) 
        FROM Cloud_Voice_Site__c WHERE CV__c = :CV ORDER BY Site_Name__c];
        return CVSites ;
    }
    public List<Cloud_Voice_Rate_Card__c> getRates() {
        List<Cloud_Voice_Rate_Card__c> rates= [SELECT Name, Provider__c, Range_End__c, Range_Start__c FROM Cloud_Voice_Rate_Card__c];
        return rates;
    }
    public List<Cloud_Voice_Rate_Card__c> getRates2() {
        List<Cloud_Voice_Rate_Card__c> rates= [SELECT Name, Provider__c, Range_End__c, Range_Start__c, x24m__c, x36m__c, x48m__c, x60m__c, x72m__c, x84m__c FROM Cloud_Voice_Rate_Card__c];
        return rates;
    }
    public List<SelectOption> getLineProducts() {  
      selOptions= new List<SelectOption>();
      //selOptions.add(new SelectOption('', 'None'));
      for (CSS_Products__c rt : [Select Id, Product_Name__c, Product_Summary__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'Line' AND Grouping__c = '' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200]) {
            selOptions.add(new SelectOption(rt.Id, rt.Product_Name__c));
      }  
      return selOptions;
    }   
    public PageReference finAll() {  
        List<Cloud_Voice_Site_Product__c> cvspRec = [SELECT Id, Purchasing_Option__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__r.CV__c = :objID AND Finance_Option__c = True ];

        for(Cloud_Voice_Site_Product__c p:cvspRec){
            p.Purchasing_Option__c = 'Finance';
        }
        update cvspRec;
        cvRec.Finance_Provider__c = Null;
        cvRec.Finance_Term__c = Null;
        cvRec.Finance_Payment__c = Null;      
        cvRec.Purchasing_Option__c = 'Finance';  
        update cvRec;
        PageReference retPg = page.CloudVoiceFinance;
        retPg.getParameters().put('ID',objID);
        return retpg.setRedirect(true);  
    }
    public PageReference sellAll() {  
        List<Cloud_Voice_Site_Product__c> cvspRec = [SELECT Id, Purchasing_Option__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__r.CV__c = :objID AND Finance_Option__c = True ];
        for(Cloud_Voice_Site_Product__c p:cvspRec){
            p.Purchasing_Option__c = 'Outright Sale';
        }
        update cvspRec;
        cvRec.Finance_Provider__c = Null;
        cvRec.Finance_Term__c = Null;
        cvRec.Finance_Payment__c = Null;      
        cvRec.Purchasing_Option__c = 'Outright Sale';      
        update cvRec;
        PageReference retPg = page.CloudVoiceFinance;
        retPg.getParameters().put('ID',objID);
        return retpg.setRedirect(true);  
    }
    public PageReference apply() {     
        List<Cloud_Voice_Site_Product__c> cvspRec = [SELECT Id, Purchasing_Option__c FROM Cloud_Voice_Site_Product__c WHERE Id = :selectedProducts];
        for(Cloud_Voice_Site_Product__c p:cvspRec){
            p.Purchasing_Option__c = purchasing;
        }
        update cvspRec;
        cvRec.Finance_Provider__c = Null;
        cvRec.Finance_Term__c = Null;
        cvRec.Finance_Payment__c = Null;      
        update cvRec;
        return null;
    }
    public PageReference applyMulti() {     
        Set<string> cvsProductIDSet = new Set<string>();
        List<string[]> discounts = new List<string[]>();    
        List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();       
        discountsCSV = discountsCSV.replace('undefined','');  
        List<String> csvList = discountsCSV.Split(',');
        for(string s : csvList){
            string[] csvVals = s.Split(':');
            string prodId = csvVals[0].substring(0,csvVals[0].length());
            string discVal = csvVals[1];
            cvsProductIDSet.Add(prodID);
            discounts.Add(new string[]{prodID, discVal});     
        }
        prodList = [SELECT ID, Purchasing_Option__c FROM Cloud_Voice_Site_Product__c WHERE ID in :cvsProductIDSet];            
        for (Cloud_Voice_Site_Product__c p: prodList){
            for (integer i = 0; i < prodList.Size(); i++){
                if(p.Id == discounts[i][0]){
                system.debug('JMM discounts[i][0]: '+ discounts[i][0]);
                system.debug('JMM p.Id: '+ p.Id);
                    p.Purchasing_Option__c = discounts[i][1];
               }     
            }
        }
        update prodList;

        PageReference retPg = page.CloudVoiceFinance;
        retPg.getParameters().put('ID',objID);
        return retpg.setRedirect(true); 
    }


public Cloud_Voice_Site__c newSite {
  get {
      if (newSite == null)
        newSite = new Cloud_Voice_Site__c();
      return newSite ;
    }
  set;
}
public Cloud_Voice_Site_Product__c newProd {
  get {
      if (newProd == null)
        newProd = new Cloud_Voice_Site_Product__c();
      return newProd ;
    }
  set;
}

public PageReference goAddTraining() {
    Integer trainI = 0;
    Integer trainR = 0;
    String trainStr ='<training>';
    String trainQuote ='';
    /*
    if (productsToAddCSV == '') {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add a Qty'));
        return null;
    }
    */

    List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();
    List<String> csvList = productsToAddCSV.Split(',');
    newSite.CV__c = cvRec.Id;
    newSite.Site_name__c = 'Training';
    newSite.Location_Code__c = String.valueOf(DateTime.now().getTime());
    insert newSite;

    for(String s : csvList){
        String[] csvVals = s.Split(':');           
        newProd = new Cloud_Voice_Site_Product__c();
        newProd.Cloud_Voice_Site__c = newSite.Id; 
        newProd.Quantity__c = Decimal.valueOf(csvVals[1]);
        newProd.SFCSSProdId__c = csvVals[0];
        if(Decimal.valueOf(csvVals[1]) > 0){
            prodList.Add(newProd);  
            trainStr += '<'+csvVals[0]+'>'+csvVals[1]+'</'+csvVals[0]+'>';
        }
    }
    trainStr += '</training>';
    if(prodList.Size() >= 1){
        try {
            insert prodList;
        } 
        catch (DMLException e) {
            ApexPages.addMessages(e);
        }       
    }
    Cloud_Voice_Site__c cvs = [SELECT Id, Total_Site_One_Off_Cost__c, Total_Site_Recurring_Cost__c FROM Cloud_Voice_site__c WHERE id = :newSite.Id];
    List<Cloud_Voice_Site_Product__c> cvsp = [SELECT Id, Name, Quantity__c, Product_Name__c, Total_One_Off_Cost__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :cvs.Id];
    for(Cloud_Voice_Site_Product__c p:cvsp){
        trainQuote += '<tr><td class="center">'+p.Quantity__c+'</td><td>'+p.Product_Name__c+'</td><td class="money">£'+p.Total_One_Off_Cost__c+'</td></tr>';
    }

    cvRec.Training_Initial_Charges__c = cvs.Total_Site_One_Off_Cost__c;
    cvRec.Training_Recurring_Charges__c = cvs.Total_Site_Recurring_Cost__c;
    cvRec.Training_Product_String__c = trainStr;
    cvRec.Training_Product_Quote__c = trainQuote;
    update cvRec;
    delete cvs;

    PageReference retPg = page.CloudVoiceTraining;
    retPg.getParameters().put('ID',cvRec.Id);
    return retpg.setRedirect(true); 
}

public PageReference editMultiAddress(){
    update cvRec;
    Cloud_Voice_Site__c cvs = [SELECT Id, CPE_Multiple_Delivery_Addresses__c FROM Cloud_Voice_site__c WHERE CV__c = :cvRec.Id LIMIT 1];  
    List<Cloud_Voice_Site_Product__c> cvsp = [SELECT Id, SFCSSProdId__c, Quantity__c, Product_Name__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :cvs.Id AND Type__c = 'CPE' AND Grouping__c = 'Phone'];  
    List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();

    if(cvs.CPE_Multiple_Delivery_Addresses__c != True){
        for(Cloud_Voice_Site_Product__c p:cvsp){
            for(Integer i = 0; i < p.Quantity__c; i++){
                Cloud_Voice_Site_Product__c newProd = new Cloud_Voice_Site_Product__c();
                newProd.Cloud_Voice_Site__c = cvs.Id; 
                newProd.Quantity__c = 1;
                newProd.SFCSSProdId__c = p.SFCSSProdId__c;
                prodList.Add(newProd);  
            }
        }
        delete cvsp;
        insert prodList;
    }
    cvs.CPE_Multiple_Delivery_Addresses__c = True;
    update cvs;

    PageReference retPg = Null;
    retPg = page.CloudVoiceMultiAddress;
    retPg.getParameters().put('cvID',cvs.Id);
    retPg.getParameters().put('accName',cvRec.Customer__c);
    retPg.getParameters().put('multiAdd','Yes');
    return retPg.setRedirect(true);     
}
    
public PageReference goFinish() {  
    PageReference retPg = new PageReference('/' + cvRec.ID);
    return retpg.setRedirect(true);
}
public List<SelectOption> getPortProduct() {  
    selOptions= new List<SelectOption>();
    selOptions.add(new SelectOption('Single','Single Line'));
    selOptions.add(new SelectOption('Multi','Multi Line'));
    return selOptions;
} 
public List<SelectOption> getLineAction() {  
    selOptions= new List<SelectOption>();
    selOptions.add(new SelectOption('Port','Port Entire Range'));
    selOptions.add(new SelectOption('Partial','Port/Cease/Retain'));
    return selOptions;

}public Number_Port_Line__c newLine {
  get {
      if (newLine == null)
        newLine = new Number_Port_Line__c();
      return newLine ;
    }
  set;
}

public Void populatePortDetail() {
    Number_Port_Line__c portLine;
    try{
        portLine = [SELECT Id, Cloud_Voice__c, Company_Name__c, Account_Number__c, Registered_Address__c, Registered_Number__c, Requesters_Name__c, Requesters_Job_Title__c, Requesters_Contact_Details__c FROM  Number_Port_Line__c WHERE Cloud_Voice__c =: CV ORDER BY Id DESC LIMIT 1];
    } catch(exception e){}
    if(portLine != null){
        companyName = portLine.Company_Name__c;
        accountNumber = portLine.Account_Number__c;
        registeredAddress = portLine.Registered_Address__c;
        registeredNumber = portLine.Registered_Number__c;
        requestersName = portLine.Requesters_Name__c;
        requestersJobTitle = portLine.Requesters_Job_Title__c;
        requestersContactDetails = portLine.Requesters_Contact_Details__c;
    }
    String attachPDF = System.currentPageReference().getParameters().get('attachPDF');
    if(attachPDF == 'y'){
        AttachNumPortToOppty(); // attach number port LOA to Oppty 
    }
}

public void AttachNumPortToOppty(){
    Cloud_Voice__c cv = [Select Id, Opportunity__c, Name FROM Cloud_Voice__c WHERE Id =: cvRec.Id];
    List<Attachment> attachments = [select Name FROM Attachment WHERE parentid =: cv.Opportunity__c];
    for(Attachment attachment : attachments){
        if(attachment.Name.Contains('Number Port')){
            delete attachment;
        }
    }
    pageReference pdf = Page.NumberPortLOA;
    pdf.getParameters().put('id', cv.Id);
    Attachment attach = new Attachment();
    Blob body;
    if(!Test.isRunningTest()){
        body = pdf.getContent();
    }
    attach.Body = body;
    attach.Name = 'Number Port '+cv.Name+'.pdf';
    attach.IsPrivate = false;
    attach.ParentId = cv.Opportunity__c;
    if(!Test.isRunningTest()){
        insert attach;
    }  
}

public PageReference addPortDetail() {  
    Long LineQty = 1; 
    String Range = Null;
    String sNum = '0'+ String.valueOf(StartNum);
    String fNum = sNum;
    if (selPortType == 'Single'){
        Range = sNum;
    }else{
        LineQty = FinishNum - StartNum + 1;
        fNum  = '0'+ FinishNum;
        Range = sNum + ' - '+ fNum ;
    }
   
    newLine.Cloud_Voice__c = CV ; // the record the file is attached to
    newLine.Required__c = LineQty;
    newLine.Product__c = selPortType + ' Port: ' + Range ;
    newLine.Start_Number__c = sNum ;
    newLine.Finish_Number__c = fNum;
    newLine.Losing_CP__c = CP;
    newLine.Gaining_CP__c = 'BT (CUPID: 001)';
    newLine.Main_Billing_Account__c = billingAcc;
    newLine.Current_Service_Post_Code__c = postCode;
    newLine.Port_Start_Number__c = '0'+ String.valueOf(pStartNum);
    newLine.Port_Finish_Number__c = '0'+ String.valueOf(pFinishNum);
    newLine.Port_Non_Sequential__c = pCSV;
    newLine.Retain_Start_Number__c = '0'+ String.valueOf(rStartNum);
    newLine.Retain_Finish_Number__c = '0'+ String.valueOf(rFinishNum);
    newLine.Retain_Non_Sequential__c = rCSV;
    newLine.Cease_Start_Number__c = '0'+ String.valueOf(cStartNum);
    newLine.Cease_Finish_Number__c = '0'+ String.valueOf(cFinishNum);
    newLine.Cease_Non_Sequential__c = cCSV;   
    newLine.Total_Lines_Required__c = cvRec.zTotalPortLines__c;
    newLine.Total_in_Range__c = totalInRange;
    newLine.Total_Allocated__c = totalAllocated;
    newLine.Full_Range__c = fullRange;
    newLine.Range_Holder__c = rangeHolder;
    newLine.Company_Name__c = companyName;
    newLine.Account_Number__c = accountNumber;
    newLine.Registered_Address__c = registeredAddress;
    newLine.Registered_Number__c = registeredNumber;
    newLine.Requesters_Name__c = requestersName;
    newLine.Requesters_Job_Title__c = requestersJobTitle;
    newLine.Requesters_Contact_Details__c = requestersContactDetails;
    newLine.Associated_Numbers_Port__c = associatedNumbersP;
    newLine.Associated_Numbers_Cease__c = associatedNumbersC;
    newLine.Associated_Numbers_Retain__c = associatedNumbersR;
    newLine.Other_Numbers_Port__c = otherNumbersP;
    newLine.Other_Numbers_Cease__c = otherNumbersC;
    newLine.Other_Numbers_Retain__c = otherNumbersR;
    newLine.Port_Type__c = selPortType;
    newLine.Multi_Line_Action__c = selLineAction;
    newLine.Additional_Information__c = addInfo;
    newLine.Non_BT_Products__c = prodNonBT;
    newLine.BT_Broadband_Products__c = prodBT;
    newLine.BT_Broadband_Products_Numbers__c = prodTel; 
    newLine.Other_BT_Products__c = prodOther; 

    try {
      insert newLine;
    } catch (DMLException e) {

      ApexPages.addMessages(e);
    } finally {
      newLine= new Number_Port_Line__c();
      Qty = Null; 
      selectedProducts = Null;
      CP= Null;
      startNum = Null;
      finishNum = Null;
    }  
    
    PageReference retPg = page.CloudVoiceNumberPort;
    retPg.getParameters().put('ID',cv );
    retPg.getParameters().put('postCode',postCode);
    retPg.getParameters().put('billingAcc',billingAcc);
    retPg.getParameters().put('attachPDF','y');
    return retPg.setRedirect(true);
}
public List<Number_Port_Line__c> getSelectedLine() {
    List<Number_Port_Line__c> prodList = [SELECT ID,Losing_CP__c, Account_Number__c, Full_Range__c,Total_in_Range__c,Total_Lines__c,Start_Number__c,Finish_Number__c FROM Number_Port_Line__c WHERE Cloud_Voice__c = :CV];
    return prodList;
}    
    public List<RatesWithFinanceRslt> getRatesWithFinance(){
        List<Cloud_Voice__c> cvRecList = [SELECT Id, Opportunity__c FROM Cloud_Voice__c WHERE Id =: cvRec.Id];
        List<RatesWithFinanceRslt> ratesWithFinanceList = new List<RatesWithFinanceRslt>();         
        List<Cloud_Voice_Rate_Card__c> ratesList = [SELECT Name, Provider__c, Range_End__c, Range_Start__c, x24m__c, x36m__c, x48m__c, x60m__c, x72m__c, x84m__c FROM Cloud_Voice_Rate_Card__c ORDER BY Provider__c];
        List<Leasing_History__c> financeRsltList = [SELECT Id, Opportunity__c, funder__c, clearance_graphic__c, Status__c FROM Leasing_History__c WHERE Opportunity__c =: cvRecList[0].Opportunity__c ORDER BY funder__c];                  
        Boolean matched = false;
        for(Cloud_Voice_Rate_Card__c rate : ratesList){     
            for(Leasing_History__c financeRslt : financeRsltList){                  
                if(financeRslt.funder__c.substring(0,2) == rate.Provider__c.substring(0,2)){
                    RatesWithFinanceRslt RatesWithFinanceRslt = new RatesWithFinanceRslt();
                    RatesWithFinanceRslt.financeRslt = financeRslt;
                    RatesWithFinanceRslt.rateCard = rate;
                    ratesWithFinanceList.Add(RatesWithFinanceRslt);
                    matched = true;
                }
            }
            if(!matched){
                RatesWithFinanceRslt RatesWithFinanceRslt = new RatesWithFinanceRslt();
                RatesWithFinanceRslt.rateCard = rate;
                ratesWithFinanceList.Add(RatesWithFinanceRslt);
            }
            matched = false;
        }
        return ratesWithFinanceList;
    }
    
    public class RatesWithFinanceRslt {
        public Leasing_History__c financeRslt {get; set;}
        public Cloud_Voice_Rate_Card__c rateCard {get; set;}
    }
    
    public PageReference goFinanceIndicator() {  
        List<Cloud_Voice__c> cvRecList = [SELECT Id, Opportunity__c FROM Cloud_Voice__c WHERE Id =: cvRec.Id];
        PageReference retPg = new PageReference('/apex/LeaseDeskAccountMatch?id=' + cvRecList[0].Opportunity__c);
        return retpg.setRedirect(true);
    }
    public PageReference oppUpdateQuote() {  
        system.debug('JMM quoteRef Q:'+quoteRef);
        quoteRef = True;
        oppUpdate();
        return null;
    }       
    public PageReference oppUpdate() {  
        Decimal oppTotal = 0;
        Integer conTerm = 0;        
        Cloud_Voice__c cv = [SELECT zOpportunityLineItem__c, zOpportunityLineItemLease__c, Total_1st_Year_Charges__c,Total_Selected_Finance__c,Total_Recurring_Charges__c,Contract_Term__c, Opportunity__r.Account_Sector__c FROM Cloud_Voice__c WHERE Id =: cvRec.Id];
        OpportunityLineItem oliT = [SELECT UnitPrice FROM OpportunityLineItem WHERE Id = :cv.zOpportunityLineItem__c];
        OpportunityLineItem oliL = [SELECT UnitPrice FROM OpportunityLineItem WHERE Id = :cv.zOpportunityLineItemLease__c];

        oppTotal = (cv.Total_1st_Year_Charges__c-cv.Total_Selected_Finance__c);

        if(cv.Contract_Term__c != Null){
            conTerm = Integer.valueOf(cv.Contract_Term__c.substring(0,2));
        }
        String sector = cv.Opportunity__r.Account_Sector__c;
        if(sector != Null){
            sector = sector.toUpperCase();
        }
        system.debug('JMM conTerm:'+conTerm);
        if(conTerm > 12 && sector == 'BT LOCAL BUSINESS'){
            oppTotal = oppTotal + (cv.Total_Recurring_Charges__c*(conTerm-12));
        }
        oliT.UnitPrice = oppTotal;
        oliL.UnitPrice = cv.Total_Selected_Finance__c;

        update oliT;
        update oliL;

        if(quoteRef){
            system.debug('JMM 1:');
            PageReference retPg = page.CloudVoiceQuote;
            retPg.getParameters().put('ID',cvRec.Id);
            return retpg;
        }else{
            system.debug('JMM 2:');
            quoteRef = False;
            PageReference retPg = new PageReference('/' + cvRec.Id);
            return retpg.setRedirect(true);
        }
    } 
    public List<Cloud_Voice_Site_Product__c> getSelectedALL() {
        Set<String> selProds= new Set<String>();
        Cloud_Voice_Site__c cvs = [SELECT Id FROM Cloud_Voice_Site__c WHERE CV__c =:cvRec.Id];

        List<Cloud_Voice_Site_Product__c> prodList = [SELECT ID, Product_Name__c, Quantity__c, Type__c, Unit__c, A_Code__c, Unit_Cost__c, Total_One_Off_Cost__c, Total_Recurring_Cost__c, Connection_Charge__c, LastModifiedBy.Name, LastModifiedDate, Locked_Edit__c, Locked_Delete__c,Purchasing_Option__c, Number_Info__c, rcSTD__c, rcDirectory__c, Discount_Initial__c,Discount_Recurring__c,Finance_Option__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :cvs.ID ORDER BY Type__c, Product_Name__c]; 
        for (Cloud_Voice_Site_Product__c p:prodList){
            selProds.add(p.Product_Name__c);
        }
        return prodList;
    }  
}