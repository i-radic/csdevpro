global class CS_CACustomerPreferences {
	webService static String getRecords() {
		List<CS_Question__c> questionObjs = [
			select
				Id,
				Question_Text__c,
				Answers__c
			from CS_Question__c
		];

		if (questionObjs.isEmpty()) {
			return null;
		}

		List<Question> questions = new List<Question>();
		for (CS_Question__c q : questionObjs) {
			questions.add(new Question(q));
		}

		return JSON.serialize(questions);
	}

	webService static Boolean saveAnswers(String aId, String answersByQuestionIdString) {
		Map<String, String> answersByQuestionId = (Map<String, String>) JSON.deserialize(
			answersByQuestionIdString,
			Map<String, String>.class
		);

		CSCAP__ClickApprove_Approver__c approver = [
			select
				Id,
				CSCAP__Customer_Approval__c,
				CSCAP__Customer_Approval__r.CSCAP__Opportunity__c
			from CSCAP__ClickApprove_Approver__c
			where Id = :aId
		];

		if (approver == null) {
			return false;
		}

		String oppId = approver.CSCAP__Customer_Approval__r.CSCAP__Opportunity__c;
		if (oppId == null) {
			return false;
		}

		List<csclm__Agreement__c> agreements = [
			select Id, CreatedDate
			from csclm__Agreement__c
			where csclm__Opportunity__c = :oppId
			order by CreatedDate desc
		];

		if (agreements.isEmpty()) {
			return false;
		}

		String agreementId = agreements[0].Id;

		List<CS_Question_Agreement_Association__c> associations = new List<CS_Question_Agreement_Association__c>();
		Integer i = 1;
		for (String qId : answersByQuestionId.keySet()) {
			associations.add(new CS_Question_Agreement_Association__c(
				Name = 'Question ' + i,
				Agreement__c = agreementId,
				Question__c = qId,
				Answer__c = answersByQuestionId.get(qId)
			));

			i++;
		}

		insert associations;

		return true;
	}

	private class Question {
		private String id { get; set; }
		private String question { get; set; }
		private String answers { get; set; }

		private Question(CS_Question__c q) {
			this.id = q.Id;
			this.question = q.Question_Text__c;
			this.answers = q.Answers__c;
		}
	}
}