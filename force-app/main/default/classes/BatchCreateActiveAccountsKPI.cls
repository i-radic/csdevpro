global class BatchCreateActiveAccountsKPI implements Database.Batchable<sObject>, Database.Stateful
{
    global map<date, set<ID>> dateMap   = new map<date, set<ID>>(); 
        
    GlobalKPISettings__c gs = GlobalKPISettings__c.getInstance();
    GlobalKPI_Helper gh = new GlobalKPI_Helper();
    public string businessName  = gs.Business_Unit__c; 
    public string busCode       = gs.Business_Unit_Code__c; 
    public string theQuery;
    public string periodCode;
    public string periodType;
    public string periodObjectFieldType;
    public string thisPeriod; 
    public string subsector = 'TIKIT'; 

    global BatchCreateActiveAccountsKPI (string period)
    {
        
        GlobalKPI_Helper gh = new GlobalKPI_Helper();
            
        thisPeriod = period;
        if (thisPeriod=='MONTH')
        {
            periodCode              ='MN';
            periodType              ='Month';
            periodObjectFieldType   ='Month';
        }
        else if (thisPeriod=='FISCAL_QUARTER')
        {
            periodCode              ='FQ';
            periodType              ='Fiscal Quarter';
            periodObjectFieldType   ='Quarter';
        }
        else if (thisPeriod=='FISCAL_YEAR')
        {
            periodCode              ='FY';
            periodType              ='Fiscal Year';
            periodObjectFieldType   ='Year';
        }
        
        //by month
        if (thisPeriod=='MONTH')
        {
            //can't do aggregate soql queries in multiple batches at time of writing, so use standard query and roll up
            theQuery = 'select opportunity.account.ID, createdDate from OpportunityHistory where opportunity.Account_Sub_Sector__c =  \'TIKIT\' and (createdDate = LAST_N_MONTHS:5 or createdDate=THIS_MONTH) ';
           // theQuery = 'select opportunity.account.ID, createdDate from OpportunityHistory where  (createdDate = LAST_N_MONTHS:5 or createdDate=THIS_MONTH) ';
            
            Date map_start_date = date.valueOf([Select startDate From Period Where type = :periodObjectFieldType and StartDate = THIS_MONTH].startDate ).addMonths(-5); //six months ago
            Date map_end_date   = [Select endDate From Period Where type = :periodObjectFieldType and StartDate = THIS_MONTH].endDate ;
            system.debug(map_start_date);
            system.debug(map_end_date);
            for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(1)  )
            {
                set<id> emptySet = new set<id>();
                dateMap.put(d, emptySet);
                
            }
            system.debug(dateMap);
            
        }
        else if(thisPeriod=='FISCAL_QUARTER')
        {
            //can't do aggregate soql queries in batches at time of writing, so use standard query and roll up
            theQuery = 'select opportunity.account.ID, createdDate from OpportunityHistory where opportunity.Account_Sub_Sector__c = \'TIKIT\' and (createdDate = LAST_N_FISCAL_QUARTERS:3 or createdDate=THIS_FISCAL_QUARTER) ';
            
            Date map_start_date = date.valueOf([Select startDate From Period Where type = :periodObjectFieldType and StartDate = THIS_FISCAL_QUARTER].startDate ).addMonths(-9); //3 quarters ago
            Date map_end_date   = [Select endDate From Period Where type = :periodObjectFieldType and StartDate = THIS_FISCAL_QUARTER].endDate ;
            system.debug(map_start_date);
            system.debug(map_end_date);
            for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(3)  )
            {
                set<id> emptySet = new set<id>();
                dateMap.put(gh.getPeriodStartDate(periodType, d), emptySet);
                
            }
            system.debug(dateMap);
            
        }
        else if(thisPeriod=='FISCAL_YEAR')
        {
            //can't do aggregate soql queries in batches at time of writing, so use standard query and roll up
            theQuery = 'select opportunity.account.ID, createdDate from OpportunityHistory where  opportunity.Account_Sub_Sector__c = \'TIKIT\' and (createdDate = LAST_FISCAL_YEAR Or createdDate=THIS_FISCAL_YEAR) ';
            
            Date map_start_date = date.valueOf([Select startDate From Period Where type = :periodObjectFieldType and StartDate = LAST_FISCAL_YEAR].startDate ); //3 quarters ago
            Date map_end_date   = [Select endDate From Period Where type = :periodObjectFieldType and StartDate = THIS_FISCAL_YEAR].endDate ;
            system.debug(map_start_date);
            system.debug(map_end_date);
            for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(12)  )
            {
                set<id> emptySet = new set<id>();
                dateMap.put(gh.getPeriodStartDate(periodType, d), emptySet);
                    
            }
            system.debug(dateMap);
            
        }
        else
        {
            return;
        }
        
        system.debug(theQuery);
        system.debug ('*****************************BatchCreateActiveAccountsKPI'); 
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug ('*****************************BatchCreateActiveAccountsKPI QueryLocator() method');
        system.debug(theQuery);     
        return Database.getQueryLocator(theQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.debug(dateMap);
        
        system.debug ('*****************************BatchCreateActiveAccountsKPI Execute() method');    
        GlobalKPI_Helper gh = new GlobalKPI_Helper();
        decimal thisAmount;
        for(sObject s : scope) //loop through rows of scope list
        {
            OpportunityHistory o = (OpportunityHistory) s;
            system.debug(o.createdDate);
            set<id> existingSet = new set<id>();
            existingSet = dateMap.get(gh.getPeriodStartDate(periodType, o.createdDate.date()));
            existingSet.add(o.opportunity.account.ID);
            dateMap.put(gh.getPeriodStartDate(periodType, o.createdDate.date()), existingSet);  //write distinct account IDs to the set
        
        }
        system.debug(dateMap);
        
        
        
    }    
    
        
    global void finish(Database.BatchableContext BC)
    {  
        
        system.debug ('*****************************BatchCreateActiveAccountsKPI Finish() method');     
        
        string dtFrom;
        string dtTo;
        
        
        decimal lostValue;
        decimal wonValue;
        
        for (date d : dateMap.keySet())
        {
            Global_KPI__c kpi           = new Global_KPI__c();
            kpi.business_unit__c        = businessName;
            kpi.Measure_Name__c         = 'Total Active Customer Count';
            kpi.Unit_of_Measure__c      = 'Count';
            kpi.Date_From__c            = d;
            dtFrom                      = string.valueOf(d);
            kpi.Date_To__c              = [Select endDate From Period Where type = :periodObjectFieldType and StartDate <= :d and endDate >= :d].endDate;
            dtTo                        = string.valueOf(kpi.Date_To__c) ;
            kpi.uniqueKey__c = busCode + ':' + 'ACT' + ':' + periodCode + ':' + dtFrom.replace('-','') + ':' + dtTo.replace('-','');  
            kpi.Period_Type__c          = periodType;
                        
            kpi.result__c = dateMap.get(d).size();
            
            
            //insert or update it using external ID field uniqueKey__c as the key
            system.debug(kpi);
            upsert kpi uniqueKey__c;
        }
            
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                         FROM AsyncApexJob WHERE Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.     
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BatchCreateActiveAccountsKPI ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }


}