@isTest
private class TestBtlbTeamOppyCtrl {
    
    static testMethod void firstTest() {
        Test_Factory.SetProperty('IsTest', 'yes');
 
        Account dummyAccount = Test_Factory.CreateAccountForDummy();
        insert dummyAccount;
        
        testData tt = new testData();
        tt.createADP();
        tt.createOpp();
        
        PageReference pageRef = Page.ADPOppertunities;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', tt.testADP.Id);        
        
        ApexPages.StandardController stc = new ApexPages.StandardController(tt.testADP);
        AdpOppyCtrl ADPOpp = new AdpOppyCtrl(stc);
        
        ADPOpp.getOpportunities();          
    }
    
    
    public class testData {
    
        public Account testAccount = null;
        public ADP__c testADP = null;
        public Opportunity testOpp = null;
        
        public void createAccount() {
            String SACCODE = 'testSAC';
            String LOB='LOB';
            
            Account testAcc2 = Test_Factory.CreateAccount();
            testAcc2.name = 'TESTCODE 2';
            testAcc2.sac_code__c = SACCODE;
            testAcc2.Sector_code__c = 'CORP';
            testAcc2.LOB_Code__c = LOB;
            Database.SaveResult[] accResult2 = Database.insert(new Account[] {testAcc2});
            
            testAccount = [Select id, Name from Account where Id= :accResult2[0].getId()];      
        }
        
        public void createADP() {
            if(testAccount == null)createAccount();
            ADP__c thisADP = new ADP__c(Customer__c = testAccount.Id);
            
            Database.SaveResult[] adpRes = Database.insert(new ADP__c[] {thisADP});
            testADP = [Select id, Name from ADP__c where Id= :adpRes[0].getId()];
        }
        
        public void createOpp() {
            if(testAccount == null)createAccount();
            Opportunity thisOpp = Test_Factory.CreateOpportunity(testAccount.Id);
            
            Database.SaveResult[] oppRes = Database.insert(new Opportunity[] {thisOpp});
            testOpp = [Select id, Name from Opportunity where Id= :oppRes[0].getId()];
        }
    }
}