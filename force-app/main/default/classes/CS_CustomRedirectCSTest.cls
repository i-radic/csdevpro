@isTest
public class CS_CustomRedirectCSTest  {
    private static ApexPages.StandardController controller;
    private static String configId = '';
    private static CS_CustomRedirectCS mainController = null;
    private static cscfga__Product_Configuration__c productConfig = null;
    private static cscfga__Product_Basket__c basket1  = null;
    
    private static cscfga__Product_Basket__c basket;
	private static cscfga__Product_Configuration__c pc;
	private static Map<Id, cscfga__Product_Configuration__c> configsMap;
	private static Map<Id, cscfga__Attribute_Definition__c> attDefsMap;
	private static List<cscfga__Product_Configuration__c> pcsList;
	private static Map<Id, cscfga__Product_Definition__c> prodDefMap;
	
	private static Map<String, Object> payload;
	
	private static Account acct;
    private static Account acct1;
    private static Opportunity opp;
    private static Opportunity opp1;
    private static Contact con;
    private static Contact agent;
//	private static cscfga__Product_Basket__c basket;
	private static cscfga__Product_Basket__c basket2;
	private static Map<Id, cscfga__Product_Configuration__c> allConfigs;
	
	
	 static testMethod void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();

        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
		cspl__PLReportSettings__c setting = new cspl__PLReportSettings__c(
            cspl__Contract_Term_Field__c = 'cscfga__Contract_Term__c',
            cspl__API_Field_name__c = 'Calculations_Product_Group__c',
            cspl__NPV_Factor__c = 1
        );
        insert setting;
        
        CS_TestDataFactory.generatePLReportConfigRecords();
		
        acct = new Account(name='test account');
        insert acct;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        os.SetupOwnerId = UserInfo.getUserId();
        insert os;
		
        CS_Approval_Level__c testapproval = new CS_Approval_Level__c(
        
         Type__c='Acquisition',
         Is_Active__c= true,
         KPI__c='Gross Margin',
         Approval_Level__c =6
        );
        
        insert testapproval;
        
         CS_Approval_Level__c testapproval1 = new CS_Approval_Level__c(
        
         Type__c='Re-sign',
         Is_Active__c= true,
         KPI__c='Revenue Dilution',
         Approval_Level__c =5
        );
        
        insert testapproval1;
        
        CS_Approval_Level__c testapproval2 = new CS_Approval_Level__c(
        
         Type__c='SME',
         Is_Active__c= true,
         KPI__c='Investment Pot Usage Percentage',
         Approval_Level__c =7
        );
        insert testapproval2;
        
        opp = new Opportunity(AccountId=acct.Id,StageName='Prospecting',CloseDate=date.today(),NextStep='Test',Next_Action_Date__c=date.today(),name='test OpportunityCI' ,
              TotalOpportunityQuantity = 0);
        insert opp;
        
        Usage_Profile__c testUsageProfiles = new Usage_Profile__c(
          Active__c = true,
          Account__c = acct.Id,
          Expected_SMS__c = 10,
          Expected_Voice__c = 5,
          Expected_Data__c = 10
        );
    
        insert testUsageProfiles;
        
        basket = new cscfga__Product_Basket__c(
          Name = 'Test Basket Trigger',
          cscfga__Opportunity__c = opp.Id,
          ReSign__c = true
        );
        
        basket.Approval_Status__c = 'Commercially Approved';
        basket.Has_Special_Conditions__c = false;
        basket.Competitive_Benchmarking__c = false;
        basket.Unlocking_Fees__c = false;
        basket.Total_Contract_Guarantee__c = false;
        basket.Total_Revenue_Guarantee__c = false;
        basket.Competitive_Clause__c = false;
        basket.Disconnect_Allowances__c = false;
        basket.Averaging_Co_Terminus__c = false;
        basket.Full_Co_Terminus__c = false;
        basket.Special_Conditions_Id__c = null;
        basket.Tenure__c = 24;
        basket.Care_Selected__c = false;
        basket.Payment_Terms__c = '45';
        basket.Monthly_Recurring_Charges__c = 10;
        basket.Total_Cost_NPV__c = 0;
        basket.SpecCon_Competitive_Clause_Cost__c = 50;
        basket.Competitive_Clause__c = true;
        basket.Tech_Fund__c = 99;
        basket.Number_of_Devices__c = 0;
        basket.Total_Buyout_Cost__c = 25;
        basket.SpecCon_Full_Co_terminus_Cost__c = 100;
        basket.Disconnection_allowance__c  = 100;
        basket.SpecCon_Unlocking_Fees_Cost__c = 100;
        
        
        
        Integer pbMonth = (Integer) basket.Tenure__c;
        
        basket.Total_Charges__c=90;
        basket.Total_Cost__c=40;
        basket.Credit_fund_total__c=100;
        insert basket;

		basket2 = new cscfga__Product_Basket__c(
          Name = 'Test Basket Trigger',
          cscfga__Opportunity__c = opp.Id,
          ReSign__c = true,
		  Tech_Fund__c = 99,
		  Number_of_Devices__c = 10,
		  Hardware_Charges__c = -200
        );
		insert basket2;
         
        cscfga__Product_Basket__c basketWithTotals = CS_ProductBasketService.calculateBasketTotals(new Map<Id, cscfga__Product_Configuration__c>(), basket);
		System.assertNotEquals(null, basketWithTotals);
        
        cscfga__Product_Basket__c basket1 = new cscfga__Product_Basket__c(
          Name = 'Test Basket Trigger1',
          
          cscfga__Opportunity__c = opp.Id
          
        );
        insert basket1;
		CS_ProductBasketService.calculateBasketTotals(new Map<Id, cscfga__Product_Configuration__c>(), basket1); 
        
       cscfga__Product_Definition__c proddef1 = new cscfga__Product_Definition__c(
          Name = 'proddef1',
          cscfga__Description__c = 'prod description1'
        );
        insert proddef1;
    
        cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
          Name = 'Ad1',
          cscfga__Data_Type__c = 'String',
          cscfga__Type__c = 'Related Product',
          cscfga__Product_Definition__c = proddef1.Id,
          cscfga__high_volume__c = true
        );
        insert ad1;
      
        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
          Name = 'Pd2',
          cscfga__Description__c = 'Pd2'
        );
        insert pd2;
        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
          Name = 'Ad2',
          cscfga__Data_Type__c = 'String',
          cscfga__Type__c = 'User Input',
          cscfga__Product_Definition__c = pd2.Id
        );
        insert ad2;
    
        CS_Related_Products__c rp = new CS_Related_Products__c(
          Name = 'Pd1',
          Related_Products__c = 'Pd2'
        );
        insert rp;
        
        CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
          Name = 'Pd2',
          Attribute_Names__c = 'Ad2',
          List_View_Attribute_Names__c = 'Ad2',
          Total_Attribute_Names__c = 'Ad2'
        );
        insert rpa;
    
		allConfigs = new map<Id, cscfga__Product_Configuration__c> ();
    
        cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
          Name = 'Root',
          cscfga__Product_Basket__c = basket.Id,
          cscfga__Product_Definition__c = proddef1.Id,
		  Type__c = 'Hardware',
		  Tech_Fund__c = 99
        );
        insert pc1;
		allConfigs.put(pc1.Id, pc1);
    
        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
          Name = 'Child',
          cscfga__Root_Configuration__c = pc1.Id,
          cscfga__Parent_Configuration__c = pc1.Id,
          cscfga__Product_Basket__c = basket.Id,
          cscfga__Product_Definition__c = proddef1.Id
        );
        insert pc2;
		allConfigs.put(pc2.Id, pc2);
    
        cscfga__Product_Configuration__c pc3 = new cscfga__Product_Configuration__c(
            Name = 'Test Lone Worker',
            cscfga__Product_Basket__c = basket1.Id,
            cscfga__Product_Definition__c = proddef1.Id,
            cscfga__Configuration_Status__c = 'Valid',
            cscfga__Unit_Price__c = 10,
            cscfga__Quantity__c = 1,
            cscfga__Recurrence_Frequency__c = 12,
            tenure__c=36,
            Care__c= 'yes',
            Total_Charges__c=100.00,
            Total_Cost__c=200.00, 
            Number_of_users__c=20,
            Data_Recurring_Charge__c=100.00,
            Data_One_Off_Charge__c=100.00,
            Data_One_Off_Cost__c=100.00,
            Data_Recurring_Cost__c=50.00,
            SMS_Recurring_Charge__c=5.00,
            SMS_One_Off_Charge__c = 5.00,
            SMS_Recurring_Cost__c=5.00,
            SMS_One_Off_Cost__c = 5.00,
            Mobile_Voice_One_Off_Cost__c =5.00,
            Mobile_Voice_Recurring_Cost__c=5.00,
            Fixed_One_Off_Charge__c =5.00,
            Fixed_Recurring_Charge__c =5.00,
            Incoming_Recurring_Charge__c=5.00,
            Service_Recurring_Cost__c =5.00,
            OPEX_Recurring_Cost__c=5.00,
            Warning_Summary__c='Test',         
            Mobile_Voice_One_Off_Charge__c =5.00,
            Mobile_Voice_Recurring_Charge__c=5.00,
            Gross_Margin__c=85.00,
            SpecCon_Competitive_Clause_Cost__c=40.00,
            SpecCon_Disconnection_Allowance_Cost__c=22.00,
            Credit_fund_total__c=17.00,
            SpecCon_Full_Co_terminus_Cost__c=18.00
        );
        insert pc3;
		allConfigs.put(pc3.Id, pc3);
        
        cscfga__Product_Configuration__c pc4 = new cscfga__Product_Configuration__c(
            Name = 'Special Conditions',
            cscfga__Product_Basket__c = basket1.Id,
            cscfga__Product_Definition__c = proddef1.Id,
            cscfga__Configuration_Status__c = 'Valid',
            cscfga__Unit_Price__c = 10,
            cscfga__Quantity__c = 1,
            cscfga__Recurrence_Frequency__c = 12,
            tenure__c=36,
            SpecCon_Averaging_co_terminus__c = true,
            SpecCon_Competitive_Benchmarking__c= true,
            SpecCon_Competitive_Clause__c=true,
            SpecCon_Competitive_Clause_Cost__c = 10.00,
            SpecCon_Disconnection_Allowance__c = true,
            SpecCon_Full_co_terminus__c= true,
            SpecCon_Full_Co_terminus_Cost__c= 34.40,
            Payment_Terms__c='45',
            SpecCon_Total_Contract_Guarantee__c= true,
            SpecCon_Total_Revenue_Guarantee__c=true,
            SpecCon_Unlocking_Fees_Cost__c=44.44,
            SPecCon_Unlocking_Fees__c= true
        );
        
        insert pc4;
        allConfigs.put(pc4.Id, pc4);
        
        basket1.Approval_Status__c = 'Approved';
        basket1.Has_Special_Conditions__c = true;
        basket1.Competitive_Benchmarking__c = true;
        basket1.Unlocking_Fees__c = true;
        basket1.Total_Contract_Guarantee__c = true;
        basket1.Total_Revenue_Guarantee__c = true;
        basket1.Competitive_Clause__c = true;
        basket1.Disconnect_Allowances__c = true;
        basket1.Averaging_Co_Terminus__c = true;
        basket1.Full_Co_Terminus__c = true;
        basket1.Special_Conditions_Id__c =pc4.id;
        basket1.Tenure__c = 24;
        basket1.Care_Selected__c = true;
        basket1.Payment_Terms__c = '45';
        basket1.Monthly_Recurring_Charges__c = 10;
        basket1.Total_Charges__c=90;
        basket1.Total_Cost__c=40;
        basket1.SpecCon_Competitive_Clause_Cost__c  = 15;
        basket1.SpecCon_Full_Co_terminus_Cost__c = 15;
        basket1.Disconnection_allowance__c = 25;
        basket1.SpecCon_Unlocking_Fees_Cost__c = 50;
        basket1.Total_Buyout_Cost__c = 100;
        basket1.Rolling_Air_Time_NPV__c  = 25;
        basket1.NPV_Staged_Air_Time__c  = 175;
        basket1.Credit_fund_total__c = 500;
        basket1.Number_of_users__c = 10;
        basket1.recalculateFormulas();
		
		
		
		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(false, 'EE SME');

        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(false, 'Test Prod Def');
		pd.cscfga__Product_Category__c = prodCategory.Id;
		INSERT pd;

		List<String> attDefNames = new List<String>{
            'Data_Costs',
            'Data_Costs_Future',
            'Data_Revenue',
            'Data_Revenue_Future',
            'Rate_Card_Line',
            'Usage_Profile',
            'Voice_Costs',
            'Voice_Costs_Future',
            'Voice_Revenue',
            'Voice_Revenue_Future'};

		List<cscfga__Attribute_Definition__c> attDefList = CS_TestDataFactory.generateAttributeDefinitions(true, attDefNames, pd);

        pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
		pc.Tenure__c = 5;
		pc.cscfga__Configuration_Status__c = 'Valid';
        INSERT pc;

		List<cscfga__Attribute__c> attsList = CS_TestDataFactory.generateAttributesForConfiguration(true, attDefList, pc);

        pcsList = new List<cscfga__Product_Configuration__c>{pc, pc, pc};
        configsMap = new Map<Id, cscfga__Product_Configuration__c>();
		attDefsMap = new Map<Id, cscfga__Attribute_Definition__c>();
		prodDefMap = new Map<Id, cscfga__Product_Definition__c>{pd.Id => pd};

        for(cscfga__Product_Configuration__c p : pcsList){
        	configsMap.put(p.Id, p);
        }

		for(cscfga__Attribute_Definition__c attDef : attDefList) {
			attDefsMap.put(attDef.Id, attDef);
		}

		payload = new Map<String, Object>();
		payload.put('AllAttrs', attsList);
		payload.put('AllConfigs', new List<cscfga__Product_Configuration__c>{pc});
		payload.put('AttributeDefinitionsMap', attDefsMap);
		payload.put('RootConfig', pc);
		payload.put('Container', basket);
		payload.put('RootConfig', pc);
		payload.put('Configs', new List<cscfga__Product_Configuration__c>{pc});


        CS_TestDataFactory.generatePLReportConfigRecords();
    	List<cscfga__Attribute_Definition__c> atrDefs = CS_TestDataFactory.generateAttributeDefinitions(true, new List<String>{'Test PD'}, pd);
    	Map<Id, cscfga__Attribute_Definition__c> atrMap = new Map<Id, cscfga__Attribute_Definition__c>();
    	for(cscfga__Attribute_Definition__c ad : atrDefs){
			atrMap.put(ad.Id, ad);
    	}

    	List<cscfga__Attribute__c> atrsList = CS_TestDataFactory.generateAttributesForConfiguration(true, atrDefs, pcsList[0]);
    	  basket1 = new cscfga__Product_Basket__c(
    	   Name = 'Test Basket Trigger1',
          cscfga__Opportunity__c = opp.Id,
          ReSign__c = true

        );
    }
    
    /* private static void setupDataOLD(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.generatePLReportConfigRecords();
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        basket =  CS_TestDataFactory.generateProductBasket(true, 'test basket', testOpp);
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile');
        pc = CS_TestDataFactory.generateProductConfiguration(true, 'Mobile Sharer', basket);
    } */
    
    
    private static void setupData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
        
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        		
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', acc);
		basket =  CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);

		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(false, 'EE SME');

        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(false, 'Test Prod Def');
		pd.cscfga__Product_Category__c = prodCategory.Id;
		INSERT pd;

		List<String> attDefNames = new List<String>{
            'Data_Costs',
            'Data_Costs_Future',
            'Data_Revenue',
            'Data_Revenue_Future',
            'Rate_Card_Line',
            'Usage_Profile',
            'Voice_Costs',
            'Voice_Costs_Future',
            'Voice_Revenue',
            'Voice_Revenue_Future'};

		List<cscfga__Attribute_Definition__c> attDefList = CS_TestDataFactory.generateAttributeDefinitions(true, attDefNames, pd);

        pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
		pc.Tenure__c = 5;
		pc.cscfga__Configuration_Status__c = 'Valid';
        INSERT pc;

		List<cscfga__Attribute__c> attsList = CS_TestDataFactory.generateAttributesForConfiguration(true, attDefList, pc);

        pcsList = new List<cscfga__Product_Configuration__c>{pc, pc, pc};
        configsMap = new Map<Id, cscfga__Product_Configuration__c>();
		attDefsMap = new Map<Id, cscfga__Attribute_Definition__c>();
		prodDefMap = new Map<Id, cscfga__Product_Definition__c>{pd.Id => pd};

        for(cscfga__Product_Configuration__c p : pcsList){
        	configsMap.put(p.Id, p);
        }

		for(cscfga__Attribute_Definition__c attDef : attDefList) {
			attDefsMap.put(attDef.Id, attDef);
		}

		payload = new Map<String, Object>();
		payload.put('AllAttrs', attsList);
		payload.put('AllConfigs', new List<cscfga__Product_Configuration__c>{pc});
		payload.put('AttributeDefinitionsMap', attDefsMap);
		payload.put('RootConfig', pc);
		payload.put('Container', basket);
		payload.put('RootConfig', pc);
		payload.put('Configs', new List<cscfga__Product_Configuration__c>{pc});


        CS_TestDataFactory.generatePLReportConfigRecords();
    	List<cscfga__Attribute_Definition__c> atrDefs = CS_TestDataFactory.generateAttributeDefinitions(true, new List<String>{'Test PD'}, pd);
    	Map<Id, cscfga__Attribute_Definition__c> atrMap = new Map<Id, cscfga__Attribute_Definition__c>();
    	for(cscfga__Attribute_Definition__c ad : atrDefs){
			atrMap.put(ad.Id, ad);
    	}

    	List<cscfga__Attribute__c> atrsList = CS_TestDataFactory.generateAttributesForConfiguration(true, atrDefs, pcsList[0]);
    	  basket1 = new cscfga__Product_Basket__c(
    	   Name = 'Test Basket Trigger1',
          cscfga__Opportunity__c = opp.Id,
          ReSign__c = true

        );
        insert basket1;        
        basket1.Approval_Status__c = 'Commercially Approved';
        basket1.Has_Special_Conditions__c = false;
        basket1.Competitive_Benchmarking__c = false;
        basket1.Unlocking_Fees__c = false;
        basket1.Total_Contract_Guarantee__c = false;
        basket1.Total_Revenue_Guarantee__c = false;
        basket1.Competitive_Clause__c = false;
        basket1.Disconnect_Allowances__c = false;
        basket1.Averaging_Co_Terminus__c = false;
        basket1.Full_Co_Terminus__c = false;
        basket1.Special_Conditions_Id__c = null;
        basket1.Tenure__c = 24;
        basket1.Care_Selected__c = false;
        basket1.Payment_Terms__c = '';
        basket1.Monthly_Recurring_Charges__c = 10;
        basket1.Total_Charges__c=90;
        basket1.Total_Cost__c=40;       
         //basket.Number_of_users__c = 10;
        basket1.Credit_fund_total__c=100; 
	}

    @isTest
    public static void testRedirect() {
        setupData();
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.CS_CustomRedirect')); 
        System.currentPageReference().getParameters().put('cfgfinish', pc.Id);
        
        System.currentPageReference().getParameters().put('id', basket.id);

        ApexPages.StandardController controller = new ApexPages.StandardController(basket);
        mainController = new CS_CustomRedirectCS(controller);
        PageReference testPage = mainController.redirectToPage();
        Test.stopTest();
        
        String URL = System.URL.getSalesforceBaseURL().getHost();
        PageReference prCheck = new PageReference('https://' + URL + '/apex/csbb__csbasketredirect?id=' + controller.getId());
        System.assertEquals(prCheck.getUrl(), testPage.getUrl());
    }
    
    @isTest
    public static void testDoBasketCalculations() {
        //setupData();
        createTestData();
        
        Test.startTest();
        
        Test.setCurrentPageReference(new PageReference('Page.CS_CustomRedirect')); 
        System.currentPageReference().getParameters().put('cfgfinish', pc.Id);
        System.debug('basket.id>'+basket.id);
        System.currentPageReference().getParameters().put('id', basket.id);

        ApexPages.StandardController controller = new ApexPages.StandardController(basket);
        mainController = new CS_CustomRedirectCS(controller);
        
        PageReference testPage = mainController.doBasketCalculations();
        CS_CR_Basket_Calculations__c setting2 = new CS_CR_Basket_Calculations__c(
            Sync_Basket_Size_Limit__c = 1
        );
        insert setting2;
        System.debug('setting2>'+JSON.serializePretty(setting2));
          testPage = mainController.doBasketCalculations();
        Test.stopTest();
        
        String URL = System.URL.getSalesforceBaseURL().getHost();
        PageReference prCheck = new PageReference('https://' + URL + '/apex/csbb__csbasketredirect?id=' + controller.getId());
        //System.assertEquals(prCheck.getUrl(), testPage.getUrl());
    }
    
    @isTest
    public static void testRedirectConfigId() {
        setupData();

        Test.startTest();
        Test.setCurrentPageReference(new PageReference('Page.CS_CustomRedirect')); 
        System.currentPageReference().getParameters().put('configId', pc.Id);
        
        ApexPages.StandardController controller = new ApexPages.StandardController(basket);
        mainController = new CS_CustomRedirectCS(controller);
        PageReference testPage = mainController.redirectToPage();
        
        Test.stopTest();
        
        String URL = System.URL.getSalesforceBaseURL().getHost();
        PageReference prCheck = new PageReference('https://' + URL + '/apex/CS_BasketViewer?id=' + controller.getId());
        System.assertEquals(prCheck.getUrl(), testPage.getUrl());
    }
}