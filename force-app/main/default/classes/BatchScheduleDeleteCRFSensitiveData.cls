global class BatchScheduleDeleteCRFSensitiveData implements Schedulable{
    global void execute(SchedulableContext sc) {
        Date myDate = System.today() - 30;
        String sDate = String.valueOf(myDate);
        BatchDeleteCRFSensitiveData b = new BatchDeleteCRFSensitiveData('SELECT Id, Customer_bank_account_number__c, Sort_code__c FROM CRF__c WHERE CreatedDate <' + sDate + 'T00:00:00Z');
        database.executebatch(b, 200);
    }
}