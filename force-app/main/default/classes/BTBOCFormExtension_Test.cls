@isTest
private class BTBOCFormExtension_Test {

		static testMethod void myUnitTest() {
 			Dataloads__c dlc = new Dataloads__c(Data_Load__c ='E2E Reporting');
    		insert dlc;

		    BTB_Order__c boc = new BTB_Order__c(
		        Name = 'Order Test',
		        OV_ORDER_NUM__c = '1234567'
		    );
    		insert boc;

		    BTB_Order_Line__c bolc = new BTB_Order_Line__c(
		        Name = 'Test Line Item',
		        BTB_ORDER__c =  boc.Id,
				Currently_Actioned_By__c=NULL
		    );
		    insert bolc;

		    BTB_Order_Line__c bolc1 = new BTB_Order_Line__c(
		        Name = 'Test Line Item',
		        BTB_ORDER__c =  boc.Id,
		        Cancellation_Current_Status__c= 'Completed',
		        Currently_Actioned_By__c=UserInfo.getName()
		    );
		    insert bolc1;

		    BTB_Order_Line__c bolc2 = new BTB_Order_Line__c(
		        Name = 'Test Line Item',
		        BTB_ORDER__c =  boc.Id,
		        Currently_Actioned_By__c='Hello',
		       Currently_Actioned_Time__c=datetime.now()
		    );
		    insert bolc2;

		    BTB_Order_Line__c bolc3 = new BTB_Order_Line__c(
		        Name = 'Test Line Item',
		        BTB_ORDER__c =  boc.Id,
		        Currently_Actioned_By__c='Hello',
		       Currently_Actioned_Time__c=Datetime.newInstance(2010,12,17,03,11,47)
		    );
		    insert bolc3;


  			User U;

		  BTLB_Order_Cancellation_Form__c BOCF=new BTLB_Order_Cancellation_Form__c();
		  BOCF.Form_Completion_Date__c=system.today();
		  BOCF.Form_Created_By_Name__c=UserInfo.getName();
		  BOCF.BTB_Order_Line__c=bolc.Id;
		  BOCF.Customer_Contacted__c='Yes';
		  bolc.Cancellation_Current_Status__c='Completed';
		  BOCF.Company_Name_OV__c='';
		  insert BOCF;

		    BTLB_Order_Cancellation_Form__c BOCF1=new BTLB_Order_Cancellation_Form__c();
		  BOCF1.Form_Completion_Date__c=system.today();
		  BOCF1.Form_Created_By_Name__c=UserInfo.getName();
		  BOCF1.BTB_Order_Line__c=bolc2.Id;
		  BOCF1.Customer_Contacted__c='No';
		 // bolc.Cancellation_Current_Status__c='Completed';
		  insert BOCF1;

		  BTB_Order_Line_Product__c BOLP =new BTB_Order_Line_Product__c();
		    BOLP.BTB_Order_Line__c = bolc.Id;
		    BOLP.Product_Name_on_CSS__c = 'Test';
		    BOLP.PROD_GROUP__c = 'BB Winback';
		    insert BOLP;

		BTB_Order_Line_Product__c BOLP2 =new BTB_Order_Line_Product__c();
		    BOLP2.BTB_Order_Line__c = bolc1.Id;
		    BOLP2.Product_Name_on_CSS__c = 'Test';
		    BOLP2.PROD_GROUP__c = 'BB Winback';
		    insert BOLP2;

		   BTB_Order_Line_Product__c BOLP1 =new BTB_Order_Line_Product__c();
		    BOLP1.BTB_Order_Line__c = bolc2.Id;
		    BOLP1.Product_Name_on_CSS__c = 'Test';
		    BOLP1.PROD_GROUP__c = 'Test';
		    insert BOLP1;

		    BTB_Order_Line_Product__c BOLP4 =new BTB_Order_Line_Product__c();
		    BOLP4.BTB_Order_Line__c = bolc3.Id;
		    BOLP4.Product_Name_on_CSS__c = 'Test';
		    BOLP4.PROD_GROUP__c = 'BB Winback';
		    insert BOLP4;

		  PageReference pageRef2 = Page.BTBOCForm;
		  Test.setCurrentPageReference(pageRef2);
		  ApexPages.currentPage().getParameters().put('BTBOrderLineId',bolc1.Id);
		  Apexpages.Standardcontroller sc1 = new Apexpages.Standardcontroller(BOCF);
		  BTBOCFormExtension BC1= new BTBOCFormExtension(sc1);

		  PageReference pageRef = Page.BTBOCForm;
		  Test.setCurrentPageReference(pageRef);
		  ApexPages.currentPage().getParameters().put('BTBOrderLineId',bolc.Id);
		  Apexpages.Standardcontroller sc = new Apexpages.Standardcontroller(BOCF);
		  BTBOCFormExtension BC= new BTBOCFormExtension(sc);
		  BC.RecordLocking();
		  BC1.RecordLocking();

		  BC.getRecLockError();
		  BC.getSaveLockError();


		  BOCF.Company_Name_OV__c='Test';
		  BOCF.Telephone_Number_OV__c=NULL;
		  update BOCF;

		  BC.getCustomer_Contacted();
		  BC.getReIssueOrder();

		  BOCF.Company_Name_OV__c='ABC Corp';
		  BOCF.Contact_Name_OV__c='Test';
		  BOCF.Telephone_Number_OV__c='123434';
		  BOCF.Number_of_lines_involved__c=112;
		  BOCF.Customer_Telephone_Number__c='123434';

		  BOCF.Name_of_Contact__c='Hello';
		  BOCF.Position_in_Company__c='PM';
		  BOCF.Address__c='UK';
		  BOCF.Number_of_Employees__c='<10';
		  update BOCF;
		  Competitors_Suppliers_Manufacturers__c C=new Competitors_Suppliers_Manufacturers__c(E2E_Competitor__c=True,Name='6666');
		  Insert C;
		  BOCF.Losing_Supplier_Name__c=C.Id;

		  BC.SubmitForm();
	}
}