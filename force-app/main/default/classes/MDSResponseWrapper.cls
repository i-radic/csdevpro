/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 8.2020
   @author Mahaboob Basha

   @description A Wrapper what holds the response from MDS File Server

   @modifications
   06.08.2020 [Mahaboob]
   
 */
public class MDSResponseWrapper {
	public String status;
	public String message;
	public Integer code;
	public List<MDSFileResponseWrapper> files;
	
	public class MDSFileResponseWrapper {
	    public String fileName;
        public String path;
        public String body;
        public Long lastModifiedDateTime;
        public String status;
        public String message;
        public String code;
	}
}