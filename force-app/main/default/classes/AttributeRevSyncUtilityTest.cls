@isTest
private class AttributeRevSyncUtilityTest {

	public static cscfga__Product_Configuration__c pc1;
	public static cscfga__Product_Definition__c pd1;
	public static cscfga__Product_Basket__c pb;
	public static cscfga__Product_Definition__c pd2;
	public static cscfga__Attribute__c at1 ,at2;
	public static BT_One_Phone__c bop;
	
	static {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    			
		Account a = new Account(
			Name = 'Test',
			NumberOfEmployees = 1
		);
		insert a;
		
		Opportunity o = new Opportunity(
			Name = 'Test',
			AccountId = a.Id,
			CloseDate = System.Today(),
			StageName = 'Closed Won',
			TotalOpportunityQuantity = 0
		);
		
		insert o;
		
		Usage_Profile__c up1 = new Usage_Profile__c(
			Active__c = true,
			Account__c = a.Id,
			Expected_SMS__c = 20,
			Expected_Voice__c = 10,
			Expected_Data__c = 20
		);
		
		insert up1;
		
		cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
			Name = 'Test',
			Default_Usage_Profile__c = up1.Id
		);
		insert pi1;
		
		cspmb__Rate_Card__c rc1 = new cspmb__Rate_Card__c(
			Name = 'Test'
		);
		insert rc1;
		
		cspmb__Rate_Card_Line__c rlVoice = new cspmb__Rate_Card_Line__c(
			Name = 'Voice',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlVoice;
		
		cspmb__Rate_Card_Line__c rlSMS = new cspmb__Rate_Card_Line__c(
			Name = 'SMS',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlSMS;
		
		cspmb__Rate_Card_Line__c rlData = new cspmb__Rate_Card_Line__c(
			Name = 'Data',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlData;
		
		cspmb__Price_Item_Rate_Card_Association__c assoc = new cspmb__Price_Item_Rate_Card_Association__c(
			cspmb__Rate_Card__c = rc1.Id,
			cspmb__Price_Item__c = pi1.Id
		);
		insert assoc;
		
		pd1 = new cscfga__Product_Definition__c(
			Name = 'BTOP CORE',
			cscfga__Description__c = 'BTOP CORE'
		);
		insert pd1;
		
		cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
			Name = 'Contract Term',
			cscfga__Data_Type__c = 'Integer',
			cscfga__Type__c = 'User Input',
			Attribute_Sync_Required__c=true,
			cscfga__Product_Definition__c = pd1.Id,
			cscfga__high_volume__c = true
		);
		insert ad1;
		
		pd2 = new cscfga__Product_Definition__c(
			Name = 'User Group',
			cscfga__Description__c = 'Pd2'
		);
		insert pd2;
		cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
			Name = 'Ad2',
			cscfga__Data_Type__c = 'String',
			cscfga__Type__c = 'User Input',
			cscfga__Product_Definition__c = pd2.Id
		);
		insert ad2;
		
		cscfga__Available_Product_Option__c apo = new cscfga__Available_Product_Option__c(
			cscfga__Attribute_Definition__c = ad1.Id,
			cscfga__Product_Definition__c = pd2.Id
		);
		insert apo;
		
		CS_Related_Products__c rp = new CS_Related_Products__c(
			Name = 'Mobile Voice',
			Related_Products__c = 'User Group'
		);
		insert rp;
		
		CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
			Name = 'User Group',
			Attribute_Names__c = 'Ad2',
			List_View_Attribute_Names__c = 'Ad2',
			Total_Attribute_Names__c = 'Ad2'
		);
		insert rpa;
		
		pb = new cscfga__Product_Basket__c(
			Name = 'Test',
			cscfga__Opportunity__c = o.Id
		);
		insert pb;
		
		pc1 = new cscfga__Product_Configuration__c(
			Name = 'BTOP CORE',
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Product_Definition__c = pd1.Id,
			Service_Plan__c = pi1.Id
		);
		insert pc1;
		
		cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
			Name = 'Child',
			cscfga__Root_Configuration__c = pc1.Id,
			cscfga__Parent_Configuration__c = pc1.Id,
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Product_Definition__c = pd2.Id,
			cscfga__Attribute_Name__c = 'Ad1'
		);
		insert pc2;
		
		 at1 = new cscfga__Attribute__c(
			Name = 'Contract Term',
			cscfga__Attribute_Definition__c = ad1.Id,
			cscfga__Value__c = '24',
			cscfga__Product_Configuration__c = pc1.Id
		);
		insert at1;
		
		
		 at2 = new cscfga__Attribute__c(
			Name = 'Ad2',
			cscfga__Attribute_Definition__c = ad2.Id,
			cscfga__Value__c = 'Test',
			cscfga__Product_Configuration__c = pc2.Id
		);
		insert at2;
		bop = new BT_One_Phone__c(
		    
		    Contract_Term__c = 48,
		    OnePhone_Product_Type__c = 'BT One Phone Office',
		    Existing_Customer_Resign__c  = 'Y',
		    Opportunity__c= o.id
		    
		    );
		insert bop;
		
	}

	private static testmethod void testAttributeRevSyncUtility() {
		List<id> attIdList = new List<id>();
		attIdList.add(at1.id);
		Test.startTest();
	    AttributeRevSyncUtility.doAttributeRevSync(attIdList);
		Test.stopTest();
	}
	
	private static testmethod void testCustomObjectFieldSyncToAttribute(){
	    List<id> objId = new List<id>();
		objId.add(bop.id);
		Test.startTest();
	    CustomObjectFieldSyncToAttribute.doObjectAttributeSync(objId);
		Test.stopTest();
	
	}

}