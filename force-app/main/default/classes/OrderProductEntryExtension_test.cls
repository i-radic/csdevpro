/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true) // Exceptional use (Needed for the accessing of Standard Pricebook)
public with sharing class OrderProductEntryExtension_test {

	private static Account mAccount;
	private static Order mOrder;
	private static Order mOrderChg;
	private static Map<String, Id> orderRequestRecordTypeMap;
    private static Product2 testProductWelcomeDay;
	private static Pricebook2 testStandardPricebook, testServicesPricebook;
	private static PricebookEntry testPbeWelcomeDay;
	
	static{
		
		// Create Custom Settings
		EE_OrderTestClass.createCustomSettings();
		
		// Create Product Records
		EE_OrderTestClass.createProductRecords();
		
		//TriggerEnabler__c trgEnablerOrderItem = EE_OrderTestClass.createTriggerEnabler('OrderItem', true);
		//upsert trgEnablerOrderItem;
		
		// Just create a single test account. false = no incremental count for the Company Name
		mAccount = EE_TestClass.createStaticAccount('Test Company', false);
		insert mAccount;
		
		// Create our Included Services Order record
        mOrder = new Order();
		mOrder.AccountId 		= mAccount.Id;
		mOrder.EffectiveDate	= system.today().addYears(1);
		mOrder.Status			= 'Draft';
//		mOrder.Order_Type_Detail__c = 'Included Services';
		mOrder.Type 			= 'Included Services';
		
		mOrder.Pricebook2Id		= EE_OrderTestClass.getPricebookId('Services Price Book');
		insert mOrder;
		
		// Create our Chargeable Services Order record
        mOrderChg = new Order();
		mOrderChg.AccountId 		= mAccount.Id;
		mOrderChg.EffectiveDate		= system.today().addYears(1);
		mOrderChg.Status			= 'Draft';
		mOrderChg.Type 				= 'Chargeable Service';
		mOrderChg.Pricebook2Id		= EE_OrderTestClass.getPricebookId('Services Price Book');
		insert mOrderChg;			
				

        // Store the record types for the order requests
        if (orderRequestRecordTypeMap == null) {
            orderRequestRecordTypeMap = new Map<String, Id>(); 
            for( RecordType rct : [SELECT Id, Name, DeveloperName
                                FROM RecordType 
                                WHERE SobjectType='Order_Request__c']){
        
                orderRequestRecordTypeMap.put(rct.DeveloperName, rct.Id);
            }
        }
        
		
	}

   /*
    * @name         testNewOrderAddWelcomeDay
    * @description	selects the Add product - for Welcome Days - in the Order Item selection screen
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */     
    static testmethod void testNewOrderAddWelcomeDay() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrder.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Welcome Day', 'Services Price Book');
        oExt.psSelect = (string)lIdPriceBookEntryId;
        
        // Add the Welcome Day Product
        oExt.addOrderItems();

		// Stop Tests
        Test.StopTest();

		// Assert that we have added 1 product
        system.AssertEquals( 1, oExt.plOrderItems.size() );   
    }


   /*
    * @name         testNewOrderRemoveWelcomeDay
    * @description	selects the Add and then Remove - for Welcome Days - in the Order Item selection screen
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */     
    static testmethod void testNewOrderRemoveWelcomeDay() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrder.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Welcome Day', 'Services Price Book');
             
        // Add the Welcome Day Product
        oExt.psSelect = (string)lIdPriceBookEntryId;
        oExt.addOrderItems();

		// Assert that we have added 1 product
        system.AssertEquals( 1, oExt.plOrderItems.size() );   
                
        // Now Remove the Welcome Day Product
        oExt.psUnselect = (string)lIdPriceBookEntryId;
        oExt.removeOrderItems();
        
		// Stop Tests
        Test.StopTest();

		// Assert that we have no products
        system.AssertEquals( 0, oExt.plOrderItems.size() );   
    }  
    
   /*******************************************************
				Create New Order Test Code
    *******************************************************/ 
    
   /*
    * @name         testNewOrderCreateSuccess
    * @description	test the constructor to create a new Order Success
    * @author       P Goodey
    * @date         Oct 2014
    * @see 
    */  
    static testmethod void testNewOrderCreateSuccess() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('aid', mAccount.Id);
        //pageRef.getParameters().put('id', null);
        Test.setCurrentPageReference(pageRef);
        
        Order lOrder = new Order();
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(lOrder));        
        
        // Insert the Order
        oExt.onSaveAndContinue();

		// Stop Tests
        Test.StopTest();

    } 


   /*
    * @name         testNewOrderCreateError
    * @description	test the constructor to create a new Order Error
    * @author       P Goodey
    * @date         Oct 2014
    * @see 
    */   
    static testmethod void testNewOrderCreateError() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        
        // do not set the account id
        // pageRef.getParameters().put('aid', mAccount.Id);
        Test.setCurrentPageReference(pageRef);
        
        Order lOrder = new Order();
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(lOrder));        
        
		// Stop Tests
        Test.StopTest();

    }
    

   /*
    * @name         testNewOrderSaveUpdatedNotOKChargeableWelcomeDay
    * @description	selects the onSave - for amended Chargeable Welcome Days - without remaining Welcome Days
    * @author       P Goodey
    * @date         Jan 2015
    * @see 
    */
    static testmethod void testNewOrderSaveUpdatedNotOKChargeableWelcomeDay() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrderChg.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Welcome Day', 'Services Price Book');
             
        // Add the Welcome Day Product
        oExt.psSelect = (string)lIdPriceBookEntryId;
        oExt.addOrderItems();
        
        // Set the Quantity to 5 welcome days
        oExt.plOrderItems[0].Quantity = 5;
        
        // Clear the validations
        oExt.pbIsQuantityExceeded = false;
      
        // Save the Welcome Day - it is a new insert record
        oExt.onSave();
        
        // Update the Quantity to 10 welcome days
        oExt.plOrderItems[0].Quantity = 10;

        // Save the Welcome Day - it is an Update record
        oExt.onSave();
                
		// Stop Tests
        Test.StopTest();
        
		// Assert that the validation states welcome days quantity exceeded
        system.AssertEquals( true, oExt.pbIsQuantityExceeded );          
        
    }



   /*
    * @name         testNewOrderSaveNotOKChargeableWelcomeDay
    * @description	selects the onSave - for Included Welcome Days - without remaining Welcome Days
    * @author       P Goodey
    * @date         Jan 2015
    * @see 
    */
/* PG BK
    static testmethod void testNewOrderSaveNotOKChargeableWelcomeDay() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrderChg.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Welcome Day', 'Services Price Book');
             
        // Add the Welcome Day Product
        oExt.psSelect = (string)lIdPriceBookEntryId;
        oExt.addOrderItems();
        
        // Set the Quantity to 5 welcome days
        oExt.plOrderItems[0].Quantity = 5;
        
        // Assert that we are adding quantity of 5
        system.AssertEquals( 5, oExt.plOrderItems[0].Quantity );
        
        // Clear the validations
        oExt.pbIsQuantityExceeded = false;
      
        // Save the Welcome Day - it is a new insert record
        oExt.onSave();
                
		// Stop Tests
        Test.StopTest();
        
		// Assert that the validation states welcome days quantity exceeded
        system.AssertEquals( false, oExt.pbIsQuantityExceeded );          
        
    }
*/


   /*
    * @name         testNewOrderSaveOKIncludedWelcomeDay
    * @description	selects the onSave - for Included Welcome Days - with remaining Welcome Days
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */     
	static testmethod void testNewOrderSaveOKIncludedWelcomeDay() {    

        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrder.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Create 10 Available Included Services Welcome Days
        Order_Level__c orderLevel = EE_OrderTestClass.createStaticOrderLevel(mAccount, 10, 'Welcome Day | Included Services');
        insert orderLevel;
        
		// Get the Order Levels
        List<Order_Level__c> lOrderLevels = [SELECT Id, Quantity__c FROM Order_Level__c WHERE Company__c = :mAccount.Id ];     
        
        // Assert that we have  1 order level to the list of order levels
        system.AssertEquals( 1, lOrderLevels.size() );
        
        // Assert that we have added a level of 10
        system.AssertEquals( 10, lOrderLevels[0].Quantity__c );
        
 
		// Assert that we have added 10 available welcome days to the Company
		Account lAccount = [select Id, Remaining_Inc_Welcome_Days__c from Account where Id = :mAccount.Id];
        system.AssertEquals( 10, lAccount.Remaining_Inc_Welcome_Days__c );
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Welcome Day', 'Services Price Book');
             
        // Add the Welcome Day Product
        oExt.psSelect = (string)lIdPriceBookEntryId;
        oExt.addOrderItems();

		// Assert that we have added 1 product to the list of selected
        system.AssertEquals( 1, oExt.plOrderItems.size() );
        
        // Set the Quantity to 5 welcome days
        oExt.plOrderItems[0].Quantity = 5;//10;
        
        // Assert that we are adding quantity of 5
        system.AssertEquals( 5, oExt.plOrderItems[0].Quantity );
        
        // Clear the validations
        //oExt.pbIsWelcomeDayQuantityExceeded = false;
        oExt.pbIsQuantityExceeded = false;
        //oExt.pbIsOrderQuantiesZero = false;

		// Start Tests	        
        Test.StartTest();
        
        // Save the Welcome Day - it is a new insert record
        oExt.onSave();
                
		// Stop Tests
        Test.StopTest();
        
		// Assert that the validation states welcome days quantity not exceeded
        system.AssertEquals( false, oExt.pbIsQuantityExceeded );   
        
		// Assert that the validation states quantity not zero
        //system.AssertEquals( false, oExt.pbIsOrderQuantiesZero );                

        // Get the Order Items
        List<OrderItem> lOrderItems = [SELECT Id FROM OrderItem WHERE OrderId=:mOrder.Id ]; //AND PriceBookEntryId=:lIdPriceBookEntryId ]; 

		// Assert that we have saved an order item
        system.AssertEquals( 1, lOrderItems.size() );

		// Get the Negative Order Levels
        lOrderLevels = [SELECT Id, Quantity__c FROM Order_Level__c WHERE System_Order_Item__c = :lOrderItems[0].Id ];     
        
        // Assert that we have  1 order level to the list of order levels
        system.AssertEquals( 1, lOrderLevels.size() );
        
        // Assert that we have added a level of 10
        system.AssertEquals( -5, lOrderLevels[0].Quantity__c );        
        
        // Now Edit

        // Load the Controller Extension
        orderProductEntryExtension oExt_edit = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));
        
        // Assert that we already have 1 product to the list of selected
        system.AssertEquals( 1, oExt.plOrderItems.size() ); 
        
     	// Test the onAddOrRemoveItems Code
        oExt.onAddOrRemoveItems();
          
         
    }


   /*
    * @name         testNewOrderSaveDeviceCustomisationStandard
    * @description	selects the onSave - for Device Customisation Standard
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
/*    
    static testmethod void testNewOrderSaveDeviceCustomisationStandard() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrder.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Device Customisation Standard', 'Services Price Book');
             
        // Add the Device Customisation Product
        oExt.psSelect = (string)lIdPriceBookEntryId;
        oExt.addOrderItems();

		// Assert that we have added 1 product
        system.AssertEquals( 1, oExt.plOrderItems.size() );

        // Save the Device Customisation
        oExt.onSave();
                
		// Stop Tests
        Test.StopTest();
        
        // Get the Order Items
        //List<OrderItem> lOrderItems = [SELECT Id FROM OrderItem WHERE OrderId=:mOrder.Id AND PriceBookEntryId=:lIdPriceBookEntryId ]; 

		// Assert that we have saved an order item
        //system.AssertEquals( 1, lOrderItems.size() );
    }
*/


   /*
    * @name         testNewOrderSaveBundleEnterprise
    * @description	selects the Bundle Enterprise - for Device Customisation
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
/*        
    static testmethod void testNewOrderSaveBundleEnterprise() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrder.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Device Customisation', 'Services Price Book');
             
        // Add the Device Customisation Product
        oExt.psSelect = 'Enterprise';
        oExt.addOrderItemsBundle();
        

		// Assert that we have added 1 product
        system.Assert( oExt.plOrderItems.size() > 0 );

    }
*/

   /*
    * @name         testNewOrderSaveBundleCorporate
    * @description	selects the Bundle Corporate - for Device Customisation
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
/*        
    static testmethod void testNewOrderSaveBundleCorporate() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrder.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Device Customisation', 'Services Price Book');
             
        // Add the Device Customisation Product
        oExt.psSelect = 'Corporate';
        oExt.addOrderItemsBundle();
        

		// Assert that we have added 1 product
        system.Assert( oExt.plOrderItems.size() > 0 );

    }    
*/

   /*
    * @name         testCodeCoverageMiscellaneous
    * @description	selects Miscellaneous Code Coverage Tests
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    static testmethod void testCodeCoverageMiscellaneous() {    

		// Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Order Id      
        PageReference pageRef = Page.orderProductEntry;
        pageRef.getParameters().put('Id', mOrder.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        orderProductEntryExtension oExt = new orderProductEntryExtension(new ApexPages.StandardController(mOrder));        
        
        // Set the Welcome Day - Services Pricebook Product to select using the Pricebook Entry Id
        Id lIdPriceBookEntryId = EE_OrderTestClass.getPricebookEntryId('Device Customisation', 'Services Price Book');
             
        // Test the Cancel Button
        oExt.onCancel();

        // Test the getWelcomeDaysRemaining Code
        //oExt.getWelcomeDaysRemaining();
        
        // Test the onAddOrRemoveItems Code
        oExt.onAddOrRemoveItems();

        // Test the getDeviceCustomisationRemaining Code
        //oExt.getDeviceCustomisationRemaining();

        
        // Test the setPriceBook Code
        oExt.setPriceBook();
                
        // Future Requirement
        // Test the Change Pricebook Code
        //oExt.changePricebook();
        
        // Future Requirement
        // Test the getChosenCurrency Code
        //oExt.getChosenCurrency();
        

    }       

    
}