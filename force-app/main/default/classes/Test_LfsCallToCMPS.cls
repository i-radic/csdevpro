/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata = true)
private class Test_LfsCallToCMPS {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId='00e20000001MX7z',
                           timezonesidkey = 'Europe/London', username = 'B2B3Profile13167cr@bt.com',
                           EIN__c = '012365'
                           );
        insert u1;

        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 3';
        A.sac_code__c = 'testSAC1';
        A.Sector_code__c = 'CORP1';
        A.LOB_Code__c = 'LOB1';
        A.OwnerId = u1.Id;
        A.CUG__c='CugTest1';
        Database.SaveResult accountResult = Database.insert(A);
        
        
        Opportunity oppLFS = Test_Factory.CreateOpportunity(null);
        oppLFS.AccountId = A.id;
        oppLFS.closedate = system.today();
        oppLFS.RecordTypeId = '01220000000ABFB'; //LFS Recordtype ID
        oppLFS.Customer_Contact_Mobile__c = '02077306784';
        Database.SaveResult opptResult = Database.insert(oppLFS);  
        
        Test.startTest();
        LfsCallToCMPS.lfsCugId = 'CUGTEST001';
        LfsCallToCMPS.getCUGId(oppLFS.Customer_Contact_Mobile__c, oppLFS.Id,'CUGTEST001');
        Test.stopTest();
        
    }
}