/******************************************************************************************************
Name : ProfilerQuestionAnswersedit 
Description : Controller for page OpportunityProfileredit
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    25/11/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public without sharing class ProfilerQuestionAnswersedit 
{
    public List<QuestionnaireQuestion__c> listOfQuestions {get;set;}
    public list<wrapperClass> lstNewWrap {get;set;}
    public Opportunity objOpp {get;set;}
    public String qtid    {get;set;}
    public Questionnaire__c objQuestionnaire{get;set;}
    public Questionnaire__c Questionnaire  = new Questionnaire__c ();
    public map<id,QuestionnaireReply__c> questreplyMap = new map<id,QuestionnaireReply__c>();
 /******************************************************************************************************
    Method Name : ProfilerQuestionAnswersedit 
    Description : Wrapper class for ProfilerQuestionAnswersedit
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public ProfilerQuestionAnswersedit () 
    {
        set<id> quesid = new set<id>();
        List<QuestionnaireReply__c> qustreplyList = new List<QuestionnaireReply__c>();
        Map<id,id> qustansMap = new Map<id,id>();
        String selectedDefault;
        qtid = ApexPages.currentPage().getParameters().get('qt'); 
        
         if(qtid !=null)
        {
            Questionnaire=[select id,Output__c,Opportunity__c from Questionnaire__c where id =:qtid];
             qustreplyList=[select id,Answer__c,Question__c,Questionnaire__c from QuestionnaireReply__c where Questionnaire__c =:qtid];
        }
        if(Questionnaire.Opportunity__c !=null)
        {
           objOpp = [SELECT Id,Name,StageName,CSC_Call_Pass__c FROM Opportunity WHERE Id=:Questionnaire.Opportunity__c ]; 
        }
            for(QuestionnaireReply__c reply : qustreplyList)
            {
                quesid.add(reply.Question__c);
                questreplyMap.put(reply.Question__c,reply);
                qustansMap.put(reply.Question__c,reply.Answer__c);
            }
            listOfQuestions = [SELECT Id, Question__c,Name,Order__c, (SELECT Answer__c , Score__c , Questionnaire_Question__c , Order__c FROM Questionnaire_Answers__r order by Order__c) 
        FROM QuestionnaireQuestion__c WHERE id IN :quesid order by Order__c];
        lstNewWrap  = new list<wrapperClass>();  
        if(!listOfQuestions.isEmpty()) 
        {
            integer intval=0;
            for(QuestionnaireQuestion__c que: listOfQuestions) 
            { 
                if(qustansMap.containsKey(que.id))
                    {
                        selectedDefault=qustansMap.get(que.id);
                    }
                List<SelectOption> options = new List<SelectOption>();
                for(QuestionnaireAnswer__c answers: que.Questionnaire_Answers__r)
                {
                    if(answers.Questionnaire_Question__c == que.Id) 
                    {                  
                        options.add(new SelectOption(answers.Id ,answers.Answer__c));  
                    }
                }
                intval++;
                wrapperClass objWrap = new wrapperClass(options,que,selectedDefault);
                objWrap.SrNo = intval;
                lstNewWrap.add(objWrap );  
            }
        }
        objQuestionnaire = Questionnaire; 
    } 
  /******************************************************************************************************
Name : wrapperClass 
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    25/11/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/  
    public without sharing class wrapperClass 
    {
        public List<SelectOption> lstQuestionswrap{get;set;}
        public QuestionnaireQuestion__c Questions{get;set;}
        public String selectedAnswer {get;set;}
        public Integer SrNo {get;set;}
/******************************************************************************************************
   Description : Method for wrapper class
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/ 
        public wrapperClass (List<SelectOption> Answers , QuestionnaireQuestion__c Questions,String selectedAnswer)
        {
        this.lstQuestionswrap=Answers;
        this.Questions=Questions; 
        this.selectedAnswer=selectedAnswer;
        SrNo=0;
        }
    }
/******************************************************************************************************
    Method Name : submitMethod
    Description : Method to submit QuestionaireReply
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public void submitMethod ()
    {  
       // List<Questionnaire__c>lstQuestionnaireobj = new List<Questionnaire__c> ();
        //QuestionnaireReply__c objQuestionnaireply = new QuestionnaireReply__c ();
        //List<QuestionnaireReply__c> lstQuestionReplyForUpdateVal = new List<QuestionnaireReply__c> ();
        //List<QuestionnaireReply__c> lstQuestionReplyForUpdateNewVal = new List<QuestionnaireReply__c> ();
        List<QuestionnaireReply__c> lstQuestionReply = new List<QuestionnaireReply__c> ();
        try
        {
            RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                             DeveloperName =: GlobalConstants.QUESTIONNAIRE_REPLY_PROFILER_RECORDTYPE];
            //declaring object out of the loop                 
            QuestionnaireReply__c queReply;
            for(wrapperClass wrap : lstNewWrap)
            {
                queReply = new QuestionnaireReply__c();
                queReply=questreplyMap.get(wrap.Questions.Id);
                queReply.Question__c = wrap.Questions.Id;
                queReply.Answer__c = wrap.selectedAnswer;
                queReply.RecordTypeId = QuestionaireReplyRecType.Id;
                lstQuestionReply.add(queReply);
            }
            if(!lstQuestionReply.isEmpty())
            {
                upsert lstQuestionReply;   
            }   
          objQuestionnaire = [select id,Output__c from Questionnaire__c where id =: Questionnaire.id];     
        }
        Catch(Exception e)
        {
            String msg = e.getMessage();
            String throwmsg=GlobalConstants.EMPTYSTRING;
            
            if (msg.CONTAINS(GlobalConstants.CUSTOM_VALIDATION_ERROR))
            {
                throwmsg = msg.substringBetween (GlobalConstants.CUSTOM_VALIDATION_ERROR,GlobalConstants.ERROR_MESSAGE_END);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,throwmsg));
            }
        }
    } 
 /******************************************************************************************************
    Method Name : canclebutton 
    Description :Page Redirecting on clicking canclebutton 
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created       
 *********************************************************************************************************/
    public pagereference canclebutton () 
    {
        try
        {
            pagereference pgreference = new pagereference (GlobalConstants.URL_HARD_CODE +Questionnaire.Opportunity__c);
            pgreference.setRedirect(true);
            return pgreference;
        }catch(exception e)
        {
            return null;
        }
    }
/******************************************************************************************************
    Method Name : close
    Description :Page Redirecting on clicking Close
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created          
 *********************************************************************************************************/     
        public Pagereference close()
    {     
        try
        {
            PageReference orderPage = new PageReference(GlobalConstants.URL_HARD_CODE + Questionnaire.Opportunity__c);
            orderPage.setRedirect(true);
            return orderPage;
        }catch(exception e)
        {
            return null;
        }
    }
}