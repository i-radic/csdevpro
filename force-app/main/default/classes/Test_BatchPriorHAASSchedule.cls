@IsTest
public class Test_BatchPriorHAASSchedule {
    static testMethod void HAASBatchschedulerTest (){
        String CRON_EXP = '0 0 0 15 3 ? *';        
        // Create your test data
        TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
        settings.Account__c = FALSE;
        settings.Contact__c = FALSE;
        settings.Opportunity__c = FALSE;
        settings.OpportunitylineItem__c = FALSE;
        settings.Task__c = FALSE;
        settings.Event__c = FALSE;
        
        upsert settings TriggerDeactivating__c.Id;
        
        Contact Con = new Contact();
        Con.Lastname= 'test';
        insert Con;        
        Test.startTest();
        
        String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new BatchUpdatePriorConsentHAAS_Schedule());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();	    
        
    }
}