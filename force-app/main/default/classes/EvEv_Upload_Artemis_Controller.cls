/*
##############################################################
# Project Name..........: EveryThing Everywhere
# File..................: EvEv_Upload_Artemis_Controller.cls
# Version...............: 1
# Created by............: Kanishk Prasad
# Created Date..........: 12-September-2011
# Last Modified by......:    
# Last Modified Date....:   
# Description...........: This class is the controller for the Aretmeis Deal Management Screen. It contains methods to prepare data for initial Load 
#                         of the screen and has action handlers to support the user actions on the Visualforce page.
#						  The user will be able to 
#							1. See the list of Aretmis Deals associated with the Opportunity
#							2. Set any particular Deal as the Primary Deal
#							3. Go to the Artemis Deal Detail Page
#							4. Upload a new Artemis Deal from the Local system
#							5. View the Deal excel of any Aretmis Deal
# 
########################################################################### 
*/
public class EvEv_Upload_Artemis_Controller {

public transient Blob document {get;set;}
public transient String filename {get;set;} 
private String opportunityId;
public list<String> test {get; set;}
public String primaryDeal {get;set;}
public String oldPrimaryDeal{get;set;}
public String strMyDealId {get;set;}
private Map<String, CustomMessages__c> customMessages;
public String strPageTitleMessage {get;set;}
public list<Artemis_Deal__c> lstAretmisDeals {get; set;}
public String strDealMsg{get;set;}
public String strDealFileMsg{get;set;}
public String strInvalidFileFormatMsg{get;set;}



	// This method is passes the Opportunity Record to the controller. The Opportunity Record will then be used to retreive the correspodning Aretmis 
	// Deals. It also stores the Id of the existing Primary Deal.
	public EvEv_Upload_Artemis_Controller(ApexPages.StandardController acon) 
	{
	 	Opportunity opty =(Opportunity)acon.getRecord();
	 	opportunityId=opty.Id;
	  	lstAretmisDeals=[Select My_Deal_Id__c,Name,Process_Status__c,status__c,Primary_Deal__c,Attachment_Id__c from Artemis_Deal__c where Opportunity__c=:opportunityId order by id];
	  	
	  	oldPrimaryDeal=returnPrimaryDealId();
	  		 	
	 	customMessages = CustomMessages__c.getAll();
	 	strPageTitleMessage=customMessages.get('PageTitle').Message__c;
	 	strDealMsg=customMessages.get('EnterDealId').Message__c;
	 	strDealFileMsg=customMessages.get('DealTobeUploaded').Message__c;
	 	strInvalidFileFormatMsg=customMessages.get('InvalidFileFormat').Message__c;
	}
		
	// This method is used to upload the Excel Attachment passed from VF page to the controller.
	// It does initial validations of My DealId to be populated from VF and a file has been selected for upload
	// Once the validation checks are passed, the 
	//	1. file is inserted in SFDC as an attachment 
	//  2. A new Aretmis Deal record is inserted in SFDC linked to the attachment.
	//  3. The Deal is marked as Primary if there exists no othe rPrimary Deal for that Opportunity.
	//	4. The Process Status of the Deal record is set as "Started" to enable it to be picked up by the Informatica Process
	//  5. An information message is displayed to the user.
	public PageReference upload()
	{
		
	    if(EvEV_Helper.isBlank(strMyDealId))
	    {
	    	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,customMessages.get('EnterDealId').Message__c);
	    	ApexPages.addMessage(myMsg);
	    	return null;
	    }
	    
	    if(EvEV_Helper.isBlank(filename))
	    {
			ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,customMessages.get('DealTobeUploaded').Message__c);
	    	ApexPages.addMessage(myMsg);
	    	return null;
	    	
	    }
		//Integer intCount=[Select count() from Artemis_Deal__c where My_Deal_Id__c=:strMyDealId];
		//if(intCount==0)
		//{	
		    Artemis_Deal__c newDeal = new Artemis_Deal__c();
		    newDeal.Opportunity__c = opportunityId;
		    newDeal.My_Deal_Id__c=strMyDealId;
			
			oldPrimaryDeal=returnPrimaryDealId();	    
		    
		    if(EvEV_Helper.isBlank(oldPrimaryDeal)==false)
		    	newDeal.Primary_Deal__c =false;
		    	    
		    insert newDeal;
		    
		    Attachment myAttachment  = new Attachment();  
		    myAttachment.Body = document;  
		    myAttachment.Name = fileName;  
		    myAttachment.ParentId = newDeal.Id;  
		    insert myAttachment;  
		    
		   
		    newDeal.Attachment_Id__c = myAttachment.Id;
		    newDeal.Process_Status__c = 'Started';
		    if(EvEV_Helper.isBlank(oldPrimaryDeal)==false)
		    	newDeal.Primary_Deal__c =false;
		    update newDeal;
			
		   
	    	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,customMessages.get('UploadConfirmation').Message__c);
	    	ApexPages.addMessage(myMsg);
		//}
	    lstAretmisDeals=[Select My_Deal_Id__c,Name,Process_Status__c,status__c,Primary_Deal__c,Attachment_Id__c from Artemis_Deal__c where Opportunity__c=:opportunityId order by id];
	    strMyDealId='';
	    
		return null;
	}

    // This method is used to return to the Opportunity Detail page
	public PageReference backToOpportunity()
	{
		return new PageReference('/'+opportunityId);
	}
	
	// This method is used to return to the Opportunity Detail page
	public PageReference goToDealDetailPage()
	{
		return new PageReference('/'+opportunityId);
	}
	
	// This method is used to mark a Deal from the list od Deals as Primary Deal.
	// Does an initial validation check on whether a new Primary Deal was selected
	// Updates the old Primary Deal by setting its Primary Deal field value as "FALSE"
	// Updates the selected Deal as the Primary Deal by setting its Primary Deal field value as "TRUE"
	// Displays a confirmatory message to the User.
	public PageReference updatePrimaryDeal()
	{
		if(EvEV_Helper.isBlank(primaryDeal) && EvEV_Helper.isBlank(oldPrimaryDeal)==false )
		{
			return null;		
		}
		if(EvEV_Helper.isBlank(primaryDeal))
	    {
	    	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,customMessages.get('MarkOneDealAsPrimary').Message__c);
	    	ApexPages.addMessage(myMsg);
	    	return null;
	    }
	    oldPrimaryDeal=returnPrimaryDealId();
	    String strMessage;
		if(EvEV_Helper.isBlank(oldPrimaryDeal)==false)
		{
			Artemis_Deal__c oldPrimaryDealRec = new Artemis_Deal__c(id=oldPrimaryDeal);
			oldPrimaryDealRec.Primary_Deal__c=false;
			update oldPrimaryDealRec;
		}
		Artemis_Deal__c newPrimaryDeal = new Artemis_Deal__c(id=primaryDeal);
		newPrimaryDeal.Primary_Deal__c=true;
		update newPrimaryDeal;
		oldPrimaryDeal=primaryDeal;
		lstAretmisDeals=[Select My_Deal_Id__c,Name,Process_Status__c,status__c,Primary_Deal__c,Attachment_Id__c from Artemis_Deal__c where Opportunity__c=:opportunityId order by id];
		Artemis_Deal__c newPrimaryAretmisDeal=[Select id,name,My_Deal_Id__c from Artemis_Deal__c where id =:primaryDeal Limit 1];
		
		if(null!=newPrimaryAretmisDeal && null!=newPrimaryAretmisDeal.My_Deal_Id__c && newPrimaryAretmisDeal.My_Deal_Id__c!='')
			strMessage=newPrimaryAretmisDeal.My_Deal_Id__c;
		else
			strMessage=newPrimaryAretmisDeal.Name;
			
		ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,strMessage+' '+customMessages.get('UpdateDealConfirmation').Message__c);
	    ApexPages.addMessage(myMsg);
	    
		return null;
	}
	
	private String returnPrimaryDealId()
	{
		List<Artemis_Deal__c> tempLstAretmisDeals=[Select id from Artemis_Deal__c where Opportunity__c=:opportunityId and Primary_Deal__c=true];
	  	if(EvEV_Helper.isListEmpty(tempLstAretmisDeals)==false)
	 		return tempLstAretmisDeals[0].id;
	 	else
	 		return'';
	}
	
}