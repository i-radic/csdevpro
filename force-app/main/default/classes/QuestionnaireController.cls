/******************************************************************************************************
Name : QuestionnaireController
Description : Controller for page EditredirectQuestionnariepage
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    1/12/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public without sharing class QuestionnaireController 
{
    public List<QuestionAnswerWrapperClass> wrapper {get;set;}
    public String gateName{get;set;}
    public id opptyId;
    public boolean showscore {get;set;}
    public Double score {get;set;}
    public integer counter;
     public map<id,QuestionnaireReply__c> questreplyMap = new map<id,QuestionnaireReply__c>();
     public Questionnaire__c objQustion = new Questionnaire__c ();
 /******************************************************************************************************
    Method Name : QuestionnaireController
    Description : Wrapper class for QuestionAnswerWrapperClass
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public QuestionnaireController ()
    {
        counter =0;
        wrapper  =  new List<QuestionAnswerWrapperClass>();
        opptyId =System.currentPagereference().getParameters().get('opp');
        Opportunity opptyRecord = new Opportunity();
        if(opptyId != null)
        {
            opptyRecord = [Select id, StageName,CSC_Call_Pass__c from Opportunity where id = :opptyId];
            if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_PRE_QUALIFICATION ))
            {
                gateName=GlobalConstants.GATENAME_Make_Pursuit;
            }
            else if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_QUALIFICATION))
            {
                gateName=GlobalConstants.GATENAME_Sign_On;
            }
            else if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_DEVELOP_SOLUTION ))
            {
                if(opptyRecord.CSC_Call_Pass__c)
                {
                    gateName=GlobalConstants.GATENAME_Sign_Off;
                }
                else
                {
                    gateName=GlobalConstants.GATENAME_CSC_Call;
                }
            }
            else if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_PRESENT_NEGOTIATE ))
            {
                gateName =GlobalConstants.GATENAME_Contract_Sign_Off;
            }
        }
        List<QuestionnaireQuestion__c> questionnaireList =  new  List<QuestionnaireQuestion__c>();
        String catgory=GlobalConstants.EMPTYSTRING;
        String toparea= GlobalConstants.EMPTYSTRING;
        integer srn =0;
        questionnaireList = [Select id,Opportunity_Gates__c,Order__c,Question__c,Category__c,Topic_Area__c,Weighting__c, (Select id,Answer__c,Order__c,Score__c from Questionnaire_Answers__r order by Order__c) from QuestionnaireQuestion__c where RecordType.Name= 'Qualification' and Opportunity_Gates__c INCLUDES (:gateName) order by Order__c];
        if(! questionnaireList.isEmpty())
        {
            for(QuestionnaireQuestion__c questions : questionnaireList)
            {
                List<QuestionnaireAnswer__c > ansList = questions.Questionnaire_Answers__r;
                List<SelectOption> options = new List<SelectOption>();
                String selectedDefault;
                for(QuestionnaireAnswer__c answers : ansList )
                {
                    if(answers.Answer__c != null)
                    {
                        options.add(new SelectOption(answers.id,answers.Answer__c));
                    }
                    if(answers.Score__c == 1)
                    {
                        selectedDefault=answers.id;
                    }
                }
                if(catgory != questions.Category__c)
                {
                    srn=1;
                    catgory=questions.Category__c;
                }
                else
                {
                    srn+=1;
                    catgory=GlobalConstants.EMPTYSTRING;
                }
                if(toparea != questions.Topic_Area__c)
                {
                    toparea=questions.Topic_Area__c;
                }
                else
                {
                    toparea=GlobalConstants.EMPTYSTRING;
                }
                wrapper.add(new QuestionAnswerWrapperClass(options,questions,selectedDefault,catgory,toparea,srn));
            }
        }       
    }
/******************************************************************************************************
    Name : QuestionAnswerWrapperClass
    Description : Wrapper class QuestionAnswerWrapperClass
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Class created        
 *********************************************************************************************************/
    public without sharing class QuestionAnswerWrapperClass
    {
        public List<SelectOption> answers{get;set;}
        public QuestionnaireQuestion__c questions{get;set;}
        public String selectedAnswer {get;set;}
        public String category{get;set;}
        public String topicarea{get;set;}
        public integer srn{get;set;}
 /******************************************************************************************************
    Method Name : QuestionAnswerWrapperClass 
    Description : To Select Option
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
        public QuestionAnswerWrapperClass (List<SelectOption> answers , QuestionnaireQuestion__c questions,String selectedAnswer,String category,String topicarea,integer srn ) 
        {
            this.answers=answers;
            this.questions=questions;    
            this.selectedAnswer =selectedAnswer;   
            this.category=category;
            this.topicarea=topicarea;  
            this.srn=srn;
        }
    }
 /******************************************************************************************************
    Method Name : submitMethod
    Description : Method to submit QuestionaireReply
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public void submitMethod () 
    {
        List<QuestionnaireReply__c> qustreplyList = new List<QuestionnaireReply__c>();
        try
        {
            if(counter == 0)
            {
                System.debug(opptyId  +'>>>>opptyId ');
                objQustion.Opportunity__c = opptyId  ;
                objQustion.Gate__c = gatename;       
                RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE];
                objQustion.RecordTypeId = QuestionaireRecType.Id;
                 System.debug(objQustion+'>>>>objQustion'); 
                insert objQustion;
               
            }
            else
            {
                qustreplyList=[select id,Answer__c,Question__c,Questionnaire__c from QuestionnaireReply__c where Questionnaire__c =:objQustion.id];
                 for(QuestionnaireReply__c reply : qustreplyList)
                {
                    questreplyMap.put(reply.Question__c,reply);
                }
            }
               
            List<QuestionnaireReply__c> lstQuestionReply = new List<QuestionnaireReply__c> ();
            RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                                 DeveloperName =: GlobalConstants.QUESTIONARRE_REPLY_QUALIFICATION_RECORDTYPE];
           //declaring object out of the loop
           QuestionnaireReply__c queReply;
            for(QuestionAnswerWrapperClass wrap : wrapper)
            {
                queReply = new QuestionnaireReply__c();
                if(counter == 0)
                {
                    queReply.Questionnaire__c = objQustion.Id;
                }
                else
                {
                    queReply=questreplyMap.get(wrap.Questions.Id);
                    system.debug(queReply+'queReply>>');
                }
                queReply.Question__c = wrap.Questions.Id;
                queReply.Question_Category__c = wrap.Questions.Topic_Area__c;
                queReply.Answer__c = wrap.selectedAnswer;
                queReply.RecordTypeId = QuestionaireReplyRecType.Id;
                lstQuestionReply.add(queReply);
            }
            if(!lstQuestionReply.isEmpty())
            {
                upsert lstQuestionReply;   
            }  
            if(objQustion.id != null)
            {
                counter +=1;
            }     
            score =[select id,Overall_Score__c from Questionnaire__c where id =:objQustion.id].Overall_Score__c;
            showscore =true;
        }
        Catch(Exception e)
        {
            String msg = e.getMessage();
            String throwmsg=GlobalConstants.EMPTYSTRING;
            
            if (msg.CONTAINS(GlobalConstants.CUSTOM_VALIDATION_ERROR))
            {
                throwmsg = msg.substringBetween (GlobalConstants.CUSTOM_VALIDATION_ERROR,GlobalConstants.ERROR_MESSAGE_END);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,throwmsg));
            }
        }  
    }  
 /******************************************************************************************************
    Method Name : cancel
    Description :Page Redirecting on clicking Cancel
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created       
 *********************************************************************************************************/
    public Pagereference cancel()
    {     
        try
        {
            PageReference orderPage = new PageReference(GlobalConstants.URL_HARD_CODE + opptyid);
            orderPage.setRedirect(true);
            return orderPage;
        }catch (exception e)
        {
            return null;
        }
    }
/******************************************************************************************************
    Method Name : close
    Description :Page Redirecting on clicking Close
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created          
 *********************************************************************************************************/
    public Pagereference close()
    {     
        try
        {
            PageReference orderPage = new PageReference(GlobalConstants.URL_HARD_CODE+ opptyid);
            orderPage.setRedirect(true);
            return orderPage;
        }catch (exception e)
        {
        return null;
        }
    }
}