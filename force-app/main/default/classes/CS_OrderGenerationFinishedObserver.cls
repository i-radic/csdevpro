global with sharing class CS_OrderGenerationFinishedObserver implements csordcb.ObserverApi.IObserver {

    global CS_OrderGenerationFinishedObserver() {}

    global void execute(csordcb.ObserverApi.Observable o, Object arg) {
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable)o;
        cssmgnt.API_1.linkSubscriptions(((csordtelcoa.OrderGenerationObservable) o).getSubscriptionIds());


        List<Id> orderIds = observable.getOrderIds();
        Id jobId = System.enqueueJob(new CS_CreateDownstreamJSONCSVPayload(orderIds));

    }
}