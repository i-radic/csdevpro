/**
 * @name CS_JSON_Constants
 * @description Constats for JSON Schema
 *
 */
public class CS_JSON_Constants {
	public static final String Attributes_Suffix = '-attributes';
	public static final String Grouping_Separator = '|';
	
	public static final String Site_SpecificationName = 'Site';
	
	public static final String ObjectType_ProductDefinition = 'Product Definition';
	public static final String ObjectType_JSONSchema = 'JSON Schema';
	
	public static final String FieldType_JSON = 'JSON';
	public static final String FieldType_String = 'String';
	
	public static final String AttributeField_Value = 'cscfga__Value__c';
}