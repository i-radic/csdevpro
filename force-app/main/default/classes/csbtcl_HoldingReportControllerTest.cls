@isTest
public class csbtcl_HoldingReportControllerTest {
    
     @isTest()
    public static void testHoldingReport() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
          Account account = new Account(
            OwnerId = UserInfo.getUserId(),
            Name = 'Account11',
            Type = 'End Customer'
        );
        insert account;
     Opportunity opportunity = new Opportunity(
            Name = 'New Opportunity1',
            OwnerId = UserInfo.getUserId(),
            StageName = 'Qualification',
            Probability = 0,
            CloseDate = system.today(),
            AccountId = account.id
        );
        insert opportunity;

        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = 'PD1',
            cscfga__Description__c = 'PD1 Desc'
        );
        insert pd;

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId(),
            cscfga__Opportunity__c = opportunity.Id
        );
        insert basket;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
            cscfga__Product_Definition__c = pd.Id,   
            cscfga__Product_Basket__c = basket.Id,
            Name='PC1',
            Ave_Days_Remaining_Months__c = 4
        );
        insert pc;
 
        BT_Resign_Option_Settings__c resOpt = new BT_Resign_Option_Settings__c(name = 'Mid Term –Buyout Resign', Maximum_Remaining_Months__c = 20,Minimum_Remaining_Months__c = 3,Description__c='testing');
        insert resOpt;
        
        ApexPages.StandardController controller;
        ApexPages.currentPage().getParameters().put('basketId',basket.id);
        ApexPages.currentPage().getParameters().put('pcId',pc.id);
        csbtcl_HoldingReportController hr = new csbtcl_HoldingReportController(controller);
        hr.importCSVFile();
        List<SelectOption> optionsList = hr.getResignTypeOptions();
        hr.resignOption = 'Mid Term –Buyout Resign';
        hr.getResignDescription();
        hr.applicableETCs = '200';
        hr.saveDataAndgoToBasket();
        hr.applicableETCs = '90';
        hr.saveDataAndgoToBasket();

        csbtcl_HoldingReportController hr2 = new csbtcl_HoldingReportController();
        hr2.csvFileName = 'test.csv';
        hr2.importCSVFile();
    }
}