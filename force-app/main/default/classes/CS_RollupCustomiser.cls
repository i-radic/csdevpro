global class CS_RollupCustomiser extends cscfga.AConfigurationCustomisation
{
	global override void afterSave(cscfga.ProductConfiguratorController ctrl)
	{
		//
		// For root configuration calculate NPV GOM
		//
		if (ctrl.getConfig().isRoot())
		{	
			/*
			cscfga__Product_Configuration__c rootConfig =
				ctrl.getConfig().getSObject();
			*/
			String basketId = ctrl.basket.Id;
			String configId = ctrl.getConfig().getSObject().Id;
			//
			// Get all descendants
			//
			/*
			List<cscfga__Product_Configuration__c> descendants =
				new List<cscfga__Product_Configuration__c>();
			if (rootConfig.Id != null)
			{
				descendants =
					[ SELECT Id, cscfga__Parent_Configuration__c,
					  Number_of_users__c, NPV_GOM_per_annum__c,
					  Total_Charges__c, Total_Cost__c,
					  Total_Summarized_Cost__c, Total_Summarized_Revenue__c,
					  cscfga__Contract_Term__c
					  FROM cscfga__Product_Configuration__c
					  WHERE cscfga__Root_Configuration__c = :rootConfig.Id
					  or Id = :rootConfig.Id
					  ];
				for (cscfga__Product_Configuration__c pc : descendants) {
					if (rootConfig.Id == pc.Id) {
						rootConfig = pc;
					}
				}
			}
			*/
			
			cscfga__Product_Basket__c basket = Database.query(
				'select ' + CS_Utils.getSobjectFields('cscfga__Product_Basket__c')
				+ ' from cscfga__Product_Basket__c'
				+ ' where id = :basketId'
			);
			List<cscfga__Product_Configuration__c> configs = Database.query(
				'select ' + CS_Utils.getSobjectFields('cscfga__Product_Configuration__c') 
				+ ',cscfga__Product_Definition__r.Name '
				+ ' from cscfga__Product_Configuration__c'
				+ ' where cscfga__Root_Configuration__c = :configId'
				+ ' or Id = :configId'
			);
			cscfga__Product_Configuration__c rootConfig;
			Decimal networkCost;
			Decimal opexCost;
			for (cscfga__Product_Configuration__c pc : configs) {
				if (pc.cscfga__Root_Configuration__c == null) {
					rootConfig = pc;
					networkCost = pc.Service_Recurring_Cost__c;
					opexCost = pc.OPEX_Recurring_Cost__c;
				}
				pc.Service_Recurring_Cost__c = 0;
				pc.OPEX_Recurring_Cost__c = 0;
			}
			CS_PLReportController plReport = new CS_PLReportController(basket.Id, new cscfga__Product_Basket__c(Name = 'Test', Tenure__c = rootConfig.Tenure__c), configs);
			Decimal NPVGOM;
			for (CS_PLReportController.Data data : plReport.Data) {
				if (Integer.valueOf(data.Name) == Integer.valueOf(rootConfig.Tenure__c)) {
					NPVGOM = data.data1 - data.data2;
				}
			}
			Decimal subscriptions = rootConfig.Number_of_users__c;
			Decimal contractTerm = rootConfig.Tenure__c;
			if (subscriptions != null && subscriptions > 0 && contractTerm != null && contractTerm > 0) {
				rootConfig.NPV_GOM_per_annum__c = NPVGOM * 12.0 / subscriptions / contractTerm;
			} else {
				rootConfig.NPV_GOM_per_annum__c = 0;
			}

			//
			// Create tree
			//
			/*
			List<cscfga__Product_Configuration__c> configs =
				new List<cscfga__Product_Configuration__c>(descendants);
			*/
			//configs.add(rootConfig);

			List<CS_Node> rootNodes = CS_Node.createTree(configs);
			/*
			Decimal NPVGOM =
				(Decimal)CS_Node.foldTree
					( 0.0
					, rootNodes
					, new CS_Node.FoldSumExpression('Total_Charges__c + Total_Cost__c') );
			Decimal subscriptions =
				(Decimal)CS_Node.foldTree
					( 0.0
					, rootNodes
					, new CS_Node.FoldSumExpression('Number_of_users__c') );
			
			Decimal contractTerm = rootConfig.cscfga__Contract_Term__c;
			System.debug(LoggingLevel.ERROR, contractTerm);

			if (subscriptions != 0.0
				&& contractTerm != null
				&& contractTerm != 0.0)
			{
				rootConfig.NPV_GOM_per_annum__c =
					NPVGOM * 12.0 / subscriptions / contractTerm;
			}
			else
			{
				rootConfig.NPV_GOM_per_annum__c = 0;
			}
			*/
			rootConfig.Total_Summarized_Revenue__c =
				(Decimal)CS_Node.foldTree
					( 0.0
					, rootNodes
					, new CS_Node.FoldSumExpression('Total_Charges__c') );

			rootConfig.Total_Summarized_Cost__c =
				(Decimal)CS_Node.foldTree
					( 0.0
					, rootNodes
					, new CS_Node.FoldSumExpression('Total_Cost__c') );
			rootConfig.OPEX_Recurring_Cost__c = opexCost;
				rootConfig.Service_Recurring_Cost__c = networkCost;
			System.debug(LoggingLevel.ERROR, 'Root config: ' + rootConfig);
			update rootConfig;
		}
		//
		// For child configurations inherit product family from the root
		//
		else
		{
			cscfga__Product_Configuration__c config =
				ctrl.getConfig().getSObject();
			cscfga__Product_Configuration__c rootConfig =
				ctrl.getRootConfig().getSObject();
			config.cscfga__Product_Family__c =
				rootConfig.cscfga__Product_Family__c;
			update config;
			System.debug('Config: ' + config);
		}
	}
}