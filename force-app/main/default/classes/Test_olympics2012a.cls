@isTest

private class Test_olympics2012a{
    
    static testmethod void myUnitTest() {
    
    // Create a new email and envelope object
    Messaging.InboundEmail email = new Messaging.InboundEmail();
    Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
     
    // Create the plainTextBody and fromAddres for the test
    email.plainTextBody = 'Here is my plainText body of the email';
    email.fromAddress ='qwerty@test.com';
    
    Messaging.InboundEmail.TextAttachment inbTextAttchment = new Messaging.InboundEmail.TextAttachment();
    inbTextAttchment.body = 'Request Type,Contact email address,Contact number,Alterative contact,Order ID,Connect email,Broadband telephone number,Customer Name,Customer Contact Number,Address/Location description,Sub location/sub premises,Building Name,Building number,Street,Post Town,County,Post code,PSTN CRD,Appointment date,TRC,ADSL CRD,IP address,PSTN Term Min (months),Hazard and warning notes,Exhange Group Code,Generic Notes,Router Required'+'\n'+'RateCard,gci.complex.customers@bt.com,7756410042,7756410042,002012-000046,2012-700181@btconnect.com,blank,Steven Jackson,7834303490,LOCOG Office,TER,Portland Marina,6,Hamm Beach Road, Portal, Dorset,DT51DX,20/07/12,20/07/12,0,25/07/12,blank,2,None,Portland PLX,None,No'+'\n'+'LOCOG,gci.complex.customers@bt.com,7756410042,7756410042,002012-000046,2012-700181@btconnect.com,blank,Steven Jackson,7834303490,LOCOG Office,TER,Portland Marina,6,Hamm Beach Road, Portal, Dorset,DT51DX,20/07/12,20/07/12,0,25/07/12,blank,2,None,Portland PLX,None,No';
    inbTextAttchment.fileName = 'myAttachment.csv';
    Messaging.InboundEmail.TextAttachment[] textAttachs = new Messaging.InboundEmail.TextAttachment[1];
    textAttachs[0] = inbTextAttchment;   
    email.textAttachments = textAttachs; 
         
    olympics2012a oly= new olympics2012a();
    oly.handleInboundEmail(email, env);
    
    }
}