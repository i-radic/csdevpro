public with sharing class ISDNEditController {

// Added By Krupakar Jogannagari
public ISDN_CRF__c ISDN {get; set;}
public Channels__c Channel  {get; set;}
// Added By Krupakar Jogannagari


public String crfId{get;set;}
public List<ISDN_CRF__c> isdnList;
public List<Channels__c> channelList;
public String RecordTypeName;
public String CRFRTId;

public CRF__c crf;

Public String BT{get;set;}
Public String channelsId;
Public String ISDNId;

public boolean validatefields() {
    String str = '[0-9]*';
    Pattern Mypattern = Pattern.compile(str);
    Matcher m3 = Mypattern.matcher(isdnList[0].No_of_DDI_s__c);
    Matcher m4 = Mypattern.matcher(isdnList[0].Total_No_of_DDI_Channels_required__c);
    Matcher m5 = Mypattern.matcher(isdnList[0].Total_No_of_non_DDI_Channels_required__c);
    Matcher m6 = Mypattern.matcher(isdnList[0].VP_Number__c);
    if ((!((m3.matches()) && (m4.matches()) && (m5.matches()) && (m6.matches()))) || (((BT == 'No') && (ISDN.Tel_no_A_c_no__c == '')) || ((ISDN.One_Bill__c == true) && (ISDN.VP_Number__c == '')) || ((ISDN.Discount_Plan__c == true) && (ISDN.Which_Discount_Plan__c == '')) || (ISDN.Room_No_where_NTE_to_be_fitted__c == '') || (BT == '-None-') || (ISDN.Floor_No_where_NTE_to_be_fitted__c == '') || ((ISDN.Dual_Parenting__c == true) && (ISDN.LOP_ref__c == '')) || ((ISDN.Dual_Parenting__c == true) && (ISDN.Secondary_exchange_name_and_code__c == '')) || ((ISDN.Diverse_Routing__c == true) && (ISDN.LOP_ref_DR__c == '')) || ((ISDN.Diverse_Routing__c == true) && (ISDN.Remote_exchange_name_and_code_DR__c == '')) || (String.isBlank(ISDN.New_BLDG_No_BT_Lines_Or_Services__c)) || ((ISDN.Site_Assurance__c == true) && ((ISDN.Option1_ACCD__c == false) && (ISDN.Option2__c == false))))) {
        if (!(m3.matches())) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Field-No of DDIs :: Please enter a valid number!'));
        }
        if (!(m4.matches())) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Field-Total No of DDI Chennels required :: Please enter a valid number!'));
        }
        if (!(m5.matches())) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Field-Total No of non DDI Chennels required :: Please enter a valid number!'));
        }
        if (!(m6.matches())) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Field-VP Number :: Please enter a valid number!'));
        }

        if ((ISDN.Room_No_where_NTE_to_be_fitted__c == '') || (ISDN.Floor_No_where_NTE_to_be_fitted__c == '') || (BT == '-None-') || ((ISDN.Dual_Parenting__c == true) && (ISDN.LOP_ref__c == '')) || ((ISDN.Dual_Parenting__c == true) && (ISDN.Secondary_exchange_name_and_code__c == '')) || ((ISDN.Diverse_Routing__c == true) && (ISDN.LOP_ref_DR__c == '')) || ((ISDN.Diverse_Routing__c == true) && (ISDN.Remote_exchange_name_and_code_DR__c == ''))) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please fill all the mandatory fields!'));
        }
        if (((BT == 'No') && (ISDN.Tel_no_A_c_no__c == ''))) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'As the Is this a new customer to BT is No please fill Classic Tel no. or A/c no.'));
        }
        if ((ISDN.One_Bill__c == true) && (ISDN.VP_Number__c == '')) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'As the One Bill is checked please fill VP Number'));
        }
        if ((ISDN.Discount_Plan__c  == true) && (ISDN.Which_Discount_Plan__c == '')) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'As the Discount Plan is checked please fill Which Discount Plan'));
        }
        if (((ISDN.Site_Assurance__c == true) && ((ISDN.Option1_ACCD__c == false) && (ISDN.Option2__c == false)))) {

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'As the Site Assurance is checked please select either Option1 or Option2 in Appendix A'));
        }
        if (String.isBlank(ISDN.New_BLDG_No_BT_Lines_Or_Services__c)) {
      		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select New BLDG - no BT lines or services'));
    	}
        return false;
    }
    else return true;
}

public ISDNEditController(ApexPages.StandardController controller) {
    this.crf = (CRF__c) controller.getRecord();
	ISDN = new ISDN_CRF__c();
	Channel = new Channels__c();
    crfId = Apexpages.currentPage().getParameters().get('id');
    channelsId = Apexpages.currentPage().getParameters().get('channelId');
    ISDNId = Apexpages.currentPage().getParameters().get('isdncrfId');

    isdnList = [Select Id, Bearer_Location__c, New_BLDG_No_BT_Lines_Or_Services__c, Alternate_Bearer__c, Alternative_Routing_check_1__c, Alternative_Routing_check_2__c, Customer_Name_ISDN__c, Customer_Name_ISDN_DP__c, Diverse_Routing_check_1__c, Diverse_Routing_check_2__c, Diverse_Routing_DP__c, Fax_ISDN__c, Fax_ISDN_DP__c, LOP_ref__c, LOP_ref_DR__c, Named_Contacts_and_contact_numbers_1__c, Named_Contacts_and_contact_numbers_2__c, Named_Contacts_and_contact_numbers_3__c, Password__c, Remote_exchange_name_and_code_DR__c, Sales_Agent__c, Secondary_exchange_name_and_code__c, Tel__c, Telephone_No_ISDN__c, Telephone_No_ISDN_DP__c, Additional_services_to_be_provided__c, ADSL_CRD__c, Alternative_Routing__c, Bus_Con_Option__c, Circuit_Number__c, Contract_term__c, Conversions__c, Customer_accepts_if_6_digits_provided__c, Customer_Contact__c, Customer_Contact_Number__c, Customer_Name_DASS2__c, Customer_Name__c, Cust_Provide_Power_from_Switch__c, DASS_2__c, DDI_Range_or_Single_numbers_1__c, DDI_Range_or_Single_numbers_2__c, DDI_Range_or_Single_numbers_3__c, DDI_Range_or_Single_numbers_4__c, DDI_Range_or_Single_numbers_5__c, DDI_Range_or_Single_numbers_10__c, DDI_Range_or_Single_numbers_6__c, DDI_Range_or_Single_numbers_7__c, DDI_Range_or_Single_numbers_8__c, DDI_Range_or_Single_numbers_9__c, Directory_Entry__c, Directory_No_digits_to_forward_by_n_w__c, Directory_No_digits_to_forward_to_switch__c, Discount_Plan__c, Diverse_Routing__c, Divert_to_A_1__c, Divert_to_A_2__c, Divert_to_A_3__c, Divert_to_A_4__c, Divert_to_A_5__c, Divert_to_A_6__c, Divert_to_B_1__c, Divert_to_B_2__c, Divert_to_B_3__c, Divert_to_B_4__c, Divert_to_B_5__c, Divert_to_B_6__c, Divert_to_C_1__c, Divert_to_C_2__c, Divert_to_C_3__c, Divert_to_C_4__c, Divert_to_C_5__c, Divert_to_C_6__c, Dual_Parenting__c, Floor_No_where_NTE_to_be_fitted__c, From_ISDN_1__c, From_ISDN_2__c, Is_a_post_commission_test_required__c, ISDN_30e__c, Is_this_a_new_customer_to_BT__c, Itemised_Billing__c, Main_Bearer_cct_1__c, Main_Bearer_cct_10__c, Main_Bearer_cct_2__c, Main_Bearer_cct_3__c, Main_Bearer_cct_4__c, Main_Bearer_cct_5__c, Main_Bearer_cct_6__c, Main_Bearer_cct_7__c, Main_Bearer_cct_8__c, Main_Bearer_cct_9__c, Main_or_DDI_Number__c, Maintenance_Type__c, No_of_DDI_s__c, Notes__c, NOU_contact_details__c, Number_of_Channels_if_applicable_1__c, Number_of_Channels_if_applicable_10__c, Number_of_Channels_if_applicable_11__c, Number_of_Channels_if_applicable_12__c, Number_of_Channels_if_applicable_13__c, Number_of_Channels_if_applicable_14__c, Number_of_Channels_if_applicable_15__c, Number_of_Channels_if_applicable_16__c, Number_of_Channels_if_applicable_17__c, Number_of_Channels_if_applicable_18__c, Number_of_Channels_if_applicable_19__c, Number_of_Channels_if_applicable_2__c, Number_of_Channels_if_applicable_20__c, Number_of_Channels_if_applicable_3__c, Number_of_Channels_if_applicable_4__c, Number_of_Channels_if_applicable_5__c, Number_of_Channels_if_applicable_6__c, Number_of_Channels_if_applicable_7__c, Number_of_Channels_if_applicable_8__c, Number_of_Channels_if_applicable_9__c, Number_Porting__c, One_Bill__c, Option1_ACCD__c, Option2__c, Order_type__c, Order_Type_2__c, Other_Associated_ISDN_30_Circuit_Numbers__c, Out_of_Hours_1__c, Out_of_Hours_2__c, Range_1_1__c, Range_1_2__c, Range_2_1__c, Range_2_2__c, Range_3_1__c, Range_3_2__c, Range_4_1__c, Range_4_2__c, Related_to_CRF__c, Room_No_where_NTE_to_be_fitted__c, Service_Centre__c, Service_Type__c, Serving_Tel_Exch__c, Site_Assurance__c, Site_Contact__c, Site_Contact_Tel_No__c, Site_Name__c, Special_Requirements__c, Standby__c, Standby_Bearer_cct1__c, Standby_Bearer_cct_10__c, Standby_Bearer_cct_2__c, Standby_Bearer_cct_3__c, Standby_Bearer_cct_4__c, Standby_Bearer_cct_5__c, Standby_Bearer_cct_6__c, Standby_Bearer_cct_7__c, Standby_Bearer_cct_8__c, Standby_Bearer_cct_9__c, Standby_Power__c, Supplementary_services_required__c, Switch_Supplier__c, Telephone_Number_1__c, Telephone_Number_10__c, Telephone_Number_2__c, Telephone_Number_3__c, Telephone_Number_4__c, Telephone_Number_5__c, Telephone_Number_6__c, Telephone_Number_7__c, Telephone_Number_8__c, Telephone_Number_9__c, Telephone_Number_or_DDI_range_1__c, Telephone_Number_or_DDI_range_10__c, Telephone_Number_or_DDI_range_2__c, Telephone_Number_or_DDI_range_3__c, Telephone_Number_or_DDI_range_4__c, Telephone_Number_or_DDI_range_5__c, Telephone_Number_or_DDI_range_6__c, Telephone_Number_or_DDI_range_7__c, Telephone_Number_or_DDI_range_8__c, Telephone_Number_or_DDI_range_9__c, Tel_No__c, Tel_no_A_c_no__c, To_ISDN_1__c, To_ISDN_2__c, Total_Channels__c, Total_No_of_DDI_Channels_required__c, Total_No_of_non_DDI_Channels_required__c, Transmission_Medium__c, Type_of_Forwarding_1__c, Type_of_Forwarding_2__c, Type_of_ISPBX__c, VP_Number__c, Which_Discount_Plan__c, Type_of_ISPBX_Known__c, Supplier_of_ISPBX_if_known__c  from ISDN_CRF__c where Related_to_CRF__c = : crfId limit 1];
    channelList = [SELECT Id, CLIP_A18025_1__c, CLIP_A18025_3__c, CLIP_A18025_2__c, CLIP_A18025_4__c, CLIP_A18025_5__c, CLIP_A18025_6__c, CLIP_A18025_7__c, CLIP_A18025_8__c, CLIP_A18025_9__c, CLIP_A18025_10__c, CLIP_A18025_11__c, CLIP_A18025_12__c, CLIP_A18025_13__c, CLIP_A18025_14__c, CLIP_A18025_15__c, CLIP_A18025_16__c, CLIP_A18025_17__c, CLIP_A18025_18__c, CLIP_A18025_19__c, CLIP_A18025_20__c, CLIP_A18025_21__c, CLIP_A18025_22__c, CLIP_A18025_23__c, CLIP_A18025_24__c, CLIP_A18025_25__c, CLIP_A18025_26__c, CLIP_A18025_27__c, CLIP_A18025_28__c, CLIP_A18025_29__c, CLIP_A18025_30__c, CLIR_A18026_1__c, CLIR_A18026_3__c, CLIR_A18026_2__c, CLIR_A18026_4__c, CLIR_A18026_5__c, CLIR_A18026_6__c, CLIR_A18026_7__c, CLIR_A18026_8__c, CLIR_A18026_9__c, CLIR_A18026_10__c, CLIR_A18026_11__c, CLIR_A18026_12__c, CLIR_A18026_13__c, CLIR_A18026_14__c, CLIR_A18026_15__c, CLIR_A18026_16__c, CLIR_A18026_17__c, CLIR_A18026_18__c, CLIR_A18026_19__c, CLIR_A18026_20__c, CLIR_A18026_21__c, CLIR_A18026_22__c, CLIR_A18026_23__c, CLIR_A18026_24__c, CLIR_A18026_25__c, CLIR_A18026_26__c, CLIR_A18026_27__c, CLIR_A18026_28__c, CLIR_A18026_29__c, CLIR_A18026_30__c, Divert_all_calls_A18016_1__c, Divert_all_calls_A18016_3__c, Divert_all_calls_A18016_2__c, Divert_all_calls_A18016_4__c, Divert_all_calls_A18016_5__c, Divert_all_calls_A18016_6__c, Divert_all_calls_A18016_7__c, Divert_all_calls_A18016_8__c, Divert_all_calls_A18016_9__c, Divert_all_calls_A18016_10__c, Divert_all_calls_A18016_11__c, Divert_all_calls_A18016_12__c, Divert_all_calls_A18016_13__c, Divert_all_calls_A18016_14__c, Divert_all_calls_A18016_15__c, Divert_all_calls_A18016_16__c, Divert_all_calls_A18016_17__c, Divert_all_calls_A18016_18__c, Divert_all_calls_A18016_19__c, Divert_all_calls_A18016_20__c, Divert_all_calls_A18016_21__c, Divert_all_calls_A18016__c, Divert_all_calls_A18016_22__c, Divert_all_calls_A18016_23__c, Divert_all_calls_A18016_24__c, Divert_all_calls_A18016_25__c, Divert_all_calls_A18016_26__c, Divert_all_calls_A18016_27__c, Divert_all_calls_A18016_28__c, Divert_all_calls_A18016_29__c, Divert_all_calls_A18016_30__c, Divert_no_reply_A18018_1__c, Divert_no_reply_A18018_3__c, Divert_no_reply_A18018_2__c, Divert_no_reply_A18018_4__c, Divert_no_reply_A18018_5__c, Divert_no_reply_A18018_6__c, Divert_no_reply_A18018_7__c, Divert_no_reply_A18018_8__c, Divert_no_reply_A18018_9__c, Divert_no_reply_A18018_10__c, Divert_no_reply_A18018_11__c, Divert_no_reply_A18018_12__c, Divert_no_reply_A18018_13__c, Divert_no_reply_A18018_14__c, Divert_no_reply_A18018_15__c, Divert_no_reply_A18018_16__c, Divert_no_reply_A18018_17__c, Divert_no_reply_A18018_18__c, Divert_no_reply_A18018_19__c, Divert_no_reply_A18018_20__c, Divert_no_reply_A18018_21__c, Divert_no_reply_A18018_22__c, Divert_no_reply_A18018_23__c, Divert_no_reply_A18018_24__c, Divert_no_reply_A18018_25__c, Divert_no_reply_A18018_26__c, Divert_no_reply_A18018_27__c, Divert_no_reply_A18018_28__c, Divert_no_reply_A18018_29__c, Divert_no_reply_A18018_30__c, Divert_on_E_F_A18017_1__c, Divert_on_E_F_A18017_3__c, Divert_on_E_F_A18017_2__c, Divert_on_E_F_A18017_4__c, Divert_on_E_F_A18017_5__c, Divert_on_E_F_A18017_6__c, Divert_on_E_F_A18017_7__c, Divert_on_E_F_A18017_8__c, Divert_on_E_F_A18017_9__c, Divert_on_E_F_A18017_10__c, Divert_on_E_F_A18017_11__c, Divert_on_E_F_A18017_12__c, Divert_on_E_F_A18017_13__c, Divert_on_E_F_A18017_14__c, Divert_on_E_F_A18017_15__c, Divert_on_E_F_A18017_16__c, Divert_on_E_F_A18017_17__c, Divert_on_E_F_A18017_18__c, Divert_on_E_F_A18017_19__c, Divert_on_E_F_A18017_20__c, Divert_on_E_F_A18017_21__c, Divert_on_E_F_A18017_22__c, Divert_on_E_F_A18017_23__c, Divert_on_E_F_A18017_24__c, Divert_on_E_F_A18017_25__c, Divert_on_E_F_A18017_26__c, Divert_on_E_F_A18017_27__c, Divert_on_E_F_A18017_28__c, Divert_on_E_F_A18017_29__c, Divert_on_E_F_A18017_30__c, Exist_No_to_convert_to_ISDN_3__c, Exist_No_to_convert_to_ISDN_2__c, Exist_No_to_convert_to_ISDN_4__c, Exist_No_to_convert_to_ISDN_5__c, Exist_No_to_convert_to_ISDN_6__c, Exist_No_to_convert_to_ISDN_7__c, Exist_No_to_convert_to_ISDN_8__c, Exist_No_to_convert_to_ISDN_9__c, Exist_No_to_convert_to_ISDN_10__c, Exist_No_to_convert_to_ISDN_11__c, Exist_No_to_convert_to_ISDN_12__c, Exist_No_to_convert_to_ISDN_13__c, Exist_No_to_convert_to_ISDN_14__c, Exist_No_to_convert_to_ISDN_15__c, Exist_No_to_convert_to_ISDN_16__c, Exist_No_to_convert_to_ISDN_17__c, Exist_No_to_convert_to_ISDN_18__c, Exist_No_to_convert_to_ISDN_19__c, Exist_No_to_convert_to_ISDN_20__c, Exist_No_to_convert_to_ISDN_21__c, Exist_No_to_convert_to_ISDN_22__c, Exist_No_to_convert_to_ISDN_23__c, Exist_No_to_convert_to_ISDN_24__c, Exist_No_to_convert_to_ISDN_25__c, Exist_No_to_convert_to_ISDN_26__c, Exist_No_to_convert_to_ISDN_27__c, Exist_No_to_convert_to_ISDN_28__c, Exist_No_to_convert_to_ISDN_29__c, Exist_No_to_convert_to_ISDN_30__c, Exist_No_to_convert_to_ISDN_1__c, Other_CSS_Order_Codes_1_1__c, Other_CSS_Order_Codes_1_2__c, Other_CSS_Order_Codes_1_3__c, Other_CSS_Order_Codes_3_1__c, Other_CSS_Order_Codes_3_2__c, Other_CSS_Order_Codes_3_3__c, Other_CSS_Order_Codes_2_1__c, Other_CSS_Order_Codes_2_2__c, Other_CSS_Order_Codes_2_3__c, Other_CSS_Order_Codes_4_1__c, Other_CSS_Order_Codes_4_2__c, Other_CSS_Order_Codes_4_3__c, Other_CSS_Order_Codes_5_1__c, Other_CSS_Order_Codes_5_2__c, Other_CSS_Order_Codes_5_3__c, Other_CSS_Order_Codes_6_1__c, Other_CSS_Order_Codes_6_2__c, Other_CSS_Order_Codes_6_3__c, Other_CSS_Order_Codes_7_1__c, Other_CSS_Order_Codes_7_2__c, Other_CSS_Order_Codes_7_3__c, Other_CSS_Order_Codes_8_1__c, Other_CSS_Order_Codes_8_2__c, Other_CSS_Order_Codes_8_3__c, Other_CSS_Order_Codes_9_1__c, Other_CSS_Order_Codes_9_2__c, Other_CSS_Order_Codes_9_3__c, Other_CSS_Order_Codes_10_1__c, Other_CSS_Order_Codes_10_2__c, Other_CSS_Order_Codes_10_3__c, Other_CSS_Order_Codes_11_1__c, Other_CSS_Order_Codes_11_2__c, Other_CSS_Order_Codes_11_3__c, Other_CSS_Order_Codes_12_1__c, Other_CSS_Order_Codes_12_2__c, Other_CSS_Order_Codes_12_3__c, Other_CSS_Order_Codes_13_1__c, Other_CSS_Order_Codes_13_2__c, Other_CSS_Order_Codes_13_3__c, Other_CSS_Order_Codes_14_1__c, Other_CSS_Order_Codes_14_2__c, Other_CSS_Order_Codes_14_3__c, Other_CSS_Order_Codes_15_1__c, Other_CSS_Order_Codes_15_2__c, Other_CSS_Order_Codes_15_3__c, Other_CSS_Order_Codes_16_1__c, Other_CSS_Order_Codes_16_2__c, Other_CSS_Order_Codes_16_3__c, Other_CSS_Order_Codes_17_1__c, Other_CSS_Order_Codes_17_2__c, Other_CSS_Order_Codes_17_3__c, Other_CSS_Order_Codes_18_1__c, Other_CSS_Order_Codes_18_2__c, Other_CSS_Order_Codes_18_3__c, Other_CSS_Order_Codes_19_1__c, Other_CSS_Order_Codes_19_2__c, Other_CSS_Order_Codes_19_3__c, Other_CSS_Order_Codes_20_1__c, Other_CSS_Order_Codes_20_2__c, Other_CSS_Order_Codes_20_3__c, Other_CSS_Order_Codes_21_1__c, Other_CSS_Order_Codes_21_2__c, Other_CSS_Order_Codes_21_3__c, Other_CSS_Order_Codes_22_1__c, Other_CSS_Order_Codes_22_2__c, Other_CSS_Order_Codes_22_3__c, Other_CSS_Order_Codes_23_1__c, Other_CSS_Order_Codes_23_2__c, Other_CSS_Order_Codes_23_3__c, Other_CSS_Order_Codes_24_1__c, Other_CSS_Order_Codes_24_2__c, Other_CSS_Order_Codes_24_3__c, Other_CSS_Order_Codes_25_2__c, Other_CSS_Order_Codes_25_1__c, Other_CSS_Order_Codes_25_3__c, Other_CSS_Order_Codes_26_1__c, Other_CSS_Order_Codes_26_2__c, Other_CSS_Order_Codes_26_3__c, Other_CSS_Order_Codes_27_1__c, Other_CSS_Order_Codes_27_2__c, Other_CSS_Order_Codes_27_3__c, Other_CSS_Order_Codes_28_1__c, Other_CSS_Order_Codes_28_2__c, Other_CSS_Order_Codes_28_3__c, Other_CSS_Order_Codes_29_1__c, Other_CSS_Order_Codes_29_2__c, Other_CSS_Order_Codes_29_3__c, Other_CSS_Order_Codes_30_1__c, Other_CSS_Order_Codes_30_2__c, Other_CSS_Order_Codes_30_3__c, Related_to_CRF__c, Stroke_Number_1__c, Stroke_Number_2__c, Stroke_Number_3__c, Stroke_Number_4__c, Stroke_Number_5__c, Stroke_Number_6__c, Stroke_Number_7__c, Stroke_Number_8__c, Stroke_Number_9__c, Stroke_Number_10__c, Stroke_Number_11__c, Stroke_Number_12__c, Stroke_Number_13__c, Stroke_Number_14__c, Stroke_Number_15__c, Stroke_Number_16__c, Stroke_Number_17__c, Stroke_Number_18__c, Stroke_Number_19__c, Stroke_Number_20__c, Stroke_Number_21__c, Stroke_Number_22__c, Stroke_Number_23__c, Stroke_Number_24__c, Stroke_Number_25__c, Stroke_Number_26__c, Stroke_Number_27__c, Stroke_Number_28__c, Stroke_Number_29__c, Stroke_Number_30__c, Telephone_Numbers_DDI_range_1__c, Telephone_Numbers_DDI_range_3__c, Telephone_Numbers_DDI_range_2__c, Telephone_Numbers_DDI_range_4__c, Telephone_Numbers_DDI_range_5__c, Telephone_Numbers_DDI_range_6__c, Telephone_Numbers_DDI_range_7__c, Telephone_Numbers_DDI_range_8__c, Telephone_Numbers_DDI_range_9__c, Telephone_Numbers_DDI_range_10__c, Telephone_Numbers_DDI_range_11__c, Telephone_Numbers_DDI_range_12__c, Telephone_Numbers_DDI_range_13__c, Telephone_Numbers_DDI_range_14__c, Telephone_Numbers_DDI_range_15__c, Telephone_Numbers_DDI_range_16__c, Telephone_Numbers_DDI_range_17__c, Telephone_Numbers_DDI_range_18__c, Telephone_Numbers_DDI_range_19__c, Telephone_Numbers_DDI_range_20__c, Telephone_Numbers_DDI_range_21__c, Telephone_Numbers_DDI_range_22__c, Telephone_Numbers_DDI_range_23__c, Telephone_Numbers_DDI_range_24__c, Telephone_Numbers_DDI_range_25__c, Telephone_Numbers_DDI_range_26__c, Telephone_Numbers_DDI_range_27__c, Telephone_Numbers_DDI_range_28__c, Telephone_Numbers_DDI_range_29__c, Telephone_Numbers_DDI_range_30__c, Working_Type_1__c, Working_Type_3__c, Working_Type_2__c, Working_Type_4__c, Working_Type_5__c, Working_Type_6__c, Working_Type_7__c, Working_Type_8__c, Working_Type_9__c, Working_Type_10__c, Working_Type_11__c, Working_Type_12__c, Working_Type_13__c, Working_Type_14__c, Working_Type_15__c, Working_Type_16__c, Working_Type_17__c, Working_Type_18__c, Working_Type_19__c, Working_Type_20__c, Working_Type_21__c, Working_Type_22__c, Working_Type_23__c, Working_Type_24__c, Working_Type_25__c, Working_Type_26__c, Working_Type_27__c, Working_Type_28__c, Working_Type_29__c, Working_Type_30__c FROM Channels__c Where Related_to_CRF__c = : crfId];
    if (isdnList.size() > 0) {
    	ISDN = 	isdnList[0];
        BT = ISDN.Is_this_a_new_customer_to_BT__c;
    }
    if (channelList.size() > 0) {
    	Channel = channelList[0];
    }

}

public PageReference OnLoad() {
    System.debug('CRFRTId-->' + crfId);
    CRFRTId = [Select RecordTypeId from CRF__c where Id = : crfId].RecordTypeId;
    System.debug('CRFRTId-->' + CRFRTId);
    RecordTypeName = [Select DeveloperName from RecordType where Id = : CRFRTId].DeveloperName;
    System.debug('CRFRTId-->' + RecordTypeName);
    if (RecordTypeName != 'ISDN30') {
        System.debug('CRFRTId-->33' + CRFRTId);
        PageReference RD = new PageReference('/' + crfId + '/e?nooverride=1&retURL=%2F' + crfId);
        RD.setRedirect(true);
        return RD;
    }
    return null;
}

Public PageReference Submit() {
    if (validatefields() != true) {
        return null;
    }
    try {

        update crf;

        ISDN.Is_this_a_new_customer_to_BT__c = BT;
        upsert ISDN;

        Channel.Related_to_CRF__c = crf.Id;
        upsert Channel;

    } Catch(Exception e) {
        ApexPages.addMessages(e);
        Return Null;
    }

	List<Address_Details__c> lstAddresses = [SELECT Id, Name, Address_Type__c, CRF_address_Id__c, Related_to_CRF__c FROM Address_Details__c WHERE Related_to_CRF__c =: crfId];
	if(lstAddresses.size()>0) {
	    pageReference crfPage = new pageReference('/' + crf.Id);
	    crfPage.setRedirect(true);
	    Return crfPage;
	}
	else {
		pageReference crfPage = new pageReference('/apex/CRF_NewAddress_Select?CF00N20000002oGtQ=' + crf.Name + '&CF00N20000002oGtQ_lkid=' + crf.Id + '&scontrolCaching=1&retURL=%2F' + crf.Id +'&sfdc.override=1' );
	    crfPage.setRedirect(true);
	    Return crfPage;
	}
}

Public PageReference Cancel1() {
    if (channelsId == null) {
        PageReference crfPage = new PageReference('/' + crf.Id);
        crfPage.setRedirect(true);
        return crfPage;
    } else {
        delete Channel;
        delete ISDN;
        delete crf;
        PageReference retpage = new PageReference('/' + crf.Opportunity__c);
        retpage.setRedirect(true);
        return retpage;
    }
}

ISDN_CRF__c ISDNcrf = new ISDN_CRF__c();

public ISDN_CRF__c getISDNcrf() {

    return ISDNcrf;
}

public List < selectOption > getServiceType() {
    List < selectOption > options = new List < selectOption > ();
    options.add(new selectOption('', '- None -'));
    options.add(new selectOption('ISDN30E', 'ISDN30E'));
    options.add(new selectOption('DASS 2', 'DASS 2'));
    return options;
}

public List < selectOption > getStandbyBattery() {
    List < selectOption > options = new List < selectOption > ();
    options.add(new selectOption('', '- None -'));
    options.add(new selectOption('Provide', 'Provide'));
    options.add(new selectOption('Replace', 'Replace'));
    return options;
}


}