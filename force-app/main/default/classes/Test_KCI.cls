@isTest
    private class Test_KCI{

    static testMethod void myUnitTest(){
    
    KCI__c kci = new KCI__c();
    kci.Name='VOL011-10000000000';
    kci.RecordTypeId=[Select Id from RecordType where DeveloperName=:'Validation_Call'].Id;
    kci.Correct_One_View_Queue__c='BTB';
    insert kci;
    
    KCI__c kci1 = new KCI__c();
    kci1.Name='VOL011-20000000000';
    kci1.RecordTypeId=[Select Id from RecordType where DeveloperName=:'Confirmation_Call'].Id;
    kci1.Correct_One_View_Queue__c='BTB';    
    insert kci1;
    
    
    KCIRecordTypeSelection myController = new KCIRecordTypeSelection(new ApexPages.StandardController(kci));
    myController.getItems();
    myController.getrecordTypes();
    myController.recordTypes='Validation Call';
    String recordTypes='Validation Call';
    myController.setrecordTypes(recordTypes);
    myController.Continue1();
    myController.Cancel1();
    
    KCIEditController_Main myController1 = new KCIEditController_Main(new ApexPages.StandardController(kci));
    myController1.OnLoad();
    
    ApexPages.currentPage().getParameters().put('Id',kci.Id);
    KCIEditController myController2 = new KCIEditController(new ApexPages.StandardController(kci));  
    myController2.getStatus();
    //myController2.getHelpTextURL();
    
    KCIEditController_Main myController3 = new KCIEditController_Main(new ApexPages.StandardController(kci1));
    myController3.OnLoad();
    
    KCIViewController myController4 = new KCIViewController(new ApexPages.StandardController(kci));
    myController4.EditPage();
    myController4.EditValidationDetails();
    myController4.PopUp();
    myController4.getAlert();

    }
    
    static testMethod void myUnitTest1(){
    
    KCI__c kci = new KCI__c();
    kci.Name='VOL011-30000000000';
    kci.RecordTypeId=[Select Id from RecordType where DeveloperName=:'Confirmation_Call'].Id;
    kci.Correct_One_View_Queue__c='BTB';
    insert kci;
    
    KCIRecordTypeSelection myController = new KCIRecordTypeSelection(new ApexPages.StandardController(kci));
    myController.getItems();
    myController.getrecordTypes();
    myController.recordTypes='Confirmation Call';
    String recordTypes='Confirmation Call';
    myController.setrecordTypes(recordTypes);
    myController.Continue1();
    myController.Cancel1();
    
    KCIEditController_Main myController1 = new KCIEditController_Main(new ApexPages.StandardController(kci));
    myController1.OnLoad();
    
    KCIViewController myController4 = new KCIViewController(new ApexPages.StandardController(kci));
    myController4.getRecordTypeName();
    myController4.EditPage();
    myController4.EditValidationDetails();
    myController4.getDisplayEditValidationButton();
    
    KCI__c kci1 = new KCI__c();
    kci1.Name='VOL011-20000000000';
    kci1.Correct_One_View_Queue__c='BTB';
    insert kci1;
    
    
    
    }


}