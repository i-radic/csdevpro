/*  ======================================================
    History                                                            
    -------                                                            
VERSION  AUTHOR             DATE              DETAIL                                 FEATURES
1.00     Dan Measures       10/06/2011        initial development for assigning owner of BTLB specific Portfolio Area Snapshot records
     	 John McGovern		13/02/2012		  Touched to change last updated away from inactive user
*/
global class BatchPortfolioAreaSnapshotOwner implements Database.Batchable<SObject>{
	private String query;

	global batchPortfolioAreaSnapshotOwner(String q){
        this.query = q;
    }
    
   	global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
   	global void execute(Database.BatchableContext BC, List<sObject> scope){
   		List<Forecasting_Portfolio_Area_Snapshot__c> fpa_to_update = new List<Forecasting_Portfolio_Area_Snapshot__c>();
   		Set<String> btlb_names = new Set<String>();
   		Map<String, String> btlb_account_owners = new Map<String, String>();
   		
		for(Sobject c : scope){
			Forecasting_Portfolio_Area_Snapshot__c fpas = (Forecasting_Portfolio_Area_Snapshot__c)c;
			btlb_names.add(fpas.BTLB_Name__c);
System.debug('---1---> ' + fpas.BTLB_Name__c);
        }
        
        for(BTLB_Master__c bmo : [select id, BTLB_Name_ExtLink__c, Account_Owner__c from BTLB_Master__c where BTLB_Name_ExtLink__c in :btlb_names]){
System.debug('---2---> ');
        	if(bmo.Account_Owner__c != null){
        		btlb_account_owners.put(bmo.BTLB_Name_ExtLink__c.tolowercase(), bmo.Account_Owner__c);
System.debug('---3---> ' + bmo.BTLB_Name_ExtLink__c.tolowercase() + '_' + bmo.Account_Owner__c);
        	}
        }
        
        for(Sobject c : scope){
			Forecasting_Portfolio_Area_Snapshot__c fpas = (Forecasting_Portfolio_Area_Snapshot__c)c;
System.debug('---4---> ');
			if(fpas.BTLB_Name__c != null && btlb_account_owners.get(fpas.BTLB_Name__c.tolowercase()) != null){
System.debug('---5---> ' + fpas.BTLB_Name__c.tolowercase() + '_' +  btlb_account_owners.get(fpas.BTLB_Name__c.tolowercase()));
				fpas.ownerid = btlb_account_owners.get(fpas.BTLB_Name__c.tolowercase());
				fpa_to_update.add(fpas);
			}
        }
        if(!fpa_to_update.isEmpty())
        	update fpa_to_update;
    }
    
	global void finish(Database.BatchableContext BC){
    }
}