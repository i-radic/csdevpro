/**
 * Created by PuneetGosain on 14/05/2020.
 */

global class CS_LookupOpportunitySolutionConsole implements cssmgnt.RemoteActionDataProvider {

    global Map<String, Object> getData(Map<String, Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>();
        String basketId = String.valueOf(inputMap.get('basketId'));
        System.debug('****'+inputMap);
        returnMap.put('opportunityId',[SELECT cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId].cscfga__Opportunity__c);

        return returnMap;
    }

}