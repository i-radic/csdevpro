public with sharing class UpdateAccountOVControllerAction{
    public Account sfaccount = null;
    public Contact sfcontact = null;
    public String primaryContactId = null;
    public Account account {get; set;} 
    
    public PageReference UpdateAccountFromCRM(){    
        PageReference pageRef = null;
          
        String cugId = ApexPages.currentPage().getParameters().get('CUG');
        
        if (cugId == null || cugId == '' ) {
            cugId = ApexPages.currentPage().getParameters().get('CUGID');
        }      
        if(Test_Factory.GetProperty('IsTest') == 'yes'){
            cugId = 'test';
        }
        else {
            primaryContactId = ApexPages.currentPage().getParameters().get('primaryContactId');
        }
                
                Account sfaccount = null;
                List<Account> sfaccounts = 
                                    [SELECT 
                                        Id,
                                        Name,
                                        CUG__c,
                                        SAC_Code__c,
                                        OwnerId 
                                    FROM 
                                        Account 
                                    WHERE CUG__c = :cugId];
                
                if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    Account account = new Account();
                    account.CUG__c = 'TEST';
                    account.SAC_Code__c = 'TEST';
                    account.Name = 'TEST';
                    sfaccounts.add(account);                
                }    
                
                sfaccount = sfaccounts[0];                                       
        
        if (primaryContactId == null || primaryContactId == '' ) {
            pageRef = new PageReference('/'+ sfaccount.Id);
            pageRef.setRedirect(true);
            return pageRef;
        }
        
                //SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway(); singhd62
                //SalesforceServicesCRM.Contact crmContact = new SalesforceServicesCRM.Contact(); singhd62
                SalesforceCRMService.CRMContact crmContact = new SalesforceCRMService.CRMContact();
                                  
                if(Test_Factory.GetProperty('IsTest') == 'yes') {
                    crmContact.Title = 'MR';
                    crmContact.FirstName= 'TEST';
                    crmContact.LastName= 'TEST';
                    crmContact.AddressId= 'TEST';
                    crmContact.AddressPostTown= 'TEST';
                    crmContact.AddressPostCode= 'TEST';
                    crmContact.AddressSubBuilding= 'TEST';
                    crmContact.AddressNumber= 'TEST';
                    crmContact.AddressStreet= 'TEST';
                    crmContact.AddressLocality= 'TEST';
                    crmContact.AddressCounty= 'TEST';
                    crmContact.AddressCountry= 'TEST';            
                }
                else {
                    //crmContact = service.GetContact('', '', cugId, primaryContactId); singhd62
                    crmContact = SalesforceCRMService.GetContact(cugId, primaryContactId); // singhd62
                    system.debug('Response XML Data : ' + crmContact);
                    if (crmContact == null){
                        pageRef = new PageReference('/'+ sfaccount.Id);
                        pageRef.setRedirect(true);
                        return pageRef;
                    }
                }
                
                List<Contact> sfcontacts = 
                                    [SELECT 
                                        Id,
                                        AccountId,
                                        IntegrationId__c,
                                        CUG_ContactId__c,
                                        Salutation,
                                        Firstname,
                                        LastName,
                                        Address_Id__c,
                                        Contact_Post_Town__c,
                                        Contact_Sub_Building__c,
                                        Contact_Address_Number__c,
                                        Contact_Address_Street__c,
                                        Contact_Locality__c,
                                        Address_County__c,
                                        Address_Country__c                                   
                                    FROM 
                                        Contact 
                                    WHERE CUG_ContactId__c = :cugId+primaryContactId];                    
                
                
                 
                 if(Test_Factory.GetProperty('IsTest') == 'yes'){
                     Contact contact = new Contact();
                     contact.IntegrationId__c = 'TEST';
                     contact.Salutation = 'TEST';
                     contact.Firstname = 'TEST';
                     contact.LastName = 'TEST';
                     contact.Address_Id__c = 'TEST';
                     contact.Contact_Post_Town__c = 'TEST';
                     contact.Contact_Sub_Building__c = 'TEST';
                     contact.Contact_Address_Number__c = 'TEST';
                     contact.Contact_Address_Street__c = 'TEST';
                     contact.Contact_Locality__c = 'TEST';
                     contact.Address_County__c = 'TEST';
                     contact.Address_Country__c = 'TEST';   
                     sfcontacts.add(contact);   
                }
                
                if (!sfcontacts.isEmpty()) {       
                    sfcontact = sfcontacts[0];
                    sfcontact.IntegrationId__c = primaryContactId;
                    sfcontact.CUG_ContactId__c = cugId+crmContact.ExternalId;
                    sfcontact.Salutation = crmContact.Title;
                    sfcontact.Firstname = crmContact.FirstName;
                    sfcontact.LastName = crmContact.LastName;
                    sfcontact.Address_Id__c = crmContact.AddressId;  
                    sfcontact.Contact_Post_Town__c = crmContact.AddressPostTown;
                    sfcontact.Contact_Post_Code__c = crmContact.AddressPostCode;
                    sfcontact.Contact_Sub_Building__c = crmContact.AddressSubBuilding;
                    sfcontact.Contact_Address_Number__c = crmContact.AddressNumber;
                    sfcontact.Contact_Address_Street__c = crmContact.AddressStreet;
                    sfcontact.Contact_Locality__c = crmContact.AddressLocality;
                    sfcontact.Address_County__c = crmContact.AddressCounty;
                    sfcontact.Address_Country__c = crmContact.AddressCountry;                  
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                    }      
                    else {
                        sfcontact.AccountId = sfaccount.Id;
                        update sfcontact;
                        pageRef = new PageReference('/'+ sfaccount.Id);
                        pageRef.setRedirect(true);                                
                        return pageRef;
                    }                              
                  }
                  
                  Contact contact = new Contact();   
                  contact.Created_by_SFOppty__c = true;
                  contact.IntegrationId__c = primaryContactId;
                  contact.CUG_ContactId__c = cugId+crmContact.ExternalId;
                  contact.Salutation = crmContact.Title;
                  contact.Firstname = crmContact.FirstName;
                  contact.LastName = crmContact.LastName;
                  contact.Address_Id__c = crmContact.AddressId;  
                  contact.Contact_Post_Town__c = crmContact.AddressPostTown;
                  contact.Contact_Post_Code__c = crmContact.AddressPostCode;
                  contact.Contact_Sub_Building__c = crmContact.AddressSubBuilding;
                  contact.Contact_Address_Number__c = crmContact.AddressNumber;
                  contact.Contact_Address_Street__c = crmContact.AddressStreet;
                  contact.Contact_Locality__c = crmContact.AddressLocality;
                  contact.Address_County__c = crmContact.AddressCounty;
                  contact.Address_Country__c = crmContact.AddressCountry;                              
                  
                  if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;
                  }  
                  contact.AccountId = sfaccount.Id;
                  system.debug('Complete Contact Data : ' + contact);       
                  insert contact;     
                  pageRef = new PageReference('/'+ sfaccount.Id);
                  pageRef.setRedirect(true);    
                  return pageRef;                                                                                          
    }
    public UpdateAccountOVControllerAction(ApexPages.StandardController stdController) {

         account = (Account)stdController.getRecord();
    }    
    
    public UpdateAccountOVControllerAction(Account account_edit) {
         account = account_edit;
    }
}