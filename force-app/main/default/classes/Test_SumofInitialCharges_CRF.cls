@isTest

    private class Test_SumofInitialCharges_CRF{
    
    static testMethod void myUnitTest(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    Opportunity o = new Opportunity();
    o.Name='TEST';
    o.stageName = 'FOS Stage';
    o.CloseDate = system.today();
    insert o;
    
    CRF__c crf = new CRF__c();
     
    crf.Opportunity__c = o.Id;
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Movers'].Id;    
    crf.Contact__c = c.Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;
    
    crf.Total_initial_charges__c=null;
    update crf;
    
    CRF__c crf1 = new CRF__c();
    crf1.Opportunity__c = o.Id;
    crf1.RecordTypeId = [Select Id from RecordType where DeveloperName='AX_Form'].Id;    
    crf1.Contact__c = c.Id;
    crf1.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf1;
    
    
    Provide_Line_Details__c LM = new Provide_Line_Details__c();
    LM.Related_To_CRF__c = crf.Id;
    LM.Name = '1';
    LM.Initial_charges__c=100;
    insert LM;
    
    /*
    CRF__c crf1 = new CRF__c();
    
        crf1.Opportunity__c = o.Id;
        crf1.RecordTypeId = [Select Id from RecordType where DeveloperName='AX_Form'].Id;
        crf1.Contact__c = c.Id;    crf1.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD'; 
        
           insert crf1; 
                     
            Provide_Line_Details__c LM = new Provide_Line_Details__c(); 
               LM.Related_To_CRF__c = crf.Id;    LM.Name = '1';    
               LM.Initial_charges__c=100;    insert LM; 
               */
    
    
    LM.Initial_charges__c=200;
    update LM;
    
    Provide_Line_Details__c LM1 = new Provide_Line_Details__c();
    LM1.Related_To_CRF__c = crf.Id;
    LM1.Name = '1';
    LM1.Initial_charges__c=100;
    insert LM1;
  
    Provide_Line_Details__c LM11 = new Provide_Line_Details__c();
    LM11.Related_To_CRF__c = crf.Id;
    LM11.Name = '1';
    LM11.Initial_charges__c=100;
    insert LM11;
    delete LM11;
    
    Provide_Line_Details__c LM2 = new Provide_Line_Details__c();
    LM2.Related_To_CRF__c = crf.Id;
    LM2.Initial_charges__c=null;
    LM2.Name = '1';
    insert LM2;
    
    /*
    Provide_Line_Details__c LM1 = new Provide_Line_Details__c();
    LM1.Related_To_CRF__c = crf.Id;
    LM1.Name = '1';
    LM1.Initial_charges__c=100;
    insert LM1;
  
    Provide_Line_Details__c LM11 = new Provide_Line_Details__c();
    LM11.Related_To_CRF__c = crf.Id;
    LM11.Name = '1';
    LM11.Initial_charges__c=100;
    insert LM11;
    delete LM11;
    
    Provide_Line_Details__c LM2 = new Provide_Line_Details__c();
    LM2.Related_To_CRF__c = crf.Id;
    LM2.Initial_charges__c=null;
    LM2.Name = '1';
    insert LM2;

    */
  
  delete LM2;
    }
}