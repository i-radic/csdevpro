/*
 * Author:609701681/607913925
 * Name: DharmaTeja/Harish
 * Created as Part of CASE00400313
 * TestClass: Test_BTLB_Master_Helper
*/
public class BTLB_Master_Helper {
    public static boolean flag1 = true;
    public static void HandleUpdateUser(Id btid,String dep, String email_sig) {
        List<User> Userlist = [select Id, Department, Company_Name__c, Email_Signature__c,Company_Registration_No__c,Company_Registered_Address__c from user where department =:dep and isactive=:true];
        List<User> UpdateUserList = new List<User>();
        BTLB_Master__c BTLBMaster = [select id,Company_Name__c,Company_Registration_No__c,Company_Registered_Address__c from BTLB_Master__c where id =: btid];
        for(User u: Userlist) {
            u.Email_Signature__c = email_sig;
            u.Company_Name__c = BTLBMaster.Company_Name__c;
            u.Company_Registration_No__c = BTLBMaster.Company_Registration_No__c;
            u.Company_Registered_Address__c = BTLBMaster.Company_Registered_Address__c;
            UpdateUserList.add(u);
        }
        if(UpdateUserList.size()>0) {
            update UpdateUserList;
        }
    }
    public static integer HandleInsertBTLBRec(String Dep_name) {
        List<BTLB_Master__c> exist_list = [Select Name from BTLB_Master__c where Name=:Dep_name limit 1];
        return exist_list.size();
    }
}