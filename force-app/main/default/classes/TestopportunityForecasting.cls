@isTest
private class TestopportunityForecasting {

   static testmethod void unittest1(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }   
    User u1 = [SELECT Id,Name FROM User WHERE Department LIKE 'BTLB%' AND IsActive = true AND Profile.Name LIKE 'BTLB%' LIMIT 1];
    
    opportunityForecasting oppfor = new opportunityForecasting();
    System.RunAs(u1){
    date dt1 = System.today();
    Account ALL = Test_Factory.CreateAccount();
    ALL.Name='Test LL';
    ALL.SAC_Code__c='SACLL11';
    ALL.Base_Team_Assign_Date__c = System.Today()+7;
    ALL.Sector__c = 'BT Local Business';
    ALL.BTLB_Common_Name__c = 'abcds';
    Insert ALL;
    
    Opportunity o = Test_Factory.CreateOpportunity(ALL.id);
    o.Name='TEST';
    o.RecordTypeId = '01220000000PjDu';
    o.stageName = 'Created';
    o.CloseDate = dt1;
    o.Expected_Close_Date__c = o.CloseDate;
    o.zCurrent_Week_Forecast_Vol__c = 1;
    o.Current_Week_Forecast_Amount__c = 200;
    o.Win_Rating_Definition_pl__c = 'BT has advantage on Price / Product / Strategy';
    insert o;
    
    Opportunity o3 = Test_Factory.CreateOpportunity(ALL.id);
    o3.Name='TEST3';
    o3.RecordTypeId = '01220000000PjDu';
    o3.stageName = 'Created';
    o3.CloseDate = dt1;
    o3.Expected_Close_Date__c = o.CloseDate;
    o3.zCurrent_Week_Forecast_Vol__c = 1;
    o3.Current_Week_Forecast_Amount__c = 200;
    //o3.Count_in_100__c = null;
    o3.Win_Rating_Definition_pl__c = 'BT has advantage on Price / Product / Strategy';
    insert o3;
    
    Opportunity o1 = Test_Factory.CreateOpportunity(ALL.id);
    o1.Name='TEST1';
    o1.RecordTypeId = '01220000000PjDu';
    o1.stageName = 'Created';
    o1.CloseDate = dt1;
    o1.Expected_Close_Date__c = o.CloseDate;
    o1.zCurrent_Week_Forecast_Vol__c = 1;
    o1.Win_Rating_Definition_pl__c = 'BT has advantage on Price / Product / Strategy';
    insert o1;
    
    
    Opportunity o2 = Test_Factory.CreateOpportunity(ALL.id);
    o2.Name='TEST2';
    o2.RecordTypeId = '01220000000PjDu';
    o2.stageName = 'Created';
    o2.CloseDate = dt1;
    o2.Expected_Close_Date__c = o.CloseDate;
    o2.zCurrent_Week_Forecast_Vol__c = 1;
    //o2.Count_in_100__c = null;
    o2.Win_Rating_Definition_pl__c = 'BT has advantage on Price / Product / Strategy';
    insert o2;
    
    System.debug('------->'+o1.Closedate);
    System.debug('------->'+o2.Closedate);
    System.debug('------->'+o3.Closedate);
    System.debug('------->'+o.Closedate);
    System.debug('------->'+o.Expected_Close_Date__c);
    System.debug('------->'+o1.Expected_Close_Date__c);
    System.debug('------->'+o2.Expected_Close_Date__c);
    System.debug('------->'+o3.Expected_Close_Date__c);
    
    
    
    }
    SchedulableContext sc;
    oppfor.execute(sc);
   }
}