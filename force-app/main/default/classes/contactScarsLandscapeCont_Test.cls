@isTest

Private class contactScarsLandscapeCont_Test{
static testMethod void runPositiveTestCases(){
   
        User thisUser = [select id from User where id=:userinfo.getUserid()];
	     System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
         }
    Account acc1 = Test_factory.createAccount();
         acc1.cug__c = 'CUGtest1Cug';
         acc1.OwnerId = thisUser.Id;
         acc1.Sub_Sector__c = 'Stratford Gold Customers';
         acc1.Sector__c = 'NI Enterprise';
         acc1.Account_Owner_Department__c = 'BT Ireland';
      // Database.SaveResult[] accResult = Database.insert(new Account[] {acc});
    insert acc1;
    
    Contact contact1 = Test_Factory.CreateContact();
        contact1.Cug__c = 'test1Cug';
        contact1.Phone = '000111222333';
        contact1.Email = 'test.Alan@test.com';
        contact1.Contact_Post_Code__c = 'WR5 3RL';
        contact1.Accountid = acc1.id;
        insert contact1;
  
	SCARS_BTLB__c Scars1 = Test_Factory.CreateSCARS_BTLB (acc1.Id); 
        insert Scars1;
    Landscape_BTLB__c Ls1 = Test_Factory.CreateLandscapeBTLB2(acc1.Id);
    insert Ls1;
    
    PageReference pageRef = Page.ContactScarsLandscapeDetails;
    Test.setCurrentPageReference(pageRef);
    ApexPages.currentPage().getParameters().put('aId',acc1.Id);
    ApexPages.currentPage().getParameters().put('Id',contact1.Id);
    contactScarsLandscapeController controller= new contactScarsLandscapeController();
    controller.getCampaignList();
    
    PageReference pageRef2 = Page.ContactScarsLandscapeDetails;
    Test.setCurrentPageReference(pageRef2);
    ApexPages.currentPage().getParameters().put('aId',null);
    ApexPages.currentPage().getParameters().put('Id',null);
    contactScarsLandscapeController controller2= new contactScarsLandscapeController();
    controller2.getCampaignList();
}
}