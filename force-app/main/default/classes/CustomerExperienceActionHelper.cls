/**************************************************************************************************************************************************
    Class Name : CustomerExperienceActionHelper
    Test Class Name : CustomerExperienceActionHelper_Test
    Description : Code to Insert and Update Customer Experience Action, And to Calculate RollUp Summary, to Update in Survey Response Object Record
    Version : V0.2
    Created By Author Name : Bikash Kumar Gupta
    Date : 08/09/2017

	Modified By  :BALAJI MS
	Modified Date : 09/02/2018
 *************************************************************************************************************************************************/

public class CustomerExperienceActionHelper {  
    
    public static void BeforeInsertMethodTrigger(List<Customer_Experience_Actions__c> NewList){
        
        if(NewList.size()>0)
            commonMethodforEmailUpdate(NewList);
        
    }
    
    public static void BeforeUpdateMethodTrigger(Map<Id, Customer_Experience_Actions__c> NewMap, Map<Id, Customer_Experience_Actions__c> OldMap){
        
        List<Customer_Experience_Actions__c> UpdateEmailField = new List<Customer_Experience_Actions__c>();
        Map<Id, Customer_Experience_Actions__c> ClientPlanActionMap = new Map<Id, Customer_Experience_Actions__c>();            
        
        for(Customer_Experience_Actions__c ClientPlanRecord : NewMap.values())
            UpdateEmailField.add(ClientPlanRecord);
        
        if(UpdateEmailField.size() > 0)
            commonMethodforEmailUpdate(UpdateEmailField);
        
    }
    
    public static void AfterInsertMethodTrigger(Map<Id, Customer_Experience_Actions__c> NewMap, List<Customer_Experience_Actions__c> NewList){
        
        if(NewList.size()>0)
            RollUpSummaryUpdate(NewList);
        
    }
    
    public static void AfterUpdateMethodTrigger(Map<Id, Customer_Experience_Actions__c> NewMap, Map<Id, Customer_Experience_Actions__c> OldMap, List<Customer_Experience_Actions__c> NewList, List<Customer_Experience_Actions__c> OldList){
        
        List<Customer_Experience_Actions__c> RecordsToUpdate = new List<Customer_Experience_Actions__c>();
        
        for(Customer_Experience_Actions__c ActionRecord : NewMap.values()){
            
            if(NewMap.get(ActionRecord.Id).Open_Closed__c != OldMap.get(ActionRecord.Id).Open_Closed__c)
                RecordsToUpdate.add(ActionRecord);
        }
        
        if(RecordsToUpdate.size() > 0)
            RollUpSummaryUpdate(OldList);
    }
    
    public static void AfterDeleteMethodTrigger(List<Customer_Experience_Actions__c> OldList){
        
        if(OldList.size() > 0)
            RollUpSummaryUpdate(OldList);
        
    }
    
    public static void AfterUnDeleteMethodTrigger(List<Customer_Experience_Actions__c> OldList){
        
        if(OldList.size() > 0)
            RollUpSummaryUpdate(OldList);
        
    }
    
    //Method to Update Total Number of Record Count and count total number of Records with Open/Closed = 'Open' and Update on Parent (Survey Response) Record
    
    public static void RollUpSummaryUpdate(List<Customer_Experience_Actions__c> CustomerExperienceActionList){
        
        Set<Id> CSATContactFeedBackId = new Set<Id>();
        List<CSAT_Contact_Feedback__c> ContactFeedbackRecordUpdate = new List<CSAT_Contact_Feedback__c>();
        
        //Iterating Parent Record Id from Action Record
        
        for(Customer_Experience_Actions__c ActionRecord : CustomerExperienceActionList){
            if(ActionRecord.CSAT_Contact_Feedback_LINK__c != null)
                CSATContactFeedBackId.add(ActionRecord.CSAT_Contact_Feedback_LINK__c);
        }
        
        Map<Id,Integer> MapToUpdateTotal = new Map<Id,Integer>();
        Map<Id,Integer> MapToUpdateOpen = new Map<Id,Integer>();
        CSAT_Contact_Feedback__c CSATContactRecord; 
        
        
        if(CSATContactFeedBackId.size() > 0){
            
            /********************************** Logic For Total Number of Record Count Begins ************************************/
            for(AggregateResult ag: [select Count(id) ActionSize, CSAT_Contact_Feedback_LINK__c CSAT From Customer_Experience_Actions__c where CSAT_Contact_Feedback_LINK__c IN:CSATContactFeedBackId Group by CSAT_Contact_Feedback_LINK__c]){
                Id CSAT = string.valueOf(ag.get('CSAT'));
                Integer total = Integer.valueOf(ag.get('ActionSize'));
                MapToUpdateTotal.put(CSAT, total);
            }
            
            /********************************** Logic For Total Number of Record Count Ends ************************************/ 
            
            /********************************** Logic For Count of Records with Open/Closed = 'Open' Begins ************************************/
            for(AggregateResult ag: [select Count(Id) ActionSize, CSAT_Contact_Feedback_LINK__c CSAT From Customer_Experience_Actions__c where CSAT_Contact_Feedback_LINK__c IN:CSATContactFeedBackId and Open_Closed__c = 'Open' Group by CSAT_Contact_Feedback_LINK__c]){
                Id CSAT = string.valueOf(ag.get('CSAT'));
                Integer total = Integer.valueOf(ag.get('ActionSize'));
                MapToUpdateOpen.put(CSAT, total);
            }
            
            /********************************** Logic For Count of Records with Open/Closed = 'Open' Ends ************************************/
            
            /********************************** Logic For Updating Zero if No Records Begins ************************************/
            
            for(Id RecId : CSATContactFeedBackId){
                if(!MapToUpdateOpen.containsKey(RecId)){
                    MapToUpdateOpen.put(RecId, 0);
                }
            }                
            
            /********************************** Logic For Updating Zero if No Records Ends ************************************/
        }
        
        //Assigning values to the Parent (Survey Response) Record
        
        if(MapToUpdateTotal.size()>0){
            for(Id csatId : MapToUpdateTotal.keyset()){
                CSATContactRecord = new CSAT_Contact_Feedback__c();
                CSATContactRecord.Id = csatId;
                CSATContactRecord.Actions_Open__c = MapToUpdateOpen.get(csatId); 
                CSATContactRecord.Linked_Actions__c = MapToUpdateTotal.get(csatId);
                ContactFeedbackRecordUpdate.add(CSATContactRecord);
            }
            
        }
        
        //Updating the Parent (Survey Response) Record
        
        try{
            Update ContactFeedbackRecordUpdate;
        }Catch (Exception Ex){
            System.debug('Exception Occured ' +Ex);
        }
        
    }
    
    //Method to Update Email Fields, from Customer Experience Client Plan Object, To send Email from Workflow Process
    
    public static void commonMethodforEmailUpdate(List<Customer_Experience_Actions__c> CustomerExperienceActionList){
        
        Set<Id> ClientPlanId = new Set<Id>();
        Set<Id> UserId = new Set<Id>();
        Map<Id, Service_Improvement_Plan__c> ServicePlanMap = new Map<Id,Service_Improvement_Plan__c>();
        Map<Id, User> UserMap = new Map<Id,User>();
        
        for(Customer_Experience_Actions__c CustomerExperienceId : CustomerExperienceActionList){
            
            //Check for BT Owner Field and Iteration
            
            if(CustomerExperienceId.BT_Owner_Lookup__c != Null)
            	UserId.add(CustomerExperienceId.BT_Owner_Lookup__c);
            
            //Check for Parent (Customer Experience Client Plan) record and Iteration
            
            if(CustomerExperienceId.Customer_Experience_Client_Plan__c != Null)
            	ClientPlanId.add(CustomerExperienceId.Customer_Experience_Client_Plan__c);
            
        }
        
        
        If(UserId.size() > 0){
            
            for(User UserRecord : [Select Id, Manager.Email, Manager.Manager.Email from User where Id IN : UserId])
                UserMap.put(UserRecord.Id, UserRecord);
            
        }
        
        If(ClientPlanId.size() > 0){
            
            for(Service_Improvement_Plan__c PlanRecord : [Select Id, Account_Owner_Email__c, SRM_Manager_Email__c, Business_Manager_Email__c, Sales_Manager_Email__c, General_Manager_Email__c, DBAM_Email__c from Service_Improvement_Plan__c where Id IN : ClientPlanId])
                ServicePlanMap.put(PlanRecord.Id, PlanRecord);
        }
            
        if(ServicePlanMap.size()>0){
            
            /*********************************** Logic To Assign Email Id Begins ***********************************************/
            
            for(Customer_Experience_Actions__c customerExperienceAction : CustomerExperienceActionList){
                
                if(ServicePlanMap.containsKey(customerExperienceAction.Customer_Experience_Client_Plan__c)){
                    
                    If(ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).Account_Owner_Email__c != Null)
                        customerExperienceAction.Account_Owner_Email__c = ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).Account_Owner_Email__c;
                    else
                        customerExperienceAction.Account_Owner_Email__c = '';
                    
                    If(ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).SRM_Manager_Email__c != Null)
                        customerExperienceAction.SRM_Manager_Email__c = ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).SRM_Manager_Email__c;
                    else
                        customerExperienceAction.SRM_Manager_Email__c = '';
                    
                    If(ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).Business_Manager_Email__c != Null)
                        customerExperienceAction.Business_Manager_Email__c = ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).Business_Manager_Email__c;
                    else
                        customerExperienceAction.Business_Manager_Email__c = '';
                    
                    If(ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).Sales_Manager_Email__c != Null)
                        customerExperienceAction.Sales_Manager_Email__c = ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).Sales_Manager_Email__c;
                    else
                        customerExperienceAction.Sales_Manager_Email__c = '';
                    
                    If(ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).General_Manager_Email__c != Null)
                        customerExperienceAction.General_Manager_Email__c = ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).General_Manager_Email__c;
                    else
                        customerExperienceAction.General_Manager_Email__c = '';
                    
                    If(ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).DBAM_Email__c != Null)
                        customerExperienceAction.DBAM_Email__c = ServicePlanMap.get(customerExperienceAction.Customer_Experience_Client_Plan__c).DBAM_Email__c;
                    else
                        customerExperienceAction.DBAM_Email__c = '';
                    
                    if(UserMap.containsKey(customerExperienceAction.BT_Owner_Lookup__c)){
                        
                        If(UserMap.get(customerExperienceAction.BT_Owner_Lookup__c).Manager.Email != Null)
                            customerExperienceAction.BT_Owner_Manager_Email__c = UserMap.get(customerExperienceAction.BT_Owner_Lookup__c).Manager.Email;
                        else
                            customerExperienceAction.BT_Owner_Manager_Email__c = '';
                        
                        If(UserMap.get(customerExperienceAction.BT_Owner_Lookup__c).Manager.Manager.Email != Null)
                            customerExperienceAction.BT_Owner_Manager_s_Manager_Email__c = UserMap.get(customerExperienceAction.BT_Owner_Lookup__c).Manager.Manager.Email;
                        else
                            customerExperienceAction.BT_Owner_Manager_s_Manager_Email__c = '';
                        
                    }
                }
            }
            
            /*********************************** Logic To Assign Email Id Ends ***********************************************/
            
        }
    }
}