public class ADPLandscapeCtrl { 
   
public ADPLandscapeCtrl(ApexPages.StandardController controller) {   
 }
ID APD_Id = ApexPages.currentPage().getParameters().get('id');
List<ADP__c> AccountID1 = [ select customer__r.id from ADP__c where id = :APD_Id];
  
public List<ADP_Landscape__c> getLandscape() {        
            
return [select id, Record_Type__c, Supplier__c, Annual_Rental_Cost__c, Contract_Expiry_Date__c, Annual_Maintenance_Cost__c, Maintenance_Expiry_Date__c,Service_Relevant__c ,Annual_Spend__c ,Expiry_Date__c from ADP_Landscape__c
where ADP__c = :APD_Id
order by Record_Type__c];
    }
 }