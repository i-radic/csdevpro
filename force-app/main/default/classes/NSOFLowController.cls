public with sharing class NSOFLowController {

  public String OppId {set; get;}
    public String OppName{set; get;}
    public String opptyAccntName{get; set;}
    public String proId {get; set;}
    public String accid{get; set;}
    public String accName{get; set;}
    public String opptyAccntId{get; set;}
    public String conId {get; set;}
    public String conName {get; set;}
    public String conPhone {get; set;}
    public String conEmail {get; set;}
    public String leCode {get; set;}
    //public String Trading {get; set;}
    public String flowcrfId {get; set;}
   //public String isdnId {get; set;}
   // public List<Id> rere {get;set;}
   //public Flow.Interview.CRF_Main_Flow FlowCRF{ get; set; }
    public Flow.Interview.NSO_Main_Screen NSO{ get; set; }
    //List < Lines_to_Move__c > ltm;
    public String vfBBRecType { get; set; }
    //public String dcid { get; set; }
    public List<opportunitycontactrole> ocr {get; set;}
    public Boolean FlowFlag{get;set;}
    
    public NSOFLowController (ApexPages.StandardController controller) {
    
        ocr = new List<opportunitycontactrole>();
        OppId=Apexpages.currentPage().getParameters().get('00N200000098SzU');
        OppName=Apexpages.currentPage().getParameters().get('OppyName');
        //proId=Apexpages.currentPage().getParameters().get('profid');
        accName= Apexpages.currentPage().getParameters().get('AccountName');
        opptyAccntId=Apexpages.currentPage().getParameters().get('AccountId');
        Opportunity opp=[Select Account.Name,Customer_Contact__c,Customer_Contact__r.Name,Customer_Contact__r.Phone,
                        Customer_Contact__r.Email,Account.LE_Code__c,Account.Trading_As__c from Opportunity where Id=:OppId];
        ocr =[Select ContactId , contact.name ,contact.Phone,contact.Email FROM opportunitycontactrole where
              OpportunityId=:OppId  and Isprimary=true];//opp.Customer_Contact__c;
        FlowFlag = True;      
        if(ocr.size()>0 ){
        conId =ocr[0].ContactId ;
        conPhone =ocr[0].contact.Phone;
        conEmail =ocr[0].contact.Email;
        conName=ocr[0].contact.Name;
        opptyAccntName=opp.Account.Name;
        //leCode=opp.Account.LE_Code__c;
        //Trading=opp.Account.Trading_As__c;
        system.debug('aaaaaaa'+OppId+''+OppName+'  '+opptyAccntName+'  '+accId +'   '+conId +'   '+leCode+'    '+conPhone +'    '+conEmail+'    '+ conName);
        vfBBRecType =BTUtilities.getRecordTypeIdByName('Flow_CRF__c', 'Broadband'); //'01220000000AtDp';
        system.debug('BBRec'+vfBBRecType );
        }else{
            FlowFlag = False;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add a Contact Role.'));    
             
        }
          
    }
    
     /* public String getCRFID() {
        if (NSO== null)
            return OppId;
        else return NSO.CRFID;
      }*/
      
    
    public PageReference getRedirect() {
       
        PageReference p;
       
             p= new PageReference('/'+OppId);
             p.setRedirect(true); 
            return p;
    }


}