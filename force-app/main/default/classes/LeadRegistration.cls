/*
Purpose: BT Sport Enquires Form to capture BT Sport Enquiries into Lead object

Author                        Date             Description
Sridevi Bayyapuneedi          08/08/2014       Initial development
Sridevi Bayyapuneedi          13/11/2014       Modified as per PostcodeAnywhere implementation
*/

public with sharing class LeadRegistration {

    public List<SelectOption> BTCustomer {get; set;}
    public Lead leadvalue{get; set;}
    public String alBTCust {get; set;}  
    public String venue {get; set;}
    public boolean checkbox1 {get; set;}
    public boolean checkbox2 {get; set;}
    public String address1 {get;set;}
    public String address2 {get;set;}
    private ApexPages.StandardController leadRegController;
      
    public LeadRegistration(ApexPages.StandardController controller) {
         leadRegController = controller;
         leadvalue= new Lead();  
    }

     public PageReference alHaveSky(){
         leadvalue.Existing_Customer__c = Boolean.valueOf(alBTCust);
         return null;
     }
       
     public PageReference setVenue(){
         leadvalue.Type_of_venue__c = venue;
         return null;
     }
       
       
     public PageReference saveRedirect(){
          
         if(alBTCust == null){
          leadvalue.Existing_Customer__c= true;
         }
         
         leadvalue.LeadSource = 'BT Sport';
         leadvalue.LeadExtId__c = 'Call' + leadvalue.Email;
         Datetime cDT = System.now();
         leadvalue.Date_of_Submission__c = String.valueOf(cDT);
         leadvalue.Street = address1.trim()+' '+address2.trim();
         leadvalue.RecordType = [select Id from RecordType where DeveloperName = 'BT_Sport' and SobjectType = 'Lead'];
         
         insert leadvalue; 
         return new PageReference('/apex/BTSportFormSucess');
     }
 }