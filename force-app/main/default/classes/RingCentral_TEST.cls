/*######################################################################################
PURPOSE
Main Test Class for all Ring Central activities

HISTORY
16/12/14    Adam Soobroy   Launch version
######################################################################################*/
@isTest(SeeAllData=true)

private class RingCentral_TEST {

	static Account a {get; set;}
    static Opportunity o {get; set;}
    static Contact c {get; set;}    
    static CSS_Products__c cp {get; set;}
    static Cloud_Voice__c cv {get; set;}
    static Cloud_Voice_Site__c cvs {get; set;}
    static Cloud_Voice_Site_Product__c cvsp {get; set;} 
    static RingCentralURL__c rcURL {get; set;} 

    static void SetVariables(){
		a = Test_Factory.CreateAccount();     
        a.CUG__c = 'cugCV1';
        a.OwnerId = UserInfo.GetUserId() ;
        Database.SaveResult aR = Database.insert(a);     
        o = Test_Factory.CreateOpportunity(aR.getid());
        o.closedate = system.today();
        insert o;      
		c = Test_Factory.CreateContact();
        c.Cug__c = 'cugCV1';
        c.Contact_Post_Code__c = 'WR5 3RL';
        insert c;  
        cp = new CSS_Products__c(Active__c = True, Type__c = 'License', Term__c = '12');
        insert cp;       
        cv = new Cloud_Voice__c (Opportunity__c = o.Id); 
        insert cv;
        cvs = new Cloud_Voice_Site__c (CV__c = cv.Id, Site_contact__c = c.Id, Customer_Required_Date__c = Date.today()); 
        insert cvs;
        cvsp = new Cloud_Voice_Site_Product__c (Cloud_Voice_Site__c = cvs.Id, SFCSSPRodId__c = cp.Id, Quantity__c = 1); 
        insert cvsp;
        rcURL = new RingCentralURL__c (Username__c='test', Password__c='test', Url__c='https://test.com', Partner_Login_Url__c='https://test.com', Name = 'testRCurl');
        insert rcURL;
    }

	static testMethod void RingCentralDealReg() {	
		SetVariables();
		ApexPages.currentPage().getParameters().put('users', 'TEST'); 
		ApexPages.currentPage().getParameters().put('telNum', 'TEST'); 
		ApexPages.currentPage().getParameters().put('MAX', 'TEST'); 
		ApexPages.currentPage().getParameters().put('ADSL2', 'TEST'); 
		ApexPages.currentPage().getParameters().put('WBC', 'TEST'); 
		ApexPages.currentPage().getParameters().put('FTTC', 'TEST'); 
		ApexPages.currentPage().getParameters().put('FTTP', 'TEST'); 
		ApexPages.currentPage().getParameters().put('FOD', 'TEST'); 
		ApexPages.currentPage().getParameters().put('selProduct', 'TEST'); 
		ApexPages.currentPage().getParameters().put('accName', 'TEST'); 
		ApexPages.currentPage().getParameters().put('cvHeadID', cv.Id); 
		ApexPages.currentPage().getParameters().put('cvID', cvs.Id); 
		ApexPages.currentPage().getParameters().put('conId', c.Id); 

		Test.startTest();
		//--------Set mock SOAP callout class 
        Test.setMock(WebServiceMock.class, new RingCentralWebSvcMockImpl());
		RingCentralDealReg dealReg = new RingCentralDealReg();
		dealReg.SetValues();
		Test.stopTest();
	}

	static testMethod void RingCentralSalesFunnel() {
		SetVariables();
		ApexPages.currentPage().getParameters().put('id', cv.Id); 
		Test.startTest();
		RingCentralSalesFunnel salesFunnel = new RingCentralSalesFunnel(new ApexPages.StandardController(cv)); 
		salesFunnel.SetTestHarnessValues();
		salesFunnel.SetValues();
		salesFunnel.FinishButton();
		Test.stopTest();
	}
}