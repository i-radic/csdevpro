public class OCSJsonUtility {
   
    
     public static void getData(String basketId){
        // String crfRecordId = ApexPages.currentPage().getParameters().get('Id');
         //String crfRecordId = 'aBx0E000000CdIt';
         String transactionType = 'OSC JSON';
         Set<String> ObjectIdSet = new Set<String>();
         Map<String,List<Map<String,String>>> dataSetMap = new Map<String,List<Map<String,String>>>();
         
         // create OBject ID Set 
         ObjectIdSet.add(basketId);
         List<cscfga__Product_Configuration__c> configs = [Select Id, Name, cscfga__Product_Basket__c,cscfga__Product_Basket__r.cscfga__Opportunity__c From cscfga__Product_Configuration__c Where cscfga__Product_Basket__c =: basketId];
		 for(cscfga__Product_Configuration__c config : configs){
		     ObjectIdSet.add(config.id);
		     ObjectIdSet.add(config.cscfga__Product_Basket__r.cscfga__Opportunity__c);
		 }
		 System.debug('ObjectIdSet>>>>'+ObjectIdSet);
		 CreateParametersValueMap(ObjectIdSet,transactionType);
     }
     
     
      public static Map<String,List<ProductConfigDataWrapper>> getproductConfigrationData(String basketId){
          Map<String,List<ProductConfigDataWrapper>> ObjectNameProductConfigDataWrapperList = new Map<String,List<ProductConfigDataWrapper>>();
          List<ProductConfigDataWrapper> ProductConfigDataWrapperList = new List<ProductConfigDataWrapper>();
          String transactionType = 'OSC JSON';
          
         // get all Product Configration data for the basket 
         Set<String> ObjectIdSet = new Set<String>(); 
         Set<String> PcIdSet = new Set<String>(); 
         String optyId= '';
         
         ObjectIdSet.add(basketId);
         List<cscfga__Product_Configuration__c> configs = [Select Id, Name, cscfga__Product_Basket__c,cscfga__Product_Basket__r.cscfga__Opportunity__c From cscfga__Product_Configuration__c Where cscfga__Product_Basket__c =: basketId];
		 for(cscfga__Product_Configuration__c config : configs){
		     optyId = config.cscfga__Product_Basket__r.cscfga__Opportunity__c;
		     ObjectIdSet.add(config.cscfga__Product_Basket__r.cscfga__Opportunity__c);
		     ObjectIdSet.add(config.id);
		     PcIdSet.add(config.id);
		 }
		 
		 // get all custom data for PC
		 
		Map<String, String> ParamNameSobjectMap = new Map<String, String>();
        Map<String, String> ParamNameFieldNameMap = new Map<String, String>();
        Map<String, String> ParamNameAttNameMap = new Map<String, String>();
        Map<String, String> ParamNameHardCodedValueMap = new Map<String, String>();
        
        Set<String> ParamNameCustomMappingSet = new Set<String>();
        
        Set<String> sobjectSet = new Set<String>();
        System.debug('TransactionType>>>>'+TransactionType);
        
        List<Data_Mapping__c> dataMappingList = new list<Data_Mapping__c>();
        dataMappingList = [select Active__c,Calculation_Logic__c,Column_Sequence__c,Display_Column_1__c,Parameter_Level__c,Parameter_Type__c,Hardcoded_Value__c,Id,Name,Parameter_Name__c,SFDC_Field_Name__c,SFDC_Object_Name__c,Transaction_Type__c
                                     from Data_Mapping__c where  Active__c = true and Transaction_Type__c includes (:transactionType)];
                            
        if(!dataMappingList.isEmpty()){
            for (Data_Mapping__c dataMappingObj : dataMappingList){
                
                if((dataMappingObj.Calculation_Logic__c == 'SFDC Object')){
                    ParamNameSobjectMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Object_Name__c.tolowercase());
                    ParamNameFieldNameMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Field_Name__c.tolowercase());
                    sobjectSet.add((dataMappingObj.SFDC_Object_Name__c).toLowerCase());
                }
                else if((dataMappingObj.Calculation_Logic__c == 'Configuration Attribute')){
                    // if we need to fatch value from attribute sfdc field name should be excact attribute name
                    ParamNameAttNameMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Field_Name__c.tolowercase());
                }
                else if (dataMappingObj.Calculation_Logic__c == 'Hardcoded'){
                    ParamNameHardCodedValueMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.Hardcoded_Value__c);
                }
            }
          }
        
		  Map<String,Map<String, String>> sfdcObjectdataListForPC = GetSFDCFiledValues(objectIdSet,ParamNameSobjectMap, ParamNameFieldNameMap, sobjectSet);
		  
		  // get HardCode Value
		  
		  // get attribute data
		  
		  Map<String,Map<String,String>> outputParamMapforAtt = GetPCAttributeValues(PcIdSet,ParamNameAttNameMap);
	 	  
	 	  for(cscfga__Product_Configuration__c config : configs){
	 	     ProductConfigDataWrapper pcw = new ProductConfigDataWrapper();
	 	     pcw.hardCodedDataMap = ParamNameHardCodedValueMap;
	 	     pcw.sObjectFiledMap = sfdcObjectdataListForPC.get(config.id);
	 	     pcw.attributeDataNameMap = outputParamMapforAtt.get(config.id);
	 	     pcw.recordId = config.id;
		     ProductConfigDataWrapperList.add(pcw);
		  }
		  ObjectNameProductConfigDataWrapperList.put('ProductConfig',ProductConfigDataWrapperList);
		  
		  ProductConfigDataWrapper pcwforbasket = new ProductConfigDataWrapper();
 	      pcwforbasket.hardCodedDataMap = ParamNameHardCodedValueMap;
 	      pcwforbasket.sObjectFiledMap = sfdcObjectdataListForPC.get(basketId);
 	      pcwforbasket.attributeDataNameMap = null;
 	      pcwforbasket.recordId = basketId;
	      ObjectNameProductConfigDataWrapperList.put('ProductBasket',new List<ProductConfigDataWrapper> {pcwforbasket});
	      
	      ProductConfigDataWrapper pcwforopty = new ProductConfigDataWrapper();
 	      pcwforopty.hardCodedDataMap = ParamNameHardCodedValueMap;
 	      pcwforopty.sObjectFiledMap = sfdcObjectdataListForPC.get(optyId);
 	      pcwforopty.attributeDataNameMap = null;
 	      pcwforopty.recordId = optyId;
	      ObjectNameProductConfigDataWrapperList.put('ProductOpty',new List<ProductConfigDataWrapper> {pcwforopty});
	      
	     
            System.Debug('ObjectNameProductConfigDataWrapperList>>>>'+JSON.serializePretty(ObjectNameProductConfigDataWrapperList));
            return null;
	     
      }
      
      public class ProductConfigDataWrapper{
        String recordId{get;set;}  
        String recordType{get;set;}
        Map<String, String> hardCodedDataMap{get;set;}
        Map<String, String> attributeDataNameMap {get;set;}
        Map<String, String> sObjectFiledMap{get;set;}
      }
    
    public static Map<String, String> CreateParametersValueMap(Set<String> objectIdSet, String TransactionType)
    {
        Map <String, String> OutputParamMap = new Map<String, String>();
        
        Map<String, String> ParamNameSobjectMap = new Map<String, String>();
        Map<String, String> ParamNameFieldNameMap = new Map<String, String>();
        Map<String, String> ParamNameAttNameMap = new Map<String, String>();
        Map<String, String> ParamNameHardCodedValueMap = new Map<String, String>();
        
        Set<String> ParamNameCustomMappingSet = new Set<String>();
        
        Set<String> sobjectSet = new Set<String>();
        System.debug('TransactionType>>>>'+TransactionType);
        
        List<Data_Mapping__c> dataMappingList = new list<Data_Mapping__c>();
        dataMappingList = [select Active__c,Calculation_Logic__c,Column_Sequence__c,Display_Column_1__c,Parameter_Level__c,Parameter_Type__c,Hardcoded_Value__c,Id,Name,Parameter_Name__c,SFDC_Field_Name__c,SFDC_Object_Name__c,Transaction_Type__c
                                     from Data_Mapping__c where  Active__c = true and Transaction_Type__c includes (:transactionType)];
                            
        if(!dataMappingList.isEmpty()){
            for (Data_Mapping__c dataMappingObj : dataMappingList){
                 System.debug('dataMappingObj.Parameter_Name__c>>'+dataMappingObj.Parameter_Name__c);
                System.debug('dataMappingObj.Parameter_Level__c>>'+dataMappingObj.Parameter_Level__c);
                System.debug('dataMappingObj.Hardcoded_Value__c>>'+dataMappingObj.Display_Column_1__c);     
                System.debug('dataMappingObj.Calculation_Logic__c>>'+dataMappingObj.Calculation_Logic__c);
                 
                if((dataMappingObj.Calculation_Logic__c == 'SFDC Object')){
                    ParamNameSobjectMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Object_Name__c.tolowercase());
                    ParamNameFieldNameMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Field_Name__c.tolowercase());
                    sobjectSet.add((dataMappingObj.SFDC_Object_Name__c).toLowerCase());
                }
                else if((dataMappingObj.Calculation_Logic__c == 'Configuration Attribute')){
                    // if we need to fatch value from attribute sfdc field name should be excact attribute name
                    ParamNameAttNameMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.SFDC_Field_Name__c.tolowercase());
                }
                else if (dataMappingObj.Calculation_Logic__c == 'Hardcoded'){
                    ParamNameHardCodedValueMap.put((dataMappingObj.Parameter_Name__c).toLowerCase(), dataMappingObj.Hardcoded_Value__c);
                }
            }
        }
        //OutputParamMap= 
        GetSFDCFiledValues(objectIdSet,ParamNameSobjectMap, ParamNameFieldNameMap, sobjectSet);
        OutputParamMap.putAll(ParamNameHardCodedValueMap);
        // get data for Custom mapping
        System.Debug('OutputParamMap111'+JSON.serializePretty(OutputParamMap));
        System.Debug('ParamNameCustomMappingSet'+ParamNameCustomMappingSet);
       // OutputParamMap= GetCustomMappingData(OutputParamMap,ParamNameCustomMappingSet);
		System.Debug('OutputParamMap222'+JSON.serializePretty(OutputParamMap));
		// 
		if(!ParamNameAttNameMap.isEmpty()){
			/*Map<String,String> outputParamMapforAtt = GetPCAttributeValues(objectIdSet,ParamNameAttNameMap, sobjectSet);
			if(!outputParamMapforAtt.isEmpty()){
			    OutputParamMap.putAll(outputParamMapforAtt);
			}*/
		}
		
        return OutputParamMap;
    }
    
    Public Static Map<String,Map<String, String>> GetSFDCFiledValues(Set<String> ObjectIdList, Map<String, String> ParamNameSobjectMap, Map<String, String> ParamNameFieldNameMap, Set<String> sobjectSet)
    {
      Map<String, String> sobjectfieldnamesMap = new Map<String, String>();
      Map<String, Set<String>> sobjectfieldnamesSetMap = new Map<String, Set<String>>();
    //   /Map<String, String> paramNameValueMap = new Map<String, String>();
      Map<String, sobject> sObjectNamesobjectMap = new Map<String, sobject>();
      list<String> FinalObjectIdList = new list<String>();
      
      Map<String, Set<String>> sobjectNameAndIdSetMap = new Map<String, Set<String>>();
     Map<String, List<sobject>> sObjectNamesObjectListMap = new Map<String, List<sobject>>();
      Map<String, List<sobject>> sObjectIdObjectListMap = new Map<String, List<sobject>>();
      
      Map<String,Map<String, String>> sObjectIdparamNameValueMap = new Map<String,Map<String, String>>();
      try
      {
          System.debug('ParamNameSobjectMap>>'+ParamNameSobjectMap);
          System.debug('ParamNameFieldNameMap>>'+ParamNameFieldNameMap);
          System.debug('sobjectSet>>'+sobjectSet);
          for (String param : ParamNameFieldNameMap.keySet())
          {
              System.debug('param>>'+param);
              String fieldsnames;
              Set<String> fieldsnameSet = new Set<String>();
              if(sobjectfieldnamesSetMap.containsKey(ParamNameSobjectMap.get(param.tolowercase())))
              {
                  fieldsnameSet = sobjectfieldnamesSetMap.get(ParamNameSobjectMap.get(param.tolowercase()));
                  System.debug('fieldname>>'+ParamNameFieldNameMap.get(param.tolowercase()));
                  fieldsnameSet.add(ParamNameFieldNameMap.get(param.tolowercase()));
              }
              else
              {
                  fieldsnameSet.add(ParamNameFieldNameMap.get(param.tolowercase()));
              }

              sobjectfieldnamesSetMap.put(ParamNameSobjectMap.get(param).tolowercase(), fieldsnameSet);
          } 
          System.debug('sobjectfieldnamesSetMap>>'+sobjectfieldnamesSetMap);
          System.debug('ObjectIdList>>'+ObjectIdList);
          if(!ObjectIdList.isEmpty()){
              for(String objId : ObjectIdList){
                  if(String.isnotblank(objId) && !(objId.equalsignorecase('null')))
                  {
                	  System.debug('inside if for rec');
                      FinalObjectIdList.add(objId);
                  }
              }
          }
          System.debug('FinalObjectIdList>>'+FinalObjectIdList);
          for (id RecordId : FinalObjectIdList)
          {
              String sObjectName = (RecordId.getSObjectType().getDescribe().getName()).tolowercase();
              System.debug('sObjectName>>'+sObjectName);
              if(sobjectNameAndIdSetMap.containsKey(sObjectName)){
                  sobjectNameAndIdSetMap.get(sObjectName).add(RecordId);
              }else{
                  sobjectNameAndIdSetMap.put(sObjectName, new Set<String> {RecordId});
              }
              
          }
          for (String sObjectName : sobjectNameAndIdSetMap.keySet())
          {
              
              System.debug('sObjectName>>'+sObjectName);
              Set<String> sObjectIdSet = sobjectNameAndIdSetMap.get(sObjectName);
              
              String soqlquery, soqlfields;
              if (sobjectfieldnamesSetMap.get(sObjectName.toLowerCase()) != null)
              {
                  if(!sobjectfieldnamesSetMap.get(sObjectName.toLowerCase()).contains('id')){
                      sobjectfieldnamesSetMap.get(sObjectName.toLowerCase()).add('id');
                  }
                  
                  soqlfields = DataMappingUtility.convertSettoString(sobjectfieldnamesSetMap.get(sObjectName.toLowerCase()));
                  System.debug('soqlfields>>'+soqlfields); 
              }    
              
              if (soqlfields != null)
              {
                  System.debug('soqlfields>>'+soqlfields);
                  System.debug('sObjectIdSet>>'+sObjectIdSet);
                  
                  soqlquery = 'select '+soqlfields+' from '+sObjectName+' where id  in :sObjectIdSet' ;
                  System.debug('soqlquery>>'+soqlquery);
                  list<sobject> QueryResult = Database.query(soqlquery);
                  System.debug('QueryResult>>'+QueryResult);
                  System.debug('QueryResult.size>>'+QueryResult.size());
                  if (QueryResult.size() > 0)
                  {
                      sObjectNamesObjectListMap.put(sObjectName, QueryResult);
                      
                  }    
              }    
          }
          System.debug('sObjectNamesObjectListMap>>'+sObjectNamesObjectListMap);
         /* for(String objectName : sObjectNamesObjectListMap.keySet()){
              List<sobject> sObjectNamesList = sObjectNamesObjectListMap.get(objectName);
              System.debug('sObjectNamesList>>'+sObjectNamesList);
              if(sObjectNamesList != null && !sObjectNamesList.isEmpty()){
                String idStr = String.valueOf(sObjectNamesList[0].get('id'));
                sObjectIdObjectListMap.put(idStr, sObjectNamesList);
              }
          }
          System.debug('sObjectIdObjectListMap>>'+sObjectIdObjectListMap);*/
          
          for(String objectName : sObjectNamesObjectListMap.keySet()){
              List<sobject> sObjectList = sObjectNamesObjectListMap.get(objectName);
              System.debug('objectName>>'+objectName);
              System.debug('sObjectNamesObjectListMap111>>'+sObjectNamesObjectListMap);
              System.debug('sObjectList>>'+sObjectList);
              for(sObject sObj : sObjectList){
                   Map<String, String> paramNameValueMap = new Map<String, String>();
                  for (String param : ParamNameFieldNameMap.keySet())
                  {
                      System.debug('param>>'+param);
                      if(String.valueOf(ParamNameSobjectMap.get(param)).equalsIgnoreCase(objectName)){
                          System.debug('DataMappingUtility.returnFieldValue(sObj, ParamNameFieldNameMap.get(param))>>'+DataMappingUtility.returnFieldValue(sObj, ParamNameFieldNameMap.get(param)));
                          paramNameValueMap.put(param.toLowerCase(), DataMappingUtility.returnFieldValue(sObj, ParamNameFieldNameMap.get(param)));
                          System.debug('paramNameValueMap>>'+paramNameValueMap);
                      }
                  }
                  System.debug('paramNameValueMap>>'+paramNameValueMap);
                  sObjectIdparamNameValueMap.put(String.valueOf(sObj.get('id')),paramNameValueMap);
              }
          }
          /*for(String objectId : sObjectIdObjectListMap.keySet()){
              List<sobject> sObjectList = sObjectIdObjectListMap.get(objectId);
              System.debug('objectId>>'+objectId);
              System.debug('sObjectList>>'+sObjectList);
              for(sObject sObj : sObjectList){
                   Map<String, String> paramNameValueMap = new Map<String, String>();
                  for (String param : ParamNameFieldNameMap.keySet())
                  {
                      System.debug('param>>'+param);
                      if(String.avlueOf((ParamNameSobjectMap.get(param)).equlasInoreCase())
                      System.debug('DataMappingUtility.returnFieldValue(sObj, ParamNameFieldNameMap.get(param))>>'+DataMappingUtility.returnFieldValue(sObj, ParamNameFieldNameMap.get(param)));
                      paramNameValueMap.put(param.toLowerCase(), DataMappingUtility.returnFieldValue(sObj, ParamNameFieldNameMap.get(param)));
                      System.debug('paramNameValueMap>>'+paramNameValueMap);
                  }
                  System.debug('paramNameValueMap>>'+paramNameValueMap);
                  sObjectIdparamNameValueMap.put(String.valueOf(sObj.get('id')),paramNameValueMap);
              }
          }*/
          
          System.debug('sObjectIdparamNameValueMap>>'+JSON.serializePretty(sObjectIdparamNameValueMap));
         
      }
      catch (exception e)
      {
          
      }
     // return paramNameValueMap;
     return sObjectIdparamNameValueMap;
    }  
    
    
    Public Static Map<String,Map<String, String>> GetPCAttributeValues(Set<String> PCIdList, Map<String, String> ParamNameAttNameMap)
    {
		Map<String, String> paramNameValueMap = new Map<String, String>();
		Map<String, String> attNameValueMap = new Map<String, String>();
		
	    Map<String,Map<String, String>> pctIdAttNameValueMap = new Map<String,Map<String, String>>();
	    Map<String,List<cscfga__Attribute__c>> pctIdAttDataListMap= new Map<String,List<cscfga__Attribute__c>>();
		 
		try{
			
			if(!PCIdList.isEmpty()){
			    	System.Debug('ParamNameAttNameMap>>'+ParamNameAttNameMap);
				List<cscfga__Attribute__c> attbuiteDataList = [SELECT cscfga__Product_Configuration__c,cscfga__Display_Value__c,cscfga__Value__c,Name FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c in :PCIdList and name in : ParamNameAttNameMap.values()];
				if(!attbuiteDataList.isEmpty()){
					System.Debug('attbuiteDataList.isEmpty()>>'+attbuiteDataList);
					for(cscfga__Attribute__c att : attbuiteDataList){	
					    if(pctIdAttDataListMap.containsKey(att.cscfga__Product_Configuration__c)){
					        pctIdAttDataListMap.get(att.cscfga__Product_Configuration__c).add(att);
					    }else{
					        pctIdAttDataListMap.put(att.cscfga__Product_Configuration__c,new List<cscfga__Attribute__c> {att});
					    }
					}
				}
				
				if(!pctIdAttDataListMap.isEmpty()){
					System.debug('pctIdAttDataListMap>>'+pctIdAttDataListMap);
					for(String pcId : pctIdAttDataListMap.keySet()){
					    
					    paramNameValueMap = new Map<String, String>();
		                attNameValueMap = new Map<String, String>();
					    List<cscfga__Attribute__c> attbuiteDataListTemp = pctIdAttDataListMap.get(pcId);
					    
					    if(!attbuiteDataListTemp.isEmpty()){
        					System.Debug('attbuiteDataListTemp>>'+attbuiteDataListTemp);
        					for(cscfga__Attribute__c att : attbuiteDataListTemp){						
        						attNameValueMap.put(String.valueOF(att.Name).toLowerCase(),att.cscfga__Value__c);
        					}
        				}
        				
        				if(!attNameValueMap.isEmpty()){
        					System.Debug('attNameValueMap>>'+attNameValueMap);
        					for(String paramName :ParamNameAttNameMap.keySet()){
        						paramNameValueMap.put(paramName,attNameValueMap.get(ParamNameAttNameMap.get(paramName)));
        					}
        				}
        				pctIdAttNameValueMap.put(pcId,paramNameValueMap);
					}
				}
				
			
				
			}
			
		}catch(Exception e){
			
		}	
		System.Debug('pctIdAttNameValueMap>>'+pctIdAttNameValueMap);
		return pctIdAttNameValueMap;
	}

}