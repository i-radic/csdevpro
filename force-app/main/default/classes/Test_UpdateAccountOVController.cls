@isTest(seealldata=true)
private class Test_UpdateAccountOVController {
    
    static testMethod void runTest1() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Account testAcc = Test_Factory.CreateAccount();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(testAcc);
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(sc);
        UpdateAccountOVController updAccountOVController2 = new UpdateAccountOVController(testAcc); 
     
        /**UpdateAccountOVController updAccountOVController = new UpdateAccountOVController(new ApexPages.StandardController(testAcc));
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(testAcc);*/
        
        updAccountOVController1.testCUGId = 'TEST';       
        updAccountOVController1.testPrimaryContactId = 'TESTexistingSFContact';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ApexPages.CurrentPage().GetParameters().put('contactid', 'TEST');                      
        updAccountOVController1.OnLoad();
       
        
    }static testMethod void runTest2() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Account testAcc = Test_Factory.CreateAccount();
        ApexPages.StandardController sc = new ApexPages.StandardController(testAcc);
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(sc);
        UpdateAccountOVController updAccountOVController2 = new UpdateAccountOVController(testAcc); 
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        updAccountOVController1.testCUGId = 'CUG5600114831';
        updAccountOVController1.testPrimaryContactId = 'TEST';
        
        ApexPages.CurrentPage().GetParameters().put('contactid', 'TEST');
                
        updAccountOVController1.OnLoad();
                 
    }
    static testMethod void runTest3() {
        Test_Factory.SetProperty('IsTest', 'no');
        Account testAcc = Test_Factory.CreateAccount();
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(testAcc);
        UpdateAccountOVController updAccountOVController = new UpdateAccountOVController(new ApexPages.StandardController(testAcc));
        updAccountOVController1.testCUGId = 'TEST';
        updAccountOVController1.testPrimaryContactId = 'TESTexistingSFContact';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ApexPages.CurrentPage().GetParameters().put('contactid', 'TEST');
        ApexPages.CurrentPage().GetParameters().put('cug', 'TEST');
        
        updAccountOVController1.OnLoad();
        
       
    }
    static testMethod void runTest4() {
        Test_Factory.SetProperty('IsTest', 'no');
        Account testAcc = Test_Factory.CreateAccount();
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(testAcc);
        UpdateAccountOVController updAccountOVController = new UpdateAccountOVController(new ApexPages.StandardController(testAcc));
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ApexPages.CurrentPage().GetParameters().put('cug', 'TEST');
        ApexPages.CurrentPage().GetParameters().put('contactid', 'TEST');
        updAccountOVController1.testCUGId = 'TEST';
        updAccountOVController1.testPrimaryContactId = 'TESTexistingSFContact';
        UpdateAccountOVController.CRMAccount account = new UpdateAccountOVController.CRMAccount();
        updAccountOVController1.OnLoad();         
    }
	
    static testMethod void runTest5() {
        Test_Factory.SetProperty('IsTest', 'No');
        Account testAcc = Test_Factory.CreateAccount();
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(testAcc);
        UpdateAccountOVController updAccountOVController = new UpdateAccountOVController(new ApexPages.StandardController(testAcc));
        updAccountOVController1.testCUGId = 'TEST';
        updAccountOVController1.testPrimaryContactId = null;
       
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        ApexPages.CurrentPage().GetParameters().put('contactid', 'TEST');
        ApexPages.CurrentPage().GetParameters().put('cug', 'TEST');
        
        
        //updAccountOVController1.OnLoad();
        
         
    }
    
     static testMethod void runTest6() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Account testAcc = Test_Factory.CreateAccount();
         
        //ApexPages.currentPage().getParameters().put('contactid',Null); 
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(testAcc);
        UpdateAccountOVController updAccountOVController = new UpdateAccountOVController(new ApexPages.StandardController(testAcc));
        updAccountOVController1.testCUGId = 'CUG5600114831';
        updAccountOVController1.testPrimaryContactId = null;
        
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator3());
        ApexPages.CurrentPage().GetParameters().put('contactid', 'Test');
        ApexPages.CurrentPage().GetParameters().put('cug', 'CUG5600114831');
        
        updAccountOVController1.OnLoad();
        
         
    }static testMethod void runTest7() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Account testAcc = Test_Factory.CreateAccount();
         
        //ApexPages.currentPage().getParameters().put('contactid',Null); 
        UpdateAccountOVController updAccountOVController1 = new UpdateAccountOVController(testAcc);
        UpdateAccountOVController updAccountOVController = new UpdateAccountOVController(new ApexPages.StandardController(testAcc));
        updAccountOVController1.testCUGId = 'CUG5600114831';
        updAccountOVController1.testPrimaryContactId = null;
        
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator3());
        ApexPages.CurrentPage().GetParameters().put('contactid', Null);
        ApexPages.CurrentPage().GetParameters().put('cug', 'CUG5600114831');
        
        updAccountOVController1.OnLoad();
        
         
    }
}