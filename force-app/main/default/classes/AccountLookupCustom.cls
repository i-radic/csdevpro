/*global class CVLookupContactQuery extends cscfga.ALookupSearch{
    //global List<Object> getData(Map<String,Object> inputMap) {
        system.debug('In the CVLookupContactQuery');
        //String accName = (String) inputMap.get('accName');
        public override String getRequiredAttributes() {
            return '[]';
        }
        public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) { 
            List<Contact> contacts = [
            select Name, Email, Phone, MobilePhone from Contact
            ];
            system.debug('contacts'+contacts);
             return contacts;
        }
}*/

global class AccountLookupCustom extends cscfga.ALookupSearch {
public override String getRequiredAttributes() {
return '[]';
}
public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) { 
system.debug(searchFields);
List<Contact> contacts = [
            select Name, Email, Phone, MobilePhone from Contact Limit 10
            ];
return contacts;
}
}