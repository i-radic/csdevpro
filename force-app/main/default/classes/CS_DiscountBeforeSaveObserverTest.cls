@IsTest
public class CS_DiscountBeforeSaveObserverTest  {
  testMethod static void testDiscountSaveObserver(){
    CS_TestDataFactory.insertTriggerDeactivatingSetting();
    CS_TestDataFactory.setupNoTriggersFlag();
    CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    
    Account acc = CS_TestDataFactory.generateAccount(true, 'test');
    Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
    cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
                cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

                cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
                pc.cscfga__Product_Definition__c = pd.ID;
                pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
                pc.cscfga__discounts__c = ' {"discounts":[{"memberDiscounts":[{"chargeType":"recurring", "customData":{"creditFundId":"aA63E0000008aGjSAI","stagedMonth":"2"}, "recordType":"single","type":"absolute","source":"Manual 1","discountCharge":"__PRODUCT__","description":"30","amount":5.0,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
                pc.Calculations_Product_Group__c = 'Future Mobile';
                INSERT pc;

                CS_DiscountBeforeSaveObserver cmtrl = new CS_DiscountBeforeSaveObserver();

                Test.startTest();

                cmtrl.execute(new List<cscfga__Product_Configuration__c>{pc});

                Test.stopTest();
  }

  testMethod static void testDiscountSaveObserver1(){
    CS_TestDataFactory.insertTriggerDeactivatingSetting();
    CS_TestDataFactory.setupNoTriggersFlag();
    CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    
    Account acc = CS_TestDataFactory.generateAccount(true, 'test');
    Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
    cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
                cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

                cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
                pc.cscfga__Product_Definition__c = pd.ID;
                pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
                pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Manual 1","discountCharge":"__PRODUCT__","description":"30","amount":5.0,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
                pc.Calculations_Product_Group__c = 'Future Mobile';
                INSERT pc;

                CS_DiscountBeforeSaveObserver cmtrl = new CS_DiscountBeforeSaveObserver();

                Test.startTest();

                cmtrl.execute(new List<cscfga__Product_Configuration__c>{pc, pc});

                Test.stopTest();
  }

    testMethod static void testDiscountSaveObserver2(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Manual 1","discountCharge":"__PRODUCT__","description":"30","amount":5.0,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
        pc.Product_Code__c = 'UNLMAT';
        pc.Calculations_Product_Group__c = 'Future Mobile';
        INSERT pc;

        CS_DiscountBeforeSaveObserver cmtrl = new CS_DiscountBeforeSaveObserver();

        Test.startTest();

        cmtrl.execute(new List<cscfga__Product_Configuration__c>{pc, pc});

        Test.stopTest();
    }

    testMethod static void testDiscountSaveObserver3(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Manual 1","discountCharge":"__PRODUCT__","description":"30","amount":5.0,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
        pc.Product_Code__c = 'UNLMAT';
        pc.Calculations_Product_Group__c = 'Future Mobile';
        INSERT pc;

        CS_DiscountBeforeSaveObserver cmtrl = new CS_DiscountBeforeSaveObserver();

        Test.startTest();

        cmtrl.execute(new List<cscfga__Product_Configuration__c>{pc});

        Test.stopTest();
    }

}