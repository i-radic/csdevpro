public without sharing class ContractChangeTriggerHandler {

    // Member Variables
    private Map<Id, Contract_Change__c> mMapContractChangeNew;
    private Map<Id, Contract_Change__c> mMapContractChangeOld;
    private List<Contract_Change__c> mListContractChangeNew;
    private List<Contract_Change__c> mListContractChangeOld;
    private Integer miTriggerSize;
    private Boolean mbIsInsert;
    private Boolean mbIsUpdate;
    private Boolean mbIsBefore;
    private Boolean mbIsAfter;

	public String documentType {get; set;}
	public List<SelectOption> documentTypeList {get; private set;}
	final Map<String, String> fieldByLabel = new Map<String, String>();

    // Class Variables
    private static Boolean cbIsExecuting;
    @TestVisible private static Boolean cbIsFirstRun = true;
    
    // Constructor
    public ContractChangeTriggerHandler(  Map<Id, Contract_Change__c> pMapContractChangeNew, Map<Id, Contract_Change__c> pMapContractChangeOld, 
    							List<Contract_Change__c> pListContractChangeNew, List<Contract_Change__c> pListContractChangeOld,
                                Integer piTriggerSize,
                                Boolean pbIsInsert, Boolean pbIsUpdate, Boolean pbIsBefore, Boolean pbIsAfter,
                                Boolean pbIsExecuting ){

        // Assign Member Variables  
        mMapContractChangeNew	= pMapContractChangeNew;
        mMapContractChangeOld	= pMapContractChangeOld;
        mListContractChangeNew	= pListContractChangeNew;
        mListContractChangeOld	= pListContractChangeOld;
        miTriggerSize   		= piTriggerSize;
        mbIsInsert      		= pbIsInsert;
        mbIsUpdate      		= pbIsUpdate;
        mbIsBefore      		= pbIsBefore;
        mbIsAfter       		= pbIsAfter;    

        // Assign Class Variables
        cbIsExecuting   = pbIsExecuting;    
                                    
    }


   /*
    * @name         processTrigger
    * @description  entry point for the Contract Change trigger to process the Contract Change trigger class
    * @author       P Goodey
    * @date         Oct 2013
    * @see 
    */
    public void processTrigger() {

        // Process Before Trigger       
        if(mbIsBefore){
        	
            // Prevent the trigger from re-entrency
            if(cbIsFirstRun){

                // Validation to prevent duplicate Contract Changes
                validateExistingContractChange();
                
                
                // Set our static to prevent transaction re-entrancy
                cbIsFirstRun = false;
            }
        }

    }



   /*
    * @name         validateExistingContractChange
    * @description  entry point for validating that duplicate Contract Changes are not created on an Opportunity
    * @author       P Goodey
    * @date         Oct 2013
    * @see 
    */
    private void validateExistingContractChange(){
    	
    	Set<String> lSetRecordTypeLabel = new Set<String>();
    	Set<String> lSetExistingStatus = new Set<String>();
		Set<String> lSetNewStatus = new Set<String>();

		
		Set<Id> lSetOpportunityNewContractChange = new Set<Id>();
		Set<Id> lSetOpportunityExistingContractChange = new Set<Id>();

		// Key: Opportunity Id + Contract Change RecordType Label Id: Contract Change Id
		Map<String, Id> lMapOpportunityExistingContractChangeRecordType_Id = new Map<String, Id>();


/*
		string lsCreationRestriction = '{"Creation_Restriction_Options" :[';
		lsCreationRestriction += '{"RecordTypeLabel": "Contract Extension","Existing Status": "Contract Extension|Rejected - Validation"},';
		lsCreationRestriction += '{"RecordTypeLabel": "Contract Extension","Existing Status": "Contract Extension|Rejected - Customer"},';
		lsCreationRestriction += '{"RecordTypeLabel": "Contract Extension Read Only","Existing Status": "Contract Extension Read Only|Rejected - Validation"},';
		lsCreationRestriction += '{"RecordTypeLabel": "Contract Extension Read Only","Existing Status": "Contract Extension Read Only|Rejected - Customer"},';
		lsCreationRestriction += '{"RecordTypeLabel": "Contract Extension","Read Only": " Read Only"},';
		lsCreationRestriction += '{"RecordTypeLabel": "Contract Extension","New Status": "Draft"}]}';
		
		Map<String, Object> lMapValidContractChangeList = (Map<String, Object>)JSON.deserializeUntyped(lsCreationRestriction);
*/
		// Get the Custom Label with the JSON settings
		Map<String, Object> lMapValidContractChangeList = (Map<String, Object>)JSON.deserializeUntyped(system.Label.Contract_Change_Create_Restrictions);
		
		List<Object> recordTypes = (List<Object>)lMapValidContractChangeList.get('Creation_Restriction_Options');

		// There can be two versions of the record type one is locked and has the suffic Read Only
		string lsReadOnly;

		for(Object objRecordType : recordTypes) {
			Map<String, Object> lMapRecordType = (Map<String, Object>)objRecordType;
			lSetRecordTypeLabel.add( (String)lMapRecordType.get('RecordTypeLabel') );
			lSetExistingStatus.add( (String)lMapRecordType.get('Existing Status') );
			lSetNewStatus.add( (String)lMapRecordType.get('New Status') );
			
			if( lsReadOnly == null || lsReadOnly == ''  ){
				lsReadOnly = (String)lMapRecordType.get('Read Only');
			}
						
		}

        // loop through the new updated Contract Change record
        for( Contract_Change__c new_cc : mListContractChangeNew ){

			// Check if the Contract Change Recordtype isone that part of the Contract Chnage Restrictions
			if( lSetRecordTypeLabel.contains(new_cc.Record_Type_Capture__c) ){
			
				// If this is a creation of a new Contract Change
				if( mbIsInsert ){

					if( !lSetNewStatus.contains(new_cc.Status__c) ){	

						// Raise an Error
						new_cc.Status__c.addError( 'Cannot Create a new Contract Change at Status:' + new_cc.Status__c);
					}
				}

				// Check if there is an Opportunity set
				if( new_cc.Opportunity__c != null ){

					// Add the Opportunity to the set of Opportunity Ids to check for existing Contract Changes
					lSetOpportunityNewContractChange.add( new_cc.Opportunity__c );				

				}else{
					
					//new_cc.Opportunity__c.addError( 'Please select an Opportunity');

				}
			}
        }
        
        
        
        // Now Check if there is an Contract Change already on the Opportunity which we ned to check for our recordtype
        if( !lSetOpportunityNewContractChange.IsEmpty() ){
        	
        	List<Contract_Change__c> lListContractChange = 
        		[SELECT Id, Status__c, Opportunity__c, Record_Type_Capture__c FROM Contract_Change__c WHERE Opportunity__c IN :lSetOpportunityNewContractChange];
        	
        	// Loop through the existing Contract Changes
        	for( Contract_Change__c existing_cc : lListContractChange ){
        		
        		// Now check the status of any existing Contract Change - if the Status is not in the list of allowed Status Values
        		// ie Rejected then this will raise an error

				// Construct a key for the existing recordtype status
				string lsExistingRecordtypeDelimiterStatus = existing_cc.Record_Type_Capture__c + '|' + existing_cc.Status__c;

        		// Check if the Status of existing Contract Changes are ones that are valid ie Rejected
 	        	if( !lSetExistingStatus.contains( lsExistingRecordtypeDelimiterStatus )){
	        			
	        		// Add to the set of duplicate Contract Changes to prevent the creation of duplicate
		        	string lsKeyExisting = existing_cc.Opportunity__c + '|' + existing_cc.Record_Type_Capture__c;
					lMapOpportunityExistingContractChangeRecordType_Id.put( lsKeyExisting, existing_cc.Id );
 	        	}
        	}
        }



        
        // Now Finally Loop through the new Contract Changes and check if there is already a Contract Change on the Opportunity
        for( Contract_Change__c new_cc : mListContractChangeNew ){

			// Check that there is an Opportunity
			if( new_cc.Opportunity__c != null ){
				
				// buils a key for Opportunity RecordType
				string lsKeyNew = new_cc.Opportunity__c + '|' + new_cc.Record_Type_Capture__c;
				
				// Check if the same recordtype is being inserted or a Read Only version of the Contract Change
				if( (lMapOpportunityExistingContractChangeRecordType_Id.containsKey( lsKeyNew)) || 
					(lMapOpportunityExistingContractChangeRecordType_Id.containsKey( lsKeyNew + lsReadOnly )) ){	

					// If this is an New Contract Change
					if( mbIsInsert ){
					
						// Raise an Error for the User	
						new_cc.Opportunity__c.addError( 'There is already a Contract Change for the Opportunity');
					}
				
				}else{
					
				// Check if the same recordtype is being inserted or a Read Only version of the Contract Change
				if( (lMapOpportunityExistingContractChangeRecordType_Id.containsKey( lsKeyNew)) || 
					(lMapOpportunityExistingContractChangeRecordType_Id.containsKey( lsKeyNew + lsReadOnly )) ){
						
						// Stop the trigger from firing for updates to records
						if( (lMapOpportunityExistingContractChangeRecordType_Id.get( lsKeyNew ) != new_cc.Id) ||
							(lMapOpportunityExistingContractChangeRecordType_Id.get( lsKeyNew + lsReadOnly ) != new_cc.Id)  ){
							// Raise an Error for the User	
							new_cc.Opportunity__c.addError( 'There is already a Contract Change for the Opportunity');
						}
					}					
				}
			}
        }        
	}

}