@isTest(SeeAllData=false)
public with sharing class CSContractSol_newReviewControllerTest {
    
    
        static Account testAccount;
        static Opportunity testOpportunity;
        static Contact testContact;
        static cscfga__Product_Basket__c testBasket;
        static csclm__Agreement__c testAgreement;
        static Associated_LEs__c testAssociatedLE;
        static cscfga__Product_Configuration__c testConfiguration;
        static cscfga__Product_Definition__c testPD;
        static cscfga__Product_Configuration__c testConfigurationUG;
        static cscfga__Product_Definition__c testPDUG;
        static csclm__Document_Template__c testTemplate;
        static csclm__Document_Definition__c testDefinition;
        static csclm__Section_Definition__c testSection;
        static csclm__Document_Template_Section_Association__c testAssoc;
        static csclm__Transactional_Document__c testTransactionalDocument;
        static csclm__Transactional_Section__c testTransactionalSection;
        static csclm__Transactional_Clause__c testTransactionalClause;

        private static void prepareTestData(){
            // testAccount = new Account(  name = 'RT test account',
            //                             OwnerId =UserInfo.getUserId() );
            // insert testAccount;
            //testAccount=[select id from account limit 1];
            //testContact=[select id from contact limit 1];
            /*testOpportunity= new Opportunity(   Name='RT test',
                                                AccountId=testAccount.id,
                                                CloseDate=date.today(),
                                                StageName='Created'
                                                );
            insert testOpportunity;
            testBasket = new cscfga__Product_Basket__c(cscfga__Opportunity__c=testOpportunity.Id);
            testBasket.Base_Proposition_Name__c = 'test Base Proposition Name';
            testBasket.csbb__Synchronised_With_Opportunity__c=true;
            insert testBasket;
            testOpportunity.Tenure__c=24.0;
            testOpportunity.Basket_ID__c='EECS-077138';
            testOpportunity.AssociatedLE__c = 'a1L3E000001iIo6';
            testOpportunity.Primary_Contact__c=testContact.Id;
            testOpportunity.Company_Signatory1__c=testContact.Id;
            testOpportunity.Company_Signatory2__c=testContact.Id;
            update testOpportunity; 
            */
            //testBasket = [select id, csbb__Synchronised_With_Opportunity__c, cscfga__Opportunity__c from cscfga__Product_Basket__c where cscfga__Opportunity__c != null limit 1];

            //testOpportunity = [select id from opportunity where Id = :testBasket.cscfga__Opportunity__c];
            
            TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
            INSERT triggerSetting;
            OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
            INSERT oliSync;

            CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
            
            testAccount=CS_TestDataFactory.generateAccount(true,'testingAccount');
            testContact=CS_TestDataFactory.generateContact(true,'testingContact');
            testOpportunity=CS_TestDataFactory.generateOpportunity(true,'testingOppo',testAccount);
            testAssociatedLE = CS_TestDataFactory.generateAssociatedLES(true,'testAssocLE',testAccount);
            testBasket=CS_TestDataFactory.generateProductBasket(true, 'testingBasket', testOpportunity);

            testConfiguration= CS_TestDataFactory.generateProductConfiguration(true, 'test configuration Future Mobile Service', testBasket);
            testPD = new cscfga__Product_Definition__c();
            testPD.Name = 'Future Mobile Service';
            testPD.cscfga__Description__c ='product definition for testing';
            insert testPD;
            //testPD =[select id from cscfga__Product_Definition__c where name like '%Future Mobile Service%' limit 1];
            testConfiguration.cscfga__Product_Definition__c = testPD.id;
            update testConfiguration;

            // need to enter one more Product Configuration, for User Group PD
            testConfigurationUG= CS_TestDataFactory.generateProductConfiguration(true, 'test configuration User Group', testBasket);
            testPDUG = new cscfga__Product_Definition__c();
            testPDUG.Name = 'User Group';
            testPDUG.cscfga__Description__c ='product definition for testing';
            insert testPDUG;
            //testPD =[select id from cscfga__Product_Definition__c where name like '%Future Mobile Service%' limit 1];
            testConfigurationUG.cscfga__Product_Definition__c = testPDUG.id;
            update testConfigurationUG;



            testBasket.csbb__Synchronised_With_Opportunity__c = true;
            update testBasket;
            testOpportunity.AssociatedLE__c = testAssociatedLE.Id;
            testOpportunity.Primary_Contact__c=testContact.Id;
            testOpportunity.company_signatory1__c=testContact.Id;
            update testOpportunity;


            system.debug('found test Basket and test Opportunity');
            testDefinition = new csclm__Document_Definition__c();
            testDefinition.Name = 'testDefinition';
            testDefinition.csclm__Linked_Object__c='Opportunity';
            insert testDefinition;
            testTemplate = new csclm__Document_Template__c();
            testTemplate.Name = 'Full EEBA Template';
            testTemplate.csclm__Active__c=true;
            testTemplate.csclm__Valid__c=true;
            testTemplate.csclm__Effective_From__c=System.today();

            testTemplate.csclm__Document_Definition__c=testDefinition.Id;
            insert testTemplate;  
            testSection = new csclm__Section_Definition__c();
            testSection.csclm__Section_Name__c = 'Part 2 - Services and Equipment';
            insert testSection;


            testAssoc= new csclm__Document_Template_Section_Association__c();
            testAssoc.csclm__Document_Template__c = testTemplate.Id;
            testAssoc.csclm__Section_Definition__c = testSection.Id;
            insert testAssoc;

            







            string templateId = testTemplate.Id;

            //string templateId = [select Id from csclm__Document_Template__c where Name ='Full EEBA Template'].Id;
            testAgreement = new csclm__Agreement__c(    Name='testClassAgreement',
                                                        csclm__Opportunity__c=testOpportunity.id,
                                                        csclm__Document_Template__c=templateId,
                                                        Products_data_populated__c=false
                                                        );
            insert testAgreement;

            testTransactionalDocument = new csclm__Transactional_Document__c();
            testTransactionalDocument.csclm__Agreement__c = testAgreement.Id;
            testTransactionalDocument.csclm__Document_Template__c = testTemplate.Id;
            insert testTransactionalDocument;

            testTransactionalSection = new csclm__Transactional_Section__c();
            testTransactionalSection.csclm__Section_Definition__c = testSection.Id;
            testTransactionalSection.csclm__Section_Name__c='Part 2 - Services and Equipment';
            testTransactionalSection.Name='Part 2 - Services and Equipment';
            testTransactionalSection.csclm__Transactional_Document__c=testTransactionalDocument.Id;

            insert testTransactionalSection;
            
            testAgreement.csclm__Latest_Transactional_Document__c = testTransactionalDocument.Id;
            UPDATE testAgreement;

            testTransactionalClause = new csclm__Transactional_Clause__c();
            testTransactionalClause.csclm__Applicable__c = true;
            testTransactionalClause.csclm__Sequence__c=1;
            testTransactionalClause.csclm__Transactional_Section__c=testTransactionalSection.Id;
            insert testTransactionalClause;


        }

        private static testMethod void testdoWork(){
         
            Test.StartTest(); 
            prepareTestData();
            PageReference pageRef = Page.CS_ContractSolution_newReview; // Add your VF page Name here
            pageRef.getParameters().put('id', testAgreement.Id);
            
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.StandardController(testAgreement);
            CSContractSol_newReviewController testController = new CSContractSol_newReviewController(sc);
            //testController.redirPage();
            testController.doWork();
            testController.insertTransactionalClause ('test');
            
            Test.StopTest();
        }
    }