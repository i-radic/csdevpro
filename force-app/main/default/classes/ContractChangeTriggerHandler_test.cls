/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class ContractChangeTriggerHandler_test {

	private static Opportunity mOppTest;
	private static Contact mContactTest;
	
	static{
		
        TriggerEnabler__c trgEnabler = EE_TestClass.createTriggerEnabler('Contract_Change__c', true);
        insert trgEnabler;
        system.assertNotEquals(trgEnabler.Id, null);
        
        User lUserTestAdmin = EE_TestClass.createStaticUser('John', 'Smith', 'john.smith@ee.co.uk', 'System Administrator', 'Dummy', false);
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        System.runAs ( thisUser ) {
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
            insert lUserTestAdmin;
        }
        system.assertNotEquals(lUserTestAdmin.Id, null);
        
        Account lAccountTest = EE_TestClass.createStaticAccount('Test Company', false);
        lAccountTest.OwnerId = lUserTestAdmin.Id;
        insert lAccountTest;
		system.assertNotEquals(lAccountTest.Id, null);

        Contact lContactTest 	= EE_TestClass.createStaticContact('John', 'Doe', null, false);
        lContactTest.AccountId 	= lAccountTest.Id;
        system.assert(lContactTest.AccountId != null, 'Check non null Account Id');
        insert lContactTest;
        system.assertNotEquals(lContactTest.Id, null);
        mContactTest = lContactTest;
        
		Opportunity lOppTest = new Opportunity();
		lOppTest.Name 		= 'Test Opportunity';
		//lOppTest.RecordType = [select id from RecordType where Name like 'Huthwaite' AND SobjectType = 'Opportunity' AND IsActive = true limit 1];
		lOppTest.StageName 	= 'Decision';
		lOppTest.ForecastCategoryName = 'Pipeline';  //Funnel
		lOppTest.CloseDate = date.today().addMonths(6);
		lOppTest.ownerid = lUserTestAdmin.Id;
      	insert lOppTest;
        system.assertNotEquals(lOppTest.Id, null);
        mOppTest = lOppTest;

	}

   /*
    * @name         testInsertNewDraftContractChangeNote
    * @description  test method for insert of Contract Change Draft for an Opportunity
    * @author       P Goodey
    * @date         Dec 2013
    * @see 			Work Request 00059997
    */
    static testmethod void testInsertNewDraftContractChangeNote() { 
    	
		Contract_Change__c lContractChangeTest = new Contract_Change__c();
		lContractChangeTest.RecordTypeId = [select id from RecordType where DeveloperName = 'Contract_Extension' AND SobjectType = 'Contract_Change__c' AND IsActive = true limit 1].Id;
		lContractChangeTest.Primary_Contact__c 	= mContactTest.Id;
		lContractChangeTest.Contact__c 			= mContactTest.Id;
		lContractChangeTest.Contract_Type__c 	= 'EE Business Agreement';
		lContractChangeTest.Contrcat_Extension_Months__c = '12';
		lContractChangeTest.Reason_for_Contract_Extension__c 		= 'Service';
		lContractChangeTest.Reason_for_Contract_Extension_Details__c = 'abcdef ghi jkl mno pqr';
		lContractChangeTest.Opportunity__c = mOppTest.Id;
		lContractChangeTest.Status__c	= 'Draft';

        Test.StartTest();
        insert lContractChangeTest;
        Test.StopTest();
        
        system.assertNotEquals(lContractChangeTest.Id, null);
        
    }

   /*
    * @name         testInsertNewSentToCustomerContractChangeNote
    * @description  test method for insert of Contract Change Sent To Customer for an Opportunity
    * @author       P Goodey
    * @date         Dec 2013
    * @see 			Work Request 00059997
    */
    static testmethod void testInsertNewSentToCustomerContractChangeNote() { 
    	
		Contract_Change__c lContractChangeTest = new Contract_Change__c();
		lContractChangeTest.RecordTypeId = [select id from RecordType where DeveloperName = 'Contract_Extension' AND SobjectType = 'Contract_Change__c' AND IsActive = true limit 1].Id;
		lContractChangeTest.Primary_Contact__c 	= mContactTest.Id;
		lContractChangeTest.Contact__c 			= mContactTest.Id;
		lContractChangeTest.Contract_Type__c 	= 'EE Business Agreement';
		lContractChangeTest.Contrcat_Extension_Months__c = '12';
		lContractChangeTest.Reason_for_Contract_Extension__c 		= 'Service';
		lContractChangeTest.Reason_for_Contract_Extension_Details__c = 'abcdef ghi jkl mno pqr';
		lContractChangeTest.Opportunity__c = mOppTest.Id;
		lContractChangeTest.Status__c	= 'Sent to Customer';

		// Try and insert the Contract Change
        try {
        	Test.startTest();
	        insert lContractChangeTest;
        } catch (Exception e) {
        	System.Assert(e.getMessage().contains('Cannot Create a new Contract Change at Status:'));
        } finally {
        	Test.stopTest();        	
        }	
        
    }

   /*
    * @name         testInsertNewDraftContractChangeNoteMultipleOnOpp
    * @description  test method for insert of Contract Change Draft for an Opportunity as a Multiple record against Opportunity
    * @author       P Goodey
    * @date         Dec 2013
    * @see 			Work Request 00059997
    */
    static testmethod void testInsertNewDraftContractChangeNoteMultipleOnOpp() { 
    	
		Contract_Change__c lContractChangeTest = new Contract_Change__c();
		lContractChangeTest.RecordTypeId = [select id from RecordType where DeveloperName = 'Contract_Extension' AND SobjectType = 'Contract_Change__c' AND IsActive = true limit 1].Id;
		lContractChangeTest.Primary_Contact__c 	= mContactTest.Id;
		lContractChangeTest.Contact__c 			= mContactTest.Id;
		lContractChangeTest.Contract_Type__c 	= 'EE Business Agreement';
		lContractChangeTest.Contrcat_Extension_Months__c = '12';
		lContractChangeTest.Reason_for_Contract_Extension__c 		= 'Service';
		lContractChangeTest.Reason_for_Contract_Extension_Details__c = 'abcdef ghi jkl mno pqr';
		lContractChangeTest.Opportunity__c = mOppTest.Id;
		lContractChangeTest.Status__c	= 'Draft';
        insert lContractChangeTest;
        system.assertNotEquals(lContractChangeTest.Id, null);
        
		Contract_Change__c lContractChangeTest_2 = new Contract_Change__c();
		lContractChangeTest_2.RecordTypeId = [select id from RecordType where DeveloperName = 'Contract_Extension' AND SobjectType = 'Contract_Change__c' AND IsActive = true limit 1].Id;
		lContractChangeTest_2.Primary_Contact__c 	= mContactTest.Id;
		lContractChangeTest_2.Contact__c 			= mContactTest.Id;
		lContractChangeTest_2.Contract_Type__c 	= 'EE Business Agreement';
		lContractChangeTest_2.Contrcat_Extension_Months__c = '12';
		lContractChangeTest_2.Reason_for_Contract_Extension__c 		= 'Service';
		lContractChangeTest_2.Reason_for_Contract_Extension_Details__c = 'abcdef ghi jkl mno pqr';
		lContractChangeTest_2.Opportunity__c = mOppTest.Id;
		lContractChangeTest_2.Status__c	= 'Draft';        
		// Try and insert the Multiple Contract Change
        try {
        	Test.startTest();
	        insert lContractChangeTest_2;
        } catch (Exception e) {
        	System.Assert(e.getMessage().contains('There is already a Contract Change for the Opportunity'));
        } finally {
        	Test.stopTest();        	
        }
        
        
    }


   /*
    * @name         testInsertNewDraftContractChangeNoteCancelledOnOpp
    * @description  test method for insert of Contract Change Draft for an Opportunity previous Cancelled
    * @author       P Goodey
    * @date         Dec 2013
    * @see 			Work Request 00059997
    */
    static testmethod void testInsertNewDraftContractChangeNoteCancelledOnOpp() { 
    	
		Contract_Change__c lContractChangeTest = new Contract_Change__c();
		lContractChangeTest.RecordTypeId = [select id from RecordType where DeveloperName = 'Contract_Extension' AND SobjectType = 'Contract_Change__c' AND IsActive = true limit 1].Id;
		lContractChangeTest.Primary_Contact__c 	= mContactTest.Id;
		lContractChangeTest.Contact__c 			= mContactTest.Id;
		lContractChangeTest.Contract_Type__c 	= 'EE Business Agreement';
		lContractChangeTest.Contrcat_Extension_Months__c = '12';
		lContractChangeTest.Reason_for_Contract_Extension__c 		= 'Service';
		lContractChangeTest.Reason_for_Contract_Extension_Details__c = 'abcdef ghi jkl mno pqr';
		lContractChangeTest.Opportunity__c = mOppTest.Id;
		lContractChangeTest.Status__c	= 'Draft';
        insert lContractChangeTest;
        system.assertNotEquals(lContractChangeTest.Id, null);
        
        ContractChangeTriggerHandler.cbIsFirstRun = true;
        Contract_Change__c lContractChangeTestUpdated = new Contract_Change__c();
        lContractChangeTestUpdated.Id 			= lContractChangeTest.Id;
        lContractChangeTestUpdated.Status__c 	= 'Cancelled / Declined';
        update lContractChangeTestUpdated;
        
        
		Contract_Change__c lContractChangeTest_2 = new Contract_Change__c();
		lContractChangeTest_2.RecordTypeId = [select id from RecordType where DeveloperName = 'Contract_Extension' AND SobjectType = 'Contract_Change__c' AND IsActive = true limit 1].Id;
		lContractChangeTest_2.Primary_Contact__c 	= mContactTest.Id;
		lContractChangeTest_2.Contact__c 			= mContactTest.Id;
		lContractChangeTest_2.Contract_Type__c 	= 'EE Business Agreement';
		lContractChangeTest_2.Contrcat_Extension_Months__c = '12';
		lContractChangeTest_2.Reason_for_Contract_Extension__c 		= 'Service';
		lContractChangeTest_2.Reason_for_Contract_Extension_Details__c = 'abcdef ghi jkl mno pqr';
		lContractChangeTest_2.Opportunity__c = mOppTest.Id;
		lContractChangeTest_2.Status__c	= 'Draft';        

        Test.StartTest();
        insert lContractChangeTest_2;
        Test.StopTest();
        
        system.assertNotEquals(lContractChangeTest_2.Id, null);
        
        
    }
    
    
    
}