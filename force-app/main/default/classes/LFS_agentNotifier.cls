public class LFS_agentNotifier
{
    private Opportunity oppty {get; set;}
    private List<Product2> purchasedProds = new List<Product2>();
    private List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
    private List<Opportunity_Submitted_Order__c> oppSubOrders = new List<Opportunity_Submitted_Order__c>();
    private List<Note> oppNotes = new List<Note>();
    Set<String> prodIds = new Set<String>();
    public String oppyId {get; set;}
    public String mailBody {get; set;}
    public User oOwner {get; set;}
    Map<Id, String> mProdLFS = new Map<Id, String>();
    
    public LFS_agentNotifier() {
        oppyId = '';        
        mailBody = '';
    }
        
    public void getNotificationText()
    {
        String mHead = '';
        String mBobyExtra = '';
        String pCategory = '';      
        String PIds = '';
        String ONotes = '';
        String TRev = '';
        Double TotValue = 0;
        
        if (oppyId != null){               
            
            oppty = [Select RecordTypeId, Stage_Reason__c,Sales_Stage_Detail__c, Product__c, Owner__c, Owner_Dept__c, Owner_Channel__c, Opportunity_Id__c, Name, 
                    Lead_Information__c, CreatedDate, Amount, Sub_Category__c, Mobile_product__c, Switch_Size__c, StageName, Description, Customer_Type__c, SOV_nonSub__c,
                    (Select Order_Reference__c From Opportunity_Submitted_Orders__r), 
                    (Select Quantity, UnitPrice, ListPrice, ProductName__c, LFS_Value__c, TotalPrice From OpportunityLineItems),
                    (Select Id, Title, Body From Notes)
                    From Opportunity where Opportunity_Id__c = :oppyId];    //ODI-00291467
                    
            oOwner = [Select Name From User where id = :oppty.Owner__c];  
            
             //Code added by Raj for LFS Oppty proj            
             Map<Id,RecordType > RCNames=new Map<Id,RecordType >([Select Id,Name From RecordType where SOBJECTTYPE='Opportunity']);                        
             String RCName=RCNames.get(oppty.RecordTypeId).Name;                 
             String DispRCName;              
             
             If(RCName=='Leads from Service')               
             DispRCName='Leads 2 Service';            
             Else              
             DispRCName=RCName;                        
             //End of the code
     
            oppLineItems = oppty.OpportunityLineItems;
            oppSubOrders = oppty.Opportunity_Submitted_Orders__r;
            oppNotes = oppty.Notes;
            
            //get purchased Products
            for(OpportunityLineItem oli: oppLineItems){
                prodIds.add(oli.ProductName__c);
                TotValue = TotValue + oli.LFS_Value__c;                
                mProdLFS.put(oli.ProductName__c,String.ValueOf(oli.LFS_Value__c)); //for ACV revenue
                //mProdLFS.put(oli.ProductName__c,String.ValueOf(oli.TotalPrice)); //For SOV revenue
                TRev += oli.LFS_Value__c + ', ';
            }        
            for(Product2 ppds: [Select Id, Name, Family From Product2 where Id IN : prodIds]){
               PIds += ', ' + ppds.Name + ' - £' + mProdLFS.get(ppds.Id);
            }
            PIds = PIds.replaceFirst(', ', '');
            
            PIds += '----Total Revenue = £' + String.ValueOf(TotValue); //Displays ACV revenue
            //PIds += '----Total Revenue = £' + oppty.SOV_nonSub__c; // Displays SOV 
            String ORefs = '';
            for(Opportunity_Submitted_Order__c sods: oppSubOrders){
               ORefs += ', ' + sods.Order_Reference__c ;
            }
            ORefs = ORefs.replaceFirst(', ', '');
            //--- Amended for CR - SF3902 - Developed By Krupakar---------
            if(oppty.Customer_Type__c == 'BT Business' || oppty.Customer_Type__c == 'SME')  {            
                for(Note opNote: oppNotes){
                   ONotes += opNote.Title + ': '+ opNote.Body + '<br />';
                }
            }
            else {
                ONotes = oppty.Description;
            }
            //--- Amended for CR - SF3902 - Developed By Krupakar---------
            // Amended for CR - SF3902
            /*for(Note opNote: oppNotes){
                   ONotes += opNote.Title + ': '+ opNote.Body + '<br />';
            }*/
            
            if(oppty.Sub_Category__c != null){ 
                pCategory = '<br />Category : '+ oppty.Sub_Category__c; 
            } 
            else if(oppty.Mobile_product__c != null){
                pCategory = '<br />Category : '+ oppty.Mobile_product__c;
            }
            else if(oppty.Switch_Size__c != null){
                pCategory = '<br />Category : '+ oppty.Switch_Size__c;
            }
             
            if(oppty != null){
                system.debug('StageName: '+oppty.StageName);
                if(oppty.StageName == 'Created'){
                    //Code added by Raj for LFS Oppty proj                    
                    mHead = 'This confirms that your ';                    
                    mHead+= DispRCName;                    
                    mHead+=' Opportunity has been created and submitted<br />';                    
                    //end of the code
                    //mHead = 'This confirms that your ‘Leads 2 Sales’ Opportunity has been created and submitted<br />';                                 
                }                
                else if(oppty.StageName == 'Won'){
                    //Code added by Raj for LFS Oppty proj                    
                    mHead = 'This confirms that your ';                    
                    mHead+= DispRCName;                    
                    mHead+=' Opportunity has been closed<br />';                    
                    //end of the code
                    //mHead = 'This confirms that your ‘Leads 2 Sales’ Opportunity has been successfully completed <br />';
                    mBobyExtra = '<br />Stage Name : '+oppty.StageName;
                    if(oppty.Sales_Stage_Detail__c!=null) {
                        mBobyExtra += '<br />Sales Stage Details : '+oppty.Sales_Stage_Detail__c;
                    } else {
                    mBobyExtra += '<br />Sales Stage Details : ';
                    }
                    
                    if(oppty.Stage_Reason__c!=null) {
                        mBobyExtra += '<br />Stage Reason : '+oppty.Stage_Reason__c;
                    } else {
                        mBobyExtra += '<br />Stage Reason : ';
                    }
                    
                    mBobyExtra += '<br />Order Reference : '+ ORefs;                 
                    mBobyExtra += '<br /><br />Product(s) : '+ PIds;
                    //mBobyExtra += '<br />Total revenue : '+ oppty.Amount;  
                    //mBobyExtra += '<br />Total revenue : '+ TRev;
                    mBobyExtra += '<br /><br />Lead Info Notes:<br />'+ oppty.Lead_Information__c; 
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;                   
                }
                else if(oppty.StageName == 'Lost'){
                    //Code added by Raj for LFS Oppty proj                    
                    mHead = 'This confirms that your ';                    
                    mHead+= DispRCName;                    
                    mHead+=' Opportunity has been closed<br />';                    
                    //end of the code
                    //mHead = 'This confirms that your ‘Leads 2 Sales’ Opportunity has not been successful<br />';
                    mBobyExtra = '<br />Stage Name : '+oppty.StageName;
                   if(oppty.Sales_Stage_Detail__c!=null) {
                        mBobyExtra += '<br />Sales Stage Details : '+oppty.Sales_Stage_Detail__c;
                    } else {
                    mBobyExtra += '<br />Sales Stage Details : ';
                    }
                    
                    if(oppty.Stage_Reason__c!=null) {
                        mBobyExtra += '<br />Stage Reason : '+oppty.Stage_Reason__c;
                    } else {
                        mBobyExtra += '<br />Stage Reason : ';
                    }
                    if(RCName!='Leads from Service')
                    {
                        mBobyExtra += '<br /><br />Product(s) : '+ PIds;
                    }
                    mBobyExtra += '<br /><br />Lead Info Notes:<br />'+ oppty.Lead_Information__c;
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;
                }
                else if(oppty.StageName == 'Cancelled'){
                    //Code added by Raj for LFS Oppty proj                    
                    mHead = 'This confirms that your ';                    
                    mHead+= DispRCName;                    
                    mHead+=' Opportunity has been closed<br />';                    
                    //end of the code
                    //mHead = 'This confirms that your ‘Leads 2 Sales’ Opportunity has been cancelled.<br />';
                    mBobyExtra = '<br />Stage Name : '+oppty.StageName;
                   if(oppty.Sales_Stage_Detail__c!=null) {
                        mBobyExtra += '<br />Sales Stage Details : '+oppty.Sales_Stage_Detail__c;
                    } else {
                    mBobyExtra += '<br />Sales Stage Details : ';
                    }
                    
                    if(oppty.Stage_Reason__c!=null) {
                        mBobyExtra += '<br />Stage Reason : '+oppty.Stage_Reason__c;
                    } else {
                        mBobyExtra += '<br />Stage Reason : ';
                    }
                    if(RCName!='Leads from Service')
                    {
                        mBobyExtra += '<br /><br />Product(s) : '+ PIds;
                    }
                    mBobyExtra += '<br /><br />Lead Info Notes:<br />'+ oppty.Lead_Information__c;
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;       
                } 
                else{ 
                    //Code added by Raj for LFS Oppty proj                    
                    mHead = 'The status of your ';                    
                    mHead+=DispRCName;                    
                    mHead+=' Opportunity has changed to :'+ oppty.StageName +'<br />';                    
                    //end of the code
                    //mHead = 'The status of your ‘Leads 2 Sales’ Opportunity has changed to :'+ oppty.StageName +'<br />';
                    if(oppty.Sales_Stage_Detail__c != null){
                        mBobyExtra = '<br />Sales Stage Details : '+oppty.Sales_Stage_Detail__c;
                    } 
                    mBobyExtra += '<br /><br />Lead Info Notes:<br />'+ oppty.Lead_Information__c;  
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;
                }
                         
                
                mailBody += mHead;
                mailBody += '<br />Opportunity Name : '+ oppty.Name; 
                mailBody += '<br />Opportunity ID : '+ oppty.Opportunity_Id__c;
                
                //mailBody += '<br />Product : '+ oppty.Product__c;
                
                mailBody += pCategory;             
                mailBody += '<br />Created Date : '+ oppty.CreatedDate;
                mailBody += '<br />Opportunity Owner : '+  oOwner.Name;
                if(oppty.Owner_Dept__c!=null) {
                    mailBody += '<br />Owner Department : '+ oppty.Owner_Dept__c;
                } else {
                    mailBody += '<br />Owner Department : ';
                }
                mailBody += mBobyExtra;
                //mailBody+='<br /><br />To view this record please click on the following URL:<br />'+ 'https://emea.salesforce.com/'+ oppty.Id; 
                
                mailBody += '<br /><br /><br />Thank you';
                mailBody += '<br /><br /><font color="#ff0000" size="1">Note: This email was sent from a notification-only email address that cannot accept incoming email. Please do not reply to this message</font>';
            }
        }
        
    }   

    /*--------------------TEST METHOD------------------------*/
  /*static testMethod void LFS_agentNotifierTest(){
        
        
        LFS_agentNotifier getOpties = new LFS_agentNotifier();
        getOpties.oppyId = 'OID-00697090';
        getOpties.getNotificationText();
        String strBody = getOpties.mailBody;
        //system.debug('mail: '+mail);
        
    } */
}