@isTest
private class Test_LEMReviewImprovementPlanList {
    static testMethod void myUnitTest() {
            
        Profile p = [select id from profile where name='System Administrator'];
        
        User uM = new User();
        uM.Username = '999999000@bt.com';
        uM.Ein__c = '999999000';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.Department='TestSupport';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = p.Id;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});
                                
        BTLB_Master__c BM = new BTLB_Master__c();
        BM.Name='TestSupport';
        BM.BTLB_Name_ExtLink__c='TestSupport';
        BM.Email_Signature__c='Regards,Test Support Team';
        BM.Company_Name__c='Test Company';
        BM.Company_Registration_No__c='12345';
        BM.Company_Registered_Address__c='Address';
        insert BM;  
        
        LEM_Review__c lr = new LEM_Review__c();
        lr.iNET_FY_Target__c = 770943;
        lr.WLR_Lines_FY_Target__c = 717;
        lr.Switch_Volume_FY_Target__c = 311;
        lr.Switch_Value_FY_Target__c =1143661;
        lr.RD_Name__c = 'Chris Freeney';
        lr.PSM_Name__c = 'Malcolm Davey';
        lr.OwnerId =  uMResult[0].Id;
        lr.Name = 'BTLB Bath and Bristol';
        lr.Mobile_FY_Target__c = 689;
        lr.MD_Name__c = 'Chris Freeney';
        lr.Financial_Year__c = '2012/13';
        lr.Data_Networks_FY_Target__c = 2469525;
        lr.Calls_FY_Target__c = 812781;
        lr.Broadband_FY_Target__c = 660;
        lr.BTLB_Master_Object__c = BM.Id;
        insert lr;
        
        LEM_Review_Improvement_Plan__c lrip = new LEM_Review_Improvement_Plan__c();
        lrip.Targeted_Paid_Correctly__c = 'testing Targeted & Paid Correctly';
        lrip.Status__c = 'Open';
        lrip.Reason_For_Improvement_Plan__c = 'testing Reason For Improvement Plan';
        lrip.Prospect_Bank__c = 'testing Prospect Bank';
        lrip.PSM_Specialist_Learning__c = 'testing PSM/Specialist Learning';
        lrip.Number_Of_People_Selling__c = 'testing Number Of People Selling';
        lrip.MD_Learning__c = 'testing MD Learning';
        lrip.LEM_Review__c = lr.Id;
        lrip.Improvement_Plan_Start_Date__c = date.today();
        lrip.Improvement_Plan_End_Date__c = date.today();
        lrip.Improvement_Area__c = 'Broadband';
        lrip.Forecast_At_Next_Review__c = 'testing Forecast At Next Review';
        lrip.Closing_Outcome__c = 'Performance At Required Level';
        lrip.Barriers_To_Sale__c =  'testing Barriers To Sale';
        lrip.Activity_Plan__c = 'testing activity'; 
        insert lrip;
        
        LEM_Review_Improvement_Plan__c lrip2 = new LEM_Review_Improvement_Plan__c();
        lrip2.Targeted_Paid_Correctly__c = 'testing Targeted & Paid Correctly';
        lrip2.Status__c = 'Open';
        lrip2.Reason_For_Improvement_Plan__c = 'testing Reason For Improvement Plan';
        lrip2.Prospect_Bank__c = 'testing Prospect Bank';
        lrip2.PSM_Specialist_Learning__c = 'testing PSM/Specialist Learning';
        lrip2.Number_Of_People_Selling__c = 'testing Number Of People Selling';
        lrip2.MD_Learning__c = 'testing MD Learning';
        lrip2.LEM_Review__c = lr.Id;
        lrip2.Improvement_Plan_Start_Date__c = date.today();
        lrip2.Improvement_Plan_End_Date__c = date.today();
        lrip2.Improvement_Area__c = 'Switch Volume';
        lrip2.Forecast_At_Next_Review__c = 'testing Forecast At Next Review';
        lrip2.Closing_Outcome__c = 'Performance At Required Level';
        lrip2.Barriers_To_Sale__c =  'testing Barriers To Sale';
        lrip2.Activity_Plan__c = 'testing activity'; 
        insert lrip2;
        
        Test.startTest();
        PageReference pageRef = new PageReference('apex/LEMReviewImprovementPlanList?id=' + lr.Id);
        Test.setCurrentPageReference(pageRef);
        ApexPages.Standardcontroller sc = New ApexPages.StandardController(lr); 
        LEMReviewImprovementPlanList objList = new LEMReviewImprovementPlanList(sc);
        List<LEM_Review_Improvement_Plan__c> myList = objList.getLEMRIPlans();
        objList.sortExpression = 'Status__c';
        objList.ViewData();
        objList.setSortDirection('DESC');
        objList.getSortDirection();
        objList.ViewData();
        Id myId = objList.getLrId();
        String MyName = objList.getLrName();
        objList.sortExpression = '';
        String MyDir = objList.getSortDirection();
        Test.stopTest();
    }
}