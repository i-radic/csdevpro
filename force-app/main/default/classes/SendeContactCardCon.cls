//M&Ps eContactCard Component Class
public class SendeContactCardCon{
    public id accountId{get;set;}
    public String GMEmail {get;set;}
    public String GMName {get;set;} 
    public String GMPhone {get;set;} 
    set<string> LLUserRoles;
    public string BillingSupportEmail {get;set;}
    public string ServiceMgmtCenterEmail {get;set;}
    public string BillingSupportPhone {get;set;}
    public string ServiceMgmtCenterPhone {get;set;}
    
    public SendeContactCardCon(){
         List<eContactCardRoles__c> listofLLRoles = eContactCardRoles__c.getAll().values();
         LLUserRoles = new set<string>();
         for(eContactCardRoles__c LLRole:listofLLRoles ){
            LLUserRoles.add(LLRole.Name); 
             if(LLRole.Name == 'Billing Support Specialist'){
             	BillingSupportEmail = LLRole.Email__c;
                BillingSupportPhone = LLRole.Phone__c; 
             }if(LLRole.Name == 'Service Management Centre'){
             	ServiceMgmtCenterEmail = LLRole.Email__c;
                ServiceMgmtCenterPhone = LLRole.Phone__c; 
             }
         }
        
    }
     public List<Lead_Locator__c> getLeadLoator()
    {                  
        List<Lead_Locator__c> LeadLoators= new List<Lead_Locator__c>(); 
        system.debug('#####'+accountId+'@@@@'+LLUserRoles);
        LeadLoators = [SELECT Ordering__c,user__r.Name, user__r.email,user__r.Phone,Person_Role__c,Account__r.General_Manager__r.Name,
                       Account__r.General_Manager__r.Phone, User__r.EIN__c,Account__r.General_Manager__r.Email,Account__r.General_Manager__c,User_Name__c,User_Email__c FROM Lead_Locator__c 
                       WHERE Account__r.id =: accountId AND Person_Role__c IN :LLUserRoles Order by Ordering__c];
        if(LeadLoators.size()==0){
            List<Account> Acc = [select id,General_Manager__r.Name,General_Manager__r.phone,General_Manager__r.Email from Account
                           Where Id =: accountId];
            if(Acc.size()>0){
                GMName = Acc[0].General_Manager__r.Name; 
                GMPhone = Acc[0].General_Manager__r.Phone;
                GMEmail = Acc[0].General_Manager__r.Email;   
            }                     
        }else if(LeadLoators.size()>0){
            if(LeadLoators[0].Account__r.General_Manager__c != Null){
                GMName = LeadLoators[0].Account__r.General_Manager__r.Name; 
                GMPhone = LeadLoators[0].Account__r.General_Manager__r.Phone;
                GMEmail = LeadLoators[0].Account__r.General_Manager__r.Email;        
            }
        }      
        system.debug('@@@@'+LeadLoators);    
        return LeadLoators ;
    }
}