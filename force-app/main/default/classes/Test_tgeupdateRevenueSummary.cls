@isTest(seeAllData=TRUE)
private class Test_tgeupdateRevenueSummary {

    static testMethod void UnitTest() {
    	Test_Factory.setProperty('IsTest', 'yes');
        
        insert Test_Factory.CreateAccountForDummy();
        Account account = Test_Factory.CreateAccount();        
        Database.SaveResult accountResult = Database.insert(account);

        //Pricebook2 priceBook = [select Id from Pricebook2 where isStandard=true limit 1];
        
        Opportunity opp1 = Test_Factory.CreateOpportunity(accountResult.getid());
        opp1.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(opp1);
        
        Product2 opplineprod = Test_Factory.CreateProduct();
        Database.SaveResult opplineprodResult = Database.insert(opplineprod);
        Pricebook2 stdPb = [select Id from Pricebook2 where isStandard=true limit 1] ;// standard pricebook
        PricebookEntry opplinepb = Test_Factory.CreatePricebookEntry(stdPb.Id, opplineprod.Id);
        Database.SaveResult opplinepbResult = Database.insert(opplinepb);
       
        Product_Revenue__c Revenue = new Product_Revenue__c();
        Revenue.Opportunity__c = opptResult.id ;
        Revenue.Product__c = opplineprod.id ;
        Insert Revenue;
        
        Revenue_Summary__c RevenueSummary = new Revenue_Summary__c();
        RevenueSummary.Opportunity__c = opptResult.id;
        Insert RevenueSummary;
        
        
       
        Update Revenue;
        
    }
}