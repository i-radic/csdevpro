/******************************************************************************************************
Test Class: ProfilerQuestionAnswersTestClass
Class:ProfilerQuestionAnswers
Method:QuestionnaireInsertTest
Description : Test class for ProfilerQuestionAnswers with req no 315.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    1/12/2015             Revan Bhadange                           class created          
*********************************************************************************************************/
@IsTest(SeeAllData=false)
public class ProfilerQuestionAnswersTestClass {
    static testMethod void QuestionnaireInsertTest()
    {
		User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     	}
       Id userProfileId = userinfo.getProfileId();

       User u = new User(Alias = 'standt', Email='standarduser111@bt.it',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = userProfileId,TimeZoneSidKey='America/Los_Angeles', UserName='BTstandarduser111@bt.it',
         EIN__c = '124798598');
        insert u;
        System.runAs(u)
        {

        test.starttest ();

             
        
        Account accnt =  new Account(name='test',AccountNumber='1234');
        insert accnt;
        
       
      
     
        RecordType RecTyp = [SELECT Id FROM RecordType WHERE DeveloperName =: GlobalConstants.OPPORTUNITY_NONSTANDARD_MCC_RECORDTYPE];  
        Opportunity opp = new Opportunity(AccountId=accnt.Id, Name='opportunityName',RecordTypeId= RecTyp.Id, StageName='Pre-Qualification', CloseDate=Date.today() );
        insert opp;
        
        RecordType QuestionaireQueRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireQuestion__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_QUESTIONNAIRE_PROFILER_RECORDTYPE];
        QuestionnaireQuestion__c objQuQue = new QuestionnaireQuestion__c ();
        objQuQue.Question__c = 'Test1';
        objQuQue.Category__c = 'TestCat';
        objQuQue.RecordTypeId = QuestionaireQueRecType.Id;
        objQuQue.Opportunity_Gates__c = 'Make Pursuit';
        objQuQue.Topic_Area__c='Test Topic Area';
        objQuQue.Weighting__c = 12;
        insert objQuQue;
        
        RecordType QuestionaireQueAnsRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireAnswer__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_ANSWER_PROFILER_RECORDTYPE];
        QuestionnaireAnswer__c objQuQA = new QuestionnaireAnswer__c ();
        objQuQA.Questionnaire_Question__c = objQuQue.Id;
        objQuQA.RecordTypeId = QuestionaireQueAnsRecType.Id;
        objQuQA.Answer__c='Yes';
        objQuQA.Score__c = 100;
        insert objQuQA ;
        
        ApexPages.currentPage().getParameters().put('opp', opp.id);
        ProfilerQuestionAnswers objCls = new ProfilerQuestionAnswers ();
        
        objCls.submitMethod();
        RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE];
        Questionnaire__c objQustion = new Questionnaire__c ();
        objQustion.Opportunity__c = opp.Id; 
        objQustion.RecordTypeId = QuestionaireRecType.Id;
        insert objQustion;
        objCls.submitMethod(); 
        objCls.canclebutton();
        objCls.close();
        
        test.stopTest();  
        }
        }
        
    
/******************************************************************************************************
Test Class: ProfilerQuestionAnswersTestClass
Class:ProfilerQuestionAnswersedit
Method:QuestionnaireInsertTest
Description : Test class for ProfilerQuestionAnswersedit with req no 315.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    2/12/2015             Revan Bhadange                           class created          
*********************************************************************************************************/
    static testMethod void QuestionnaireEditTest()
    {		
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }

       
           Id userProfileId = userinfo.getProfileId();

            User u = new User(Alias = 'standt', Email='standarduser111@bt.it',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = userProfileId,TimeZoneSidKey='America/Los_Angeles', UserName='BTstandarduser111@bt.it',
         EIN__c='12305678');
        insert u;

        System.runAs(u)
        {
        test.starttest ();

         
            
        Account accnt =  new Account(name='test',AccountNumber='1234');
        insert accnt;
        
      
        RecordType RecTyp = [SELECT Id FROM RecordType WHERE DeveloperName =: GlobalConstants.OPPORTUNITY_NONSTANDARD_MCC_RECORDTYPE];  
        Opportunity opp = new Opportunity(AccountId=accnt.Id, Name='opportunityName',RecordTypeId= RecTyp.Id, StageName='Pre-Qualification', CloseDate=Date.today());
        insert opp;
        
        RecordType QuestionaireQueRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireQuestion__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_QUESTIONNAIRE_PROFILER_RECORDTYPE];
        QuestionnaireQuestion__c objQuQue = new QuestionnaireQuestion__c ();
        objQuQue.Question__c = 'Test1';
        objQuQue.Category__c = 'TestCat';
        objQuQue.RecordTypeId = QuestionaireQueRecType.Id;
        objQuQue.Opportunity_Gates__c = 'Make Pursuit';
        objQuQue.Topic_Area__c='Test Topic Area';
        objQuQue.Weighting__c = 12;
        insert objQuQue;
        
        RecordType QuestionaireQueAnsRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireAnswer__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_ANSWER_PROFILER_RECORDTYPE];
        QuestionnaireAnswer__c objQuQA = new QuestionnaireAnswer__c ();
        objQuQA.Questionnaire_Question__c = objQuQue.Id;
        objQuQA.RecordTypeId = QuestionaireQueAnsRecType.Id;
        objQuQA.Answer__c='Yes';
        objQuQA.Score__c = 100;
        insert objQuQA ;
        RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE];
        Questionnaire__c objQustion = new Questionnaire__c ();
        objQustion.Opportunity__c = opp.Id; 
        objQustion.RecordTypeId = QuestionaireRecType.Id;
        insert objQustion;
        
        RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONNAIRE_REPLY_PROFILER_RECORDTYPE];
        QuestionnaireReply__c objQuestionReply = new QuestionnaireReply__c ();
        objQuestionReply.Answer__c = objQuQA.Id;
        objQuestionReply.AnswerValue__c = 'Yes';
        objQuestionReply.Question__c = objQuQue.Id;
        objQuestionReply.Questionnaire__c = objQustion.Id;
        objQuestionReply.RecordTypeId = QuestionaireReplyRecType.Id;
        insert objQuestionReply;
        
        ApexPages.currentPage().getParameters().put('qt', objQustion.id);
        ProfilerQuestionAnswersedit objCls = new ProfilerQuestionAnswersedit ();
        objCls.submitMethod();
      
        objCls.canclebutton();
        objCls.close();
        test.stopTest();  
        }
    }
    
    /******************************************************************************************************
Test Class: ProfilerQuestionAnswersTestClass
Class:ProfilerQuestionAnswersedit
Method:QuestionnaireViewTest
Description : Test class for ProfilerQuestionAnswersview with req no 315.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    2/12/2015             Revan Bhadange                           class created          
*********************************************************************************************************/
    static testMethod void QuestionnaireViewTest()
    {		
        	User thisUser = [select id from User where id=:userinfo.getUserid()];
             System.runAs( thisUser ){
                 
                 TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
                 settings.Account__c = FALSE;
                 settings.Contact__c = FALSE;
                 settings.Opportunity__c = FALSE;
                 settings.OpportunitylineItem__c = FALSE;
                 settings.Task__c = FALSE;
                 settings.Event__c = FALSE;
                 
                 upsert settings TriggerDeactivating__c.Id;
             }

           Id userProfileId = userinfo.getProfileId();

           User u = new User(Alias = 'standt', Email='standarduser111@bt.it',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = userProfileId,TimeZoneSidKey='America/Los_Angeles', UserName='BTstandarduser111@bt.it'
                             , EIN__c = '124798598');
        insert u;

        System.runAs(u)
        {
        test.starttest ();

       

        Account accnt =  new Account(name='test',AccountNumber='1234');
        insert accnt;
        
        
        
        RecordType RecTyp = [SELECT Id FROM RecordType WHERE DeveloperName =: GlobalConstants.OPPORTUNITY_NONSTANDARD_MCC_RECORDTYPE];  
        Opportunity opp = new Opportunity(AccountId=accnt.Id, Name='opportunityName',RecordTypeId= RecTyp.Id, StageName='Pre-Qualification', CloseDate=Date.today());
        insert opp;
        
        RecordType QuestionaireQueRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireQuestion__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_QUESTIONNAIRE_PROFILER_RECORDTYPE];
        QuestionnaireQuestion__c objQuQue = new QuestionnaireQuestion__c ();
        objQuQue.Question__c = 'Test1';
        objQuQue.Category__c = 'TestCat';
        objQuQue.RecordTypeId = QuestionaireQueRecType.Id;
        objQuQue.Opportunity_Gates__c = 'Make Pursuit';
        objQuQue.Topic_Area__c='Test Topic Area';
        objQuQue.Weighting__c = 12;
        insert objQuQue;
        
        RecordType QuestionaireQueAnsRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireAnswer__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_ANSWER_PROFILER_RECORDTYPE];
        QuestionnaireAnswer__c objQuQA = new QuestionnaireAnswer__c ();
        objQuQA.Questionnaire_Question__c = objQuQue.Id;
        objQuQA.RecordTypeId = QuestionaireQueAnsRecType.Id;
        objQuQA.Answer__c='Yes';
        objQuQA.Score__c = 100;
        insert objQuQA ;
        RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE];
        Questionnaire__c objQustion = new Questionnaire__c ();
        objQustion.Opportunity__c = opp.Id; 
        objQustion.RecordTypeId = QuestionaireRecType.Id;
        insert objQustion;
        
        RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONNAIRE_REPLY_PROFILER_RECORDTYPE];
        QuestionnaireReply__c objQuestionReply = new QuestionnaireReply__c ();
        objQuestionReply.Answer__c = objQuQA.Id;
        objQuestionReply.AnswerValue__c = 'Yes';
        objQuestionReply.Question__c = objQuQue.Id;
        objQuestionReply.Questionnaire__c = objQustion.Id;
        objQuestionReply.RecordTypeId = QuestionaireReplyRecType.Id;
        insert objQuestionReply;
        
        ApexPages.currentPage().getParameters().put('qt', objQustion.id);
        ProfilerQuestionAnswersview  objCls = new ProfilerQuestionAnswersview ();
        objCls.edit();
        test.stopTest();  
        }
    }             
}