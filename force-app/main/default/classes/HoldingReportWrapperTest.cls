@isTest
public class HoldingReportWrapperTest{
    
    @isTest
    public static void testController(){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Test.startTest();
        
        HoldingReportWrapper controller = new HoldingReportWrapper();
        controller = new HoldingReportWrapper(',AUDIE,357514,2175985,FLCP01,,Adrian Lewis,07760765236,09/07/2015,27,26/11/2013,EE,Okay');
        
        controller.calculateCed();
        controller.calculateCedDays();
        controller.calculateTermMTHS();
        
        Test.stopTest();
        
    }
}