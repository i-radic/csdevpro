@isTest
private class Test_ipsRecruitmentController {
    static testMethod void myUnitTest() {
        ipsRecruitmentController ips=new ipsRecruitmentController();        
        IPS_Recruitment__c ipsobj=ips.getIPSRecruitment();
        //ips.info();
        ips.step1();
        ips.step2();
        ips.step3();
        ips.step4();
        ips.step5();
        ips.save();  
        ipsobj.Company_Name__c = 'Rita Test Company';
        ipsobj.Trading_Name__c = 'Rita trading company name';
        ipsobj.Company_Registered_Address__c = '1 Bat Cave';
        ipsobj.Post_Code__c ='n15 8jy';
        ipsobj.Trading_Address_if_different__c = 'Alternative trading address';
        ipsobj.Post_Code_for_different_address__c = 'n8 4ft';
        ipsobj.Telephone_No__c ='02088000860';
        ipsobj.Website__c = 'www.test.com';
        ipsobj.Contact_Person__c = 'bat man';
        ipsobj.Contact_Number__c ='02088000860';
        ipsobj.Position__c ='ceo';
        ipsobj.Email_Address__c='rita.opokuserebuoh@bt.com';
        ipsobj.Turnover__c =10000.90;
        ipsobj.Company_Registration_Date__c = date.today();
        ipsobj.VAT_Registration_No__c = string.valueof(1234);
        ipsobj.Company_Registration_No__c = string.valueof(1234);            
        ipsobj.Core_Business_Activities__c = 'fishbait';
        ipsobj.Number_of_People_Employed__c ='1000+';
        ipsobj.Geographical_Coverage__c = 'midlands';
        ipsobj.Customer_Base_Size__c = 500;
        // insert ipsobj;
        ips.save();
        
        ipsobj.VAT_Registration_No__c = 'abcd'; 
        ips.validateCompRegDate(); 
        ipsobj.Company_Registration_Date__c = date.today()+1; 
        ips.validateCompRegDate();
        ipsobj.Company_Registration_No__c = 'companyregnum';
        ips.validateCompRegDate();        
        
        ipsobj.VAT_Registration_No__c = '12345';
        ips.validateCompRegDate();
        ipsobj.Company_Registration_Date__c = date.today()-1;
        ips.validateCompRegDate();
        ipsobj.Company_Registration_No__c = '123456';
        ips.validateCompRegDate();        
        
        ips.validateSubmissionReasons(); 
        ipsobj.Internet_search__c = TRUE; 
        ipsobj.BT_Contact__c = TRUE;
        ipsobj.BT_Com__c = TRUE;
        ipsobj.BT_IPS_Campaign__c = TRUE;
        ipsobj.Channel_Press__c = TRUE;
        ipsobj.Personal_Contacts__c = TRUE;
        ipsobj.Other_please_state__c = TRUE;          
        ips.validateSubmissionReasons();
        ipsobj.VAT_Registration_No__c = '1234'; 
        ipsobj.Company_Registration_Date__c = date.today()-1; 
        ips.validateCompRegDate(); 
        ips.cancel();
    }
}