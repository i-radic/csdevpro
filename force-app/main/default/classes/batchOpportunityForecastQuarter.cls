global class batchOpportunityForecastQuarter implements Database.Batchable<SObject>{
  private String query;

     global batchOpportunityForecastQuarter (String q){
      this.query = q;
    }
     global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
     global void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('JB batchOpportunityForecastQuarter Execute method size:' + scope.size());
        List<Opportunity> opps = new List<Opportunity>();
        for(Sobject c : scope){
          Opportunity opp = (Opportunity)c;
            system.debug('jb opportunity:' + opp);
            // copy this Quarters figures to last Quarters
        //opp.zLast_Quarter_Forecast_ACV__c = opp.zCurrent_Quarter_Forecast_ACV__c;
          opp.zLast_Quarter_Forecast_ACV__c = opp.Net_ACV__c;
        //opp.zLast_Quarter_Forecast_CY_NIBR__c = opp.zCurrent_Quarter_Forecast_CY_NIBR__c;
          opp.zLast_Quarter_Forecast_CY_NIBR__c = opp.SOV_GM__c;
          opp.zLast_Quarter_Forecast_NIBR__c = opp.zCurrent_Quarter_Forecast_NIBR__c;
          opp.zLast_Quarter_Forecast_Vol__c = opp.zCurrent_Quarter_Forecast_Vol__c;
          opp.Last_Quarter_Forecast_Amount__c = opp.Current_Quarter_Forecast_Amount__c;
            
            if(opp.Close_Date_Fiscal_Quarter__c == opp.zCurrent_Fiscal_Quarter__c) {
              //opp.zCurrent_Quarter_Forecast_ACV__c = opp.ACV_Calc__c; 
                opp.zCurrent_Quarter_Forecast_ACV__c = opp.Net_ACV__c;              
                opp.zCurrent_Quarter_Forecast_NIBR__c = opp.NIBR_Next_Year__c;
                opp.zCurrent_Quarter_Forecast_Vol__c = 1;
              //opp.zCurrent_Quarter_Forecast_CY_NIBR__c = opp.NIBR_Current_Year__c;
                opp.zCurrent_Quarter_Forecast_CY_NIBR__c = opp.SOV_GM__c;
                opp.Current_Quarter_Forecast_Amount__c = opp.Amount;
                opp.ForecastUpdatedQuarter__c = Date.today();
            }else {
                opp.zCurrent_Quarter_Forecast_ACV__c = 0;                       
                opp.zCurrent_Quarter_Forecast_NIBR__c = 0;
                opp.zCurrent_Quarter_Forecast_Vol__c = 0;
                opp.zCurrent_Quarter_Forecast_CY_NIBR__c = 0;
                opp.Current_Quarter_Forecast_Amount__c=0;
                opp.ForecastUpdatedQuarter__c = Date.today();
            }
            opps.add(opp);
        } 
        update opps;
    }
  global void finish(Database.BatchableContext BC){
    }
}