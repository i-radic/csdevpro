@isTest
private class AddressBTOnePhoneSiteSearch_Test {

    static testMethod void runPositiveTestCases() {
        user currentuser=[Select Id from user where id=:userInfo.getUserId()];
        System.runAs( currentuser ){
        Test_Factory.setProperty('IsTest', 'yes');
		Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
       	User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
        System.runAs( u1 ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
    	}
        insert Test_Factory.CreateAccountForDummy();
        Account account = Test_Factory.CreateAccount();
        account.OwnerId = u1.Id;
        account.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(account);

        Contact con = Test_Factory.CreateContact();
        con.Cug__c = 'cugAlan1';
        con.Contact_Post_Code__c = 'WR5 3RL';
        insert con;    
        
        Opportunity opp = Test_Factory.CreateOpportunity(accountResult.getid());
        opp.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(opp);

        BT_One_Phone__c bop = new BT_One_Phone__c();
		bop.Opportunity__c = opp.Id;
        bop.OnePhone_Product_Type__c = 'BT One Phone Office';
		bop.Contract_Value__c = 1200;
        bop.Volume_of_Data_SIMs__c = 5;
        bop.Contract_Duration__c = '24 Months';
        bop.Contract_Value__c = 10;
        bop.Existing_Customer_Resign__c  = 'Y';
		insert bop;
        
        BT_One_Phone_Site__c bops = new BT_One_Phone_Site__c();
        bops.BT_One_Phone__c = bop.id;
        bops.Site_Telephone_Number_Landline__c = '01234 567890';
        bops.Site_Diagram_or_Sketch_Attached__c = true;
        bops.Number_of_Users__c = 10;
        bops.Number_of_users__c = 1;
        bops.Number_Concurrent_Calls_Expected_at_Site__c = 10;
        bops.Number_of_Users_in_Parallel_Hunt_Group__c = '5 or Less';
        bops.mobilecoverageResults__c = 'Good Outdoor Only';
        bops.Post_Code__c = 'w13 8qd';
		bops.Customer_Contact__c = con.id;
        bops.Contract_Duration__c = '24 Months';
        bops.Onsite_mobile_network__c=true;
        insert bops;
            
		BTSalesforceServicesURL__c btsurl = new BTSalesforceServicesURL__c();
		btsurl.Name = 'CRMServices';
		btsurl.Url__c = 'https://eric-gotham-salesforceservices.bt.com/SF.CRMGateway/CRMService.asmx';
		insert btsurl;

        PageReference PageRef = Page.addressBTOnePhoneSiteResults;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id',bop.Id);
        ApexPages.currentPage().getParameters().put('Pc','w13 8qd');

        ApexPages.StandardController ctrl = new ApexPages.StandardController(bops);
        AddressBTOnePhoneSiteSearch abopss = new AddressBTOnePhoneSiteSearch(ctrl);
        abopss.getTest();
        abopss.CreateAddress();
        abopss.Back();
        abopss.ReDirect();
        abopss.getAddresses();
        abopss.getSelected();
        abopss.getselectedAddress();
        
        BT_One_Phone_Site__c bops2 = new BT_One_Phone_Site__c();
        bops2.BT_One_Phone__c = bop.id;
        bops2.Site_Telephone_Number_Landline__c = '01234 567890';
        bops2.Site_Diagram_or_Sketch_Attached__c = true;
        bops2.Number_of_Users__c = 10;
        bops2.mobilecoverageResults__c = 'Outdoor only';
        bops2.Post_Code__c = 'w13 8qd';
		bops2.Customer_Contact__c = con.id;
        bops2.Contract_Duration__c = '24 Months';
         
         SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = 'w13 8qf';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
        	
        
        Test_Factory.setProperty('IsTest', 'no');
        abopss.CreateAddress();
        abopss.Back();
        abopss.ReDirect();
        abopss.getSelected();
        
        
        AddressBTOnePhoneSiteSearch abops2 = new AddressBTOnePhoneSiteSearch(bops2);    
        AddressBTOnePhoneSiteSearch.Address addr=new AddressBTOnePhoneSiteSearch.Address();
        addr.IdentifierId='';
        addr.IdentifierName='test';
        addr.IdentifierValue='';
        addr.Country='test';
        addr.County='';
        addr.POBox='';
        addr.Name='test';
        addr.BuildingNumber='';
        addr.Street='';
        addr.Locality='';
        addr.DoubleDependentLocality='';
        addr.PostCode='';
        addr.Town='';
        addr.SubBuilding = 'TEST';
    }
    }
}