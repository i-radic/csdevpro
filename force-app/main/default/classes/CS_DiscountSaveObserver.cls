global class CS_DiscountSaveObserver implements csdiscounts.IDiscountsObserver {
    
    public CS_DiscountSaveObserver() {  }

    private static String CONTROLLER_NAME = 'CS_DiscountSaveObserver';
    @TestVisible
    private static List<String> mccRequiredAttributes = new List<String>{'Contract Term', 'Service Plan Name', 'Total Voice Charge', 'Total Data Charge', 'Subscription Type', 'FCC', 'Maximum FCC', 'Subscription Type Name'};

    String returnMessage = '';

    public String execute(List<cscfga__Product_Configuration__c> productConfigurations){
        Boolean solutionConfigurations = false;

        cscfga__Product_Configuration__c basketConfig = [Select cscfga__Product_Basket__c, Calculations_Product_Group__c from cscfga__Product_Configuration__c where Id = :productConfigurations[0].Id];
        Id basketIdContext = basketConfig.cscfga__Product_Basket__c;
        String productGroup = basketConfig.Calculations_Product_Group__c;

        CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
        String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;
        
        if (calculateProductGroup.contains(productGroup)) {
            solutionConfigurations = true;
        }
        System.debug('solutionConfigurations : ' + solutionConfigurations);
        if (solutionConfigurations) {
            cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketIdContext);

            CS_SolutionDealCalculations dealCalculations = new CS_SolutionDealCalculations(basket);
            dealCalculations.calculateBasketAttributes();
            dealCalculations.calculateBasketApprovals();

            update CS_ProductBasketService.desyncBasket(new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket}).values();
            returnMessage = validateFCC(productConfigurations);
            System.debug('returnMessage : ' + returnMessage);
            return returnMessage;
        }

        try{
            if(productConfigurations != null && productConfigurations.size() > 0){
                Map<Id, cscfga__Product_Configuration__c> configsMap = CS_Util.fillConfigurationsMap(productConfigurations, CONTROLLER_NAME);
                Id basketId;
                for(cscfga__Product_Configuration__c cfg : configsMap.values()) {
                    basketId = cfg.cscfga__Product_Basket__c;
                    break;
                }
                
                cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
                returnMessage = CS_ProductConfigurationService.calculateApprovals(configsMap, basket, CONTROLLER_NAME, true);
                
                update CS_ProductBasketService.desyncBasket(new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket}).values();
            }
        }
        catch(Exception exc){
            logger('csexception = ' + exc);
        }

        logger('returnMessage = ' + returnMessage);
        return returnMessage;
    }

    @TestVisible private void logger(String msg){
        system.debug(LoggingLevel.WARN, 'CS_DiscountSaveObserver.' + msg);
    }
    
    private String validateFCC(List<cscfga__Product_Configuration__c> productConfigurations) {
        String result = '';
        Map<Id, List<cscfga__Attribute__c>> pcToAttributesMap = new Map<Id, List<cscfga__Attribute__c>>();
        List<cscfga__Product_Configuration__c> pcs = null;
        if (productConfigurations.size() == 1) {
            pcs = new List<cscfga__Product_Configuration__c>();
            cscfga__Product_Configuration__c pc = [SELECT Id, cscfga__Parent_Configuration__c FROM cscfga__Product_Configuration__c WHERE Id IN :productConfigurations LIMIT 1];
            if (pc.cscfga__Parent_Configuration__c != null) {
                cscfga__Product_Configuration__c  pcP = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Id = :pc.cscfga__Parent_Configuration__c];
                pcs.add(pcP);
            } else {
                pcs.add(pc);
            }
        } else {
            pcs = productConfigurations;
        }
        for (cscfga__Attribute__c attribute : [SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c IN :pcs AND Name IN :mccRequiredAttributes]) {
            if(!pcToAttributesMap.containsKey(attribute.cscfga__Product_Configuration__c)) {
                pcToAttributesMap.put(attribute.cscfga__Product_Configuration__c, new List<cscfga__Attribute__c>());
            }
            pcToAttributesMap.get(attribute.cscfga__Product_Configuration__c).add(attribute);
        }

        for (cscfga__Product_Configuration__c prodConfig : [SELECT Id, Name, cscfga__Quantity__c, cscfga__Contract_Term__c, (SELECT Id, Name, cscfga__Recurring_Charge__c, Grouping__c FROM cscfga__Related_Configurations__r ORDER BY Grouping__c) FROM cscfga__Product_Configuration__c WHERE Id IN :pcs AND Product_Definition_Name__c LIKE 'User Group']) {
            Decimal accessFee = 0.0;
            Decimal lineRental = 0.0;
            Decimal totalVoiceCharge = 0.0;
            Decimal totalDataCharge = 0.0;
            Decimal maxFCC = 0.0;
            Decimal fcc = 0.0;
            Boolean isSharedPlan = FALSE;
            String subscriptionType = '';
            for (cscfga__Attribute__c attribute : pcToAttributesMap.get(prodConfig.Id)) {
                if (attribute.Name == 'Service Plan Name' && String.isNotEmpty(attribute.cscfga__Value__c) && attribute.cscfga__Value__c.contains('Shared')) {
                    isSharedPlan = TRUE;
                }
                /*if (attribute.Name == 'Total Voice Charge' && String.isNotEmpty(attribute.cscfga__Value__c) && Decimal.valueOf(attribute.cscfga__Value__c) > 0) {
                    totalVoiceCharge = Decimal.valueOf(attribute.cscfga__Value__c);
                }
                if (attribute.Name == 'Total Data Charge' && String.isNotEmpty(attribute.cscfga__Value__c) && Decimal.valueOf(attribute.cscfga__Value__c) > 0) {
                    totalDataCharge = Decimal.valueOf(attribute.cscfga__Value__c);
                }*/
                if (attribute.Name == 'FCC' && String.isNotEmpty(attribute.cscfga__Value__c) && Decimal.valueOf(attribute.cscfga__Value__c) > 0) {
                    fcc = Decimal.valueOf(attribute.cscfga__Value__c);
                }
                if (attribute.Name == 'Subscription Type Name' && String.isNotEmpty(attribute.cscfga__Value__c)) {
                    subscriptionType = attribute.cscfga__Value__c;
                }
            }
            if (subscriptionType == 'Data Only') {
                continue;
            }
            for (cscfga__Product_Configuration__c relatedConfig : prodConfig.cscfga__Related_Configurations__r) {
                if (String.isNotEmpty(relatedConfig.Name) && relatedConfig.Name.contains('Shared Access Fee')) {
                    accessFee = relatedConfig.cscfga__Recurring_Charge__c;
                }
                if (String.isNotEmpty(relatedConfig.Grouping__c) && relatedConfig.Grouping__c.equalsIgnoreCase('Voice')) {
                    totalVoiceCharge = relatedConfig.cscfga__Recurring_Charge__c;
                }
                if (String.isNotEmpty(relatedConfig.Grouping__c) && relatedConfig.Grouping__c.equalsIgnoreCase('Data')) {
                    totalDataCharge = relatedConfig.cscfga__Recurring_Charge__c;
                }
            }
            if (isSharedPlan) {
                lineRental = accessFee;
            } else {
                lineRental = totalVoiceCharge + totalDataCharge;
            }
            maxFCC = Math.round(lineRental * prodConfig.cscfga__Contract_Term__c * 0.55);
            if (fcc > maxFCC) {
                //result = '{"status":"Error", "message":"FCC on User Group Invalid, please update"}';
                //result = '{"status":"Error", "message":"FCC on User Group Invalid, please update. FCC = ' + fcc + ' Maximum FCC = ' + maxFCC + '"}';
                result = 'FCC on User Group Invalid, please update. FCC = ' + fcc + ' Maximum FCC = ' + maxFCC;
            }
        }
        return result;
    }
}