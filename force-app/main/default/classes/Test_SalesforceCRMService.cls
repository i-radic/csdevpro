@isTest(seeAllData=true)
private class Test_SalesforceCRMService{
 
    static testMethod void initiateClasses()
    {
        SalesforceCRMService.CRMAccount account = new SalesforceCRMService.CRMAccount();
        account= new SalesforceCRMService.CRMAccount();
        account.CUG = '';
        account.SAC = '';
        account.PrimaryContactid = '';
        account.CustomerName = '';
        account.IsResidential = false;
        account.Type_x = 'type';
        account.CustomerClass = 'cust';
        account.IsBTBLegacy = false;
        account.OrganizationName = 'test';
        account.Postcode='';
        account.Status='';
        account.Sector='';
        account.SubSector='';
        account.LineOfBusiness='';
        account.wlr3Flag='';
        
        //Contact
        SalesforceCRMService.CRMContact contact = new SalesforceCRMService.CRMContact();
        contact.CUG = '0123456';
        contact.AccountManagerContact = '';   
        contact.AddressDoubleDependentLocality='';
        contact.AddressCounty='';
        contact.AddressCountry='';
        contact.isExisting=false;
        contact.ActiveFlag = 'Y';        
        contact.Comment = '';        
        contact.Type_x = '';        
        contact.ExternalId = '';        
        contact.Title = 'Mr';        
        contact.FirstName = 'FirstName';        
        contact.MiddleName = 'MiddleName';        
        contact.LastName = 'LastName';        
        contact.AliasName = 'AliasName';        
        contact.ContactSource = 'Salesforce';       
        contact.JobSeniority = '';        
        contact.Profession = '';        
        contact.DateOfBirth = DateTime.now();        
        contact.MotherMaidenName = '';        
        contact.Status = 'Active';        
        contact.JobTitle = '';        
        contact.KeyDecisionMaker = '';        
        contact.FaxNumber = '';        
        contact.crmOrganization = '';       
        contact.Department = '';        
        contact.RightToContact = '';        
        contact.PreferredContactChannel = '';        
        contact.SecondaryPreferredContactChannel = '';        
        contact.EmailAddress = 'test@crmtest.com';        
        contact.EmailAddressId = '';        
        contact.EmailConsentPermission = '';        
        contact.EmailConsentDate = DateTime.now();        
        contact.EmailConsentType = '';        
        contact.HomePhoneId = '';        
        contact.HomePhone = '';        
        contact.HomePhoneConsentPermission = '';        
        contact.HomePhoneConsentDate = DateTime.now();        
        contact.HomePhoneConsentType = '';        
        contact.MobilePhoneId = '';        
        contact.MobilePhone = '';        
        contact.MobilePhoneConsentPermission = '';        
        contact.MobilePhoneConsentDate = DateTime.now();        
        contact.MobilePhoneConsentType = '';        
        contact.FaxId = '';        
        contact.Fax = '';        
        contact.FaxConsentPermission = '';        
        contact.FaxConsentDate = DateTime.now();        
        contact.FaxConsentType = '';        
        contact.WorkPhoneId = '';        
        contact.WorkPhone = '';        
        contact.WorkPhoneConsentPermission = '';        
        contact.WorkPhoneConsentDate = DateTime.now();        
        contact.WorkPhoneConsentType = '';        
        contact.AddressId = '';        
        contact.AddressNumber = '';        
        contact.AddressStreet = '';        
        contact.AddressLocality = '';        
        contact.AddressPostCode = '';        
        contact.AddressPostTown = '';        
        contact.AddressSector = '';        
        contact.AddressSubBuilding = '';        
        contact.AddressZipCode = '';        
        contact.AddressConsentPermission = '';        
        contact.AddressConsentDate = DateTime.now();        
        contact.AddressTimeAt = '';        
        contact.AddressConsentType = '';
        contact.ContactCategory='';
        contact.Email1IntegrationId = '';
        contact.Email2IntegrationId = '';
        contact.Email3IntegrationId = '';
        contact.EmailPreferred = '';
        contact.Email1DeleteIntegrationId = '';
        contact.Email2DeleteIntegrationId = '';
        contact.Email3DeleteIntegrationId = '';
        contact.Voice1Delete = '';
        contact.Voice2Delete = '';
        contact.Voice3Delete = '';
        contact.VoicePreferred = '';
        contact.VoiceType1 = '';
        contact.VoiceType2 = '';
        contact.VoiceType3 = '';
        contact.VoiceType1Delete = '';
        contact.VoiceType2Delete = '';
        contact.VoiceType3Delete = '';
        contact.Voice1DeleteIntegrationID = '';
        contact.Voice2DeleteIntegrationID = '';
        contact.Voice3DeleteIntegrationID = '';
        contact.EmailConsent = '';
        contact.PhoneConsent = '';
        contact.SMSConsent = '';
        contact.AutomatedPhonecallsConsent = '';
        contact.ProfileConsent = '';
        contact.DigitalMarketingConsent = '';
        contact.AutomatedPhonecallsConsentDate = DateTime.Now();
        contact.ProfilingConsentDate = DateTime.Now();
        contact.DigitalMarketingConsentDate = DateTime.Now();
        
    }
    static testMethod void GetAccount() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceCRMService.CRMAccount account = new SalesforceCRMService.CRMAccount();
        
        try
        {   
          Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());      
          account=SalesforceCRMService.GetAccount('123456789');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
        
        account= new SalesforceCRMService.CRMAccount();
        account.CUG = '';
        account.SAC = '';
        account.PrimaryContactid = '';
        account.CustomerName = '';
        account.IsResidential = false;
        account.Type_x = 'type';
        account.CustomerClass = 'cust';
        account.IsBTBLegacy = false;
        account.OrganizationName = 'test';
        
        
        
    }
   
    static testMethod void  GetAddress() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceCRMService.CRMAddress crmAddr = null;
        crmAddr = new SalesforceCRMService.CRMAddress();
        
        try
        {
             Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
            crmAddr=SalesforceCRMService.GetAddress('1234567');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
    static testMethod void GetNADAddress() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceCRMService.CRMAddressList CRMAddList = null;
        CRMAddList = new SalesforceCRMService.CRMAddressList();
        try
        {
             Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
            CRMAddList = SalesforceCRMService.GetNADAddress('SW19 1AA');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
    static testMethod void GetContact() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceCRMService.CRMContact crmAcc  = null;
        crmAcc = new SalesforceCRMService.CRMContact();
        try
        {
             Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
            crmAcc = SalesforceCRMService.GetContact('cug','contactid');
             Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
            String res=SalesforceCRMService.GetCUGId('01234567890');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
    private static SalesforceCRMService.CRMContact CreateContactForTest() {
     	
        SalesforceCRMService.CRMContact contact = new SalesforceCRMService.CRMContact();
        contact.CUG = '0123456';
        contact.AccountManagerContact = '';        
        contact.ActiveFlag = 'Y';        
        contact.Comment = '';        
        contact.Type_x = '';        
        contact.ExternalId = '';        
        contact.Title = 'Mr';        
        contact.FirstName = 'FirstName';        
        contact.MiddleName = 'MiddleName';        
        contact.LastName = 'LastName';        
        contact.AliasName = 'AliasName';        
        contact.ContactSource = 'Salesforce';       
        contact.JobSeniority = '';        
        contact.Profession = '';        
        contact.DateOfBirth = DateTime.now();        
        contact.MotherMaidenName = '';        
        contact.Status = 'Active';        
        contact.JobTitle = '';        
        contact.KeyDecisionMaker = '';        
        contact.FaxNumber = '';        
        //contact.Organization = '';       
        contact.Department = '';        
        contact.RightToContact = '';        
        contact.PreferredContactChannel = '';        
        contact.SecondaryPreferredContactChannel = '';        
        contact.EmailAddress = 'test@crmtest.com';        
        contact.EmailAddressId = '';        
        contact.EmailConsentPermission = '';        
        contact.EmailConsentDate = DateTime.now();        
        contact.EmailConsentType = '';        
        contact.HomePhoneId = '';        
        contact.HomePhone = '';        
        contact.HomePhoneConsentPermission = '';        
        contact.HomePhoneConsentDate = DateTime.now();        
        contact.HomePhoneConsentType = '';        
        contact.MobilePhoneId = '';        
        contact.MobilePhone = '';        
        contact.MobilePhoneConsentPermission = '';        
        contact.MobilePhoneConsentDate = DateTime.now();        
        contact.MobilePhoneConsentType = '';        
        contact.FaxId = '';        
        contact.Fax = '';        
        contact.FaxConsentPermission = '';        
        contact.FaxConsentDate = DateTime.now();        
        contact.FaxConsentType = '';        
        contact.WorkPhoneId = '';        
        contact.WorkPhone = '';        
        contact.WorkPhoneConsentPermission = '';        
        contact.WorkPhoneConsentDate = DateTime.now();        
        contact.WorkPhoneConsentType = '';        
        contact.AddressId = '';        
        contact.AddressNumber = '';        
        contact.AddressStreet = '';        
        contact.AddressLocality = '';        
        contact.AddressPostCode = '';        
        contact.AddressPostTown = '';        
        contact.AddressSector = '';        
        contact.AddressSubBuilding = '';        
        contact.AddressZipCode = '';        
        contact.AddressConsentPermission = '';        
        contact.AddressConsentDate = DateTime.now();        
        contact.AddressTimeAt = '';        
        contact.AddressConsentType = '';
        contact.Email1IntegrationId = '';
        contact.Email2IntegrationId = '';
        contact.Email3IntegrationId = '';
        contact.EmailPreferred = '';
        contact.Email1DeleteIntegrationId = '';
        contact.Email2DeleteIntegrationId = '';
        contact.Email3DeleteIntegrationId = '';
        contact.Voice1Delete = '';
        contact.Voice2Delete = '';
        contact.Voice3Delete = '';
        contact.VoicePreferred = '';
        contact.VoiceType1 = '';
        contact.VoiceType2 = '';
        contact.VoiceType3 = '';
        contact.VoiceType1Delete = '';
        contact.VoiceType2Delete = '';
        contact.VoiceType3Delete = '';
        contact.Voice1DeleteIntegrationID = '';
        contact.Voice2DeleteIntegrationID = '';
        contact.Voice3DeleteIntegrationID = '';
        contact.EmailConsent = '';
        contact.PhoneConsent = '';
        contact.SMSConsent = '';
        contact.AutomatedPhonecallsConsent = '';
        contact.ProfileConsent = '';
        contact.DigitalMarketingConsent = '';
        contact.AutomatedPhonecallsConsentDate = DateTime.Now();
        contact.ProfilingConsentDate = DateTime.Now();
        contact.DigitalMarketingConsentDate = DateTime.Now();
        
        return contact;
    }
    
    static testMethod void InsertContact() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceCRMService.CRMContact contact = CreateContactForTest();
               
        
        try
        {
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
            contact = SalesforceCRMService.InsertContact(contact,FALSE);
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
     static testMethod string  UpdateContact() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
         SalesforceCRMService.CRMContact contact = CreateContactForTest();
         SalesforceCRMService.CRMAddress Address = null;
         string s;              
        try
        {
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
           s = SalesforceCRMService.UpdateContact(contact);
           return s;
        }
        catch(Exception e){
            // test cannot run webservice calls
             return null;
        }
    }
    
    
    
    static testMethod string CreateNADAddress() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceCRMService.CRMAddress address = CreateAddressForTest();
        string s;    
        try
        {
             Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
            s=SalesforceCRMService.CreateNADAddress(address);
            return s;
        }
        catch(Exception e){
            // test cannot run webservice calls
             return null;
        }
    }
    
     private static SalesforceCRMService.CRMAddress CreateAddressForTest() {
        SalesforceCRMService.CRMAddress address = new SalesforceCRMService.CRMAddress();
        address.IdentifierId = 'test';
        address.IdentifierName = '';
        address.IdentifierValue = '';
        address.Country = '';
        address.County = '';
        address.Name = '';
        address.POBox = '';
        address.BuildingNumber = '';
        address.Street = '';
        address.Locality = '';
        address.DoubleDependentLocality = '';
        address.PostCode = '';
        address.Town = '';
        address.SubBuilding = '';
        address.ExchangeGroupCode = '';
        
        return address;
    }
    
    static testMethod void CreateContact() {
       
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceCRMService.CRMAddress address = CreateAddressForTest();
        SalesforceCRMService.CRMContact contact = CreateContactForTest();
        string s;    
        try
        {
             Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
            s=SalesforceCRMService.CreateContact(contact);
            
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
        
       
    }
    static testMethod void CreateEventInteraction() {
    	
       
        SalesforceCRMService.BTLBEventInteraction btlbEvent = null;
        btlbEvent = new SalesforceCRMService.BTLBEventInteraction();
        btlbEvent.EventId = '';
        btlbEvent.CUG = '';
        btlbEvent.RecordId = '';
        btlbEvent.agentEIN = '';
        btlbEvent.interactionIntegrationId = '';
     	SalesforceCRMService.CreateEventInteraction(btlbEvent);
    }
    static testMethod void GetCustomerIsLegacy()
    {
        SalesforceCRMService.CRMAccount crmAccount = new SalesforceCRMService.CRMAccount();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1());   
        crmAccount=SalesforceCRMService.GetCustomerIsLegacy('0123456');
    }
    
}