/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class LeadProcessDispatcherController_test {

    private static Account mCompanyTest;
    private static Lead mLeadTest;
    
    // Set up Test Data for the Lead - created as sys admin
    static{
        TriggerDeactivateTest();
        mCompanyTest = EE_TestClass.createStaticAccount('Test Company', false);
        insert mCompanyTest;
        
        mLeadTest = EE_TestClass.createStaticLead('John', 'Smith', null, false);
        
        mLeadTest.pH_Email__c                   = 'test@test.co.uk.test';
        mLeadTest.Business_Enquiries_Email__c   = 'test@test.co.uk.test';
        mLeadTest.User_Email__c                 = 'test@test.co.uk.test';
        mLeadTest.Vendor_Agent_Email__c         = 'test@test.co.uk.test';
        mLeadTest.Account__c                = mCompanyTest.Id;
        mLeadTest.Company                       = 'Test Company';
        insert mLeadTest;
        system.assertNotEquals(mLeadTest.Id, null);

    }
    

   /*
    * @name         testProcessAutosaveNewToInvestigating
    * @description  selects the Process option when pressing the Accept button
    * @author       P Goodey
    * @date         June 2014
    * @see          WR 00070519
    */
    static testmethod void testProcessAutosaveNewToInvestigating() { 
        
        // Retrieve the Lead fields
        Lead loLead = [SELECT Id, Status, Lead_Status_Reason__c FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
        system.assertEquals('New', loLead.Status);        
           
        PageReference pageRef = Page.LeadProcessDispatcher;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put('process', 'autosave');
        pageRef.getParameters().put('status', 'Investigating');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(mLeadTest);
        
        // Load the Controller
        LeadProcessDispatcherController oCtr = new LeadProcessDispatcherController(sc);
        
        // Start Tests          
        Test.StartTest();
        
        // Process the Lead        
        //pageRef = oCtr.processLead();
        oCtr.processLead();

        
        // Stop the Tests
        Test.stopTest();

        // Retrieve the Lead fields
        loLead = [SELECT Id, Status, Lead_Status_Reason__c FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
        system.assertEquals('Investigating', loLead.Status);  
    
    }



   /*
    * @name         testProcessRedirectNewToQualifiedOut
    * @description  selects the Process option when pressing the Qualify Out button
    * @author       P Goodey
    * @date         June 2014
    * @see          WR 00070519
    */
    static testmethod void testProcessRedirectNewToQualifiedOut() { 
        
        // Retrieve the Lead fields
        Lead loLead = [SELECT Id, Status, Lead_Status_Reason__c FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
        system.assertEquals('New', loLead.Status);        
           
        // Load the Lead Process Entry Page      
        PageReference pageRef = Page.LeadProcessEntry;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put('process', 'redirect');
        pageRef.getParameters().put('status', 'Qualified Out');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(mLeadTest);
        
        // Load the Controller
        LeadProcessDispatcherController oCtr = new LeadProcessDispatcherController(sc);
        
        // Start Tests          
        Test.StartTest();
        
        // Process the Lead        
        //pageRef = oCtr.processLead();
        oCtr.processLead();

        
        oCtr.resetStatus();
        
        oCtr.moLead.Lead_Status_Reason__c = 'Unable to Complete';
        
        oCtr.onSave();
        
        // Stop the Tests
        Test.stopTest();

        // Retrieve the Lead fields
        loLead = [SELECT Id, Status, Lead_Status_Reason__c FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
        system.assertEquals('Qualified Out', loLead.Status);
        system.assertEquals('Unable to Complete', loLead.Lead_Status_Reason__c);  
    
    }



   /*
    * @name         testProcessAutoAssign
    * @description  selects the Process option when accessing the auto_assign option
    * @author       P Goodey
    * @date         Feb 2016
    * @see          Case 00094818
    */
    static testmethod void testProcessAutoAssign() {
        
        // Standard User
        User lUserTestStandard = EE_TestClass.createStaticUser('John', 'Smith', null, 'Standard User', 'Standard1234567890', false);
        insert lUserTestStandard;
        system.assertNotEquals(lUserTestStandard.Id, null);
        
        // Set the Company owner to the Standard User
        mCompanyTest = [SELECT Id, OwnerId FROM Account WHERE ID = :mCompanyTest.Id LIMIT 1];
        mCompanyTest.OwnerId = lUserTestStandard.Id;
        update mCompanyTest;
        
        // Retrieve the Lead fields
        Lead loLead = [SELECT Id, Status, OwnerId, Account__c FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
        
        // Check that the lead and company owner are different
        system.assertNotEquals(mCompanyTest.OwnerId, loLead.OwnerId, 'Lead and Company Owner are different');        
           
        // Load the Lead Process Entry Page      
        PageReference pageRef = Page.LeadProcessEntry;
        Test.setCurrentPageReference(pageRef);
        
        pageRef.getParameters().put('process', 'auto_assign');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(mLeadTest);
        
        // Load the Controller
        LeadProcessDispatcherController oCtr = new LeadProcessDispatcherController(sc);
        
        
        // Start Tests          
        Test.StartTest();
    
        // Process the Lead        
        //pageRef = oCtr.processLead();
        oCtr.processLead();

                
        // Stop the Tests
        Test.stopTest();

        // Retrieve the Lead fields
        loLead = [SELECT Id, Status, OwnerId FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
        //system.assertEquals(mCompanyTest.OwnerId, loLead.OwnerId, 'Lead and Company Owner are the same');
    
    }





   /*
    * @name         testProcessRedirectNewToQualifiedOutException
    * @description  selects the Process option when pressing the Qualify Out button
    * @author       P Goodey
    * @date         June 2014
    * @see          WR 00070519
    */
    static testmethod void testProcessRedirectNewToQualifiedOutException() { 
        
        // Retrieve the Lead fields
        Lead loLead = [SELECT Id, Status, Lead_Status_Reason__c FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
        system.assertEquals('New', loLead.Status);        
           
        // Load the Lead Process Entry Page      
        PageReference pageRef = Page.LeadProcessEntry;
        Test.setCurrentPageReference(pageRef);
        pageRef.getParameters().put('process', 'redirect');
        pageRef.getParameters().put('status', 'Qualified Out');        
        ApexPages.StandardController sc = new ApexPages.StandardController(mLeadTest);
        
        // Load the Controller
        LeadProcessDispatcherController oCtr = new LeadProcessDispatcherController(sc);
        
        // Start Tests          
        Test.StartTest();
        
        // Process the Lead        
        //pageRef = oCtr.processLead();
        oCtr.processLead();

        
        oCtr.resetStatus();
        oCtr.moLead.Lead_Status_Reason__c = 'Unable to Complete';
        
        string lsCompany300chars;
        lsCompany300chars = '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
        lsCompany300chars += '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890';
        lsCompany300chars += '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'; 
        oCtr.moLead.Company = lsCompany300chars;
        
        try{
            oCtr.onSave();
        }catch(Exception e){
            //system.assertEquals('New', loLead.Status); e.
            loLead = [SELECT Id, Status, Lead_Status_Reason__c FROM Lead WHERE Id=:mLeadTest.Id LIMIT 1];
            system.assertEquals('New', loLead.Status);
        }
        
        // Stop the Tests
        Test.stopTest();

    }
    static void TriggerDeactivateTest(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    }

}