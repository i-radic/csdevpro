/******************************************************************************************************
Name : SMSPluginTest 
Description : This class is used for code coverage for SMSPlugin class used for mobile 2FA
Test Class: SMSPlugin_Test

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                   23/01/2018                TCS DEV                            Class created          
*********************************************************************************************************/
@isTest
private class SMSPluginTest {

    /******************************************************************************************************
    Method Name : CreateUser
    Description : Method To Create Test User
    
    Version                 Date                    Author                             Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
    1.0                   23/01/2018                TCS DEV                             Method created        
    **********************************************************************************************************/ 
    private static User createUser(){
        //Inserting test User
        id userProfileId = [select id,Name from Profile where Name='System Administrator'].id;        
        User record = new User(Alias = 'standt', Email='standarduser111@testorg2.com',EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',LocaleSidKey='en_US', ProfileId = userProfileId,TimeZoneSidKey='America/Los_Angeles', UserName='BTstandarduser111@testorg2.com',
        MobilePhone='+12345678900',EIN__c = '0989898', Work_Area__c='UKSME');
        insert record;
        return record;
    }
    
    /******************************************************************************************************
    Method Name : Test Methods
    Description : Method For Code Ceverage for SMS Plugin
    
    Version                 Date                    Author                             Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
    1.0                   23/01/2018                TCS DEV                             Method created        
    **********************************************************************************************************/ 
    static testmethod void SMSPluginTest(){
        User record = createUser();
        SMSPlugin plugin = new SMSPlugin();
        Map<String, Object> inputParams = new Map<String, Object>();
        string to = record.MobilePhone;
        inputParams.put('To', to);
        Process.PluginRequest request = new Process.PluginRequest(inputParams);
        plugin.invoke(request);
        Process.PluginDescribeResult result = plugin.describe();    
    }
}