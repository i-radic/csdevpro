/**
 * Project Name..........: Everything Everywhere
 * File..................: TrialHelper.cls
 * Version...............: 1 
 * Created by............: Martin Eley
 * Created Date..........: 28th August 2012
 * Last Modified by......:    
 * Last Modified Date....:   
 * Description...........: Provides methods for processing Trials
 */
public with sharing class TrialHelper {
     
     
    /**
     * Method for calculating trial length in days
     *
     * @param String Trial Length
     * @return Numner of days
     */
    public Integer calculateTrialLength(String trialPeriod){
        if(trialPeriod == '1 Week'){
            return 7;
        } else if(trialPeriod == '2 Weeks'){
            return 14;
        } else if(trialPeriod == '3 Weeks'){
            return 21;
        } else if(trialPeriod == '4 Weeks'){
            return 28;
        } else {
            return 0;
        }
    }
    
    /**
     * Method to create a task for the opportunity owner to collect devices
     *
     * @param List<Trial__c> List of Trials records
     */
    public void CreateEndOfTrialTasks(List<Trial__c> trials){
        List<Task> tasks = new List<Task>();
        for(Trial__c aTrial : trials){
            Task aTask = new Task(WhatId = aTrial.Id
                ,Subject = aTrial.Company_Name__c + ', trial ' + aTrial.Name + ' collections and returns.'
                ,OwnerId = aTrial.Opportunity_Owner_Id__c
                ,Description = 'Please contact ' + aTrial.Customer_Contact_Name__c + ' to arrange collection and return of trial devices at the end of your trial period.'
                ,ActivityDate = aTrial.Trial_End_Date__c - 6);
            tasks.add(aTask);
        }
        insert tasks;
    }
     
    /**
     * Method for setting status when all line items dispatched, also sets trial start/end dates
     * Trial end date is not calculated using a formula field as Trial Team can sometimes overide
     *
     * @param List<Trial__c> List of Trials records
     */
    public List<Trial__c> SetDispatchedStatus(List<Trial__c> trials){
        Integer trialLength = 0;
        for(Trial__c aTrial : trials){
            aTrial.Status__c = 'Dispatched';
            aTrial.Trial_Start_Date__c = Date.Today() + 1;          
            aTrial.Trial_End_Date__c = aTrial.Trial_Start_Date__c 
                + this.calculateTrialLength(aTrial.Trial_Period__c);  
        }
        
        //Create task for opp owner to follow up before end of trial
        this.CreateEndOfTrialTasks(trials);
        
        return trials;
    }
     
     /**
      * Method for setting estimated strial start date
      * 
      * @param List<Trial__c> List of Trials records
      */ 
      public List<Trial__c> UpdateEstimatedTrialStartDate(List<Trial__c> trials){
        for(Trial__c aTrial : trials){
            aTrial.Estimated_Trial_Start_Date__c = Date.Today() + 2;
        }
        
        return trials;
      }
      
     /**
     * Method to validate that signed trials have a signed agreement
     * 
     * @param List<Trial__c> List of Trials to be processed
     * @return List of updated Trials
     */
     
     /* THIS HAS BEEN COMMENTED OUT TO TEMP PREVENT VALIDATION AS PER CLIENT REQUEST
     * WILL IMPLEMENT AGAIN WHEN THUNDERHEAD.COM HAVE COMPLETED THEIR ECHOSIGN 
     * INTEGRATION DUE FEB 2013 
     public List<Trial__c> ValidateSignedAgreement(List<Trial__c> trials){
        //Build set of trial IDs
        Set<Id> trialIds = new Set<Id>();
        for(Trial__c aTrial : trials){
            trialIds.add(aTrial.Id);
        }
        
        //Get all signed agreements for trials
        Set<Id> agreementTrialIds = new Set<Id>();      
        for(echosign_dev1__SIGN_Agreement__c agreement : [SELECT Trial__c
            FROM echosign_dev1__SIGN_Agreement__c
            WHERE echosign_dev1__Status__c = 'Signed'
                AND Trial__c IN :trialIds]){
            agreementTrialIds.add(agreement.Trial__c);
        }
                     
        //Validte trial has signed agreement
        for(Trial__c aTrial : trials){
            if(!agreementTrialIds.contains(aTrial.Id) && aTrial.Status__c == 'Signed'){
                aTrial.addError('You cannot set the status to "Signed".  There are no signed agreements for this trial');
            }
        }
        
        return trials;
     }
     */
     
     
    /**
     * Method to validate that sent trials have a sent agreement
     * 
     * @param List<Trial__c> List of Trials to be processed
     * @return List of updated Trials
     */
     /* THIS HAS BEEN COMMENTED OUT TO TEMP PREVENT VALIDATION AS PER CLIENT REQUEST
     * WILL IMPLEMENT AGAIN WHEN THUNDERHEAD.COM HAVE COMPLETED THEIR ECHOSIGN 
     * INTEGRATION DUE FEB 2013 
     public List<Trial__c> ValidateSentAgreement(List<Trial__c> trials){
        //Build set of trial IDs
        Set<Id> trialIds = new Set<Id>();
        for(Trial__c aTrial : trials){
            trialIds.add(aTrial.Id);
        }
        
        //Get all sent agreements for trials
        Set<Id> agreementTrialIds = new Set<Id>();      
        for(echosign_dev1__SIGN_Agreement__c agreement : [SELECT Trial__c
            FROM echosign_dev1__SIGN_Agreement__c
            WHERE echosign_dev1__Status__c = 'Out for Signature'
                AND Trial__c IN :trialIds]){
            agreementTrialIds.add(agreement.Trial__c);
        }
                     
        //Validte trial has signed agreement
        for(Trial__c aTrial : trials){
            if(!agreementTrialIds.contains(aTrial.Id) && aTrial.Status__c == 'Out for Signature'){
                aTrial.addError('You cannot set the status to "Out for Signature".  There are no agreements out for Signature for this trial');
            }
        }
        
        return trials;
     }
     */
     
     
    
}