@IsTest
public class CS_FileUploadControllerTest {

	private static Blob csv = null;

	private static void createTestData(){
		//CS_TestDataFactory.insertTriggerDeactivatingSetting();
		csv = Blob.valueOf('MSISDN,Kolumna,447700900590,1,447700900333,2,447700900455,3,447700900007,4,447700900078,5,447700900280,6,447700900831,7,447700900713,8,');
		cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = 'PD1',
            cscfga__Description__c = 'PD1 Desc'
        );
        insert pd;

		cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
			Name = 'Product Configuration',
			cscfga__Product_Definition__c = pd.Id
		);
		insert pc;

		PageReference ref = Page.CS_FileUpload;
		ApexPages.currentPage().getParameters().put('configId', pc.Id);
	}

	private static testMethod void TestUpload(){

		createTestData();
		
		Test.startTest();
		
		CS_FileUploadController ctrl = new CS_FileUploadController();
		ctrl.contentFile = csv;
		PageReference result = ctrl.Upload();

		System.assert(result.getUrl().contains('success=true'));

		Test.stopTest();

	}
}