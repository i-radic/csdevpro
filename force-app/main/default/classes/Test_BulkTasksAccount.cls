@isTest

private class Test_BulkTasksAccount{
    
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
       // Account dummyAccount = Test_Factory.CreateAccountForDummy();
      //  insert dummyAccount;
            
        createBulkTasksAccount myAT = new createBulkTasksAccount(); 
       
       String LOB='FIELDM';
       String SAC='xyz124';
       String LE='LExyz124';
       
       Account testAcc = Test_Factory.CreateAccount();
       testAcc.Sector_code__c='CORP';
       testAcc.LOB_Code__c=LOB;
       testAcc.sac_Code__c=SAC;
       testAcc.LE_Code__c = null;
       Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc});  
           
    //Test a Corp (LOB Level Task)
        //add test user as team member LOB Task
        AccountTeamMember  ATM1 = Test_Factory.CreateAccountTeamMember();
        ATM1.AccountId = accResult[0].getId();
        ATM1.userId = Userinfo.getUserId();
        ATM1.TeamMemberRole = 'TestScript';
      	insert ATM1;
          
    //create tasks for applicable to account & team member
        Bulk_Tasks__c  Desk1 = Test_Factory.CreateBulkTasks();
        Desk1.description__c = 'D1test';
        Desk1.task_unique__c = 'D1test123';
        Desk1.Task_id__c = 'D1test1';
        Desk1.Minimum_Lead_Time__c = 10;
        Desk1.Reminder_Days__c = 1;
        Desk1.Minimum_Repeat_Time__c = 20;
        Desk1.USER_Type__c = 'TestScript';
        Desk1.Task_Category__c = 'Account';
        Desk1.LOB_Code__c = LOB;
      	insert Desk1;
          
        Bulk_Tasks__c  Desk2 = Test_Factory.CreateBulkTasks();
        Desk2.description__c = 'D2test';
        Desk2.task_unique__c = 'D2test123';
        Desk2.Task_id__c = 'D2test1';
        Desk2.Minimum_Lead_Time__c = 10;
        Desk2.Reminder_Days__c = 1;
        Desk2.USER_Type__c = 'TestScript';
        Desk2.Task_Category__c = 'Account';
        Desk2.LOB_Code__c = LOB;
      	insert Desk2;  
                
        myAT.account = Test_Factory.CreateAccount(accResult[0].getId());
        myAT.createAccountTasks();
        myAT.createAccountTasks();  
        
    	//Test a BTLB (Sector Level Task)
     	//String SACCODE2 = 'testSAC2';
       	//Account testAcc2 = new Account(name = 'TESTCODE',  sac_code__c= 'xyz124' ,Sector_code__c='BTLB', LE_code__c='TESTLE');
       	//Database.SaveResult[] accResult2 = Database.insert(new Account[] {testAcc2});
    
       Account testAcc2 = Test_Factory.CreateAccount();
       testAcc2.Sector_code__c='BTLB';
       testAcc2.LOB_Code__c = LOB;
       testAcc2.sac_Code__c = SAC;
       testAcc2.LE_Code__c = LE;
       Database.SaveResult[] accResult2 = Database.insert(new Account[] {testAcc2});  
    
    
        //add test user as team member LOB Task
        AccountTeamMember  ATM2 = Test_Factory.CreateAccountTeamMember();
        ATM2.AccountId = accResult2[0].getId();
        ATM2.userId = Userinfo.getUserId();
        ATM2.TeamMemberRole = 'TestScript';
	    insert ATM2;

    	//create tasks for applicable to account & team member
        Bulk_Tasks__c  Btlb1 = Test_Factory.CreateBulkTasks();
        Btlb1.description__c = 'Btest';
        Btlb1.task_unique__c = 'Btest123';
        Btlb1.Task_id__c = 'Btest1';
        Btlb1.Minimum_Lead_Time__c = 10;
        Btlb1.Reminder_Days__c = 1;
        Btlb1.Minimum_Repeat_Time__c = 20;
        Btlb1.USER_Type__c = 'TestScript';
        Btlb1.Task_Category__c = 'Account';
        Btlb1.Sector_Code__c = 'BTLB';
	    insert Btlb1;
    
        Bulk_Tasks__c  Btlb2 = Test_Factory.CreateBulkTasks();
		Btlb2.description__c = 'B2test';
		Btlb2.task_unique__c = 'B2test123';
		Btlb2.Task_id__c = 'B2test1';
		Btlb2.Minimum_Lead_Time__c = 10;
		Btlb2.Reminder_Days__c = 1;
		Btlb2.USER_Type__c = 'TestScript';
		Btlb2.Task_Category__c = 'Account';
		Btlb2.Sector_Code__c = 'BTLB';
	  	insert Btlb2; 
          
        myAT.account = Test_Factory.CreateAccount(accResult2[0].getId());
        myAT.createAccountTasks();     
        myAT.createAccountTasks();
    }
}