global class OrderControllerExtensions {

    global csord__Order__c order { get; set; }
    global List<Contact> contacts { get; set; }
    
    global OrderControllerExtensions(ApexPages.StandardController stdController) {
        csord__Order__c record = (csord__Order__c)stdController.getRecord();
       	this.order = [SELECT Id, csord__Account__c FROM csord__Order__c WHERE Id = :record.Id]; // order id
        this.contacts = [SELECT Id, Name, Email FROM Contact WHERE AccountId = :this.order.csord__Account__c];      
    }
    
    @RemoteAction
	public static String sendEmail(String targetEmail, Id orderId){
        String timestamp = String.valueOf(Datetime.now().getTime());
        Blob bKey = Crypto.generateAesKey(SiteUtilities.ENC_SIZE);
        Blob bEmail = SiteUtilities.encryptString(bKey, targetEmail);

        CS_Site_PIN__c sp = new CS_Site_PIN__c();
        sp.User__c = UserInfo.getUserId();
        sp.Order__c = orderId;
        sp.Target_Email__c = targetEmail;
        sp.Hash__c = EncodingUtil.base64Encode(bKey);
        sp.Timestamp__c = timestamp;
        insert sp;
        
        Blob bId = SiteUtilities.encryptString(bKey, sp.Id);
        Map<String, Blob> toEncode = new Map<String, Blob> { SiteUtilities.EMAIL => bEmail, SiteUtilities.TIMESTAMP => Blob.valueOf(timestamp), SiteUtilities.SPID => bId };
        
        String body = '';
        String fullURI = SiteUtilities.SITE_URL + SiteUtilities.encodeURI(toEncode);
        body += 'Please follow <a href="' + fullURI + '">this link</a> to proceed to Order Enrichment.';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] { targetEmail });
        mail.setHtmlBody(body);
        try {
        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            sp.Is_Email_Sent__c = true;
            update sp;
            return 'success'; 
        }catch(Exception e){
            return 'error'; 
        }
    }  
}