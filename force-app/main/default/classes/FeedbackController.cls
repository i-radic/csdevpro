public with sharing class FeedbackController {
public List<Feedback__c> searchResults {get;set;}
public string CheckStatus = null;
    public FeedbackController(ApexPages.StandardController controller) {

    }

public string FeedbackID = System.currentPageReference().getParameters().get('id');
 
      
public Attachment attachment {
  get {
      if (attachment == null)
      attachment = new Attachment();
      return attachment;
    }
  set;
  }
  
public Note note {
  get {
      if (note == null)
        note = new Note();
      return note ;
    }
  set;
  }

public List<Attachment> getAtt() {    
      return [SELECT ID, Name, Description, Body, BodyLength, CreatedByID, CreatedBy.Name, CreatedDate FROM Attachment WHERE ParentID = :FeedbackID  ORDER BY CreatedDate DESC ];
  }

public List<Note> getNoteList() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :FeedbackID ORDER BY CreatedDate DESC];
  }
 
public PageReference CATyes() {
// CAT Sign off using a note
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = FeedbackID ; // the record the file is attached to
    //note.IsPrivate = false;
    note.Title= 'CAT Sign Off';
    note.Body= 'CAT Passed. ' + note.body;
           
    try {
      insert Note;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }

    try {
        Feedback__c fbkStatus = [SELECT ID, Status__c FROM Feedback__c WHERE ID = :FeedbackID LIMIT 1];
        fbkStatus.Status__c = 'Passed CAT';

        update fbkStatus;
    } Catch (DMLException e) {
      ApexPages.addMessages(e);
      return null;
    }

    return new PageReference('/'+FeedbackID );

}


public PageReference CATno() {
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = FeedbackID ; // the record the file is attached to
    //note.IsPrivate = false;
    note.Title= 'CAT Failed';
    if (note.Body == '') {
        note.adderror('Comments must be added for Rejecting CAT'); 
        return null;
    }
    else {
    note.Body= 'CAT FAILED. Rejection Comments: ' + note.body;
    }
    
    try {
      insert Note;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }

    try {
        Feedback__c fbkStatus = [SELECT ID, Status__c FROM Feedback__c WHERE ID = :FeedbackID LIMIT 1];
        fbkStatus.Status__c = 'Failed CAT';

        update fbkStatus;
    } Catch (DMLException e) {
      ApexPages.addMessages(e);
      return null;
    }
    
     
    return new PageReference('/'+FeedbackID );
}

public PageReference CATcancel() {
    return new PageReference('/'+FeedbackID );
}

public PageReference upload() {
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = FeedbackID ; // the record the file is attached to
    attachment.IsPrivate = false;
    if (attachment.Description == '') {
        attachment.adderror('Please enter a Title for the attachment'); 
        return null;
    }
    else {
    note.Body= 'CAT FAILED. Rejection Comments: ' + note.body;
    } 
    try {
      insert attachment;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      attachment = new Attachment(); 
      attachment.body  = null;
    }
 
    //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
    
  }
  
public PageReference addNote() {
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = FeedbackID ; // the record the file is attached to
    //note.IsPrivate = false;
    note.Title= 'Feedback Note';
 
    try {
      insert Note;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
}
 
}