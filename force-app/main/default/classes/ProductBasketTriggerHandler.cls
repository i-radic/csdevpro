public class ProductBasketTriggerHandler 
{

 	public static void AfterInsertHandle(List<cscfga__Product_Basket__c> lstNewPB){
    	system.debug('****AfterInsertHandle lstNewPB=' + lstNewPB);
    	UnSyncProductBasketsAfterInsertUpdate(lstNewPB,null);
    }
    
    public static void AfterUpdateHandle(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB){
    	system.debug('****AfterUpdateHandle lstOldPB=' + lstOldPB);
    	system.debug('****AfterUpdateHandle lstNewPB=' + lstNewPB);
    	Boolean changedBasket = false;
    	Map<Id, cscfga__Product_Basket__c> oldBaskets = new Map<Id, cscfga__Product_Basket__c>(lstOldPB);
    	for(cscfga__Product_Basket__c basket : lstNewPB) {
    		cscfga__Product_Basket__c oldBasket = oldBaskets.get(basket.Id);
    		if(basket.get(ProductUtility.GetSyncField()) != oldBasket.get(ProductUtility.GetSyncField())) {
    			changedBasket = true;
    			break;
    		}
    	}
    	
    	if(changedBasket) {
	    	UnSyncProductBasketsAfterInsertUpdate(lstNewPB,lstOldPB);
	    	DeleteOLIsProductDetailsAfterUpdate(lstNewPB,lstOldPB);
	    	InsertOLIsProductDetailsAfterUpdate(lstNewPB,lstOldPB);
    	}
    }

	private static void UnSyncProductBasketsAfterInsertUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB)
	{
		//to be called after insert or after update, 
		//if newly updated Product Baskets were synced then un-sync all others which have same Opportunity
		//if newly inserted Product Baskets are synced then un-sync all others which have same Opportunity
		
		//set<string> setOpportunityId = new set<string>();
		set<Id> setOpportunityId = new set<Id>();
		set<string> setSyncedProductBasketId = new set<string>();
		Boolean Pass;
				
		for (integer i=0; i < lstNewPB.size(); ++i){
			Pass=false;
			
			if (lstOldPB==null){
				if  (lstNewPB[i] != null && lstNewPB[i].get(ProductUtility.GetSyncField()) != null &&
					(Boolean) lstNewPB[i].get(ProductUtility.GetSyncField()))
					Pass=true;
			}
			else
			{
				Boolean OldSync = (Boolean) lstOldPB[i].get(ProductUtility.GetSyncField());
				Boolean NewSync = (Boolean) lstNewPB[i].get(ProductUtility.GetSyncField());
				if (NewSync && !OldSync)
					Pass=true;
			}
			
			if ((Pass) && (lstNewPB[i].cscfga__Opportunity__c!=null)){
				setOpportunityId.add(lstNewPB[i].cscfga__Opportunity__c);
				setSyncedProductBasketId.add(lstNewPB[i].Id);
			}
		}
		
		if (setOpportunityId.size()>0){
			String SyncField = ProductUtility.GetSyncField();
			string SOQLQuery = 'SELECT Id,cscfga__opportunity__c, ' + SyncField + ' from cscfga__Product_Basket__c where cscfga__opportunity__c in (' + ProductUtility.CSVFromsetId(setOpportunityId) + ')';
			System.debug('***SOQLQuery=' + SOQLQuery);
			
			list<cscfga__Product_Basket__c> lstAllProductBasket = Database.query(SOQLQuery);

			list<cscfga__Product_Basket__c> lstProductBasketUpdate = new list<cscfga__Product_Basket__c>();
				
			for (cscfga__Product_Basket__c tmpPB : lstAllProductBasket){
				if (!setSyncedProductBasketId.contains(tmpPB.Id)){
					if ((Boolean)(tmpPB.get(SyncField))){
						tmpPB.put(SyncField,false);
						tmpPB.Synchronised_with_Opportunity__c = false;
						lstProductBasketUpdate.add(tmpPB);
					}
				}
			}
			
			if (lstProductBasketUpdate.size()>0)
				update lstProductBasketUpdate;
		}
	}

	@testvisible
	private static void DeleteOLIsProductDetailsAfterUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB){
    	set<string> setUnSyncedProductBasketId = new set<string>();
    	
    	for (integer i=0;i<lstNewPB.size();++i){
			Boolean OldSync = (Boolean) lstOldPB[i].get(ProductUtility.GetSyncField());
			Boolean NewSync = (Boolean) lstNewPB[i].get(ProductUtility.GetSyncField());
			if (NewSync != null && OldSync != null && NewSync != OldSync)
    			setUnSyncedProductBasketId.add(lstNewPB[i].Id);
    	}
    	
    	if (setUnSyncedProductBasketId.size()>0)
    		ProductUtility.DeleteHardOLIs(setUnSyncedProductBasketId); 
    }
    
    @testvisible
    private static void InsertOLIsProductDetailsAfterUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB){
    	set<string> setSyncedProductBasketId = new set<string>();
    	set<string> setSyncedOppId = new set<string>();
    	
    	for (integer i=0;i<lstNewPB.size();++i){

			Boolean OldSync = (Boolean) lstOldPB[i].get(ProductUtility.GetSyncField());
			Boolean NewSync = (Boolean) lstNewPB[i].get(ProductUtility.GetSyncField());

			if (NewSync != null && OldSync != null && NewSync && !OldSync){
    			setSyncedProductBasketId.add(lstNewPB[i].Id);
    			setSyncedOppId.add(lstNewPB[i].cscfga__Opportunity__c);
    		}
    	}
    	
    	if (setSyncedProductBasketId.size()>0){ 		
    		if (setSyncedProductBasketId.size()>0)
    			ProductUtility.CreateOLIs(setSyncedProductBasketId);
    	}
    }
}