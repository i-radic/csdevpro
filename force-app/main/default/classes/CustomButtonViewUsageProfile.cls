global with sharing class CustomButtonViewUsageProfile extends csbb.CustomButtonExt {

    public String performAction (String basketId) {
    	String newUrl = '/apex/c__CS_UsageTotalCalculator?basketId='+basketId+'&basketEdit=true';
        
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    } 
}