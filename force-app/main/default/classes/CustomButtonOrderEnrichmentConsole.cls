global with sharing class CustomButtonOrderEnrichmentConsole extends csbb.CustomButtonExt {
 
    public String performAction (String basketId) {
        String newUrl = '/apex/csoe__NonCommercialSpecifications?basketId=' + basketId;
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    }  
}