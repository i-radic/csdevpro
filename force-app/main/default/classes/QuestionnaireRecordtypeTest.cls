/******************************************************************************************************
Test Class: QuestionnaireRecordtypeSelectclass
Class: QuestionnaireRecordtypeSelectclass
Method:QuestionnaireInsertTest
Description : Test class for viewredirectQuestionnarieclass with req no 310.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    3/12/2015             Jacob Thomas                         class created          
*********************************************************************************************************/
@IsTest(SeeAllData=false)
public class QuestionnaireRecordtypeTest{
    
/******************************************************************************************************
Test Class: QuestionnaireRecordtypeTest
Class: QuestionnaireRecordtypeSelectclass
Method: QuestionnaireRecordtypeTest
Description : Test class for EditredirectQuestionnarieclass with req no 310.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    3/12/2015              Jacob Thomas                          class created          
*********************************************************************************************************/    
 
 
    
    static testMethod void QuestionnaireRecordtypeTest()
    {
        
        ID oppRecortTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Complex').getRecordTypeId();
        ID oppRecortTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Complex').getRecordTypeId();
        Id userProfileId = userinfo.getProfileId();
         
       // Setup test data
      // This code runs as the system user
       User thisUser = [select id from User where id=:userinfo.getUserid()];
      System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
      Profile p = [SELECT Id FROM Profile WHERE Name='BT: Dataloader2']; 
      User u = new User(Alias = 'Test', Email='standarddataloderuser@bt.it', 
      EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      LocaleSidKey='en_US', ProfileId = p.Id, 
      TimeZoneSidKey='America/Los_Angeles', UserName='standarddataloderuser@bt.it', EIN__c='123576848');

            
        System.runAs(u)
        {
        
       
        
        Account accnt =  new Account(name='test',AccountNumber='1234');
        insert accnt;
        
       
           
        Opportunity opp = new Opportunity(AccountId=accnt.Id,RecordTypeId=oppRecortTypeId , Name='opportunityName', StageName='Pre-Qualification', CloseDate=Date.today());
        insert opp;
        
        RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE];
       
        Questionnaire__c objQustion = new Questionnaire__c ();
        objQustion.Opportunity__c = opp.Id; 
        objQustion.RecordTypeId = QuestionaireRecType.Id;
        insert objQustion;
        
        
        test.starttest ();     
        ApexPages.StandardController cntrl  = new ApexPages.StandardController(objQustion);
        QuestionnaireRecordtypeSelectclass objCls = new QuestionnaireRecordtypeSelectclass(cntrl);
        objCls.recordtype='Qualification';
        objCls.gotonewpage();
        objCls.getmyOptions();
        objCls.recordtype='Profiler';
        objCls.gotonewpage();
        objCls.recordtype='Profiler1';
        objCls.gotonewpage();
            
      
        RecordType QuestionaireRecType1 = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE];
        Questionnaire__c objQustion1 = new Questionnaire__c ();
        objQustion1.Opportunity__c = opp.Id; 
        objQustion1.RecordTypeId = QuestionaireRecType1.Id;
        insert objQustion1;
        
        
        ApexPages.StandardController cntrl1  = new ApexPages.StandardController(objQustion1);
        QuestionnaireRecordtypeSelectclass  objCls1 = new QuestionnaireRecordtypeSelectclass (cntrl1);
        objCls1.recordtype='Profiler';
        objCls1.gotonewpage();
        
        
        opp.stagename=GlobalConstants.OPPORTUNITY_STAGE_DEVELOP_SOLUTION;
        opp.CSC_Call_Pass__c=true;
        update opp;   
        objQustion1.Gate__c='Sign Off';
        update objQustion1;
        ApexPages.StandardController cntrl2  = new ApexPages.StandardController(objQustion1);
        QuestionnaireRecordtypeSelectclass  objCls2 = new QuestionnaireRecordtypeSelectclass (cntrl2);
        objCls2.recordtype='Qualification';
        objCls2.gotonewpage();
        objCls2.cancel();
        
        opp.RecordtypeId =oppRecortTypeId1 ;
       opp.stagename=GlobalConstants.OPPORTUNITY_STAGE_CLOSE_WON;
        update opp;
         ApexPages.StandardController cntrl3  = new ApexPages.StandardController(objQustion1);
        QuestionnaireRecordtypeSelectclass  objCls3 = new QuestionnaireRecordtypeSelectclass (cntrl3);
        objCls3.recordtype='Qualification';
        objCls3.gotonewpage();
                           
        test.stopTest(); 
        } 
        
    }  
}