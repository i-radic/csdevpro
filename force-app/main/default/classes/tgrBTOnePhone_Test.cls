@isTest
private class tgrBTOnePhone_Test {

    static testMethod void myUnitTest() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }
   Date uDate = date.today().addDays(7);
   RecordType BTOPOppyType = [select id from RecordType where SobjectType='Opportunity' and name ='BT OnePhone' limit 1];
   
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BTOP';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.RecordTypeId = BTOPOppyType.Id;
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});  
        
        BT_One_Phone__c btop1 = new BT_One_Phone__c();
        btop1.Opportunity__c = oppResult[0].id; 
        btop1.Volume_of_Data_SIMs__c = 5;
        btop1.Contract_Duration__c = '24 Months';
        btop1.Contract_Value__c = 10;
        btop1.OnePhone_Product_Type__c = 'BT One Phone Mobile';
        btop1.Existing_Customer_Resign__c  = 'Y';
        Database.SaveResult[] btop1Result = Database.insert(new BT_One_Phone__c [] {btop1});  
        
        
                
        BT_One_Phone_Site__c btops1 = new BT_One_Phone_Site__c();
        btops1.BT_One_Phone__c = btop1Result[0].id;
         btops1.Contract_Duration__c = '24 Months';
        btops1.Site_Telephone_Number_Landline__c ='01234567890';
        btops1.Number_of_users__c = 1;
        btops1.Number_Concurrent_Calls_Expected_at_Site__c = 10;
        btops1.Number_of_Users_in_Parallel_Hunt_Group__c = '5 or Less';
        btops1.mobilecoverageResults__c = 'Excellent Indoor & Outdoor';
        btops1.Post_Code__c ='WR5 3RL';
        btops1.Site_Diagram_or_Sketch_Attached__c = true;
        
        Contact btops1Contact = Test_Factory.CreateContact();
        insert btops1Contact;
        btops1.Onsite_mobile_network__c=true;
        btops1.Customer_Contact__c = btops1Contact.Id;
        Database.SaveResult[] btops1Result = Database.insert(new BT_One_Phone_Site__c [] {btops1});  
        
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        Attachment attachment=new Attachment(); 
        attachment.Name='An attachment 1';
        attachment.body=bodyBlob; 
        attachment.parentId=btop1Result[0].id;
        insert attachment;
        
        
        Attachment attachment1=new Attachment(); 
        attachment1.Name='An attachment 1';
        attachment1.body=bodyBlob; 
        attachment1.parentId=btops1Result[0].id;
        insert attachment1;
        
        
       
        
        
        
    }
}