@isTest
public class CustomUsageProfileLookupTest {
    public static testMethod void TestUsageProfile() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Test.StartTest();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account acc = CS_TestDataFactory.generateAccount(True,'TestingAccount');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(True, 'TestOpp',acc);
        Usage_Profile__c usProf = CS_TestDataFactory.generateUsageProfile(True,acc);
        usProf.Is_Default_Profile__c = true;
        update usProf;
        cscfga__Product_Basket__c bskt = CS_TestDataFactory.generateProductBasket(True, 'TestBskt',opp);
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(True,'BT Mobile');
        
         cscfga__Attribute_Definition__c attrDef1 =
            new cscfga__Attribute_Definition__c
                ( Name = 'AccountId'
                , cscfga__Product_Definition__c = prodDef.Id );
        insert attrDef1;
        
        cscfga__Attribute_Definition__c attrDef2 =
            new cscfga__Attribute_Definition__c
                ( Name = 'Proposition Name'
                , cscfga__Product_Definition__c = prodDef.Id );
        insert attrDef2;
        
        cscfga__Attribute_Definition__c attrDef3 =
            new cscfga__Attribute_Definition__c
                ( Name = 'AccountsUsageProfileId'
                , cscfga__Product_Definition__c = prodDef.Id );
        insert attrDef3;

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(True,'BT Mobile Flex', bskt);
        cscfga__Attribute__c attr1 = new cscfga__Attribute__c(Name = 'AccountId', cscfga__Value__c = acc.id, cscfga__Attribute_Definition__c = attrDef1.ID,
                cscfga__Product_Configuration__c = pc.ID, cscfga__is_active__c = true);  
        insert attr1;            
        cscfga__Attribute__c attr2 = new cscfga__Attribute__c(Name = 'Proposition Name', cscfga__Value__c = 'Mobile Sharer', cscfga__Attribute_Definition__c = attrDef2.ID,
                cscfga__Product_Configuration__c = pc.ID, cscfga__is_active__c = true);  
        insert attr2;             
        cscfga__Attribute__c attr3 = new cscfga__Attribute__c(Name = 'AccountsUsageProfileId', cscfga__Value__c = usProf.id, cscfga__Attribute_Definition__c = attrDef3.ID,
                cscfga__Product_Configuration__c = pc.ID, cscfga__is_active__c = true);              
        insert attr3; 
        
        CustomUsageProfileLookup customObj = new CustomUsageProfileLookup();
         Map<String, String> searchFields = new Map<String, String>();
         Id[] excludeIds;
         searchFields.put('AccountId',acc.id);
         searchFields.put('Proposition Name','Mobile Sharer');
         searchFields.put('AccountsUsageProfileId',usProf.id);
        String res1 = customObj.getRequiredAttributes();
        Object[] res2 = customObj.doDynamicLookupSearch(searchFields,prodDef.id);
        Object[] res3 = customObj.doLookupSearch(searchFields, prodDef.id,excludeIds, 1, 1);
        Test.StopTest();
	}
}