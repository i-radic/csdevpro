public class CS_CloudVoice_AddressCtrl {

	Account accountObj { get; set; }
	public Boolean displaySaveButton { get; set; }
	public Boolean foundAddress { get; set; }
	public boolean displayNADAddressSection { get; set; }
	public boolean displayNADAddressResults { get; set; }
	public boolean displayOrderAddressSection { get; set; }
	public Boolean isshow { get; set; }
	public List<Order_Address__c> orderAddressListWrapper { get; set; }
	public SalesforceCRMService.CRMAddressList crmAddressListWrapper { get; set; }
	public AddressDataWrapper selectedAddressObj { get; set; }
	public String selectedAddressidentifier { get; set; }
	public List<AddressDataWrapper> addressDataWrapperList { get; set; }
	public String addressString { get; set; }
	public String addressId { get; set; }
	public String errorMessage { get; set; }
	public String postCode { get; set; }
	public String nadKey {get; set;}
	public String configGuid {get; set;}
	public String addressType {get; set;}

	@TestVisible
	class AddressDataWrapper {
		public String Account { get; set; }
		public String Id { get; set; }
		public Boolean selectedAddress { get; set; }
		public String IdentifierId { get; set; }
		public String IdentifierName { get; set; }
		public String IdentifierValue { get; set; }
		public String Country { get; set; }
		public String County { get; set; }
		public String Name { get; set; }
		public String POBox { get; set; }
		public String BuildingNumber { get; set; }
		public String BuildingName { get; set; }
		public String Street { get; set; }
		public String Locality { get; set; }
		public String DoubleDependentLocality { get; set; }
		public String PostCode { get; set; }
		public String Town { get; set; }
		public String SubBuilding { get; set; }
		public String ExchangeGroupCode { get; set; }
		public String Easting { get; set; }
		public String Northing { get; set; }
		public String ExchangeDistID { get; set; }

		public AddressDataWrapper() {
		}

		public AddressDataWrapper(SalesforceCRMService.CRMAddress crmAddressObj) {
			this.IdentifierId = crmAddressObj.IdentifierId;
			this.IdentifierName = crmAddressObj.IdentifierName;
			this.IdentifierValue = crmAddressObj.IdentifierValue;
			this.Country = crmAddressObj.Country;
			this.County = crmAddressObj.County;
			this.Name = crmAddressObj.Name;
			this.POBox = crmAddressObj.POBox;
			this.BuildingNumber = crmAddressObj.BuildingNumber;
			this.BuildingName = crmAddressObj.BuildingName;
			this.Street = crmAddressObj.Street;
			this.Locality = crmAddressObj.Locality;
			this.DoubleDependentLocality = crmAddressObj.DoubleDependentLocality;
			this.PostCode = crmAddressObj.PostCode;
			this.Town = crmAddressObj.Town;
			this.SubBuilding = crmAddressObj.SubBuilding;
			this.ExchangeGroupCode = crmAddressObj.ExchangeGroupCode;
			this.Easting = crmAddressObj.Easting;
			this.Northing = crmAddressObj.Northing;
			this.ExchangeDistID = crmAddressObj.ExchangeDistID;
		}

		public AddressDataWrapper(Order_Address__c orderAddressObj) {
			this.Id = orderAddressObj.Id;
			this.Account = orderAddressObj.Account__c;
			this.BuildingName = orderAddressObj.Building_Name__c;
			this.Country = orderAddressObj.Country__c;
			this.County = orderAddressObj.County__c;
			this.Locality = orderAddressObj.Locality__c;
			this.IdentifierValue = orderAddressObj.NAD__c;
			this.BuildingNumber = orderAddressObj.Number__c;
			this.POBox = orderAddressObj.PO_Box__c;
			this.PostCode = orderAddressObj.Post_Code__c;
			this.Town = orderAddressObj.Post_Town__c;
			this.Street = orderAddressObj.Street__c;
			this.SubBuilding = orderAddressObj.Sub_Building__c;
		}
	}

	public CS_CloudVoice_AddressCtrl(ApexPages.StandardController stdController) {
		displayOrderAddressSection = true;
		displayNADAddressSection = false;
		displayNADAddressResults = false;
		displaySaveButton = true;
		//accountObj = (Account) stdController.getRecord();

		String basketId = System.currentPageReference().getParameters().get('basketId');
		configGuid = System.currentPageReference().getParameters().get('configGuid');
		addressType = System.currentPageReference().getParameters().get('addressType');
		cscfga__Product_Basket__c basket = [select id, csbb__Account__c from cscfga__Product_Basket__c where id = :basketId];
		Account account = [select Id from Account where Id = :basket.csbb__Account__c];
		accountObj = account;

		selectedAddressidentifier = '';
		addressString = '';
		addressId = '';
		errorMessage = '';

		//String recId = ApexPages.currentPage().getParameters().get('Id');
	}

	public PageReference getOrderAddressDetails() {
		try {
			orderAddressListWrapper = [Select Id, Account__c, Building_Name__c, Country__c, County__c, Locality__c, NAD__c, Number__c, PO_Box__c, Post_Code__c, Post_Town__c, Street__c, Sub_Building__c FROM Order_Address__c WHERE Account__c = :accountObj.Id];
		} catch(Exception e) {
			return null;
		}
		AddressDataWrapper addressDataWrapper;
		addressDataWrapperList = new List<AddressDataWrapper> ();

		if (orderAddressListWrapper != null) {
			displayOrderAddressSection = true;
			List<Order_Address__c> AddressList = orderAddressListWrapper;
			if (!AddressList.isEmpty()) {
				Integer c = 0;
				for (Order_Address__c addressObj : AddressList) {
					addressDataWrapper = new AddressDataWrapper(addressObj);
					if (c == 0) {
						addressDataWrapper.selectedAddress = true;
					}
					if (String.isNotBlank(addressDataWrapper.IdentifierValue)) {
						addressDataWrapperList.add(addressDataWrapper);

						if (selectedAddressidentifier != null && addressDataWrapper.IdentifierValue == selectedAddressidentifier) {
							selectedAddressObj = new AddressDataWrapper();
							selectedAddressObj = addressDataWrapper;
							foundAddress = true;
						}
						c++;
					}
				}
			}
		}
		if (!addressDataWrapperList.isEmpty() && foundAddress == false) {
			selectedAddressObj = new AddressDataWrapper();
			selectedAddressObj = addressDataWrapperList[0];
			selectedAddressidentifier = selectedAddressObj.IdentifierValue;
		}
		return null;
	}

	public PageReference getNADAddressDetails() {
		String postCode = postCode;
		displaySaveButton = true;
		foundAddress = false;

		if(!Test.isRunningTest()){
			crmAddressListWrapper = SalesforceCRMService.GetNADAddress(postCode);
		}
		AddressDataWrapper addressDataWrapper;
		addressDataWrapperList = new List<AddressDataWrapper> ();

		if (String.isBlank(postCode)) {
			ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter Post Code'));
			return null;
		}
		if (crmAddressListWrapper != null) {
			displayNADAddressResults = true;
			List<SalesforceCRMService.CRMAddress> AddressList = crmAddressListWrapper.Addresses.Address;
			if (!AddressList.isEmpty()) {
				Integer c = 0;
				for (SalesforceCRMService.CRMAddress crmAddressObj : AddressList) {
					addressDataWrapper = new AddressDataWrapper(crmAddressObj);
					if (c == 0) {
						addressDataWrapper.selectedAddress = true;
					}
					if (String.isNotBlank(addressDataWrapper.IdentifierValue)) {
						addressDataWrapperList.add(addressDataWrapper);

						if (selectedAddressidentifier != null && addressDataWrapper.IdentifierValue == selectedAddressidentifier) {
							selectedAddressObj = new AddressDataWrapper();
							selectedAddressObj = addressDataWrapper;
							foundAddress = true;
						}
						c++;
					}
				}
			}
		}
		if (!addressDataWrapperList.isEmpty() && foundAddress == false) {
			selectedAddressObj = new AddressDataWrapper();
			selectedAddressObj = addressDataWrapperList[0];
			selectedAddressidentifier = selectedAddressObj.IdentifierValue;
		}
		return null;
	}

	public Void addSelectedNADAddress() {
		selectedAddressObj = new AddressDataWrapper();
		for (AddressDataWrapper addressDataWrapperObj : addressDataWrapperList) {
			if (addressDataWrapperObj.IdentifierValue == selectedAddressidentifier) {
				selectedAddressObj = addressDataWrapperObj;
				isshow = true;

				Order_Address__c address = new Order_Address__c();
				address.Account__c = accountObj.Id;
				address.Building_Name__c = selectedAddressObj.BuildingName;
				address.Country__c = selectedAddressObj.Country;
				address.County__c = selectedAddressObj.County;
				address.Locality__c = selectedAddressObj.Locality;
				address.NAD__c = selectedAddressObj.IdentifierValue;
				address.Number__c = selectedAddressObj.BuildingNumber;
				address.PO_Box__c = selectedAddressObj.POBox;
				address.Post_Code__c = selectedAddressObj.PostCode;
				address.Post_Town__c = selectedAddressObj.Town;
				address.Street__c = selectedAddressObj.Street;
				address.Sub_Building__c = selectedAddressObj.SubBuilding;

				try {
					insert address;
					addressId = address.Id;
					addressString = selectedAddressObj.SubBuilding + ' ' + selectedAddressObj.BuildingName + ' ' + selectedAddressObj.BuildingNumber + ' ' + selectedAddressObj.Street + ' ' + selectedAddressObj.Town;
					addressString = addressString.replace('null', '');
					nadKey = selectedAddressObj.IdentifierValue;
					errorMessage = '';
				}
				catch(System.DmlException dmlEx) {
					errorMessage = 'Address already added!';
				}
			}
		}
	}

	public Void addSelectedOrderAddress() {
		selectedAddressObj = new AddressDataWrapper();
		for (AddressDataWrapper addressDataWrapperObj : addressDataWrapperList) {
			if (addressDataWrapperObj.IdentifierValue == selectedAddressidentifier) {
				selectedAddressObj = addressDataWrapperObj;
				isshow = true;

				// ######### NOTE - Delivery address will default to Install address!
				addressString = selectedAddressObj.SubBuilding + ' ' + selectedAddressObj.BuildingName + ' ' + selectedAddressObj.BuildingNumber + ' ' + selectedAddressObj.Street + ' ' + selectedAddressObj.Town;
				addressString = addressString.replace('null', '');
				addressId = selectedAddressObj.Id;
				nadKey = selectedAddressObj.IdentifierValue;
				errorMessage = '';
			}
		}
	}

	public PageReference displayNADAddressSection() {
		displayNADAddressSection = true;
		displayOrderAddressSection = false;
		//page.getParameters().put('Test', 'test2');
		return null;
	}
}