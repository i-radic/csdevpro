/**
* Apex class used to insert HTML elements for Usage profile
*
*/
public class CS_UsageProfileCustomiserExtension implements CS_ICustomiserExtension {
	
	/**
	 * Method returns html representation of usage charges and costs. This will be mapped
	 * to atributes using JS actions. 
	 *
	 * Usage Profile is taken either from Account or from the 
	 * default one (lookup on Service Plan). Rate Card with Rate Card Lines are linked through 
	 * Price Item Rate Card association. If there is one linked to the Account, that one will be used
	 * instead of default one. 
	 *
	 * @param ctrl
	 * @return String html to insert on configurator page
	 */ 
	public String getTopComponentHtml(cscfga.ProductConfiguratorController ctrl) {
		
		cscfga.ProductConfiguration pc = ctrl.getRootConfig();
		String servicePlanId;
		String accountId;
		String associatedLe;
		List<Schema.FieldSetMember> voiceFields = SObjectType.Usage_Profile__c.FieldSets.Voice.getFields();
		List<Schema.FieldSetMember> smsFields = SObjectType.Usage_Profile__c.FieldSets.SMS.getFields();
		List<Schema.FieldSetMember> dataFields = SObjectType.Usage_Profile__c.FieldSets.Data.getFields();
		cscfga__Product_Configuration__c prodConfig = pc.getSObject();
		cscfga__Product_Basket__c basket = ctrl.basket;
		Opportunity opp = [select Id, AccountId, AssociatedLE__c from Opportunity where id = :basket.cscfga__Opportunity__c];
		// edit and new behave differnetly -> need to re-fetch object if it's in edit mode
		System.debug(LoggingLevel.ERROR, pc.getSObject());
		if (pc.getSObject().Id == null) {
			if (pc.containsAttribute('Service Plan Level')) {
				servicePlanId = pc.getAttribute('Service Plan Level').getSObject().cscfga__Value__c;
			}
			//servicePlanId = prodConfig.Service_Plan__c;
			accountId = opp.AccountId;
			associatedLe = opp.AssociatedLE__c;
		} else {
			List<cscfga__Product_Configuration__c> pcList = [Select Id, Service_Plan__c, Account__c from cscfga__Product_Configuration__c where Id = :pc.getSObject().Id];
			if (!pcList.isEmpty()) {
				prodConfig =  pcList[0];
				servicePlanId = prodConfig.Service_Plan__c;
				accountId = opp.AccountId;
				associatedLe = opp.AssociatedLE__c;
			}
		}
		System.debug('servicePlanId' + servicePlanId);
		system.debug('accountId + ' + ctrl.basket);
		String html = 'null';
		Map<String, Decimal> usageTotals = new Map<String, Decimal>();
		Map<String, Decimal> expectedUsage = new Map<String, Decimal>();
		if (servicePlanId != null) {
			List<cspmb__Price_Item__c> priceItems = [Select Id, Default_Usage_Profile__c from cspmb__Price_Item__c where Id = :servicePlanId];
			cspmb__Price_Item__c servicePlan;
			if (!priceItems.isEmpty()) {
				servicePlan = priceItems[0];
				List<Usage_Profile__c> usageProfiles = Database.query('select ' 
					+ CS_Utils.getSobjectFields('Usage_Profile__c') + ' from Usage_Profile__c where Id = \'' 
					+ servicePlan.Default_Usage_Profile__c + '\'' + 
					'or (AssociatedLE__c = \'' + associatedLe + '\' AND Active__c = true)');
				Usage_Profile__c accProfile;
				for (Usage_Profile__c up : usageProfiles) {
					if (up.Account__c != null) {
						accProfile = up;
					}
				}
				List<cspmb__Price_Item_Rate_Card_Association__c> rateCardAssociations = [Select 
					Id, cspmb__Rate_Card__c, cspmb__Price_Item__c, cspmb__Rate_Card__r.cspmb__Account__c 
					from cspmb__Price_Item_Rate_Card_Association__c 
					where cspmb__Price_Item__c = :servicePlanId];
				Id defaultRateCard;
				Id accountRateCard;
				for (cspmb__Price_Item_Rate_Card_Association__c rca : rateCardAssociations) {
					if (rca.cspmb__Rate_Card__r.cspmb__Account__c == accountId) {
						accountRateCard = rca.cspmb__Rate_Card__c;
					} else if (rca.cspmb__Rate_Card__r.cspmb__Account__c == null) {
						defaultRateCard = rca.cspmb__Rate_Card__c;
					}
				}
				Id rateCardId; 
				if (accountRateCard != null) {
					rateCardId = accountRateCard;
				} else {
					rateCardId = defaultRateCard;
				}
				List<cspmb__Rate_Card_Line__c> rateCardLines = new List<cspmb__Rate_Card_Line__c>();
				Map<String, cspmb__Rate_Card_Line__c> rateCardLinesMap = new Map<String, cspmb__Rate_Card_Line__c>();
				if (!rateCardAssociations.isEmpty()) {
					rateCardLines = Database.query('select ' + CS_Utils.getSobjectFields('cspmb__Rate_Card_Line__c') + 
						' from cspmb__Rate_Card_Line__c where cspmb__Rate_Card__c = \'' + 
						rateCardId + '\'');
					for (cspmb__Rate_Card_Line__c rl : rateCardLines) {
						rateCardLinesMap.put(rl.Name, rl);
					}
				}
				if (!usageProfiles.isEmpty()) {
					Usage_Profile__c usageProfile;
					if (accProfile != null) {
						usageProfile = accProfile;
					} else {
						usageProfile = usageProfiles[0];
					}
					// check if there is basket level Usage Profile
					List<Attachment> usageProfileAttachment = [
						select Id, Body, ParentId, Name
						from Attachment
						where ParentId = :basket.Id
						and Name = 'UsageProfile'
					];
					if (!usageProfileAttachment.isEmpty()) {
						List<Usage_Profile__c> usageProfileList = (List<Usage_Profile__c>) JSON.deserialize(usageProfileAttachment.get(0).Body.toString(), List<Usage_Profile__c>.class);
						usageProfile = usageProfileList[0];
					}
					expectedUsage.put('SMS', usageProfile.Expected_SMS_per_User__c);
					expectedUsage.put('Voice', usageProfile.Expected_Voice_per_User__c);
					
					for (Schema.FieldSetMember flm : voiceFields) {
						String costPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost__c';
						String chargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge__c';
						String totalChargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge_0';
						String totalCostPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost_0';
						Decimal totalCost = 0;
						Decimal totalCharge = 0 ;
						if (usageProfile.get('Expected_Voice_per_User__c') != null && usageProfile.get(flm.getFieldPath()) != null) {
							totalCharge = (Decimal) usageProfile.get('Expected_Voice_per_User__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
							totalCost = (Decimal) usageProfile.get('Expected_Voice_per_User__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
						}
						if (rateCardLinesMap.get('Voice') != null) {
							Decimal chargeMultiplier = 0;
							Decimal costMultiplier = 0;
							if (rateCardLinesMap.get('Voice').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('Voice').get(chargePath);
							}
							
							if (rateCardLinesMap.get('Voice').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('Voice').get(costPath);
							}
							
							totalCharge *= chargeMultiplier / 100;
							totalCost *= costMultiplier / 100;
						}
						usageTotals.put(totalChargePath, totalCharge.setScale(2));
						usageTotals.put(totalCostPath, totalCost.setScale(2));
					}
					for (Schema.FieldSetMember flm : smsFields) {
						String costPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost__c';
						String chargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge__c';
						String totalChargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge_0';
						String totalCostPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost_0';
						Decimal totalCharge = 0;
						Decimal totalCost = 0;
						if (usageProfile.get('Expected_SMS_per_User__c') != null && usageProfile.get(flm.getFieldPath()) != null) {
							totalCharge = (Decimal) usageProfile.get('Expected_SMS_per_User__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
							totalCost = (Decimal) usageProfile.get('Expected_SMS_per_User__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
						}
						if (rateCardLinesMap.get('SMS') != null) {
							Decimal chargeMultiplier = 0;
							Decimal costMultiplier = 0;
							if (rateCardLinesMap.get('SMS').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('SMS').get(chargePath);
							}
							if (rateCardLinesMap.get('SMS').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('SMS').get(costPath);
							}
							totalCharge *= chargeMultiplier / 100;
							totalCost *= costMultiplier / 100;
						}
						usageTotals.put(totalChargePath, totalCharge.setScale(2));
						usageTotals.put(totalCostPath, totalCost.setScale(2));
					}
					for (Schema.FieldSetMember flm : dataFields) {
						String costPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost__c';
						String chargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge__c';
						String totalChargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge_0';
						String totalCostPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost_0';
						Decimal totalCharge = 0;
						Decimal totalCost = 0;
						if (usageProfile.get('Expected_Data_per_User__c') != null && usageProfile.get(flm.getFieldPath()) != null) {
							totalCharge = (Decimal) usageProfile.get('Expected_Data_per_User__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
							totalCost = (Decimal) usageProfile.get('Expected_Data_per_User__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
						}
						if (rateCardLinesMap.get('Data') != null) {
							Decimal chargeMultiplier = 0;
							Decimal costMultiplier = 0;
							if (rateCardLinesMap.get('Data').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('Data').get(chargePath);
							}
							if (rateCardLinesMap.get('Data').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('Data').get(costPath);
							}
							totalCharge *= chargeMultiplier / 100;
							totalCost *= costMultiplier / 100;
						}
						usageTotals.put(totalChargePath, totalCharge.setScale(2));
						usageTotals.put(totalCostPath, totalCost.setScale(2));
					}
					html = JSON.serialize(usageTotals);
				}
			}
		}
		String expUsage = JSON.serialize(expectedUsage);
		html = 'usageTotals = ' + html + '; ';
		html += 'var expectedUsage = ' + expUsage + '; ';
		html += 'function setAttributeValues(usageTotals)'; 
		html += '{var attributeName = jQuery(\'[id$=\"Name_0\"]\').attr(\'name\');';
		html += 'var childPrefix = attributeName.substr(0, attributeName.indexOf(\':\'));';
		html += 'usageTotalKeys = Object.keys(usageTotals);';
		html += 'for (key in usageTotalKeys) ';
		html += '{var fieldName = childPrefix + \":\" + usageTotalKeys[key];';
		html += 'if (fieldName.indexOf(\"function\") == -1)';
		html += '{CS.setAttribute(fieldName, usageTotals[usageTotalKeys[key]]);}}}';
		return html;
	}
	
	/**
	 * Does nothing
	 *
	 */
	public void afterSave(cscfga.ProductConfiguratorController ctrl) {
		
	}
}