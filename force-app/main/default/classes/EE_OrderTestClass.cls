/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest //(SeeAllData=true)
public class EE_OrderTestClass {

   /*
    * @name         createCustomSettings
    * @description	creates the Custom Settings needed for the Order Request code
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    public static void createCustomSettings() {
    	
    	// Set the Trigger Enabler Custom Setting for Order Item
        List<TriggerEnabler__c> lListTriggerEnabler = [SELECT Id, Name, TriggerEnabled__c FROM TriggerEnabler__c WHERE Name='OrderItem' LIMIT 1]; 
    	if(lListTriggerEnabler.isEmpty()){
    		TriggerEnabler__c trgEnabler = EE_TestClass.createTriggerEnabler('OrderItem', true);
			insert trgEnabler;    		
    	}

		// Set the Trigger Enabler Custom Setting for Order Request Items
        List <OrderRequest_RecordTypeSettings__c> lListOrRctSettings = [SELECT Id FROM OrderRequest_RecordTypeSettings__c WHERE Name='Welcome Day' LIMIT 1 ]; 
    	if(lListOrRctSettings.isEmpty()){
	        OrderRequest_RecordTypeSettings__c lOrRct = new OrderRequest_RecordTypeSettings__c();
	        lOrRct.Name 				= 'Welcome Day';
	        lOrRct.Record_Type_Name__c 	= 'Welcome_Day';
	        lOrRct.put('Supplier_Email__c', 'sales.enablers@ee.co.uk');
	        lOrRct.Product_Name__c		= 'Welcome Day';
	        lOrRct.Record_Type_Label__c	= 'Welcome Day';
			insert lOrRct;  		
    	}
    	
    }

   /*
    * @name         createProductRecords
    * @description	creates the Product, Pricebook and PricebookEntries needed for Orders
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    public static void createProductRecords() {
    	
    	Product2 lPrdWelcomeDay, lPrdDeviceCustomisation;
    	Pricebook2 lPbkServices;
    	
    	PricebookEntry lPbeWelcomeDay, lPbeDeviceCustomisation;
		PricebookEntry lPbeStdWelcomeDay, lPbeStdDeviceCustomisation;
		//Pricebook2 lPbkStandard;

        // Standard Price Book
        Pricebook2 lPbkStandard = [SELECT Id, Name, isActive FROM Pricebook2 WHERE isStandard = true LIMIT 1];
        //if(!lListPbkStandard.isEmpty()){
	        if (!lPbkStandard.isActive) {
	        	lPbkStandard.isActive = true;
	        	update lPbkStandard;
	    	}        	
        //}else{
        //	lPbkStandard = new Pricebook2(Name='Standard Price Book', isActive=true, isStandard=true );
    	//	insert lPbkStandard;
        //}

    	
    	// Create Welcome Day Product
    	Id lIdPrdWelcomeDay = getProductId('Welcome Day');
    	if( lIdPrdWelcomeDay == null ){
    		lPrdWelcomeDay = new Product2(Name='Welcome Day', isActive=true, Bundled_Service__c='Corporate');
    		insert lPrdWelcomeDay;
    		lIdPrdWelcomeDay = lPrdWelcomeDay.Id;
    	}    	
    	
    	// Create Device Customisation Standard Product
    	Id lIdPrdDeviceCustomisation = getProductId('Device Customisation Standard');
    	if( lIdPrdDeviceCustomisation == null ){
    		lPrdDeviceCustomisation = new Product2(Name='Device Customisation Standard', isActive=true, Bundled_Service__c='Corporate; Enterprise');
    		insert lPrdDeviceCustomisation;
    		lIdPrdDeviceCustomisation = lPrdDeviceCustomisation.Id;
    	} 
    	    	
    	// Create Pricebook
    	Id lIdPbkServices = getPricebookId('Services Price Book');
    	if( lIdPbkServices == null ){
    		lPbkServices = new Pricebook2(Name='Services Price Book', isActive=true);
    		insert lPbkServices;
    		lIdPbkServices = lPbkServices.Id;
    	}
    	
    	// Create Standard PricebookEntry - Welcome Day
    	Id lIdPbeStdWelcomeDay = getPricebookEntryId('Welcome Day', lPbkStandard.Name);
    	if( lIdPbeStdWelcomeDay == null ){
    		lPbeStdWelcomeDay = new PricebookEntry(Product2Id=lIdPrdWelcomeDay, Pricebook2Id=lPbkStandard.Id, UnitPrice=0);
    		insert lPbeStdWelcomeDay;
    	}
    	
    	// Create PricebookEntry - Welcome Day
    	Id lIdPbeWelcomeDay = getPricebookEntryId('Welcome Day', 'Services Price Book');
    	if( lIdPbeWelcomeDay == null ){
    		lPbeWelcomeDay = new PricebookEntry(Product2Id=lIdPrdWelcomeDay, Pricebook2Id=lIdPbkServices, UnitPrice=0);
    		insert lPbeWelcomeDay;
    	}
    	
    	// Create Standard PricebookEntry - Device Customisation Standard
    	Id lIdPbeStdDeviceCustomisation = getPricebookEntryId('Device Customisation Standard', lPbkStandard.Name);
    	if( lIdPbeStdDeviceCustomisation == null ){
    		lPbeStdDeviceCustomisation = new PricebookEntry(Product2Id=lIdPrdDeviceCustomisation, Pricebook2Id=lPbkStandard.Id, UnitPrice=0);
    		insert lPbeStdDeviceCustomisation;
    	}
    	
    	// Create PricebookEntry - Device Customisation Standard
    	Id lIdPbeDeviceCustomisation = getPricebookEntryId('Device Customisation Standard', 'Services Price Book');
    	if( lIdPbeDeviceCustomisation == null ){
    		lPbeDeviceCustomisation = new PricebookEntry(Product2Id=lIdPrdDeviceCustomisation, Pricebook2Id=lIdPbkServices, UnitPrice=0);
    		insert lPbeDeviceCustomisation;
    	}
    	
    	
    }


   /*
    * @name         getPricebookEntryId
    * @description	returns the PricebookEntryId for the passed Product and Pricebook
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    public static Id getPricebookEntryId(String psProductName, String psPricebook) {

        Id lIdReturn = null;
        
        // Get the Product
        //List<Product2> lListProducts = [SELECT Id, Name, isActive FROM Product2 WHERE Name=:psProductName AND isActive=true LIMIT 1 ];        

        // Get the Pricebook
        //List<Pricebook2> lListPricebooks = [SELECT Id, Name, isActive FROM Pricebook2 WHERE Name=:psPricebook AND isActive=true LIMIT 1 ];
        
        Id lIdPrd = getProductId(psProductName);
        
        Id lIdPbk = getPricebookId(psPricebook);
 
        // Get the PricebookEntryId
        //List<PricebookEntry> lListPricebookEntries = [SELECT Id, Name, isActive FROM PricebookEntry WHERE Product2Id = :lListProducts[0].Id AND Pricebook2Id =:lListPricebooks[0].Id AND isActive=true LIMIT 1 ];
        List<PricebookEntry> lListPricebookEntries = [SELECT Id, Name, isActive FROM PricebookEntry WHERE Product2Id = :lIdPrd AND Pricebook2Id =:lIdPbk AND isActive=true LIMIT 1 ];
        
        //return lListPricebookEntries[0].Id;

        if(!lListPricebookEntries.isEmpty()){
        	lIdReturn = lListPricebookEntries[0].Id;
        }


        return lIdReturn;
        
	}

   /*
    * @name         getProductId
    * @description	returns the ProductId for the passed Product Name
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    public static Id getProductId(String psProductName) {
        // Get the Pricebook
        Id lIdReturn = null;
        List<Product2> lListProducts = [SELECT Id, Name, isActive FROM Product2 WHERE Name=:psProductName AND isActive=true LIMIT 1];
        if(!lListProducts.isEmpty()){
        	lIdReturn = lListProducts[0].Id;
        }
        return lIdReturn;
	}

   /*
    * @name         getPricebookId
    * @description	returns the PricebookId for the passed Pricebook
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    public static Id getPricebookId(String psPricebook) {
        // Get the Pricebook
        Id lIdReturn = null;
        List<Pricebook2> lListPricebooks = [SELECT Id, Name, isActive FROM Pricebook2 WHERE Name=:psPricebook AND isActive=true LIMIT 1 ];
        if(!lListPricebooks.isEmpty()){
        	lIdReturn = lListPricebooks[0].Id;
        }
        return lIdReturn;
	}


   /*
    * @name         createStaticOrderLevel
    * @param		psProductOrderType = 'Welcome Day | Included Services'
    * @description	creates an Included Services Welcome Day
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    public static Order_Level__c createStaticOrderLevel(Account psAccount, Decimal pdQuantity, String psProductOrderType) {

        Order_Level__c lOrderLevel = new Order_Level__c();
		lOrderLevel.Is_Welcome_Day_Quantity_To_Be_Summed__c = true;
		lOrderLevel.Company__c 				= psAccount.Id;
        lOrderLevel.Product_Order_Type__c	= psProductOrderType;
		lOrderLevel.Quantity__c				= pdQuantity;
        return lOrderLevel;
    }
    
    


}