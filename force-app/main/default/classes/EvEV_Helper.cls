/*
###########################################################################
# Project Name..........: EveryThing Everywhere
# File..................: EvEV_Helper.cls
# Version...............: 1
# Created by............: Kanishk Prasad
# Created Date..........: 15-September-2011
# Last Modified by......: Puneet Khosla   
# Last Modified Date....: 03-Aug-2015  
# Description...........: This Class contains utility methods to support EvEv functionality
# 
########################################################################### 
*/
public class EvEV_Helper {
    
     private class OLICreationException extends Exception {}
    
//  This method takes care of the following tasks
//  1. Works only when the record of the list is a Primary Deal
//  2. Deletes all opportunity line items of the given opportunity which have Primary_Deal__c attribute as "true"
//  3. Get the Product Ids for products {'Voice','Blackberry Data Only','Tablet Data Only','Mobile Broadband Data Only','Fleetlink','M2M'}
//  4. Create OLIs for only those products which have Quantity>0 Revenue generated>0  
    public static void createUpdateOLIs(List<Artemis_Deal__c> lstArtemisDeals)
    {
      Map<String,String> connectionTypes=new Map<String,String>{'Acquisition'=>'New / Growth Connections','Growth'=>'New / Growth Connections','Resign with Growth'=>'New / Growth Connections','Resign'=>'Re-sign Connections'}; 
        List<OpportunityLineItem> lstOLIs=new List<OpportunityLineItem>();
        List<Opportunity> lstOpportunity =new List<Opportunity>();
        for(Artemis_Deal__c deal:lstArtemisDeals)
        {
            System.debug('DEAL pRIMARY:-'+deal.Primary_Deal__c);
            Boolean isValidOLI=false;
            if(deal.Primary_Deal__c==true && deal.Process_Status__c=='Processed')
            {
               List<OpportunityLineItem> lstOldOlIs=[Select id from OpportunityLineItem where OpportunityId=:deal.Opportunity__c and Related_To_Primary_Deal__c=true];
               if (lstOldOlIs.size()>0)
                delete lstOldOlIs; 
               
               Map<String,Id> mapIds=new Map<String,Id>();
               List<PriceBookEntry> tempLst=[Select p.id, p.Product2.Name From PricebookEntry p where p.Product2.Brand__c = :deal.Brand__c and p.Product2.Name in ('Voice','Blackberry Data Only','Tablet Data Only','Mobile Broadband Data Only','Fleetlink','M2M')];
               for(PriceBookEntry PBEntry:tempLst)
               {
                mapIds.put(PBEntry.Product2.Name,PBEntry.id);
               }
               
               if(isGreaterThanZero(deal.Voice_Users__c) && mapIds.size()>0)
               {
                    
                    OpportunityLineItem oli = new OpportunityLineItem(New_Growth_Resign__c=connectionTypes.get(deal.Connection_Type__c),Related_To_Primary_Deal__c=true,OpportunityId=deal.Opportunity__c,PricebookEntryId=mapIds.get('Voice'),TotalPrice=deal.Mobile_Outgoing_Voice_Revenue__c,Quantity=deal.Voice_Users__c );
                    lstOLIs.add(oli);
                    isValidOLI=true;
               }
               if(isGreaterThanZero(deal.Blackberry_DO_Users__c)&& mapIds.size()>0 )
               {
                    OpportunityLineItem oli = new OpportunityLineItem(New_Growth_Resign__c=connectionTypes.get(deal.Connection_Type__c),Related_To_Primary_Deal__c=true, OpportunityId=deal.Opportunity__c,PricebookEntryId=mapIds.get('Blackberry Data Only'),TotalPrice=deal.Data_Only_Blackberry_Revenue__c,Quantity=deal.Blackberry_DO_Users__c);
                    lstOLIs.add(Oli);
                    isValidOLI=true;
               }
               if(isGreaterThanZero(deal.Mobile_Broadband_DO_Users__c) && mapIds.size()>0)
               {
                    OpportunityLineItem oli = new OpportunityLineItem(New_Growth_Resign__c=connectionTypes.get(deal.Connection_Type__c), Related_To_Primary_Deal__c=true,OpportunityId=deal.Opportunity__c,PricebookEntryId=mapIds.get('Mobile Broadband Data Only'),TotalPrice=deal.Data_Only_Mobile_Broadband_Revenue__c,Quantity=deal.Mobile_Broadband_DO_Users__c);
                    lstOLIs.add(Oli);
                    isValidOLI=true;
               }
               if(isGreaterThanZero(deal.Tablet_DO_Users__c)&& mapIds.size()>0)
               {
                    OpportunityLineItem oli = new OpportunityLineItem(New_Growth_Resign__c=connectionTypes.get(deal.Connection_Type__c), Related_To_Primary_Deal__c=true,OpportunityId=deal.Opportunity__c,PricebookEntryId=mapIds.get('Tablet Data Only'),TotalPrice=deal.Data_Only_Tablet_Revenue__c,Quantity=deal.Tablet_DO_Users__c);
                    lstOLIs.add(Oli);
                    isValidOLI=true;
               }
               if(isGreaterThanZero(deal.Fleetlink_Users__c)&& mapIds.size()>0)
               {
                    OpportunityLineItem oli = new OpportunityLineItem(New_Growth_Resign__c=connectionTypes.get(deal.Connection_Type__c), Related_To_Primary_Deal__c=true,OpportunityId=deal.Opportunity__c,PricebookEntryId=mapIds.get('Fleetlink'),TotalPrice=deal.Fleetlink_Revenue__c,Quantity=deal.Fleetlink_Users__c);
                    lstOLIs.add(Oli);
                    isValidOLI=true;
               }
               if(isGreaterThanZero(deal.Total_M2M_Users__c)&& mapIds.size()>0)
               {
                    OpportunityLineItem oli = new OpportunityLineItem(New_Growth_Resign__c=connectionTypes.get(deal.Connection_Type__c), Related_To_Primary_Deal__c=true,OpportunityId=deal.Opportunity__c,PricebookEntryId=mapIds.get('M2M'),TotalPrice=deal.M2M_Revenue__c,Quantity=deal.Total_M2M_Users__c);
                    lstOLIs.add(Oli);
                    isValidOLI=true;
               }
               if(isValidOLI)
               {
                    Opportunity op=new Opportunity(id=deal.Opportunity__c,Brand__c=deal.Brand__c);
                    lstOpportunity.add(op);
                    //OppBrand.put(deal.Opportunity__c,deal.Brand__c);
               }
                                   
                
            }
        }
        if(lstOLIs.size()>0)
        {
            try{
                insert lstOLIs;
                if(lstOpportunity.size()>0)
                {
                    update lstOpportunity;
                }
            }catch(DMLException dmlEx)
            {
                //Exception ex=new Exception;
                //throw new OLICreationException('Opportunity Line Items could not be created');
            }
        }
    }
    
    //This method is used to check if the given string is blank or null and return a Boolean response accordingly
    public static Boolean isBlank(String param)
    {
        if(null==param || param.trim().equals(''))
            return true;
        else 
            return false;
    }
    
    //This method is used to check if the given list is Empty or null and return a Boolean response accordingly
    public static Boolean isListEmpty(List<Artemis_Deal__c> lstParam)
    {
        if(null==lstParam || lstParam.size()==0)
            return true;
        else
            return false;
    }
    
    //This method is used to check if the given Numerical Value is null or equal to zero and return a Boolean response accordingly
    public static Boolean isGreaterThanZero(Decimal param)
    {
        if(null!=param && param>0)
            return true;
        else 
            return false;
    }
    /*
    * @author               : Puneet Khosla
    * @description          : Method to return the recordtype using describe call
    * @param sobjectName    : Object Name
    * @param recordTypeName : Record Type Name
    * @return Id            : Id of the record type
    */
    public static ID getRecordTypeID(SObject sobjectName, string recordTypeName)
    {
        Schema.DescribeSObjectResult describeSobjectSchema = sobjectName.getSObjectType().getDescribe(); 
        Map<String,Schema.RecordTypeInfo> recordTypeInfo = describeSobjectSchema.getRecordTypeInfosByName();
        return recordTypeInfo.get(recordTypeName).getRecordTypeId();
    }
}