@isTest

private class Test_FeedbackDeploymentController{
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        // test general feebdack      
        Feedback__c fbk1 = new Feedback__c (Feedback_Title__c = 'Test Feedback', Requested_Priority__c = 'low', RecordTypeID = '01220000000PlHa', Type__c = 'Change Request', Description__c = 'Testing',Reason__c = 'Teast Reason' ); 
        Database.Saveresult fbkResult1 = Database.insert(fbk1);  
        system.debug ('Feedback created:' +fbk1.ID);
        Deployment_Actions__c depA = new Deployment_Actions__c (CR__c = fbk1.ID, Version__c = 1.00); 
        Database.Saveresult depAResult = Database.insert(depA);    
            
        PageReference pageRef = Page.FeedbackDeploymentPDF;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', depA.Id);          
        system.debug ('JMM1:' +fbk1.ID);
        system.debug ('JMM2:' +pageRef);      
        system.debug ('JMM3:' +depA.Id);
        // test attachment
        Blob attachBody = Blob.valueOf('attachment body');
        Attachment attach = new Attachment (OwnerId = UserInfo.getUserId(), ParentId = depA.ID, isPrivate = false, body = attachBody, Name = 'test');
        Database.Saveresult attachmentResult = Database.insert(attach);    

        
        // invoke controller actions               
        FeedbackDeploymentController controller = new FeedbackDeploymentController(null);
        controller.savePdf();  
        controller.addDevNote();  
        controller.addPreNote();
        controller.addPostNote();
        controller.getDevNoteList();
        controller.getPreNoteList();
        controller.getPostNoteList();
        controller.getFBAttDev();
        controller.getFBNoteDev();
    }    
}