@isTest
private class Test_CSSApprovalHistoryController {
    
    static testMethod void myUnitTest() {
        
        /*
        
        Account a = Test_Factory.CreateAccount();
        a.Name = 'Test';
        insert a;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        Battleplan__c batPlan = new Battleplan__c(Opportunity_Name__c = o.Id);
        insert batPlan;
                
        RecordType rt_BSP = [select id from RecordType where SobjectType='Customer_Sales_Support__c' and name = 'Bespoke Pricing' limit 1];
        
        Customer_Sales_Support__c css = new Customer_Sales_Support__c(Opportunity_Name__c = o.Id, RecordTypeId = rt_BSP.Id, Level_2_Attached__c = true, Full_Evaluation_Attached__c = true, Status__c = 'Pricing/Financial Approval Sought');
        try{
        insert css;
        }
        catch(Exception f)
        {
        f.getmessage();
        }
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        Attachment attachment=new Attachment(); 
        attachment.Name='An attachment 1';
        attachment.body=bodyBlob; 
        attachment.parentId=css.id;
        try{
        insert attachment;
        }
        catch(Exception f)
        {
        f.getmessage();
        }
        Attachment attachment1=new Attachment(); 
        attachment1.Name='An attachment 2';
        attachment1.body=bodyBlob; 
        attachment1.parentId=css.id;
        try{
        insert attachment1;
        }
        catch(Exception f)
        {
        f.getmessage();
        }
        
        Attachment attachment2=new Attachment(); 
        attachment2.Name='An attachment 3';
        attachment2.body=bodyBlob; 
        attachment2.parentId=css.id;
        try{
        insert attachment2;
        }
        catch(Exception f)
        {
        f.getmessage();
        }
        
        // create the new approval request to submit
        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
        req.setComments('Submitted for approval. Please approve.');
        req.setObjectId(css.id);
        // submit the approval request for processing
        Approval.ProcessResult result = Approval.process(req);
        // display if the reqeust was successful
        System.debug('Submitted for approval successfully: '+result.isSuccess());
        
        // First, get the ID of the newly created item
        List<Id> newWorkItemIds = result.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
        pwr.setComments('Your Request Has Been Rejected');
        pwr.setAction('Reject');
        pwr.getWorkitemId();
        pwr.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        pwr.setWorkitemId(newWorkItemIds.get(0));
        
        // Submit the request for approval
        Approval.ProcessResult result2 = Approval.process(pwr);
        // Verify the results
        System.assert(result2.isSuccess(), 'Result Status:' + result2.isSuccess());
        
        CSSApprovalHistoryController objCSSApprovalHistoryController = new CSSApprovalHistoryController();
        objCSSApprovalHistoryController.cssId = css.Id;
        objCSSApprovalHistoryController.cssComments = 'Reject';
        string comments = objCSSApprovalHistoryController.getLastCommentsOnStep().Comments;
        
        css.Status__c = 'Pricing/Financial Approval Sought';
        try{
        update css;
        }
        catch(Exception f)
        {
        f.getmessage();
        }
        
        // create the new approval request to submit
        Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
        app.setComments('Submitted for approval. Please approve.');
        app.setObjectId(css.id);
        // submit the approval request for processing
        Approval.ProcessResult appresult = Approval.process(app);
        // display if the reqeust was successful
        System.debug('Submitted for approval successfully: '+appresult.isSuccess());
        
        // First, get the ID of the newly created item
        List<Id> appnewWorkItemIds = appresult.getNewWorkitemIds();
        
        // Instantiate the new ProcessWorkitemRequest object and populate it
        Approval.ProcessWorkitemRequest apppwr = new Approval.ProcessWorkitemRequest();
        apppwr.setComments('Your Request Has Been Approved');
        apppwr.setAction('Approve');
        apppwr.getWorkitemId();
        apppwr.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        // Use the ID from the newly created item to specify the item to be worked
        apppwr.setWorkitemId(appnewWorkItemIds.get(0));
        
        // Submit the request for approval
        Approval.ProcessResult appresult2 = Approval.process(apppwr);
        // Verify the results
        System.assert(appresult2.isSuccess(), 'Result Status:' + appresult2.isSuccess());
        objCSSApprovalHistoryController.cssComments = 'Approve';
        string comments2 = objCSSApprovalHistoryController.getLastCommentsOnStep().Comments;
        
        */
    }
}