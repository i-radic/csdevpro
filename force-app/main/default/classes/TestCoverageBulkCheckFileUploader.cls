@isTest(SeeAllData=true)
private class TestCoverageBulkCheckFileUploader
{
    static testMethod void testCoverageBulkCheckFileUploader()
    {
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        //mock.setStaticResource('CoverageRestAPIResponse');
        mock.setStaticResource('CoverageSPGAPIResponse');
        mock.setStatusCode(200);
        mock.setStatus('OK');
        mock.setHeader('Content-Type', 'text/plain');    
        Test.setMock(HttpCalloutMock.class, mock);
        Coverage_Check_Site__c ccs = getValidCoverageCheckSite();
        Coverage_Check_Site__c testCCS = [Select Location_Name2__c,Coverage_Check__c ,Post_Code__c,CER_Reason__c,Site_Contact_Name__c,Telephone_Number__c,
                                     Floorplan_Attached__c,Building_Type__c,No_of_Buildings_Requiring_Enhancements__c,Which_floors_require_enhancement__c from Coverage_Check_Site__c where Id = :ccs.id ];
        Coverage_Check__c cc =[Select id,name from Coverage_Check__c where id=:testCCS.Coverage_Check__c];
        Test.startTest();
         buildTestController3(cc).ReadFile(); 
        Test.stopTest();  
        buildTestController(cc).uploadInputFile('TestCoverageSitesInput.csv');
        //buildTestController(cc).getandUpdateCoverageInformationRequest(ccs,null);     
        buildTestController(cc).getandUpdateCoverageInformationRequestSPG(ccs,null);      
    }
    static testMethod void testCoverageBulkCheckFileUploader2()
    {
        StaticResourceCalloutMock mock2 = new StaticResourceCalloutMock();
        //mock2.setStaticResource('CoverageRestAPIResponseNull');
        mock2.setStaticResource('CoverageSPGAPIResponseNull');
        mock2.setStatusCode(200);
        mock2.setStatus('OK');
        mock2.setHeader('Content-Type', 'text/plain');    
        Test.setMock(HttpCalloutMock.class, mock2);
        Coverage_Check_Site__c ccs = getValidCoverageCheckSite();
        Coverage_Check_Site__c testCCS = [Select Location_Name2__c,Coverage_Check__c ,Post_Code__c,CER_Reason__c,Site_Contact_Name__c,Telephone_Number__c,
                                     Floorplan_Attached__c,Building_Type__c,No_of_Buildings_Requiring_Enhancements__c,Which_floors_require_enhancement__c from Coverage_Check_Site__c where Id = :ccs.id ];
        Coverage_Check__c cc =[Select id,name from Coverage_Check__c where id=:testCCS.Coverage_Check__c];
        Test.startTest();
        buildTestController2(cc).ReadFile();
        Test.stopTest();
    
    }
    static testMethod void testCoverageBulkCheckFileUploader3()
    {
        StaticResourceCalloutMock mock2 = new StaticResourceCalloutMock();
        //mock2.setStaticResource('CoverageRestAPIResponseNull');
        mock2.setStaticResource('CoverageSPGAPIResponseNull');
        mock2.setStatusCode(200);
        mock2.setStatus('OK');
        mock2.setHeader('Content-Type', 'text/plain');    
        Test.setMock(HttpCalloutMock.class, mock2);
        Coverage_Check_Site__c ccs = getValidCoverageCheckSite();
        Coverage_Check_Site__c testCCS = [Select Location_Name2__c,Coverage_Check__c ,Post_Code__c,CER_Reason__c,Site_Contact_Name__c,Telephone_Number__c,
                                     Floorplan_Attached__c,Building_Type__c,No_of_Buildings_Requiring_Enhancements__c,Which_floors_require_enhancement__c from Coverage_Check_Site__c where Id = :ccs.id ];
        Coverage_Check__c cc =[Select id,name from Coverage_Check__c where id=:testCCS.Coverage_Check__c];
        Test.startTest();
        buildTestController4(cc).ReadFile();
        Test.stopTest();
    
    }
    static CoverageBulkCheckFileUploader buildTestController(Coverage_Check__c covcheck)
    {
        PageReference pageRef = Page.UploadCoverageCheckSites;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', covcheck.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(covcheck);
        CoverageBulkCheckFileUploader controllerExt = new CoverageBulkCheckFileUploader(sc);
        return controllerExt ;      
     }
     static CoverageBulkCheckFileUploader buildTestController2(Coverage_Check__c covcheck)
     {
        PageReference pageRef = Page.UploadCoverageCheckSites;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', covcheck.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(covcheck);
        CoverageBulkCheckFileUploader controllerExt = new CoverageBulkCheckFileUploader(sc);
        StaticResource sr = [Select Body From StaticResource Where Name = 'BulkCheckPostcodes2' LIMIT 1];
        controllerExt.fileContent=Blob.ValueOf(sr.Body.toString());
        controllerExt.fileName ='TestPostcodes.csv';
         return controllerExt ;      
     }
     static CoverageBulkCheckFileUploader buildTestController3(Coverage_Check__c covcheck)
     {
        PageReference pageRef = Page.UploadCoverageCheckSites;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', covcheck.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(covcheck);
        CoverageBulkCheckFileUploader controllerExt = new CoverageBulkCheckFileUploader(sc);
        StaticResource sr = [Select Body From StaticResource Where Name = 'BulkCheckPostcodes3' LIMIT 1];
        controllerExt.fileContent=Blob.ValueOf(sr.Body.toString());
        controllerExt.fileName ='TestPostcodes.csv';
        controllerExt.requestCurrent =283;
        return controllerExt ;      
     }
     static CoverageBulkCheckFileUploader buildTestController4(Coverage_Check__c covcheck)
     {
        PageReference pageRef = Page.UploadCoverageCheckSites;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', covcheck.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(covcheck);
        CoverageBulkCheckFileUploader controllerExt = new CoverageBulkCheckFileUploader(sc);
        StaticResource sr = [Select Body From StaticResource Where Name = 'BulkCheckPostcodes' LIMIT 1];
        controllerExt.fileContent=Blob.ValueOf(sr.Body.toString());
        controllerExt.fileName ='TestPostcodes.csv';
        return controllerExt ;      
     }
     public static Coverage_Check_Site__c getValidCoverageCheckSite() 
     {
       Opportunity opp = new Opportunity();
       opp.Name ='TestOpp2';
       opp.StageName='Changes Over Time';
       opp.CloseDate = Date.Today();
       insert opp;
       Coverage_Check__c cc = new Coverage_Check__c ();
      
       cc.Opportunity__c = opp.id;
       insert cc;
       Coverage_Check_Site__c  ccs = new Coverage_Check_Site__c ();
       ccs.Location_Name2__c = 'Location';
       ccs.I_understand2__c = TRUE;
       ccs.Post_Code__c = 'BB11';
       ccs.CER_Reason__c ='No coverage confirmed on site';
       ccs.Site_Contact_Name__c ='John Smith';
       ccs.Telephone_Number__c ='123456';
       ccs.Floorplan_Attached__c = true;
       ccs.No_of_Buildings_Requiring_Enhancements__c = 5;
       ccs.No_of_floors_in_this_building__c = 6;
       ccs.Which_floors_require_enhancement__c ='2 and 3rd';
       ccs.Coverage_Check__c = cc.id;
       ccs.Commercial__c =true;
       insert ccs;      
       return ccs;
    }
    
}