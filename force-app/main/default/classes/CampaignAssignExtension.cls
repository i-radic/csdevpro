public without sharing class CampaignAssignExtension {
	private String contactId;
	private Boolean bShow = true;
	private Boolean bShowBack = false;
	private Boolean bShowResult = false;
	public 	Boolean showCampaignLookup { get { return bShow;}}
	public 	Boolean showBack { get { return bShowBack;}}
	public 	Boolean showResult { get { return bShowResult;}}
	public String campaign { get; set;}
	public String campaignId;
	public String campaignName { get; set;}
	public Double campaignXDayRule { get; set;}
	public String listReferer {get; set;}
	public 	Boolean bBTLBUser { get; set;}
	public String whatId {get; set;}
	

	public CampaignAssignExtension(ApexPages.StandardSetController controller) {
		System.debug('Initialise CampaignAssignExtension...');
		for (String keyv : ApexPages.currentPage().getParameters().keySet()) {
      		System.debug ('currentPage().getParameters() : ' + keyv + ' : ' + ApexPages.currentPage().getParameters().get(keyv));
    	}
    	this.contactId = ApexPages.currentPage().getParameters().get('id');
    	setPageComponentVisibility(ApexPages.currentPage().getParameters().get('SelectCampaign'));
    	bBTLBUser=true;
    	if(listReferer==null){
    		listReferer = ApexPages.currentPage().getHeaders().get('Referer');
    		listReferer='/003';
    	}
    	if(ApexPages.currentPage().getParameters().get('FromTaskList') != null && ApexPages.currentPage().getParameters().get('FromTaskList').equals('true')){
    		listReferer = '/007';
    		bBTLBUser=false;
    	}
    	if(ApexPages.currentPage().getParameters().get('whatId') != null){
    		whatId = ApexPages.currentPage().getParameters().get('whatId');
    	}
	}
		
	public PageReference performAssignment(){
		if(bBTLBUser){
			//if(userCampaign == null){
				//this.bShow = true;
				this.bShow = false;
			//}
		}else{
			this.bShow = false;
		}
		if(bShowBack){
					
					Contact c = [select Id, Account.Assigned_User__c, Name, Account.Last_Account_Call_Date__c, DoNotCall, Phone_Consent__c, Assigned_User_Contact__c, Account.Call_Back_Time__c, Campaign_Member_Notes__c, Account.Assigned_User_Datetime__c from Contact where Id =: contactId];
					Account a = [select Id, Account.Assigned_User__c, Account.Last_Account_Call_Date__c, Account.Call_Back_Time__c from Account where Id =: c.AccountId];
					Date dLastCall = c.Account.Last_Account_Call_Date__c;
					//if(dLastCall == null ){
						Datetime dCallBackTime = c.Account.Call_Back_Time__c;
						if(dCallBackTime == null || (dCallBackTime != null && dCallBackTime < datetime.now())){
							//We allow upto 3 hours for an agent to call a contact before it is visible to other agents (covers case where agent may select contact to call via this VF page and then not complete a task for the user.)
							if((c.Account.Assigned_User__c == null || c.Account.Assigned_User__c == UserInfo.getUserId() || c.Account.Assigned_User_Datetime__c < datetime.now().addHours(-3)) 
									&& (c.Assigned_User_Contact__c == null || c.Assigned_User_Contact__c == UserInfo.getUserId())){
								if(c.DoNotCall != true && c.Phone_Consent__c != 'No'){
										a.Assigned_User__c = UserInfo.getUserId();
										a.Assigned_User_Datetime__c = datetime.now();
										update a;
										//Now update all Open relevant Tasks (so they do not appear in Task list view (Corp and DBAM))
										List<Task> lTaskUpdate = new List<Task>();
										for(Task[] tasks : [select Id, Assigned_User_Id__c from Task where AccountId = :c.AccountId AND Campaign_Task__c = 'TRUE' AND Type = 'Call']){
											for(Task t : tasks){
												t.Assigned_User_Id__c= UserInfo.getUserId();
												t.Assigned_User_Datetime__c = datetime.now();
												lTaskUpdate.add(t);	
											}
											if(lTaskUpdate.size() == 1000){
												update lTaskUpdate;
												lTaskUpdate.clear();
											}
										}
										if(!lTaskUpdate.isEmpty()){
											update lTaskUpdate;
										}
										
										PageReference retpr = new PageReference('/' + c.Id);
					    				return retpr;
								}else{
									ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Contact has elected not to be called, please select another contact.');
				        			ApexPages.addMessage (myMsg);
								}
							}else{
								ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Contact already assigned to another user, please select another contact.');
				        		ApexPages.addMessage (myMsg);
							}
						}else{
							ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Contact is marked for a Call back but they where called < 3 hours ago so should not be contacted, please select another contact.');
				        	ApexPages.addMessage (myMsg);
						}
					//}else{
	    			//	bShow = false;
					//}
		}
		return null;
	}
		
	public void setPageComponentVisibility(String str){
		if(str == 'false'){
			this.bShow = false;
			this.bShowBack = true;
		}
	}
}