@isTest
private class CS_Test_Download_Baskets {
    private static Account testAcc;
    private static cscfga__Product_Basket__c testBasket;
    private static cscfga__Product_Configuration__c config;

    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        // create account
        testAcc = new Account();
        testAcc.Name = 'TestDownloadBasketAccount';
        testAcc.NumberOfEmployees = 10;
        insert testAcc; 
        
        // create opportunity
        Opportunity testOpp = new Opportunity();    
        testOpp.Name = 'Test download opp: ' + testAcc.Name + ':'+datetime.now();
        testOpp.AccountId = testAcc.Id;
        testOpp.CloseDate = System.today();
        testOpp.StageName = 'Closed Won';   //@TODO - This should be a configuration
        testOpp.TotalOpportunityQuantity = 0; // added this as validation rule was complaining it was missing
        insert testOpp;
        
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
        // create basket
        testBasket = new cscfga__Product_Basket__c();
        Datetime d = system.now();
        String strDatetime = d.format('yyyy-MM-dd HH:mm:ss');
        testBasket.Name = 'Test Order ' + strDatetime;
        testBasket.cscfga__Opportunity__c = testOpp.Id;
        insert testBasket;
        
        createProductConfigurations();
    }

    private static void createProductConfigurations() {
        String offerId = null;
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c(Name='Test Definition 1', cscfga__Description__c = 'Test helper');
        insert prodDef;
        cscfga__Product_Definition__c prodDefNew = new cscfga__Product_Definition__c(Name='Test Definition 2', cscfga__Description__c = 'Test helper');
        insert prodDefNew;
        
        config = new cscfga__Product_Configuration__c();
        config.cscfga__Product_Basket__c = testBasket.Id;
        config.cscfga__Product_Definition__c = prodDef.Id;
        if (offerId != null)
            config.cscfga__Configuration_Offer__c = offerId;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.cscfga__Unit_Price__c = 10;
        config.cscfga__Quantity__c = 1;
        config.cscfga__Recurrence_Frequency__c = 12;
        insert config;
        
        cscfga__Product_Configuration__c subConfig = new cscfga__Product_Configuration__c();
        subConfig.cscfga__Product_Basket__c = testBasket.Id;
        subConfig.cscfga__Product_Definition__c = prodDefNew.Id;
        if (offerId != null)
            subConfig.cscfga__Configuration_Offer__c = offerId;
        subConfig.cscfga__Configuration_Status__c = 'Valid';
        subConfig.cscfga__Unit_Price__c = 10;
        subConfig.cscfga__Quantity__c = 1;
        subConfig.cscfga__Recurrence_Frequency__c = 12;
        subConfig.cscfga__Root_Configuration__c = config.Id;
        insert subConfig;
    }

    private static testMethod void testDownloadBasketCtrl() {
        CreateTestData();
        
        Test.StartTest();
        
        PageReference pageRef = Page.CS_Download_Baskets;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('oppId', testBasket.cscfga__Opportunity__c);
         ApexPages.currentPage().getParameters().put('selectedBasket',testBasket.Id);
        CS_Download_Baskets ctrl = new CS_Download_Baskets();
        ctrl.download();
        ctrl.checkNull('test, download');
       
        System.assertNotEquals(null, ctrl.csvFile);
        
        Test.StopTest();
    }

}