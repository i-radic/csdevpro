/**
 * Utility class
 */
public with sharing class CS_Utils {
	
	private static Map<String, String> cachedFields {
		get {
			if(cachedFields == null) {
				cachedFields = new Map<String, String>();
			}
			
			return cachedFields;
		}
		set;
	}
    
    /**
     * Returns the list of all fields to be used in dynamic SOQL
     * excludes non custom fields
     *
     * @param so sObject name
     * @return String csv list of all object fields
     */
    public static String geCustomtSobjectFields(String so) {
        String fieldString;

        SObjectType sot = Schema.getGlobalDescribe().get(so);
        if (sot == null) {
            return null;
        }
        
        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();

        fieldString = fields[0].getDescribe().LocalName;
        for (Integer i = 1; i < fields.size(); i++) {
            if (fields[i].getDescribe().LocalName != 'ContractId' && fields[i].getDescribe().custom) {
                fieldString += ',' + fields[i].getDescribe().LocalName;
            }
        }
        return fieldString;
    }
    
    /**
     * Returns the list of all fields to be used in dynamic SOQL
     *
     * @param so sObject name
     * @return String csv list of all object fields
     */
    public static String getSobjectFields(String so) {
        String fieldString;

        SObjectType sot = Schema.getGlobalDescribe().get(so);
        if (sot == null) {
            return null;
        }
        
        if(cachedFields.containsKey(so)) {
        	return cachedFields.get(so);
        }
        
        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();

        fieldString = fields[0].getDescribe().LocalName;
        for (Integer i = 1; i < fields.size(); i++) {
            if (fields[i].getDescribe().LocalName != 'ContractId')
                fieldString += ',' + fields[i].getDescribe().LocalName;
        }
        
        cachedFields.put(so, fieldString);
        return fieldString;
    }
    
    public static String getSobjectFieldsExcludeFormulas(String so) {
        String fieldString;

        SObjectType sot = Schema.getGlobalDescribe().get(so);
        if (sot == null) {
            return null;
        }
        
        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();

        fieldString = fields[0].getDescribe().LocalName;
        for (Integer i = 1; i < fields.size(); i++) {
            if ((fields[i].getDescribe().LocalName != 'ContractId') && fields[i].getDescribe().isUpdateable())
                fieldString += ',' + fields[i].getDescribe().LocalName;
        }
        return fieldString;
    }
    
    /**
     * Converts list of objects to CSV list to use in dynamic SOQL
     *
     * @param List<sObject>
     * @return String
     */
    public static String convertListToString(List<sObject> vList) {
        String listString = '(\'\')';
        if (vList.size() > 0) {
            listString = '(\''+vList[0].Id+'\'';
            for (Integer i = 1; i < vList.size(); i++) {
                listString += ',\'' + vList[i].Id+'\'';
            }
            listString += ')';
        }
        return listString ;
    }
    
    /**
	 * Converts list of objects to CSV list to use in dynamic SOQL
	 *
	 * @param List<sObject>
	 * @return String
	 */
	public static String convertStringListToString(List<String> vList) {
		String listString = '(\'\')';
		if (vList.size() > 0) {
			listString = '(\'' + vList[0] + '\'';
			for (Integer i = 1; i < vList.size(); i++) {
				listString += ',\'' + vList[i] + '\'';
			}
			listString += ')';
		}
		return listString ;
	}
    
    /**
     * Loads static resource
     * @param String resourceName
     * @return resource body
     */
    public static Blob loadStaticResource(String resourceName) {
        List<StaticResource> sr = new List<StaticResource>();
        if (resourceName != null && resourceName != '') {
            sr = [
                select Body
                from
                StaticResource
                where Name = :resourceName
            ];
        }
        return sr != null && sr.size() > 0 ? sr[0].Body : null;
    }
    

}