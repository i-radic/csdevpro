@isTest
public class TestCoverageCommercialSitesList
{
    static testMethod void testController()
    {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
        }    
        
        Opportunity opp = new Opportunity();
        opp.Name ='TestOpp1';
        opp.StageName='Changes Over Time';
        opp.CloseDate = Date.Today();
        insert opp;
        Coverage_Check__c cc = new Coverage_Check__c ();
        cc.Opportunity__c = opp.id;
        insert cc;
        Coverage_Check_Site__c  ccs = new Coverage_Check_Site__c ();
        ccs.Location_Name2__c = 'Location';
        ccs.Post_Code__c = 'LS10 1LJ';
        ccs.CER_Reason__c ='No coverage confirmed on site';    //Coverage
        ccs.Site_Contact_Name__c ='John Smith';
        ccs.Telephone_Number__c ='123456';
        ccs.Floorplan_Attached__c = true;
        ccs.No_of_Buildings_Requiring_Enhancements__c = 5;
        ccs.No_of_floors_in_this_building__c = 6;
        ccs.Which_floors_require_enhancement__c ='2 and 3rd';
        ccs.Coverage_Check__c = cc.id;
        ccs.I_understand2__c = TRUE;
        insert ccs; 
        PageReference pageRef = Page.CoverageCommercialSites;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', cc.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(cc);
        CoverageCommercialSitesListController controllerExt = new CoverageCommercialSitesListController(sc);
        controllerExt.getCoverageCommercialSites();
        try{
        controllerExt.doNew();
        }Catch(Exception ex){}
        controllerExt.getCoverageCommercialSites();
    }
}