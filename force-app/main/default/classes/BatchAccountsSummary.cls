global class BatchAccountsSummary implements Database.Batchable<SObject>,Database.Stateful{
/*
###################################################################################################
To calculate the number of account owner by a user and set the do not auot delete flag to prevent
account owners from being made inactive automatically.

18/11/10    John McGovern    Intial Build

###################################################################################################
*/

public String query;

global Map<Id,Decimal> umap = new Map<Id, Decimal>();

global BatchAccountsSummary() {
//create map
For (User u : [Select Id from User where isActive = TRUE]) { 
umap.put(u.Id, 0);
}
}

global database.querylocator start(Database.BatchableContext BC){
return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<sObject> scope){

for(sObject s : scope) {

Account a = (Account)s;

if (umap.containsKey(a.OwnerId)) {
Decimal cu = umap.get(a.OwnerId) + 1;
umap.put(a.OwnerId,cu);
}
}
}

global void finish(Database.BatchableContext BC){

List<User> lmap = new List<User>();
Boolean jmTick = False;

for (Id smap : umap.keySet()) { 
system.debug('get(smap): ' + umap.get(smap));
    if (umap.get(smap) > 0) {
        jmTick  = True;
    }
    else {
         jmTick = False;
    }
    lmap.add(new User (Id = smap, Accounts_Owned__c = umap.get(smap), Exclude_from_Auto_Delete__c = jmTick));

}

update lmap;

}
} //end class