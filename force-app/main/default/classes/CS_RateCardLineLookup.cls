global class CS_RateCardLineLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["RateCard"]'; 
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
        
        List<cspmb__Rate_Card_Line__c> rcl = null;
        String rclFields = getSobjectFields('cspmb__Rate_Card_Line__c');
        String rcID = searchFields.get('RateCard');

        String query = 'SELECT ' + rclFields  + ' FROM cspmb__Rate_Card_Line__c WHERE cspmb__Rate_Card__c = :rcID';

        rcl = Database.query(query);

        return rcl;
    }

    public String getSobjectFields(String so) {
        String fieldString;
   
        SObjectType sot = Schema.getGlobalDescribe().get(so);
        if (sot == null) return null;
   
        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();
       
        fieldString = fields[0].getDescribe().LocalName;
        for (Integer i = 1; i < fields.size(); i++) {
            fieldString += ',' + fields[i].getDescribe().LocalName;
        }
        return fieldString;
    }
}