@isTest(SeeAllData=true)

public class TestVFFloorPlanUpload{
    
    static testMethod void testController() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
        }
        Opportunity o = new Opportunity ();
        o.Name ='TestCoveragecheck1';
        o.StageName='Changes Over Time';        
        o.CloseDate = System.today().addDays(10) ; 
        insert o;
        Coverage_Check__c cc = new Coverage_Check__c ();
        cc.Opportunity__c = o.id;
        insert cc;
        Coverage_Check_Site__c ccSite = new  Coverage_Check_Site__c();
        ccSite.Coverage_Check__c = cc.id;
        ccSite.I_understand2__c = TRUE;
        insert ccSite;
        
        
        PageReference pageRef = Page.UploadFloor_Plan;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('id', ccSite.id);
        ApexPages.StandardController sc = new ApexPages.StandardController(ccSite);
        VFFloorPlanUpload controller = new VFFloorPlanUpload(sc);
        controller.UploadFile();
        controller.fileName='TestFloorPlanUpload';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        
        controller.fileBody =bodyBlob;
        controller.UploadFile();
        
    }
}