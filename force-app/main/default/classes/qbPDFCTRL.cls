public class qbPDFCTRL {
    ApexPages.StandardController Gcontroller;
    public Order_Header__c orderRec;
    public Static String urlType {get;set;}
    public Static String urlTypeProper {get;set;}
    public Static String urlVer {get;set;}
    public Static String pStatus {get;set;}
    public Static String pNotes {get;set;}
    public Static String oStatus {get;set;}
    public Static String oNotes {get;set;}
    public Static String mainColor {get;set;}
    public Static String hlColor {get;set;}
    
    public Static String UKcallplanName {get;set;}
    public Static String INTcallplanName {get;set;}
    public Static String presNumber {get;set;}
    
    public Integer trainingItems {get;set;}
        
    public Map<Id, Integer> ufpTotalMap { get; set; }   
    public Map<Id, Integer> ufpBasicMap { get; set; }  
    public Map<Id, Integer> ufpConnectMap { get; set; }   
    public Map<Id, Integer> ufpCollaborateMap { get; set; }   
    public Map<Id, Integer> ufpUserSetupMap { get; set; }    
    public Map<Id, Integer> acUsersMap { get; set; } 
    public Map<Id, Integer> acConCallsMap { get; set; } 
    public Map<Id, Integer> cpeInstallMap { get; set; }   
    public Map<Id, Integer> cpePhysicalPhonesMap { get; set; } 

    public Map<Id, String> lanPoEMap { get; set; }   
    public Map<Id, String> acProductSelection1Map { get; set; }    
    public Map<Id, String> acProductSelection2Map { get; set; }    
    public Map<Id, String> siteContact { get; set; }
    public Map<Id, String> newSwitchType { get; set; }
    public Map<Id, String> cpNewDate { get; set; }
    public Map<Id, String> cvNewDate { get; set; }
    public Map<Id, String> cvNewDiscount { get; set; }
    public Map<Id, String> cvNewTerm { get; set; }
	
    public qbPDFCTRL() { }
    public qbPDFCTRL(ApexPages.StandardController controller) {
        Gcontroller = controller;
        this.orderRec= (Order_Header__c)controller.getRecord();      
    }

    String pgId = System.currentPageReference().getParameters().get('id');
    String pgTarget = System.currentPageReference().getParameters().get('type');
    String pgArea = System.currentPageReference().getParameters().get('pArea');
    String pgVer = System.currentPageReference().getParameters().get('ver');
    
    public void orderData(){
        Order_Header__c orderRec = [SELECT Quote_Type__c, zXMLInfo__c FROM Order_Header__c WHERE Id = :orderRec.Id LIMIT 1];
        String urlString = 'id='+pgId+'&type='+pgTarget+'&pArea='+pgArea+'&ver='+pgVer;

        UKcallplanName = qbOrderCTRL.readApexXML(orderRec.zXMLinfo__c, 'UKcallplanName');
        INTcallplanName = qbOrderCTRL.readApexXML(orderRec.zXMLinfo__c, 'INTcallplanName');
        presNumber = qbOrderCTRL.readApexXML(orderRec.zXMLinfo__c, 'presNumber');
    }

    public List<Order_Site__c> getSiteData() {
        List<Order_Site__c> sites = [SELECT ID, zXMLinfo__c, Order_Header__r.zXMLinfo__c,Order_Header__r.Quote_Type__c,  CRD__c, Site_name__c, Site_Contact__r.Name, Site_Contact__r.Phone, Site_Contact__r.Email, Site_Type__c, Service_Site__c,
        Total_Initial2__c, Total_Recurring2__c, Total_Initial_Disc2__c, Total_Recurring_Disc2__c,Access_Service_Provision__c, LAN_Requirement__c, Structured_Cabling_Requirement__c, 
        Licences_Initial__c, Licences_Recurring__c, CPE_Initial__c, CPE_Recurring__c, LAN_Initial__c, LAN_Recurring__c, Cabling_Initial__c, Cabling_Recurring__c, Numbers_Initial__c, Numbers_Recurring__c,
        Address_Delivery__c, Address_Delivery__r.Building_Name__c, Address_Delivery__r.Sub_Building__c, Address_Delivery__r.Number__c, 
        Address_Delivery__r.Street__c, Address_Delivery__r.Street_2__c, Address_Delivery__r.Locality__c, Address_Delivery__r.County__c, Address_Delivery__r.Post_Town__c,
        Address_Delivery__r.Post_Code__c, Address_Delivery__r.Country__c,
        Address_Installation__c, Address_Installation__r.Building_Name__c, Address_Installation__r.Sub_Building__c, Address_Installation__r.Number__c, 
        Address_Installation__r.Street__c, Address_Installation__r.Street_2__c, Address_Installation__r.Locality__c, Address_Installation__r.County__c, Address_Installation__r.Post_Town__c, 
        Address_Installation__r.Post_Code__c, Address_Installation__r.Country__c,Total_UFP__c, Total_UFP_inc_SIP__c, 
        zprodSwitch__c, zprodBTnet__c, zprodSIP__c, zprodFirewall__c,  
        Switch_Initial__c, Switch_Recurring__c, Switch_Journey__c, Switch_Base_Level__c, Switch_Maintenance_Level__c, Switch_Maintenance_Term__c, Switch_Phone_Maintenance_Level__c,
        BTnet_Access__c, BTnet_Contract_Term__c, BTnet_Variant__c, BTnet_Port_Speed__c, BTnet_Bearer_Speed__c, BTnet_Port_Speed_Secondary__c, BTnet_Bearer_Speed_Secondary__c, 
        SIP_Channels__c, SIP_Contract_Term__c, Switch_Provision__c, Switch_Model__c, 
        Firewall_Type__c, Firewall_Contract_Term__c, Firewall_Bearer_Speed__c, Firewall_Concurrent_Users__c, 
            (SELECT ID, Product_name__c, Quantity__c, Unit_Cost__c, Type__c, Grouping__c, Image__c, Purchasing_Option__c, Number_Info__c, Finance_Option__c, Information__c, Directory_Entry__c,
            Total_Original_Initial__c, Total_Initial__c, Total_Discount_Initial__c,Discount_Initial__c, Term__c, zInSolution__c, 
            Total_Original_Recurring_Cost__c, Total_Recurring_Cost__c, Total_Discount_Recurring__c
            FROM Order_Product__r ORDER BY Type__c, Sort_Order__c, Product_Name__c) 
        FROM Order_Site__c 
        WHERE Order_Header__c = :pgId
        ORDER BY Service_Site__c DESC, Site_Type__c, Site_Name__c];

        ufpTotalMap = new Map<Id, Integer>();
        ufpBasicMap = new Map<Id, Integer>();
        ufpConnectMap = new Map<Id, Integer>();
        ufpCollaborateMap = new Map<Id, Integer>();
        ufpUserSetupMap = new Map<Id, Integer>();
        acUsersMap = new Map<Id, Integer>();
        acConCallsMap = new Map<Id, Integer>();
        cpeInstallMap = new Map<Id, Integer>();
        cpePhysicalPhonesMap = new Map<Id, Integer>();

        lanPoEMap = new Map<Id, String>();
        acProductSelection1Map = new Map<Id, String>();
        acProductSelection2Map = new Map<Id, String>();
        siteContact = new Map<id, String>();
        newSwitchType = new Map<id, String>();
        cpNewDate = new Map<id, String>();
        cvNewDate = new Map<id, String>();
        cvNewDiscount = new Map<id, String>();
        cvNewTerm = new Map<id, String>();
		
        for (Order_Site__c s:sites){
            if(!s.Service_Site__c){
                acUsersMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'acUsers'));
                acConCallsMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'acConCalls'));
                ufpTotalMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'ufpTotal'));
                ufpBasicMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'ufpBasic'));
                ufpConnectMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'ufpConnect'));
                ufpCollaborateMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'ufpCollaborate'));
                ufpUserSetupMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'ufpUserSetup'));
                cpeInstallMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'cpeInstall'));
                cpePhysicalPhonesMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'cpePhysicalPhones'));

                lanPoEMap.put(s.Id, qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'lanPoE'));
                acProductSelection1Map.put(s.Id, qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'acProductSelection1'));
                acProductSelection2Map.put(s.Id, qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'acProductSelection2'));
                siteContact.put(s.Id, getSiteContactStr(qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'siteSettings')));
                newSwitchType.put(s.Id, qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'newSwitchType'));
/*
                pStatus = qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'proposalStatus');
                pNotes = qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'proposalNotes');
                oStatus = qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'orderStatus');
                oNotes = qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'orderNotes');
*/
            }else{
                trainingItems = qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'trainingItems');
                ufpTotalMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.zXMLinfo__c, 'cpNewUsers'));
                acProductSelection1Map.put(s.Id, qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'cpNewProdName'));
                cpNewDate.put(s.Id, qbOrderCTRL.readApexXML(s.zXMLinfo__c, 'cpNewDate'));

                ufpBasicMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.Order_Header__r.zXMLinfo__c, 'cvNewUsersBas'));
                ufpConnectMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.Order_Header__r.zXMLinfo__c, 'cvNewUsersCon'));
                ufpCollaborateMap.put(s.Id, qbOrderCTRL.readApexXMLNumber(s.Order_Header__r.zXMLinfo__c, 'cvNewUsersCol'));
                cvNewDate.put(s.Id, qbOrderCTRL.readApexXML(s.Order_Header__r.zXMLinfo__c, 'cvNewDate'));
                cvNewDiscount.put(s.Id, qbOrderCTRL.readApexXML(s.Order_Header__r.zXMLinfo__c, 'cvNewDiscount'));
                cvNewTerm.put(s.Id, qbOrderCTRL.readApexXML(s.Order_Header__r.zXMLinfo__c, 'cvNewTerm'));
            }
        }
        return sites;
    }
    public static String getSiteContactStr(string siteSettingsXml){
        string contactStr='';
        string conPhone= qbOrderCTRL.readApexXML(siteSettingsXml, 'conPhone');
        string conMobile= qbOrderCTRL.readApexXML(siteSettingsXml, 'conMobile');
        string conEmail= qbOrderCTRL.readApexXML(siteSettingsXml, 'conEmail');
        
        contactStr += qbOrderCTRL.readApexXML(siteSettingsXml, 'conFirstName')+' '+qbOrderCTRL.readApexXML(siteSettingsXml, 'conLastname');
        
        if(string.isNotBlank(conPhone))
        	contactStr += ', '+qbOrderCTRL.readApexXML(siteSettingsXml, 'conPhone');
        if(string.isNotBlank(conMobile))
        	contactStr += ', '+qbOrderCTRL.readApexXML(siteSettingsXml, 'conMobile');
        if(string.isNotBlank(conEmail))
        	contactStr += ', '+qbOrderCTRL.readApexXML(siteSettingsXml, 'conEmail');
    	
        return contactStr;
    }
}