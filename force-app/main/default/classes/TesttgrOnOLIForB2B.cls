@isTest
private class TesttgrOnOLIForB2B {   
    static testMethod void myUnitTest() {        
        Competitors_Suppliers_Manufacturers__c csm=new Competitors_Suppliers_Manufacturers__c();    
        csm.Name='Sample';    
        csm.Opportunity__c=true;    
        insert csm;    
        Competitors_Suppliers_Manufacturers__c csm2=new Competitors_Suppliers_Manufacturers__c();  
        csm2.Name='Sample2';   
        csm2.Opportunity__c=true; 
        insert csm2;        
        Account account = Test_Factory.CreateAccount();      
        //  Pricebook2 priceBook = [select Id from Pricebook2 where isStandard=true limit 1];       
        String ICTBooktoBillId='01220000000AGRl';//Hard coding of RecordType Id to avoid Too 'Many SOQL Queries' Issue    
        Id PriceBook = test.getStandardPricebookId();        
        Opportunity opp1 = Test_Factory.CreateOpportunity(account.Id);    
        opp1.closedate = system.today();       
        opp1.Main_Competitor__c=csm.Id;  
        
        Database.SaveResult opptResult = Database.insert(opp1);  
		       
        Product2 product = Test_Factory.CreateProduct();    
        Database.SaveResult productResult = Database.insert(product);      
        PricebookEntry pricebookEntry = Test_Factory.CreatePricebookEntry(priceBook, productResult.getId());   
        Database.SaveResult pricebookEntryResult = Database.insert(pricebookEntry); 
		test.startTest();         
           
        OpportunityLineItem opportunityLineItem = Test_Factory.CreateOpportunityLineItem(pricebookEntryResult.getId(), opptResult.getId()); 
        opportunityLineItem.Category__c='Main';      
        opportunityLineItem.Qty__c=400;      
        opportunityLineItem.Initial_Cost__c=400000;      
        opportunityLineItem.First_Bill_Date__c=System.toDay();   
        insert opportunityLineItem;             
        opp1.Main_Competitor__c=csm2.Id;       
        Database.SaveResult opptResult2 = Database.update(opp1); 
       
        opportunityLineItem.Qty__c=500;     
        opportunityLineItem.Initial_Cost__c=500000;    
        opportunityLineItem.First_Bill_Date__c=System.toDay()+2;  
        opportunityLineItem.Competitor__c=csm.Id;     
        update opportunityLineItem;                          
        BookToBill__c bb2=new BookToBill__c();     
        bb2.Opportunity__c=opptResult.getId();     
        //test.startTest();     
        bb2.RecordTypeId=ICTBooktoBillId;//[Select Id from RecordType where DeveloperName='ICT_Book_to_Bill' LIMIT 1].Id;   
        insert bb2;       
        //test.stopTest();        
        ////Test.StartTest();      
          OpportunityLineItem opportunityLineItem1 = Test_Factory.CreateOpportunityLineItem(pricebookEntryResult.getId(), opptResult.getId());    
        opportunityLineItem1.Category__c='Main';   
        opportunityLineItem1.Qty__c=400;   
        opportunityLineItem1.Initial_Cost__c=400000;     
        opportunityLineItem1.First_Bill_Date__c=System.toDay(); 
        insert opportunityLineItem1;           
        opp1.Main_Competitor__c=csm2.Id;       
        Database.SaveResult opptResult3 = Database.update(opp1);   
         Test.StopTest();  
        
        opportunityLineItem1.Qty__c=500;      
        opportunityLineItem1.Initial_Cost__c=500000;    
        opportunityLineItem1.First_Bill_Date__c=System.toDay()+2;       
        opportunityLineItem1.Competitor__c=csm.Id;     
        update opportunityLineItem1;    
         BookToBill__c bb=new BookToBill__c();     
        bb.Opportunity__c=opptResult.getId();          
        bb.RecordTypeId=ICTBooktoBillId;//[Select Id from RecordType where DeveloperName='ICT_Book_to_Bill'].Id;      
        insert bb;  
        Map<Id,BookToBill__c> B2BMap=new Map<Id,BookToBill__c>(); 
        B2BMap.PUT(bb.Opportunity__c,bb);       
           
    }   
}