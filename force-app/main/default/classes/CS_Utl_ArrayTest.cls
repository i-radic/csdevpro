@IsTest
public class CS_Utl_ArrayTest  {
	@IsTest
	public static void testUtlArrayUsingList() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		List<String> stringList = new List<String>();
		stringList.add('first');
		stringList.add('second');
		stringList.add('third');
		stringList.add('first');

		Test.startTest();
		String checkString = CS_Utl_Array.join(stringList, ',');
		Test.stopTest();
		System.assertEquals('first,second,third,first', checkString);
	}

	@IsTest
	public static void testUtlArrayUsingSet() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Set<String> stringSet = new Set<String>();
		stringSet.add('first');
		stringSet.add('second');
		stringSet.add('third');
		stringSet.add('first');

		Test.startTest();
		String checkString = CS_Utl_Array.join(stringSet, ',');
		Test.stopTest();
		System.assertEquals('first,second,third', checkString);
	}
}