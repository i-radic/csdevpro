@isTest
private class Test_ISDNviewcontroller {
    static testMethod void runPositiveTestCases() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        CRF__c crf = new CRF__C();
        ISDN_CRF__c isdn = new ISDN_CRF__c();
        channels__c channel = new channels__c();
        opportunity o = new opportunity();
        Id MoversId;
        Id NSOId;
        Id BroadbandId;
        Id DDId;
        Id AXId;
        Id WLR3Id;
        Id ResignId;
        Id NPId;
        Id NCId;
        Id ISDNId;
        Id NCPId;
        List<RecordType> RTList = [Select Id,DeveloperName from RecordType where SobjectType=:'CRF__c'];
        if(RTList.size()>0){
            for(RecordType RT:RTList){
                if(RT.DeveloperName=='Movers')
                    MoversId = RT.Id;
                else if(RT.DeveloperName=='NSO_BS')
                    NSOId = RT.Id;
                else if(RT.DeveloperName=='Broadband')
                    BroadbandId = RT.Id;
                else if(RT.DeveloperName=='Direct_Debit_CRF')
                    DDId = RT.Id;
                else if(RT.DeveloperName=='AX_Form')
                    AXId = RT.Id;
                else if(RT.DeveloperName=='WLR3_Authorisation')
                    WLR3Id = RT.Id;
                else if(RT.DeveloperName=='Resign')
                    ResignId = RT.Id;
                else if(RT.DeveloperName=='Number_Port')
                    NPId = RT.Id;
                else if(RT.DeveloperName=='Name_Change_CRF')
                    NCId = RT.Id;
                else if(RT.DeveloperName=='ISDN30')
                    ISDNId = RT.Id;
                else if(RT.DeveloperName=='NCP')
                    NCPId = RT.Id;

            }
        }
        o.name='test';
        o.StageName='FOS_Stage';
        insert o;
        crf.Opportunity__c = o.Id;
        crf.RecordTypeId = ISDNId;
        insert crf;
        isdn.Related_to_CRF__c = crf.Id;
        isdn.Room_No_where_NTE_to_be_fitted__c = '111';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '1';
        isdn.Is_this_a_new_customer_to_BT__c = 'Yes';
        isdn.Tel_no_A_c_no__c = '1111';
        isdn.DASS_2__c = '111';
        isdn.ISDN_30e__c = '2';
        isdn.Total_No_of_DDI_Channels_required__c = '5';
        isdn.Total_No_of_non_DDI_Channels_required__c = '5';
        isdn.VP_Number__c = '14';
        isdn.No_of_DDI_s__c = '15';
        insert isdn;
        channel.Related_to_CRF__c = crf.Id;
        insert channel;
        PageReference PageRef = Page.VF_ISDN_Edit;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id',crf.Id);
        ApexPages.currentPage().getParameters().put('channelId',channel.Id);
        ApexPages.currentPage().getParameters().put('isdncrfId',isdn.Id);

        ApexPages.StandardController ctrl = new ApexPages.StandardController(crf);
        ISDNViewController isdnedit = new ISDNViewController(ctrl);
        isdnedit.OnLoad();
        isdnedit.EditPage();
        isdnedit.gettotalchannels();
        isdnedit.crfid();

    }
}