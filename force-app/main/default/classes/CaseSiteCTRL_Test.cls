@isTest
private class CaseSiteCTRL_Test {
    
    static testMethod void UnitTest() { 
        Case caseRec = new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRec);
        CaseSiteCTRL csc = new CaseSiteCTRL(sc); 
        //caseRec.Creator_EIN_Site__c = '000000000';
        caseRec.Creator_OUC_Site__c = 'TEST';
        caseRec.Type = 'New User Request';
        //caseRec.Subject = 'TEST';
        //caseRec.Description = 'TEST';
        caseRec.SF_Instance__c = 'BPS';
        csc.OnLoad();
        csc.Submit();
        csc.getCategory();
    }
    static testMethod void UnitTest2() {    
        Case caseRec = new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRec);
        CaseSiteCTRL csc = new CaseSiteCTRL(sc); 
        caseRec.Creator_EIN_Site__c = '00000000';
        csc.OnLoad();
        csc.Submit();
        csc.getCategory();
    }
    static testMethod void UnitTest3() {    
        Case caseRec = new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRec);
        CaseSiteCTRL csc = new CaseSiteCTRL(sc); 
        caseRec.Creator_EIN_Site__c = '000000000';
        //caseRec.Creator_OUC_Site__c = 'TEST';
        csc.OnLoad();
        csc.Submit();
        csc.getCategory();
    }
    static testMethod void UnitTest4() {    
        Case caseRec = new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRec);
        CaseSiteCTRL csc = new CaseSiteCTRL(sc); 
        caseRec.Creator_EIN_Site__c = '000000000';
        caseRec.Creator_OUC_Site__c = 'TEST';
        caseRec.Type = 'New User Request';
        csc.OnLoad();
        csc.Submit();
        csc.getCategory();
    }
    static testMethod void UnitTest5() {    
        Case caseRec = new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRec);
        CaseSiteCTRL csc = new CaseSiteCTRL(sc); 
        caseRec.Creator_EIN_Site__c = '000000000';
        caseRec.Creator_OUC_Site__c = 'TEST';
        caseRec.Type = 'New User Request';
        caseRec.Subject = 'TEST';
        csc.OnLoad();
        csc.Submit();
        csc.getCategory();
    }
    static testMethod void UnitTest6() {    
        Case caseRec = new Case();
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRec);
        CaseSiteCTRL csc = new CaseSiteCTRL(sc); 
        caseRec.Creator_EIN_Site__c = '000000000';
        caseRec.Creator_OUC_Site__c = 'TEST';
        caseRec.Type = 'New User Request';
        caseRec.Subject = 'TEST';
        caseRec.Description = 'TEST';
        csc.OnLoad();
        csc.Submit();
        csc.getCategory();
    }
    static testMethod void UnitTest7() {    
        RecordType recordType = [Select ID, Name From RecordType Where sObjectType = 'Case' And RecordType.Name = 'People / User Request']; 
        Case caseRec = new Case();
        caseRec.RecordTypeId = recordType.Id;
        caseRec.Creator_EIN_Site__c = '000000000';
        caseRec.Creator_OUC_Site__c = 'TEST';
        caseRec.Type = 'New User Request';
        caseRec.Subject = 'TEST';
        caseRec.Description = 'TEST';
        caseRec.SF_Instance__c = 'BPS';
        caseRec.Include_In_Updates_Email__c = 'shruthi.ragula@bt.com';
        insert caseRec;
        System.currentPageReference().getParameters().put('id', caseRec.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(caseRec);
        CaseSiteCTRL csc = new CaseSiteCTRL(sc); 
        csc.OnLoad();
        csc.getCategory();
    }
}