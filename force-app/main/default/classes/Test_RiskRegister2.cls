@isTest

private class Test_RiskRegister2{

static testMethod void runPositiveTestCases() {
  
 RiskRegister_escalate escalateRR = new RiskRegister_escalate();
//
//Create Users & hierachy
List<Profile> profiles2 = [Select p.Id, p.Name from Profile p Where p.name = 'Standard User'];
        string profileId2 = profiles2[0].Id;
        
        
 List<User> thisUser = [Select id, Run_Apex_Triggers__c, Manager_EIN__c from User where id = :UserInfo.getUserId() Limit 1];
    thisUser[0].Run_Apex_Triggers__c = False;
    update thisUser[0]; 
 


  User uGM = new User();
      uGM.Username = '999999001@bt.com';
      uGM.Ein__c = '999999001';
      uGM.LastName = 'TestLastname';
      uGM.FirstName = 'TestFirstname';
      uGM.MobilePhone = '07918672032';
      uGM.Phone = '02085878834';
      uGM.Title='What i do';
      uGM.OUC__c = 'DKW';
      uGM.Manager_EIN__c = '123456789';
      uGM.Email = 'no.reply@bt.com';
      uGM.Alias = 'boatid01';
      uGM.TIMEZONESIDKEY = 'Europe/London';
      uGM.LOCALESIDKEY  = 'en_GB';
      uGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
      uGM.PROFILEID = profileId2;
      uGM.LANGUAGELOCALEKEY = 'en_US';
      //uGM.ISACTIVE = FALSE;
      uGM.email = 'no.reply@bt.com';
      insert uGM;
      
    
 
   User uDGM = new User();
      uDGM.Username = '999999002@bt.com';
      uDGM.Ein__c = '999999002';
      uDGM.LastName = 'TestLastname';
      uDGM.FirstName = 'TestFirstname';
      uDGM.MobilePhone = '07918672032';
      uDGM.Phone = '02085878834';
      uDGM.Title='What i do';
      uDGM.OUC__c = 'DKW';
      uDGM.Manager_EIN__c = '999999001';
      uDGM.Email = 'no.reply@bt.com';
      uDGM.Alias = 'boatid01';
      uDGM.managerID = uGM.Id;
      uDGM.TIMEZONESIDKEY = 'Europe/London';
      uDGM.LOCALESIDKEY  = 'en_GB';
      uDGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
      uDGM.PROFILEID = profileId2;
      uDGM.LANGUAGELOCALEKEY = 'en_US';
      //uDGM.ISACTIVE = FALSE;
      uDGM.email = 'no.reply@bt.com';
      insert uDGM;
      
 User uSM = new User();
      uSM.Username = '999999003@bt.com';
      uSM.Ein__c = '999999003';
      uSM.LastName = 'TestLastname';
      uSM.FirstName = 'TestFirstname';
      uSM.MobilePhone = '07918672032';
      uSM.Phone = '02085878834';
      uSM.Title='What i do';
      uSM.OUC__c = 'DKW';
      uSM.Manager_EIN__c = '999999002';
      uSM.Email = 'no.reply@bt.com';
      uSM.Alias = 'boatid01';
      uSM.managerID = uDGM.Id;
      uSM.TIMEZONESIDKEY = 'Europe/London';
      uSM.LOCALESIDKEY  = 'en_GB';
      uSM.EMAILENCODINGKEY = 'ISO-8859-1';                               
      uSM.PROFILEID = profileId2;
      uSM.LANGUAGELOCALEKEY = 'en_US';
      //uSM.ISACTIVE = FALSE;
      uSM.email = 'no.reply@bt.com';
      insert uSM;
      
     
//
//link user running test to hierachy created above
    
    thisUser[0].ManagerId = uSM.ID;
    update thisUser[0]; 
 
 


//Create a Corp Account No 1
   String SACCODE='SACzqx';
   String sacname = 'TestName';
   String LOB='RR9Tst';
   Integer term = 2;
   Integer monthsAgo=4;
   
   Account testAcc = new Account(name = 'TESTCODE',  sac_code__c=SACCODE, Sector_code__c='CORP',LOB_Code__c=LOB);
   Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc}); 
   
   //Create contract on Test Account
Date startDate = Date.today().addMonths(monthsAgo);
Contract testContract = new Contract(Name = 'TESTCODE', 
                                       sac_code__c=SACCODE,
                                       accountid=accResult[0].getId(), 
                                       name__c=SACCODE, startDate=startDate,
                                       Contract_Term_years__c=term);
   Database.SaveResult[] ctrResult = Database.insert(new Contract [] {testContract});
   
 /* create account team contacts
  Account_Team_Contacts__c atc = new Account_Team_Contacts__c();
        atc.sac_code__C = SACCODE;
        atc.UVS_Specialist__c = Userinfo.getUserId();
        atc.UVS_Value_Add__c = Userinfo.getUserId();
        atc.Account_Owner_Primary__c = Userinfo.getUserId(); 
        insert atc; */
  
     Test.startTest();   
     
     

    //create risk
      Risk_Register__c rrA1 = new Risk_Register__c();
        rrA1.Account__c = accResult[0].id;
        rrA1.OwnerId = Userinfo.getUserId();
        rrA1.Annual_Call_Revenue__c = 1;
        rrA1.Annual_Line_Rental_Revenue__c = 1;
        rrA1.At_Risk_Rating__c = '2. High Risk';
        insert rrA1;
  
escalateRR.risk = rrA1;
        rrA1.UVS_SM__c = Userinfo.getUserId();
        rrA1.Risk_Escalated_To__c = Userinfo.getUserId();
        update rrA1; 
escalateRR.escalate();

        rrA1.UVS_SM__c = null;
        rrA1.Risk_Escalated_To__c = Userinfo.getUserId();
        rrA1.UVS_DGM__c = Userinfo.getUserId();
//        update rrA1;  

escalateRR.escalate();




  
/*        
//add record to assett lines  
 Asset_Line_Count__c alc = new Asset_Line_Count__c();
    alc.SAC_Code__c = SACCODE; 
    alc.PSTN__c = 100;
     insert alc;       
         
     //create risk
      Risk_Register__c rrA2 = new Risk_Register__c();
        rrA2.Account__c = accResult[0].id;
        rrA2.OwnerId = Userinfo.getUserId();
        rrA2.Annual_Call_Revenue__c = 1;
        rrA2.Annual_Line_Rental_Revenue__c = 1;
        rrA2.At_Risk_Rating__c = '2. High Risk';
        insert rrA2;    
     

escalateRR.risk = rrA2;
escalateRR.escalate();
escalateRR.escalate();

////////Test against Contract

// create account team contacts
  Account_Team_Contacts__c atc2 = new Account_Team_Contacts__c();
        atc2.sac_code__C = 'testSAC4';
        atc2.UVS_Specialist__c = Userinfo.getUserId();
        atc2.UVS_Value_Add__c = Userinfo.getUserId();
        atc2.Account_Owner_Primary__c = Userinfo.getUserId(); 
        insert atc2;
    
List<Contract> TestContract1 = [Select id from Contract where sac_code__c = 'testSAC4' and name__c = 'Test Contract 4' limit 1];
*/

/*

//Create contract on Test Account
Date startDate = Date.today().addMonths(monthsAgo);
Contract testContract = new Contract(Name = 'TESTCODE', 
                                       sac_code__c=SACCODE,
                                       accountid=accResult[0].getId(), 
                                       name__c=SACCODE, startDate=startDate,
                                       Contract_Term_years__c=term);
   Database.SaveResult[] ctrResult = Database.insert(new Contract [] {testContract});

Risk_Register__c rrC1 = new Risk_Register__c();
        rrC1.Contract__c = ctrResult[0].id;
        rrC1.OwnerId = Userinfo.getUserId();
        rrC1.Annual_Call_Revenue__c = 1;
        rrC1.Annual_Line_Rental_Revenue__c = 1;
        rrC1.At_Risk_Rating__c = '2. High Risk';
        insert rrC1;
        
 escalateRR.risk = rrC1;
escalateRR.escalate();
escalateRR.escalate();       
        
//add record to assett lines  
 Asset_Line_Count__c alc2 = new Asset_Line_Count__c();
    alc2.SAC_Code__c = SACCODE; 
    alc2.PSTN__c = 100;
     insert alc2;     
     
Risk_Register__c rrC2 = new Risk_Register__c();
        rrC2.Contract__c = ctrResult[0].id;
        rrC2.OwnerId = Userinfo.getUserId();
        rrC2.Annual_Call_Revenue__c = 123456789;
        rrC2.Annual_Line_Rental_Revenue__c = 987654321;
        rrC2.At_Risk_Rating__c = '2. High Risk';
        rrC2.Details__c = 'Testing RR Escalate Class';
        insert rrC2;           
        


       
 //remove UVS SM
 List<Risk_Register__c> risklist = [select Id, UVS_SM__c, UVS_DGM__c, Details__c from Risk_Register__c  where Annual_Call_Revenue__c = 123456789 and Annual_Line_Rental_Revenue__c = 987654321 ];
    
escalateRR.risk = rrC2;
        risklist[0].UVS_SM__c = Userinfo.getUserId();
        risklist[0].Risk_Escalated_To__c = Userinfo.getUserId();
        update risklist[0]; 
escalateRR.escalate();

        risklist[0].UVS_SM__c = null;
        risklist[0].Risk_Escalated_To__c = Userinfo.getUserId();
        risklist[0].UVS_DGM__c = Userinfo.getUserId();
        update risklist[0]; 

escalateRR.escalate();
*/
Test.stopTest();

 }
 }