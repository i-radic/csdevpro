@isTest
public class BatchUpdatePriorConsentHAASTest {
	  public static testMethod void testMethod1()
    {  
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Contact c = new Contact();
        c.LastName = 'Test';
        c.Email = 'test@test.com';
        c.EmailPriorConsent__c = 'Yes';
        c.Email_Consent__c = 'Not Asked';
        c.EmailPriorConsentDate__c = Date.today()+1;
        c.Email_Consent_Date__c = Date.Today();
        c.VoicePriorConsent__c = 'No';
        c.Phone_Consent__c = 'Not Asked';
        c.Phone_Consent_Date__c = Date.Today();
        c.VoicePriorConsentDate__c = Date.today()+1;
        c.SMSPriorConsent__c = 'Yes';
        c.Mobile_Consent__c = 'Not Asked';
        c.SMSPriorConsentDate__c = Date.today()+1;
        c.Mobile_Consent_Date__c = Date.today();
        c.PostalPriorConsent__c = 'Yes';
        c.Address_Consent__c = 'Not Asked';
        c.Address_Consent_Date__c = Date.today();
        c.PostalPriorConsentDate__c = Date.today()+1;
        c.Status__c = 'Active';
        c.PriorStatus__c = 'Active';
        Insert c;
        String query = 'Select Id,Name,EmailPriorConsent__c,Email_Consent__c,EmailPriorConsentDate__c,Email_Consent_Date__c,VoicePriorConsent__c,Phone_Consent__c,VoicePriorConsentDate__c,Phone_Consent_Date__c,SMSPriorConsent__c,Mobile_Consent__c,SMSPriorConsentDate__c,Mobile_Consent_Date__c,PostalPriorConsent__c,Address_Consent__c,PostalPriorConsentDate__c,Address_Consent_Date__c,DL_Contact_Sector__c,Email,emailHidden__c,Email_2nd__c,emailHidden_2nd__c,Email_3rd__c,emailHidden_3rd__c,PriorStatus__c,Status__c from Contact where id='+'\'' + c.id + '\'';
        Id batchId= Database.executeBatch(new BatchUpdatePriorConsentHAAS(query),200);
    }   
     public static testMethod void testMethod2()
    {   
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Contact c = new Contact();
        c.LastName = 'Test';
        c.Email = 'test@test.com';
        c.EmailPriorConsent__c = 'Yes';
        c.Email_Consent__c = '';
        c.EmailPriorConsentDate__c = Date.today()+1;
        c.Email_Consent_Date__c = Date.Today();
        c.VoicePriorConsent__c = 'No';
        c.Phone_Consent__c = '';
        c.Phone_Consent_Date__c = Date.Today();
        c.VoicePriorConsentDate__c = Date.today()+1;
        c.SMSPriorConsent__c = 'Yes';
        c.Mobile_Consent__c = '';
        c.SMSPriorConsentDate__c = Date.today()+1;
        c.Mobile_Consent_Date__c = Date.today();
        c.PostalPriorConsent__c = 'Yes';
        c.Address_Consent__c = '';
        c.Address_Consent_Date__c = Date.today();
        c.PostalPriorConsentDate__c = Date.today()+1;
        c.Status__c = 'Active';
        c.PriorStatus__c = 'Active';
        Insert c;
        String query = 'Select Id,Name,EmailPriorConsent__c,Email_Consent__c,EmailPriorConsentDate__c,Email_Consent_Date__c,VoicePriorConsent__c,Phone_Consent__c,VoicePriorConsentDate__c,Phone_Consent_Date__c,SMSPriorConsent__c,Mobile_Consent__c,SMSPriorConsentDate__c,Mobile_Consent_Date__c,PostalPriorConsent__c,Address_Consent__c,PostalPriorConsentDate__c,Address_Consent_Date__c,DL_Contact_Sector__c,Email,emailHidden__c,Email_2nd__c,emailHidden_2nd__c,Email_3rd__c,emailHidden_3rd__c,PriorStatus__c,Status__c from Contact where id='+'\'' + c.id + '\'';
        Id batchId= Database.executeBatch(new BatchUpdatePriorConsentHAAS(query),200);
    }
}