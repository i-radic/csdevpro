public class OPP_agentNotifier
{
    private Opportunity oppty {get; set;}
    //private List<Product2> purchasedProds = new List<Product2>();
    private List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
    private List<Opportunity_Submitted_Order__c> oppSubOrders = new List<Opportunity_Submitted_Order__c>();
    private List<Note> oppNotes = new List<Note>();
    Set<String> prodIds = new Set<String>();
    public String oppyId {get; set;}
    public String mailBody {get; set;}
    public User oOwner {get; set;}
    Map<Id, String> mProdLFS = new Map<Id, String>();
    
    public OPP_agentNotifier() {
        oppyId = '';        
        mailBody = '';
    }
        
    public void getNotificationText()
    {
        String mHead = '';
        String mBobyExtra = '';
        String pCategory = '';      
        //String PIds = '';
        String ONotes = '';
        String TRev = '';
        Double TotValue = 0;
        
        if (oppyId != null){                
            
            oppty = [Select Sales_Stage_Detail__c, Product__c, Owner__c, Owner_Dept__c, Owner_Channel__c, Opportunity_Id__c, Name, 
                    Lead_Information__c, CreatedDate, Amount, Sub_Category__c, Mobile_product__c, Switch_Size__c, StageName, Description, Customer_Type__c,
                    (Select Order_Reference__c From Opportunity_Submitted_Orders__r), 
                    (Select Quantity, UnitPrice, ListPrice, ProductName__c, LFS_Value__c From OpportunityLineItems),
                    (Select Id, Title, Body From Notes)
                    From Opportunity where Opportunity_Id__c = :oppyId];    //ODI-00291467
                    
            oOwner = [Select Name From User where id = :oppty.Owner__c];  
     
            oppLineItems = oppty.OpportunityLineItems;
            oppSubOrders = oppty.Opportunity_Submitted_Orders__r;
            oppNotes = oppty.Notes;
            
            //get purchased Products
            for(OpportunityLineItem oli: oppLineItems){
                prodIds.add(oli.ProductName__c);
                TotValue = TotValue + oli.LFS_Value__c;
                mProdLFS.put(oli.ProductName__c,String.ValueOf(oli.LFS_Value__c));
                //TRev += oli.LFS_Value__c + ', ';
            }        
            /*for(Product2 ppds: [Select Id, Name, Family From Product2 where Id IN : prodIds]){
               PIds += ', ' + ppds.Name + ' - £' + mProdLFS.get(ppds.Id);
            }
            PIds = PIds.replaceFirst(', ', '');
            PIds += '----Total Revenue = £' + String.ValueOf(TotValue);
            */
            String ORefs = '';
            for(Opportunity_Submitted_Order__c sods: oppSubOrders){
               ORefs += ', ' + sods.Order_Reference__c ;
            }
            ORefs = ORefs.replaceFirst(', ', '');
            //--- Amended for CR - SF3902 - Developed By Krupakar---------
            if(oppty.Customer_Type__c == 'BT Business' || oppty.Customer_Type__c == 'SME')  {            
                for(Note opNote: oppNotes){
                   ONotes += opNote.Title + ': '+ opNote.Body + '<br />';
                }
            }
            else {
                ONotes = oppty.Description;
            }
            //--- Amended for CR - SF3902 - Developed By Krupakar---------
            // Amended for CR - SF3902
            /*for(Note opNote: oppNotes){
                   ONotes += opNote.Title + ': '+ opNote.Body + '<br />';
            }*/
            
            /*if(oppty.Sub_Category__c != null){ 
                pCategory = '<br />Category : '+ oppty.Sub_Category__c; 
            } 
            else if(oppty.Mobile_product__c != null){
                pCategory = '<br />Category : '+ oppty.Mobile_product__c;
            }
            else if(oppty.Switch_Size__c != null){
                pCategory = '<br />Category : '+ oppty.Switch_Size__c;
            }*/
             
            if(oppty != null){
                system.debug('StageName: '+oppty.StageName);
                /*if(oppty.StageName == 'Created'){
                    mHead = 'This confirms that your ‘Leads 2 Sales’ Opportunity has been created and submitted<br />';                                 
                }                
                else if(oppty.StageName == 'Won'){
                    mHead = 'This confirms that your ‘Leads 2 Sales’ Opportunity has been successfully completed <br />';
                    mBobyExtra = '<br />Order Reference : '+ ORefs;                 
                    mBobyExtra += '<br /><br />Product(s) sold : '+ PIds;
                    //mBobyExtra += '<br />Total revenue : '+ oppty.Amount;  
                    //mBobyExtra += '<br />Total revenue : '+ TRev;
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;                   
                }
                else if(oppty.StageName == 'Lost'){
                    mHead = 'This confirms that your ‘Leads 2 Sales’ Opportunity has not been successful<br />';
                    mBobyExtra = '<br />Sales Stage Details : '+oppty.Sales_Stage_Detail__c;
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;
                }
                else if(oppty.StageName == 'Cancelled'){*/
                if(oppty.StageName == 'Cancelled'){
                    mHead = 'This confirms that your Opportunity has been cancelled.<br />';
                    mBobyExtra = '<br />Sales Stage Details : '+oppty.Sales_Stage_Detail__c;
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;       
                } 
                /*else{ 
                    mHead = 'The status of your ‘Leads 2 Sales’ Opportunity has changed to :'+ oppty.StageName +'<br />';
                    if(oppty.Sales_Stage_Detail__c != null){
                        mBobyExtra = '<br />Sales Stage Details : '+oppty.Sales_Stage_Detail__c;
                    }                   
                    mBobyExtra += '<br /><br />Notes:<br />'+ ONotes;
                }*/
                
                mailBody += mHead;
                mailBody += '<br />Opportunity Name : '+ oppty.Name; 
                mailBody += '<br />Opportunity ID : '+ oppty.Opportunity_Id__c;
                mailBody += '<br />Product : '+ oppty.Product__c;
                mailBody += pCategory;             
                mailBody += '<br />Created Date : '+ oppty.CreatedDate;
                mailBody += '<br />Opportunity Owner : '+  oOwner.Name;
                mailBody += '<br />Owner Department : '+ oppty.Owner_Dept__c;
                mailBody += mBobyExtra;
                
                mailBody += '<br /><br /><br />Thank you';
                mailBody += '<br /><br /><font color="#ff0000" size="1">Note: This email was sent from a notification-only email address that cannot accept incoming email. Please do not reply to this message</font>';
            }
        }
        
    }   

    /*--------------------TEST METHOD------------------------*/
    static testMethod void OPP_agentNotifierTest(){        
        Account account = Test_Factory.CreateAccount();
        Account dummyAccount = Test_Factory.CreateAccountForDummy();
        insert dummyAccount;
    
        Pricebook2 priceBook = [select Id from Pricebook2 where isStandard=true limit 1];
       
        Opportunity opp1 = Test_Factory.CreateOpportunity(account.id);
        opp1.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(opp1);
         
        Product2 product = Test_Factory.CreateProduct();
        Database.SaveResult productResult = Database.insert(product);
       
        PricebookEntry pricebookEntry = Test_Factory.CreatePricebookEntry(priceBook.Id, productResult.getId());
        Database.SaveResult pricebookEntryResult = Database.insert(pricebookEntry);

        OpportunityLineItem opportunityLineItem = Test_Factory.CreateOpportunityLineItem(pricebookEntryResult.getId(), opptResult.getId());
        insert opportunityLineItem;
        
        OPP_agentNotifier getOpties = new OPP_agentNotifier();
        getOpties.oppyId = 'OID-00697090';
        getOpties.getNotificationText();
        String strBody = getOpties.mailBody;
        //system.debug('mail: '+mail);
        
    }
}