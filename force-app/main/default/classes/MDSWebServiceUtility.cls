/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 8.2020
   @author Mahaboob Basha

   @description A Utility that performs the operations required by MDS Web Service

   @modifications, 
   06.08.2020 [Mahaboob]
   
 */
public class MDSWebServiceUtility {

    public static String FILE_UPLOAD = 'upload';

    public static CS_Integration_Settings__c mdsConfiguration {
        get {
            if (mdsConfiguration == NULL) {
                return getMDSConfiguration();
            }
            return mdsConfiguration;
        }
        set;
    }
    
    private static CS_Integration_Settings__c getMDSConfiguration() {
        CS_Integration_Settings__c mdsConfiguration = new CS_Integration_Settings__c();
        mdsConfiguration = [SELECT MDS_Content_Type__c, MDS_Host__c, MDS_Password__c, MDS_Port__c, MDS_Type__c, MDS_Username__c FROM CS_Integration_Settings__c LIMIT 1];
        return mdsConfiguration;
    }
    
    public static MDSResponseWrapper getMDSResponseWrapper(String status, String message, Integer code){
		MDSResponseWrapper mdsResponseWrapper = new MDSResponseWrapper();
		mdsResponseWrapper.Status = status;
		mdsResponseWrapper.Message = message;
		mdsResponseWrapper.Code = code;
		return mdsResponseWrapper;
	}
}