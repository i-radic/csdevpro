global class BatchLS2L_Schedule02 implements Schedulable{
        
global void execute(SchedulableContext sc) {
    Date DESK= System.Today() - 45;
    String sDESK = String.valueOf(DESK);        

    //DBAM Landscape data matchin criteria to create a lead, some of the logic is in the landscape object formula fields returning true.
    String qString2 = 'SELECT Id, Account__c, Account__r.Name, Account__r.OwnerId, Account__r.Owner.Department ';
    qString2 = qString2 + ' ,xLeads_Flag_Calls__c, xLeads_Flag_Lines__c, xLeads_Flag_BB__c, xLeads_Flag_Mobile__c, xLeads_Flag_Switch__c, xLeads_Flag_LAN__c, xLeads_Flag_WAN__c';
    qString2 = qString2 + ' FROM Landscape_DBAM__c';        
    qString2 = qString2 + ' WHERE (Account__r.xLead_Last_Created__c < ' + sDESK + ' OR Account__r.xLead_Last_Created__c = Null)';
    qString2 = qString2 + ' AND (Account__r.Sub_Sector__c = \'Corporate Desk\')';
    qString2 = qString2 + ' AND (xLeads_Flag_Calls__c = \'True\'';
    qString2 = qString2 + ' OR xLeads_Flag_Lines__c = \'True\'';
    //qString2 = qString2 + ' OR xLeads_Flag_BB__c = \'True\''; 
    qString2 = qString2 + ' OR xLeads_Flag_Mobile__c = \'True\'';
    //qString2 = qString2 + ' OR xLeads_Flag_Switch__c = \'True\''; 
    //qString2 = qString2 + ' OR xLeads_Flag_LAN__c = \'True\'';
    qString2 = qString2 + ' OR xLeads_Flag_WAN__c = \'True\'';
    qString2 = qString2 + ')';
    BatchLS2L_02DBAM b = new BatchLS2L_02DBAM(qString2);
    database.executebatch(b, 100);
} 

}