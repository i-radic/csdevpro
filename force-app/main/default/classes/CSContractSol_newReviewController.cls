public class CSContractSol_newReviewController {
    
    //get all the data needed for insertion of Transactional Clauses
    Public csclm__Agreement__c currentAgreement {get;set;}
    Public Opportunity currentOpportunity {get;set;}
    Public cscfga__Product_Basket__c currentBasket {get;set;} 
    //Public List<csclm__Transactional_Section__c> myListTSections {get; private set;}
    Public csclm__Transactional_Section__c myTargetSection {get;set;}
    Public csclm__Transactional_Clause__c myTargetClause {get;set;}
    Public List<csclm__Transactional_Clause__c> myListTClauses {get; private set;}
    Public double latestSequence {get;set;}
    Public string selectedServicePlan {get;set;}
    

    
    
    Public string configurationsText {get; set;}
    
	Public CSContractSol_newReviewController(ApexPages.StandardController controller) {
    	
        // get the Agreement data from current Agreement
        currentAgreement = [select id,Name,csclm__Latest_Transactional_Document__c, csclm__Opportunity__c, Products_data_populated__c from csclm__Agreement__c where id=:ApexPages.CurrentPage().getparameters().get('id')];

      
    }

	Public void populateMinimumSpend(csclm__Agreement__c agreement) {
        List<csclm__Transactional_Clause__c> clausesList = [Select Id, Name, csclm__Transactional_Section__r.csclm__Transactional_Document__c,
                          csclm__Final_Rich_Text__c
                          FROM csclm__Transactional_Clause__c
                          WHERE csclm__Transactional_Section__r.csclm__Transactional_Document__c =:currentAgreement.csclm__Latest_Transactional_Document__c];

        for (csclm__Transactional_Clause__c clause : clausesList) {
            if (clause.Clause_Description__c != null && clause.Clause_Description__c.contains('Minimum Spend CS_placeholder')){
                myTargetClause = clause;

            }
        }
        myTargetClause.csclm__Final_Rich_Text__c = 'Minimum Spend <b>£ ' + currentBasket.Total_Price__c + '</b>';
        //myTargetClause.csclm__Proposed_Rich_Text__c = myTargetClause.csclm__Final_Rich_Text__c;
        update myTargetClause;
    }

    
    Public void populateMinimumAgreementTerm (csclm__Agreement__c agreement) {
        
        
        List<csclm__Transactional_Clause__c> clausesList = [Select Id, Name, csclm__Transactional_Section__r.csclm__Transactional_Document__c,
                          csclm__Final_Rich_Text__c
                          FROM csclm__Transactional_Clause__c
                          WHERE csclm__Transactional_Section__r.csclm__Transactional_Document__c =:currentAgreement.csclm__Latest_Transactional_Document__c];
        /*
        for (csclm__Transactional_Clause__c clause : clausesList) {
            if (clause.Clause_Description__c != null && clause.Clause_Description__c.contains('Minimum Agreement Term CS_placeholder')){
                myTargetClause = clause;

            }
        }
       */
        /*
        myTargetClause = [Select Id, Name, csclm__Transactional_Section__r.csclm__Transactional_Document__c, Clause_Description__c,
                          csclm__Final_Rich_Text__c
                          FROM csclm__Transactional_Clause__c
                          WHERE csclm__Transactional_Section__r.csclm__Transactional_Document__c =:currentAgreement.csclm__Latest_Transactional_Document__c
                          		AND
                          		Clause_Description__c = 'Part 1 - Commercial Commitments\nMinimum Agreement Term CS_placeholder'];
        */
        myTargetClause.csclm__Final_Rich_Text__c = 'Minimum Agreement Term <b>' + currentBasket.Contract_Term_Period__c + ' Months</b>';
        //myTargetClause.csclm__Proposed_Rich_Text__c = myTargetClause.csclm__Final_Rich_Text__c;
        update myTargetClause;
        
    }
    
    Public string getServicePlan (cscfga__Product_Configuration__c productConfigurationFutureMobileService, List<cscfga__Attribute__c> attributes){
        string result;
        for (cscfga__Attribute__c a : attributes){
            if ((a.cscfga__Product_Configuration__c == productConfigurationFutureMobileService.Id) && (a.Name == 'Service Plan'))
                result = a.cscfga__Display_Value__c;
        }
        return result;
    }
    
    
    Public void populateProductsData (csclm__Agreement__c agreement) {
        
        
        
        
        
        myTargetSection = [Select Id, Name, csclm__Section_Name__c, csclm__Marked_For_Inclusion__c, csclm__Transactional_Document__c
                          FROM csclm__Transactional_Section__c
                          WHERE csclm__Transactional_Document__c =:currentAgreement.csclm__Latest_Transactional_Document__c
                          		AND
                          		Name = 'Part 2 - Services and Equipment'];
        
        
        myListTClauses = [Select Id, Name, csclm__Applicable__c, csclm__Sequence__c, csclm__Transactional_Section__c
                          FROM csclm__Transactional_Clause__c
                          WHERE csclm__Transactional_Section__c =:myTargetSection.Id];
        
        // to determine the vlaue of last clause in this section
		latestSequence = determineLatestSequence (myListTClauses);
        
        //get the Id of Product Basket synchronized with this Opportunity
        currentBasket = [Select Id, Name, Basket_ID__c, csbb__Synchronised_With_Opportunity__c, cscfga__Opportunity__c, Contract_Term_Period__c, Total_Price__c
                         FROM cscfga__Product_Basket__c
                         WHERE cscfga__Opportunity__c =:currentAgreement.csclm__Opportunity__c AND csbb__Synchronised_With_Opportunity__c = true];
        
        // get all the Product Configurations from this Product Basket
        List<cscfga__Product_Configuration__c> configurations = new List<cscfga__Product_Configuration__c>();
        configurations = [Select Id, Name, 
                          cscfga__Product_Basket__c,
                          cscfga__Product_Family__c,
                          cscfga__Contract_Term_Period__c,
                          cscfga__Product_Definition__c,
                          Product_Definition_Name__c,
                          cscfga__total_contract_value__c,
                          
                          cscfga__Parent_Configuration__r.Id
                          from cscfga__Product_Configuration__c
                          where cscfga__Product_Basket__c = :currentBasket.Id                          
                         ];
        
        // get all Attributes for Product Configurations in this Product Basket
		List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>();
        attributes = [Select Id, Name, 
                          cscfga__Product_Configuration__c,
                          Product_Definition_Name__c,
                          Product_Quantity__c,
                          cscfga__Value__c,
                      	  cscfga__Display_Value__c
                          from cscfga__Attribute__c
                          where cscfga__Product_Configuration__c IN :configurations                          
                         ];        
       
		/*
        configurationsText = 'The current Product Basket: ' + currentBasket.Name + ' has the following Product Configurations:\n';
        for (cscfga__Product_Configuration__c ova : configurations){
            configurationsText = configurationsText + ova.Name + '\n';
        }
		*/
        
      	
    	cscfga__Product_Configuration__c productConfigurationFutureMobileService;	// there can be only one Future Mobile Service configuration
        List<cscfga__Product_Configuration__c> productConfigurationsSharedVAS = new List<cscfga__Product_Configuration__c>();
        
    	List<cscfga__Product_Configuration__c> productConfigurationsUserGroup = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> productConfigurationsUserPlans = new List<cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> productConfigurationsUserAddons = new List<cscfga__Product_Configuration__c>();

        List<cscfga__Product_Configuration__c> productConfigurationsServicePlans = new List<cscfga__Product_Configuration__c>();

        List<cscfga__Product_Configuration__c> productConfigurationsFutureMobileCreditFunds = new List<cscfga__Product_Configuration__c>();

        List<cscfga__Product_Configuration__c> productConfigurationsDevices = new List<cscfga__Product_Configuration__c>();

        List<cscfga__Product_Configuration__c> productConfigurationsSpecialConditions = new List<cscfga__Product_Configuration__c>();        
        
        
        //now to sort the configuration based on Product Definition Name
        for (cscfga__Product_Configuration__c currentPC : configurations) {

            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('Future Mobile Service')) {
                productConfigurationFutureMobileService = currentPC;
            }

            if (currentPC.Product_Definition_Name__c != null && (currentPC.Product_Definition_Name__c.contains('Shared VAS')) ) {
                productConfigurationsSharedVAS.add(currentPC);
            }

            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('User Group')) {
                productConfigurationsUserGroup.add(currentPC);
            }
            
            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('User Plans')) {
                productConfigurationsUserPlans.add(currentPC);
            }

            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('User Addons')) {
                productConfigurationsUserAddons.add(currentPC);
            }   
            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('Service Plans')) {
                productConfigurationsServicePlans.add(currentPC);
            }  
            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('Future Mobile Credit Funds')) {
                productConfigurationsFutureMobileCreditFunds.add(currentPC);
            }  
            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('Devices')) {
                productConfigurationsDevices.add(currentPC);
            }  
            if (currentPC.Product_Definition_Name__c != null && currentPC.Product_Definition_Name__c.contains('Special Conditions')) {
                productConfigurationsSpecialConditions.add(currentPC);
            }  
        }
        
        // determine which Service Plan is selected: Individual, Teams, Shared or Custom Caller
        selectedServicePlan = getServicePlan (productConfigurationFutureMobileService, attributes);
        
        
        
        
        // now to insert all the needed Transactional Clauses into CLM structure
         
        string textToInsert = '';
        
        textToInsert += productConfigurationFutureMobileService.Name;
        textToInsert += 'Selected Service Plan is: ' + selectedServicePlan;
        
        
        csclm__Transactional_Clause__c myNewClause = new csclm__Transactional_Clause__c();
        myNewClause.csclm__Applicable__c = true;
        myNewClause.csclm__Final_Rich_Text__c = textToInsert;
        myNewClause.csclm__Proposed_Rich_Text__c = textToInsert;
        myNewClause.csclm__Manually_Added__c = false;
        myNewClause.csclm__Marked_For_Inclusion__c = true;
        myNewClause.csclm__Override_Text__c = false;
        myNewClause.csclm__Sequence__c = latestSequence + 1.0;
        myNewClause.csclm__Transactional_Section__c = myTargetSection.Id;
        //myNewClause.Clause_Description__c='nesto';
        
        insert myNewClause;
        
        
        
        
        
        
        
        
        /*
        // first insert Clauses for Future Mobile Service
        for (cscfga__Product_Configuration__c currentPC : productConfigurationsFutureMobileService) {
            textToInsert = 'Configured solution: <b>' + textToInsert + currentPC.cscfga__Description__c + '</b>';
            insertTransactionalClause(textToInsert);
            textToInsert = '';
            
            // now determine if any other Product Configuration has this one for it's parent
            for (cscfga__Product_Configuration__c possibleChild : productConfigurationsSharedVAS) {
                if (possibleChild.cscfga__Parent_Configuration__r.Id == currentPC.Id) {
                    textToInsert = 'Shared VAS: <b>' + possibleChild.Name + '</b>';
                    insertTransactionalClause(textToInsert);
                    textToInsert = '';
                }
            }
            
        }
        */
        // now insert User Groups
        for (cscfga__Product_Configuration__c currentPC : productConfigurationsUserGroup) {
            textToInsert = 'User Group: <b>' + textToInsert + currentPC.Name + '</b>';
            insertTransactionalClause(textToInsert);
            textToInsert = '';
        }
        
        
        
        
        
        
        
        
        /*
        //here goes the code for creating Transactional Clauses for insertion in CLM
        csclm__Transactional_Clause__c myNewClause = new csclm__Transactional_Clause__c();
        myNewClause.csclm__Applicable__c = true;
        myNewClause.csclm__Final_Rich_Text__c = 'ovo je moj text u myNewClause' + ' i Basket je: ' + currentBasket.Name + ' na Id: ' + currentBasket.Id;
        myNewClause.csclm__Proposed_Rich_Text__c = myNewClause.csclm__Final_Rich_Text__c;
        myNewClause.csclm__Manually_Added__c = false;
        myNewClause.csclm__Marked_For_Inclusion__c = true;
        myNewClause.csclm__Override_Text__c = false;
        myNewClause.csclm__Sequence__c = latestSequence + 1.0;
        myNewClause.csclm__Transactional_Section__c = myTargetSection.Id;
        //myNewClause.Clause_Description__c='nesto';
        
        insert myNewClause;
        
        
		//here goes the code for creating Transactional Clause with list of Product Configurations
        csclm__Transactional_Clause__c myPCList = new csclm__Transactional_Clause__c();
        myPCList.csclm__Applicable__c = true;
        myPCList.csclm__Final_Rich_Text__c = configurationsText;
        myPCList.csclm__Proposed_Rich_Text__c = myPCList.csclm__Final_Rich_Text__c;
        myPCList.csclm__Manually_Added__c = false;
        myPCList.csclm__Marked_For_Inclusion__c = true;
        myPCList.csclm__Override_Text__c = false;
        myPCList.csclm__Sequence__c = latestSequence + 2.0;
        myPCList.csclm__Transactional_Section__c = myTargetSection.Id;
        //myPCList.Clause_Description__c='nesto';
        
        insert myPCList;        
        */
        
        //populateMinimumAgreementTerm(agreement);
        
        //populateMinimumSpend(agreement);
        
        agreement.Products_data_populated__c = true;
        update agreement;
    }
    
    //here goes the code for creating Transactional Clauses for insertion in CLM
    Public void insertTransactionalClause (string text){
               
        csclm__Transactional_Clause__c myNewClause = new csclm__Transactional_Clause__c();
        myNewClause.csclm__Applicable__c = true;
        myNewClause.csclm__Final_Rich_Text__c = text;
        myNewClause.csclm__Proposed_Rich_Text__c = myNewClause.csclm__Final_Rich_Text__c;
        myNewClause.csclm__Manually_Added__c = false;
        myNewClause.csclm__Marked_For_Inclusion__c = true;
        myNewClause.csclm__Override_Text__c = false;
        myNewClause.csclm__Sequence__c = latestSequence + 1.0;
        latestSequence = latestSequence + 1.0;
        myNewClause.csclm__Transactional_Section__c = myTargetSection.Id;      
        insert myNewClause;
    }
    
    private double determineLatestSequence (List<csclm__Transactional_Clause__c> lista){
        double a = 0;
        
        for (csclm__Transactional_Clause__c current : lista) {
            if (current.csclm__Sequence__c > a)
            	a = current.csclm__Sequence__c;
    	}
        return a;
    }    
    
    
    
    
    Public PageReference doWork() {
        
        // for initial call of "CS Review Document", populates the data with Product Basket/Product Configuration/Attributes
        // and sets the Products_data_populated__c flag on Agreement record
        if (!currentAgreement.Products_data_populated__c)
            populateProductsData (currentAgreement);
        
        return new PageReference ('/apex/csclmcb__ReviewDocumentLoader?id=' + ApexPages.CurrentPage().getparameters().get('id'));
    }


}