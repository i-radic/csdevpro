//------------------------------------------------------------------------------
// Class for generating CSV table
//------------------------------------------------------------------------------
public with sharing class CS_CSVTable implements CS_ITable
{
    //--------------------------------------------------------------------------
    // Private members
    //--------------------------------------------------------------------------
    private List<CS_IRow> rows;
    private String separator;
    private String newLine;
    private Boolean escapeAll;

    //--------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------
    public CS_CSVTable(List<CS_IRow> rows, String separator, String newLine)
    {
        init(rows, separator, newLine, false);
    }

    //--------------------------------------------------------------------------
    // Constructor
    //--------------------------------------------------------------------------
    public CS_CSVTable
        ( List<CS_IRow> rows
        , String separator
        , String newline
        , Boolean escapeAll )
    {
        init(rows, separator, newLine, escapeAll);
    }

    //--------------------------------------------------------------------------
    // Initialize members
    //--------------------------------------------------------------------------
    private void init
        ( List<CS_IRow> rows
        , String separator
        , String newLine
        , Boolean escapeAll )
    {
        this.rows = rows;
        this.separator = separator;
        this.newLine = newLine;
        this.escapeAll = escapeAll;
    }

    //--------------------------------------------------------------------------
    // Generates table in CSV string
    //--------------------------------------------------------------------------
    public String generate()
    {
        String returnValue = CS_Util.utf8BOM();
        for (CS_IRow row : rows)
        {
            returnValue += generateRow(row);
        }
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Generate one row
    //--------------------------------------------------------------------------
    private String generateRow(CS_IRow row)
    {
        String returnValue = '';
        List<String> cells = row.getCells();
        Iterator<String> it = cells.iterator();
        while (it.hasNext())
        {
            String cell = it.next();
            returnValue += generateCell(cell);
            //
            // If not last element put separator
            //
            if (it.hasNext())
            {
                returnValue += separator;
            }
            //
            // Else put newline
            //
            else
            {
                returnValue += newLine;
            }
        }
        return returnValue;
    }

    //--------------------------------------------------------------------------
    // Generate one cell
    //--------------------------------------------------------------------------
    private String generateCell(String cell)
    {
        String returnValue;
        //
        // Threat null cell like empty
        //
        if (cell == null)
        {
            cell = '';
        }
        //
        // Check if cell contains separator, new line or quote
        // or escapeAll flag and cell is not empty
        //
        if ((escapeAll && !String.isEmpty(cell))
            || cell.indexOf(separator) >= 0
            || cell.indexOf('\n') >= 0
            || cell.indexOf('\r') >= 0
            || cell.indexOf('"') >= 0)
        {
            //
            // Put string in quotes
            //
            returnValue = '"'
                    + cell.replaceAll('"', '""')
                    + '"';
        }
        else
        {
            //
            // Leave cell as it is
            //
            returnValue = cell;
        }
        return returnValue;
    }
}