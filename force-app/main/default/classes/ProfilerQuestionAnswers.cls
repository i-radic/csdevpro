/******************************************************************************************************
Name : ProfilerQuestionAnswers 
Description : Controller for page OpportunityProfiler
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    25/11/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public without sharing class ProfilerQuestionAnswers 
{
    public List<Id> listQue { get; set; }
    public List<QuestionnaireQuestion__c> listOfQuestions {get;set;}
    public list<wrapperClass> lstNewWrap {get;set;}
    public String OppProfilerlabel {get;set;}
    public Opportunity objOpp {get;set;}
    public String strOppId    {get;set;}
    public boolean Showscore    {get;set;}
    public Questionnaire__c objQuestionnaire{get;set;}
    public Questionnaire__c Questionnaire= new Questionnaire__c ();
    //public String strValues {get;set;}
    public integer counter;
    public map<id,QuestionnaireReply__c> questreplyMap = new map<id,QuestionnaireReply__c>();
 /******************************************************************************************************
    Method Name : ProfilerQuestionAnswers
    Description : Wrapper class for ProfilerQuestionAnswers
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public ProfilerQuestionAnswers () 
    {
        OppProfilerlabel = GlobalConstants.PROFILER_LABEL;
        counter=0;
        Showscore=false; 
        listOfQuestions = [SELECT Id, Question__c,Name,Order__c, (SELECT Answer__c , Score__c , Questionnaire_Question__c , Order__c FROM Questionnaire_Answers__r WHERE RecordType.DeveloperName=: GlobalConstants.QUESTIONARRE_ANSWER_PROFILER_RECORDTYPE order by Order__c) 
        FROM QuestionnaireQuestion__c WHERE RecordType.DeveloperName =: GlobalConstants.QUESTIONARRE_QUESTIONNAIRE_PROFILER_RECORDTYPE order by Order__c];
        strOppId = ApexPages.currentPage().getParameters().get('opp');
        if(strOppId!=NULL) 
        {
            objOpp = [SELECT Id,Name,StageName,CSC_Call_Pass__c FROM Opportunity WHERE Id=:strOppId];
        }
        listQue = new List<Id>();
        lstNewWrap  = new list<wrapperClass>();  
        if(!listOfQuestions.isEmpty()) 
        {
            integer intval=0;
            for(QuestionnaireQuestion__c que: listOfQuestions) 
            {   
                listQue.add(que.Id);
                List<SelectOption> options = new List<SelectOption>();
                for( QuestionnaireAnswer__c answers: que.Questionnaire_Answers__r)
                {
                    if(answers.Questionnaire_Question__c == que.Id) 
                    {                  
                        options.add(new SelectOption(answers.Id ,answers.Answer__c));  
                    }
                }
                intval++;
                wrapperClass objWrap = new wrapperClass(options,que);
                system.debug('%%%%%%%%%%%'+objWrap);
                objWrap.SrNo = intval;
                lstNewWrap.add(objWrap );  
            }
        }
    } 
 /******************************************************************************************************
Name : wrapperClass 
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    25/11/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/    
    public without sharing class wrapperClass 
    {
        public List<SelectOption> lstQuestionswrap{get;set;}
        public QuestionnaireQuestion__c Questions{get;set;}
        public String selectedAnswer {get;set;}
        public Integer SrNo {get;set;}
        //public Id selectedAnswerId {get;set;}
/******************************************************************************************************
   Description : Method for wrapper class
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/ 
        public wrapperClass (List<SelectOption> Answers , QuestionnaireQuestion__c Questions)
        {
        this.lstQuestionswrap=Answers;
        this.Questions=Questions;     
        SrNo=0;
        }
    }
/******************************************************************************************************
    Method Name : submitMethod
    Description : Method to submit QuestionaireReply
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public void submitMethod ()
    {  
        //List<Questionnaire__c>lstQuestionnaireobj = new List<Questionnaire__c> ();
       // QuestionnaireReply__c objQuestionnaireply = new QuestionnaireReply__c ();
        //List<QuestionnaireReply__c> lstQuestionReplyForUpdateVal = new List<QuestionnaireReply__c> ();
        //List<QuestionnaireReply__c> lstQuestionReplyForUpdateNewVal = new List<QuestionnaireReply__c> ();
        List<QuestionnaireReply__c> lstQuestionReply = new List<QuestionnaireReply__c> ();
        List<QuestionnaireReply__c> qustreplyList = new List<QuestionnaireReply__c>();
        RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                         DeveloperName =: GlobalConstants.QUESTIONNAIRE_REPLY_PROFILER_RECORDTYPE];
       try{
            if(counter == 0)
            {
                if(objOpp !=null)
                {
                    Questionnaire.Opportunity__c = objOpp.Id; 
                }
                RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                      DeveloperName =: GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE];
                Questionnaire.RecordTypeId = QuestionaireRecType.Id;
                insert Questionnaire;    
                counter+=1;
                //declaring object out of the loop
                QuestionnaireReply__c queReply;
                for(wrapperClass wrap : lstNewWrap)
                {
                    queReply = new QuestionnaireReply__c();
                    queReply.Questionnaire__c =  Questionnaire.id;
                    queReply.Question__c = wrap.Questions.Id;
                    queReply.Answer__c = wrap.selectedAnswer;
                    queReply.RecordTypeId = QuestionaireReplyRecType.Id;
                    System.debug('***********queReply***********'+queReply);
                    lstQuestionReply.add(queReply);
                }
            }
            else
            {
                qustreplyList=[select id,Answer__c,Question__c,Questionnaire__c from QuestionnaireReply__c where Questionnaire__c =:Questionnaire.id];
                for(QuestionnaireReply__c reply : qustreplyList)
                {
                    questreplyMap.put(reply.Question__c,reply);
                }
                //declaring object out of the loop
                 QuestionnaireReply__c queReply;
                for(wrapperClass wrap : lstNewWrap)
                {
                    queReply = new QuestionnaireReply__c();
                    queReply=questreplyMap.get(wrap.Questions.Id);
                    queReply.Question__c = wrap.Questions.Id;
                    queReply.Answer__c = wrap.selectedAnswer;
                    queReply.RecordTypeId = QuestionaireReplyRecType.Id;
                    System.debug('***********queReply***********'+queReply);
                    lstQuestionReply.add(queReply);
                }
            }
                if(!lstQuestionReply.isEmpty())
                {
                    upsert lstQuestionReply;   
                }   
              objQuestionnaire =[select id,Output__c from Questionnaire__c where id =: Questionnaire.id];  
              system.debug(objQuestionnaire.Output__c +'Output__c >>');
              Showscore=true;     
        }
        Catch(Exception e)
        {
            String msg = e.getMessage();
            String throwmsg;
            
            if (msg.CONTAINS(GlobalConstants.CUSTOM_VALIDATION_ERROR))
            {
                throwmsg = msg.substringBetween (GlobalConstants.CUSTOM_VALIDATION_ERROR,GlobalConstants.ERROR_MESSAGE_END);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,throwmsg));
            }
        }
    } 
 /******************************************************************************************************
    Method Name : canclebutton 
    Description :Page Redirecting on clicking canclebutton 
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created       
 *********************************************************************************************************/
    public pagereference canclebutton () 
    {
        try
        {
            pagereference pgreference = new pagereference (GlobalConstants.URL_HARD_CODE+strOppId);
            pgreference.setRedirect(true);
            return pgreference;
        }catch(exception e)
        {
            return null;
        }
    }
/******************************************************************************************************
    Method Name : close
    Description :Page Redirecting on clicking Close
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created          
 *********************************************************************************************************/
      public Pagereference close()
    {     
        try
        {
            PageReference orderPage = new PageReference(GlobalConstants.URL_HARD_CODE+ strOppId);
            orderPage.setRedirect(true);
            return orderPage;
        }catch(exception e)
        {
            return null;
        }
    }
}