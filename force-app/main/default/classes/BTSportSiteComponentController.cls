/*
*  This controller generates email template body for the visualforce component "BT_Sport_Template_Component" 
* 
*      Author                 Date         Version      Description
*  Venkata Srinivas M       04/05/2015                  Initial development
*   Alan Jackson            04/06/2015      V1.01       update flag around line 107
*   Praveen Malyala         07/07/2017      V1.02       get end dates for PSTN, BB and Music

*/

public class BTSportSiteComponentController{
    
    public String btSportId {get;set;}
    public BT_Sport_Site__c btSport {get;set;}
    public String custName {get;set;}
    public Date myDate {get; set;}
    public Date endDate {get; set;}
    public Date firstRowDate {get; set;}
    public Date secondRowDate {get; set;}
    public Date thirdRowDate {get; set;}
    public Boolean firstRowFlag {get; set;}
    public Boolean secondRowFlag {get; set;}
    public Boolean thirdRowFlag {get; set;}
    public OpportunityContactRole OppContactRole{get;set;}
    public Boolean flag {get; set;}
    public String sName {get;set;}
    public String type {get; set;}
    public Boolean proText {get; set;}
    public Boolean SportTotalFlag {get; set;}
    public Date season3RowDate {get; set;}
    public Boolean season3RowFlag {get; set;}
    public Date season4RowDate {get; set;}
    public Boolean season4RowFlag {get; set;}
    public Date endDatePSTN {get; set;}
    public Date endDateBB {get; set;}
    public Date endDateWiFi {get; set;}
    public Date endDateMusic {get; set;}
    public Integer addMonths {get; set;}
    
    
    public void getData(){
        try {      
            if(type.contains('Quote'))
            {
                flag=false;
            }
            else
            {
                flag=true;
            }
            thirdRowFlag = false; 
            btSport = new BT_Sport_Site__c();
            btSport=[Select  Order_Reference__c,BT_Sport_CL__r.Opportunity__c,Name,BT_Sport_CL__r.Opportunity__r.Order_Reference__c,Order_Date__c,
                     Building_Number__c,County__c,Post_Code__c,Discounts__c,Street__c,Town__c,Site_Type_S2__c,Discounts_S2__c,
                     Price_after_Discounts_S2__c,Gross_Price_S2__c,Product__c,Site_Type__c,Price_after_Discounts__c,Gross_Price__c,
                     BT_Sport_CL__r.Opportunity__r.Opportunity_Id__c,Discount_Period_Months_S2__c,Discount_Period_End_Date__c,
                     PMF_Effective_Date__c,Discount_Period_End_Date_S2__c,Discount_Period_Months__c,Price_after_Discounts_RO__c,Contract_Type__c,
                     Broadband_Product__c,Broadband_Offers__c,Broadband_Bundle_Offers__c,PSTN_Product__c,PSTN_Offer__c,Broadband_Price__c,PSTN_Price__c,PSTN_Price_S5__c,
                     Music_for_Business_Price__c, Music_for_Business_Offer__c,Broadband_Price_s4__c,	BT_Sport_Wi_Fi__c, BT_Sport_Wi_Fi_Price__c, BT_Sport_Wi_Fi_Offer__c,
                     Standard_One_Off_charges__c,BT_WiFi_Discounted_One_Off_Charge__c,Music_for_Business__c,Music_Std_One_Off_Charge__c,Music_Discounted_One_Off_after_Offer__c
                     from BT_Sport_Site__c where Id=:btSportId];
            
            if(btSport.Product__c!=NULL && btSport.Product__c=='BT Sport Total')
            {
                proText=true;
            }      
            else 
            {
                proText=false;
            } 
            
            //Season4 Settings START ////////////////////////////////
            if(btSport.Product__c!=NULL && btSport.Product__c=='BT Sport Total')
            {
                SportTotalFlag=true;
            }      
            else 
            { 
                SportTotalFlag=false;
            }
            
            //// set dates for quote Display            
            
            if(btSport.PMF_Effective_Date__c !=NULL)
            {
                season3RowDate=btSport.PMF_Effective_Date__c;
            }
            else{
                if(btSport.Order_Date__c != NULL)
                {
                    season3RowDate=btSport.Order_Date__c;
                }
                else
                {
                    season3RowDate=System.today();
                }
            }
            if(season3RowDate > Date.newInstance(2016, 8, 31))
            {
                season3RowFlag = false;
            }
            else
            {
                season3RowFlag = true;
            }
            season4RowFlag = true;
            season4RowDate =Date.newInstance(2016, 9, 1);          
            
            //Season4 Settings END  ///////////////////////////////////
            
            if(btSport.PMF_Effective_Date__c !=NULL)
            {
                firstRowDate=btSport.PMF_Effective_Date__c;      
            }
            else{
                if(btSport.Order_Date__c != NULL){
                    firstRowDate=btSport.Order_Date__c;//System.today();
                }
                else
                {
                    firstRowDate=System.today();
                }
            }
            if(firstRowDate > Date.newInstance(2015, 6, 30))
            {
                firstRowFlag = false;                
            }
            else
            {
                //secondRowDate =Date.newInstance(2015, 7, 1);
                firstRowFlag = true;
            }
            if(firstRowDate > Date.newInstance(2015, 8, 31))
            {
                secondRowFlag =false;
            }
            else
            {
                secondRowFlag =true;
            }
            if(btSport.Order_Date__c> Date.newInstance(2015, 7, 1))
            {
                secondRowDate =btSport.Order_Date__c;
            }
            else
            {
                secondRowDate =Date.newInstance(2015, 7, 1);
            }
            if(btSport.Order_Date__c> Date.newInstance(2015, 9, 1))
            {
                thirdRowDate =btSport.Order_Date__c;
            }
            else
            {
                thirdRowDate =Date.newInstance(2015, 9, 1);
            }
            system.debug('***firstRowDate'+firstRowDate+' flag '+firstRowFlag +'  secondrowFlag'+secondRowFlag+'thirdRowDate'+thirdRowDate);
            system.debug('*******'+btSport.Discount_Period_Months_S2__c+'----'+btSport.Building_Number__c);
            if(btSport.Discount_Period_Months_S2__c!=null) {       
                myDate =btSport.Order_Date__c.addMonths(Integer.valueOf(btSport.Discount_Period_Months_S2__c));
                myDate = myDate.addDays(-1);
            }
            else if(btSport.Discount_Period_End_Date_S2__c!=null)
            {
                myDate = btSport.Discount_Period_End_Date_S2__c;
            }
            /*else
{
myDate = Date.newInstance(2015, 8, 31); 
}*/
            system.debug('***MyDate****'+myDate);
            if(btSport.Discount_Period_Months__c!=null && thirdRowDate!=null)
            {
                endDate=thirdRowDate.addMonths(Integer.valueOf(btSport.Discount_Period_Months__c));
                endDate= endDate.addDays(-1);
            }
            else
            {
                endDate=Date.newInstance(2016, 8, 31);
            }
            system.debug('**End Date**'+endDate);
            if(myDate >=Date.newInstance(2015, 9, 1) && btSport.Discounts__c==null)
            {
                thirdRowFlag =true; // v1.01 Change :- was set to True to display season2 discounts if they went into S3
            }
            else
            {
                thirdRowFlag =false;
            }
            system.debug('****thirdRowFlag***'+thirdRowFlag);
            List<OpportunityContactRole> oppConRole= new List<OpportunityContactRole>();
            OppContactRole = new OpportunityContactRole();
            
            oppConRole=[Select Id,Contact.Name,Contact.Id,Contact.Contact_Address_Number__c,Contact.Contact_Address_Street__c,Contact.Contact_Building__c,
                        Contact.Contact_Post_Town__c,Contact.Address_County__c,Contact.Contact_Post_Code__c,Contact.Contact_Sub_Building__c
                        from OpportunityContactRole where OpportunityId=:btSport.BT_Sport_CL__r.Opportunity__c];
            
            if(oppConRole.size()>0){
                OppContactRole = oppConRole[0];
            }
            
            if(btSport.BT_Sport_Wi_Fi_Offer__c!=null)
            {                
                addmonths=getAddMonths(btSport.BT_Sport_Wi_Fi_Offer__c);
                
                endDateWiFi =btSport.Order_Date__c.addMonths(addmonths);
                endDateWiFi= endDateWiFi.addDays(-1);
            }
            else
            {
                endDateWiFi=Date.newInstance(2016, 8, 31);
            }
            if(btSport.PSTN_Offer__c!=null)
            {                
                addmonths=getAddMonths(btSport.PSTN_Offer__c);
                
                endDatePSTN =btSport.Order_Date__c.addMonths(addmonths);
                endDatePSTN= endDatePSTN.addDays(-1);
            }
            else
            {
                endDatePSTN=Date.newInstance(2016, 8, 31);
            }
            if(btSport.Broadband_Bundle_Offers__c!=null)
            {                
                addmonths=getAddMonths(btSport.Broadband_Bundle_Offers__c);
                
                endDateBB =btSport.Order_Date__c.addMonths(addmonths);
                endDateBB= endDateBB.addDays(-1);
            }
            else
            {
                endDateBB=Date.newInstance(2016, 8, 31);
            }
            if(btSport.Music_for_Business_Offer__c!=null)
            {                
                addmonths=getAddMonths(btSport.Music_for_Business_Offer__c);
                
                endDateMusic =btSport.Order_Date__c.addMonths(addmonths);
                endDateMusic= endDateMusic.addDays(-1);
            }
            else
            {
                endDateMusic=Date.newInstance(2016, 8, 31);
            }
            system.debug('mpk endDateWiFi '+endDateWiFi+' endDatePSTN '+endDatePSTN +'  endDateBB '+endDateBB+' endDateMusic '+endDateMusic);
        }
        catch(Exception ex) {
            system.debug(ex);
        }
    }
    Private Integer getAddMonths(string offerName)
    {
        integer addMnths=null;
        if(offerName.contains('6 Month'))
            addMnths= 6;
        else if(offerName.contains('12 Month'))
            addMnths= 12;
        else if(offerName.contains('24 Month'))
            addMnths= 24;
        
            return addMnths;
    }
    
}