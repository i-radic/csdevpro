public with sharing class LeaseDeskUpdateProposalController {
    /******************************************************************************************************************
     Name:  LeaseDeskUpdateProposalController.class()
     Copyright © 2012  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    This class covers use case SF-007 Check/Refresh Proposal Status
    Proposal status will be automatically updated in SF via the batch feed but we will also 
    provide a button in Salesforce to refresh the data on-demand if a more recent update is required.
                                                           
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR                 DATE              DETAIL                           FEATURES
    1.0 -    Rita Opoku-Serebuoh    13/04/2012        INITIAL DEVELOPMENT              Initial Build: 
             Adam Soobroy
    ******************************************************************************************************************/
    
    public Leasing_History__c leasingHistory {get;set;}
    public String leasingHistoryId {get;set;}
    public String opptyId {get;set;}
    public Account account {get;set;}
    public Boolean displayMessage {get;set;}
    public User user {get;set;}
    public LeaseDeskAPI.ProfileType profileType {get;set;}
    
    public Boolean getdisplayMessage()
    {
        return displayMessage;    
    } 
     
    public void setdisplayMessage(Boolean value)
    {
        this.displayMessage = value;
    }    
    
    public void LeaseDeskUpdateProposalController(){
    }
    
    public LeaseDeskUpdateProposalController(ApexPages.StandardController stdController) {
    }
    
    public PageReference OnLoad()
    {       
        // get AgreementId,LeaseDeskId, CreaditLimit etc.
        leasingHistoryId = ApexPages.currentPage().getParameters().get('id');  
        opptyId = ApexPages.currentPage().getParameters().get('opptyid');      
        if(Test_Factory.GetProperty('IsTest') == 'yes' || Test_Factory.GetProperty('IsTest') == 'no'){
        
        }
        else{
            leasingHistory = LeaseDeskUtil.getLeasingHistory(leasingHistoryId);
            account = LeaseDeskUtil.getAccountDetails(leasingHistory.Account__c);
        }        
        user = LeaseDeskUtil.getUserDetails();
        
        // check if Corp or LB and set LD.API Profile Type
        if(LeaseDeskUtil.isBTLBAccount(account.Id)){
        	profileType = LeaseDeskAPI.ProfileType.LB;
        }
        else{
        	profileType = LeaseDeskAPI.ProfileType.Corp;
        }
                
        if (leasingHistory != null){
            LeaseDeskAPI leaseDeskAPI = new LeaseDeskAPI();
            String checkProposalResponse = leaseDeskAPI.CheckProposal(user.EIN__c, user.OUC__c,leasingHistory.AgreementID__c, user.Leasing_iManage_Username__c, profileType);
            
            if (checkProposalResponse.contains('ERROR')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, checkProposalResponse));  
                displayMessage = true;           
                return null;
            }else{
                leasingHistory.clearance__c = checkProposalResponse;
                update leasingHistory;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Status successfully updated'));
                displayMessage = true;
                return null;
            } 
        }
        return null;            
    }
}