global class CS_DiscountBeforeBasketSaveObserver implements csdiscounts.IBasketObserver {

    public CS_DiscountBeforeBasketSaveObserver() {}

    private static String CONTROLLER_NAME = 'CS_DiscountBeforeBasketSaveObserver';

    String returnMessage = '';

    public String execute(String basketId) {

        try {
            cscfga__Product_Basket__c basket = (cscfga__Product_Basket__c) JSON.deserialize(basketId, cscfga__Product_Basket__c.class);
                if (CS_ProductBasketService.containsCalculateProductGroup(basket.Id)) {
                return returnMessage;
            }
            Map<Id, cscfga__Product_Configuration__c> configs = CS_Util.getConfigurationsInBasket(basket.Id, CONTROLLER_NAME);
            System.debug('CS_DiscountBeforeBasketSaveObserver configs>>'+configs);
            if(!CS_ProductBasketService.validateCreditFunds(configs)) {
                returnMessage = '{"status":"500", "message":"Airtime Fund, Device Fund and Hardware Extra are applicable only for BT Mobile. Please do not enter these for other products."}';
            }
            else {
                returnMessage = '';
            }
        }
        catch(Exception exc){
            logger('csexception = ' + exc);
        }

        logger('returnMessage = ' + returnMessage);
        return returnMessage;
    }

    @TestVisible private void logger(String msg){
        system.debug(LoggingLevel.WARN, 'CS_DiscountBeforeBasketSaveObserver.' + msg);
    }
}