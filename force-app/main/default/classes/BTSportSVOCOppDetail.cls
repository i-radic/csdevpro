public with sharing class BTSportSVOCOppDetail {
    
    public Opportunity Opp{get;set;}
    public Lead LeadRec{get;set;}
    public BTSportSVOCOppDetail(ApexPages.StandardController controller) {
         Opp = new Opportunity ();
         List<Lead> LeadList = new List<Lead>();
         LeadRec = new Lead();
         LeadList = [select Id,Type_of_Venue__c,preSales_Status__c,preSales_User__c,preSales_Notes__c,preSales_User_Update__c,Other_venue__c,
                     Venue_detail__c,Existing_Customer__c,Comment__c,Date_of_Submission__c,Number_of_business_premises__c,Are_you_a_Sky_customer__c,BT_Account_Number__c,Call_Back__c,
                     How_likely_is_DM_to_subscribe_c__c ,How_They_Classify_Venue__c,BTSport_Genesis_how_many_tvs__c,Prospect_Type__c,Territory__c,BT_Sport_Price_Sent__c,
                     RV_Description__c,Premises_Category__c,BTSport_Genesis_Rateable_Value__c,Price__c,Banding__c, 
                     ConvertedOpportunityId from Lead Where ConvertedOpportunityId =: ApexPages.currentPage().getparameters().get('id') LIMIT 1];
         if(LeadList.size()>0){
             LeadRec= LeadList[0];            
                          
         }
           
    }
    
}