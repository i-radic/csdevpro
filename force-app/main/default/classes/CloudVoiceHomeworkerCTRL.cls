/*######################################################################################
PURPOSE
Controller for the cut down version of the journey for the homworker RT.

HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
public with sharing class CloudVoiceHomeworkerCTRL {

    ApexPages.StandardController Gcontroller ;

    public Integer licenses {get; set;}  

    public Boolean dirEntry {get; set;}  

    public String cvId = System.currentPageReference().getParameters().get('CV');
    public String licenseType {get; set;}    
    public String accessType {get; set;} 
    public String bundleSTD {get; set;} 
    public String phone {get; set;}  
    public String objID {get; set;}
    public String custName {get; set;}
    public String uName {get; set;}
    public String uAddress {get; set;}
    public String u999Contact {get; set;}
    public String u999Phone {get; set;}
    public String u999Email {get; set;}
    public String locationCode {get; set;}
    public String productsToAddCSV {get; set;}
    public String licenseTypeTxt {get; set;}
    public String errMessage {get; set;}
    public String portNumbers {get; set;}

    public String custCRD {get; set;}

    public Cloud_Voice__c cvRec;
    public Cloud_Voice_Site__c cvsRec;

    private List<SelectOption> selOptions; 


    public CloudVoiceHomeworkerCTRL(ApexPages.StandardController controller) { 
        Gcontroller = controller;
        if(!Test.isRunningTest()){
        } 
        objID = System.currentPageReference().getParameters().get('CV');
        custName = System.currentPageReference().getParameters().get('cName');
        cvRec = [Select Id, Contract_Term__c FROM Cloud_Voice__c WHERE Id = :cvId LIMIT 1];
        this.cvsRec= (Cloud_Voice_Site__c)controller.getRecord();
    }   

    public List<SelectOption> getLicenseTypes() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('', 'Select'));
      for (CSS_Products__c rt : [Select Id, Grouping__c, Term__c,Unit_Cost__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'License' AND Grouping__c <> 'Basic' AND Term__c = :cvRec.Contract_Term__c AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200]) {
            selOptions.add(new SelectOption(rt.Id, rt.Grouping__c));
      }
      return selOptions;
    }
    public List<CSS_Products__c> getLicenseOptionsCon() {
        List<CSS_Products__c> CSSprodListAll = [Select Id, Name, Product_Name__c, Description__c, ShopLink__c, Unit_Cost__c, Connection_Charge__c, Required_Product__r.Name, Grouping__c, Discountable__c,Term__c,Line_Required__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'License Options' AND Grouping__c Like '%Connect%' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200];
        return CSSprodListAll;
    }
    public List<CSS_Products__c> getLicenseOptionsCol() {
        List<CSS_Products__c> CSSprodListAll = [Select Id, Name, Product_Name__c, Description__c, ShopLink__c, Unit_Cost__c, Connection_Charge__c, Required_Product__r.Name, Grouping__c, Discountable__c,Term__c,Line_Required__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'License Options' AND Grouping__c Like '%Collaborate%' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200];
        return CSSprodListAll;
    }    
    public List<CSS_Products__c> getAllCPE() {
        List<CSS_Products__c> CSSprodList = [Select Id, Product_Name__c, Description__c, ShopLink__c, Unit_Cost__c, Discountable__c,Connection_Charge__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'CPE' AND ShopLink__c = Null AND Grouping__c = 'Phone' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 200];
        return CSSprodList ;
    }
    public List<CSS_Products__c> getAllCPEPics() {
        List<CSS_Products__c> CSSprodList = [Select Id, Product_Name__c, Description__c, ShopLink__c, Unit_Cost__c, Discountable__c,Connection_Charge__c FROM CSS_Products__c WHERE Active__c = True AND Type__c = 'CPE' AND ShopLink__c <> Null AND Grouping__c = 'Phone' AND Provider__c = 'Cloud Voice'  AND Required_Product__c = '' ORDER BY Sort_Order__c ASC LIMIT 200];
        return CSSprodList ;
    }  
    public List<Number_Port_Reference__c> getSTDs() {
        List<Number_Port_Reference__c> rlist = [Select Id, Type__c,Reference__c,Description__c,Area__c FROM Number_Port_Reference__c WHERE Type__c = 'STD' ORDER BY Reference__c LIMIT 1000];
        return rlist ;
    }
    public List<SelectOption> getAccessTypes() {  
        selOptions= new List<SelectOption>();    
        selOptions.add(new SelectOption('Select','Select'));
        selOptions.add(new SelectOption('Existing BT Business Broadband','Existing BT Business Broadband'));
        selOptions.add(new SelectOption('Existing BT Business Fibre','Existing BT Business Fibre'));
        selOptions.add(new SelectOption('Existing BT Broadband (Res)','Existing BT Broadband (Res)'));
        selOptions.add(new SelectOption('Existing BT Fibre (Res)','Existing BT Fibre (Res)'));
        selOptions.add(new SelectOption('New BT Business Broadband','New BT Business Broadband'));
        selOptions.add(new SelectOption('New BT Business Fibre','New BT Business Fibre'));
        selOptions.add(new SelectOption('New BT Broadband (Res)','New BT Broadband (Res)'));
        selOptions.add(new SelectOption('New BT Fibre (Res) ','New BT Fibre (Res) '));
        selOptions.add(new SelectOption('Non-BT Broadband','Non-BT Broadband'));
        selOptions.add(new SelectOption('Non-BT Fibre','Non-BT Fibre'));
        return selOptions;
    } 

    public Cloud_Voice_Site__c newSite {
        get {
        if (newSite == null)
            newSite = new Cloud_Voice_Site__c();
            return newSite ;
        }
        set;
    }    
    public Cloud_Voice_Site_Product__c newProd {
        get {
        if (newProd == null)
            newProd = new Cloud_Voice_Site_Product__c();
            return newProd ;
        }
        set;
    } 

    public PageReference addHomeworker() { 
        errMessage = '';
        system.debug('JMM type: ' + licenseType);
            
        if(!Test.isRunningTest()){
            if(uName == ''){
              errMessage = '<br/>&nbsp;&nbsp; - Enter the name';
              cvsRec.addError('Enter the name.');
            }
            if(uAddress == ''){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Enter the address';
              cvsRec.addError('Enter the address.');
            } 
            if(u999Contact == ''){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Enter the 999 contact name';
              cvsRec.addError('Enter the 999 contact nam.');
            } 
            if(u999Phone == ''){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Enter the 999 phone number';
              cvsRec.addError('Enter the 999 phone number.');
            } 
            if(u999Email == ''){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Enter the 999 email address';
              cvsRec.addError('Enter the 999 email address.');
            }  
            if(locationCode == ''){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Enter the location code';
              cvsRec.addError('Enter the location code.');
            }  
            if(custCRD == Null || custCRD == ''){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Enter the required by date';
              cvsRec.addError('Enter the required by date.');
            }
            if(accessType == 'Select'){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select the Access';
              cvsRec.addError('Select the Access.');
            }     
            if(licenseType == Null){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select the license type';
              cvsRec.addError('Select the license type.');
            }     
            if(phone == ''){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select the phone';
              cvsRec.addError('Select the phone.');
            }   
            if(bundleSTD == '' && portNumbers == 'false'){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select the telephone number area, or select to port numbers';
              cvsRec.addError('Select the telephone number area, or select to port numbers.');
            }            
        }


        //set to stop opp trigger running
        StaticVariables.setCloudVoiceDontRun(true);
        //stop rollup summaries
        //HelperRollUpSummary.setCVSiteStop(true);

        List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();

        //add homeworkersite
        String[] myDateOnly = custCRD.split(' ');
        String[] strDate = myDateOnly[0].split('/');
        Integer myIntDate = integer.valueOf(strDate[0]);
        Integer myIntMonth = integer.valueOf(strDate[1]);
        Integer myIntYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
        

        newSite.CV__c = cvId; 
        newSite.RecordTypeId = '01220000000ArN3';
        newSite.Site_name__c = 'HW: ' + uName;
        newSite.Location_Code__c = locationCode;
        newSite.Site_Address__c = uAddress;
        newSite.Delivery_address__c = uAddress;
        newSite.X999_Contact__c = u999Contact;
        newSite.X999_Contact_Phone__c = u999Phone;
        newSite.X999_Contact_Email__c = u999Email;
        newSite.Customer_Required_Date__c = d;
        newSite.Access_product_decision__c = accessType;
        try {
            insert newSite;
        } catch (System.DmlException e) {
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                errMessage = errMessage + '<br/>&nbsp;&nbsp; - Location code must be unique across all sites for this customer.';
                cvsRec.addError('Location code must be unique across all sites for this customer.');
            }
        }
        
        if(errMessage != ''){
            errMessage = '<font color="red"><strong>Please correct the following errors:</strong> ' + errMessage + '<br/>&nbsp;</font>';
            return null;
        }
        errMessage = Null;
        
        //insert product lines
        newProd.Cloud_Voice_Site__c = newSite.ID;
        newProd.Quantity__c = 1 ;

        //add license
        newProd = Null;
        newProd.Cloud_Voice_Site__c = newSite.ID;
        newProd.Quantity__c = 1 ;
        newProd.SFCSSProdId__c = licenseType;        
        prodList.Add(newProd); 
        system.debug('JMM license licenseType: '+ licenseType);
        //add license options
        List<String> csvList = productsToAddCSV.Split(',');
        system.debug('JMM csvList: '+ csvList);
        system.debug('JMM csvList size: '+ csvList.size() );
        for(String s : csvList){     
            newProd = Null;
            newProd.Cloud_Voice_Site__c = newSite.ID; 
            newProd.Quantity__c = 1;
            newProd.SFCSSProdId__c = s;
            newProd.Number_Info__c = licenseTypeTxt;
            if(s != '' && s != Null){
               prodList.Add(newProd);
            }
        }
        //add phone
        newProd = Null;
        newProd.Cloud_Voice_Site__c = newSite.ID;
        newProd.Quantity__c = 1 ;
        newProd.SFCSSProdId__c = phone;      
        prodList.Add(newProd); 
        //add PSU
        newProd = Null;
        newProd.Cloud_Voice_Site__c = newSite.ID;
        newProd.Quantity__c = 1 ;
        CSS_Products__c cpe = [SELECT ID, A_Code__c, Product_Name__c, Power__c FROM CSS_Products__c WHERE Id = :phone];

        String phoneName = cpe.Product_Name__c;
        Integer vvx = cpe.Product_Name__c.indexOf('VVX');
        Integer yea = cpe.Product_Name__c.indexOf('T4');

        if(vvx > 0){
            phoneName = phoneName.substring(vvx+3,vvx+6);
        }else if(yea > 0){ 
            phoneName = phoneName.substring(yea,yea+3);

        }
        String pName = '%' + phoneName + '%'; 
        if(cpe.A_Code__c != 'CV0000' && cpe.Power__c < 0){
            CSS_Products__c cpePSU1 = [SELECT ID, Name FROM CSS_Products__c WHERE Product_Name__c Like :pName AND End__c = 1 AND Type__c = 'CPE' AND Grouping__c = 'PSU' AND Provider__c = 'Cloud Voice'];
            newProd.SFCSSProdId__c = cpePSU1.Id;   
            prodList.Add(newProd); 
        }
        //add delivery
        newProd = Null;
        newProd.Cloud_Voice_Site__c = newSite.ID;
        newProd.Quantity__c = 1 ;
        CSS_Products__c cpeDel = [SELECT ID, Name FROM CSS_Products__c WHERE Start__c = 1 AND Type__c = 'CPE' AND Grouping__c = 'Delivery' AND Provider__c = 'Cloud Voice'];
        newProd.SFCSSProdId__c = cpeDel.Id;
        prodList.Add(newProd);
        
        //insert order
        system.debug('JMM final prodList: '+ prodList);
        insert prodList;

        //add numbers
            String lineType = 'New: ' + bundleSTD;
            if(bundleSTD ==''){
                lineType = 'Porting Numbers';
            }
            if(dirEntry == True){
                lineType = lineType + ' **Directory Entry Required**';
            }
            CSS_Products__c line= [SELECT ID, End__c FROM CSS_Products__c WHERE Type__c = 'Line' AND Grouping__c = '' AND Provider__c = 'Cloud Voice' ORDER BY Sort_Order__c ASC LIMIT 1];
            Cloud_Voice_Site__c numReqd= [SELECT ID, zTotalNumbersRequired__c FROM Cloud_Voice_Site__c WHERE ID = :newSite.ID LIMIT 1];
            newProd = Null;
            newProd.Number_Info__c =  lineType;
            newProd.Locked_Edit__c = True;
            newProd.Cloud_Voice_Site__c = newSite.ID;
            newProd.Quantity__c = numReqd.zTotalNumbersRequired__c;
            newProd.SFCSSProdId__c = line.Id;   
            HelperRollUpSummary.setCVSiteStop(false);
            insert newProd;

        //HelperRollUpSummary.setCVSiteStop(false);
        //StaticVariables.setCloudVoiceDontRun(false);

        PageReference retPg = new PageReference('/' + newSite.Id );
        return retpg.setRedirect(true); 

    }   
    public PageReference Cancel() { 
        PageReference retPg = new PageReference('/' + cvId );
        return retpg.setRedirect(true);         
    }   
}