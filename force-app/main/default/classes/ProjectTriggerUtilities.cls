public class ProjectTriggerUtilities {

    /**
     * Updates the Stage record owner when the project record owner has
     * changed 
     *
     * @param oldMap Map of projects before the update indexed by their id
     * @param newmap Map of project after the update indexed by their id
     */
    public static void UpdateStageOwner(Map<Id, Project__c> oldMap, Map<Id, Project__c> newMap){
        // Create a map of stages for each project      
        List<Phase__c> stages = [SELECT Id,
                Project__c
            FROM Phase__c
            WHERE Project__c IN :newMap.keySet()];
        
        Map<Id, Phase__c[]> projectStages = new Map<Id, Phase__c[]>();
        for(Phase__c aStage : stages){
            if(projectStages.containsKey(aStage.Project__c)){ //Project already added to map
                projectStages.get(aStage.Project__c).add(aStage);
            } else { //Add new project map
                projectStages.put(aStage.Project__c, new Phase__c[]{aStage});
            }
        }
        
        // For each project if project manager has changed then update record owner on each stage for that project
        List<Phase__c> stagesForUpdate = new List<Phase__c>();
        for(Project__c aProject : newMap.values()){         
            if(aProject.Project_Manager__c != oldMap.get(aProject.Id).Project_Manager__c){ //Project manager has changed
                //Update stages with new owner
                if(projectStages.containsKey(aProject.Id)){ //Test to see if project has stages
                    for(Phase__c aStage : projectStages.get(aProject.Id)){
                        if(aProject.Project_Manager__c != null){ //Test to see if PM is null
                            aStage.Phase_Owner__c = aProject.Project_Manager__c;
                            stagesForUpdate.add(aStage);
                        }               
                    }
                }
            }
        }
        
        //Update
        update stagesForUpdate;
    }
        
    public static CPM_Documentation_Settings__c getWorkspaceIds() {
        // Retrieve the custom setting and get the Id of the workspace
        //TODO: Test for no records
        CPM_Documentation_Settings__c documentSettings = [SELECT Document_Template_Workspace_Id__c, Document_Workspace_Id__c FROM CPM_Documentation_Settings__c][0];
        return documentSettings;
        
    }
    
    public static ContentWorkspace getContentWorkspace(Id workspaceId) {
    	List<ContentWorkspace> workspaces = [SELECT Id 
    										FROM ContentWorkspace 
    										WHERE Id = :workspaceId];
    										
    	if (workspaces.size() == 0) {
    		throw new ProjectTriggerUtilityException('Cannot find workspace with ID ' + workspaceId + '. Please ensure the workspace exists and that the running user has access to it.');
    	}
    	
    	return workspaces[0];
    }
    
    public static void createProjectTemplates(List<Project__c> projects) {
        // Get the Workspace
        CPM_Documentation_Settings__c settings = ProjectTriggerUtilities.getWorkspaceIds();
        
        ContentWorkspace templatesWorkspace = getContentWorkspace(settings.Document_Template_Workspace_Id__c);
        ContentWorkspace projectWorkspace = getContentWorkspace(settings.Document_Workspace_Id__c);
        
        // Get the list of document templates and their current published version Ids
        List<ContentWorkspaceDoc> templates = [SELECT ContentDocumentId FROM ContentWorkspaceDoc WHERE ContentWorkspaceId = :templatesWorkspace.Id];
        List<String> templateIds = new List<String>();
        for (ContentWorkspaceDoc doc : templates) {
            templateIds.add(doc.ContentDocumentId);
        }
        List<ContentDocument> contentDocuments = [SELECT LatestPublishedVersionId FROM ContentDocument WHERE Id in :templateIds];
        
        List<String> latestVersionTemplateIds = new List<String>();
        for (ContentDocument cs : contentDocuments) {
            latestVersionTemplateIds.add(cs.LatestPublishedVersionId);
        }
        
        
        if (latestVersionTemplateIds.size() > 0) {
            //loop through all the templates and pull back the latest published version of each of them
            List<ContentVersion> latestVersionOfTemplate = [SELECT c.VersionData, c.Project__c, c.Title, c.PublishStatus, 
                                                                    c.PositiveRatingCount, c.PathOnClient, c.Origin, c.NegativeRatingCount, c.Id, c.FirstPublishLocationId, 
                                                                   c.FileType, c.FeaturedContentDate, c.FeaturedContentBoost, c.Description, c.ContentDocumentId
                                                            FROM ContentVersion c
                                                            WHERE ID in :latestVersionTemplateIds];
                                                            
            List<ContentVersion> allNewDocuments = new List<ContentVersion>();
            
            for (Project__c p : projects) {                                                     
                //create the new documents from the templates
                //corrected project__c in line 88 to Project__c
                List<ContentVersion> newProjectDocuments = latestVersionOfTemplate.deepClone();
                for (ContentVersion cv : newProjectDocuments) {
                    cv.FirstPublishLocationId = settings.Document_Workspace_Id__c;
                    cv.Project__c = p.Id;
                    cv.ContentDocumentId = null;
                    cv.Title = p.Project_Name__c + '_' + cv.Title;
                    cv.Description = '';
                }
                allNewDocuments.addAll(newProjectDocuments);
            }
            
            try {
            	insert allNewDocuments;
            }
            catch (System.DmlException ex) {
            	
            	try {
	            	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	            	
	            	String[] toAddresses = new String[] {'pbanerjee@astadia.com', 'dchoate@astadia.com'};
	            	
	            	mail.setToAddresses(toAddresses);
					
					mail.setReplyTo('dchoate@astadia.com');
					
					mail.setSenderDisplayName('Derek Choate - Astadia');
					
					mail.setSubject('Code fault');
					
					mail.setPlainTextBody(ex.getStackTraceString());
					
					Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            	}
            	catch (System.Exception ex2) {
            		System.debug('===---=== Email failed ===---===');
            		System.debug(ex2);
            	}
            	
            	System.debug('===---=== Document Creation Failed ===---===');
            	System.debug(ex.getStackTraceString());
            	throw ex;
            }
            
            
        } else {
            // Do nothing as there are no templates...
        }
    }
         
}