global class BatchUpdatePriorConsentHAAS implements 
    Database.Batchable<sObject>  {
    private String query;
	
	global BatchUpdatePriorConsentHAAS(String q){
		this.query = q;
	}
        global Database.QueryLocator start(Database.BatchableContext bc) {
            system.debug('Query**********'+Query);
        return Database.getQueryLocator(Query);
    }
        
        global void execute(Database.BatchableContext bc, List<Contact> scope){
            
            for(Contact Con :Scope){
                if(Con.EmailPriorConsent__c!=Null && Con.Email_Consent__c!=Null){
                    if(Con.EmailPriorConsent__c!=Con.Email_Consent__c){
                    if(Con.EmailPriorConsentDate__c >  Con.Email_Consent_Date__c){
                        Con.Email_Consent__c = Con.EmailPriorConsent__c;
                        Con.Email_Consent_Date__c = Con.EmailPriorConsentDate__c;
                    }
                    }
                } if(Con.VoicePriorConsent__c!=Null && Con.Phone_Consent__c!=Null){
                    if(Con.VoicePriorConsent__c!=Con.Phone_Consent__c){
                    if(Con.VoicePriorConsentDate__c >  Con.Phone_Consent_Date__c){
                        Con.Phone_Consent__c = Con.VoicePriorConsent__c;
                        Con.Phone_Consent_Date__c = Con.VoicePriorConsentDate__c;
                    }
                    }
                } if(Con.SMSPriorConsent__c!=Null && Con.Mobile_Consent__c!=Null){
                    if(Con.SMSPriorConsent__c!=Con.Mobile_Consent__c){
                    if(Con.SMSPriorConsentDate__c >  Con.Mobile_Consent_Date__c){
                        Con.Mobile_Consent__c = Con.SMSPriorConsent__c; 
                        Con.Mobile_Consent_Date__c = Con.SMSPriorConsentDate__c;    
                    }
                    }
                } if(Con.PostalPriorConsent__c!=Null && Con.Address_Consent__c!=Null){
                    if(Con.PostalPriorConsent__c!= Con.Address_Consent__c){
                    if(Con.PostalPriorConsentDate__c >  Con.Address_Consent_Date__c){
                        Con.Address_Consent__c = Con.PostalPriorConsent__c;
                        Con.Address_Consent_Date__c = Con.PostalPriorConsentDate__c;
                    }    
                } 
                }
                
                if(Con.EmailPriorConsent__c!=Null && Con.Email_Consent__c==Null){
                    Con.Email_Consent__c = Con.EmailPriorConsent__c;
                    Con.Email_Consent_Date__c = Con.EmailPriorConsentDate__c;    
                    }
                if(Con.VoicePriorConsent__c!=Null && Con.Phone_Consent__c==Null){
                    Con.Phone_Consent__c = Con.VoicePriorConsent__c;
                    Con.Phone_Consent_Date__c = Con.VoicePriorConsentDate__c;
                }
                if(Con.SMSPriorConsent__c!=Null && Con.Mobile_Consent__c==Null){
                    Con.Mobile_Consent__c = Con.SMSPriorConsent__c; 
                    Con.Mobile_Consent_Date__c = Con.SMSPriorConsentDate__c;
                }
                if(Con.PostalPriorConsent__c!=Null && Con.Address_Consent__c==Null){
                    Con.Address_Consent__c = Con.PostalPriorConsent__c;
                    Con.Address_Consent_Date__c = Con.PostalPriorConsentDate__c;
                }
                //Scope.add(con);
               if (con.DL_Contact_Sector__c != 'Corporate' && con.DL_Contact_Sector__c != 'Major and Public Sector' && con.DL_Contact_Sector__c != 'ROI Business' &&
             con.DL_Contact_Sector__c != 'ROI Wholesale' && con.DL_Contact_Sector__c != 'NI Business' && con.DL_Contact_Sector__c != 'NI DBAM' ) {
            if (con.Email != 'DoNotContact@bt.co.uk' && con.Email_Consent__c == 'TPS' || con.Email_Consent__c == 'No') {
                con.emailHidden__c = con.Email;
                con.Email = 'DoNotContact@bt.co.uk';
            }if (con.Email_2nd__c != 'DoNotContact@bt.co.uk' && con.Email_Consent__c == 'TPS' || con.Email_Consent__c == 'No') {
                con.emailHidden_2nd__c = con.Email_2nd__c;
                con.Email_2nd__c = 'DoNotContact@bt.co.uk';
            }if (con.Email_3rd__c != 'DoNotContact@bt.co.uk' && con.Email_Consent__c == 'TPS' || con.Email_Consent__c == 'No') {
                con.emailHidden_3rd__c = con.Email_3rd__c;
                con.Email_3rd__c = 'DoNotContact@bt.co.uk';
            } 
        }
                if(Con.PriorStatus__c !=Null && Con.Status__c !=Null){
                	if(Con.PriorStatus__c != Con.Status__c ){	
                            Con.Status__c = Con.PriorStatus__c;
                    }
                }if(Con.PriorStatus__c !=Null && Con.Status__c == Null){
                	Con.Status__c = Con.PriorStatus__c;    
                }
		              
            }
            
            Database.Update(Scope,false);
            system.debug('*****'+Scope.size());
        }
        
        global void finish(Database.BatchableContext bc){}

}