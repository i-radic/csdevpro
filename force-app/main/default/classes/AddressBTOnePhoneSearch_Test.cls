@isTest
private class AddressBTOnePhoneSearch_Test {

    static testMethod void runPositiveTestCases() {
        user thisUser = [select id from user where id=:userinfo.getuserid()];
        system.runAs(thisUser){
        Test_Factory.setProperty('IsTest', 'yes');
		Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
       	User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
        System.runAs( u1 ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
    	}
        insert Test_Factory.CreateAccountForDummy();
        Account account = Test_Factory.CreateAccount();
        account.OwnerId = u1.Id;
        account.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(account);

        Opportunity opp = Test_Factory.CreateOpportunity(accountResult.getid());
        opp.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(opp);

        BT_One_Phone__c bop = new BT_One_Phone__c();
		bop.Opportunity__c = opp.Id;
        bop.OnePhone_Product_Type__c = 'BT One Phone Office';
		bop.Contract_Value__c = 1200;
        bop.Existing_Customer_Resign__c  = 'Y';
		insert bop;

		BTSalesforceServicesURL__c btsurl = new BTSalesforceServicesURL__c();
		btsurl.Name = 'CRMServices';
		btsurl.Url__c = 'https://eric-gotham-salesforceservices.bt.com/SF.CRMGateway/CRMService.asmx';
		insert btsurl;

        PageReference PageRef = Page.addressBTOnePhoneResults;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id',bop.Id);
        ApexPages.currentPage().getParameters().put('Pc','w13 8qd');

        ApexPages.StandardController ctrl = new ApexPages.StandardController(bop);
        AddressBTOnePhoneSearch abops = new AddressBTOnePhoneSearch(ctrl);
        abops.getTest();
        abops.CreateAddress();
        abops.Back();
        abops.ReDirect();
        abops.getAddresses();
        abops.getSelected();
        abops.getselectedAddress();
        
        BT_One_Phone__c bop2 = new BT_One_Phone__c();
		bop2.Opportunity__c = opp.Id;
        bop2.OnePhone_Product_Type__c = 'BT One Phone Office';
		bop2.Contract_Value__c = 1200;
        abops.getAddresses();
        
        
        
        Test_Factory.setProperty('IsTest', 'no');
        abops.CreateAddress();
        abops.Back();
        abops.ReDirect();
        abops.getSelected();
        
        AddressBTOnePhoneSearch abops2 = new AddressBTOnePhoneSearch(bop);
    }
    }
}