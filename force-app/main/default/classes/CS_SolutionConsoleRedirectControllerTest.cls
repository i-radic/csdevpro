@IsTest
public class CS_SolutionConsoleRedirectControllerTest {
    
    
   public testMethod static void SolutionConsoleRedirectControllerTest(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        PageReference pageRef = new PageReference('/cssmgnt__SCEditor');
        test.setCurrentPageReference(pageRef);
        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BTnet');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BTnet';
       
        pc.Volume__c = 10;
        INSERT pc;
        pageRef.getParameters().put('basketId',basket.id);      
            
        //insert setting2;
        CS_SolutionConsoleRedirectController redirectToSolutionConsoleObj= new CS_SolutionConsoleRedirectController();
        redirectToSolutionConsoleObj.redirectToSolutionConsole();           
    } 
    
}