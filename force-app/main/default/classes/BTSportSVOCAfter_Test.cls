@isTest

private class BTSportSVOCAfter_Test {

    static testMethod void runPositiveTestCases1() {
     
      Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		     
        User thisUser = [select id from User where id=:userinfo.getUserid()];
	  System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
             
      User U1 = new User();
      u1.Username = 'jaovohoauh124@bt.com';
      u1.Ein__c = 'test12345';
      u1.LastName = 'TestLastname';
      u1.FirstName = 'TestFirstname';
      u1.MobilePhone = '07918672032';
      u1.Phone = '02085878834';
      u1.Title='What i do';
      u1.OUC__c = 'DKW';
      u1.Manager_EIN__c = 'test12346';
      u1.Email = 'no.reply@bt.com';
      u1.Alias = 'boatid01';
      u1.TimeZoneSidKey = 'Europe/London';
      u1.LocaleSidKey = 'en_US';
      u1.emailencodingkey = 'UTF-8';
      u1.ProfileId = prof.Id;
      u1.LanguageLocaleKey = 'en_US';
   
      insert U1;    
      }   
      //create test BTLB    
      BTLB_Master__c testBTLB = new BTLB_Master__c( name = 'TEST',  Account_Owner__c = UserInfo.getUserId(), BTLB_Name_ExtLink__c ='TEST');
      Database.SaveResult[] BTLBResult = Database.insert(new BTLB_Master__c[] {testBTLB});     
    
       
      // create dummy Market sacs for test BTLB
      BTLB_Market_SAC__c testMktSac = new BTLB_Market_SAC__c();
      testMktSac.BTLB_Name__c = BTLBResult[0].id;
      testMktSac.SAC_Code__c = 'aSac999';
      testMktSac.ExtId__c = '999';
      insert testMktSac;
   
      // create dummy LOB etc for test BTLB      
      BTLB_CCAT__c testLOB = new BTLB_CCAT__c();
      testLOB.BTLB_Name__c = BTLBResult[0].id;
      testLOB.LOB_Code__c = 'lob999';
      testLOB.LOB_Name__c = 'BTLBTest';
      testLOB.Sub_Sector_Code__c = 'ssc999';
      testLOB.Sub_Sector__c ='ssTest';
      testLOB.ExtId__c = '999';
      insert testLOB;  
  
      // a LE Level Account to link to  
      Account acc = Test_Factory.CreateAccount(); 
      acc.Sector__c = 'BT Local Business';
      acc.LOB_Code__c = 'lob999';
      acc.SAC_Code__c = 'bSac999';
      acc.LE_Code__c = 'TEST3';
      acc.AM_EIN__c = '802537216';
      acc.Sub_Sector__c = 'ssTest';
      acc.CUG__c = '1234567';
      insert acc;
       
      Contact con = Test_Factory.CreateContact();
      con.AccountCUG__c =  acc.Id;
      con.AccountId = acc.Id;
      con.SAC_Code__c = acc.SAC_Code__c;
      insert con;     
      
        List<BT_Sport_SVOC__c> svocList = new List<BT_Sport_SVOC__c>();
        svocList.add(new BT_Sport_SVOC__c(Contact_Name__c = 'TestContactName',Business_Name__c = 'TestBusinessName',Prospect_Postcode__c = 'MH3 1GH',
                                          CUG_ID__c = '1234567',BTS_Single_View_UID__c = 'BTSCOM_UID_121212_012345')); 
         Test.startTest(); 
        insert svocList;
        
        BT_Sport_SVOC__c svocUpdate = [Select Id,Lead__c from BT_Sport_SVOC__c WHERE Id =: svocList[0].Id ];
        Lead svocLead = [Select Id,Status,Account__c,Contact__c from Lead where ID =:svocUpdate.Lead__c];
        svocLead.Status = 'Opportunity Identified';
        svocLead.Contact__c = con.Id;
        svocLead.Account__c = acc.Id;
        Update svocLead;
        test.stopTest();
         
    }
    
    static testMethod void runPositiveTestCases2() {
     
      Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
      User thisUser = [select id from User where id=:userinfo.getUserid()];
	  System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;     
      User U1 = new User();
      u1.Username = 'jaovohoauh124@bt.com';
      u1.Ein__c = 'test12345';
      u1.LastName = 'TestLastname';
      u1.FirstName = 'TestFirstname';
      u1.MobilePhone = '07918672032';
      u1.Phone = '02085878834';
      u1.Title='What i do';
      u1.OUC__c = 'DKW';
      u1.Manager_EIN__c = 'test12346';
      u1.Email = 'no.reply@bt.com';
      u1.Alias = 'boatid01';
      u1.TimeZoneSidKey = 'Europe/London';
      u1.LocaleSidKey = 'en_US';
      u1.emailencodingkey = 'UTF-8';
      u1.ProfileId = prof.Id;
      u1.LanguageLocaleKey = 'en_US';
         
      insert U1;    
      }    
      //create test BTLB    
      BTLB_Master__c testBTLB = new BTLB_Master__c( name = 'TEST',  Account_Owner__c = UserInfo.getUserId(), BTLB_Name_ExtLink__c ='TEST');
      Database.SaveResult[] BTLBResult = Database.insert(new BTLB_Master__c[] {testBTLB});     
    
       
      // create dummy Market sacs for test BTLB
      BTLB_Market_SAC__c testMktSac = new BTLB_Market_SAC__c();
      testMktSac.BTLB_Name__c = BTLBResult[0].id;
      testMktSac.SAC_Code__c = 'aSac999';
      testMktSac.ExtId__c = '999';
      insert testMktSac;
   
      // create dummy LOB etc for test BTLB      
      BTLB_CCAT__c testLOB = new BTLB_CCAT__c();
      testLOB.BTLB_Name__c = BTLBResult[0].id;
      testLOB.LOB_Code__c = 'lob999';
      testLOB.LOB_Name__c = 'BTLBTest';
      testLOB.Sub_Sector_Code__c = 'ssc999';
      testLOB.Sub_Sector__c ='ssTest';
      testLOB.ExtId__c = '999';
      insert testLOB;  
  
      // a LE Level Account to link to  
      Account acc = Test_Factory.CreateAccount(); 
      acc.Sector__c = 'BT Local Business';
      acc.LOB_Code__c = 'lob999';
      acc.SAC_Code__c = 'bSac999';
      acc.LE_Code__c = 'TEST3';
      acc.AM_EIN__c = '802537216';
      acc.Sub_Sector__c = 'ssTest';
      acc.CUG__c = '1234567';
      insert acc;
       
      Contact con = Test_Factory.CreateContact();
      con.AccountCUG__c =  acc.Id;
      con.AccountId = acc.Id;
      con.SAC_Code__c = acc.SAC_Code__c;
      insert con;     
      
        List<BT_Sport_SVOC__c> svocList = new List<BT_Sport_SVOC__c>();
        svocList.add(new BT_Sport_SVOC__c(Contact_Name__c = 'TestContactName',Business_Name__c = 'TestBusinessName',Prospect_Postcode__c = 'MH3 1GH',
                                          Legacy_Customer_Group_Key__c = '1234567',BTS_Single_View_UID__c = 'BTSCOM_UID_121212_012345'  ));
         Test.startTest();
        insert svocList; 
        
        
        svocList[0].Account__c = null;
        update svocList[0];
        
        Test.stopTest(); 
    }        
}