@isTest
private class CS_CustomButtonBTHoldingReportTest { 
    @isTest
    private static void test4() {
        crateTestData();

        Test.startTest();

        String pCFG = '[' + JSON.serialize(pcr.Id) + ']';
        
        String performAction = customBtnBTholdRep.performAction(String.valueOf(basket.Id), pCFG);

        Test.stopTest();
    }

	@IsTest
	private static void testPerformActionWithResign() {
		crateTestData();

		cscfga__Product_Configuration__c testPC = [SELECT Id, Name, Deal_Type__c FROM cscfga__Product_Configuration__c WHERE Id = :pcr.csbb__Product_Configuration__c];
		testPC.Deal_Type__c = 'Resign';
		UPDATE testPC;
		testPC = [SELECT Id, Name, Deal_Type__c, cscfga__Product_Definition__c, cscfga__Product_Definition__r.Custom_Resign_Process__c FROM cscfga__Product_Configuration__c WHERE Id = :pcr.csbb__Product_Configuration__c];

		pcr.csbb__Product_Configuration__c = testPC.Id;
		UPDATE pcr;
		pcr = [SELECT Id, Name, csbb__Product_Configuration__c, csbb__Product_Configuration__r.cscfga__Product_Definition__c, csbb__Product_Configuration__r.Deal_Type__c, csbb__Product_Configuration__r.cscfga__Product_Definition__r.Custom_Resign_Process__c FROM csbb__Product_Configuration_Request__c WHERE Id = :pcr.Id];

		Test.startTest();
        String pCFG = '[' + JSON.serialize(pcr.Id) + ']';
        String performAction = customBtnBTholdRep.performAction(String.valueOf(basket.Id), pCFG);
        Test.stopTest();

		System.assertEquals('Resign', pcr.csbb__Product_Configuration__r.Deal_Type__c);
		System.assertEquals(true, pcr.csbb__Product_Configuration__r.cscfga__Product_Definition__r.Custom_Resign_Process__c);
	}

    private static CustomButtonBTHoldingReport customBtnBTholdRep = null;
    private static cscfga__Product_Basket__c basket = null;
    private static cscfga__Product_Configuration__c prodConfig = null;
    private static csbb__Product_Configuration_Request__c pcr = null;

    private static void crateTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        customBtnBTholdRep = new CustomButtonBTHoldingReport();
        Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        basket =  CS_TestDataFactory.generateProductBasket(true, 'test basket', testOpp);
        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinitionResign(true, 'BT Mobile', true);
        prodConfig = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);
		prodConfig.cscfga__Product_Definition__c = prodDef.Id;
		INSERT prodConfig;
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'BT Mobile');
        cscfga__Offer_Category_Association__c oca = CS_TestDataFactory.generateCategoryDefinitionAssoc(true, prodCategory, testOffer);
        pcr = CS_TestDataFactory.generateProductConfigRequest(true, prodCategory.Id, prodConfig.Id);
    }
}