global class BatchCreateWinRatioByValueKPI implements Database.Batchable<sObject>, Database.Stateful
{
    global map<date, decimal> dateMapWon    = new map<date, decimal>(); 
    global map<date, decimal> dateMapLost   = new map<date, decimal>();
    
    GlobalKPISettings__c gs = GlobalKPISettings__c.getInstance();
    GlobalKPI_Helper gh = new GlobalKPI_Helper();
    public string businessName  = gs.Business_Unit__c; 
    public string busCode       = gs.Business_Unit_Code__c; 
    public string theQuery;
    public string periodCode;
    public string periodType;
    public string periodObjectFieldType;
    public string thisPeriod; 
    global BatchCreateWinRatioByValueKPI (string period)
    {
        
        GlobalKPI_Helper gh = new GlobalKPI_Helper();
            
        thisPeriod = period;
        if (thisPeriod=='MONTH')
        {
            periodCode              ='MN';
            periodType              ='Month';
            periodObjectFieldType   ='Month';
        }
        else if (thisPeriod=='FISCAL_QUARTER')
        {
            periodCode              ='FQ';
            periodType              ='Fiscal Quarter';
            periodObjectFieldType   ='Quarter';
        }
        else if (thisPeriod=='FISCAL_YEAR')
        {
            periodCode              ='FY';
            periodType              ='Fiscal Year';
            periodObjectFieldType   ='Year';
        }
        
        //by month
        if (thisPeriod=='MONTH')
        {
            //can't do aggregate soql queries in multiple batches at time of writing, so use standard query and roll up
            theQuery = 'select closeDate, isWon, Amount from Opportunity where Account_Sub_Sector__c =  \'TIKIT\' and isClosed = true and (closeDate = LAST_N_MONTHS:5 or closeDate=THIS_MONTH) ';
            
            Date map_start_date = date.valueOf([Select startDate From Period Where type = :periodObjectFieldType and StartDate = THIS_MONTH].startDate ).addMonths(-5); //six months ago
            Date map_end_date   = [Select endDate From Period Where type = :periodObjectFieldType and StartDate = THIS_MONTH].endDate ;
            system.debug(map_start_date);
            system.debug(map_end_date);
            for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(1)  )
            {
                dateMapWon.put(d, 0);
                dateMapLost.put(d, 0);  
            }
            system.debug(dateMapWon);
            system.debug(dateMapLost);
        }
        else if(thisPeriod=='FISCAL_QUARTER')
        {
            //can't do aggregate soql queries in batches at time of writing, so use standard query and roll up
            theQuery = 'select closeDate, isWon, Amount from Opportunity where Account_Sub_Sector__c =  \'TIKIT\' and isClosed = true and (closeDate = LAST_N_FISCAL_QUARTERS:3 or closeDate=THIS_FISCAL_QUARTER) ';
            
            Date map_start_date = date.valueOf([Select startDate From Period Where type = :periodObjectFieldType and StartDate = THIS_FISCAL_QUARTER].startDate ).addMonths(-9); //3 quarters ago
            Date map_end_date   = [Select endDate From Period Where type = :periodObjectFieldType and StartDate = THIS_FISCAL_QUARTER].endDate ;
            system.debug(map_start_date);
            system.debug(map_end_date);
            for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(3)  )
            {
                dateMapWon.put(gh.getPeriodStartDate(periodType, d), 0);
                dateMapLost.put(gh.getPeriodStartDate(periodType, d), 0);   
            }
            system.debug(dateMapWon);
            system.debug(dateMapLost);
        }
        else if(thisPeriod=='FISCAL_YEAR')
        {
            //can't do aggregate soql queries in batches at time of writing, so use standard query and roll up
            theQuery = 'select closeDate, isWon, Amount from Opportunity where Account_Sub_Sector__c =  \'TIKIT\' and isClosed = true and (closeDate = LAST_FISCAL_YEAR Or closeDate=THIS_FISCAL_YEAR) ';
            
            Date map_start_date = date.valueOf([Select startDate From Period Where type = :periodObjectFieldType and StartDate = LAST_FISCAL_YEAR].startDate ); //3 quarters ago
            Date map_end_date   = [Select endDate From Period Where type = :periodObjectFieldType and StartDate = THIS_FISCAL_YEAR].endDate ;
            system.debug(map_start_date);
            system.debug(map_end_date);
            for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(12)  )
            {
                dateMapWon.put(gh.getPeriodStartDate(periodType, d), 0);
                dateMapLost.put(gh.getPeriodStartDate(periodType, d), 0);   
            }
            system.debug(dateMapWon);
            system.debug(dateMapLost);
        }
        else
        {
            return;
        }
        
        system.debug(theQuery);
        system.debug ('*****************************BatchCreateWinRatioByValueKPI'); 
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug ('*****************************BatchCreateWinRatioByValueKPI QueryLocator() method');
        system.debug(theQuery);     
        return Database.getQueryLocator(theQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.debug(dateMapWon);
        system.debug(dateMapLost);
        system.debug ('*****************************BatchCreateWinRatioByValueKPI Execute() method');   
        GlobalKPI_Helper gh = new GlobalKPI_Helper();
        decimal thisAmount;
        for(sObject s : scope) //loop through rows of scope list
        {
            Opportunity o = (Opportunity) s;
            system.debug(o.amount);
            if (o.isWon==true) 
            {
                if (o.Amount==null) {thisAmount=0;} else {thisAmount=o.Amount;}
                dateMapWon.put(gh.getPeriodStartDate(periodType, o.closeDate),   dateMapWon.get(gh.getPeriodStartDate(periodType, o.closeDate)) + thisAmount); //update won map value
            } 
            else 
            {   
                if (o.Amount==null) {thisAmount=0;} else {thisAmount=o.Amount;}
                dateMapLost.put(gh.getPeriodStartDate(periodType, o.closeDate),  dateMapLost.get(gh.getPeriodStartDate(periodType, o.closeDate)) + thisAmount); //update lost map value
                
            }
        }
        system.debug(dateMapWon);
        system.debug(dateMapLost);      
        system.debug(dateMapWon.size());
        system.debug(dateMapLost.size());
        
        
    }    
    
        
    global void finish(Database.BatchableContext BC)
    {  
        
        system.debug ('*****************************BatchCreateWinRatioByValueKPI Finish() method');        
        
        string dtFrom;
        string dtTo;
        
        
        decimal lostValue;
        decimal wonValue;
        
        for (date d : dateMapWon.keySet())
        {
            Global_KPI__c kpi           = new Global_KPI__c();
            kpi.business_unit__c        = businessName;
            kpi.Measure_Name__c         = 'Win Ratio by Value';
            kpi.Unit_of_Measure__c      = 'Percent';
            lostValue                   = dateMapLost.get(d).setScale(2);
            wonValue                    = dateMapWon.get(d).setScale(2);
            kpi.Date_From__c            = d;
            dtFrom                      = string.valueOf(d);
            system.debug('Select endDate From Period Where type = ' + periodObjectFieldType + ' and StartDate <= ' + d + ' and EndDate > ' + d);
            kpi.Date_To__c              = [Select endDate From Period Where type = :periodObjectFieldType and StartDate <= :d and endDate >= :d].endDate;
            dtTo                        = string.valueOf(kpi.Date_To__c) ;
            kpi.uniqueKey__c = busCode + ':' + 'WRV' + ':' + periodCode + ':' + dtFrom.replace('-','') + ':' + dtTo.replace('-','');  
            kpi.Period_Type__c          = periodType;
                        
            if ((wonValue+lostValue) == 0)  //to prevent divide by 0 errors
            {
                kpi.result__c = 0;
            }
            else
            {
                kpi.result__c = wonValue*100 / (wonValue+lostValue);
            }
            kpi.Numerator__c            = wonValue;
            kpi.Denominator__c          = lostValue+wonValue;
            //insert or update it using external ID field uniqueKey__c as the key
            system.debug(kpi);
            upsert kpi uniqueKey__c;
        }
            
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                         FROM AsyncApexJob WHERE Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.     
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BatchCreateWinRatioByValueKPI ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }


}