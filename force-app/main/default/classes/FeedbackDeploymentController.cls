public with sharing class FeedbackDeploymentController {
public List<FeedbackDeploymentController> searchResults {get;set;}
public string CheckStatus = null;

    public FeedbackDeploymentController(ApexPages.StandardController controller) {

    }

public string DeploymentID = System.currentPageReference().getParameters().get('id');
public string FeedbackID = [SELECT CR__c FROM Deployment_Actions__c WHERE Id = :DeploymentID].CR__c;
public integer NoteNum = 0;
      
/*      
public Attachment attachment {
  get {
      if (attachment == null)
      attachment = new Attachment();
      return attachment;
    }
  set;
  }
*/

public Note note {
  get {
      if (note == null)
        note = new Note();
      return note ;
    }
  set;
  }
/*
public List<Attachment> getAtt() {    
      return [SELECT ID, Name, Description, Body, BodyLength, CreatedByID, CreatedBy.Name, CreatedDate FROM Attachment WHERE ParentID = :DeploymentID ORDER BY CreatedDate DESC ];
  }
*/  

public List<Note> getDevNoteList() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :DeploymentID AND Title = 'Dev' ORDER BY CreatedDate];
  }
public List<Note> getPreNoteList() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :DeploymentID AND Title Like 'Pre%' ORDER BY Title];
  }
public List<Note> getPostNoteList() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :DeploymentID AND Title Like 'Post%' ORDER BY Title];
  }

//Details from the main SF record to populate the Release Note
public List<Attachment> getFBAttDev() {    
      return [SELECT ID, Name, Description, Body, BodyLength, CreatedByID, CreatedBy.Name, CreatedDate FROM Attachment WHERE ParentID = :FeedbackID  ORDER BY CreatedDate DESC ];
}
public List<Note> getFBNoteDev() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :FeedbackID ORDER BY CreatedDate DESC];
}

//attach the pdf to the deployment record
  public PageReference savePdf() {
  decimal DepVer = null;
  string DepName= null;
     List<Deployment_Actions__c> DeployInf = [SELECT Name, Version__c FROM Deployment_Actions__c WHERE Id = :DeploymentID LIMIT 1];
                    if (DeployInf.size() > 0) {
                            DepVer = DeployInf[0].Version__c;
                            DepName = DeployInf[0].Name;
                            }
                            
    string pdfName = DepName +' v' +DepVer + '.pdf';
 
    PageReference pdf = Page.FeedbackDeploymentPDF;
    // add parent id to the parameters for standardcontroller
    pdf.getParameters().put('id',DeploymentID );
    
    // create the new attachment
    Attachment attach = new Attachment();
 
    // the contents of the attachment from the pdf
    Blob body;
 
    try {
 
        // returns the output of the page as a PDF
        body = pdf.getContent();
 
    // need to pass unit test -- current bug    
    } catch (VisualforceException e) {
        body = Blob.valueOf('Some Text');
    }
 
    attach.Body = body;
    // add the user entered name
    attach.Name = pdfName;
    attach.IsPrivate = false;
    // attach the pdf to the account
    attach.ParentId = DeploymentID ;
    insert attach;
 
    // send the user to the account to view results
    return new PageReference('/'+DeploymentID );
 
  }


public PageReference addDevNote() {
    //NoteNum = [SELECT count() FROM Note WHERE ParentID = :DeploymentID AND Title Like 'Dev%'] + 1 ;
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = DeploymentID ; // the record the file is attached to
    note.Body = note.Body;
    note.Title= 'Dev' ;// + NoteNum;
 
    try {
      insert Note;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
}

public PageReference addPreNote() {
    NoteNum = [SELECT count() FROM Note WHERE ParentID = :DeploymentID AND Title Like 'Pre%'] + 1 ;
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = DeploymentID ; // the record the file is attached to
    note.Body = note.Body;
    note.Title= 'Pre' + NoteNum;
 
    try {
      insert Note;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
}

public PageReference addPostNote() {
    NoteNum = [SELECT count() FROM Note WHERE ParentID = :DeploymentID AND Title Like 'Post%'] + 1 ;
    note.OwnerId = UserInfo.getUserId();
    note.ParentId = DeploymentID ; // the record the file is attached to
    note.Body = note.Body;
    note.Title= 'Post' + NoteNum;
 
    try {
      insert Note;
    } catch (DMLException e) {
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    } finally {
      note= new Note(); 
    }
 
    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
}
 
}