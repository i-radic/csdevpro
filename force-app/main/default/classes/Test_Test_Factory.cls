@isTest
private class Test_Test_Factory {
    
    static testMethod void RunAllTests() {
        Test_Factory.CreateUser();
        Test_Factory.SetProperty('name', 'value');
        Test_Factory.GetProperty('name');
        
        Test_Factory.CreateAccount();
        
        Test_Factory.CreateAccount('001S000000AjR2g');
        Test_Factory.CreateContactwithAccount('001S000000AjR2g');
        Test_Factory.CreateLandscapeBTLB2('001S000000AjR2g');
        
        Test_Factory.CreateCustomerExperienceActionPlan('001S000000AjR2g');
        Test_Factory.CreateCSAT('001S000000AjR2g');
        Test_Factory.CreatecustomerExperience('001S000000AjR2g', '001S000000AjR2g', '001S000000AjR2g', '001S000000AjR2g');
        Test_Factory.CreateCSATContactFeedback('001S000000AjR2g');
        
        Test_Factory.CreateSCARS_BTLB('001S000000AjR2g');
        
        Test_Factory.CreateAccountForDummy();
        
        Test_Factory.CreateContact();
        
        Test_Factory.CreateAssetLineCount();
        
        Test_Factory.CreateAccountTeamMember();
        
        Test_Factory.CreateBulkTasks();
        
        Test_Factory.CreateContract('001S000000AjR2g');
        
        Test_Factory.CreateCase('001S000000AjR2g', '001S000000AjR2g');
        
        Test_Factory.CreateTask();
        
        Test_Factory.CreateAccountTeamContacts('001S000000AjR2g');
        
        Test_Factory.CreateLandscapeBTLB();
        
        Test_Factory.CreateOpportunity('001S000000AjR2g');
        Test_Factory.CreateOpportunityContactRole('001S000000AjR2g','001S000000AjR2g');
        Test_Factory.CreateProductBasket('001S000000AjR2g');
        
        Test_Factory.CreateEvent('001S000000AjR2g');
        
        Test_Factory.CreateValidateLandscapeBTLB_RecordLimit(null);
        
        Test_Factory.CreateProduct();
        
        Test_Factory.CreatePricebookEntry('001S000000AjR2g','001S000000AjR2g');
        //Test_Factory.CreateOpportunitySite('001S000000AjR2g');
        Test_Factory.CreateOpportunityLineItem('001S000000AjR2g','001S000000AjR2g');
        Test_Factory.CreatePhase('001S000000AjR2g');
        Test_Factory.CreateProject('001S000000AjR2g');
    }
}