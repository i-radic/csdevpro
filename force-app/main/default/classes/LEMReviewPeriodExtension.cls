public class LEMReviewPeriodExtension {
	
	private final LEM_Review_Period__c lrp;
	private String lrpId = '';
	public Attachment attachment {
	  get {
	     if (attachment == null)
	      attachment = new Attachment();
	     return attachment;
	  }
	  set;
	}
  
	public Note note {
	  get {
	     if (note == null)
	       note = new Note();
	     return note ;
	  }
	  set;
	}
	
	public LEMReviewPeriodExtension(ApexPages.StandardController stdController) {
        this.lrp = (LEM_Review_Period__c)stdController.getRecord();
        this.lrpId = System.currentPageReference().getParameters().get('id');
    }
    
    public List<Attachment> getAttList() {    
      return [SELECT ID, Name, Description, Body, BodyLength, CreatedByID, CreatedBy.Name, CreatedDate FROM Attachment WHERE ParentID = :lrpId  ORDER BY CreatedDate DESC ];
  	}

	public List<Note> getNoteList() {
      return [SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = :lrpId ORDER BY CreatedDate DESC];
  	}
  	
  	public PageReference upload() {
	    attachment.OwnerId = UserInfo.getUserId();
	    attachment.ParentId = lrpId ; // the record the file is attached to
	    attachment.IsPrivate = false;
	    if (attachment.Description == '') {
	        attachment.adderror('Please enter a Title for the attachment'); 
	        return null;
	    }
	    try {
	      insert attachment;
	    } 
	    catch (DMLException e) {
	      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
	      return null;
	    } 
	    finally {
	      attachment = new Attachment(); 
	      attachment.body  = null;
	    }
	    return null;  	
	}
  
	public PageReference addNote() {
	    note.OwnerId = UserInfo.getUserId();
	    note.ParentId = lrpId ; // the record the file is attached to
	    note.Title= 'Feedback Note';
	 
	    try {
	      insert Note;
	    } catch (DMLException e) {
	      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading Note'));
	      return null;
	    } finally {
	      note= new Note(); 
	    }
	 
	    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Note Added successfully'));
	    return null;
	}
}