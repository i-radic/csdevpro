/**
 * @name CS_JSONService_Base
 * @description Base class for Services that handle JSON generation
 *
 */
public abstract class CS_JSONService_Base {
	protected String QueryTemplate = 'SELECT {0} FROM {1} WHERE {2}';
	
	public CS_JSON_Schema.CS_JSON_Setting setting {get; set;}
	 
	/**
	 * Class Constructor
	 * @return 	CS_JSONService_Base
	 */
	public CS_JSONService_Base() {

	}
	
	public virtual void init(CS_JSON_Schema.CS_JSON_Setting setting) {
		this.setting = setting;
	}
	
	public abstract Object getResult(Id id);
	
	@TestVisible
	protected virtual String escapeId(Id id) {
		return '\''+ id + '\'';
	}
	
	@TestVisible
	protected virtual Map<String, Object> getValues(CS_JSON_Schema.CS_JSON_Object spec, SObject record) {
		Map<String, Object> result = new Map<String, Object>();
		
		for(CS_JSON_Schema.CS_JSON_Field field : spec.fields) {
			result.put(field.label, CS_Utl_SObject.getFieldValue(record, field.name));
		}
		
		return result;
	}
	
	@TestVisible
	protected virtual Map<String, Object> getValues(CS_JSON_Schema.CS_JSON_Object spec, Map<String, Object> untypedInstance) {
		Map<String, Object> result = new Map<String, Object>();
		
		for(CS_JSON_Schema.CS_JSON_Field field : spec.fields) {
			result.put(field.label, untypedInstance.get(field.name));
		}
		
		return result;
	}
}