public without sharing class ChecklistSendEmailTo3rdParty {

    private ApexPages.StandardController stdController;
    public Checklist__c checklist {public get; private set;}
    public String redirectUrl {public get; private set;}
    public boolean showGenerateDocument {public get; private set;}
    public boolean showEmailThirdParty {public get; private set;}
    public List<Attachment> lstAtt {public get; private set;}
	public String msToAddress {public get; private set;}
	public String msCcAddress {public get; private set;}
	public String msFromAddress {public get; private set;}
	public String msSubject {public get; private set;}
    
   /*
    * @name         ChecklistSendEmailTo3rdParty
    * @description	Constructor 
    *				- Sets the checklist details
    *				- Sets the flags to allow Generate Document / Email options
    * @author       P Goodey
    * @date         Sep 2015
    * @see 
    */	
    public ChecklistSendEmailTo3rdParty (ApexPages.StandardController stdController) {
        this.stdController = stdController;
        redirectUrl = stdController.view().getUrl();
        this.checklist = (Checklist__c)stdController.getRecord();
        this.checklist = [  SELECT Id, Release_SIM_Cards__c, Email_3rd_Party__c, Email_Body_3rd_Party__c, Email_Content__c,
        						Name, Product_Types__c, 
                                Company_Implementation__r.Opportunity__c, Company_Implementation__r.Opportunity__r.Account.Name,
                                Contact_Name__c, Contact_Email__c, Contact_Phone__c, Delivery_Address__c
                            FROM Checklist__c 
                            WHERE id=:this.checklist.Id ];
        showGenerateDocument = false;
        showEmailThirdParty = false;
        
		// Set the Email Message Details
		msToAddress = checklist.Email_3rd_Party__c;
		msCcAddress = Label.Checklist_Email_Cc;
		msFromAddress = Label.Checklist_Email_From;
		msSubject = this.checklist.Name + ' ' + Label.Checklist_Email_Subject;         
        
		// Store Temp Details of Email Body
        this.checklist.Email_Body_3rd_Party__c = buildEmailBody();
        
		// Store Temp Details of Email Content
		this.checklist.Email_Content__c = 'Cc: ' + this.msCcAddress + '\n';
		this.checklist.Email_Content__c += 'From: ' + this.msFromAddress + '\n';
		this.checklist.Email_Content__c += 'Subject: ' + this.msSubject + '\n\n';        
        this.checklist.Email_Content__c += this.checklist.Email_Body_3rd_Party__c.replaceAll('<br>','\n');
        
    }


   /*
    * @name         getAttachmentExists
    * @description	Checks whether the Attachment exists (filename specified by custom label)
    * @author       P Goodey
    * @date         Sep 2015
    * @see 
    */
    public boolean getAttachmentExists(){
        boolean bReturn = false;
        // Get the Product Information Attachment
        lstAtt = [  SELECT Name, Body, BodyLength, CreatedDate, ParentId 
                                    FROM Attachment 
                                    WHERE ParentId = :checklist.Company_Implementation__r.Opportunity__c
                                    AND Name = :Label.Checklist_Product_Filename
                                    ORDER BY CreatedDate DESC
                                    LIMIT 1];
        if( !lstAtt.isEmpty() ){
            bReturn= true;
        }           
        return bReturn;
    }
    

   /*
    * @name         getAttachmentExists
    * @description	Checks whether the Email has been sent (checkbox Release_SIM_Cards__c used to flag that email sent)
    * @author       P Goodey
    * @date         Sep 2015
    * @see 
    */
    public boolean getEmailSent(){
        boolean bReturn = [SELECT Id, Release_SIM_Cards__c FROM Checklist__c WHERE Id=:this.checklist.Id].Release_SIM_Cards__c;
        return bReturn;
    }     


   /*
    * @name         sendEmailNew
    * @description	Sends the Email (uses Messaging.SingleEmailMessage without template as there is no contact, lead etc)
    * @author       P Goodey
    * @date         Sep 2015
    * @see 
    */
    public pageReference sendEmail(){
        
        pageReference pageRef = null;

        try {

            // Get the organisation wide address for contract setup
            OrgWideEmailAddress owe = [ SELECT Id, IsAllowAllProfiles, DisplayName, Address
                                        FROM OrgWideEmailAddress
                                        WHERE Address = :msFromAddress LIMIT 1];
                
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            
            String[] sToAddresses = new String[] {msToAddress};
			String sEmailBody = this.checklist.Email_Body_3rd_Party__c;
            mail.setToAddresses(sToAddresses);
            
            // Check if a cc address has been specified
            String[] sCcAddresses;
            if(msCcAddress != null){
            	sCcAddresses = msCcAddress.split(';');
            	mail.setCcAddresses(sCcAddresses);
            }
            mail.setReplyTo(msToAddress);
            mail.setOrgWideEmailAddressId(owe.Id);
            mail.setSubject(msSubject);
            
            mail.setBccSender(false);
            mail.setUseSignature(true);
            mail.setHtmlBody(sEmailBody);

            List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                
            // Get the Product Information Attachment
            for (Attachment a : [   SELECT Name, Body, BodyLength 
                                    FROM Attachment 
                                    WHERE ParentId = :checklist.Company_Implementation__r.Opportunity__c
                                    AND Name = :Label.Checklist_Product_Filename
                                    ORDER BY CreatedDate DESC
                                    LIMIT 1]){
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                efa.setFileName(a.Name);
                efa.setBody(a.Body);
                fileAttachments.add(efa);
            }
                
            // Set the attachments
            mail.setFileAttachments(fileAttachments);
                
			// Send the Email
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
            // Update the Email Flag
            checklist.Release_SIM_Cards__c = true;
            update checklist;
            
            pageRef = new PageReference('/'+checklist.Id);
            pageRef.setRedirect(true);
            
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }

        return pageRef;
    }


   /*
    * @name         buildEmailBody
    * @description	Builds the Email Body
    * @author       P Goodey
    * @date         Sep 2015
    * @see 
    */
    private String buildEmailBody(){
 		String sEmailBody;
        try {
			String sEmailBodyHeader = Label.Checklist_Email_Body_Header;
			if(sEmailBodyHeader!=null){
				sEmailBody = sEmailBodyHeader.replaceAll('\n','<br>');
			}
            sEmailBody += '<br><br>';
            sEmailBody += 'Company Name: ' + checklist.Company_Implementation__r.Opportunity__r.Account.Name;
            sEmailBody += '<br><br>';
            sEmailBody += 'Contact Name: ' + checklist.Contact_Name__c;
            sEmailBody += '<br>';
            sEmailBody += 'Contact Email: ' + checklist.Contact_Email__c;
            sEmailBody += '<br>';
            sEmailBody += 'Contact Phone: ' + checklist.Contact_Phone__c;
            sEmailBody += '<br><br>';
            sEmailBody += 'Delivery Address: ';
            String sDeliveryAddress = checklist.Delivery_Address__c;
            if(sDeliveryAddress != null){
            	sEmailBody += '<br>' + sDeliveryAddress.replaceAll('\n','<br>');
            }
            sEmailBody += '<br><br>';
			String sEmailBodyFooter = Label.Checklist_Email_Body_Footer;
			if(sEmailBodyFooter!=null){
				sEmailBody += sEmailBodyFooter.replaceAll('\n','<br>');
			}  
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        return sEmailBody; 
    }




}