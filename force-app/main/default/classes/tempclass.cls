public class tempclass implements Queueable,Database.AllowsCallouts {
    
    public String productDefinitionname;
    
    public tempclass (String name) {
        this.productDefinitionname = name;
    }
    
    public void execute (QueueableContext context) {
        List<cscfga__Product_Definition__c> pds = [Select Id from cscfga__Product_Definition__c WHERE cscfga__Product_Category__r.Name = 'Cloud Voice' AND Name = :productDefinitionname ];
        
        for (cscfga__Product_Definition__c pd : pds) {
            
            cscfga__Attribute_Definition__c productFamily = new cscfga__Attribute_Definition__c();
            productFamily.cscfga__Product_Definition__c = pd.Id;
            productFamily.Name = 'Product Family';
            productFamily.cscfga__Label__c = 'Product Family';
            productFamily.cscfga__Type__c = 'Calculation';
            productFamily.cscfga__Data_Type__c = 'String';
            productFamily.cscfga__Default_Value__c = 'Cloud Voice';
            productFamily.cscfga__configuration_output_mapping__c = 'Calculations_Product_Group__c';
            productFamily.cscfga__Calculation_Sequence__c = 100;
            
            insert productFamily;
            
            cscfga__Attribute_Definition__c volume = new cscfga__Attribute_Definition__c();
            volume.cscfga__Product_Definition__c = pd.Id;
            volume.Name = 'Volume';
            volume.cscfga__Label__c = 'Volume';
            volume.cscfga__Type__c = 'Calculation';
            volume.cscfga__Data_Type__c = 'Integer';
            volume.cscfga__Default_Value__c = '1';
            volume.cscfga__configuration_output_mapping__c = 'Volume__c';
            volume.cscfga__Calculation_Sequence__c = 101;
            
            insert volume;
            
            cscfga__Attribute_Definition__c oneOffCost = new cscfga__Attribute_Definition__c();
            oneOffCost.cscfga__Product_Definition__c = pd.Id;
            oneOffCost.Name = 'One Off Cost';
            oneOffCost.cscfga__Label__c = 'One Off Cost';
            oneOffCost.cscfga__Type__c = 'Calculation';
            oneOffCost.cscfga__Data_Type__c = 'Decimal';
            oneOffCost.cscfga__Default_Value__c = '0.0';
            oneOffCost.cscfga__configuration_output_mapping__c = 'Mobile_Voice_One_Off_Cost__c';
            oneOffCost.cscfga__Calculation_Sequence__c = 102;
            
            
            insert oneOffCost;
            
            cscfga__Attribute_Definition__c recurringCost = new cscfga__Attribute_Definition__c();
            recurringCost.cscfga__Product_Definition__c = pd.Id;
            recurringCost.Name = 'Recurring Cost';
            recurringCost.cscfga__Label__c = 'Recurring Cost';
            recurringCost.cscfga__Type__c = 'Calculation';
            recurringCost.cscfga__Data_Type__c = 'Decimal';
            recurringCost.cscfga__Default_Value__c = '0.0';
            recurringCost.cscfga__configuration_output_mapping__c = 'Mobile_Voice_Recurring_Cost__c';
            recurringCost.cscfga__Calculation_Sequence__c = 103;
            
            
            insert recurringCost;
            
            cscfga__Attribute_Definition__c oneOffCharge = [Select Id, cscfga__configuration_output_mapping__c from cscfga__Attribute_Definition__c Where cscfga__Product_Definition__c = :pd.Id AND Name = 'One Off Charge'];
            oneOffCharge.cscfga__configuration_output_mapping__c = 'Mobile_Voice_One_Off_Charge__c';
            update oneOffCharge;
            
            cscfga__Attribute_Definition__c recurringCharge = [Select Id, cscfga__configuration_output_mapping__c from cscfga__Attribute_Definition__c Where cscfga__Product_Definition__c = :pd.Id AND Name = 'Recurring Charges'];
            recurringCharge.cscfga__configuration_output_mapping__c = 'Mobile_Voice_Recurring_Charge__c';
            update recurringCharge;
        }
    }
}