@isTest


private class BatchLS2L_TEST{

    static testMethod void runPositiveTestCases() {
    StaticVariables.setAccountDontRun(True);
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
        }
    Account acc1 = Test_Factory.CreateAccount();
    insert acc1;  
    Account acc2 = Test_Factory.CreateAccount();   
    insert acc2;
    Account acc3 = Test_Factory.CreateAccount();    
    insert acc3;   
         
    User u1 = new User(); 
    u1.Username = 'jaofh124@bt.com';
    u1.Ein__c = '9876543f1';
    u1.LastName = 'TestLastname';
    u1.FirstName = 'TestFirstname';
    u1.MobilePhone = '07918672032';
    u1.Phone = '02085878834';
    u1.Title='What i do';
    u1.OUC__c = 'DKW';
    u1.Manager_EIN__c = '123456789';
    u1.Email = 'no.reply@bt.com';
    u1.Alias = 'boatid01';
    u1.Division = 'BTLB';
    u1.ProfileId = '00e20000001MX7z';
    u1.TimeZoneSidKey = 'Europe/London'; 
    u1.LocaleSidKey = 'en_GB';
    u1.EmailEncodingKey = 'ISO-8859-1';
    u1.LanguageLocaleKey = 'en_US';  
    u1.zLeadsOwned__c = 10;  
    u1.IsActive = True;
    insert u1;

    User u2 = new User(); 
    u2.Username = 'cot@bt.com';
    u2.Ein__c = '98765COT';
    u2.LastName = 'TestLastname';
    u2.FirstName = 'TestFirstname';
    u2.MobilePhone = '07918672032';
    u2.Phone = '02085878834';
    u2.Title='What i do';
    u2.OUC__c = 'DKW';
    u2.Manager_EIN__c = '123456789';
    u2.Email = 'no.reply@bt.com';
    u2.Alias = 'boatid01';
    u2.Division = 'COT Sheffield';
    u2.ProfileId = '00e20000001MX7z';
    u2.TimeZoneSidKey = 'Europe/London'; 
    u2.LocaleSidKey = 'en_GB';
    u2.EmailEncodingKey = 'ISO-8859-1';
    u2.LanguageLocaleKey = 'en_US';     
    u2.zLeadsOwned__c = 10;     
    u2.IsActive = True;
    insert u2;

    User u3 = new User(); 
    u3.Username = 'bsclass@bt.com';
    u3.Ein__c = '9bsclass';
    u3.LastName = 'TestLastname';
    u3.FirstName = 'TestFirstname';
    u3.MobilePhone = '07918672032';
    u3.Phone = '02085878834';
    u3.Title='What i do';
    u3.OUC__c = 'DKW';
    u3.Manager_EIN__c = '123456789';
    u3.Email = 'no.reply@bt.com';
    u3.Alias = 'boatid01';
    u3.Division = 'BS Classic Glasgow';
    u3.ProfileId = '00e20000001MX7z';
    u3.TimeZoneSidKey = 'Europe/London'; 
    u3.LocaleSidKey = 'en_GB';
    u3.EmailEncodingKey = 'ISO-8859-1';
    u3.LanguageLocaleKey = 'en_US';    
    u3.zLeadsOwned__c = 10;      
    u3.IsActive = True;
    insert u3;        

    Landscape_DBAM__c lsDBAM= new Landscape_DBAM__c (Account__c = acc1.Id, 
    Calls_contract_end_date__c = Date.today()+10,
    Lines_contract_end_date__c = Date.today()+10,
    Mobile_End_Date__c = Date.today()+10,
    WAN_End_Date__c = Date.today()+10
    ); 
    insert lsDBAM;
    
    Landscape_BTLB__c lsLEM= new Landscape_BTLB__c (Customer__c = acc1.Id, 
    Calls_contract_expiry_date__c = Date.today()+10,
    Lines_contract_expiry_date__c = Date.today()+10,
    ISP_contract_expiry_date__c = Date.today()+10,
    Mobile_contract_expiry_date__c = Date.today()+10,
    Phone_system_maint_contract_expiry_date__c = Date.today()+10,
    LAN_contract_renewal_date__c = Date.today()+10,
    Network_contract_renewal_date__c = Date.today()+10
    ); 
    insert lsLEM;
    Landscape_BTLB__c lsLEM2 = new Landscape_BTLB__c (Customer__c = acc2.Id, 
    Calls_contract_expiry_date__c = Date.today()+10,
    Lines_contract_expiry_date__c = Date.today()+10,
    ISP_contract_expiry_date__c = Date.today()+10,
    Mobile_contract_expiry_date__c = Date.today()+10,
    Phone_system_maint_contract_expiry_date__c = Date.today()+10,
    LAN_contract_renewal_date__c = Date.today()+10,
    Network_contract_renewal_date__c = Date.today()+10 
    );     
    insert lsLEM2;
    Landscape_BTLB__c lsLEM3 = new Landscape_BTLB__c (Customer__c = acc2.Id, 
    Calls_contract_expiry_date__c = Date.today()+10,
    Lines_contract_expiry_date__c = Date.today()+10,
    ISP_contract_expiry_date__c = Date.today()+10,
    Mobile_contract_expiry_date__c = Date.today()+10,
    Phone_system_maint_contract_expiry_date__c = Date.today()+10,
    LAN_contract_renewal_date__c = Date.today()+10,
    Network_contract_renewal_date__c = Date.today()+10 
    );   
    insert lsLEM3;  

    Test.startTest();
    System.RunAs(u2){
        lsLEM.Calls_contract_expiry_date__c = Date.today()+11;
        update lsLEM;
    }  
    System.RunAs(u3){
        lsLEM.LAN_contract_renewal_date__c = Date.today()+11;
        update lsLEM;
    }    

    System.RunAs(u2){
        lsLEM2.Calls_contract_expiry_date__c = Date.today()+20;
        lsLEM2.Lines_contract_expiry_date__c = Date.today()+20;
        lsLEM2.ISP_contract_expiry_date__c = Date.today()+20;
        lsLEM2.Mobile_contract_expiry_date__c = Date.today()+20;
        lsLEM2.Phone_system_maint_contract_expiry_date__c = Date.today()+20;
        lsLEM2.LAN_contract_renewal_date__c = Date.today()+20;
        lsLEM2.Network_contract_renewal_date__c = Date.today()+20;
        update lsLEM2;
    }  
    System.RunAs(u3){
        lsLEM3.Calls_contract_expiry_date__c = Date.today()+20;
        lsLEM3.Lines_contract_expiry_date__c = Date.today()+20;
        lsLEM3.ISP_contract_expiry_date__c = Date.today()+20;
        lsLEM3.Mobile_contract_expiry_date__c = Date.today()+20;
        lsLEM3.Phone_system_maint_contract_expiry_date__c = Date.today()+20;
        lsLEM3.LAN_contract_renewal_date__c = Date.today()+20;
        lsLEM3.Network_contract_renewal_date__c = Date.today()+20;
        update lsLEM3;
    }          
    
    //execute batch  
    //LEM Landscape data matchin criteria to create a lead, some of the logic is in the landscape object formula fields returning true.
    String qString = 'SELECT Id, Customer__c, Customer__r.Name, Customer__r.OwnerId, Customer__r.Owner.Department';
    qString = qString + ' ,xLeads_Flag_Calls__c, xLeads_Flag_Lines__c, xLeads_Flag_BB__c, xLeads_Flag_Mobile__c, xLeads_Flag_Switch__c, xLeads_Flag_LAN__c, xLeads_Flag_WAN__c';
    qString = qString + ' ,If_not_BT_who_do_you_use_for_calls__c, Calls_contract_expiry_date__c';
    qString = qString + ' FROM Landscape_BTLB__c';
    qString = qString + ' WHERE Id = \'' + lsLEM.Id +'\' OR Id = \'' + lsLEM2.Id + '\' OR Id = \'' + lsLEM3.Id +'\'';
    BatchLS2L_01LEM b = new BatchLS2L_01LEM(qString);
    database.executeBatch(b, 5);
    //DBAM Landscape data matchin criteria to create a lead, some of the logic is in the landscape object formula fields returning true.
    String qString2 = 'SELECT Id, Account__c, Account__r.Name, Account__r.OwnerId, Account__r.Owner.Department ';
    qString2 = qString2 + ' ,xLeads_Flag_Calls__c, xLeads_Flag_Lines__c, xLeads_Flag_BB__c, xLeads_Flag_Mobile__c, xLeads_Flag_Switch__c, xLeads_Flag_LAN__c, xLeads_Flag_WAN__c';
    qString2 = qString2 + ' FROM Landscape_DBAM__c';        
    qString2 = qString2 + ' WHERE Id = \'' + lsDBAM.Id +'\'';
    BatchLS2L_02DBAM b2 = new BatchLS2L_02DBAM(qString2);
    database.executebatch(b2, 5);
  
    /*
    BatchLS2L_03CountLeads lCountCL = new BatchLS2L_03CountLeads();
    lCountCL.query = 'SELECT Id, OwnerId FROM Lead WHERE CreatedDate = LAST_N_DAYS:2';
    ID batchprocessid = Database.executeBatch(lCountCL);    
    BatchLS2L_04AlertChatter lCountC = new BatchLS2L_04AlertChatter();
    lCountC.query = 'SELECT Id, Email, Name, zLeadsOwned__c FROM User WHERE isActive = TRUE AND zLeadsOwned__c > 0';
    database.executeBatch(lCountC, 5);     

    BatchLS2L_05AlertEmail lCountE = new BatchLS2L_05AlertEmail();
    lCountE.query = 'SELECT Id, Email, Name, zLeadsOwned__c FROM User WHERE isActive = TRUE AND zLeadsOwned__c > 0';
    database.executeBatch(lCountE, 10);    

    BatchLS2L_06ZeroLeads lCountD = new BatchLS2L_06ZeroLeads();
    lCountD.query = 'SELECT Id, Email, Name, zLeadsOwned__c FROM User WHERE isActive = TRUE';
    database.executeBatch(lCountD, 200);
    Test.stopTest();                 
    */
    }
}