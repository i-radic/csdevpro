public class assignPermSet {

//Method to assign permission set to user
@future (callout=true)
public static void insertPermRecord(id permSet,Id userID){ 
    PermissionSetAssignment permAssign = new PermissionSetAssignment(PermissionSetId = permSet, AssigneeId = userID );
    try{
        insert permAssign;
    }
    catch(system.dmlexception e){
        system.debug( e );}
                                 
}    
}