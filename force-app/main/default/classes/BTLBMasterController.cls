public with sharing class BTLBMasterController {

private User u; 

    public BTLBMasterController(ApexPages.StandardController stdcontroller) {
    this.u= (User)stdController.getRecord();
    }
    
    private List<SelectOption> BTLBnames;
    Public String SelectedName {get; set;}
    
    public List<SelectOption> getBTLBnames(){
        BTLBnames = new List<SelectOption>();
        BTLBnames.add(new SelectOption('--None--','--None--'));
        for (BTLB_Master__c rt : [Select Id, Name From BTLB_Master__c
            Order By Name ASC]) {
            BTLBnames.add(new SelectOption(rt.Name, rt.Name));            
            }
            
    return BTLBnames;
    }
   
    //Update user details
    public PageReference UpdateUser()
    {
      pageReference DelP=new pageReference('/'+u.Id);

      If(SelectedName!=null && SelectedName!='--None--'){      
            List<BTLB_Master__c> BMList = [Select Id,Name,Company_Name__c,Email_Signature__c,Company_Registration_No__c,Company_Registered_Address__c from BTLB_Master__c where Name=:SelectedName LIMIT 1];
            if(BMList.size()>0){
                u.Department=SelectedName;
                u.Division='BTLB';
                if(BMList[0].Email_Signature__c!=null)
                    u.Email_Signature__c=BMList[0].Email_Signature__c;
                if(BMList[0].Company_Name__c!=null)
                    u.Company_Name__c=BMList[0].Company_Name__c;
                if(BMList[0].Company_Registration_No__c!=null) 
                    u.Company_Registration_No__c=BMList[0].Company_Registration_No__c;
                if(BMList[0].Company_Registered_Address__c!=null)
                    u.Company_Registered_Address__c=BMList[0].Company_Registered_Address__c;
                
                update u;
                return DelP;        
            }  
            return DelP;          
         }
       Else           
            return DelP;
    }
 

}