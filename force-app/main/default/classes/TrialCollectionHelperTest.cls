/*
###########################################################################
# File..................: TrialCollectionHelperTest.cls
# Version...............: 1
# Created by............: Martin Eley
# Created Date..........: 9th October 2012
# Last Modified by......: Prashant Kalia
# Last Modified Date....: 4th April 2013
# Description...........: Test Class                         
# VF page...............:             
# Change Log:               
# Apex Class............: TrialCollectionHelper.cls
# Apex Trigger..........: TrialCollectionTrigger.Trigger
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#	
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
#
###########################################################################
*/

@IsTest
(SeeAllData=True)
public with sharing class TrialCollectionHelperTest {
	
	static testMethod void testSendCollectionRequestEmail(){
		
	   Test.startTest();      
       Account acc = Test_Utils.createAccount('TestAcc');
       Contact cnt = Test_Utils.createContact(null,null,acc.Id,null);
       Opportunity opp= Test_Utils.createOpp(null,null,null,acc.Id);
       Trial__c objTrial = Test_Utils.createTrial(opp,cnt);
       system.debug('********objTrial'+objTrial.Id);
       Test_Utils.createTrialCourier();
       
       Trial_Collection__c trialColl = Test_Utils.createTrialCollection(objTrial);
       Test.stopTest();       
	}
}