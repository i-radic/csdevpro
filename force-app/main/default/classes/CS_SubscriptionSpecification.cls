global with sharing class CS_SubscriptionSpecification {

    public CS_SubscriptionSpecification(String name, List<CS_ServiceSpecification> serviceSpecificationList, String subId) {
        this.serviceName = name;
        this.serviceSpecificationList = serviceSpecificationList;
        this.subscriptionId = subId;
    }
    public List<CS_ServiceSpecification> serviceSpecificationList { get; set; }
    String serviceName { get; set; }
    String subscriptionId { get; set; }

}