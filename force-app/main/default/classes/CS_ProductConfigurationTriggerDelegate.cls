public with sharing class CS_ProductConfigurationTriggerDelegate extends CS_TriggerHandler.DelegateBase {
    List<cscfga__Product_Configuration__c> deletedConfigs = new List<cscfga__Product_Configuration__c>();
    Map<Id, cscfga__Product_Configuration__c> createdRootConfigs = new Map<Id, cscfga__Product_Configuration__c>();
    
    // do any preparation here – bulk loading of data etc
    public override void prepareBefore() {

    }
    
    // do any preparation here - bulk loading of data etc
    public override void prepareAfter() {
        
    }
    
    // Apply before insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeInsert(sObject o) {

    }
    
    // Apply before update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeUpdate(sObject old, sObject o) {
        cscfga__Product_Configuration__c config = (cscfga__Product_Configuration__c) o;
        cscfga__Product_Configuration__c oldConfig = (cscfga__Product_Configuration__c) old;

        CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
        String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;

        if(config.Calculations_Product_Group__c != null && calculateProductGroup.contains(config.Calculations_Product_Group__c)) {
            return;
        }

        if(config.Commercial_Product__c != oldConfig.Commercial_Product__c || config.Add_On__c != oldConfig.Add_On__c) {
            config.cscfga__discounts__c = null;
        }
        
        if(String.isNotBlank(config.cscfga__discounts__c)) {
            CS_ProductConfigurationService.updateDiscounts(config, oldConfig);
        }
        // to Update CV NPV values
       // if(config.Calculations_Product_Group__c == 'Cloud Voice'){
         // config.NPV_Monthly_Revenue__c = config.cscfga__Recurring_Charge__c*config.cscfga__Quantity__c;
         // config.NPV_Upfront_Revenue__c = config.cscfga__One_Off_Charge__c*config.cscfga__Quantity__c;
          //config.NPV_Monthly_Cost__c = config.Enterprise_Monthly_Cost__c*config.cscfga__Quantity__c*(-1);
         // config.NPV_Upfront_Cost__c = config.Enterprise_Upfront_Cost__c*config.cscfga__Quantity__c*(-1);          
        // }  // end to Update CV NPV values

        if(config.Volume__c != null) {
            if(config.Product_Definition_Name__c == 'BT Mobile Hardware' || config.Product_Definition_Name__c == 'Sharer Subscription' || 
                config.Product_Definition_Name__c == 'MDM' || config.Product_Definition_Name__c == 'Shared Data' || 
                config.Product_Definition_Name__c == 'BT Mobile Extras' || config.Product_Definition_Name__c == 'Office 365') {
                    config.Incremental_Overheads__c = 0.04 * (config.Other_Revenue__c + config.Voice_Revenue_inc_SMS__c + config.Data_Revenue__c + config.Care_and_Chargable_Service_Revenue__c);
            }
            else if(config.Product_Definition_Name__c == 'BT Mobile Broadband') {
                config.Incremental_Overheads__c = 0.04 * (config.Other_Revenue__c + config.Voice_Revenue_inc_SMS__c + config.Data_Revenue__c + config.Care_and_Chargable_Service_Revenue__c) + (0.33 * config.Volume__c);
            }
            else if(config.Product_Definition_Name__c == 'BT Mobile Flex') {
                config.Usage_Profile_Voice_Revenue__c = config.Usage_Profile_Voice_Revenue__c != null ? config.Usage_Profile_Voice_Revenue__c : 0;
                config.Usage_Profile_Data_Revenue__c = config.Usage_Profile_Data_Revenue__c != null ? config.Usage_Profile_Data_Revenue__c : 0;
                config.Incremental_Overheads__c = (0.04 * (config.Voice_Revenue_inc_SMS__c + config.Data_Revenue__c)) + (0.33 * config.Volume__c);
            }
            else if(config.Product_Definition_Name__c == 'BT Mobile Sharer') {
                config.Incremental_Overheads__c = 0.04 * (config.Other_Revenue__c + config.Voice_Revenue_inc_SMS__c + config.Data_Revenue__c) + (0.33 * (config.Total_Subscribers__c));
            }
            else if(config.Product_Definition_Name__c != null && config.Product_Definition_Name__c.startsWith('BTOP')) {
                config.Incremental_Overheads__c = 0.01 *  (config.Voice_Revenue_inc_SMS__c + config.Data_Revenue__c + config.Care_and_Chargable_Service_Revenue__c + config.Site_And_Sub_Install__c + config.Other_Revenue__c);
            }
        }
        
        if(config.Commission_Contribution__c != null && config.Commission_Contribution__c != 0) {
            config.Sales_Commision_One_Off_Cost__c = (config.cscfga__Recurring_Charge__c * config.Commission_Contribution__c + config.cscfga__One_Off_Charge__c * config.Commission_Contribution__c) * (-1);
        }
        
        //custom caller change
        /*if(config.Custom_Caller_Unit_Charge__c== null) {
            config.Custom_Caller_Unit_Charge__c=0.0;
            config.Custom_Caller_Discounted_Unit_Charge__c =0.0;
        }else{
            if(String.isBlank(config.cscfga__discounts__c)) {
                config.Custom_Caller_Discounted_Unit_Charge__c = config.Custom_Caller_Unit_Charge__c;
            }
        }*/
    }
    
    // Apply before delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeDelete(sObject o) {
        cscfga__Product_Configuration__c cfg = (cscfga__Product_Configuration__c) o;

        CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
        String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;

        if(cfg.Calculations_Product_Group__c != null && calculateProductGroup.contains(cfg.Calculations_Product_Group__c)) {
            return;
        }

        deletedConfigs.add((cscfga__Product_Configuration__c) o);
    }

    // Apply after insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterInsert(sObject o) {
        cscfga__Product_Configuration__c cfg = (cscfga__Product_Configuration__c) o;

        CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
        String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;
        if(cfg.Calculations_Product_Group__c != null && calculateProductGroup.contains(cfg.Calculations_Product_Group__c)) {
            return;
        }

        if(cfg.cscfga__Root_Configuration__c == null && cfg.cscfga__Product_Basket__c != null) {
            system.debug(' ==== > createdRootConfigs ==>' +cfg.Id);
            createdRootConfigs.put(cfg.Id, cfg);
        }
    }

    // Apply after update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUpdate(sObject old, sObject o) {

    }

    // Apply after delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterDelete(sObject o) {

    }

    // Apply after undelete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUndelete(sObject o) {
        
    }

    // finish logic - process stored records and perform any dml action
    // updates product baskets if bundle is desynchronised
    public override void finish() {
        if(!deletedConfigs.isEmpty()) {
            CS_ProductConfigurationService.deleteProdConfigTotals(deletedConfigs);
        }
        if(!createdRootConfigs.isEmpty()) {
            CS_ProductConfigurationService.createProductConfigurationTotals(createdRootConfigs);
        }
    }
}