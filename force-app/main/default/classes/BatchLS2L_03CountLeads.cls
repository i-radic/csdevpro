global class BatchLS2L_03CountLeads implements Database.Batchable<sobject>,Database.Stateful{
/*
###################################################################################################
To calculate the number of leads created by owner by a user and post chatter message or email
account owners from being made inactive automatically.

24/01/12    John McGovern    Intial Build

###################################################################################################
*/

public String query;
public Set<id> uIds = new Set<id>();
global Map<Id,Decimal> umap = new Map<Id, Decimal>();
List<User> users = new List<User>();

global BatchLS2L_03CountLeads() {
    for (User u:[Select Id from User where isActive = TRUE]) { 
        umap.put(u.Id, 0);
        uIds.add(u.Id);
    }
}

global database.querylocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    for(sObject s : scope) {
        Lead l = (Lead)s;
        if (umap.containsKey(l.OwnerId)) {
            Decimal cu = umap.get(l.OwnerId) + 1;
            umap.put(l.OwnerId,cu);
        }
    }
   
    List<User> lmap = new List<User>();
    for (Id smap : umap.keySet()) { 
        if(Test.isRunningTest() || umap.get(smap) > 0){     
            lmap.add(new User (Id = smap, zLeadsOwned__c = umap.get(smap)));
        }
    }
    StaticVariables.setUserDontRun(true); 
    update lmap;
}

global void finish(Database.BatchableContext BC){  
    //create next batch
    if(!Test.isRunningTest()){  
        BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
        DateTime n = datetime.now().addMinutes(2);
        String cron = '';
    
        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();
    
        b.scheduled_id4__c = System.schedule('LS2L Batch 4', cron, new BatchLS2L_Schedule04());
    
        update b;         
    }       
}

}