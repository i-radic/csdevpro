@istest

private class Test_SalesforceServicesCRM{
    static testMethod void GetAccount() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        SalesforceServicesCRM.Account account = null;
        
        try
        {
            client.GetAccount('test','test','test');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
        
        account = new SalesforceServicesCRM.Account();
        account.CUG = '';
        account.SAC = '';
        account.PrimaryContactid = '';
        account.CustomerName = '';
        account.IsResidential = false;
        account.Type_x = 'type';
        account.CustomerClass = 'cust';
        account.IsBTBLegacy = false;
        account.OrganizationName = 'test';
    }
    
    static testMethod void CreateEventInteraction() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        SalesforceServicesCRM.BTLBEventInteraction btlbEvent = null;
        btlbEvent = new SalesforceServicesCRM.BTLBEventInteraction();
        btlbEvent.EventId = '';
        btlbEvent.CUG = '';
        btlbEvent.RecordId = '';
        btlbEvent.agentEIN = '';
        btlbEvent.interactionIntegrationId = '';
        try
        {
            client.CreateEventInteraction('','',btlbEvent);
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
    static testMethod void GetAddress() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        
        try
        {
            client.GetAddress('test','test','test','test');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }

    static testMethod void GetNADAddress() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        try
        {
            SalesforceServicesCRM.AddressList addressList = client.GetNADAddress('test','test','SW19 1AA');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }

    static testMethod void GetContact() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        try
        {
            SalesforceServicesCRM.Contact contact = client.GetContact('test','test','cug','contactid');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }

    private static SalesforceServicesCRM.Contact CreateContactForTest() {
        SalesforceServicesCRM.Contact contact = new SalesforceServicesCRM.Contact();
        contact.CUG = '';
        contact.AccountManagerContact = '';        
        contact.ActiveFlag = 'Y';        
        contact.Comment = '';        
        contact.Type_x = '';        
        contact.ExternalId = '';        
        contact.Title = 'Mr';        
        contact.FirstName = 'FirstName';        
        contact.MiddleName = 'MiddleName';        
        contact.LastName = 'LastName';        
        contact.AliasName = 'AliasName';        
        contact.ContactSource = 'Salesforce';       
        contact.JobSeniority = '';        
        contact.Profession = '';        
        contact.DateOfBirth = DateTime.now();        
        contact.MotherMaidenName = '';        
        contact.Status = '';        
        contact.JobTitle = '';        
        contact.KeyDecisionMaker = '';        
        contact.FaxNumber = '';        
        contact.Organization = '';        
        contact.Department = '';        
        contact.RightToContact = '';        
        contact.PreferredContactChannel = '';        
        contact.SecondaryPreferredContactChannel = '';        
        contact.EmailAddress = '';        
        contact.EmailAddressId = '';        
        contact.EmailConsentPermission = '';        
        contact.EmailConsentDate = DateTime.now();        
        contact.EmailConsentType = '';        
        contact.HomePhoneId = '';        
        contact.HomePhone = '';        
        contact.HomePhoneConsentPermission = '';        
        contact.HomePhoneConsentDate = DateTime.now();        
        contact.HomePhoneConsentType = '';        
        contact.MobilePhoneId = '';        
        contact.MobilePhone = '';        
        contact.MobilePhoneConsentPermission = '';        
        contact.MobilePhoneConsentDate = DateTime.now();        
        contact.MobilePhoneConsentType = '';        
        contact.FaxId = '';        
        contact.Fax = '';        
        contact.FaxConsentPermission = '';        
        contact.FaxConsentDate = DateTime.now();        
        contact.FaxConsentType = '';        
        contact.WorkPhoneId = '';        
        contact.WorkPhone = '';        
        contact.WorkPhoneConsentPermission = '';        
        contact.WorkPhoneConsentDate = DateTime.now();        
        contact.WorkPhoneConsentType = '';        
        contact.AddressId = '';        
        contact.AddressNumber = '';        
        contact.AddressStreet = '';        
        contact.AddressLocality = '';        
        contact.AddressPostCode = '';        
        contact.AddressPostTown = '';        
        contact.AddressSector = '';        
        contact.AddressSubBuilding = '';        
        contact.AddressZipCode = '';        
        contact.AddressConsentPermission = '';        
        contact.AddressConsentDate = DateTime.now();        
        contact.AddressTimeAt = '';        
        contact.AddressConsentType = '';
        
        return contact;
    }

    static testMethod void InsertContact() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.Contact contact = CreateContactForTest();
               
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        try
        {
            client.InsertContact('test','test',contact,'test');
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
    static testMethod void UpdateContact() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.Contact contact = CreateContactForTest();
               
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        try
        {
            client.UpdateContact('test','test',contact);
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
     static testMethod void CreateNADAddress() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        SalesforceServicesCRM.Address address = CreateAddressForTest();
               
        SalesforceServicesCRM.CRMServiceSoap client = Endpoints.SFCRMGateway();
        try
        {
            client.CreateNADAddress('test','test',address);
        }
        catch(Exception e){
            // test cannot run webservice calls
        }
    }
    
     private static SalesforceServicesCRM.Address CreateAddressForTest() {
        SalesforceServicesCRM.Address address = new SalesforceServicesCRM.Address();
        address.IdentifierId = 'test';
        address.IdentifierName = '';
        address.IdentifierValue = '';
        address.Country = '';
        address.County = '';
        address.Name = '';
        address.POBox = '';
        address.BuildingNumber = '';
        address.Street = '';
        address.Locality = '';
        address.DoubleDependentLocality = '';
        address.PostCode = '';
        address.Town = '';
        address.SubBuilding = '';
        address.ExchangeGroupCode = '';
        
        return address;
    }
    
}