global class Batch_DeleteInactiveContacts implements Database.Batchable<sObject>
{
    
    global Database.QueryLocator start(Database.BatchableContext BC) { 
        String query = 'select id from Contact where (DL_Contact_Sector__c= \'BT Local Business\' OR DL_Contact_Sector__c=\'BT LOCAL BUSINESS\' OR DL_Contact_Sector__c= \'enterprise\' OR DL_Contact_Sector__c = \'Enterprise\') AND (Status__c = \'InActive\' OR Status__c = \'N\')' ;
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> scope) 
    {   
        system.debug('***BatchDebug'+scope);
        List<Id> deleteids = new List<Id>();
        Database.DeleteResult [] DeleteContactresult;
        if(scope.size()>0)
        DeleteContactresult  = Database.delete(Scope, false);
        for(Database.DeleteResult dr : DeleteContactresult  ) {
        if (dr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully deleted account with ID: ' + dr.getId());
            deleteids.add(dr.getId());
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : dr.getErrors()) {
                System.debug('The following error has occurred.');                    
                System.debug(err.getStatusCode() + ': ' + err.getMessage());
                System.debug('Contact fields that affected this error: ' + err.getFields());
            }
        }
    }
        if(deleteids.size()>0)
        database.emptyRecycleBin(deleteids);

    }

    global void finish(Database.BatchableContext BC) {}
 }