@isTest

private class Test_OpportunityCloseDate{
 
    static testMethod void runPositiveTestCases() {
        Account account = Test_Factory.CreateAccount();
        Account dummyAccount = Test_Factory.CreateAccountForDummy();
        insert dummyAccount;
    
        Pricebook2 priceBook = [select Id from Pricebook2 where isStandard=true limit 1];
       
        Opportunity opp1 = Test_Factory.CreateOpportunity(account.id);
        opp1.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(opp1);
         
        Product2 product = Test_Factory.CreateProduct();
        Database.SaveResult productResult = Database.insert(product);
       
        PricebookEntry pricebookEntry = Test_Factory.CreatePricebookEntry(priceBook.Id, productResult.getId());
        Database.SaveResult pricebookEntryResult = Database.insert(pricebookEntry);

        OpportunityLineItem opportunityLineItem = Test_Factory.CreateOpportunityLineItem(pricebookEntryResult.getId(), opptResult.getId());
        insert opportunityLineItem;
        
        opp1.StageName = 'Prospecting';
        opp1.Sales_Stage_Detail__c = 'Appointment Made';
        update opp1;
    }
}