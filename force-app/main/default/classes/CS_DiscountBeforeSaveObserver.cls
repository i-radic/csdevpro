global class CS_DiscountBeforeSaveObserver implements csdiscounts.IDiscountsObserver {

    public CS_DiscountBeforeSaveObserver() {    }

    String returnMessage = '';

    public String execute(List<cscfga__Product_Configuration__c> productConfigurations){
        Boolean isUnlimitedProduct = false;
        Boolean isIndividualBasket = false;
        Decimal unlimitedDiscountAmount;
        Id unlimitedConfigId;

        System.debug(LoggingLevel.WARN, '**** Topic: ' + productConfigurations);
        try{
            if(productConfigurations != null && productConfigurations.size() > 0){
                Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>(productConfigurations);

                Map<Id, cscfga__Product_Configuration__c> oldConfigs = new Map<Id, cscfga__Product_Configuration__c>([SELECT
                        Id, Name, Volume__c,Product_Code__c, cscfga__Product_Basket__c, Calculations_Product_Group__c
                FROM
                        cscfga__Product_Configuration__c
                WHERE
                        Id = :configs.keySet()]);

                List<cscfga__Product_Configuration__c> unlimitedConfigs = [
                                                            SELECT Product_Code__c
                                                            FROM cscfga__Product_Configuration__c
                                                            WHERE cscfga__Product_Basket__c = :oldConfigs.values().get(0).cscfga__Product_Basket__c
                                                            AND (Product_Code__c = 'UNLMAT' OR Product_Code__c = 'VOUNLM')];
                
                if (unlimitedConfigs != null) {
                    List<cscfga__Product_Configuration__c> mainComponent = [SELECT Name FROM cscfga__Product_Configuration__c WHERE is_Main_Component__c = True 
                                                            AND  cscfga__Product_Basket__c = :oldConfigs.values().get(0).cscfga__Product_Basket__c AND Name Like '%Individual%'];

                    if (mainComponent != null) {
                        isIndividualBasket = true;
                    }
                }                                            
                
                for(cscfga__Product_Configuration__c cfg : configs.values()) {
                    if (oldConfigs.get(cfg.Id).Product_Code__c == 'UNLMAT' || oldConfigs.get(cfg.Id).Product_Code__c == 'VOUNLM' && oldConfigs.get(cfg.Id).Calculations_Product_Group__c == 'Future Mobile' && !isIndividualBasket) {
                        Map<String, Object> dJSON = (Map<String, Object>) JSON.deserializeUntyped(cfg.cscfga__discounts__c);
                        List<Object> disList = (List<Object>) dJSON.get('discounts');
                        if(disList.size() > 0){
                            if (unlimitedConfigs.size() > 1 && productConfigurations.size() == 1) {
                                return '{"status":"500", "message" : "More than 1 Unlimited Texts in the basket, use global Save Discounts button"}';
                            }

                            Map<String, Object> m2 = (Map<String, Object>)disList[0]; //Creating a map with Discount list
                            List<Object> mDisList = (List<Object>) m2.get('memberDiscounts'); //getting the member discounts
                            if(mDisList.size() > 0) {
                                for(Object disObj : mDisList) {
                                    Map<String, Object> dis = (Map<String, Object>) disObj;
                                    system.debug('Discount : '+dis);
                                    if(dis.get('type') == 'absolute') {
                                        cscfga__Product_Configuration__c oldCfg = oldConfigs.get(cfg.Id);
                                        decimal amt=  Double.valueOf(dis.get('amount'));
                                        unlimitedDiscountAmount = amt;
                                    }
                                }
                            }
                            isUnlimitedProduct = true;
                            unlimitedConfigId = cfg.Id;
                            break;
                        }
                    }

                    if(String.isNotBlank(cfg.cscfga__discounts__c)) {
                        Set<String> chargeTypes = new Set<String>();
                        System.debug('Test file '+cfg.cscfga__discounts__c);
                        //CS_DiscountJSON dJSON = (CS_DiscountJSON) JSON.deserialize(cfg.cscfga__discounts__c,  CS_DiscountJSON.class);
                        //cscfga.ProductConfiguration.ProductDiscount dJSON = (cscfga.ProductConfiguration.ProductDiscount)JSON.deserializeStrict(cfg.cscfga__discounts__c, cscfga.ProductConfiguration.ProductDiscount.class);
                        Map<String, Object> dJSON = (Map<String, Object>) JSON.deserializeUntyped(cfg.cscfga__discounts__c);
                        System.debug('JSON Value ' +dJSON );

                        List<Object> disList = (List<Object>) dJSON.get('discounts');
                        if(disList.size() > 0){
                            Map<String, Object> m2 = (Map<String, Object>)disList[0]; //Creating a map with Discount list
                            List<Object> mDisList = (List<Object>) m2.get('memberDiscounts'); //getting the member discounts
                            System.debug('Member Discount List :' +mDisList);

                            //for(cscfga.ProductConfiguration.Discount dis : dJSON.discounts) {
                            //for(CS_DiscountJSON.Discount dis : dJSON.discounts) {
                            if(mDisList.size() > 0){
                                for(Object disObj : mDisList) {
                                    Map<String, Object> dis = (Map<String, Object>) disObj;
                                    system.debug('Discount : '+dis);
                                    if(dis.get('type') == 'absolute') {
                                        cscfga__Product_Configuration__c oldCfg = oldConfigs.get(cfg.Id);
                                        decimal amt=  Double.valueOf(dis.get('amount'));
                                        System.debug('oldCfg.Volume__c>>> ' +oldCfg.Volume__c );
                                        //Z.C. commented out as it caused user group add-ons to calculate discounts wrong in Deal Management
                                        amt = amt * oldCfg.Volume__c;
                                        System.debug('Amount : '+amt);
                                        //String typ= String.valueOf(dis.get('chargeType')); //Redacted
                                        //System.debug('Type Value : ' +typ); //Redacted
                                        dis.put('amount', amt);
                                        /*Redacted
                                        if (typ == 'recurring')
                                        {
                                        cfg.put('cscfga__recurring_charge_product_discount_value__c', amt);
                                            system.debug('cscfga__recurring_charge_product_discount_value__c '+cfg.cscfga__recurring_charge_product_discount_value__c);
                                        }
                                        else
                                        {
                                            cfg.put('cscfga__one_off_charge_product_discount_value__c', amt);
                                        }
                                        redacted End*/

                                        //dis.amount = amt;//amt.toPlainString();
                                    }
                                    if(chargeTypes.contains((String)dis.get('discountCharge'))) {
                                        return '{"status":"500", "message":"Only one discount allowed per charge."}';
                                    }
                                    else {
                                        chargeTypes.add((String)dis.get('discountCharge'));
                                    }
                                    Map<String, Object> customData;
                                    String discountCode;
                                    if (dis.get('customData') != null) {
                                        customData = (Map<String, Object>) dis.get('customData');
                                        System.debug('customData>>>>' + customData);
                                        discountCode = String.valueOf(customData.get('discountCode'));
                                    }
                                    System.debug('discountCode>>>>'+discountCode);
                                    if(String.isNotBlank(discountCode)){
                                        cfg.Discount_Code__c = discountCode;
                                    }
                                    if(customData!=null){
                                        if(String.valueOf(dis.get('source')).contains('Manual -Infinity')){

                                            String stagedMonth = String.valueOf(customData.get('stagedMonth'));
                                            System.debug('stagedMonth>>>>'+stagedMonth);
                                            if(String.isNotBlank(stagedMonth)){
                                                return '{"status":"500", "message":"Staged Month Not applicable for selected discount type."}';
                                            }
                                        }else{
                                            String stagedMonth = String.valueOf(customData.get('stagedMonth'));
                                            if(String.isNotBlank(stagedMonth)){
                                                cfg.Credit_fund_staged_Month__c = Integer.valueOf(stagedMonth);
                                            }
                                        }
                                    }
                                }

                            }
                        }
                        cfg.cscfga__discounts__c = JSON.serialize(dJSON);
                    }
                }

                if (isUnlimitedProduct && !isIndividualBasket) {
                    for(cscfga__Product_Configuration__c cfg : productConfigurations) {
                        if (oldConfigs.get(cfg.Id).Product_Code__c == 'UNLMAT' || oldConfigs.get(cfg.Id).Product_Code__c == 'VOUNLM') {
                            Map<String, Object> dJSON = (Map<String, Object>) JSON.deserializeUntyped(cfg.cscfga__discounts__c);
                            List<Object> disList = (List<Object>) dJSON.get('discounts');
                            System.debug('@@@@@@'+disList);
                            if(disList.size() == 0){
                                return '{"status":"500", "message" : "The discounts added to ‘The Individual Plan - Unlimited Calls and Text’ product in each user group much be the same"}';
                            }

                            if(disList.size() > 0 && cfg.Id != unlimitedConfigId){
                                Map<String, Object> m2 = (Map<String, Object>)disList[0]; //Creating a map with Discount list
                                List<Object> mDisList = (List<Object>) m2.get('memberDiscounts'); //getting the member discounts
                                if(mDisList.size() > 0) {
                                    for(Object disObj : mDisList) {
                                        Map<String, Object> dis = (Map<String, Object>) disObj;
                                        system.debug('Discount : '+dis);
                                        if(dis.get('type') == 'absolute') {
                                            cscfga__Product_Configuration__c oldCfg = oldConfigs.get(cfg.Id);
                                            decimal amt=  Double.valueOf(dis.get('amount'));
                                            if (amt != unlimitedDiscountAmount) {
                                                return '{"status":"500", "message" : "The discounts added to ‘The Individual Plan - Unlimited Calls and Text’ product in each user group much be the same"}';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(Exception exc) {
            System.debug('Inside Catch'+exc); //mpk
            return '{"status":"500", "message":"' + exc +'"}';
        }
        return '';
    }
}