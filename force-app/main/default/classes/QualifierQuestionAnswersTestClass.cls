/******************************************************************************************************
Test Class: QualifierQuestionAnswersTestClass
Class: QuestionnaireControlleredit
Method:QuestionnaireControllereditTest
Description : Test class for  QuestionnaireControlleredit with req no 310.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    2/12/2015             Revan Bhadange                           class created          
*********************************************************************************************************/
@IsTest(SeeAllData=false)
public class QualifierQuestionAnswersTestClass {
    static testMethod void QuestionnaireControllereditTest()
    {	
        test.starttest ();
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Account accnt =  new Account(name='test',AccountNumber='1234');
        insert accnt;
        
        
        
        RecordType RecTyp = [SELECT Id FROM RecordType WHERE DeveloperName =: GlobalConstants.OPPORTUNITY_NONSTANDARD_MCC_RECORDTYPE];  
        Opportunity opp = new Opportunity(AccountId=accnt.Id, Name='opportunityName',RecordTypeId= RecTyp.Id, StageName='Pre-Qualification', CloseDate=Date.today());
        insert opp;
        
        RecordType QuestionaireQueRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireQuestion__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_QUESTION_QUALIFICATION_RECORDTYPE];
        QuestionnaireQuestion__c objQuQue = new QuestionnaireQuestion__c ();
        objQuQue.Question__c = 'Test1';
        objQuQue.Category__c = 'TestCat';
        objQuQue.RecordTypeId = QuestionaireQueRecType.Id;
        objQuQue.Opportunity_Gates__c = 'Make Pursuit';
        objQuQue.Topic_Area__c='Test Topic Area';
        objQuQue.Weighting__c = 12;
        insert objQuQue;
        
        RecordType QuestionaireQueAnsRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireAnswer__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_ANSWER_QUALIFICATION_RECORDTYPE];
        QuestionnaireAnswer__c objQuQA = new QuestionnaireAnswer__c ();
        objQuQA.Questionnaire_Question__c = objQuQue.Id;
        objQuQA.RecordTypeId = QuestionaireQueAnsRecType.Id;
        objQuQA.Answer__c='Yes';
        objQuQA.Score__c = 100;
        insert objQuQA ;
        RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE];
        Questionnaire__c objQustion = new Questionnaire__c ();
        objQustion.Opportunity__c = opp.Id; 
        objQustion.RecordTypeId = QuestionaireRecType.Id;
        insert objQustion;
        
        RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_REPLY_QUALIFICATION_RECORDTYPE];
        QuestionnaireReply__c objQuestionReply = new QuestionnaireReply__c ();
        objQuestionReply.Answer__c = objQuQA.Id;
        objQuestionReply.AnswerValue__c = 'Yes';
        objQuestionReply.Question__c = objQuQue.Id;
        objQuestionReply.Questionnaire__c = objQustion.Id;
        objQuestionReply.RecordTypeId = QuestionaireReplyRecType.Id;
        insert objQuestionReply;
        
        ApexPages.currentPage().getParameters().put('qt', objQustion.id);
        QuestionnaireControlleredit objCls = new QuestionnaireControlleredit ();
        objCls.submitMethod();
        objCls.cancel();
        objCls.close();
        test.stopTest();  
    }  
/******************************************************************************************************
Test Class: QualifierQuestionAnswersTestClass
Class: QuestionnaireControllerview
Method:QuestionnaireControllerviewTest
Description : Test class for  QuestionnaireControllerview with req no 310.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    2/12/2015             Revan Bhadange                           class created          
*********************************************************************************************************/  
     static testMethod void QuestionnaireControllerviewTest()
    {   
        
        test.starttest ();
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Account accnt =  new Account(name='test',AccountNumber='1234');
        insert accnt;
        
        
        
        RecordType RecTyp = [SELECT Id FROM RecordType WHERE DeveloperName =: GlobalConstants.OPPORTUNITY_NONSTANDARD_MCC_RECORDTYPE];  
        Opportunity opp = new Opportunity(AccountId=accnt.Id, Name='opportunityName',RecordTypeId= RecTyp.Id, StageName='Pre-Qualification', CloseDate=Date.today());
        insert opp;
        
        RecordType QuestionaireQueRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireQuestion__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_QUESTION_QUALIFICATION_RECORDTYPE];
        QuestionnaireQuestion__c objQuQue = new QuestionnaireQuestion__c ();
        objQuQue.Question__c = 'Test1';
        objQuQue.Category__c = 'TestCat';
        objQuQue.RecordTypeId = QuestionaireQueRecType.Id;
        objQuQue.Opportunity_Gates__c = 'Make Pursuit';
        objQuQue.Topic_Area__c='Test Topic Area';
        objQuQue.Weighting__c = 12;
        insert objQuQue;
        
        RecordType QuestionaireQueAnsRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireAnswer__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_ANSWER_QUALIFICATION_RECORDTYPE];
        QuestionnaireAnswer__c objQuQA = new QuestionnaireAnswer__c ();
        objQuQA.Questionnaire_Question__c = objQuQue.Id;
        objQuQA.RecordTypeId = QuestionaireQueAnsRecType.Id;
        objQuQA.Answer__c='Yes';
        objQuQA.Score__c = 100;
        insert objQuQA ;
        RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE];
        Questionnaire__c objQustion = new Questionnaire__c ();
        objQustion.Opportunity__c = opp.Id; 
        objQustion.RecordTypeId = QuestionaireRecType.Id;
        insert objQustion;
        
        RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_REPLY_QUALIFICATION_RECORDTYPE];
        QuestionnaireReply__c objQuestionReply = new QuestionnaireReply__c ();
        objQuestionReply.Answer__c = objQuQA.Id;
        objQuestionReply.AnswerValue__c = 'Yes';
        objQuestionReply.Question__c = objQuQue.Id;
        objQuestionReply.Questionnaire__c = objQustion.Id;
        objQuestionReply.RecordTypeId = QuestionaireReplyRecType.Id;
        insert objQuestionReply;
        
        ApexPages.currentPage().getParameters().put('qt', objQustion.id);
        QuestionnaireControllerview  objCls = new QuestionnaireControllerview ();
        objCls.edit();
        test.stopTest();  
    }       
    /******************************************************************************************************
Test Class: QualifierQuestionAnswersTestClass
Class: QuestionnaireController
Method:QuestionnaireControllerinsertTest
Description : Test class for  QuestionnaireController with req no 310.
Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    2/12/2015             Revan Bhadange                           class created          
*********************************************************************************************************/  
     static testMethod void QuestionnaireControllerinsertTest()
    {	
        test.starttest ();
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Account accnt =  new Account(name='test',AccountNumber='1234');
        insert accnt;
        
        
        
                
        RecordType RecTyp = [SELECT Id FROM RecordType WHERE DeveloperName =: GlobalConstants.OPPORTUNITY_NONSTANDARD_MCC_RECORDTYPE];  
        Opportunity opp = new Opportunity(AccountId=accnt.Id, Name='opportunityName',RecordTypeId= RecTyp.Id, StageName='Pre-Qualification', CloseDate=Date.today());
        insert opp;
        
        /*opp.StageName = 'Qualification';
        opp.JA_Code__c = 'JA123';
        opp.Seed_Corn_funding__c =  2000;
        opp.Make_Pursuit_Pass__c = true;
        opp.Make_Pursuit_Pass_Date__c=date.today();
        update opp;*/
        
        
        list<QuestionnaireQuestion__c > qustList = new list<QuestionnaireQuestion__c >();
        RecordType QuestionaireQueRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireQuestion__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_QUESTION_QUALIFICATION_RECORDTYPE];
        QuestionnaireQuestion__c objQuQue = new QuestionnaireQuestion__c ();
        objQuQue.Question__c = 'Test1';
        objQuQue.Category__c = 'TestCat';
        objQuQue.RecordTypeId = QuestionaireQueRecType.Id;
        objQuQue.Opportunity_Gates__c = 'Make Pursuit';
        objQuQue.Topic_Area__c='Test Topic Area';
        objQuQue.Weighting__c = 12;
        qustList.add(objQuQue); 
        
        QuestionnaireQuestion__c objQuQue1 = new QuestionnaireQuestion__c ();
        objQuQue1.Question__c = 'Test1';
        objQuQue1.Category__c = 'TestCat';
        objQuQue1.RecordTypeId = QuestionaireQueRecType.Id;
        objQuQue1.Opportunity_Gates__c = 'Make Pursuit';
        objQuQue1.Topic_Area__c='Test Topic Area';
        objQuQue1.Weighting__c = 12;
         qustList.add(objQuQue1);
        insert qustList;
        
        RecordType QuestionaireQueAnsRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireAnswer__c' AND 
                                            DeveloperName =: GlobalConstants.QUESTIONARRE_ANSWER_QUALIFICATION_RECORDTYPE];
        QuestionnaireAnswer__c objQuQA = new QuestionnaireAnswer__c ();
        objQuQA.Questionnaire_Question__c = objQuQue.Id;
        objQuQA.RecordTypeId = QuestionaireQueAnsRecType.Id;
        objQuQA.Answer__c='Yes';
        objQuQA.Score__c = 100;
        objQuQA.Order__c=1;
        insert objQuQA ;
        RecordType QuestionaireRecType = [SELECT Id FROM RecordType WHERE sObjectType='Questionnaire__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE];
        Questionnaire__c objQustion = new Questionnaire__c ();
        objQustion.Opportunity__c = opp.Id; 
        objQustion.RecordTypeId = QuestionaireRecType.Id;
        insert objQustion;
        
        RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                                          DeveloperName =: GlobalConstants.QUESTIONARRE_REPLY_QUALIFICATION_RECORDTYPE];
        QuestionnaireReply__c objQuestionReply = new QuestionnaireReply__c ();
        objQuestionReply.Answer__c = objQuQA.Id;
        objQuestionReply.AnswerValue__c = 'Yes';
        objQuestionReply.Question__c = objQuQue.Id;
        objQuestionReply.Questionnaire__c = objQustion.Id;
        objQuestionReply.RecordTypeId = QuestionaireReplyRecType.Id;
        insert objQuestionReply;
        
        ApexPages.currentPage().getParameters().put('opp', opp.id);
        QuestionnaireController  objCls = new QuestionnaireController ();
        objCls.submitMethod();
        objCls.Counter = 1;
        objCls.submitMethod();
        objCls.cancel();
        objCls.close();
        test.stopTest();  
    }                             
}