/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_EvEV_Helper {

    static testMethod void myUnitTest() {
        
        Account acct = new Account(name='test account');
        insert acct;
        Opportunity op = new Opportunity(AccountId=acct.Id,StageName='Prospecting',Brand__c='',name='test Opportunity',CloseDate=date.newinstance(2009,6,6));
        insert op;
       //Account account = (Account)SmartFactory.createSObject('Account');
        
        Pricebook2 pBook = [select id from Pricebook2 where IsStandard = TRUE limit 1];        
        
        List<Product2> products = new List<Product2>();
        Product2 prod1 = new Product2(Name='Voice', isactive=true,Brand__c='Orange');        
        Product2 prod2 = new Product2(Name='Blackberry Data Only', isactive=true,Brand__c='Orange');     
        Product2 prod3 = new Product2(Name='Tablet Data Only', isactive=true,Brand__c='Orange');      
        Product2 prod4 = new Product2(Name='Mobile Broadband Data Only', isactive=true,Brand__c='Orange');
        Product2 prod5 = new Product2(Name='Fleetlink', isactive=true,Brand__c='Orange');      
        Product2 prod6 = new Product2(Name='M2M', isactive=true,Brand__c='Orange');
        
        products.add(prod1);
        products.add(prod2);
        products.add(prod3);
        products.add(prod4);
        products.add(prod5);
        products.add(prod6);
        
        insert products ;
        
        List<PricebookEntry> PricebookEntries = new List<PricebookEntry>();
        PricebookEntry PricBkEty1 = new PricebookEntry(Product2Id=products[0].Id,Pricebook2Id=pBook.Id,UnitPrice=2.0,IsActive=true);
        PricebookEntry PricBkEty2 = new PricebookEntry(Product2Id=products[1].Id,Pricebook2Id=pBook.Id,UnitPrice=2.0,IsActive=true);
        PricebookEntry PricBkEty3 = new PricebookEntry(Product2Id=products[2].Id,Pricebook2Id=pBook.Id,UnitPrice=2.0,IsActive=true);
        PricebookEntry PricBkEty4 = new PricebookEntry(Product2Id=products[3].Id,Pricebook2Id=pBook.Id,UnitPrice=2.0,IsActive=true);
        PricebookEntry PricBkEty5 = new PricebookEntry(Product2Id=products[4].Id,Pricebook2Id=pBook.Id,UnitPrice=2.0,IsActive=true);
        PricebookEntry PricBkEty6 = new PricebookEntry(Product2Id=products[5].Id,Pricebook2Id=pBook.Id,UnitPrice=2.0,IsActive=true);
        PricebookEntries.add(PricBkEty1);
        PricebookEntries.add(PricBkEty2);
        PricebookEntries.add(PricBkEty3);
        PricebookEntries.add(PricBkEty4);
        PricebookEntries.add(PricBkEty5);
        PricebookEntries.add(PricBkEty6);
        
        insert PricebookEntries;               
        
        
        List<String> lstStrings=new List<String>{null,'','Test'};
        Test.startTest();
        
        //Testing isBlank()
        System.assert(EvEV_Helper.isBlank(lstStrings[0]));
        System.assert(EvEV_Helper.isBlank(lstStrings[1]));
        System.assert(EvEV_Helper.isBlank(lstStrings[2])==false);
        
        //Testing isListEmpty()
        List<Artemis_Deal__c> lstDeals=new List<Artemis_Deal__c>(); 
        System.assert(EvEV_Helper.isListEmpty(lstDeals)); 
        Artemis_Deal__c deal= new Artemis_Deal__c(Brand__c='Orange',Opportunity__c=op.id,Primary_Deal__c=true,Process_Status__c='Processed',Voice_Users__c=10,Blackberry_DO_Users__c=20,Mobile_Broadband_DO_Users__c=30,Tablet_DO_Users__c=40,Fleetlink_Users__c=50,Total_M2M_Users__c=60);
        //Artemis_Deal__c deal= new Artemis_Deal__c(Opportunity__c=op.id,Primary_Deal__c=true,Process_Status__c='Processed',Voice_Users__c=10,Blackberry_DO_Users__c=20,Mobile_Broadband_DO_Users__c=30,Tablet_DO_Users__c=40,Fleetlink_Users__c=50,Total_M2M_Users__c=60);
        deal.Mobile_Outgoing_Voice_Revenue__c=100;
        deal.Data_Only_Blackberry_Revenue__c=200;
        deal.Data_Only_Mobile_Broadband_Revenue__c=300;
        deal.Data_Only_Tablet_Revenue__c=400;
        deal.Fleetlink_Revenue__c=500;
        deal.M2M_Revenue__c=600;
        insert deal;
        List<OpportunityLineItem> lstOldOlIs=[Select id from OpportunityLineItem where OpportunityId=:deal.Opportunity__c and Related_To_Primary_Deal__c=true];
        //System.assert(lstOldOlIs.size()==6);
        lstDeals.add(deal);
        System.assert(EvEV_Helper.isListEmpty(lstDeals)==false);
        Test.stopTest();
        }
}