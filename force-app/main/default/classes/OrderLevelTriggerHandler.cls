public without sharing class OrderLevelTriggerHandler {

    // Member Variables
    private Map<Id, Order_Level__c> mMapOrderLevelNew;
    private Map<Id, Order_Level__c> mMapOrderLevelOld;
    private List<Order_Level__c> mListOrderLevelNew;
    private List<Order_Level__c> mListOrderLevelOld;
    private Integer miTriggerSize;
    private Boolean mbIsInsert;
    private Boolean mbIsUpdate;
    private Boolean mbIsBefore;
    private Boolean mbIsAfter;
    private Boolean mbIsDelete;

    // Class Variables
    private static Boolean cbIsExecuting;
    //@TestVisible 
    private static Boolean cbIsFirstRun = true;
    
    // Constructor
    public OrderLevelTriggerHandler(  Map<Id, Order_Level__c> pMapOrderLevelNew, Map<Id, Order_Level__c> pMapOrderLevelOld, 
    List<Order_Level__c> pListOrderLevelNew, List<Order_Level__c> pListOrderLevelOld,
                                Integer piTriggerSize,
                                Boolean pbIsInsert, Boolean pbIsUpdate, Boolean pbIsBefore, Boolean pbIsAfter,
                                Boolean pbIsExecuting,
                                Boolean pbIsDelete ){

        // Assign Member Variables  
        mMapOrderLevelNew = pMapOrderLevelNew;
        mMapOrderLevelOld = pMapOrderLevelOld;
        mListOrderLevelNew = pListOrderLevelNew;
        mListOrderLevelOld = pListOrderLevelOld;
        miTriggerSize   = piTriggerSize;
        mbIsInsert      = pbIsInsert;
        mbIsUpdate      = pbIsUpdate;
        mbIsBefore      = pbIsBefore;
        mbIsAfter       = pbIsAfter;
        mbIsDelete		= pbIsDelete;

        // Assign Class Variables
        cbIsExecuting   = pbIsExecuting;    
                                    
    }


   /*
    * @name         processTrigger
    * @description  entry point for the OrderLevel trigger to process the OrderLevel trigger class
    * @author       P Goodey
    * @date         Nov 2014
    * @see 
    */
    public void processTrigger() {

        // Process After Trigger       
        if(mbIsAfter){

	        system.debug('**********' + ' processTrigger() mbIsAfter');

	        // Prevent the trigger from re-entrency
	        if(cbIsFirstRun){
	        	
	        	system.debug('**********' + ' processTrigger() cbIsFirstRun');

				// This routine will not bulk update all roll up fields from previous Order Levels so we need to stop the user from changing the Product Order Type
				// otherwise the counts for the old Product Order Type will incorrectly remain set
				if( mbIsUpdate ){
					for (Order_Level__c ol : mListOrderLevelNew){
						if(ol.Product_Order_Type__c != mMapOrderLevelOld.get(ol.Id).Product_Order_Type__c ){
							// An attempt to change the product order is being made
							ol.addError(Label.Order_Level_Update_Product_Order_Type_Message);
						}
					}
				}
	        				
				// Rollup Summary to the Company Record
				// Get the Company Rollups fields to update
 				Map<String, OrderLevel_RollupSettings__c> orAllRollups = OrderLevel_RollupSettings__c.getAll();
        		for(OrderLevel_RollupSettings__c orRollup : orAllRollups.values()) {
					// Check the custom setting to see if the rollup is required and has been activated        		
        			if(orRollup.Activate_Rollup_Field_on_Company__c == true){
	        			// Create or update the Rollups
						setOrderLevelRollupsOnCompanyRecord(
							orRollup.Product_Order_Type__c, //'Welcome Day | Chargeable Service',
							orRollup.Company_Rollup_Field_Name__c //'Welcome_Days_Remaining_Chargeable_LRE__c'
						);
        			}
        		}

		        // Set our static to prevent transaction re-entrancy
		        cbIsFirstRun = false;
	        }
	    
	    // Before Trigger            
        }else{
        	
        	// Update
        	if( mbIsUpdate ){

				// Cascading delete from grandparent - we have to use code to delete this order level
				//deleteOrderLevelCascadeDeletefromOrder();
					
        	}
        }
    }
    


   /*
    * @name         deleteOrderLevelCascadeDeletefromOrder
    * @description  This deletes the Order Level if there is a cascade delete from grandparent level (ie from the order)
    * @author       P Goodey
    * @date         Jan 2015
    * @see 
    */

/*    
    private void deleteOrderLevelCascadeDeletefromOrder() {  

     	List<Order_Level__c> lListOrderLevel = new List<Order_Level__c>();   
		for (Order_Level__c ol : mListOrderLevelNew){
			
			// Cascading delete from grandparent - we have to use code to delete this order level
			
			// if the Order Request link is being removed
			if( (mMapOrderLevelOld.get(ol.Id).Order_Request__c != null) && (ol.Order_Request__c == null) &&		
				(ol.System_Order_Item__c != null) ){
				lListOrderLevel.add(ol);
			}
		}
		delete lListOrderLevel;
    }
*/


   /*
    * @name         setOrderLevelRollupsOnCompanyRecord
    * @description  This sets the Order Level roll up summaries on the Company record
    * 				It uses the LREngine code from tgerm.com 
    				see https://github.com/abhinavguptas/Salesforce-Lookup-Rollup-Summaries
    * @author       P Goodey
    * @date         Nov 2014
    * @see 
    */ 
    private void setOrderLevelRollupsOnCompanyRecord(string psProductOrderType, string psCompanyField) {  

		Schema.DescribeSObjectResult drSObj1 = Schema.sObjectType.Account;
		Map<String,Schema.SObjectField> mapFields = drSObj1.fields.getMap();
		Schema.SObjectField tField1 = mapFields.get(psCompanyField);
		Schema.DescribeFieldResult drField1 = tField1.getDescribe();

     	List<Order_Level__c> lListOrderLevel = new List<Order_Level__c>();
        system.debug('**********' + ' entering setOrderLevelRollupsOnCompanyRecord');    
     
		if( mbIsDelete == true ){
			lListOrderLevel = mListOrderLevelOld;
		}else{

		 	// Check if update that the Quantity has changed
		 	for( Order_Level__c ol : mListOrderLevelNew ){
		 		if( (string)ol.get('Product_Order_Type__c') == psProductOrderType){
		 			
		 			// Only update if changed quantity or Quantity to be summed flad is changed
		 			if( mbIsUpdate == true ){
			 			if( ((Double)ol.get('Quantity__c') != mMapOrderLevelOld.get(ol.Id).Quantity__c )  ||
			 				(ol.get('Is_Quantity_To_Be_Summed__c') != mMapOrderLevelOld.get(ol.Id).Is_Quantity_To_Be_Summed__c ) ){
			 				lListOrderLevel.add(ol);		 				
			 			}		 				
		 			}else{
		 				lListOrderLevel.add(ol);
		 			}
		 		}
		 	}
		}
     
     	// Build the Filter for the Order Level Picklist value
    	string lsFilter = 'Product_Order_Type__c = \'' + psProductOrderType + '\'';
    	
    	// Only include Orders that are to be summed (this excludes orders that are cancelled and marked with the False)
    	lsFilter+= ' AND Is_Quantity_To_Be_Summed__c = True';
    
     	// Step 1 - Create context for LREngine
		LREngine.Context ctx = new LREngine.Context(Account.SobjectType, // parent object
			Order_Level__c.SobjectType,  // child object
			Schema.SObjectType.Order_Level__c.fields.Company__c, // relationship field name
			lsFilter
		);
		
		// Step 2 - Add Rollup Fields
		ctx.add(
			new LREngine.RollupSummaryField(
				drField1, // This is for example: Schema.SObjectType.Account.fields.Welcome_Days_Remaining_Chargeable__c
				Schema.SObjectType.Order_Level__c.fields.Quantity__c,
				LREngine.RollupOperation.Sum // Choosing to sum the values
		)); 
		

      	// Calling rollup method returns in memory master objects with aggregated values in them. 
		Sobject[] masters = LREngine.rollUp(ctx, lListOrderLevel);    
		
		// Persiste the changes in master
		update masters;  		  

    }

}