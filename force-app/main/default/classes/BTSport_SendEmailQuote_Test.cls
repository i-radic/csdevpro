@isTest
private class BTSport_SendEmailQuote_Test {

     static testMethod void myUnitTest() {
     
        Test_Factory.setProperty('IsTest', 'yes');
        StaticVariables.setContactIsRunBefore(False);     
        
        RecordType BTSOppyType = [select id from RecordType where SobjectType='Opportunity' and name ='BT Sport' limit 1];
        RecordType BTSHeaderType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='Header' limit 1];
        
        Date uDate = date.today().addDays(7);
        
        
         User thisUser = [select id from User where id=:userinfo.getUserid()];
		 System.runAs( thisUser ){
    
     	 TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }

        Account acc = Test_Factory.CreateAccount();
        acc.name = 'TESTCODE 2';
        acc.sac_code__c = 'testSAC';
        acc.Sector_code__c = 'CORP';
        acc.LOB_Code__c = 'LOB';
        acc.OwnerId = thisUser.Id;
        acc.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(acc);
                
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BTS';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.RecordTypeId = BTSOppyType.Id;
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});
        
        Contact cnt = Test_Factory.CreateContact();
        cnt.AccountCUG__c = accountResult.Id;
        cnt.Email = 'test@test.com';
        cnt.AccountId = accountResult.Id;
        cnt.SAC_Code__c = acc.SAC_Code__c;
        Database.SaveResult[] cntResult = Database.insert(new Contact [] {cnt});
        
        Lead l = New Lead();
        l.Title = acc.Name;
        l.LastName = 'Landscape Lead: ' + acc.Name;
        l.Company = acc.Name;
        l.Account__c = cnt.AccountCUG__c;
        l.Contact__c = cnt.Id;
        l.Email = 'test@test.com';
        l.Description = 'Test Landscape to Lead';      
        insert l;
        
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cntResult[0].Id;
        ocr.OpportunityId = oppResult[0].Id;
        ocr.Role = 'Deleloper';
        Database.SaveResult[] ocrResult = Database.insert(new OpportunityContactRole [] {ocr});   
              
        BT_Sport__c Header1 = new BT_Sport__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Header1.RecordTypeId = BTSHeaderType.id;
        Header1.BT_Sport_MSA_Oppy__c = 'No';
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport__c [] {Header1});
        
        
        BTSport_SendEmailQuote.SENDMAIL(Header1Result[0].Id, l.Id, 'Lead');
        BTSport_SendEmailQuote.SENDMAIL(Header1Result[0].Id, oppResult[0].Id, 'Opportunity');
        
     
     }

}