global class BatchLS2L_Schedule06 implements Schedulable{
        
global void execute(SchedulableContext sc) {
    //batch for email
    BatchLS2L_06ZeroLeads lCountD = new BatchLS2L_06ZeroLeads();
    lCountD.query = 'SELECT Id, Email, Name, zLeadsOwned__c FROM User WHERE isActive = TRUE';
    database.executeBatch(lCountD, 1);
}      
}