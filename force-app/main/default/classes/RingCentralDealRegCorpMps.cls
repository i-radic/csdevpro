public class RingCentralDealRegCorpMps {

    public String uL {get; set;}
    public String accName {get; set;}
    public String rcDealManID {get; set;}
    public String conId {get; set;}
    public String partnerAgentName {get; set;}
    public String partnerAgentID {get; set;}
    public String company {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String phone {get; set;}
    public String numberOfEmployees {get; set;}
    public String zipCode {get; set;}
    public RingCentralAPI.DealRegResponse dealRegResponse {get; set;}
    public String btCugNumber {get; set;}

    public PageReference SetValues(){
        uL = System.currentPageReference().getParameters().get('users'); 
        accName = System.currentPageReference().getParameters().get('accName'); 
        conId = System.currentPageReference().getParameters().get('conId');
        rcDealManID = System.currentPageReference().getParameters().get('rcDealManID');
        btCugNumber ='';
        
        String userId = UserInfo.getUserId();
        User user = [select ein__c from User where Id =: userId];
        Contact contact = [select firstName, lastname, email, phone from Contact where Id =: conId];
        
        partnerAgentName = Userinfo.getName();  
        partnerAgentID = user.ein__c;
        company = accName;
        firstName = contact.firstName;
        lastName = contact .lastName;
        email = contact.email;
        phone = contact.phone;
        numberOfEmployees = uL;
        
        return SubmitDealRegistration();
    }

    public PageReference SubmitDealRegistration()
    {
        RingCentralURL__c RSUrl = null;
        if (Test.isRunningTest()){   
            RSUrl = [select Name, Username__c, Password__c, Url__c, Partner_Login_Url__c from RingCentralURL__c where Name = 'testRCurl'];
        }else{
            RSUrl = [select Name, Username__c, Password__c, Url__c, Partner_Login_Url__c from RingCentralURL__c where Name = 'DealRegistration'];
        }
        String url = String.valueOf(RSUrl.Url__c);
        String partnerLoginUrl = String.valueOf(RSUrl.Partner_Login_Url__c);
        String username = String.valueOf(RSUrl.Username__c);
        String password = String.valueOf(RSUrl.Password__c);
        RingCentralAPI api = new RingCentralAPI();
        RingCentralAPI.SessionHeader_element sessionHeader = new RingCentralAPI.SessionHeader_element();
        RingCentralAPI.DealRegistrationSubmissionCls webSvc = new RingCentralAPI.DealRegistrationSubmissionCls(url);
        
        if (Test.isRunningTest()){       
            sessionHeader.sessionId = 'TEST';
        }
        else{
            String sessionId = api.Login(username, password, partnerLoginUrl);       
            sessionHeader.sessionId = sessionId;
        }
        webSvc.timeout_x = 120000;
        webSvc.SessionHeader = sessionHeader;
        //Commented this line as the API is not yet live 24/07/18
        ////dealRegResponse = webSvc.submitDealRegistration(partnerAgentName, partnerAgentID, company, firstName,lastName, email, phone, numberOfEmployees, zipCode, btCugNumber);  
        dealRegResponse = webSvc.submitDealRegistration(partnerAgentName, partnerAgentID, company, firstName,lastName, email, phone, numberOfEmployees, zipCode);   
 

        PageReference retPg = null;       
        retPg = page.RingCentralDealReg;
        AddPageRefParams(retPg);
        System.debug('dealRegResponse.partnerDealID======='+dealRegResponse.partnerDealID);
        System.debug('rcDealManID======='+rcDealManID);
        if(dealRegResponse.status.Contains('Failure') || dealRegResponse.approvalStatus.Contains('Rejected'))
        {
            CloudPhone_RC_Deal_Management__c rcDM = [select RCDeal_ID__c,RC_Approval_Status__c from  CloudPhone_RC_Deal_Management__c where Id =:rcDealManID];
            rcDM.RCDeal_ID__c = dealRegResponse.partnerDealID;
            rcDM.RC_Approval_Status__c= dealRegResponse.approvalStatus;
            update rcDM;  
            
            AddPageRefParams(retPg);
        }
        else
        {
            //--------Store RCDeal_ID
             CloudPhone_RC_Deal_Management__c rcDM = [select RCDeal_ID__c,RC_Approval_Status__c,RC_Valid_Until_Date__c from  CloudPhone_RC_Deal_Management__c where Id =: rcDealManID];
            rcDM.RCDeal_ID__c = dealRegResponse.partnerDealID;
            rcDM.RC_Approval_Status__c=dealRegResponse.approvalStatus;
            rcDm.RC_Valid_Until_Date__c=dealRegResponse.validUntilDate;
            update rcDM;  
        }
        return null;
        //return retPg.setRedirect(true);
    }   
    
    private PageReference AddPageRefParams(PageReference retPg){
        retPg.getParameters().put('users',uL);       
        retPg.getParameters().put('accName',accName);  
        retPg.getParameters().put('conID',conId);
        retPg.getParameters().put('status',dealRegResponse.status);
        retPg.getParameters().put('errorResp',dealRegResponse.errorResp);
        retPg.getParameters().put('approvalStatus',dealRegResponse.approvalStatus);
        retPg.getParameters().put('partnerDealID',dealRegResponse.partnerDealID);
        retPg.getParameters().put('validUntilDate',dealRegResponse.validUntilDate);
        return retPg;
    }
    public PageReference Back()
    {
        Id pageid = rcDealManID;
        Pagereference ref = new Pagereference('/'+pageid);
        ref.setRedirect(true);
        return ref;
    
    }
}