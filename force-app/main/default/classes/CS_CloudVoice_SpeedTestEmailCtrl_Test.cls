@IsTest(SeeAllData=true)
private class CS_CloudVoice_SpeedTestEmailCtrl_Test { 

	static Account account {get; set;}
    static Contact contact {get; set;}
	static cscfga__Product_Basket__c prodBasket {get; set;}

	static void SetVariables(){
		account = Test_Factory.CreateAccount();     
        account.CUG__c = 'cugCV1';
        account.OwnerId = UserInfo.GetUserId() ;
        Database.SaveResult aR = Database.insert(account);     
                     
        contact = Test_Factory.CreateContact();
        contact.Cug__c = 'cugCV1';
		contact.FirstName = 'TEST';
		contact.LastName = 'TEST';
        contact.Contact_Post_Code__c = 'WR5 3RL';
        contact.Email = 'test@email.com';
        insert contact;       	

		prodBasket = new cscfga__Product_Basket__c(csbb__Account__c = account.Id, Customer_Contact__c = contact.Id);
		insert prodBasket;
	}

	static testMethod void UnitTest() {  
        SetVariables(); 
		PageReference page = Page.CS_CloudVoice_SpeedTestEmail;
		Test.setCurrentPage(page);		
		ApexPages.currentPage().getParameters().put('basketId', prodBasket.id);
		ApexPages.currentPage().getParameters().put('accessType','Existing');
		CS_CloudVoice_SpeedTestEmailCtrl ctrl = new CS_CloudVoice_SpeedTestEmailCtrl();
		ctrl.sendEmail();
	}

	static testMethod void UnitTest2() {  
        SetVariables(); 
		PageReference page = Page.CS_CloudVoice_SpeedTestEmail;
		Test.setCurrentPage(page);
		ApexPages.currentPage().getParameters().put('basketId', prodBasket.id);
		ApexPages.currentPage().getParameters().put('accessType','OTT');
		CS_CloudVoice_SpeedTestEmailCtrl ctrl = new CS_CloudVoice_SpeedTestEmailCtrl();
		ctrl.sendEmail();
	}

	static testMethod void UnitTest3() {  
        SetVariables(); 
		PageReference page = Page.CS_CloudVoice_SpeedTestEmail;
		Test.setCurrentPage(page);
		ApexPages.currentPage().getParameters().put('basketId', prodBasket.id);
		ApexPages.currentPage().getParameters().put('accessType','');
		CS_CloudVoice_SpeedTestEmailCtrl ctrl = new CS_CloudVoice_SpeedTestEmailCtrl();
		ctrl.sendEmail();
	}
}