global class CS_UsageProfileLookup extends cscfga.ALookupSearch {

	public override String getRequiredAttributes(){
		return '["Product Basket Id", "Service Plan Level"]'; 
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
    	
    	List<Usage_Profile__c> usageProfile = null;
    	List<Attachment> attachments = null;
    	List<cspmb__Price_Item__c> priceItems = null;
    	String upFields = getSobjectFields('Usage_Profile__c');

    	attachments = [SELECT Id, Name, Body FROM Attachment WHERE ParentId = :searchFields.get('Product Basket Id') AND Name = 'UsageProfile'];

    	if(attachments != null && !attachments.isEmpty()){

				String Usage_Profile_JSON = attachments.get(0).Body.toString();
				usageProfile = (List<Usage_Profile__c>) JSON.deserialize(Usage_Profile_JSON, List<Usage_Profile__c>.class);
                usageProfile.get(0).Name = 'Default';
    	

    	} else {

    		priceItems = [SELECT Id, Name, Default_Usage_Profile__c FROM cspmb__Price_Item__c WHERE Id = :searchFields.get('Service Plan Level')];

    		if(priceItems != null && !priceItems.isEmpty()){
    			
    			String upID = priceItems.get(0).Default_Usage_Profile__c;
    			String query = 'SELECT ' + upFields  + ' FROM Usage_Profile__c WHERE Id = :upID';
    			usageProfile = Database.query(query);
    		}

    	}

    	return usageProfile;
    }

    public String getSobjectFields(String so) {
        String fieldString;
   
        SObjectType sot = Schema.getGlobalDescribe().get(so);
        if (sot == null) return null;
   
        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();
       
        fieldString = fields[0].getDescribe().LocalName;
        for (Integer i = 1; i < fields.size(); i++) {
            fieldString += ',' + fields[i].getDescribe().LocalName;
        }
        return fieldString;
    }
}