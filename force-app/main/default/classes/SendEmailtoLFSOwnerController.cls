Public class SendEmailtoLFSOwnerController {

       public Boolean STList=False,SEButton=False; 
       public List<categoryWrapper> searchResults {set;get;}
       public List<categoryWrapper> selectedCategories=new List<categoryWrapper>();
    
       public class CategoryWrapper {
 
            public Boolean checked{ get; set; }
            public OpportunityTeamMember OppTM { get; set;} 
            
            public CategoryWrapper(OpportunityTeamMember  O){
                OppTM = O;
                checked = false;
            }
       } 
  
          String OpId;
          List<OpportunityTeamMember> OTM;
          List<OpportunityTeamMember> SelectedOTM=new List<OpportunityTeamMember>();

          Public SendEmailtoLFSOwnerController(){
            OpId=Apexpages.currentPage().getParameters().get('OppId');
          }
  
          Public String EmailList{get; set;} 
  
          public List<SelectOption> getEmailItems() {
                List<SelectOption> options = new List<SelectOption>();
                options.add(new SelectOption('None','-- None --'));
                options.add(new SelectOption('Oppty Owner','Opportunity Owner'));
                options.add(new SelectOption('Sales Team','Sales Team'));
                return options;
          }     
    
          public PageReference getSelected(){  
              SelectedOTM.clear();
              SEButton=False;
              for(categoryWrapper SR:searchResults) {
              If(SR.checked==true){
                   SEButton=True;
                   SelectedOTM.add(SR.OppTM);
                }
              }        
           return null;
         }  
    
    
          Map<Id,String> EmailArray=new Map<Id,String>();
              public PageReference incrementQ1() { 
                  SEButton=False;  
                  If(EmailList=='Sales Team'){
                      searchResults=new List<categoryWrapper>();
                      OTM=[Select Id,OpportunityId,UserId,User.Email,TeamMemberRole FROM OpportunityTeamMember where OpportunityId=:OpId];  
  
                    for(OpportunityTeamMember  c : OTM) {
                         EmailArray.put(C.UserId,c.User.Email);
                         CategoryWrapper cw = new CategoryWrapper(c);     
                         searchResults.add(cw);
                    }
                    STList=True;  
   
                  }Else If(EmailList=='Oppty Owner'){
                      STList=False; 
                      SEButton=True; 
                  }  Else {
                    STList=False; 
                    SEButton=False; 
                  }   
   
                  return null;
                 }
 

             Public Boolean getSalesTeamList(){
               Return STList;
              } 
   
             Public Boolean getsendEmailButton(){
               Return SEButton;
              }  
  
            public PageReference SendEmail() {
            
            System.debug('AAAAAAAAAAAAAAAAAAAAAAAAA'); 
              Try{  
                  List<String> Address=new List<String>();  
                  Opportunity Op=[Select Name,Owner.Email, (Select Title,Body From Notes Order By CreatedDate Desc Limit 1) From Opportunity where Id=:Opid];
                   If(EmailList=='Sales Team'){
                        for(OpportunityTeamMember SC:SelectedOTM){
                          Address.add(EmailArray.get(SC.UserId));  
                         }
                         Address.add(Op.Owner.Email);
                   }  Else     Address.add(Op.Owner.Email);
   
                   If(Op.Notes.size()>0){   
                     system.debug('BBBBBBBBBBBBBBBBB'+Op.Notes[0].Body);
                     String Bod=Op.Notes[0].Body;
 
                   //create a mail object to send a single email.
                     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                     mail.setToAddresses(Address);
                     system.debug('BBBBBBBBBBBBBBBBB'+Address);
                     mail.setSubject('Latest Notes of Opportunity: '+Op.Name);
                     mail.setPlainTextBody('Opportunity Name: '+Op.Name +'\n\nLatest Notes:\n'+ Op.Notes[0].Body + '\n\nTo view this record please click on the following URL:\n'+ 'https://emea.salesforce.com/'+ Op.Id );

                     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail } );
                  }
                    Return Null;
    
                  }  
                  Catch(Exception e){return null;} 
              }
              /*
              private string getSFInstance() {
                 String baseurl= URL.getSalesforceBaseUrl().toExternalForm();
                 String instancename;
                 Integer pos1 = baseurl.indexOf('.');
                 Integer pos2 = baseurl.indexOf('.', pos1+1);
                 if(pos1 != -1 && pos2 != -1)
                 instancename= baseurl.substring(pos1+1, pos2);
                 return instancename;  
                }
                */
}