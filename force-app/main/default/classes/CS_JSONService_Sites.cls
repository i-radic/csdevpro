/**
 * @name CS_JSONService_Sites
 * @description Base service for Sites
 * @revision
 *
 */
public class CS_JSONService_Sites extends CS_JSONService_Products {
    /**
     * Class Constructor
     * @return  CS_JSONService_Sites
     */
    public CS_JSONService_Sites() {
        super();
    }
    
    @TestVisible
    protected override Object getValues(Map<Id, ProductConfiguration> configs) {
        Map<String, Site> sites = getSites(configs);
         System.debug('configs>>>'+configs);
        List<Object> result = new List<Object>();
        //CS_JSON_Schema.CS_JSON_Object siteSpec = this.setting.getDefinition(CS_JSON_Constants.Site_SpecificationName);
        //System.debug('siteSpec>>>'+siteSpec);
        if (sites != null){
            for(Site site : sites.values()) {
                System.debug('site>>>'+site);
                System.debug('this.setting>>>'+JSON.serializePretty(this.setting));
                CS_JSON_Schema.CS_JSON_Object siteSpec = this.setting.getDefinition(site.Name);
                System.debug('siteSpec>>>'+siteSpec);
                 Map<String, Object> siteObj;
                if(siteSpec != null){ 
                    if(!test.isRunningTest()){
                        siteObj = getValues(siteSpec, CS_Utl_Object.getUntyped(site));
                    }
                     System.debug('siteObj>>>'+siteObj);
                     List<Object> siteProducts = new List<Object>();
                     for(ProductConfiguration cfg : site.configs) {
                         //System.debug('getDefinition 11>>>'+JSON.serializepretty(this.setting.definitions));
                        CS_JSON_Schema.CS_JSON_Object spec = siteSpec;
                        //CS_JSON_Schema.CS_JSON_Object spec = this.setting.getDefinition(site.Name);
                        //System.debug('spec>>>'+JSON.serializePretty(spec));
                        //if(spec != null) {
                            Map<String, Object> cfgObj = new Map<String, Object>();
                            cfgObj = setConfig(cfg, spec);
                            
                            // add all the related object field to the site defination
                            if(spec.relatedChild != null && !spec.relatedChild.isEmpty()){
                                for(CS_JSON_Schema.CS_JSON_Object obj : spec.relatedChild) {
                                    System.debug('spec.relatedChild>>>'+JSON.serializePretty(spec.relatedChild));
                                    Id parentId = String.valueOf(cfg.config.get(obj.parentField));
                                    System.debug('parentId>>>'+parentId);
                                    List<SObject> relatedRecords = Database.query(getQuery(obj, parentId));
                                     System.debug('relatedRecords>>>'+JSON.serializePretty(relatedRecords));
                                    if(!relatedRecords.isEmpty()) {
                                        //siteProducts.add(getValues(obj, relatedRecords.get(0)));
                                        System.debug('cfgObj 111>>>'+cfgObj);
                                        System.debug('spec.relatedList>>>'+spec.relatedList);
                                        Object relatedFields = getValues(obj, relatedRecords.get(0));
                                        System.debug('relatedFields>>>'+relatedFields);
                                        cfgObj.putAll(getValues(obj, relatedRecords.get(0)));
                                        //System.debug('relatedProducts>>>'+relatedProducts);
                                    }
                                }
                            }
                            System.debug('cfgObj>>>'+cfgObj);
                            //siteProducts.add(cfgObj);
                            siteObj.putAll(cfgObj);
                            //add siteProducts fields to siteProduct elated list
                            Map<String, Object> siteProductFiledMap = new Map<String, Object>();
                            if(spec.siteProducts != null && !spec.siteProducts.isEmpty()){
                                for(CS_JSON_Schema.CS_JSON_Object obj : spec.siteProducts) {
                                    System.debug('spec.siteProducts>>>'+JSON.serializePretty(spec.siteProducts));
                                    Id parentId = String.valueOf(cfg.config.get(obj.parentField));
                                    System.debug('parentId>>>'+parentId);
                                    List<SObject> relatedRecords = Database.query(getQuery(obj, parentId));
                                    System.debug('relatedRecords>>>'+JSON.serializePretty(relatedRecords));
                                    if(!relatedRecords.isEmpty()) {
                                        Object relatedFields = getValues(obj, relatedRecords.get(0));
                                        System.debug('relatedFields>>>'+relatedFields);
                                        siteProductFiledMap.putAll(getValues(obj, relatedRecords.get(0)));
                                        //System.debug('relatedProducts>>>'+relatedProducts);
                                    }
                                }
                            }
                            siteProducts.add(siteProductFiledMap);
                            
                    //  }
                    }
                    System.debug('siteProducts >>>'+siteProducts);
                    siteObj.put(siteSpec.relatedList, siteProducts);
                    System.debug('siteObj>>>'+siteObj);
                    result.add(siteObj);
                }
            }
            
        }
            
        return result;
    }
    
//  @TestVisible
//  protected override Object getValues_OLD(Map<Id, ProductConfiguration> configs) {
   /* public Object getValues_OLD(Map<Id, ProductConfiguration> configs) {
        Map<String, Site> sites = getSites(configs);
         System.debug('configs>>>'+configs);
        List<Object> result = new List<Object>();
        CS_JSON_Schema.CS_JSON_Object siteSpec = this.setting.getDefinition(CS_JSON_Constants.Site_SpecificationName);
        System.debug('siteSpec>>>'+siteSpec);
        if (sites != null){
            for(Site site : sites.values()) {
                System.debug('site>>>'+site);
                Map<String, Object> siteObj = getValues(siteSpec, CS_Utl_Object.getUntyped(site));
                System.debug('siteObj>>>'+siteObj);
                List<Object> siteProducts = new List<Object>();
                for(ProductConfiguration cfg : site.configs) {
                     System.debug('getDefinition 11>>>'+JSON.serializepretty(this.setting.definitions));
                    //CS_JSON_Schema.CS_JSON_Object spec = this.setting.getDefinition(cfg.config.Product_Definition_Name__c);
                    CS_JSON_Schema.CS_JSON_Object spec = this.setting.getDefinition(site.Name);
                    System.debug('spec>>>'+JSON.serializePretty(spec));
                    if(spec != null) {
                        Map<String, Object> cfgObj = new Map<String, Object>();
                        cfgObj = setConfig(cfg, spec);
                        
                        //siteProducts.add(setConfig(cfg, spec));
                        
                        System.debug('siteProducts>>>'+JSON.serializePretty(siteProducts));
                        if(spec.relatedObjects != null && !spec.relatedObjects.isEmpty()){
                            for(CS_JSON_Schema.CS_JSON_Object obj : spec.relatedObjects) {
                                System.debug('spec.relatedObjects>>>'+JSON.serializePretty(spec.relatedObjects));
                                Id parentId = String.valueOf(cfg.config.get(obj.parentField));
                                System.debug('parentId>>>'+parentId);
                                List<SObject> relatedRecords = Database.query(getQuery(obj, parentId));
                                 System.debug('relatedRecords>>>'+JSON.serializePretty(relatedRecords));
                                if(!relatedRecords.isEmpty()) {
                                    //siteProducts.add(getValues(obj, relatedRecords.get(0)));
                                    System.debug('cfgObj 111>>>'+cfgObj);
                                    System.debug('spec.relatedList>>>'+spec.relatedList);
                                    Object relatedFields = getValues(obj, relatedRecords.get(0));
                                    System.debug('relatedFields>>>'+relatedFields);
                                    cfgObj.putAll(getValues(obj, relatedRecords.get(0)));
                                    //System.debug('relatedProducts>>>'+relatedProducts);
                                }
                            }
                        }
                        System.debug('cfgObj>>>'+cfgObj);
                        siteProducts.add(cfgObj);
                        siteObj.putAll(cfgObj);
                    }
                }
                System.debug('siteObj000>>>'+siteObj);
                siteObj.put(siteSpec.relatedList, siteProducts);
                System.debug('siteObj>>>'+siteObj);
                result.add(siteObj);
            }
            
        }
            
        return result;
    }*/
    @TestVisible
    private String getQuery(Id id) {
        String queryStr =  String.format(QueryTemplate, new List<String>{
            'Id',
            'cscfga__Product_Configuration__c',
            getFilter(id)
        });
        System.debug('queryStr>>>'+queryStr);
        return queryStr;
    }
    @TestVisible
    private String getQuery(CS_JSON_Schema.CS_JSON_Object def, Id id) {
        System.debug('def>>>>'+def);
        return String.format(QueryTemplate, new List<String>{
            getFields(def.fields),
            def.type,
            getFilter(def.filter, id)
        });
    }
    @TestVisible
    private String getFields(List<CS_JSON_Schema.CS_JSON_Field> fieldsList) {
        return CS_Utl_Array.join(getFieldNames(fieldsList), ',');
    }
    
    @TestVisible
    private Set<String> getFieldNames(List<CS_JSON_Schema.CS_JSON_Field> fieldsList) {
        Set<String> fieldNames  = new Set<String>();
        for(CS_JSON_Schema.CS_JSON_Field field : fieldsList) {
            fieldNames.add(field.Name);
        }
        
        return fieldNames;
    }
    @TestVisible
    private String getFilter(String filter, Id id) {
        System.debug('filter>>>'+filter);
        System.debug('id>>>'+id);
        return String.format(filter, new List<String>{escapeId(id)});
    }
    
    @TestVisible
    private String getFilter(Id id) {
        System.debug('(this.setting.filter >>>>'+this.setting.filter );
        return String.format(this.setting.filter + ' AND cscfga__Product_Basket__c = {0}', new List<String>{escapeId(id)});
    }
    @TestVisible
    private Map<String, Site> getSites(Map<Id, ProductConfiguration> configs) {
        System.debug('configs>>'+configs);
        Map<String, Site> sites = new Map<String, Site>();
        
        if (configs != null){
            for(ProductConfiguration cfg : configs.values()) {
                System.debug('cfg.config.Site__c>>'+cfg.config.Site__c);
                System.debug('cfg.config.Site__c>>'+cfg.config.BT_Net_Primary__c);
                if(cfg.config.Name.containsignorecase('BTNet'))
                {
                    if(cfg.config.BT_Net_Primary__c !=null){
                        Site site = new Site('BTnet Primary');
                        site.configs.add(cfg);
                        sites.put(site.name, site);
                    }
                    if(cfg.config.BT_Net_Secondary__c !=null){
                        Site site = new Site('BTnet Secondary');
                        site.configs.add(cfg);
                        sites.put(site.name, site);
                    }
                }else{
                    if(sites.containsKey(cfg.config.Product_Definition_Name__c)) {
                        sites.get(cfg.config.Product_Definition_Name__c).configs.add(cfg);
                    }
                    else {
                        Site site = new Site(cfg.config.Product_Definition_Name__c);
                        site.configs.add(cfg);
                        sites.put(site.name, site);
                    }
                }
            }
        }
            
        return sites;
    }
    
    public class Site {
        public String name {get; set;}
        
        public List<CS_JSONService_Products.ProductConfiguration> configs {get; set;}
        
        public Site(String name) {
            this.name = name;
            this.configs = new List<CS_JSONService_Products.ProductConfiguration>();
        }
    }
}