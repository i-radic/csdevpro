@isTest
private class Test_LandscapeDBAMinsertupdatedby {

static testMethod void TestCases() {

Date TestDate2 = date.today();
Date TestDate1 = date.today()+1;
 Profile checkProfile;
 

        checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Account acc2 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc2.Sector__c = 'others';
        acc2.LOB_Code__c = 'TEST1';
        acc2.SAC_Code__c = 'JMMO12312';
        acc2.LE_Code__c = null;
        acc2.AM_EIN__c = '803268114';
        acc2.postcode__c = 'WR5 3RM';
        acc2.Base_Team_Assign_Date__c = Date.today();
        insert acc2;
        
Landscape_DBAM__c LS2 = new Landscape_DBAM__c();     
LS2.Account__c = acc2.id; 
 
LS2.Total_Calls_Spend__c=11;
checkProfile.Name='DBAM';  

try
{
Insert LS2;
}
catch(Exception e)
{
  e.getMessage();
}


}


}