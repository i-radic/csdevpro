@IsTest
public class CS_DiscountBeforeBasketSaveObserverTest  {
	testMethod static void testDiscountBeforeBasketSaveObserverTest(){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
		
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc.Calculations_Product_Group__c = 'Future Mobile';

        INSERT pc;

        CS_DiscountBeforeBasketSaveObserver cmtrl = new CS_DiscountBeforeBasketSaveObserver();

        Test.startTest();

        String returnMessage = cmtrl.execute(JSON.serialize(basket));
        System.assertNotEquals(returnMessage, '');

        Test.stopTest();
	}
	
	testMethod static void testDiscountBeforeBasketSaveObserverTest1(){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
        
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.Calculations_Product_Group__c = 'Cloud Voice';

        INSERT pc;

        CS_DiscountBeforeBasketSaveObserver cmtrl = new CS_DiscountBeforeBasketSaveObserver();

        Test.startTest();

        String returnMessage = cmtrl.execute(JSON.serialize(basket));
        System.assertEquals(returnMessage, '');

        Test.stopTest();
	}

	testMethod static void testDiscountBeforeBasketSaveObserverValidTest(){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
		
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
		cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc.Calculations_Product_Group__c = 'BT Mobile';

        INSERT pc;

        CS_DiscountBeforeBasketSaveObserver cmtrl = new CS_DiscountBeforeBasketSaveObserver();

        Test.startTest();

        String returnMessage = cmtrl.execute(JSON.serialize(basket));
        System.assertEquals(returnMessage, '');

        Test.stopTest();
	}

}