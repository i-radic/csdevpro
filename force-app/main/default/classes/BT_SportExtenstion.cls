/*
Client      : BT
Author      : Krupakar J
Date        : 30/01/2013 (dd/mm/yyyy)
Description : This class will get BT Sport details to populate in the visual force page. 
The visualforce page is rendered in PDF format for printing purpose.
*/
 
public class BT_SportExtenstion {
    
    public BT_Sport__c bts {get;set;}
    public BT_Sport__c btsWithRelated {get;set;}
    public list<BT_Sport__c> lstSportSites {get;set;}
    public Contact oContact {get;set;}
    public set<Id> sSportSiteIds {get;set;}
    public list<BT_Sport_Viewing_Card__c> lstSportViewingCards {get;set;}
    public map<Id, list<BT_Sport_Viewing_Card__c>> mSportViewingCards {get;set;}
    public map<Id, boolean> mSportViewingCardsRender {get;set;}

    public BT_SportExtenstion (ApexPages.StandardController stdcon) {
            
            // Initialize Variables
            bts = new BT_Sport__c();
            btsWithRelated = new BT_Sport__c();
            lstSportSites = new list<BT_Sport__c>();
            sSportSiteIds = new set<Id>();
            lstSportViewingCards = new list<BT_Sport_Viewing_Card__c>();
            mSportViewingCards = new map<Id, list<BT_Sport_Viewing_Card__c>>();
            mSportViewingCardsRender = new map<Id, boolean>();
            oContact = new Contact();
            bts = (BT_Sport__c)stdcon.getRecord();
            
            //Get All Related Objects In One Query 
     /*
            btsWithRelated = [Select b.WiFI__c, b.Viewing_Cards_Required__c, b.Town__c, b.SystemModstamp, b.Street__c, b.Site_Type__c, b.Site_Contact_Name__c, b.RecordTypeId, b.Rateable_Value__c, b.Pub_Finder_Website__c, b.Pub_Finder_Telephone__c, b.Price_after_Discounts__c, b.Price__c, b.Post_Code__c, b.PmfLookupKey__c, b.PMF_Version__c, b.OwnerId, b.Opportunity__r.Name, b.Opportunity__c, b.Opportunity_ID__c, b.Customer_Name__c, b.Onsite_contact_telephone_number__c, b.Onsite_contact_email__c, b.Number_of_Rooms_receiving_BT_Sport__c, b.Net_Price__c, b.Name, b.MSA_Discounted_Price__c, b.MSA_Discount__c, b.MSA_Cat_2__c, b.MSA_Cat_1__c, b.MSA_CAT_2_Discount_Price__c, b.MSA_CAT_1_Discount_Price__c, b.LastModifiedDate, b.LastModifiedById, b.LastActivityDate, b.Is_Satellite_Install_Required__c, b.IsDeleted, b.Id, b.Gross_Price__c, b.Discounts__c, b.Discount_Amount__c, b.CreatedDate, b.CreatedById, b.County__c, b.Contract_Type__c, b.ConnectionSentId, b.ConnectionReceivedId, b.Building_Number__c, b.Building_Name__c, b.Billing_authority_number__c, b.Band__c, b.BT_Sport_Total_Discounts__c, b.BT_Sport_Price_after_Discounts__c, b.BT_Sport_MSA_Start_Date__c, b.BT_Sport_MSA_Period__c, b.BT_Sport_MSA_Oppy__c, b.BT_Sport_MSA_Discounted_Price__c, b.BT_Sport_Header__c, b.BT_Sport_Gross_Price__c, b.BT_Sport_General_MSA_Term__c, b.BT_Sport_General_MSA_Discount__c, b.BT_Sport_Cat_2_Pub_MSA_Discount__c, b.BT_Sport_Cat_2_MSA_Term__c, 
            (Select Id, OwnerId, IsDeleted, Name, RecordTypeId, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, Opportunity__c, Band__c, Billing_authority_number__c, Building_Name__c, Building_Number__c, Contract_Type__c, County__c, Discount_Amount__c, Discounts__c, Gross_Price__c, Is_Satellite_Install_Required__c, MSA_Cat_1__c, MSA_Cat_2__c, MSA_Discount__c, MSA_Discounted_Price__c, Net_Price__c, Number_of_Rooms_receiving_BT_Sport__c, Onsite_contact_email__c, Onsite_contact_telephone_number__c, PMF_Version__c, PmfLookupKey__c, Post_Code__c, Price__c, Price_after_Discounts__c, Rateable_Value__c, Site_Contact_Name__c, Site_Type__c, Street__c, Town__c, Viewing_Cards_Required__c, WiFI__c, BT_Sport_Cat_2_MSA_Term__c, BT_Sport_Header__c, BT_Sport_Cat_2_Pub_MSA_Discount__c, BT_Sport_General_MSA_Discount__c, BT_Sport_General_MSA_Term__c, BT_Sport_Gross_Price__c, BT_Sport_MSA_Discounted_Price__c, BT_Sport_MSA_Oppy__c, BT_Sport_MSA_Period__c, BT_Sport_MSA_Start_Date__c, BT_Sport_Price_after_Discounts__c, BT_Sport_Total_Discounts__c, MSA_CAT_1_Discount_Price__c, MSA_CAT_2_Discount_Price__c, Pub_Finder_Telephone__c, Pub_Finder_Website__c From BT_Sport_header__r) 
            From BT_Sport__c b
            WHERE b.Id =: bts.Id];
     */       
            // removed pubfinder referencies
             btsWithRelated = [Select b.WiFI__c, b.Viewing_Cards_Required__c, b.Town__c, b.SystemModstamp, b.Street__c, b.Site_Type__c, b.Site_Contact_Name__c, b.RecordTypeId, b.Rateable_Value__c, b.Price_after_Discounts__c, b.Price__c, b.Post_Code__c, b.PmfLookupKey__c, b.PMF_Version__c, b.OwnerId, b.Opportunity__r.Name, b.Opportunity__c, b.Opportunity_ID__c, b.Customer_Name__c, b.Onsite_contact_telephone_number__c, b.Onsite_contact_email__c, b.Number_of_Rooms_receiving_BT_Sport__c, b.Net_Price__c, b.Name, b.MSA_Discounted_Price__c, b.MSA_Discount__c, b.MSA_Cat_2__c, b.MSA_Cat_1__c, b.MSA_CAT_2_Discount_Price__c, b.MSA_CAT_1_Discount_Price__c, b.LastModifiedDate, b.LastModifiedById, b.LastActivityDate, b.Is_Satellite_Install_Required__c, b.IsDeleted, b.Id, b.Gross_Price__c, b.Discounts__c, b.Discount_Amount__c, b.CreatedDate, b.CreatedById, b.County__c, b.Contract_Type__c, b.ConnectionSentId, b.ConnectionReceivedId, b.Building_Number__c, b.Building_Name__c, b.Billing_authority_number__c, b.Band__c, b.BT_Sport_Total_Discounts__c, b.BT_Sport_Price_after_Discounts__c, b.BT_Sport_MSA_Start_Date__c, b.BT_Sport_MSA_Period__c, b.BT_Sport_MSA_Oppy__c, b.BT_Sport_MSA_Discounted_Price__c, b.BT_Sport_Header__c, b.BT_Sport_Gross_Price__c, b.BT_Sport_General_MSA_Term__c, b.BT_Sport_General_MSA_Discount__c, b.BT_Sport_Cat_2_Pub_MSA_Discount__c, b.BT_Sport_Cat_2_MSA_Term__c, 
            (Select Id, OwnerId, IsDeleted, Name, RecordTypeId, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, Opportunity__c, Band__c, Billing_authority_number__c, Building_Name__c, Building_Number__c, Contract_Type__c, County__c, Discount_Amount__c, Discounts__c, Gross_Price__c, Is_Satellite_Install_Required__c, MSA_Cat_1__c, MSA_Cat_2__c, MSA_Discount__c, MSA_Discounted_Price__c, Net_Price__c, Number_of_Rooms_receiving_BT_Sport__c, Onsite_contact_email__c, Onsite_contact_telephone_number__c, PMF_Version__c, PmfLookupKey__c, Post_Code__c, Price__c, Price_after_Discounts__c, Rateable_Value__c, Site_Contact_Name__c, Site_Type__c, Street__c, Town__c, Viewing_Cards_Required__c, WiFI__c, BT_Sport_Cat_2_MSA_Term__c, BT_Sport_Header__c, BT_Sport_Cat_2_Pub_MSA_Discount__c, BT_Sport_General_MSA_Discount__c, BT_Sport_General_MSA_Term__c, BT_Sport_Gross_Price__c, BT_Sport_MSA_Discounted_Price__c, BT_Sport_MSA_Oppy__c, BT_Sport_MSA_Period__c, BT_Sport_MSA_Start_Date__c, BT_Sport_Price_after_Discounts__c, BT_Sport_Total_Discounts__c, MSA_CAT_1_Discount_Price__c, MSA_CAT_2_Discount_Price__c From BT_Sport_header__r) 
            From BT_Sport__c b
            WHERE b.Id =: bts.Id];
            
            
            list<OpportunityContactRole> oContactRole= new list<OpportunityContactRole>();
            if(String.isNotBlank(String.valueOf(btsWithRelated.Opportunity__c))) {
            	oContactRole = [Select o.OpportunityId, o.Id, o.ContactId From OpportunityContactRole o WHERE o.OpportunityId =: btsWithRelated.Opportunity__c Order By o.CreatedDate ASC LIMIT 1];
            	if(oContactRole.size()>0 && String.isNotBlank(String.valueOf(oContactRole[0].ContactId))) {
            		oContact = [Select c.Name, c.Phone, c.Id, c.Email From Contact c WHERE c.Id =:oContactRole[0].ContactId];
            	} 
            }
            
            //Pass Related Objects Data To Initiated Variables 
            lstSportSites = btsWithRelated.BT_Sport_header__r;
            
            //Populate SiteIds To Get Viewing Cards
            for(BT_Sport__c ss : lstSportSites) {
                sSportSiteIds.add(ss.Id);
                list<BT_Sport_Viewing_Card__c> lstBSViewingCards = new list<BT_Sport_Viewing_Card__c>();
                mSportViewingCards.put(ss.Id, lstBSViewingCards);
            }
            
            //Query To Get Viewing Cards And Build List
            lstSportViewingCards = [Select b.is_this_part_of_a_SMATV_installation__c, b.Type_of_sky__c, b.BT_Sport__r.Name, b.Telephone_number__c, b.SystemModstamp, b.Sky_viewing_card_number__c, b.Name, b.LastModifiedDate, b.LastModifiedById, b.LastActivityDate, b.IsDeleted, b.Id, b.Customer_has_Sky_Viewing_Card__c, b.CreatedDate, b.CreatedById, b.ConnectionSentId, b.ConnectionReceivedId, b.BT_Sport__c From BT_Sport_Viewing_Card__c b WHERE b.BT_Sport__c IN : sSportSiteIds];
            
            system.debug('Map Values and Keys' + mSportViewingCards);
            system.debug('Viewing Cards : ' + lstSportViewingCards);
            
            //Build Map For BT Sport Multiple Viewing Cards For Site
            for(BT_Sport_Viewing_Card__c vc : lstSportViewingCards) {
                system.debug('Entered For Loop Key : ' + vc.BT_Sport__c);
                list<BT_Sport_Viewing_Card__c> lstBSViewingCards = new list<BT_Sport_Viewing_Card__c>();
                lstBSViewingCards = mSportViewingCards.get(vc.BT_Sport__c);
                lstBSViewingCards.add(vc);
                mSportViewingCards.put(vc.BT_Sport__c, lstBSViewingCards);
                
            }
            system.debug('Map Values and Keys' + mSportViewingCards);
            
            //Build MapRender To Show Or Hide Viewing Cards
            for(Id key:mSportViewingCards.keySet()) {
                list<BT_Sport_Viewing_Card__c> lstBSViewingCards = new list<BT_Sport_Viewing_Card__c>();  
                lstBSViewingCards = mSportViewingCards.get(key);
                if(lstBSViewingCards.size()==0) {
                    mSportViewingCardsRender.put(key, false);
                }
                else {
                    mSportViewingCardsRender.put(key, true);
                }
            }
            
    }
}