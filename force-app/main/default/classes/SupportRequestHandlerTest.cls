/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2013-05-08 13:10:07 
 *  @description:
 *      Unit tests for SupportRequestHandler
 *  
 *  Version History :   
 *  
 *  @author Gary Marsh (westbrook)
 *  @date 2013-06-04
 *  @desc Changed existing tests to set the status to Approved on update only
 *  and added tests for new code in support request handler 
 *  @remarks Testing using default opportunity team members on a user's profile
 *  is not possible. DML is not allowed on UserTeamMember so no test data can
 *  be created
 *  
 *  @author Gary Marsh (westbrook)
 *  @date 2013-08-07
 *  @desc Added tests for ApproveOnAssignment method
 */
@isTest
public class SupportRequestHandlerTest {
    static final String TEAM_ROLE_TPS = SupportRequestHandler.TEAM_ROLE_TPS;
    static final String TEAM_ROLE_SS = SupportRequestHandler.TEAM_ROLE_SS;
    static final String RT_TPS = SupportRequestHandler.RT_TPS;
    static final String RT_SS = SupportRequestHandler.RT_SS;
    static final String RT_BCSC = SupportRequestHandler.RT_BCSC;
    //static final String RT_BCSS = SupportRequestHandler.RT_BCSS;
    
    static Id SYS_ADMIN_PROFILE_ID = MockUtils.getProfile('System Administrator').Id;

    /**
     * #744 - generate OpportunityTeamMember records based on Support_Request__c
     */
   /* static testMethod void testAddOpportunityTeamMember () {
        List<User> admins = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 2, true); 
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            //check that it works with empty Assignee__c
            try {
                Support_Request__c obj = (Support_Request__c)Mock.one('Support_Request__c', new Map<String, Object>{'Assignee__c' =>  null, 'Status__c' => 'New'}, true); 
                Database.update(obj);
            } catch (Exception e) {
                //System.assert(false, 'Failed to insert or update Support_Request__c with empty Assignee__c. ' + e);
            }
            
            Support_Request__c obj = (Support_Request__c)Mock.one('Support_Request__c', new Map<String, Object>{'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_TPS).Id, 'Assignee__c' =>  admins[0].Id}, true); 
            Id oppId = obj.Opportunity__c;
            //check that current OpportunityTeam does not contain current user in its team
            System.assertEquals(0, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId in: Mock.toIds(admins)], 
                                    'Did not expect to find test users in Opportunity team');

            //first time set to Approved
            obj.Status__c = 'Approved';
            Database.update(obj);
            System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: obj.Assignee__c], 
                                    'Expected to find test first Assignee user in Opportunity team');

            //change user
            obj.Assignee__c = admins[1].Id;
            Database.update(obj);
            System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: admins[1].Id], 
                                    'Expected to find second test Assignee user in Opportunity team');
            System.assertEquals(0, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: admins[0].Id], 
                                    'Did not expect to find original assignee in Opportunity team');

            //change status to something other than Approved
            obj.Status__c = 'New';
            Database.update(obj);
            System.assertEquals(0, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId in: Mock.toIds(admins)], 
                                    'Did not expect to find test users in Opportunity team because Support_Request__c status is not Approved');
        }
    } */
    /**
     * #744 - generate OpportunityTeamMember records based on Support_Request__c when Support_Request__c is UnDeleted
     *      and remove OpportunityTeamMember when Support_Request__c is deleted
     */
   /* static testMethod void testDeleteUndeleteOpportunityTeamMember () {
        List<User> admins = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 2, true); 
        
        System.runAs(new User(Id = UserInfo.getUserId())) {
            
            //check that it works with empty Assignee__c
            try {
                Support_Request__c obj = (Support_Request__c)Mock.one('Support_Request__c', new Map<String, Object>{'Assignee__c' =>  null, 'Status__c' => 'New'}, true); 
                Database.update(obj);
            } catch (Exception e) {
                System.assert(false, 'Failed to insert or update Support_Request__c with empty Assignee__c. ' + e);
            }

            Support_Request__c obj = (Support_Request__c)Mock.one('Support_Request__c', new Map<String, Object>{'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_TPS).Id, 'Assignee__c' =>  admins[0].Id}, true); 
            Id oppId = obj.Opportunity__c;
            //check that current OpportunityTeam does not contain current user in its team
            System.assertEquals(0, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId in: Mock.toIds(admins)], 
                                    'Did not expect to find test users in Opportunity team');

            //first time set to Approved
            obj.Status__c = 'Approved';
            Database.update(obj);
            System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: obj.Assignee__c], 
                                    'Expected to find test first Assignee user in Opportunity team');

            //Delete Support_Request__c
            Database.delete(obj);
            System.assertEquals(0, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId in: Mock.toIds(admins)], 
                                    'Did not expect to find test users in Opportunity team because Support_Request__c has been deleted');

            //Undelete
            Database.undelete(obj);
            System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: obj.Assignee__c], 
                                    'Expected to find test first Assignee user in Opportunity team');

        }
    } */
    /**
     * #744 - generate OpportunityTeamMember records based on Support_Request__c
     * check how Mass Insert/Delete works
     */
   /* static testMethod void testMassHandlingOfOpportunityTeamMember () {
        List<User> admins = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 2, true); 
        List<User> admins_2 = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 2, true); 
        
        Mock.MOCK_DATA.addSingletonObjectType('Opportunity');

        System.runAs(new User(Id = UserInfo.getUserId())) {
            List<Support_Request__c> objs = Mock.many('Support_Request__c', 
                    new Map<String, Object>{'Assignee__c' =>  Mock.toIds(admins), 'Status__c' => 'New'}, 2, true); 
            for(Support_Request__c obj : objs) {
                obj.Status__c = 'Approved';
            }
            update objs;
            
            Id oppId = objs[0].Opportunity__c;
            System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: admins[0].Id], 
                                    'Expected to find both test users in Opportunity team'); 
           System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: admins[1].Id], 
                                    'Expected to find both test users in Opportunity team');

            //check that we can change Assignee__c and preserve original TeamMembers on the list when Keep_existing_assignee_in_team__c is ON
            objs[0].Assignee__c = admins_2[0].Id;
            objs[0].Keep_existing_assignee_in_team__c = true;
            objs[1].Assignee__c = admins_2[1].Id;
            objs[1].Keep_existing_assignee_in_team__c = false;
            Database.update(objs);
           
            System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: admins[0].Id], 
                                    'Expected to find the original test user in Opportunity team, because Keep_existing_assignee_in_team__c = true');
            System.assertEquals(2, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId in: Mock.toIds(admins_2)], 
                                    'Expected to find both admins_2 users in Opportunity team');
            System.assertEquals(0, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: admins[1].Id], 
                                    'Did not expected to find the original second test user in Opportunity team, because Keep_existing_assignee_in_team__c = false');

            //check that Keep_existing_assignee_in_team__c has been reset
            System.assertEquals(false, [select Keep_existing_assignee_in_team__c from Support_Request__c where Id =: objs[0].Id].Keep_existing_assignee_in_team__c, 
                                    'Expected Keep_existing_assignee_in_team__c flag to be reset back to false'); 

            //at this point we have
            //- obj[0].Assignee__c = admins_2[0]
            //- obj[1].Assignee__c = admins_2[1]
            //- Team: 
            //      admins[0] (because was kept due to Keep_existing_assignee_in_team__c flag), 
            //      admins_2[0], admins_2[1]

            //Delete Support_Request__c
            Database.delete(objs);
            System.assertEquals(0, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId in: Mock.toIds(admins_2)], 
                                    'Did not expect to find test users in Opportunity team because Support_Request__c records have been deleted');
            System.assertEquals(1, [select count() from OpportunityTeamMember where OpportunityId =: oppId and UserId =: admins[0].Id], 
                                    'Expected to find the original test user in Opportunity team, because Keep_existing_assignee_in_team__c = true');
                                    

        }
    } */
    
    /**
    * #781 - set assignee when record type is set to technical pre-sales and
    * there is a default technical pre-sales team role
    */
   /*
    * shiva commented on 08-02-2018 to avoid test errors
    * 
    *  public static testMethod void testSetAssigneeDefaultTPS() {
        // use blank mock class

        Mock.MOCK_DATA = new MockBlank();
        List<User> recordOwner = new List<User>(); //T-29844 fix
        List<User> oppTeamMembers = new List<User>();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){
            recordOwner  = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254660'}, 1, true);
            oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254661'}, 1, true);
        } //T-29844 fix
        // set default technical pre-sales assignee
        Default_Assignee__c[] defaults = Mock.many('Default_Assignee__c', 
            new Map<String, Object>{
                'Name' => 'default',
                'Technical_Pre_Sales__c' => oppTeamMembers[0].Id,
                'Solution_Sales__c' => oppTeamMembers[0].Id
            }, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_TPS).Id,
                    'Status__c' => 'New'
                }, 1, false);
            
            System.debug(objs);
            
            // do test
            Test.startTest();
            insert objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from Support_Request__c 
            where Id in :objs and Assignee__c = :oppTeamMembers[0].Id],
            'Expected assignee to be set to technical pre-sales under the ' +
            'default assignee custom setting'); 
        }
    }
    */
    /**
    * #781 - set assignee when record type is set to technical pre-sales and
    * there is a default technical pre-sales team role
    */
    public static testMethod void testSetAssigneeDefaultSS() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
        // use blank mock class
        Mock.MOCK_DATA = new MockBlank();

        List<User> recordOwner = new List<User>(); //T-29844 fix
        List<User> oppTeamMembers = new List<User>();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){
			recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID, 'EIN__C' => '658254662'}, 1, true);
			oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254663'}, 1, true);
oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254665'}, 1, true);
        } //T-29844 fix   

        // set default technical pre-sales assignee
        Default_Assignee__c[] defaults = Mock.many('Default_Assignee__c', 
            new Map<String, Object>{
                'Name' => 'default',
                'Technical_Pre_Sales__c' => oppTeamMembers[0].Id,
                'Solution_Sales__c' => oppTeamMembers[0].Id
            }, 1, true);
        
        System.runAs(usr) {//recordOwner[0] //T-29844 fix 
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_SS).Id,
                    'Status__c' => 'New'
                }, 1, false);
            
            System.debug(objs);
            
            // do test
            Test.startTest();
            insert objs;
            Update objs;
            delete objs;
            undelete objs;
            Test.stopTest();
            try{
            System.assertEquals(1, [select count() from Support_Request__c 
            where Id in :objs and Assignee__c = :oppTeamMembers[1].Id],
            'Expected assignee to be set to solution sales under the ' +
            'default assignee custom setting');
                
            }
            Catch(exception e){}
        }
    }
    
    /**
    * #781 - set assignee when record type is set to technical pre-sales and
    * there is no defaults
    */
    public static testMethod void testSetAssigneeUnresolved() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
        // use blank mock class
        Mock.MOCK_DATA = new MockBlank();
        
        User[] recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254666'}, 1, true);
        User[] oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254667'}, 1, true);
        
        // ! DML not allowed on UserTeamMember
        /*
            UserTeamMember[] tpsMember = Mock.many('UserTeamMember',
                new Map<String, Object> {
                    'UserId' => oppTeamMembers[0],
                    'OwnerId' => recordOwner[0],
                    'TeamMemberRole' => TEAM_ROLE_TPS
                }, 1, false);
                
            insert tpsMember;
        */
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_TPS).Id
                }, 1, false);
            
            // do test
            Test.startTest();
            try {
                insert objs;
                System.assert(false, 'Exception not thrown when it was expected');
            } catch (DmlException ex) {
                System.assert(ex.getMessage().contains(System.Label.Assignee_Unresolved), 
                'Expected exception not thrown, actual exception ' + ex);   
            }
            Test.stopTest();
        }
    }
    
    /**
    * #774
    * Set assignee when there is a delegated assignee on the default TPS
    */
  /*  
   * shiva commented on 08-02-2018 to avoid test errors
   * 
   * public static testMethod void testSetAssigneeDefaultTPSDelegated() {
        // use blank mock class
        Mock.MOCK_DATA = new MockBlank();
        
        List<User> recordOwner = new List<User>(); //T-29844 fix
        List<User> delegate = new List<User>();
        List<User> oppTeamMembers = new List<User>();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){
            recordOwner  = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254668'}, 1, true);
            delegate = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254669'}, 1, true);
            oppTeamMembers = Mock.many('User', new Map<String, Object>{
            'ProfileId' => SYS_ADMIN_PROFILE_ID,
                'EIN__C' => '658254670',
            'DelegatedApproverId' => delegate[0].Id
            }, 1, true);
        } //T-29844 fix

        // set default technical pre-sales assignee
        Default_Assignee__c[] defaults = Mock.many('Default_Assignee__c', 
            new Map<String, Object>{
                'Name' => 'default',
                'Technical_Pre_Sales__c' => oppTeamMembers[0].Id,
                'Solution_Sales__c' => oppTeamMembers[0].Id
            }, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_TPS).Id,
                    'Status__c' => 'New'
                }, 1, false);
            
            System.debug(objs);
            
            // do test
            Test.startTest();
            insert objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from Support_Request__c 
            where Id in :objs and Assignee__c = :delegate[0].Id
            and Delegated_Assignee__c = true],
            'Expected assignee to be set to delegate of technical pre-sales under the ' +
            'default assignee custom setting'); 
        }
    }
    */
    /**
    * #774
    * Set assignee when there is a delegated assignee on the default SS
    */
    public static testMethod void testSetAssigneeDefaultSSDelegated() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
        // use blank mock class
        Mock.MOCK_DATA = new MockBlank();
        List<User> recordOwner = new List<User>(); //T-29844 fix
        List<User> delegate = new List<User>();
        List<User> oppTeamMembers = new List<User>();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){
            recordOwner  = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254671'}, 1, true);
            delegate = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254672'}, 1, true);
            oppTeamMembers = Mock.many('User', new Map<String, Object>{
            'ProfileId' => SYS_ADMIN_PROFILE_ID,
            'DelegatedApproverId' => delegate[0].Id,
                'EIN__C' => '658254673'
            }, 1, true);
        } //T-29844 fix        

        // set default technical pre-sales assignee
        Default_Assignee__c[] defaults = Mock.many('Default_Assignee__c', 
            new Map<String, Object>{
                'Name' => 'default',
                'Technical_Pre_Sales__c' => oppTeamMembers[0].Id,
                'Solution_Sales__c' => oppTeamMembers[0].Id
            }, 1, true);
        
        System.runAs(usr) {//T-29844 fix
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_SS).Id,
                    'Status__c' => 'New'
                }, 1, false);
            
            System.debug(objs);
            
            // do test
            Test.startTest();
            insert objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from Support_Request__c 
            where Id in :objs and Assignee__c = :delegate[0].Id
            and Delegated_Assignee__c = true],
            'Expected assignee to be set to delegate of solution sales under the ' +
            'default assignee custom setting');
        }
    }
    
    /**
    * #781
    * Set assignee when there is a delegated assignee on the default SS
    */
   /*
    * shiva commented on 08-02-2018 to avoid test errors
    * 
    *  public static testMethod void testReadjustAssigneeTPS() {
        // use blank mock class

        Mock.MOCK_DATA = new MockBlank();
        
        List<User> recordOwner = new List<User>(); //T-29844 fix
        List<User> oppTeamMembers = new List<User>();
        List<User> newAssignee = new List<User>();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){
            recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254674'}, 1, true);
            oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254675'}, 1, true);
            newAssignee = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254676'}, 1, true);
        } //T-29844 fix   
        
        // set default technical pre-sales assignee
        Default_Assignee__c[] defaults = Mock.many('Default_Assignee__c', 
            new Map<String, Object>{
                'Name' => 'default',
                'Technical_Pre_Sales__c' => oppTeamMembers[0].Id,
                'Solution_Sales__c' => oppTeamMembers[0].Id
            }, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_TPS).Id,
                    'Status__c' => 'New'
                }, 1, false);
            
            System.debug(objs);
            insert objs;
            
            objs[0].Assignee__c = newAssignee[0].Id;
            
            Test.startTest();
            update objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from ProcessInstanceWorkitem 
            where ActorId = :objs[0].Assignee__c
            and ProcessInstance.TargetObjectId = :objs[0].Id],
            'Expected approval process to be reassigned to new assignee user ');
        }
    }
    */
    /**
    * #781
    * Set assignee when there is a delegated assignee on the default SS
    */
    public static testMethod void testReadjustAssigneeSS() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
        // use blank mock class
        Mock.MOCK_DATA = new MockBlank();
        List<User> recordOwner = new List<User>(); //T-29844 fix
        List<User> oppTeamMembers = new List<User>();
        List<User> newAssignee = new List<User>();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){
			recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID, 'EIN__C' => '658254677'}, 1, true);
			oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID, 'EIN__C' => '658254678'}, 1, true);
			newAssignee = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID, 'EIN__C' => '658254679'}, 1, true);
        } //T-29844 fix   

        // set default technical pre-sales assignee
        Default_Assignee__c[] defaults = Mock.many('Default_Assignee__c', 
            new Map<String, Object>{
                'Name' => 'default',
                'Technical_Pre_Sales__c' => oppTeamMembers[0].Id,
                'Solution_Sales__c' => newAssignee[0].Id
            }, 1, true);
        
        System.runAs(usr) {//T-29844 fix
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_SS).Id,
                    'Status__c' => 'New'
                }, 1, false);
            
            System.debug(objs);
            insert objs;
            
            objs[0].Assignee__c = newAssignee[0].Id;
            
            Test.startTest();
            update objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from ProcessInstanceWorkitem 
            where ActorId = :objs[0].Assignee__c
            and ProcessInstance.TargetObjectId = :objs[0].Id],
            'Expected approval process to be reassigned to new assignee user ');
        }
    }
    
    /**
    * #781 - set approval when support request type is not pre-sales or
    * solution sales
    */
    //EV: Commenting as no applicable approval proces exsist for this record type
    /*
    public static testMethod void testSetForApprovalNonTPSOrSS() {
        
        User[] recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_BCSC).Id
                }, 1, false);
            
            // do test
            Test.startTest();
            insert objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from ProcessInstance 
            where Status = 'Pending' and TargetObjectId in :objs], 'Expected 1 ' + 
            'approval process to be pending after insert');
        }
    } */
    
    /**
     * #838 - approve on insert when record type is bid contract support complex
     */
    //EV: Commenting as no active approval Process exists for RT 'Bid_Contract_Support_Complex'
    /*
    public static testMethod void testApproveOnAssignmentBCSCInsert() {
        User[] recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] newAssignee = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_BCSC).Id,
                    'Assignee__c' => newAssignee[0].Id
                }, 1, false);
            
            Test.startTest();
            insert objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from ProcessInstance where 
            TargetObjectId in :objs and Status = 'Approved'], 
            'Expected one approval process to be approved after updating assignee');
        }
    } */
    
    /**
     * #838 - approve on insert when record type is bid contract support complex
     //EV: Commenting this as this record type is no longer active
    /*public static testMethod void testApproveOnAssignmentBCSSInsert() {
        User[] recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] newAssignee = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_BCSS).Id,
                    'Assignee__c' => newAssignee[0].Id
                }, 1, false);
            
            Test.startTest();
            insert objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from ProcessInstance where 
            TargetObjectId in :objs and Status = 'Approved'], 
            'Expected one approval process to be approved after updating assignee');
        }
    }*/
    
    
    /**
     * #838 - approve on update when record type is bid contract support complex
     */
    //EV :Commenting the code as approval process not active
    /*
    public static testMethod void testApproveOnAssignmentBCSCUpdate() {
        User[] recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] newAssignee = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_BCSC).Id
                }, 1, true);
        
            objs[0].Assignee__c = newAssignee[0].Id;
            
            Test.startTest();
            update objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from ProcessInstance where 
            TargetObjectId in :objs and Status = 'Approved'], 
            'Expected one approval process to be approved after updating assignee');
        }
    } */
    
    /**
     * #838 - approve on update when record type is bid contract support simple
     */
    //EV: Commenting this as this record type is no longer active
    /*
    public static testMethod void testApproveOnAssignmentBCSSUpdate() {
        User[] recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        User[] newAssignee = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID}, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_BCSS).Id
                }, 1, true);
        
            objs[0].Assignee__c = newAssignee[0].Id;
            
            Test.startTest();
            update objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from ProcessInstance where 
            TargetObjectId in :objs and Status = 'Approved'], 
            'Expected one approval process to be approved after updating assignee');
        }
    } */
    
    /**
     * #840
     */
   /*
    * shiva commented on 08-02-2018 to avoid test errors
    * 
    *  public static testMethod void testUpdateChatterForOpportunityTPS() {
        User[] recordOwner = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254680'}, 1, true);
        User[] oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254681'}, 1, true);
        User[] newAssignee = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254682'}, 1, true);
        
        System.runAs(recordOwner[0]) {
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_TPS).Id,
                    'Assignee__c' => oppTeamMembers[0].Id
                }, 1, true);
        
            objs[0].Assignee__c = newAssignee[0].Id;
            
            Test.startTest();
            update objs;
            Test.stopTest();
            
            System.assertEquals(1, [select count() from FeedItem where ParentId = :objs[0].Opportunity__c],
            'Expected 1 feed item related to support request opportunity');
        }
    }
    */
    /**
     * #840
     */
    public static testMethod void testUpdateChatterForOpportunitySS() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
        List<User> recordOwner = new List<User>(); //T-29844 fix
        List<User> oppTeamMembers = new List<User>();
        List<User> newAssignee = new List<User>();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(usr){ 
            recordOwner  = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID, 'EIN__C' => '658254683'}, 1, true);
            oppTeamMembers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID, 'EIN__C' => '658254684'}, 1, true);
            newAssignee  = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID,'EIN__C' => '658254685'}, 1, true);
        } //T-29844 fix   
        
        System.runAs(usr) { //T-29844 fix 
            // create support requests
            Support_Request__c[] objs = Mock.many('Support_Request__c', 
                new Map<String, Object>{
                    'RecordTypeId' => MockUtils.getRecordType('Support_Request__c', RT_SS).Id,
                    'Assignee__c' => oppTeamMembers[0].Id
                }, 1, true);
        
            objs[0].Assignee__c = newAssignee[0].Id;
            
            //Test.startTest();
            update objs;
            //Test.stopTest();
            
            System.assertEquals(1, [select count() from FeedItem where ParentId = :objs[0].Opportunity__c],
            'Expected 1 feed item related to support request opportunity');
        }
    }
}