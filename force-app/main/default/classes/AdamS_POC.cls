public class AdamS_POC {

	private Id basketId;
	public Number_Port_Line_CPQ__c numberPort { get; set; }
	List<numberPortWrapper> numberPortWrapperList = new List<numberPortWrapper> ();
	List<Number_Port_Line_CPQ__c> numberPortList = new List<Number_Port_Line_CPQ__c> ();
	public Contact contactObj { get; set; }
	public Account accountObj { get; set; }


	public AdamS_POC() {
		basketId = ApexPages.currentPage().getParameters().get('basketId');
		cscfga__Product_Basket__c basket = [select id, Customer_Contact__c, csbb__Account__c from cscfga__Product_Basket__c where id = :basketId];
		contactObj = [select Id, Name, FirstName, LastName, Job_Title__c, Phone, Email from Contact where Id = :basket.Customer_Contact__c];
		accountObj = [select Id, Name, Company_Registration_Number__c, BillingStreet, BillingCity, BillingState, BillingPostalcode from Account where Id = :basket.csbb__Account__c];

		populatenumberPortWrapperList();
	}

	public void populatenumberPortWrapperList() {
		numberPortWrapperList.clear();
		List<SelectOption> addressList = new List<SelectOption> ();
		for (Order_Address__c address :[select Id, Display_Name__c from Order_Address__c order where Account__c = :accountObj.Id])
			addressList.add(new SelectOption(address.Id, address.Display_Name__c));

		for (Number_Port_Line_CPQ__c numberPort :[SELECT Id,
		     Product_Basket__c,
		     Losing_CP__c,
		     Billing_A_C_Number__c,
		     Number_Type__c,
		     Main_Number__c,
			 Order_Address__r.Display_Name__c,
		     Range_Holder__c,
		     Associated_Numbers__c,
		     Service_Usage__c,
		     Temporary_Number_Required__c,
		     PVT_Ref__c,
		     LOA_Status__c FROM Number_Port_Line_CPQ__c Where Product_Basket__c = :basketId ORDER BY Losing_CP__c ASC])

		numberPortWrapperList.add(new numberPortWrapper(numberPort, addressList));
	}

	public List<numberPortWrapper> getnumberPortWrapperList() {
		return numberPortWrapperList;
	}

	public void getSelected() {
		numberPortList.clear();
		for (numberPortWrapper npWrapper : numberPortWrapperList){
			if (npWrapper.selected == true){
				npWrapper.np.Order_Address__c = npWrapper.selectedAddress;
				numberPortList.add(npWrapper.np);
			}
		}
	}

	public void tosave() {
		update numberPortList;
		populatenumberPortWrapperList();
	}


	public class numberPortWrapper
	{
		public Number_Port_Line_CPQ__c np { get; set; }
		public list<SelectOption> addressList { get; set; }
		public Boolean selected { get; set; }
		public String selectedAddress { get; set; }
		public numberPortWrapper(Number_Port_Line_CPQ__c n, list<SelectOption> s)
		{
			np = n;
			addressList = s;
			selected = false;
		}
	}

	public List<Number_Port_Reference__c> getCPs() {
        List<Number_Port_Reference__c> rlist = [Select Id, Type__c,Reference__c,Description__c,Area__c, EstablishedIP__c FROM Number_Port_Reference__c WHERE Type__c = 'CP' ORDER BY Reference__c LIMIT 1000 ];
        return rlist ;
    }
}