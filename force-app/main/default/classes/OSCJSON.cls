public class OSCJSON{
	public Sections[] sections;
	public Settings[] settings;
	public ChildSetting[] childSetting;
	class Sections {
		public String name;	//BasketInformation
		public String setting;	//productBasket
	}
	class Settings {
		public String settingName;	//Sites
		public String name;	//SiteProducts
		public String childSetting;	//SiteChildProduct,Extra
		public String DataMappingParam;	//ProductConfig
		public String SourceObject;	//cscfga__Product_Configuration__c
		public String filter;	//cscfga__Product_Basket__c = {0} and cscfga__Parent_Configuration__c = null
	}
	class ChildSetting {
		public String settingName;	//SiteChildProduct
		public String DataMappingParamType;	//SiteChildProduct
		public String childSetting;	//
		public String DataMappingParam;	//ProductConfig
		public String SourceObject;	//cscfga__Product_Configuration__c
		public String filter;	//cscfga__Product_Basket__c Site__c  = {0} and cscfga__Parent_Configuration__c != null
	}
	public static OSCJSON parse(String json){
		return (OSCJSON) System.JSON.deserialize(json, OSCJSON.class);
	}
}