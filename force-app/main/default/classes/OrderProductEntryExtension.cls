public with sharing class OrderProductEntryExtension {

    private ApexPages.StandardController controller;
	
	// Querystring Parameters 
    public Map<string,string> mMapQsParams {get;set;}
    
    // Order Related Variables and Boolean Flags
    public Order poOrder {get;set;}
    public Boolean pbIsErrorCreatingOrder {get;set;}
    public Boolean pbIsErrorDeletingOrder {get;set;}

	// Order Types
	final private String CONST_ORDER_TYPE_CHARGEABLE 		= 'Chargeable Service';
	final private String CONST_ORDER_TYPE_INCLUDED 			= 'Included Services';
	final private String CONST_ORDER_TYPE_CHARGEABLE_ONLY	= 'Chargeable Service Only';	
	final private String CONST_ORDER_TYPE_INCLUDED_ONLY		= 'Included Services Only';
		
	// Status	
	final private string CONST_STATUS_DRAFT = 'Draft';
	final private string CONST_STATUS_OPEN 	= 'Open';
        
	// Product Selector Variables
    private Account moAccount = new Account();
    public List<orderItem> plOrderItems {get;set;}
    public List<priceBookEntry> plPbe {get;set;}
    public Pricebook2 poPbk {get;set;}   
    public String psSelect {get; set;}
    public String psUnselect {get; set;}
    
    //private Boolean pbIsPricebookSelectionRequired = false;
    private List<orderItem> mlOrderItemsForDeletion = new List<orderItem>();
	public String psSaveError {get;set;}
	public Boolean pbIsSaveError {get;set;}
	public Boolean pbIsQuantityExceeded {get;set;}

	// Order Levels
	public List<Order_level__c> plOrderLevels {get;set;}

    // Future Use
    //public String psSearchString {get;set;}
    //public Decimal pdTotal {get;set;}
    
    // Future use in case the number of products grow
    // public Boolean pbIsLimitReached {get;set;}
    // Future use in case EE deacticates multicurrency
    public Boolean pbIsMultiCurrencyOrg {get; set;}



   /*
    * @name         OrderProductEntryExtension
    * @description	Constructor 
    *				- Sets the list of selected and available products
    *				- Sets the pricebook (order pricebook update is done via vf page action)
    *				- Sets any Order levels (such as welcome days)
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    public OrderProductEntryExtension(ApexPages.StandardController controller) {

		// Set the controller
		this.controller = controller;
		
		// Get the Querystring Parameters
		mMapQsParams = ApexPages.currentPage().getParameters();
		
		// Need to know if org has multiple currencies enabled
		pbIsMultiCurrencyOrg = UserInfo.isMultiCurrencyOrganization();
		
		// This is a create Order			
		if(controller.getRecord().Id == null){

			// Error flags for Order
			pbIsErrorCreatingOrder = false;
			pbIsErrorDeletingOrder = false;
			
			// Check that there is an companyid (aid) in the parameters
	        if(mMapQsParams.containsKey('aid')){           
			
	        	poOrder = new Order();
	        	poOrder.AccountId 		= mMapQsParams.get('aid');
		        poOrder.EffectiveDate 	= system.today();
		        poOrder.Status 			= CONST_STATUS_DRAFT;
		        poOrder.RecordTypeId	= [SELECT Id FROM recordtype WHERE SobjectType='Order' AND DeveloperName = 'Supplier' LIMIT 1].Id;
		        
				// Disply the list of products that could be selected for the Order Type
		        toggleOrderType();
	        	
	        }else{
	        	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error initialising new order.'));
	        	pbIsErrorCreatingOrder = true;
	        }
		}
		else{

			try{

				// Clear the Quantity exceeded flag
				pbIsQuantityExceeded = false;
		        
		        // EE has activated multi-currency - so need to get CurrencyIsoCode for Prices 
		        // - Future code in case this is deactivated
		        if(pbIsMultiCurrencyOrg){
		            poOrder = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode, Type, Account.Id, Is_Deletable__c from Order where Id = \'' + controller.getRecord().Id + '\' limit 1');
		        }else{
		            poOrder = database.query('select Id, Pricebook2Id, Pricebook2.Name, Type, Account.Id, Is_Deletable__c from Order where Id = \'' + controller.getRecord().Id + '\' limit 1');
		        }
		        
		        // If products were previously selected need to put them in the "selected products" section to start with
		        plOrderItems = [select Id, Quantity, Category__c, ListPrice, UnitPrice, Description, PriceBookEntryId, 
		        		PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.Product2.Description, 
		        		PriceBookEntry.PriceBook2Id, Status__c, Remaining_Quantity__c, Order_Request_Status__c, PriceBookEntry.Product2.Order_Item_Quantity_Fixed__c 
		        		from orderItem 
		        		where OrderId=:poOrder.Id];

				// Get the Company Remaining Quantity details from the Company record
				String lsSOQL = getRemainingQuantityDynamicSOQL();
				moAccount = database.query(lsSOQL);
				
				// Update the Remaining Quantity
				updateRemainingQuantities();
	
				// Use the Services Price Book and set the picebook variabale (not persisted at this time)
		        if( (poOrder.Pricebook2Id == null) && (!Test.isRunningTest()) ){
		            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where isActive = true and Name = 'Services Price Book' LIMIT 1];
			        // Check if the pricebook was found
			        try{
				        poPbk = activepbs[0];
			        }catch(Exception e){
			        	ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Services Price Book not found');
				        ApexPages.addMessage(errMsg);
			        }
		        }
		        else{
		            poPbk = poOrder.Pricebook2;
		        }
		        
		        // Set the List of Available Products to be selected
		        updateAvailableList();
	
			}catch(Exception e){
				ApexPages.addMessages(e);
			}
		}
    }


   /*
    * @name         toggleOrderType
    * @description	called from the Create Order page to show which products are available
    * @author       P Goodey
    * @date         Jan 2015
    * @see 
    */    
    public PageReference toggleOrderType() {

		// Get the Services Price book
		Id lPkId = [select Id, Name from Pricebook2 where isActive = true and Name = 'Services Price Book' LIMIT 1].Id;			
        // Dynamically build a query string and exclude items already selected
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.Bundled_Service__c, Product2.IsActive, Product2.Description, Product2.Order_Type__c, Product2.Order_Item_Quantity_Fixed__c, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + lPkId + '\'';
		// Query for the Order Type
		if(poOrder.Type == CONST_ORDER_TYPE_CHARGEABLE){
            qstring += ' and Product2.Order_Type__c != \'' + CONST_ORDER_TYPE_INCLUDED_ONLY + '\'';
		}else if(poOrder.Type == CONST_ORDER_TYPE_INCLUDED){
            qstring += ' and Product2.Order_Type__c != \'' + CONST_ORDER_TYPE_CHARGEABLE_ONLY + '\'';	
		}
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';
		
		// Execute the Query
        plPbe = database.query(qString);			
			
		return null;
    }



   /*
    * @name         setPriceBook
    * @description	this is the called from an 'action' method on the vf page - it is needed as you cannot perform DML actions in the constructor
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    public PageReference setPriceBook(){
        try{
        	// Set the Pricebook if there is none currently set
        	if( poOrder.Pricebook2Id == null ){
	            poOrder.Pricebook2Id = poPbk.Id;
	            update(poOrder);        		
        	}
        }catch(Exception e){
        	ApexPages.Message errMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Unable to update the Price Book for the Order');
	        ApexPages.addMessage(errMsg);
        }
        return null;
    }


   /*
    * @name         updateAvailableList
    * @description	updates the available list of products
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */    
    public void updateAvailableList() {
    
        // Dynamically build a query string and exclude items already selected
        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.Bundled_Service__c, Product2.IsActive, Product2.Description, Product2.Order_Type__c, Product2.Order_Item_Quantity_Fixed__c, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + poPbk.Id + '\'';
        
        //Future Use in case EE deactivated multicurrency
        if(pbIsMultiCurrencyOrg){
        	qstring += ' and CurrencyIsoCode = \'' + poOrder.get('currencyIsoCode') + '\'';
        }

		// Query for the Order Type
		if(poOrder.Type == CONST_ORDER_TYPE_CHARGEABLE){
            qstring += ' and Product2.Order_Type__c != \'' + CONST_ORDER_TYPE_INCLUDED_ONLY + '\'';
		}else if(poOrder.Type == CONST_ORDER_TYPE_INCLUDED){
            qstring += ' and Product2.Order_Type__c != \'' + CONST_ORDER_TYPE_CHARGEABLE_ONLY + '\'';	
		}		
        
        // If looking for a search string entered by the user in the name OR description
        // modify this to search other fields if desired
        // This is a future requirement
		/*
        if(psSearchString!=null){
            qString+= ' and (Product2.Name like \'%' + psSearchString + '%\' or Product2.Description like \'%' + psSearchString + '%\' or Product2.Family like \'%' + psSearchString + '%\')';
        }
        */

		// Add the Ids of the already selected products to exclude from the query
        Set<Id> selectedEntries = new Set<Id>();
        for(orderItem d:plOrderItems){
            selectedEntries.add(d.PricebookEntryId);
        }
        if(selectedEntries.size()>0){
            String tempFilter = ' and Id not in (';
            for(Id i : selectedEntries){
                tempFilter+= '\'' + (String)i + '\',';
            }
            String extraFilter = tempFilter.substring(0,tempFilter.length()-1);
            extraFilter+= ')';
            qString+= extraFilter;
        }
        qString+= ' order by Product2.Name';
        qString+= ' limit 101';

		// Execute the Query
        plPbe = database.query(qString);
        
        // Only display up to 100 results... if there are more than we let the user know (see vf page)
        // Future possible requirement
        /*
        if( (plPbe.size()>100) && (!Test.isRunningTest()) ){
            plPbe.remove(100);
            pbIsLimitReached = true;
        }
        else{
            pbIsLimitReached=false;
        }
        */
    }

    
    
   /*
    * @name         addOrderItems
    * @description	This is called from the vf page when a user presses the "Add" button next to an available product
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */        
    public void addOrderItems(){
    	
        // Loop thru all the selected products 
        for(PricebookEntry d : plPbe){
            if((String)d.Id==psSelect){
				Double dUnitPrice = 0;
				if( poOrder.Type != CONST_ORDER_TYPE_INCLUDED){
					dUnitPrice = d.UnitPrice;
				}

				OrderItem oi = new OrderItem();
				oi.OrderId			= poOrder.Id;
                oi.PriceBookEntry	= d; 
                oi.PriceBookEntryId	= d.Id; 
                oi.UnitPrice		= dUnitPrice;
                
                // Set the Quantity using the fixed value on the prodcut if it exists
                oi.Quantity 		= d.Product2.Order_Item_Quantity_Fixed__c==null? 1 : d.Product2.Order_Item_Quantity_Fixed__c;
                oi.Status__c		= CONST_STATUS_OPEN;

        		// Future Clone
        		// This is a fix to allow Orders to be cloned with products - if the ServiceDate (Order Product Start Date) is null
        		// then the date is set from the Order the EffectiveDate (Start Date) which produces and error 
                oi.ServiceDate		= date.newinstance(2099, 12, 31);
    			//oi.Remaining_Quantity__c = getRemainingQuantity(oi);
    			oi.Remaining_Quantity__c = getRemainingQuantityDynamic(oi);
    			
    			// Add the Order Item
				plOrderItems.add(oi);
    			
                break;
            }
        }
        
        // Set the List of Available Products to be selected
        updateAvailableList();
    }
    

   /*
    * @name         getRemainingQuantityDynamicSOQL
    * @description	Return the SOQL for the Remaining Product Quantity using Dynamic Custom Setting
    * @author       P Goodey
    * @date         Dec 2014
    * @see 
    */        
    public String getRemainingQuantityDynamicSOQL(){

		// Start to build SOQL string
		String lsSOQL = 'SELECT Id';
				
		// Build a dynamically generated set of order level limits
				
		// Rollup Summary fields on the Company Record from the custom setting
		Map<String, OrderLevel_RollupSettings__c> orAllRollups = OrderLevel_RollupSettings__c.getAll();
		for(OrderLevel_RollupSettings__c orRollup : orAllRollups.values()) {

			// Check the custom setting for the fields to be used to limit the quantities
        	if(orRollup.Activate_Remaining_Quantity_Check__c == true){
    			// Create or update the Rollups
				lsSOQL += ', ' + orRollup.Company_Rollup_Field_Name__c; // For example 'Welcome_Days_Remaining_Chargeable_LRE__c'        		
        	}        		
		}

		lsSOQL += ' FROM Account WHERE Id = \'' + poOrder.Account.Id + '\' limit 1';

		return lsSOQL;
    }
    


   /*
    * @name         getRemainingQuantityDynamic
    * @description	Return the Remaining Product Quantity using Dynamic Custom Setting
    * @author       P Goodey
    * @date         Dec 2014
    * @see 
    */        
    public Decimal getRemainingQuantityDynamic(OrderItem poi){

        Decimal ldRemainingQuantity = null;
        Decimal ldPriorQuantity;        
        
		String lsSOQL = getRemainingQuantityDynamicSOQL();
		moAccount = database.query(lsSOQL);
		
		// Build a dynamically generated set of order level limits
		// Rollup Summary fields on the Company Record from the custom setting
		Map<String, OrderLevel_RollupSettings__c> orAllRollups = OrderLevel_RollupSettings__c.getAll();
		for(OrderLevel_RollupSettings__c orRollup : orAllRollups.values()) {
        	// Check if the field is to be used in the avaiable quantity check
        	if(orRollup.Activate_Remaining_Quantity_Check__c == true){

				// Retrieve the remaining count
				// Here we can change the Order Type onto the product in the future
				if( (orRollup.Product_Name__c == poi.PriceBookEntry.Product2.Name) &&
						(orRollup.Order_Type__c == poOrder.Type) ){
					
					// assign the remaning quantity
					ldRemainingQuantity = (decimal)moAccount.get(orRollup.Company_Rollup_Field_Name__c);
					
					// Glitch Fix 3rd Fab 2015 PG
					// We've found that there is custom setting for the rollup so all good for the services that we want to cap
					// however we need to check if the stamped value on the company record is null.  if so then we need to set the 
					// remaining quantity to zero
					ldRemainingQuantity = (ldRemainingQuantity == null ? 0 : ldRemainingQuantity);
					
					// exit the loop
					break;
				}        		
        	}
		}
		
		// For services that are not capped we return a null value which bypasses the quantity check
		return ldRemainingQuantity;
		
    }


    
   /*
    * @name         removeOrderItems
    * @description	This is called from the vf page when a user presses the "Remove" button next to a selected Product
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */ 
    public PageReference removeOrderItems(){
        Integer count = 0;
        for(orderItem d : plOrderItems){
            if((String)d.PriceBookEntryId==psUnselect){
                if(d.Id!=null)
                    mlOrderItemsForDeletion.add(d);
                plOrderItems.remove(count);
                break;
            }
            count++;
        }
        
        // Set the List of Available Products to be selected
        updateAvailableList();
        
        return null;
    }


   /*
    * @name         onSaveAndContinue
    * @description	This is called from the Order Creation vf page when a user presses the "Save and Continue" button
    * @author       P Goodey
    * @date         Aug 2014
    * @see 
    */     
	public PageReference onSaveAndContinue(){
	    try {
	        insert poOrder; 
	        PageReference pageRef = new PageReference('/apex/OrderProductEntry?id='+poOrder.Id);
	        pageRef.setRedirect(true);
	        return pageRef;
	    } catch (DMLException e) {
	        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error creating new order.'));
	        return null;
	    } 
    }
    

   /*
    * @name         onDeleteOrder
    * @description	This is called from the Order Creattion vf page when a user presses the "Delete" button
    * @author       P Goodey
    * @date         Aug 2014
    * @see 
    */     
	public PageReference onDeleteOrder(){

		PageReference pageRef = new PageReference('/'+poOrder.AccountId);
		pageRef.setRedirect(true);
	    try {
	    	if(poOrder.Is_Deletable__c == true){
	    		delete poOrder;
	    	}else{
	    		pbIsErrorDeletingOrder = true;
	    		pageRef = null;
	    	}
	    	return pageRef;
	    } catch (DMLException e) {
	        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error deleting order.'));
	        return null;
	    } 
    }
    
        
    

   /*
    * @name         onAddOrRemoveItems
    * @description	This is called from the Order View vf page when a user presses the "Add or Remove Items" button
    * @author       P Goodey
    * @date         Oct 2014
    * @see 
    */     
	public PageReference onAddOrRemoveItems(){
		try {
	        PageReference pageRef = new PageReference('/apex/OrderProductEntry?id='+poOrder.Id);
	        pageRef.setRedirect(true);
	        return pageRef;
	    } catch (DMLException e) {
	        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Error redirecting to order product entry page.'));
	        return null;
	    } 
    }
    
    
   /*
    * @name         onSave
    * @description	This is called from the vf page when a user presses the "Save" button
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */     
    public PageReference onSave(){
    
		// Set the Page Ref
    	PageReference pageRef = new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));

    	// Clear any errors
    	pbIsQuantityExceeded = false;
    	pbIsSaveError = false;
    	psSaveError = '';
    
        // If previously selected products are now removed, we need to delete them
        if(mlOrderItemsForDeletion.size()>0){
        	
        	// Delete the Order Items
        	delete(mlOrderItemsForDeletion);
        }
    
        // Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
        try{
            if(plOrderItems.size()>0){
            	
         		// Check that there are no errors with the order request quantity
         		psSaveError += validateQuantity();
            	if( !pbIsSaveError ){

            		// This is to set the remaining quantities should the order item be updated 
            		updateRemainingQuantities();

					// Update or insert the Order Items            		
            		upsert(plOrderItems);
            		
            	}else{
            		pageRef = null;
            	}
            }
        }
        catch(Exception e){
        	      	
        	string lsFinalDebug;
        	if( e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION')){
        		lsFinalDebug = e.getMessage().substringAfter('FIELD_CUSTOM_VALIDATION_EXCEPTION,').substringBefore(':');
        	}else{
        		lsFinalDebug = e.getMessage() + ' Exception type caught: ' + e.getTypeName() + ' Cause: ' + e.getCause();
        	}   	
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,lsFinalDebug));
            return null;
        }  
           
        // After save return the user to the Order
        //return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
        return pageRef;
        
    }
    


   /*
    * @name         updateRemainingQuantities
    * @description	This is called during the "onSave" operation
    * @author       P Goodey
    * @date         Aug 2014
    * @see 
    */ 
    private void updateRemainingQuantities(){
    	Decimal ldRemainingQuantity;
    	for( OrderItem oi: plOrderItems){
    		ldRemainingQuantity = getRemainingQuantityDynamic(oi);
    		oi.Remaining_Quantity__c = ldRemainingQuantity;
    	}
    }
    

   /*
    * @name         validateQuantity
    * @description	This is called during the "onSave" operation
    * @author       P Goodey
    * @date         Dec 2014
    * @see 
    */
    private String validateQuantity(){
		
		Decimal ldAvailableQuantity = 0;		// This is the available quantity as stamped on the Company record
		Decimal ldPriorQuantity 	= 0;
		boolean lbIsQuantityExceeded = false;
		// // Oct 2015 PG - Enhance the Message
		// String lsErrorMessage 		= 'Error: ';
		String lsErrorMessage 		= '';
		pbIsSaveError 				= false;
		pbIsQuantityExceeded 		= false;
		Map<Id, Order_Level__c> lMapOrderIdPriorQuantity = new Map<Id, Order_Level__c>();
		Set<Id> lSetOrderItemId 	= new Set<Id>();

    	system.debug('***************' + ' adding to set ');

		// Get all the Prior Quantities
    	for( OrderItem oi: plOrderItems){

			// Only need to validate if the Order Item status is open (Order Item Status can be Open, Cancelled or Complete)
    		if( oi.Status__c == CONST_STATUS_OPEN ){
    			
    			// Need to check if Insert or Update
				if(oi.Id != null){
					
					// This is an update so add the order item to the set
					lSetOrderItemId.add(oi.Id);
				}
    		}		
    	}

    	// Get the Prior Order Levels
    	system.debug('***************' + ' Starting SOQL ');
    	List<Order_Level__c> lListOrderLevels = [SELECT Id, Quantity__c, System_Order_Item__c FROM Order_Level__c WHERE System_Order_Item__c IN :lSetOrderItemId and Is_Quantity_To_Be_Summed__c = true];

    	system.debug('***************' + ' adding to map ');
    			
		// Loop thru the Order Levels
		for( Order_Level__c ol : lListOrderLevels){
			// Add to the map
			system.debug('***************' + ' looping map quantity = ' + ol.Quantity__c);
			lMapOrderIdPriorQuantity.put(ol.System_Order_Item__c, ol);
		}

		// Main loop throu all Order Items - this includes newly added products and existing ones
    	for( OrderItem oi: plOrderItems){
    		
			// Only need to validate if the Order Item status is open (Order Item Status can be Open, Cancelled or Complete)
    		if( oi.Status__c == CONST_STATUS_OPEN ){
    			
    			// 1st Validate that the Quantity is not less than or equal to zero
    			if( oi.Quantity <= 0 ){
					pbIsSaveError = true;
					lsErrorMessage += highlightText(oi.PriceBookEntry.Product2.Name) + ' order quantity of ' + oi.Quantity.intValue() + ' cannot be saved, ';
    			}
    			
    			// 2nd Validate that the Quantity is not exceeded   				
    				
				// Get the Remaining Quantity from the Company record
				ldAvailableQuantity = getRemainingQuantityDynamic(oi);

				// Check that the Quantity is set	
				if( ldAvailableQuantity != null ){
				
	    			// Need to check if Insert or Update
					if(oi.Id != null){
						
						// This is an Update so get the prior quantity (this is a negative number)
						
    					// Get the Prior Quantity using previous batch SOQL routine
    					system.debug('***************' + ' getting Quantity from map ');						
						ldPriorQuantity = lMapOrderIdPriorQuantity.get(oi.Id).Quantity__c;
    					system.debug('***************' + ' got Quantity from map = ' + ldPriorQuantity.toPlainString());						
						
						if( oi.Quantity > ((ldPriorQuantity * -1) + ldAvailableQuantity) ){
							lbIsQuantityExceeded = true;
						}	
					}else{

						// Insert - Simply check that the available quantity is not exceeded and is not zero
						if( ( (ldAvailableQuantity == 0) || (oi.Quantity > ldAvailableQuantity) ) && (!Test.isRunningTest()) ){
							lbIsQuantityExceeded = true;
						}
					}
					
					system.debug('***************' + ' building error string ');
					
					// Build an Error String Message
					if( lbIsQuantityExceeded == true ){
						pbIsSaveError = true;
						pbIsQuantityExceeded = true;

						// Highlight the product name with double quotes
						lsErrorMessage += highlightText(oi.PriceBookEntry.Product2.Name) + ' Quantity [' + oi.Quantity.intValue() + '] exceeds Quantity Remaining [' + ldAvailableQuantity + '], ';
					}
				} 
				
				// Clear the error flag
				lbIsQuantityExceeded = false;
				
    		}
    	}
    	
    	// Oct 2015 PG - Enhance the Message
    	if( pbIsQuantityExceeded == true ){
    		lsErrorMessage = Label.Order_Request_Quantity_Exceeded + ' : ' + lsErrorMessage;
    	}
    	
    	return lsErrorMessage;
    	
    }



   /*
    * @name         highlightText
    * @description	This adds highlight characters at start end of text
    * @author       P Goodey
    * @date         Jan 2015
    * @see 
    */
    private String highlightText(String psString){
		String lsStartChar = '\"';
		String lsEndChar = '\"';
		return lsStartChar + psString + lsEndChar;
    }
    



   /*
    * @name         onCancel
    * @description	This is called from the vf page when a user presses the "Cancel" button
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */      
    public PageReference onCancel(){
 
        // If user hits cancel we commit no changes and return them to the Order   
        return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }


	/**************************************************************************
	
	FUTURE METHOD CALLS OR FUTURE REQUIREMENTS - PLEASE LEAVE
	
	***************************************************************************/	  

   /*
    * @name         addOrderItemsBundle
    * @description	This is called from the vf page when a user presses the "Enterprise" or "Corporate" bundle buttons
    *				Future Requirement?
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
/*         
    public void addOrderItemsBundle(){
    
    	if(psSelect == 'Enterprise'){

	        // This function runs when a user hits the "bundle" button
	        for(PricebookEntry d : plPbe){
	            if( d.Product2.Bundled_Service__c != null){
	                if(d.Product2.Bundled_Service__c.contains(psSelect)){
	                    plOrderItems.add(
	                        new orderItem( 
	                            Quantity 			= 1,
	                            OrderId 			= poOrder.Id, 
	                            PriceBookEntry 		= d, 
	                            PriceBookEntryId 	= d.Id,
	                            Status__c			= CONST_STATUS_OPEN, //CONST_STATUS_DRAFT, 
	                            UnitPrice 			= d.UnitPrice
	                       )
	                   );
	                }
	            }
	        }
            		
    	}else if(psSelect == 'Corporate'){

	        // This function runs when a user hits the "bundle" button
	        for(PricebookEntry d : plPbe){
	            if( d.Product2.Bundled_Service__c != null){
	                if(d.Product2.Bundled_Service__c.contains(psSelect)){
	                	integer liQuantity = 1;
	                	if(d.Product2.Name == CONST_PRODUCT_NAME_WELCOME_DAY){
	                		liQuantity = 10;
	                	}
	                    plOrderItems.add(
	                        new orderItem( 
	                            Quantity 			= liQuantity,
	                            OrderId 			= poOrder.Id, 
	                            PriceBookEntry 		= d, 
	                            PriceBookEntryId 	= d.Id,
	                            Status__c			= CONST_STATUS_OPEN, //CONST_STATUS_DRAFT, 
	                            UnitPrice 			= d.UnitPrice
	                       )
	                   );
	                }
	            }
	        }    		
    	}  
        
        // Set the List of Available Products to be selected
        updateAvailableList();
    }
*/

 
   /*
    * @name         getChosenCurrency
    * @description	Future Requirement?
    *				This is called from the vf page
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
/*
    public String getChosenCurrency(){
        if(pbIsMultiCurrencyOrg){
        	return (String)poOrder.get('CurrencyIsoCode');
        }else{
            return '';        	
        }
    }
*/

  
   /*
    * @name         changePricebook
    * @description	Future Requirement?
    *				This is called from the vf page
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */    
/*    
    public PageReference changePricebook(){
    
        // This simply returns a PageReference to the standard Pricebook selection screen
        // Note that is uses retURL parameter to make sure the user is sent back after they choose

        PageReference ref = new PageReference('/_ui/busop/orderitem/ChooseOrderPricebook/e');
        ref.getParameters().put('id',poOrder.Id);
        ref.getParameters().put('retURL','/apex/orderProductEntry?id=' + poOrder.Id);
        
        return ref;
    }
 */
 
}