@isTest
private class CampaignShareParent_Test {
  
    static Campaign c;
    static Campaign c1;
    static Task t1;
    static Task t2;
    static User uUser;
    static User uUser1;
    static Contact contact;
    static Account a;
    
    static String t1Id;
    static String t2Id;
    
    static{
            Profile pProfile = [select id from profile where name='System Administrator'];
        uUser = new User(alias = 'tst2', email='tst2@btlocalbusiness.co.uk', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = pProfile.Id, timezonesidkey='Europe/London', username='tst2@testemail.com', EIN__c='tst2',isActive=True);
        
        uUser1 = new User(alias = 'tst3', email='tst3@btlocalbusiness.co.uk', 
            emailencodingkey='UTF-8', lastname='Testing3', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = pProfile.Id, timezonesidkey='Europe/London', username='tst3@testemail.com', EIN__c='tst3',isactive=True);
            
        insert uUser;   
        insert uUser1;
    
    List<Campaign_Call_Status__c> ccsToInsert = new List<Campaign_Call_Status__c>();
        Campaign_Call_Status__c ccs1 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status1', Responded__c = true, Default__c=true);
        Campaign_Call_Status__c ccs2 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status2', Responded__c = false, Default__c=false);
        ccsToInsert.add(ccs1);
        ccsToInsert.add(ccs2);
        insert ccsToInsert;
    
        Date enddate = date.today() + 1;
        c = new Campaign(Name='TestCampaign', Type='Telemarketing', Status='Planned', 
            Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, OwnerId=uUser.Id, isActive=true, Data_Source__c='Self Generated');
        insert c;
        
        enddate = date.today() + 2;
        c1 = new Campaign(Name='TestCampaign2', Type='Telemarketing', Status='Planned', 
            parentid = c.id, Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 5, EndDate = enddate, OwnerId=uUser.Id, isActive=true, Data_Source__c='Self Generated');
        insert c1;
  
        
        
       
    }
  
     static testMethod void testCampaignRedirectEdit() {
                
        Test.startTest();   
                
            PageReference pageRef = Page.campaignShareParent;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController camp = new ApexPages.StandardController(c1);

            campaignShareParent controller = new campaignShareParent(camp);
            controller.doEditRedirect();
            controller.doRedirect();
        
     }
}