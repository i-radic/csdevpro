public with sharing class accountView {

    public Account account{get; set;}

    public accountView(ApexPages.StandardController stdController) {
        account = (Account)stdController.getRecord();
    }

    public PageReference onStart() {
        Account a = new Account();
        a = [select Id, CUG__c from Account where Id = :account.Id limit 1];

        PageReference retPageRef = new PageReference('/'+account.Id+'?nooverride=1&page=account&cugid='+a.CUG__c);
        return retPageRef;
    }
}