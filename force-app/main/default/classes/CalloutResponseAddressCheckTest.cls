@isTest
private class CalloutResponseAddressCheckTest {
	@isTest static void test_method_one() {
		// Implement test code
		CalloutResponseAddressCheck controller = new CalloutResponseAddressCheck();
		Map<String, Object> dynamicParameters = controller.getDynamicRequestParameters(new Map<String, Object>());
		Map<String, Object> returnMap = controller.processResponseRaw(dynamicParameters);
		controller.runBusinessRules('test');

		System.assert(controller != null);
		System.assert(dynamicParameters != null);
		System.assert(returnMap != null);
	}

	@IsTest
	private static void testController() {
		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'Test Category');

		csbb.ProductCategory productCategory = new csbb.ProductCategory(prodCategory.Id);

		csbb.CalloutResponse calloutResponse = new csbb.CalloutResponse();
		Map<String, csbb.CalloutResponse> mapCR = new Map<String, csbb.CalloutResponse> {'Test Map' => calloutResponse};
		
		csbb.CalloutProduct.ProductResponse productResponse = new csbb.CalloutProduct.ProductResponse();

		Test.startTest();
		CalloutResponseAddressCheck controller = new CalloutResponseAddressCheck(mapCR, productCategory, productResponse);
		Test.stopTest();

		System.assert(controller != null);
	}

	@IsTest
	private static void testCanOffer() {
		csbb.CalloutProduct.ProductResponse productResponse = new csbb.CalloutProduct.ProductResponse();

		Test.startTest();
		CalloutResponseAddressCheck controller = new CalloutResponseAddressCheck();
		csbb.Result result = controller.canOffer(new Map<String, String>(), new Map<String, String>(), productResponse);
		Test.stopTest();

		System.assert(controller != null);
		System.assert(result == null);
	}
}

/*
	global void runBusinessRules (String categoryIndicator) {
		// Skeleton method, does nothing
	}
	global csbb.Result canOffer (Map<String, String> attMap, Map<String, String> responseFields, csbb.CalloutProduct.ProductResponse productResponse) {
		// Skeleton method, does nothing
		return null;
	}
    
    public class AddressMatchResponse {
        public AddressMatchRes addressMatchRes {get; set;}
    }
    
    public class AddressMatchRes {
        public List<Location> location {get; set;}
    }
    
    public class Location {
        public LocationWrapper location {get; set;}
    }
    
    public class LocationWrapper {
        public Address address {get; set;}
    }
    
    public class Address {
        public AddressDetails addressDetails {get; set;}
    }
    
    public class AddressDetails {
        public String locality {get; set;}
        public String deliveryPoint {get; set;}
        public String SubBuilding {get; set;}
        public String BuildingNumber {get; set;}
        public String postalOrganisation {get; set;}
        public String postOutCode {get; set;}
        public String postInCode {get; set;}
        public String poBox {get; set;}
        public String countyStateProvince {get; set;}
        public String subLocality {get; set;}
        public String Street {get; set;}
        public String SubStreet {get; set;}
        public String building {get; set;}
        public String postCode {get; set;}
        public String city {get; set;}
    }
*/