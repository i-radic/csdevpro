/**************************************************************************************************************************************************
		Class Name : CSATContactFeedbackTriggerHandler
		Test Class Name : Test_CSAT_Feedback_SRM_BM
		Description : Code to Insert and Update Survey Response Records, And to Calculate RollUp Summary, to Update in CSAT Record
		Version : V0.1
		Created By Author Name : BALAJI MS
		Date : 08/09/2017
 *************************************************************************************************************************************************/

public class CSATContactFeedbackTriggerHandler {
    
    public static Boolean BeforeUpdateRecurssionFlag = True;
    public static Boolean AfterUpdateRecurssionflag = True;
    
    public static void BeforeInsertHandler(List<CSAT_Contact_Feedback__c> CSATContactList){
        
        CSATContactFeedbackTriggerHelper.BeforeInsertMethod(CSATContactList);
        
    }
    
    public static void BeforeUpdateHandler(Map<Id, CSAT_Contact_Feedback__c> CSATContactNewMap, Map<Id, CSAT_Contact_Feedback__c> CSATContactOldMap){
        
        CSATContactFeedbackTriggerHelper.BeforeUpdateMethod(CSATContactNewMap, CSATContactOldMap);
        
    }
    
    public static void AfterInsertHandler(Map<Id, CSAT_Contact_Feedback__c> CSATContactNewMap, List<CSAT_Contact_Feedback__c> CSATContactList){
        
        CSATContactFeedbackTriggerHelper.AfterInsertMethod(CSATContactNewMap, CSATContactList);
        
    }
    
    public static void AfterUpdateHandler(Map<Id, CSAT_Contact_Feedback__c> CSATContactNewMap, Map<Id, CSAT_Contact_Feedback__c> CSATContactOldMap, List<CSAT_Contact_Feedback__c> ListOfRec){
        
        CSATContactFeedbackTriggerHelper.AfterUpdateMethod(CSATContactNewMap, CSATContactOldMap, ListOfRec);
    }
    
    public static void AfterDeleteHandler(List<CSAT_Contact_Feedback__c> CSATContactList){
        
        CSATContactFeedbackTriggerHelper.AfterDeleteMethod(CSATContactList);
        
    }
    
    public static void AfterUnDeleteHandler(List<CSAT_Contact_Feedback__c> CSATContactList){
        
        CSATContactFeedbackTriggerHelper.AfterUnDeleteMethod(CSATContactList);
    }
    
}