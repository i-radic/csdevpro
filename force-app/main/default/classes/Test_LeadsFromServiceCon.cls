@isTest(seeAllData=true)
public class Test_LeadsFromServiceCon {
    
    public static testMethod void LFS_test(){
        
        //Account account = Test_Factory.CreateAccount();        
        //Database.SaveResult accountResult = Database.insert(account);
        Opportunity testOpp= new Opportunity();
        testOpp.Product__c = 'Broadband';//'Service Self-fulfil';
        testOpp.LFS_CUG__c='5000005777';
        testOpp.CloseDate=Date.today();
        testOpp.StageName='Assigned';
        testOpp.Name='testOpp';
        testOpp.Business_Name__c='test';
        testOpp.Lead_Source__c='Leads from Service';
        testOpp.Customer_Contact_Salutation__c='';
        testOpp.Tariff__c='Voice';
        testOpp.Handset__c='a';
        testOpp.Revenue__c=25.22;
        testOpp.Originator_EIN__c='609056422';
        testOpp.RecordTypeId='01220000000ABFB';
        testOpp.Outright_Sale__c= true;
        //testOpp.UKBS_Customer__c=true;        
        ApexPages.StandardController controller= new ApexPages.StandardController(testOpp);
        LeadsFromServiceCon lfs = new LeadsFromServiceCon(controller);
        //lfs.O=o;
        lfs.saveOpportunity();
                
        // test non-Broadband cases        
        testOpp.Product__c = 'Service Self-fulfil';        
        lfs.O=testOpp;
        lfs.saveOpportunity();

        lfs.CreateNewOpp();
        lfs.setCookie();
        
    }
}