public with sharing class csbtcl_CS_Holding_Report_JSON_Generator {
	private static String EXCLUDE_VALUE = 'Exclude';
	private static String INCLUDE_VALUE = 'Include';
	public String 		product 	{get;set;}

	public csbtcl_CS_Holding_Report_JSON_Generator() {}

	public static String getJSONHoldingReport(String[] holdingReportCsvStringArray, String product, String pcId){
	    system.debug('**holdingReportCsvStringArray'+holdingReportCsvStringArray);
		Integer subsInReport = 0;
		Integer aveTermRemainingDays = 0;
		Integer termOfContract = 0;
		Integer aveTermOldDeal = 0;
		Decimal aveDaysRemaining = 0;
		Integer aveTermRemaining = 0;
		Integer averageMonthsRemaining = 0;

		List<HoldingReportWrapper> hrwList = new List<HoldingReportWrapper>();
		List<csbtcl_bt_resign_exclusion_list__c> resignExclusionList = csbtcl_bt_resign_exclusion_list__c.getall().values();
		Map<String,csbtcl_bt_resign_exclusion_list__c> exclusionsMap = new Map<String,csbtcl_bt_resign_exclusion_list__c>();
		csbtcl_bt_resign_exclusion_list__c currentExclusionSetting = new csbtcl_bt_resign_exclusion_list__c();

		if( resignExclusionList != null ) {
			for( csbtcl_bt_resign_exclusion_list__c exclusion : resignExclusionList ){
				if( exclusion.product__c == product ) {
					currentExclusionSetting = exclusion;
					break;
				}
			}
		}

		for( String csvLine : holdingReportCsvStringArray ){
		    System.debug('csvLine>>>'+csvLine);
			HoldingReportWrapper hrw = new HoldingReportWrapper( csvLine );
			 System.debug('hrw>>>'+hrw);
			if( currentExclusionSetting.Tariff_Code__c != hrw.tariff )
				hrw.exclusions = INCLUDE_VALUE;
			else
				hrw.exclusions = EXCLUDE_VALUE;
			if( hrw.exclusions != EXCLUDE_VALUE ) {
				hrw.calculateCed();
				hrw.calculateCedDays();
				hrw.calculateTermMTHS();
				aveTermRemainingDays += hrw.daysCed;
			}
            
			subsInReport++;
			termOfContract += hrw.termOfContract;
			averageMonthsRemaining += hrw.termMTHSRemaining;
			hrwList.add( hrw );
		}

		Decimal aveTermRemainingDaysDecimal = Decimal.valueOf(aveTermRemainingDays);
		Decimal subsInReportDecimal = Decimal.valueOf(subsInReport);
		
		system.debug(LoggingLevel.WARN, 'csbtcl_CS_Holding_Report_JSON_Generator.getJSONHoldingReport.subsInReport = ' + subsInReport);
	    system.debug(LoggingLevel.WARN, 'csbtcl_CS_Holding_Report_JSON_Generator.getJSONHoldingReport.aveTermRemainingDays = ' + aveTermRemainingDays);
        aveDaysRemaining = (aveTermRemainingDaysDecimal / subsInReportDecimal).round(System.RoundingMode.HALF_EVEN);
        system.debug(LoggingLevel.WARN, 'csbtcl_CS_Holding_Report_JSON_Generator.getJSONHoldingReport.aveDaysRemaining = ' + aveDaysRemaining);
        //17/05/2018 - ISSUE38791 - Darko switched multiplication with 12 months directly because Math.ceil was always 0 
		aveTermRemaining = Integer.valueOf(((aveDaysRemaining * 12) / 365).round(System.RoundingMode.UP)); 
		system.debug(LoggingLevel.WARN, 'csbtcl_CS_Holding_Report_JSON_Generator.getJSONHoldingReport.aveTermRemaining = ' + aveTermRemaining);
		
		if(aveTermRemaining == 0)
		//	return Label.BT_ResignHurdleNotMet;
		aveTermOldDeal = termOfContract / subsInReport;

		cscfga__Product_Configuration__c pc = new cscfga__product_configuration__c(Id = pcId, Ave_Term_Remaining_Days__c = aveTermRemainingDays, 
			Subs_in_Report__c = subsInReport, Ave_Term_on_old_Deal__c = aveTermOldDeal,Ave_Days_Remaining_Months__c = aveTermRemaining);
		update pc;
		return JSON.serializePretty( hrwList );
	}
}