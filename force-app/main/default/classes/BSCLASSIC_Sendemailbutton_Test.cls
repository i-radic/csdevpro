@isTest
private class BSCLASSIC_Sendemailbutton_Test {

    static testMethod void myUnitTest() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
		System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }
     
       Account a = Test_Factory.CreateAccount();
       a.Name = 'Test';
       a.ownerId = thisUser.id ;
       a.Base_Team_Assign_Date__c=System.today();
       insert a;
       
        Contact c = Test_Factory.CreateContact();
        c.AccountId = a.Id;
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        
       BS_Classic_Sales_log__c bl=new BS_Classic_Sales_log__c();
       bl.Account__c=a.Id;
       bl.Contact__c = c.Id;
       bl.Order_Number__c='test12345678';
       bl.Work_Area__c='Blend';
       bl.Call_Type__c='test1234';
       insert bl;
       
       BS_Classic_Sales_log__c bl2 = [Select Id, Name From BS_Classic_Sales_log__c LIMIT 1]; 
       BSCLASSIC_Sendemailbutton.SENDMAIL(bl2.Id);
    }

}