/*
* This class is the controller behind the ipsRecruitmentController
* wizard. The new wizard is comprised of three pages, each of
* which utilizes the same instance of this controller.
* Date                 Author                  Comment
* 08/11/2011           Rita Opoku-Serebuoh     initial development         
* 16/10/2014           Sridevi Bayyapuneedi    Made changes as per CR6225
* Copyright © 2010  BT.
*/  
public class ipsRecruitmentController {
    // These class variable maintains the state of the wizard.  
    IPS_Recruitment__c ipsRecruitment;
    
    // The method returns the class variables. If this is the first time the method is being called,  
    // a new empty record will be created for the variable.  
    public IPS_Recruitment__c getIPSRecruitment() {
        if(ipsRecruitment == null) ipsRecruitment = new IPS_Recruitment__c();
        return ipsRecruitment;
    }
    
    // The five methods below control the navigation through the wizards & other pages
    //Below code is commented as the page IPSINFO is no longer used in the IPS Recruitment Site
    /*public PageReference info() {
return Page.IPSINFO;
}
*/
    public PageReference step1() {
        return Page.ipsRecruitmentStep1;
    }
    
    public PageReference step2() {
        return Page.ipsRecruitmentStep2;
    }
    
    public PageReference step3() {
        return Page.ipsRecruitmentStep3;
    }
    
    public PageReference step4() {
        return Page.ipsRecruitmentStep4; 
    }
    
    public PageReference step5() {
        return Page.ipsRecruitmentStep5;
    }
    
    public PageReference save() {
        
        // Create the IPS Recruitment.     
        try {
            insert (ipsRecruitment);
        }
        catch(System.DMLException e) {
            ApexPages.addMessages(e);
            return null;
        }
        
        // Redirect to newly created IPS Recruitment record     
        PageReference ipsrecruitmentPage = Page.IPSRecruitmentConfirmation;
        ipsrecruitmentPage.setRedirect(true);
        return ipsrecruitmentPage;
    }
    
    // This method cancels the wizard, and returns the user to the  
    // home page 
    public PageReference validateCompRegDate() {
        DateTime dTime = DateTime.now();   
        DateTime compDate = ipsRecruitment.Company_Registration_Date__c;
        
        if (compDate > dTime) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please ensure Company Registration Date is not in the future'));
            return Null; 
        }        
        
        if (ipsRecruitment.VAT_Registration_No__c != null && ipsRecruitment.VAT_Registration_No__c != ''){
            if(!ipsRecruitment.VAT_Registration_No__c.isNumeric()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'VAT registration number contains non-numerical values. Please correct'));
                return Null; 
            }
        }
        
        if (ipsRecruitment.Company_Registration_No__c != null && ipsRecruitment.Company_Registration_No__c != ''){
            if(!ipsRecruitment.Company_Registration_No__c.isNumeric()) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Company registration number contains non-numerical values. Please correct'));
                return Null; 
            }
        }        
        return Step2();
    } 
    
    // This method verifies at least a checkbox has been selected for step 4
    // before proceeding to the next page.
    public PageReference validateSubmissionReasons(){        
        if ((!ipsRecruitment.EE_Com__c) && (!ipsRecruitment.BT_Com__c) && (!ipsRecruitment.BT_IPS_Campaign__c) &&
            (!ipsRecruitment.BT_Contact__c) && (!ipsRecruitment.Channel_Press__c) && (!ipsRecruitment.Personal_Contacts__c) &&
            (!ipsRecruitment.Other_please_state__c)){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please select at least one of the checkbox options'));
                return Null;}
        
        return Step5();
    }
    
    public PageReference cancel() {
        PageReference homePage = new PageReference('/apex/IPS_Recruitment_HomePage');
        homePage.setRedirect(true);
        return homePage; 
    }
}