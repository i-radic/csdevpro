public class RingCentralDealReg {

    public String uL {get; set;}
    public String lineCheckNumber {get; set;}
    public String tMAX {get; set;}
    public String tADSL2 {get; set;}
    public String tWBC {get; set;}
    public String tFTTC {get; set;}
    public String tFTTP {get; set;}
    public String tFOD {get; set;}
    public String selProduct {get; set;}
    public String accName {get; set;}
    public String cvHeadID {get; set;}
    public String cvID {get; set;}
    public String conId {get; set;}
    public String partnerAgentName {get; set;}
    public String partnerAgentID {get; set;}
    public String company {get; set;}
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public String phone {get; set;}
    public String numberOfEmployees {get; set;}
    public String zipCode {get; set;}
    public RingCentralAPI.DealRegResponse dealRegResponse {get; set;}

    public PageReference SetValues(){
        uL = System.currentPageReference().getParameters().get('users');
        lineCheckNumber = System.currentPageReference().getParameters().get('telNum');
        tMAX = System.currentPageReference().getParameters().get('MAX');
        tADSL2 = System.currentPageReference().getParameters().get('ADSL2');
        tWBC = System.currentPageReference().getParameters().get('WBC');
        tFTTC = System.currentPageReference().getParameters().get('FTTC');
        tFTTP = System.currentPageReference().getParameters().get('FTTP');
        tFOD = System.currentPageReference().getParameters().get('FOD');
        selProduct = System.currentPageReference().getParameters().get('selProduct');
        accName = System.currentPageReference().getParameters().get('accName');
        cvHeadID = System.currentPageReference().getParameters().get('cvHeadID');
        cvID = System.currentPageReference().getParameters().get('cvID');
        conId = System.currentPageReference().getParameters().get('conId');
        
        String userId = UserInfo.getUserId();
        User user = [select ein__c from User where Id =: userId];
        Contact contact = [select firstName, lastname, email, phone from Contact where Id =: conId];
        
        partnerAgentName = Userinfo.getName();  
        partnerAgentID = user.ein__c;
        company = accName;
        firstName = contact.firstName;
        lastName = contact .lastName;
        email = contact.email;
        phone = contact.phone;
        numberOfEmployees = uL;
        
        return SubmitDealRegistration();
    }

    public PageReference SubmitDealRegistration(){
        RingCentralURL__c RSUrl = null;
        if (Test.isRunningTest()){   
            RSUrl = [select Name, Username__c, Password__c, Url__c, Partner_Login_Url__c from RingCentralURL__c where Name = 'testRCurl'];
        }else{
            RSUrl = [select Name, Username__c, Password__c, Url__c, Partner_Login_Url__c from RingCentralURL__c where Name = 'DealRegistration'];
        }
        String url = String.valueOf(RSUrl.Url__c);
        String partnerLoginUrl = String.valueOf(RSUrl.Partner_Login_Url__c);
        String username = String.valueOf(RSUrl.Username__c);
        String password = String.valueOf(RSUrl.Password__c);
        RingCentralAPI api = new RingCentralAPI();
        RingCentralAPI.SessionHeader_element sessionHeader = new RingCentralAPI.SessionHeader_element();
        RingCentralAPI.DealRegistrationSubmissionCls webSvc = new RingCentralAPI.DealRegistrationSubmissionCls(url);
        
        if (Test.isRunningTest()){       
            sessionHeader.sessionId = 'TEST';
        }
        else{
            String sessionId = api.Login(username, password, partnerLoginUrl);       
            sessionHeader.sessionId = sessionId;
        }
        webSvc.timeout_x = 120000;
        webSvc.SessionHeader = sessionHeader;
        dealRegResponse = webSvc.submitDealRegistration(partnerAgentName, partnerAgentID, company, firstName,lastName, email, phone, numberOfEmployees, zipCode);   

        PageReference retPg = null;
        retPg = page.CloudVoiceQuickStart;
        AddPageRefParams(retPg);

        if(dealRegResponse.status.Contains('Failure') || dealRegResponse.approvalStatus.Contains('Rejected')){
            //--------Deal Reg fail/rejected, redirect to ipcStart
            retPg = page.ipcStart;
            //--------Skip showing failure on non production orgs
            if(UserInfo.getOrganizationId() != '00D20000000MNmTEAW'){
                retPg = page.CloudVoiceQuickStart;
            }
            AddPageRefParams(retPg);
        }
        else{
            //--------Store RCDeal_ID
            Cloud_Voice__c cv = [select RCDeal_ID__c from Cloud_Voice__c where Id =: cvHeadID];
            cv.RCDeal_ID__c = dealRegResponse.partnerDealID;
            update cv;  
        }
        return retPg.setRedirect(true);
    }   

    private PageReference AddPageRefParams(PageReference retPg){
        retPg.getParameters().put('users',uL);
        retPg.getParameters().put('telNum',lineCheckNumber);
        retPg.getParameters().put('MAX',tMAX);
        retPg.getParameters().put('ADSL2',tADSL2);
        retPg.getParameters().put('WBC',tWBC);
        retPg.getParameters().put('FTTC',tFTTC);
        retPg.getParameters().put('FTTP',tFTTP);
        retPg.getParameters().put('FOD',tFOD);
        retPg.getParameters().put('selProduct',selProduct);
        retPg.getParameters().put('accName',accName);
        retPg.getParameters().put('cvHeadID',cvHeadID);
        retPg.getParameters().put('cvID',cvID);
        retPg.getParameters().put('conID',conId);
        retPg.getParameters().put('status',dealRegResponse.status);
        retPg.getParameters().put('errorResp',dealRegResponse.errorResp);
        retPg.getParameters().put('approvalStatus',dealRegResponse.approvalStatus);
        retPg.getParameters().put('partnerDealID',dealRegResponse.partnerDealID);
        retPg.getParameters().put('validUntilDate',dealRegResponse.validUntilDate);
        return retPg;
    }
}