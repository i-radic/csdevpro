@isTest
private class ADPLandscapeViewController_Test {

    static testMethod void myUnitTest() {
        //  create test data
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
    	System.runAs ( thisUser ) {
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
        
        TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
        upsert settings TriggerDeactivating__c.Id;
        Account acc = Test_Factory.CreateAccount();
        acc.name = 'TESTCODE 2';
        acc.sac_code__c = 'testSAC';
        acc.Sector_code__c = 'CORP';
        acc.LOB_Code__c = 'LOB';
        acc.OwnerId = u1.Id;
        acc.CUG__c='CugTest';
        insert acc;
        ADP__c adp = new ADP__c(Customer__c = acc.Id);
        insert adp;
        ADP_Landscape__c adpl = new ADP_Landscape__c(ADP__c = adp.Id, Annual_Rental_Cost__c = 100, Annual_Maintenance_Cost__c = 100, Contract_Expiry_Date__c = System.today(), Maintenance_Expiry_Date__c = System.today());
        insert adpl;
        
        //  start Test - Coverage
        Test.startTest();
        
        PageReference pageRef=Page.ADPLandscapeView;
        Test.setCurrentPageReference(pageRef);
        ADPLandscapeViewController myController = new ADPLandscapeViewController(new ApexPages.StandardController(adpl));
        
        List<SelectOption> recordTypes = myController.getRecordTypes();
        myController.selectedRecordTypes = new List<String>{recordTypes[0].getValue()};
        myController.getTypes();
        myController.selectedType = 'Defence';
        myController.getReportParams(true);
        //  commented out as reports reference in controller do not exist in production at time of writing
        //myController.updateReportURL();
        myController.selectedType = 'Acquisition';
        myController.getReportParams(false);
        //  commented out as reports reference in controller do not exist in production at time of writing
        //myController.updateReportURLSpecialist();
        Test.stopTest();
    }
    }
}