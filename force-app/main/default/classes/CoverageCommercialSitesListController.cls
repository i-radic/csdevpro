public with sharing class CoverageCommercialSitesListController
{
    private id coveragecheckid;
    public Coverage_Check__c cc;
    public List<Coverage_Check_Site__c> coverageCommercialSites;
    public  CoverageCommercialSitesListController(ApexPages.StandardController sController)
    {    
         this.coveragecheckid= System.currentPageReference().getParameters().get('id'); 
         cc= [Select id,Name from Coverage_Check__c where id =:System.currentPageReference().getParameters().get('id')];
         System.debug('CC Name'+cc.name);
    }
    public List<Coverage_Check_Site__c> getCoverageCommercialSites() 
    {    
        coverageCommercialSites= [SELECT id,Name,Status__c,post_code__c,Commercial__c ,CER_Required__c,Number_of_Handsets_at_this_location__c,Coverage_2G_Description__c,Coverage_3G_Description__c,Coverage_4G_Description__c from Coverage_Check_Site__c  where Commercial__c = true and Coverage_Check__c =:this.coveragecheckid order by CER_Required__c ]; 
        if (coverageCommercialSites.size() > 0 )       
            return coverageCommercialSites;
        else
            return null;
    }
    public PageReference doNew()
    {
    
        Coverage_Check_Site__c ccSite = new Coverage_Check_Site__c();
        ccSite.Coverage_Check__c = coveragecheckid;
        ccSite.commercial__c = true;
        insert ccSite;
        PageReference pageRef = new PageReference('/'+ccSite.id+'/e?retURL=%2F'+ccSite.id);  
        pageRef.setRedirect(true);
        return pageRef;
    
    }
 }