@isTest
private class FormattedDatetime_Test{
 static testMethod void runPositiveTestCases(){
     User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
     Opportunity newOppty = new Opportunity();
       // newOppty.RecordTypeId = fosRecType.Id;
        Date uDate = date.today().addDays(7);
        newOppty.Name = 'Test';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        /*newOppty.DM_Salutaion__c = 'Mr';
        newOppty.DM_First_Name__c = 'Peter';
        newOppty.DM_Last_Name__c = 'Rabbit';
        newOppty.DM_Contact_Number__c = '22223333333';
        newOppty.EIN_Number__c = '803226959';
        newOppty.Type ='WinBack';
        newOppty.Lead_Source__c = 'Cosine'; */
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});  
        
        Note newNote = new Note();
        newNote.ParentId = oppResult[0].Id;
        newNote.Title = 'test';
        newNote.Body = 'body';
       // newNote.IsPrivate = false;
        insert newNote;
        
     Note newNote1 = [select id,CreatedDate from Note where id=: newNote.id];
 FormattedDatetime controller1 = new FormattedDatetime(); 
     FormattedDatetime.getTimeZoneValue(newNote1.CreatedDate);
 }
}