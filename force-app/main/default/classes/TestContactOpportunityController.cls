@isTest
private class TestContactOpportunityController {
    static testMethod void myUnitTest() {
  
    Account newAccount = new Account (name='XYZ Organization');
    insert newAccount;

    Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',AccountId=newAccount.id);
    insert myContact;
    System.debug('zzzzzzzz'+myContact.Id);
    Opportunity opty = new Opportunity(Name='abc',StageName='Assigned',Product_Family__c='BT FINANCE',
    Amount=1000,CloseDate=System.today(),AccountID=newAccount.id);
    insert opty;
     
    OpportunityContactRole ocr = new OpportunityContactRole();
    ocr.IsPrimary = true;
    ocr.OpportunityId = opty.Id;
    ocr.ContactId = myContact.Id;
    insert ocr;
    
    PageReference pageRef = Page.Contact_Show_All_Account_Opportunity;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('ID',myContact.Id);
    
    ContactOpportunityController myController = new ContactOpportunityController (new ApexPages.StandardController(myContact));
    myController.getAccOppHist();
 
    
}
}