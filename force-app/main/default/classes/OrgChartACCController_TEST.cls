@isTest
private class OrgChartACCController_TEST{
    
    static testMethod void firstTest() {
        Test_Factory.SetProperty('IsTest', 'yes');
 
        Account dummyAccount = Test_Factory.CreateAccountForDummy();
        insert dummyAccount;
        
        testData tt = new testData();
        tt.createADP();
        
        ApexPages.currentPage().getParameters().put('ADPId', tt.testADP.Id);       
        //ApexPages.StandardController stc = new ApexPages.StandardController(tt.testADP);
       // AdpOppyCtrl ADPOpp = new AdpOppyCtrl(stc);


        //test ADP OrgChart
        OrgChartACCController ADPOrg = new OrgChartACCController();
        
        StaticVariables.setAccountDontRun(true);  
        StaticVariables.setContactIsRunBefore(true);     
        Contact con1 = new Contact(LastName = 'Con1'); con1.AccountId = tt.testADP.Customer__c; con1.MailingPostalCode = 'WR5 3RL'; con1.Top_of_Org_Chart__c = True;
        insert con1; 
        Contact con2 = new Contact(LastName = 'Con2'); con2.AccountId = tt.testADP.Customer__c; con2.MailingPostalCode = 'WR5 3RL'; con2.ReportsToId = con1.Id; 
        insert con2;
        Contact con3 = new Contact(LastName = 'Con3'); con3.AccountId = tt.testADP.Customer__c; con3.MailingPostalCode = 'WR5 3RL'; con3.ReportsToId = con2.Id; 
        insert con3;
        Contact con4 = new Contact(LastName = 'Con4'); con4.AccountId = tt.testADP.Customer__c; con4.MailingPostalCode = 'WR5 3RL'; con4.ReportsToId = con3.Id; 
        insert con4;
        Contact con5 = new Contact(LastName = 'Con5'); con5.AccountId = tt.testADP.Customer__c; con5.MailingPostalCode = 'WR5 3RL'; con5.ReportsToId = con4.Id; 
        insert con5;
        Contact con6 = new Contact(LastName = 'Con6'); con6.AccountId = tt.testADP.Customer__c; con6.MailingPostalCode = 'WR5 3RL'; con6.ReportsToId = con5.Id; 
        insert con6;
        Contact con7 = new Contact(LastName = 'Con7'); con7.AccountId = tt.testADP.Customer__c; con7.MailingPostalCode = 'WR5 3RL'; con7.ReportsToId = con6.Id; 
        insert con7;     
        Contact con8 = new Contact(LastName = 'Con8'); con8.AccountId = tt.testADP.Customer__c; con8.MailingPostalCode = 'WR5 3RL'; con8.ReportsToId = con7.Id; 
        insert con8;                
        
        
        
        ADPOrg.getContacts();
        ADPOrg.getCon();
        ADPOrg.getCon1();
        ADPOrg.getCon2();
        ADPOrg.getCon3();   
        ADPOrg.getCon4();   
        ADPOrg.getCon5();   
        ADPOrg.getCon6();   
        ADPOrg.getCon7();   
                       
    }
    
    
    public class testData {
    
        public Account testAccount = null;
        public ADP__c testADP = null;
        
        public void createAccount() {
            String SACCODE = 'testSAC';
            String LOB='LOB';
            
            Account testAcc2 = Test_Factory.CreateAccount();
            testAcc2.name = 'TESTCODE 2';
            testAcc2.sac_code__c = SACCODE;
            testAcc2.Sector_code__c = 'CORP';
            testAcc2.LOB_Code__c = LOB;
            testAcc2.CUG__c = 'jmmCUG1';
            Database.SaveResult[] accResult2 = Database.insert(new Account[] {testAcc2});
            
            testAccount = [Select id, CUG__c, Name from Account where Id= :accResult2[0].getId()];      
        }
        
        public void createADP() {
            if(testAccount == null)createAccount();
            ADP__c thisADP = new ADP__c(Customer__c = testAccount.Id);
            
            Database.SaveResult[] adpRes = Database.insert(new ADP__c[] {thisADP});
            testADP = [Select id,Customer__c, Name from ADP__c where Id= :adpRes[0].getId()];
        }
    }
}