@isTest
private class CS_Test_ProductConfigEditorController {
    static cscfga__Product_Configuration__c generateTestData(String prodDefName, Boolean generateProductConfiguration) {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = prodDefName,
            cscfga__Description__c = 'Test',
            cscfga__Active__c = true);
        insert pd;

        cscfga__Configuration_Screen__c cs = new cscfga__Configuration_Screen__c(
            Name = 'CPanel',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Index__c = 1,
            cscfga__Label__c = 'CPanel');
        insert cs;

        cscfga__Screen_Section__c ss = new cscfga__Screen_Section__c(
            Name = 'ScreenTop',
            cscfga__Configuration_Screen__c = cs.Id,
            cscfga__Index__c = 1,
            cscfga__Label__c = 'ScreenTop');
        insert ss;
        
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        insert new List<cscfga__Attribute_Definition__c>{
            new cscfga__Attribute_Definition__c(
                Name = 'Thingamabob 1',
                cscfga__Product_Definition__c = pd.Id,
                cscfga__Row__c = 0,
                cscfga__Column__c = 0),
            new cscfga__Attribute_Definition__c(
                Name = 'Thingamabob 2',
                cscfga__Product_Definition__c = pd.Id,
                cscfga__Row__c = 0,
                cscfga__Column__c = 1),
            new cscfga__Attribute_Definition__c(
                Name = 'Thingamabob 3',
                cscfga__Product_Definition__c = pd.Id),
            new cscfga__Attribute_Definition__c(
                Name = 'Thingamabob 4',
                cscfga__Product_Definition__c = pd.Id,
                cscfga__Row__c = 1,
                cscfga__Column__c = 1,
                cscfga__Configuration_Screen__c = cs.Id,
                cscfga__Screen_Section__c = ss.Id)
        };

        if (!generateProductConfiguration)
            return null;

        Account acc = new Account(Name = 'Dummy');
        insert acc;

        Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = acc.Id, StageName = 'Pending', CloseDate = Date.today());
        insert opp;

        cscfga__Product_Bundle__c pbun = new cscfga__Product_Bundle__c(Name = 'Test Bundle', cscfga__Opportunity__c = opp.Id);
        insert pbun;

        cscfga__Configuration_Offer__c co = new cscfga__Configuration_Offer__c(Name = 'Test Offer');
        insert co;

        cscfga__Product_Basket__c pbas = new cscfga__Product_Basket__c(Name = 'Test Basket', cscfga__Opportunity__c = opp.Id);
        insert pbas;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
            Name = 'TestProdConfig',
            cscfga__Description__c = 'Test',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Product_Bundle__c = pbun.Id,
            cscfga__Configuration_Offer__c = co.Id,
            cscfga__Product_Basket__c = pbas.Id);
        insert pc;

        return pc;
    }

    static testmethod void instantiateController() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        System.assertNotEquals(null, ctrlr, 'Controller should be instantiated.');

        ctrlr.isEditable = true;
    }

    static testmethod void attributeEmptyTableTest() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        List<List<CS_ProductConfigEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
        System.assertEquals(0, attributeTable.size());
    }

    static testmethod void attributeTableTestWithProdDef() {
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        String prodDefName = 'Credit Funds';
        generateTestData(prodDefName, false);

        ctrlr.prodDefName = prodDefName;
        System.assertEquals(prodDefName, ctrlr.prodDefName);
        System.assertEquals('New ' + prodDefName, ctrlr.prodConfEditorTitle);
        System.assertEquals('Save New ' + prodDefName, ctrlr.saveButtonText);
        System.assertEquals('Clear Editor', ctrlr.cancelButtonText);

        List<List<CS_ProductConfigEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }

    static testmethod void attributeTableTestWithProdConf() {
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;
        ctrlr.columnsForDisplay = 'Type__c';

        System.assertEquals('Currently editing: [' + pc.Name + ']', ctrlr.prodConfEditorTitle);
        System.assertEquals('Save Changes', ctrlr.saveButtonText);
        System.assertEquals('Cancel Changes', ctrlr.cancelButtonText);

        List<cscfga__Product_Configuration__c> confList = ctrlr.productConfigList;

        List<List<CS_ProductConfigEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }
/*
    static testmethod void saveWithProductDefinitionTest() {
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodBasketId = pc.cscfga__Product_Basket__c;

        List<List<CS_ProductConfigEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;

        ctrlr.saveProdConfig();
    }
*/
    static testmethod void saveWithProductConfigurationTest() {
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;

        List<List<CS_ProductConfigEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;

        ctrlr.saveProdConfig();
    }

    static testmethod void editTest() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();
        ctrlr.edit();
    }

    static testmethod void clearTest() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();
        ctrlr.clearEditor();
        System.assertEquals(null, ctrlr.prodConfigId);
        System.assertEquals(null, ctrlr.prodConfig);
    }

    static testmethod void deleteOpenPCTest() {
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;
        List<cscfga__Product_Configuration__c> confList = ctrlr.productConfigList;

        ctrlr.deleteOpenProdConfig();

        List<List<CS_ProductConfigEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }

    static testmethod void deleteSelectedPCTest() {
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;
        ctrlr.prodConfigIdToDelete = pc.Id;
        List<cscfga__Product_Configuration__c> confList = ctrlr.productConfigList;

        ctrlr.deleteProdConfig();

        List<List<CS_ProductConfigEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }

    static testmethod void formatFieldMessageTest() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_ProductConfigEditorController ctrlr = new CS_ProductConfigEditorController();
        cscfga.FieldMessage fm = new cscfga.FieldMessage('TestReference_0', 'TestMessage');

        System.assertEquals('Error on field [TestReference]: TestMessage', ctrlr.formatFieldMessage(fm));
    }
}