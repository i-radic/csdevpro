@isTest
private class CS_Test_Customisations
{
    private static cscfga__Product_Configuration__c config;
	private static cscfga__Product_Configuration__c subConfig;
    private static void createTestData(Boolean sme)
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;

		if(sme) {
			createProductConfigurationsSME(testBasket.Id);
		}
		else {
        	createProductConfigurations(testBasket.Id);
		}
        
    }

    private static void createProductConfigurations(Id basketId)
    {
        cscfga__Product_Definition__c prodDef =
            new cscfga__Product_Definition__c
                ( Name = 'Mobile Voice'
                , cscfga__Description__c = 'Test helper' );
        insert prodDef;

        cscfga__Product_Definition__c prodDefNew =
            new cscfga__Product_Definition__c
                ( Name = 'User Group'
                , cscfga__Description__c = 'Test helper' );
        insert prodDefNew;

        config = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = prodDef.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12 );
        insert config;

        subConfig =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = basketId
                , cscfga__Product_Definition__c = prodDefNew.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12
                , cscfga__Root_Configuration__c = config.Id
                , cscfga__Parent_Configuration__c = config.Id );
        insert subConfig;
         CS_Customisations__c settings = new CS_Customisations__c(
         	Classes__c = 'CS_Customiser,CS_RollupCustomiser'
         );
         insert settings;
        
    }
    
    private static void createProductConfigurationsSME(Id basketId)
    {
        cscfga__Product_Definition__c prodDef =
            new cscfga__Product_Definition__c
                ( Name = 'Mobile Voice SME'
                , cscfga__Description__c = 'Test helper' );
        insert prodDef;

        cscfga__Product_Definition__c prodDefNew =
            new cscfga__Product_Definition__c
                ( Name = 'User Group SME'
                , cscfga__Description__c = 'Test helper' );
        insert prodDefNew;

        config = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = prodDef.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12 );
        insert config;

        subConfig =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = basketId
                , cscfga__Product_Definition__c = prodDefNew.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12
                , cscfga__Root_Configuration__c = config.Id
                , cscfga__Parent_Configuration__c = config.Id );
        insert subConfig;
         CS_Customisations__c settings = new CS_Customisations__c(
         	Classes__c = 'CS_Customiser,CS_RollupCustomiser'
         );
         insert settings;
        
    }

    private static testMethod void testRollupCustomizer()
    {
        createTestData(false);
        CS_Customisations customisations = new CS_Customisations();
        cscfga.API_1.ApiSession session = cscfga.API_1.getApiSession(config);
        customisations.beforeSave(session.getController());
        customisations.afterSave(session.getController());
        customisations.getTopComponentHtml(session.getController());
        customisations.redirectAfterFinish(session.getController());
        customisations.redirectAfterCancel(session.getController());
        customisations.getTopComponent();
        customisations.getTopComponentHtml();
        
        //session = cscfga.API_1.getApiSession(subConfig);
        customisations.beforeSave(session.getController());
        customisations.afterSave(session.getController());
        customisations.getTopComponentHtml(session.getController());
        customisations.redirectAfterFinish(session.getController());
        customisations.redirectAfterCancel(session.getController());

        CS_Customiser customiser = new CS_Customiser();
        customiser.beforeSave(session.getController());
        customiser.afterSave(session.getController());
        customiser.getTopComponentHtml(session.getController());
        customiser.redirectAfterFinish(session.getController());
        customiser.redirectAfterCancel(session.getController()); 
    }
    
    private static testMethod void testGetConfigurationMsgData()
    {
        createTestData(false);
        CS_Customiser customiser = new CS_Customiser();
        cscfga.API_1.ApiSession session = cscfga.API_1.getApiSession(config);
        customiser.getConfigurationMsgData(session.getConfiguration().getAttributes(), 'Mobile Voice');
    }
    
    private static testMethod void testGetConfigurationMsgDataSME() {
    	createTestData(true);       
        Profile p = [SELECT Id FROM Profile WHERE Name Like '%BTLB%' Limit 1];
        User u1 = new User(Alias = 'testusr', Country = 'United Kingdom',
        				   Email = 'testusr@bt.test.com',
        				   EmailEncodingKey='UTF-8',
        				   LastName='Testing',
        				   LanguageLocaleKey='en_US',
        				   LocaleSidKey='en_US',
        				   ProfileId = p.Id,
        				   TimeZoneSidKey = 'Europe/London',
        				   EIN__c = String.valueOf(System.now().getTime()).substring(0, 9),
        				   UserName='testusr@bt.test.com' + System.now().getTime());
        insert u1;
        System.runAs(u1){
        	CS_Customiser customiser = new CS_Customiser();
        	cscfga.API_1.ApiSession session = cscfga.API_1.getApiSession(config);
        	customiser.getConfigurationMsgData(session.getConfiguration().getAttributes(), 'Mobile Voice SME');
        }
    }
    
    private static testMethod void testGetConfigurationMsgDataCfg()
    {
        createTestData(false);
        CS_Customiser customiser = new CS_Customiser();
        cscfga.API_1.ApiSession session = cscfga.API_1.getApiSession(config);
        customiser.getConfigurationMsgData(session.getConfiguration().getSobject());
    }
    
    private static testMethod void testGetConfigurationMsgDataCfgSME()
    {
        createTestData(true);      
        Profile p = [SELECT Id FROM Profile WHERE Name Like '%BTLB%' Limit 1];
        User u1 = new User(Alias = 'testusr', Country = 'United Kingdom',
        				   Email = 'testusr@bt.test.com',
        				   EmailEncodingKey='UTF-8',
        				   LastName='Testing',
        				   LanguageLocaleKey='en_US',
        				   LocaleSidKey='en_US',
        				   ProfileId = p.Id,
        				   TimeZoneSidKey = 'Europe/London',
        				   EIN__c = String.valueOf(System.now().getTime()).substring(0, 9),
        				   UserName='testusr@bt.test.com' + System.now().getTime());
        insert u1;
        System.runAs(u1){
        	CS_Customiser customiser = new CS_Customiser();
        	cscfga.API_1.ApiSession session = cscfga.API_1.getApiSession(config);
        	customiser.getConfigurationMsgData(session.getConfiguration().getSobject());
        }
    }
}