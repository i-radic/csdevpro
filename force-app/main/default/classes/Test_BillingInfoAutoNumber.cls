@isTest

    private class Test_BillingInfoAutoNumber{
    
    static testMethod void myUnitTest(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    Opportunity o = new Opportunity();
    o.Name='TEST';
    o.stageName = 'FOS Stage';
    o.CloseDate = system.today();
    insert o;
    
    Integer k=0;
    
    CRF__c crf = new CRF__c();    
    crf.Opportunity__c = o.Id;
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Movers'].Id;
    crf.Contact__c = c.Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;
    
    Billing_Information__c LM = new Billing_Information__c();
    LM.Related_To_CRF__c = crf.Id;
    LM.Name = '1';
    insert LM;
    
    Billing_Information__c LM1 = new Billing_Information__c();
    LM1.Related_To_CRF__c = crf.Id;
    LM1.Name = '2';
    insert LM1;
    
    //LM.Name='NewName';
    //update LM;
    
    delete LM;
    }
}