@isTest(SeeAllData=true)
private class Test_LeaseDeskAPI {

    static testMethod void UnitTest() {
        RunTest('Yes');
        RunTest('No'); 
    }
    
    static void RunTest(String isTest) {
        Test_Factory.SetProperty('IsTest', isTest);
        LeaseDeskAPI api = new LeaseDeskAPI();        
        LeaseDeskAPI.ContactType contactType = new LeaseDeskAPI.contactType();
        contactType.name = 'TEST';
        contactType.phoneNumber = 'TEST';
        contactType.emailAddress = 'TEST';
               
        List<LeaseDeskAPI.Company> queryCompanies = api.QueryCompanies('TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST', 'TEST');
        String createCompany = api.CreateCompany('TEST', 'TEST', 'TEST', contactType, 'TEST', 'TEST', 'TEST', 'TEST', 'TEST');
        LeaseDeskAPI.QuickDecisionResponse quickDecision = api.QuickDecision('TEST', 'TEST', 'TEST', 'TEST', 0, 0, LeaseDeskAPI.Funder.GE, 'TEST', LeaseDeskAPI.ProfileType.LB);
        String createProposal = api.CreateProposal('TEST', 'TEST', 'TEST', 'TEST', 0);
        String checkProposal = api.CheckProposal('TEST', 'TEST', 'TEST', 'TEST', LeaseDeskAPI.ProfileType.LB);
        List<LeaseDeskAPI.LeasingDetails> getLeasingHistory = api.GetLeasingHistory('TEST', 'TEST', 'TEST');
        LeaseDeskAPI.HoldingProposalResponse holdingProposal = api.CreateHoldingProposal('TEST', 'TEST', 'TEST', 'TEST', 0, 0);
        
        
        HttpRequest req = api.GetHttpRequest();
        HTTPResponse resp = new HttpResponse();
        LeaseDeskAPI.Company company = new LeaseDeskAPI.Company();
        String testBody = '<CompanyDetails>'
                            + '<Test></Test>'   
                        + '</CompanyDetails>';
                
        resp.SetBody(testBody);     
        
        XmlDom doc = new XmlDom(resp.getBody());
        XmlDom.Element[] elements = doc.getElementsByTagName('CompanyDetails');
        for (XmlDom.Element element : elements){
                company = api.toCompany(element);
        }
        
        LeaseDeskAPI.LeasingDetails leasingDetails = new LeaseDeskAPI.LeasingDetails();
        testBody = '<LeasingDetails>'
                    + '<Test></Test>'
                 + '</LeasingDetails>';
                
        resp.SetBody(testBody);     
        
        doc = new XmlDom(resp.getBody());
        elements = doc.getElementsByTagName('LeasingDetails');
        for (XmlDom.Element element : elements){
            leasingDetails = api.toLeasingDetails(element);
        }       
    }
}