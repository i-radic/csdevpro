/** 
 * Clones configuration structure. 
 *
 * @author  Kristijan K
 * 
 */
public with sharing class CS_ConfigurationCopyHelper {
	
	public static CS_AfterCopyEventHandler aceHandler;

	/**
	 * clones configuration structure and invokes after copy events
	 * @param List<Id> childConfigurationIds list of child configurations
	 * @param Id basketId Id of target basket
	 * @param Id configurationId root configuration
	 */
	public static void cloneConfiguration(List<Id> childConfigurationIds, Id basketId, Id configurationId) {
		Map<Id, cscfga__Product_Configuration__c> configMap = new Map<Id, cscfga__Product_Configuration__c>([
			select id
			from cscfga__Product_Configuration__c
			where id in :childConfigurationIds
			or cscfga__Parent_Configuration__c in :childConfigurationIds
			or cscfga__Root_Configuration__c in :childConfigurationIds]
		);
		cscfga__Product_Configuration__c rootConfiguration = [
			select id, Clone_In_Progress__c
			from cscfga__Product_Configuration__c
			where id = :configurationId
		];
		
		rootConfiguration.Clone_In_Progress__c = true;
		update rootConfiguration;
		
		aceHandler = new CS_AfterCopyEventHandler();
		if (configMap.size() > 5) {
			cscfga.ProductConfigurationBulkActions.createCopyBuilder(configMap.keySet())
				.target(null, basketId)
				.eventHandler(aceHandler)
				.noRevalidation()
				.runBatch();
		} else {
			cscfga.ProductConfigurationBulkActions.createCopyBuilder(configMap.keySet())
				.target(null, basketId)
				.eventHandler(aceHandler)
				.noRevalidation()
				.run();
		}
		
		
	}
	
}