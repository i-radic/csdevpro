@isTest
private class CVRemoteActionsSolutionConsole_Test {

    private static testMethod void testCS_SendToService() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        Opportunity opp = new Opportunity(AccountId = oneAccount.Id, StageName = 'Created');
        insert opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id, cscfga__Opportunity__c = opp.Id);
        insert basket;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'CS_SendToService');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        Object message = returnMap.get('status');

        system.assertEquals(String.valueOf(message) == 'ok', true);
    }

    private static testMethod void testCS_TakeOwnership() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id);
        insert basket;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'CS_TakeOwnership');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        Object message = returnMap.get('status');
        system.debug(message);
        system.debug(String.valueOf(message));

        system.assertEquals(String.valueOf(message) == 'ok', true);
    }
    private static testMethod void testCS_SaveTSV() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id);
        insert basket;
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(Total_Solution_Value__c = 50, cscfga__Product_Basket__c = basket.id);
        insert config;
        csord__Solution__c sol = new csord__Solution__c(
                Name = 'Test Solution',
                cssdm__total_one_off_charge__c = 0.0,
                cssdm__total_recurring_charge__c = 0.0,
                csord__Identification__c = 'Sol'
        );
        insert sol;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('configId', config.Id);
        inputMap.put('action', 'CS_SaveTSV');
        inputMap.put('solutionId', sol.Id);
        inputMap.put('Total Solution Value', 50.00);
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
    }
    private static testMethod void testCS_SynchronizeWithOpportunity() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id);
        insert basket;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'CS_SynchronizeWithOpportunity');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        Object message = returnMap.get('status');
        system.debug(message);

        system.assertEquals(true, true);
    }
    private static testMethod void testCS_LookupOpportunitySolutionConsole() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        Opportunity opp = new Opportunity(AccountId = oneAccount.Id, StageName = 'Created');
        insert opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id, cscfga__Opportunity__c = opp.Id);
        insert basket;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'CS_LookupOpportunitySolutionConsole');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        Object message = returnMap.get('oppStage');

        system.assertEquals(String.valueOf(message) == 'Created', true);
    }
    private static testMethod void testgetConfigInfoRemote() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id);
        insert basket;
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(Total_Solution_Value__c = 50, cscfga__Product_Basket__c = basket.id);
        insert config;

        String speed = config.BTnet_Primary_Port_Speed__c;
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('configID', config.Id);
        inputMap.put('action', 'getConfigInfoRemote');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        system.debug(returnMap);
        system.assertEquals(speed, returnMap.get('speed'));
    }
    private static testMethod void testCS_LookupQueryForAccountDetails() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id);
        system.debug(basket);
        insert basket;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'CS_LookupQueryForAccountDetails');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        Object message = returnMap.get('DataFound');

        system.assertEquals(message, true);
    }
    private static testMethod void testCS_SolutionConsoleBasketCalculation() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id);
        insert basket;
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(Total_Solution_Value__c = 50, cscfga__Product_Basket__c = basket.id);
        insert config;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'CS_SolutionConsoleBasketCalculation');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        Object message = returnMap.get('DataFound');

        //system.assertEquals(message, true);
    }
    private static testMethod void testCS_SolutionConsoleDesyncBasket() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id,Synchronised_with_Opportunity__c=true);
        insert basket;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'CS_SolutionConsoleDesyncBasket');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);

        cscfga__Product_Basket__c changedBasket = [select id,Synchronised_with_Opportunity__c from cscfga__Product_Basket__c where Id=:basket.Id ];
        system.assertEquals(basket.Synchronised_with_Opportunity__c, basket.Synchronised_with_Opportunity__c);
    }

    private static testMethod void testCS_LookupQueryForSolutionConsole() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        cspmb__Price_Item__c item = new cspmb__Price_Item__c(Name = 'item', Module__c = 'mod', Grouping__c = 'grp', cspmb__Contract_Term__c = '12');
        insert item;
        system.debug(item);
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('module', 'mod');
        inputMap.put('grouping', 'grp');
        inputMap.put('contractTerm', '12');
        inputMap.put('action', 'CS_LookupQueryForSolutionConsole');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        system.debug(returnMap);
        Object message = returnMap.get('PriceItemId');

        system.assertEquals(item.Id, item.Id);
    }
    private static testMethod void testCS_BTNetLookupQueryForSolutionConsole() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        cspmb__Price_Item__c item = new cspmb__Price_Item__c(Name = 'item', Module__c = 'Access', Grouping__c = 'Bearer', cspmb__Contract_Term__c = '36',
        Access_Type__c = 'LA', Bearer_Speed__c = 1000, Geography__c = 'National', cspmb__Is_Active__c = true);
        insert item;
        system.debug(item);
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('accessType', 1000);
        inputMap.put('contractTerm', '36');
        inputMap.put('geography', 'National');
        inputMap.put('standardLA', 'LA');
        inputMap.put('action', 'CS_BTNetLookupQueryForSolutionConsole');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        system.debug(returnMap);
        Object message = returnMap.get('PriceItemName');

        system.assertEquals(item.Id, item.Id);
    }
    private static testMethod void testCS_BTNetBandwidthForSolutionConsole() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        cspmb__Price_Item__c item = new cspmb__Price_Item__c(Name = 'item', Bandwidth__c = 1000);
        insert item;
        system.debug(item);

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('piId', item.Id);
        inputMap.put('action', 'CS_BTNetBandwidthForSolutionConsole');

        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        system.debug(returnMap);
        Object message = returnMap.get('piId');
    }
    private static testMethod void testgetProfileInfoRemote() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id,Synchronised_with_Opportunity__c=true);
        insert basket;

        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('basketId', basket.Id);
        inputMap.put('action', 'getProfileInfoRemote');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        system.debug(returnMap);
        Object message = returnMap.get('exception');


    }
    private static testMethod void testBadData() {
        Map<String, Object> inputMap = new Map<String, Object>();
        integer errorCount=0;

        inputMap.put('action', 'CS_SendToService');
        Map<String, Object> returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        if(returnMap.get('status')=='error') errorCount+=1;

        inputMap.put('action', 'CS_TakeOwnership');
        returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        if(returnMap.get('status')=='error') errorCount+=1;

        inputMap.put('action', 'CS_SynchronizeWithOpportunity');
        returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        if(returnMap.get('status')=='error') errorCount+=1;

        inputMap.put('action', 'getConfigInfoRemote');
        returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        if(returnMap.get('status')=='error') errorCount+=1;

        inputMap.put('action', 'CS_LookupQueryForAccountDetails');
        returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        if(returnMap.get('status')=='error') errorCount+=1;

        inputMap.put('action', 'CS_LookupQueryForSolutionConsole');
        returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        if(returnMap.get('status')=='error') errorCount+=1;

        inputMap.put('action', 'getProfileInfoRemote');
        returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        if(returnMap.get('status')=='error') errorCount+=1;

        system.debug(errorCount);
    }
    
    private static testMethod void testgetSharedVAS() {
        Map<String, Object> returnMap = new Map<String, Object>();
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        Account oneAccount = CS_TestDataFactory.generateAccount(true, 'Test Account');
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = oneAccount.Id,Synchronised_with_Opportunity__c=true);
        INSERT basket;
        cspmb__Price_Item__c servicePlan = CS_TestDataFactory.generatePriceItem(TRUE, 'Test Service Plan');
        cspmb__Add_On_Price_Item__c sharedVASAddOn = CS_TestDataFactory.generateAddOnPriceItem(FALSE, 'Test VAS');
        sharedVASAddOn.cspmb__Is_Active__c = TRUE;
        sharedVASAddOn.Minimum_Connections__c = 250;
        sharedVASAddOn.Maximum_Connections__c = 500;
        INSERT sharedVASAddOn;
        cspmb__Price_Item_Add_On_Price_Item_Association__c priceItemAddOnAssoc = CS_TestDataFactory.generatePriceItem(FALSE, sharedVASAddOn, servicePlan);
        priceItemAddOnAssoc.Module__c = 'Shared VAS';
        priceItemAddOnAssoc.Add_On_Type__c = 'Annual';
        priceItemAddOnAssoc.cspmb__Group__c = 'Total Resource';
        INSERT priceItemAddOnAssoc;
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('action', 'getSharedVAS');
        inputMap.put('servicePlanId', servicePlan.Id);
        inputMap.put('addOnType', 'Annual');
        inputMap.put('addOngroup', 'Total Resource');
        inputMap.put('noOfUsers', 250);
        Test.startTest();
            returnMap = CVRemoteActionsSolutionConsole.getData(inputMap);
        Test.stopTest();
        System.assertEquals(FALSE, returnMap.containsKey('status'));
    }
}