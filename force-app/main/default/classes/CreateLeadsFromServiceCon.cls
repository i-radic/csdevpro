public class CreateLeadsFromServiceCon {
    Public Lead CreateLead{get;set;}
    Public string Tariff{get;set;}
    Public string Handset{get;set;}
    Public string OppGroup{get;set;}
    Public string DiceAccountNumber{get;set;}
    public string Corporate{get;set;}
    public string OutrightSale{get;set;}
    public string ResignConnections{get;set;}
    public string ContractTerm{get;set;}
    public string AddtoFleetConnections{get;set;}
    public string OrderReference{get;set;}
    public string BBTelephoneNumber{get;set;} 
    public string ConLastName {get;set;}
    public string CompanyName {get;set;}
    public string AnnualRevenue{get;set;}
    public string Subscription{get;set;}
    public Opportunity O {get;set;}
    public Opportunity Opp {get;set;}
    public String oppstageName{get;set;}
    public Boolean ShowOppId{get;set;}
    public Boolean ShowLeadId{get;set;}
    public Boolean ShowFormId{get;set;}
    public Boolean ShowError{get;set;}
    
    public CreateLeadsFromServiceCon(ApexPages.StandardController stdController){
        ShowFormId = TRUE;
        ShowError = False;
        this.CreateLead = (Lead)stdController.getRecord();
        List<RecordType> LeadRType =[select id from RecordType where 
                                     DeveloperName = 'Leads_from_Service' 
                                     AND SobjectType = 'Lead'];
        if(LeadRType.size()>0)
        CreateLead.RecordTypeId=LeadRType[0].id;
        
        Cookie Sitecookie = ApexPages.currentPage().getCookies().get('SiteName');
        Cookie EINcookie = ApexPages.currentPage().getCookies().get('EIN');
        Cookie OEmailcookie = ApexPages.currentPage().getCookies().get('OEmail');
        if (Sitecookie != null) {
            CreateLead.Service_Site__c = Sitecookie.getValue();        
        }
        if (EINCookie != null) {
            CreateLead.LFS_EIN__c = EINCookie.getValue();        
        }if (OEmailcookie != null) {
            CreateLead.LFS_Originator_Email__c = OEmailCookie.getValue();        
        }
    }
    
    
    public List<selectOption> getTarrifValues(){
        List<selectOption> TarrifVal = new List<selectOption>();
        TarrifVal.add(new selectoption('--Select--','--Select--'));
        TarrifVal.add(new selectoption('Voice','Voice'));
        TarrifVal.add(new selectoption('Voice and Data','Voice and Data'));
        TarrifVal.add(new selectoption('Voice and BIS','Voice and BIS'));
        TarrifVal.add(new selectoption('Voice and BES','Voice and BES'));
        TarrifVal.add(new selectoption('Data MBP','Data MBP'));
        TarrifVal.add(new selectoption('Data IPad','Data IPad'));
        TarrifVal.add(new selectoption('Data Other','Data Other'));
        TarrifVal.add(new selectoption('Single Assist £0.00','Single Assist £0.00'));
        TarrifVal.add(new selectoption('Single Assist £6.00','Single Assist £6.00'));
        return TarrifVal;
    }
    public void saveLead(){
       
        if(CreateLead.LFS_CUG__c!=null){
        if(!CreateLead.LFS_CUG__c.contains('CUG')){
        	CreateLead.LFS_CUG__c = 'CUG'+CreateLead.LFS_CUG__c;
        }
        }
        List<Account> A = [select id from Account Where CUG__c =: CreateLead.LFS_CUG__c Limit 1];
        if (A.size() >0){
           CreateLead.Account__C = A[0].id; 
        }
        
        if(CreateLead.Product_Group__c != 'Service Self-fulfil' && CreateLead.Product_Group__c != 'Mobile Self Fulfil' && CreateLead.preSales_User_EIN__c == null ){
            CreateLead.LastName=ConLastName;
            CreateLead.Company=CompanyName;                
            CreateLead.Rating = 'LFS Lead';                        
            List<RecordType> RType =[select id from RecordType where 
                                     DeveloperName = 'Leads_from_Service' 
                                     AND SobjectType = 'Lead'];
            if(RType.Size() > 0){            
                CreateLead.RecordTypeId = RType[0].id;
            }
            CreateLead.LeadSource = 'Leads from Service'; 
            if(CreateLead.Salutation == '' || CreateLead.Salutation == '--None--')
                CreateLead.Salutation = '';
            CreateLead.Sub_Status__c = 'Email to be Sent';        
            if (CreateLead.Product_Group__c == 'Broadband')
            {
                CreateLead.Description = 'Broadband Telephone Number: ' + BBTelephoneNumber + ' \n Lead Info Notes: ' + CreateLead.Description;
            }
            else
            {
                CreateLead.Description = CreateLead.Description;
            }             
            
            if(CreateLead.id == null){               
                insert CreateLead;             
            }        
            
            List<Lead> LeadCreated = [select Name,Service_site__c,LFS_Originator_Email__c,LFS_EIN__c
                                      from Lead Where Id=:CreateLead.id];
            if(LeadCreated.size() >0){                                
                CreateLead = LeadCreated[0];
                ShowLeadId = TRUE;
                ShowOppId = FALSE;
                ShowFormId = FALSE;
            }
        }else{
            List<RecordType> RType =[select id from RecordType where 
                                     DeveloperName = 'Leads_from_Service' 
                                     AND SobjectType = 'Opportunity'];
            
            O = new Opportunity();
            O.Name = 'LFS_'+CompanyName;
            O.Lead_Indicator__c = 'LFS Lead';
            O.CloseDate=Date.today();
            if(RType.Size() > 0){            
                O.RecordTypeId = RType[0].id;
            }
            o.Sales_Agent_EIN__c = CreateLead.preSales_User_EIN__c;
            o.Product__c = CreateLead.Product_Group__c;
            o.Sub_Category__c = CreateLead.Product_Group_Type__c;
            o.Service_Site__c = CreateLead.Service_Site__c; 
            o.Customer_Type__c = CreateLead.Customer_Type__c;
            o.Send_Email_to_Mobile_Team__c = CreateLead.Send_Email_to_Mobile_Team__c;
            o.Customer_Contact_Salutation__c =CreateLead.Salutation;
            o.Customer_Contact_First_Name__c=CreateLead.FirstName;
            o.Customer_Contact_Last_Name__c =ConLastName;
            o.Account_Telephone_Number__c =CreateLead.Phone; 
            o.Customer_Contact_Number__c=CreateLead.MobilePhone;
            o.Email_Address__c=CreateLead.Email;
            o.Postcode__c=CreateLead.Lead_Post_Code__c;
            o.Business_Name__c=CompanyName;
            if (O.Product__c == 'Broadband'){
                O.Lead_Information__c = 'Broadband Telephone Number: ' + BBTelephoneNumber + ' \n Lead Info Notes: ' + CreateLead.Description;
            }else{
                O.Lead_Information__c = CreateLead.Description;} 
            o.Tariff__c=Tariff;
            o.Group__c=OppGroup;
            o.Corporate__c=Corporate;
            o.Dice_Account_Number__c=DiceAccountNumber;
            o.Order_Reference__c=OrderReference;
            if(ResignConnections!=Null)
                o.Resign_Connection__c=Decimal.valueof(ResignConnections);
            o.Add_to_Fleet_Connection__c=AddtoFleetConnections;
            system.debug('AnnualRevenue***'+AnnualRevenue);
            if(AnnualRevenue != null)
                o.Revenue__c=Decimal.valueof(AnnualRevenue);
            o.Qty__c=CreateLead.Quantity_1__c;
            o.Outright_Sale__c = CreateLead.Is_Lead_Active__c ;
            system.debug('OyrightSale*****'+OutrightSale);
            if(CreateLead.Is_Lead_Active__c  ){
                O.Contract_Term__c='0 Months';
            }else{
                O.Contract_Term__c= ContractTerm;        
            }
            
            o.Handset__c=Handset;
            if(Subscription!=null)
                o.Subscription__c=Decimal.valueof(Subscription);           
            o.Contract_Start_Date__c=CreateLead.Primary_Contract_End_Date__c ;
            if(oppstageName!=Null)
                O.StageName = oppstageName;
            else
                O.StageName = 'Won';    
            o.Opportunity_Go_live_date__c=CreateLead.Opportunity_Go_live_date__c;
            o.Originator_EIN__C = CreateLEad.LFS_EIN__c;
            o.Originator_Email__C = CreateLEad.LFS_Originator_Email__C;
            
            if(O.id == null){
                if(CreateLead.preSales_User_EIN__c != null){ 
                    set<Id> UserId = new  set<Id>();
                    set<string> UserEin = new  set<string>();
                    for(Group pGroup : [select id,(select UserOrGroupId from GroupMembers) from Group where Name ='LFS BTLB Sheffield']){
                        for(GroupMember Gmembers : pGroup.GroupMembers){
                            UserId.add(Gmembers.UserOrGroupId);
                        }               
                    }
                    
                    for(user u : [select id,Ein__c from user where id In:UserId]){
                        UserEin.add(u.Ein__c);
                    }
                    
                    system.debug('Seet of Eins********'+UserEin);
                    if(UserEin.contains(CreateLead.preSales_User_EIN__c)){
                        insert O;
                    }else{
                        ShowError = TRUE;    
                    }
                } else{
                    insert O;
                }          
                
            }  
            
            List<Opportunity> Op = [select Name,Opportunity_Id__c,Originator_EIN__c,Originator_Email__c,
                                    Service_site__c from Opportunity Where Id=:O.id];
            system.debug('%%%%%%'+op);
            if(op.size() >0){                                
                O = Op[0];
                ShowOppId = TRUE;
                ShowLeadId = FALSE;
                ShowFormId = FALSE;
                ShowError = FALSE;
            }  
            
        }
    }
    
    public Pagereference CreateNewLead(){
        setCookie();
        ShowOppId = FALSE;
        ShowLeadId = FALSE;
        ShowFormId = TRUE;
        //pagereference Pg= new Pagereference('/apex/LeadsFromService?SiteName='+O.Service_Site__c+'&EIN='+O.Originator_EIN__c);
        pagereference Pg= new Pagereference ('/apex/CreateLeadsFromService');
        pg.setredirect(true);
        return pg;        
    }
    
    public void setCookie() {
        Cookie SiteCookie= new Cookie('SiteName',CreateLead.Service_Site__c, null, 315569260, false); //Here 315569260 represents cookie expiry date = 10 years. You can set this to what ever expiry date you want. Read apex docs for more details.
        Cookie EINCookie = new Cookie('EIN',CreateLead.LFS_EIN__c, null, 315569260, false); //Here 315569260 represents cookie expiry date = 10 years. You can set this to what ever expiry date you want. Read apex docs for more details.
        // system.debug('*************** '+EINCookie);
        Cookie OEmailcookie = new Cookie('OEmail',CreateLead.LFS_Originator_Email__c, null, 315569260, false); 
        ApexPages.currentPage().setCookies(new Cookie[] {SiteCookie,EINCookie,OEmailcookie});
    }
    
}