/**
 * Observer executes on add configuration to basket
 * 
 * @author Darko Cukovecki
 * @since 09/05/2018
*/

global with sharing class CS_TSCObserver implements csbb.ObserverApi.IObserver {  
    global void execute (csbb.ObserverApi.Observable o, Object arg) {
        csbb.ProductConfigurationObservable observable = (csbb.ProductConfigurationObservable)o;          
        String context = observable.getContext();     
        csbb__Product_Configuration_Request__c pcr = observable.getProductConfigurationRequest();     
        cscfga__Product_Configuration__c sentpc = observable.getProductConfiguration();          
        
        checkConfigsInBasket(pcr, sentPc);
    }

    @TestVisible private void checkConfigsInBasket(csbb__Product_Configuration_Request__c pcr, cscfga__Product_Configuration__c sentPc){
        Profile currentProfile = [
            SELECT Id, Name 
            FROM Profile 
            WHERE Id = :userinfo.getProfileId() 
            LIMIT 1
        ];
        
        CustomConsoleFiltering.ProductConfigurationStructure pcs = CustomConsoleFiltering.getJSONResource();

        cscfga__Product_Configuration__c currentPC = [
            SELECT 
                Id, 
                cscfga__Product_Definition__r.Name, 
                cscfga__Product_Basket__c,
                cscfga__Product_Definition__r.cscfga__Product_Category__r.Name, 
                cscfga__Product_Basket__r.Sector__c, cscfga__originating_offer__c
            FROM cscfga__Product_Configuration__c
            WHERE Id = :sentpc.Id
        ];

        cscfga__Product_Basket__c basket = [
            SELECT
                Id, Sector__c, 
                (
                    SELECT 
                        Id, cscfga__Product_Definition__r.Name, cscfga__originating_offer__r.Id,
                        cscfga__originating_offer__r.Product_Definitions__c,
                        cscfga__Product_Definition__r.cscfga__Product_Category__r.Name
                    FROM cscfga__Product_Configurations__r
                    WHERE Id != :sentpc.Id
                )
            FROM
                cscfga__Product_Basket__c
            WHERE
                Id = :currentPC.cscfga__Product_Basket__c
        ];

        List<cscfga__Offer_Category_Association__c> ocaList = [ 
            SELECT Id, cscfga__Configuration_Offer__c, 
                cscfga__Configuration_Offer__r.Name,
                cscfga__Configuration_Offer__r.Id,
                cscfga__Configuration_Offer__r.Product_Definitions__c,
                cscfga__Configuration_Offer__r.cscfga__Description__c,
                cscfga__Configuration_Offer__r.Parent_Offer_Name__c,
                cscfga__Product_Category__c,
                cscfga__Product_Category__r.Name,
                cscfga__Product_Category__r.cscfga__Parent_Category__r.Name
            FROM cscfga__Offer_Category_Association__c 
            WHERE cscfga__Configuration_Offer__c = :currentPC.cscfga__originating_offer__c
        ];
        
        List<cscfga__Product_Configuration__c> configsForDelete = new List<cscfga__Product_Configuration__c>();
        
        for(cscfga__Offer_Category_Association__c oca : ocaList){
            if(pcs.isInvalid(basket, currentProfile.Name, oca) || !pcs.isProductRulesValid(basket.cscfga__Product_Configurations__r, oca)) {
                Set<String> exclude = pcs.ProductRules.get(pcs.formatName(oca.cscfga__Configuration_Offer__r.Product_Definitions__c));
                if(exclude == null) 
                    exclude = pcs.ProductRules.get(oca.cscfga__Product_Category__r.Name);
                if(exclude == null) 
                    exclude = pcs.ProductRules.get(oca.cscfga__Product_Category__r.cscfga__Parent_Category__r.Name);
                for(cscfga__Product_Configuration__c cfg : basket.cscfga__Product_Configurations__r) {
                    if(exclude != null && (exclude.contains(cfg.cscfga__Product_Definition__r.Name) ||
                        exclude.contains(cfg.cscfga__Product_Definition__r.cscfga__Product_Category__r.Name))) {
                        configsForDelete.add(cfg);   
                    }
                }        
            }
        }

        if(!configsForDelete.isEmpty())
            DELETE configsForDelete;
    }
}