@isTest

private class Test_AssetLineCount{
 
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');

        Account dummyAccount = Test_Factory.CreateAccountForDummy();
        insert dummyAccount;
 
        Account accA = Test_Factory.CreateAccount();
        accA.SAC_Code__c = 'TST123';
        accA.LE_Code__c = null;
        insert accA;
      
        Account acc1 = Test_Factory.CreateAccount(); // a SAC Level Account to link to
        acc1.SAC_Code__c = 'TST321';
        acc1.Name += '1';
        acc1.LE_Code__c = null;
        insert acc1;
 
    
  		//add lines info , where sac is unknown
       	Asset_Line_Count__c lc1 = Test_Factory.CreateAssetLineCount();
        lc1.Month__c = Date.today().addMonths(-0);
        lc1.Featurenet_Embark__c = 1;
        lc1.ISDN2__c = 2;
        lc1.ISDN_30__c = 3;
        lc1.PSTN__c = 4;
        lc1.Total_Lines__c = 10;
        insert lc1;    
  
  
  		//add lines info
       	Asset_Line_Count__c lc2 = Test_Factory.CreateAssetLineCount();
        lc2.SAC_Code__c = acc1.SAC_Code__c;
        lc2.Month__c = Date.today().addMonths(-0);
        lc2.Featurenet_Embark__c = 1;
        lc2.ISDN2__c = 2;
        lc2.ISDN_30__c = 3;
        lc2.PSTN__c = 4;
        lc2.Total_Lines__c = 10;
        insert lc2;     
   
   		//add lines info
       	Asset_Line_Count__c lc3 = Test_Factory.CreateAssetLineCount();
        lc3.SAC_Code__c = acc1.SAC_Code__c;
        lc3.Month__c = Date.today().addMonths(-0);
        lc3.Featurenet_Embark__c = 1;
        lc3.ISDN2__c = 2;
        lc3.ISDN_30__c = 3;
        lc3.PSTN__c = 4;
        lc3.Total_Lines__c = 10;
        insert lc3;           
        
        
     //Delete all lines added
       List<Asset_Line_Count__c> lines = [select id from Asset_Line_Count__c where SAC_Code__c like :acc1.SAC_Code__c];
        delete lines;
         List<Asset_Line_Count__c> lines2 = [select id from Asset_Line_Count__c where SAC_Code__c = 'zzz999'];
        delete lines2; 
     //Delete all accounts
        List<Account> accs = [select id from Account where Name like :acc1.Name];
        delete accs;
     
        
  }}