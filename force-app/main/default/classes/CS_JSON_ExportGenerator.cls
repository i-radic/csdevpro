/**
 * @name CS_JSON_ExportGenerator
 * @description 
 * @revision
 *
 */
public class CS_JSON_ExportGenerator {
	public static final String ResourceName = 'JSONExport';
	
	public static final Set<String> attachmentNames = new Set<String>{
		'Apple DEP_schema.json',
		'Account Admins_schema.json',
		'Implementation Form_schema.json'
	};
	
	public CS_JSON_Schema schema {get; set;}
	
	public Id id {get; set;}
	
	public csbtcl_Temporary_Data_Store_CS_JSON__c dataStore {get; set;}
	
	/**
	 * Class Constructor
	 * @return 	CS_JSON_ExportGenerator
	 */
	public CS_JSON_ExportGenerator(Id id) {
		this.schema = getSchema();
		this.id = id;
		this.dataStore = new csbtcl_Temporary_Data_Store_CS_JSON__c(
			Opportunity__c = id,
			JSON_Generated_Date_Time__c = System.now()
		);
	}
	
	public void generate() {
		List<cscfga__Product_Basket__c> basket = [SELECT Id, Basket_Sync_Counter__c,cscfga__Opportunity__c FROM cscfga__Product_Basket__c
												 WHERE cscfga__Opportunity__c = :id
												 AND csbb__Synchronised_With_Opportunity__c = true];
		if(basket.size() != 1) {
			return;
		}
		
		Map<String, Object> jsonResult = getJSONResult(basket.get(0).Id);		
		insert dataStore;

		List<Attachment> atts = copyImplementationForm(basket.get(0), dataStore.Id);
		atts.add(new Attachment(
			Name = basket.get(0).cscfga__Opportunity__c +'_'+basket.get(0).Basket_Sync_Counter__c+'_JSONExport.json',
			ParentId = dataStore.Id,
			ContentType = 'CS JSON',
			Body = Blob.valueOf(JSON.serialize(jsonResult))
		));
        
        for(CS_JSON_Schema.CS_JSON_Email emailSetting : schema.emails) {
            CS_JSON_SendEmail emailWorker = new CS_JSON_SendEmail(emailSetting, jsonResult);
            Map<String, String> emailResult = emailWorker.sendEmail();
            for(String subject : emailResult.keySet()) {
                atts.add(new Attachment(
					Name = subject + '_Email',
					ParentId = dataStore.Id,
					ContentType = 'Email',
					Body = Blob.valueOf(emailResult.get(subject))
				));
            }	
        }
        
        insert atts;
	}
	
	public CS_JSON_Schema getSchema() {
		return (CS_JSON_Schema) JSON.deserialize(CS_Utils.loadStaticResource(ResourceName).toString(), CS_JSON_Schema.class);
	}
	
	public List<Attachment> copyImplementationForm(cscfga__Product_Basket__c basket, Id parentId) {
		List<Attachment> atts = [SELECT	Id, Body, Name
								 FROM Attachment
								 WHERE ParentId IN (SELECT Id FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basket.Id)
								 AND Name = :attachmentNames];
		Map<String, Object> attBody = new Map<String, Object>();
		for(Attachment att : atts) {
			attBody.put(att.Name, JSON.deserializeUntyped(att.Body.toString()));
		}
		
		List<Attachment> attToInsert = new List<Attachment>{
			new Attachment(Body = Blob.valueOf(JSON.serialize(attBody)), ParentId = parentId, ContentType = 'CS JSON',
							Name = basket.cscfga__Opportunity__c + 'Implementation Form.json')
		};
		
		return attToInsert;
	}

	public Map<String, Object> getJSONResult(Id basketId) {
		Map<String, Object> result = new Map<String, Object>();
		for(CS_JSON_Schema.CS_JSON_Section sec : schema.sections) {
			CS_JSON_Schema.CS_JSON_Setting sett = schema.getSetting(sec.setting);
			CS_JSONService_Base base = (CS_JSONService_Base) Type.forName(sett.service).newInstance();
			base.init(sett);
			
			result.put(sec.name, base.getResult(basketId));
		}

		return result;
	}
}