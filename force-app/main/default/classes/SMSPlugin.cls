/*********************************************************************************************************
Name : SMSPlugin 
Description : This class is used to send SMS message via BT Messaging System For 2FA
Test Class: SMSPlugin_Test

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                   23/01/2018                TCS DEV                            Class created          
*********************************************************************************************************/
global class SMSPlugin implements Process.Plugin {
    
    
    /**********************************************************************************************************
    Method Name : Process Describe
    Description : Method To Pass Mobile Phone Of Logged User From Flow
    
    Version                 Date                    Author                             Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
    1.0                   23/01/2018                TCS DEV                             Method created        
    **********************************************************************************************************/ 
    global Process.PluginDescribeResult describe() {
        
        // Describe Process Info. Can be seen in Flow Designer
        Process.PluginDescribeResult result = new Process.PluginDescribeResult();
        result.tag='Identity';
        result.name='SMS Plugin';
        result.description='Two factor authentication with SMS';
        
        // Pass MobilePhone To Invoke Method  
        result.inputParameters = new List<Process.PluginDescribeResult.InputParameter> {            
            new Process.PluginDescribeResult.InputParameter('To', Process.PluginDescribeResult.ParameterType.STRING, true)            
        };
       
       // Pass Output Variables To Validate In Flow
        result.outputParameters = new List<Process.PluginDescribeResult.OutputParameter> {
           new Process.PluginDescribeResult.OutputParameter('Status', Process.PluginDescribeResult.ParameterType.STRING),
            new Process.PluginDescribeResult.OutputParameter('VerificationCode', Process.PluginDescribeResult.ParameterType.STRING)
        };
         
        return result;
     
    }
    
    
    /*********************************************************************************************************
    Method Name : Invoke HTTP Callout
    Description : Method To Send SMS By Callout
    
    Version                 Date                    Author                             Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
    1.0                   23/01/2018                TCS DEV                             Method created        
    **********************************************************************************************************/  
    global Process.PluginResult invoke(Process.PluginRequest request) {  
     
        //Declare Variables Needed
        Map<String, Object> result = new Map<String, Object>();
        Mobile_2FA_Via_SMS_Settings__c settings = Mobile_2FA_Via_SMS_Settings__c.getInstance(Userinfo.getUserId());
        String Req = (String)settings.get('End_Point__c');         
        String apikey = (String)settings.get('API_Key__c');
        String mobilephone = (String)request.inputParameters.get('To');     
        System.debug('MobilePhone : ' + mobilephone + ' WithoutSpaces :' + mobilephone.deleteWhitespace());
        mobilephone = mobilephone.deleteWhitespace();       
        String USCanadaRegex = '';
        if(Test.isRunningTest()) {
            USCanadaRegex = '^\\+?1[\\(\\)\\.\\- ]{0,}[0-9]{3}[\\(\\)\\.\\- ]{0,}[0-9]{3}[\\(\\)\\.\\- ]{0,}[0-9]{4}[\\(\\)\\.\\- ]{0,}$';
            System.debug('Default Regex From Code : ' + USCanadaRegex);
        }
        else{
            USCanadaRegex = (String)settings.get('US_Canada_Regex__c');
            System.debug('Pick Regex From Custom Setting : ' + USCanadaRegex);
        }
        Boolean isUSMobile = false;
        isUSMobile = validatePhone(mobilephone, USCanadaRegex);
        String sender = '';
        if(isUSMobile)
            sender = (String)settings.get('US_Canada_Sender__c');
        else
            sender = (String)settings.get('Sender__c');
        System.debug('Sender : ' + sender);       
        //Generate Random Digit Code
        Integer rand = Math.round(Math.random()*100000);
        String VerificationCode = string.valueOf(rand);
        String Message = 'Your%20verification%20code%20for%20BT%20SF%20mobile%20app%20is:%20' + VerificationCode;

        //Declare and Build HTTP Request Object
        Http http = new Http();
        HttpRequest smsrequest = new HttpRequest();
        HTTPResponse smsresponse = new HTTPResponse();
        string params = '?apikey=' + apikey + '&sender=' + sender + '&numbers=' + mobilephone + '&message=' + Message;
        smsrequest.setEndpoint(Req + params);
        smsrequest.setMethod('POST');
                
        //Invoke and Capture Resonse      
        try {
            System.debug('*****    ' + smsrequest.toString());
            smsresponse = http.send(smsrequest);
            System.debug('*****    ' + smsresponse.toString());
        } catch(Exception ex) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"balance":999849.8,"batch_id":531318060,"cost":1,"num_messages":1,"message":{"num_parts":1,"sender":"07848012128","content":"text of message in UTF-8 encoding using URL encoding as necessary"},"receipt_url":"","custom":"","messages":[{"id":"1727822452","recipient":447848012128}],"status":"exceptioncatch"}');
            res.setStatusCode(500);
            smsresponse = res;          
            result.put('Status', 'Failure');
        }

        //Parse JSON Response to get Status 
        JSONParser parser = JSON.createParser(smsresponse.getBody());
        string status = '';
        while (parser.nextToken() != null) {
            if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && 
                (parser.getText() == 'status')) {
                // Get the value.
                parser.nextToken();
                // Compute the grand total price for all invoices.
                status = parser.getText();
            }
        }
          result.put('Status', status);
          result.put('VerificationCode', VerificationCode);
          return new Process.PluginResult(result);
      }

      /*********************************************************************************************************
      Method Name : Validate Phone
      Description : Method To Validate Phone Format
    
      Version                 Date                    Author                             Summary Of Change
      --------------------------------------------------------------------------------------------------------- 
      1.0                   23/01/2018                TCS DEV                             Method created        
      **********************************************************************************************************/ 
      global static Boolean validatePhone(string mphone, string regex) {
        //Declare Boolean Variable To Capture Result        
        Boolean result = false;
        //Compile The Regex Code For Pattern Matching
        Pattern validPhone = Pattern.compile(regex);
        //Match Phone Number Format
        Matcher validPhoneMatch = validPhone.matcher(mphone);
        //Validate Phone Number
        result = validPhoneMatch.matches();
        //Return Result
        return result;

      }
  }