@IsTest
public with sharing class ManageRateCardLinesTest {
    private static testMethod void testNewRateCard() {
        No_Triggers__c notriggers=new No_Triggers__c(Flag__c=true);
        insert notriggers;
        Id userId = UserInfo.getUserId();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        TriggerDeactivating__c TD = new TriggerDeactivating__c(Account__c=true,SetupOwnerId=userId,Name='Trigger Deactivating (User)');
        insert TD;
        Account acc = new Account(Name='Test account');
        insert acc;
        Opportunity opp = new Opportunity(AccountId = acc.Id, StageName = 'Created');
        insert opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = acc.Id, cscfga__Opportunity__c = opp.Id,
                Synchronised_with_Opportunity__c=false,csbb__Synchronised_With_Opportunity__c=false,csordtelcoa__Synchronised_with_Opportunity__c=false);
        insert basket;
        Usage_Profile__c usageProfile = new Usage_Profile__c(Name = 'test UP', Account__c=acc.Id,Active__c=true,Expected_Voice__c=10000,
        Expected_SMS__c=10000, FM_UK_Landlines__c=10,FM_Receiving_call_in_Zone_A__c=10,FM_Calls_to_EE__c=10,FM_Receiving_call_in_Zone_B__c=10,
        FM_Calls_to_other_UK_mobile_networks__c=10,FM_Receiving_call_in_Zone_C__c=10,FM_IDD_Zone_A__c=10,FM_MMS_Messaging__c=10,FM_IDD_Zone_B__c=10,FM_MMS__c=10,
        FM_IDD_Zone_C__c=10,FM_Roaming_SMS_Zone_A__c=10,FM_Non_Geographic_Calls__c=10,FM_Roaming_SMS_Zone_B__c=10,FM_SMS_to_EE__c=10,FM_Roaming_SMS_Zone_C__c=10
        );
        insert usageProfile;

        cscfga__Product_Category__c Pcategory =  new cscfga__Product_Category__c(Name='Future Mobile');
        insert Pcategory;
        cspmb__Rate_Card__c templateRC = new cspmb__Rate_Card__c(Name = 'Custom Caller',Is_Template__c=true,
                cspmb__Is_Active__c=true,Product_Category__c = Pcategory.Id);
        insert templateRC;
        cspmb__Rate_Card_Line__c RCL = new cspmb__Rate_Card_Line__c(cspmb__Rate_Card__c =templateRC.Id,name='IDD Zone A',cspmb__rate_value__c=10,Standard_Rate_Cost__c=0.1,Standard_Rate_Value__c=10);
        insert RCL;

        cspmb__Discount_Level__c DL1 = new cspmb__Discount_Level__c(Rate_Card_Line__c=RCL.Id,name='FM-Recurring-Percentage-IDD Zone A-33',
                cspmb__Discount_Values__c='33',cspmb__Discount_Level_Code__c='IDZA33',cspmb__Discount__c=33);
        insert DL1;

        PageReference myVfPage = Page.ManageRateCardLines;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('accountId',acc.Id);
        ApexPages.currentPage().getParameters().put('rateCardId','');
        ApexPages.currentPage().getParameters().put('basketId',basket.Id);


        ManageRateCardLinesController controller = new ManageRateCardLinesController();
        controller.saveChanges();

        List<cspmb__Rate_Card__c> newRateCards= [select name,id,Is_Template__c,cspmb__Account__c
        from cspmb__Rate_Card__c
        where Is_Template__c=false and cspmb__Account__c=:acc.Id];

        system.assertEquals(newRateCards.size()==1,true);

    }

    private static testMethod void testExistingCard() {
        No_Triggers__c notriggers=new No_Triggers__c(Flag__c=true);
        insert notriggers;
        Id userId = UserInfo.getUserId();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        TriggerDeactivating__c TD = new TriggerDeactivating__c(Account__c=true,SetupOwnerId=userId,Name='Trigger Deactivating (User)');
        insert TD;
        Account acc = new Account(Name='Test account');
        insert acc;
        Opportunity opp = new Opportunity(AccountId = acc.Id, StageName = 'Created');
        insert opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(csbb__Account__c = acc.Id, cscfga__Opportunity__c = opp.Id);
        insert basket;
        Usage_Profile__c usageProfile = new Usage_Profile__c(Name = 'test UP', Account__c=acc.Id,Active__c=true,Expected_Voice__c=10000,
                Expected_SMS__c=10000, FM_UK_Landlines__c=10,FM_Receiving_call_in_Zone_A__c=10,FM_Calls_to_EE__c=10,FM_Receiving_call_in_Zone_B__c=10,
                FM_Calls_to_other_UK_mobile_networks__c=10,FM_Receiving_call_in_Zone_C__c=10,FM_IDD_Zone_A__c=10,FM_MMS_Messaging__c=10,FM_IDD_Zone_B__c=10,FM_MMS__c=10,
                FM_IDD_Zone_C__c=10,FM_Roaming_SMS_Zone_A__c=10,FM_Non_Geographic_Calls__c=10,FM_Roaming_SMS_Zone_B__c=10,FM_SMS_to_EE__c=10,FM_Roaming_SMS_Zone_C__c=10
        );
        insert usageProfile;

        cscfga__Product_Category__c Pcategory =  new cscfga__Product_Category__c(Name='Future Mobile');
        insert Pcategory;
        cspmb__Rate_Card__c templateRC = new cspmb__Rate_Card__c(Name = 'Custom Caller',Is_Template__c=true,
                cspmb__Is_Active__c=true,Product_Category__c = Pcategory.Id);
        insert templateRC;
        cspmb__Rate_Card__c existingRC = new cspmb__Rate_Card__c(Name = 'Custom Caller',Is_Template__c=false,
                cspmb__Is_Active__c=true,Product_Category__c = Pcategory.Id,cspmb__Account__c=acc.Id);
        insert existingRC;

        cspmb__Rate_Card_Line__c RCL = new cspmb__Rate_Card_Line__c(cspmb__Rate_Card__c =templateRC.Id,
                name='IDD Zone A',cspmb__rate_value__c=10,Standard_Rate_Cost__c=0.1,Standard_Rate_Value__c=10);
        insert RCL;
        cspmb__Rate_Card_Line__c existingRCL = new cspmb__Rate_Card_Line__c(cspmb__Rate_Card__c =existingRC.Id,
                name='IDD Zone A',cspmb__rate_value__c=10,Standard_Rate_Cost__c=0.1,Standard_Rate_Value__c=10);
        insert existingRCL;

        cspmb__Discount_Level__c DL1 = new cspmb__Discount_Level__c(Rate_Card_Line__c=RCL.Id,name='FM-Recurring-Percentage-IDD Zone A-33',
                cspmb__Discount_Values__c='33',cspmb__Discount_Level_Code__c='IDZA33',cspmb__Discount__c=33);
        insert DL1;

        PageReference myVfPage = Page.ManageRateCardLines;
        Test.setCurrentPage(myVfPage);
        ApexPages.currentPage().getParameters().put('accountId',acc.Id);
        ApexPages.currentPage().getParameters().put('rateCardId',existingRC.Id);
        ApexPages.currentPage().getParameters().put('basketId',basket.Id);

        ManageRateCardLinesController controller = new ManageRateCardLinesController();
        controller.saveChanges();

        List<cspmb__Rate_Card__c> newRateCards= [select name,id,Is_Template__c,cspmb__Account__c
                                                from cspmb__Rate_Card__c
                                                where Is_Template__c=false and cspmb__Account__c=:acc.Id];

        system.assertEquals(newRateCards.size()==1,true);
    }
}