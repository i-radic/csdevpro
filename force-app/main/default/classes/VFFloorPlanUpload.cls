public class VFFloorPlanUpload
    {  
        private final Coverage_Check_Site__c ccSite;
        
        public Id recordId  
       {    get;set;    }  
          
        
        public Boolean refreshPage {get; set;}
        
        public string fileName   
        {    get;set;    }  
          
        public Blob fileBody   
        {    get;set;    }  
        public VFFloorPlanUpload(ApexPages.StandardController stdController)  
        {  
           refreshPage = false;
           recordId  = stdController.getRecord().Id;       
           ccSite= [select id,Floorplan_Attached__c  from Coverage_Check_Site__c
                       where id = :ApexPages.currentPage().getParameters().get('id')];
        }  
        public PageReference UploadFile()  
        {  
            PageReference pr;  
            if(fileBody != null && fileName != null)  
            {  
              Attachment myAttachment  = new Attachment();  
              myAttachment.Body = fileBody;  
              myAttachment.Name = fileName;  
              myAttachment.ParentId = recordId  ;  
              fileBody =null;
              insert myAttachment;  
              ccSite.Floorplan_Attached__c =true;
              pr = new PageReference('/' + myAttachment.Id);               
              update ccSite; 
              refreshPage=true;
              
            }  
          
            return null;  
        }      
    }