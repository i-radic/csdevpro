public with sharing class CRFProvideLineDetails {

public Provide_Line_Details__c PL;

    public CRFProvideLineDetails(ApexPages.StandardController stdController){
    PL= (Provide_Line_Details__c)stdController.getRecord();
    }
    
    
    Public PageReference OnStart(){
    
    
    
    List<CRF__c> crf = [Select Id,Name,RecordTypeId from CRF__c where Id=:PL.Related_to_CRF__c];
    List<Flow_CRF__c> flowcrf = [Select Id,Name,RecordTypeId from Flow_CRF__c where Id=:PL.Related_to_Flow_CRF__c];
    
    if(crf.size()>0){
    Id crfId= crf[0].Id;
    String crfName= crf[0].Name;
    Id crfRTId= crf[0].RecordTypeId;
    
    String crfRTName = [Select DeveloperName from RecordType where Id=:crfRTId].DeveloperName;
    String ProvideLineAXRTId = [Select Id from RecordType where DeveloperName=:'Provide_Lines_Details_AX'].Id;
    String ProvideLineRTId = [Select Id from RecordType where DeveloperName=:'Provide_Line_Details'].Id;    
    System.debug('CRF Record Type'+crfRTName); 
    
    if(crfRTName == 'AX_Form'){
  
    /* https://cs4.salesforce.com/a21/e?CF00N20000002oGvl={!CRF__c.Name}&CF00N20000002oGvl_lkid={!CRF__c.Id}&Name=Auto+Number&RecordType=012P00000004Oky&ent=01I200000007TPC&saveURL=/{!CRF__c.Id}&retURL=/{!CRF__c.Id} */
    PageReference pageRef = new PageReference( '/a21/e?'+'CF00N20000002oGvl='+crfName+'&CF00N20000002oGvl_lkid='+crfId+'&Name=Auto+Number&RecordType='+ProvideLineAXRTId+'&ent=01I200000007TPC&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1');
    Return pageRef;
    }
    
    else if(crfRTName != 'AX_Form'){ 
    
    /* https://cs4.salesforce.com/a21/e?CF00N20000002oGvl={!CRF__c.Name}&CF00N20000002oGvl_lkid={!CRF__c.Id}&Name=Auto+Number&RecordType=012P00000004Oky&ent=01I200000007TPC&saveURL=/{!CRF__c.Id}&retURL=/{!CRF__c.Id} */
    PageReference pageRef = new PageReference( '/a21/e?'+'CF00N20000002oGvl='+crfName+'&CF00N20000002oGvl_lkid='+crfId+'&Name=Auto+Number&RecordType='+ProvideLineRTId+'&ent=01I200000007TPC&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1');
    Return pageRef;
     }
    
    else return null;
    
    }
    
    if(flowcrf.size()>0){
    Id crfId= flowcrf [0].Id;
    String crfName= flowcrf [0].Name;
    Id crfRTId= flowcrf [0].RecordTypeId;
    
    String crfRTName = [Select DeveloperName from RecordType where Id=:crfRTId].DeveloperName;
    String ProvideLineAXRTId = [Select Id from RecordType where DeveloperName='Provide_Lines_Details_AX'].Id;
    String ProvideLineRTId = [Select Id from RecordType where DeveloperName='Provide_Line_Details'].Id;    
    System.debug('CRF Record Type'+crfRTName); 
    
    if(crfRTName == 'AXForm'){
   
    /* https://cs4.salesforce.com/a21/e?CF00N20000002oGvl={!CRF__c.Name}&CF00N20000002oGvl_lkid={!CRF__c.Id}&Name=Auto+Number&RecordType=012P00000004Oky&ent=01I200000007TPC&saveURL=/{!CRF__c.Id}&retURL=/{!CRF__c.Id} */
    PageReference pageRef = new PageReference( '/a21/e?'+'CF00N20000009AzoB='+crfName+'&CF00N20000009AzoB_lkid='+crfId+'&Name=Auto+Number&RecordType='+ProvideLineAXRTId+'&ent=01I200000007TPC&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1');
    Return pageRef;
    }
    
    else if(crfRTName != 'AXForm'){ 
   
    /* https://cs4.salesforce.com/a21/e?CF00N20000002oGvl={!CRF__c.Name}&CF00N20000002oGvl_lkid={!CRF__c.Id}&Name=Auto+Number&RecordType=012P00000004Oky&ent=01I200000007TPC&saveURL=/{!CRF__c.Id}&retURL=/{!CRF__c.Id} */
    PageReference pageRef = new PageReference( '/a21/e?'+'CF00N20000009AzoB='+crfName+'&CF00N20000009AzoB_lkid='+crfId+'&Name=Auto+Number&RecordType='+ProvideLineRTId+'&ent=01I200000007TPC&saveURL=/'+crfId+'&retURL=/'+crfId+'&nooverride=1');
    Return pageRef;
     }
    
    else return null;
    
    }
    else return null;
    }
}