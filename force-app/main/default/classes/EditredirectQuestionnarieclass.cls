/******************************************************************************************************
Name : EditredirectQuestionnarieclass
Description : Controller for page EditredirectQuestionnariepage
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    1/12/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public class EditredirectQuestionnarieclass
{
    public Questionnaire__c qt;
    public EditredirectQuestionnarieclass(ApexPages.StandardController controller)
    {
        qt=(Questionnaire__c)controller.getRecord();
    }
    public Pagereference redirect()
    {   
        Questionnaire__c rectype=new Questionnaire__c();
        Opportunity opptyRecord = new Opportunity();
        if(qt.id!=null)
        {
            rectype =[select id,Recordtype.DeveloperName,Opportunity__c From Questionnaire__c where id=:qt.id];
        }
        if(rectype.Opportunity__c !=null)
        {
            opptyRecord=[select id,stagename from Opportunity where id=:rectype.Opportunity__c];
        }
        if(opptyRecord.StageName !=null && (opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_CLOSE_WON)||opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_CLOSED_LOST)||opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_CLOSED_CANCELLED)))
        {
            if(rectype.Recordtype.DeveloperName != null && rectype.Recordtype.DeveloperName.Equalsignorecase(GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE))
            {
                PageReference pageRef = Page.questionairepageview;
                pageRef.getParameters().put('qt',qt.id);
                return PageRef;
            } 
            else if(rectype.Recordtype.DeveloperName != null && rectype.Recordtype.DeveloperName.Equalsignorecase(GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE))
            {
                PageReference pageRef = Page.OpportunityProfilerview;
                pageRef.getParameters().put('qt', qt.id);
                return PageRef;
            }
        }
        else
        {
            if(rectype.Recordtype.DeveloperName != null && rectype.Recordtype.DeveloperName.Equalsignorecase(GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE))
            {
                PageReference pageRef = Page.Questionnairepageedit;
                pageRef.getParameters().put('qt',qt.id);
                return PageRef;
            } 
            else if(rectype.Recordtype.DeveloperName != null && rectype.Recordtype.DeveloperName.Equalsignorecase(GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE))
            {
                PageReference pageRef = Page.OpportunityProfileredit;
                pageRef.getParameters().put('qt', qt.id);
                return PageRef;
            }
        }
        return null;
    }
}