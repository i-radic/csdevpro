public class contactScarsLandscapeController { 
    
    public Id landscapeId {get; set;}
    public Id scarsId {get; set;}
    public Id AccId {get; set;}
    public Id cId {get; set;}
    public Id campId {get; set;}
   
    public contactScarsLandscapeController() {
    //landscapeId = ApexPages.currentPage().getParameters().get('landscapeId');
    //scarsId = ApexPages.currentPage().getParameters().get('scarsId');
    AccId = ApexPages.currentPage().getParameters().get('aId');
    cId = ApexPages.currentPage().getParameters().get('Id');
    try {
        scarsId = [Select Id, Name From SCARS_BTLB__c WHERE Account__c = : AccId LIMIT 1].Id;
        landscapeId = [Select Id, Name From Landscape_BTLB__c WHERE Customer__c = : AccId LIMIT 1].Id;
    } catch (Exception ex) {
        ApexPages.Message error = new ApexPages.message(ApexPages.severity.INFO, 'No Data Exists!');
        ApexPages.addMessage(error);
      }
    }
    
    public List < CampaignMember > getCampaignList() {
    try {
        Contact c = [Select(Select Status, Id, HasResponded, CreatedDate, Contact_Response__c, Campaign_Name__c, CampaignId From CampaignMembers) From Contact WHERE Id = : cId LIMIT 1];
        List < CampaignMember > camps = c.CampaignMembers;
        //campId = [Select Id, Name From Campaign WHERE Account__c = :AccId LIMIT 1].Id;            
        //campId = camps[0].Id;
        //System.debug('CampaignId:'+camps[0].Id);
        //campId = '00vR0000003QZD7IAO';
        return camps;
    } catch (Exception ex) {
        ApexPages.Message error = new ApexPages.message(ApexPages.severity.INFO, 'No Data Exists!');
        ApexPages.addMessage(error);
        return null;
    }
    }    
}