@isTest
public class CS_OESaveHandlerTest {

	private static Map<String, Object> createTestData(String action){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
		Map<String, Object> inputMap = new Map<String, Object>();

		Account testAcc = new Account();
        testAcc.Name = 'TestBasket';
        testAcc.NumberOfEmployees = 10;
        insert testAcc;
        
        // create opportunity
        Opportunity testOpp = new Opportunity();    
        testOpp.Name = 'Test basket compare opp: ' + testAcc.Name + ':'+datetime.now();
        testOpp.AccountId = testAcc.Id;
        testOpp.CloseDate = System.today();
        testOpp.StageName = 'Closed Won';   //@TODO - This should be a configuration
        testOpp.TotalOpportunityQuantity = 0; // added this as validation rule was complaining it was missing
        insert testOpp;

        Basket_Totals__c bt = new Basket_Totals__c(
			Name = 'bt1'
		);
		insert bt;

		cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = 'PD',
            cscfga__Description__c = 'PD Desc'
        );
        insert pd;

		cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
			Name = 'Basket',
			Basket_Totals__c = bt.Id,
			cscfga__Opportunity__c = testOpp.Id,
			csbb__Account__c = testAcc.Id,
			Customer_Apple_Id__c = 'testId'
		);
		insert pb;

		cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
			Name = 'PC',
			cscfga__Product_Definition__c = pd.Id,
			cscfga__Product_Basket__c = pb.Id,
			Volume__c = 2,
			cscfga__Configuration_Status__c = 'Valid'
		);
		insert pc;

		inputMap.put('configId', pc.Id);
		inputMap.put('action', action);
		inputMap.put('basketId', pb.Id);
		inputMap.put('definitionName', 'BTMobile');
		inputMap.put('invalidCount', 0);
		inputMap.put('customerAppleId', 'testId');
		inputMap.put('optOut', false);

		return inputMap;
	}

	private static Map<String, Object> createBadTestData(String action){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Map<String, Object> inputMap = new Map<String, Object>();

		Account testAcc = new Account();
        testAcc.Name = 'TestBasket';
        testAcc.NumberOfEmployees = 10;
        insert testAcc;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;
        
        // create opportunity
        Opportunity testOpp = new Opportunity();    
        testOpp.Name = 'Test basket compare opp: ' + testAcc.Name + ':'+datetime.now();
        testOpp.AccountId = testAcc.Id;
        testOpp.CloseDate = System.today();
        testOpp.StageName = 'Closed Won';   //@TODO - This should be a configuration
        testOpp.TotalOpportunityQuantity = 0; // added this as validation rule was complaining it was missing
        insert testOpp;

        Basket_Totals__c bt = new Basket_Totals__c(
			Name = 'bt1'	
		);
		insert bt;

		cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = 'PD',
            cscfga__Description__c = 'PD Desc'
        );
        insert pd;

        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
			Name = 'Basket',
			Basket_Totals__c = bt.Id,
			cscfga__Opportunity__c = testOpp.Id,
			csbb__Account__c = testAcc.Id,
			Customer_Apple_Id__c = 'testId'
		);
		insert pb;

		cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
			Name = 'PC',
			cscfga__Product_Definition__c = pd.Id,
			cscfga__Product_Basket__c = pb.Id,
			Volume__c = 2,
			cscfga__Configuration_Status__c = 'Valid'
		);
		insert pc;


        inputMap.put('configId', pc.Id);
		inputMap.put('action', action);
		inputMap.put('basketId', pb.Id);
		inputMap.put('definitionName', 'BTOP');
		inputMap.put('invalidCount', 10);
		inputMap.put('customerAppleId', 'testId');
		inputMap.put('optOut', true);

		return inputMap;
	}

	private static testMethod void testImplementationForm(){
		Test.startTest();
		CS_OESaveHandler cls = new CS_OESaveHandler();
		cls.execute(createTestData('ImplementationForm'));
		Test.stopTest();
	}
	private static testMethod void testAccountAdmins(){
		Test.startTest();
		CS_OESaveHandler cls = new CS_OESaveHandler();
		cls.execute(createTestData('AccountAdmins'));
		Test.stopTest();
	}
	private static testMethod void testAppleDEP(){
		Test.startTest();
		CS_OESaveHandler cls = new CS_OESaveHandler();
		cls.execute(createTestData('AppleDEP'));
		Test.stopTest();
	}
}