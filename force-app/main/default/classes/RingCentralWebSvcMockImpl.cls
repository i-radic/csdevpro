@isTest
                        
global class RingCentralWebSvcMockImpl implements WebServiceMock {
   RingCentralAPI.LogInfo logInfo = new RingCentralAPI.LogInfo();
   RingCentralAPI.AllowFieldTruncationHeader_element fieldTruc = new RingCentralAPI.AllowFieldTruncationHeader_element();
   RingCentralAPI.DebuggingHeader_element dubugHeader = new RingCentralAPI.DebuggingHeader_element();
   RingCentralAPI.CallOptions_element callOptions = new RingCentralAPI.CallOptions_element();
   RingCentralAPI.DebuggingInfo_element debugInfo = new RingCentralAPI.DebuggingInfo_element();
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       RingCentralAPI.submitDealRegistrationResponse_element response_x = new RingCentralAPI.submitDealRegistrationResponse_element();
       response_x.result = '{'
          + '"Status" : "Success",'
          + '"ErrorCode" : "01",'
          + '"ApprovalStatus" : "Approved",'
          + '"PartnerDealID" : "8",'
          + '"ValidUntilDate" : "2015-03-17"'
          + '}';
       response.put('response_x', response_x); 
   }
}