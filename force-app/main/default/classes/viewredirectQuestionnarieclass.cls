/******************************************************************************************************
Name : viewredirectQuestionnarieclass 
Description : Controller for page viewredirectQuestionnariepage
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    1/12/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public class viewredirectQuestionnarieclass 
{
    public Questionnaire__c qt;
    public viewredirectQuestionnarieclass(ApexPages.StandardController controller) 
    {
        qt=(Questionnaire__c)controller.getRecord();
    }
    public Pagereference redirect()
    {   
         String rectype ='';
        if(qt.id != null)
        {
           rectype=[select id,Recordtype.DeveloperName From Questionnaire__c where id=:qt.id].Recordtype.DeveloperName;  
        }
        if(rectype.Equalsignorecase(GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE))
        {
            PageReference pageRef = Page.questionairepageview;
            pageRef.getParameters().put('qt',qt.id);
            return PageRef;
        }
        else if(rectype.Equalsignorecase(GlobalConstants.QUESTIONNAIRE_PROFILER_RECORDTYPE)) 
        {
             PageReference pageRef = Page.OpportunityProfilerview;
            pageRef.getParameters().put('qt',qt.id);
            return PageRef;
        }
        return null;
    }
}