/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 04/11/2020
   @author Maahaboob Basha

   @description Test class for CustomButtonTakeOwnership.

   @modifications
   
*/
@isTest(SeeAllData=FALSE)
private class CustomButtonTakeOwnershipTest {

    /*******************************************************************************************************
    * Method Name : performActionPositiveTest
    * Description : Used to simulate and test the logic of performAction method in CustomButtonTakeOwnership 
    * Parameters  : NA
    * Return      : NA                      
    *******************************************************************************************************/
    static testmethod void performActionPositiveTest() {
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        String successMsg = 'Service Owner updated successfully';
        String redirectURL = 'apex/BasketbuilderApp?id=' + basket.Id;
        Test.startTest();
            CustomButtonTakeOwnership cbOwnership = new CustomButtonTakeOwnership();
            result = cbOwnership.performAction(basket.Id);
        Test.stopTest();
        System.assertNotEquals(NULL, result);
        System.assertEquals('{"status":"ok","redirectURL":"' + redirectURL + '","text":"' + successMsg + '"}', result);
    }
}