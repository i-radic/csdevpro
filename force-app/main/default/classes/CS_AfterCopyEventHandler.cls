/**
 * Class used for after copy process - creates attachments for HVRP logic
 */
global class CS_AfterCopyEventHandler extends cscfga.AEventHandler {

	/**
	 * Mandatory method for event handler
	 * @return System.Type class type
	 */
	public override Type getClass() {
		Type t = CS_AfterCopyEventHandler.class;
		return t;
	}

	/**
	 * Handles after copy event - creates attachments for HVRP logic
	 * @param String eventName name of event after which the logic will be processed
	 * @param Object payLoad cloned records
	 */
	public override void handleEvent(String eventName, Object payload) {
		system.debug(LoggingLevel.Error, eventName);
		system.debug(LoggingLevel.Error, payload);
		if (eventName == 'copyConfigurations_finished') {
			Map<String, Object> payloadMap = (Map<String, Object>) payload;
			Object sourceConfigurationIds = payloadMap.get('cloneConfigurations');
			Object originalConfigIds = payloadMap.get('sourceConfigurations');
			Set<Id> configIds = (Set<Id>) sourceConfigurationIds;
			Set<Id> oldConfigIds = (Set<Id>) originalConfigIds;
			Map<String, Set<String>> definitionAttributesMap = CS_CloneBasketController.mapDefinitionAttributes( CS_Related_Product_Attributes__c.getAll());
			Set<String> attributeNames = CS_CloneBasketController.foldAttributes(definitionAttributesMap);
			String configurationQuery2 = 'select '
                + CS_Utils.getSobjectFields('cscfga__Product_Configuration__c')
                + ', cscfga__Product_Definition__r.Name'
                + ' from cscfga__Product_Configuration__c'
                + ' where Id in '
                + CS_Utils.convertStringListToString(new List<Id>(configIds));
			List<cscfga__Product_Configuration__c> sourceConfigurations = Database.query(configurationQuery2);
			
			
			String configurationQuery = 'select '
                + CS_Utils.getSobjectFields('cscfga__Product_Configuration__c')
                + ', cscfga__Product_Definition__r.Name'
                + ' from cscfga__Product_Configuration__c'
                + ' where Id in '
                + CS_Utils.convertStringListToString(new List<Id>(oldConfigIds))
                + ' or cscfga__Root_Configuration__c in ' 
                + CS_Utils.convertStringListToString(new List<Id>(oldConfigIds));
			List<cscfga__Product_Configuration__c> originalConfigs = Database.query(configurationQuery);
			Map<String, cscfga__Product_Configuration__c> originalPCMap = new Map<String, cscfga__Product_Configuration__c>();
			for (cscfga__Product_Configuration__c pc : originalConfigs) {
				originalPCMap.put(pc.clone_identifier__c, pc);
			}
			Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.cscfga__Product_Configuration__c.fields.getMap();
			
			Id rootConfigId;
			String basketId;
			String oldBasketId;
			List<csbb__Product_Configuration_Request__c> pcrs = new List<csbb__Product_Configuration_Request__c>();
			List<cscfga__Product_Configuration__c> ugConfigs = new List<cscfga__Product_Configuration__c>();
			for (cscfga__Product_Configuration__c pc : sourceConfigurations) {
				cscfga__Product_Configuration__c oldPc = originalPCMap.get(pc.clone_identifier__c);
				oldBasketId = oldPc.cscfga__product_basket__c;
				for (String fieldName : fieldMap.keySet()) {
					if (fieldMap.get(fieldName).getDescribe().isUpdateable()) {
						string sdataType = string.valueOf(fieldMap.get(fieldName).getDescribe().getType());
						if (sdataType != 'REFERENCE' && !fieldName.startsWithIgnoreCase('cscfga')) {
							pc.put(fieldName, oldPc.get(fieldName));
						}
					}
				}
				if (pc.cscfga__Root_Configuration__c != null) {
					rootConfigId = pc.cscfga__Root_Configuration__c;
					pc.cscfga__Configuration_Status__c = 'Valid';
				} else {
					basketId = pc.cscfga__product_basket__c;
					pc.cscfga__Configuration_Status__c = 'Requires Update';
					pcrs.add(new csbb__Product_Configuration_Request__c(
						csbb__product_basket__c = pc.cscfga__product_basket__c,
						csbb__product_configuration__c = pc.Id,
						csbb__Optionals__c = '{}'
					));
				}
				if (pc.cscfga__Product_Definition__r.Name == 'User Group' || pc.cscfga__Product_Definition__r.Name == 'User Group SME') {
					pc.Name = 'Clone ' + pc.Name;
					ugConfigs.add(pc);
				}
			}
			
			update sourceConfigurations;
			if (!pcrs.isEmpty()) {
				insert pcrs;
			}
			if (basketId != null) {
				cscfga__product_basket__c pb = new cscfga__product_basket__c(
					cloning_in_progress__c = false,
					id = basketId
				);
				update pb;
			}
			
			if (oldBasketId != null) {
				List<Attachment> attList = [
					select body, id, parentId, name
					from Attachment
					where parentId = :oldBasketId
					and Name = 'UsageProfile'
				];
				if (!attList.isEmpty()) {
					List<Attachment> attList2 = [
						select body, id, parentId, name
						from Attachment
						where parentId = :basketId
						and Name = 'UsageProfile'
					];
					if (!attList2.isEmpty()) {
						delete attList2;
					}
					Attachment att = new Attachment(
						parentId = basketId,
						body = attList[0].body,
						name = attList[0].Name
					);
					insert att;
				}
			}
			
			
			if (rootConfigId != null) {
				Map<Id, cscfga__Product_Configuration__c> configMap = new Map<Id, cscfga__Product_Configuration__c>([ 
					select Id, Name, cscfga__Configuration_Status__c, cscfga__Validation_Message__c, cscfga__Parent_Configuration__c,
						cscfga__Product_Definition__r.Name, cscfga__Root_Configuration__c
					from cscfga__Product_Configuration__c
					where cscfga__Root_Configuration__c = :rootConfigId
					or Id = :rootConfigId
				]);
				List<cscfga__Attribute__c> attributes =[
					select Name, cscfga__Value__c, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Name,
						cscfga__Product_Configuration__r.Add_On_Recurring_List_Price__c, cscfga__Product_Configuration__r.Tenure__c, 
						cscfga__Product_Configuration__r.Group_No_of_Users__c, cscfga__Product_Configuration__r.Data_Recurring_Cost__c,
						cscfga__Product_Configuration__r.Data_Recurring_Charge__c, cscfga__Product_Configuration__r.SMS_Recurring_Cost__c,
						cscfga__Product_Configuration__r.SMS_Recurring_Charge__c, cscfga__Product_Configuration__r.QFCC__c,
						cscfga__Product_Configuration__r.Mobile_Voice_Recurring_Cost__c, cscfga__Product_Configuration__r.Mobile_Voice_Recurring_Charge__c
					from cscfga__Attribute__c
					where cscfga__Product_Configuration__c in :configMap.keySet()
					and Name in :attributeNames 
				];
				system.debug('***attributeNames'+attributeNames);

				
				List<cscfga__Product_Configuration__c> configsToUpdate = new List<cscfga__Product_Configuration__c>();
				for (cscfga__Product_Configuration__c pc : configMap.values()) {
					if (pc.cscfga__Configuration_Status__c == 'Requires Update') {
						//pc.cscfga__Configuration_Status__c = 'Valid';
						//configsToUpdate.add(pc);
					}
				}
				
				if (!configsToUpdate.isEmpty()) {
					//update configsToUpdate;
				}
				/*
				List<Attachment> oldattachments = [
					select Id, Body, Name
					from Attachment
					where ParentId = :rootConfigId
				];
				
				List<CS_Node> configurationTree = CS_Node.createTree(configMap.values());
				Map<String, Map<String, String>> attributesMap = CS_CloneBasketController.mapAttributesMap(attributes);
				List<Attachment> attachments = CS_CloneBasketController.createAttachments(configurationTree, definitionAttributesMap, attributesMap);

				delete oldattachments;
				insert attachments;
				
				cscfga__Product_Configuration__c rootConfig = [
					select Id, Clone_In_Progress__c
					from cscfga__Product_Configuration__c
					where Id = :rootConfigId
				];
				rootConfig.Clone_In_Progress__c = false;
				
				update rootConfig;
				*/
			}
			
			
		}
	}

}