public class LEMReviewPeriodsList {
	
	private final LEM_Review__c lr;
	private LEM_Review__c lrInfo;
	private List<LEM_Review_Period__c> LEMPeriods;   
	private String sortDirection = 'ASC';   
	private String sortExp = 'name';
	
	public String sortExpression {
		get {
			return sortExp;     
		}     
		set {
			//if the column is clicked on then switch between Ascending and Descending modes       
			if (value == sortExp)         
				sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';       
			else         
				sortDirection = 'ASC';       
			sortExp = value;     
		}   
	}
	
	public String getSortDirection() {    
		//if not column is selected     
		if (sortExpression == null || sortExpression == '')      
			return 'ASC';    
		else     
			return sortDirection; 
	} 
	
	public void setSortDirection(String value) {
		sortDirection = value; 
	}
		
    public LEMReviewPeriodsList(ApexPages.StandardController stdController) {
        this.lr = (LEM_Review__c)stdController.getRecord();
        this.lrInfo = [Select l.Id, l.Name, l.BTLB_Master_Object__c From LEM_Review__c l where l.Id =: lr.Id];
        ViewData();
    }
    
    public List<LEM_Review_Period__c> getLEMPeriods() {       
    	return LEMPeriods;   
    }   
    
    public Id getLrId() {
		return lr.Id;
	}
    
    public String getLrName() {
		return lrInfo.Name;
	}
    
    public PageReference ViewData() {
    	string recId = ApexPages.currentPage().getParameters().get('id');       
    	//build the full sort expression       
    	string sortFullExp = sortExpression  + ' ' + sortDirection;             
    	//query the database based on the sort expression       
    	LEMPeriods = Database.query('Select l.zCheck__c, l.iNET_Previous_30_Day_Forecast__c, l.iNET_Next_30_Day_Forecast__c, l.iNET_Actual_YTD__c, l.WLR_Lines_Previous_30_Day_Forecast__c, l.WLR_Lines_Next_30_Day_Forecast__c, l.WLR_Lines_Actual_YTD__c, l.SystemModstamp, l.Switch_Volume_Previous_30_Day_Forecast__c, l.Switch_Volume_Next_30_Day_Forecast__c, l.Switch_Volume_Actual_YTD__c, l.Switch_Value_Previous_30_Day_Forecast__c, l.Switch_Value_Next_30_Day_Forecast__c, l.Switch_Value_Actual_YTD__c, l.Sustainable_For_Next_30_Days__c, l.Status__c, l.Quarter__c, l.Period__c, l.PSM_Signoff_Date__c, l.PSM_Signoff_Comments__c, l.Name, l.Mobile_Previous_30_Day_Forecast__c, l.Mobile_Next_30_Day_Forecast__c, l.Mobile_Actual_YTD__c, l.MD_Signoff_Date__c, l.MD_Signoff_Comments__c, l.Levelled_Signoff_Date__c, l.Levelled_Signoff_Comments__c, l.LastModifiedDate, l.LastModifiedById, l.LastActivityDate, l.LEM_Review__c, l.IsDeleted, l.Improvement_Plan_Needed__c, l.Id, l.Data_Networks_Previous_30_Day_Forecast__c, l.Data_Networks_Next_30_Day_Forecast__c, l.Data_Networks_Actual_YTD__c, l.CreatedDate, l.CreatedById, l.Calls_Previous_30_Day_Forecast__c, l.Calls_Next_30_Day_Forecast__c, l.Calls_Actual_YTD__c, l.Broadband_Previous_30_Day_Forecast__c, l.Broadband_Next_30_Day_Forecast__c, l.Broadband_Actual_YTD__c From LEM_Review_Period__c l where l.LEM_Review__c=\'' + recId + '\' order by ' + sortFullExp + ' limit 1000');
    	system.debug('Select l.zCheck__c, l.iNET__c, l.iNET_Actual_YTD__c, l.iNET_90_Day_Forecast__c, l.iNET_60_Day_Forecast__c, l.iNET_30_Day_Forecast__c, l.WLR_Lines__c, l.WLR_Lines_Actual_YTD__c, l.WLR_Lines_90_Day_Forecast__c, l.WLR_Lines_60_Day_Forecast__c, l.WLR_Lines_30_Day_Forecast__c, l.SystemModstamp, l.Switch_Volume__c, l.Switch_Volume_Actual_YTD__c, l.Switch_Volume_90_Day_Forecast__c, l.Switch_Volume_60_Day_Forecast__c, l.Switch_Volume_30_Day_Forecast__c, l.Switch_Value__c, l.Switch_Value_Actual_YTD__c, l.Switch_Value_90_Day_Forecast__c, l.Switch_Value_60_Day_Forecast__c, l.Switch_Value_30_Day_Forecast__c, l.RecordTypeId, l.Quarter__c, l.Period__c, l.Name, l.Mobile__c, l.Mobile_Actual_YTD__c, l.Mobile_90_Day_Forecast__c, l.Mobile_60_Day_Forecast__c, l.Mobile_30_Day_Forecast__c, l.LastModifiedDate, l.LastModifiedById, l.LastActivityDate, l.LEM_Review__c, l.IsDeleted, l.Id, l.Data_Networks__c, l.Data_Networks_Actual_YTD__c, l.Data_Networks_90_Day_Forecast__c, l.Data_Networks_60_Day_Forecast__c, l.Data_Networks_30_Day_Forecast__c, l.CreatedDate, l.CreatedById, l.Broadband__c, l.Broadband_Actual_YTD__c, l.Broadband_90_Day_Forecast__c, l.Broadband_60_Day_Forecast__c, l.Broadband_30_Day_Forecast__c, l.BT_Finance__c, l.BT_Finance_Actual_YTD__c, l.BT_Finance_90_Day_Forecast__c, l.BT_Finance_60_Day_Forecast__c, l.BT_Finance_30_Day_Forecast__c, l.Acquisition__c, l.Acquisition_Actual_YTD__c, l.Acquisition_90_Day_Forecast__c, l.Acquisition_60_Day_Forecast__c, l.Acquisition_30_Day_Forecast__c From LEM_Review_Period__c l where l.LEM_Review__c=\'' + recId + '\' order by ' + sortFullExp + ' limit 1000');
    	return null;   
    }
}