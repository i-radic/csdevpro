/**
 * Class to handle Case Business logic for Contract Validations.
 * Used to set the Users who checked the checklist
 * Used to set the Sharing Permissions for Sales Contacts when a Sales Contact is selected in Case of type "Contract Validations"
 * Used to set the 'Agreed SLA' and 'Priority" based on 'Contract Category'
 * Used to set the 'Expected Close Date' excluding Weekends, 
 *  Test Class: TestCaseHelper 
 */
public class CaseHelper {

    private static Id contractValidationRecordTypeId;
    private static Map<String,Contract_Validation_SLA__mdt> allsla = new Map<String,Contract_Validation_SLA__mdt>();
    static
    {
        List<RecordType> recordTypeList = [SELECT id FROM RecordType WHERE SObjectType = 'Case' AND DeveloperName = 'Contract_Validations'];
        
        if(recordTypeList.size() > 0)
        {
            contractValidationRecordTypeId = recordTypeList[0].Id;            
        }
    
        // Querying Custom Metadata. Need to update this metadata object whenever we make changes to Case Picklists (Contract_Category__c,CV_Agreed_SLA__c,Priority__c)  
        Contract_Validation_SLA__mdt[] allcontsla = [SELECT Contract_Sub_Category__c,DeveloperName, MasterLabel,Agreed_SLA__c, Priority__c FROM Contract_Validation_SLA__mdt];    
        
        for (Contract_Validation_SLA__mdt md : allcontsla){ 
            allsla.put(md.Contract_Sub_Category__c,md);
        } 
    }   
    
        
     public static void beforeInsert(List<Case> caseList){
        for (Case listCase : caseList) {
            // If "Contract Category" is selected, set the "Agreed SLA", "Priority"
            if(listCase.RecordTypeId == contractValidationRecordTypeId && listCase.Contract_Category__c != Null){
                if (allsla.containsKey(listCase.Contract_Category__c)) { 
                    listCase.CV_Agreed_SLA__c = allsla.get(listCase.Contract_Category__c).Agreed_SLA__c;
                    listCase.Priority = allsla.get(listCase.Contract_Category__c).Priority__c;   
                } 
            }             
            
            // If "Agreed SLA" is not null, then set the "Expected Close Date" 
            if(listCase.RecordTypeId == contractValidationRecordTypeId && listCase.CV_Agreed_SLA__c != NULL) { 
                DateTime createdDate = DateTime.Now();
                System.debug('createdDate >>>>>>>>>>>>>>>>>>>>>>>'+createdDate);
                DateTime expectedCloseDateInclWeekend;
                
                if (listCase.CV_Agreed_SLA__c == '48 hours') {                
                    expectedCloseDateInclWeekend= createdDate.addHours(48);
                } else if (listCase.CV_Agreed_SLA__c == '72 hours') {
                    expectedCloseDateInclWeekend= createdDate.addHours(72);
                } else if (listCase.CV_Agreed_SLA__c == '120 hours'){
                    expectedCloseDateInclWeekend= createdDate.addHours(120);
                }
                System.debug('expectedCloseDateInclWeekend>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+expectedCloseDateInclWeekend);                            
               
               listCase.Expected_Close_Date_1__c = expectedCloseDateInclWeekend + expectedCloseDateExcludingWeekends (createdDate, expectedCloseDateInclWeekend);     
          }   
       }   
    }
    
    
    public static void beforeUpdate(List<Case> updatedCaseList,Map<Id,Case> oldCaseMap)
    {
        Map<Id,Id> caseIdToUserIdMap = new Map<Id,Id>();

        for(Case updateCase : updatedCaseList)
        {
            
           if(updateCase.RecordTypeId == contractValidationRecordTypeId){
            
                if(updateCase.X1st_Check__c != Null && oldCaseMap.get(updateCase.id).X1st_Check__c != updateCase.X1st_Check__c)
                {                
                    updateCase.X1st_Check_Completed_By__c = Userinfo.getUserID();           
                }
                
                if(updateCase.Subsequent_Check_1__c != Null && oldCaseMap.get(updateCase.id).Subsequent_Check_1__c != updateCase.Subsequent_Check_1__c)
                {                
                    updateCase.Subsequent_Completed_By__c = Userinfo.getUserID();           
                }
                
                if(updateCase.Subsequent_Check_2__c != Null && oldCaseMap.get(updateCase.id).Subsequent_Check_2__c != updateCase.Subsequent_Check_2__c)
                {                
                    updateCase.Subsequent_Completed_By_1__c = Userinfo.getUserID();           
                }
                
                if(updateCase.Completion_of_Checklist__c != Null && oldCaseMap.get(updateCase.id).Completion_of_Checklist__c != updateCase.Completion_of_Checklist__c)
                {                
                    updateCase.Completion_of_Checklist_Completed_By__c = Userinfo.getUserID();           
                }
                
               // Check if the Case is Closed. If Yes, set the Case Age excluding Weekends 
                if (updateCase.isClosed == True) {                        
                   updateCase.Time_to_Complete_Days__c = daysBetweenExcludingWeekends (updateCase.CreatedDate, updateCase.ClosedDate);             
                }
                
                
            //  if(updateCase.Contract_Category__c != Null && updateCase.Contract_Category__c != oldCaseMap.get(updateCase.id).Contract_Category__c){
                if(updateCase.Contract_Category__c != Null){
                // If "Contract Category" is selected, set the "Agreed SLA", "Priority"                
                    if (allsla.containsKey(updateCase.Contract_Category__c)) { 
                        updateCase.CV_Agreed_SLA__c = allsla.get(updateCase.Contract_Category__c).Agreed_SLA__c;
                        updateCase.Priority = allsla.get(updateCase.Contract_Category__c).Priority__c;   
                    } 
                }     
                
              // If "Agreed SLA" is not null, then set the "Expected Close Date"  
                if(updateCase.CV_Agreed_SLA__c != NULL) {                                     
                    DateTime createdDate = updateCase.CreatedDate;
                    DateTime expectedCloseDateInclWeekend;
                    
                    if (updateCase.CV_Agreed_SLA__c == '48 hours') {                
                        expectedCloseDateInclWeekend= createdDate.addHours(48);
                    } else if (updateCase.CV_Agreed_SLA__c == '72 hours') {
                        expectedCloseDateInclWeekend= createdDate.addHours(72);
                    } else if (updateCase.CV_Agreed_SLA__c == '120 hours'){
                        expectedCloseDateInclWeekend= createdDate.addHours(120);
                    }
                    System.debug('expectedCloseDateInclWeekend>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+expectedCloseDateInclWeekend);                            
                                
                   updateCase.Expected_Close_Date_1__c = expectedCloseDateInclWeekend + expectedCloseDateExcludingWeekends (createdDate,expectedCloseDateInclWeekend); 
                   System.debug('expectedCloseDate Excluding Weekend>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+ updateCase.Expected_Close_Date_1__c);
                }   
           } 
        }
    }    
    

    public static void afterInsert(List<Case> caseList)
    {
        Map<Id,Id> caseIdToUserIdMap = new Map<Id,Id>();

        for(Case newCase : caseList)
        {
            if(newCase.RecordTypeId == contractValidationRecordTypeId &&
               newCase.Contact__c != NULL)
            {                
                caseIdToUserIdMap.put(newCase.Id,newCase.Contact__c);                   
            }              
        }

        if(caseIdToUserIdMap.size() > 0)
            shareCaseWith(caseIdToUserIdMap);
    }

    public static void afterUpdate(List<Case> updatedCaseList,Map<Id,Case> oldCaseMap)
    {
        Map<Id,Id> caseIdToUserIdMap = new Map<Id,Id>();
        Map<Id,Id> deleteCaseIdToUserIdMap = new Map<Id,Id>();
        Set<String> deleteCaseShareSet = new Set<String>();
        for(Case newCase : updatedCaseList)
        {                
            if(newCase.RecordTypeId == contractValidationRecordTypeId &&
               newCase.Contact__c != NULL 
               && newCase.Contact__c != oldCaseMap.get(newCase.id).Contact__c) 
            {
                caseIdToUserIdMap.put(newCase.Id,newCase.Contact__c);                                      
            }
            
            if(newCase.RecordTypeId == contractValidationRecordTypeId &&               
               newCase.Contact__c != oldCaseMap.get(newCase.id).Contact__c) 
            {
              deleteCaseIdToUserIdMap.put(newCase.Id,oldCaseMap.get(newCase.id).Contact__c); 
              deleteCaseShareSet.add(String.valueOf(newCase.Id)+String.valueOf(oldCaseMap.get(newCase.id).Contact__c));  
            }                                     
        }

        if(caseIdToUserIdMap.size() > 0)
            shareCaseWith(caseIdToUserIdMap);   
            
        if (deleteCaseIdToUserIdMap.Size() > 0)
           deleteCaseShare (deleteCaseShareSet,deleteCaseIdToUserIdMap);   
    }

    public static void shareCaseWith(Map<Id,Id> salesContactMap)
    {
         List<CaseShare> caseShares = new List<CaseShare>();

         for(Id caseToBeShared : salesContactMap.keySet())
         {                 
             CaseShare caseShareRecord = new CaseShare();
             caseShareRecord.CaseId = caseToBeShared;
             caseShareRecord.UserOrGroupId = salesContactMap.get(caseToBeShared);
             caseShareRecord.CaseAccessLevel = 'read';
             
             caseShares.add(caseShareRecord);
         }

         Database.SaveResult[] caseShareInsertResult = Database.insert(caseShares,false);    
    }
    
    
    public static void deleteCaseShare (Set<String> deleteCaseShareSet,Map<Id,Id> deleteCaseIdToUserIdMap)
    {
         List<CaseShare> deleteCaseShares = new List<CaseShare>();         
         Map<ID,CaseShare> caseShMap = New Map<ID,CaseShare>([Select ID,CaseId,UserOrGroupId From CaseShare where CaseId IN : deleteCaseIdToUserIdMap.keyset()]);
     
         Map<String,CaseShare> delCaseShare = new Map<String,CaseShare>();      
         for(CaseShare caseShareToBeDeleted : caseShMap.values())
         {    
            delCaseShare.put(String.valueOf(caseShareToBeDeleted.CaseId)+String.valueOf(caseShareToBeDeleted.UserOrGroupId),caseShareToBeDeleted ); 
         }
         
         for (String delCase:deleteCaseShareSet)
         {
            if (delCaseShare.containsKey(delCase))            
               deleteCaseShares.add(delCaseShare.get(delCase));            
         }
         
        if(deleteCaseShares.Size() > 0)
         Database.DeleteResult[] caseShareDeleteResult = Database.delete(deleteCaseShares,false);          
    }
    
    
    
    // Method used to calculate No. of Weekend Days between Expected Closed Date and Case Created Date excluding Weekends
     public static Integer expectedCloseDateExcludingWeekends (Datetime CreatedDate, Datetime ExpectedClosedDate) {
        Integer i = 0;
    
        while (CreatedDate <= ExpectedClosedDate) {
             System.debug('Created date format >>>>>>>>>>>>>>>>>'+CreatedDate.format('E'));
            if (CreatedDate.format('E') == 'Sat' || CreatedDate.format('E') == 'Sun') {
                i++; 
                if (ExpectedClosedDate.format('E') == 'Sat'){
                    i=i+1;
                } 
              System.debug('No. of Weekend Days >>>>>>>>>>>>>>>>>>'+i);   
            }
            CreatedDate = CreatedDate.addDays(1);
        }   
         System.debug('No. of Weekend Days between 2 Dates >>>>>>>>>>>>>>>>>>'+i);  
        return i;  
          
      }        
    
    
    // Method used to calculate No. of Days between Case Closed Date and Case Created Date excluding Weekends
     public static Integer daysBetweenExcludingWeekends(Datetime CreatedDate, Datetime ClosedDate) {
        Integer i = 0;
    
        while (CreatedDate < ClosedDate) {
            if (CreatedDate.format('E') != 'Sat' && CreatedDate.format('E') != 'Sun') {
                i++;  
            }
            CreatedDate = CreatedDate.addDays(1);
        }
    
        return i;      
      }  
}