@IsTest
public class CS_DiscountBasketSaveObserverTest  {
	testMethod static void testApprovalObserver(){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
        
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc.Calculations_Product_Group__c = 'Future Mobile';
        INSERT pc;

        CS_DiscountBasketSaveObserver cmtrl = new CS_DiscountBasketSaveObserver();

        Test.startTest();

        cmtrl.execute(basket.Id);

        Test.stopTest();
	}
}