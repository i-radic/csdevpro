@isTest
private class Test_CRFController {

    static testMethod void myUnitTest() {
        //  set up data
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99991';
        insert a;
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o; 
        String RTId = [Select Id from RecordType where DeveloperName=:'One_Plan'].Id;
        //  insert CRF - ensure oneplan product is selected to attain coverage
        CRF__c crf = new CRF__c(Opportunity__c = o.Id, 
                                    Contact__c = c.Id, 
                                    Account_number__c = 'ls3456789',
                                    Customer_bank_account_number__c = '111122223',
                                    //Customer_Email__c = 'test@test456.com',
                                    //Customer_phone_number__c = '9632587410',
                                    Alt_contact_phone_number__c = '7854123690',
                                    Order_type__c = 'Linked Delivery', 
                                    Payment_Method__c = 'DD details entered',
                                    DD_Confirm__c = True,
                                    Password__c = 'qwerty12',
                                    Sort_Code__c='123123',
                                    Override_Account_Name__c='Simon',
                                    Your_sales_channel_ID__c='B101',
                                    How_Many_Employees_are_in_your_company__c='10 or less',
                                    OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN
                                    );
                                    
        insert crf; 

        System.debug('---->'+crf.Id);
        System.debug('---->'+crf.CreatedDate);
        System.debug('---->'+crf.LastModifiedDate);
 
        
        test.starttest();
         ID newid = crf.id;
        CRF__c ds48 = [SELECT id,OP_Committment_Period__c,Existing_customer__c,CreatedBy.Name,ADSL2__c,BT_PC_backup__c,OP_Which_One_Plan_is_required__c,BT_PC_security_qty__c,Business_email_on_address_1__c,Business_email_on_address_2__c,Business_email_on_address_3__c,Business_email_on_address_4__c,Business_email_on_address_5__c,Web_consult_build_consult__c,Bt_Web_hosting_starterpack_basic__c,Bt_Web_hosting_prof_adv__c,BT_eSHOP__c,BT_eSHOP_elite__c,ITSM__c,Line1_Unlimited_calls_package__c,Line1_Falcon_IP_phone__c,Line2_Unlimited_calls_package__c,Line2_Falcon_IP_phone__c,Falcon_IP_phone_sale__c,Add_Out_of_Area_Geo_sale__c,Upgrade_to_Call_Mider_for_Bus__c,Bolt_on_Mobile_BB_1GB__c,DD_Confirm__c,Keep_Existing_Billing_Account__c,CreatedDate,How_Many_Employees_are_in_your_company__c,OP_Nominal_Committed_Spend__c,OP_Unlimited_Calls_Package_needed_1__c,OP_Unlimited_Calls_Package_needed_2__c,OP_Unlimited_Calls_Package_needed_3__c,OP_Unlimited_Calls_Package_needed_4__c,OP_Unlimited_Calls_Package_needed_5__c,OP_Unlimited_Calls_Package_needed_6__c,OP_Unlimited_Calls_Package_needed_7__c,OP_Unlimited_Calls_Package_needed_8__c,OP_Unlimited_Calls_Package_needed_9__c,OP_Unlimited_Calls_Package_needed_10__c FROM CRF__c WHERE id = :newid LIMIT 1];
        //ds48.ActivityDate = Date.today().addDays(3);
        update ds48;
        
        test.stoptest();
        
        PageReference pageRef=Page.CRF_pdf_form;
        Test.setCurrentPageReference(pageRef);
        
        //if(crf.CreatedDate!=null){
        CRFController myController = new CRFController(new ApexPages.StandardController(ds48));   

        ds48.OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN_INCLUSIVE;
        myController = new CRFController(new ApexPages.StandardController(ds48));
        ds48.OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN_PLUS;
        myController = new CRFController(new ApexPages.StandardController(ds48));
        ds48.OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN_PLUS_INCLUSIVE;
        myController = new CRFController(new ApexPages.StandardController(ds48));
        ds48.OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN_PLUS_INCLUSIVE_SPEICAL_OFFER;
        myController = new CRFController(new ApexPages.StandardController(ds48));
        ds48.OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN_TRADITIONAL;
        myController = new CRFController(new ApexPages.StandardController(ds48));
        ds48.OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN_TRADITIONAL_INCLUSIVE;
        myController = new CRFController(new ApexPages.StandardController(ds48));
        ds48.OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN_TRADITIONAL_INCLUSIVE_SPEICAL_OFFER;
        myController = new CRFController(new ApexPages.StandardController(ds48));

        
        //   retrieve properties
        String str = myController.heading;
        str = myController.first_paragraph;
        str = myController.second_paragraph;
        str = myController.second_paragraph_1;
        str = myController.second_paragraph_2;
        str = myController.third_paragraph;
        str = myController.fourth_paragraph;
        str = myController.unlimited_calls_required;
        str = myController.customer_statement;
        str = myController.tc_1_statement;
        str = myController.tc_2_statement;
        str = myController.tc_3_statement;
        str = myController.tc_4_statement;
        str = myController.tc_5_statement;
        str = myController.tc_6_statement;
        str = myController.tc_7_statement;
        //}
    }
}