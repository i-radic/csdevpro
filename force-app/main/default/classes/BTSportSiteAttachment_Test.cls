@isTest(SeeAllData=true)
private class BTSportSiteAttachment_Test {
testmethod static void Test_FlagIsSetExist(){
        Test_Factory.setProperty('IsTest', 'yes');

Date uDate = date.today().addDays(7);
                
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BT Sport Test';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.Product_Family__c = 'BT Sport';
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty}); 


 BT_Sport_CL__c Header1 = new BT_Sport_CL__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport_CL__c [] {Header1});        

test.startTest();

BT_Sport_Site__c Site1 = new BT_Sport_Site__c();
        Site1.BT_Sport_CL__c = Header1Result[0].id; 
        Site1.Product__c = 'BT Sport Total';
        Site1.Site_Type__c = 'BT Sport Total Pub (GB)';
        Site1.Contract_Type__c = '12 month contract';
        Site1.Band__c = 'Band A - £0 - £5,000';
        Site1.Discounts__c = 'First Month FREE';
        Database.SaveResult[] Site1Result = Database.insert(new BT_Sport_Site__c [] {Site1});
    
        Blob a = Blob.valueOf('Test Data');
        Attachment BTSAttachmenta = new Attachment();
        BTSAttachmenta.body = a;
        BTSAttachmenta.name = 'Order_BTSportSite';
        BTSAttachmenta.ParentId = Site1Result[0].Id;
        Database.SaveResult[] BTSportSiteAttachmentResultx = Database.insert(new Attachment[] {BTSAttachmenta});  

        Blob b = Blob.valueOf('Test Data');
        Attachment BTSAttachmentb = new Attachment();
        BTSAttachmentb.body = b;
        BTSAttachmentb.name = 'Order_BTSportSite';
        BTSAttachmentb.ParentId = Site1Result[0].Id;
        Database.SaveResult[] BTSportSiteAttachmentResult = Database.insert(new Attachment[] {BTSAttachmentb});  

        delete BTSAttachmenta;

        delete BTSAttachmentb;

StaticVariables.setTaskIsRun(false);
}
}