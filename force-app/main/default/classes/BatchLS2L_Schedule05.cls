global class BatchLS2L_Schedule05 implements Schedulable{
        
global void execute(SchedulableContext sc) {
    //batch for email
    BatchLS2L_05AlertEmail lCountE = new BatchLS2L_05AlertEmail();
    // query changed to return no results until switch on is required.
    lCountE.query = 'SELECT Id, Email, Name, zLeadsOwned__c FROM User WHERE isActive = TRUE AND zChannel__c = \'Desk\' AND zLeadsOwned__c > 0 AND Name = \'dksnfdlsdfn\'';
    database.executeBatch(lCountE, 10);   
}
}