global with sharing class CustomButtonPLNPV extends csbb.CustomButtonExt {
    public String performAction(String basketId) {
        Map<Id, cscfga__Product_Configuration__c> configs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{new cscfga__Product_Basket__c(Id = basketId)});
        cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
        try {
            CS_ProductBasketService.prepareTechFund(basket, configs, 'Tech_Fund__c');
            
            CS_Util.upsertReportConfigurationAttachment(configs.keySet());
            CS_ProductBasketService.calculateBasketTotals(configs, basket);
            CS_ProductBasketService.setSpecialConditions(configs, basket);
            update basket;
        }
        catch (Exception e) {}
        return '{"status":"ok","redirectURL":"/apex/cspl__PLReportNew?id=' + basketId + '&npvMode=true"}';
    }
}