@IsTest
public class CS_DiscountApprovalObserverTest  {

	testMethod static void testApprovalObserver(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test', opp);
        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
        pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc.Approval_Level_3__c = UserInfo.getUserId();
        pc.Approval_Level_2__c = UserInfo.getUserId();
        pc.Approval_Level_1__c = UserInfo.getUserId();
        pc.Approval_Status__c = 'Approved';
        INSERT pc;

        CS_DiscountApprovalObserver dao = new CS_DiscountApprovalObserver();

        Test.startTest();

        dao.execute(pc.Id, 'approve');

        Test.stopTest();
	}
}