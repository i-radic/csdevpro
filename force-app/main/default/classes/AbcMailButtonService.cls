global class AbcMailButtonService {

    WebService static void SendEmailNotification(string id) {

        //create a mail object to send a single email.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<string> cclist = new List<string>();
        //cclist.add('phanidra.mangipudi@bt.com');
        //set the email properties
        iDepot__c record = [SELECT Id, Name, OwnerId, ABC2_Status__c, ABC2_Sub_Status__c, Request_Type__c, Company_Name_Trading_Name__c, ABC2_Raised_By__c    FROM iDepot__c WHERE Id = :id LIMIT 1];
        
        // getting PSM managers Id
        Boolean isSentPSM = false;
        
        user o = [SELECT Name,ProfileName__c, ManagerId FROM User WHERE Id =: record.OwnerId LIMIT 1];
        
        try{
        	o = [SELECT Name ,ProfileName__c, email, Manager.Name, ManagerId FROM User WHERE Id =: o.ManagerId LIMIT 1];
	        if(o.ProfileName__c == 'BTLB: RDs and PSMs' && !isSentPSM){
	            cclist.add(o.email);
	            isSentPSM = true;
	        }
	        
	        if(o.ManagerId != null)
	            o = [SELECT Id, ProfileName__c, email FROM User WHERE Id = :o.ManagerId LIMIT 1];
	            
	        if(o.ProfileName__c == 'BTLB: RDs and PSMs' && !isSentPSM){
	            cclist.add(o.email);
	        }
	        
	        mail.setTargetObjectId(record.OwnerId);        
	        mail.setSenderDisplayName(Userinfo.getName());
	        mail.setCcAddresses(cclist);        
	        mail.setSubject('Salesforce Base Enquiry Ref '+record.Name+' Requires Your Attention');
	        string link = 'https://emea.salesforce.com/'+record.Id;
	        string mailBody = 'Base Enquiry ref: '+record.Name+' requires your attention <br /><br />' +
	                          'The current status is: '+record.ABC2_Status__c+'<br />' +
	                          'Sub Status:'+record.ABC2_Sub_Status__c+'<br />' +
	                          'Request Type: '+record.Request_Type__c+'<br />' +
	                          'Company Name/Trading Name:'+record.Company_Name_Trading_Name__c+'<br />' + 
	                          'Raised By:'+record.ABC2_Raised_By__c+'<br /><br />' +
	                          'Please click link below to view the full record paying particular notice to any new attachments or notes.<br />' +
	                          ''+link+'<br /><br />' +
	                          'Thank you';                          
	                            
	        mail.setHtmlBody(mailBody);
	
	        //send the email
	        mail.setSaveAsActivity(false);
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	        
	        // inserting record in iDepot History Object
	        
	        Note n = new Note();
	        n.OwnerId = UserInfo.getUserId();
	        n.ParentId = record.id;
	        n.Title = 'Testing Notes';
	        //n.Body= 'Urge email sent to <b>'+o.Name+'</b> and PSM : <b>'+u.Name+', '+u.Manager.Name+'</b>';
	        n.Body = 'Urge email sent to Record Owner & PSM\'s';
	        insert n;  
        } catch(Exception e){
        	system.debug('ABC email Error '+e);
        }    
        	
    }
}