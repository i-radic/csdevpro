global class BatchTaskUpdate implements Database.Batchable<SObject>{
	 
    private String query;
    private Double xday;
    private Date endDate;
    
    global BatchTaskUpdate(String q, Double newXday, Date newEndDate){
        this.query = q;
        this.xday =  newXday;
        this.endDate = newEndDate;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Task> tasks = new List<Task>();
        for(Sobject t : scope){
            Task task = (Task)t;
            task.X_Day_Rule_Copy__c = xday;
            task.ActivityDate = endDate;
            tasks.add(task);
        } 
        update tasks;
    }

    global void finish(Database.BatchableContext BC){
    }
}