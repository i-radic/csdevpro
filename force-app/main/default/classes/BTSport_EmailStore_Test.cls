@isTest
private class BTSport_EmailStore_Test {

    static testMethod void UnitTest() {
    
        Test_Factory.setProperty('IsTest', 'yes');
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
		System.runAs( thisUser ){
    
     	 TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }

        //StaticVariables.setContactIsRunBefore(False);     
        RecordType BTSOppyType = [select id from RecordType where SobjectType='Opportunity' and name ='BT Sport' limit 1];
        RecordType BTSHeaderType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='Header' limit 1];
        
        Date uDate = date.today().addDays(7);
                
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BTS';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.RecordTypeId = BTSOppyType.Id;
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});  
        
              
        BT_Sport__c Header1 = new BT_Sport__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Header1.RecordTypeId = BTSHeaderType.id;
        Header1.BT_Sport_MSA_Oppy__c = 'No';
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport__c [] {Header1}); 
       
       BT_Sport__c bts = [Select Id, Name From BT_Sport__c LIMIT 1]; 
    
        // Create a new email, envelope object and Attachment
       Messaging.InboundEmail email = new Messaging.InboundEmail();
       Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
       
       list<string> toAddresses = new list<string>();
       toAddresses.add('test@testaddress.com');        
       email.toAddresses = toAddresses; 
       email.subject = 'test' + ' ' + bts.Name;
       email.htmlBody = 'Hello, this a test email body. for testing purposes only. Bye';
           
       // set the body of the attachment
       inAtt.body = blob.valueOf('test');
       inAtt.fileName = 'my attachment name';
       inAtt.mimeTypeSubType = 'plain/txt';
    
       email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt }; 
    
       // call the class and test it with the data in the testMethod
       BTSport_EmailStore  BTSport_EmailStoreObj = new BTSport_EmailStore();
       BTSport_EmailStoreObj.handleInboundEmail(email, env ); 
    }

}