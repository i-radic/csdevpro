global class CS_CustomLookupBTOPUserSubExtras extends cscfga.ALookupSearch{

    public override String getRequiredAttributes(){
        return '["Product","BT Products in Basket","Product Name","Exclude Product","Excluded product addon names"]';
    }

    public override Object[] doLookupSearch(Map<String, String>
			searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset,
			Integer pageLimit){

    	final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
		final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
		Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
			
		String product = searchFields.get('Product');
		String productName = searchFields.get('Product Name');
		String btProductsInBasket = searchFields.get('BT Products in Basket');
		String priceItemDescription = '';
		String excludeProduct = searchFields.get('Exclude Product');
		String excludeProductAddons = searchFields.get('Excluded product addon names');
		List<String> excludeProductList = new List<String>();
		if (excludeProduct != null)
			excludeProductList = excludeProduct.split(',');
		List<String> excludeProductAddonsList = new List<String>();
		if(excludeProductAddons != null)
            excludeProductAddonsList = excludeProductAddons.split(',');
		
		String searchTerm = searchFields.get('searchValue');
        if (string.isEmpty(searchTerm)) {
            searchTerm = '%';
        } else {
            searchTerm = '%' + searchTerm + '%';
        }

		//this part not needed after moving Port Landline Numbers under BTOP Core
        //if(btProductsInBasket.contains('Convergence Services') && productName.equals('Landline')) {
        	//// ones with Convergence Services in description
        	//priceItemDescription = 'AND cspmb__Price_Item_Description__c LIKE \'%Convergence Services%\'';
        //}
        //else if(!btProductsInBasket.contains('Convergence Services') && productName.equals('Landline')){
        	//// ones without Convergence Services in description
        	//priceItemDescription = 'AND (NOT cspmb__Price_Item_Description__c LIKE \'%Convergence Services%\')';
        //}

        DescribeSObjectResult describeResult = cspmb__Price_Item__c.getSObjectType().getDescribe();
		List<String> fieldNames = new List<String>(describeResult.fields.getMap().keySet());
		//String query = ' SELECT ' + String.join( fieldNames, ',' ) + ' FROM ' + describeResult.getName() + ' WHERE	cspmb__Is_Active__c = true AND Product_Type__c = :product AND Record_Type_Name__c = \'BTOP User Subscription\' AND Name LIKE :searchTerm ' + priceItemDescription + ' ORDER BY Sequence__c LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset';
		String query = ' SELECT ' + String.join( fieldNames, ',' ) + ' FROM ' + describeResult.getName() + ' WHERE	cspmb__Is_Active__c = true AND Product_Type__c = :product AND Record_Type_Name__c = \'BTOP User Subscription\' AND Name LIKE :searchTerm AND Id NOT IN :excludeProductList AND Name NOT IN :excludeProductAddonsList ORDER BY Sequence__c LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset';

		List<SObject> pcList = Database.query(query);

		return Database.query(query);
    }
}