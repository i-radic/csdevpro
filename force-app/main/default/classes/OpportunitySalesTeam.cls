//
// History
//
// Version      Date            Author          Comments
// 1.0.0        10-03-2011      Dan Measures    Initial version - CR2627
//

//
// Comments
//
// 1. This classes method(s) are called by OpportunitySalesTeamController to enable a user to update 
//		a field on an Opp they haven't got read/write access too.  This field 'Sale Team Access Requested' is used for
//		reporting purposes and denotes at least 1 x Opp Sales Team Request has been submitted for the Opportunity.
//
public without sharing class OpportunitySalesTeam {
	public static void markOppSalesTeamAccessRequested(Opportunity o){
		o.Sales_Team_Access_Requested__c = true;
		update o;
	}
}