@isTest

private class Test_emailToSM {
    static TestMethod void RunValidTest(){        
        Test_Factory.SetProperty('IsTest', 'yes');
        
        string SYSTEM_ADMINISTRATOR_PROFILE_ID = '00e20000001MX7z';
        Id CASE_REC_TYPE_CVD ='01220000000AG7XAAW';
        
        // TODO - implement
        List<User> thisUser = [Select id, Run_Apex_Triggers__c, Manager_EIN__c from User where id = :UserInfo.getUserId() Limit 1];
        thisUser[0].Run_Apex_Triggers__c = False;
        update thisUser[0]; 
        
        User uM = new User();
        uM.Username = '999999990@bt.com';
        uM.Ein__c = '999999990';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = SYSTEM_ADMINISTRATOR_PROFILE_ID;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});

        User uGM = new User();
        uGM.Username = '999999991@bt.com';
        uGM.Ein__c = '999999991';
        uGM.LastName = 'TestLastname';
        uGM.FirstName = 'TestFirstname';
        uGM.MobilePhone = '07918672032';
        uGM.Phone = '02085878834';
        uGM.Title='What i do';
        uGM.OUC__c = 'DKW';
        uGM.Manager_EIN__c = '999999990';
        uGM.ManagerId = uMResult[0].id;
        uGM.Email = 'no.reply@bt.com';
        uGM.Alias = 'boatid01';
        uGM.TIMEZONESIDKEY = 'Europe/London';
        uGM.LOCALESIDKEY  = 'en_GB';
        uGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uGM.PROFILEID = SYSTEM_ADMINISTRATOR_PROFILE_ID;
        uGM.LANGUAGELOCALEKEY = 'en_US';
        uGM.email = 'no.reply@bt.com';
        Database.SaveResult[] uGMResult = Database.insert(new User [] {uGM});
 
        User uDGM = new User();
        uDGM.Username = '999999002@bt.com';
        uDGM.Ein__c = '999999002';
        uDGM.LastName = 'TestLastname';
        uDGM.FirstName = 'TestFirstname';
        uDGM.MobilePhone = '07918672032';
        uDGM.Phone = '02085878834';
        uDGM.Title='What i do';
        uDGM.OUC__c = 'DKW';
        uDGM.Manager_EIN__c = '999999991';
        uDGM.ManagerId = uGMResult[0].id;
        uDGM.Email = 'no.reply@bt.com';
        uDGM.Alias = 'boatid01';
        uDGM.managerID = uGM.Id;
        uDGM.TIMEZONESIDKEY = 'Europe/London';
        uDGM.LOCALESIDKEY  = 'en_GB';
        uDGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uDGM.PROFILEID = SYSTEM_ADMINISTRATOR_PROFILE_ID;
        uDGM.LANGUAGELOCALEKEY = 'en_US';
        uDGM.email = 'no.reply@bt.com';
        Database.SaveResult[] uDGMResult = Database.insert(new User [] {uDGM});
        
        User uSM = new User();
        uSM.Username = '999999993@bt.com';
        uSM.Ein__c = '999999993';
        uSM.LastName = 'TestLastname';
        uSM.FirstName = 'TestFirstname';
        uSM.MobilePhone = '07918672032';
        uSM.Phone = '02085878834';
        uSM.Title='What i do';
        uSM.OUC__c = 'DKW';
        uSM.Manager_EIN__c = '999999992';
        uSM.ManagerId = uDGMResult[0].id;
        uSM.Email = 'no.reply@bt.com';
        uSM.Alias = 'boatid01';
        uSM.managerID = uDGM.Id;
        uSM.TIMEZONESIDKEY = 'Europe/London';
        uSM.LOCALESIDKEY  = 'en_GB';
        uSM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uSM.PROFILEID = SYSTEM_ADMINISTRATOR_PROFILE_ID;
        uSM.LANGUAGELOCALEKEY = 'en_US';
        uSM.email = 'no.reply@bt.com';
        insert uSM;
      
        //link user running test to hierachy created above
        thisUser[0].ManagerId = uSM.ID;
        update thisUser[0];
        
        insert Test_Factory.CreateAccountForDummy();
        
        Account acc1 = Test_Factory.CreateAccount(); 
        acc1.LOB_Code__c = 'TEST';        
        acc1.SAC_Code__c = 'TEST2';        
        acc1.LE_Code__c = null;        
        Database.Saveresult accResult = Database.insert(acc1);           

        Contact con1 = Test_Factory.CreateContact(); 
        con1.SAC_code__c = acc1.SAC_Code__c; 
        Database.Saveresult conResult = Database.insert(con1);           
                
        Account_Team_Contacts__c team = Test_Factory.CreateAccountTeamContacts(accResult.getId());
        insert team;
        
        Case c = new Case();
        c.Vol_Reference__c = 'VOL011-99384968123';
        c.AccountId = acc1.ID;
        c.ContactId = con1.Id;
        c.Status = 'New'; 
        c.Origin = 'Phone';
        c.Reason = 'test';
        // c.Front_Office_ein__c = '80253721h';
        //c.Front_Office_ein__c = '605821420';
        c.Front_Office_ein__c = '803226959';
        c.RecordTypeId = CASE_REC_TYPE_CVD;
        c.CVD_Feedback__c = true;
        try{
        	insert c;
        }
        catch(exception e){
        }
        
        /*Case case1 = Test_Factory.CreateCase(accResult.getId(), conResult.getId()); 
        case1.Status = 'New'; 
        case1.Origin = 'Phone'; 
        insert case1;*/
    }
}