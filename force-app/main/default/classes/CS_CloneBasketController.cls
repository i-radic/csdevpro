global with sharing class CS_CloneBasketController
{
    ApexPages.StandardController stdController;
    Id basketId;

    global CS_CloneBasketController(ApexPages.StandardController controller)
    {
        this.stdController = controller;
        this.basketId = this.stdController.getId();
        this.stdController
        .getRecord()
        .put('Name', 'Clone ' + this.stdController.getRecord().get('Name'));
        this.stdController.getRecord().put('Id', null);
        this.stdController.getRecord().put('Special_Conditions_Id__c', null);
        this.stdController.getRecord().put('Approval_Status__c', 'Draft');
        this.stdController.getRecord().put('Cloning_in_Progress__c', true);
    }

    public PageReference cloneBasket()
    {
        PageReference returnValue;
        returnValue = this.stdController.save();
        //
        // Copy product configurations to the new basket
        //
        Map<Id, cscfga__Product_Configuration__c> configMap =
            new Map<Id, cscfga__Product_Configuration__c>
                ([ SELECT Id, cscfga__Product_Basket__c, clone_identifier__c
                   FROM cscfga__Product_Configuration__c
                   WHERE cscfga__Parent_Configuration__c = null
                   AND cscfga__Product_Basket__c = :basketId ]);
        Integer i = 0;
        List<cscfga__Product_Configuration__c> pcsToClone = [
        	select id, clone_identifier__c, cscfga__Product_Basket__c
        	from cscfga__Product_Configuration__c
        	where cscfga__Product_Basket__c = :basketId
        ];
		for (cscfga__Product_Configuration__c pc : pcsToClone) {
			pc.clone_identifier__c = String.valueOf(pc.cscfga__Product_Basket__c) + String.valueOf(i);
			i++;
		}
		update pcsToClone;

        cscfga.ProductConfigurationBulkActions.createCopyBuilder(configMap.keySet())
        .target(null, this.stdController.getId())
        .eventHandler(new CS_AfterCopyEventHandler())
        .noRevalidation()
        .runBatch();

        return returnValue;
    }

    public static Map<String, Set<String>> mapDefinitionAttributes
        (Map<String, CS_Related_Product_Attributes__c> settings)
    {
        Map<String, Set<String>> returnValue =
            new Map<String, Set<String>>();
        for (String definitionName : settings.keySet())
        {
            Set<String> attributes = new Set<String>();
            CS_Related_Product_Attributes__c setting =
                settings.get(definitionName);
            attributes.addAll(mapAttributes(setting.Attribute_Names__c));
            attributes.addAll(mapAttributes(setting.Attribute_Names_2__c));
            returnValue.put(definitionName, attributes);
        }
        return returnValue;
    }

    public static Set<String> mapAttributes(String value)
    {
        Set<String> returnValue = new Set<String>();
        if (value != null)
        {
            returnValue.addAll(value.split(','));
        }
        return returnValue;
    }

    public static Set<String> foldAttributes
        ( Map<String, Set<String>> definitionAttributes )
    {
        Set<String> returnValue = new Set<String>();
        for (Set<String> attributes : definitionAttributes.values())
        {
            returnValue.addAll(attributes);
        }
        return returnValue;
    }

    public static Map<String, Map<String, String>> mapAttributesMap
        ( List<cscfga__Attribute__c> attributes )
    {
        Map<String, Map<String, String>> returnValue =
            new Map<String, Map<String, String>>();
        for (cscfga__Attribute__c attribute : attributes)
        {
            Map<String, String> attributesMap =
                returnValue.get(attribute.cscfga__Product_Configuration__c) != null
                    ? returnValue.get(attribute.cscfga__Product_Configuration__c)
                    : new Map<String, String>();
            attributesMap.put(attribute.Name, attribute.cscfga__Value__c);
            returnValue.put
                ( attribute.cscfga__Product_Configuration__c
                , attributesMap );
        }
        return returnValue;
    }

    public static List<Attachment> createAttachments
        ( List<CS_Node> configurationTree
        , Map<String, Set<String>> definitionAttributes
        , Map<String, Map<String, String>> attributesMap )
    {
        List<Attachment> returnValue = new List<Attachment>();
        CS_Node.FoldOp foldOp = new CS_Node.FoldAttributes
            ( definitionAttributes
            , attributesMap );
        for (CS_Node root : configurationTree)
        {
            Map<String, Map<String, Map<String, String>>> foldResult =
            (Map<String, Map<String, Map<String, String>>>)CS_Node.foldTree
                ( new Map<String, Map<String, Map<String, String>>>()
                , root.getChildren()
                , foldOp );
            returnValue.addAll(mapAttachments(root.getConfig().Id, foldResult));
        }
        return returnValue;
    }

    public static List<Attachment> mapAttachments
        ( Id configId
        , Map <String, Map<String, Map<String, String>>> foldResult )
    {
        List<Attachment> returnValue = new List<Attachment>();
        for (String definitionName : foldResult.keySet())
        {
            returnValue.add
                ( new Attachment
                    ( Name = definitionName
                    , ParentId = configId
                    , Body = Blob.valueOf
                        ( JSON.serialize(foldResult.get(definitionName)) )));
        }
        return returnValue;
    }
}