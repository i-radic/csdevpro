@isTest
private class CS_UsageProfileTriggerDelegateTest{
    @testSetup static void setupData(){
        
    }
    
    @isTest static void testInsertAndUpdateAndDeleteUsageProfile() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account acc = CS_TestDataFactory.generateAccount(true, 'test account');
        Usage_Profile__c usageProfile = CS_TestDataFactory.generateUsageProfile(true, acc);
        system.assert(usageProfile.ID != null);
        usageProfile.Expected_Voice__c = 20;
        update usageProfile;
        system.assert(usageProfile.Expected_Voice__c == 20);
        delete usageProfile;
        List<Usage_Profile__c> uprofiles = [SELECT ID FROM Usage_Profile__c];
        system.assert(uprofiles.size() == 0);
    }
}