@isTest
private class CS_AppDirectGroupCustomLookup_Test {

    @isTest
    private static void getRequiredAttributes_Test() {
        String result;
        Test.startTest();
            CS_AppDirectGroupCustomLookup appDirectProductLookup = new CS_AppDirectGroupCustomLookup();
            result = appDirectProductLookup.getRequiredAttributes();
        Test.stopTest();
        System.assertEquals('["BrandId","Brand"]', result);
    }
    
     @isTest
    private static void doLookupSearch_Test() {
        Object[] result;
        cspmb__Price_Item__c appDirectBrand = CS_TestDataFactory.generatePriceItem(TRUE, 'Test App Direct Brand');
        cspmb__Add_On_Price_Item__c appDirectProduct = CS_TestDataFactory.generateAddOnPriceItem(FALSE, 'Test App Direct Product');
        appDirectProduct.cspmb__Is_Active__c = TRUE;
        INSERT appDirectProduct;
        cspmb__Price_Item_Add_On_Price_Item_Association__c priceItemAddOnAssoc = CS_TestDataFactory.generatePriceItem(FALSE, appDirectProduct, appDirectBrand);
        priceItemAddOnAssoc.Module__c = 'Test App Direct Brand';
        priceItemAddOnAssoc.cspmb__Group__c = 'Microsoft 365 | Business';
        INSERT priceItemAddOnAssoc;
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('BrandId', appDirectBrand.Id);
        searchFields.put('Brand', appDirectBrand.Name);
        Test.startTest();
            CS_AppDirectGroupCustomLookup appDirectProductLookup = new CS_AppDirectGroupCustomLookup();
            result = appDirectProductLookup.doLookupSearch(searchFields, '', NULL, 10, 10);
        Test.stopTest();
        System.assertEquals(1, result.size());
    }
}