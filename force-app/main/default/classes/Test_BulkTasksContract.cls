@isTest

private class Test_BulkTasksContract{

    static testMethod void runPositiveTestCasesCorp() {
        Test_Factory.SetProperty('IsTest', 'yes');

        Account dummyAccount = Test_Factory.CreateAccountForDummy();
        insert dummyAccount;
        
        createBulkTasksContract myCT = new createBulkTasksContract(); 
        
        //Test a Corp (LOB Level Task)
        Integer term = 2;
        Integer monthsAgo = -4;
        Date startDate = Date.today().addMonths(monthsAgo);
        
        Account testAcc = new Account();
        testAcc.name = 'Contract';
        testAcc.sac_code__c = 'CorpSac';
        testAcc.Sector_code__c= 'CORPsec';
        testAcc.LOB_Code__c = 'CorpLob';
        Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc});
        system.debug('ADJ Account' + accResult);
        system.debug('ADJ Account LOB' +  testAcc.LOB_Code__c);
                
        //Contract testContract = Test_Factory.CreateContract(accResult[0].getId());
        Contract testContract = new Contract();
        testContract.AccountId = accResult[0].id;
        testContract.Name = 'TESTContract';
        testContract.sac_code__c = testAcc.sac_code__c;
        testContract.name__c = testAcc.sac_code__c;
        testContract.startDate=startDate;
        testContract.Contract_Term_years__c=term;
        Database.SaveResult[] ctrResult = Database.insert(new Contract [] {testContract});
        
        system.debug('ADJ Contract' + ctrResult);
        system.debug('ADJ Contract cont sac code ' + testContract.sac_code__c);
        system.debug('ADJ Contract account sac code ' + testAcc.sac_code__c);
        

        List<Contract> TestContract1 = new List<Contract>{new Contract(id=ctrResult[0].getId())};
        
        system.debug('ADJ contract' + TestContract1);

        //add test user as team member LOB Task
        AccountTeamMember  ATM1 = Test_Factory.CreateAccountTeamMember();
        ATM1.accountid = accResult[0].getid();
        ATM1.TeamMemberRole = 'TestScript';
        insert ATM1;
        system.debug('ADJ ATM' + ATM1);

        //create tasks for applicable to account & team member
        Bulk_Tasks__c  Desk1 = Test_Factory.CreateBulkTasks();
        Desk1.description__c = 'D1test';
        Desk1.task_unique__c = 'D1test123';
        Desk1.Task_id__c = 'D1test1';
        Desk1.Minimum_Lead_Time__c = 10;
        Desk1.Reminder_Days__c = 1;
        Desk1.Minimum_Repeat_Time__c = 20;
        Desk1.USER_Type__c = 'TestScript';
        Desk1.Task_Category__c = 'Contract';
        Desk1.LOB_Code__c = 'CorpLob';
        Desk1.Active_Task_Switch__c = true;
        Desk1.Task_Id__c = 'D1test123';
        insert Desk1;
        
        Bulk_Tasks__c  Desk2 = Test_Factory.CreateBulkTasks();
        Desk2.description__c = 'D2test';
        Desk2.task_unique__c = 'D2test123';
        Desk2.Task_id__c = 'D2test1';
        Desk2.Minimum_Lead_Time__c = 10;
        Desk2.Reminder_Days__c = 1;
        Desk2.USER_Type__c = 'TestScript';
        Desk2.Task_Category__c = 'Contract';
        Desk2.LOB_Code__c = 'CorpLob';
        Desk2.Active_Task_Switch__c = true;
        Desk2.Task_Id__c = 'D2test123';
        insert Desk2;  

 system.debug('ADJ Desk Tasks1' + Desk1);
 system.debug('ADJ Desk Tasks2' + Desk2);

        myCT.contract = TestContract1[0];
        myCT.createContractTasks();
        myCT.createContractTasks();  

        // TEST UVS Tasks
        Bulk_Tasks__c  UVS1 = Test_Factory.CreateBulkTasks();
        UVS1.description__c = 'UVS1test';
        UVS1.task_unique__c = 'UVS1test123';
        UVS1.Task_id__c = 'UVS1test1';
        UVS1.USER_Type__c = 'TestScript';
        UVS1.Task_Category__c = 'UVS Defence Portal';
        UVS1.Sector_Code__c = 'CORP';
        UVS1.Quarter__c = 2;
        UVS1.LOB_Code__c = 'CorpLob';
        UVS1.Contract_Length__c = term;
        UVS1.Mandatory_Activity__c = False;
        UVS1.Active_Task_Switch__c = true;
        insert UVS1;

        Bulk_Tasks__c  UVS2 = Test_Factory.CreateBulkTasks();
        UVS2.description__c = 'UVS2test';
        UVS2.task_unique__c = 'UVS2test123';
        UVS2.Task_id__c = 'UVS2test1';
        UVS2.USER_Type__c = 'TestScript';
        UVS2.Task_Category__c = 'UVS Defence Portal';
        UVS2.Sector_Code__c = 'CORP';
        UVS2.Quarter__c =1; 
        UVS2.LOB_Code__c = 'CorpLob';
        UVS2.Contract_Length__c = term;
        UVS2.Mandatory_Activity__c = TRUE; 
        UVS2.Active_Task_Switch__c = true;
        insert UVS2;   

        myCT.contract = TestContract1[0];
        myCT.createContractTasks();
        test.starttest();
        myCT.createContractTasks();    
        test.stoptest();
    }


    static testMethod void runPositiveTestCasesBTLB() {
        Test_Factory.SetProperty('IsTest', 'yes');

        Account dummyAccount = Test_Factory.CreateAccountForDummy();
        insert dummyAccount;
        
        createBulkTasksContract myCT = new createBulkTasksContract(); 
        
        Integer term = 2;
        Integer monthsAgo=4;
        Date startDate = Date.today().addMonths(monthsAgo);

        //Test a BTLB (Sector Level Task)
        Account testAcc2 = Test_Factory.CreateAccount();
        testAcc2.name = 'TESTCODE';
        testAcc2.sac_code__c='testSAC2';
        testAcc2.le_code__c = null;
        testAcc2.Sector_code__c='BTLB';
        testAcc2.LOB_Code__c = 'BTLB';
        Database.SaveResult[] accResult2 = Database.insert(new Account[] {testAcc2});


        Contract testContract22 = Test_Factory.CreateContract(accResult2[0].getId());
        testContract22.Name = 'TESTCODE';
        testContract22.sac_code__c= testAcc2.sac_code__c;
        testContract22.name__c= testAcc2.sac_code__c;
        testContract22.startDate=startDate;
        Database.SaveResult[] ctrResult2 = Database.insert(new Contract [] {testContract22});

        List<Contract> TestContract2 = new List<Contract>{new Contract(id=ctrResult2[0].getId())};

        //add test user as team member LOB Task
        AccountTeamMember  ATM2 = new AccountTeamMember();
        ATM2.AccountId = accResult2[0].getId();
        ATM2.userId = Userinfo.getUserId();
        ATM2.TeamMemberRole = 'TestScript';
        insert ATM2;
        
        //create tasks for applicable to account & team member
        Bulk_Tasks__c  Btlb1 = Test_Factory.CreateBulkTasks();
        Btlb1.description__c = 'Btest';
        Btlb1.task_unique__c = 'Btest123';
        Btlb1.Task_id__c = 'Btest1';
        Btlb1.Minimum_Lead_Time__c = 10;
        Btlb1.Reminder_Days__c = 1;
        Btlb1.Minimum_Repeat_Time__c = 20;
        Btlb1.USER_Type__c = 'TestScript';
        Btlb1.Task_Category__c = 'Contract';
        Btlb1.Sector_Code__c = testAcc2.Sector_code__c;
        //Btlb1.Active_Task_Switch__c = true;
        Btlb1.LOB_Code__c = 'BTLB';
        insert Btlb1;
        
        Bulk_Tasks__c  Btlb2 = Test_Factory.CreateBulkTasks();
        Btlb2.description__c = 'B2test';
        Btlb2.task_unique__c = 'B2test123';
        Btlb2.Task_id__c = 'B2test1';
        Btlb2.Minimum_Lead_Time__c = 10;
        Btlb2.Reminder_Days__c = 1;
        Btlb2.USER_Type__c = 'TestScript';
        Btlb2.Task_Category__c = 'Contract';
        Btlb2.Sector_Code__c = testAcc2.Sector_code__c;
        //Btlb2.Active_Task_Switch__c = true;
        Btlb2.LOB_Code__c = 'BTLB';
        insert Btlb2; 
        
        myCT.contract = TestContract2[0];
        myCT.createContractTasks();     
        myCT.createContractTasks();      
    }
}