global class CS_MDSXMLGeneratorService {

    webservice static String generateXML (Id oppId) {
        String result = '';
        try {
            CS_MDSXMLGenerator xmlGenerator = new CS_MDSXMLGenerator(oppId, getOrderIdsFromOppId(oppId));
            xmlGenerator.createData();
            //Id enqueueJobId = System.enqueueJob(new CS_MDSXMLGenerator(orderIds, new LIst<CS_SolutionDownstreamDataBase>{new CS_MDSXMLGenerator()}));
            result = 'OK';
        } catch (Exception e) {
            result = 'Exception while generating XML : ' + e.getMessage();
            System.debug('Exception while generating XML : \n Message : ' + e.getMessage() + '\n Cause : ' + e.getCause() + '\n Line Number : ' + e.getLineNumber() + '\n Stack Trace : ' + e.getStackTraceString() + '\n Type : ' + e.getTypeName());
        }
        return result;
    }
    
    private static List<Id> getOrderIdsFromOppId (Id oppId) {
        List<Id> orderIds = new List<Id>();
        Map<Id, csord__Order__c> ordersMap = new Map<Id, csord__Order__c>([
                SELECT
                    Id,
                    Name,
                    csordtelcoa__Opportunity__c
                FROM csord__Order__c
                WHERE csordtelcoa__Opportunity__c = :oppId
        ]);
        orderIds.addAll(ordersMap.KeySet());
        return orderIds;
    }
}