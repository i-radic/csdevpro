/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class Test_VordelAdapterService {

    static testMethod void accountInfoCallOut() {
        SalesforceCRMService.CRMAccount account = new SalesforceCRMService.CRMAccount();
        account.CUG = '0123456';
        account.SAC = '';
        account.PrimaryContactid = 'contact.id';
        account.CustomerName = '';
        account.IsResidential = false;
        account.Type_x = 'type';
        account.CustomerClass = 'cust';
        account.IsBTBLegacy = false;
        account.OrganizationName = 'test';
        account.strategicJourney='N'; 
        //account.isLegacy = 'N';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator3());
        account=new VordelAdapterService.GetCustomer().doWebRequest(account.CUG);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        String CUGResponse= new VordelAdapterService.GetCustomer().doWebRequestForCUG('01234567890');
        
        VordelAdapterService.logApiInfo();
    }    
    static testMethod void contactInfoCallOut() {  
       SalesforceCRMService.CRMContact contact=new SalesforceCRMService.CRMContact();
        contact=createContact();
        /*contact = new SalesforceCRMService.CRMContact();
        contact.CUG = '0123456';
        contact.AccountManagerContact = '';        
        contact.ActiveFlag = 'Y';        
        contact.Comment = '';        
        contact.Type_x = '';        
        contact.ExternalId = '';        
        contact.Title = 'Mr';        
        contact.FirstName = 'FirstName';        
        contact.MiddleName = 'MiddleName';        
        contact.LastName = 'LastName';        
        contact.AliasName = 'AliasName';        
        contact.ContactSource = 'Salesforce';       
        contact.JobSeniority = '';        
        contact.Profession = '';        
        contact.DateOfBirth = DateTime.now();        
        contact.MotherMaidenName = '';        
        contact.Status = 'Active';        
        contact.JobTitle = '';        
        contact.KeyDecisionMaker = '';        
        contact.FaxNumber = '';        
        //contact.Organization = '';       
        contact.Department = '';        
        contact.RightToContact = '';        
        contact.PreferredContactChannel = '';        
        contact.SecondaryPreferredContactChannel = '';        
        contact.EmailAddress = 'test@crmtest.com';        
        contact.EmailAddressId = '';        
        contact.EmailConsentPermission = '';        
        contact.EmailConsentDate = DateTime.now();        
        contact.EmailConsentType = '';        
        contact.HomePhoneId = '';        
        contact.HomePhone = '';        
        contact.HomePhoneConsentPermission = '';        
        contact.HomePhoneConsentDate = DateTime.now();        
        contact.HomePhoneConsentType = '';        
        contact.MobilePhoneId = '';        
        contact.MobilePhone = '';        
        contact.MobilePhoneConsentPermission = '';        
        contact.MobilePhoneConsentDate = DateTime.now();        
        contact.MobilePhoneConsentType = '';        
        contact.FaxId = '';        
        contact.Fax = '';        
        contact.FaxConsentPermission = '';        
        contact.FaxConsentDate = DateTime.now();        
        contact.FaxConsentType = '';        
        contact.WorkPhoneId = '';        
        contact.WorkPhone = '';        
        contact.WorkPhoneConsentPermission = '';        
        contact.WorkPhoneConsentDate = DateTime.now();        
        contact.WorkPhoneConsentType = '';        
        contact.AddressId = '';        
        contact.AddressNumber = '';        
        contact.AddressStreet = '';        
        contact.AddressLocality = '';        
        contact.AddressPostCode = '';        
        contact.AddressPostTown = '';        
        contact.AddressSector = '';        
        contact.AddressSubBuilding = '';        
        contact.AddressZipCode = '';        
        contact.AddressConsentPermission = '';        
        contact.AddressConsentDate = DateTime.now();        
        contact.AddressTimeAt = '';        
        contact.AddressConsentType = '';
        contact.isLegacyResponse='N';*/
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SalesforceCRMService.CRMContact contact1=new VordelAdapterService.GetContact().doWebRequest(contact.CUG,'0031100000gEKji','N');
        VordelAdapterService.logApiInfo();
    }    
    static testMethod void addressListInfoCallOut(){
        SalesforceCRMService.CRMAddressList CRMAddList = new SalesforceCRMService.CRMAddressList();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        CRMAddList=new VordelAdapterService.GetNADAddress().doWebRequest('SW19 1AA','ROBT',false);
    }
    static SalesforceCRMService.CRMContact createContact()
    {
        SalesforceCRMService.CRMContact contact2=new SalesforceCRMService.CRMContact();
        contact2.CUG = '0123456';
        contact2.AccountManagerContact = '';        
        contact2.ActiveFlag = 'Y';        
        contact2.Comment = '';        
        contact2.Type_x = '';        
        contact2.ExternalId = '';        
        contact2.Title = 'Mr';        
        contact2.FirstName = 'FirstName';        
        contact2.MiddleName = 'MiddleName';        
        contact2.LastName = 'LastName';        
        contact2.AliasName = 'AliasName';        
        contact2.ContactSource = 'Salesforce';       
        contact2.JobSeniority = '';        
        contact2.Profession = '';        
        contact2.DateOfBirth = DateTime.now();        
        contact2.MotherMaidenName = '';        
        contact2.Status = 'Active';        
        contact2.JobTitle = '';        
        contact2.KeyDecisionMaker = '';        
        contact2.FaxNumber = '';        
        //contact.Organization = '';       
        contact2.Department = '';        
        contact2.RightToContact = '';        
        contact2.PreferredContactChannel = '';        
        contact2.SecondaryPreferredContactChannel = '';        
        contact2.EmailAddress = 'test@crmtest.com';        
        contact2.EmailAddressId = '';     
        contact2.EmailAddress1 = 'test@crmtest.com';        
        contact2.Email1IntegrationId= '';
        contact2.EmailConsent='';               
        contact2.EmailConsentPermission = '';        
        contact2.EmailConsentDate = DateTime.now();        
        contact2.EmailConsentType = '';        
        contact2.HomePhoneId = '';
        contact2.Voice1Id='';
        contact2.Voice2Id='';
        contact2.Voice3Id='';
        contact2.Voice1='';
        contact2.Voice2='';
        contact2.Voice3='';
        contact2.PostConsent='';
        contact2.EmailAddress2='test@crmtest.com';
        contact2.Email2IntegrationId='';
        contact2.Email3IntegrationId='';
        contact2.EmailAddress3='test@crmtest.com';
        contact2.HomePhone = '';        
        contact2.HomePhoneConsentPermission = '';        
        contact2.HomePhoneConsentDate = DateTime.now();        
        contact2.HomePhoneConsentType = '';        
        contact2.MobilePhoneId = '';        
        contact2.MobilePhone = '';        
        contact2.MobilePhoneConsentPermission = '';        
        contact2.MobilePhoneConsentDate = DateTime.now();        
        contact2.MobilePhoneConsentType = '';        
        contact2.FaxId = '';        
        contact2.Fax = '';        
        contact2.FaxConsentPermission = '';        
        contact2.FaxConsentDate = DateTime.now();        
        contact2.FaxConsentType = '';        
        contact2.WorkPhoneId = '';        
        contact2.WorkPhone = '';        
        contact2.WorkPhoneConsentPermission = '';        
        contact2.WorkPhoneConsentDate = DateTime.now();        
        contact2.WorkPhoneConsentType = '';        
        contact2.AddressId = '';        
        contact2.AddressNumber = '';        
        contact2.AddressStreet = '';        
        contact2.AddressLocality = '';        
        contact2.AddressPostCode = '';        
        contact2.AddressPostTown = '';        
        contact2.AddressSector = '';        
        contact2.AddressSubBuilding = '';        
        contact2.AddressZipCode = '';        
        contact2.AddressConsentPermission = '';        
        contact2.AddressConsentDate = DateTime.now();        
        contact2.AddressTimeAt = '';        
        contact2.AddressConsentType = '';
        contact2.isLegacyResponse='N';
        
        return contact2;
    }
    static testMethod void addressInfoCallOut(){
        SalesforceCRMService.CRMAddress address = new SalesforceCRMService.CRMAddress();
        address.IdentifierId = 'test';
        address.IdentifierName = '';
        address.IdentifierValue = '';
        address.Country = '';
        address.County = '';
        address.Name = '';
        address.POBox = '';
        address.BuildingNumber = '';
        address.Street = '';
        address.Locality = '';
        address.DoubleDependentLocality = '';
        address.PostCode = '';
        address.Town = '';
        address.SubBuilding = '';
        address.ExchangeGroupCode = '';
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator2());
        String responseaddress= new VordelAdapterService.CreateNADAddress().doWebRequest(address);
        SalesforceCRMService.CRMContact contact1=new SalesforceCRMService.CRMContact();
        contact1=createContact();
        system.debug('ccccccc'+contact1);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());    
        responseaddress = new VordelAdapterService.CreateAddress().doWebRequest(contact1,address,'N');   
    }
    static testMethod void validateContactCallOut(){    
        SalesforceCRMService.CRMContact contact2=new SalesforceCRMService.CRMContact();
        contact2=createContact();
        system.debug('ccccccc'+contact2);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());       
        contact2 = new VordelAdapterService.ValidateContact().doWebRequest(contact2);
    }
    static testMethod void ManageContactCallOut(){
        SalesforceCRMService.CRMContact contact3=new SalesforceCRMService.CRMContact();
        contact3=createContact();
        system.debug('ccccccc'+contact3);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        String resp=new VordelAdapterService.ManageContact().doWebRequest(contact3,'InsertContact');
    }
    static testMethod void eventInteractionCallOut(){
         SalesforceCRMService.BTLBEventInteraction btlbEvent = null;
        btlbEvent = new SalesforceCRMService.BTLBEventInteraction();
        btlbEvent.EventId = '';
        btlbEvent.CUG = '';
        btlbEvent.RecordId = '';
        btlbEvent.agentEIN = '';
        btlbEvent.interactionIntegrationId = '';
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        String resp=new VordelAdapterService.EventInteraction().doWebRequest(btlbEvent,'N');
    }
    static testMethod void getAddressCallOut()
    {
        SalesforceCRMService.CRMAddress crmAddr = null;
        crmAddr = new SalesforceCRMService.CRMAddress();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator4());   
        crmAddr=new VordelAdapterService.GetAddress().doWebRequest('1234567');
    }
    //mpa calls
    static testMethod void GetRadialDistanceCallOut()
    {
        SalesforceCRMService.CRMAddressList crmAddr = null;
        crmAddr = new SalesforceCRMService.CRMAddressList();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        crmAddr = new VordelAdapterService.GetRadialDistance().doWebRequest('BT41 3DG','549239.06','121461.53','1Gbit/s');
    }
    static testMethod void GetLineQltyByAddrCallOut()
    {
        SalesforceCRMService.CRMAddressList crmAddr = null;
        crmAddr = new SalesforceCRMService.CRMAddressList();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        crmAddr = new VordelAdapterService.GetLineQltyByAddr().doWebRequest('WS7 9HL','A00026023399 ','CM','GEA'); 
    }
    static testMethod void GetECCCallOut()
    {
        SalesforceCRMService.CRMAddressList crmAddr = null;
        crmAddr = new SalesforceCRMService.CRMAddressList();
        
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());   
        crmAddr = new VordelAdapterService.GetECC().doWebRequest('CM','A00026023399'); 
    }
}