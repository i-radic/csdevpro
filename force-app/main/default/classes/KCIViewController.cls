public with sharing class KCIViewController{

Id kciId,KCIRTId,UserId,ProfileId;
public String RTName,EIN,ProfileName;
KCI__c kci; 
public boolean v=false;

    public KCIViewController(ApexPages.StandardController controller) {
        kci = (KCI__c)controller.getRecord(); 
        kciId=Apexpages.currentPage().getParameters().get('id');
    }
    
    public PageReference PopUp(){
       v=true;
       return null;
    }
    
    public boolean getAlert(){
        return v;
    }

    
    public string getRecordTypeName(){
        RTName = [Select DeveloperName from RecordType where Id=:kci.RecordTypeId].DeveloperName;
        if(RTName=='Confirmation_call')
        {
            if(kci.Converted__c=='Yes')
                return 'BOTH';
            else
                return RTName;
        }
        else //if(RTName=='Validation_Call')
        {    
            return RTName;
        }
    }
    
    public PageReference EditPage(){       
        KCIRTId = [Select RecordTypeId from KCI__c where Id=:kci.Id].RecordTypeId;        
        RTName = [Select DeveloperName from RecordType where Id=:KCIRTId].DeveloperName; 
        
        if(RTName =='Validation_Call'){  
                  
            PageReference RD = new PageReference('/apex/KCIEditPage_Validation?id='+kci.Id+'&retURL='+kci.Id+'&RecordType='+kci.RecordTypeId+'&nooverride=1');
            RD.setRedirect(true);
            return RD;
        }
        else if(RTName =='Confirmation_Call'){                    
            PageReference RD = new PageReference('/apex/KCIEditPage_Confirmation?id='+kci.Id+'&retURL='+kci.Id+'&RecordType='+kci.RecordTypeId+'&nooverride=1');
            RD.setRedirect(true);
            return RD;
        }
    return null;
    }
    
    public PageReference EditValidationDetails(){
        PageReference ValidationPage = new PageReference('/apex/KCIEditPage_Validation?id='+kciId);
        ValidationPage.setRedirect(true);
        return ValidationPage;
    }
    
    //to get the RTname to display/hide Edit Validation Call button in view page.
    //to be displayed only in confirmation page and only for kci validation team(3 users) and System Administrator
    public boolean getDisplayEditValidationButton(){
        RTName = [Select DeveloperName from RecordType where Id=:kci.RecordTypeId].DeveloperName;
        UserId = UserInfo.getUserId();
        
        List<User> u = [Select Id,ProfileId,EIN__c from User where Id=:UserId LIMIT 1];
        if(u.size()>0){
            EIN = u[0].EIN__c;
            ProfileName = [Select Name from Profile where Id=:u[0].ProfileId].Name;
        
        }
        //EINs - Sunil Solanki, Anumeha Singh, Earnest Simon
        if(RTName=='Confirmation_Call' && kci.Converted__c=='Yes' && (ProfileName=='System Administrator' || EIN=='606032467' || EIN=='604798723' || EIN=='606504544')){
            return true;
        }
        else return false;
    }
}