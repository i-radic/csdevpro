global with sharing class CustomButtonBTHoldingReport extends csbb.CustomButtonExt {
	public static final String errorMsg 				= '{"status":"error","title":"Error","text":"' + Label.BT_NoConfigurationsInBasket + '"}';
	public static final String errorMsgMultiplePCs 		= '{"status":"error","title":"Error","text":"' + Label.BT_MultiplePcInBasket + '"}';
	public static final String errorMsgPDNotResignType 	= '{"status":"error","title":"Error","text":"' + Label.BT_ProductDefinitionNotResignType + '"}';
	public static final String errorMsgNoAccountNumber	= '{"status":"error","title":"Error","text":"' + Label.BT_ResignAccountNotPresent + '"}';

	public String performAction (String basketId, String pcrIds) {
		String action 	= '';
		String[] pcrArray;
		
		if( String.isNotBlank( pcrIds ) ){
			pcrArray = (List<String>) JSON.deserialize( pcrIds, List<String>.class);

			if( pcrArray != null && pcrArray.size() == 1 ){
				if( [SELECT Id FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId].size() < 1 )
					action = errorMsg;
				else {	
				    system.debug('**pcrArray[0] '+pcrArray[0]);
					csbb__Product_Configuration_Request__c pcr = [SELECT Id, csbb__Product_Configuration__c, csbb__Product_Configuration__r.cscfga__Product_Definition__r.Custom_Resign_Process__c,csbb__Product_Configuration__r.Deal_Type__c FROM csbb__Product_Configuration_Request__c WHERE id = :pcrArray[0] ];
					if( pcr.csbb__Product_Configuration__r.cscfga__Product_Definition__r.Custom_Resign_Process__c== false || pcr.csbb__Product_Configuration__r.Deal_Type__c !='Resign'){
						system.debug('***errorMsgPDNotResignType'+errorMsgPDNotResignType);
						action = errorMsgPDNotResignType;
					}else{
						/*No account number present Business Rule START*/
						if([SELECT cscfga__Opportunity__r.Account_Name__c FROM cscfga__Product_Basket__c WHERE Id = :basketId].size() < 1)
							action = errorMsgNoAccountNumber;
						else
							action =  '{"status":"ok","redirectURL":"' + Label.BTSFOrganizationLink + '/apex/csbtcl_bt_custom_resign_screen?basketId=' + basketId + '&pcId=' + pcr.csbb__Product_Configuration__c + '"}';
						/*No account number present Business Rule END*/
					}
				}
			}
			else
				action = errorMsgMultiplePCs; 
		}		
		return action;
	}
}