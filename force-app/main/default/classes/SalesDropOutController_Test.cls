@isTest
public class SalesDropOutController_Test
  {
    static testMethod void runPositiveTestCases() {
         User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        
    Account acc = Test_Factory.CreateAccount();  
    acc.OwnerId = thisUser.Id; 
    acc.CUG__c='CugTest';
    Database.SaveResult[] acc1Result = Database.insert(new Account[] {acc});
                    
    Contact con = Test_Factory.CreateContact();
    con.AccountCUG__c = acc.Id;
    con.AccountId = acc.Id;
    con.SAC_Code__c = acc.SAC_Code__c;
    insert con;     
   
    Lead l = New Lead();
    l.Title = acc.Name;
    l.LastName = 'Landscape Lead: ' + acc.Name;
    l.Company = acc.Name;
    l.Account__c = con.AccountCUG__c;
    l.Contact__c = con.Id;
    l.Description = 'Test Landscape to Lead';      
    insert l;  
    
        Lead l1= [Select Id from Lead where id=: l.Id];
        String L1iD =l1.Id;
       
        SalesDropOutController controller = new SalesDropOutController();
        controller.sdoLeadId = L1iD;
        controller.getLead();
        
        }
    
    
    }