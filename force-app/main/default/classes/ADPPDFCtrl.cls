public class ADPPDFCtrl { 
    public ADPPDFCtrl (ApexPages.StandardSetController controller) {

    }
   
public ADPPDFCtrl (ApexPages.StandardController controller) {   
 }
ID APD_Id = ApexPages.currentPage().getParameters().get('id');
//List<ADP__c> AccountID1 = [ select customer__r.id from ADP__c where id = :APD_Id];
  
public List<ADP_Current_Service_Performance__c> getCSR() {           
    return [SELECT id,  Performance__c, Issues__c,Area__c,Status__c,Owner__c, Owner__r.Name, Target_Completion_Date__c, Outcome__c 
    FROM ADP_Current_Service_Performance__c
    WHERE ADP__c = :APD_Id];
    }

public List<ADP_Relationship_development_plan__c> getRDP() {             
    return [SELECT id, Customer_Contact__c, Role_title__c, Existing__c, Relationship_objective__c, Relationship_actions__c, Owner__c, Timescale__c, Target_Date__c, Outcome__c, BTs_Status__c, Buying_Status__c, Budget__c
    FROM ADP_Relationship_development_plan__c
    WHERE ADP__c = :APD_Id];
    }    

public List<ADP_MIDAS__c> getMIDAS() {             
    return [SELECT id, Customers_Drivers_and_Events__c, Customers_Implications_and_Responses__c, Customers_Desires_and_Needs__c,Functional_Area__c , BTs_Added_Value__c, BTs_Solution_and_Capability_Set__c
    FROM ADP_MIDAS__c
    WHERE ADP__c = :APD_Id];
    }  
     
public List<Attachment> getOrg() {             
    return [SELECT id FROM Attachment WHERE ParentId = :APD_Id AND Name = 'Capture.PNG' ORDER BY CreatedDate DESC LIMIT 1];
    }   

public List<ADP_BT_Account_Team__c> getAMName() {             
    return [SELECT User__r.Name, Short_Role__c FROM ADP_BT_Account_Team__c WHERE ADP__c = :APD_Id AND Role__c = 'Account Director/Manager' LIMIT 1];
    }   
    
public List<ADP_BT_Account_Team__c> getNames() {             
    return [SELECT User__r.Name, Short_Role__c FROM ADP_BT_Account_Team__c WHERE ADP__c = :APD_Id AND Role__c <> 'Account Director/Manager' LIMIT 10 ];
    }          


 }