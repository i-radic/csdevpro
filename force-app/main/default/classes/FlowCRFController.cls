public class FlowCRFController {

    public String OppId {set; get;}
    public String OppName{set; get;}
    public String accName{get; set;}
    public String proId {get; set;}
    public String accId {get; set;}
    public String conId {get; set;}
    public String conName {get; set;}
    public String conPhone {get; set;}
    public String conEmail {get; set;}
    public String leCode {get; set;}
    public String Trading {get; set;}
    public Flow.Interview.CRF_Main_Flow FlowCRF{ get; set; }
    List < Lines_to_Move__c > ltm;
    public String vfBBRecType { get; set; }
    public String dcid { get; set; }
    public opportunitycontactrole ocr {get; set;}
    public List<opportunitycontactrole> ocrList {get; set;}
    
    public FlowCRFController(ApexPages.StandardController controller) {
    
        ocr = new opportunitycontactrole();
        ocrList = new List<opportunitycontactrole>();
        
        OppId=Apexpages.currentPage().getParameters().get('00N200000098SzU');
        OppName=Apexpages.currentPage().getParameters().get('OppyName');
        List<Profile> pid= new List<Profile>();
        //proId =Apexpages.currentPage().getParameters().get('profid');
        system.debug('$$$$$'+proId);
        pid=[Select Id from Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        system.debug('sdfsdfsdfd'+pid);
        proId = pid[0].Id;
        accId=Apexpages.currentPage().getParameters().get('AccountId');
        Opportunity opp=[Select Account.Name,Customer_Contact__c,Customer_Contact__r.Name,Customer_Contact__r.Phone,
                         Customer_Contact__r.Email,Account.LE_Code__c,Account.Trading_As__c from Opportunity where Id=:OppId];
        ocrList =[Select ContactId , contact.name ,contact.Phone,contact.Email FROM opportunitycontactrole 
                  where OpportunityId=:OppId  and Isprimary=true];
        if(ocrList.size()>0 ){
            ocr =  ocrList[0];  
        } else{
            
        }      
        conId =ocr.ContactId ;
        conPhone =ocr.contact.Phone;
        conEmail =ocr.contact.Email;
        conName=ocr.contact.Name;
        accName=opp.Account.Name;
        system.debug('aaaaaaa'+OppId+''+OppName+'  '+accName+'   '+proId+'  '+accId +'   '+conId +'   '+leCode+'    '+conPhone +'    '+conEmail+'    '+ conName+'  '+proId);
        vfBBRecType = BTUtilities.getRecordTypeIdByName('Flow_CRF__c', 'Broadband');

    }
    
      public String getFlowCRFID() {
        if (FlowCRF== null)
            return OppId;
        else return FlowCRF.FlowCRFId;
      }
      
     
      public String getCRFs() {
        if (FlowCRF== null)
            return NULL;
        else 
        {    
            return FlowCRF.CRFs;
        }
      }
    
    public PageReference getRedirect() {
        
        PageReference p;  
        system.debug('ppppppppp'+getCRFs()); 
        if(getCRFs()=='AX Form')
        {                         
             p=new Pagereference('/apex/Redirect?oppId='+OppId+'&accId='+accId+'&category=AX');
             system.debug('Redirect String in AX Form'+p);       
        }
        else if(getCRFs()=='ISDN 30')
        {                        
            p=new Pagereference('/apex/ISDN_FlowCRF_Edit?type=new&opptyId='+OppId+'&opptyAccntId='+accId);
        }
         else if(getCRFs()=='Movers')
        {
            p=new Pagereference('/apex/Redirect?oppId='+OppId+'&accId='+accId+'&category=movers');           
        }
         else if(getCRFs()=='Movers with Existing BB')
        {
             p=new Pagereference('/apex/Redirect?oppId='+OppId+'&accId='+accId+'&category=moverswithBB');           
        }
        else
        {
             p=new Pagereference('/'+OppId);     
        }
        p.setRedirect(true); 
        return p;
    }

    public List < Lines_to_Move__c > getLinesToMoveList() {
        if ((FlowCRF!= null)) {
            ltm = [SELECT Id, name, Provide_crd__c, Cease_crd__c, Product_Type__c, CreatedDate FROM Lines_to_Move__c WHERE  Related_to_Flow_CRF__c = : FlowCRF.FlowCRFId ORDER BY CreatedDate Desc];
            return ltm;
        } else {
            return null;
        }

    }
    
    public pagereference deleteCon() {
        List < Lines_to_move__c > lstacc1 = [Select id, Name from Lines_to_Move__c where id = : dcid];
        delete lstacc1;
        return null;
    }


}