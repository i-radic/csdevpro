@IsTest
public class CS_Utl_ObjectTest  {
	@IsTest
	public static void testUtlObject() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Account');

		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', acc);
		opp = [SELECT Id, Name, TotalOpportunityQuantity, Next_Action_Date__c, NextStep, CloseDate, StageName, AccountID FROM Opportunity WHERE Id = :opp.Id];
		
		Test.startTest();
		Map<String, Object> untypedObj = CS_Utl_Object.getUntyped(opp);
		Test.stopTest();

		System.assertEquals(acc.Id, untypedObj.get('AccountId'));
	}

	@IsTest
	public static void testUtlObjectWrongObjectType() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Test.startTest();
		Map<String, Object> untypedObj = CS_Utl_Object.getUntyped('Test String');
		Test.stopTest();
		System.debug('sasa:: untypedObj: ' + untypedObj);
	}
}