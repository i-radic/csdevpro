@isTest
private class CS_Test_BasketViewerCtrl {
    private static Account testAcc;
    private static cscfga__Product_Basket__c testBasket;
    private static cscfga__Product_Configuration__c config;

    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        // create account
        testAcc = new Account();
		testAcc.Name = 'TestAccount';
		testAcc.NumberOfEmployees = 1;
		insert testAcc;

        // create opportunity
        Opportunity testOpp = new Opportunity();
		testOpp.Name = 'Online Order: ' + testAcc.Name + ':'+datetime.now();
		testOpp.AccountId = testAcc.Id;
		testOpp.CloseDate = System.today();
		testOpp.StageName = 'Closed Won';   //@TODO - This should be a configuration
		testOpp.TotalOpportunityQuantity = 0; // added this as validation rule was complaining it was missing
		insert testOpp;
		
		// create OLI_Sync__c
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        // create basket
        testBasket = new cscfga__Product_Basket__c();
		Datetime d = system.now();
		String strDatetime = d.format('yyyy-MM-dd HH:mm:ss');
		testBasket.Name = 'Test Order ' + strDatetime;
		testBasket.cscfga__Opportunity__c = testOpp.Id;
		insert testBasket;

		createProductConfigurations();
    }

    private static void createProductConfigurations() {
        String offerId = null;
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c(Name='Fixed Line', cscfga__Description__c = 'Test helper');
        insert prodDef;
        cscfga__Product_Definition__c prodDefNew = new cscfga__Product_Definition__c(Name='BB Test', cscfga__Description__c = 'Test helper');
        insert prodDefNew;

        config = new cscfga__Product_Configuration__c();
        config.cscfga__Product_Basket__c = testBasket.Id;
        config.cscfga__Product_Definition__c = prodDef.Id;
        if (offerId != null)
        	config.cscfga__Configuration_Offer__c = offerId;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.cscfga__Unit_Price__c = 10;
        config.cscfga__Quantity__c = 1;
        config.cscfga__Recurrence_Frequency__c = 12;
        insert config;
        
        cscfga__Attribute__c attr = new cscfga__Attribute__c();
        attr.Name = 'testKey';
        attr.cscfga__Value__c = 'testValue';
        attr.cscfga__Product_Configuration__c = config.Id;
        cscfga__Attribute__c attr2 = new cscfga__Attribute__c();
        attr2.Name = 'testKey2';
        attr2.cscfga__Value__c = 'testValue2';
        attr2.cscfga__Product_Configuration__c = config.Id;
        
        insert attr;
        insert attr2;
        

        cscfga__Product_Configuration__c subConfig = new cscfga__Product_Configuration__c();
        subConfig.cscfga__Product_Basket__c = testBasket.Id;
        subConfig.cscfga__Product_Definition__c = prodDefNew.Id;
        if (offerId != null)
        	subConfig.cscfga__Configuration_Offer__c = offerId;
        subConfig.cscfga__Configuration_Status__c = 'Valid';
        subConfig.cscfga__Unit_Price__c = 10;
        subConfig.cscfga__Quantity__c = 1;
        subConfig.cscfga__Recurrence_Frequency__c = 12;
        subConfig.cscfga__Root_Configuration__c = config.Id;
        insert subConfig;
    }

	private static testMethod void testBasketViewerCtrl() {
        CreateTestData();

        Test.StartTest();

        CS_BasketViewerCtrl ctrl = new CS_BasketViewerCtrl(new ApexPages.StandardController(testAcc));
        System.assert(ctrl.orderedDefinitions == null);

        Decimal nr = ctrl.currProductTotalRevenue;
        nr = ctrl.currProductGrossMargin;
        nr = ctrl.currProductTenure;
        nr = ctrl.currProductFCC;

        ctrl = new CS_BasketViewerCtrl(new ApexPages.StandardController(testBasket));
        System.assert(ctrl.orderedDefinitions.size() == 1);
        System.assert(ctrl.spaceList.size() == 1);

        ctrl.loadSubProducts();
        System.assert(ctrl.subProducts.size() == 1);

        ctrl.cancelSubProducts();
        System.assert(ctrl.subProducts == null);

        ctrl.delProduct();

        ctrl.currProductName = 'test';
        System.assert(ctrl.currProductName == 'test');
        CS_BasketViewerCtrl.Product testProduct = new CS_BasketViewerCtrl.Product(config);
        testProduct.hierachyLvl = 1;
        System.assert(testProduct.hierachyLvl == 1);
        testproduct.addSubProduct(null);
        System.assert(testProduct.subProduct == null);
        testProduct.productType = 'test';
        System.assert(testProduct.productType == 'test');
        System.assert(testProduct.CompareTo(testProduct) == 0);

        ctrl.getTables();

        Test.StopTest();
	}
    
    private static testmethod void test_init() {
        CreateTestData();

        Test.StartTest();

        CS_BasketViewerCtrl ctrl = new CS_BasketViewerCtrl(new ApexPages.StandardController(testBasket));
        ctrl.init();
        
        Test.stopTest();
    }
	
	private static testmethod void CS_Node_Test(){
	    // duplicate
	    // filtertree
	    CreateTestData();
	    Test.StartTest();
	    
	    
	    CS_Node node = new CS_Node(config);
	    List<CS_Node> lst = new List<CS_Node>();
	    lst.add(node);
	    CS_Node duplicate = node.duplicate();
	    
	    //System.assert(node.equals(duplicate));
	    
	    Map<String, Set<String>> defAttrs = new Map<String, Set<String>> {
          'testAttr'  => new Set<String>{ 'a', 'b'},
          'testAttr2'  => new Set<String>{ 'c', 'd'}
        };
        
        Map<String, String> attrMap2 = new Map<String, String>{ 'testKey' => 'testValue', 'testKey2' => 'testValue2'};
        Map<String, Map<String, String>> attrMap = new Map<String, Map<String, String>>{ config.Id => attrMap2 };
        
	    
	    CS_Node.FilterConfigId fltrCfg = new CS_Node.FilterConfigId(config.Id);
	    Set<String> nms = new Set<String>{'testKey', 'testKey2'};
	    CS_Node.FilterDefinitionNames fltrDefNames = new CS_Node.FilterDefinitionNames(nms);
	    CS_Node.FilterOr f_or = new CS_Node.FilterOr(fltrCfg, fltrDefNames);
	    CS_Node.FilterAnd f_and = new CS_Node.FilterAnd(fltrCfg, fltrDefNames);
	    CS_Node.FilterNot f_not = new CS_Node.FilterNot(fltrCfg);

	    List<CS_Node> filtered = CS_Node.filterTree(lst, f_or);
	    List<CS_Node> filtered2 = CS_Node.filterTree(lst, f_not);
	    
	    System.assertNotEquals(filtered2, filtered);
	    
	    CS_Node.FoldAttributes foldAttr = new CS_Node.FoldAttributes(defAttrs, attrMap);
	    Map<String, Map<String, Map<String, String>>> foldMap = new Map<String, Map<String, Map<String, String>>>{ config.cscfga__Product_Definition__r.Name => attrMap };
	    foldAttr.fold(foldMap, node);
	    
	    Test.StopTest();
	    
	}

}