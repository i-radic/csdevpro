public with sharing class LeadLocatorAccountDetail {

private Final Account acct;
List<Lead_Locator__c> LCList=new List<Lead_Locator__c>();
List<Lead_Locator__c> LCListOne=new List<Lead_Locator__c>();
List<Lead_Locator__c> LCListTwo=new List<Lead_Locator__c>();
 
Id AccId;
Public Static Account SAccnt;

    public LeadLocatorAccountDetail (ApexPages.StandardController stdController) {
      
       this.acct = (Account)stdController.getRecord();
       AccId=acct.Id;
       If(acct!=NULL)
         SAccnt=acct;
      
   } 
       

    Public List<Lead_Locator__c> getLeadLocsOne(){
    
       
       If(acct!=NULL)
          AccId=acct.Id;
          
       Else 
          AccId=SAccnt.Id; 
         
       LCList.clear();
       LCListOne.clear();
       LCListTwo.clear();
       LCList=[Select Id,Account__c,Person_Role__c,User_Name__c,User__c From Lead_Locator__c where Account__c=:AccId Order By Person_Role__c];
       
       Integer j=LCList.size()/2;
       For(Integer i=0;i<LCList.size();i++){
            if(i<j)
            LCListOne.add(LCList[i]);
            if((i>=j)&&(i<=LCList.size()))
            LCListTwo.add(LCList[i]);
            }
            
      return LCListOne;
    }
    Public List<Lead_Locator__c> getLeadLocsTwo(){
       return LCListTwo;
    }  
  
         
   Public pageReference NewLeadLocator(){
    Account A=[Select Id,Name From Account where Id=:acct.Id];
    String AccName=A.Name;
    //00N20000002oWwU(CAT) && 00N20000002oWwU--> Account Lookup field
    //00N20000002oWwV(CAT) && 00N20000002oWwV-->Person Role field
    //a1o(CAT) && a23
    pageReference P=new pageReference('/a23/e?retURL='+AccId+'&saveURL='+AccId+'&CF00N20000002oWwU='+A.Name+'&CF00N20000002oWwU_lkid='+AccId+'&00N20000002oWwV=Service Relationship Manager'+'&nooverride=1');
    return p;
    
       }   
   
    Public pageReference EditLeadLocator(){
     Id Eid=ApexPages.currentPage().getParameters().get('LLEId');
     pageReference EditP=new pageReference('/'+Eid+'/e?retURL='+AccId+'&saveURL='+AccId);
     return EditP;
   }    
   
   Public pageReference DelLeadLocator(){
   Lead_Locator__c Did=[Select Id From Lead_Locator__c where id=:ApexPages.currentPage().getParameters().get('LLDId')];
   Delete Did;
   pageReference DelP=new pageReference('/'+AccId);
   return DelP;
   }    
   
   public Boolean getNButton(){
    List<Profile> ProfileLst=[Select Id From Profile Where Name In ('System Administrator','Corporate: Admin user','Custom:Standard User')];
    List<Lead_Locator__c> LLsize=[Select Id From Lead_Locator__c where Account__c=:AccId and Person_Role__c='Service Relationship Manager'];
    set<Id> ProName=new set<Id>();
    for(Profile P:ProfileLst)
     ProName.add(p.Id);
    If( ProName.contains(UserInfo.getProfileId()) && LLsize.size()==0)
         Return True;
      
     Else
     Return False; 
    }
    public Boolean getELink(){
    List<Profile> ProfileLst=[Select Id From Profile Where Name In ('System Administrator','Corporate: Admin user','Custom:Standard User')];
    set<Id> ProName=new set<Id>();
    for(Profile P:ProfileLst)
     ProName.add(p.Id);
    If( ProName.contains(UserInfo.getProfileId()) )
         Return True;
      
     Else
     Return False; 
    }
     
}