@IsTest(SeeAllData=true)

public class CS_CloudVoice_AddressCtrl_Test { 

	static Account account {get; set;}
	static Order_Address__c orderAddress {get; set;}
	static cscfga__Product_Basket__c prodBasket {get; set;}

	static void SetVariables(){
		account = Test_Factory.CreateAccount();     
        account.CUG__c = 'cugCV1';
        account.OwnerId = UserInfo.GetUserId() ;
        Database.SaveResult aR = Database.insert(account);

		orderAddress = new Order_Address__c(Account__c = account.id, 
											Building_Name__c = 'TEST', 
											Country__c = 'TEST', 
											County__c = 'TEST', 
											Locality__c = 'TEST', 
											NAD__c = 'TEST', 
											Number__c = 'TEST', 
											PO_Box__c = 'TEST', 
											Post_Code__c = 'TEST', 
											Post_Town__c = 'TEST', 
											Street__c = 'TEST', 
											Sub_Building__c = 'TEST');
		insert orderAddress;
	
		prodBasket = new cscfga__Product_Basket__c(csbb__Account__c = account.Id);
		insert prodBasket;
	}
	
	static testMethod void UnitTest() {
		SetVariables(); 
		PageReference page = Page.CS_CloudVoice_AddressSearch;
		Test.setCurrentPage(page);		
		ApexPages.currentPage().getParameters().put('basketId', prodBasket.id);
		ApexPages.StandardController sc = new ApexPages.StandardController(account);
		CS_CloudVoice_AddressCtrl ctrl = new CS_CloudVoice_AddressCtrl(sc);
		ctrl.getOrderAddressDetails();	
	}

	static testMethod void UnitTest2() {
		SalesforceCRMService.CRMAddress address = new SalesforceCRMService.CRMAddress();
        address.IdentifierId = 'test';
        address.IdentifierName = 'test';
        address.IdentifierValue = 'Test12';
        address.Country = 'test';
        address.County = 'test';
        address.Name = 'test';
        address.POBox = '242';
        address.BuildingNumber = '23';
        address.Street = 'test';
        address.Locality = 'test';
        address.DoubleDependentLocality = '';
        address.PostCode = 'WA15 7SU';
        address.Town = '';
        address.SubBuilding = '';
        address.ExchangeGroupCode = '';
        address.MainLinkDistance = '30';
        
        List<SalesforceCRMService.CRMAddress> addressList = new List<SalesforceCRMService.CRMAddress>();
        addressList.add(address);
        
		SalesforceCRMService.CRMArrayOfAddress CRMarray = new SalesforceCRMService.CRMArrayOfAddress();
        CRMarray.Address = addressList;
		
		SalesforceCRMService.CRMAddressList crmaddresslist = new SalesforceCRMService.CRMAddressList();
        crmaddresslist.responseStatus ='GREEN';
        CRMAddressList.Addresses = CRMarray;        
		
		SetVariables(); 
		PageReference page = Page.CS_CloudVoice_AddressSearch;
		Test.setCurrentPage(page);		
		ApexPages.currentPage().getParameters().put('basketId', prodBasket.id);
		ApexPages.StandardController sc = new ApexPages.StandardController(account);
		CS_CloudVoice_AddressCtrl ctrl = new CS_CloudVoice_AddressCtrl(sc);
		
		ctrl.crmAddressListWrapper = crmaddresslist;
		ctrl.postCode = 'GU19 5QT';
		ctrl.selectedAddressidentifier = 'Test12';		
		ctrl.getNADAddressDetails();		
		ctrl.addSelectedNADAddress();
		ctrl.addSelectedOrderAddress();
		ctrl.displayNADAddressSection();
	}
}