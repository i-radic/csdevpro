public with sharing class BPSCUGSeacrhCon {

    public BPSCUGSeacrhCon() {
        AccountTel = new Account();
        if(Apexpages.currentpage().getparameters().get('Tel') != null){
            AccountTel.phone = Apexpages.currentpage().getparameters().get('Tel');
        }
        Response  = '';       
    }

    
    public Account AccountTel{get;set;} 
    Public String CUGID{get;set;}
    public Boolean ShowAccount{get;set;}
    public static string Response{get;set;} 
    public string UserDivsion{get;set;} 
    public string UserDepartment{get;set;}
    public Account Acc = new Account(); 
      
    public static string test(string S){
        Response = S;
        return Response;
    } 
       
    public void RetriveCUG(){    
        List <Account>  A = new list <Account>();           
        A = [select id,name,Phone,CUG__c,Sector__c,Sub_Sector__c from Account where phone=:AccountTel.phone limit 1];               
        List<User> U = new List<User> ();
        U = [select id,Division,Department From User Where Id =: Userinfo.getUserId()];       
         if(U.size() > 0){
             UserDivsion = U[0].Division;
             UserDepartment = U[0].Department;
        }        
        if(A.size()<= 0){         
          
           if(Test.isRunningTest() == False){
                CUGID = SalesforceCRMService.GetCUGId(AccountTel.phone);
            }
                if(CUGID != null){            
                    RetriveCUGSeacrhCon R = new RetriveCUGSeacrhCon();       
                    Acc = R.SendCUG(CUGID);             
                    if(U.size() > 0){
                        AccountTel.Sector__c =  UserDivsion;
                        AccountTel.Sub_Sector__c = UserDepartment;
                    }else{
                        AccountTel.Sector__c = '';
                        AccountTel.Sub_Sector__c = '';
                    }                        
                    if(Acc !=null){    
                    AccountTel.Name=Acc.Name;
                    AccountTel.Phone=Acc.Phone;
                    AccountTel.AccountClonedId__c  = Acc.CUG__c.removeStart('CUG');
                    AccountTel.Original_SAC_Code__c = Acc.SAC_Code__c;
                    AccountTel.Original_LE_Code__c = Acc.LE_Code__c;
                    AccountTel.Original_Sector__c = Acc.Sector__c;
                    AccountTel.Original_Sub_Sector__c  = Acc.Sub_Sector__c;
                    ShowAccount = TRUE;   
                    }    
                    else{
                    AccountTel.Name = AccountTel.Name;
                    AccountTel.Phone=AccountTel.Phone;
                    AccountTel.AccountClonedId__c  = CUGID.removeStart('CUG');
                    ShowAccount = TRUE;
                    }  
        }        
        else if(CUGID == NULL ){        
        Response = 'No Account found for the telephone numbere entered - please check number or follow link to create new Account';     
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,+Response);
        ApexPages.addMessage(myMsg); 
        }
        }
        else if(A.size()>0){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'Account already exist with given Telephone Number  <b>-</b> <a href=/'+A[0].id+ ' style="font-size:13px;font-weight:bold;">  Account Name </a>');
        ApexPages.addMessage(myMsg);         
        }        
    } 
        
    public Pagereference SaveAccount(){
    
        AccountTel.CUG__c  = '';
        insert AccountTel; 
        
        List<Account> ADetails = [select id,CUG__c from Account where id =:AccountTel.id Limit 1 ]; 
        if(ADetails.size()>0){
        ADetails[0].CUG__c  = ADetails[0].id;
        Update ADetails[0] ;
        }     
                 
        Pagereference pg= new Pagereference('/'+AccountTel.Id);
        return pg;
    }
   
     
    public Pagereference CancelAccount(){
    
      Pagereference Pg = new Pagereference('/home/home.jsp');
      Pg.setRedirect(true);
      return Pg;
        
    }
}