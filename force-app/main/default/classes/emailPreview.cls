/*
Client      : BT Business
Author      : Krupakar Reddy J
Date        : 30/09/2013 (dd/mm/yyyy)
Description : This class is a result of custom functionality For BTLB and BS Emails.
*/

/*
Modification: As per CR5794 options for Warrington Users and Managers have been set same as for BS Classic Users. Author:Sridevi Bayyapuneedi. Date:26/08/2014
Modification : As per CR 5884, options for BTNI Standard Team profile have been added to emulate the same process as per BS Classic users. Author: Manyam Anusha date:26/09/2014
*/
public class emailPreview{

    public String BTLBmailValue { get; set; }
    public String getValue { get; set; }
    public string conId { get; set; }
    public Contact con;
    public boolean alert{get; set;}
    

    public emailPreview() {
        conId = ApexPages.currentPage().getParameters().get('id');
            system.debug('Conid' + conid+' Alert :::::::::'+ApexPages.currentPage().getParameters().get('alert')+'Substring');
        if(String.isNotBlank(conId)) {
            if(conid.length() == 26 ){
                
               // system.debug('KKKKKKKK '+conid.substring(22, 26));
                if(conid.substring(22, 26) == 'true' ){
                alert = TRUE;
                }Else{
                alert = FALSE;
                }
                conid = conid.substring(0, 15);
           
            }    
           
            con = [SELECT Id, Name, Email_Preview__c FROM Contact Where id=:conid];
        }
        String usrProfileName = [select Id, Name from Profile where id = :Userinfo.getProfileId()].Name;
        system.debug('User Profile Name : ' + usrProfileName);
        if(usrProfileName.contains('BTNI'))
        {
        getValue = 'BTNI';
        }else if(usrProfileName.contains('Warrington')){
            getValue = 'Warrington';
        }else if(usrProfileName.startsWith('BTLB') && !usrProfileName.contains('COT') && !usrProfileName.contains('Warrington')) {
            getValue = 'BTLB';
        }else {
            getValue = 'BS';
        }
     //   alert = true;
    }

    public PageReference reset() {
        PageReference pgReset = new  PageReference('/' + conId);
        pgReset.setRedirect(true);
        return pgReset;
    }


    public PageReference test() {
        return null;
    }

    public List<SelectOption> getBtlbMailOptions() {
        List<SelectOption> options = new List<SelectOption>();
        if(getValue == 'BS' || getValue == 'Warrington' ||getValue == 'BTNI') {
            options.add(new SelectOption('/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2a&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail','Packages'));
            options.add(new SelectOption('/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Z&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail','Calls & Lines'));
            options.add(new SelectOption('/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Y&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail','Broadband'));
            options.add(new SelectOption('/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IP&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail','Mobile'));
            options.add(new SelectOption('/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IO&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail','General products & services'));
        }
        else {
            options.add(new SelectOption('/p/email/template/EmailTemplateEditorUi/s?id=00X0J000002WlKU&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail','About us'));
            options.add(new SelectOption('/p/email/template/EmailTemplateEditorUi/s?id=00X200000014XwL&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail','Education'));
        }
        return options;
    }

    //Add Contact to Campaign
    public void campaignAdd() {

        try {
            String preview = '';
            if((getValue == 'BS' || getValue == 'Warrington' ||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2a&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
                 preview = 'Packages';
            } else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Z&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
                 preview = 'Calls';
            } else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Y&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
                 preview = 'Broadband';
            }else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IP&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
                 preview = 'Mobile';
            }else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IO&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
                 preview = 'General';
            }  else {
                preview = '';
            }
            system.debug('preview1 is'+preview);
            
            //Add Contact to Campaign
            if(!string.isBlank(preview) && (getValue == 'BS' || getValue == 'Warrington') ) {
                string strWhere = 'Second Chance - ' + preview;
                List <Campaign> c = [select id, name from Campaign where name = : strWhere limit 1];
                List <CampaignMember> cm = new list<CampaignMember>();
                if(!c.isEmpty()){
                    CampaignMember cmc = new CampaignMember();
                    cmc.campaignid = c[0].id;
                    cmc.ContactId = conId;
                    cmc.Status = 'Targeted';
                    cm.add(cmc);
                 }
                 
                  if(!cm.isEmpty()){
                    insert cm;
                }
                }
                 
             if(!string.isBlank(preview) && (getValue == 'BTNI') ) {
                string strWhere1 = 'BTNI Second Chance - ' + preview;
                List <Campaign> c1 = [select id, name from Campaign where name = : strWhere1 limit 1];
                system.debug('campaign is'+c1);
                List <CampaignMember> cm1 = new list<CampaignMember>();
                if(!c1.isEmpty()){
                    CampaignMember cmc1 = new CampaignMember();
                    cmc1.campaignid = c1[0].id;
                    cmc1.ContactId = conId;
                    cmc1.Status = 'Targeted';
                    cm1.add(cmc1);
                 }
              if(!cm1.isEmpty()){
                    insert cm1;
                }

            }
        }
        
        catch(DmlException e) {
            system.debug(e);
        }
    }

    public PageReference sendMail() {

        //Add Contact to Campaign
        campaignAdd();
         alert=True;
        //Build Standdard Email Template URL to send Email
        PageReference pg;
        if((getValue == 'BS' || getValue == 'Warrington' ||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2a&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
             
             pg = new PageReference('/email/author/emailauthor.jsp?retURL=/apex/emailPreview?id=' + conId + '?alert=' + alert + '&rtype=003&p2_lkid=' + conId + '&p26=noreply.btbusiness@bt.com:BT Business - No Reply&template_id=00X200000014q2a&p5=&save=1');
        } else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Z&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
             pg = new PageReference('/email/author/emailauthor.jsp?retURL=/apex/emailPreview?id=' + conId +'?alert=' + alert + '&rtype=003&p2_lkid=' + conId + '&p26=noreply.btbusiness@bt.com:BT Business - No Reply&template_id=00X200000014q2Z&p5=&save=1');
        } else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Y&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
             pg = new PageReference('/email/author/emailauthor.jsp?retURL=/apex/emailPreview?id=' + conId + '?alert=' + alert + '&rtype=003&p2_lkid=' + conId + '&p26=noreply.btbusiness@bt.com:BT Business - No Reply&template_id=00X200000014q2Y&p5=&save=1');
        } else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IP&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
             pg = new PageReference('/email/author/emailauthor.jsp?retURL=/apex/emailPreview?id=' + conId + '?alert=' + alert + '&rtype=003&p2_lkid=' + conId + '&p26=noreply.btbusiness@bt.com:BT Business - No Reply&template_id=00X20000001y2IP&p5=&save=1');
        } else if((getValue == 'BS' || getValue == 'Warrington'||getValue == 'BTNI') &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IO&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
             pg = new PageReference('/email/author/emailauthor.jsp?retURL=/apex/emailPreview?id=' + conId +'?alert=' + alert +  '&rtype=003&p2_lkid=' + conId + '&p26=noreply.btbusiness@bt.com:BT Business - No Reply&template_id=00X20000001y2IO&p5=&save=1');
        } else if(getValue == 'BTLB' &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X0J000002WlKU&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
            pg = new PageReference('/email/author/emailauthor.jsp?retURL=/apex/emailPreview?id=' + conId + '?alert=' + alert + '&rtype=003&p2_lkid=' + conId + '&p26=noreply.btbusiness@bt.com:BT Business - No Reply&template_id=00X0J000002WlKU&p5=&save=1');
        }
        else {
            pg = new PageReference('/email/author/emailauthor.jsp?retURL=/apex/emailPreview?id=' + conId + '?alert=' + alert + '&rtype=003&p2_lkid=' + conId + '&p26=noreply.btbusiness@bt.com:BT Business - No Reply&template_id=00X200000014XwL&p5=&save=1');
        }
      
        pg.setRedirect(false);
        return pg;
    
    }

    /*public void sendMail() {
        system.debug ( 'BTLBmailValue ' + BTLBmailValue );
        system.debug ( 'GetValue ' + getValue );
        String preview = '';
        if(getValue == 'BS' &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2a&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
            preview = 'Packages';
        } else if(getValue == 'BS' &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Z&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
            preview = 'Calls & Lines';
        } else if(getValue == 'BS' &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Y&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
            preview = 'Broadband';
        } else if(getValue == 'BTLB' &&  BTLBmailValue == '/p/email/template/EmailTemplateEditorUi/s?id=00X0J000002WlKU&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail') {
            preview = 'About us';
        }
        else {
            preview = 'Education';
        }
        system.debug ( 'Preview String ' + preview );
        if(preview != '') {
            con.Email_Preview__c = preview;
            update con;
        }
    }*/

}