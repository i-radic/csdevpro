@isTest(SeeAllData=true)
private class Test_LeaseDeskUtil {
    static testMethod void UnitTest() {
        RunTest('Yes', '01');
        RunTest('No', '02'); 
    }
 
    
    static void RunTest(String isTest, String uniqueVal){
        
        Test_Factory.SetProperty('IsTest', isTest);
        // create user
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'BTLB: Admin User']; 
        
        User u1 = new User();
        u1.Username = 'test9874' + uniqueVal + '@bt.com';
        u1.Ein__c = '9876543' + uniqueVal;
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.ProfileId = prof.Id;
        u1.TimeZoneSidKey = 'Europe/London'; 
        u1.LocaleSidKey = 'en_GB';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'en_US';
        u1.Apex_Trigger_Account__c = false; 
        u1.Run_Apex_Triggers__c = false;
        insert u1; 

        // create an account
        Account a1 = Test_Factory.CreateAccount();
        a1.Name = 'LD Account Test ' + isTest;
        a1.OwnerId = U1.Id;
        a1.Postcode__c = 'n16 9al';
        a1.BTLB_Common_Name__c = 'BTLB Bath and Bristol';
        a1.LOB__c = 'BTLB Bath and Bristol';
        a1.Sector__c = 'BT Local Business';
        a1.Sub_Sector__c = 'BTLB Bath and Bristol';
        a1.Sector_Code__c = 'BTLB';
        //a1.LeaseDeskID__c = 'TEST';
        insert a1;
        
        
        // create a standard opportunity
        Opportunity opp = Test_Factory.CreateOpportunity(a1.Id);
        opp.RecordTypeId = '01220000000PjDu';       // Standard record type
        opp.Type ='Sale';
        upsert opp; 
        
        LeaseDeskUtil.getLeaseDeskId(opp.Id);
        LeaseDeskUtil.isOutrightSale(opp.Id);
        LeaseDeskUtil.getOutrightSaleItems(opp.Id);
        LeaseDeskUtil.getTotalOutrightSaleCost(opp.Id);
        LeaseDeskUtil.getAccountDetails(a1.Id);
        LeaseDeskUtil.getOpportunityDetails(opp.Id);
        LeaseDeskUtil.getExistingFunders(a1.Id);
        LeaseDeskUtil.getFunders(a1.Id);
        LeaseDeskUtil.isBTLBAccount(a1.Id);
        LeaseDeskUtil.getLeasingHistoryList(a1.Id);
        LeaseDeskUtil.getLeasingHistory(a1.Id);
        LeaseDeskUtil.getUserDetails();
        LeaseDeskUtil.getDepartment();
        LeaseDeskUtil.isOutrightSaleCORP(opp.Id);
        LeaseDeskUtil.getTotalOutrightSaleCostCORP(opp.Id);
    }
}