/**
 *	This is a template for custom fixtures, the existense and name of the class are irrelevant.
 *	Class name can be anything, but by default if class MockData exists then Mock will load it, unless told otherwise
 *	via Mock.MOCK_DATA variable assignment
 *
 */

public with sharing class MockData extends MockDataStandard {
	private static Map<String, Map<String, Object>> DEFAULT_VALUES_BY_TYPE = new Map<String, Map<String, Object>>();

	public MockData() {
		//init standard Pricebook
		addSingletonObjectType('Pricebook2');
		//AI_Project_GetDocumentTemplates requires a record in CPM_Documentation_Settings__c
		//which references ContentWorkspace which can NOT be created via API
		TriggerUtils.setSkipReason('AI_Project_GetDocumentTemplates', 'Can not create ContentWorkspace in unit test');
	}

	public static Map<String, Object> OPPORTUNITY_VALUES = new Map<String, Object> {
		'Name' => 'Opp 000#{COUNTER}',
		'StageName' => 'Prospecting',
		'CloseDate' => System.today()//,
		//'Contract_Length_in_Months__c' => 12
	};

	public static Map<String, Object> PRICEBOOK2_VALUES = new Map<String, Object> {
		'Name' => 'Test PriceBook-#{COUNTER}',
		'IsActive' => true,
		'IsStandard' => true 
	};
	public static Map<String, Object> PRODUCT2_VALUES = new Map<String, Object> {
		'Name' => 'Test Product-#{COUNTER}',
		'IsActive' => true,
		'IsVAS__c' => true, // must be set so opportunity line items are triggered correctly
		'ProductCode' => 'code-#{COUNTER}'
	};
	public static Map<String, Object> PRICEBOOK_ENTRY_VALUES = new Map<String, Object> {
		'Name' => 'Test Product Entry-#{COUNTER}',
		'UnitPrice' => 1,
		'CurrencyIsoCode' => 'GBP',
		'IsActive' => true,
		Mock.DEPENDS_ON => new Map<String, String> {'Pricebook2Id' => 'Pricebook2', 'Product2Id' => 'Product2'}
	};
	public static Map<String, Object> SUPPORT_REQUEST_VALUES = new Map<String, Object> {
		'Deadline__c' => System.today() + 10,
		'Extension_Date__c' => System.today() + 20,
		'Landing_Date__c' => System.today(),

		'Status__c' => 'New',
		'Support_Required_From_Date__c' => System.today(),
		// GM : 13-06-04 : Added Bid_Contact_Type__c to pass validation rule
		'Bid_Contract_Type__c' => 'Formal Tender (ITT/RFP)',

		Mock.DEPENDS_ON => new Map<String, String> {'Opportunity__c' => 'Opportunity'}
	};

	public static Map<String, Object> PROJECT_VALUES = new Map<String, Object> {
		'Project_Name__c' => 'Test Proj-#{COUNTER}',
		'Stage__c' => 'Project',
		Mock.DEPENDS_ON => new Map<String, String> {'Opportunity__c' => 'Opportunity'}
	};


	static {
		//all fixture maps specified above need to be added here
		DEFAULT_VALUES_BY_TYPE.put('Opportunity', OPPORTUNITY_VALUES);
		DEFAULT_VALUES_BY_TYPE.put('Pricebook2', PRICEBOOK2_VALUES);
		DEFAULT_VALUES_BY_TYPE.put('Product2', PRODUCT2_VALUES);
		DEFAULT_VALUES_BY_TYPE.put('PricebookEntry', PRICEBOOK_ENTRY_VALUES);
		DEFAULT_VALUES_BY_TYPE.put('Support_Request__c', SUPPORT_REQUEST_VALUES);
		DEFAULT_VALUES_BY_TYPE.put('Project__c', PROJECT_VALUES);

	}

	public override Map<String, Object> getValueMap(final String objApiName, final Map<String, Object> fieldValues) {
		return MockData.DEFAULT_VALUES_BY_TYPE.get(objApiName);
	}

}