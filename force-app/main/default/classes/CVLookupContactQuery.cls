global class CVLookupContactQuery extends cscfga.ALookupSearch{
//implements csoe.OELookup{
//extends cscfga.ALookupSearch{
    global List<Object> getData(Map<String,Object> inputMap) {
        system.debug('In the CVLookupContactQuery');
        //String accName = (String) inputMap.get('accName');
        List<Contact> contacts = [
            select Name, Email, Phone, MobilePhone from Contact 
            //where name = :accName
        ];
        system.debug('contacts'+contacts);
        return contacts;
    }
}