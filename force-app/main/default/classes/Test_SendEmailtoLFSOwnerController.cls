@isTest(seeAllData=true)
private class Test_SendEmailtoLFSOwnerController {

  static testMethod void myUnitTest() {
  
  String p = [SELECT Id FROM Profile WHERE Name='System Administrator'].Id;
  User u1 = new User(); 
      u1.Username = 'jaovohoauh124@bt.com';
      u1.Ein__c = '987654321';
      u1.LastName = 'TestLastname';
      u1.FirstName = 'TestFirstname';
      u1.MobilePhone = '07918672032';
      u1.Phone = '02085878834';
      u1.Title='What i do';
      u1.OUC__c = 'DKW';
      u1.Manager_EIN__c = '123456789';
      u1.Email = 'no.reply@bt.com';
      u1.Alias = 'boatid01';
      u1.ProfileId=p;
      u1.TimeZoneSidKey='Europe/London';
        u1.LocaleSidKey='en_GB';
        u1.EmailEncodingKey='ISO-8859-1';
        u1.LanguageLocaleKey='en_US';
        u1.TimeZoneSidKey='Europe/London';
        u1.LocaleSidKey='en_GB';
        u1.EmailEncodingKey='ISO-8859-1';
        u1.LanguageLocaleKey='en_US';
      insert u1;
  
       //Create Account
       Account Account1 = Test_factory.createAccount();
       Account1.cug__c = 'test1Cug';
       Account1.OwnerId = u1.Id;
       Account1.Sub_Sector__c = 'Stratford Gold Customers';
       Account1.Sector__c = 'BT Local Business';
       Database.SaveResult[] Account1R = Database.insert(new Account[] {Account1});
          
       //Create Contact
       Contact contact1 = Test_Factory.CreateContact();
       contact1.AccountId=Account1.Id;
       contact1.Cug__c = 'test1Cug';
       contact1.Phone = '000111222333';
       contact1.Email = 'test.Alan@test.com';
       contact1.Contact_Post_Code__c = 'WR5 3RL';
           Database.SaveResult[] contact1R = Database.insert(new Contact[] {contact1});
   
       //Get recordtype 'Leads_from_Service' 
       List<RecordType> rt = [select Id,name from RecordType where DeveloperName ='Leads_from_Service' and SobjectType = 'Opportunity'];
       
       //Create LFS opportunity
       //Opportunity Oppy1 = Test_Factory.CreateOpportunity(Account1R[0].getId());
       Opportunity Oppy1= new Opportunity();
        Oppy1.AccountId = Account1.id;
        Oppy1.Name = 'tst_oppty';
        //Oppy1.NIBR_Year_2009_10__c = 1;
        //Oppy1.NIBR_Year_2010_11__c = 1;
        //Oppy1.NIBR_Year_2011_12__c = 1;
        Oppy1.StageName = 'Created';
        Oppy1.Sales_Stage_Detail__c = 'Appointment Made';
        Oppy1.CloseDate = system.today();
       Oppy1.RecordTypeId = rt[0].id;
       Oppy1.postcode__c = 'WR5 3RL';
       Oppy1.Customer_Contact_Number__c = '000111222333';
       oppy1.Email_Address__c = 'test.Alan@test.com';
       oppy1.Customer_Type__c = 'BT Business';
       oppy1.OwnerId=UserInfo.getUserId();
       oppy1.Product__c = 'pstn';
           Database.SaveResult[] Oppy1R = Database.insert(new Opportunity[] {Oppy1});
   
    
       List<Opportunity> opp = [select id, AccountId, Customer_Contact__c, Customer_Contact_Number__c, Email_Address__c from Opportunity where Id = :Oppy1R[0].id];
       
       //Create Note
       Note oppnote=new Note();
       oppnote.ParentId=opp[0].Id;
       oppnote.Title='Test Note';
       oppnote.IsPrivate=False;
       oppnote.Body='Test Note Body';
       oppnote.OwnerId=UserInfo.getUserId();   
           Database.SaveResult[] oppnote1R = Database.insert(new Note[] {oppnote});  
   
       //Create OpportunityTeamMember
       OpportunityTeamMember otm=new OpportunityTeamMember();
       otm.OpportunityId=opp[0].Id;
       otm.UserId=UserInfo.getUserId();
       otm.TeamMemberRole='Inbound Specialist';
           Database.SaveResult[] otm1R = Database.insert(new OpportunityTeamMember[] {otm});
      
        PageReference pageRef = Page.SendEmailtoLFSOwner;
        Test.setCurrentPage(pageRef);
        Apexpages.currentPage().getParameters().put('OppId',opp[0].Id);
        SendEmailtoLFSOwnerController ctr=new SendEmailtoLFSOwnerController();
        
        //Case 1: Send email to opportunity owner
        ctr.EmailList='Oppty Owner';    
        Boolean b1=ctr.getSalesTeamList();
        Boolean b2=ctr.getsendEmailButton();    
        List<SelectOption> lst=ctr.getEmailItems();
        SendEmailtoLFSOwnerController.categoryWrapper lstcw=new SendEmailtoLFSOwnerController.categoryWrapper(otm);
        lstcw.checked=False;
        ctr.incrementQ1();
        ctr.SendEmail(); 
        
        //Case 2: Send email to sales team member and ateleast one team member selected.     
        ctr.EmailList='Sales Team';
        lstcw.checked=True;
        List<SendEmailtoLFSOwnerController.categoryWrapper> cwl=new List<SendEmailtoLFSOwnerController.categoryWrapper>();
        cwl.add(lstcw);
        ctr.searchResults=cwl;
        ctr.getSelected();
        ctr.incrementQ1();
        ctr.SendEmail(); 
    
        ////Case 3: None selected.
        ctr.EmailList='None';
        ctr.incrementQ1();
    
        ////Case 2: Send email to sales team member and no team member selected and to test exception.
        ctr.EmailList='Sales Team';
        lstcw.checked=False;
        ctr.searchResults=cwl;
        ctr.getSelected();
        ctr.incrementQ1(); 
        ctr.SendEmail(); 
     }
}