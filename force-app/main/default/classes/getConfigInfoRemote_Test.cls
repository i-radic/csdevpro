@isTest(SeeAllData=true)
private class getConfigInfoRemote_Test {

    private static testMethod void testSuccssfulFetch() {
        cscfga__Product_Configuration__c config = [select id,BTnet_Primary_Port_Speed__c from cscfga__Product_Configuration__c limit 1];
        Id configId= config.Id;
        String speed = config.BTnet_Primary_Port_Speed__c;
        
        Map<String,Object> inputMap = new Map<String,Object>();
        inputMap.put('configID',configId);
        Map<String, Object> returnMap = getConfigInfoRemote.getData(inputMap);
        system.debug(returnMap);
        system.assertEquals(speed, returnMap.get('speed'));
	}

}