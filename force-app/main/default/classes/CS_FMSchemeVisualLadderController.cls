public class CS_FMSchemeVisualLadderController {

    public List<FMSchemeLadderWrapper> voiceSchemeLadderData {get; set;}
    public List<FMSchemeLadderWrapper> dataSchemeLadderData {get; set;}
    public String teamVoicePlan;
    public String teamVoiceScheme;
    public String teamDataPlan;
    public String teamDataScheme;
    
    public CS_FMSchemeVisualLadderController() {
        voiceSchemeLadderData = new List<FMSchemeLadderWrapper>();
        
        teamVoicePlan = Apexpages.currentpage().getparameters().get('teamVoicePlan');
        teamVoiceScheme = Apexpages.currentpage().getparameters().get('teamVoiceScheme');
        teamDataPlan = Apexpages.currentpage().getparameters().get('teamDataPlan');
        teamDataScheme = Apexpages.currentpage().getparameters().get('teamDataScheme');
        
        for (cspmb__Price_Item__c voicePlan : [SELECT Id, Name, cspmb__Recurring_Charge__c, (SELECT Id, Name, cspmb__Recurring_Charge__c FROM cspmb__Variants__r WHERE Name != 'Standard' AND cspmb__Is_Active__c = TRUE AND cspmb__Product_Definition_Name__c = 'Service Plan' AND Grouping__c = 'Voice' AND Module__c = 'Scheme' ORDER BY Sequence__c) FROM cspmb__Price_Item__c WHERE cspmb__Is_Active__c = TRUE AND cspmb__Product_Definition_Name__c = 'Service Plan' AND Grouping__c = 'Voice' AND Module__c = 'Team' ORDER BY Sequence__c]) {
            FMSchemeLadderWrapper voiceSchemeData = new FMSchemeLadderWrapper();
            voiceSchemeData.name = voicePlan.Name;
            voiceSchemeData.price = voicePlan.cspmb__Recurring_Charge__c;
            for (cspmb__Price_Item__c voiceScheme : voicePlan.cspmb__Variants__r) {
                if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 1')) {
                    voiceSchemeData.sch1DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch1Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 2')) {
                    voiceSchemeData.sch2DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch2Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 3')) {
                    voiceSchemeData.sch3DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch3Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 4')) {
                    voiceSchemeData.sch4DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch4Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 5')) {
                    voiceSchemeData.sch5DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch5Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 6')) {
                    voiceSchemeData.sch6DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch6Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 7')) {
                    voiceSchemeData.sch7DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch7Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 8')) {
                    voiceSchemeData.sch8DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch8Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 9')) {
                    voiceSchemeData.sch9DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch9Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                } else if (String.isNotEmpty(voiceScheme.Name) && voiceScheme.Name.equalsIgnoreCase('Scheme 10')) {
                    voiceSchemeData.sch10DiscountedPrice = voicePlan.cspmb__Recurring_Charge__c + voiceScheme.cspmb__Recurring_Charge__c;
                    voiceSchemeData.sch10Selected = checkForSelectedVoicePlanAndScheme(voicePlan.Name, voiceScheme.Name);
                }
            }
            voiceSchemeLadderData.add(voiceSchemeData);
        }
        dataSchemeLadderData = new List<FMSchemeLadderWrapper>();
        List<cspmb__Price_Item__c> dataSchemes = [SELECT Id, Name, cspmb__Recurring_Charge__c FROM cspmb__Price_Item__c WHERE Name != 'Standard' AND cspmb__Is_Active__c = TRUE AND cspmb__Product_Definition_Name__c = 'Service Plan' AND Grouping__c = 'Data' AND Module__c = 'Scheme' ORDER BY Sequence__c];
        for (cspmb__Price_Item__c dataPlan : [SELECT Id, Name, cspmb__Recurring_Charge__c FROM cspmb__Price_Item__c WHERE cspmb__Is_Active__c = TRUE AND cspmb__Product_Definition_Name__c = 'Service Plan' AND Grouping__c = 'Data' AND Module__c = 'Team' ORDER BY Sequence__c]) {
            FMSchemeLadderWrapper dataSchemeData = new FMSchemeLadderWrapper();
            dataSchemeData.name = dataPlan.Name;
            dataSchemeData.price = dataPlan.cspmb__Recurring_Charge__c;
            for (cspmb__Price_Item__c dataScheme : dataSchemes) {
                if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 1')) {
                    dataSchemeData.sch1DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch1Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 2')) {
                    dataSchemeData.sch2DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch2Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 3')) {
                    dataSchemeData.sch3DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch3Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 4')) {
                    dataSchemeData.sch4DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch4Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 5')) {
                    dataSchemeData.sch5DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch5Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 6')) {
                    dataSchemeData.sch6DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch6Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 7')) {
                    dataSchemeData.sch7DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch7Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 8')) {
                    dataSchemeData.sch8DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch8Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 9')) {
                    dataSchemeData.sch9DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch9Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 10')) {
                    dataSchemeData.sch10DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch10Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                } else if (String.isNotEmpty(dataScheme.Name) && dataScheme.Name.equalsIgnoreCase('Scheme 11')) {
                    dataSchemeData.sch11DiscountedPrice = dataPlan.cspmb__Recurring_Charge__c + dataScheme.cspmb__Recurring_Charge__c;
                    dataSchemeData.sch11Selected = checkForSelectedDataPlanAndScheme(dataPlan.Name, dataScheme.Name);
                }
            }
            dataSchemeLadderData.add(dataSchemeData);
        }
    }
  
    public class FMSchemeLadderWrapper {
        public String name {get; set;}
        public Decimal price {get; set;}
        public Decimal sch1DiscountedPrice {get; set;}
        public Boolean sch1Selected {get; set;}
        public Decimal sch2DiscountedPrice {get; set;}
        public Boolean sch2Selected {get; set;}
        public Decimal sch3DiscountedPrice {get; set;}
        public Boolean sch3Selected {get; set;}
        public Decimal sch4DiscountedPrice {get; set;}
        public Boolean sch4Selected {get; set;}
        public Decimal sch5DiscountedPrice {get; set;}
        public Boolean sch5Selected {get; set;}
        public Decimal sch6DiscountedPrice {get; set;}
        public Boolean sch6Selected {get; set;}
        public Decimal sch7DiscountedPrice {get; set;}
        public Boolean sch7Selected {get; set;}
        public Decimal sch8DiscountedPrice {get; set;}
        public Boolean sch8Selected {get; set;}
        public Decimal sch9DiscountedPrice {get; set;}
        public Boolean sch9Selected {get; set;}
        public Decimal sch10DiscountedPrice {get; set;}
        public Boolean sch10Selected {get; set;}
        public Decimal sch11DiscountedPrice {get; set;}
        public Boolean sch11Selected {get; set;}
    }
    
    private Boolean checkForSelectedVoicePlanAndScheme (String planName, String schemeName) {
        Boolean result;
        if (planName.contains(teamVoicePlan) && schemeName.equalsIgnoreCase(teamVoiceScheme)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }
    private Boolean checkForSelectedDataPlanAndScheme (String planName, String schemeName) {
        Boolean result;
        if (planName.contains(teamDataPlan) && schemeName.equalsIgnoreCase(teamDataScheme)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }
}