/************************************************************************************************************************
 *          Name        :       Test_PhaseListApex
 *          Apex Class  :       PhaseListApex
 *          VF Page     :       PhaseListSLDS
 *          Description :       Added a VF Page related list to edit Phase records related to Project objects on the go.
 *          Author      :       BALAJI MS
 *          Date        :       15-11-2017
 *          Version     :       v1.0
 ***********************************************************************************************************************/
@IsTest
public class Test_PhaseListApex {
    static testmethod void PhaseListTestMethod(){
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }
        Account AccRecord = Test_Factory.CreateAccount();        
        Insert AccRecord;
        
        Opportunity OppRecord = Test_Factory.CreateOpportunity(AccRecord.Id);
        Insert OppRecord;
        
        Project__c ProjectRecord = Test_Factory.CreateProject(OppRecord.Id);
        ProjectRecord.Project_Name__c = 'Test';
        ProjectRecord.Name = 'Test';
        ProjectRecord.EE_Project_Number__c='123';
        Insert ProjectRecord;
        
        Phase__c PhaseRecord = Test_Factory.CreatePhase(ProjectRecord.Id);
        Insert PhaseRecord;
        
        ApexPages.StandardController controller = new Apexpages.Standardcontroller(ProjectRecord);
        PhaseListApex PhaseApexClass = new PhaseListApex(controller);
        
        PhaseRecord.Milestones__c = '1 DB & Literature';
        PhaseApexClass.Save();
        PhaseApexClass.Cancel();
        PhaseApexClass.getPhaseRecords();
    }
}