/********************************************************************************************************************************
Apex Class Name	: BTSportMVOCHelper
Test Class Name : Test_BTSportMVOC
Description	: Code to Calculate RollUp Summary
Version	: V0.1
Created By Author Name : RAJI VUBA
Date : 30/05/2018
*********************************************************************************************************************************/
public class BTSportMVOCHelper {
    
    public static void AfterInsertMethod(List<BT_Sport_SVOC__c> SVOCs){
        RollUpMethodToUpdateMVOC(SVOCs);
        
    }
    public static void AfterUpdateMethod(List<BT_Sport_SVOC__c> SVOCs){
        RollUpMethodToUpdateMVOC(SVOCs);
        
    }
    public static void AfterDeleteMethod(List<BT_Sport_SVOC__c> SVOCs){
        RollUpMethodToUpdateMVOC(SVOCs);
        
    }
    public static void AfterUnDeleteMethod(List<BT_Sport_SVOC__c> SVOCs){
        RollUpMethodToUpdateMVOC(SVOCs);
    }
    
    public static void RollUpMethodToUpdateMVOC(List<BT_Sport_SVOC__c> SVOCs){
        
        Set<Id> setofMVOCId = new Set<Id>();
        List<BT_Sport_MVOC__c> MVOCList = new List<BT_Sport_MVOC__c>();
        Map<Id, Decimal> SVOCMapAg = new Map<Id, Decimal>();
        Map<Id, Decimal> SVOCMap = new Map<Id, Decimal>();
        Map<Id, Decimal> SVOCFullPrice = new Map<Id, Decimal>();
        Map<Id, Decimal> SVOCMusicPrice = new Map<Id, Decimal>();
        Integer total = 0; 
        
        for(BT_Sport_SVOC__c SVOC : SVOCs){
            if(SVOC.BT_Sport_MVOC__c != Null)
                setofMVOCId.add(SVOC.BT_Sport_MVOC__c);
            system.debug('setofMVOCId	'+setofMVOCId);
        }
        system.debug('SVOCMap Size	'+setofMVOCId.size());
        if(setofMVOCId.size() > 0){  
            for(AggregateResult ag: [select Count(id) ActionSize, BT_Sport_MVOC__c MVOC From BT_Sport_SVOC__c where BT_Sport_MVOC__c IN:setofMVOCId Group by BT_Sport_MVOC__c]){
                Id MVOC = string.valueOf(ag.get('MVOC'));
                total = Integer.valueOf(ag.get('ActionSize'));  
                //system.debug('total	'+total);
                SVOCMapAg.put(MVOC, total);
            }
            for(BT_Sport_SVOC__c BTSVOC : [Select Id,Discounted_Price__c, Calculated_FULL_Price__c, Music_Price__c, Music_in_Venue_and_or_via_ext_Provider__c, BT_Sport_MVOC__c from BT_Sport_SVOC__c where BT_Sport_MVOC__c IN:setofMVOCId]){                
                Decimal PriceValue=0,FullPrice=0,MusicPrice=0;
                Id MVOCId = BTSVOC.BT_Sport_MVOC__c;
                
                
                if(BTSVOC.Discounted_Price__c != null){
                    PriceValue =  BTSVOC.Discounted_Price__c;
                }
                if(BTSVOC.Calculated_FULL_Price__c != null){
                    FullPrice =  BTSVOC.Calculated_FULL_Price__c;
                }
                if(BTSVOC.Music_Price__c != null && BTSVOC.Music_in_Venue_and_or_via_ext_Provider__c == 'Bar Beats'){
                    MusicPrice =  BTSVOC.Music_Price__c;
                }
                
                
                if(SVOCMap.containsKey(MVOCId)){                 
                    PriceValue += SVOCMap.get(MVOCId);                    
                }
                if(SVOCFullPrice.containsKey(MVOCId)){                                    
                    FullPrice += SVOCFullPrice.get(MVOCId);
                }
                if(SVOCMusicPrice.containsKey(MVOCId)){                                    
                    MusicPrice += SVOCMusicPrice.get(MVOCId);
                }                
                
                SVOCMap.put(MVOCId,PriceValue);//PriceAvg);
                SVOCFullPrice.put(MVOCId,FullPrice);
                SVOCMusicPrice.put(MVOCId,MusicPrice);
                system.debug('SVOCMap2' +SVOCMap);
            }
        }
        for(Id MVOCId : SVOCMapAg.KeySet()){
            BT_Sport_MVOC__c MVOC = new BT_Sport_MVOC__c();
            MVOC.Id = MVOCId;
            if(SVOCMap.containsKey(MVOCId)){
                MVOC.Contractual_Price__c = (SVOCMap.get(MVOCId) == 0 ? null : SVOCMap.get(MVOCId));
                MVOC.MSA_Monthly_Calculated_Price__c = (SVOCFullPrice.get(MVOCId) == 0 ? null : SVOCFullPrice.get(MVOCId));
                MVOC.Bar_Beats_Monthly_Price__c = (SVOCMusicPrice.get(MVOCId) == 0 ? null : SVOCMusicPrice.get(MVOCId)); 
                MVOC.Sites__c=String.valueOf(total);
                //system.debug('MVOC.Sites__c	'+MVOC.Sites__c);
            }
            MVOCList.add(MVOC);            
        }        
        if(MVOCList.size() > 0){
            try{
                Update MVOCList;
            }
            catch (Exception e){
                system.debug('expection	'+e.getMessage());
            }
        }
        
    }
}