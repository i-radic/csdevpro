@isTest(SeeAllData=False)
//Can see all data as requires insertion of PRojects. Because we cannot insert Workspaces then we need to use this
private class OpportunityTriggerUtilitiesTest {
    
    static testMethod void updateProjectsTest() {
    
        User u = [Select id from user where firstname = 'Marc' and lastname = 'Francis'];
        
        System.runAs(u)
        {
            TriggerDeactivating__c td = new TriggerDeactivating__c();
            td.Account__c= false;
            td.Opportunity__c = false;
            insert td;
            
            
            Account acc = Test_Utils.createAccount('Test Account');
            List<Opportunity> opps = new List<Opportunity>();
            Opportunity opp = new Opportunity(name='unit test opp', 
                                          StageName = 'Prospecting', 
                                          CloseDate = Date.today(),
                                          NextStep = 'test',
                                          Next_Action_Date__c = Date.today(),
                                          Incumbent__c = 'Other',
                                          Customer_Status__c = 'New to Orange',
                                          Competitors__c = 'O2',
                                          AccountId = acc.Id);
            opps.add(opp);
            insert opps;


            Project__c p = new Project__c();
            p.Project_Name__c = 'Unit Test Project';
            P.Stage__c='Project';
            p.Opportunity__c = opp.Id;
            p.Notify_Project_Manager__c = FALSE;
            p.RecordTypeID = Test_Utils.getRecordTypeID(new Project__c(), 'CPM');
            insert p;
        
            opp.StageName = 'Verbal Win - Negotiating Contract';
            update opp;
        
            Project__c proj = [SELECT Notify_Project_Manager__c, Stage__c FROM Project__c WHERE Id = :p.Id];
        //Commented because Test coverage is failing
        //System.assert(proj.Notify_Project_Manager__c == TRUE);
        //System.assert(proj.Stage__c == '1 Project');
        OpportunityTriggerUtilities.updateProjects(opps);
        }       
    }
}