@isTest
public class CS_RefreshAllConfigurationsInBasketTest {
	
	static {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
		Account a = new Account(
			Name = 'Test',
			NumberOfEmployees = 1
		);
		insert a;
		
		Opportunity o = new Opportunity(
			Name = 'Test',
			AccountId = a.Id,
			CloseDate = System.Today(),
			StageName = 'Changes Over Time',
			TotalOpportunityQuantity = 1
		);
		
		insert o;
		
		Usage_Profile__c up1 = new Usage_Profile__c(
			Active__c = true,
			Account__c = a.Id,
			Expected_SMS__c = 20,
			Expected_Voice__c = 10,
			Expected_Data__c = 20,
			Total_Number_of_Connections__c = 3,
			Data_Only_Connections__c = 2
		);
		
		insert up1;
		
		cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
			Name = 'Test',
			Default_Usage_Profile__c = up1.Id
		);
		insert pi1;
		
		cspmb__Rate_Card__c rc1 = new cspmb__Rate_Card__c(
			Name = 'Test',
			cspmb__Account__c = a.Id
		);
		insert rc1;
		
		cspmb__Price_Item_Rate_Card_Association__c assoc = new cspmb__Price_Item_Rate_Card_Association__c(
			cspmb__Rate_Card__c = rc1.Id,
			cspmb__Price_Item__c = pi1.Id
		);
		insert assoc;
		
		cspmb__Rate_Card_Line__c rlVoice = new cspmb__Rate_Card_Line__c(
			Name = 'Voice',
			cspmb__Rate_Card__c = rc1.Id,
			Calls_to_Orange_Charge__c = 10,
			Calls_to_Orange_Cost__c = 5
		);
		insert rlVoice;
		
		cspmb__Rate_Card_Line__c rlSMS = new cspmb__Rate_Card_Line__c(
			Name = 'SMS',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlSMS;
		
		cspmb__Rate_Card_Line__c rlData = new cspmb__Rate_Card_Line__c(
			Name = 'Data',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlData;
		
		cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c(
			Name = 'Mobile Voice',
			cscfga__Description__c = 'Pd1'
		);
		insert pd1;
		
		cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
			Name = 'Ad1',
			cscfga__Data_Type__c = 'String',
			cscfga__Type__c = 'Related Product',
			cscfga__Product_Definition__c = pd1.Id,
			cscfga__high_volume__c = true
		);
		insert ad1;
		
		cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
			Name = 'User Group',
			cscfga__Description__c = 'Pd2'
		);
		insert pd2;
		
		cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
			Name = 'Ad2',
			cscfga__Data_Type__c = 'Decimal',
			cscfga__Type__c = 'User Input',
			cscfga__Product_Definition__c = pd2.Id,
			cscfga__store_as_json__c = True,
			cscfga__Default_Value__c = '-0.28'
		);
		insert ad2;
		
		cscfga__Available_Product_Option__c apo = new cscfga__Available_Product_Option__c(
			cscfga__Attribute_Definition__c = ad1.Id,
			cscfga__Product_Definition__c = pd2.Id
		);
		insert apo;
		
		CS_Related_Products__c rp = new CS_Related_Products__c(
			Name = 'Pd1',
			Related_Products__c = 'Pd2'
		);
		insert rp;
		
		CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
			Name = 'Pd2',
			Attribute_Names__c = 'Ad2',
			List_View_Attribute_Names__c = 'Ad2',
			Total_Attribute_Names__c = 'Ad2'
		);
		insert rpa;
		
		cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
			Name = 'Test',
			cscfga__Opportunity__c = o.Id,
			cscfga__Opportunity__r = o
		);
		insert pb;
		
		cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
			Name = 'Root',
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Product_Definition__c = pd1.Id,
			Service_Plan__c = pi1.Id,
			Tenure__c = 24
		);
		insert pc1;
		
		cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
			Name = 'Child',
			cscfga__Root_Configuration__c = pc1.Id,
			cscfga__Parent_Configuration__c = pc1.Id,
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Product_Definition__c = pd2.Id,
			cscfga__Attribute_Name__c = 'Ad1',
			Tenure__c = 1
		);
		insert pc2;
		
		cscfga__Attribute__c at1 = new cscfga__Attribute__c(
			Name = 'Ad1',
			cscfga__Attribute_Definition__c = ad1.Id,
			cscfga__Value__c = '10.00',
			cscfga__Product_Configuration__c = pc1.Id
		);
		insert at1;
		
		cscfga__Attribute__c at2 = new cscfga__Attribute__c(
			Name = 'Shared Add On',
			cscfga__Attribute_Definition__c = ad2.Id,
			cscfga__Value__c = '15.00',
			cscfga__Product_Configuration__c = pc1.Id
		);
		insert at2;
		
		
	}

	private static String attNamesJSON = '["Monthly Recurring Charges","Monthly Cost","Unsecured Revenue","Total Charges","Total Cost","Null","Name for Document","Hardware Charges","Service Plan","Customiser Processed","Device Discount","Type","Related Product Ids","Profit Boost","Value to customer","Number of devices","Config Id","Single Roaming","Disable Roaming","Care Recurring Total","Care One Off Total","FCC","Parent Tenure","Allowed Care Discount","Care Monthly Charges","Care Monthly Costs","ReSign","ReSignFilter","Parent FCC","Device Name","Total Care One Off Cost","UG FCC","tmpFCC","SP Parent","Care Tenure Type","Care Discount","Care Name","Roaming Name","Care Recurring Unit Net","User Profile","Warranty One Off Unit Net","Warranty Recurring Unit Net","Insurance One Off Unit Net","Insurance Recurring Unit Net","Care Insurance Name","Allowed Insurance Discount","Allowed Warranty Discount","Care Warranty Name","Name","Number of users","Single Add On","Accessory","Tenure","Connection Type","Device Cost","Care Recurring Cost","Care Setup Cost","UGCosts","Device Manufacturer","Device Type","Active From","Device product code","Device Price","Gross Margin","Active To","Eligible for Care","Care Tenure","Roaming Tenure Type","Roaming Tenure","Roaming Type","Care Recurring Charge","Care Setup Charge","UGCharges","Airtime FCC","Hardware FCC","Total FCC","Hardware/Airtime","Maximum Hardware FCC","Warranty Discount","Insurance Discount","Insurance One Off Cost","Insurance Recurring Charge","Insurance Recurring Cost","Insurance Setup Charge","Device","Care","Roaming","Insurance","Warranty","IDD Voice Usage","IDD Voice Cost","Roaming Data Usage","Roaming Data Cost","Roaming Voice Usage","Roaming Voice Cost","OOB","OOB Cost","Roaming Recurring Total","Tot Rec Chg","Roaming Setup Total","Incoming Revenue","Voice","SMS","Data","Voice Usage","Voice Cost","SMS Usage","SMS Cost","Data Usage","Data Cost","SMS Roaming Usage","SMS Roaming Cost","tmpRoamingUsage","tmpRoamingCost","Total Non Roaming Cost","Roaming Monthly Cost","Roaming Setup Cost","Roaming Total Cost","Roaming Total Usage","Total Non roaming Usage","Roaming Monthly Charge","Roaming Setup Charge"]';
	static testMethod void testRefresh() {	
		Account a = new Account(
			Name = 'Test',
			NumberOfEmployees = 1
		);
		insert a;
		
		Opportunity o = new Opportunity(
			Name = 'Test',
			AccountId = a.Id,
			CloseDate = System.Today(),
			StageName = 'Closed Won',
			TotalOpportunityQuantity = 0
		);
		
		insert o;
		
		Usage_Profile__c up1 = new Usage_Profile__c(
			Active__c = true,
			Account__c = a.Id,
			Expected_SMS__c = 20,
			Expected_Voice__c = 10,
			Expected_Data__c = 20
		);
		
		insert up1;
		
		cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
			Name = 'Test',
			Default_Usage_Profile__c = up1.Id
		);
		insert pi1;
		
		cspmb__Rate_Card__c rc1 = new cspmb__Rate_Card__c(
			Name = 'Test'
		);
		insert rc1;
		
		cspmb__Rate_Card_Line__c rlVoice = new cspmb__Rate_Card_Line__c(
			Name = 'Voice',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlVoice;
		
		cspmb__Rate_Card_Line__c rlSMS = new cspmb__Rate_Card_Line__c(
			Name = 'SMS',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlSMS;
		
		cspmb__Rate_Card_Line__c rlData = new cspmb__Rate_Card_Line__c(
			Name = 'Data',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlData;
		
		cspmb__Price_Item_Rate_Card_Association__c assoc = new cspmb__Price_Item_Rate_Card_Association__c(
			cspmb__Rate_Card__c = rc1.Id,
			cspmb__Price_Item__c = pi1.Id
		);
		insert assoc;
		
		List<String> attNames = (List<String>) JSON.deserialize(attNamesJSON, List<String>.class);
		cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
			Name = 'myBasket',
			cscfga__Opportunity__c = o.Id
		);
		insert pb;
		
		cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
			Name = 'User Group',
			cscfga__Description__c = 'User Group'
		);
		insert pd;
		cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
			Name = 'Mobile Voice',
			cscfga__Description__c = 'Mobile Voice'
		);
		insert pd2;
		cscfga__Attribute_DefinitioN__c ad = new cscfga__Attribute_Definition__c(
			Name = 'test',
			cscfga__Product_Definition__c = pd.Id
		);
		insert ad;
		
		cscfga__Product_Configuration__c rpc = new cscfga__Product_Configuration__c(
			cscfga__Product_Definition__c = pd2.Id,
			name = 'Mobile Voice',
			cscfga__Product_Basket__c = pb.Id,
			service_plan__c = pi1.Id,
			Tenure__c = 24
		);
		insert rpc;

		cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
			cscfga__Product_Definition__c = pd.Id,
			name = 'User Group',
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Root_Configuration__c = rpc.Id,
			Tenure__c = 24,
			Group_No_of_Users__c = 10
		);
		insert pc;

		List<cscfga__Attribute__c> attList = new List<cscfga__Attribute__c>();
		List<cscfga__Attribute__c> attList2 = new List<cscfga__Attribute__c>();
		for (String attName : attNames) {
			attList.add(new cscfga__Attribute__c(
					Name = attName,
					cscfga__Attribute_Definition__c = ad.Id,
					cscfga__Value__c = '20.00',
					cscfga__Product_Configuration__c = pc.Id
				)
			);
			attList2.add(new cscfga__Attribute__c(
					Name = attName,
					cscfga__Attribute_Definition__c = ad.Id,
					cscfga__Value__c = '20.00',
					cscfga__Product_Configuration__c = rpc.Id
				)
			);
		}
		insert attList;
		insert attList2;
		/*	
		Attachment att = new Attachment(
		    Name = 'Attributes As JSON',
		    Body = Blob.valueOf(JSON.serialize(attList2)),
		    ParentId = rpc.Id
		    );	
		insert att;*/
			
		
		Test.startTest();
		Database.executeBatch(new CS_RefreshAllConfigurationsInBasket(pb.Id));
		Test.stopTest();
	}
	
	static testMethod void testRecalculate() {	
		Account a = new Account(
			Name = 'Test',
			NumberOfEmployees = 1
		);
		insert a;
		
		Opportunity o = new Opportunity(
			Name = 'Test',
			AccountId = a.Id,
			CloseDate = System.Today(),
			StageName = 'Closed Won',
			TotalOpportunityQuantity = 0
		);
		
		insert o;
		
		Usage_Profile__c up1 = new Usage_Profile__c(
			Active__c = true,
			Account__c = a.Id,
			Expected_SMS__c = 20,
			Expected_Voice__c = 10,
			Expected_Data__c = 20
		);
		
		insert up1;
		
		cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
			Name = 'Test',
			Default_Usage_Profile__c = up1.Id
		);
		insert pi1;
		
		cspmb__Rate_Card__c rc1 = new cspmb__Rate_Card__c(
			Name = 'Test'
		);
		insert rc1;
		
		cspmb__Rate_Card_Line__c rlVoice = new cspmb__Rate_Card_Line__c(
			Name = 'Voice',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlVoice;
		
		cspmb__Rate_Card_Line__c rlSMS = new cspmb__Rate_Card_Line__c(
			Name = 'SMS',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlSMS;
		
		cspmb__Rate_Card_Line__c rlData = new cspmb__Rate_Card_Line__c(
			Name = 'Data',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlData;
		
		cspmb__Price_Item_Rate_Card_Association__c assoc = new cspmb__Price_Item_Rate_Card_Association__c(
			cspmb__Rate_Card__c = rc1.Id,
			cspmb__Price_Item__c = pi1.Id
		);
		insert assoc;
		
		List<String> attNames = (List<String>) JSON.deserialize(attNamesJSON, List<String>.class);
		cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
			Name = 'myBasket',
			cscfga__Opportunity__c = o.Id
		);
		insert pb;
		
		cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
			Name = 'Single Add On',
			cscfga__Description__c = 'Single Add On'
		);
		insert pd;
		cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
			Name = 'Mobile Voice',
			cscfga__Description__c = 'Mobile Voice'
		);
		insert pd2;
		cscfga__Attribute_DefinitioN__c ad = new cscfga__Attribute_Definition__c(
			Name = 'test',
			cscfga__Product_Definition__c = pd.Id
		);
		insert ad;
		
		cscfga__Product_Configuration__c rpc = new cscfga__Product_Configuration__c(
			cscfga__Product_Definition__c = pd2.Id,
			name = 'Mobile Voice',
			cscfga__Product_Basket__c = pb.Id,
			service_plan__c = pi1.Id,
			Tenure__c = 24,
			Group_No_of_Users__c = 10
		);
		insert rpc;

		cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
			cscfga__Product_Definition__c = pd.Id,
			name = 'Single Add On',
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Root_Configuration__c = rpc.Id,
			cscfga__Parent_Configuration__c = rpc.Id,
			Tenure__c = 24,
			Group_No_of_Users__c = 5
		);
		insert pc;

		List<cscfga__Attribute__c> attList = new List<cscfga__Attribute__c>();
		List<cscfga__Attribute__c> attList2 = new List<cscfga__Attribute__c>();
		for (String attName : attNames) {
			attList.add(new cscfga__Attribute__c(
					Name = attName,
					cscfga__Attribute_Definition__c = ad.Id,
					cscfga__Value__c = '20.00',
					cscfga__Product_Configuration__c = pc.Id
				)
			);
			attList2.add(new cscfga__Attribute__c(
					Name = attName,
					cscfga__Attribute_Definition__c = ad.Id,
					cscfga__Value__c = '20.00',
					cscfga__Product_Configuration__c = rpc.Id
				)
			);
		}
		insert attList;
		insert attList2;
		/*	
		Attachment att = new Attachment(
		    Name = 'Attributes As JSON',
		    Body = Blob.valueOf(JSON.serialize(attList2)),
		    ParentId = rpc.Id
		    );	
		insert att;*/
			
		
		Test.startTest();
		
		CS_RecalculateConfigurationTotals tot = new CS_RecalculateConfigurationTotals(rpc.Id);
		
		Test.stopTest();
	}
}