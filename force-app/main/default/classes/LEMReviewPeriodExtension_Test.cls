@isTest
private class LEMReviewPeriodExtension_Test {
static testMethod void MyUnitTest() {
    Profile p = [select id from profile where name='System Administrator'];
        
        User uM = new User();
        uM.Username = '999999000@bt.com';
        uM.Ein__c = '999999000';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.Department='TestSupport';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = p.Id;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});
						        
    
    BTLB_Master__c BM = new BTLB_Master__c();
	    BM.Name='TestSupport';
	    BM.BTLB_Name_ExtLink__c='TestSupport';
	    BM.Email_Signature__c='Regards,Test Support Team';
	    BM.Company_Name__c='Test Company';
	    BM.Company_Registration_No__c='12345';
	    BM.Company_Registered_Address__c='Address';
	    insert BM;	
		
		LEM_Review__c lr = new LEM_Review__c();
		lr.iNET_FY_Target__c = 770943;
		lr.WLR_Lines_FY_Target__c = 717;
		lr.Switch_Volume_FY_Target__c = 311;
		lr.Switch_Value_FY_Target__c =1143661;
		lr.RD_Name__c = 'Chris Freeney';
		lr.PSM_Name__c = 'Malcolm Davey';
		lr.OwnerId =  uMResult[0].Id;
		lr.Name = 'BTLB Bath and Bristol';
		lr.Mobile_FY_Target__c = 689;
		lr.MD_Name__c = 'Chris Freeney';
		lr.Financial_Year__c = '2012/13';
		lr.Data_Networks_FY_Target__c = 2469525;
		lr.Calls_FY_Target__c = 812781;
		lr.Broadband_FY_Target__c = 660;
		lr.BTLB_Master_Object__c = BM.Id;
		insert lr;
		
    
    LEM_Review_Period__c lrp2 = new LEM_Review_Period__c();
		lrp2.zCheck__c = 'Test 2';
		lrp2.iNET_Previous_30_Day_Forecast__c = 123;
		lrp2.iNET_Next_30_Day_Forecast__c = 123; 
		lrp2.iNET_Actual_YTD__c = 123;
		lrp2.WLR_Lines_Previous_30_Day_Forecast__c = 123; 
		lrp2.WLR_Lines_Next_30_Day_Forecast__c = 123;
		lrp2.WLR_Lines_Actual_YTD__c = 123;
		lrp2.Switch_Volume_Previous_30_Day_Forecast__c = 123; 
		lrp2.Switch_Volume_Next_30_Day_Forecast__c = 123;  
		lrp2.Switch_Volume_Actual_YTD__c = 123;
		lrp2.Switch_Value_Previous_30_Day_Forecast__c = 123;
		lrp2.Switch_Value_Next_30_Day_Forecast__c = 123;
		lrp2.Switch_Value_Actual_YTD__c = 123;
		lrp2.Sustainable_For_Next_30_Days__c = 'Test Yes';
		lrp2.Quarter__c = 'Quarter 1';
		lrp2.Period__c =  '60 Day';
		lrp2.PSM_Signoff_Date__c = date.today(); 
		lrp2.PSM_Signoff_Comments__c = 'Test Comments';
		lrp2.Mobile_Previous_30_Day_Forecast__c = 123;
		lrp2.Mobile_Next_30_Day_Forecast__c = 123;
		lrp2.Mobile_Actual_YTD__c = 123;
		lrp2.MD_Signoff_Date__c = date.today(); 
		lrp2.MD_Signoff_Comments__c =  'Test';
		lrp2.Levelled_Signoff_Date__c = date.today(); 
		lrp2.Levelled_Signoff_Comments__c =  'Test Comments';
		lrp2.LEM_Review__c = lr.Id;
		lrp2.Improvement_Plan_Needed__c = 'Yes';
		lrp2.Data_Networks_Previous_30_Day_Forecast__c = 123;
		lrp2.Data_Networks_Next_30_Day_Forecast__c = 123; 
		lrp2.Data_Networks_Actual_YTD__c = 123;
		lrp2.Calls_Previous_30_Day_Forecast__c = 123;
		lrp2.Calls_Next_30_Day_Forecast__c = 123;
		lrp2.Calls_Actual_YTD__c = 123;
		lrp2.Broadband_Previous_30_Day_Forecast__c = 123;
		lrp2.Broadband_Next_30_Day_Forecast__c = 123;
		lrp2.Broadband_Actual_YTD__c = 123;
		insert lrp2; 
    
    PageReference pageRef = Page.LEMReviewPeriod_Attachment;
    Test.setCurrentPageReference(pageRef);
    ApexPages.currentPage().getParameters().put('id',lrp2.id);
    Apexpages.Standardcontroller sc = new Apexpages.Standardcontroller(lrp2);
    LEMReviewPeriodExtension controller = new LEMReviewPeriodExtension(sc);
    
    
    Note xyz = controller.note ;
    Attachment A = controller.Attachment;
    controller.upload();
    controller.addNote();
    controller.getAttList();
    controller.getNoteList();
    
    ApexPages.currentPage().getParameters().put('id',null);
    LEMReviewPeriodExtension controller1 = new LEMReviewPeriodExtension(sc);
    controller1.addNote();
   
    
    Attachment A1 = controller.Attachment;
    A1.Description = '';
    controller.upload();
    
}
}