global class BatchLS2L_05AlertEmail implements Database.Batchable<sobject>{
/*
###################################################################################################
To calculate the number of leads created by owner by a user and post chatter message or email
account owners from being made inactive automatically.

24/01/12    John McGovern    Intial Build

###################################################################################################
*/

public String query;

global BatchLS2L_05AlertEmail() {
}

global database.querylocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<sObject> scope){

    for(sObject s : scope) {
        User u = (User)s;

        String bodytext = '';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {u.Email}; 
        String[] ccAddresses = new String[] {'john.mcgovern@bt.com'};
        bodytext = bodytext + '################################################\n';
        bodytext = bodytext + 'THIS IS AN UNMONITORED EMAIL ACCOUNT. DO NOT REPLY.\n';
        bodytext = bodytext + '################################################\n\n';
        bodytext = bodytext + u.Name+'\n';
        bodytext = bodytext + 'You have '+u.zLeadsOwned__c.format()+' new lead(s) on Salesforce generated from Landscaping data.\n';
        bodytext = bodytext + 'Please review them at https://emea.salesforce.com/00Q?fcf=00B20000005OnX1';
        mail.setToAddresses(toAddresses);
        mail.setCcAddresses(ccAddresses);
        //mail.setReplyTo('noreply.btbusiness@bt.com');
        //mail.setSenderDisplayName('BT Salesforce Admin');
        mail.setSubject('Salesforce Alert: You Have New Landscaping Leads');
        mail.setOrgWideEmailAddressId('0D220000000CbZl');
        mail.setBccSender(false);
        mail.setUseSignature(false);
        mail.setPlainTextBody(bodytext);

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
    }
    
}

global void finish(Database.BatchableContext BC){  
    //create next batch
    if(!Test.isRunningTest()){  
        BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
        DateTime n = datetime.now().addMinutes(2);
        String cron = '';
        
        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();
        
        b.scheduled_id6__c = System.schedule('LS2L Batch 6', cron, new BatchLS2L_Schedule06());
        
        update b;       
    }           
}

}