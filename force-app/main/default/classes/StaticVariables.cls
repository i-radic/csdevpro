public with sharing class StaticVariables {
    public static String s2sType = Null;
    private static boolean taskIsrun = false;
    private static boolean taskIsBeforerun = false;
    private static boolean contactIsRunBefore = false;
    private static boolean contactDone = false;
    private static boolean AccountDontRun = false;
    private static boolean contactBeforeTriggerRun = false;
       
    private static boolean AccountBeforeDontRun = false;
    private static boolean AccountAfterDontRun = false;
    private static boolean CaseDontRun = false;
    private static boolean s2sOpportunityDontRun = false;
    private static boolean ContactValidation = false;
    private static boolean UserDontRun = false;
    //cloud voice site
    private static boolean CloudVoiceDontRun= false;
    Public static boolean CVoiceNoRun= false; // added CV error
    private static boolean taskUpdatingCampaignTasks = false;
    //public static final String campaign_task_record_type = '012S00000004MfrIAE';
    //LIVE org--> public static final String campaign_task_record_type = '01220000000Pkq5AAC';  
    public static final String campaign_task_record_type = '01220000000Pkq5AAC';  
    //CAT
    //public static final String corp_campaign_task_record_type = '012R0000000D2OwIAK';   
    public static final String deluxe_record_type = '01220000000A1D4AAK';   
    
    //LIVE                                                        
    public static final String corp_campaign_task_record_type = '01220000000AFipAAG';   
    
    public static boolean gets2sOpportunityDontRun(){
        return s2sOpportunityDontRun;
    }
    
    public static void sets2sOpportunityDontRun(boolean b){
        s2sOpportunityDontRun = b;
    }
     public static boolean getUserDontRun(){
        return UserDontRun;
    }
    
    public static void setUserDontRun(boolean b){
        UserDontRun = b;
    } 
    public static boolean getContactValidation(){
        return ContactValidation;
    }
    
    public static void setContactValidation(boolean b){
        ContactValidation = b;
    }    
    
    
     public static boolean getCaseDontRun(){
        return AccountDontRun;
    }
    
    public static void setCaseDontRun(boolean b){
        AccountDontRun = b;
    }
    
    
    public static boolean getAccountDontRun(){
        return AccountDontRun;
    }
    
    public static void setAccountDontRun(boolean b){
        AccountDontRun = b;
    }
    
    public static boolean getAccountBeforeDontRun(){
        return AccountBeforeDontRun;
    }
    
    public static void setAccountBeforeDontRun(boolean b){
        AccountBeforeDontRun = b;
    }
     public static boolean getAccountAfterDontRun(){
        return AccountAfterDontRun;
    }
    
    public static void setAccountAfterDontRun(boolean b){
        AccountAfterDontRun = b;
    }
    
    public static boolean getcontactBeforeTriggerRun(){
        return contactBeforeTriggerRun;
    }
    
    public static void setcontactBeforeTriggerRun(boolean b){
        contactBeforeTriggerRun = b;
    }
    
    public static boolean getTaskIsRun(){
        return taskIsrun;
    }
    
    public static void setTaskIsRun(boolean b){
        taskIsrun = b;
    }
    
    public static boolean getContactIsRunBefore(){
        return contactIsRunBefore;
    }
    
    public static void setContactIsRunBefore(boolean b){
        contactIsRunBefore = b;
    }
    public static boolean getContactDone(){  
        return contactDone;
    }
    
    public static void setContactDone(boolean b){
        contactDone = b;
    }
    
    public static boolean getTaskIsBeforeRun(){
        return taskIsBeforerun;
    }
    
    public static void setTaskIsBeforeRun(boolean b){
        taskIsBeforerun = b;
    }

    public static boolean getTaskUpdatingCampaignTasks(){
        return taskUpdatingCampaignTasks;
    }
    
    public static void setTaskUpdatingCampaignTasks(boolean b){
        taskUpdatingCampaignTasks = b;
    }
    
    public static boolean getCloudVoiceDontRun(){
        return CloudVoiceDontRun;
    }    
    public static void setCloudVoiceDontRun(boolean b){
        CloudVoiceDontRun = b;
    }    
    
    /*  E2E REPORTING STATIC VARIABLES - Start */
    
    //  statuses
    public final static String STATUS_SELL = 'Sell';
    public final static String STATUS_FULFILL = 'Fulfill';
    public final static String STATUS_DELIVER = 'Deliver';
    
    //  sub-statuses - SFDC STATUS
    public final static String SUBSTATUS_ORDER_CREATED = 'Order Created';
    public final static String SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS = 'Credit Referral In Progress';
    public final static String SUBSTATUS_CREDIT_VET_DISCREPANCY = 'Credit Vet Discrepancy';
    public final static String SUBSTATUS_CREDIT_REFERRAL_PENDING= 'Credit Referral Pending'; 
    public final static String SUBSTATUS_AWAITING_SUBMISSION = 'Awaiting Submission';
    
    public final static String SUBSTATUS_MANUAL_FULFILLMENT_IN_PROGRESS = 'Manual Fulfillment In Progress';
    public final static String SUBSTATUS_SUBMITTED = 'Submitted';
    public final static String SUBSTATUS_MANUAL_FULFILLMENT_PENDING = 'Manual Fulfillment Pending';
    public final static String SUBSTATUS_AUTOMATED= 'Automated';
    public final static String SUBSTATUS_ORDER_DISCREPANCY_FULL = 'Order Discrepancy - Full';
    public final static String SUBSTATUS_ORDER_DISCREPANCY_PARTIAL = 'Order Discrepancy - Partial';
    public final static String SUBSTATUS_WAITING_BT = 'Waiting BT';
    public final static String SUBSTATUS_WAITING_CUSTOMER_INFO = 'Waiting Customer Info';
    public final static String SUBSTATUS_ENGINEERS_AND_CONTRACT = 'Engineers & Contract';
    public final static String SUBSTATUS_ERROR_ON_CLOSURE = 'Error On Closure';
    public final static String SUBSTATUS_CLOSED = 'Closed';
    public final static String SUBSTATUS_CANCELLED = 'Cancelled';
    public final static String SUBSTATUS_WITH_ENGINEERS = 'With Engineers';
    public final static String SUBSTATUS_JOB_COMPLETED = 'Job Completed';
    public final static String SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT = 'Engineers, Contract & Deposit';
    public final static String SUBSTATUS_ENGINEERS_AND_DEPOSIT = 'Engineers & Deposit';
    public final static String SUBSTATUS_WAITING_ARCHIVE = 'Waiting Archive';
    public final static String SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT = 'Waiting Contract & Deposit';
    public final static String SUBSTATUS_WAITING_CANCELLATION = 'Waiting Cancellation';
    public final static String SUBSTATUS_WAITING_CONTRACT_RETURN = 'Waiting Contract Return';
    public final static String SUBSTATUS_WAITING_DEPOSIT = 'Waiting Deposit';
    public final static String SUBSTATUS_AWAITING_KCI_0 = 'Awaiting KCI 0 Pending';
    public final static String SUBSTATUS_AWAITING_KCI_1 = 'Awaiting KCI 1 Acknowledged';
    public final static String SUBSTATUS_AWAITING_KCI_2 = 'Awaiting KCI 2 Committed';
    public final static String SUBSTATUS_AWAITING_KCI_3 = 'Awaiting KCI 3 Completed';
    public final static String SUBSTATUS_CANCELLATION_IN_BES = 'Cancellation in BES';
    public final static String SUBSTATUS_CANCELLED_IN_OV = 'Cancellation in OV';
    public final static String SUBSTATUS_CANCELLED_BY_BT = 'Cancelled by BT';
    public final static String SUBSTATUS_CANCELLED_BY_CP = 'Cancelled by CP';
    public final static String SUBSTATUS_CANCELLED_UNKNOWN = 'Cancelled Unknown';
    public final static String SUBSTATUS_ORDER_CLOSED = 'Order Closed';
    public final static String SUBSTATUS_ERROR_SUBMISSION = 'Error - In Submission';
    public final static String SUBSTATUS_ERROR_ORDER = 'Error - In Order';
    public final static String SUBSTATUS_COMPLETE = 'Complete';
    
    //  oneview status
    public final static String STATUS_OV_OPEN = 'Open';
    public final static String STATUS_OV_CLOSED = 'Closed';
    public final static String STATUS_OV_COMPLETE = 'Complete';
    public final static String STATUS_OV_CANCELLED = 'Cancelled';
    public final static String STATUS_OV_PENDING = 'Pending';
    
    //  oneview sub-status
    public final static String SUBSTATUS_OV_MANUAL_FULFILMENT_REQUIRED = 'Manual Fulfilment Required';
    public final static String SUBSTATUS_OV_SUBMITTED_TO_OFS = 'Submitted To OFS';
    
    //  css status
    public final static String STATUS_CSS_OPEN = 'OPEN';
    public final static String STATUS_CSS_PENDING = 'PENDING';
    public final static String STATUS_CSS_CANCELLED = 'Cancelled';
    public final static String STATUS_CSS_CLOSED = 'Closed';
    public final static String STATUS_CSS_COMPLETED = 'COMPLETED';
    
    //  cancellation type
    public final static String STATUS_CT_CANCELLED_BY_CP = 'Cancelled by CP';
    public final static String STATUS_CT_CANCELLED_UNKNOWN = 'Cancelled Unknown';
    public final static String STATUS_CT_CANCELLED_BY_BT = 'Cancelled by BT';
    
    public final static String DISCREPANCY_RED = 'Red';
    public final static String DISCREPANCY_AMBER = 'Amber';
    public final static String DISCREPANCY_GREEN = 'Green';
    
    //LIVE ID's
    public final static String REPORT_ID_ORDERS_SELL = '00O20000004GVPI';
    public final static String REPORT_ID_ORDERS_SELL_WINBACK = '00O20000004Gaz1';
    
    public final static String REPORT_ID_ORDERS_FULLFILL = '00O20000004GVPH';
    public final static String REPORT_ID_ORDERS_FULLFILL_WINBACK = '00O20000004Gayg';
    
    public final static String REPORT_ID_ORDERLINES = '00O20000004Gaye';
    public final static String REPORT_ID_ORDERLINES_WINBACK = '00O20000004Gayf';
    
    
    //CAT ID's after refresh
    /*public final static String REPORT_ID_ORDERS_SELL = '00OP0000000JUkk';
    public final static String REPORT_ID_ORDERS_SELL_WINBACK = '00OP0000000JVD3';
    
    public final static String REPORT_ID_ORDERS_FULLFILL = '00OP0000000JUkf';
    public final static String REPORT_ID_ORDERS_FULLFILL_WINBACK = '00OP0000000JVDD';
    
    public final static String REPORT_ID_ORDERLINES = '00O20000004GQct';
    public final static String REPORT_ID_ORDERLINES_WINBACK = '00OP0000000JVDI';*/
     
    
    public final static String CAT_OWNER_NIGEL_STAGG = '00520000001U9VR';
    public final static String LIVE_OWNER_NIGEL_STAGG = '00520000001U9VR';
    
    /*  E2E REPORTING STATIC VARIABLES - End */    
    
    
}