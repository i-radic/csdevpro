@isTest(SeeAllData=true)
private class BTSportSiteAddressSearch_Test 
{    
    static testMethod void runPositiveTestCases() 
    {        
        Test_Factory.SetProperty('IsTest', 'yes');        
        BT_Sport_Site__c BTsport = new BT_Sport_Site__c();        
        BTSportSiteAddressSearch addSearchResults = new BTSportSiteAddressSearch(BTsport);        
        addSearchResults.refferalPage = 'new';        
        addSearchResults.postcode = 'GU195QT';        
        addSearchResults.getTest();        
        system.debug('getTest');        
        //addSearchResults.getAddresses();        
        //system.debug('getAddresses');        
        addSearchResults.getSelected();        
        system.debug('getSelected');        
        addSearchResults.getselectedAddress();         
        system.debug('getselectedAddress');                
        addSearchResults.CreateAddress();        
        system.debug('CreateAddress');        
        addSearchResults.ReDirect();        
        system.debug('ReDirect');         
        addSearchResults.Back();        
        system.debug('Back');
        addSearchResults.pullAddresses();
        system.debug('pullAddresses');
        
		      
     }         
    static testMethod void runPositiveTestCases2() 
    {        
        Test_Factory.SetProperty('IsTest', 'yes');        
        BT_Sport_Site__c BTsport = new BT_Sport_Site__c();        
        BTSportSiteAddressSearch addSearchResults = new BTSportSiteAddressSearch(BTsport);        
        addSearchResults.refferalPage = 'update';        
        addSearchResults.postcode = 'GU195QT';        
        addSearchResults.getTest();        
        system.debug('getTest');        
        addSearchResults.getSelected();        
        system.debug('getSelected');        
        addSearchResults.getselectedAddress();         
        system.debug('getselectedAddress');                
        addSearchResults.CreateAddress();        
        system.debug('CreateAddress');        
        addSearchResults.ReDirect();        
        system.debug('ReDirect');        
        addSearchResults.Back();        
        system.debug('Back'); 
        addSearchResults.pullAddresses();
        system.debug('pullAddresses');
    }    
    static testMethod void runPositiveTestCases3() 
    {        
        Test_Factory.SetProperty('IsTest', 'yes');        
        BT_Sport_Site__c BTsport = new BT_Sport_Site__c();        
        BTSportSiteAddressSearch addSearchResults = new BTSportSiteAddressSearch(BTsport);        
        addSearchResults.refferalPage = 'detail';        
        addSearchResults.postcode = 'GU195QT';        
        addSearchResults.getTest();        
        system.debug('getTest');        
        addSearchResults.getSelected();        
        system.debug('getSelected');        
        addSearchResults.getselectedAddress();         
        system.debug('getselectedAddress');                
        addSearchResults.CreateAddress();        
        system.debug('CreateAddress');        
        addSearchResults.ReDirect();        
        system.debug('ReDirect');        
        addSearchResults.Back();        
        system.debug('Back');  
        addSearchResults.pullAddresses();
        system.debug('pullAddresses'); 
    }
    static testMethod void runPositiveTestCases4() {
               
        BT_Sport_Site__c BTsport = new BT_Sport_Site__c();
        ApexPages.StandardController con = new ApexPages.StandardController(BTsport); 
        BTSportSiteAddressSearch addSearchResults = new BTSportSiteAddressSearch(con);
        
        Test_Factory.SetProperty('IsTest', 'No');
        addSearchResults.Back();        
        system.debug('Back');
        addSearchResults.ReDirect();        
        system.debug('ReDirect');
       
    }
    
}