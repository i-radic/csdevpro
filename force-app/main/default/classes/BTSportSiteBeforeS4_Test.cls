@isTest(SeeAllData=true)
public class BTSportSiteBeforeS4_Test {
    static BT_Sport_Pricing__c CreateOffer() {
        BT_Sport_Pricing__c offer = new BT_Sport_Pricing__c();
        offer.Entry_Type__c = 'Discount';
        offer.Start_Date__c = (Date.today()-10);
        offer.End_Date__c = (Date.today()+10);
        offer.Discount_Name__c = '15 percent discount test offer';
        offer.Discount_Amount__c = 15.00;
        offer.Discount_Period__c = 1;
        offer.UnavailableContractTypes__c = '';
        offer.Discount_Period_Months__c = 12;
        return offer;        
    }    
    static testMethod void myUnitTest1(){
        Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
        
        RecordType rtFors4 = [select id from RecordType where SobjectType='BT_Sport_Site__c' and DeveloperName ='Season4' limit 1];
        
        Account ac = Test_Factory.CreateAccount();
        insert ac;
        
        Contact c1= Test_Factory.CreateContact();
        insert c1;
        
        opportunity op = Test_factory.CreateOpportunity(ac.id);
        op.Product_Family__c= 'BT SPORT';
        insert op;
        
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId=c1.id;
        ocr.OpportunityId=op.id;
        ocr.IsPrimary= true;
        insert ocr;
        
        BT_Sport_CL__c btsCL = new BT_Sport_CL__c();
        btsCL.Opportunity__c=op.id;
        btsCL.Point_of_Origin__c='None - Trial'; 
        btsCL.Why_did_the_customer_call__c='Press advert';
        btsCL.Notes__c='some test notes';
        btsCL.What_did_you_do_Sales__c='Call back booked';
        insert btsCL;
        
        BT_Sport_Pricing__c ofr = CreateOffer();
        insert ofr;
        
        BT_Sport_SVOC__c btsSVOC = new BT_Sport_SVOC__c();
        btsSVOC.Opportunity__c=op.id;
        
        //btsSVOC.name='BTSportSite-0000028094'; 
        btsSVOC.X2010_Rateable_Value__c='6750';
        btsSVOC.X2017_Rateable_Value__c='7560';
        btsSVOC.Customer_band__c='G';
        btsSVOC.Band_for_2017_Rateable_Value__c = 'Band C - £7,001 - £9,000';
        btsSVOC.Rate_Card_Based_on_2017_Rateable_Value__c=283.00;
        btsSVOC.Site_Address_Line_1__c='testLine1';
        btsSVOC.Site_Address_Line_2__c='testLine1';
        btsSVOC.Site_Address_Line_3__c='testLine1';
        btsSVOC.Site_City__c='testCity';
        btsSVOC.Site_County__c='testcounty';
        btsSVOC.Site_ZIP_Code__c='TS1 2TS';
        btsSVOC.Does_customer_have_Hero_offer__c = 'Y';
        btsSVOC.Master_Product__c='BT Sport 1 Pub (GB)';
        btsSVOC.Master_Product_Detail__c='12 month contract|BT Sport Package|Sky Equipment Install';
        btsSVOC.BTS_Single_View_UID__c ='BTSCOM_UID_201014_0164632';
        btsSVOC.BT_Sport_Service_ID__c='BS0000062674';
        btsSVOC.BT_Sport_Customer_Status__c='Active';
        btsSVOC.DM_Email__c='test@sport.com';
        btsSVOC.DM_Alternative_Contact_Number__c='7777777777';
        btsSVOC.DM_Telephone_Number__c='9999999999';        
        btsSVOC.BAC_List__c='BAC List Test';
        insert btsSVOC;
        
        
        
        /* 
BT_Sport_Pricing__c btsPrice = new BT_Sport_Pricing__c();
btsPrice.Site_Type__c='BT Sport Total Pub (GB)';
btsPrice.Price_Type__c='3RV';
btsPrice.Band__c='Band A - £0 - £5,000';
btsPrice.PRICE_BAND_START__C = 0.00;
btsPrice.PRICE_BAND_END__C = 5000.00;
insert btsPrice;

btsPrice = new BT_Sport_Pricing__c();            //PricePoint
btsPrice.Band__c='Band A - £0 - £5,000';
btsPrice.CONTRACT_TYPE__C = '12 month contract';
btsPrice.Entry_Type__C = 'PricePoint';
btsPrice.PMF_Code__c = 'PMF1000000';
btsPrice.Site_Type__c='BT Sport Total Pub (GB)';
btsPrice.Price_Type__c='3RV';  
btsPrice.PRICE_BAND_START__C =0;
btsPrice.PRICE_BAND_END__C=5000;
insert btsPrice;

btsPrice = new BT_Sport_Pricing__c();       //PMF
btsPrice.Entry_Type__C = 'PMF';
btsPrice.PMF_VERSION__C=1.00;
btsPrice.PRICE__C=62.00;
btsPrice.START_DATE__C = Date.valueOf('2013-07-01');// '17/02/2013';
btsPrice.End_Date__c = Date.valueOf('2020-10-01');// '17/02/2013';
btsPrice.PMF_Code__c = 'PMF1000000'; 
btsPrice.EXTERNAL_ID__C = 'PMF1000000'; 
insert btsPrice;

btsPrice = new BT_Sport_Pricing__c();            //Discount
btsPrice.Entry_Type__C = 'Discount';
btsPrice.DISCOUNT_AMOUNT__C = 100;
btsPrice.UNAVAILABLECONTRACTTYPES__C='Pay as you go monthly::12 month contract with June and July 2016 FREE::18 month contract::24 month contract::';
btsPrice.START_DATE__C = Date.valueOf('2013-07-01');// '17/02/2013';
btsPrice.End_Date__c = Date.valueOf('2021-01-01');// '17/02/2013';
btsPrice.Discount_Name__C = 'First Month FREE';
btsPrice.Discount_Period__c=1;
btsPrice.Discount_Period_Months__c=1;
insert btsPrice;
*/
        BT_Sport_Site__c btss = new BT_Sport_Site__c();
        btss.RecordTypeID=rtFors4.id;//'01220000000cklV';
        btss.BT_Sport_CL__c=btsCL.Id;
        btss.Order_Type__c='Provide';        
        btss.Product__c='BT Sport Total';
        btss.Site_Type__c='BT Sport Total General';
        btss.Contract_Type__c='24 Month Bundle';
        btss.Band__c = 'Band A - £0 - £5,000';
        btss.Discounts__c = '15 percent discount test offer';
        //btss.Discounts_S2__c = 'First Month FREE';
        btss.Onsite_contact_email__c='test@test.com';
        btss.Site_Contact_Name__c='tester';
        btss.Onsite_contact_telephone_number__c='0123456789';        
        btss.Post_Code__c='e6 3nj';
        //btss.Viewing_Cards_Required__c='0';
        //btss.Is_Satellite_Install_Required__c='Yes';
        btss.gross_price__c=116;
        btss.Discount_Amount__c=0;
        btss.Price_after_Discounts__c=116;
        btss.Price_after_Discounts_S2__c=116;
        btss.Price_after_Discounts_RO__c=100;
        
        btss.BT_Sport_Wi_Fi_Contract__c = '12 Month';
        btss.BT_Sport_Wi_Fi__c='Required';
        btss.BT_Sport_Wi_Fi_Offer__c='6 Months Free BT Sport Wi-Fi';
        btss.BT_Sport_Wi_Fi_Price__c='£17.00';
        btss.BT_Sport_Wi_Fi_Order_number__c='BT12TEST';
        btss.Site_Code__c='SPOR-04057-90';
        btss.Site_Name__c='SPORTHyderabad';
        btss.Rosita_ref__c='resRefNum123';
        btss.Broadband_Product__c= 'BT Business Infinity Unlimited';
        btss.Broadband_Bundle_Offers__c='12 Months Free with BT Sport';
        btss.Best_Contact_Date_Time__c = 'anytime in the evening';
        
        BT_Sport_Site__c Site2 = new BT_Sport_Site__c();
        Site2.BT_Sport_CL__c = btsCL.Id;
        Site2.RecordTypeID=rtFors4.id;
        Site2.Product__c = 'BT Sport 1';
        Site2.Site_Type__c = 'BT Sport 1 Hotel Rooms';
        Site2.Contract_Type__c = '12 month contract';
        Site2.Band__c = 'Band A - £0 - £5,000';
        //Site2.Discounts_S2__c = '50 percent off for 3 months';
        Site2.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site2.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site2.Site_Display_Name__c = 'TESTER';
        Site2.Site_Telephone__c = '02085878834'; 
        
        
        BT_Sport_Site__c Site3 = new BT_Sport_Site__c();
        Site3.BT_Sport_CL__c = btsCL.Id;
        Site3.RecordTypeID=rtFors4.id;
        Site3.Product__c = 'BT Sport Total';
        Site3.Site_Type__c = 'BT Sport Total Pub (GB)';
        Site3.Contract_Type__c = 'Pay as you go monthly';
        Site3.Band__c = 'Band A - £0 - £5,000';
        Site3.Discounts__c = '';
        Site3.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site3.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site3.Site_Display_Name__c = 'TESTER';
        Site3.Site_Telephone__c = '02085878834';
        Site3.Override_Band__c='Band A - £0 - £5,000';
        
        Test.startTest();
        insert btss;           
        insert Site2;
        insert Site3;
        Test.stopTest();
    }
}