@isTest

public class BTLBSuiteCTRL_TEST {
    static Account a {get; set;}
    static Product2  p {get; set;}
    static PricebookEntry pbe {get; set;}
    static Opportunity o {get; set;}
    static OpportunityLineItem oli {get; set;}
    static Contact c {get; set;}   
    static BTLB_Suite__c bs {get; set;} 
    static BTLB_Suite__c bs2 {get; set;} 
    static BTLB_Suite_Order_Lines__c bsol {get; set;} 
    static BTLB_Suite_Order_Lines__c bsol2 {get; set;} 
    static BTLB_Suite_Rate_Card__c bsrc {get; set;} 

    static void SetVariables(){ 
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
        } 
        a = Test_Factory.CreateAccount();     
        a.CUG__c = 'cugCV1';
        a.OwnerId = UserInfo.GetUserId() ;
        Database.SaveResult aR = Database.insert(a); 
        
        o = Test_Factory.CreateOpportunity(a.Id);
        o.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(o);  
		
        p= Test_Factory.CreateProduct();
        insert p; 
		pbe= Test_Factory.CreatePricebookEntry('01s20000000HXVd',p.Id);
        insert pbe; 
		oli= Test_Factory.CreateOpportunityLineItem(pbe.Id,o.Id);
        insert oli; 

        c= Test_Factory.CreateContact();
        c.Cug__c = 'cugCV1';
        c.Contact_Post_Code__c = 'WR5 3RL';
        c.Email = 'test@email.com';
        insert c;   
 

		bsrc= new BTLB_Suite_Rate_Card__c(Product_Description__c='test',Commission__c=10,Commission_Type__c='Fixed');
        insert bsrc;
		bs= new BTLB_Suite__c(Name='test',Opportunity__c = o.Id, Order_Reference__c='ApexTest12345', Order_Date__c=system.today());
        insert bs;
		bsol= new BTLB_Suite_Order_Lines__c(Product_Description__c='test',Order_Reference__c=bs.Id,Order_Line_Type__c='BT Entered', Commission_Agent_1__c = UserInfo.GetUserId(), Commission_Agent_2__c = UserInfo.GetUserId(), Commission_Agent_3__c = UserInfo.GetUserId(), Commission_Agent_4__c = UserInfo.GetUserId(), Referrer__c = UserInfo.GetUserId(), Commission_Split_1__c = 100, Quantity_Split_1__c=100, SOV_Split_1__c=100);
        insert bsol;
		bsol2= new BTLB_Suite_Order_Lines__c(Product_Description__c='test',Order_Reference__c=bs.Id,Order_Line_Type__c='BT Entered', Referrer__c = UserInfo.GetUserId(), Commission_Split_1__c = 100, Quantity_Split_1__c=100, SOV_Split_1__c=100);
        insert bsol2;
	}

	public static testMethod void testBTLBSuite1(){      
        Test.setCurrentPageReference(new PageReference('Page.BTLBSuiteNew')); 
        SetVariables(); 

        Test.startTest();
        System.currentPageReference().getParameters().put('oppid', o.Id); 
        System.currentPageReference().getParameters().put('userName', 'Test Name'); 
		
        BTLBSuiteCTRL stdCtrl = new BTLBSuiteCTRL(new ApexPages.StandardController(bs));  

        BTLBSuiteCTRL ctrl= new BTLBSuiteCTRL();       
        ctrl.oppName='test';       
        ctrl.accName='test';       
        ctrl.OID='test';      
        ctrl.orderDescr='test'; 
        ctrl.textColor='test'; 
        ctrl.showData='test'; 
        ctrl.oliUpdate=o.id+':test,'; 
        ctrl.accId=a.Id;
        ctrl.existingRef='test'; 		
	
		ctrl.orderRef0 ='test'; ctrl.orderRef1 ='test'; ctrl.orderRef2 ='test'; ctrl.orderRef3 ='test'; ctrl.orderRef4 ='test'; ctrl.orderRef5 ='test'; ctrl.orderRef6 ='test'; ctrl.orderRef7 ='test'; ctrl.orderRef8 ='test'; ctrl.orderRef9 ='test'; ctrl.orderRef10 ='test'; ctrl.orderRef11 ='test'; 
		ctrl.orderType0 ='test'; ctrl.orderType1 ='test'; ctrl.orderType2 ='test'; ctrl.orderType3 ='test'; ctrl.orderType4 ='test'; ctrl.orderType5 ='test'; ctrl.orderType6 ='test'; ctrl.orderType7 ='test'; ctrl.orderType8 ='test'; ctrl.orderType9 ='test'; ctrl.orderType10 ='test'; ctrl.orderType11 ='test'; 
		ctrl.commAgent1 =UserInfo.GetUserId(); ctrl.commAgent2 =UserInfo.GetUserId(); ctrl.commAgent3 =UserInfo.GetUserId(); ctrl.commAgent4 =UserInfo.GetUserId(); 

		ctrl.orderQty0 =1888; ctrl.orderQty1 =1888; ctrl.orderQty2 =1888; ctrl.orderQty3 =1888; ctrl.orderQty4 =1888; ctrl.orderQty5 =1888; ctrl.orderQty6 =1888; ctrl.orderQty7 =1888; ctrl.orderQty8 =1888; ctrl.orderQty9 =1888; ctrl.orderQty10 =1888; ctrl.orderQty11 =1888; 
		ctrl.orderSOV0 =1888; ctrl.orderSOV1 =1888; ctrl.orderSOV2 =1888; ctrl.orderSOV3 =1888; ctrl.orderSOV4 =1888; ctrl.orderSOV5 =1888; ctrl.orderSOV6 =1888; ctrl.orderSOV7 =1888; ctrl.orderSOV8 =1888; ctrl.orderSOV9 =1888; ctrl.orderSOV10 =1888; ctrl.orderSOV11 =1888; 
		ctrl.commSplit1 =25; ctrl.commSplit2 =25; ctrl.commSplit3 =25; ctrl.commSplit4 =25; 

		ctrl.orderCRD0 =system.today();ctrl.orderCRD1 =system.today();ctrl.orderCRD2 =system.today();ctrl.orderCRD3 =system.today();ctrl.orderCRD4 =system.today();ctrl.orderCRD5 =system.today();ctrl.orderCRD6 =system.today();ctrl.orderCRD7 =system.today();ctrl.orderCRD8 =system.today();ctrl.orderCRD9 =system.today();ctrl.orderCRD10 =system.today();ctrl.orderCRD11 =system.today();
		ctrl.orderDate0 =system.today();ctrl.orderDate1 =system.today();ctrl.orderDate2 =system.today();ctrl.orderDate3 =system.today();ctrl.orderDate4 =system.today();ctrl.orderDate5 =system.today();ctrl.orderDate6 =system.today();ctrl.orderDate7 =system.today();ctrl.orderDate8 =system.today();ctrl.orderDate9 =system.today();ctrl.orderDate10 =system.today();ctrl.orderDate11 =system.today();
		
		ctrl.onLoad();
		ctrl.getPCTs();
		ctrl.saveRecord();
		ctrl.cancelRecord();
		ctrl.noAction();
		ctrl.getOppLineItems();
		ctrl.getOppOrders();
		ctrl.getOrders();
		ctrl.getCommissions();
		ctrl.getCommissionsAgg();
		ctrl.getStatusAgg();
		ctrl.getBTStatusAgg();
		ctrl.getStatusSch5();
		ctrl.getAllStatus();
		ctrl.goDownloadAll();
		ctrl.goDownloadUser();
		ctrl.getRateCardProducts();

        ctrl.records=bs.id+':test,'; 
		ctrl.saveRecords();
		
        stdCtrl.oliUpdate=oli.id+':1,'; 
		stdCtrl.oliUpdate();
		stdCtrl.thisRec.Id=null;
        stdCtrl.oliUpdate=oli.id+':1,'; 
		stdCtrl.oliUpdate();
		stdCtrl.getOppLineItems2();
		stdCtrl.getOrderLineItems();
		stdCtrl.getPeople();

        Test.stopTest();
	}	
	
	public static testMethod void testBTLBSuite2(){      
        Test.setCurrentPageReference(new PageReference('Page.BTLBSuiteNew')); 
        SetVariables(); 
		System.currentPageReference().getParameters().put('oppId', o.Id);  
        System.currentPageReference().getParameters().put('filterUser', UserInfo.GetUserId()); 
        System.currentPageReference().getParameters().put('userName', 'Test Name'); 
		    
		o.StageName = 'Won';
		o.Amount = 10;
		update o;

        BTLBSuiteCTRL ctrl= new BTLBSuiteCTRL();
		ctrl.oppCheck = 'ready';
		ctrl.userName = '';
		ctrl.onLoad();
		
		delete bsol;
	}	

	public static testMethod void testBTLBSuite3(){  
        SetVariables();   
        Test.setCurrentPageReference(new PageReference('Page.BTLBSuiteView')); 
		System.currentPageReference().getParameters().put('oppId', null);  

        BTLBSuiteCTRL ctrl= new BTLBSuiteCTRL();
		ctrl.oppId = '';
		ctrl.onLoad();
		ctrl.goLoad();

	}
	public static testMethod void testBTLBSuite4(){    
        SetVariables(); 
        Test.setCurrentPageReference(new PageReference('Page.BTLBSuiteNew')); 
		System.currentPageReference().getParameters().put('oppId', o.Id);   
        System.currentPageReference().getParameters().put('filterUser', UserInfo.GetUserId()); 
        System.currentPageReference().getParameters().put('filterDate', 'LAST_MONTH'); 
        System.currentPageReference().getParameters().put('filterStatus', 'Test Name'); 
        System.currentPageReference().getParameters().put('filterBTStatus', 'Test Name'); 
        System.currentPageReference().getParameters().put('filterSch5', 'Test Name'); 
        System.currentPageReference().getParameters().put('filterSOV', '000-100'); 
        System.currentPageReference().getParameters().put('filterComm', '000-100'); 

        BTLBSuiteCTRL ctrl= new BTLBSuiteCTRL();
		ctrl.oppId = '';
		ctrl.onLoad();
		ctrl.goLoad();

	}

	public static testMethod void testBTLBSuite5(){      
        Test.setCurrentPageReference(new PageReference('Page.BTLBSuiteCommissions')); 
        SetVariables(); 
		System.currentPageReference().getParameters().put('oppId', o.Id);  
        System.currentPageReference().getParameters().put('filterUser', UserInfo.GetUserId()); 
        System.currentPageReference().getParameters().put('userName', 'Test Name'); 
		    
		o.StageName = 'Won';
		o.Amount = 10;
		update o;

        BTLBSuiteCTRL ctrl= new BTLBSuiteCTRL();
		ctrl.oppCheck = 'ready';
		ctrl.userName = '';
		ctrl.onLoad();
	}	
}