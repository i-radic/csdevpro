@isTest
public class CS_BTnetDiscountCtrl_Test {
   
  
    public static testmethod void BTnetDiscountCtrlTest1(){
        
        CS_BTnet_Discount_Pricing__c BtnetDiscPrice= new CS_BTnet_Discount_Pricing__c();
        CS_BTnet_Discount_Pricing__c BtnetDiscPrice2= new CS_BTnet_Discount_Pricing__c();
       // BtnetDiscPrice.name ='TestDsicountBTnet';
        BtnetDiscPrice.Bearer_Speed__c = '100MB';
        BtnetDiscPrice.Code__c ='EFM121';
        BtnetDiscPrice.Contract_Term__c =12;
        BtnetDiscPrice.Term__c =1;
        BtnetDiscPrice.Component__c ='Port Speed';
        BtnetDiscPrice.Price__c = 300;
        Insert BtnetDiscPrice;
        BtnetDiscPrice2.Bearer_Speed__c = '1000MB';
        BtnetDiscPrice2.Code__c ='EFM121';
        BtnetDiscPrice2.Contract_Term__c =36;
        BtnetDiscPrice2.Term__c =3;
        BtnetDiscPrice2.Component__c ='Port Speed';
        BtnetDiscPrice2.Price__c = 450;
        Insert BtnetDiscPrice2;
        
         PageReference pageRef = Page.CS_BTnetDiscount;
         test.setCurrentPageReference(pageRef);
         pageRef.getParameters().put('BTnetDiscountedWinPrice','25');//PrimaryBearerSpeed
         pageRef.getParameters().put('BTnetDiscounted','true');
         pageRef.getParameters().put('ContractTerm','1');
         pageRef.getParameters().put('NPVTotalUpfrontRevenue','25');
         pageRef.getParameters().put('NPVTotalUpfrontCost','20');
         pageRef.getParameters().put('NPVTotalMonthlyRevenue','45');
         pageRef.getParameters().put('NPVTotalMonthlyCost','10');
         pageRef.getParameters().put('ServiceVariant','Standard');
         pageRef.getParameters().put('PrimaryAccessBearerCost','20');
        pageRef.getParameters().put('ShadowPrimaryPortSpeed','25'); //25
         pageRef.getParameters().put('PrimaryAccessBearerMonthlyPrice','30');
          pageRef.getParameters().put('PrimaryPortSpeedMonthlyPrice','30');
        pageRef.getParameters().put('PrimaryPortSpeedCost','30');
         pageRef.getParameters().put('PrimaryMonthlyRental','30');
         pageRef.getParameters().put('PrimaryBearerSpeed','100MB');//primaryAccessBearer
       // pageRef.getParameters().put('primaryAccessBearer','100MB');
             CS_BTnetDiscountCtrl cntrol= new CS_BTnetDiscountCtrl();
             cntrol.setDiscount();
             cntrol.saveDiscount();
        CS_BTnetDiscountCtrl.BtNetDiscount BTDiscWrp =new CS_BTnetDiscountCtrl.BtNetDiscount();
       CS_BTnetDiscountCtrl.BtNetCommission BtNetCommission = new CS_BTnetDiscountCtrl.BtNetCommission();
    }
    public static testmethod void BTnetDiscountCtrlTest2(){
         CS_BTnet_Discount_Pricing__c BtnetDiscPrice= new CS_BTnet_Discount_Pricing__c();
         CS_BTnet_Discount_Pricing__c BtnetDiscPrice2= new CS_BTnet_Discount_Pricing__c();
       // BtnetDiscPrice.name ='TestDsicountBTnet';
        BtnetDiscPrice.Bearer_Speed__c = '100MB';
        BtnetDiscPrice.Code__c ='EFM121';
        BtnetDiscPrice.Contract_Term__c =12;
        BtnetDiscPrice.Term__c =1;
        BtnetDiscPrice.Component__c ='Access Bearer';
        BtnetDiscPrice.Price__c = 100;
        Insert BtnetDiscPrice;
        BtnetDiscPrice2.Bearer_Speed__c = '100MB';
        BtnetDiscPrice2.Code__c ='EFM122';
        BtnetDiscPrice2.Contract_Term__c =12;
        BtnetDiscPrice2.Term__c =1;
        BtnetDiscPrice2.Component__c ='Port Speed';
        BtnetDiscPrice2.Price__c = 100;
        Insert BtnetDiscPrice2;
        
        
         PageReference pageRef = Page.CS_BTnetDiscount;
         test.setCurrentPageReference(pageRef);
         pageRef.getParameters().put('BTnetDiscountedWinPrice','25');//PrimaryBearerSpeed
         pageRef.getParameters().put('BTnetDiscounted','true');
         pageRef.getParameters().put('ContractTerm','1');
         pageRef.getParameters().put('NPVTotalUpfrontRevenue','25');
         pageRef.getParameters().put('NPVTotalUpfrontCost','20');
         pageRef.getParameters().put('NPVTotalMonthlyRevenue','125');
         pageRef.getParameters().put('NPVTotalMonthlyCost','10');
         pageRef.getParameters().put('ServiceVariant','Loadbalanced');
         pageRef.getParameters().put('PrimaryAccessBearerCost','20');
        pageRef.getParameters().put('ShadowPrimaryPortSpeed','2'); //25
         pageRef.getParameters().put('PrimaryAccessBearerMonthlyPrice','30');
          pageRef.getParameters().put('PrimaryPortSpeedMonthlyPrice','30');
        pageRef.getParameters().put('PrimaryPortSpeedCost','30');
         pageRef.getParameters().put('PrimaryMonthlyRental','30');
         pageRef.getParameters().put('PrimaryBearerSpeed','100MB');
        pageRef.getParameters().put('SecondaryBearerSpeed','100MB');
        pageRef.getParameters().put('SecondaryAccessBearerMonthlyPrice','25');
        pageRef.getParameters().put('SecondaryAccessBearerCost','30');
        //pageRef.getParameters().put('SecondaryAccessBearerBCode','25');
        pageRef.getParameters().put('ShadowSecondaryPortSpeed','12');
        pageRef.getParameters().put('SecondaryPortSpeedMonthlyPrice','35');
        pageRef.getParameters().put('SecondaryPortSpeedCost','40');
       //  pageRef.getParameters().put('SecondaryPortSpeedBCode','40');
         pageRef.getParameters().put('SecondaryMonthlyRental','40');   
             CS_BTnetDiscountCtrl cntrol= new CS_BTnetDiscountCtrl();
             cntrol.setDiscount();
             cntrol.saveDiscount();
        CS_BTnetDiscountCtrl.BtNetDiscount BTDiscWrp =new CS_BTnetDiscountCtrl.BtNetDiscount();
       CS_BTnetDiscountCtrl.BtNetCommission BtNetCommission = new CS_BTnetDiscountCtrl.BtNetCommission();
    }
    
}