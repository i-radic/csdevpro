@istest

private class Test_OpportunityHasICTCapability {

    testmethod static void Test_FlagIsSetExist(){
        Test_Factory.setProperty('IsTest', 'yes');
        
        insert Test_Factory.CreateAccountForDummy();
        Account account = Test_Factory.CreateAccount();        
        Database.SaveResult accountResult = Database.insert(account);

        Pricebook2 priceBook = [select Id from Pricebook2 where isStandard=true limit 1];
        
        Opportunity opp1 = Test_Factory.CreateOpportunity(accountResult.getid());
        opp1.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(opp1);
        
        Product2 producta = Test_Factory.CreateProduct();
        Database.SaveResult productaResult = Database.insert(producta);

        PricebookEntry pricebookEntrya = Test_Factory.CreatePricebookEntry(priceBook.Id, productaResult.getId());        
        Database.SaveResult pricebookEntryResulta = Database.insert(pricebookEntrya);        
        
        OpportunityLineItem opportunityLineItema = Test_Factory.CreateOpportunityLineItem(pricebookEntryResulta.getId(), opptResult.getId());        
        insert opportunityLineItema;
        
        Product2 productb = Test_Factory.CreateProduct();
        productb.Family = 'ICT_TEST';
        Database.SaveResult productbResult = Database.insert(productb);
        
        PricebookEntry pricebookEntryb = Test_Factory.CreatePricebookEntry(priceBook.Id, productbResult.getId());        
        Database.SaveResult pricebookEntryResultb = Database.insert(pricebookEntryb);        
        
        OpportunityLineItem opportunityLineItemb = Test_Factory.CreateOpportunityLineItem(pricebookEntryResultb.getId(), opptResult.getId());        
        opportunityLineItemb.Category__c = 'Additional';
        insert opportunityLineItemb;        
    }
}