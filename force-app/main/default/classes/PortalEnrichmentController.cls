public class PortalEnrichmentController {
    public Id basketId { get; set;}
    public PortalEnrichmentController(){
        this.basketId = System.currentPagereference().getParameters().get('basketId');
    }
    
    public PageReference onLoad(){
		Boolean isGuest = UserInfo.getUserType() == 'Guest';
		if(isGuest){
            PageReference retURL = new PageReference('/CommunitiesLogin');
            retURL.setRedirect(true);
            return retURL;
        }
        return null;
	}
}