public class CS_MDSXMLGenerator extends CS_SolutionDownstreamDataBase {

    public Id opportunityId;
    public String oppName;
    public Map<String, String> xmlDataMap = new Map<String, String>();

    public CS_MDSXMLGenerator (Id oppId, List<Id> orderIds) {
        super(orderIds);
        this.opportunityId = oppId;
    }
    
    /*protected override void CreateData (
            Map<Id, csord__Order__c> orderByIds,
            Map<Id, csord__Service__c> servicesByIds,
            Map<Id, cscfga__Product_Basket__c> productBasketsByIds,
            Map<Id, cscfga__Product_Configuration__c> productConfigurationsByIds,
            Map<Id, CS_ServiceSpecification> serviceSpecificationsByIds) {
        queryAdditionalRequiredData();
        prepareXMLDataMap(serviceSpecificationsByIds);
        String xmlOutput = constructXML();
        storeXML(xmlOutput);
    }*/
    
    public override void createData () {
        super.generateSpecifications();
        super.mapSpecifications();
        super.queryData();
        System.debug('orderByIds : ' + orderByIds);
        System.debug('servicesByIds : ' + servicesByIds);
        System.debug('productBasketsByIds : ' + productBasketsByIds);
        System.debug('productConfigurationsByIds : ' + productConfigurationsByIds);
        System.debug('serviceSpecificationsByIds : ' + serviceSpecificationsByIds);
        
        queryAdditionalRequiredData();
        prepareOrderXMLDataMap(serviceSpecificationsByIds);
        System.debug('xmlDataMap : ' + xmlDataMap);
        String xmlPayload = constructXML();
        System.debug('xmlPayload : ' + xmlPayload);
        storeXML(xmlPayload);
    }
    
    private void queryAdditionalRequiredData () {
        Opportunity opp = [SELECT Id, Name, Payment_Type__c, AssociatedLE__c, AssociatedLE__r.Name, AssociatedLE__r.Registered_Address__c, AssociatedLE__r.Registered_Postcode__c, AssociatedLE__r.Company_Phone__c FROM Opportunity WHERE Id = :opportunityId];
        if (opp != NULL) {
            oppName = opp.Name;
            xmlDataMap.put('PaymentType', opp.Payment_Type__c);
            if (opp.AssociatedLE__c != NULL) {
                xmlDataMap.put('CompanyName', opp.AssociatedLE__r.Name);
                List<String> address = opp.AssociatedLE__r.Registered_Address__c.split('\n');
                Integer i = 1;
                for (String addr : address) {
                    xmlDataMap.put('Address' + i, addr);
                    i++;
                }
                xmlDataMap.put('Postcode', opp.AssociatedLE__r.Registered_Postcode__c);
                xmlDataMap.put('CompanyTelephoneNumber', opp.AssociatedLE__r.Company_Phone__c);
                xmlDataMap.put('CustomerReference', '');
            }
        }
        List<AggregateResult> userGroupSvcs = [SELECT count(Id) userGroupCount FROM csord__Service__c WHERE Id IN :servicesByIds.KeySet() AND csordtelcoa__Product_Configuration__r.Product_Definition_Name__c = 'User Group'];
        xmlDataMap.put('Quantity', String.valueOf(userGroupSvcs[0].get('userGroupCount')));
    }
    
    private void prepareOrderXMLDataMap (Map<Id, CS_ServiceSpecification> serviceSpecificationsByIds) {
        for (CS_ServiceSpecification serviceSpec : serviceSpecificationsByIds.values()) {
            for (CS_ServiceSpecification.SimpleAttribute smpAttr : serviceSpec.simpleAttributes) {
                xmlDataMap.put(smpAttr.name, smpAttr.value);
            }
        }
    }
    
    private void prepareSubscriptionXMLDataMap (csord__Service__c service) {
        for (CS_ServiceSpecification.SimpleAttribute smpAttr : serviceSpecificationsByIds.get(service.Id).simpleAttributes) {
            xmlDataMap.put(smpAttr.name, smpAttr.value);
        }
        xmlDataMap.put('TariffCode', service.csordtelcoa__Product_Configuration__r.Tariff_code__c);
        xmlDataMap.put('PackageCode', service.csordtelcoa__Product_Configuration__r.Package_Code__c);
    }
    
    private String constructXML () {
        XmlStreamWriter w = new XmlStreamWriter();
        	w.writeStartDocument(null, '1.0');
        		//w.writeAttribute(null, null, 'encoding', 'UTF-8');
        		w.writeStartElement('n', 'BulkProcessRequest', 'http://mdsuk.com/dise3g/bulkprocess');
        		w.writeNamespace('n', 'http://mdsuk.com/dise3g/bulkprocess');
        		w.writeNamespace('createCustomerStructure', 'http://mdsuk.com/dise3g/bulkprocess/createCustomerStructure');
        		w.writeNamespace('createOrder', 'http://mdsuk.com/dise3g/bulkprocess/createOrder');
        		w.writeNamespace('xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        		//w.writeNamespace('schemaLocation', 'http://mdsuk.com/dise3g/bulkprocess C:\Program Files (x86)\CMP Bulk Order\DISEBulkTools\schema\BulkProcess\BulkProcessRequest.xsd');
        			w.writeStartElement(null, 'CreateOrder', null);
        			
        				/*w.writeStartElement(null, 'AccountNumber', null);
        					w.writeCharacters('Pull the AccountNumber here');
        				w.writeEndElement(); //end AccountNumber*/
        				checkAndWriteElement(w, 'AccountNumber');
        				
        				w.writeStartElement(null, 'OrderHeader', null);
        					
        					/*w.writeStartElement(null, 'SalesAccountNumber', null);
        						w.writeCharacters(xmlDataMap.get('SalesAccountNumber'));
        					w.writeEndElement(); //end SalesAccountNumber*/
        					checkAndWriteElement(w, 'SalesAccountNumber');
        					
        					/*w.writeStartElement(null, 'PromisedForDate', null);
        						w.writeCharacters(xmlDataMap.get('PromisedForDate'));
        					w.writeEndElement(); //end PromisedForDate*/
        					checkAndWriteElement(w, 'PromisedForDate');
        					
        					/*w.writeStartElement(null, 'DespatchByDate', null);
        						w.writeCharacters(xmlDataMap.get('DespatchByDate'));
        					w.writeEndElement(); //end DespatchByDate*/
        					checkAndWriteElement(w, 'DespatchByDate');
        					
        					/*w.writeStartElement(null, 'WarehouseCode', null);
        						w.writeCharacters(xmlDataMap.get('WarehouseCode'));
        					w.writeEndElement(); //end WarehouseCode*/
        					checkAndWriteElement(w, 'WarehouseCode');
        					
        					/*w.writeStartElement(null, 'DeliveryMethodCode', null);
        						w.writeCharacters(xmlDataMap.get('DeliveryMethodCode'));
        					w.writeEndElement(); //end DeliveryMethodCode*/
        					checkAndWriteElement(w, 'DeliveryMethodCode');
        					
        					/*w.writeStartElement(null, 'DeliveryInstructions', null);
        						w.writeCharacters(xmlDataMap.get('DeliveryInstructions'));
        					w.writeEndElement(); //end DeliveryInstructions*/
        					checkAndWriteElement(w, 'DeliveryInstructions');
        					
        					checkAndWriteElement(w, 'SubscriptionAccountCode');
        					
        					/*w.writeStartElement(null, 'CustomerReference', null);
        						w.writeCharacters(xmlDataMap.get('CustomerReference'));
        					w.writeEndElement(); //end CustomerReference*/
        					checkAndWriteElement(w, 'CustomerReference');
        					
        					/*w.writeStartElement(null, 'PaymentType', null);
        						w.writeCharacters(xmlDataMap.get('PaymentType'));
        					w.writeEndElement(); //end PaymentType*/
        					checkAndWriteElement(w, 'PaymentType');
        					
        					w.writeStartElement(null, 'ShippingAddressInformation', null);
        						w.writeStartElement(null, 'Address', null);
        							w.writeStartElement(null, 'BusinessAddress', null);
        							
        							    checkAndWriteElement(w, 'Title');
        							    checkAndWriteElement(w, 'Surname');
        							    checkAndWriteElement(w, 'Forename');
        							    
        								/*w.writeStartElement(null, 'CompanyName', null);
        									w.writeCharacters(xmlDataMap.get('CompanyName'));
        								w.writeEndElement(); //end CompanyName*/
        								checkAndWriteElement(w, 'CompanyName');
        								
        								/*w.writeStartElement(null, 'Address1', null);
        									w.writeCharacters(xmlDataMap.get('Address1'));
        								w.writeEndElement(); //end Address1*/
        								checkAndWriteElement(w, 'Address1');
        								
        								/*w.writeStartElement(null, 'Address2', null);
        									w.writeCharacters(xmlDataMap.get('Address2'));
        								w.writeEndElement(); //end Address2*/
        								checkAndWriteElement(w, 'Address2');
        								
        								/*w.writeStartElement(null, 'Address3', null);
        									w.writeCharacters(xmlDataMap.get('Address3'));
        								w.writeEndElement(); //end Address3*/
        								checkAndWriteElement(w, 'Address3');
        								
        								/*w.writeStartElement(null, 'Postcode', null);
        									w.writeCharacters(xmlDataMap.get('Postcode'));
        								w.writeEndElement(); //end Postcode*/
        								checkAndWriteElement(w, 'Postcode');
        								
        								checkAndWriteElement(w, 'ContactTelephoneNumber');
        								
        							w.writeEndElement(); //end BusinessAddress
        						w.writeEndElement(); //end Address
        					w.writeEndElement(); //end ShippingAddressInformation
        					
        				w.writeEndElement(); //end OrderHeader
        				
        				for (Id serviceId : servicesByIds.KeySet()) {
        				    csord__Service__c service = servicesByIds.get(serviceId);
        				    if (String.isNotEmpty(service.csordtelcoa__Product_Configuration__r.Package_Code__c)) {
        				        prepareSubscriptionXMLDataMap(service);
            				    addOrderProductDetails(w);
        				    }  
        				}
        				//End of Order Products Loop
        				
        			w.writeEndElement(); //end CreateOrder
        		w.writeEndElement(); //end library
        	w.writeEndDocument();
        String xmlOutput = w.getXmlString();
        w.close();
        System.debug('xmlOutput : ' + xmlOutput);
        return xmlOutput;
    }
    
    private void addOrderProductDetails (XmlStreamWriter w) {
        w.writeStartElement(null, 'OrderProduct', null);
			w.writeStartElement(null, 'OrderProductData', null);
			
			    checkAndWriteElement(w, 'SubsOrderType');
			
				/*w.writeStartElement(null, 'ProductCode', null);
					w.writeCharacters(xmlDataMap.get('ProductCode'));
				w.writeEndElement(); //end ProductCode*/
				checkAndWriteElement(w, 'ProductCode');
				
				/*w.writeStartElement(null, 'Quantity', null);
					w.writeCharacters(xmlDataMap.get('Quantity'));
				w.writeEndElement(); //end Quantity*/
				checkAndWriteElement(w, 'Quantity');
				
				/*w.writeStartElement(null, 'ActivationType', null);
					w.writeCharacters(xmlDataMap.get('ActivationType'));
				w.writeEndElement(); //end ActivationType*/
				checkAndWriteElement(w, 'ActivationType');
				
				checkAndWriteElement(w, 'Comment');
				
				/*w.writeStartElement(null, 'ProductPriceOverride', null);
					w.writeCharacters(xmlDataMap.get('ProductPriceOverride'));
				w.writeEndElement(); //end ProductPriceOverride*/
				checkAndWriteElement(w, 'ProductPriceOverride');
				
			w.writeEndElement(); //end OrderProductData
		
			w.writeStartElement(null, 'Subscriptions', null);
			    //Loop through Subscriptions
				for (Id serviceId : servicesByIds.KeySet()) {
				    csord__Service__c service = servicesByIds.get(serviceId);
				    //if (service.csordtelcoa__Product_Configuration__r.Product_Definition_Name__c == 'User Group') {
				        //xmlDataMap.clear();
				        prepareSubscriptionXMLDataMap(service);
				        addSubscriptionDetails(w);
				    //}
				}
				//End of Subscriptions Loop
			w.writeEndElement(); //end Subscriptions
		
		w.writeEndElement(); //end OrderProduct
    }
    
    private void addSubscriptionDetails (XmlStreamWriter w) {
        w.writeStartElement(null, 'Subscription', null);
		    w.writeStartElement(null, 'SubscriptionData', null);
		
			    checkAndWriteElement(w, 'AgreementNumber');
			
			    w.writeStartElement(null, 'Contract', null);

					checkAndWriteElement(w, 'ContractNumber');
					checkAndWriteElement(w, 'ContractTerm');
					checkAndWriteElement(w, 'ContractStartedOn');
					
				w.writeEndElement(); //end Contract
				
				checkAndWriteElement(w, 'UserName');
				checkAndWriteElement(w, 'TariffCode');
				checkAndWriteElement(w, 'PackageCode');
				checkAndWriteElement(w, 'SalesAccountCode');
				
				w.writeStartElement(null, 'ConnectionDetails', null);

					checkAndWriteElement(w, 'ConnectionType');
					checkAndWriteElement(w, 'ConnectionDate');
					checkAndWriteElement(w, 'ConnectionReason');
					checkAndWriteElement(w, 'ConnectionTime');
					
				w.writeEndElement(); //end ConnectionDetails
				
				w.writeStartElement(null, 'Address', null);
					w.writeStartElement(null, 'BusinessAddress', null);
					
						checkAndWriteElement(w, 'CompanyName');
						checkAndWriteElement(w, 'Address1');
						checkAndWriteElement(w, 'Address2');
						checkAndWriteElement(w, 'Address3');
						checkAndWriteElement(w, 'Postcode');

					w.writeEndElement(); //end BusinessAddress - Subscription
				w.writeEndElement(); //end Address - Subscription
			
			    checkAndWriteElement(w, 'ManagedSerialNumbers');
			    
			w.writeEndElement(); //end SubscriptionData
		w.writeEndElement(); //end Subscription
    }
    
    private void checkAndWriteElement (XmlStreamWriter w, String element) {
        if (xmlDataMap.containsKey(element) && xmlDataMap.get(element) != NULL) {
            w.writeStartElement(null, element, null);
				w.writeCharacters(xmlDataMap.get(element));
			w.writeEndElement();
        }
    }
    
    private void storeXML (String xmlOutput) {
        csbtcl_Temporary_Data_Store_CS_JSON__c dataStoreObject;
        List<csbtcl_Temporary_Data_Store_CS_JSON__c> dataStoreObjects = [SELECT Id FROM csbtcl_Temporary_Data_Store_CS_JSON__c WHERE Opportunity__c = :opportunityId ORDER BY CreatedDate DESC];
        if (dataStoreObjects == NULL && dataStoreObjects.isEmpty()) {
            dataStoreObject = dataStoreObjects[0];
        } else {
            dataStoreObject = new csbtcl_Temporary_Data_Store_CS_JSON__c(Opportunity__c = opportunityId);
            INSERT dataStoreObject;
        }
        Attachment xmlAttachment = new Attachment();
        xmlAttachment.Body = Blob.valueOf(xmlOutput);
        //xmlAttachment.Name = 'Order for ' + oppName + '_' + 'XMLOuput.xml';
        xmlAttachment.Name = opportunityId + '_' + 'XMLOuput.xml';
        xmlAttachment.ContentType = 'CS XML';
        xmlAttachment.ParentId = dataStoreObject.Id;

        INSERT xmlAttachment;
    }
}