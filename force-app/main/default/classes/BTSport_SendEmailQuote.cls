global class BTSport_SendEmailQuote{  
   
    WebService static void SENDMAIL(string BT_Sport_Id, string Parent_Id, String Parent_Type ){    
        system.debug('BT_Sport_Id'+BT_Sport_Id);   
        system.debug('BT_Sport_Parent_Id'+Parent_Id);  
        system.debug('BT_Sport_Parent_Type'+Parent_Type);    
        
 
       
        
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();       
        
         
        String[] ccAddresses = new String[] {'btsportquote@9-1w2gtnmr5gywdji49is1sopdvo9dty6daij24r8z6ak0mfkt00.2-mnmteaw.2.apex.salesforce.com'}; // 
        
        If (Parent_Type == 'Lead'){
        List <Lead> LeadCon = [Select id From Lead where id = :Parent_Id]; 
        mail.setTargetObjectId(LeadCon[0].id);
        EmailTemplate et=[Select Id, Name ,HtmlValue, Encoding, FolderId, Body, Subject   From EmailTemplate where name='BT Sport nonMsa Quote Lead' limit 1];    
        mail.setTemplateId(et.id); 
                
        }
        else {
        Opportunity OppCon = [Select (Select Id, OpportunityId, ContactId, Role, IsPrimary From OpportunityContactRoles Limit 1) From Opportunity where id = :Parent_Id]; 
         mail.setTargetObjectId(OppCon.OpportunityContactRoles[0].contactId);  
        EmailTemplate et=[Select Id, Name ,HtmlValue, Encoding, FolderId, Body, Subject   From EmailTemplate where name='BT Sport nonMsa Quote' limit 1];    
        mail.setTemplateId(et.id);  
         
           
        }
        
              
         
            
        mail.setbCcAddresses(ccAddresses);
        mail.setUseSignature(false);
        mail.setOrgWideEmailAddressId('0D220000000CbxE');
        mail.setReplyTo('btsport.businesssales@bt.com');
        mail.saveAsActivity = false;    
        mail.setWhatId(BT_Sport_Id);
          
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
        
        Profile checkProfile;    
        checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];             
        try {      
        }      
        catch(exception e)      
        {         
            e.getMessage();      
        }
    }
}