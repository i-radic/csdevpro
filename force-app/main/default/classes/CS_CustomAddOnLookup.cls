global class CS_CustomAddOnLookup extends cscfga.ALookupSearch {
    
    
/*  public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        List<Account>
      accs = [SELECT Id, Website, Industry, Name, Type FROM Account WHERE id = :searchFields.get('A Custom Account ID')];
      return accs ;
    }
*/
    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;

    private String ADD_ON_OBJECT_NAME_CONST = 'cspmb__Add_On_Price_Item__c';
    private String PRODUCT_DEF_NAME = 'Mobile Voice';
     
    public override String getRequiredAttributes(){
        return '["Service Plan Parent", "Type", "Exclude Type", "Record Type", "Disable Roaming", "Name", "Number of users", "Managed MDM"]'; 
        /*
        added Parent NumberOfUsers
        by CloudSense on 2020-10-26 by Marko Marovic
        for filtering of returned Add On records based on number of connections, work related to adding Shared VAS "Flexible Workspace"
        
        added Managed MDM
        */
    }
        
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, 
        Integer pageOffset, Integer pageLimit){
        List<Id> addOnAssocId;
        List<cspmb__Price_Item_Add_On_Price_Item_Association__c> addOnAssoc;
        List <cspmb__Add_On_Price_Item__c> singleAddOnPriceItems;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
        
        
        addOnAssoc = [SELECT cspmb__Add_On_Price_Item__c FROM cspmb__Price_Item_Add_On_Price_Item_Association__c WHERE cspmb__Price_Item__c = :searchFields.get('Service Plan Parent')];
        addOnAssocId = new List <Id>();
  
        for(cspmb__Price_Item_Add_On_Price_Item_Association__c a : addOnAssoc){
            addOnAssocId.add(a.cspmb__Add_On_Price_Item__c);
        }
        
        String objectFields = CS_Utils.getSobjectFields(ADD_ON_OBJECT_NAME_CONST);
        if(objectFields == null){
            return null;
        }
        String addOnType = searchFields.get('Type');
        String excludeType = searchFields.get('Exclude Type');
        String recordType = searchFields.get('Record Type');
        String disableRoaming = searchFields.get('Disable Roaming');
        String addOnName = searchFields.get('Name');
        
        Boolean disableRoamingFlag = false;
        System.debug('** SIMO: ' + JSON.serializePretty(searchFields));
        
        if (disableRoaming != null && disableRoaming.contains('Roaming')){
            disableRoamingFlag = true;
        }
        System.debug('** Roaming: ' + disableRoamingFlag + '   ' + disableRoaming);
       
        List <String> typesToExcludeList = new List <String>();
        if (excludeType != null){
            typesToExcludeList = excludeType.split (',', 0);
        }
        Integer i = 0;
        
        while (i < typesToExcludeList.size()){
            if(typesToExcludeList.get(i).contains('Roaming') || typesToExcludeList.get(i).contains('Professional Services') || typesToExcludeList.get(i).contains('Voice')){
                typesToExcludeList.remove(i);
            } else if (typesToExcludeList.get(i).contains('SMS') && recordType.contains('Shared Add On')){
                typesToExcludeList.remove(i);
            }  else if (addOnName!= null && typesToExcludeList.get(i).contains(addOnType)){
                typesToExcludeList.remove(i);
            } else {
                i++;
            }   
        }
        
        if (disableRoamingFlag){
            typesToExcludeList.add('Roaming');
        }
        
        String searchTerm = searchFields.get('searchValue');
        if (string.isEmpty(searchTerm)) {
            searchTerm = '%';
        } else {
            searchTerm = '%' + searchTerm + '%';
        }

        String addOnRecordsQuery = 'SELECT '+ objectFields + ' FROM cspmb__Add_On_Price_Item__c WHERE cspmb__Is_Active__c = true AND cspmb__Product_Definition_Name__c = :PRODUCT_DEF_NAME  AND Type__c = :addOnType AND Id IN :addOnAssocId AND Record_Type_Name__c  = :recordType AND Type__c NOT IN :typesToExcludeList AND Name LIKE :searchTerm Order By Name ASC LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset';
        singleAddOnPriceItems = Database.query(addOnRecordsQuery);
        
        //return singleAddOnPriceItems;             <-- commented by Marko Marovic
        
        /* code added 
            by CloudSense on 2020-10-26 by Marko Marovic
            for filtering of returned Add On records based on number of connections, work related to adding Shared VAS "Flexible Workspace"
            
            2021-03-15 change of name "Flexible Workplace" to "Complete Mobility"
        
        */
        string addonNumberOfUsers = searchFields.get('Number of users');
        string managedMdm = searchFields.get('Managed MDM'); // Tier 1 === TIER_1
        
        // code added 2020-12-14 to fix the issue with Number of users not being set
        if (String.isBlank(addonNumberOfUsers))
                addonNumberOfUsers = '0';
        // end of code changes on 2020-12-14
        
        List <cspmb__Add_On_Price_Item__c> filteredSingleAddOnPriceItems = new List<cspmb__Add_On_Price_Item__c>();
        
        System.debug('**MM: entering MM code, Number of users is: ' + Integer.ValueOf(addonNumberOfUsers));
        
        for (cspmb__Add_On_Price_Item__c current : singleAddOnPriceItems) {
            
            if(current.Name.contains('Complete Mobility')){
                if(Integer.ValueOf(addonNumberOfUsers) >= current.Minimum_Connections__c && Integer.ValueOf(addonNumberOfUsers) <= current.Maximum_Connections__c){
                    filteredSingleAddOnPriceItems.add(current);
                }
            }else if(current.Name.contains('Managed MDM')) {
                if(current.cspmb__Add_On_Price_Item_Code__c.contains(managedMdm.toUpperCase().replace(' ', '_'))){
                    filteredSingleAddOnPriceItems.add(current);
                    System.debug('**Managed MDM added:  ' + current.Name);
                }
            }else{
                filteredSingleAddOnPriceItems.add(current);
            }
            
        }
        
        
        
        
        return filteredSingleAddOnPriceItems;
                
        /*
        end of added code by Marko Marovic
        */


    }
    
}