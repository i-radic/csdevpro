/**************************************************************************************************************************************************
    Test Class Name : Test_CustomerExpClientplanBefore
    Description : Code to Insert and Update Customer Experience client plan records. 
    Version : V0.1
    Created By Author Name : BALAJI MS
    Date : 08/09/2017
 *************************************************************************************************************************************************/

@isTest
private class Test_CustomerExpClientplanBefore {
    
    static testMethod void myUnitTest() {
         User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
     
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u1 = new User(
            alias = 'B2B18', email = 'B2B138cr@bt.com',
            emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
            languagelocalekey = 'en_US',
            localesidkey = 'en_US', ProfileId=prof.Id,
            timezonesidkey = 'Europe/London', username = 'B2B28Profile13167cr@testemail.com',
            EIN__c = 'OTHER_def'
        );
        insert u1;
        
        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 2';
        A.sac_code__c = 'testSAC';
        A.Sector_code__c = 'CORP';
        A.LOB_Code__c = 'LOB';
        A.OwnerId = u1.Id;
        A.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(A);
        try{
            Service_Improvement_Plan__c SIP = new Service_Improvement_Plan__c();
            SIP.Account__c = A.Id;
            insert SIP;
            Service_Improvement_Plan__c SIP2 = Test_Factory.CreateCustomerExperienceActionPlan(A.Id);
            insert SIP2;
        }
        catch(Exception exp){
            Boolean expectedExceptionThrown =  exp.getMessage().contains('You should only have one Customer Experience Plan per SAC') ? true : false;
            System.assertEquals(expectedExceptionThrown, True);
        }
        
         }
    }
    
    static testmethod void MedalliaTestMethod(){
         User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Account AccountRecord = Test_Factory.CreateAccount();
        insert AccountRecord;
        
        Service_Improvement_Plan__c CustomerExpActionRec = Test_Factory.CreateCustomerExperienceActionPlan(AccountRecord.Id);
        insert CustomerExpActionRec;
        
        CustomerExpActionRec.RAG__c = 'Amber';
        update CustomerExpActionRec;
    }
}