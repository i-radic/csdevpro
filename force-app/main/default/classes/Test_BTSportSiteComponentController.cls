@isTest(SeeAllData=true)
public class Test_BTSportSiteComponentController {
    
    static testMethod void myTest1()
    {
        Date uDate = date.today().addDays(7);
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BT Sport Test';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.Product_Family__c = 'BT Sport';
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty}); 
        
        
        BT_Sport_CL__c Header1 = new BT_Sport_CL__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport_CL__c [] {Header1});       
        
        //test.startTest();
        
        /*BT_Sport_Site__c Site1 = new BT_Sport_Site__c();
Site1.BT_Sport_CL__c = Header1Result[0].id; 
Site1.Product__c = 'BT Sport Total';
Site1.Site_Type__c = 'BT Sport Total Pub (GB)';
Site1.Contract_Type__c ='12 month triple-play bundle contract';//'12 month contract';
site1.Site_Code__c='SPOR-123';
site1.Site_Name__c='Test_s2s_10';
site1.Contract_Start_Date__c=Date.today();
site1.Contract_End_Date__c=Date.today().addDays(3);
site1.Broadband_Product__c='BT Business Fibre Sub 15';

//Site1.Band__c = 'Band A - £0 - £5,000';
//Site1.Discounts__c = 'First Month FREE';
Database.SaveResult[] Site1Result = Database.insert(new BT_Sport_Site__c [] {Site1});*/
        
        BT_Sport_Site__c Site1 = new BT_Sport_Site__c();
        Site1.BT_Sport_CL__c = Header1Result[0].id; 
        Site1.Product__c = 'BT Sport Total';
        Site1.Site_Type__c = 'BT Sport Total Pub (GB)';
        Site1.Contract_Type__c = '24 Month Bundle ';
        Site1.Band__c = 'Band A - £0 - £5,000';
        //Site1.Discounts__c = 'First Month FREE';
        site1.Site_Code__c='SPOR-123';
        site1.Site_Name__c='Test_s2s_10';
        site1.Contract_Start_Date__c=Date.today();
        site1.Contract_End_Date__c=Date.today().addDays(3);
        site1.Building_Name__c='build';
        site1.Building_Number__c='b123';
        site1.Street__c='Grorge street';
        site1.Town__c='Glasgow';
        site1.County__c='Lanarshire';
        site1.Rosita_ref__c='RosiRef';
        site1.Best_Contact_Date_Time__c='anytime';
        
        site1.Music_for_Business__c='Required';
        site1.Music_for_Business_Contract__c='12 Month';
        site1.Music_for_Business_Order_Number__c='BT234562';
        site1.Music_for_Business_Offer__c='12 Months Free with Free Activation';
        
        site1.PSTN_Product__c = 'BT Business Critical Line';
        site1.PSTN_Contract__c = '12 Month';
        site1.PSTN_Offer__c = 'Free PSTN Line with BT Sport 12 Months';
                
        site1.Broadband_Product__c = 'BT Business Infinity Unlimited';
        site1.Broadband_Bundle_Offers__c = '12 Months Free with BT Sport';        
        
        site1.BT_Sport_Wi_Fi__c = 'Required';
        site1.BT_Sport_Wi_Fi_Contract__c = '24 Month';
        site1.BT_Sport_Wi_Fi_Offer__c = '12 Months Free with Free Activation';
        site1.BT_Sport_Wi_Fi_Order_number__c ='BT123RTF';
        
        Database.SaveResult[] Site1Result = Database.insert(new BT_Sport_Site__c [] {Site1});
        
        BTSportSiteComponentController bsc= new BTSportSiteComponentController();
        bsc.type='Quote';
        bsc.btSportId=site1.Id;
        bsc.custName='Test';
        bsc.sName='Test';
        bsc.getData();
    }
    static testMethod void myTest2()
    {
        //StaticVariables.setAccountDontRun(False);  
        Account acc1 = Test_Factory.CreateAccount(); // a SAC Level Account to link to
        acc1.OwnerId = UserInfo.getUserId();
        /*acc1.Sector__c = 'BT Local Business';
acc1.LOB_Code__c = 'lob999';
acc1.SAC_Code__c = 'aSac999';
acc1.LE_Code__c = '';
acc1.AM_EIN__c = '802537216';
acc1.Sub_Sector__c = 'ssTest';*/
        Database.SaveResult accountResult = Database.insert(acc1);
        
        
        //Creation of Contact
        Contact con1 = Test_Factory.CreateContact();
        con1.Cug__c = 'cugAlan1';
        con1.Contact_Post_Code__c = 'WR5 3RL';       
        insert con1;  
        
        //Creation of Opportunity
        
        Opportunity oppS2S = Test_Factory.CreateOpportunity(accountResult.getid());
        oppS2s.Name='Test_BTSportSite';
        oppS2S.closedate = system.today();
        oppS2S.Order_Reference__c='ABCD1234';
        oppS2S.StageName='Won';
        Database.SaveResult opptResult = Database.insert(oppS2S);
        
        //Creation of OpportunityContactRole
        
        OpportunityContactRole OppContRole= new OpportunityContactRole();
        OppContRole.ContactId=con1.Id;//'003g000000KKx17';
        OppContRole.OpportunityId=oppS2S.id;//'006g0000006uVC5';
        insert OppContRole;
        
        //Insertion of BTSportCL Object        
        BT_Sport_CL__c SportCL= new BT_Sport_CL__c();
        SportCL.Opportunity__c=oppS2S.id;//'006g0000006uVC5';
        insert SportCL;
        
        //Insertion of BTSportPricing Object
        
        BT_Sport_Pricing__c sportPrice= new BT_Sport_Pricing__c();
        sportPrice.Discount_Period_End_Date__c=Date.newInstance(2016,08,31);
        sportPrice.Discount_Period_Months__c=3;
        sportPrice.Discount_Amount__c=10;
        sportPrice.Discount_Name__C='35 percent off for 3 months';
        //sportPrice.Entry_Type__c='Discount';
        sportPrice.Start_Date__c=Date.newInstance(2015,04,10);
        sportPrice.End_Date__c=Date.newInstance(2020,05,12);
        sportPrice.Discount_Period__c=1;
        insert sportPrice;
        
        //Insertion of BTSportSite Object
        //
        BT_Sport_Site__c site1= new BT_Sport_Site__c();
        site1.Product__c='BT Sport Total';
        site1.Site_Type__c='BT Sport Total Pub (GB)';
        site1.Contract_Type__c='12 month triple-play bundle contract';
        site1.Band__c='Band A - £0 - £5,000';
        site1.BT_Sport_CL__c=SportCL.Id;
        site1.Order_Date__c=System.today();
        site1.Building_Number__c='123';
        site1.County__c='Worcs';
        site1.Post_Code__c='WR11 5QR';
        //site1.Discounts__c='35 percent off for 3 months';
        site1.Street__c='arrow lane';
        site1.Town__c='evesham';
        site1.Site_Type_S2__c='BT Sport Pack Pub (GB)';
        site1.Site_Type__c='BT Sport Total Pub (GB)';
        //site1.Discounts_S2__c='First Month FREE';
        site1.Site_Display_Name__c='Order1';
        site1.Site_Code__c='SPOR-123';
        site1.Site_Name__c='Test_s2s_10';
        site1.Contract_Start_Date__c=Date.today();
        site1.Contract_End_Date__c=Date.today().addDays(3);
        insert site1;
        
        
        /*BT_Sport_Site__c Site1 = new BT_Sport_Site__c();
Site1.BT_Sport_CL__c = SportCL.id; 
Site1.Product__c = 'BT Sport Total';
Site1.Site_Type__c = 'BT Sport Total Pub (GB)';
Site1.Contract_Type__c ='12 month triple-play bundle contract';//'12 month contract';
site1.Site_Code__c='SPOR-123';
site1.Site_Name__c='Test_s2s_10';
site1.Contract_Start_Date__c=Date.today();
site1.Contract_End_Date__c=Date.today().addDays(3);
site1.Broadband_Product__c='BT Business Fibre Sub 15';
insert Site1;*/
        
        BTSportSiteComponentController bsc= new BTSportSiteComponentController();
        bsc.type='Order';
        bsc.btSportId=site1.Id;
        bsc.getData();
    }
}