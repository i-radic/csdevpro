@isTest
private class BSClassicSLRecapTest {
    static testMethod void TestEmailCases() {
    Test_Factory.SetProperty('IsTest', 'yes');
      	User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }
        Account  acc=Test_Factory.CreateAccount();
        acc.Name='Test';
        acc.Sector__c = 'other';
        acc.LOB_Code__c = 'TEST';
        acc.SAC_Code__c = 'LATO1231';
        acc.LE_Code__c = null;
        acc.AM_EIN__c = '802537216';
        acc.postcode__c = 'WR5 3RL';
        acc.Base_Team_Assign_Date__c = Date.today();
        insert acc;
        
        BS_Classic_Sales_log__c bsClassic=new BS_Classic_Sales_log__c();
        bsClassic.Call_Outcome__c='Quote Sent';
        bsClassic.Account__c=acc.Id;
        insert bsClassic;
        
        BSCLassicEmail__c blsEmail_1=new BSCLassicEmail__c();
        blsEmail_1.email_codes__c='LN10LN20LN25';
        blsEmail_1.type__c='product'; 
        blsEmail_1.Product__c='PSTN 2 Year Resign';
        blsEmail_1.Email_Text__c = 'Morning';
        insert blsEmail_1;

        BSCLassicEmail__c blsEmail=new BSCLassicEmail__c();
        blsEmail.email_code__c='LN10';
        blsEmail.type__c='email'; 
        blsEmail.Email_Text__c = 'Morning appointments are usually between 09:00 and 13:00 and afternoon appointments';
        insert blsEmail;
       
        BSCLassicEmail__c blsFooter=new BSCLassicEmail__c();
        blsFooter.email_code__c='LN20';
        blsFooter.type__c='footer'; 
        blsFooter.Email_Text__c = 'footer statement';
        insert blsFooter;
                
        BS_Classic_SL_Product__c bls=new BS_Classic_SL_Product__c();
        bls.Product__c='PSTN 2 Year Resign';
        bls.Sales_Log__c=bsClassic.id;
        bls.Product__c='PSTN 2 Year Resign';
        bls.Product_Type2__c='Broadband';
        bls.Quantity__c=1;
        bls.Sales_Log__c=bsClassic.id;
        insert bls;
        
        List<BSCLassicEmail__c>listbs=new List<BSCLassicEmail__c>();
        //EmailTemBSClassicSLProduct con=new EmailTemBSClassicSLProduct();
        BSClassicSLProductEmail con=new BSClassicSLProductEmail();
        con.SLProductid = bsClassic.id;
        con.getBSClassicSLProductTable();
        con.getBSCLassicEmailStatements();
        con.getEmailFooter();
        
        ApexPages.currentPage().getParameters().put('Id',bsClassic.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(bsClassic);
        BSClassicSLRecapCtrl controller = new BSClassicSLRecapCtrl(sc);
        controller.getProductList();  
        controller.getProductList2(); 
        controller.reasonCapture();
        controller.save();
        
        
     }

}