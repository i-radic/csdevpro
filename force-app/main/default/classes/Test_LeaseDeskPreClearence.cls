@isTest(SeeAllData=true)
private class Test_LeaseDeskPreClearence {

    static testMethod void UnitTest() {
        RunTest('yes', '11', '1', '1');
        RunTest('yes2', '22', '1', '2');
        RunTest('No', '33', null, null); 
    }
    
    static void RunTest(String isTest, String uniqueVal, String totalCost, String ldID) {
            
        Test_Factory.SetProperty('IsTest', isTest);
        // create user
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'BTLB: Admin User']; 
        
        User u1 = new User();
        u1.Username = 'test9874' + uniqueVal + '@bt.com';
        u1.Ein__c = '9876543' + uniqueVal;
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '1234567' + uniqueVal;
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.ProfileId = prof.Id;
        u1.TimeZoneSidKey = 'Europe/London'; 
        u1.LocaleSidKey = 'en_GB';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'en_US';
        u1.Apex_Trigger_Account__c = false; 
        u1.Run_Apex_Triggers__c = false;
        u1.Leasing_iManage_Username__c = 'test';
        insert u1; 

        // create an account
        Account a1 = Test_Factory.CreateAccount();
        a1.Name = 'LD Account Test ' + isTest;
        a1.OwnerId = U1.Id;
        a1.Postcode__c = 'n16 9al';
        a1.BTLB_Common_Name__c = 'BTLB Bath and Bristol Test';
        a1.LOB__c = 'BTLB Bath and Bristol Test';
        a1.Sector__c = 'BT Local Business Test';
        a1.Sub_Sector__c = 'BTLB Bath and Bristol Test';
        a1.Sector_Code__c = 'BTLB Test';
        //a1.LeaseDeskID__c = ldID;
        insert a1;
        
        // Create btlb master object
        BTLB_Master__c bm = new BTLB_Master__c();
        //bm.Name = 'BTLB Bath and Bristol Test';
        //bm.Account_Owner__c = u1.Id;
        bm.BTLB_Name_ExtLink__c = uniqueVal;
        bm.Funders__c = 'Shire';
        insert bm;
        
        
        // create a standard opportunity
        Opportunity opp = Test_Factory.CreateOpportunity(a1.Id);
        opp.RecordTypeId = '01220000000PjDu';       // Standard record type
        opp.Type ='Sale';
        upsert opp; 
        
        List<LeaseDeskAPI.QuickDecisionResponse> qdecisionResponseList = new List<LeaseDeskAPI.QuickDecisionResponse>();
        LeaseDeskAPI.QuickDecisionResponse qdResponse1 = new LeaseDeskAPI.QuickDecisionResponse();
        qdResponse1.ragStatus = 'Green';
        qdResponse1.proposalID = 'TEST1';
        qdResponse1.creditLimit = 500;
        qdResponse1.Funder = 'GE'; 
        qdResponse1.ErrorType = 'Internal-Retryable';
        qdResponse1.ErrorCode = 'TEST';  
        qdResponse1.ErrorText = 'TEST';
        qdecisionResponseList.Add(qdResponse1);
        
        LeaseDeskAPI.QuickDecisionResponse qdResponse2 = new LeaseDeskAPI.QuickDecisionResponse();
        qdResponse2.ragStatus = 'Amber';
        qdResponse2.proposalID = 'TEST2';
        qdResponse2.creditLimit = 500;
        qdResponse2.Funder = 'Shire'; 
        qdResponse2.ErrorType = 'Internal-Retryable';
        qdResponse2.ErrorCode = 'TEST';  
        qdResponse2.ErrorText = 'TEST';
        qdecisionResponseList.Add(qdResponse2);
        
        LeaseDeskAPI.QuickDecisionResponse qdResponse3 = new LeaseDeskAPI.QuickDecisionResponse();
        qdResponse3.ragStatus = 'Red';
        qdResponse3.proposalID = 'TEST3';
        qdResponse3.creditLimit = 500;
        qdResponse3.Funder = 'CIT'; 
        qdResponse3.ErrorType = 'Internal-Retryable';
        qdResponse3.ErrorCode = 'TEST';  
        qdResponse3.ErrorText = 'TEST';
        qdecisionResponseList.Add(qdResponse3);
        
        LeaseDeskAPI.QuickDecisionResponse qdResponse4 = new LeaseDeskAPI.QuickDecisionResponse();
        qdResponse4.ragStatus = 'Green';
        qdResponse4.proposalID = 'TEST4';
        qdResponse4.creditLimit = 500;
        qdResponse4.Funder = 'DLL'; 
        qdResponse4.ErrorType = 'Internal-Retryable';
        qdResponse4.ErrorCode = 'TEST';  
        qdResponse4.ErrorText = 'TEST';
        qdecisionResponseList.Add(qdResponse4);
        
        ApexPages.currentPage().getParameters().put('opptyid', opp.Id);        
        ApexPages.StandardController stdController = new ApexPages.StandardController(opp);
        LeaseDeskPreClearanceController preClearance = new LeaseDeskPreClearanceController(stdController);
        preClearance.quickDecisionResponseList = qdecisionResponseList;
        preClearance.acct = a1;
        preClearance.user = u1;
        preClearance.setDisplayBackButton(true);
        preClearance.setSelectedContractTerm('1');
        preClearance.setTotalCost(totalCost);
        preClearance.displayPreClearance();
        preClearance.performQuickDecision();
        preClearance.back();
        preClearance.getSelectedContractTerm();
        preClearance.getDisplayBackButton();
        preClearance.getTotalCost();
        preClearance.LeaseDeskPreClearanceController(); 
    }
}