/**************************************************************************************************************************************************
    Test Class Name : CustomerExperienceActionHelper_Test
    Description : Code to Insert and Update Customer Experience Action, And to Calculate RollUp Summary, to Update in Survey Response Object Record
    Version : V0.1
    Created By Author Name : Bikash Kumar Gupta
    Date : 08/09/2017
 *************************************************************************************************************************************************/

@IsTest
public class CustomerExperienceActionHelper_Test {
    
    public static testmethod void CustomerExperienceActionHelper(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
		System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        
        User userData = Test_Factory.CreateUser();
        userData.email = 'abc@bt.it';
        userData.username = 'abc@bt.it';
        insert userData;
        
        Account accountData = Test_Factory.CreateAccount();
        insert accountData;
        Contact contactData = Test_Factory.CreateContactwithAccount(accountData.Id);
        Insert ContactData;
        Service_Improvement_Plan__c CustomerExperiencePlan = Test_Factory.CreateCustomerExperienceActionPlan(accountData.Id);
        CustomerExperiencePlan.RAG__c = 'Red';
        Insert CustomerExperiencePlan;
        CSAT_Contact_Feedback__c CSATContactFeedback = Test_Factory.CreateCSATContactFeedback(ContactData.Id);
        CSATContactFeedback.RecordTypeId = Schema.SObjectType.CSAT_Contact_Feedback__c.getRecordTypeInfosByName().get('Corporate Touch Point').getRecordTypeId();
        Insert CSATContactFeedback;
        Customer_Experience_Actions__c customerExperience = Test_Factory.CreatecustomerExperience(CustomerExperiencePlan.Id, CSATContactFeedback.Id, userData.Id, ContactData.Id);
        customerExperience.Open_Closed__c = 'Open';
        Insert customerExperience;
        
        customerExperience.Open_Closed__c = 'Closed';
        Update customerExperience;
        String RecName = customerExperience.Name;
        Delete customerExperience;
        
        Customer_Experience_Actions__c[] customerExperienceRec = [Select Name from Customer_Experience_Actions__c where Name =: RecName all rows];
        Undelete customerExperienceRec;
        }
    }
    
}