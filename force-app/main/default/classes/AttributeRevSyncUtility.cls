/* *************************************************************************************************************    
Class Name    : AttributeRevSyncUtility
Description   : Utility Call for Attribte Sync                      
CreatedDate   : 12-OCt-2018       
Version       : 1.0     
Author        : TCS(Sunny Kashyap)
-----------------------  Revision History -------------------

Sno      Version      Modification Done     Modified By    Modified Date                  
 1. 
 2.
*****************************************************************************************************************/


public class AttributeRevSyncUtility {
    
    /************************************************************************************************
     * CR10478
    Method Name            : doAttributeRevSync
    Input Parameters       : list of attribute Id where reverseSyncrequired is true
    Return Type            : void
     Method Description     : This Method will be called frorm Process Builder on Attribute object witch  value needs yo be updated on related child object.
    
   ***********************************************************************************************/
    
    @invocableMETHOD
    public static void doAttributeRevSync(List<id> attributeIdList){
        //String JSON_SRC='{\r\n\"attributeSyncJSONParserDataList\":[{\r\n\"isactive\":\"true\",\r\n\"attname\": \"Contract Term\",\r\n\"objectname\": \"BT_One_Phone__c\",\r\n\"mode\": \"AttrbuteSync\",\r\n\"fieldname\": \"Contract_Term__c\",\r\n\"sourcefielddatatype\": \"double\",\r\n\"lookupFieldonObject\": \"Opportunity__c\",\r\n\"relationship\":\"indirect\",\r\n\"relatedObject\": \"Opptunity\",\r\n\"attlookupForRelatedObject\": \"cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c\",\r\n\"filedlookupForRelatedObject\": \"Opptunity\"\r\n},\r\n{\r\n\"isactive\":\"true\",\r\n\"attname\": \"Contract Term\",\r\n\"objectname\": \"Opportunity\",\r\n\"mode\": \"AttrbuteSync\",\r\n\"fieldname\": \"Contract_Number__c \",\r\n\"sourcefielddatatype\": \"string\",\r\n\"lookupFieldonObject\": \"id\",\r\n\"relationship\":\"direct\",\r\n\"relatedObject\": \"Opptunity\",\r\n\"attlookupForRelatedObject\": \"cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c\",\r\n\"filedlookupForRelatedObject\": \"Opptunity\"\r\n}]\r\n}';
        String JSON_SRC=getStaticResourceByName('AttSyncJson');
        List<cscfga__Attribute__c> attributeDataList = [select id,Name,cscfga__Display_Value__c,cscfga__Value__c from cscfga__Attribute__c where id in: attributeIdList];
        Map<String,String> attribteDataMap = new Map<String,String>();
        Map<String,cscfga__Attribute__c> attribteNameAttRecordMap = new Map<String,cscfga__Attribute__c>();
        Map<string, string> attNameLookupForRelatedObjectMap = new Map<string, string>();
        Set<String> attlookupFieldForRelatedObjectSet = new Set<String>();
        Map<String,String> attNameObjectMap = new Map<String,String>();
        Map<String,String> filedNameAttNameLookup = new Map<String,String>();
        Map<String,String> filedNamedataTypeMap = new Map<String,String>();
        
        Map<String,String> attNameLookupObjectIdMap = new Map<String,String>(); 
        
        Set<String> objectnameSet = new Set<String>();
        Map<string, string> objectNameAttNameMap = new Map<string, string>();
        Map<string, string> ojectNameLookupFieldMap = new Map<string, string>();
        Map<string, Set<string>> objNameFiledSetMap = new Map<string, Set<string>>();
        try{
            for(cscfga__Attribute__c att : attributeDataList){
                attribteDataMap.put(att.Name.trim().tolowercase(), att.cscfga__Value__c);
                attribteNameAttRecordMap.put(att.Name.trim().tolowercase(), att);
            }
            System.debug('attribteDataMap>>>'+attribteDataMap);
            AttributeSyncJSONParser attributeSyncJSONParser =  (AttributeSyncJSONParser)System.JSON.deserialize(JSON_SRC, AttributeSyncJSONParser.class);
            //List <AttributeSyncJSONParser> attributeSyncJSONParserDataList = new List<AttributeSyncJSONParser> {(AttributeSyncJSONParser)System.JSON.deserialize(JSON_SRC, AttributeSyncJSONParser.class)};
            List <AttributeSyncJSONParser.AttributeSyncJSONParserDataList> attributeSyncJSONParserDataList = attributeSyncJSONParser.attributeSyncJSONParserDataList;
            System.debug('attributeSyncJSONParserDataList>>>'+attributeSyncJSONParserDataList);
            for(AttributeSyncJSONParser.AttributeSyncJSONParserDataList attributeSyncJSONParserObj : attributeSyncJSONParserDataList){
                if(attributeSyncJSONParserObj.isactive.equalsIgnoreCase('true')){
                    attlookupFieldForRelatedObjectSet.add(attributeSyncJSONParserObj.attlookupForRelatedObject.trim().tolowercase());
                    attNameLookupForRelatedObjectMap.put(attributeSyncJSONParserObj.attname.trim().tolowercase(),attributeSyncJSONParserObj.attlookupForRelatedObject.trim().tolowercase());
                    attNameObjectMap.put(attributeSyncJSONParserObj.attname.trim().tolowercase(),attributeSyncJSONParserObj.relatedObject.trim().tolowercase());  
                    filedNameAttNameLookup.put(attributeSyncJSONParserObj.fieldname.trim().tolowercase(),attributeSyncJSONParserObj.attname.trim().tolowercase());  
                    filedNamedataTypeMap.put(attributeSyncJSONParserObj.fieldname.trim().tolowercase(),attributeSyncJSONParserObj.sourcefielddatatype.trim().tolowercase());  
                    
                    System.debug('attlookupFieldForRelatedObjectSet>>>'+attlookupFieldForRelatedObjectSet);
                    System.debug('attNameLookupForRelatedObjectMap>>>'+attNameLookupForRelatedObjectMap);
                    System.debug('attNameObjectMap>>>'+attNameObjectMap);
                    
                    objectnameSet.add(attributeSyncJSONParserObj.objectname.trim().tolowercase());
                    objectNameAttNameMap.put(attributeSyncJSONParserObj.objectname.trim().tolowercase(),attributeSyncJSONParserObj.attname.trim().tolowercase());
                    ojectNameLookupFieldMap.put(attributeSyncJSONParserObj.objectname.trim().tolowercase(),attributeSyncJSONParserObj.lookupFieldonObject.trim().tolowercase());
                    if(objNameFiledSetMap.containsKey(attributeSyncJSONParserObj.objectname.trim().tolowercase())){
                        Set<String> tempFieldSet = objNameFiledSetMap.get(attributeSyncJSONParserObj.objectname.trim().tolowercase());
                        tempFieldSet.add(attributeSyncJSONParserObj.fieldname.trim().tolowercase());
                        objNameFiledSetMap.put(attributeSyncJSONParserObj.objectname.trim().tolowercase(),tempFieldSet);               
                    }else{
                        objNameFiledSetMap.put(attributeSyncJSONParserObj.objectname.trim().tolowercase(),new Set<String> {attributeSyncJSONParserObj.fieldname.trim().tolowercase()});  
                    }
                }
                
                System.debug('objectnameSet>>>'+objectnameSet);
                System.debug('objectNameAttNameMap>>>'+objectNameAttNameMap);
                System.debug('ojectNameLookupFieldMap>>>'+ojectNameLookupFieldMap);
                System.debug('objNameFiledSetMap>>>'+objNameFiledSetMap);
            }
            
            attNameLookupObjectIdMap = getRelatedObjectDetails(attributeIdList,attlookupFieldForRelatedObjectSet, attNameLookupForRelatedObjectMap);
            
            updateObjectDetails(objectnameSet,attribteDataMap,attribteNameAttRecordMap, attNameLookupObjectIdMap,filedNameAttNameLookup,filedNamedataTypeMap,objNameFiledSetMap,ojectNameLookupFieldMap,objectNameAttNameMap);
        }catch(Exception e){
            System.debug('Exception accrued:::::'+e);
        }
    }
    public static Map<String,String> getRelatedObjectDetails(List<id> attributeIdList,Set<String> attlookupFieldForRelatedObjectSet, Map<String,String> attlookupForRelatedObjectMap){
        Map<String,String> attNameObjectIdMap = new Map<String,String> ();
        String soqlfields ='', soqlquery='',filedName='';
        try{    
           
            if(!attlookupFieldForRelatedObjectSet.isEmpty()){
                attlookupFieldForRelatedObjectSet.add('id');
                attlookupFieldForRelatedObjectSet.add('Name');
                soqlfields = convertsettostring(attlookupFieldForRelatedObjectSet);
                soqlquery = 'select '+soqlfields+' from cscfga__Attribute__c where id in : attributeIdList';
                System.debug('soqlquery>>'+soqlquery);
                List<cscfga__Attribute__c> attbutedataList = (List<cscfga__Attribute__c>)Database.query(soqlquery);
                System.debug('attbutedataList>>'+attbutedataList);
                if(!attbutedataList.isEmpty()){
                    for(cscfga__Attribute__c attObj : attbutedataList){
                        System.debug('Test::::'+attObj.cscfga__product_configuration__r.cscfga__product_basket__r.cscfga__opportunity__c);
                        System.debug('attlookupForRelatedObjectMap>>'+attlookupForRelatedObjectMap);
                        System.debug('attObj.Name.trim().tolowercase()>>'+attObj.Name.trim().tolowercase());
                        if(!attlookupForRelatedObjectMap.isEmpty()){
                            filedName= attlookupForRelatedObjectMap.get(attObj.Name.trim().tolowercase());
                        }
                        System.debug('filedName>>'+filedName);
                        if(String.isNotBlank(filedName)){
                            String tempFieldValue = returnFieldValue(attObj,filedName);
                            System.debug('tempFieldValue>>'+tempFieldValue);
                            attNameObjectIdMap.put(attObj.Name.trim().tolowercase(),tempFieldValue);
                        }
                    }
                }
                System.debug('attNameObjectIdMap>>'+attNameObjectIdMap);
            }
        }catch(Exception e){
            System.debug('Exception accrued:::::'+e);
        }
        return attNameObjectIdMap;
    }
    
    public static Map<String,String> updateObjectDetails(Set<String> objectnameSet, Map<String,String> attribteDataMap,Map<String,cscfga__Attribute__c> attribteNameAttRecordMap, Map<String,String> attNameLookupObjectIdMap, Map<String,String> filedNameAttNameLookup, Map<String,String> filedNamedataTypeMap,Map<String,Set<String>> objNameFiledSetMap, Map<String,String> ojectNameLookupFieldMap, Map <String,String> objectNameAttNameMap){
        Map<String,String> attNameObjectIdMap = new Map<String,String> ();
        map<string, sobject> sObjectNamesobjectmap = new map<string, sobject>();
        List<sobject> listToUpdate = new List<sobject>();
        String soqlfields ='', soqlquery='';
        try{            
            if(!objectnameSet.isEmpty()){
                for(String sObjName : objectnameSet){
                    soqlfields = convertsettostring(objNameFiledSetMap.get(sObjName));
                    soqlquery = 'select  '+soqlfields+' from '+sObjName+' where '+ojectNameLookupFieldMap.get(sObjName)+' = \''+ attNameLookupObjectIdMap.get(objectNameAttNameMap.get(sObjName))+ '\'';
                    system.debug('soqlquery>>'+soqlquery);
                    list<sobject> QueryResult = Database.query(soqlquery);
                    system.debug('QueryResult>>'+QueryResult);
                    system.debug('QueryResult.size>>'+QueryResult.size());
                    if (QueryResult.size() > 0)
                    {
                        sObjectNamesobjectmap.put(sObjName.trim().tolowercase(), QueryResult[0]);
                    }    
                }
                system.debug('sObjectNamesobjectmap>>'+sObjectNamesobjectmap);          
            }
            if(!sObjectNamesobjectmap.isEmpty()){
                for(String sObjName : sObjectNamesobjectmap.keySet()){
                    
                    //get all field fr the objectNameAttNameMap
                    sObject currentObject = null;
                    Set<String> fieldSet= objNameFiledSetMap.get(sObjName);
                    for(String field : fieldSet){
                        String curFiledValue= attribteDataMap.get(filedNameAttNameLookup.get(field));
                        system.debug('field>>'+field);  
                        system.debug('curFiledValue>>'+curFiledValue); 
                        if(String.isNotBLank(curFiledValue)){
                            currentObject = sObjectNamesobjectmap.get(sObjName);
                            system.debug('currentObject if 2>>'+currentObject);  
                            if(String.isNotBLank(filedNamedataTypeMap.get(field)) && filedNamedataTypeMap.get(field).equalsIgnoreCase('double')){
                                currentObject.put(field,decimal.valueOf(curFiledValue));
                            }else{
                                currentObject.put(field,curFiledValue);
                            }
                             system.debug('currentObject if 2>>'+currentObject);  
                        }else{
                            curFiledValue= returnFieldValue(sObjectNamesobjectmap.get(sObjName), field);
                            system.debug('curFiledValue else >>'+curFiledValue); 
                            if(String.isNotBLank(curFiledValue)){
                                currentObject = attribteNameAttRecordMap.get(filedNameAttNameLookup.get(field));
                                system.debug('currentObject else 1 >>'+currentObject);  
                                currentObject.put('cscfga__Value__c',curFiledValue);
                                currentObject.put('cscfga__Display_Value__c',curFiledValue);
                                system.debug('currentObject else 2>>'+currentObject);  
                            }
                        }
                        
                    }
                    if(currentObject != null){
                        listToUpdate.add(currentObject);
                    }
                }           
            }
            system.debug('listToUpdate 22>>'+listToUpdate);
            if(!listToUpdate.isEmpty()){
                update listToUpdate;
            }
        }catch(Exception e){
           System.debug('Exception accrued:::::'+e);
        }
        return attNameObjectIdMap;
    }
    
    public static String returnFieldValue(sObject sobj, String fieldName){
        try{
            if(fieldName.contains('.')){
                List<String> fieldsList = fieldName.split('\\.');
                String fieldValue = '';
                if(fieldsList.size() == 6){
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]).getSObject(fieldsList[4]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]).getSObject(fieldsList[4]).get(fieldsList[5]));
                }
                else if(fieldsList.size() == 5){
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).getSObject(fieldsList[3]).get(fieldsList[4]));
                }
                else if(fieldsList.size() == 4){
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).getSObject(fieldsList[2]).get(fieldsList[3]));
                }
                else if(fieldsList.size() == 3){
                    if(sobj.getSObject(fieldsList[0]) != null && sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).getSObject(fieldsList[1]).get(fieldsList[2]));
                }
                else if(fieldsList.size() == 2){
                    if(sobj.getSObject(fieldsList[0]) != null)
                        fieldValue = String.valueOf(sobj.getSObject(fieldsList[0]).get(fieldsList[1]));
                }
                return fieldValue != null ? fieldValue : '';
            } else {
                return sobj.get(fieldName) != null ? String.valueOf(sobj.get(fieldName)) : null;
            }
        }catch(Exception ex){
            system.debug('Exception Message: '+ ex.getMessage());
            return 'Default Value';
        }
    }  
    
    public static string convertsettostring(set<string> StringList)
    {
        try{
            String commaSepratedList='';
            for(String str : StringList)
            {
             commaSepratedList += str + ',' ;
            }
             
            // remove last additional comma from string
            commaSepratedList = commaSepratedList.subString(0,commaSepratedList.length()-1);
            return commaSepratedList;
        }catch(Exception e){
            System.debug('Exception accrued:::::'+e);
        }
        return '';
    }
    public static String getStaticResourceByName(String fileName){
        String allcontents ='';
        trY{
            StaticResource sr= [select id,body from StaticResource Where Name = :fileName];
            allcontents = sr.body.toString();
            system.debug('SR@@@'+allcontents );
        }catch(Exception e){
            
        }
        return allcontents;
    }

}