/**************************************************************************************************************************************************
Class Name : BatchBTUserGroupMember
Test Class Name : Test_BatchBTUserGroupMember
Description : Code to Insert Bt User Group Member records based on User department and Bt User Group name.
Version : V0.1
Created By Author Name : Phanikanth P
Date : 15/02/2019
Modified Date : 
*************************************************************************************************************************************************/

global class BatchBTUserGroupMember implements Database.Batchable<sObject>, schedulable{
    global string query;
    Map<string,BT_User_Group__c> btUGroup=new Map<string,BT_User_Group__c>();
    Map<id,id> btUser_GrpMemIDs = new Map<id,id>();
    Map<id,user> usrNotExists = new Map<id,user>();
    List<BT_User_Group_Member__c> btUserGrpMemList= new List<BT_User_Group_Member__c>();
    
    global BatchBTUserGroupMember(){
        
        For(BT_User_Group__c btUG:[select id, name, Active__c from BT_User_Group__c where Active__c=true]){
            btUGroup.put(btUG.name,btUG);
        }
        For(BT_User_Group_Member__c btUGMem: [select id, name, BT_User_Group__c,BT_User_Group__r.name, User__c, User_Profile_Id__c, User_Profile_Name__c from BT_User_Group_Member__c]){
            btUser_GrpMemIDs.put(btUGMem.user__c,btUGMem.BT_User_Group__c);
        }
        
    }
    global database.QueryLocator start(Database.BatchableContext bc){
        if(Test.isRunningTest()){
            query='select id, Department, Division, Name, ProfileId, Username from user where isActive=true AND lastname like \'Testing%\'';
        } else{
            query='select id, Department, Division, Name, ProfileId, Username from user where isActive=true';
        }
        
        return database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext bc, List<sObject> scope){
        List<user> userList=(List<user>)scope;
        system.debug('UserGroup----'+btUGroup.size());
        if(btUGroup.size()>0){
            for(User ul : userList){
                system.debug('**********Test'+ul);
                if(!btUser_GrpMemIDs.containskey(ul.id)){                    
                    if(btUGroup.containskey(ul.Department)){
                        btUserGrpMemList.add(new BT_User_Group_Member__c(User__c=ul.id,BT_User_Group__c=btUGroup.get(ul.Department).id)); 
                    }
                }
            }
            
        }
        if(btUserGrpMemList.size()>0){
            Database.insert(btUserGrpMemList, false);
        }
    }
    global void finish(Database.BatchableContext bc){
    }
    
    global void execute(SchedulableContext sc)
    {
        BatchBTUserGroupMember bt = new BatchBTUserGroupMember();
        Database.executebatch(bt);
    }
    
}