/*
Client      : BT
Author      : Krupakar J
Date        : 09/02/2011 (dd/mm/yyyy)
Description : This class will get Battleplan details to populate in the visual force page. 
The visualforce page is rendered in PDF format for printing purpose.
*/

public class battlePlanExtension {
    
    // Public Variables to access in Visualforce Page
    public Battleplan__c bp {get;set;}
    public Battleplan__c bpWithRelated {get;set;}
    public List<Opportunity> bpOppList;
    public List<Account> bpAccnts;
    public List<Battleplan_Competitor__c> bpComps;
    public List<OpenActivity> bpOpActs;
    public List<Battleplan_Note__c> bpNotes;
    public List<Battleplan_Customer_Contact__c> bpConts;
    public List<ActivityHistory> bpActsHty;
    public List<Battleplan_Sales_Strategy__c> bpStrats;
    public List<Battleplan_Sales_Tactic_Result__c> bpTResults;
    
    // Controller to Initiate and get Battle Plan Record
    public battlePlanExtension(ApexPages.StandardController con) {
    	//Initialize Variables
    	initVariables();
       //Populate Battle Plan Record     
       bp = (Battleplan__c)con.getRecord();
       //Populate Battle Plan Related Objects
       BattleplanWithRelatedValues(bp.Id);
    }
    
    public void initVariables() {
    	bp = null;
    	bpAccnts = null;
    	bpWithRelated = null;
    	bpOppList = null;
    	bpComps = null;
    	bpOpActs = null;
    	bpNotes = null;
    	bpConts = null;
    	bpActsHty = null;
    	bpStrats = null;
    }
    
    public void BattleplanWithRelatedValues (Id Id) {
    	//IsDeleted, Name, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp,
        bpWithRelated = [	Select b.SystemModstamp, b.Proposition__c, b.Project_Name__c, b.Owner__c, b.Opportunity_Name__c, b.Name, b.NIBR__c, b.Id, b.Gross_ACV__c, b.Forecast_Probability__c, b.Feedback__c, b.Extending_the_Proposition__c, b.Estimated_Close_Date__c, b.Customer_Project_or_Challenge__c, b.Business_Overview__c, b.Business_Outcome_for_Customer__c, b.Battleplan_Owners_Manager__c, b.Battleplan_Customer_Contact__c, b.Associated_Opportunity_Id__c, b.Account_Name__c, 
        					//(Select Id, AccountId, WhoId, WhatId, Subject, IsTask, ActivityDate, OwnerId, Status, Priority, ActivityType, IsClosed, IsAllDayEvent, DurationInMinutes, Location, Description, CallDurationInSeconds, CallType, CallDisposition, CallObject, ReminderDateTime, IsReminderSet, Activity_Area__c, Activity_Quarter__c, Activity_type__c, Actual_Completion_Date__c, Collateral__c, Collateral_text__c, Collateral_url__c, Details__c, Due_Date_Proxy__c, Event_Status__c, ExpireTask__c, LE_Code__c, Mandatory_Activity__c, Minimum_Repeat_Date__c, Open_and_Overdue__c, Original_Adder_Contract__c, Original_Contract__c, Original_WHATID__c, Promote__c, Qtr_End_Date__c, Qtr_Historic_Start_Date__c, Qtr_Start_Date__c, Reporting_Status__c, SAC_Code__c, Start_Date_Calendar_Week__c, Start_Date_Fiscal_Month__c, Start_Date_Month__c, Start_Proxy__c, Status_Current_Qtr_Closed__c, Status_Current_Qtr_Open__c, Status_Historic_Qtr_Closed__c, Status_Historic_Qtr_Open__c, Status_Previous_Qtr_Closed__c, Status_Previous_Qtr_Open__c, System_Text__c, System_URL__c, System__c, Target_Completion_Date__c, Task_Category__c, Task_Creation_Qtr__c, Task_Id__c, To_Be_Deleted__c, Ribbit1__EnableSMSReminder__c, Ribbit1__SMSEventReminder__c, Ribbit1__SMSReminderDateTime__c, Activity_Role__c, Owner_EIN__c, Primary_Campaign_Source__c, Reminder_Days__c, Excluded_Reason__c, Call_Status__c, Campaign_Task__c, Assigned_User_Id__c, Last_Account_Call_Date__c, Call_Succeeded__c, Call_Back_Time_Copy__c, Task_to_Campaign__c, Auto_Genereted__c, Assigned_User_Datetime__c, showInCampaignList__c, Related_Campaign_Opportunity__c, Activity_Original__c, Activity_auto_number__c, Activity_ID_ext__c, Contact_Id__c, CreatedBy_EIN__c, LastUpdatedBy_EIN__c, Task_Owner_EIN__c, Campaign_Agent_Notes__c, Comments_Intro__c, Account_CUG__c, Event_Contact_Id__c, Event_Owner_EIN__c, BTLB_Common_Name__c, Reference_Number__c, System_Excluded_Reason_Recorded_On__c, Activity_ID2__c, Activity_Id__c, Owner_Department__c, Owner_Division__c, CreatedBy_Department__c, CreatedBy_Division__c, Advisor_Extension__c , Date__c From OpenActivities),
        					(Select Id, AccountId, WhoId, WhatId, Subject, IsTask, ActivityDate, OwnerId, Status, Priority, ActivityType, IsClosed, IsAllDayEvent, DurationInMinutes, Location, Description, CallDurationInSeconds, CallType, CallDisposition, CallObject, ReminderDateTime, IsReminderSet From OpenActivities), 
        					(Select Id, Name, Battleplan__c, Sales_Strategy__c, Strategy_Outcome__c, Strategy_Type__c From Battleplan_Sales_Strategies__r ORDER By Strategy_Type__c ASC), 
        					(Select Id, Name, Battleplan__c, Actual_Result__c, Checklist_Rating__c, Completion_Date__c, Customer_Contact__c, Date_Tactic_Scheduled__c, Describe_Tactic__c, Status__c, Tactic_Owner__c From Battleplan_Sales_Tactics_Results__r),
        					(Select Id, Name, Battleplan__c, Note__c From Battleplan_Notes__r), 
        					(Select Id, Name, Battleplan__c, Buying_Style__c, Customer_Contact_Type__c, Customer_Contact__c, Decision_Role__c, Job_Title__c, Relationship_Status__c, Time_Spent__c From Battleplan_Customer_Contacts__r), 
        					(Select Id, Name, Battleplan__c, BT_Feature_10__c, BT_Feature_1__c, BT_Feature_2__c, BT_Feature_3__c, BT_Feature_4__c, BT_Feature_5__c, BT_Feature_6__c, BT_Feature_7__c, BT_Feature_8__c, BT_Feature_9__c, Benefits_of_BT_Solution__c, Benefits_of_Competitor_Solution__c, Business_BT__c, Business_Competitor__c, Compelling_Mechanism_BT__c, Compelling_Mechanism_Competitor__c, Competitor_Feature_10__c, Competitor_Feature_1__c, Competitor_Feature_2__c, Competitor_Feature_3__c, Competitor_Feature_4__c, Competitor_Feature_5__c, Competitor_Feature_6__c, Competitor_Feature_7__c, Competitor_Feature_8__c, Competitor_Feature_9__c, Competitor__c, Decision_BT__c, Decision_Competitor__c, Decision_makers_BT__c, Decision_makers_Competitor__c, Executives_BT__c, Executives_Competitor__c, Financial_Situation_BT__c, Financial_Situation_Competitor__c, Implementation_BT__c, Implementation_Competitor__c, Political_BT__c, Political_Competitor__c, Project_Understood_BT__c, Project_Understood_Competitor__c, Supporters_BT__c, Supporters_Competitor__c, Technical_BT__c, Technical_Competitor__c, Value_Contribution_BT__c, Value_Contribution_Competitor__c, Who_1_BT__c, Who_1_Competitor__c, Who_2_BT__c, Who_2_Competitor__c, Why_1_BT__c, Why_1_Competitor__c, Why_2_BT__c, Why_2_Competitor__c From Battleplan_Competitors__r), 
        					//(Select Id, AccountId, LastModifiedDate, WhoId, WhatId, Subject, IsTask, ActivityDate, OwnerId, Status, Priority, ActivityType, IsClosed, IsAllDayEvent, DurationInMinutes, Location, Description, CallDurationInSeconds, CallType, CallDisposition, CallObject, ReminderDateTime, IsReminderSet, Activity_Area__c, Activity_Quarter__c, Activity_type__c, Actual_Completion_Date__c, Collateral__c, Collateral_text__c, Collateral_url__c, Details__c, Due_Date_Proxy__c, Event_Status__c, ExpireTask__c, LE_Code__c, Mandatory_Activity__c, Minimum_Repeat_Date__c, Open_and_Overdue__c, Original_Adder_Contract__c, Original_Contract__c, Original_WHATID__c, Promote__c, Qtr_End_Date__c, Qtr_Historic_Start_Date__c, Qtr_Start_Date__c, Reporting_Status__c, SAC_Code__c, Start_Date_Calendar_Week__c, Start_Date_Fiscal_Month__c, Start_Date_Month__c, Start_Proxy__c, Status_Current_Qtr_Closed__c, Status_Current_Qtr_Open__c, Status_Historic_Qtr_Closed__c, Status_Historic_Qtr_Open__c, Status_Previous_Qtr_Closed__c, Status_Previous_Qtr_Open__c, System_Text__c, System_URL__c, System__c, Target_Completion_Date__c, Task_Category__c, Task_Creation_Qtr__c, Task_Id__c, To_Be_Deleted__c, Ribbit1__EnableSMSReminder__c, Ribbit1__SMSEventReminder__c, Ribbit1__SMSReminderDateTime__c, Activity_Role__c, Owner_EIN__c, Primary_Campaign_Source__c, Reminder_Days__c, Excluded_Reason__c, Call_Status__c, Campaign_Task__c, Assigned_User_Id__c, Last_Account_Call_Date__c, Call_Succeeded__c, Call_Back_Time_Copy__c, Task_to_Campaign__c, Auto_Genereted__c, Assigned_User_Datetime__c, showInCampaignList__c, Related_Campaign_Opportunity__c, Activity_Original__c, Activity_auto_number__c, Activity_ID_ext__c, Contact_Id__c, CreatedBy_EIN__c, LastUpdatedBy_EIN__c, Task_Owner_EIN__c, Campaign_Agent_Notes__c, Comments_Intro__c, Account_CUG__c, Event_Contact_Id__c, Event_Owner_EIN__c, BTLB_Common_Name__c, Reference_Number__c, System_Excluded_Reason_Recorded_On__c, Activity_ID2__c, Activity_Id__c, Owner_Department__c, Owner_Division__c, CreatedBy_Department__c, CreatedBy_Division__c, Advisor_Extension__c, Date__c From ActivityHistories)
        					(Select Id, AccountId, LastModifiedDate, WhoId, WhatId, Subject, IsTask, ActivityDate, OwnerId, Status, Priority, ActivityType, IsClosed, IsAllDayEvent, DurationInMinutes, Location, Description, CallDurationInSeconds, CallType, CallDisposition, CallObject, ReminderDateTime, IsReminderSet  From ActivityHistories)
        					//(Select Id, Name, Description, StageName, Amount, Probability, ExpectedRevenue, TotalOpportunityQuantity, CloseDate, Type, NextStep, LeadSource, IsClosed, IsWon From Opportunities__r) 
        					From Battleplan__c b WHERE b.Id =:Id];
        bpOppList = [Select Id,AccountId, Name, Description, StageName, Amount, Probability, ExpectedRevenue, TotalOpportunityQuantity, CloseDate, Type, NextStep, LeadSource, IsClosed, IsWon From Opportunity Where Id =: bpWithRelated.Opportunity_Name__c];
        bpAccnts =  [Select Id, Name From Account Where Id =: bpOppList[0].AccountId];
        bpOpActs = bpWithRelated.OpenActivities;
        bpComps = bpWithRelated.Battleplan_Competitors__r;
        bpNotes = bpWithRelated.Battleplan_Notes__r;
        bpConts = bpWithRelated.Battleplan_Customer_Contacts__r;
    	bpActsHty = bpWithRelated.ActivityHistories;
    	bpStrats = bpWithRelated.Battleplan_Sales_Strategies__r;
    	bpTResults = bpWithRelated.Battleplan_Sales_Tactics_Results__r;
    }
    
    public Opportunity getbpOpp() {
    	return bpOppList[0];
    }
    
    public Account getbpAccnt() {
    	return bpAccnts[0];
    }
    
    public List<OpenActivity> getbpOpActs() {
    	return bpOpActs;
    }
    
    public List<ActivityHistory> getbpActsHty() {
    	return bpActsHty;
    }
    
    public List<Battleplan_Customer_Contact__c> getbpConts() {
    	return bpConts;
    }
    
    public List<Battleplan_Sales_Strategy__c> getbpStrats() {
    	return bpStrats;
    }
    
    public List<Battleplan_Competitor__c> getbpComps() {
    	return bpComps;
    }
    
    public List<Battleplan_Sales_Tactic_Result__c> getbpTResults() {
    	return bpTResults;
    }
    
    public List<Battleplan_Note__c> getbpNotes() {
    	return bpNotes;
    }
    
    /*public static testMethod void myUnitTest() {
		Account a = Test_Factory.CreateAccount();
        a.Name = 'Test';
        insert a;
       	Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        Battleplan__c batPlan = new Battleplan__c(Opportunity_Name__c = o.Id);
    	insert batPlan;
    	
    	ApexPages.StandardController stdcon = new ApexPages.StandardController(batPlan);
    	battlePlanExtension becon = new battlePlanExtension(stdcon);
    	System.debug(becon.getbpOpp());
    	System.debug(becon.getbpAccnt());
    	System.debug(becon.getbpOpActs());
    	System.debug(becon.getbpActsHty());
    	System.debug(becon.getbpConts());
    	System.debug(becon.getbpStrats());
    	System.debug(becon.getbpComps());
    	System.debug(becon.getbpTResults());
    	System.debug(becon.getbpNotes());
	}*/
}