@isTest(SeeAllData=false)
private class CustomButtonBacktoSolutionTest {
    
    static Account testAccount;
    static Opportunity testOpportunity;
    static cscfga__Product_Basket__c testBasket;
        
    private static void prepareTestData(){
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        testAccount = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        testOpportunity= new Opportunity(   Name='RT test',
                                            AccountId=testAccount.id,
                                            CloseDate=date.today(),
                                            StageName='Created' );
        insert testOpportunity;
        
        testBasket = new cscfga__Product_Basket__c(cscfga__Opportunity__c=testOpportunity.Id);
        insert testBasket;
    
        system.debug('inserted test Basket');
    }
    
    private static testMethod void testperformAction(){
        test.Starttest();
        prepareTestData();
        string basketId = testBasket.Id;
        String sfdcURL = Label.CS_SolutionRedirect_Org_Url;
        System.debug('retrieved sfdcURL====>' +sfdcURL);  //sfdcURL+
        CustomButtonBacktoSolution myButton = new CustomButtonBacktoSolution();
        string fromClass = myButton.performAction(basketId);
        string myTestString = '{"status":"All ok","status":200,"redirectURL":"' + sfdcURL+'/apex/sceditor?basketId='+basketId + '"}';
        system.assertEquals(fromClass,myTestString);
        test.Stoptest();
    }
    
}