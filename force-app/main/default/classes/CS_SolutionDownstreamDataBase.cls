/**
 * Base class for CS_SolutionDownstreamData functionality
 */
public virtual class CS_SolutionDownstreamDataBase {
    protected List<Id> orderIds;
    protected Map<Id, csord__Order__c> orderByIds;
    protected Map<Id, CS_ServiceSpecification> serviceSpecificationsByIds = new Map<Id, CS_ServiceSpecification>();
    protected Map<Id, csord__Service__c> servicesByIds;
    protected Map<Id, cscfga__Product_Basket__c> productBasketsByIds;
    protected Map<Id, cscfga__Product_Configuration__c> productConfigurationsByIds;

    public CS_SolutionDownstreamDataBase(List<Id> orderIds) {
        this.orderIds = orderIds;
    }

    /*public void execute() {
        generateSpecifications();
        mapSpecifications();
        queryData();

        for(CS_SolutionDownstreamDataBase solutionData : solutionDataObjects) {
            solutionData.createData(
                    orderByIds,
                    servicesByIds,
                    productBasketsByIds,
                    productConfigurationsByIds,
                    serviceSpecificationsByIds
            );
        }
    }

    protected virtual void createData(
            Map<Id, csord__Order__c> orderByIds,
            Map<Id, csord__Service__c> servicesByIds,
            Map<Id, cscfga__Product_Basket__c> productBasketsByIds,
            Map<Id, cscfga__Product_Configuration__c> productConfigurationsByIds,
            Map<Id, CS_ServiceSpecification> serviceSpecificationsByIds
    ) {
        //implementation in child class
      }
    */

    protected virtual void createData () {
        
    }

    protected void generateSpecifications() {
        List<Id> subscriptionIds = new List<Id>();
        for (csord__Subscription__c subscription : [SELECT Id FROM csord__Subscription__c WHERE csord__Order__c IN :orderIds]) {
            subscriptionIds.add(subscription.Id);
        }
        //dummy list to pass in API
        List<Id> deliverableIds = new List<Id>();
        csedm.API_1.generateSpecifications(subscriptionIds, deliverableIds, true);
    }

    protected void mapSpecifications() {
        List<CS_OrderSpecification> allOrderSpecs = new List<CS_OrderSpecification>();

        for(Id orderId : orderIds) {
            List<CS_OrderSpecification> orderSpecs = (List<CS_OrderSpecification>)JSON.deserialize(
                    csedm.API_1.getOrderSpecificationsById(orderId), List<CS_OrderSpecification>.class
            );

            allOrderSpecs.addAll(orderSpecs);
        }

        for (CS_OrderSpecification orderSpec :  allOrderSpecs) {
            for (CS_SubscriptionSpecification subSpec : orderSpec.subscriptionSpecificationList) {
                for (CS_ServiceSpecification servSpec : subSpec.serviceSpecificationList) {
                    serviceSpecificationsByIds.put(servSpec.serviceId, mergeSimpleComplexAttributes(servSpec));
                }
            }
        }
    }

    protected void queryData() {
        List<Id> productBasketIds = new List<Id>();
        List<Id> productConfigurationIds = new List<Id>();

        orderByIds = new Map<Id, csord__Order__c>([
                SELECT
                    Id,
                    Name,
                    csordtelcoa__Opportunity__c
                FROM csord__Order__c
                WHERE Id IN :orderIds
        ]);

        servicesByIds = new Map<Id, csord__Service__c>([
                SELECT
                    ID,
                    csordtelcoa__Product_Configuration__c,
                    csordtelcoa__Product_Basket__c,
                    csord__Order__c,
                    csord__Service__c,
                    csordtelcoa__Product_Configuration__r.Site_NAD_Key__c,
                    csordtelcoa__Product_Configuration__r.DeliveryNADKey__c,
                    csordtelcoa__Product_Configuration__r.Product_Definition_Name__c,
                    csordtelcoa__Product_Configuration__r.Tariff_code__c,
                    csordtelcoa__Product_Configuration__r.Package_Code__c,
                    csordtelcoa__Product_Configuration__r.BT_Net_Primary__c,
                    csordtelcoa__Product_Configuration__r.BT_Net_Secondary__c,
                    csordtelcoa__Product_Configuration__r.BT_Net_Primary__r.Identifier_Value__c,
                    csordtelcoa__Product_Configuration__r.BT_Net_Secondary__r.Identifier_Value__c,
                    csordtelcoa__Product_Configuration__r.BT_Net_Primary__r.Address_Line__c,
                    csordtelcoa__Product_Configuration__r.BT_Net_Secondary__r.Address_Line__c,
                    csordtelcoa__Service_Number__c,
                    csordtelcoa__Replaced_Service__r.csordtelcoa__Service_Number__c
                FROM  csord__Service__c
                WHERE csord__Order__c IN :orderIds
        ]);

        if (servicesByIds.isEmpty() || servicesByIds == null) {
            return;
        }

        for (csord__Service__c service : servicesByIds.values()) {
            productBasketIds.add(service.csordtelcoa__Product_Basket__c);
            productConfigurationIds.add(service.csordtelcoa__Product_Configuration__c);
        }

        if (productBasketIds.isEmpty()) {
            return;
        }

        productBasketsByIds = new Map<Id, cscfga__Product_Basket__c>([
                SELECT
                        Id,
                        Name,
                        cscfga__Opportunity__c,
                        csbb__Account__c,
                        Customer_Contact__c,
                        csbb__Account__r.Name,
                        Customer_Contact__r.Name,
                        Customer_Contact__r.Email,
                        Customer_Contact__r.Phone,
                        Customer_Contact__r.MobilePhone,
                        cscfga__total_contract_value__c,
                        cscfga__Opportunity__r.Owner.Name,
                        cscfga__Opportunity__r.Owner.Email,
                        cscfga__Opportunity__r.Owner.EIN__c,
                        cscfga__Opportunity__r.Owner.Phone,
                        cscfga__Opportunity__r.Account_Sector__c,
                        cscfga__Opportunity__r.AssociatedLE__c,
                        csbb__Account__r.SAC_Code__c,
                        csbb__Account__r.Account_Owner_Department__c,
                        csbb__Account__r.Sector__c,
                        csbb__Account__r.Sub_Sector__c,
                        csbb__Account__r.CUG__c,
                        cscfga__Opportunity__r.Name,
                        cscfga__Opportunity__r.Campaign.Name,
                        cscfga__Opportunity__r.CloseDate,
                        Total_Subscribers__c,
                        Basket_Reference__c,
                        cscfga__Opportunity__r.StageName,
                        cscfga__Opportunity__r.Opportunity_Id__c,
                        cscfga__Opportunity__r.Description,
                        cscfga__Opportunity__r.Amount,
                        cscfga__Opportunity__r.IsClosed__c,
                        cscfga__Opportunity__r.Closed_By__c,
                        cscfga__Opportunity__r.Account_Le__c,
                        cscfga__Opportunity__r.SOV_nonSub__c,
                        cscfga__Opportunity__r.Closing_Channel__c,
                        cscfga__Opportunity__r.AssociatedLE__r.Registered_Address__c,
                        cscfga__Opportunity__r.SEC_Code__c,
                        csbb__Account__r.zAccountMarker__c
                FROM cscfga__Product_Basket__c
                WHERE Id IN :productBasketIds
        ]);

        if(productConfigurationIds.isEmpty()) {
            return;
        }

        productConfigurationsByIds = new Map<Id, cscfga__Product_Configuration__c>([
                SELECT
                        Id,
                        Name,
                        Change_Type__c,
                        cscfga__Serial_Number__c,
                        cscfga__Quantity__c,
                        cscfga__One_Off_Charge__c,
                        cscfga__Recurring_Charge__c,
                        cscfga__Contract_Term__c,
                        cscfga__Product_Family__c,
                        Product_Definition_Name__c,
                        Module__c,
                        Grouping__c,
                        Financed__c,
                        Customer_Required_Date__c,
                        cscfga__total_contract_value__c,
                        cscfga__total_recurring_charge__c,
                        cscfga__total_one_off_charge__c,
                        cscfga__one_off_charge_product_discount_value__c,
                        cscfga__recurring_charge_product_discount_value__c,
                        Product_Code__c,
                        cscfga__Description__c,
                        Site_NAD_Key__c,
                        Site_Name__c,
                        BT_Net_Primary__c,
                        BT_Net_Secondary__c,
                        BT_Net_Primary__r.Identifier_Value__c,
                        BT_Net_Secondary__r.Identifier_Value__c,
                        BT_Net_Primary__r.Address_Line__c,
                        BT_Net_Secondary__r.Address_Line__c,
                        csordtelcoa__is_quantity_based_selling__c,
                        DeliveryNADKey__c,
                        Add_On_Quantity__c
                FROM cscfga__Product_Configuration__c
                WHERE Id IN :productConfigurationIds
        ]);
    }

    private CS_ServiceSpecification mergeSimpleComplexAttributes(CS_ServiceSpecification spec) {
        List<String> complexSpecifications = new List<String>(spec.complexAttributes.keySet());

        for (String complexSpecification : complexSpecifications) {
            for (CS_ServiceSpecification.ComplexAttribute cmpAttribute : spec.complexAttributes.get(complexSpecification)) {
                spec.simpleAttributes.addAll(cmpAttribute.simpleAttributes);
            }
        }
        return spec;
    }

    protected Map<String,String> createAttributeMap(List<CS_ServiceSpecification.SimpleAttribute> simpleAttributes) {
        Map<String,String> attributeMap = new Map<String,String>();
        if (simpleAttributes == null) {
            return attributeMap;
        }
        for (CS_ServiceSpecification.SimpleAttribute smpAttr : simpleAttributes) {
            attributeMap.put(smpAttr.name, smpAttr.value);
        }
        return attributeMap;
    }

    protected void insertTempDataAttachment(Id oppId, Blob attachment, String attachmentName) {
        csbtcl_Temporary_Data_Store_CS_JSON__c tempDataStore = new csbtcl_Temporary_Data_Store_CS_JSON__c(Opportunity__c = oppId);
        insert tempDataStore;

        Attachment tempDataAttachment = new Attachment();
        tempDataAttachment.Body = attachment;
        tempDataAttachment.Name = attachmentName;
        tempDataAttachment.ParentId = tempDataStore.Id;

        insert tempDataAttachment;
    }
}