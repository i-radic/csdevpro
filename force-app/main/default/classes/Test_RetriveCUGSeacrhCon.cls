/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 @isTest(SeeAllData=true)
 public class Test_RetriveCUGSeacrhCon{
     
     public static testMethod void MyUnittest(){
     
            Account A = Test_Factory.CreateAccount();
            A.Name = 'Test1'; 
            A.Sector__c = 'TestSector';
            A.Sub_sector__c = 'Test_Sub_Sector'; 
            A.CUG__c = 'CUG12345678';
            Insert A;
         
            RetriveCUGSeacrhCon RetrieveCUG = new RetriveCUGSeacrhCon();
            RetrieveCUG.SendCUG(A.CUG__c);
            
            
            RetriveCUGSeacrhCon RetrieveCUG1 = new RetriveCUGSeacrhCon();
            RetrieveCUG1.SendCUG('CUG00005678');
        
        } 
     }