@isTest
private class Test_CRFRTContinue{

    static TestMethod void myUnitTest(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf = new CRF__c();
    crf.Name='Auto Number';
    crf.Opportunity__c = o.Id;
    crf.Contact__c = c.Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId = [Select Id from RecordType where DeveloperName='NSO_BS' and SObjectType='CRF__c'].Id;
    crf.RecordTypeId = recId;
    insert crf;
    
    CRFRTContinue myController = new CRFRTContinue(new ApexPages.StandardController(crf));
    //myController.returl = 'a'+crf.Id;
    myController.Continue1();
    
    
    
    }
    
    static TestMethod void myUnitTest1(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf1 = new CRF__c();
    crf1.Name='Auto Number';
    crf1.Opportunity__c = o.Id;
    crf1.Contact__c = c.Id;
    crf1.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId1 = [Select Id from RecordType where DeveloperName='AX_FORM' and SObjectType='CRF__c'].Id;
    crf1.RecordTypeId = recId1;
    insert crf1;
    
    CRFRTContinue myController1 = new CRFRTContinue(new ApexPages.StandardController(crf1));
    //myController1.returl = 'a'+crf1.Id;
    myController1.Continue1();
    
    }
    
    
    
   /* static TestMethod void myUnitTest2(){
    
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='ISDN30' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    ////myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }*/
    
    
    static TestMethod void myUnitTest3(){
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Broadband' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    ////myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest4(){
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Direct_Debit_CRF' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    ////myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest5(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Mover_with_existing_BB' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    ////myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest6(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Movers' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest7(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Name_Change_CRF' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest8(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Number_Port' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    crf2.Vol_ref_Mover__c = 'BT011';
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest9(){
    
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='One_Plan' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest10(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='One_Plan_and_Broadband' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest11(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Resign' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest12(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='WLR3_Authorisation' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    
    static TestMethod void myUnitTest13(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='Offer_Exception' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
    
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;
    myController2.Continue1();
        
    }
    static TestMethod void myUnitTest14(){
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Opportunity o = new Opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Contact c = Test_Factory.CreateContact();
    c.Contact_Building__c = '1';
    c.Contact_Sub_Building__c = '2';
    c.Contact_Address_Number__c = '3';
    c.Address_POBox__c = '4';
    c.Contact_Address_Street__c = '5';
    c.Contact_Locality__c = '6';
    c.Contact_Post_Town__c = '7';
    c.Contact_Post_Code__c = '8';
    c.Address_County__c = '9';
    c.Address_Country__c = '10';
    c.Email = 'test12345@test.com';
    c.Phone = '9874562230';
    c.MobilePhone = '4567891230';
    insert c;
    
    CRF__c crf2 = new CRF__c();
    crf2.Name='Auto Number';
    crf2.Opportunity__c = o.Id;
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Dual signatory mandate required';
    Id recId2 = [Select Id from RecordType where DeveloperName='ISDN30' and SObjectType='CRF__c'].Id;
    crf2.RecordTypeId = recId2;
    insert crf2;
        
    ApexPages.currentPage().getParameters().put('CF00N20000002o7Kh_lkid',o.Id);
    CRFRTContinue myController2 = new CRFRTContinue(new ApexPages.StandardController(crf2));
    //myController2.returl = 'a'+crf2.Id;    
    myController2.Continue1();
        
    }
    
}