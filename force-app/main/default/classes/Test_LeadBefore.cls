@isTest
private class Test_LeadBefore {
	
	 static testMethod void myUnitTest() {
	 	
	 	Test.startTest();
	 	
	 	Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
	 	
	 	User uM = new User();
        uM.Username = '999999000@bt.com';
        uM.Ein__c = '999999000';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = profileId;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});
		
		Lead l = New Lead();
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('BT Sport').getRecordTypeId();
         l.LeadSource = 'Desk Landscape';
	    l.Title = 'Testing Title';
	    l.LastName = 'Testing Name';
	    l.Company = 'Testing Company';
	    l.OwnerId = uM.Id;
	    l.Description = 'The alert has been generated based on a landscape with an upcoming contract expiry date in the following area(s): Broadband Calls Lines Mobile Switch LAN WAN';      
	    insert l;
	    Test.stopTest();     
	 	
	 }
}