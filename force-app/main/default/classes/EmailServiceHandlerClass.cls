global class EmailServiceHandlerClass implements Messaging.InboundEmailHandler{


  global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
    Messaging.InboundEnvelope envelope) {
     try
    {
    Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
    //system.debug('************'+email.binaryAttachments);
    if (email.binaryAttachments != null && email.binaryAttachments.size() > 0) { 
    //if (email.TextAttachments != null && email.TextAttachments.size() > 0) {  
        Blob csvFileBody = email.binaryAttachments[0].body;
        //Blob csvFileBody = email.TextAttachments[0].body;       
        string csvAsString = csvFileBody.toString();
        //string csvAsString = email.TextAttachments[0].body;        
        String[] csvFileLines = csvAsString.split('\n');                
        
        List<Usage_Profile__c> usageProfileList = new   List<Usage_Profile__c> () ;
       
        system.debug('*******Headers'+csvFileLines.size());
        
        for(Integer i=1;i<csvFileLines.size();i++){
               
               Usage_Profile__c usageProfile = new  Usage_Profile__c () ;
               string[] csvRecordData = csvFileLines[i].split(',');
                                         
               usageProfile.Name = csvRecordData[0];
               usageProfile.EOLDateRun__c = csvRecordData[1];
               usageProfile.Usage_Profile_External_ID__c = csvRecordData[2];
               usageProfile.EOLNumberOfBills__c = csvRecordData[3];
               usageProfile.EOLDateRange__c = csvRecordData[4];
               usageProfile.EOLBillDays__c = csvRecordData[5];
               usageProfile.EOLBillType__c = csvRecordData[6];
               usageProfile.EOLAccountName__c = csvRecordData[7];
               usageProfile.Total_Number_of_Connections__c = Decimal.valueof(csvRecordData[8].trim());
               //usageProfile.Expected_Voice_per_User__c = csvRecordData[9]; --> This field is not Writable because of being a Formula Field.
               usageProfile.Expected_Voice__c = Decimal.valueof(csvRecordData[10].trim());
               usageProfile.Answer_phone__c = Decimal.valueof(csvRecordData[11].trim());
               usageProfile.Calls_to_EE__c = Decimal.valueof(csvRecordData[12].trim());
               usageProfile.Calls_to_other_UK_mobile_networks__c = Decimal.valueof(csvRecordData[13].trim());
               usageProfile.Closed_user_group__c = Decimal.valueof(csvRecordData[14].trim());
               usageProfile.Incoming_roaming_in_EU__c = Decimal.valueof(csvRecordData[15].trim());
               usageProfile.Incoming_roaming_in_ROW__c = Decimal.valueof(csvRecordData[16].trim());
               usageProfile.International_calls__c = Decimal.valueof(csvRecordData[17].trim());
               usageProfile.Landline__c = Decimal.valueof(csvRecordData[18].trim());
               usageProfile.Other__c = Decimal.valueof(csvRecordData[19].trim());
               usageProfile.Outgoing_roaming_from_EU__c = Decimal.valueof(csvRecordData[20].trim());
               usageProfile.Outgoing_roaming_from_ROW__c = Decimal.valueof(csvRecordData[21].trim());
               usageProfile.Freephone__c = Decimal.valueof(csvRecordData[22].trim());
               usageProfile.Expected_SMS__c = Decimal.valueof(csvRecordData[23].trim());
               //usageProfile.Expected_SMS_per_User__c = Decimal.valueof(csvRecordData[24]); --> This field is not Writable because of being a Formula Field.
               usageProfile.Closed_user_group_text_messaging__c = Decimal.valueof(csvRecordData[25].trim());
               usageProfile.International_numbers_text_messag__c = Decimal.valueof(csvRecordData[26].trim());
               usageProfile.MMS__c = Decimal.valueof(csvRecordData[27].trim());
               usageProfile.Other_text_messaging__c = Decimal.valueof(csvRecordData[28].trim());
               usageProfile.Outgoing_roaming_text_messaging__c = Decimal.valueof(csvRecordData[29].trim());
               usageProfile.Text_message_to_EE__c = Decimal.valueof(csvRecordData[30].trim());
               usageProfile.Text_messaging_to_other_UK_mobile__c = Decimal.valueof(csvRecordData[31].trim());
               usageProfile.Expected_Data__c = Decimal.valueof(csvRecordData[32].trim());
               //usageProfile.Expected_Data_per_User__c = Decimal.valueof(csvRecordData[33]); --> This field is not Writable because of being a Formula Field.
               usageProfile.Data__c = Decimal.valueof(csvRecordData[34].trim());
               usageProfile.Roaming_data_in_EU__c = Decimal.valueof(csvRecordData[35].trim());
               usageProfile.Roaming_data_in_ROW__c = Decimal.valueof(csvRecordData[36].trim());
               usageProfile.MMS_Roaming__c = Decimal.valueof(csvRecordData[37].trim());
               usageProfile.Data_Only_Connections__c = Decimal.valueof(csvRecordData[38].trim());
               //usageProfile.Net_Active_Voice_Connections__c = Decimal.valueof(csvRecordData[39]); --> This field is not Writable
               usageProfile.Total_Number_of_Inactive_Connections__c = Decimal.valueof(csvRecordData[40].trim());
               
               usageProfileList.add(usageProfile);   
           }
        //upsert usageProfileList Usage_Profile_External_ID__c;
        Schema.SObjectField UsageProfileObj = usage_profile__c.Fields.Usage_Profile_External_ID__c ;

        Database.UpsertResult[] results = Database.upsert(usageProfileList,UsageProfileObj,false); 
        
        system.debug('results --> '+ results);        
         for(Database.UpsertResult theResult:results) {
             if(theResult.isSuccess())
               continue; // next item
              List<Database.Error> errors = theResult.getErrors();
              for(Database.Error theError:Errors) {
                  system.debug('****** Errors'+theError); 
              }                        
        }         
    }
    return result;
    }
    catch(SObjectException se) 
   {

       System.debug('The following exception has occurred: ' + se.getMessage());
        return null;
    }
    

  }
  }