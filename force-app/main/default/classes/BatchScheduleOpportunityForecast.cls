global class BatchScheduleOpportunityForecast implements Schedulable{
   global void execute(SchedulableContext sc) {
   	Date myDate = (Date.today()- 3);
   	String sDate = String.valueOf(myDate);
		String qString = 'select Account.Sector_Code__c, Close_Date_Fiscal_Week__c,Net_ACV__c,SOV_GM__c, zCurrent_Fiscal_Week__c, zCurrent_Week_Forecast_ACV__c, ACV_Calc__c, zCurrent_Week_Forecast_NIBR__c, NIBR_Next_Year__c, zCurrent_Week_Forecast_Vol__c, zCurrent_Week_Forecast_CY_NIBR__c, NIBR_Current_Year__c, Current_Week_Forecast_Amount__c, Amount, ForecastUpdated__c from opportunity where zToProcessInForecastBatch__c = \'PROCESS\'and ( ForecastUpdated__c < '+ sDate +' or ForecastUpdated__c  = null) ';
        batchOpportunityForecast b = new batchOpportunityForecast(qString);
        database.executebatch(b, 50);
    }
}