@IsTest
public class CS_JSONService_SitesTest  {
	private static CS_JSON_Schema jsonSchema;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
		jsonSchema = new CS_JSON_Schema();
		jsonSchema = (CS_JSON_Schema) JSON.deserialize(CS_JSONExportTestFactory.getJSON(), CS_JSON_Schema.class);
	}

	@IsTest
	private static void testGetValues() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
		pc.Site__c = 'Test Site';
		INSERT pc;

		CS_JSONService_Products.ProductConfiguration pcTest = new CS_JSONService_Products.ProductConfiguration(pc, new List<cscfga__Attribute__c>());
		Map<Id, CS_JSONService_Products.ProductConfiguration> pcMap = new Map<Id, CS_JSONService_Products.ProductConfiguration>();
		pcMap.put(pc.Id, pcTest);

		Test.startTest();
		CS_JSONService_Sites controller = new CS_JSONService_Sites();
		controller.init(jsonSchema.settings.get(2));
		List<Object> objCheckList = (List<Object>)controller.getValues(pcMap);
		Test.stopTest();

		Map<String, Object> objCheckMap = (Map<String, Object>) objCheckList[0];
		System.assertEquals(pc.Site__c, objCheckMap.get('Site Name'));

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	private static void testGetValuesNull() {
		createTestData();

		Test.startTest();
		CS_JSONService_Sites controller = new CS_JSONService_Sites();
		controller.init(jsonSchema.settings.get(2));
		List<Object> objCheckList = (List<Object>)controller.getValues(null);
		Test.stopTest();

		System.assert(objCheckList.isEmpty());
	}
}