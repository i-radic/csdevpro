@isTest
private class CloudVoiceItemListControllerTest {
    
    static Account testAccount;
    static Opportunity testOpportunity;
    static cscfga__Product_Basket__c testBasket;
    static cscfga__Product_Configuration__c testConfig;
    static cscfga__Product_Definition__c prodDef;

    
    private static void prepareTestData(){
        // testAccount = new Account(  name = 'RT test account',
        //                             OwnerId =UserInfo.getUserId() );
        // insert testAccount;
        //testAccount=[select id from account limit 1];
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account testAccount = CS_TestDataFactory.generateAccount(true, 'testingAccount');
        Opportunity testOpportunity = CS_TestDataFactory.generateOpportunity(true, 'RT test', testAccount);
        
        cscfga__Product_Basket__c testBasket =  CS_TestDataFactory.generateProductBasket(true, 'Basket', testOpportunity);
        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');
        prodDef = CS_TestDataFactory.generateProductDefinitionResign(true, 'CV', true);

        cscfga__Product_Configuration__c testConfig = CS_TestDataFactory.generateProductConfiguration(false, 'CV', testBasket);
		testConfig.cscfga__Product_Definition__c = prodDef.Id;
		INSERT testConfig;

        List<cscfga__Attribute__c> attributes= new List<cscfga__Attribute__c>();
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfig.id,name='Finance Option',cscfga__Value__c='Yes'));
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfig.id,name='One Off Charge',cscfga__Value__c='5.50'));
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfig.id,name='Product Name',cscfga__Value__c='Test Product'));
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfig.id,name='Site Name',cscfga__Value__c='Test Site'));
        insert attributes;
        
        system.debug(attributes);
        system.debug('inserted');
    }
    
	private static testMethod void testConstructor() {
        prepareTestData();
          
        test.Starttest();
        PageReference pageRef = Page.CloudVoiceItemList;
        pageRef.getParameters().put('basketId',String.valueOf(testBasket.id));
        Test.setCurrentPage(pageRef);
        system.debug(pageRef);
        CloudVoiceItemListController controller = new CloudVoiceItemListController();
        test.Stoptest();
	}
	
	private static testMethod void testSaveResults() {
	    prepareTestData();
	    test.Starttest();
        PageReference pageRef = Page.CloudVoiceItemList;
        pageRef.getParameters().put('basketId',String.valueOf(testBasket.id));
        Test.setCurrentPage(pageRef);
        system.debug(pageRef);
        CloudVoiceItemListController controller = new CloudVoiceItemListController();
        controller.wrappedCfgs[0].financeOption=True;
        controller.SaveResults();
        
        List<cscfga__Product_Configuration__c> cfg = [select id,Financed__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c=:testBasket.Id limit 1];
        system.assertEquals(True,cfg[0].Financed__c );
        test.Stoptest();
	    
	}

	private static testMethod void testrecalculateFinances() {
	    prepareTestData();
        cscfga__Product_Configuration__c testConfigTwo = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', testBasket);
		testConfigTwo.cscfga__Product_Definition__c = prodDef.Id;
		INSERT testConfigTwo;
        List<cscfga__Attribute__c> attributes= new List<cscfga__Attribute__c>();
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfigTwo.id,name='Finance Option',cscfga__Value__c='Yes'));
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfigTwo.id,name='One Off Charge',cscfga__Value__c='10.50'));
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfigTwo.id,name='Product Name',cscfga__Value__c='Test Product'));
        attributes.add(new cscfga__Attribute__c(cscfga__Product_Configuration__c=testConfigTwo.id,name='Site Name',cscfga__Value__c='Test Site'));
        insert attributes;
        
        
        test.Starttest();
        PageReference pageRef = Page.CloudVoiceItemList;
        pageRef.getParameters().put('basketId',String.valueOf(testBasket.id));
        Test.setCurrentPage(pageRef);
        system.debug(pageRef);
        CloudVoiceItemListController controller = new CloudVoiceItemListController();
        controller.wrappedCfgs[0].financeOption=True;
        controller.wrappedCfgs[1].financeOption=True;
        
        controller.recalculateFinances();
        
        system.assertEquals(0.00,controller.selectedForFinance - 16 );
        test.Stoptest();
        
        
	}
}