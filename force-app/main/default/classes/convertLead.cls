global class convertLead
{
	    
    webservice static String convertLeadMethod(String currentLeadId) 
    { 
        
        String redirectId;
        Lead lds= [select Company_Custom_Id__c,LeadSource,Contact_Custom_Id__c,Lead_Profile__c from Lead where Id=:currentLeadId];
        Database.LeadConvert lc = new Database.LeadConvert();
        lc.setLeadId(currentLeadId);
        lc.setAccountId(lds.Company_Custom_Id__c);
        lc.setContactId(lds.Contact_Custom_Id__c);
        lc.setConvertedStatus('Converted');
        
       Database.LeadConvertResult lcr = Database.convertLead(lc);
       if(lcr.isSuccess())
       {
           if(lcr.opportunityId == null){
             redirectId= currentLeadId;
           }else
           {
			   Lead LeadSourceValue = [select Id,LeadSource from Lead where Id=:lcr.getLeadId()];
               Opportunity opp = new Opportunity();
               opp.id= lcr.opportunityId;
               opp.CloseDate=System.Today()+120;
               Opp.StageName='Recognition of Needs';
               Opp.Type ='Win';
               Opp.Brand__c = 'EE';
               Opp.LeadSource = LeadSourceValue.LeadSource;
               
               Update Opp;
               redirectId= Opp.Id;
               
           }
           
       }
       
        return redirectId;
    }
}