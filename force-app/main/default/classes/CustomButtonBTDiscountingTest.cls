@IsTest
public class CustomButtonBTDiscountingTest  {
	@IsTest
	public static void testPerformAction() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account testAcc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', testAcc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', testOpp);

		CustomButtonBTDiscounting controller = new CustomButtonBTDiscounting();
		Test.startTest();
		String testUrl = controller.performAction(basket.Id);
		String check = '{"status":"ok","redirectURL":"/apex/csdiscounts__DiscountPage?basketId=' + basket.Id + '"}';
		System.assertEquals(check, testUrl);
		Test.stopTest();
	}
}