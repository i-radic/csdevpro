@isTest
 
private class AccountUpdateMerge_Test{
 
    static testMethod void runPositiveTestCases() {
    

// ensure user has privaledges to run trigger
       User thisUser = new User (id=UserInfo.getUserId(), Apex_Trigger_Account__c=false, Run_Apex_Triggers__c=false, division ='BTLB' );
       update thisUser;
     System.runAs( thisUser ){
    
     TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
     settings.Account__c = FALSE;
     settings.Contact__c = FALSE;
     settings.Opportunity__c = FALSE;
     settings.OpportunitylineItem__c = FALSE;
     settings.Task__c = FALSE;
     settings.Event__c = FALSE;
     
     upsert settings TriggerDeactivating__c.Id;
    }    
  Test_Factory.SetProperty('IsTest', 'no');      
//create new user to own BTLB2
      
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'BT: Standard User']; 
    //UserRole rol = [SELECT Id FROM UserRole WHERE Name = 'BTLB Base Team'];   
        User U1 = new User();
        u1.Username = 'test98745@bt.com';
        u1.Ein__c = '987654321';
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.ProfileId = prof.Id;
        u1.TimeZoneSidKey = 'Europe/London'; 
        u1.LocaleSidKey = 'en_GB';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'en_US';
        u1.Apex_Trigger_Account__c = false; 
        u1.Run_Apex_Triggers__c = false;
        u1.ISACTIVE = true;
        u1.Department = 'Corporate';
      	u1.Division = 'BT';
        //u1.UserRoleId = rol.Id;
        
         Database.SaveResult[] u1Result = Database.insert(new User[] {u1}); 
     
        id BTLBowner2 = u1Result[0].id;
         system.debug('ADJ AUM U1  ' + u1 ); 
      
     Test_Factory.SetProperty('IsTest', 'yes');
      
 
//create test BTLB    
 BTLB_Master__c testBTLB = new BTLB_Master__c( name = 'TEST',  Account_Owner__c = UserInfo.getUserId(), BTLB_Name_ExtLink__c ='TEST');
   Database.SaveResult[] BTLBResult = Database.insert(new BTLB_Master__c[] {testBTLB});     
   system.debug('ADJ TestBTLB ' + testBTLB ); 
       
// create dummy Market sacs for test BTLB
BTLB_Market_SAC__c testMktSac = new BTLB_Market_SAC__c();
        testMktSac.BTLB_Name__c = BTLBResult[0].id;
        testMktSac.SAC_Code__c = 'aSac999';
        testMktSac.ExtId__c = '999';
        insert testMktSac;
system.debug('ADJ TestMktSAC ' + testMktSac );

// create dummy LOB etc for test BTLB      
BTLB_CCAT__c testLOB = new BTLB_CCAT__c();
        testLOB.BTLB_Name__c = BTLBResult[0].id;
        testLOB.LOB_Code__c = 'lob999';
        testLOB.LOB_Name__c = 'BTLBTest';
        testLOB.Sub_Sector_Code__c = 'ssc999';
        testLOB.Sub_Sector__c ='ssTest';
        testLOB.ExtId__c = '999';
        insert testLOB;  
  system.debug('ADJ TestLOB ' + testLOB );  

//create test BTLB 2   
 BTLB_Master__c testBTLB2 = new BTLB_Master__c( name = 'TEST2',  Account_Owner__c = BTLBowner2, BTLB_Name_ExtLink__c ='TEST2');
   Database.SaveResult[] BTLBResult2 = Database.insert(new BTLB_Master__c[] {testBTLB2});     
   system.debug('ADJ TestBTLB2 ' + testBTLB2 ); 
       
// create dummy Market sacs for test BTLB2
BTLB_Market_SAC__c testMktSac2 = new BTLB_Market_SAC__c();
        testMktSac2.BTLB_Name__c = BTLBResult2[0].id;
        testMktSac2.SAC_Code__c = 'aSac99z';
        testMktSac2.ExtId__c = '999z';
        insert testMktSac2;
system.debug('ADJ TestMktSAC ' + testMktSac );

// create dummy LOB etc for test BTLB2      
BTLB_CCAT__c testLOB2 = new BTLB_CCAT__c();
        testLOB2.BTLB_Name__c = BTLBResult2[0].id;
        testLOB2.LOB_Code__c = 'lob992';
        testLOB2.LOB_Name__c = 'BTLBTest2';
        testLOB2.Sub_Sector_Code__c = 'ssc9992';
        testLOB2.Sub_Sector__c ='ssTest2';
        testLOB2.ExtId__c = '9992';
        insert testLOB2;  
  system.debug('ADJ TestLOB2 ' + testLOB );      

Test.StartTest();    
   
        
//create BTLB Market SAC
        StaticVariables.setAccountDontRun(False);  
        Account acc1 = Test_Factory.CreateAccount(); // a SAC Level Account to link to
        acc1.OwnerId = UserInfo.getUserId();
        acc1.Sector__c = 'BT Local Business';
        acc1.LOB_Code__c = 'lob999';
        acc1.SAC_Code__c = 'aSac999';
        acc1.LE_Code__c = '';
        acc1.AM_EIN__c = '802537216';
        acc1.Sub_Sector__c = 'ssTest';
        Database.SaveResult[] acc1Result = Database.insert(new Account[] {acc1});



        StaticVariables.setAccountDontRun(False);  
        Account acc2 = Test_Factory.CreateAccount(); // a SAC Level Account to link to
        acc2.OwnerId = UserInfo.getUserId();
        acc2.Sector__c = 'BT Local Business';
        acc2.LOB_Code__c = 'lob999';
        acc2.SAC_Code__c = 'aSac999';
        acc2.LE_Code__c = 'TEST2';
        acc2.AM_EIN__c = '802537216';
        acc2.Sub_Sector__c = 'ssTest2';
        Database.SaveResult[] acc2Result = Database.insert(new Account[] {acc2});
        system.debug('ADJ AUM acc2 ' + acc2 ); 
	List <Id> Acc = new List <Id>();
	Acc.add(acc1.id);
 Map<String,Id> AccMap = new Map<String,Id>();
    AccMap.put(acc1.LE_Code__c,acc1.Id);
 Map<Id,Id> AccMap2 = new Map<Id,Id>();
    AccMap2.put(acc2.Id,acc1.Id);
        
Opportunity opp1 = Test_Factory.CreateOpportunity(acc1Result[0].getId());
        opp1.closedate = system.today();
        opp1.Owner__c = UserInfo.getUserId();
        Database.SaveResult[] opp1Result = Database.insert(new Opportunity[] {opp1});     
        
Opportunity opp2 = Test_Factory.CreateOpportunity(acc2Result[0].getId());
        opp2.closedate = system.today();
        opp2.Owner__c = u1Result[0].id;
        Database.SaveResult[] opp2Result = Database.insert( new Opportunity[] {opp2});  

    /* List <Opportunity> OppList = New List<Opportunity>();
        for(Integer i=1; i<=200; i++) {
            opportunity O= new Opportunity();
            O.Name = 'tst_oppty';
            O.accountID=acc2Result[0].getId();
            O.closedate = system.today();
            O.Owner__c = u1Result[0].id;
            O.StageName = 'Created';
            O.NIBR_Year_2009_10__c = 1;
            O.NIBR_Year_2010_11__c = 1;
            O.NIBR_Year_2011_12__c = 1;
         	O.Sales_Stage_Detail__c = 'Appointment Made';
            
            OppList.add(O);  
        }      
     insert OppList;
   */     
Landscape_BTLB__c Ls1 = Test_Factory.CreateLandscapeBTLB2(acc1Result[0].getId());
        Database.SaveResult[] Ls1Result = Database.insert(new Landscape_BTLB__c[] {Ls1});    
        
Landscape_BTLB__c Ls2 = Test_Factory.CreateLandscapeBTLB2(acc2Result[0].getId());
        Database.SaveResult[] Ls2Result = Database.insert(new Landscape_BTLB__c[] {Ls2});       
        
SCARS_BTLB__c Scars1 = Test_Factory.CreateSCARS_BTLB (acc1Result[0].getId());
        Database.SaveResult[] Scars1Result = Database.insert(new SCARS_BTLB__c[] {Scars1});  
        
SCARS_BTLB__c Scars2 = Test_Factory.CreateSCARS_BTLB (acc2Result[0].getId());
        Database.SaveResult[] Scars2Result = Database.insert(new SCARS_BTLB__c[] {Scars2});          

Task Tsk1=Test_Factory.CreateTask();
        Tsk1.WhatID=acc1Result[0].getId();
        Tsk1.Task_Category__c = 'Bulk Tasks';
        Tsk1.sac_code__c = 'aSac999';
      
        Database.SaveResult[] Tsk1Result = Database.insert(new Task[] {Tsk1}); 
       
List <Id> TaskIds = new List <Id>();
TaskIds.add(Tsk1.id);
       
//Move BTLB Market SAC customer
        
        StaticVariables.setAccountDontRun(False);  
        acc2.Sub_Sector__c = 'ssTest2';   
        update acc2;
        system.debug('ADJ AUM acc2 ' + acc2 ); 
        
//AccountUpdateMerge.mergeAccounts(AccMap);
AccountUpdateMerge.updateTasks(TaskIds);
//merge accounts
StaticVariables.setAccountDontRun(False);  
acc1.MergeLe_Code__c = 'TEST2';       
update acc1;
        
AccountUpdateMerge.updateAccOpps(Acc);
AccountUpdateMerge.mergeAccountsByIds(AccMap2);

Test.StopTest();       
        }  
   
}