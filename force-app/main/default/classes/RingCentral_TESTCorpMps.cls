/*######################################################################################
PURPOSE
Main Test Class for all Ring Central Corp Mps activities


######################################################################################*/
@isTest(SeeAllData=true)

private class RingCentral_TESTCorpMps 
{

        static Account a {get; set;}
        static Opportunity o {get; set;}
        static Contact c {get; set;}    
        static CloudPhone_RC_Deal_Management__c  rcDealMan {get; set;} 
        static Associated_LEs__c ale{get; set;}
        static RingCentralURL__c rcURL {get; set;} 
        static Project__c pro {get; set;}
        static RecordType rc1 ;
        static void SetVariables(){
        a = Test_Factory.CreateAccount();     
        a.CUG__c = 'cugCV1';
        a.OwnerId = UserInfo.GetUserId() ;
        Database.SaveResult aR = Database.insert(a);
        System.debug('AR.id'+ar.id);
        System.debug('A.id'+a.id);
        ale= new Associated_LEs__c(Account__c =a.id,pH_Account_Name__c= 'Test LE',pH_Employees__c=555, Registered_Postcode__c ='RG2 1AG');     
        insert ale;
        o = Test_Factory.CreateOpportunity(aR.getid());
        o.closedate = system.today();
        o.AssociatedLE__c = ale.id;
        insert o;      
        c = Test_Factory.CreateContact();
        c.Cug__c = 'cugCV1';
        c.Contact_Post_Code__c = 'WR5 3RL';
        insert c;
        pro= new Project__c();
        pro.Name='ABC';
       
        pro.Stage__c='Project Under Review';
        pro.Service_Offering__c='Cloud Voice';
        //insert pro;
         rc1 = [SELECT Id,developerName FROM RecordType WHERE SObjectType = 'project__c' and developerName ='Implementation' limit 1];
        pro.RecordTypeId=rc1.id;
        pro.Opportunity__c=o.id;
        pro.Customer_Required_By_Date__c=Date.newInstance(2019, 12, 9);
        insert pro; 
        
     
        rcDealMan = new CloudPhone_RC_Deal_Management__c(Name ='test deal Man',Opportunity__c = o.Id,     RingCentralDealRegContact__c = c.Id);
        insert rcDealMan ;       
         update o; 
        
        rcURL = new RingCentralURL__c (Username__c='test', Password__c='test', Url__c='test', Partner_Login_Url__c='test', Name = 'testRCurl');
        insert rcURL;
    }

    static testMethod void RingCentralDealRegCorpMps() {   
        SetVariables();
        ApexPages.currentPage().getParameters().put('users', 'TEST'); 
        ApexPages.currentPage().getParameters().put('accName', 'TEST'); 
        ApexPages.currentPage().getParameters().put('rcDealManID', rcDealMan.Id); 
        ApexPages.currentPage().getParameters().put('conId', c.Id); 

        Test.startTest();
        //--------Set mock SOAP callout class 
        Test.setMock(WebServiceMock.class, new RingCentralWebSvcMockImpl());
        RingCentralDealRegCorpMps dealReg = new RingCentralDealRegCorpMps ();
        dealReg.SetValues();
        Test.stopTest();
    }


     static testMethod void RingCentralSalesFunnelCorpMps() 
     {
            SetVariables();
            ApexPages.currentPage().getParameters().put('id', rcDealMan .Id); 
            ApexPages.currentPage().getParameters().put('users', '555'); 
                ApexPages.currentPage().getParameters().put('oppID', o.id); 
            ApexPages.currentPage().getParameters().put('pro', pro.id); 
             ApexPages.currentPage().getParameters().put('recordTypeId', rc1.id); 
            Test.startTest();
            RingCentralSalesFunnelCorpMps salesFunnel = new RingCentralSalesFunnelCorpMps(new ApexPages.StandardController(rcDealMan )); 
            salesFunnel.testID ='';
            salesFunnel.simultaneousUsers='';
            salesFunnel.bandwidthResult='';
            salesFunnel.mosScore='';
            salesFunnel.accountActivationDate='15/09/2018' ;
            salesFunnel.contractEndDateDD='15' ;
            salesFunnel.contractEndDateMM='09' ;
             salesFunnel.contractEndDateYYYY='20' ;
             salesFunnel.SetRecordTypeId(rc1.id) ;
     
        
           pro.Opportunity__c=o.id;
            
           pro.RecordTypeId=rc1.id;
           
         salesFunnel.setPro(pro);
        salesFunnel.SetTestHarnessValues();
        salesFunnel.SetValues();
        salesFunnel.FinishButton();
        Test.stopTest();
    }

}