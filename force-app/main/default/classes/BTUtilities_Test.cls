/*
Client      : BT Business
Author      : Krupakar Reddy J
Date        : 09/02/2013 (dd/mm/yyyy)
Description : This class is a Utilities Class For Developers Writing Classes and Triggers
To Avoid writing same code in multiple classes.

******************************************************************
Copyright (c) 2013, BT Business, Krupakar Reddy J
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
 
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of the BTUtilities nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written
      permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

******************************************************************
*/

@isTest
private class BTUtilities_Test {
        
    static testMethod void getRecordTypesTest() {    
        map<String,Id> recTypes = BTUtilities.getRecordTypes('Opportunity');
        system.debug('Opportunity Record Types : ' + recTypes);
    }
    
    static testMethod void getRecordTypeIdByNameTest() {     
        Id recType = BTUtilities.getRecordTypeIdByName('Opportunity', 'BT OnePhone');
        system.debug('BT OnePhone Record Type Id Is : ' + recType);
        
        Id recTypenull = BTUtilities.getRecordTypeIdByName(null, null);
        system.debug('BT OnePhone Record Type Id Is : ' + recTypenull);
    }
    
    static testMethod void getPicklistValuesTest() {     
        list<SelectOption> pcklstValues = BTUtilities.getPicklistValues('BTLB_Order_Cancellation_Form__c', 'Customer_Contacted__c');
        system.debug('Picklist Values Are : ' + pcklstValues);
    }
    
    static testMethod void getSObjectIdSetTest() {
        List <Issue__c> lstIssues = new List <Issue__c> ();
        for(integer i=0; i<10; i++) {
            Issue__c iss = new Issue__c();
            iss.Brief_Title__c = 'Title'+i;          
            lstIssues.add(iss);
        }
        insert lstIssues;
        list<Issue__c> lstIssue = [SELECT Id, Name, Brief_Title__c From Issue__c LIMIT 10];
        //test getSObjectIdSet   
        Set<Id> IdSet = BTUtilities.getSObjectIdSet(lstIssue);
        system.debug('Issue Id Set : ' + IdSet);
        //test getDesiredSet
        Set<String> dsSet = BTUtilities.getDesiredSet(lstIssue, 'Brief_Title__c');
        system.debug('Desired Set : ' + dsSet);
        //test getDesiredSetWithEqualFilter
        Set<String> dsSetWF = BTUtilities.getDesiredSetWithEqualFilter(lstIssue, 'Brief_Title__c', 'Brief_Title__c', 'Title0;Title1');
        system.debug('Desired Set With Equal Filter : ' + dsSetWF);
        //test getDesiredSetWithNotEqualFilter
        Set<String> dsSetNEWF = BTUtilities.getDesiredSetWithNotEqualFilter(lstIssue, 'Brief_Title__c', 'Brief_Title__c', 'Title0;Title1');
        system.debug('Desired Set With Not Equal Filter : ' + dsSetNEWF);
        //test getDesiredSObjectSetWithEqualFilter
        Set<SObject> dsSobjectSetWF = BTUtilities.getDesiredSobjectSetWithEqualFilter(lstIssue, 'Brief_Title__c', 'Title0;Title1');
        system.debug('Desired SObject Set With Equal Filter : ' + dsSobjectSetWF);
        //test getDesiredSobjectSetWithNotEqualFilter
        Set<SObject> dsSobjectSetNEWF = BTUtilities.getDesiredSobjectSetWithNotEqualFilter(lstIssue, 'Brief_Title__c', 'Title0;Title1');
        system.debug('Desired Sobject Set With Not Equal Filter : ' + dsSobjectSetNEWF);
        //test getDesiredList
        list<String> dsList = BTUtilities.getDesiredList(lstIssue, 'Brief_Title__c');
        system.debug('Desired List : ' + dsList);       
        //test getDesiredListWithEqualFilter
        list<String> dsListWF = BTUtilities.getDesiredListWithEqualFilter(lstIssue, 'Brief_Title__c', 'Brief_Title__c', 'Title0');
        system.debug('Desired List With Equal Filter : ' + dsListWF);       
        //test getDesiredListWithNotEqualFilter
        list<String> dsListNEWF = BTUtilities.getDesiredListWithNotEqualFilter(lstIssue, 'Brief_Title__c', 'Brief_Title__c', 'Title0');
        system.debug('Desired List With Not Equal Filter : ' + dsListNEWF);
        //test getDesiredSObjectListWithEqualFilter
        list<SObject> dsSobjectListWF = BTUtilities.getDesiredSObjectListWithEqualFilter(lstIssue, 'Brief_Title__c', 'Title0');
        system.debug('Desired SObject List With Equal Filter : ' + dsSobjectListWF);        
        //test getDesiredSObjectListWithNotEqualFilter
        list<SObject> dsSObjectListNEWF = BTUtilities.getDesiredSObjectListWithNotEqualFilter(lstIssue, 'Brief_Title__c', 'Title0');
        system.debug('Desired Sobject List With Not Equal Filter : ' + dsSObjectListNEWF);
        //test getDesiredMap
        Map<String, String> mDSMap = BTUtilities.getDesiredMap(lstIssue, 'Id', 'Brief_Title__c');
        system.debug('Desired Map : ' + mDSMap);
        //test getDesiredMapWithEqualFilter
        Map<String, String> mDSMapEF = BTUtilities.getDesiredMapWithEqualFilter(lstIssue, 'Id', 'Brief_Title__c','Brief_Title__c', 'Title0');
        system.debug('Desired Map With Equal Filter: ' + mDSMapEF);
        //test getDesiredMapWithNotEqualFilter
        Map<String, String> mDSMapNEF = BTUtilities.getDesiredMapWithNotEqualFilter(lstIssue, 'Id', 'Brief_Title__c','Brief_Title__c', 'Title0');
        system.debug('Desired Map With Not Equal Filter: ' + mDSMapNEF);
        //test getSObjectMap
        Map<String, SObject> mSOMap = BTUtilities.getSObjectMap(lstIssue);
        system.debug('SObject Map : ' + mSOMap);
        //test getSObjectMapWithEqualFilter
        Map<String, SObject> mSOMapEF = BTUtilities.getSObjectMapWithEqualFilter(lstIssue,'Brief_Title__c', 'Title0');
        system.debug('SObject Map With Equal Filter: ' + mSOMapEF);
        //test getSObjectMapWithNotEqualFilter
        Map<String, SObject> mSOMapNEF = BTUtilities.getSObjectMapWithNotEqualFilter(lstIssue,'Brief_Title__c', 'Title0');
        system.debug('SObject Map With Not Equal Filter: ' + mSOMapNEF);            
        //test getDesiredSObjectMap
        Map<String, SObject> mDSOMap = BTUtilities.getDesiredSObjectMap(lstIssue, 'Brief_Title__c');
        system.debug('Desired SObject Map : ' + mDSOMap);
        //test getDesiredSObjectMapWithEqualFilter
        Map<String, SObject> mDSOMapEF = BTUtilities.getDesiredSObjectMapWithEqualFilter(lstIssue, 'Brief_Title__c','Brief_Title__c', 'Title0');
        system.debug('Desired SObject Map Equal Filter: ' + mDSOMapEF);
        //test getDesiredSObjectMapWithNotEqualFilter
        Map<String, SObject> mDSOMapNEF = BTUtilities.getDesiredSObjectMapWithNotEqualFilter(lstIssue, 'Brief_Title__c','Brief_Title__c', 'Title0');
        system.debug('Desired SObject Map Not Equal Filter: ' + mDSOMapNEF);
    }
    
    static testMethod void getCreateableFieldsSoqlTest() {   
        String CreateableFieldsSOQL = BTUtilities.getCreateableFieldsSoql('Issue__c', '');
        system.debug('Createable Fields SOQL : ' + CreateableFieldsSOQL);
    }
    
    static testMethod void getAccessibleFieldsSoqlTest() {   
        String AccessibleFieldsSOQL = BTUtilities.getAccessibleFieldsSoql('Issue__c', '');
        system.debug('Accessible Fields SOQL : ' + AccessibleFieldsSOQL);
    }
    
    static testMethod void getSObjectListByQueryTest() {
        List <Issue__c> lstIssues = new List <Issue__c> ();
        for(integer i=0; i<10; i++) {
            Issue__c iss = new Issue__c();
            iss.Brief_Title__c = 'Title'+i;          
            lstIssues.add(iss);
        }
        insert lstIssues;
        list<SObject> lstSOIssues = BTUtilities.getSObjectListByQuery(BTUtilities.getAccessibleFieldsSoql('Issue__c', ''));      
        system.debug('Issue Object Query List : ' + lstSOIssues);
    }
    
    static testMethod void DMLRecordsTest() {
        List <Issue__c> lstIssues = new List <Issue__c> ();
        for(integer i=0; i<10; i++) {
            Issue__c iss = new Issue__c();
            iss.Brief_Title__c = 'Title'+i;          
            lstIssues.add(iss);
        }       
        Database.Saveresult[] isr = BTUtilities.insertRecords(lstIssues);       
        system.debug('Insert Save Result : ' + isr);
        Database.Saveresult[] usr = BTUtilities.updateRecords(lstIssues);       
        system.debug('Update Save Result : ' + usr);
        Database.UpsertResult[] upsr = BTUtilities.upsertRecordsOnIds(lstIssues);       
        system.debug('Upsert Save Result : ' + upsr);
        Database.UpsertResult[] upsrext = BTUtilities.upsertRecordsOnExternalId(lstIssues, 'Issue__c', 'Brief_Title__c');       
        system.debug('Upsert on External Id Save Result : ' + upsrext);
    }
    
    static testMethod void rollUpTriggerTest() {
        list<BTUtilities.fieldDefinition> fdHeader = new list<BTUtilities.fieldDefinition> {
            new BTUtilities.fieldDefinition('COUNT', 'ID', 'BT_Sport_Header__c')};
        
        BTUtilities.rollUp(fdHeader, null, null,
        'BT_Sport__c', 'BT_Sport_Header__c', 'BT_Sport__c', '');                
    }
    
    static testMethod void parseCSVTest() {
        string strCSV = 'BROADSWORD_STATE__c,BROADSWORD_SUB_STATE__c,Cancellation_Current_Status__c,COMPLETION_DATE__c\n';
               strCSV += 'DNZ642WE,PR01OCN,PAA,12/04/2012 17:00:00\n';
               strCSV += 'ZPT670ND,PR01OCN,COM,10/18/2012 17:00:00\n';
               strCSV += 'TFV159CM,PR01MXB,DBE,11/16/2012 17:00:00\n';
               strCSV += 'GZM810WM,PR01MXB,PAA,01/03/2013 17:00:00\n';
        List<List<String>> lstCSV = BTUtilities.parseCSV(strCSV,false);  
        system.debug('CSV File : ' + lstCSV);
        
        list<sObject> lstSOCSV = BTUtilities.csvTosObject(BTUtilities.parseCSV(strCSV,false), 'BTB_Order_Line__c');  
        system.debug('SObjects CSV File : ' + lstSOCSV);
    }
    
    static testMethod void isEmailValidTest() {
        boolean isValid = BTUtilities.isEmailValid('test@test');     
        system.debug('Is Valid Email : ' + isValid + '  Null' + BTUtilities.isEmailValid(null));
    }
    
    static testMethod void isPostCodeValidTest() {
        boolean isValid = BTUtilities.isPostCodeValid('w13 8qd');    
        system.debug('Is Valid Post Code : ' + isValid + '  Null' + BTUtilities.isPostCodeValid(null));
    }
    
    static testMethod void isNotNullOrEmptyTest() {
        boolean isNullEmpty = BTUtilities.isNotNullOrEmpty('w13 8qd');   
        system.debug('Is String Null or Empty : ' + isNullEmpty);
    }
    
    static testMethod void redirectPageTest() {
        PageReference pageRef = BTUtilities.redirectPage('/home/home.jsp');  
        system.debug('Redirect Page : ' + pageRef);
    }
    
    static testMethod void getPageParamValueTest() {
        String param = BTUtilities.getPageParamValue('Id');  
        system.debug('Param value : ' + param);
    }
    
    static testMethod void addConfirmTest() {
        BTUtilities.addConfirm('Confirm');
    }
    
    static testMethod void addInfoTest() {
        BTUtilities.addInfo('Info');
    }
    
    static testMethod void addWarningTest() {
        BTUtilities.addWarning('Warning');
    }
    
    static testMethod void addErrorTest() {
        BTUtilities.addError('Error');
    }
    
    static testMethod void addFatalTest() {
        BTUtilities.addFatal('Fatal');
    }
    private static User createDummyUser()
    {
        Profile B2BProfile = [select id from profile where name = 'BTLB: RDs and PSMs'];
        
        return new User(alias = 'B2B2', email = 'B2B132cr@btlocalbusiness.co.uk',
        emailencodingkey = 'UTF-8', lastname = 'Testing22222 B2B1', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile.Id, timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr2222@testemail.com',
        EIN__c = 'B2B5cr');
    }
    static testmethod void  getRoleSubordinateUsersTest()
    {        
        User B2BTestUser = createDummyUser();
        insert B2BTestUser;
        
        ID testId = B2BTestUser.ID;
        Set <ID> userIds = BTUtilities.getRoleSubordinateUsers(testId);
        system.debug('userIds count : ' + userIds.size());
    }
    static testmethod void getUserTest()
    {
        User B2BTestUser = createDummyUser();
        insert B2BTestUser;
        
        ID testId = B2BTestUser.ID;
		BTUtilities.userDetails ud = BTUtilities.getUser(testId);
        system.debug('getUserTest : ' + ud.getFirstName);
    }
    static testmethod void getCreatableFieldsSOQLTest()
    {
        User B2BTestUser = createDummyUser();
        insert B2BTestUser;
        
		string str = BTUtilities.getCreatableFieldsSOQL('User','email = "B2B132cr@btlocalbusiness.co.uk"');
        system.debug('getCreatableFieldsSOQL : ' + str);
    }
    
    static testmethod void sendMailTest()
    {
        BTUtilities.sendMail(new List <String>{'testmail@tmail.com'}, null, 'some Subject', 'String body'); 
    }
        
    static testMethod void testsendEmail_test() {
        List<String> recepients=new String[]{'test@test.com','test2@test.com'};
        BTUtilities.sendTextEmail(recepients,'Test method', 'This is to test the sendTextNotificationEmail method');
        BTUtilities.sendHTMLEmail(recepients,'Test method', 'This is to test the sendTextNotificationEmail method');
    } 
    
    static testMethod void testsendEmailNoReceipients_test() {
        List<String> recepients=null;
        BTUtilities.sendTextEmail(recepients,'Test method', 'This is to test the sendTextNotificationEmail method');
        
        recepients=new List<String>();
        BTUtilities.sendHTMLEmail(recepients,'Test method', 'This is to test the sendTextNotificationEmail method');
    }
    
    static testMethod void testsendEmailWithAttachment_test() {
        List<String> recepients=new String[]{'test@test.com','test2@test.com'};
        List<Attachment> stdAttachments = new List<Attachment>();
        Attachment a = new Attachment();
        a.Name = 'Test';
        a.Body = EncodingUtil.base64Decode('Test Body');
        stdAttachments.add(a);
        BTUtilities.sendEmailWithStandardAttachments(recepients,'Test method', 'This is to test the sendTextNotificationEmail method',false,stdAttachments);
    }
    
    static testMethod void testsendEmailWithAttachmentIDs_test() {
        List<String> recepients=new String[]{'test@test.com','test2@test.com'};
        List<ID> stdAttachments = new List<ID>();  
        
        Case cse = new Case();
        insert cse;             
        
        Attachment a = new Attachment();
        a.ParentId = cse.Id;
        a.Name = 'Test';
        a.Body = EncodingUtil.base64Decode('Test Body');
        insert a;
        stdAttachments.add(a.Id);
        BTUtilities.sendEmailWithStandardAttachments(recepients,'Test method', 'This is to test the sendTextNotificationEmail method',false,stdAttachments);
    }  
}