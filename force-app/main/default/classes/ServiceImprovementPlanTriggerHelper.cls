/**************************************************************************************************************************************************
    Class Name : ServiceImprovementPlanTriggerHelper
    Test Class Name : Test_CustomerExpClientplanBefore
    Description : Code to Insert and Update Customer Experience client plan records. 
    Version : V0.1
    Created By Author Name : BALAJI MS
    Date : 08/09/2017
 *************************************************************************************************************************************************/

public class ServiceImprovementPlanTriggerHelper {
 
    public static void BeforeInsertTrigger(List<Service_Improvement_Plan__c> ClientPlanList){
        
        If(ClientPlanList.size() > 0) 
        	CommonMethodFortrigger(ClientPlanList);
        
    }
    
    public static void BeforeUpdateTrigger(Map<Id, Service_Improvement_Plan__c> NewServicePlanMap, Map<Id, Service_Improvement_Plan__c> OldServicePlanMap){
        
        List<Service_Improvement_Plan__c> ClientPlanListToUpdate = new List<Service_Improvement_Plan__c>();
        
        for(Service_Improvement_Plan__c ClientPlan :NewServicePlanMap.values()) 
            ClientPlanListToUpdate.add(ClientPlan);
        
        if(ClientPlanListToUpdate.size()>0)
            CommonMethodFortrigger(ClientPlanListToUpdate);
    }
    
    //Method to Update Email fields from Account Object. For sending Email using WorkFlow
    
    public static void CommonMethodFortrigger(List<Service_Improvement_Plan__c> ClientPlanList){
        
        Set<Id> AccountIdSet = new Set<Id>();
        Map<Id, Account> AccountMap = new Map<Id, Account>();
        
        for(Service_Improvement_Plan__c ServicePlanAccountId : ClientPlanList)
            AccountIdSet.add(ServicePlanAccountId.Account__c);
        
        If(AccountIdSet.size() > 0){
            
        for(Account AccountRecord : [Select Id, Owner.Email, SRM_Managers__r.Email,Owner.Manager.Email,Business_Managers__r.Email, Sales_Manager_Primary__r.Email, General_Manager__r.Email, DBAM_User__r.Email from Account where Id IN : AccountIdSet])
            AccountMap.put(AccountRecord.Id, AccountRecord);
            
        }
         /****************************** Email Field Update Logic Begins ******************************************************/
        
        if(AccountMap.size() > 0){
            for(Service_Improvement_Plan__c ServicePlanEmailMapping : ClientPlanList){
                if(AccountMap.containsKey(ServicePlanEmailMapping.Account__c)){
                    If(AccountMap.get(ServicePlanEmailMapping.Account__c).Owner.Email != Null)
                        ServicePlanEmailMapping.Account_Owner_Email__c = AccountMap.get(ServicePlanEmailMapping.Account__c).Owner.Email;
                    else
                        ServicePlanEmailMapping.Account_Owner_Email__c = '';
                    If(AccountMap.get(ServicePlanEmailMapping.Account__c).Owner.Manager.Email != Null)
                        ServicePlanEmailMapping.Account_Owner_Manager_Email__c = AccountMap.get(ServicePlanEmailMapping.Account__c).Owner.Manager.Email;
                    else
                        ServicePlanEmailMapping.Account_Owner_Manager_Email__c = '';
                    If(AccountMap.get(ServicePlanEmailMapping.Account__c).SRM_Managers__r.Email != Null)
                        ServicePlanEmailMapping.SRM_Manager_Email__c = AccountMap.get(ServicePlanEmailMapping.Account__c).SRM_Managers__r.Email;
                    else
                        ServicePlanEmailMapping.SRM_Manager_Email__c = '';
                    If(AccountMap.get(ServicePlanEmailMapping.Account__c).Business_Managers__r.Email != Null)
                        ServicePlanEmailMapping.Business_Manager_Email__c = AccountMap.get(ServicePlanEmailMapping.Account__c).Business_Managers__r.Email;
                    else
                        ServicePlanEmailMapping.Business_Manager_Email__c = '';
                    If(AccountMap.get(ServicePlanEmailMapping.Account__c).Sales_Manager_Primary__r.Email != Null)
                        ServicePlanEmailMapping.Sales_Manager_Email__c = AccountMap.get(ServicePlanEmailMapping.Account__c).Sales_Manager_Primary__r.Email;
                    else
                        ServicePlanEmailMapping.Sales_Manager_Email__c = '';
                    If(AccountMap.get(ServicePlanEmailMapping.Account__c).General_Manager__r.Email != Null)
                        ServicePlanEmailMapping.General_Manager_Email__c = AccountMap.get(ServicePlanEmailMapping.Account__c).General_Manager__r.Email;
                    else
                        ServicePlanEmailMapping.General_Manager_Email__c = '';
                    If(AccountMap.get(ServicePlanEmailMapping.Account__c).DBAM_User__r.Email != Null)
                        ServicePlanEmailMapping.DBAM_Email__c = AccountMap.get(ServicePlanEmailMapping.Account__c).DBAM_User__r.Email;
                    else
                        ServicePlanEmailMapping.DBAM_Email__c = '';
                }
            }
        }
        
        /****************************** Email Field Update Logic Ends ******************************************************/
        
    }
}