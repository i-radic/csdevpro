public with sharing class updateFlowCRFAddress {
        
    public Flow_Address_Details__c addressDet{get;set;} 
    public String retUrl{get;set;}
    public string CRfRecordTypeName{get;set;} 

    public updateFlowCRFAddress(ApexPages.StandardController controller) {
        addressDet = (Flow_Address_Details__c)controller.getRecord();
        if(ApexPages.currentPage().getParameters().get('FlowType') == 'ISDN'){
            addressDet.Flow_CRF__c = ApexPages.currentPage().getParameters().get('CF00N20000002oGtQ_lkid');
        }
        
        List<Flow_CRF__c> FindRecordType = new List<Flow_CRF__c>();
        FindRecordType  = [select id,Recordtype.DeveloperName from Flow_Crf__c where Id =:addressDet.Flow_CRF__c];
        if(FindRecordType.size()>0){
            CRfRecordTypeName = FindRecordType[0].Recordtype.DeveloperName  ;
        } 
       
    }
    
    public PageReference NextPage(){
        return Page.FlowCRFAddress;
    
    }
    
    public PageReference saveAndReturn(){
    
        if(addressDet.Post_Code__c != null && addressDet.Type__c != null){
            
        upsert addressDet;
        PageReference PageRef = new PageReference('/'+addressDet.Flow_CRF__c);                                  
        
        PageRef.setRedirect(true);
        
        return PageRef;
            
       }else{
       
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode and click on Address Search !!'));    
            return null;
       }
    
    }
    
    
     public PageReference AddressSearch() {
      String integrationId;       
      String AType;
      if(addressDet.Type__c=='Cease Address'){
      AType='CeaseAddress';      
      }
      if(addressDet.Type__c=='Provide Address'){      
      AType='ProvideAddress';     
      }
      if(addressDet.Type__c=='Alternate Billing Details')
      AType='AlternateBillingAddress';

      if(addressDet.Type__c=='Delivery Address')
      AType='DeliveryAddress';
      
      if(addressDet.Type__c=='Existing Address')
      Atype='ExistingAddress';
      
      if(addressDet.Type__c=='Billing Address')
      Atype='BillingAddress';
      
      if(addressDet.Type__c=='Correspondence Address')
      Atype='CorrespondenceAddress';
      
        try {
        System.debug('valueab'+AType);
        
        System.debug('valueab3'+AType);
            if (Test_Factory.GetProperty('IsTest') == 'yes'){
                    integrationId = 'TEST';
                    addressDet.Post_Code__c = 'TEST';
            }   
                                       
            if (addressDet.Post_Code__c != null){
            System.debug('valueab4'+AType);
                string pCode = addressDet.Post_Code__c;
                List<String> pCodeParts = new List<String>();
                pCodeParts = pCode.split(' ');
                Integer pCodePartsSize = pCodeParts.size();
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    pCodePartsSize = 3;
                }
                if (pCodePartsSize > 2){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Too many spaces in postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                    else {
                        return null;
                    }    
                }
                if (pCodePartsSize == 2){
                    //valid!    
                }
                else {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add space to postcode!'));
                    if(Test_Factory.GetProperty('IsTest') == 'yes'){
                    }
                   else {
                        return null;
                    }
                }
                
                upsert addressDet;
                PageReference PageRef = new PageReference('/apex/flowaddressResultsCRF?id='+ addressDet.Id + '&p='+addressDet.Post_Code__c + '&refPage=update'+'&AddType='+AType);                                  
                PageRef.setRedirect(true);
                return PageRef;
            }                          
            else {
                ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a postcode!'));    
                return null;
            } 
            }
            catch (System.DmlException ex){
            
            for (Integer i = 0; i < ex.getNumDml(); i++) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getDmlMessage(i)));
            }
            return null;
           
            }
                               
}

    

}