@isTest
private class Test_CRFBefore {

    static testMethod void myUnitTest() {
        //  set up data
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99990';
        insert a;
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';        
        insert c;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o; 
        //   insert CRF and invoke trigger being tested
        CRF__c crf = new CRF__c(Opportunity__c = o.Id,password__c='qwerty123', Contact__c = c.Id,Sort_Code__c='123123',Customer_bank_account_number__c='12312312', Order_type__c = 'Linked Delivery', Payment_Method__c = 'DD details entered' , DD_Confirm__c = true, Override_Account_Name__c='Simon',Alt_contact_phone_number__c='0123456789',Your_sales_channel_ID__c='B101');
        insert crf;
        //   check autopopulation of CRF address fields completed
        crf = [select 
                Billing_Building_Name__c,
                Billing_Sub_Building__c,
                Billing_Number__c,
                Billing_POBox__c,
                Billing_Street__c,
                Billing_Locality__c,
                Billing_Post_Town__c,
                Billing_Post_Code__c,
                Billing_County__c,
                Billing_Country__c,
                Building_Name__c,
                Sub_Building__c,
                Number__c,
                POBox__c,
                Street__c,
                Locality__c,
                Post_Town__c,
                Post_Code__c,
                County__c,
                Country__c
                from CRF__c where Id =:crf.Id];
        
        System.assertEquals(crf.Billing_Building_Name__c,c.Contact_Building__c);
        System.assertEquals(crf.Billing_Sub_Building__c,c.Contact_Sub_Building__c);
        System.assertEquals(crf.Billing_Number__c,c.Contact_Address_Number__c);
        System.assertEquals(crf.Billing_POBox__c,c.Address_POBox__c);
        System.assertEquals(crf.Billing_Street__c,c.Contact_Address_Street__c);
        System.assertEquals(crf.Billing_Locality__c,c.Contact_Locality__c);
        System.assertEquals(crf.Billing_Post_Town__c,c.Contact_Post_Town__c);
        System.assertEquals(crf.Billing_Post_Code__c,c.Contact_Post_Code__c);
        System.assertEquals(crf.Billing_County__c,c.Address_County__c);
        System.assertEquals(crf.Billing_Country__c,c.Address_Country__c);
        System.assertEquals(crf.Building_Name__c,c.Contact_Building__c);
        System.assertEquals(crf.Sub_Building__c,c.Contact_Sub_Building__c);
        System.assertEquals(crf.Number__c,c.Contact_Address_Number__c);
        System.assertEquals(crf.POBox__c,c.Address_POBox__c);
        System.assertEquals(crf.Street__c,c.Contact_Address_Street__c);
        System.assertEquals(crf.Locality__c,c.Contact_Locality__c);
        System.assertEquals(crf.Post_Town__c,c.Contact_Post_Town__c);
        System.assertEquals(crf.Post_Code__c,c.Contact_Post_Code__c);
        System.assertEquals(crf.County__c,c.Address_County__c);
        System.assertEquals(crf.Country__c,c.Address_Country__c);       
    }
}