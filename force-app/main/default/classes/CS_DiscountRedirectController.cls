public class CS_DiscountRedirectController {

    private String basketId = '';
    public Boolean asyncMode {get; set;}
    private Id asyncJobId;
    public CS_DiscountRedirectController(){
        basketId = ApexPages.currentPage().getParameters().get('basketId');
    }
    public PageReference redirectToPage() {
        PageReference pr;
        String URL = System.URL.getSalesforceBaseURL().getHost();
        pr = new PageReference('/apex/csdiscounts__DiscountPage?basketId='+basketId);
        if (asyncJobId != null) {
             System.debug('asyncJobId:::'+asyncJobId);
             AsyncApexJob asyncJob = [SELECT Status FROM AsyncApexJob  WHERE Id = :asyncJobId];
             if (asyncJob.Status == 'Completed') {
                 return pr; 
             }else{ 
                 return null;
             }
        }else {
            return pr;
        }
     }
     public PageReference doDiscountCalculations() {
        System.debug('basketId:::'+basketId);
        cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
        Map<Id, cscfga__Product_Configuration__c> configs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{new cscfga__Product_Basket__c(Id = basketId)});

         if (CS_ProductBasketService.containsCalculateProductGroup(basketId)) {
            asyncMode = false;
            if(!test.isRunningTest())
                cspl.AttachmentUtil.upsertAttachments(configs.keySet());
            CS_SolutionDealCalculations dealCalculations = new CS_SolutionDealCalculations(basket);
            dealCalculations.calculateBasketAttributes();
            dealCalculations.calculateBasketApprovals();
            update basket;
            return redirectToPage();
        }


        System.debug('test:::'+CS_CR_Basket_Calculations__c.getInstance().Sync_Basket_Size_Limit__c+':::'+configs.size());

        if (configs.size() <= CS_CR_Basket_Calculations__c.getInstance().Sync_Basket_Size_Limit__c) {
            asyncMode = false;
            if(!test.isRunningTest())
                CS_ProductConfigurationService.calculateApprovals(CS_Util.getConfigurationsInBasket(basketId, 'CustomButtonBTDiscounting'), null, 'CustomButtonBTDiscounting', true);
            return redirectToPage();
        } else {
            asyncMode = true;
            if(!test.isRunningTest())
             asyncJobId = System.enqueueJob(new CS_CustomRedirectQueueable(basket, configs,'Discount'));
            return null;
        }
    }
}