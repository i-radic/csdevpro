@isTest(SeeAllData=true)

private class Test_LeaseDeskScheduledCSVLoad {	
    
    static testmethod void Test1(){
	    Test.startTest();
	    LeaseDeskScheduledCSVLoad sched = new LeaseDeskScheduledCSVLoad();
	    Id job_id = System.schedule('test', '0 0 0 30 12 ? 2099', sched);    
	    LeaseDeskScheduledCSVLoad.StatusToRAGMapping('Prospect.Preclearance');
	    LeaseDeskScheduledCSVLoad.StatusToRAGMapping('Prospect.Acceptance');
	    LeaseDeskScheduledCSVLoad.StatusToRAGMapping('Prospect.Rejection');
	    LeaseDeskScheduledCSVLoad.QuarterlyPayment('Monthly', 500);
	    LeaseDeskScheduledCSVLoad.QuarterlyPayment('Quarterly', 500);
	    LeaseDeskScheduledCSVLoad.QuarterlyPayment('Annually', 500);
	    LeaseDeskScheduledCSVLoad.QuarterlyPayment('Two Monthly', 500);
	    LeaseDeskScheduledCSVLoad.QuarterlyPayment('Four Monthly', 500);
	    LeaseDeskScheduledCSVLoad.QuarterlyPayment('Six Monthly', 500);
	    Test.stopTest();
    }
}