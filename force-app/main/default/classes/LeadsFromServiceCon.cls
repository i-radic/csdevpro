public class LeadsFromServiceCon {
    
    public Opportunity O{get;set;}
    public string BBTelephoneNumber{get;set;} 
    //public List<SelectOption> getSiteOptions {get;set;}
    //public List<SelectOption> getSalutationOptions {get;set;}
    //public List<SelectOption> getProductOptions {get;set;}
    
    
    public LeadsFromServiceCon(ApexPages.StandardController controller) {
        O = new Opportunity();
        this.O =(Opportunity) controller.getRecord(); 
        O.RecordTypeId='01220000000ABFB';
        //this.O =(Opportunity) controller.getRecord(); 
        Cookie Sitecookie = ApexPages.currentPage().getCookies().get('SiteName');
        Cookie EINcookie = ApexPages.currentPage().getCookies().get('EIN');
        Cookie OEmailcookie = ApexPages.currentPage().getCookies().get('OEmail');
        if (Sitecookie != null) {
            O.Service_Site__c = Sitecookie.getValue();        
        }
        if (EINCookie != null) {
            O.Originator_EIN__c = EINCookie.getValue();        
        }if (OEmailcookie != null) {
            O.Originator_Email__c = OEmailCookie.getValue();        
        }
        //get selectOptions
        //getSiteOptions = BTUtilities.getPicklistValues('Opportunity', 'Service_Site__c');
        //getSalutationOptions = BTUtilities.getPicklistValues('Opportunity', 'DM_Salutaion__c');
        //getProductOptions = BTUtilities.getPicklistValues('Opportunity', 'Product__c');
        
    }
    public static DateTime Submit(DateTime myDate) {
        Integer days=5;
        //system.debug('h*********fgdfg'+Date.valueOf(myDate));
        DateTime startDate=Date.valueOf(myDate);
        DateTime endDate = startDate.addDays(days );
        Integer iCount = 0;
        while (startDate< =endDate ) {
            if (startDate.format('E') == 'Sat' | startDate.format('E') == 'Sun'){
                iCount = iCount + 1;
            }
            if(startDate==endDate)
            {
                startDate= startDate.addDays(1);
                if (startDate.format('E') == 'Sat'){
                    iCount = iCount + 2;
                }
                else if(startDate.format('E') == 'Sun')
                {
                    iCount = iCount + 1;
                }
            }
            startDate= startDate.addDays(1);
        }
        endDate=endDate.addDays(iCount);
        system.debug('End*********'+endDate);
        return endDate ;
    }
    
    public void saveOpportunity(){
        
        //system.debug('----srisri--');  
        //system.debug('ssssssssssssss'+O.Qty__c);
                
        O.Name = 'LFS_'+O.Business_Name__c;
        O.Lead_Indicator__c = 'LFS Lead';
        if(O.Product__c == 'Service Self-fulfil' || O.Product__c == 'Mobile Self Fulfil'){
            O.CloseDate=Date.today();
        }
        else{
            O.CloseDate= Date.valueOf(LeadsFromServiceCon.Submit(System.Now()));
        }
        if(O.Product__c == 'Service Self-fulfil' || O.Product__c == 'Mobile Self Fulfil'){
            O.StageName = O.StageName;
        }else{
            O.StageName = 'Created';
        }
        
        List<RecordType> RType =[select id from RecordType where DeveloperName = 'Leads_from_Service' AND SobjectType = 'Opportunity'];
        if(RType.Size() > 0){
            
            O.RecordTypeId = RType[0].id;
        }
        O.Lead_Source__c = 'Leads from Service'; 
        if(O.Customer_Contact_Salutation__c == '' || O.Customer_Contact_Salutation__c == '--None--')
            O.Customer_Contact_Salutation__c = '';
        O.Tariff__c = O.Tariff__c;
        O.Handset__c = O.Handset__c;
        O.Revenue__c = O.Revenue__c;
        
        if (O.Product__c == 'Broadband')
        {
            O.Lead_Information__c = 'Broadband Telephone Number: ' + BBTelephoneNumber + ' \n Lead Info Notes: ' + O.Lead_Information__c;
        }
        else
        {
            O.Lead_Information__c = O.Lead_Information__c;
        } 
        /*CR10834 Start*/
        System.debug('======> Sales agent ==>'+O.Mandatory__c);
        if(O.Mandatory__c=='Yes')
            O.Sales_Agent_EIN__c ='803206562';
        /*CR10834 END*/
        system.debug('Originator_EIN__c ----- '+O.Originator_EIN__c);        
       /** if(O.Originator_EIN__c != null){
            List<User> U = [select id,EIN__c,Email from User Where EIN__c=:O.Originator_EIN__c];
            if(U.size()>0)
               O.Originator_Email__c = U[0].Email; 
           Commented to avoid Anonomise User Email id's.
        } */
        
        //O.Outright_Sale__c = O.Outright_Sale__c; 
        
        if(O.Outright_Sale__c){
            O.Contract_Term__c='0 Months';}      
        
        //system.debug('****************'+O.id+'------------------');        
        if(O.id == null){            
            insert O;             
        }        
        //cookieJar pageCookies = new cookieJar(O.Service_Site__c O.Originator_EIN__c);        
        List<Opportunity> Op = [select Name,Opportunity_Id__c,Originator_EIN__c,Originator_Email__c,
                                Service_site__c from Opportunity Where Id=:O.id];
        if(op.size() >0){                                
            O = Op[0];
        }        
    }
    
    public Pagereference CreateNewOpp(){
        setCookie();
        //pagereference Pg= new Pagereference('/apex/LeadsFromService?SiteName='+O.Service_Site__c+'&EIN='+O.Originator_EIN__c);
        pagereference Pg= new Pagereference ('/apex/LeadsFromService');
        pg.setredirect(true);
        return pg;        
    }
    
    public void setCookie() {
        Cookie SiteCookie= new Cookie('SiteName',O.Service_Site__c, null, 315569260, false); //Here 315569260 represents cookie expiry date = 10 years. You can set this to what ever expiry date you want. Read apex docs for more details.
        Cookie EINCookie = new Cookie('EIN',O.Originator_EIN__c, null, 315569260, false); //Here 315569260 represents cookie expiry date = 10 years. You can set this to what ever expiry date you want. Read apex docs for more details.
        // system.debug('*************** '+EINCookie);
        Cookie OEmailcookie = new Cookie('OEmail',O.Originator_Email__c, null, 315569260, false); 
        ApexPages.currentPage().setCookies(new Cookie[] {SiteCookie,EINCookie,OEmailcookie});
    }
}