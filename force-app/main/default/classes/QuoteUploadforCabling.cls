public class QuoteUploadforCabling {
    Public Attachment quotedoc{get;set;}  
    public String basketId;
        public QuoteUploadforCabling() {
              basketId = System.currentPagereference().getParameters().get('basketid');
              List<Attachment> attlist =[select id,body,name,Description from attachment where parentId=:basketId and Description='Cabling' Limit 1];
              quotedoc = new Attachment();
            if(attlist.size() > 0){
                quotedoc.Body = attlist[0].body;
                quotedoc.name = attlist[0].Name;
            }      
        }
    Public Pagereference Savedoc()
    {
       
        //String basketId = System.currentPagereference().getParameters().get('basketid');
        system.debug('=====> +' +quotedoc);
        Attachment a = new Attachment(parentId = basketId, name=quotedoc.name, body = quotedoc.body,Description='Cabling');
        insert a;
        //Pagereference pg = page.QuoteDocumentUpload;
        PageReference  pg =new PageReference(+'/apex/QuoteDocumentUpload?basketid='+basketId);
        pg.setRedirect(true);
        return pg;
    }    
}