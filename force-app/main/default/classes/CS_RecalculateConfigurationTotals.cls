/**
 * Recalculates totals for Basket calculations
 * @author Kristijan Kosutic
 */
public class CS_RecalculateConfigurationTotals {
	
	Map<Id, Integer> prodConfigMap;
	Map<Integer, cscfga__Product_Configuration__c> prodConfigMap2;
	
	/**
	 * Constructor
	 * @param configId
	 */
	public CS_RecalculateConfigurationTotals(Id configId) {
		
		List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();
		String configurationQuery = 'select '
			+ CS_Utils.getSobjectFields('cscfga__Product_Configuration__c')
			+ ', cscfga__Product_Definition__r.Name'
			+ ', cscfga__Parent_Configuration__r.Group_No_of_Users__c'
			+ ' from cscfga__Product_Configuration__c'
			+ ' where Id = \'' + configId + '\''
			+ ' or cscfga__Root_Configuration__c = \'' + configId + '\'';
		productConfigurations = Database.query(configurationQuery);
		system.debug(productConfigurations.size());
		Decimal Tenure = 0;
		cscfga__Product_Basket__c basket;
		Id basketId;
		for (cscfga__Product_Configuration__c pc : productConfigurations) {
			basketId = pc.cscfga__Product_Basket__c;
			if (pc.cscfga__Root_Configuration__c == null) {
				Tenure = pc.Tenure__c;
			}
		}
		basket = [select id from cscfga__Product_Basket__c where id = :basketId];
		CS_PLReportController reportController;
		List<cscfga__Product_Configuration__c> clonedConfigs = cloneConfigs(productConfigurations);
		Decimal smsRecurringCharge = 0;
		Decimal smsRecurringCost = 0;
		Decimal smsOneOffCharge = 0;
		Decimal smsOneOffCost = 0;
		Decimal dataRecurringCharge = 0;
		Decimal dataRecurringCost = 0;
		Decimal dataOneOffCharge = 0;
		Decimal dataOneOffCost = 0;
		Decimal voiceRecurringCharge = 0;
		Decimal voiceRecurringCost = 0;
		Decimal voiceOneOffCharge = 0;
		Decimal voiceOneOffCost = 0;
		Decimal incomingRecurringCharge = 0;
		for (cscfga__Product_Configuration__c pc : clonedConfigs) {
			if (pc.SMS_Recurring_Charge__c == null) {
				pc.SMS_Recurring_Charge__c = 0;
			}
			if (pc.SMS_Recurring_Cost__c == null) {
				pc.SMS_Recurring_Cost__c = 0;
			}
			if (pc.SMS_One_Off_Cost__c == null) {
				pc.SMS_One_Off_Cost__c = 0;
			}
			if (pc.SMS_One_Off_Charge__c == null) {
				pc.SMS_One_Off_Charge__c = 0;
			}
			if (pc.Data_One_Off_Cost__c == null) {
				pc.Data_One_Off_Cost__c = 0;
			}
			if (pc.Data_One_Off_Charge__c == null) {
				pc.Data_One_Off_Charge__c = 0;
			}
			if (pc.Data_Recurring_Cost__c == null) {
				pc.Data_Recurring_Cost__c = 0;
			}
			if (pc.Data_Recurring_Charge__c == null) {
				pc.Data_Recurring_Charge__c = 0;
			}
			if (pc.Mobile_Voice_One_Off_Cost__c == null) {
				pc.Mobile_Voice_One_Off_Cost__c = 0;
			}
			if (pc.Mobile_Voice_One_Off_Charge__c == null) {
				pc.Mobile_Voice_One_Off_Charge__c = 0;
			}
			if (pc.Mobile_Voice_Recurring_Cost__c == null) {
				pc.Mobile_Voice_Recurring_Cost__c = 0;
			}
			if (pc.Mobile_Voice_Recurring_Charge__c == null) {
				pc.Mobile_Voice_Recurring_Charge__c = 0;
			}
			if (pc.Incoming_Recurring_Charge__c == null) {
				pc.Incoming_Recurring_Charge__c = 0;
			}
			if (pc.cscfga__Product_Definition__r.Name == 'User Group') {
				smsRecurringCharge += pc.SMS_Recurring_Charge__c;
				pc.SMS_Recurring_Charge__c = 0;
				smsRecurringCost += pc.SMS_Recurring_Cost__c;
				pc.SMS_Recurring_Cost__c = 0;
				smsOneOffCharge += pc.SMS_One_Off_Charge__c;
				pc.SMS_One_Off_Charge__c = 0;
				smsOneOffCost += pc.SMS_One_Off_Cost__c;
				pc.SMS_One_Off_Cost__c = 0;
				dataRecurringCharge += pc.Data_Recurring_Charge__c;
				pc.Data_Recurring_Charge__c = 0;
				dataRecurringCost += pc.Data_Recurring_Cost__c;
				pc.Data_Recurring_Cost__c = 0;
				dataOneOffCharge += pc.Data_One_Off_Charge__c;
				pc.Data_One_Off_Charge__c = 0;
				dataOneOffCost += pc.Data_One_Off_Cost__c;
				pc.Data_One_Off_Cost__c = 0;
				voiceRecurringCharge += pc.Mobile_Voice_Recurring_Charge__c;
				pc.Mobile_Voice_Recurring_Charge__c = 0;
				voiceRecurringCost += pc.Mobile_Voice_Recurring_Cost__c;
				pc.Mobile_Voice_Recurring_Cost__c = 0;
				voiceOneOffCharge += pc.Mobile_Voice_One_Off_Charge__c;
				pc.Mobile_Voice_One_Off_Charge__c = 0;
				voiceOneOffCost += pc.Mobile_Voice_One_Off_Cost__c;
				pc.Mobile_Voice_One_Off_Cost__c = 0;
				incomingRecurringCharge += pc.Incoming_Recurring_Charge__c;
				pc.Incoming_Recurring_Charge__c = 0;
			}
			if (pc.cscfga__Product_Definition__r.Name == 'Single Add On') {
				if (pc.Group_No_of_Users__c != pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c) {
					if (pc.SMS_Recurring_Charge__c != null) {
						pc.SMS_Recurring_Charge__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.SMS_Recurring_Cost__c != null) {
						pc.SMS_Recurring_Cost__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.SMS_One_Off_Cost__c != null) {
						pc.SMS_One_Off_Cost__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.SMS_One_Off_Charge__c != null) {
						pc.SMS_One_Off_Charge__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Data_One_Off_Cost__c != null) {
						pc.Data_One_Off_Cost__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Data_One_Off_Charge__c != null) {
						pc.Data_One_Off_Charge__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Data_Recurring_Cost__c != null) {
						pc.Data_Recurring_Cost__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Data_Recurring_Charge__c != null) {
						pc.Data_Recurring_Charge__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Mobile_Voice_One_Off_Cost__c != null) {
						pc.Mobile_Voice_One_Off_Cost__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Mobile_Voice_One_Off_Charge__c != null) {
						pc.Mobile_Voice_One_Off_Charge__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Mobile_Voice_Recurring_Cost__c != null) {
						pc.Mobile_Voice_Recurring_Cost__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Mobile_Voice_Recurring_Charge__c != null) {
						pc.Mobile_Voice_Recurring_Charge__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					if (pc.Net_Line_Rental__c != null) {
						pc.Net_Line_Rental__c *= (pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c / pc.Group_No_of_Users__c);
					}
					pc.Group_No_of_Users__c = pc.cscfga__Parent_Configuration__r.Group_No_of_Users__c;
				}
			}
		}
		Decimal TotalCharges = 0;
		Decimal TotalCosts = 0;
		for (cscfga__Product_Configuration__c pc : clonedConfigs) {
			if (pc.cscfga__Product_Definition__r.Name == 'Mobile Voice') {
				pc.SMS_Recurring_Charge__c += smsRecurringCharge;
				pc.SMS_Recurring_Cost__c += smsRecurringCost;
				pc.SMS_One_Off_Cost__c += smsOneOffCost;
				pc.SMS_One_Off_Charge__c += smsOneOffCharge;
				pc.Data_One_Off_Cost__c += dataOneOffCost;
				pc.Data_One_Off_Charge__c += dataOneOffCharge;
				pc.Data_Recurring_Cost__c += dataRecurringCost;
				pc.Data_Recurring_Charge__c += dataRecurringCharge;
				pc.Mobile_Voice_One_Off_Cost__c += voiceOneOffCost;
				pc.Mobile_Voice_One_Off_Charge__c += voiceOneOffCharge;
				pc.Mobile_Voice_Recurring_Cost__c += voiceRecurringCost;
				pc.Mobile_Voice_Recurring_Charge__c += voiceRecurringCharge;
				pc.Incoming_Recurring_Charge__c += incomingRecurringCharge;
			}
			
			pc.Tenure__c = Tenure;
			Decimal Opex = pc.OPEX_Recurring_Cost__c;
			Decimal Service = pc.Service_Recurring_Cost__c;
			pc.OPEX_Recurring_Cost__c = 0;
			pc.Service_Recurring_Cost__c = 0;
			Decimal Charges = 0;
			Decimal Costs = 0;
			/*
			reportController = new CS_PLReportController(null, new cscfga__Product_Basket__c(), new List<cscfga__Product_Configuration__c>{pc});
			List<CS_PLReportController.Data> datalist =  reportController.Data;
			for (CS_PLReportController.Data data : datalist) {
				if (Decimal.valueOf(data.Name) == Tenure) {
					Charges += data.data1;
					Costs += data.data2;
				}
			}*/
			Decimal recurringCharge = 0;
			Decimal oneOffCharge = 0;
			Decimal recurringCost = 0;
			Decimal oneOffCost = 0;
			recurringCharge += pc.SMS_Recurring_Charge__c != null ? pc.SMS_Recurring_Charge__c : 0;
			recurringCharge += pc.Data_Recurring_Charge__c != null ? pc.Data_Recurring_Charge__c : 0;
			recurringCharge += pc.Mobile_Voice_Recurring_Charge__c != null ? pc.Mobile_Voice_Recurring_Charge__c : 0;
			recurringCharge += pc.Incoming_Recurring_Charge__c != null ? pc.Incoming_Recurring_Charge__c : 0;
			recurringCharge += pc.Other_Recurring_Charge__c != null ? pc.Other_Recurring_Charge__c : 0;
			recurringCharge += pc.Fixed_Recurring_Charge__c != null ? pc.Fixed_Recurring_Charge__c : 0;
			recurringCharge += pc.Device_Recurring_Charge__c != null ? pc.Device_Recurring_Charge__c : 0;
			recurringCharge += pc.Device_Recurring_Cost__c != null ? pc.Device_Recurring_Cost__c : 0;
			recurringCost += pc.SMS_Recurring_Cost__c != null ? pc.SMS_Recurring_Cost__c : 0;
			recurringCost += pc.Data_Recurring_Cost__c != null ? pc.Data_Recurring_Cost__c : 0;
			recurringCost += pc.Mobile_Voice_Recurring_Cost__c != null ? pc.Mobile_Voice_Recurring_Cost__c : 0;
			recurringCost += pc.Other_Recurring_Cost__c != null ? pc.Other_Recurring_Cost__c : 0;
			recurringCost += pc.Fixed_Recurring_Cost__c != null ? pc.Fixed_Recurring_Cost__c : 0;
			oneOffCharge += pc.SMS_One_Off_Charge__c != null ? pc.SMS_One_Off_Charge__c : 0;
			oneOffCharge += pc.Data_One_Off_Charge__c != null ? pc.Data_One_Off_Charge__c : 0;
			oneOffCharge += pc.Mobile_Voice_One_Off_Charge__c != null ? pc.Mobile_Voice_One_Off_Charge__c : 0;
			oneOffCharge += pc.Other_One_Off_Charge__c != null ? pc.Other_One_Off_Charge__c : 0;
			oneOffCharge += pc.Fixed_One_Off_Charge__c != null ? pc.Fixed_One_Off_Charge__c : 0;
			oneOffCharge += pc.Device_One_Off_Charge__c != null ? pc.Device_One_Off_Charge__c : 0;
			oneOffCharge += pc.Device_One_Off_Cost__c != null ? pc.Device_One_Off_Cost__c : 0;
			oneOffCost += pc.SMS_One_Off_Cost__c != null ? pc.SMS_One_Off_Cost__c : 0;
			oneOffCost += pc.Data_One_Off_Cost__c != null ? pc.Data_One_Off_Cost__c : 0;
			oneOffCost += pc.Mobile_Voice_One_Off_Cost__c != null ? pc.Mobile_Voice_One_Off_Cost__c : 0;
			oneOffCost += pc.Other_One_Off_Cost__c != null ? pc.Other_One_Off_Cost__c : 0;
			oneOffCost += pc.Fixed_One_Off_Cost__c != null ? pc.Fixed_One_Off_Cost__c : 0;
			oneOffCost += pc.Sales_Commision_One_Off_Cost__c != null ? pc.Sales_Commision_One_Off_Cost__c : 0;
			Charges = recurringCharge * Tenure + oneOffCharge;
			Costs = recurringCost * Tenure + oneOffCost;
			pc.Total_Charges__c = Charges;
			TotalCharges += Charges;
			pc.Total_Cost__c = Costs;
			TotalCosts += Costs;
			pc.OPEX_Recurring_Cost__c = Opex;
			pc.Service_Recurring_Cost__c = Service;
		}
		for (cscfga__Product_Configuration__c pc : productConfigurations) {
			if (pc.Credit_fund_total__c != null && pc.Tenure__c != null) {
				pc.Credit_fund_total__c = (pc.Credit_fund_total__c * tenure) / pc.Tenure__c;
			}
			pc.Tenure__c = tenure;
			pc.Total_Cost__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Total_Cost__c;
			pc.Total_Charges__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Total_Charges__c;
			if (pc.cscfga__Root_Configuration__c == null) {
				pc.Total_Summarized_Cost__c = TotalCosts;
				pc.Total_Summarized_Revenue__c = TotalCharges;
			}
			if (pc.cscfga__Product_Definition__r.Name == 'Data VPN' || pc.cscfga__Product_Definition__r.Name == 'Coverage Solutions') {
				pc.Total_Cost__c = TotalCosts;
				pc.Total_Charges__c = TotalCharges;
			}
			if (pc.cscfga__Product_Definition__r.Name == 'Coverage Solutions Device' || pc.cscfga__Product_Definition__r.Name == 'Leased Line' || pc.cscfga__Product_Definition__r.Name == 'Router Requirements' || pc.cscfga__Product_Definition__r.Name == 'Additional Services') {
				pc.Total_Cost__c = 0;
				pc.Total_Charges__c = 0;
			}
			if (pc.cscfga__Product_Definition__r.Name == 'Single Add On') {
				pc.Group_No_of_Users__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Group_No_of_Users__c;
				if (pc.VAS__c != null && pc.VAS__c > 0) {
					pc.VAS__c = pc.Total_Charges__c;
				} 
				pc.Mobile_Voice_One_Off_Cost__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Mobile_Voice_One_Off_Cost__c;
				pc.Mobile_Voice_One_Off_Charge__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Mobile_Voice_One_Off_Charge__c;
				pc.Mobile_Voice_Recurring_Cost__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Mobile_Voice_Recurring_Cost__c;
				pc.Mobile_Voice_Recurring_Charge__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Mobile_Voice_Recurring_Charge__c;
				pc.Data_One_Off_Cost__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Data_One_Off_Cost__c;
				pc.Data_One_Off_Charge__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Data_One_Off_Charge__c;
				pc.Data_Recurring_Cost__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Data_Recurring_Cost__c;
				pc.Data_Recurring_Charge__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Data_Recurring_Charge__c;
				pc.SMS_One_Off_Charge__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).SMS_One_Off_Charge__c;
				pc.SMS_One_Off_Cost__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).SMS_One_Off_Cost__c;
				pc.SMS_Recurring_Cost__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).SMS_Recurring_Cost__c;
				pc.SMS_Recurring_Charge__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).SMS_Recurring_Charge__c;
				pc.Net_Line_Rental__c = prodConfigMap2.get(prodConfigMap.get(pc.Id)).Net_Line_Rental__c;
			}
		}
		update productConfigurations;
		update basket;
	}
	
	private List<cscfga__Product_Configuration__c> cloneConfigs(List<cscfga__Product_Configuration__c> configsToClone) {
		prodConfigMap2 = new Map<Integer, cscfga__Product_Configuration__c>();
		prodConfigMap = new Map<Id, Integer>();
		Integer i = 0;
		List<cscfga__Product_Configuration__c> newList = new List<cscfga__Product_Configuration__c>();
		for (cscfga__Product_Configuration__c pc : configsToClone) {
			prodConfigMap.put(pc.Id, i);
			cscfga__Product_Configuration__c clonedPc = pc.clone(false, true, false, false);
			prodConfigMap2.put(i, clonedPc);
			newList.add(clonedPc);
			i++;
		}
		return newList;
	}

}