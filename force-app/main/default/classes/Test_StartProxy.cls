@isTest

private class Test_StartProxy{

    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');

        insert Test_Factory.CreateAccountForDummy();

        Account account = Test_Factory.CreateAccount();
        Database.SaveResult[] accountResult = Database.insert(new Account[] {account});

        Contact contact = Test_Factory.CreateContact();
        contact.accountid = accountResult[0].getId();
        Database.SaveResult[] contactResult = Database.insert(new Contact[] {contact});
         
        Event newEvent = Test_Factory.CreateEvent(contactResult[0].getId());
        newEvent.Subject = 'DELETE ME - Unit Test Account';
        newEvent.startdatetime = datetime.newInstance((Date.today() +1).year(),(Date.today() +1).month(),(Date.today() +1).day(),8,0,0);
        newEvent.DurationInMinutes = 100;
        newEvent.OwnerId = UserInfo.getUserId(); 
        insert newEvent;   
    }   
}