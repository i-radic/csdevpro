/**
 *  @author: Usha Panwar
 *  @date: 2015-06-26
 *  @description: Controller to ovverride the New button on the 
 *  product basket to capture the Resign and Public Sector flag
 *  on Product basket creation.   
 *
 */
public with sharing class CS_NewProductBasketController {
    
    public cscfga__Product_Basket__c basket {get; set;}
    
    // to capture opportunity Id 
    private final Id opportunityId;
    
    // to capture product basket id
    private final Id productBasketId;
    
    // to store old value of product basket
    private String basketName;
    
    // to store old value of oldArpmCurrentVal
    private decimal oldArpmCurrentVal;
    
    private decimal oldtotalBuyoutCostVal;

    // to store whether New is clicked or Edit is clicked
    public boolean isNew{get; set;}
    
    public boolean isSMEUser;
    
    // getter for Resign
    public boolean getIsResign() {
        return this.basket.ReSign__c;
    }
    
    // getter for Public Sector
    public boolean getIsPublicSector(){
        
        return this.basket.Public_Sector__c;
    }
    
    // getter for AprmCurrent
    public decimal getAprmCurrent(){
        
        return this.basket.ARPM_Current__c;
    
    }
    
    // getter for totalBuyout Cost
    public decimal getTotalBuyoutCost(){
    
        return this.basket.Total_Buyout_Cost__c;
    }
    
    // getter for Product basket name
    public String getName(){
        
        return this.basket.Name;
        
    }

    // getter for isSMEUser
    public boolean getIsSMEUser(){
        
        return this.isSMEUser;
        
    }
 // to handle exceptions
 public class CS_Exception extends Exception {}
 public CS_NewProductBasketController(apexPages.standardController sc){
    
    // fetch opportunity Id
    opportunityId = ApexPages.currentPage().getParameters().get('oppId');
    
    //get user profile
    Id profileId=userinfo.getProfileId();
    String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
    system.debug('ProfileName'+profileName);
     if(ProfileName == 'SME BTLB Sales' || ProfileName == 'SME System Admin' ||
        ProfileName.contains('BTLB') || ProfileName == 'BPS Internal User' ||
        ProfileName == 'BPS Partner User') {
        isSMEUser = true;
    }else{
        isSMEUser = false;
    }
    
    
    //fetch product basket id to know whether called from New or Edit
    
    productBasketId = ApexPages.currentPage().getParameters().get('id');
    
    if(productBasketId == null){
        isNew= true;
        this.basket = new cscfga__Product_Basket__c(ReSign__c = false,Public_Sector__c=false, ARPM_Current__c=0.00, Name = null,Total_Buyout_Cost__c=0.00);
    }else{
        
        // Edit scenario
        isNew= false;
        
        List<cscfga__Product_Basket__c> basketList = [select Id,Name, ReSign__c, Public_Sector__c, ARPM_Current__c,Total_Buyout_Cost__c
             from cscfga__Product_Basket__c where id = :productBasketId];
        if(basketList.isEmpty()) {
            throw new CS_Exception('Basket not found!');
        }
        this.basket = new cscfga__Product_Basket__c();
        this.basket.Name = basketList[0].Name;
        this.basket.ReSign__c= basketList[0].ReSign__c;
        this.basket.Public_Sector__c= basketList[0].Public_Sector__c;
        this.basket.ARPM_Current__c= basketList[0].ARPM_Current__c;
        this.basket.Total_Buyout_Cost__c=basketList[0].Total_Buyout_Cost__c;
        
        // To store basket Name and ARPM Current Value
        
        basketName = this.basket.Name;
    
        oldArpmCurrentVal = this.basket.ARPM_Current__c;
        
        system.debug('Product bask name in Edit is :' + this.basket.Name);
        system.debug('Resign flag in Edit is :' + this.basket.ReSign__c);
        system.debug('Public sector flag in Edit is :' + this.basket.Public_Sector__c);
        system.debug('APRM value in Edit is :' + this.basket.ARPM_Current__c);
    
    }
 }
    
    public PageReference init() {
       if(isNew) {
          	return null;   
       }
       else {
            return new PageReference('/apex/csbb__BasketbuilderApp?id=' + productBasketId);
       }
    }
 
   public PageReference save(){
    
    
    PageReference pageRef;
     try {
            
               // New Product Basket Scenario
               if(isNew){
                        
               
                   this.basket.Name =  getName();
                   this.basket.cscfga__Opportunity__c=opportunityId;
                   this.basket.ReSign__c= this.getIsResign();
                   this.basket.Public_Sector__c= this.getIsPublicSector();
                   this.basket.ARPM_Current__c= this.getAprmCurrent();
                   this.basket.Total_Buyout_Cost__c = this.getTotalBuyoutCost();
                   
                   insert this.basket;
                   
                   system.debug('id is :' + this.basket.id);
               
               }else{ // Edit Product Basket Scenario
                
                    this.basket.Name =  getName();
                    this.basket.ARPM_Current__c= this.getAprmCurrent();
                    this.basket.id= productBasketId;
                    this.basket.Total_Buyout_Cost__c = this.getTotalBuyoutCost();
                    
                    if((basketName != this.basket.Name) || (oldArpmCurrentVal != this.basket.ARPM_Current__c) || (oldtotalBuyoutCostVal != this.basket.Total_Buyout_Cost__c )){
                        
                            update this.basket;
                    }
                   
              }
   
     } catch(DmlException ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }   
  
   
    pageRef =  new PageReference('/'+this.basket.id);
    
    return pageRef;
   
  }


}