@isTest
private class Test_LeadRegistration{
    
    static testMethod void myUnitTest(){
    
         Lead LL = new Lead();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(LL);
        LeadRegistration leadR = new LeadRegistration(sc);
        leadR.leadvalue.FirstName = 'TestF';
        leadR.leadvalue.LastName = 'TestL';
        leadR.leadvalue.Company ='TestCompany';
        leadR.leadvalue.PostalCode = '1234';
        leadR.leadvalue.Country = 'testCountry';
        leadR.leadvalue.City = 'testCity';
        leadR.leadvalue.Phone = '+44 207 566 7000';
        leadR.leadvalue.Email = 'test@test.com';
        leadR.address1 = 'test address1';
        leadR.address2 = 'test address2';
      
        leadR.alBTCust='True';
        leadR.venue='Pub';
        leadR.alHaveSky();
        leadR.setVenue();
       
        leadR.alBTCust= null;
        leadR.venue= null;
        leadR.saveRedirect();
    }
      
}