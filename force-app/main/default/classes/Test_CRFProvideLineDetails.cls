@isTest

    private class Test_CRFProvideLineDetails{
    
    static testMethod void myUnitTest(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    Opportunity o = new Opportunity();
    o.Name='TEST';
    o.stageName = 'FOS Stage';
    o.CloseDate = system.today();
    insert o;
    
    CRF__c crf = new CRF__c();
     
    crf.Opportunity__c = o.Id;
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Movers'].Id;    
    crf.Contact__c = c.Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;
    
    CRF__c crf1 = new CRF__c();
    crf1.Opportunity__c = o.Id;
    crf1.RecordTypeId = [Select Id from RecordType where DeveloperName='AX_Form'].Id;    
    crf1.Contact__c = c.Id;
    crf1.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf1;
    
    
    Provide_Line_Details__c LM = new Provide_Line_Details__c();
    LM.Related_To_CRF__c = crf.Id;
    LM.Name = '1';
    insert LM;
    
    Provide_Line_Details__c LM1 = new Provide_Line_Details__c();
    LM1.Related_To_CRF__c = crf1.Id;
    LM1.Name = '1';
    insert LM1;
    
    CRFProvideLineDetails myController=new CRFProvideLineDetails(new ApexPages.StandardController(LM ));
    
    myController.OnStart(); 
    
    CRFProvideLineDetails myController1=new CRFProvideLineDetails(new ApexPages.StandardController(LM1 ));
    
    myController1.OnStart();
    
    Flow_CRF__c flowcrf = new Flow_CRF__c();
     
    flowcrf.Opportunity__c = o.Id;
    flowcrf.RecordTypeId = [Select Id from RecordType where DeveloperName='BusinessMovers'].Id;    
    flowcrf.Contact__c = c.Id;
    flowcrf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert flowcrf;
    
    Flow_CRF__c flowcrf1 = new Flow_CRF__c();
    flowcrf1.Opportunity__c = o.Id;
    flowcrf1.RecordTypeId = [Select Id from RecordType where DeveloperName='AXForm'].Id;    
    flowcrf1.Contact__c = c.Id;
    flowcrf1.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert flowcrf1;
    
    
    Provide_Line_Details__c FlowLM = new Provide_Line_Details__c();
    FlowLM.Related_to_Flow_CRF__c = flowcrf.Id;
    FlowLM.Name = '1';
    insert FlowLM;
    
    Provide_Line_Details__c FlowLM1 = new Provide_Line_Details__c();
    FlowLM1.Related_to_Flow_CRF__c = flowcrf1.Id;
    FlowLM1.Name = '1';
    insert FlowLM1;
    
    CRFProvideLineDetails myFlowController =new CRFProvideLineDetails(new ApexPages.StandardController(FlowLM ));
    
    myFlowController.OnStart(); 
    
    CRFProvideLineDetails myFlowController1 =new CRFProvideLineDetails(new ApexPages.StandardController(FlowLM1 ));
    
    myFlowController1.OnStart();    
    
  
    
    }
}