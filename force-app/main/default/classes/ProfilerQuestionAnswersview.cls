public class ProfilerQuestionAnswersview
{
    public List<QuestionnaireQuestion__c> listOfQuestions {get;set;}
    public list<wrapperClass> lstNewWrap {get;set;}
    public Opportunity objOpp {get;set;}
    public String qtid    {get;set;}
    public Questionnaire__c objQuestionnaire{get;set;}
    public Questionnaire__c Questionnaire  = new Questionnaire__c ();
    //public String strValues {get;set;}
    public map<id,QuestionnaireReply__c> questreplyMap = new map<id,QuestionnaireReply__c>();
    public ProfilerQuestionAnswersview() 
    {
        set<id> quesid = new set<id>();
        List<QuestionnaireReply__c> qustreplyList = new List<QuestionnaireReply__c>();
        Map<id,id> qustansMap = new Map<id,id>();
        String selectedDefault;
        qtid = ApexPages.currentPage().getParameters().get('qt');  
         if(qtid !=null)
        { 
        Questionnaire=[select id,Output__c,Opportunity__c from Questionnaire__c where id =:qtid];
         qustreplyList=[select id,Answer__c,Question__c,Questionnaire__c from QuestionnaireReply__c where Questionnaire__c =:qtid];
        }
        if(Questionnaire.Opportunity__c !=null)
        {
           objOpp = [SELECT Id,Name,StageName,CSC_Call_Pass__c FROM Opportunity WHERE Id=:Questionnaire.Opportunity__c ]; 
        }
            for(QuestionnaireReply__c reply : qustreplyList)
            {
                quesid.add(reply.Question__c);
                questreplyMap.put(reply.Question__c,reply);
                qustansMap.put(reply.Question__c,reply.Answer__c);
            }
            listOfQuestions = [SELECT Id, Question__c,Name,Order__c, (SELECT Answer__c , Score__c , Questionnaire_Question__c , Order__c FROM Questionnaire_Answers__r order by Order__c) 
        FROM QuestionnaireQuestion__c WHERE id IN :quesid order by Order__c];
        lstNewWrap  = new list<wrapperClass>();  
        if(!listOfQuestions.isEmpty()) 
        {
            integer intval=0;
            for(QuestionnaireQuestion__c que: listOfQuestions) 
            { 
               
                List<QuestionnaireAnswer__c> options = new List<QuestionnaireAnswer__c>();
                for(QuestionnaireAnswer__c answers: que.Questionnaire_Answers__r)
                {
                     if(qustansMap.containsKey(que.id) && qustansMap.get(que.id)==answers.id )
                    {
                        selectedDefault=answers.Answer__c ;
                    }
                    if(answers.Questionnaire_Question__c == que.Id) 
                    {                  
                        options.add(answers);  
                    }
                }
                intval++;
                wrapperClass objWrap = new wrapperClass(options,que,selectedDefault);
                objWrap.SrNo = intval;
                lstNewWrap.add(objWrap );  
            }
        }
        objQuestionnaire = Questionnaire;
    } 
    public class wrapperClass 
    {
        public List<QuestionnaireAnswer__c> lstQuestionswrap{get;set;}
        public QuestionnaireQuestion__c Questions{get;set;}
        public String selectedAnswer {get;set;}
        public Integer SrNo {get;set;}
        //public Id selectedAnswerId {get;set;}
        public wrapperClass (List<QuestionnaireAnswer__c> Answers , QuestionnaireQuestion__c Questions,String selectedAnswer)
        {
        this.lstQuestionswrap=Answers;
        this.Questions=Questions; 
        this.selectedAnswer=selectedAnswer;
        SrNo=0;
        }
    }
    
    public pagereference edit() 
    {
        PageReference pageRef = Page.OpportunityProfileredit;
            pageRef.getParameters().put('qt',Questionnaire.id);
            return PageRef;
    }
}