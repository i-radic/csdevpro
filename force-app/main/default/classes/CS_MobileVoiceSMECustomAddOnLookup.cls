global class CS_MobileVoiceSMECustomAddOnLookup extends cscfga.ALookupSearch {
	private String ADD_ON_OBJECT_NAME_CONST = 'cspmb__Add_On_Price_Item__c';
	private String PRODUCT_DEF_NAME = 'Mobile Voice SME';
     
	public override String getRequiredAttributes(){
		//return '["Service Plan Parent", "Type", "Exclude Type", "Record Type", "Disable Roaming", "Name"]'; 
		return '["Parent Service Plan Level", "Type", "Record Type","Name","Tenure"]';
    }
        
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, 
    	Integer pageOffset, Integer pageLimit){

        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
        Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;

    	List<Id> addOnAssocId;
    	List<cspmb__Price_Item_Add_On_Price_Item_Association__c> addOnAssoc;
    	List <cspmb__Add_On_Price_Item__c> singleAddOnPriceItems;
        
		
    	addOnAssoc = [SELECT cspmb__Add_On_Price_Item__c FROM cspmb__Price_Item_Add_On_Price_Item_Association__c WHERE cspmb__Price_Item__c = :searchFields.get('Parent Service Plan Level')];
    	addOnAssocId = new List <Id>();
  
    	for(cspmb__Price_Item_Add_On_Price_Item_Association__c a : addOnAssoc){
        	addOnAssocId.add(a.cspmb__Add_On_Price_Item__c);
        }
        
		String objectFields = CS_Utils.getSobjectFields(ADD_ON_OBJECT_NAME_CONST);
    	if(objectFields == null){
        	return null;
        }
        String addOnType = searchFields.get('Type');
        String recordType = searchFields.get('Record Type');
        String addOnName = searchFields.get('Name');
        String tenure = searchFields.get('Tenure');

        String filter = '';
        String searchTerm = searchFields.get('searchValue');
        if(searchTerm != ''){
            filter = ' AND Name LIKE \'%' + searchTerm + '%\' ';
        }
        
        System.debug('** SIMO: ' + JSON.serializePretty(searchFields));
        
        
        //String addOnRecordsQuery = 'SELECT '+ objectFields + ' FROM cspmb__Add_On_Price_Item__c WHERE cspmb__Is_Active__c = true AND cspmb__Product_Definition_Name__c = :PRODUCT_DEF_NAME  AND Type__c = :addOnType AND Id IN :addOnAssocId AND Record_Type_Name__c  = :recordType AND Type__c NOT IN :typesToExcludeList';
        String addOnRecordsQuery = '';
        if(recordType == 'Shared Add-On'){
             addOnRecordsQuery = 'SELECT '+ objectFields + ' FROM cspmb__Add_On_Price_Item__c WHERE cspmb__Is_Active__c = true AND cspmb__Product_Definition_Name__c = :PRODUCT_DEF_NAME  AND Type__c = :addOnType AND Id IN :addOnAssocId AND Record_Type_Name__c  = :recordType AND Tenure__c = :tenure '+ filter +'LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset';
        }else{
            addOnRecordsQuery = 'SELECT '+ objectFields + ' FROM cspmb__Add_On_Price_Item__c WHERE cspmb__Is_Active__c = true AND cspmb__Product_Definition_Name__c = :PRODUCT_DEF_NAME  AND Type__c = :addOnType AND Id IN :addOnAssocId AND Record_Type_Name__c  = :recordType ' + filter + 'LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset';
        }
       
        singleAddOnPriceItems = Database.query(addOnRecordsQuery);
     

    	return singleAddOnPriceItems;
    }
    
}