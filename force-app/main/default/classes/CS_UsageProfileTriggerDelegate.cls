public with sharing class CS_UsageProfileTriggerDelegate extends CS_TriggerHandler.DelegateBase {
	
	Map<Id, Id> usageProfileaccountIds;
	
	// do any preparation here – bulk loading of data etc
	public override void prepareBefore() {
		usageProfileaccountIds = new Map<Id, Id>();
	}
	
	// do any preparation here - bulk loading of data etc
	public override void prepareAfter() {
		usageProfileaccountIds = new Map<Id, Id>();
	}
	
	// Apply before insert logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	//public override void beforeInsert(sObject o) {	}
	
	// Apply before update logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	//public override void beforeUpdate(sObject old, sObject o) {}
	
	// Apply before delete logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	//public override void beforeDelete(sObject o) {}

	// Apply after insert logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	public override void afterInsert(sObject o) {
		Usage_Profile__c up = (Usage_Profile__c) o;
		if (up.AssociatedLE__c != null && up.Active__c) {
			usageProfileaccountIds.put(up.Id, up.AssociatedLE__c);	
		}
	}

	// Apply after update logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	public override void afterUpdate(sObject old, sObject o) {
		Usage_Profile__c up = (Usage_Profile__c) o;
		Usage_Profile__c oldUp = (Usage_Profile__c) old;
		if (up.Active__c  && !oldUp.Active__c && up.AssociatedLE__c != null) {
			usageProfileaccountIds.put(up.Id, up.AssociatedLE__c);
		}
	}

	// Apply after delete logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	//public override void afterDelete(sObject o) {	}

	// Apply after undelete logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	//public override void afterUndelete(sObject o) {	}

	// finish logic - process stored records and perform any dml action
	// updates product baskets if bundle is desynchronised
	public override void finish() {
		if (!usageProfileaccountIds.isEmpty()) {
			Set<Id> associatedLEs = new Set<Id>(usageProfileaccountIds.values());
			Set<Id> usageProfileIds = usageProfileaccountIds.keySet();
			List<Usage_Profile__c> usageProfiles = [Select Id, Active__c 
				from Usage_Profile__c 
				where Active__c = true 
				and Id not in :usageProfileIds
				and AssociatedLE__c in :associatedLEs
			];
			if (!usageProfiles.isEmpty()) {
				for (Usage_Profile__c up : usageProfiles) {
					up.Active__c = false;
				}
				update usageProfiles;
			}
		}
	}

}