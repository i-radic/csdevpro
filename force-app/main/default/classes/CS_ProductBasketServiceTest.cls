@IsTest
public class CS_ProductBasketServiceTest  {
    private static Account acct;
    private static Account acct1;
    private static Opportunity opp;
    private static Opportunity opp1;
    private static Contact con;
    private static Contact agent;
    private static cscfga__Product_Basket__c basket;
    private static cscfga__Product_Basket__c basket2;
    private static Map<Id, cscfga__Product_Configuration__c> allConfigs;

    static testMethod void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        cspl__PLReportSettings__c setting = new cspl__PLReportSettings__c(
                cspl__Contract_Term_Field__c = 'cscfga__Contract_Term__c',
                cspl__API_Field_name__c = 'Calculations_Product_Group__c'
        );
        insert setting;

        CS_TestDataFactory.generatePLReportConfigRecords();

        acct = new Account(name='test account');
        insert acct;

        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        os.SetupOwnerId = UserInfo.getUserId();
        insert os;

        CS_Approval_Level__c testapproval = new CS_Approval_Level__c(

                Type__c='Acquisition',
                Is_Active__c= true,
                KPI__c='Gross Margin',
                Approval_Level__c =6
        );

        insert testapproval;

        CS_Approval_Level__c testapproval1 = new CS_Approval_Level__c(

                Type__c='Re-sign',
                Is_Active__c= true,
                KPI__c='Revenue Dilution',
                Approval_Level__c =5
        );

        insert testapproval1;

        CS_Approval_Level__c testapproval2 = new CS_Approval_Level__c(

                Type__c='SME',
                Is_Active__c= true,
                KPI__c='Investment Pot Usage Percentage',
                Approval_Level__c =7
        );
        insert testapproval2;

        opp = new Opportunity(AccountId=acct.Id,StageName='Prospecting',CloseDate=date.today(),NextStep='Test',Next_Action_Date__c=date.today(),name='test OpportunityCI' ,
                TotalOpportunityQuantity = 0);
        insert opp;

        Usage_Profile__c testUsageProfiles = new Usage_Profile__c(
                Active__c = true,
                Account__c = acct.Id,
                Expected_SMS__c = 10,
                Expected_Voice__c = 5,
                Expected_Data__c = 10
        );

        insert testUsageProfiles;

        basket = new cscfga__Product_Basket__c(
                Name = 'Test Basket Trigger',
                cscfga__Opportunity__c = opp.Id,
                ReSign__c = true,
                Sales_Commision_One_Off_Cost__c = 100,
                Product_Definitions__c='Future Mobile'
        );

        basket.Approval_Status__c = 'Commercially Approved';
        basket.Has_Special_Conditions__c = true;
        basket.Competitive_Benchmarking__c = true;
        basket.Unlocking_Fees__c = true;
        basket.Total_Contract_Guarantee__c = true;
        basket.Total_Revenue_Guarantee__c = true;
        basket.Competitive_Clause__c = true;
        basket.Disconnect_Allowances__c = true;
        basket.Averaging_Co_Terminus__c = true;
        basket.Full_Co_Terminus__c = true;
        basket.Special_Conditions_Id__c = null;
        basket.Tenure__c = 24;
        basket.Care_Selected__c = false;
        basket.Payment_Terms__c = '45';
        basket.Monthly_Recurring_Charges__c = 10;
        basket.Total_Cost_NPV__c = 0;
        basket.SpecCon_Competitive_Clause_Cost__c = 50;
        basket.Competitive_Clause__c = true;
        basket.Tech_Fund__c = 99;
        basket.Number_of_Devices__c = 0;
        basket.Total_Buyout_Cost__c = 25;
        basket.SpecCon_Full_Co_terminus_Cost__c = 100;
        basket.Disconnection_allowance__c  = 100;
        basket.SpecCon_Unlocking_Fees_Cost__c = 100;



        Integer pbMonth = (Integer) basket.Tenure__c;

        basket.Total_Charges__c=90;
        basket.Total_Cost__c=40;
        basket.Credit_fund_total__c=100;
        insert basket;

        basket2 = new cscfga__Product_Basket__c(
                Name = 'Test Basket Trigger',
                cscfga__Opportunity__c = opp.Id,
                ReSign__c = true,
                Tech_Fund__c = 99,
                Number_of_Devices__c = 10,
                Hardware_Charges__c = -200,
                Sales_Commision_One_Off_Cost__c = 100,
                Product_Definitions__c='Future Mobile'
        );
        insert basket2;

        cscfga__Product_Basket__c basketWithTotals = CS_ProductBasketService.calculateBasketTotals(new Map<Id, cscfga__Product_Configuration__c>(), basket);
        System.assertNotEquals(null, basketWithTotals);

        cscfga__Product_Basket__c basket1 = new cscfga__Product_Basket__c(
                Name = 'Test Basket Trigger1',
                cscfga__Opportunity__c = opp.Id,
                Sales_Commision_One_Off_Cost__c = 100,
                Product_Definitions__c='Future Mobile'

        );
        insert basket1;
        CS_ProductBasketService.calculateBasketTotals(new Map<Id, cscfga__Product_Configuration__c>(), basket1);

        cscfga__Product_Definition__c proddef1 = new cscfga__Product_Definition__c(
                Name = 'proddef1',
                cscfga__Description__c = 'prod description1'
        );
        insert proddef1;

        cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
                Name = 'Ad1',
                cscfga__Data_Type__c = 'String',
                cscfga__Type__c = 'Related Product',
                cscfga__Product_Definition__c = proddef1.Id,
                cscfga__high_volume__c = true
        );
        insert ad1;

        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
                Name = 'Pd2',
                cscfga__Description__c = 'Pd2'
        );
        insert pd2;

        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
                Name = 'Ad2',
                cscfga__Data_Type__c = 'String',
                cscfga__Type__c = 'User Input',
                cscfga__Product_Definition__c = pd2.Id
        );
        insert ad2;

        CS_Related_Products__c rp = new CS_Related_Products__c(
                Name = 'Pd1',
                Related_Products__c = 'Pd2'
        );
        insert rp;

        CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
                Name = 'Pd2',
                Attribute_Names__c = 'Ad2',
                List_View_Attribute_Names__c = 'Ad2',
                Total_Attribute_Names__c = 'Ad2'
        );
        insert rpa;

        allConfigs = new map<Id, cscfga__Product_Configuration__c> ();

        cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
                Name = 'Root',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                Type__c = 'Hardware',
                Tech_Fund__c = 99,
                Calculations_Product_Group__c = 'Future Mobile',
                cscfga__Contract_Term_Period__c = 12, 
                cscfga__Contract_Term__c = 12
        );
        insert pc1;
        allConfigs.put(pc1.Id, pc1);

        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
                Name = 'Child',
                cscfga__Root_Configuration__c = pc1.Id,
                cscfga__Parent_Configuration__c = pc1.Id,
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                Calculations_Product_Group__c = 'Future Mobile',
                cscfga__Contract_Term_Period__c = 12, 
                cscfga__Contract_Term__c = 12
        );
        insert pc2;
        allConfigs.put(pc2.Id, pc2);

        cscfga__Product_Configuration__c pc3 = new cscfga__Product_Configuration__c(
                Name = 'Test Lone Worker',
                cscfga__Product_Basket__c = basket1.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                cscfga__Configuration_Status__c = 'Valid',
                cscfga__Unit_Price__c = 10,
                cscfga__Quantity__c = 1,
                cscfga__Recurrence_Frequency__c = 12,
                tenure__c=36,
                Care__c= 'yes',
                Total_Charges__c=100.00,
                Total_Cost__c=200.00,
                Number_of_users__c=20,
                Data_Recurring_Charge__c=100.00,
                Data_One_Off_Charge__c=100.00,
                Data_One_Off_Cost__c=100.00,
                Data_Recurring_Cost__c=50.00,
                SMS_Recurring_Charge__c=5.00,
                SMS_One_Off_Charge__c = 5.00,
                SMS_Recurring_Cost__c=5.00,
                SMS_One_Off_Cost__c = 5.00,
                Mobile_Voice_One_Off_Cost__c =5.00,
                Mobile_Voice_Recurring_Cost__c=5.00,
                Fixed_One_Off_Charge__c =5.00,
                Fixed_Recurring_Charge__c =5.00,
                Incoming_Recurring_Charge__c=5.00,
                Service_Recurring_Cost__c =5.00,
                OPEX_Recurring_Cost__c=5.00,
                Warning_Summary__c='Test',
                Mobile_Voice_One_Off_Charge__c =5.00,
                Mobile_Voice_Recurring_Charge__c=5.00,
                Gross_Margin__c=85.00,
                SpecCon_Competitive_Clause_Cost__c=40.00,
                SpecCon_Disconnection_Allowance_Cost__c=22.00,
                Credit_fund_total__c=17.00,
                SpecCon_Full_Co_terminus_Cost__c=18.00,
                cscfga__Contract_Term_Period__c = 12, 
                cscfga__Contract_Term__c = 12,
                Calculations_Product_Group__c = 'Future Mobile'
        );
        
        insert pc3;
        allConfigs.put(pc3.Id, pc3);

        cscfga__Product_Configuration__c pc4 = new cscfga__Product_Configuration__c(
                Name = 'Special Conditions',
                cscfga__Product_Basket__c = basket1.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                cscfga__Configuration_Status__c = 'Valid',
                cscfga__Unit_Price__c = 10,
                cscfga__Quantity__c = 1,
                cscfga__Recurrence_Frequency__c = 12,
                tenure__c=36,
                SpecCon_Averaging_co_terminus__c = true,
                SpecCon_Competitive_Benchmarking__c= true,
                SpecCon_Competitive_Clause__c=true,
                SpecCon_Competitive_Clause_Cost__c = 10.00,
                SpecCon_Disconnection_Allowance__c = true,
                SpecCon_Full_co_terminus__c= true,
                SpecCon_Full_Co_terminus_Cost__c= 34.40,
                Payment_Terms__c='45',
                SpecCon_Total_Contract_Guarantee__c= true,
                SpecCon_Total_Revenue_Guarantee__c=true,
                SpecCon_Unlocking_Fees_Cost__c=44.44,
                SPecCon_Unlocking_Fees__c= true,
                cscfga__Contract_Term_Period__c = 12, 
                cscfga__Contract_Term__c = 12,
                Calculations_Product_Group__c = 'Future Mobile'
        );
        insert pc4;
        allConfigs.put(pc4.Id, pc4);

        basket1.Approval_Status__c = 'Approved';
        basket1.Has_Special_Conditions__c = true;
        basket1.Competitive_Benchmarking__c = true;
        basket1.Unlocking_Fees__c = true;
        basket1.Total_Contract_Guarantee__c = true;
        basket1.Total_Revenue_Guarantee__c = true;
        basket1.Competitive_Clause__c = true;
        basket1.Disconnect_Allowances__c = true;
        basket1.Averaging_Co_Terminus__c = true;
        basket1.Full_Co_Terminus__c = true;
        basket1.Special_Conditions_Id__c = pc4.id;
        basket1.Tenure__c = 24;
        basket1.Care_Selected__c = true;
        basket1.Payment_Terms__c = '45';
        basket1.Monthly_Recurring_Charges__c = 10;
        basket1.Total_Charges__c=90;
        basket1.Total_Cost__c=40;
        basket1.SpecCon_Competitive_Clause_Cost__c  = 15;
        basket1.SpecCon_Full_Co_terminus_Cost__c = 15;
        basket1.Disconnection_allowance__c = 25;
        basket1.SpecCon_Unlocking_Fees_Cost__c = 50;
        basket1.Total_Buyout_Cost__c = 100;
        basket1.Rolling_Air_Time_NPV__c  = 25;
        basket1.NPV_Staged_Air_Time__c  = 175;
        basket1.Credit_fund_total__c = 500;
        basket1.Number_of_users__c = 10;
        basket1.recalculateFormulas();
    }

    static testMethod void createTestData2() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        cspl__PLReportSettings__c setting = new cspl__PLReportSettings__c(
                cspl__Contract_Term_Field__c = 'cscfga__Contract_Term__c',
                cspl__API_Field_name__c = 'Calculations_Product_Group__c'
        );
        insert setting;

        CS_TestDataFactory.generatePLReportConfigRecords();

        acct = new Account(name='test account');
        insert acct;

        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        os.SetupOwnerId = UserInfo.getUserId();
        insert os;

        CS_Approval_Level__c testapproval = new CS_Approval_Level__c(

                Type__c='Acquisition',
                Is_Active__c= true,
                KPI__c='Gross Margin',
                Approval_Level__c =6
        );

        insert testapproval;

        CS_Approval_Level__c testapproval1 = new CS_Approval_Level__c(

                Type__c='Re-sign',
                Is_Active__c= true,
                KPI__c='Revenue Dilution',
                Approval_Level__c =5
        );

        insert testapproval1;

        CS_Approval_Level__c testapproval2 = new CS_Approval_Level__c(

                Type__c='SME',
                Is_Active__c= true,
                KPI__c='Investment Pot Usage Percentage',
                Approval_Level__c =7
        );
        insert testapproval2;

        opp = new Opportunity(AccountId=acct.Id,StageName='Prospecting',CloseDate=date.today(),NextStep='Test',Next_Action_Date__c=date.today(),name='test OpportunityCI' ,
                TotalOpportunityQuantity = 0);
        insert opp;

        Usage_Profile__c testUsageProfiles = new Usage_Profile__c(
                Active__c = true,
                Account__c = acct.Id,
                Expected_SMS__c = 10,
                Expected_Voice__c = 5,
                Expected_Data__c = 10
        );

        insert testUsageProfiles;

        basket = new cscfga__Product_Basket__c(
                Name = 'Test Basket Trigger',
                cscfga__Opportunity__c = opp.Id,
                ReSign__c = true,
                Sales_Commision_One_Off_Cost__c = 100,
                Product_Definitions__c='Future Mobile'
        );

        basket.Approval_Status__c = 'Commercially Approved';
        basket.Has_Special_Conditions__c = false;
        basket.Competitive_Benchmarking__c = false;
        basket.Unlocking_Fees__c = false;
        basket.Total_Contract_Guarantee__c = false;
        basket.Total_Revenue_Guarantee__c = false;
        basket.Competitive_Clause__c = false;
        basket.Disconnect_Allowances__c = false;
        basket.Averaging_Co_Terminus__c = false;
        basket.Full_Co_Terminus__c = false;
        basket.Special_Conditions_Id__c = null;
        basket.Tenure__c = 24;
        basket.Care_Selected__c = false;
        basket.Payment_Terms__c = '45';
        basket.Monthly_Recurring_Charges__c = 10;
        basket.Total_Cost_NPV__c = 0;
        basket.SpecCon_Competitive_Clause_Cost__c = 50;
        basket.Competitive_Clause__c = true;
        basket.Tech_Fund__c = 99;
        basket.Number_of_Devices__c = 0;
        basket.Total_Buyout_Cost__c = 25;
        basket.SpecCon_Full_Co_terminus_Cost__c = 100;
        basket.Disconnection_allowance__c  = 100;
        basket.SpecCon_Unlocking_Fees_Cost__c = 100;



        Integer pbMonth = (Integer) basket.Tenure__c;

        basket.Total_Charges__c=90;
        basket.Total_Cost__c=40;
        basket.Credit_fund_total__c=100;
        insert basket;

        basket2 = new cscfga__Product_Basket__c(
                Name = 'Test Basket Trigger',
                cscfga__Opportunity__c = opp.Id,
                ReSign__c = true,
                Tech_Fund__c = 99,
                Number_of_Devices__c = 10,
                Hardware_Charges__c = -200,
                Sales_Commision_One_Off_Cost__c = 100,
                Product_Definitions__c='Future Mobile'
        );
        insert basket2;

        cscfga__Product_Basket__c basketWithTotals = CS_ProductBasketService.calculateBasketTotals(new Map<Id, cscfga__Product_Configuration__c>(), basket);
        System.assertNotEquals(null, basketWithTotals);

        cscfga__Product_Basket__c basket1 = new cscfga__Product_Basket__c(
                Name = 'Test Basket Trigger1',
                cscfga__Opportunity__c = opp.Id,
                Sales_Commision_One_Off_Cost__c = 100,
                Product_Definitions__c='Future Mobile'

        );
        insert basket1;
        CS_ProductBasketService.calculateBasketTotals(new Map<Id, cscfga__Product_Configuration__c>(), basket1);

        cscfga__Product_Definition__c proddef1 = new cscfga__Product_Definition__c(
                Name = 'proddef1',
                cscfga__Description__c = 'prod description1'
        );
        insert proddef1;

        cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
                Name = 'Ad1',
                cscfga__Data_Type__c = 'String',
                cscfga__Type__c = 'Related Product',
                cscfga__Product_Definition__c = proddef1.Id,
                cscfga__high_volume__c = true
        );
        insert ad1;

        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
                Name = 'Pd2',
                cscfga__Description__c = 'Pd2'
        );
        insert pd2;

        cscfga__Product_Definition__c mdm = new cscfga__Product_Definition__c(
                Name = 'MDM',
                cscfga__Description__c = 'MDM'
        );
        insert mdm;
        cscfga__Product_Definition__c btmb = new cscfga__Product_Definition__c(
                Name = 'BT Mobile Broadband',
                cscfga__Description__c = 'BT Mobile Broadband'
        );
        insert btmb;
        cscfga__Product_Definition__c btmf = new cscfga__Product_Definition__c(
                Name = 'BT Mobile Flex',
                cscfga__Description__c = 'BT Mobile Flex'
        );
        insert btmf;
        cscfga__Product_Definition__c btms = new cscfga__Product_Definition__c(
                Name = 'BT Mobile Sharer',
                cscfga__Description__c = 'BT Mobile Sharer'
        );
        insert btms;
        cscfga__Product_Definition__c btop = new cscfga__Product_Definition__c(
                Name = 'BTOP',
                cscfga__Description__c = 'BTOP'
        );
        insert btop;

        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
                Name = 'Ad2',
                cscfga__Data_Type__c = 'String',
                cscfga__Type__c = 'User Input',
                cscfga__Product_Definition__c = pd2.Id
        );
        insert ad2;

        CS_Related_Products__c rp = new CS_Related_Products__c(
                Name = 'Pd1',
                Related_Products__c = 'Pd2'
        );
        insert rp;

        CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
                Name = 'Pd2',
                Attribute_Names__c = 'Ad2',
                List_View_Attribute_Names__c = 'Ad2',
                Total_Attribute_Names__c = 'Ad2'
        );
        insert rpa;

        allConfigs = new map<Id, cscfga__Product_Configuration__c> ();

        cscfga__Product_Configuration__c toDelete = new cscfga__Product_Configuration__c(
                Name = 'To Delete',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                Calculations_Product_Group__c = 'Future Mobile'
        );
        insert toDelete;
        delete toDelete;

        cscfga__Product_Configuration__c mdmpc = new cscfga__Product_Configuration__c(
                Name = 'MDM',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = mdm.Id,
                Commission_Contribution__c = 2
        );
        mdmpc.Calculations_Product_Group__c = 'Future Mobile';
        insert mdmpc;
        allConfigs.put(mdmpc.id, mdmpc);

        cscfga__Product_Configuration__c btmbpc = new cscfga__Product_Configuration__c(
                Name = 'BT Mobile Broadband',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = btmb.Id
        );
        btmbpc.Calculations_Product_Group__c = 'Future Mobile';
        insert btmbpc;
        allConfigs.put(btmbpc.id, btmbpc);

        cscfga__Product_Configuration__c btmfpc = new cscfga__Product_Configuration__c(
                Name = 'BT Mobile Flex',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = btmf.Id
        );
        btmfpc.Calculations_Product_Group__c = 'Future Mobile';
        insert btmfpc;
        allConfigs.put(btmfpc.id, btmfpc);

        cscfga__Product_Configuration__c btmspc = new cscfga__Product_Configuration__c(
                Name = 'BT Mobile Sharer',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = btms.Id
        );
        btmspc.Calculations_Product_Group__c = 'Future Mobile';
        insert btmspc;
        allConfigs.put(btmspc.id, btmspc);

        cscfga__Product_Configuration__c btoppc = new cscfga__Product_Configuration__c(
                Name = 'BTOP',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = btop.Id
        );
        btoppc.Calculations_Product_Group__c = 'Future Mobile';
        insert btoppc;
        allConfigs.put(btoppc.id, btoppc);



        cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
                Name = 'Root',
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                Type__c = 'Hardware',
                Tech_Fund__c = 99
        );
        pc1.Calculations_Product_Group__c = 'Future Mobile';
        insert pc1;
        allConfigs.put(pc1.Id, pc1);

        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
                Name = 'Child',
                cscfga__Root_Configuration__c = pc1.Id,
                cscfga__Parent_Configuration__c = pc1.Id,
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Product_Definition__c = proddef1.Id
        );
        pc2.Calculations_Product_Group__c = 'Future Mobile';
        insert pc2;
        allConfigs.put(pc2.Id, pc2);

        cscfga__Product_Configuration__c pc3 = new cscfga__Product_Configuration__c(
                Name = 'Test Lone Worker',
                cscfga__Product_Basket__c = basket1.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                cscfga__Configuration_Status__c = 'Valid',
                cscfga__Unit_Price__c = 10,
                cscfga__Quantity__c = 1,
                cscfga__Recurrence_Frequency__c = 12,
                tenure__c=36,
                Care__c= 'yes',
                Total_Charges__c=100.00,
                Total_Cost__c=200.00,
                Number_of_users__c=20,
                Data_Recurring_Charge__c=100.00,
                Data_One_Off_Charge__c=100.00,
                Data_One_Off_Cost__c=100.00,
                Data_Recurring_Cost__c=50.00,
                SMS_Recurring_Charge__c=5.00,
                SMS_One_Off_Charge__c = 5.00,
                SMS_Recurring_Cost__c=5.00,
                SMS_One_Off_Cost__c = 5.00,
                Mobile_Voice_One_Off_Cost__c =5.00,
                Mobile_Voice_Recurring_Cost__c=5.00,
                Fixed_One_Off_Charge__c =5.00,
                Fixed_Recurring_Charge__c =5.00,
                Incoming_Recurring_Charge__c=5.00,
                Service_Recurring_Cost__c =5.00,
                OPEX_Recurring_Cost__c=5.00,
                Warning_Summary__c='Test',
                Mobile_Voice_One_Off_Charge__c =5.00,
                Mobile_Voice_Recurring_Charge__c=5.00,
                Gross_Margin__c=85.00,
                SpecCon_Competitive_Clause_Cost__c=40.00,
                SpecCon_Disconnection_Allowance_Cost__c=22.00,
                Credit_fund_total__c=17.00,
                SpecCon_Full_Co_terminus_Cost__c=18.00
        );
        pc3.Calculations_Product_Group__c = 'Future Mobile';
        insert pc3;
        allConfigs.put(pc3.Id, pc3);

        cscfga__Product_Configuration__c pc4 = new cscfga__Product_Configuration__c(
                Name = 'Special Conditions',
                cscfga__Product_Basket__c = basket1.Id,
                cscfga__Product_Definition__c = proddef1.Id,
                cscfga__Configuration_Status__c = 'Valid',
                cscfga__Unit_Price__c = 10,
                cscfga__Quantity__c = 1,
                cscfga__Recurrence_Frequency__c = 12,
                tenure__c=36,
                SpecCon_Averaging_co_terminus__c = true,
                SpecCon_Competitive_Benchmarking__c= true,
                SpecCon_Competitive_Clause__c=true,
                SpecCon_Competitive_Clause_Cost__c = 10.00,
                SpecCon_Disconnection_Allowance__c = true,
                SpecCon_Full_co_terminus__c= true,
                SpecCon_Full_Co_terminus_Cost__c= 34.40,
                Payment_Terms__c='45',
                SpecCon_Total_Contract_Guarantee__c= true,
                SpecCon_Total_Revenue_Guarantee__c=true,
                SpecCon_Unlocking_Fees_Cost__c=44.44,
                SPecCon_Unlocking_Fees__c= true
        );
        pc4.Calculations_Product_Group__c = 'Future Mobile';
        insert pc4;
        allConfigs.put(pc4.Id, pc4);



        basket1.Approval_Status__c = 'Approved';
        basket1.Has_Special_Conditions__c = true;
        basket1.Competitive_Benchmarking__c = true;
        basket1.Unlocking_Fees__c = true;
        basket1.Total_Contract_Guarantee__c = true;
        basket1.Total_Revenue_Guarantee__c = true;
        basket1.Competitive_Clause__c = true;
        basket1.Disconnect_Allowances__c = true;
        basket1.Averaging_Co_Terminus__c = true;
        basket1.Full_Co_Terminus__c = true;
        basket1.Special_Conditions_Id__c =pc4.id;
        basket1.Tenure__c = 24;
        basket1.Care_Selected__c = true;
        basket1.Payment_Terms__c = '45';
        basket1.Monthly_Recurring_Charges__c = 10;
        basket1.Total_Charges__c=90;
        basket1.Total_Cost__c=40;
        basket1.SpecCon_Competitive_Clause_Cost__c  = 15;
        basket1.SpecCon_Full_Co_terminus_Cost__c = 15;
        basket1.Disconnection_allowance__c = 25;
        basket1.SpecCon_Unlocking_Fees_Cost__c = 50;
        basket1.Total_Buyout_Cost__c = 100;
        basket1.Rolling_Air_Time_NPV__c  = 25;
        basket1.NPV_Staged_Air_Time__c  = 175;
        basket1.Credit_fund_total__c = 500;
        basket1.Number_of_users__c = 10;
        basket1.Credit_fund_total__c=100;
        basket1.Total_Buyout_Cost__c=100;
        basket1.Tenure__c = 24;
        basket1.recalculateFormulas();
        
        CS_ProductBasketService.calculateBasketTotals(new map<Id, cscfga__Product_Configuration__c> (), basket1);
        
    }

    testMethod static void createIndirectAccTestData(){
        createTestData();

        acct1 = new Account(name='Indirect test account');
        acct1.Staging_Area_Classification__c ='Non Managed Operational Account';
        insert acct1;

        Opportunity opp11 = new Opportunity(AccountId=acct1.Id,StageName='Prospecting',CloseDate=date.today(),NextStep='Test',Next_Action_Date__c=date.today(),name='test OpportunityCI' ,
                TotalOpportunityQuantity = 0);
        insert opp11;

        cscfga__Product_Basket__c basket1 = new cscfga__Product_Basket__c(
                Name = 'Test Basket Trigger1',
                cscfga__Opportunity__c = opp11.Id,
                ReSign__c = true,
                Sales_Commision_One_Off_Cost__c = 100,
                Product_Definitions__c='Future Mobile'

        );
        
        basket1.Approval_Status__c = 'Commercially Approved';
        basket1.Has_Special_Conditions__c = false;
        basket1.Competitive_Benchmarking__c = false;
        basket1.Unlocking_Fees__c = false;
        basket1.Total_Contract_Guarantee__c = false;
        basket1.Total_Revenue_Guarantee__c = false;
        basket1.Competitive_Clause__c = false;
        basket1.Disconnect_Allowances__c = false;
        basket1.Averaging_Co_Terminus__c = false;
        basket1.Full_Co_Terminus__c = false;
        basket1.Special_Conditions_Id__c = null;
        basket1.Tenure__c = 24;
        basket1.Care_Selected__c = false;
        basket1.Payment_Terms__c = '';
        basket1.Monthly_Recurring_Charges__c = 10;
        basket1.Total_Charges__c=90;
        basket1.Total_Cost__c=40;
        //basket.Number_of_users__c = 10;
        basket1.Credit_fund_total__c=100;
        basket1.Total_Buyout_Cost__c=100;
        basket1.Tenure__c = 24;
        basket1.Total_Charges_NPV__c = 24;
        basket1.Total_Cost_NPV__c = 2;
        basket1.Total_Buyout_Cost__c = 2;
        basket1.recalculateFormulas();

        insert basket1;

        Test.startTest();
        
        cscfga__Product_Configuration__c pcTest1 = CS_TestDataFactory.generateProductConfiguration(true, 'testConfig1', basket1);
        pcTest1.SMS_Recurring_Cost__c = 2;
        pcTest1.SMS_One_Off_Charge__c =20;
        pcTest1.cscfga__one_off_charge_product_discount_value__c = 4;
        pcTest1.Data_One_Off_Charge__c = 4;
        pcTest1.cscfga__one_off_charge_product_discount_value__c = 2;
        
        pcTest1.recalculateFormulas();
        update pcTest1;
        
        Map<Id, cscfga__Product_Configuration__c> getConfigs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{basket1});
        
         try {
             CS_ProductBasketService.calculateBasketTotals(getConfigs, basket1);
        }
        catch (Exception e) {
            
        }
        
       
        CS_ProductBasketService.containsCalculateProductGroup(basket1.Id);
        cscfga__Product_Basket__c basketX = CS_TestDataFactory.generateProductBasket(false, 'aaaaTest', opp11);
        CS_ProductBasketService.containsCalculateProductGroup(basketX.Id);
        Test.stopTest();
    }

    testMethod static void testProductBasketService(){
        Test.startTest();
        createTestData();

        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Configuration__c pc1 = CS_TestDataFactory.generateProductConfiguration(false, 'Special Conditions', basket);
        cscfga__Product_Configuration__c pc2 = CS_TestDataFactory.generateProductConfiguration(false, 'Special Conditions', basket);
        cscfga__Product_Configuration__c pc3 = CS_TestDataFactory.generateProductConfiguration(false, 'Special Conditions', basket);
        cscfga__Product_Configuration__c pc4 = CS_TestDataFactory.generateProductConfiguration(false, 'Special Conditions', basket);
        cscfga__Product_Configuration__c pc5 = CS_TestDataFactory.generateProductConfiguration(false, 'Special Conditions', basket);
        
        pc1.Calculations_Product_Group__c = 'BT Mobile';
        pc2.Calculations_Product_Group__c = 'Future Mobile';
        pc3.Calculations_Product_Group__c = 'Future Mobile';
        pc4.Calculations_Product_Group__c = 'Future Mobile';
        pc5.Calculations_Product_Group__c = 'Future Mobile';

        cscfga__Product_Definition__c pd1 = CS_TestDataFactory.generateProductDefinition(true, 'Mobile Voice');
        pc1.cscfga__Product_Definition__c = pd1.Id;
        pc1.Tenure__c = 24;
        pc1.cscfga__Configuration_Status__c = 'Valid';
        pc1.Care__c = 'test';
        pc1.Mobile_Voice_Recurring_Charge__c = 100;
        pc1.Mobile_Voice_One_Off_Charge__c = 500;
        pc1.SMS_Recurring_Cost__c = 10;
        pc1.SMS_Recurring_Charge__c = 10;
        pc1.Data_Recurring_Charge__c = 10;
        pc1.Data_One_Off_cost__c = 10;
        pc1.Data_One_Off_Charge__c = 10;
        pc1.SMS_One_Off_Cost__c = 10;
        pc1.SMS_One_Off_Charge__c = 10;
        pc1.Incoming_Recurring_Charge__c = 10;
        pc1.Mobile_Voice_Recurring_Cost__c = 10;
        pc1.Mobile_Voice_One_Off_Cost__c = 10;
        pc1.Data_Recurring_Cost__c = 10;
        pc1.Warning_Summary__c = 'test';
        pc1.OPEX_Recurring_Cost__c = 10;
        pc1.Service_Recurring_Cost__c = 10;
        pc1.Fixed_One_Off_Charge__c = 100;
        pc1.Fixed_Recurring_Charge__c  = 100;
        pc1.cscfga__recurring_charge_product_discount_value__c = 2;
        pc1.cscfga__one_off_charge_product_discount_value__c = 2;
        pc1.SMS_Recurring_Cost__c = 10;
        pc1.SMS_One_Off_Cost__c = 2;
        pc1.Data_One_Off_cost__c = 2;
        pc1.Data_Recurring_Cost__c = 2;
        pc1.Leader_Fee__c = 0;
        pc1.Sharer_Fee__c = 0;
        pc1.recalculateFormulas();
        

        cscfga__Product_Definition__c pd2 = CS_TestDataFactory.generateProductDefinition(true, 'User Group');
        pc2.cscfga__Product_Definition__c = pd2.Id;
        pc2.Tenure__c = 24;
        pc2.cscfga__Configuration_Status__c = 'Valid';
        pc2.Other_Recurring_Charge__c = 100;
        pc2.cscfga__recurring_charge_product_discount_value__c = 2;
        pc2.recalculateFormulas();

        cscfga__Product_Definition__c pd3 = CS_TestDataFactory.generateProductDefinition(true, 'Test Product Definition');
        pc3.cscfga__Product_Definition__c = pd3.Id;
        pc3.Tenure__c = 24;
        pc3.cscfga__Configuration_Status__c = 'Valid';

        cscfga__Product_Definition__c pd4 = CS_TestDataFactory.generateProductDefinition(true, 'Other Costs');
        pc4.cscfga__Product_Definition__c = pd4.Id;
        pc4.Tenure__c = 24;
        pc4.cscfga__Configuration_Status__c = 'Valid';

        cscfga__Product_Definition__c pd5 = CS_TestDataFactory.generateProductDefinition(true, 'Service Add On');
        pc5.cscfga__Product_Definition__c = pd5.Id;
        pc5.cscfga__Configuration_Status__c = 'Valid';

        List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>{pc1, pc2, pc3, pc4, pc5};

        INSERT productConfigurations;

        List<cscfga__Attribute_Definition__c> tmpAtrDefList = CS_TestDataFactory.generateAttributeDefinitions(true,
                new List<String>{'Full Co Terminus', 'Averaging Co Terminus', 'Disconnect Allowance', 'Competitive Clause',
                        'Total Revenue Guarantee', 'Total Contract Guarantee', 'Unlocking Fees', 'Competitive Benchmarking', 'Payment Terms',
                        'Crown Commercial Service', 'To Delete', 'To Update', 'Opt Out of Price Increases'},
                pd1);

        List<cscfga__Attribute__c> attrs = CS_TestDataFactory.generateAttributesForConfiguration(false, tmpAtrDefList, pc1);
        attrs.get(0).cscfga__Attribute_Definition__c = null;
        attrs.get(1).cscfga__Attribute_Definition__c = null;
        attrs.get(1).cscfga__Recurring__c = true;
        cscfga__Attribute__c toDelete = null;
        cscfga__Attribute__c toUpdate = null;
        for(cscfga__Attribute__c a : attrs){
            if(a.Name.equalsIgnoreCase('Payment Terms'))
                a.cscfga__Value__c = '60';
            else
                    a.cscfga__Value__c = 'Yes';

            if(a.Name.equalsIgnoreCase('To Delete')) toDelete = a;
            if(a.Name.equalsIgnoreCase('To Update')) toUpdate = a;
        }
        INSERT attrs;


        Map<Id, cscfga__Product_Basket__c> basketMap = new Map<Id, cscfga__Product_Basket__c>{ basket.Id => basket };
        Map<Id, cscfga__Product_Configuration__c> configsMap = new Map<Id, cscfga__Product_Configuration__c>{
                pc1.Id => pc1,
                pc2.Id => pc2,
                pc3.Id => pc3,
                pc4.Id => pc4,
                pc5.Id => pc5
        };



        toUpdate.Name = 'Updated Name';
        update toUpdate;

        delete toDelete;

        List<CS_Approval_Level__c> approvalLevels = CS_ProductBasketService.approvalLevels;
        Map<String, List<CS_Approval_Level__c>> approvalLevelsByType = CS_ProductBasketService.approvalLevelsByType;
        Map<String, Integer> STATUS_MAP = CS_ProductBasketService.STATUS_MAP;

        Boolean validateCreditFunds = CS_ProductBasketService.validateCreditFunds(configsMap);
        cscfga__Product_Basket__c getBasket = CS_ProductBasketService.getBasket(basket.Id);
        Map<Id, cscfga__Product_Configuration__c> getConfigs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{basket});
        getConfigs = CS_ProductBasketService.getConfigs(new Set<Id> {basket.Id});
        getBasket = CS_ProductBasketService.setSpecialConditions(configsMap, basket);
        Map<Id, cscfga__Product_Basket__c> desyncBasket = CS_ProductBasketService.desyncBasket(basketMap);

        desyncBasket = CS_ProductBasketService.unsyncBasketsFromMap(basketMap);
        getBasket = CS_ProductBasketService.getBasketFieldData(basket.Id);
        validateCreditFunds = CS_ProductBasketService.validateCreditFundsFromConfigMap(configsMap);
        Map<Id, cscfga__Product_Configuration__c> configs = CS_ProductBasketService.getConfigsFromBasketIdSet(new Set<Id>{basket.Id});
        getBasket = CS_ProductBasketService.setSpecialConditionFromConfigurations(configsMap, basket);

        System.debug('allConfigs ++++++ ' +  allConfigs);

        try{
            cscfga__Product_Basket__c b1 = CS_ProductBasketService.calculateBasketParameters(allConfigs, basket);
        }
        catch(Exception exc){}

        try{
            cscfga__Product_Basket__c b2 = CS_ProductBasketService.calculateBasketTotals(allConfigs, basket);
        }
        catch(Exception exc){}

        Test.stopTest();

        System.assertNotEquals(null, getBasket.Tech_Fund__c);
    }

    @IsTest
    public static void testPrepareTechFund() {
        Test.startTest();
        createTestData2();
        CS_ProductBasketService.prepareTechFund(basket, allConfigs, 'Tech_Fund__c');
        CS_ProductBasketService.prepareTechFund(basket2, allConfigs, 'Tech_Fund__c');
        Test.stopTest();
    }
    
}