@isTest
public class CS_Holding_Report_JSON_GeneratorTest {
    @isTest
     public static void testHoldingJSONGenerator() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		        
        Account account = new Account(
            OwnerId = UserInfo.getUserId(),
            Name = 'Account11',
            Type = 'End Customer'
        );
        insert account;
		Opportunity opportunity = new Opportunity(
            Name = 'New Opportunity1',
            OwnerId = UserInfo.getUserId(),
            StageName = 'Qualification',
            Probability = 0,
            CloseDate = system.today(),
            AccountId = account.id
        );
        insert opportunity;

        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = 'PD1',
            cscfga__Description__c = 'PD1 Desc'
        );
        insert pd;

        cscfga__Attribute_Definition__c ad = new cscfga__Attribute_Definition__c(
            cscfga__Product_Definition__c = pd.Id,
            Name = 'AD1'
        );
        insert ad;
        
        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
            cscfga__Product_Definition__c = pd.Id,
            Name = 'Resign Option'
        );
        insert ad2;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
            Name = 'New Basket',
            OwnerId = UserInfo.getUserId(),
            cscfga__Opportunity__c = opportunity.Id
        );
        insert basket;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
            cscfga__Product_Definition__c = pd.Id,   
            cscfga__Product_Basket__c = basket.Id,
            Name='PC1',
            Ave_Days_Remaining_Months__c = 4
        );
        insert pc;

        cscfga__Attribute__c att = new cscfga__Attribute__c(
            cscfga__Product_Configuration__c = pc.Id,
            Name = 'Test',
            cscfga__Value__c = '10',
            cscfga__is_active__c = true,
            cscfga__Attribute_Definition__c = ad.Id
        );
        insert att;
        
        cscfga__Attribute__c att2 = new cscfga__Attribute__c(
            cscfga__Product_Configuration__c = pc.Id,
            Name = 'Resign Option',
            cscfga__Value__c = 'Test',
            cscfga__is_active__c = true,
            cscfga__Attribute_Definition__c = ad2.Id
        );
        insert att2;
        csbtcl_bt_resign_exclusion_list__c rExcList = new csbtcl_bt_resign_exclusion_list__c(Product__c='BT Mobile',Tariff_Code__c='BTSAG3',name='BT Mobile');
        insert rExcList;
        String[] holdingReportCsvStringArray = new String[]{',AUDIE,357514,2175985,FLCP01,,Adrian Lewis,07760765236,09/07/2015,27,26/11/2119,EE,Okay',',AUDIE,357514,2175985,FLCP01,,Adrian Lewis,07760765236,09/07/2015,27,26/11/2119,EE,Okay'};
        String jsonString = csbtcl_CS_Holding_Report_JSON_Generator.getJSONHoldingReport(holdingReportCsvStringArray,'BT Mobile',pc.id);

		//assert setup
		List<HoldingReportWrapper> hrwList = new List<HoldingReportWrapper>();
		for( String csvLine : holdingReportCsvStringArray ){
			HoldingReportWrapper hrw = new HoldingReportWrapper( csvLine );
			hrw.calculateCed();
			hrw.calculateCedDays();
			hrw.calculateTermMTHS();
			hrw.exclusions = 'Include';
			hrwList.add(hrw);
		}
		
		System.assertEquals(JSON.serializePretty(hrwList), jsonString);
     }
}