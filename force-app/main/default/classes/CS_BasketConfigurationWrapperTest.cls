@IsTest
public class CS_BasketConfigurationWrapperTest  {

	static testmethod void testWrapper(){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		No_Triggers__c notriggers =  new No_Triggers__c();
        notriggers.Flag__c = true;
        INSERT notriggers;
		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'test', opp);

		Test.startTest();
		CS_BasketConfigurationWrapper bcw = new CS_BasketConfigurationWrapper(basket);
		Test.stopTest();
	}
}