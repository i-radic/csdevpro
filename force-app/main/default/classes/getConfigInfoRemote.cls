global with sharing class getConfigInfoRemote implements cssmgnt.RemoteActionDataProvider {
    @RemoteAction
    global static Map<String, Object> getData(Map<String, Object> inputMap) {
        System.debug('Input parameters received ' + inputMap);
        Map<String, Object> returnMap= new Map<String, Object>();
        try{
            String configId=(String) inputMap.get('configID');
            List<cscfga__Product_Configuration__c> configs = [select id,BTnet_Primary_Port_Speed__c 
                                                            from cscfga__Product_Configuration__c 
                                                            where id=:configId];
            System.debug(configs);
            System.debug(configs[0]);

            returnMap.put('config',configs[0]);
            returnMap.put('speed',configs[0].BTnet_Primary_Port_Speed__c);
        }
        catch (exception e) {
            returnMap.put('exception', e.getMessage());
        }
        System.debug(returnMap);
        return returnMap;
    }
}