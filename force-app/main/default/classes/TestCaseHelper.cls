/*
###########################################################################
# File..................: TestCaseHelper
# Created by............: Sridhar Aluru
# Created Date..........: 30-Apr-2016
# Last Modified by......:
# Last Modified Date....: 
# Description...........: Test class to cover the code for a class: CaseHelper
# Change Log............:
###########################################################################
*/
@isTest
(SeeAllData=True)
private class TestCaseHelper {

    public static testMethod void testCaseHelper () {
        test.starttest();         
        Case cs = Test_Utils.createCaseRecordforContractValidation();          
        System.assertNotEquals(cs.Id, null); 
        System.debug('1st Check Before >>>>>>>>>>>>>>>>>'+cs.X1st_Check__c);
        User usr1 = [Select ID from User where Email='testSalesUser1@bt.com'];
        System.assertNotEquals(usr1.ID, null); 
        cs.Contact__c = usr1.ID;
        cs.Contract_Category__c = 'GENERAL QUERIES';
        update cs;
        System.debug(LoggingLevel.Info, 'Your info');
        User usr2 = [Select ID,Username from User where Email='testUser2@bt.com'];
        System.assertNotEquals(usr2.ID, null);  
        System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>'+usr2.Username);
       system.RunAs(usr2){ 
            cs.X1st_Check__c=System.Today()-4;
            update cs;
            System.debug('1st Check After>>>>>>>>>>>>>>>>>'+cs.X1st_Check__c);
            System.debug('>>>>>>>>>>>>>>>>>>>>>>'+cs.X1st_Check_Completed_By__c);
         // System.assertNotEquals(cs.X1st_Check_Completed_By__c, null);             
            cs.Subsequent_Check_1__c=System.Today()-3;
            update cs;
        //  System.assertNotEquals(cs.Subsequent_Completed_By__c, null);
            
            cs.Subsequent_Check_2__c=System.Today()-2;
            cs.Contract_Category__c = 'ARTEMIS - BID';
              update cs;     
        //  System.assertNotEquals(cs.Subsequent_Completed_By_1__c, null);  
            
            cs.Completion_of_Checklist__c = System.Today();
            update cs;
       //   System.assertNotEquals(cs.Completion_of_Checklist_Completed_By__c, null);            
        }  
        test.stoptest();
    } 
}