global class SyncBasketFromPageLayout  
{
	webservice static string SynchronizeBasket(string ProductBasketId)
	{
		
		ProductBasketId = (string) (Id) ProductBasketId;
		//system.debug('****ProductBasketId=' + ProductBasketId);

		String SyncField = ProductUtility.GetSyncField();
		string SOQLQuery = 'SELECT Id,cscfga__opportunity__c, ' + SyncField + ' from cscfga__Product_Basket__c where id =\'' + ProductBasketId + '\'';
		System.debug('***SOQLQuery=' + SOQLQuery);

		//cscfga__Product_Basket__c CurrentBasket = [select Id, csordtelcoa__Synchronised_with_Opportunity__c, cscfga__Opportunity__c
		//	from cscfga__Product_Basket__c where Id=:ProductBasketId][0];

		cscfga__Product_Basket__c CurrentBasket = Database.query(SOQLQuery);
		
		//system.debug('****CurrentBasket before update=' + CurrentBasket);
		
		//de-sync first and then synce again
		//if (CurrentBasket.csordtelcoa__Synchronised_with_Opportunity__c)
		if ((Boolean) CurrentBasket.get(SyncField))
		{
			//CurrentBasket.csordtelcoa__Synchronised_with_Opportunity__c=false;
			CurrentBasket.put(SyncField,false);
			update CurrentBasket;
		}
		
		//CurrentBasket.csordtelcoa__Synchronised_with_Opportunity__c=true;
		CurrentBasket.put(SyncField,true);
		update CurrentBasket;
		
		//system.debug('****CurrentBasket after update=' + CurrentBasket);
		
		return CurrentBasket.cscfga__Opportunity__c;
	}
}