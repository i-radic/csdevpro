/**
 * Project Name..........: Everything Everywhere
 * File..................: TrialLineItemHelper.cls
 * Version...............: 1 
 * Created by............: Martin Eley
 * Created Date..........: 23rd August 2012
 * Last Modified by......: Martin Eley   
 * Last Modified Date....: 28th August 2012  
 * Description...........: Provides methods for processing Trial Line Items
 */
public with sharing class TrialLineItemHelper {
	/**
	 * Static method to set the type on line item
	 * 
	 * @param List<Trial_Line_Item__c> List of Trial Line Items to be processed
	 * @return List of updates Trial Line Items
	 */
	 public List<Trial_Line_Item__c> SetLineItemType(List<Trial_Line_Item__c> trialLineItems){
	 	//Build set of SKU ids
	 	Set<Id> sKUIds = new Set<Id>();
	 	for(Trial_Line_Item__c aTrialLineItem : trialLineItems){
	 		sKUIds.add(aTrialLineItem.SKU__c);
	 	}
	 	
	 	//Select all SKUs
	 	List<SKU__c> sKUs = [SELECT Id
	 		,Type__c
	 		FROM SKU__c
	 		WHERE Id IN :sKUIds];
	 	
	 	//Build map of SKUs
	 	Map<Id, String> sKUTypes = new Map<Id, String>();
	 	for(SKU__c aSKU : sKUs){
	 		sKUTypes.put(aSKU.Id, aSKU.Type__c);
	 	}
	 	
	 	//Set type for each line item
	 	for(Trial_Line_Item__c aTrialLineItem : trialLineItems){
	 		aTrialLineItem.Type__c = sKUTypes.get(aTrialLineItem.SKU__c);
	 	}
	 	
	 	return trialLineItems;
	 }
	
	/**
	 * Static method to calculate the sub total for each line item
	 * 
	 * @param List<Trial_Line_Item__c> List of Trial Line Items to be processed
	 * @return List of updated Trial Line Items
	 */	
	public List<Trial_Line_Item__c> CalculateSubTotals(List<Trial_Line_Item__c> trialLineItems){
		//Build set of SKU ids
		Set<Id> sKUIds = new Set<Id>();
		for(Trial_Line_Item__c aTrialLineItem : trialLineItems){
			sKUIds.add(aTrialLineItem.SKU__c);
		}
		
		//Build map of all SKUs
		Map<Id, SKU__c> sKUs = new Map<Id, SKU__c>();
		for(SKU__c aSKU : [SELECT Id
			,Price__c
			FROM SKU__c
			WHERE Id IN :sKUIds]){
			sKUs.put(aSKU.Id, aSKU);
		}
		
		//Iterate over line items and calculate sub total
		for(Trial_Line_Item__c aTrialLineItem : trialLineItems){
			aTrialLineItem.Sub_Total__c = aTrialLineItem.Quantity__c * sKUs.get(aTrialLineItem.SKU__c).Price__c;
		}
		
		return trialLineItems;
	}
	
	/**
	 * Static method to set the tablet field to true for line items where the SKU is defined
	 * as a tablet device.  The updated field is used in a roll up calculation on the trial 
	 * object which in turn is used in a validation rule.
	 * 
	 * @param List<Trial_Line_Item__c> List of Trial Line Items to be processed
	 * @return List of updated Trial Line Items
	 */
	public List<Trial_Line_Item__c> SetTabletDevice(List<Trial_Line_Item__c> trialLineItems){
		//Build set of SKU ids
		Set<Id> sKUIds = new Set<Id>();
		for(Trial_Line_Item__c aTrialLineItem : trialLineItems){
			sKUIds.add(aTrialLineItem.SKU__c);
		}
		
		//Build MAP of all SKUs
		Map<Id, SKU__c> sKUs = new Map<Id, SKU__c>();
		for(SKU__c aSKU : [SELECT Id
			,Name__c
			,Tablet_Device__c
			FROM SKU__c
			WHERE Id IN :sKUIds]){
			sKUs.put(aSKU.Id, aSKU);
		}
		
		//For each line item check to see if SKU is tablet and set line item
		for(Trial_Line_Item__c aTrialLineItem : trialLineItems){
			if(sKUs.get(aTrialLineItem.SKU__c).Tablet_Device__c == TRUE){
				aTrialLineItem.Tablet_Device__c = TRUE;
			}
		}
		
		//Return
		return trialLineItems;
	}
}