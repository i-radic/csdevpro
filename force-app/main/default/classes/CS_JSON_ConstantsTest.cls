@IsTest
public class CS_JSON_ConstantsTest  {
	@IsTest
	public static void testJsonConstants() {
		CS_JSON_Constants c = new CS_JSON_Constants();

		String  attSuffix = CS_JSON_Constants.Attributes_Suffix;
		String  separator = CS_JSON_Constants.Grouping_Separator;
		String  attValue = CS_JSON_Constants.AttributeField_Value;
		String  typeJson = CS_JSON_Constants.FieldType_JSON;
		String  typeString = CS_JSON_Constants.FieldType_String;
		String  jsonSchema = CS_JSON_Constants.ObjectType_JSONSchema;
		String  typePD = CS_JSON_Constants.ObjectType_ProductDefinition;
		String  siteSpecName = CS_JSON_Constants.Site_SpecificationName;
		
		System.assertEquals(CS_JSON_Constants.Attributes_Suffix, CS_JSON_Constants.Attributes_Suffix);
	}
	
}