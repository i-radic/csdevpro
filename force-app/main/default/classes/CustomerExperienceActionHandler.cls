/**************************************************************************************************************************************************
    Class Name : CustomerExperienceActionHandler
    Test Class Name : CustomerExperienceActionHelper_Test
    Description : Code to Insert and Update Customer Experience Action, And to Calculate RollUp Summary, to Update in Survey Response Object Record
    Version : V0.1
    Created By Author Name : Bikash Kumar Gupta
    Date : 08/09/2017
 *************************************************************************************************************************************************/

public class CustomerExperienceActionHandler {    
    
    public static Boolean BeforeUpdateRecursionCheck = True;
    public static Boolean AfterUpdateRecursionCheck = True;
    
    public static void BeforeInsertTrigger(List<Customer_Experience_Actions__c> NewList){
        
        CustomerExperienceActionHelper.BeforeInsertMethodTrigger(NewList);
        
    }
    
    public static void BeforeUpdateTrigger(Map<Id, Customer_Experience_Actions__c> NewMap, Map<Id, Customer_Experience_Actions__c> OldMap){
        
        CustomerExperienceActionHelper.BeforeUpdateMethodTrigger(NewMap, OldMap);
        
    }
    
    public static void AfterInsertTrigger(Map<Id, Customer_Experience_Actions__c> NewMap, List<Customer_Experience_Actions__c> NewList){
        
        CustomerExperienceActionHelper.AfterInsertMethodTrigger(NewMap, NewList);
        
    }
    
    public static void AfterUpdateTrigger(Map<Id, Customer_Experience_Actions__c> NewMap, Map<Id, Customer_Experience_Actions__c> OldMap, List<Customer_Experience_Actions__c> NewList, List<Customer_Experience_Actions__c> OldList){
        
        CustomerExperienceActionHelper.AfterUpdateMethodTrigger(NewMap, OldMap, NewList, OldList);
    }
    
    public static void AfterDeleteTrigger(List<Customer_Experience_Actions__c> OldList){
        
        CustomerExperienceActionHelper.AfterDeleteMethodTrigger(OldList);
        
    }
    
    public static void AfterUnDeleteTrigger(List<Customer_Experience_Actions__c> OldList){
        
        CustomerExperienceActionHelper.AfterUnDeleteMethodTrigger(OldList);
        
    }
}