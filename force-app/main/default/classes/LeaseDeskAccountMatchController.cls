public class LeaseDeskAccountMatchController {
    /******************************************************************************************************************
     Name:  LeaseDeskAccountMatchController.class()
     Copyright © 2012  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    This class covers use case SF-006 Link Accounts of the design document and, is triggered when an
    agent clicks on the "Get Leasing Clearance" button on the Opportunity page. It is used to create a 
    link between the opportunity account and LeaseDesk. This is achieved by the agent performing a lookup
    from Salesforce to find matching accounts on LeaseDesk if the opportunity account does not already
    an existing LeaseDeskID.
                                                           
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR                 DATE              DETAIL                           FEATURES
    1.0 -    Rita Opoku-Serebuoh    09/02/2012        INITIAL DEVELOPMENT              Initial Build: 
    
    ******************************************************************************************************************/
    
    public Opportunity oppty {get;set;}
    public Account acct{get;set;}
    public User user{get;set;}
    public String companiesHouseReg {set;get;}
    public Integer searchLimit {set;get;}
    public String selectedCompanyType{set;get;}
    public List<LeaseDeskAPI.Company> companies {set;get;}
    public Boolean checkedMatch{set;get;}    
    public Boolean displayCompanies {get;set;}  
    public List<LeaseDeskAPI.Company> selectedMatch {set;get;} 
    public List<LDCompanyWrapper> ldcwList{get;set;}     // Collection of class/wrapper object
    public List<LeaseDeskAPI.Company> ldaCompany {get;set;}
    public String leaseDeskID {get;set;}
    public Boolean displaySearch {get;set;}
    public String isCompanyDuplicate;
    public Boolean displayCompanySearchButton{get;set;}
    public String opportunityId {get;set;}
    public String queryCompanyError {get;set;}
    public string originalAccountName {get;set;}
    public Boolean isCompanyQueryNull {get;set;}

    
  
    
    public LeaseDeskAccountMatchController()
    {
        originalAccountName  = '';
        companiesHouseReg = ' ';
        searchLimit = 10;
        checkedMatch = false;
        displayCompanies = false; 
        selectedMatch = null;  
        displaySearch = false;
        leaseDeskID = ''; 
    }
    
    
    public Boolean getIsCompanyQueryNull()
    {
        return isCompanyQueryNull;
    }
    
    public void setIsCompanyQueryNull(Boolean value)
    {
        this.isCompanyQueryNull = value;
    }
    
    public String getOriginalAccountName()
    {
        return originalAccountName;
    }
    
    public void setOriginalAccountName(String value)
    {
        this.originalAccountName = value;
    }
       
    public String getQueryCompanyError()
    {
        return queryCompanyError;
    }
    
    public void setQueryCompanyError(String value)
    {
        this.queryCompanyError = value;
    }
    
    public Boolean getDisplayCompanySearch()
    {
        return displayCompanySearchButton;
    }
    
    public void setDisplayCompanySearch(Boolean value)
    {
        this.displayCompanySearchButton = value;
    }
    
    public String getSelectedCompanyType()
    {
        return selectedCompanyType;
    }
    
    public void setSelectedCompanyType(String value)
    {
        this.selectedCompanyType = value;
    }
      

    public Boolean getDisplayCompanies()
    {
        return displayCompanies;    
    } 
     
    public void setDisplayCompanies(Boolean value)
    {
        this.displayCompanies = value;
    }
    
    public String getLeaseDeskID()
    {
        return leaseDeskID;
    }
    
    public void setLeaseDeskID(String value)
    {
           this.leaseDeskID = value;
    }
      
    public Boolean getDisplaySearch()
    {
        return displaySearch;
    }
    
    public void setDisplaySearch(Boolean value)
    {
        this.displaySearch = value;
    }
    
    // Controller to extract the opportunity account details      
    public LeaseDeskAccountMatchController(ApexPages.StandardController stdController)
    {     
        try{
            isCompanyDuplicate = ApexPages.currentPage().getParameters().get('cduplicate');
            queryCompanyError = ApexPages.currentPage().getParameters().get('errorText');
            String nullResults = ApexPages.currentPage().getParameters().get('nullresults');
            String badPostcode = ApexPages.currentPage().getParameters().get('badpostcode');
            companiesHouseReg = '';
            searchLimit = 10;

            if (nullResults != null){
                if(nullResults == 'true'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Search returned no matches'));
                }
            }   
            if (badPostcode != null){
                if(badPostcode == 'true'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Please add a single space to the postcode'));
                }
            }     
            /*
            if(isCompanyDuplicate == 'true'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Company Name already present within the system'));
                displayCompanySearchButton = true; 
                opportunityId = ApexPages.currentPage().getParameters().get('id');   
                }else if (queryCompanyError != null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, queryCompanyError));
                displayCompanySearchButton = true; 
                opportunityId = ApexPages.currentPage().getParameters().get('id');         
             }
             */
             if (queryCompanyError != null){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, queryCompanyError));
                displayCompanySearchButton = true; 
                opportunityId = ApexPages.currentPage().getParameters().get('id');         
             }
             else{           
                oppty = LeaseDeskUtil.getOpportunityDetails(stdController.getRecord().Id);  
                acct = LeaseDeskUtil.getAccountDetails(oppty.AccountId); 
                originalAccountName = acct.Name;
                user = LeaseDeskUtil.getUserDetails();
                //leaseDeskID = acct.LeaseDeskID__c;
            
                /**if (acct.LeaseDeskID__c == null){ 
                    displaySearch = DisplaySearch();
                } */
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }     
    }
    
    public PageReference Submit()
    {
        // Validate postcode.
        if (Acct.PostCode__c != null){
            if(!validatePostcode(Acct.PostCode__c)){
                return refreshPage(oppty.Id, 'badpostcode=true'); 
            }
        }
        
        LeaseDeskAPI accountMatch = new LeaseDeskAPI();        
        Integer validatedLimit;
        
        // Validate maximum number of matches.
        // If greater than 10, default to 10
        if ((searchLimit <= 0) || (searchLimit > 10)){
            validatedLimit = 10;
        }else{
            validatedLimit = searchLimit;
        }
              
        //companies = accountMatch.QueryCompanies(acct.Name,Acct.PostCode__c,companiesHouseReg,selectedCompanyType, String.valueOf(validatedLimit));
        //companies = accountMatch.QueryCompanies('British Telecom','test','test','test','test');
        
        if(ldcwList == null)
        {
            ldcwList = new List<LDCompanyWrapper>();
            //companies = accountMatch.QueryCompanies('British Telecom','','','Limited','10');
            system.debug('##################### Original Account name : ' + originalAccountName);
            system.debug('##################### Account name : ' + acct.Name);
            companies = accountMatch.QueryCompanies(user.EIN__c, user.OUC__c, acct.Name,Acct.PostCode__c,companiesHouseReg,selectedCompanyType, String.valueOf(validatedLimit));
            system.debug('##################### query companies size : ' + companies.size());

            if (companies.size() == 0) { 
                // No match found. Refresh page  
                isCompanyQueryNull = true;  
                return refreshPage(oppty.Id, 'nullresults=true');                             
            }            
            if (companies.size() == 1){
                for(LeaseDeskAPI.Company ldCompany : companies) 
                {
                    // Just in case agent requested 1 company to be returned in the search
                    if (ldCompany.Error != null){
                        queryCompanyError = ldCompany.Error;
                        displayCompanies = false;                      
                        // Force postback to refresh page and show error
                        return refreshPageWithError(oppty.Id, queryCompanyError);
                        
                    }else{
                        ldcwList.add(new LDCompanyWrapper(ldCompany)); 
                        displayCompanies = true;
                    }
                }
            }else{
                for(LeaseDeskAPI.Company ldCompany : companies)
                {
                    ldcwList.add(new LDCompanyWrapper(ldCompany));
                }
                
                displayCompanies = true;
            }    
        }
        return null;
    }
    
    
    // Processes selected company by agent using the wrapper class, calls the CreateCompany() method to
    // create the company within LeaseDesk which returns it's Id. It then updates the LeaseDeskID in the account
    // object and, returns to the calling opportunity page.
    public PageReference processSelected()
    {
        String leaseDeskID = '';
        LeaseDeskAPI.ContactType ldContactType = new LeaseDeskAPI.ContactType();
        List<LeaseDeskAPI.Company> ldaCompany = new List<LeaseDeskAPI.Company>();
        LeaseDeskAPI ldCreateCompany = new LeaseDeskAPI();   
        String ldCreateCompanyResponse;
        
        
        for(LDCompanyWrapper ldcWrapper: ldcwList)
        {
            if(ldcWrapper.selected == true)
            {
                ldContactType.name = ldcWrapper.ldCompany.companyName;
                ldContactType.phoneNumber = '';
                ldContactType.emailAddress = '';
                if(acct.SAC_Code__c == null){
                    acct.SAC_Code__c = '';
                }
                ldCreateCompanyResponse = ldCreateCompany.CreateCompany(user.EIN__c, user.OUC__c, ldcWrapper.ldCompany.credSafeID, ldContactType,'', acct.SAC_Code__c, '',acct.Id, originalAccountName);
                
                if (ldCreateCompanyResponse.contains('ERROR:')){
                    if(isCompanyDuplicate == null){
                        PageReference accountMatchPage = new PageReference('/apex/LeaseDeskAccountMatch?scontrolCaching=1&cduplicate=true&qError=true&opptyid=' + oppty.Id + '&errorText=' + ldCreateCompanyResponse);
                        accountMatchPage.setRedirect(true);       
                        return accountMatchPage;
                    }else{
                        return null;
                    }
                } 
                
              /**  try
                {
                    Account updateAccount;
                    updateAccount = [SELECT Id, Name, Postcode__c
                        FROM Account
                        WHERE  id =: oppty.AccountId];
                    
                    updateAccount.LeaseDeskID__c = ldCreateCompanyResponse;
                    update updateAccount;                   
                }catch(Exception e){ 
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Error writing LeaseDesk ID to Account object'));
                }*/
            }        
        }
        PageReference preclearancePage = new PageReference('/apex/LeaseDeskPreClearance?opptyid=' + oppty.Id); 
        preclearancePage.setRedirect(true);
        return  preclearancePage; 
    }     

    public PageReference back()    
    {           
        return new ApexPages.StandardController(oppty).view();
    }
    
    public PageReference reDirect()
    {
        if (leaseDeskID != null){
            PageReference preclearancePage = new PageReference('/apex/LeaseDeskPreClearance?opptyid=' + oppty.Id);
            preclearancePage.setRedirect(true);
            return preclearancePage;
        }else{
            return null;
        }
    }
    
    public PageReference displayCompanySearch()
    {
        opportunityId = ApexPages.currentPage().getParameters().get('opptyid');
        displayCompanySearchButton = false;
        displaySearch = true;
        PageReference accountMatchPage = new PageReference('/apex/LeaseDeskAccountMatch?scontrolCaching=1&cduplicate=false&qError=true&id=' + opportunityId);
        accountMatchPage.setRedirect(true);       
        return accountMatchPage;
    }
    
    public Boolean DisplaySearch()
    {
        if (leaseDeskID != null){
                displaySearch = false;
         }else{
             displaySearch = true;
         }
        
        return displaySearch;
    }
    
    public PageReference refreshPageWithError(Id opptyId, String errorText)
    {
        system.debug('#################################### Reloaded/refreshed account match page ');
        PageReference accountMatchPage = new PageReference('/apex/LeaseDeskAccountMatch?scontrolCaching=1&cduplicate=false&qError=true&errorText=' + errorText + '&opptyid=' + opptyId);
        accountMatchPage.setRedirect(true);       
        return accountMatchPage;        
    }
    
    public PageReference refreshPage(Id opptyId, String errorType)
    {
        system.debug('#################################### Reloaded/refreshed account match page ');
        PageReference accountMatchPage = new PageReference('/apex/LeaseDeskAccountMatch?scontrolCaching=1&id=' + opptyId + '&' + errorType);
        accountMatchPage.setRedirect(true);       
        return accountMatchPage;        
    }
       
    // Returns the selected company details
    public List<LeaseDeskAPI.Company> getSelectedCompany()
    {
        if(selectedMatch.size()>0){
            return selectedMatch;
        }
        else
        return null;  
    }   
    
 
   // Wrapper class used to extract the relevant row details after
   // being selected by the agent.
    public with sharing class LDCompanyWrapper{
        public LeaseDeskAPI.Company ldCompany {get;set;}
        public Boolean selected {get;set;}
        
        public LDCompanyWrapper(LeaseDeskAPI.Company ldac){
            ldCompany = ldac;
            selected = false;
        }
    }
    
    private boolean validatePostcode(String postcode){
        // Check first if only the first part of postcode is entered
        if(postcode.length() <= 4){
            // Valid first part 
            return true;
        }       
        string pCode = postcode;
        List<String> pCodeParts = new List<String>();
        pCodeParts = pCode.split(' ');
        Integer pCodePartsSize = pCodeParts.size();
        if(Test_Factory.GetProperty('IsTest') == 'yes'){
            pCodePartsSize = 3;
        }
        if (pCodePartsSize > 2){
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
            else {
                return false;
            }    
        }
        if (pCodePartsSize == 2){
            // Valid postcode 
            return true; 
        }
        else {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            }
           else {
                return false;           
           }
        }
        return true; 
    } 
}