public class BTSportBillingAdjustmentCTRL {
    ApexPages.StandardController Gcontroller;
    public BT_Sport_Billing_Adjustment__c thisRec;
	public string attachmentId{get;set;}
	
	public String action {
        get {return System.currentPageReference().getParameters().get('action');}
        set;
    }

    public BTSportBillingAdjustmentCTRL(ApexPages.StandardController controller) {
        Gcontroller = controller;
        if (!Test.isRunningTest()) {controller.addFields(new List<String>{'Bill_Issue_Date__c'});controller.addFields(new List<String>{'Bill_Reference__c'});controller.addFields(new List<String>{'BT_Sport_MVOC__c'});controller.addFields(new List<String>{'Bill_Adjustment_Version__c'});controller.addFields(new List<String>{'Name'});controller.addFields(new List<String>{'BT_Sport_MVOC__r.Billing_Email__c'});controller.addFields(new List<String>{'Bill_Send_Count__c'});controller.addFields(new List<String>{'Correct_Amount_exc_VAT__c'});controller.addFields(new List<String>{'Customer__c'});}
        this.thisRec= (BT_Sport_Billing_Adjustment__c)controller.getRecord();
    }
    
	public void onLoadAction  (){
		if(action == 'createBill'){
			createBill();
		}else if (action == 'sendEmail'){
			sendEmail();
		}
	}
    public void createBill(){
        thisRec.Bill_Adjustment_Version__c = thisRec.Bill_Adjustment_Version__c+1;
        update thisRec;
        BT_Sport_MVOC__c mvoc = [select MSA_Monthly_Last_Bill_Discounted_Price__c FROM BT_Sport_MVOC__c WHERE id =: thisRec.BT_Sport_MVOC__c];
		mvoc.MSA_Monthly_Last_Bill_Discounted_Price__c = thisRec.Correct_Amount_exc_VAT__c;
		update mvoc;

        List<Attachment> attachments = [select Name FROM Attachment WHERE parentid =: thisRec.Id];
		delete attachments;
        Datetime dt = thisRec.Bill_Issue_Date__c;
        String fmtDate = dt.format('d MMM YY');
        //String fmtTime = string.valueOf(thisRec.Bill_Adjustment_Version__c); //system.now().format('yyMMdd/HHmmss');
        String fileName = thisRec.Name+' Adjustment to bill '+thisRec.Bill_Reference__c+' on '+fmtDate+'.pdf';

        pageReference pdf = Page.BTSportBillingAdjustment;
        pdf.getParameters().put('id',thisRec.Id);
        Attachment attach = new Attachment();
        Blob body;
        try{
            body = pdf.getContentAsPDF();
        }
        catch(Exception t){
            body = Blob.valueOf('Cannot create PDF');
        }
        attach.Body = body;
        attach.Name = fileName;
        attach.IsPrivate = false;
        attach.ParentId = thisRec.Id;
	    insert attach;
    }

    public String emlSubject = '';
    public String emlPlainTextBody = '';
    public String emlHtmlBody = '';

    public PageReference sendEmail(){	
		emlSubject = 'BT Sport Billing Adjustment to bill '+thisRec.Bill_Reference__c;
        emlPlainTextBody = 'THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY\n\n'
            +'Dear '+ thisRec.Customer__c +'.\n'
            +'Please find attached your latest invoice relating to your BT Sport service.\n'
            +'If you have any questions, please contact your BT Sport team on 08000 807 000.\n\n'
            +'Thanks\n'
            +'Your BT Sport Team';
        emlHtmlBody = '<span style="font-family:Arial;font-size:13px">'
            +'<span style="font-weight:bold;color:red;">THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY<br/><br/></span>'
            +'Dear '+ thisRec.Customer__c +'<br/>'
            +'Please find attached your latest invoice relating to your BT Sport service.<br/>'
            +'If you have any questions, please contact your BT Sport team on 08000 807 000.<br/><br/>'
            +'Thanks<br/>'
            +'Your BT Sport Team'
            +'</span>'; 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        String[] toAddresses = new String[] {thisRec.BT_Sport_MVOC__r.Billing_Email__c};
        
        mail.setOrgWideEmailAddressId('0D220000000CbxE');
        mail.setToAddresses(toAddresses);
		mail.setWhatId(thisRec.BT_Sport_MVOC__c);
	    List<Id> entityAttachments = new List<Id>();
	    for (Attachment a : [select Id, Name, Body, BodyLength from Attachment where parentid =: thisRec.Id]){
			entityAttachments.add(a.Id);
		}
	    mail.setEntityAttachments(entityAttachments);
        mail.setUseSignature(false);
        mail.setSubject(emlSubject);
        mail.setPlainTextBody(emlPlainTextBody);
        mail.setHtmlBody(emlHtmlBody);   
		
        if (!Test.isRunningTest()) {Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });}

		if (!Test.isRunningTest()) {thisRec.Bill_Send_Count__c = thisRec.Bill_Send_Count__c+1;}
		update thisRec;

        return null;
    }
}