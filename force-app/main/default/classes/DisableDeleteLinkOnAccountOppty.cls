public class DisableDeleteLinkOnAccountOppty{

    public DisableDeleteLinkOnAccountOppty(){}
    
    public DisableDeleteLinkOnAccountOppty(ApexPages.StandardController con){
    }

    public PageReference disableDelete(){
        PageReference pr = new PageReference('/'+ ApexPages.currentPage().getParameters().get('retURL'));
        pr.setRedirect(true);
        return pr;
    }

}