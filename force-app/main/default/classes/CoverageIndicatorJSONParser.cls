public without sharing class CoverageIndicatorJSONParser {

    public class Coverage {
        public String type ; 
        public String strength ; 
        public String comingsoon ;
         public String is4GHighSpeed;
    }

    public class Links {
        public String rel;
        public String href;
        public String method;
    }

    public List<Links> links;
    public List<Locations> locations;
    public String lastUpdated;

    public class Locations {
        public String title;
        public String lat;
        public String lng;
        public List<Links> links;
        public List<Coverage> coverage;
    }

    
    public static CoverageIndicatorJSONParser parse(String json) {
        return (CoverageIndicatorJSONParser) System.JSON.deserialize(json, CoverageIndicatorJSONParser.class);
    }
    
   
}