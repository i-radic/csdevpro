/**************************************************************************************************************************************************
		Test Class Name	: Test_CSAT_Feedback_SRM_BM
		Description	: Code to Insert and Update Survey Response Records, And to Calculate RollUp Summary, to Update in CSAT Record
		Version	: V0.1
		Created By Author Name : BALAJI MS
		Date : 08/09/2017
 *************************************************************************************************************************************************/

@isTest
private class  Test_CSAT_Feedback_SRM_BM {
    static TestMethod void CsatContactFeedbackTestClassMethod(){
        
        List<User> UserList = new List<User>();
        List<Lead_Locator__c> LeadLocatorList = new List<Lead_Locator__c>();
        List<CSAT_Contact_Feedback__c> CSATContactFeedBackList = new List<CSAT_Contact_Feedback__c>();
        List<Account> AccountList = new List<Account>();
        List<Contact> ContactList = new List<Contact>();
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
            
         }
        Profile SysAdminProfile1 = [select id from profile where name='System Administrator']; 
        
        User SysAdmin=new User(alias ='SysA',email='LL1cr@bt.com',emailencodingkey='UTF-8',lastname='Testing SysAdmin',languagelocalekey='en_US',localesidkey='en_US',profileid = SysAdminProfile1.Id, timezonesidkey='Europe/London',username='Systemadmincr@testemail.com',EIN__c='Sys11');        
        UserList.add(SysAdmin);  
        
        Profile LLProfile1 = [select id from profile where name='Field: Standard User']; 
        
        User Leaduser1=new User(alias ='LL1',email='LL1cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL1',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile13167cr@testemail.com',EIN__c='LL11');        
        UserList.add(Leaduser1);   
        
        User Leaduser2=new User(alias ='LL2',email='LL2cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL2',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile22cr@testemail.com',EIN__c='LL22');        
        UserList.add(Leaduser2);  
        
        User Leaduser3= new User(alias ='LL3',email='LL3cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL3',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile321cr@testemail.com',EIN__c='LL33');        
        UserList.add(Leaduser3);
        
        User Leaduser4= new User(alias ='LL4',email='LL4cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL4',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile431cr@testemail.com',EIN__c='LL44');        
        UserList.add(Leaduser4);
        
        User Leaduser5= new User(alias ='LL5',email='LL5cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL5',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile3214cr@testemail.com',EIN__c='LL335');        
        UserList.add(Leaduser5);
        
        User Leaduser6= new User(alias ='LL6',email='LL6cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL6',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile4315cr@testemail.com',EIN__c='LL446');        
        UserList.add(Leaduser6);
        
        //test.startTest();
        Insert UserList;
        
        User Leaduser7= new User(manager=leaduser6,alias ='LL6',email='LL6cr@bt.com',emailencodingkey='UTF-8',lastname='Testing LL6',languagelocalekey='en_US',localesidkey='en_US',profileid = LLProfile1.Id, timezonesidkey='Europe/London',username='LLProfile43175cr@testemail.com',EIN__c='LL447');  
        insert Leaduser7;
        system.runAS(SysAdmin){
         
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
            
		Account Act =new Account();
        Act.Name='Test LLL';
        Act.SAC_Code__c='SACLL13';
        Act.Sub_Sector__c = 'Corporate 503';
            Account Act2 =new Account();
        Act2.Name='Test LLLL';
        Act2.SAC_Code__c='SACLL14';
        Act2.Sub_Sector__c = 'Corporate 503';
          AccountList.add(Act2);
        AccountList.add(Act);
        
        Insert AccountList;
           list<Service_Improvement_Plan__c> siplist = new list<Service_Improvement_Plan__c>();
        Service_Improvement_Plan__c sip = new Service_Improvement_Plan__c();
            sip.Account__c=act.id;
            sip.RAG__c='Red';
            //insert sip;
            //siplist.add(sip);
            Service_Improvement_Plan__c sip2 = new Service_Improvement_Plan__c();
            sip2.Account__c=act2.id;
            sip2.RAG__c='Red';
           // insert sip2;
            
        
        contact c = new contact();
        c.lastname = 'Test123';
        c.AccountId = Act.id;
        ContactList.add(c);       
        test.startTest();
        insert ContactList;
       test.stopTest();
        BTLB_CSAT__c CreateCSATRecord = Test_Factory.CreateCSAT(Act.Id);
        //CreateCSATRecord.RecordTypeId=Schema.SObjectType.BTLB_CSAT__c.getRecordTypeInfosByName().get('BT Buy').getRecordTypeId();
        insert CreateCSATRecord;
            
         opportunity opp= new opportunity();
            opp.accountid=act.id;
            opp.StageName='Won';
            insert opp;
        
        Lead_Locator__c LL1=new Lead_Locator__c(Account__c=Act.Id,Person_Role__c='Service Manager',User__c=Leaduser1.Id); 
        LeadLocatorList.add(LL1);
        
        Lead_Locator__c LL2=new Lead_Locator__c(Account__c=Act.Id,Person_Role__c='Business Manager',User__c=Leaduser2.Id); 
        LeadLocatorList.add(LL2);
        
        Lead_Locator__c LL3=new Lead_Locator__c(Account__c=Act.Id,Person_Role__c='General Manager',User__c=Leaduser3.Id); 
        LeadLocatorList.add(LL3);
        
        Lead_Locator__c LL4=new Lead_Locator__c(Account__c=Act.Id,Person_Role__c='Sales Manager (Primary)',User__c=Leaduser4.Id); 
        LeadLocatorList.add(LL4);
        
        Lead_Locator__c LL5=new Lead_Locator__c(Account__c=Act.Id,Person_Role__c='Desk Based Account Manager',User__c=Leaduser5.Id); 
        LeadLocatorList.add(LL5);
        
        Lead_Locator__c LL6=new Lead_Locator__c(Account__c=Act.Id,Person_Role__c='Desk Based Sales Manager',User__c=Leaduser6.Id); 
        LeadLocatorList.add(LL6);
        
        Insert LeadLocatorList;
            
        
       
        CSAT_Contact_Feedback__c CSATFB1 = Test_Factory.CreateCSATContactFeedback(c.Id);
        CSATFB1.RecordType_Name__c = 'Corporate Touch Point';
            CSATFB1.RecordTypeId=SObjectType.CSAT_Contact_Feedback__c.getRecordTypeInfosByName().get('MPS Touch Point').getRecordTypeId();
        CSATFB1.Individual_NPS__c = 1;
        CSATFB1.Survey_acknowledged_with_customer__c = True;
        CSATFB1.Action_required__c = 'Yes';
        CSATFB1.Closed_Loop_Status__c = 'Open';
             CSATFB1.Review_meeting_planned_date__c=Date.newInstance(1960, 3, 17);
        CSATContactFeedBackList.add(CSATFB1);
        CSAT_Contact_Feedback__c CSATFB2 = Test_Factory.CreateCSATContactFeedback(c.Id); 
        CSATFB2.RecordType_Name__c = 'Corporate Touch Point';
        CSATFB2.Individual_NPS__c = 5;
        CSATContactFeedBackList.add(CSATFB2);
        CSAT_Contact_Feedback__c CSATFB3 = Test_Factory.CreateCSATContactFeedback(c.Id);
        CSATFB3.RecordType_Name__c = 'Corporate Touch Point';
          CSATFB3.RecordTypeId=Schema.SObjectType.CSAT_Contact_Feedback__c.getRecordTypeInfosByName().get('MPS Touch Point').getRecordTypeId();
          // act.OwnerId ='00520000005XZQ1AAO';
          CSATFB3.Account__c=act.id;
            CSATFB3.Contact__c=c.id;
        CSATFB3.CSAT__c = CreateCSATRecord.Id;
        CSATFB3.Survey_acknowledged_with_customer__c = True;
        CSATFB3.Individual_NPS__c = 8;
           CSATFB3.Sat_we_understood_requirements__c='4';
            CSATFB3.Sat_with_info_during_order_delivery__c='4';
                CSATFB3.Sat_with_overall_delivery_experience__c='4';
                CSATFB3.Services_delivered_when_promised__c='4';
            CSATFB3.Sat_with_mob_network_experience_data__c='4';
                CSATFB3.Sat_with_mob_network_experience_voice__c='4';
            CSATFB3.Overall_sat_with_SRM__c='4';
                CSATFB3.Sat_SRM_takes_ownership_of_issues__c='4';
                CSATFB3.Sat_SRM_adds_value_to_relationship__c='4';
            
				CSATFB3.Satisfaction_with_AM__c='4';
                    CSATFB3.AM_responsiveness__c='4';
                    CSATFB3.AM_shows_how_BT_can_add_value__c='4';
           
            CSATFB1.Closed_Loop_Owner__c= Leaduser7.Id;
            
        CSATContactFeedBackList.add(CSATFB3); 
            
        Insert CSATContactFeedBackList;
            
            Event E = new Event();
            e.Event_Status__c ='Open';
            E.Type = 'Email';
            E.OwnerId = Leaduser7.Id;
            E.WhatId = CSATFB1.Id;
            e.DurationInMinutes=10;
            e.ActivityDateTime =Datetime.newInstance(1960, 2, 17);
            e.Activity_type__c='Customer Audio';
            insert E;
        
        CSATContactFeedBackList.clear();
      //  Act.Sub_Sector__c = 'Mid Market North 771';
            act.SRM_Managers__c=Leaduser7.Id;
            act.General_Manager__c=Leaduser7.Id;
          //  act.CTL_Owner__c='CTL Helpdesk';
            act.CTL_Owner__c='Corporate Team';
            Act.Sub_Sector__c ='Corporate 503';
        Update Act;
            
             CSATFB1.Review_meeting_planned_date__c=Date.newInstance(1960, 2, 17);
           CSATFB1.Account__c=act.Id; 
            CSATFB1.UK_Ireland__c = 'Ireland';
        CSATFB1.Individual_NPS__c = 1;
        CSATFB1.Closed_Loop_Status__c = 'Closed';
           CSATFB1.Closed_Loop_Owner__c= Leaduser7.Id;
        CSATFB1.Discussion_notes__c = 'Test notes';
           CSATFB1.RecordTypeId=Schema.SObjectType.CSAT_Contact_Feedback__c.getRecordTypeInfosByName().get('Corporate Touch Point').getRecordTypeId();
        CSATFB1.RCA_Category__c='Billing';
        CSATFB1.RC__c='Analyst Converge';
        CSATContactFeedBackList.add(CSATFB1);
        CSATFB2.Individual_NPS__c = 6;
        CSATFB2.Contact_Account_SRM_Manager_Email__c = Leaduser1.Email;
        CSATFB2.Contact_Account_Business_Manager_Email__c = Leaduser2.Email;
        CSATContactFeedBackList.add(CSATFB2);   
        CSATContactFeedbackTriggerHandler.BeforeUpdateRecurssionFlag = True;
        
        Update CSATContactFeedBackList;
            delete CSATFB2;
             undelete CSATFB2;
            
     CSATContactFeedBackList.clear();
     CSAT_Contact_Feedback__c BTBUYCSAT = Test_Factory.CreateCSATContactFeedback(c.Id);
     BTBUYCSAT.RecordTypeId=Schema.SObjectType.CSAT_Contact_Feedback__c.getRecordTypeInfosByName().get('BT Buy').getRecordTypeId();
     BTBUYCSAT.Account__c=Act.Id;
            BTBUYCSAT.Opportunity_Id_new__c= opp.Id;
     CSATContactFeedBackList.add(BTBUYCSAT);
    //test.startTest();
     upsert CSATContactFeedBackList;
            
     CSATContactFeedBackList.clear();
     
        
        CSATContactFeedBackList.clear();
        Act.Sub_Sector__c = 'Major Corporate 503';
        Update Act;
        CSAT_Contact_Feedback__c CSATFB4 = Test_Factory.CreateCSATContactFeedback(c.Id);
        CSATFB4.RecordType_Name__c = 'MPS Touch Point';
        CSATFB4.Satisfaction_with_AM__c = String.valueOf(3);
        CSATFB4.CSAT__c = CreateCSATRecord.Id;
        CSATContactFeedBackList.add(CSATFB4);
        CSAT_Contact_Feedback__c CSATFB5 = Test_Factory.CreateCSATContactFeedback(c.Id); 
        CSATFB5.RecordType_Name__c = 'MPS Touch Point';
        CSATFB5.Satisfaction_with_AM__c = String.valueOf(5);
        CSATContactFeedBackList.add(CSATFB5);
      // Insert CSATContactFeedBackList;
      //  delete CSATFB5;
        CSATContactFeedBackList.clear();
        Act.Sub_Sector__c = 'Local Devolved Govt & Health 503';
        Update Act;
        CSAT_Contact_Feedback__c CSATFB6 = Test_Factory.CreateCSATContactFeedback(c.Id);
        CSATFB6.RecordType_Name__c = 'MPS Touch Point';
        CSATFB6.Satisfaction_with_AM__c = String.valueOf(3);
        CSATContactFeedBackList.add(CSATFB6);
        CSAT_Contact_Feedback__c CSATFB7 = Test_Factory.CreateCSATContactFeedback(c.Id); 
        CSATFB7.RecordType_Name__c = 'MPS Touch Point';
        CSATFB7.Satisfaction_with_AM__c = String.valueOf(6);
        CSATContactFeedBackList.add(CSATFB7);
    //   Insert CSATContactFeedBackList;
        
       // Delete CSATFB7;
        } 
    
    }
}