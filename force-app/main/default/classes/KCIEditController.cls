public with sharing class KCIEditController{

    Id KCIRTId,ImageURL,KCIId;
    String RTName;
    KCI__c kci; 
    
    public KCIEditController(ApexPages.StandardController controller) {
        kci = (KCI__c)controller.getRecord(); 
        KCIRTId=Apexpages.currentPage().getParameters().get('RecordType');
        KCIId=Apexpages.currentPage().getParameters().get('Id');
        kci.RecordTypeId=KCIRTId;
    }
    
    public Id getHelpTextURL(){
        ImageURL = [Select Id from Document where DeveloperName=:'Help_Image'].Id;
            return ImageURL;
    }
    
    //To differentiate if its a new confirmation call or existing record. 
    //To show/hide validation call details section in confirmation edit&new page   
    public boolean getStatus(){
        if(KCIId!=null){//existing confirmation call
            if(kci.Converted__c=='Yes')//if converted, validation call details exists, hence displays(true)
                return true;
            else
                return false;
        }
        else//new confirmation call
            return false;
    } 
    
    
     
    }