public with sharing class addressSearch{

    public Contact contact;
    public string searchText {get; set;}   
    
    public void setSearchText(String searchText) { this.searchText = searchText; }
       
    public addressSearch(ApexPages.StandardController stdController) {

         contact = (Contact)stdController.getRecord();
     }
     
     public addressSearch(Contact contact_edit) {
         contact = contact_edit;
     }
     
     public PageReference Submit()
    {    
        if (searchText != null){
            PageReference PageRef = new PageReference('/apex/addressSearchResults?id='+contact.Id + '&p='+searchText + '&refPage=detail');                                  
            PageRef.setRedirect(true);
            return PageRef;
        }                          
        else {
            ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please select an address!'));    
        }  
        return null;    
    }
}