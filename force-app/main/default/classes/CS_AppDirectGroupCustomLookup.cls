global class CS_AppDirectGroupCustomLookup extends cscfga.ALookupSearch {

	public override String getRequiredAttributes(){
		return '["BrandId","Brand"]'; 
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
    	
    	Map<String, cspmb__Price_Item_Add_On_Price_Item_Association__c> uniqueGroupsMap = new Map<String, cspmb__Price_Item_Add_On_Price_Item_Association__c>();
        System.debug('Brand Id : ' + searchFields.get('BrandId'));
        System.debug('Brand : ' + searchFields.get('Brand'));
    	for (cspmb__Price_Item_Add_On_Price_Item_Association__c commPrdAddOnAssoc : [SELECT Id, Name, add_on_name__c, cspmb__add_on_price_item__r.name, cspmb__group__c, cspmb__add_on_price_item__r.cspmb__one_off_charge__c, cspmb__add_on_price_item__r.cspmb__recurring_charge__c, add_on_recurring_charge__c, cspmb__add_on_price_item__r.cspmb__add_on_price_item_code__c, cspmb__add_on_price_item__r.cspmb__add_on_price_item_description__c, cspmb__add_on_price_item__r.cspmb__contract_term__c, add_on_contract_term__c, cspmb__add_on_price_item__r.cspmb__recurring_cost__c, cspmb__add_on_price_item__r.cspmb__one_off_cost__c FROM cspmb__Price_Item_Add_On_Price_Item_Association__c WHERE Module__c = :searchFields.get('Brand') AND cspmb__add_on_price_item__r.cspmb__Is_Active__c = TRUE AND cspmb__Price_Item__c = :searchFields.get('BrandId') ORDER By cspmb__Group__c]) {
    	    if (String.isNotEmpty(commPrdAddOnAssoc.cspmb__group__c) && !uniqueGroupsMap.containsKey(commPrdAddOnAssoc.cspmb__group__c)) {
    	        uniqueGroupsMap.put(commPrdAddOnAssoc.cspmb__group__c, commPrdAddOnAssoc);
    	    }
    	}
    	

    	return uniqueGroupsMap.values();
    }
}