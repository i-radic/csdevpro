@isTest
private class ADPSWOTCtrl_Test {

    static testMethod void myUnitTest(){
	User thisUser = [select id from User where id=:userinfo.getUserid()];
    System.runAs(thisuser){
     TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
     settings.Account__c = FALSE;
     settings.Contact__c = FALSE;
     settings.Opportunity__c = FALSE;
     settings.OpportunitylineItem__c = FALSE;
     settings.Task__c = FALSE;
     settings.Event__c = FALSE;
     
     upsert settings TriggerDeactivating__c.Id;
    
     Profile p= [select id from profile where name='Field: Standard User'];
     User u1=[select id from User where name='OTHER Default Account Owner'];
     User u;
     u= new User(alias='asd',email='testing@bt.it',EIN__c='87fgter',emailencodingkey='UTF-8',lastname='Testingasd', languagelocalekey='en_US', localesidkey='en_US',isActive=True,isActive2__c='True',Department='TestSupport', profileid = p.Id, timezonesidkey='Europe/London', username='testingasd@testemail.com');
     insert u;
     //Account ac = new Account (name='Test Account');
      Account retval = new Account(id = null);
      retval.Name = 'tst_Account';
      retval.LOB_Code__c = 'tst_lob';
      retval.SAC_Code__c = 't_sac';
      retval.LE_Code__c = 'tst_le6789';
      retval.AM_EIN__c = '802537216';
      retval.Sector_code__c = 'CORP';
      retval.OwnerId=u.Id;
      retval.Base_Team_Assign_Date__c=System.today();
      insert retval;
     //ac.OwnerId=u1.Id;
     //insert ac;
     ADP__c ad=new ADP__c(Customer__c=retval.id);
     insert ad;
     ADP_BT_SWOT__c ab=new ADP_BT_SWOT__c(ADP__c=ad.id);
     insert ab;
     ApexPages.StandardController std=new ApexPages.StandardController(ab);
     ADPSWOTCtrl adp=new ADPSWOTCtrl(std);
     //adp.ADP_Id=ad.id;
     List<ADP_BT_SWOT__c> adpbt=adp.getBT();
     List<ADP_Client_SWOT__c> adpc=adp.getClient();
     adpbt=adp.getBTBoth();
     adpc=adp.getClientBoth();

    }
    }
}