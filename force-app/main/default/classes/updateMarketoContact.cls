public class updateMarketoContact {
    
    //Future annotation to mark the method as async.
    @Future(callout=true)
    
    public static void updateContact(String contactId) {        
        //SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway(); singhd62
        //service.timeout_x = 60000; singhd62
        try {                   
            Contact contact = [Select Id,
            zCUG__c,
            Active__c,
            Comment__c,
            Type__c,
            IntegrationId__c,
            Salutation,
            FirstName,
            MiddleName__c,
            LastName,
            AliasName__c,
            ContactSource__c,
            JobSeniority__c,
            Job_Function__c,
            Birthdate,
            MotherMaidenName__c,
            Status__c,
            Job_Title__c,
            Key_Decision_Maker__c,
            Organization__c,
            Department,
            Preferred_Contact_Channel__c,
            Secondary_Preferred_Contact_Channel__c,
            Email,
            emailHidden__c,
            Email_Id__c,
            Email_Consent__c,
            Email_Consent_Date__c,
            Mobile_Id__c,
            MobilePhone,
            Mobile_Consent__c,
            Mobile_Consent_Date__c,
            Phone_Id__c,
            Phone,
            Phone_Consent__c,
            Phone_Consent_Date__c,
            Fax_Id__c,
            Fax,
            Fax_Consent__c,
            Fax_Consent_Date__c,
            Contact_Address_Number__c,
            Address_Id__c,
            Contact_Address_Street__c,
            Contact_Locality__c,
            Address_County__c,
            Address_Country__c,
            Contact_Post_Code__c,
            Contact_Post_Town__c,
            Contact_Sector__c,
            Contact_Sub_Building__c,
            Contact_Zip_Code__c,
            Address_Consent__c,
            Address_Consent_Date__c from Contact WHERE Id = :contactId LIMIT 1];
        
            //SalesforceServicesCRM.Contact crmContact = new SalesforceServicesCRM.Contact(); singhd62
            SalesforceCRMService.CRMContact crmContact = new SalesforceCRMService.CRMContact(); //singhd62
            crmContact.CUG = contact.zCUG__c;           
            crmContact.ActiveFlag = contact.Active__c;
            crmContact.Comment = contact.Comment__c;
            crmContact.Type_x = contact.Type__c;
            crmContact.ExternalId = contact.IntegrationId__c;
            crmContact.Title = contact.Salutation;
            crmContact.FirstName = contact.FirstName;
            crmContact.MiddleName = contact.MiddleName__c;
            crmContact.LastName = contact.LastName;
            crmContact.AliasName = contact.AliasName__c;
            crmContact.ContactSource = contact.ContactSource__c;
            crmContact.JobSeniority = contact.JobSeniority__c;
            crmContact.Profession = contact.Job_Function__c;
            if (contact.Birthdate != null){
                crmContact.DateOfBirth = Convert.ConvertToDateTime(contact.Birthdate);
            }
            crmContact.MotherMaidenName = contact.MotherMaidenName__c;
            crmContact.Status = contact.Status__c;
            crmContact.JobTitle = contact.Job_Title__c;
            crmContact.KeyDecisionMaker = contact.Key_Decision_Maker__c;
            crmContact.FaxNumber = contact.Fax;
            crmContact.crmOrganization = contact.Organization__c;
            crmContact.Department = contact.Department;
            crmContact.PreferredContactChannel = contact.Preferred_Contact_Channel__c;
            crmContact.SecondaryPreferredContactChannel = contact.Secondary_Preferred_Contact_Channel__c;           
            if (contact.Email == 'DoNotContact@bt.co.uk') {
                crmContact.EmailAddress = contact.emailHidden__c;
            }
            else {
                crmContact.EmailAddress = contact.Email;
            }   
            crmContact.EmailAddressId = contact.Email_Id__c;
            crmContact.EmailConsentPermission = contact.Email_Consent__c;
            crmContact.EmailConsentDate = contact.Email_Consent_Date__c;            
            crmContact.MobilePhoneId = contact.Mobile_Id__c;
            crmContact.MobilePhone = contact.MobilePhone;
            crmContact.MobilePhoneConsentPermission = contact.Mobile_Consent__c;
            crmContact.MobilePhoneConsentDate = contact.Mobile_Consent_Date__c;        
            crmContact.WorkPhoneId = contact.Phone_Id__c;
            crmContact.WorkPhone = contact.Phone;
            crmContact.WorkPhoneConsentPermission = contact.Phone_Consent__c;
            crmContact.WorkPhoneConsentDate = contact.Phone_Consent_Date__c;                
            crmContact.FaxId = contact.Fax_Id__c;
            crmContact.Fax = contact.Fax;
            crmContact.FaxConsentPermission = contact.Fax_Consent__c;
            crmContact.FaxConsentDate = contact.Fax_Consent_Date__c;              
            crmContact.AddressNumber = contact.Contact_Address_Number__c;
            crmContact.AddressId = contact.Address_Id__c;
            crmContact.AddressStreet = contact.Contact_Address_Street__c;
            crmContact.AddressLocality = contact.Contact_Locality__c;
            crmContact.AddressDoubleDependentLocality = contact.Contact_Locality__c;
            crmContact.AddressCounty =contact.Address_County__c;
            crmContact.AddressCountry =contact.Address_Country__c;
            crmContact.AddressPostCode = contact.Contact_Post_Code__c;
            crmContact.AddressPostTown = contact.Contact_Post_Town__c;
            crmContact.AddressSector = contact.Contact_Sector__c;
            crmContact.AddressSubBuilding = contact.Contact_Sub_Building__c;
            crmContact.AddressZipCode = contact.Contact_Zip_Code__c;
            crmContact.AddressConsentPermission = contact.Address_Consent__c;
            crmContact.AddressConsentDate = contact.Address_Consent_Date__c;
            crmContact.RightToContact = 'Y';
            crmContact.AddressTimeAt = '18+ months';
            crmContact.isExisting = false;                   
            
            system.debug('logs -> Service call : UpdateMarketoContact');  
            //service.UpdateContact('', '', crmContact); //singhd62
            SalesforceCRMService.UpdateContact(crmContact); //singhd62              
        }                             
        catch (Exception e) { 
            system.debug('logs -> DmlException : UpdateMarketoContact' + e);                                
        }       
    }
}