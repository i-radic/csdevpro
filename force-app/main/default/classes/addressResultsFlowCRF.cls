public with sharing class addressResultsFlowCRF {

     public Flow_Address_Details__c addressDet;  
     public Boolean test= true;
    //public String ExchangeGroupCode;

    
    
    public string refferalPage = ApexPages.currentPage().getParameters().get('refPage');
    public string postcode = ApexPages.currentPage().getParameters().get('p');


    public string selectedAddr = ApexPages.currentPage().getParameters().get('AddType');
    List<Addresswrapper> ResultsList = new List<Addresswrapper>();
    
    List<SalesforceCRMService.CRMAddress> selectedAddress = new List<SalesforceCRMService.CRMAddress>(); // singhd62 
    
    public Boolean getTest(){
        return test;
    }
  
    public addressResultsFlowCRF(Flow_Address_Details__c address_edit){
         addressDet = address_edit;
    }
    
    public PageReference CreateAddress(){  System.debug('abccd'+selectedAddr);                                   
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }
        PageReference PageRef = new PageReference('/apex/createFlowCRFAddress?id='+addressDet.Id+'&refPage='+refferalPage+'&AddType1='+selectedAddr);
        PageRef.setRedirect(true);      
        return PageRef;                 
    }
    
    public PageReference Back(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
        if (refferalPage == 'new'){
            PageReference PageRef = new PageReference('/apex/FlowCRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
                       
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }    
            return PageRef;                       
        }
        if (refferalPage == 'update'){
            PageReference PageRef = new PageReference('/apex/FlowCRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }   
            return PageRef;    
        }
        if (refferalPage == 'detail'){
            PageReference PageRef = new PageReference('/'+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }      
            return PageRef;  
        }
        return null;
    }
     
    public addressResultsFlowCRF(ApexPages.StandardController controller) {
     addressDet = (Flow_Address_Details__c)controller.getRecord();
     
    }
    
    public List<Addresswrapper> pullAddr {
        get {
          //  system.debug('KKKKKKK KKKKKK KKKKKK '+pullAddresses());
            return getAddresses();
        }
        
    }
    
    public List<Addresswrapper> getAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceCRMService.CRMAddressList NADResponse = null;        
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceCRMService.CRMAddressList();
            NADResponse.Addresses = new SalesforceCRMService.CRMArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceCRMService.CRMAddress[0];
                                     
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            //a.ExchangeGroupCode = '_TEST_';
            //a.IdentifierId = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                
                NADResponse = SalesforceCRMService.GetNADAddress(postCode);
            }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;

            }                                             
        }                                       
              
        for(SalesforceCRMService.CRMAddress sfsAddress : NADResponse.Addresses.Address){
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();            
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
            //a.ExchangeGroupCode = sfsAddress.ExchangeGroupCode;
            
            ResultsList.add(new Addresswrapper(a));                                                         
        }

        if(ResultsList.size()>1){       
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please \'Create Address\'.'));          
             return null;
        }
    }
    
    /* -- singhd62 
    public List<Addresswrapper> getAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceServicesCRM.AddressList NADResponse = null;        
        SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway(); 
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceServicesCRM.AddressList();
            NADResponse.Addresses = new SalesforceServicesCRM.ArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceServicesCRM.Address[0];
                                     
            SalesforceServicesCRM.Address a = new SalesforceServicesCRM.Address();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            //a.ExchangeGroupCode = '_TEST_';
            //a.IdentifierId = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                service.timeout_x = 60000;
                NADResponse = Endpoints.SFCRMGateway().GetNADAddress('', '', postCode);
            }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;

            }                                             
        }                                       
              
        for(SalesforceServicesCRM.Address sfsAddress : NADResponse.Addresses.Address){
            Address a = new Address();            
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
            //a.ExchangeGroupCode = sfsAddress.ExchangeGroupCode;
            
            ResultsList.add(new Addresswrapper(a));                                                         
        }

        if(ResultsList.size()>1){       
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please \'Create Address\'.'));          
             return null;
        }
    }
    */
    
        
    public PageReference getSelected()
    {
        selectedAddress.clear();
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();
            
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            //a.ExchangeGroupCode = '_TEST_';
                
            Addresswrapper wrapper = new Addresswrapper(a);
            wrapper.selected = true;
            ResultsList.add(wrapper);                        
        }
        
      
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                    addressDet.Country__c = accwrapper.acc.Country;
                    addressDet.County__c = accwrapper.acc.County;
                    addressDet.Building_Name__c = accwrapper.acc.Name;
                   // addressDet.Address_POBox__c = accwrapper.acc.POBox;
                    addressDet.Number__c = accwrapper.acc.BuildingNumber;
                    addressDet.Street__c = accwrapper.acc.Street;
                    addressDet.Locality__c = accwrapper.acc.Locality;
                    addressDet.Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Post_Town__c = accwrapper.acc.Town;
                    addressDet.Sub_Building__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Existing__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality;   
                    
                                                             
                    //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    
                    //System.debug('NADID-->'+accwrapper.acc.IdentifierValue);
                    //System.debug('ExchangeGroupCode-->'+ExchangeGroupCode); 
                    
                    //if(ExchangeGroupCode!=null)
                    //addressDet.Existing_Exchange_Group_Code__c = ExchangeGroupCode;

                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        }    
       
        
        
        
        
        return null;
    
    }
    
    public List<SalesforceCRMService.CRMAddress> getselectedAddress()
    {
        if(selectedAddress.size()>0){
            return selectedAddress;
        }
        else
        return null;
    }    
    

     
    public with sharing class Addresswrapper
    {
        public SalesforceCRMService.CRMAddress acc {get; set;} //singhd62
        public Boolean selected {get; set;}
        public Addresswrapper(SalesforceCRMService.CRMAddress a) //singhd62
        {
            acc = a;
            selected = false;
        }
    }
}