/******************************************************************************************************
Name : QuestionnaireControlleredit
Description : Controller for page Questionnairepageedit
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    1/12/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public without sharing class QuestionnaireControlleredit 
{
    public List<QuestionAnswerWrapperClass> wrapper {get;set;}
    public String gateName {get;set;}
    public id qtid;
    public List<Questionnaire__c> qtrecord = new List<Questionnaire__c>();
    //public boolean showscore {get;set;}
    public Double score {get;set;}
    public map<id,QuestionnaireReply__c> questreplyMap = new map<id,QuestionnaireReply__c>();
     /******************************************************************************************************
    Method Name : QuestionnaireControlleredit 
    Description : Wrapper class QuestionAnswerWrapperClass
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public QuestionnaireControlleredit ()
    {
        wrapper  =  new List<QuestionAnswerWrapperClass>();
        qtid =System.currentPagereference().getParameters().get('qt');
        
        if(qtid != null){
            qtrecord = new List<Questionnaire__c>();
            qtrecord = [select id,Overall_Score__c,Opportunity__c,Gate__c from Questionnaire__c where id =: qtid limit 1];
        }
        
        List<QuestionnaireReply__c> qustreplyList = new List<QuestionnaireReply__c>();
        Set<Id> qustid = new Set<Id>();
        Map<Id,Id> qustansMap = new Map<Id,Id>();
        
        if(!qtrecord.isEmpty())
        {
            qustreplyList = [select id,Answer__c,Question__c,Questionnaire__c from QuestionnaireReply__c where Questionnaire__c =:qtrecord[0].id];
            gateName=qtrecord[0].Gate__c ;
        }
        if(! qustreplyList.isEmpty()){
            for(QuestionnaireReply__c reply : qustreplyList)
            {
                qustid.add(reply.Question__c);
                qustansMap.put(reply.Question__c,reply.Answer__c);
                questreplyMap.put(reply.Question__c,reply);
            }
        }
        
        List<QuestionnaireQuestion__c> questionnaireList =  new  List<QuestionnaireQuestion__c>();
        String catgory=GlobalConstants.EMPTYSTRING;
        String toparea= GlobalConstants.EMPTYSTRING;
        integer srn =0;
        questionnaireList = [Select id,Opportunity_Gates__c,Order__c,Question__c,Category__c,Topic_Area__c,Weighting__c, (Select id,Answer__c,Order__c,Score__c from Questionnaire_Answers__r order by Order__c) from QuestionnaireQuestion__c where RecordType.Name= 'Qualification' and id IN:qustid order by Order__c];
        
        if(!questionnaireList.isEmpty())
        {
            for(QuestionnaireQuestion__c questions : questionnaireList)
            {
                List<QuestionnaireAnswer__c > ansList = questions.Questionnaire_Answers__r;
                List<SelectOption> options = new List<SelectOption>();
                String selectedDefault;
                for(QuestionnaireAnswer__c answers : ansList )
                {
                    options.add(new SelectOption(answers.id,answers.Answer__c));
                    if(qustansMap.containsKey(questions.id))
                    {
                        selectedDefault=qustansMap.get(questions.id);
                    }
                }
                if(catgory != questions.Category__c)
                {
                    catgory=questions.Category__c;
                    srn=1;
                }
                else
                {
                    catgory=GlobalConstants.EMPTYSTRING;
                    srn+=1;
                }
                if(toparea != questions.Topic_Area__c)
                {
                    toparea=questions.Topic_Area__c;
                }
                else
                {
                    toparea=GlobalConstants.EMPTYSTRING;
                }
                wrapper.add(new QuestionAnswerWrapperClass(options,questions,selectedDefault,catgory,toparea,srn));
            }
        }
        if(!qtrecord.isEmpty()){
            score =qtrecord[0].Overall_Score__c;
        }
    }
 /******************************************************************************************************
Name : QuestionAnswerWrapperClass
Description : Controller for page Questionnairepageedit
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Class created        
 *********************************************************************************************************/    
    public without sharing class QuestionAnswerWrapperClass
    {
        public List<SelectOption> answers{get;set;}
        public QuestionnaireQuestion__c questions{get;set;}
        public String selectedAnswer {get;set;}
        public String category{get;set;}
        public String topicarea{get;set;}
        public integer srn {get;set;}
      /******************************************************************************************************
    Method Name : QuestionAnswerWrapperClass
    Description : Wrapper class QuestionAnswerWrapperClass
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
        public QuestionAnswerWrapperClass (List<SelectOption> answers , QuestionnaireQuestion__c questions,String selectedAnswer,String category,String topicarea, integer srn)
        {
            this.answers=answers;
            this.questions=questions;    
            this.selectedAnswer =selectedAnswer;   
            this.category=category;
            this.topicarea=topicarea;  
            this.srn=srn;
        }
    }
 /******************************************************************************************************
    Method Name : submitMethod
    Description :Method to submit QuestionaireReply
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/    
    public void submitMethod () 
    {
        List<QuestionnaireReply__c> lstQuestionReply = new List<QuestionnaireReply__c> ();
        try
        {
            RecordType QuestionaireReplyRecType = [SELECT Id FROM RecordType WHERE sObjectType='QuestionnaireReply__c' AND 
                                     DeveloperName =: GlobalConstants.QUESTIONARRE_REPLY_QUALIFICATION_RECORDTYPE];
            //declaring object out of the loop
            QuestionnaireReply__c queReply;
            for(QuestionAnswerWrapperClass wrap : wrapper)
            {
                if(questreplyMap.containsKey(wrap.Questions.Id))
                {
                    queReply= new QuestionnaireReply__c();
                    queReply=questreplyMap.get(wrap.Questions.Id);
                    queReply.Question__c = wrap.Questions.Id;
                    queReply.Answer__c = wrap.selectedAnswer;
                    queReply.Question_Category__c = wrap.Questions.Topic_Area__c;
                    queReply.RecordTypeId = QuestionaireReplyRecType.Id;
                    lstQuestionReply.add(queReply);
                }
            }
            if(!lstQuestionReply.isEmpty())
            {
                upsert lstQuestionReply;   
            }       
            if(!qtrecord.isEmpty()){
                score =[select id,Overall_Score__c from Questionnaire__c where id =:qtrecord[0].id].Overall_Score__c; 
            }
        }
        Catch(Exception e)
        {
            String msg = e.getMessage();
            String throwmsg=GlobalConstants.EMPTYSTRING;
            
            if (msg.CONTAINS(GlobalConstants.CUSTOM_VALIDATION_ERROR))
            {
                throwmsg = msg.substringBetween (GlobalConstants.CUSTOM_VALIDATION_ERROR,GlobalConstants.ERROR_MESSAGE_END);
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,throwmsg));
            }
        }
    }
   /******************************************************************************************************
    Method Name : cancel
    Description :Page Redirecting on clicking Cancel
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/       
    public Pagereference cancel()
    {     
        try
        {
            PageReference orderPage = new PageReference(GlobalConstants.URL_HARD_CODE + qtrecord[0].Opportunity__c);
            orderPage.setRedirect(true);
            return orderPage;
        }catch(exception e)
        {
        return null;
        }
    }
   /******************************************************************************************************
    Method Name : close
    Description :Page Redirecting on clicking Close
    Output :  void 
    Exception : NA    
    
    Version                 Date                    Author                       Summary Of Change
    --------------------------------------------------------------------------------------------------------- 
      1.0                      1/12/2015           Jacob Thomas                   Method created        
 *********************************************************************************************************/
    public Pagereference close()
    {     
        try
        {
            PageReference orderPage = new PageReference(GlobalConstants.URL_HARD_CODE+ qtrecord[0].Opportunity__c);
            orderPage.setRedirect(true);
            return orderPage;
        }catch(exception e)
        {
        return null;
        }
    }
}