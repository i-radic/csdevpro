/**
 * Project Name..........: Everything Everywhere
 * File..................: TrialStockItemHelper.cls
 * Version...............: 1 
 * Created by............: Martin Eley
 * Created Date..........: 10th September 2012
 * Last Modified by......:    
 * Last Modified Date....:   
 * Description...........: Provides methods for processing Trials Stock Items
 */
public with sharing class TrialStockItemHelper {
	/**
	 * Method to populate stock item details
	 * 
	 * @param List<Trial_Stock_Item__c> List of Trials Stock Items to be processed
	 * @return List of updated Trial Stock Items
	 */
	 public List<Trial_Stock_Item__c> GetStockItemDetails(List<Trial_Stock_Item__c> trialStockItems){
	 	//Build set of stock items ids
	 	Set<Id> stockItemIds = new Set<Id>();
	 	for(Trial_Stock_Item__c aTrialStockItem : trialStockItems){
	 		stockItemIds.add(aTrialStockItem.Stock_Item__c);
	 	}
	 	
	 	//Build map of stock items
	 	Map<Id, Stock_Item__c> stockItems = new Map<Id, Stock_Item__c>();
	 	for(Stock_Item__c aStockItem : [SELECT Id
	 		,SKU__r.Name__c
	 		,SKU__r.Type__c
	 		,Identification_Number__c
	 		,CTN__c
	 		FROM Stock_Item__c
	 		WHERE Id IN :stockItemIds]){
			stockItems.put(aStockItem.Id, aStockItem);
	 	}
	 		 	
	 	//Iterate over trial stock items and set set item details
	 	for(Trial_Stock_Item__c aTrialStockItem :trialStockItems){
	 		aTrialStockItem.Name__c = stockItems.get(aTrialStockItem.Stock_Item__c).SKU__r.Name__c;
	 		aTrialStockItem.Type__c = stockItems.get(aTrialStockItem.Stock_Item__c).SKU__r.Type__c;
	 		aTrialStockItem.Identification_Number__c = stockItems.get(aTrialStockItem.Stock_Item__c).Identification_Number__c;
	 		aTrialStockItem.CTN__c = stockItems.get(aTrialStockItem.Stock_Item__c).CTN__c;
	 	}
	 	
	 	return trialStockItems;
	 }
}