public with sharing class BpsNewAccountCon {
    
    
    public Account AccountTel{get;set;}
    public BpsNewAccountCon(ApexPages.StandardController controller) {
        
        AccountTel = new Account();
        
        List<User> U = new List<User>();
        
        U = [select id,Division,Department from User Where Id =: UserInfo.getUserId()];
        
        if(U.Size()>0){
            AccountTel.Sector__c = U[0].Division;
            AccountTel.Sub_Sector__c = U[0].Department;
        }
        if(Apexpages.currentpage().getparameters().get('Tel') != null){
            AccountTel.phone = Apexpages.currentpage().getparameters().get('Tel');
        }
    }
    
    public Pagereference SaveAccount(){
        
        Insert AccountTel;
        
        Pagereference  pg = new Pagereference('/'+AccountTel.id);
        Pg.setredirect(true);
        return pg;
    }
    

}