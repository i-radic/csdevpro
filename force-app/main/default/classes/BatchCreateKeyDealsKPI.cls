global class BatchCreateKeyDealsKPI implements Database.Batchable<sObject>, Database.Stateful
{
    global list<Opportunity> oppList = new list<Opportunity>(); //holds final results
    
    GlobalKPISettings__c gs = GlobalKPISettings__c.getInstance();
    GlobalKPI_Helper gh = new GlobalKPI_Helper();
    public string businessName  = gs.Business_Unit__c; 
    public string busCode       = gs.Business_Unit_Code__c; 
        
    public string theQuery;
    public string thisPeriod; 
    global List<Global_KPI__c> gkpiUpdList = new List<Global_KPI__c>();      
    global BatchCreateKeyDealsKPI ()
    {
        
        
        theQuery = 'select Account.Name, Name, Amount, Probability from Opportunity where Account_Sub_Sector__c =  \'TIKIT\' and isClosed = false and (closeDate = THIS_FISCAL_YEAR Or closeDate=NEXT_FISCAL_YEAR) and amount != NULL ORDER BY Amount DESC LIMIT 10';
        
        system.debug(theQuery);
        system.debug ('*****************************BatchCreateKeyDealsKPI'); 
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug ('*****************************BatchCreateKeyDealsKPI QueryLocator() method');     
        return Database.getQueryLocator(theQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.debug ('*****************************BatchCreateKeyDealsKPI Execute() method');  
        
        for(sObject s : scope) //loop through rows of scope list
        {
            Opportunity o = (Opportunity) s;
            oppList.add(o);
        }
        system.debug(oppList); 
        
    }    
    
        
    global void finish(Database.BatchableContext BC)
    {  
        
        system.debug ('*****************************BatchCreateKeyDealsKPI Finish() method');       
        
        string periodCode;
        
        periodCode='CU';//current
        for (integer i=0; i<oppList.size() ; i++)
        {
            Global_KPI__c kpi           = new Global_KPI__c();
            kpi.business_unit__c        = businessName;
            kpi.Measure_Name__c         = 'Key Deal';
            kpi.Unit_of_Measure__c      = 'Currency';
            kpi.Date_From__c            = date.today();
            kpi.Date_To__c              = date.today();
            system.debug(date.today());
            system.debug(i);    
            kpi.uniqueKey__c = busCode + ':' + 'OP'+ string.valueOf(i) + ':' + periodCode + ':' + '00000000' + ':' + '00000000';  
            kpi.Period_Type__c          = 'Current';
            system.debug(oppList[i].Amount);
            kpi.result__c               = oppList[i].Amount;
            kpi.flexText1__c            = oppList[i].Name; 
            kpi.flexNumber1__c          = oppList[i].Amount * (oppList[i].probability/100);
            
            //insert or update it using external ID field uniqueKey__c as the key
            system.debug(kpi);
            upsert kpi uniqueKey__c;
        }
            
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                         FROM AsyncApexJob WHERE Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.     
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BatchCreateKeyDealsKPI ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }


}