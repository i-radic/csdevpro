@isTest
private class Test_RecordTypeSelection {

    static testMethod void myUnitTest() {


        String opptyName, opptyLookup, retURLValue;
        Date uDate1 = date.today().addDays(7);
        Opportunity op = new Opportunity();
        op.Name = '&TESTB2B';
        op.StageName = 'Created';
        op.closedate = system.today();
        insert op;
        op.closedate = uDate1;
        op.StageName = 'Won';
        update op;

        BookToBill__c bb = new BookToBill__c();

        PageReference PageRef = Page.Book2BillRecordTypeSelection;
        test.setCurrentPage(PageRef);
        opptyName = '&TESTB2B';
        opptyLookup = op.Id;
        retURLValue = 'test';
        ApexPages.currentPage().getParameters().put('CF00N20000002oN6E', opptyName);
        ApexPages.currentPage().getParameters().put('CF00N20000002oN6E_lkid', opptyLookup);
        ApexPages.currentPage().getParameters().put('retURL', retURLValue);

        Apexpages.Standardcontroller sc = new Apexpages.Standardcontroller(bb);
        RecordTypeSelection RC = new RecordTypeSelection(sc);
        
        
        
        

        List < RecordType > RCT = [Select Id, Name, SOBJECTTYPE From RecordType where SOBJECTTYPE = 'BookToBill__c'];
        Map < string, string > RCMap = new Map < string, string > ();

        List < OpportunityLineItem > OLI = new List < OpportunityLineItem > ();
        List < OpportunityLineItem > OLI1 = new List < OpportunityLineItem > ();
        Opportunity Oppty = new Opportunity();
        Map < String, Decimal > ProdQuantity = new Map < String, Decimal > ();
        Decimal pstnCount = 0;
        set < string > stCompardIDs = new Set < String > ();


        RC.getItems();


        RC.getrecordTypes();
        String recordTypes = 'Calls And Lines';
        RC.setrecordTypes(recordTypes);
        RC.Continue1();
        RC.Cancel1();
        
        User B2BUser1;
        Profile B2BProfile1 = [select id from profile where name = 'Field: Standard User'];

        B2BUser1 = new User(alias = 'B2B1', email = 'B2B13cr@bt.com',
        emailencodingkey = 'UTF-8', lastname = 'Testing B2B1', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile1.Id, timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@bt.com',
        EIN__c = 'B2B3cr');
        insert B2BUser1;        
                
        System.runAs(B2BUser1) {
            RC.getItems();            
            //RC.Continue1();
        }

        User B2BUser2, B2bUser3, B2bUser4, B2BUser5, B2BUser6;
        //  Profile B2BProfile1 = [select id from profile where name='Field: Standard User'];
        Profile B2BProfile2 = [select id from profile where name = 'Field: BooktoBill Sales Manager'];
        Profile B2BProfile3 = [select id from profile where name = 'BookToBill Team'];
        Profile B2BProfile4 = [select id from profile where name = 'BookToBill WinBack'];
        Profile B2BProfile5 = [select id from profile where name = 'Major and Public Sales'];
        Profile B2BProfile6 = [select id from profile where name = 'Reporting Team'];

        B2BUser2 = new User(alias = 'B2B2', email = 'B2B23cr@bt.com',
        emailencodingkey = 'UTF-8', lastname = 'Testing B2B2', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile2.Id, timezonesidkey = 'Europe/London', username = 'B2BProfile1312cr@bt.com',
        EIN__c = 'B2B32cr');
        insert B2BUser2;
        System.runAs(B2BUser2) {
            RC.getItems();
            //RC.Continue1();
        }

        B2BUser3 = new User(alias = 'B2B23', email = 'B2B233cr@bt.com',
        emailencodingkey = 'UTF-8', lastname = 'Testing B2B2', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile3.Id, timezonesidkey = 'Europe/London', username = 'B2BProf132cr@bt.com',
        EIN__c = 'B22cr');
        insert B2BUser3;
        System.runAs(B2BUser3) {
            RC.getItems();          
        }
        
        B2BUser4 = new User(alias = 'B2B26', email = 'B2B24cr@bt.com',
        emailencodingkey = 'UTF-8', lastname = 'Testing B2B2', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile4.Id, timezonesidkey = 'Europe/London', username = 'B2BPro342cr@bt.com',
        EIN__c = 'B2B34cr');
        insert B2BUser4;
        System.runAs(B2BUser4) {
            RC.getItems();            
        }
        B2BUser5 = new User(alias = 'B2B25', email = 'B2B25cr@bt.com',
        emailencodingkey = 'UTF-8', lastname = 'Testing B2B2', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile5.Id, timezonesidkey = 'Europe/London', username = 'B2BPro345cr@bt.com',
        EIN__c = 'B2B35cr');
        insert B2BUser5;
        System.runAs(B2BUser5) {
            RC.getItems();            
        }
        B2BUser6 = new User(alias = 'B2B27', email = 'B2B27cr@bt.com',
        emailencodingkey = 'UTF-8', lastname = 'Testing B2B2', languagelocalekey = 'en_US',
        localesidkey = 'en_US', profileid = B2BProfile6.Id, timezonesidkey = 'Europe/London', username = 'B2BPro347cr@bt.com',
        EIN__c = 'B2B37cr');
        insert B2BUser6;
        System.runAs(B2BUser6) {
            RC.getItems();            
        }
        
        
        BookToBill__c bb1 = new BookToBill__c();
        PageReference PageRef1 = Page.Book2BillRecordTypeSelection;
        test.setCurrentPage(PageRef1);
        opptyName = '&TESTB2B';
        opptyLookup = null;
        retURLValue = 'test1';
        ApexPages.currentPage().getParameters().put('CF00N20000002oN6E', opptyName);
        ApexPages.currentPage().getParameters().put('CF00N20000002oN6E_lkid', opptyLookup);
        ApexPages.currentPage().getParameters().put('retURL', retURLValue);

        Apexpages.Standardcontroller sc1 = new Apexpages.Standardcontroller(bb1);
        RecordTypeSelection RC1 = new RecordTypeSelection(sc1);
        rc1.Cancel1();
        
        
    }
}