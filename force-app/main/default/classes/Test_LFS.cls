@isTest
private class Test_LFS {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
//set up accounts and contacts    

        Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
        Profile p1 = [select id from profile where name='Corporate: Leads from Service'];
        String profileId1 = p1.Id;
        
        User uM = new User();
        uM.Username = '999999000@bt.com';
        uM.Ein__c = '999999000';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = profileId;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});
    
        User uM1 = new User();
        uM1.Username = '999999001@bt.com';
        uM1.Ein__c = '999999001';
        uM1.LastName = 'TestLastname';
        uM1.FirstName = 'TestFirstname';
        uM1.MobilePhone = '07918672032';
        uM1.Phone = '02085878834';
        uM1.Title='What i do';
        uM1.OUC__c = 'DKW';
        uM1.Manager_EIN__c = '123456789';
        uM1.Email = 'no.reply@bt.com';
        uM1.Alias = 'boatid01';
        uM1.TIMEZONESIDKEY = 'Europe/London';
        uM1.LOCALESIDKEY  = 'en_GB';
        uM1.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM1.PROFILEID = '00e200000015P8S';
        uM1.LANGUAGELOCALEKEY = 'en_US';       
        uM1.email = 'no1.reply@bt.com';
        Database.SaveResult[] uMResult1 = Database.insert(new User [] {uM1});
   
        List<Account> acclist = new List<Account>();
         Account Account1 = Test_factory.createAccount();
         Account1.cug__c = 'CUGtest1Cug';
         Account1.OwnerId = uM.Id;
         Account1.Sub_Sector__c = 'Stratford Gold Customers';
         Account1.Sector__c = 'BT Corporate';
         Account1.Account_Owner_Department__c = 'BT Ireland';
        Database.SaveResult[] Account1R = Database.insert(new Account[] {Account1});
        
       
       Account Account2 = Test_factory.createAccount();
         Account2.cug__c = 'CUGtest1Cug2';
         Account2.OwnerId = uM.Id;
         
         Account2.Sub_Sector__c = 'Stratford Gold Customers';
         Account2.Sector__c = 'NI DBAM';
         Account2.Account_Owner_Department__c = 'BT Ireland';
       
       //Database.SaveResult[] Account2R = Database.insert(new Account[] {Account2});      
      
        Account Account3 = Test_factory.createAccount();
         Account3.cug__c = 'CUGtest1Cug3';
         Account3.OwnerId = uM.Id;
         
         Account3.Sub_Sector__c = 'Stratford Gold Customers';
         Account3.Sector__c = 'NI Enterprise';
         Account3.Account_Owner_Department__c = 'BT Ireland';
       //Database.SaveResult[] Account3R = Database.insert(new Account[] {Account3});  
      
       Account Account4 = Test_factory.createAccount();
         Account4.cug__c = 'CUGtest1Cug4';
         Account4.OwnerId = uM.Id;
         
         Account3.Sub_Sector__c = 'Stratford Gold Customers';
         Account4.Sector__c = 'NI DBAM';
         Account4.Account_Owner_Department__c = 'BT Ireland';
       //Database.SaveResult[] Account4R = Database.insert(new Account[] {Account4});
       
      // acclist.add(Account1);
       acclist.add(Account2);
       acclist.add(Account3);
       acclist.add(Account4);
       insert acclist;     
       
       Contact contact1 = Test_Factory.CreateContact();
        contact1.Cug__c = 'test1Cug';
        contact1.Phone = '000111222333';
        contact1.Email = 'test.Alan@test.com';
        contact1.Contact_Post_Code__c = 'WR5 3RL';
        contact1.Accountid = acclist[0].id;
        Database.SaveResult[] contact1R = Database.insert(new Contact[] {contact1});
  
       Opportunity Oppy1 = new opportunity();
       Oppy1.RecordTypeId = '01220000000ABFB';
       //Oppy1.AccountId = Account1.id;
       Oppy1.postcode__c = 'WR5 3RL';
       Oppy1.Customer_Contact_Number__c = '000111222333';
       oppy1.Email_Address__c = 'test.Alan@test.com';
       oppy1.Customer_Type__c = 'BT Business';
       oppy1.Product__c = 'pstn';
        oppy1.LFS_CUG__c = 'test1Cug';
        oppy1.StageName='Created';
             
       Database.SaveResult[] Oppy1R = Database.insert(new Opportunity[] {Oppy1});
      
       Opportunity opp=new Opportunity ();  
     opp = [select id, AccountId, Customer_Contact__c, Customer_Contact_Number__c, Email_Address__c from Opportunity where Id = :Oppy1R[0].id];
     
   
//  invoke relink

    
     LfsManagement rLink = new LfsManagement(); 
     rLink.opportunity = opp;
     rLink.relink();     
     
     Apexpages.StandardController stdController = new Apexpages.StandardController(Opp);
     LfsManagement controller = new LfsManagement (StdController);   

  
     

      Opportunity Oppty1 = new opportunity(Name='LFS_Test',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug', postcode__c='WR5 3RL', Product__c='BTBD Enquiry',AccountId = Account1.id,
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Created',Customer_Type__c='Corporate', Lead_Source__c ='Leads from Service',Originator_EIN__c='999999000',Sales_Agent_EIN__c='999999000');
 
 Opportunity Oppty2 = new opportunity(Name='LFS_Test1',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug2', postcode__c='WR5 3RL', Customer_Type__c='BT Business',AccountId = Account2.id,
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com',Product__c='Switch', StageName='Created',Lead_Source__c ='Leads from Service',Originator_EIN__c='999999000');
    
     Opportunity Oppty3 = new opportunity(Name='LFS_Test2',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug3', postcode__c='WR5 3RL', Customer_Type__c='Northern Ireland',AccountId = Account3.id,
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Created', Product__c='Mobile',Sub_Category__c='New Mobile (Non BT More than 5)',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service');
 
    Opportunity Oppty4 = new opportunity(Name='LFS_Test2',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug3', postcode__c='WR5 3RL', Customer_Type__c='BT Business',AccountId = Account2.id,
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Created', Product__c='Mobile',Sub_Category__c='New Mobile (Non BT More than 5)',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service');
 
 Opportunity Oppty5 = new opportunity(Name='LFS_Test2',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cu', postcode__c='WR5 3RL', Customer_Type__c='BT Business',AccountId = Account3.id,
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Created', Product__c='Mobile',Sub_Category__c='New Mobile (Non BT More than 5)',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service');
 
 Opportunity Oppty6 = new opportunity(Name='LFS_Test',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cu', postcode__c='WR5 3RL', Product__c='BTBD Enquiry',UKBS_Customer__c=true,AccountId = Account4.id,
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Created',Customer_Type__c='Corporate', Lead_Source__c ='Leads from Service',Originator_EIN__c='999999000');
 
 Opportunity Oppty7 = new opportunity(Name='LFS_Test1',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cu', postcode__c='WR5 3RL', Customer_Type__c='BT Business',AccountId = Account4.id,
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com',Product__c='Switch', StageName='Created',Lead_Source__c ='Leads from Service',Originator_EIN__c='999999000');
 
 
    List<Opportunity> opplist = new List <Opportunity>();  
    opplist.add(Oppty1);
    opplist.add(Oppty2);
    opplist.add(Oppty3);
    opplist.add(Oppty4);
    
    opplist.add(Oppty5);
    opplist.add(Oppty6);
    opplist.add(Oppty7);
    insert opplist;
        
 /*       
 //NEW CODE_ Anusha
        system.runas(uM){
 Opportunity Oppty1 = new opportunity(Name='LFS_Test',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug', postcode__c='WR5 3RL', 
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Straight Order',Customer_Type__c='BT Business', Lead_Source__c ='Leads from Service',Originator_EIN__c='999999000',Sales_Agent_EIN__c='999999000',UKBS_Customer__c=true,ownerid='00520000001Bu2U');
        
 Opportunity Oppty2 = new opportunity(Name='LFS_Test1',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug2', postcode__c='WR5 3RL', 
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com',Product__c='Switch', StageName='Lost',Lead_Source__c ='Leads from Service',Originator_EIN__c='999999000',ownerid='00520000001Bu2U');
 
 Opportunity Oppty3 = new opportunity(Name='LFS_Test2',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug3', postcode__c='WR5 3RL', Customer_Type__c='Northern Ireland',
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Straight Order', Product__c='Mobile',Sub_Category__c='New Mobile (Non BT More than 5)',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service');
 
 Opportunity Oppty4 = new opportunity(Name='LFS_Test3',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug4', postcode__c='WR5 3RL', Customer_Type__c='Northern Ireland',
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Return to Creator', Product__c='BTBD Enquiry',Originator_EIN__c='999999000',Sub_Category__c='New mobile (non-BT mobile)',Lead_Source__c ='Leads from Service');
 
 Opportunity Oppty5 = new opportunity(Name='LFS_Test5',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug3', postcode__c='WR5 3RL', 
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Straight Order', Product__c='BT Sport',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service',ownerid='00520000001Bu2U');
 
 Opportunity Oppty6 = new opportunity(Name='LFS_Test6',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug3', postcode__c='WR5 3RL', 
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Lost', Product__c='Mobile',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service',ownerid='00520000001Bu2U');
 
 Opportunity Oppty7 = new opportunity(Name='LFS_Test7',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug2', postcode__c='WR5 3RL', 
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Straight Order', Product__c='Mobile',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service',ownerid='00520000001Bu2U');

 Opportunity Oppty8 = new opportunity(Name='LFS_Test',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug6', postcode__c='WR5 3RL',
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Lost', Product__c='Mobile',Sub_Category__c = 'New Mobile (Non BT More than 5)',Originator_EIN__c='999999000',Sales_Agent_EIN__c='999999002',Lead_Source__c ='Leads from Service',ownerid='00520000001Bu2U');

  Opportunity Oppty9 = new opportunity(Name='LFS_Test',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug6', postcode__c='WR5 3RL', 
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Lost',product__c='BTBD Enquiry',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service');
  
  Opportunity Oppty10 = new opportunity(Name='LFS_Test',RecordTypeId='01220000000ABFB',LFS_CUG__c='test1Cug6', postcode__c='WR5 3RL', 
 Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Straight Order',Originator_EIN__c='999999000',Lead_Source__c ='Leads from Service',ownerid='00520000001Bu2U');
  
 //Opportunity Oppty10 = new opportunity(Name='LFS_Test',RecordTypeId='01220000000ABFB', postcode__c='WR5 3RL', 
// Customer_Contact_Number__c='000111222333',Email_Address__c='test.Alan@test.com', StageName='Won',ownerid='00520000001Bu2U',product__c='Switch',Lead_Source__c ='Leads from Service'); 
        
 List<Opportunity> OPPLIST = new List <Opportunity>();   
 OPPLIST.add(Oppty1);
   OPPLIST.add(Oppty2);
   OPPLIST.add(Oppty3);
   OPPLIST.add(Oppty4);
   OPPLIST.add(Oppty5);
   OPPLIST.add(Oppty6);
   OPPLIST.add(Oppty7);
   OPPLIST.add(Oppty8);
   OPPLIST.add(Oppty9);
   OPPLIST.add(Oppty10);
  // OPPLIST.add(Oppty10);
   insert OPPLIST;
   */
 
 /*
 OPPLIST[0].Customer_Type__c='Northern Ireland';
 OPPLIST[0].Sales_Agent_EIN__c = '';
 OPPLIST[1].Customer_Type__c='Northern Ireland';
 OPPLIST[1].Sales_Agent_EIN__c = '';            
 OPPLIST[2].Customer_Type__c='Northern Ireland';
 OPPLIST[3].Customer_Type__c='BT Business';
 OPPLIST[3].Sales_Agent_EIN__c = '';
 OPPLIST[4].Customer_Type__c='BT Business';
 OPPLIST[4].Sales_Agent_EIN__c = '';
 OPPLIST[4].Product__c = 'Mobile';
 OPPLIST[4].Sub_Category__c = 'New Mobile (Non BT More than 5)';
 OPPLIST[5].Customer_Type__c='Corporate';
 OPPLIST[5].Product__c ='BTBD Enquiry';
 OPPLIST[6].Customer_Type__c='Northern Ireland';
 OPPLIST[7].Customer_Type__c='BT Business'; 
 OPPLIST[8].Customer_Type__c='Corporate';
 OPPLIST[9].Product__c = 'BTBD Enquiry';
 //OPPLIST[9].Customer_Type__c='BT Business'; 
 update OPPLIST;
*/
 //OPPLIST[8].Product__c = 'BTBD Enquiry';
// update OPPLIST;
        //}  
    }
}