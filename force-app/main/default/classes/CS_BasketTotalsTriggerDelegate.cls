public with sharing class CS_BasketTotalsTriggerDelegate extends CS_TriggerHandler.DelegateBase {
    @TestVisible
    Map<Id, List<Product_Configuration_Totals__c>> configTotalsByBasketTotals = new Map<Id, List<Product_Configuration_Totals__c>>();
    
    // do any preparation here – bulk loading of data etc
    public override void prepareBefore() {
        List<Product_Configuration_Totals__c> configTotals = [SELECT
                                                                Id, Basket_Totals__c, Product__c, Product_Type__c, Product_Name__c, Quantity__c,
                                                                Product_Configuration__c, Total_Recurring_Charge__c, Charge_Type__c, Contract_Term_Period__c,
                                                                BT_Hardware_Fund__c, BT_Technology_Fund__c, Voice_Subscribers__c, Deal_Type__c, 
                                                                Product_Configuration__r.cscfga__Product_Family__c, Product_Configuration__r.Calculations_Product_Group__c,Product_Configuration__r.Product_Definition_Name__c,
                                                                 Total_One_Off_Charge__c, BTOP_Deal_Type__c,Product_Configuration__r.Airtime_Fund__c, BTOP_Voice_Subscribers__c
                                                              FROM
                                                                Product_Configuration_Totals__c
                                                              WHERE
                                                                Basket_Totals__c IN :Trigger.newMap.keySet()];
        for(Product_Configuration_Totals__c pcTotals : configTotals) {
            if(!configTotalsByBasketTotals.containsKey(pcTotals.Basket_Totals__c)) {
                configTotalsByBasketTotals.put(pcTotals.Basket_Totals__c, new List<Product_Configuration_Totals__c>());
            }
            
            configTotalsByBasketTotals.get(pcTotals.Basket_Totals__c).add(pcTotals);
        }
    }
    
    // do any preparation here - bulk loading of data etc
    public override void prepareAfter() {
        
    }
    
    // Apply before insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeInsert(sObject o) {

    }
    
    // Apply before update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeUpdate(sObject old, sObject o) {
        Basket_Totals__c basketTotals = (Basket_Totals__c) o;
        basketTotals.Total_Extra_Data__c = 0;
        basketTotals.Total_Extra_Text__c = 0;
        basketTotals.Total_Shared_Data__c = 0;
        basketTotals.Total_Extra_Shared_Data__c = 0;
        basketTotals.Total_Devices__c = 0;
        basketTotals.Default_Text_Usage__c = 0;
        basketTotals.Default_Voice_Usage__c = 0;
        basketTotals.Default_Usage_Profile_Total__c = 0;
        basketTotals.Total_International_Shared_Calling_Extra__c = 0;
        basketTotals.Total_Shared_Data_Extra_Recurring_Charge__c = 0;
        basketTotals.Daily_Usage_Factor__c = 0;
        basketTotals.Voice_Subsidy__c = 0;
        basketTotals.Data_Subsidy__c = 0;
//        basketTotals.BTOP_Deal_Type__c = '';
        basketTotals.Total_BTOP_Voice_Revenue__c = 0;
        basketTotals.BT_Autobalance_Users__c = 0;
        basketTotals.Total_BTOP_Voice_Users_Without_SIM_Only__c = 0;
        basketTotals.Total_BTOP_Landline_Users__c = 0;
        basketTotals.Total_BTOP_Calculated_MRC__c = 0;
        
        List<String> extrasInBasket = new List<String>();
        Set<String> typesInBasket = new Set<String>(); // changed List to Set to avoid duplicates
        List<String> typesInBasketClone = new List<String>(); // added to use string methids
        List<String> productsInBasket = new List<String>();
        System.debug('o.Id>>'+o.Id);
        List<Product_Configuration_Totals__c> configTotals = configTotalsByBasketTotals.get(o.Id);
        if(configTotals != null) {
            for(Product_Configuration_Totals__c pcTotals : configTotals) {
                if(pcTotals.Deal_Type__c != null && pcTotals.Deal_Type__c != '')
                    typesInBasket.add(pcTotals.Deal_Type__c);

                if(pcTotals.Product_Configuration__r.Calculations_Product_Group__c == 'BT OnePhone' && pcTotals.Product_Configuration__r.Product_Definition_Name__c == 'BTOP CORE')
                    basketTotals.BTOP_Deal_Type__c = pcTotals.BTOP_Deal_Type__c;

                if(pcTotals.Product__c == 'BT Mobile Extras') {
                    extrasInBasket.add(pcTotals.Product_Name__c + '|' + pcTotals.Product_Configuration__c);
                    
                    if(pcTotals.Product_Type__c == 'Single User Data' || pcTotals.Product_Type__c == 'Shared Data') {
                        basketTotals.Total_Extra_Data__c += pcTotals.Quantity__c;
                        if(pcTotals.Product_Type__c == 'Shared Data') {
                            basketTotals.Total_Extra_Shared_Data__c += pcTotals.Quantity__c;
                            basketTotals.Total_Shared_Data_Extra_Recurring_Charge__c += pcTotals.Total_Recurring_Charge__c;
                        }
                    }
                    else if(pcTotals.Product_Type__c == 'International Shared Calling') {
                        basketTotals.Total_International_Shared_Calling_Extra__c += pcTotals.Quantity__c;
                    }
                    else if(pcTotals.Product_Type__c == 'Domestic Text' || pcTotals.Product_Type__c == 'International Text Extra') {
                        basketTotals.Total_Extra_Text__c += pcTotals.Quantity__c;
                    }
                    else if(pcTotals.Product_Type__c == 'Value Added Services' && pcTotals.Total_Recurring_Charge__c > 0) {
                        basketTotals.BT_Autobalance_Users__c += pcTotals.Quantity__c;
                    }
                    
                    if(pcTotals.Charge_Type__c == 'Voice') {
                        basketTotals.Voice_Subsidy__c += pcTotals.BT_Hardware_Fund__c;
                    }
                    else if(pcTotals.Charge_Type__c == 'Data') {
                        basketTotals.Data_Subsidy__c += pcTotals.BT_Hardware_Fund__c;
                    }
                }
                else if(pcTotals.Product__c == 'Shared Data') {
                    basketTotals.Total_Shared_Data__c += pcTotals.Quantity__c;
                    
                    basketTotals.Data_Subsidy__c += pcTotals.BT_Hardware_Fund__c;
                }
                else if(pcTotals.Product__c == 'Sharer Subscription') {
                    if(pcTotals.Charge_Type__c == 'Data') {
                        basketTotals.Data_Subsidy__c += pcTotals.BT_Hardware_Fund__c;
                    }
                    else {
                        basketTotals.Voice_Subsidy__c += pcTotals.BT_Hardware_Fund__c;  
                    }
                }
                else if(pcTotals.Product__c == 'BT Mobile Hardware') {
                    if(pcTotals.Product_Type__c == 'Handset' || pcTotals.Product_Type__c == 'Tablet') {
                        basketTotals.Total_Devices__c += pcTotals.Quantity__c;
                    }
                }
                else if(pcTotals.Product__c == 'BT Mobile Broadband') {
                    basketTotals.Data_Subsidy__c += pcTotals.BT_Technology_Fund__c;
                }
                else if(pcTotals.Product__c == 'BT Mobile Flex') {
                    basketTotals.Voice_Subsidy__c += pcTotals.BT_Technology_Fund__c;
                }
                else if(pcTotals.Product__c == 'BTOP Company Extras') {
                    extrasInBasket.add(pcTotals.Product_Name__c + '|' + pcTotals.Product_Configuration__c);
                }
                else if(pcTotals.Product__c == 'BTOP User Subscription') {
                   if(pcTotals.Product_Type__c == 'Mobile Worker – SIM Only' || 
                        pcTotals.Product_Type__c == 'Mobile Worker – Subsidised' ||
                        pcTotals.Product_Type__c == 'Office Worker SIP - Subsidised' ||
                        pcTotals.Product_Type__c == 'Number Subscription Only') {
                        basketTotals.Total_BTOP_Voice_Revenue__c += pcTotals.Total_Recurring_Charge__c * pcTotals.Contract_Term_Period__c;
                    }
                    if(pcTotals.Product_Type__c == 'Mobile Worker – Subsidised' ||
                        pcTotals.Product_Type__c == 'Office Worker SIP - Subsidised' ||
                        pcTotals.Product_Type__c == 'Number Subscription Only') {
                        if(pcTotals.BTOP_Voice_Subscribers__c !=null) {   
                           basketTotals.Total_BTOP_Voice_Users_Without_SIM_Only__c += pcTotals.BTOP_Voice_Subscribers__c;
                        }
                    }
                }

                if(pcTotals.Product_Type__c != null) {
                    productsInBasket.add(pcTotals.Product_Type__c + '|' + pcTotals.Product_Configuration__c);
                }

                if(pcTotals.Product__c == 'BTOP Subscription Level Add-On' 
                    && pcTotals.Product_Type__c == 'Landline' 
                    && pcTotals.Product_Name__c == 'New Landline') {
                    basketTotals.Total_BTOP_Landline_Users__c += pcTotals.Quantity__c;
                }

                if(pcTotals.Product_Configuration__r != null && pcTotals.Product_Configuration__r.cscfga__Product_Family__c != null &&
                 pcTotals.Product_Configuration__r.cscfga__Product_Family__c.contains('BTOP')) {
                    if(pcTotals.Product_Type__c == 'Mobile Worker – SIM Only' || 
                        pcTotals.Product_Type__c == 'Mobile Worker – Subsidised' ||
                        pcTotals.Product_Type__c == 'Office Worker SIP - Subsidised' ||
                        pcTotals.Product_Type__c == 'Number Subscription Only') {
                            basketTotals.Total_BTOP_Calculated_MRC__c += pcTotals.Total_Recurring_Charge__c * pcTotals.Contract_Term_Period__c * 0.95;
                    }
                    else {
                        if (!(pcTotals.Product_Type__c == 'Roaming Extras' && pcTotals.Product_Name__c.contains('Daily'))) {
                            if(pcTotals.Total_Recurring_Charge__c!= null && pcTotals.Contract_Term_Period__c!= Null)
							basketTotals.Total_BTOP_Calculated_MRC__c += pcTotals.Total_Recurring_Charge__c * pcTotals.Contract_Term_Period__c;
						}
                    }

                    basketTotals.Total_BTOP_Calculated_MRC__c += pcTotals.Total_One_Off_Charge__c;
                }
            }
            Decimal TotalBuyAmt =0;
            for(Product_Configuration_Totals__c pcTotals : configTotals) {
                if(pcTotals.Product__c == 'BT Mobile Sharer') {
                    basketTotals.Daily_Usage_Factor__c = 0.11;
                    basketTotals.Default_Voice_Usage__c = 158.45;
                    basketTotals.Default_Text_Usage__c = basketTotals.Total_Extra_Text__c > 0 ? 145.00 : 65.00;
                    basketTotals.Default_Usage_Profile_Total__c = basketTotals.Total_Extra_Text__c > 0 ? 2.128 : 1.408;

                    if(String.isNotBlank(pcTotals.Product_Name__c) && pcTotals.Product_Name__c.contains('Unlimited')) {
                        basketTotals.Default_Text_Usage__c *= 1.1;
                        basketTotals.Default_Voice_Usage__c *= 1.1;
                        basketTotals.Default_Usage_Profile_Total__c *= 1.1;
                    }
                }
                else if(pcTotals.Product__c == 'BT Mobile Flex') {
                    basketTotals.Daily_Usage_Factor__c = 0.05;
                }
                else if(pcTotals.Product__c=='Mobile Voice SME'){
                   if(pcTotals.Product_Configuration__r.Airtime_Fund__c !=null){
                       TotalBuyAmt += pcTotals.Product_Configuration__r.Airtime_Fund__c;
                   }
               }
            }
            basketTotals.Total_BuyOut_Amount__c = TotalBuyAmt;
        }
        
        if(!extrasInBasket.isEmpty()) {
            basketTotals.BT_Extras_In_Basket__c = String.join(extrasInBasket, ',');
        }
        else {
            basketTotals.BT_Extras_In_Basket__c = '';   
        }
        typesInBasketClone.addAll(typesInBasket); // adding set to list to use string join
        if(!typesInBasketClone.isEmpty()) { // changed typesInBasket to typesInBasketClone
            basketTotals.Deal_Type__c = String.join(typesInBasketClone, ','); // changed typesInBasket to typesInBasketClone
        }
        else {
            basketTotals.Deal_Type__c = '';   
        }
        if(!productsInBasket.isEmpty()) {
            basketTotals.BT_Products_in_Basket__c = String.join(productsInBasket, ',');
        }
        else {
            basketTotals.BT_Products_in_Basket__c = '';   
        }
    }
    
    // Apply before delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeDelete(sObject o) {

    }

    // Apply after insert logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterInsert(sObject o) {
    }

    // Apply after update logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUpdate(sObject old, sObject o) {

    }

    // Apply after delete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterDelete(sObject o) {

    }

    // Apply after undelete logic to this sObject. DO NOT do any SOQL
    // or DML here – store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUndelete(sObject o) {
        
    }

    // finish logic - process stored records and perform any dml action
    // updates product baskets if bundle is desynchronised
    public override void finish() {

    }
}