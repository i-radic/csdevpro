public class LEMReviewImprovementPlanList {
	
	private final LEM_Review__c lr;
	private LEM_Review__c lrInfo;
	private List<LEM_Review_Improvement_Plan__c> LEMRIPlans;   
	private String sortDirection = 'ASC';   
	private String sortExp = 'name';
	
	public String sortExpression {
		get {
			return sortExp;     
		}     
		set {
			//if the column is clicked on then switch between Ascending and Descending modes       
			if (value == sortExp)         
				sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';       
			else         
				sortDirection = 'ASC';       
			sortExp = value;     
		}   
	}
	
	public String getSortDirection() {    
		//if not column is selected     
		if (sortExpression == null || sortExpression == '')      
			return 'ASC';    
		else     
			return sortDirection; 
	} 
	
	public void setSortDirection(String value) {
		sortDirection = value; 
	}
		
    public LEMReviewImprovementPlanList(ApexPages.StandardController stdController) {
        this.lr = (LEM_Review__c)stdController.getRecord();
        this.lrInfo = [Select l.Id, l.Name, l.BTLB_Master_Object__c From LEM_Review__c l where l.Id =: lr.Id];
        ViewData();
    }
    
    public List<LEM_Review_Improvement_Plan__c> getLEMRIPlans() {       
    	return LEMRIPlans;   
    }   
    
    public Id getLrId() {
		return lr.Id;
	}
	
	public String getLrName() {
		return lrInfo.Name;
	}
    
    public PageReference ViewData() {
    	string recId = ApexPages.currentPage().getParameters().get('id');       
    	//build the full sort expression       
    	string sortFullExp = sortExpression  + ' ' + sortDirection;             
    	//query the database based on the sort expression       
    	LEMRIPlans = Database.query('Select l.Targeted_Paid_Correctly__c, l.SystemModstamp, l.Status__c, l.Reason_For_Improvement_Plan__c, l.Prospect_Bank__c, l.PSM_Specialist_Learning__c, l.Number_Of_People_Selling__c, l.Name, l.MD_Learning__c, l.LastModifiedDate, l.LastModifiedById, l.LEM_Review__c, l.IsDeleted, l.Improvement_Plan_Start_Date__c, l.Improvement_Plan_End_Date__c, l.Next_Review_Date__c, l.Improvement_Area__c, l.Id, l.Forecast_At_Next_Review__c, l.CreatedDate, l.CreatedById, l.Closing_Outcome__c, l.Barriers_To_Sale__c, l.Activity_Plan__c From LEM_Review_Improvement_Plan__c l where l.LEM_Review__c=\'' + recId + '\' order by ' + sortFullExp + ' limit 1000');
    	return null;   
    }
}