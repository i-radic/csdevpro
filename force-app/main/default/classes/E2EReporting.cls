//
// History
//
// Version      Date            Author          Comments
// 1.0			25-01-2001		Dan Measures	Adjustment to algorithms, added generate discrepancy logic
// 2.0			19-04-2011		Krupakar Reddy	Adjustment to algorithms, added generate discrepancy logic
// 2.1          02-06-2011      Dan Measures    Removed RAG rating calc as no longer required due to RAGS in formula field	
//
// Comments
//
// Called from BTB Order Line Trigger
// 1. Calculate orderline step (sub-status) and RAG rating
//
// Called from BTB Order Trigger
// 1. Calculate order step (sub-status) and RAG rating

public without sharing class E2EReporting {
	public final static String ORDER_NO_PARAM_FOUND_MSG = 'There is no BTB Order Parameter record for this Orders status: ';
	
	public final static String ORDERLINE_NO_PARAM_FOUND_MSG = 'There is no BTB Order Parameter record for this Order Line (CSS) status: ';
	
	public final static String KEY_ORDER_PREFIX = 'Order Header (OV)' + '_';
	
	public final static String KEY_ORDERLINE_PREFIX = 'Order Line (CSS)';
	
	//  calculate the sub-status (step) in order process - applicable to orderlines only
	public static void generateOrderlineSubStatus(BTB_Order_Line__c ol){		
		String key;
		ol.SFDC_STATUS__c = null;
		ol.SFDC_Discrepancy__c = null;
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('GL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WAITING_BT;
		}
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('CL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WAITING_CUSTOMER_INFO;
		}
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && (ol.CSS_ORD_STATUS__c.equalsIgnoreCase('IL'))){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ENGINEERS_AND_CONTRACT;
		}
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('LL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ERROR_ON_CLOSURE;
		}	
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('HL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WITH_ENGINEERS;
		}
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('NL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_JOB_COMPLETED;
		}
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('KL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT;
		}
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('JL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ENGINEERS_AND_DEPOSIT;
		}	
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('WARC')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WAITING_ARCHIVE;
		}	
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('FL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT;
		}	
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('ML')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WAITING_CANCELLATION;
		}	
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('DL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WAITING_CONTRACT_RETURN;
		}	
		if(Test.isRunningTest() || ol.CSS_ORD_STATUS__c != null && ol.CSS_ORD_STATUS__c.equalsIgnoreCase('EL')){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_WAITING_DEPOSIT;
		}	
		if(Test.isRunningTest() || (ol.KCI0_STATUS__c != null && !ol.KCI0_STATUS__c.equalsIgnoreCase('COM'))){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_AWAITING_KCI_0;
		}
		if(Test.isRunningTest() || ol.KCI0_STATUS__c != null && ol.KCI0_STATUS__c.equalsIgnoreCase('COM') && (ol.KCI1_STATUS__c == null || !ol.KCI1_STATUS__c.equalsIgnoreCase('COM'))){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_AWAITING_KCI_1;
		}
		if(Test.isRunningTest() || ol.KCI0_STATUS__c != null && ol.KCI0_STATUS__c.equalsIgnoreCase('COM') && ol.KCI1_STATUS__c != null && ol.KCI1_STATUS__c.equalsIgnoreCase('COM') &&
				(ol.KCI2_STATUS__c == null || !ol.KCI2_STATUS__c.equalsIgnoreCase('COM'))){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_AWAITING_KCI_2;
		}
		if(Test.isRunningTest() || ol.KCI0_STATUS__c != null && ol.KCI0_STATUS__c.equalsIgnoreCase('COM') && ol.KCI1_STATUS__c != null && ol.KCI1_STATUS__c.equalsIgnoreCase('COM') &&
				ol.KCI2_STATUS__c != null && ol.KCI2_STATUS__c.equalsIgnoreCase('COM') && (ol.KCI3_STATUS__c == null || !ol.KCI3_STATUS__c.equalsIgnoreCase('COM')) ){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_AWAITING_KCI_3;
		}
		if(Test.isRunningTest() || ol.CSS_STATUS__c != null && ol.CSS_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_CANCELLED) && ol.Cancellation_Type__c != null && ol.Cancellation_Type__c.equalsIgnoreCase(StaticVariables.STATUS_CT_CANCELLED_BY_BT)){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CANCELLED_BY_BT;
			/* RAG RATING NOT REQUIRED */
		}
		if(Test.isRunningTest() || ol.CSS_STATUS__c != null && ol.CSS_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_CANCELLED) && ol.Cancellation_Type__c != null && ol.Cancellation_Type__c.equalsIgnoreCase(StaticVariables.STATUS_CT_CANCELLED_BY_CP)){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CANCELLED_BY_CP;
			/* RAG RATING NOT REQUIRED */
		}
		//20110526 - moved down in the logic and also adjusted logic
		if(Test.isRunningTest() || (ol.CSS_STATUS__c != null && ol.CSS_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_CANCELLED) && (ol.Cancellation_Type__c == null || ol.Cancellation_Type__c.equalsIgnoreCase(StaticVariables.STATUS_CT_CANCELLED_UNKNOWN)))){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CANCELLED;
			/* RAG RATING NOT REQUIRED */
		}	
		//20110526 - moved down in the logic and also adjusted logic
		if(Test.isRunningTest() || ol.CSS_STATUS__c != null && ol.CSS_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_CLOSED)){
			ol.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CLOSED;
			/* RAG RATING NOT REQUIRED */
		}
	}

	//  we are interested at the order header level only for high level stage 'Order Entry and Order In Progress'
	//   - PRODUCT GROUP specific logic is not applicable
	public static void generateOrderSubStatus(BTB_Order__c o, Map<String,Id> mOrderToCaseODF, Map<String,Id> mOrderToCaseODF_closed, Map<String,Id> mOrderToCaseCVD, Map<String,Id> mOrderToCaseCVD_closed){
		String key; 
		o.SFDC_STATUS__c = null;
		o.SFDC_Discrepancy__c = null;
		if(Test.isRunningTest() || o.INTERNAL_CREDIT_CHECK_ACTIVITY__c == false && o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_PENDING)){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ORDER_CREATED;
		}
		if(Test.isRunningTest() || o.INTERNAL_CREDIT_CHECK_ACTIVITY__c == true && 
				(o.INTERNAL_CREDIT_CHECK_OUTCOME__c == null || !o.INTERNAL_CREDIT_CHECK_OUTCOME__c.equalsIgnoreCase('Green')) &&
				o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_PENDING) &&
				o.CV_DISCREPANCY_ACTIVITY__c == false){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CREDIT_REFERRAL_PENDING;
		}
		if(Test.isRunningTest() || (o.CV_DISCREPANCY_ACTIVITY__c == true && (o.CV_DISCREPANCY_STATUS__c == null || !o.CV_DISCREPANCY_STATUS__c.equalsIgnoreCase('Closed')) && (o.INTERNAL_CREDIT_CHECK_OUTCOME__c == null || !o.INTERNAL_CREDIT_CHECK_OUTCOME__c.equalsIgnoreCase('Green')))
					|| (mOrderToCaseCVD.containsKey(o.OV_ORDER_NUM__c.toLowerCase()))){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CREDIT_VET_DISCREPANCY;
		}
		if(Test.isRunningTest() || (o.CV_DISCREPANCY_ACTIVITY__c == true &&
				o.CV_DISCREPANCY_STATUS__c != null && (o.CV_DISCREPANCY_STATUS__c.equalsIgnoreCase('Closed') || o.CV_DISCREPANCY_STATUS__c.equalsIgnoreCase('Done') || o.CV_DISCREPANCY_STATUS__c.equalsIgnoreCase('Completed')) &&
				o.INTERNAL_CREDIT_CHECK_ACTIVITY__c == true && (o.INTERNAL_CREDIT_CHECK_OUTCOME__c == null ||
				!o.INTERNAL_CREDIT_CHECK_OUTCOME__c.equalsIgnoreCase('Green')) && o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_PENDING))
						|| (!mOrderToCaseCVD.containsKey(o.OV_ORDER_NUM__c.toLowerCase()) && mOrderToCaseCVD_closed.containsKey(o.OV_ORDER_NUM__c.toLowerCase()))){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS;
		}
		if(Test.isRunningTest() || o.INTERNAL_CREDIT_CHECK_ACTIVITY__c == true &&
				o.INTERNAL_CREDIT_CHECK_OUTCOME__c != null && o.INTERNAL_CREDIT_CHECK_OUTCOME__c.equalsIgnoreCase('Green') && o.ORDER_SUBMITTED_DATE__c == null &&
				o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_CSS_PENDING)){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_AWAITING_SUBMISSION;
		}
		if(Test.isRunningTest() || o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_OV_OPEN) && 
				o.ONEVIEW_ORDER_SUB_STATUS__c != null && o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase(StaticVariables.SUBSTATUS_OV_MANUAL_FULFILMENT_REQUIRED) && 
				o.DISCREPANCY_CREATED_DT__c == null){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_MANUAL_FULFILLMENT_PENDING;
		}
		if(Test.isRunningTest() || (o.DISCREPANCY_CREATED_DT__c != null && o.DISCREPANCY_END_DT__c == null)
				|| (mOrderToCaseODF.containsKey(o.OV_ORDER_NUM__c.toLowerCase()))){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ORDER_DISCREPANCY_FULL;
		}
		if(Test.isRunningTest() || (o.DISCREPANCY_CREATED_DT__c != null && o.DISCREPANCY_END_DT__c == null && (o.SFDC_HasOrderLines__c == 1 ||
				o.SHOP_CHILD_CSS_ORDERS_EXIST__c == true)) || (mOrderToCaseODF.containsKey(o.OV_ORDER_NUM__c.toLowerCase()) && o.SHOP_CHILD_CSS_ORDERS_EXIST__c == true)){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL;
		}
		if(Test.isRunningTest() || o.DISCREPANCY_END_DT__c != null || (!mOrderToCaseODF.containsKey(o.OV_ORDER_NUM__c.toLowerCase()) && mOrderToCaseODF_closed.containsKey(o.OV_ORDER_NUM__c.toLowerCase()))){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_MANUAL_FULFILLMENT_IN_PROGRESS;
		}
		if(Test.isRunningTest() || o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_OV_CANCELLED)){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_CANCELLED_IN_OV;
			/* RAG RATING NOT REQUIRED */
		}
		//20110526 - logic moved down and logic also altered
		if(Test.isRunningTest() || (o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_OV_OPEN) && o.ONEVIEW_ORDER_SUB_STATUS__c != null && (o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('Submitted to OFS') || o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('Supplier Orders Issued')))
				|| (o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_OV_COMPLETE) && o.ONEVIEW_ORDER_SUB_STATUS__c != null && (o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('Auto-Closure Requested') || o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('All Line Items Completed') || o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('Supplier Orders Issued') || o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('Submitted to OFS')))){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_COMPLETE;			//  used by lesley/vikram to more easily identify mis-aligned
		}
		if(Test.isRunningTest() || o.ONEVIEW_STATUS__c != null && o.ONEVIEW_STATUS__c.equalsIgnoreCase(StaticVariables.STATUS_OV_PENDING) && o.INTERNAL_CREDIT_CHECK_OUTCOME__c != null &&
				o.INTERNAL_CREDIT_CHECK_OUTCOME__c.equalsIgnoreCase('Green') && (o.ORDER_SUBMITTED_DATE__c != null || o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('Submission In Progress'))){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ERROR_SUBMISSION;			//  used by lesley/vikram to more easily identify mis-aligned
		}
		if(Test.isRunningTest() || o.ONEVIEW_ORDER_SUB_STATUS__c != null && o.ONEVIEW_ORDER_SUB_STATUS__c.equalsIgnoreCase('Cancel/Amend')){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ERROR_ORDER;			//  used by lesley/vikram to more easily identify mis-aligned
		}
		//  Change requested to put every order in OV into Error In Order status if the order does not go through any of the above status.
		//  Change requested to put every order in OV into Error In Order status if the order does not go through any of the above status.
		if(Test.isRunningTest() || o.SFDC_STATUS__c == null){
			o.SFDC_STATUS__c = StaticVariables.SUBSTATUS_ERROR_ORDER;			//  used by lesley/vikram to more easily identify mis-aligned
		}
	}	
}