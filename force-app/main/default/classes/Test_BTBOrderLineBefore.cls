/*
*   NB) At time of writing Product Family Formula (custom field) returned product family.
*   With later datasets this may alter and the below test woudl need modifying.
*
*/
@isTest
private class Test_BTBOrderLineBefore {
    
    public final static String FAMILY_CALLS_LINES = 'Calls and Lines';
    
    public final static String ORDERTYPE_CALLS_LINES = 'Winback';
    
    static testMethod void myUnitTest() {
        
        //   set up test data - start
        Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
        
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99999';
        insert a;
        
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        
        /*Opportunity o1;
        o1 =  [Select Id, Opportunity_Id__c from  Opportunity where Name =: 'Dummy Opp For EDW'];*/
        
        //   CVD OR ODF COVERAGE
        List<User> thisUser = [Select id, Run_Apex_Triggers__c, Manager_EIN__c from User where id = :UserInfo.getUserId() Limit 1];
        thisUser[0].Run_Apex_Triggers__c = False;
        update thisUser[0]; 
        
        User uM = new User();
        uM.Username = '999999000@bt.com';
        uM.Ein__c = '999999000';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = profileId;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});

        User uGM = new User();
        uGM.Username = '999999001@bt.com';
        uGM.Ein__c = '999999001';
        uGM.LastName = 'TestLastname';
        uGM.FirstName = 'TestFirstname';
        uGM.MobilePhone = '07918672032';
        uGM.Phone = '02085878834';
        uGM.Title='What i do';
        uGM.OUC__c = 'DKW';
        uGM.Manager_EIN__c = '999999000';
        uGM.ManagerId = uMResult[0].id;
        uGM.Email = 'no.reply@bt.com';
        uGM.Alias = 'boatid01';
        uGM.TIMEZONESIDKEY = 'Europe/London';
        uGM.LOCALESIDKEY  = 'en_GB';
        uGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uGM.PROFILEID = profileId;
        uGM.LANGUAGELOCALEKEY = 'en_US';
        uGM.email = 'no.reply@bt.com';
        Database.SaveResult[] uGMResult = Database.insert(new User [] {uGM});
 
        User uDGM = new User();
        uDGM.Username = '999999002@bt.com';
        uDGM.Ein__c = '999999002';
        uDGM.LastName = 'TestLastname';
        uDGM.FirstName = 'TestFirstname';
        uDGM.MobilePhone = '07918672032';
        uDGM.Phone = '02085878834';
        uDGM.Title='What i do';
        uDGM.OUC__c = 'DKW';
        uDGM.Manager_EIN__c = '999999001';
        uDGM.ManagerId = uGMResult[0].id;
        uDGM.Email = 'no.reply@bt.com';
        uDGM.Alias = 'boatid01';
        uDGM.managerID = uGM.Id;
        uDGM.TIMEZONESIDKEY = 'Europe/London';
        uDGM.LOCALESIDKEY  = 'en_GB';
        uDGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uDGM.PROFILEID = profileId;
        uDGM.LANGUAGELOCALEKEY = 'en_US';
        uDGM.email = 'no.reply@bt.com';
        Database.SaveResult[] uDGMResult = Database.insert(new User [] {uDGM});
        
        User uSM = new User();
        uSM.Username = '999999003@bt.com';
        uSM.Ein__c = '999999003';
        uSM.LastName = 'TestLastname';
        uSM.FirstName = 'TestFirstname';
        uSM.MobilePhone = '07918672032';
        uSM.Phone = '02085878834';
        uSM.Title='What i do';
        uSM.OUC__c = 'DKW';
        uSM.Manager_EIN__c = '999999002';
        uSM.ManagerId = uDGMResult[0].id;
        uSM.Email = 'no.reply@bt.com';
        uSM.Alias = 'boatid01';
        uSM.managerID = uDGM.Id;
        uSM.TIMEZONESIDKEY = 'Europe/London';
        uSM.LOCALESIDKEY  = 'en_GB';
        uSM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uSM.PROFILEID = profileId;
        uSM.LANGUAGELOCALEKEY = 'en_US';
        uSM.email = 'no.reply@bt.com';
        insert uSM;
      
        //link user running test to hierachy created above
        thisUser[0].ManagerId = uSM.ID;
        update thisUser[0];
                
        RecordType rt = [select id from RecordType where SobjectType='Case' and name ='Order Discrepancy' limit 1];
        RecordType rt_creditvet = [select id from RecordType where SobjectType='Case' and name ='Credit Vet' limit 1];

        Case c1 = new case();
        c1.Vol_Reference__c = 'VOL011-12345678900';
        c1.Status = 'Assigned to Front Office';
        c1.Reason = 'test';
        c1.Front_Office_ein__c = '999999003';
        c1.RecordTypeId = rt.Id;
        insert c1;
        
        Case c2 = new case();
        c2.Vol_Reference__c = 'VOL011-12345678900';
        c2.Status = 'Assigned to Front Office';
        c2.Reason = 'test';
        c2.Front_Office_ein__c = '999999003';
        c2.RecordTypeId = rt_creditvet.Id;
        insert c2;
        
        BTB_Order__c bo = new BTB_Order__c();
        bo.Name = 'VOL-12345'; 
        bo.EIN__c = uSM.Ein__c;
        bo.OPPORTUNITY_ID__c = o.Opportunity_Id__c;
        bo.LE_CODE__c = a.LE_CODE__c;
        bo.BACKOFFICE_QUEUE_ENTER_DT__c = System.now() - 10;
        bo.CRED_CREATE_DT__c = System.now();
        //bo.CRED_END_DT__c = System.now();
        bo.DISCREPANCY_CREATED_DT__c = System.now();
        bo.DISCREPANCY_END_DT__c = System.now();
        bo.EDW_UPDATE_DT__c = System.now();
        bo.ORDER_CREATED_DT__c = System.now();
        insert bo;
        
        BTB_Order_Product__c op = new BTB_Order_Product__c();
        op.PROD_GROUP__c = FAMILY_CALLS_LINES;
        op.Order_Type__c  = ORDERTYPE_CALLS_LINES;
        op.BTB_Order__c = bo.Id;
        op.BTB_OV_ORDER_KEY__c = 'test123';
        
        //Start the Test Coverage    
        Test.startTest(); 
        BTB_Order_Line__c bol = new BTB_Order_Line__c();
        bol.BTB_ORDER__c = bo.Id;
        bol.BTB_CSS_ORDER_NO__c = 'test1234';
        bol.CSS_ORD_STATUS__c = 'GL';
        bol.KCI0_REQUIRED_BY_DT__c = System.now() - 15;
        bol.KCI1_REQUIRED_BY_DT__c = System.now() - 10;
        bol.KCI2_REQUIRED_BY_DT__c = System.now() - 8;
        bol.KCI3_REQUIRED_BY_DT__c = System.now() - 8;
        insert bol;
        Test.stopTest(); 
        //End the Test Coverage    
    }
}