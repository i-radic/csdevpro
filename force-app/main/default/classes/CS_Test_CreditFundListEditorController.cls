@isTest
private class CS_Test_CreditFundListEditorController {
    
    static cscfga__Product_Configuration__c generateTestData(String prodDefName, Boolean generateProductConfiguration) {
        
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = prodDefName,
            cscfga__Description__c = 'Test',
            cscfga__Active__c = true);
        insert pd;

        cscfga__Configuration_Screen__c cs = new cscfga__Configuration_Screen__c(
            Name = 'CPanel',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Index__c = 1,
            cscfga__Label__c = 'CPanel');
        insert cs;

        cscfga__Screen_Section__c ss = new cscfga__Screen_Section__c(
            Name = 'ScreenTop',
            cscfga__Configuration_Screen__c = cs.Id,
            cscfga__Index__c = 1,
            cscfga__Label__c = 'ScreenTop');
        insert ss;

        if (!generateProductConfiguration)
            return null;

        Account acc = new Account(Name = 'Dummy');
        insert acc;

        Opportunity opp = new Opportunity(Name = 'Test Opp', AccountId = acc.Id, StageName = 'Pending', CloseDate = Date.today());
        insert opp;

        cscfga__Product_Bundle__c pbun = new cscfga__Product_Bundle__c(Name = 'Test Bundle', cscfga__Opportunity__c = opp.Id);
        insert pbun;

        cscfga__Configuration_Offer__c co = new cscfga__Configuration_Offer__c(Name = 'Test Offer');
        insert co;

        cscfga__Product_Basket__c pbas = new cscfga__Product_Basket__c(Name = 'Test Basket', cscfga__Opportunity__c = opp.Id);
        insert pbas;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
            Name = 'TestProdConfig',
            cscfga__Description__c = 'Test',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Product_Bundle__c = pbun.Id,
            cscfga__Configuration_Offer__c = co.Id,
            cscfga__Product_Basket__c = pbas.Id);
        insert pc;

        return pc;

    }
    
    static {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
                
        Account a = new Account(
            Name = 'Test',
            NumberOfEmployees = 1
        );
        insert a;
        
        Opportunity o = new Opportunity(
            Name = 'Test',
            AccountId = a.Id,
            CloseDate = System.Today(),
            StageName = 'Closed Won',
            TotalOpportunityQuantity = 0
        );
        
        insert o;
        
        Usage_Profile__c up1 = new Usage_Profile__c(
            Active__c = true,
            Account__c = a.Id,
            Expected_SMS__c = 20,
            Expected_Voice__c = 10,
            Expected_Data__c = 20
        );
        
        insert up1;
        
        cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
            Name = 'Test',
            Default_Usage_Profile__c = up1.Id
        );
        insert pi1;
        
        cspmb__Rate_Card__c rc1 = new cspmb__Rate_Card__c(
            Name = 'Test'
        );
        insert rc1;
        
        cspmb__Rate_Card_Line__c rlVoice = new cspmb__Rate_Card_Line__c(
            Name = 'Voice',
            cspmb__Rate_Card__c = rc1.Id
        );
        insert rlVoice;
        
        cspmb__Rate_Card_Line__c rlSMS = new cspmb__Rate_Card_Line__c(
            Name = 'SMS',
            cspmb__Rate_Card__c = rc1.Id
        );
        insert rlSMS;
        
        cspmb__Rate_Card_Line__c rlData = new cspmb__Rate_Card_Line__c(
            Name = 'Data',
            cspmb__Rate_Card__c = rc1.Id
        );
        insert rlData;
        
        cspmb__Price_Item_Rate_Card_Association__c assoc = new cspmb__Price_Item_Rate_Card_Association__c(
            cspmb__Rate_Card__c = rc1.Id,
            cspmb__Price_Item__c = pi1.Id
        );
        insert assoc;
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c(
            Name = 'Credit Funds',
            cscfga__Description__c = 'Pd1',
            cscfga__Active__c = true
        );
        insert pd1;
        
        cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
            Name = 'Ad1',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'Related Product',
            cscfga__Product_Definition__c = pd1.Id,
            cscfga__high_volume__c = true,
            cscfga__Screen_Section__c = null,
            cscfga__Row__c = 1,
            cscfga__Column__c = 1
        );
        insert ad1;
        
        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
            Name = 'Pd2',
            cscfga__Description__c = 'Pd2'
        );
        insert pd2;
        
        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
            Name = 'Ad2',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'User Input',
            cscfga__Product_Definition__c = pd2.Id
        );
        insert ad2;
        
        cscfga__Available_Product_Option__c apo = new cscfga__Available_Product_Option__c(
            cscfga__Attribute_Definition__c = ad1.Id,
            cscfga__Product_Definition__c = pd2.Id
        );
        insert apo;
        
        CS_Related_Products__c rp = new CS_Related_Products__c(
            Name = 'Credit Funds',
            Related_Products__c = 'Pd2'
        );
        insert rp;
        
        CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
            Name = 'Pd2',
            Attribute_Names__c = 'Ad2',
            List_View_Attribute_Names__c = 'Ad2',
            Total_Attribute_Names__c = 'Ad2'
        );
        insert rpa;
        
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
            Name = 'Test',
            cscfga__Opportunity__c = o.Id
        );
        insert pb;
        
        cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
            Name = 'Root',
            cscfga__Product_Basket__c = pb.Id,
            cscfga__Product_Definition__c = pd1.Id,
            Service_Plan__c = pi1.Id
        );
        insert pc1;
        
        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
            Name = 'Child',
            cscfga__Root_Configuration__c = pc1.Id,
            cscfga__Parent_Configuration__c = pc1.Id,
            cscfga__Product_Basket__c = pb.Id,
            cscfga__Product_Definition__c = pd2.Id,
            cscfga__Attribute_Name__c = 'Ad1'
        );
        insert pc2;
        
        cscfga__Attribute__c at1 = new cscfga__Attribute__c(
            Name = 'Ad1',
            cscfga__Attribute_Definition__c = ad1.Id,
            cscfga__Value__c = pc2.Id,
            cscfga__Product_Configuration__c = pc1.Id
        );
        insert at1;
        
        cscfga__Attribute__c at2 = new cscfga__Attribute__c(
            Name = 'Ad2',
            cscfga__Attribute_Definition__c = ad2.Id,
            cscfga__Value__c = 'Test',
            cscfga__Product_Configuration__c = pc2.Id
        );
        insert at2;
        
        
    }
    
    static testmethod void instantiateController() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));

        System.assertNotEquals(null, ctrlr, 'Controller should be instantiated.');

        ctrlr.isEditable = true;
    }

    static testmethod void attributeEmptyTableTest() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));

        List<List<CS_CreditFundListEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }

    static testmethod void attributeTableTestWithProdDef() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));

        String prodDefName = 'Credit Funds';
        generateTestData(prodDefName, false);

        ctrlr.prodDefName = prodDefName;
        System.assertEquals(prodDefName, ctrlr.prodDefName);
        System.assertEquals('New ' + prodDefName, ctrlr.prodConfEditorTitle);
        System.assertEquals('Save New ' + prodDefName, ctrlr.saveButtonText);
        System.assertEquals('Clear Editor', ctrlr.cancelButtonText);

        List<List<CS_CreditFundListEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }

    static testmethod void attributeTableTestWithProdConf() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;
        //ctrlr.columnsForDisplay = 'Type__c';

        System.assertEquals('Currently editing: [' + pc.Name + ']', ctrlr.prodConfEditorTitle);
        System.assertEquals('Save Changes', ctrlr.saveButtonText);
        System.assertEquals('Cancel Changes', ctrlr.cancelButtonText);

        List<cscfga__Product_Configuration__c> confList = ctrlr.productConfigList;

        List<List<CS_CreditFundListEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }

    static testmethod void saveWithProductDefinitionTest() {
        //cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        
        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(Id = pc.cscfga__Product_Basket__c);
        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(basket));
        ctrlr.prodDefName = prodDefName;
        ctrlr.prodBasketId = pc.cscfga__Product_Basket__c;

        List<List<CS_CreditFundListEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
        Test.startTest();
        cscfga.ProductConfiguratorController.setParameters(new Map<String,String> {'containerType'=>'basket'});
        ctrlr.saveProdConfig();
        Test.stopTest();
    }

    static testmethod void saveWithProductConfigurationTest() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];
        

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;

        List<List<CS_CreditFundListEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
		Test.startTest();
        ctrlr.saveProdConfig();
        Test.stopTest();
    }

    static testmethod void editTest() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));
        ctrlr.edit();
    }

    static testmethod void clearTest() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));
        ctrlr.clearEditor();
        System.assertEquals(null, ctrlr.prodConfigId);
        System.assertEquals(null, ctrlr.prodConfig);
    }
/*
    static testmethod void deleteOpenPCTest() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));
        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;
        List<cscfga__Product_Configuration__c> confList = ctrlr.productConfigList;

        ctrlr.deleteOpenProdConfig();

        List<List<CS_CreditFundListEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }

    static testmethod void deleteSelectedPCTest() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));

        String prodDefName = 'Credit Funds';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);

        ctrlr.prodDefName = prodDefName;
        ctrlr.prodConfigId = pc.Id;
        ctrlr.prodConfigIdToDelete = pc.Id;
        List<cscfga__Product_Configuration__c> confList = ctrlr.productConfigList;

        ctrlr.deleteProdConfig();

        List<List<CS_CreditFundListEditorController.AttributeTableRow>> attributeTable = ctrlr.attributeTable;
    }
*/
    static testmethod void formatFieldMessageTest() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));
        cscfga.FieldMessage fm = new cscfga.FieldMessage('TestReference_0', 'TestMessage');

        System.assertEquals('Error on field [TestReference]: TestMessage', ctrlr.formatFieldMessage(fm));
    }
    
    static testmethod void instantiateController2() {
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));
        //System.assertNotEquals(null, ctrlr, 'Controller should be instantiated.');
        //System.assertEquals(pb.Id, ctrlr.prodBasketId, 'prodBasketId incorrectly assigned.');
        //System.assertEquals(pb.Name, ctrlr.prodBasketDisplayName, 'prodBasketId incorrectly assigned.');
    }

    static testmethod void basketRetrieverTest() {
        CS_CreditFundListEditorController ctrlr =
            new CS_CreditFundListEditorController(new ApexPages.StandardController(new cscfga__Product_Basket__c(Name = 'Test')));

        //System.assertEquals(null, ctrlr.getProductBasket(null), 'There should be no baskets.');

        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];

        //System.assertEquals(pb.Id, ctrlr.getProductBasket(pb.Id).Id, 'Should have returned a basket.');

        delete pb;

        //System.assertEquals(null, ctrlr.getProductBasket(pb.Id), 'There should be no baskets.');
    }
    
    static testmethod void reRenderBasketTest(){
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];
        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));
        PageReference pr = ctrlr.reRenderBasket();
        System.assertEquals(null, pr, 'Should be null');
    }
    
    static testmethod void getProductBasketTest(){
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];
        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));
        cscfga__Product_Basket__c pb2 = ctrlr.getProductBasket(pb.Id);
        System.assertEquals(pb2.id, pb.id, 'Should be same basket id');
    }
    
    static testmethod void checkAsyncJobStatusTest(){
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];
        Boolean res = CS_CreditFundListEditorController.checkAsyncJobStatus(pb.Id);
        System.assertEquals(true, res, 'Should be true because no jobs are made');
    }
    
    static testmethod void getMLEIframeSourceTest(){
        
        cscfga__Product_Basket__c pb = [select Id, Name from cscfga__Product_Basket__c limit 1];
        CS_CreditFundListEditorController ctrlr = new CS_CreditFundListEditorController(new ApexPages.StandardController(pb));

        String prodDefName = 'Test Name';
        cscfga__Product_Configuration__c pc = generateTestData(prodDefName, true);
        ctrlr.prodDefName = prodDefName;
        
        System.assertNotEquals('/SomeInvalidUri/', ctrlr.getMLEIframeSource(), 'Should return valid URI based on Db data');

    }
    
    static testmethod void attrsTest(){
        
        CS_CreditFundListEditorController.AttributeTableRow atr = new CS_CreditFundListEditorController.AttributeTableRow();
        CS_CreditFundListEditorController.AttributeTableRowItem atri = new CS_CreditFundListEditorController.AttributeTableRowItem();
        
        atri.attribute = null;
        atri.sectionIndex = 1;
        atri.sectionName = 'Dummy';
        
        atr.leftAttribute = null;
        atr.leftAttribute = null;
        atr.sectionIndex = 2;
        atr.sectionName = 'Second one';
        atr.row = 2;
    }   
}