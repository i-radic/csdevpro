public with sharing class KCIEditController_Main{

    Id KCIRTId,kciId;
    String RecordTypeName;
    
    KCI__c kci; 
        
    public KCIEditController_Main(ApexPages.StandardController controller) {
        kci = (KCI__c)controller.getRecord();             
    }
       
    public PageReference OnLoad(){
        
        KCIRTId = [Select RecordTypeId from KCI__c where Id=:kci.Id].RecordTypeId;
        
        RecordTypeName = [Select DeveloperName from RecordType where Id=:KCIRTId].DeveloperName; 
        
        if(RecordTypeName=='Validation_Call'){  
                  
            PageReference RD = new PageReference('/apex/KCIEditPage_Validation?id='+kci.Id+'&retURL='+kci.Id+'&RecordType='+kci.RecordTypeId+'&nooverride=1');
            RD.setRedirect(true);
            return RD;
        }
        else if(RecordTypeName=='Confirmation_Call'){
                    
            PageReference RD = new PageReference('/apex/KCIEditPage_Confirmation?id='+kci.Id+'&retURL='+kci.Id+'&RecordType='+kci.RecordTypeId+'&nooverride=1');
            RD.setRedirect(true);
            return RD;
        }
    return null;
    }
    
     
    
    
    }