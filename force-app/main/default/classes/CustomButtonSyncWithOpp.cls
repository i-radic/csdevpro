global with sharing class CustomButtonSyncWithOpp extends csbb.CustomButtonExt {
 
    public String performAction (String basketId) {
		cscfga__Product_Basket__c pb = [select id, cscfga__Opportunity__c, cscfga__Basket_Status__c from cscfga__Product_Basket__c where id = :basketId];
        
        String newUrl = '';
        String status = '';
        
        if(pb.cscfga__Basket_Status__c == 'Valid'){
            String res = CS_SyncBasketAndOpp.SyncBasket(basketId);
            if(res.contains('error')){
                status = 'nok';
            } else {
                status = 'ok';
                newUrl = '/'+res;
            }
        } else {
            status = 'nok';
        }
        return '{"status":"'+status+'","redirectURL":"' + newUrl + '"}';
    }
}