@isTest
private class BatchDeleteForecastPortfolioArea_Test{

    static testMethod void testOne() {
      User thisUser = [select id from User where id=:userinfo.getUserid()];
	  System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
     
     	upsert settings TriggerDeactivating__c.Id; 
          
      Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
      insert u1;  
      Account a = Test_Factory.CreateAccount();
      a.name = 'TESTCODE 2';
      a.sac_code__c = 'testSAC';
      a.Sector_Code__c = 'BTLB';
      a.LOB_Code__c = 'LOB';
      a.OwnerId = u1.Id;
      a.CUG__c='CugTest';
      insert a;
      Opportunity o = Test_Factory.CreateOpportunity(a.Id);
      insert o;
      Forecasting_Portfolio_Area__c fpa = new Forecasting_Portfolio_Area__c(Opportunity__c = o.Id);
      insert fpa;
      Test.startTest(); 
    string q = 'select id from Forecasting_Portfolio_Area__c where Opportunity__c = \'' + o.Id + '\'';
        BatchDeleteForecastPortfolioArea b = new BatchDeleteForecastPortfolioArea(q);
        ID batchprocessid = Database.executeBatch(b,200);
        Test.stopTest();
      List<Forecasting_Portfolio_Area__c> list_fpa = [select id from Forecasting_Portfolio_Area__c where Opportunity__c =:o.id];
      System.assertEquals(0, list_fpa.size());  //should now be deleted  
    }
    }
}