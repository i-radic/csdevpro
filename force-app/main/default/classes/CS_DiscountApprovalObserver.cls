global without sharing class CS_DiscountApprovalObserver implements csdiscounts.IApprovalObserver {
	/*
	approvalEvent => approve or reject 
	recordId => pc or basket
	*/
	private String CONTROLLER_NAME = 'CS_DiscountApprovalObserver';

	public String execute(Id recordId, String approvalEvent){
		logger('recordId = ' + recordId);
		logger('approvalEvent = ' + approvalEvent);

		String objectType = recordId.getSObjectType().getDescribe().getName(); //cscfga__Product_Basket__c or cscfga__Product_Configuration__c
		if(objectType.equalsIgnoreCase('cscfga__Product_Configuration__c')){
			cscfga__Product_Configuration__c pc = [
				SELECT Id, Name, Approval_Level_4__c,Approval_Level_3__c, Approval_Level_2__c, Approval_Level_1__c, Approval_Status__c, cscfga__Product_Basket__c
				FROM cscfga__Product_Configuration__c
				WHERE Id = :recordId
				LIMIT 1
			];
			logger('pc = ' + pc);

			if(pc != null && (pc.Approval_Status__c.equalsIgnoreCase('Approved') || pc.Approval_Status__c.equalsIgnoreCase('Rejected'))){
				String appStatus = pc.Approval_Status__c.equalsIgnoreCase('Approved') ? 'Approve' : 'Reject';
				cscfga__Product_Basket__c basket = [
					SELECT Id, BT_Basket_Approval_Status__c
					FROM cscfga__Product_Basket__c
					WHERE Id = :pc.cscfga__Product_Basket__c
					LIMIT 1
				];
				Map<Id, cscfga__Product_Configuration__c> productConfigurationsMap = CS_Util.getConfigurationsInBasket(basket.Id, CONTROLLER_NAME);
				Map<String, Map<String, Object>> productFamilyJSONMap = CS_Util.getProductFamiliesJSONMap(CONTROLLER_NAME);
				Map<Id, CS_BasketConfigurationWrapper> basketConfigsMap = CS_Util.fillBasketConfigsMap(new Set<Id>{basket.Id});
                CS_Util.fillProductConfigurationsSetInBasketConfigsMap(productConfigurationsMap, basketConfigsMap, productFamilyJSONMap);
                
                String approvalFamily = '';
                CS_BasketConfigurationWrapper wrapper = basketConfigsMap.get(basket.Id);
                for(String pFamily : wrapper.basketProductFamilyMap.keySet()) {
                	Set<cscfga__Product_Configuration__c> configs = wrapper.basketProductFamilyMap.get(pFamily);
                	for(cscfga__Product_Configuration__c cfg : configs) {
                		if(cfg.Id == pc.Id) {
                			approvalFamily = pFamily;
                			break;
                		}
                	}
                }
                logger('approvalFamily = ' + approvalFamily);
                
                if(String.isNotBlank(approvalFamily)) {
                	Map<Id, cscfga__Product_Configuration__c> approvalConfigs = new Map<Id, cscfga__Product_Configuration__c>(new List<cscfga__Product_Configuration__c>(wrapper.basketProductFamilyMap.get(approvalFamily)));
                	List<ProcessInstanceWorkitem> approvals = [Select ProcessInstance.Status, ProcessInstance.TargetObjectId, ProcessInstanceId, Id
                			  									From ProcessInstanceWorkitem
							  									Where ProcessInstance.TargetObjectId = : approvalConfigs.keySet() and ProcessInstance.Status = 'Pending'];
					List<Approval.ProcessWorkitemRequest> allReq = new List<Approval.ProcessWorkitemRequest>(); 
					for (ProcessInstanceWorkitem item : approvals) {
						Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
						req.setComments('Auto Approved/Rejected');
				        req.setAction(appStatus);
				        req.setWorkitemId(item.Id);
				        allReq.add(req);
				    }
				    
				    Approval.ProcessResult[] results =  Approval.process(allReq);
                }    

				logger('basket = ' + basket);
				basket.BT_Basket_Approval_Status__c = pc.Approval_Status__c;
				update basket;
			}
		}
		else{
			//cscfga__Product_Basket__c

		}

		return '';
	}

	@TestVisible private void logger(String msg){
    	system.debug(LoggingLevel.INFO, 'CS_DiscountApprovalObserver.' + msg);
    }
}