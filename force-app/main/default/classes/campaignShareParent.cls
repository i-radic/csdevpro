public class campaignShareParent {

    private Campaign thisCampaign = null;
    
    public campaignShareParent(ApexPages.StandardController controller) {
        System.debug('Initialise campaignShareParent...');
        thisCampaign = (Campaign)controller.getRecord();
    }
    
    public pageReference doRedirect() {
        pageReference campaignPage;
        String pageURL = '/' + thisCampaign.Id;
        
        campaignPage = new PageReference(pageUrl);
        campaignPage.setRedirect(true);
        
        if(thisCampaign.Parentid != null) {

                campaignShare toDelete = [select id from campaignShare where (userorgroupid = :userinfo.getuserid() and campaignid = :thisCampaign.Parentid)];
                try{
                delete todelete;
                }
                catch(Exception e){
                    // if we cannot delete the record the owner is editing the record
                }
            
        }
        
        return campaignPage;
    }
    
    public pageReference doEditRedirect() {
        pageReference campaignPage;

        String pageURL = '/' + thisCampaign.Id + '/e?retURL=/apex/campaignShareParent?id=' + thisCampaign.Id + '&nooverride=1' ;
        
        campaignPage = new PageReference(pageUrl);
        campaignPage.setRedirect(true);
        
        if(thisCampaign.parentid != null) {
            //JMM 24/02/11- Added a check to see if the sharing exists before actioning
            List<campaignShare> checkCamp = [SELECT Id FROM campaignShare WHERE UserOrGroupId = :userinfo.getuserid() AND campaignId = :thisCampaign.parentId LIMIT 1];
            System.debug('campShare: ' + checkCamp );
            If(checkCamp.size() == 0) {  
            // JMM END
                campaignShare thisCampShare = new campaignShare(UserOrGroupId = userinfo.getuserid(), campaignId = thisCampaign.parentId, campaignaccesslevel='Read');
                insert thisCampShare;
                System.debug('campShareExe: ' + thisCampShare);
             }
        }
        
        return campaignPage;
    }
    
    
}