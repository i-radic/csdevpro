/**
 *	@author: astadia ???
 *	
 *	Version History :   
 *	2013-08-01 - #831 - AG
 *	only update Project records where the project RecordType.DeveloperName = "CPM"
 *		
 */
public class OpportunityTriggerUtilities {
	
	public static boolean companyImplementationTrigger = false;
	public static void updateProjects(list<Opportunity> oppsWithProjectsToUpdate) {
		// Get all the Ids of the Opportunities
		list<String> oppIds = new List<String>();
		for (Opportunity o : oppsWithProjectsToUpdate) {
			oppIds.add(o.Id);
		}
		// get All the Projects belonging to those Opportunities
		List<Project__c> projectsToUpdate = [SELECT Stage__c, Notify_Project_Manager__c FROM Project__c 
											where Opportunity__c in :oppIds and RecordType.DeveloperName = 'CPM'];
		if (projectsToUpdate == null || projectsToUpdate.size() < 1) {
			// Do Nothing
		} else {
			//loop through the projects
			for (Project__c proj : projectsToUpdate) {
				// Set the stage to 1 Project
				proj.stage__c = 'Project';
				// Set Notify Project Manager to true - Workflow will then send an email
				proj.Notify_Project_Manager__c = true;
		    }
		    update projectsToUpdate;
	    }
	}
	
}