Global Class CS_SyncBasketAndOpp {
	webservice static String SyncBasket(Id basketId) {
		Savepoint sp = Database.setSavepoint();
		try {
            String productCode = '';
            Map<Id, cscfga__Product_Configuration__c> configs = CS_Util.getConfigurationsInBasket(basketId, 'CS_SyncBasketAndOpp');
            for (cscfga__Product_Configuration__c pc : configs.values()) {
                if(pc.Product_Definition_Name__c == 'Cloud Phone') {
                	productCode = pc.Product_Code__c;
            	}
            }

			cscfga__Product_Basket__c productBasket = Database.query('SELECT ' + CS_Utils.getSobjectFields('cscfga__Product_Basket__c')
			+ ' from cscfga__Product_Basket__c where Id = \'' 
					+ basketId + '\'');
            /*productBasket.Synchronised_with_Opportunity__c = false;
            productBasket.csbb__Synchronised_with_Opportunity__c = false;
            update productBasket;
            */
			// 20.02.2018 CSP - T-35734 - unsync other baskets from opp & sync new basket
			List<cscfga__Product_Basket__c> productBaskets = [	SELECT id, csbb__Synchronised_with_Opportunity__c, synchronised_with_Opportunity__c
																FROM cscfga__Product_Basket__c
																WHERE cscfga__Opportunity__c =: productBasket.cscfga__Opportunity__c
																	AND id !=: productBasket.Id];

			if(productBaskets != NULL && productBaskets.size() > 0){
				for(cscfga__Product_Basket__c basket : productBaskets){
					basket.Synchronised_with_Opportunity__c 		= false;
					basket.csbb__Synchronised_with_Opportunity__c 	= false;
				}
			}
			productBasket.Synchronised_with_Opportunity__c = true;
            productBasket.csbb__Synchronised_with_Opportunity__c = true;
            if(productBasket.Basket_Sync_Counter__c!=null){
                productBasket.Basket_Sync_Counter__c =  productBasket.Basket_Sync_Counter__c+1;
            }else{
                productBasket.Basket_Sync_Counter__c =1;
            }
            
			productBaskets.add(productBasket);
			update productBaskets;

			
			SyncThunderhead(productBasket, productCode);
			List<OpportunityLineItem> olis = [select Id, Name, BigMachines_Quote__c, Product_Family__c
				from OpportunityLineItem
				where OpportunityId = :productBasket.cscfga__Opportunity__c];
			if (!olis.isEmpty()) {
				for (OpportunityLineItem oli : olis) {
					oli.BigMachines_Quote__c = 'CPQ';
				}
			}
			update olis;
			
			CustomButtonSynchronizeWithOpportunity.syncCRFForBTNet(productBasket,configs);
			
			return productBasket.cscfga__Opportunity__c;
			
			
		}
		catch (Exception ex) {
			Database.rollback(sp);
			system.debug('Sync basket exception: ' + ex);
			return 'error: ' + ex;
		}
	}
	
	private static void SyncThunderhead(cscfga__Product_Basket__c productBasket, String productCode) {
		List<Schema.FieldSetMember> basketFields = SObjectType.cscfga__Product_Basket__c.FieldSets.Opportunity_Sync.getFields();
		Opportunity currOpp = new Opportunity(Id = productBasket.cscfga__Opportunity__c);
		
		for (Schema.FieldSetMember field : basketFields) {
			currOpp.put(field.getFieldPath(), productBasket.get(field.getFieldPath()));
		}
		currOpp.Basket_Name__c = productBasket.Name;
		currOpp.ChequeFlag__c = 'No';
		currOpp.FCC_2__c = false;
		currOpp.Total_Revenue_Guarantee__c = productBasket.Total_Revenue_Guarantee__c;
		currOpp.Competitive_Clause__c = productBasket.Competitive_Clause__c;
		currOpp.Averaging_Co_Terminus__c = productBasket.Averaging_Co_Terminus__c;
		currOpp.Full_Co_Terminus__c = productBasket.Full_Co_Terminus__c;
		currOpp.Unlocking_Fees__c = productBasket.Unlocking_Fees__c;
		currOpp.Total_Revenue_Contract__c = productBasket.Total_Contract_Guarantee__c;
		currOpp.Disconnect_Allowances__c = productBasket.Disconnect_Allowances__c;
		if (productBasket.Rolling_Air_Time__c != null && productBasket.Rolling_Air_Time__c > 0) {
			currOpp.RollingAirtimeFlag__c = 'Yes';
		} else {
			currOpp.RollingAirtimeFlag__c = 'No';
		}
		if (productBasket.Staged_Air_Time__c != null && productBasket.Staged_Air_Time__c > 0) {
			currOpp.StagedAirtimeFlag__c = 'Yes';
		} else {
			currOpp.StagedAirtimeFlag__c = 'No';
		}
		if (productBasket.Tech_Fund__c != null && productBasket.Tech_Fund__c > 0) {
			currOpp.TechFundFlag__c = 'Yes';
		} else {
			currOpp.TechFundFlag__c = 'No';	
		}
		currOpp.FCC__c = productBasket.FCC__c;
		if (currOpp.FCC__c != null && currOpp.FCC__c > 0) {
			currOpp.FCC_2__c = true;
		}
		currOpp.Unconditional_cheque__c = productBasket.Unconditional_cheque__c;
		if (productBasket.Unconditional_cheque__c != null && productBasket.Unconditional_cheque__c > 0) {
			currOpp.Unconditional_cheque_2__c = 'Yes';
			currOpp.ChequeFlag__c = 'Yes';
		} else {
			currOpp.Unconditional_cheque_2__c = 'No';
			currOpp.ChequeFlag__c = 'No';
		}
		currOpp.Buy_out_Cheque__c = productBasket.Buy_out_cheque__c;
		if (productBasket.Buy_out_cheque__c != null && productBasket.Buy_out_cheque__c > 0) {
			currOpp.Buy_out_cheque_2__c = 'Yes';
			currOpp.ChequeFlag__c = 'Yes';
		} else {
			currOpp.Buy_out_cheque_2__c = 'No';
			if (currOpp.ChequeFlag__c == 'Yes') {
				currOpp.ChequeFlag__c = 'No';
			}
		}
		if (productBasket.Payment_Terms__c == '45' || productBasket.Payment_Terms__c == '60') {
			currOpp.Payment_Terms__c = productBasket.Payment_Terms__c;
		} else {
			currOpp.Payment_Terms__c = '';
		}
		currOpp.TRC_Value__c = productBasket.TRC_TRG_Value__c;
		currOpp.TRG_Value__c = productBasket.TRC_TRG_Value__c;
		currOpp.Product_Code__c = productCode;
        
		update currOpp;
	}
}