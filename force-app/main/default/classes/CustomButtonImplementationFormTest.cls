@IsTest
public class CustomButtonImplementationFormTest  {
	@IsTest
	public static void testPerformActionNoPc() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account testAcc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', testAcc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', testOpp);

		CustomButtonImplementationForm controller = new CustomButtonImplementationForm();
		
		Test.startTest();
		controller.performAction(basket.Id);
		Test.stopTest();

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testPerformActionWithPc() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account testAcc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', testAcc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', testOpp);
		cscfga__Product_Definition__c testPd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Flex');
		cscfga__Product_Configuration__c testPc = CS_TestDataFactory.generateProductConfiguration(false, 'test PC', basket);
		testPc.cscfga__Product_Definition__c = testPd.Id;
		testPc.cscfga__Product_Basket__c = basket.Id;
		testPc.cscfga__Configuration_Status__c = 'Valid';
		INSERT testPc;

		CustomButtonImplementationForm controller = new CustomButtonImplementationForm();
		
		Test.startTest();
		controller.performAction(basket.Id, testPc.Id);
		Test.stopTest();

		notriggers.Flag__c = false;
		DELETE notriggers;
	}
}