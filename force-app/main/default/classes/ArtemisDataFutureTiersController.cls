public class ArtemisDataFutureTiersController {

    private static final Integer MAX_ROWS = 10;
    public String startingTier;
    public String tierIncrements;
    public String startingTierPrice;
    public String tierIncrementsPrice;
    public List<ArtemisDataFutureTier> artemisDataFutureTiers {get; set;}
    
    public ArtemisDataFutureTiersController () {
        startingTier = Apexpages.currentpage().getparameters().get('startingTierValue');
        tierIncrements = Apexpages.currentpage().getparameters().get('tierIncrements');
        startingTierPrice = Apexpages.currentpage().getparameters().get('startingTierCharge');
        tierIncrementsPrice = Apexpages.currentpage().getparameters().get('tierIncrementsPrice');
        
        artemisDataFutureTiers = new List<ArtemisDataFutureTier>();
        //List<String> startingTiers = String.isNotEmpty(startingTier) && startingTier.contains(' ') ? startingTier.split(' ') : new List<String>();
        //Integer startingTierValue = !startingTiers.isEmpty() ? Integer.valueOf(startingTiers.get(0)) : 0;
        //List<String> tierIncrementsList = String.isNotEmpty(tierIncrements) && tierIncrements.contains(' ') ? tierIncrements.split(' ') : new List<String>();
        //Integer tierIncrementValue = String.isNotEmpty(tierIncrements) ? Integer.valueOf(tierIncrementsList.get(0)) : 0;
        Decimal startingTierValue = String.isNotEmpty(startingTier) ? Decimal.valueOf(startingTier) : 0.00;
        Decimal tierIncrementValue = String.isNotEmpty(tierIncrements) ? Decimal.valueOf(tierIncrements) : 0.00;
        Decimal startingTierCharge = String.isNotEmpty(startingTierPrice) ? Decimal.valueOf(startingTierPrice) : 0.00;
        Decimal tierIncrementsCharge = String.isNotEmpty(tierIncrementsPrice) ? Decimal.valueOf(tierIncrementsPrice) : 0.00;
        if (startingTierValue != 0 && tierIncrementValue != 0 && startingTierCharge != 0.00 && tierIncrementsCharge != 0.00) {
            Decimal dataTier = startingTierValue;
            Decimal charge = startingTierCharge;
            Decimal previousDataTierCharge = 0.00;
            Integer priceIncrease;
            for (Integer i = 0; i < MAX_ROWS; i++) {
                Decimal pricePerGB = (charge / dataTier).setScale(2);
                if (i > 0) {
                    priceIncrease = Integer.valueOf((((charge - previousDataTierCharge) / previousDataTierCharge) * 100).round());
                }
                if (String.valueOf(dataTier).contains('.') && Integer.valueOf(String.valueOf(dataTier).subStringAfter('.')) == 0) {
                    if (Math.mod(Long.valueOf(String.valueOf(dataTier)), Long.valueOf(String.valueOf(10))) == 0 ) 
                        dataTier = dataTier.intValue();
                    else
                        dataTier = dataTier.stripTrailingZeros();
                } else {
                    dataTier = dataTier.stripTrailingZeros();
                }
                artemisDataFutureTiers.add(new ArtemisDataFutureTier(String.valueOf(dataTier) + ' GB', charge.setScale(2), 'Up to ' + dataTier + ' GB', priceIncrease, pricePerGB));
                previousDataTierCharge = charge;
                dataTier += tierIncrementValue;
                charge += tierIncrementsCharge;
            }
        }
    }
    
    public class ArtemisDataFutureTier {
        
        public String dataTier {get; set;}
        public Decimal charge {get; set;}
        public String allowance {get; set;}
        public Integer priceIncrease {get; set;}
        public Decimal pricePerGB {get; set;}
        
        public ArtemisDataFutureTier(String dataTier, Decimal charge, String allowance, Integer priceIncrease, Decimal pricePerGB) {
            this.dataTier = dataTier;
            this.charge = charge;
            this.allowance = allowance;
            this.priceIncrease = priceIncrease;
            this.pricePerGB = pricePerGB;
        }
    }
}