global class CRFDeleteController{

private CRF__c crf;

public CRFDeleteController(ApexPages.StandardController stdController) {
        this.crf = (CRF__c)stdController.getRecord();
        }

        
WebService static void DeleteCRF(string crfId){
    string msg;
    List<CRF__c> crf = [Select Id,Name from CRF__c where Id=:crfId];
    if(crf.size()>0){
    delete crf[0];  
    //this.step1();
    //msg = 'yes';
    //PageReference PageRef = new Page.CRFTabPage;
   // return Page.CRFTabPage;
    //PageRef.setRedirect(true);
     
    }}
    public PageReference step1(){
    PageReference PageRef = new PageReference('CRFTabPage');
    PageRef.setRedirect(true);  
    return PageRef;  }



}