public with sharing class CRsSearchController {
 
  // the soql without the order and limit
  private String soql {get;set;}
  private String soqldetail {get;set;}
  // the collection of contacts to display
  public List<Change_Request__c> contacts {get;set;}
  public List<Change_Request__c> CRdetail {get;set;}
  public List<Note> CRnotes {get;set;}
 
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'Status__c'; } return sortField;  }
    set;
  }
  
  // CR details to show
  public String cr{
    get  { if (cr== null) {cr= ''; } return cr;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
  public String debugSoql2 {
    get { return soqldetail + ' WHERE Name = \'' + cr + '\''; }
    set;
  }  
 
  // init the controller and display some sample data when the page loads
  public CRsSearchController () {
    soql = 'SELECT name, zSiteEmail__c, Salesforce_Org__c, Status__c FROM Change_Request__c WHERE Salesforce_Org__c != \'B&PS\' AND Type__c = \'Change Request\'';
    runQuery();
  }
 
  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
 
  // runs the actual query
  public void runQuery() {
 
    try {
      contacts = Database.query(soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20');
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
    }
  }

  // runs the detailed query
  public void runDetail() {
  soqldetail = 'Select zSiteEmail__c, zMonthOpened__c, zMonthClosed__c, Type__c, Test_CAT__c, Test_CAT_Comments__c, Targeted_Phase__c, SystemModstamp, Status__c, Stake_Holder__c, Sign_Off__c, Secondary_Object__c, Salesforce_Org__c, STORM_Title__c, STORM_Story__c, SF_Reference__c, Requires_New_Profile__c, Required_By__c, Requested_Priority__c, Related_To__c, Related_Bridge_Case__c, RecordTypeId, Reason__c, Ready_For_Sign_Off__c, Priority__c, Primary_Object__c, OwnerId, New_Profile_Signed_Off__c, Name, MOSCOW__c, Lead_Designer__c, LastModifiedDate, LastModifiedById, LastActivityDate, Kick_Off_Meeting__c, Kick_Off_Meeting_Comments__c, IsDeleted, Id, Feedback__c, Feedback_Title__c, Estimated_Delivery_Date__c, Effort__c, Dev__c, Dev_Comments__c, Design__c, Design_Comments__c, Description__c, Deployment_Date__c, Deployment_Bridge_Case__c, Deployer_Comments__c, Deploy__c, Deploy_Comments__c, Creator_Channel__c, Created_Count__c, CreatedDate, CreatedById, Code_Coverage__c, Closed_Date__c, Closed_Count__c, Channel_s__c, Channel__c, Category__c, Business_Benefit_Value__c, Budget__c, Assigned_To__c, Assigned_To_ID__c From Change_Request__c';   
    try {
      CRdetail = Database.query(soqldetail + ' WHERE Name = \'' + cr + '\'');
      CRnotes = Database.query('SELECT ID, Title, Body, CreatedByID, CreatedBy.Name, CreatedDate, IsPrivate FROM Note WHERE ParentID = \'' + CRDetail[0].ID + '\' ORDER BY CreatedDate DESC');
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Ooops!'));
    }
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String name = Apexpages.currentPage().getParameters().get('name');
    String Status = Apexpages.currentPage().getParameters().get('Status');
    String Salesforce_Org = Apexpages.currentPage().getParameters().get('Salesforce_Org');

    soql = 'SELECT name, zSiteEmail__c, Salesforce_Org__c, Status__c FROM Change_Request__c WHERE Salesforce_Org__c != \'B&PS\' AND Type__c = \'Change Request\'';
    if (!name.equals(''))
      soql += ' and name LIKE \''+String.escapeSingleQuotes(name)+'%\'';
    if (!Status.equals(''))
      soql += ' and Status__c LIKE \''+String.escapeSingleQuotes(Status)+'%\'';
    if (!Salesforce_Org.equals(''))
      soql += ' and Salesforce_Org__c LIKE \''+String.escapeSingleQuotes(Salesforce_Org)+'%\'';         
    // run the query again
    runQuery();
 
    return null;
  }
  // use apex describe to build the picklist values
  public List<String> Salesforce_Org{
    get {
      if (Salesforce_Org == null) {
 
        Salesforce_Org = new List<String>();
        Schema.DescribeFieldResult field = Change_Request__c.Salesforce_Org__c.getDescribe();
 
        for (Schema.PicklistEntry f : field.getPicklistValues())
          if (f.getLabel() != 'BT Business'){
                  Salesforce_Org.add(f.getLabel());
             }
      }
      return Salesforce_Org ;          
    }
    set;
  } 
  public List<String> Status{
    get {
      if (Status == null) {
 
        Status = new List<String>();
        Schema.DescribeFieldResult field = Change_Request__c.Status__c.getDescribe();
 
        for (Schema.PicklistEntry f : field.getPicklistValues())
          Status.add(f.getLabel());
 
      }
      return Status;          
    }
    set;
  }   
}