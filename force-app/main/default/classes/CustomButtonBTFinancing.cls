global with sharing class CustomButtonBTFinancing extends csbb.CustomButtonExt {
  public String performAction (String basketId) {
      String sfdcURL = Label.CS_DiscountRedirect_Org_Url;
        System.debug('sfdcURL====>' +sfdcURL); 
        String newUrl = sfdcURL+'/apex/CloudVoiceItemList?basketId='+basketId;
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
  }
}