public with sharing class IssueVote {

private Issue__c thisIssue = null;
	
	public IssueVote(ApexPages.StandardController controller) {
		System.debug('Initialise IssueVote...');
		thisIssue = (Issue__c)controller.getRecord();
	}
	
	public pageReference doIssueVote() {
		pageReference issuePage;
		String pageURL = '/' + thisIssue.Id;
		
		issuePage = new PageReference(pageUrl);
     	issuePage.setRedirect(true);
		
	    Issue_Vote__c thisIV = new Issue_Vote__c(Issue__c = thisIssue.Id);
	    
    	try{
    	    insert thisIV;
    	}
    	catch(Exception e){
    	   // if we cannot delete the record the owner is editing the record
    	}
	
		
		return issuePage;
	}
	
}