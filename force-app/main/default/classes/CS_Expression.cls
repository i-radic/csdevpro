public class CS_Expression
{
    public final static List<Set<String>> OPERATORS = new List<Set<String>>
        { new Set<String>{'+', '-'}
        , new Set<String>{'*', '/'} };

    public interface Eval
    {
        Object eval(SObject environment);
        Set<String> fields();
    }

    public class Add implements Eval
    {
        Eval left;
        Eval right;

        public Add(Eval left, Eval right)
        {
            this.left = left;
            this.right = right;
        }

        public Object eval(SObject environment)
        {
            Decimal leftValue = CS_Util.decimalValue(left.eval(environment));
            Decimal rightValue = CS_Util.decimalValue(right.eval(environment));

            return leftValue + rightValue;
        }

        public Set<String> fields()
        {
            Set<String> returnValue = new Set<String>();
            returnValue.addAll(left.fields());
            returnValue.addAll(right.fields());
            return returnValue;
        }
    }

    public class Sub implements Eval
    {
        Eval left;
        Eval right;

        public Sub(Eval left, Eval right)
        {
            this.left = left;
            this.right = right;
        }

        public Object eval(SObject environment)
        {
            Decimal leftValue = CS_Util.decimalValue(left.eval(environment));
            Decimal rightValue = CS_Util.decimalValue(right.eval(environment));

            return leftValue - rightValue;
        }

        public Set<String> fields()
        {
            Set<String> returnValue = new Set<String>();
            returnValue.addAll(left.fields());
            returnValue.addAll(right.fields());
            return returnValue;
        }
    }

    public class Mul implements Eval
    {
        Eval left;
        Eval right;

        public Mul(Eval left, Eval right)
        {
            this.left = left;
            this.right = right;
        }

        public Object eval(SObject environment)
        {
            Decimal leftValue = CS_Util.decimalValue(left.eval(environment));
            Decimal rightValue = CS_Util.decimalValue(right.eval(environment));

            return leftValue * rightValue;
        }

        public Set<String> fields()
        {
            Set<String> returnValue = new Set<String>();
            returnValue.addAll(left.fields());
            returnValue.addAll(right.fields());
            return returnValue;
        }
    }

    public class Div implements Eval
    {
        Eval left;
        Eval right;

        public Div(Eval left, Eval right)
        {
            this.left = left;
            this.right = right;
        }

        public Object eval(SObject environment)
        {
            Decimal leftValue = CS_Util.decimalValue(left.eval(environment));
            Decimal rightValue = CS_Util.decimalValue(right.eval(environment));
            Decimal returnValue = null;

            if (rightValue != 0.0)
            {
                returnValue = leftValue / rightValue;
            }

            return returnValue;
        }

        public Set<String> fields()
        {
            Set<String> returnValue = new Set<String>();
            returnValue.addAll(left.fields());
            returnValue.addAll(right.fields());
            return returnValue;
        }
    }

    public class Field implements Eval
    {
        String fieldName;

        public Field(String fieldName)
        {
            this.fieldName = fieldName.trim().toLowerCase();
        }

        public Object eval(SObject environment)
        {
            return environment.get(fieldName);
        }

        public Set<String> fields()
        {
            return new Set<String>{ fieldName };
        }
    }

    public class Constant implements Eval
    {
        Decimal value;

        public Constant(String text)
        {
            value = Decimal.valueOf(text);
        }

        public Object eval(SObject environment)
        {
            return value;
        }

        public Set<String> fields()
        {
            return new Set<String>();
        }
    }

    public static Eval parseExpression(String expression)
    {
        String simplified = simplifyExpression(expression);
        SplitResult splitResult = splitExpression(simplified);
        if (splitResult instanceOf SplitNone)
        {
            if (isNumber(simplified))
            {
                return new Constant(simplified);
            }
            else
            {
                return new Field(simplified);
            }
        }
        else if (splitResult instanceOf SplitSome)
        {
            SplitSome splitSome = (SplitSome)splitResult;
            if (splitSome.operator == '+')
            {
                return new Add
                    ( parseExpression(splitSome.left)
                    , parseExpression(splitSome.right));
            }
            else if (splitSome.operator == '-')
            {
                return new Sub
                    ( parseExpression(splitSome.left)
                    , parseExpression(splitSome.right));
            }
            else if (splitSome.operator == '*')
            {
                return new Mul
                    ( parseExpression(splitSome.left)
                    , parseExpression(splitSome.right));
            }
            else if (splitSome.operator == '/')
            {
                return new Div
                    ( parseExpression(splitSome.left)
                    , parseExpression(splitSome.right));
            }
        }
        return new Field(expression);
    }

    public static SplitResult splitExpression(String expression)
    {
        for (Set<String> ops: OPERATORS)
        {
            //
            // One level
            //
            List<Split> splits = new List<Split>();
            for (String op : ops)
            {
                Integer index = expression.length();
                while (index > 0)
                {
                    index = expression.lastIndexOf(op, index - 1);
                    if (index >= 0)
                    {
                        splits.add(new Split(op, index));
                    }
                }
            }
            splits.sort();
            for (Split split : splits)
            {
                //
                // Check split
                //
                String left = expression.substring(0, split.index);
                if (!isInBracket(left))
                {
                    //
                    // We found operator that is not in bracket
                    //
                    String right =
                        expression.substring(split.index + split.operator.length());
                    return new SplitSome(split.operator, left, right);
                }
            }
        }
        return new SplitNone();
    }

    public static Boolean isNumber(String expression)
    {
        return expression.replaceAll('\\.', '').isNumeric();
    }

    public static Boolean isInBracket(String expression)
    {
        return expression.countMatches('(') > expression.countMatches(')');
    }

    public static String simplifyExpression(String expression)
    {
        String simplified = expression.trim();
        if (simplified.startsWith('(') && simplified.endsWith(')'))
        {
            return simplifyExpression
                ( simplified.substring
                    ( 1
                    , simplified.length() - 1 )).trim();
        }
        return simplified;
    }

    public interface SplitResult
    {
    }

    public class SplitNone implements SplitResult
    {
    }

    public class SplitSome implements SplitResult
    {
        String operator;
        String left;
        String right;

        public SplitSome(String operator, String left, String right)
        {
            this.operator = operator;
            this.left = left;
            this.right = right;
        }
    }


    public class Split implements Comparable
    {
        String operator;
        Integer index;

        public Split
            ( String operator
            , Integer index )
        {
            this.operator = operator;
            this.index = index;
        }

        public Integer compareTo(Object split)
        {
            //
            // It is sorted in the reverse order
            //
            Split other = (Split)split;
            return other.index - this.index;
        }
    }
}