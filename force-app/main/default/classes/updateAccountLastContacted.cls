public with sharing class updateAccountLastContacted {
    public list<account> updateAccount(set<id> accountIDs){
        //Take a list of a account id's and return a list of accounts
        List<account> accountList = new List<account>();
        accountlist = [select last_account_call_date__c, Last_Update_to_Last_Account_Call_Date__c from account where id in :accountids];
        
        for (account thisaccount:accountlist){
            thisaccount.Last_Account_Call_Date__c = date.today();
        }
        Return accountlist;
    }
    
    //start CR2587 Changes
    public list<account> updateAccount(set<id> accountIDs, String objName){
        //Take a list of a account id's and return a list of accounts
        List<account> accountList = new List<account>();
        accountlist = [select last_account_call_date__c, Last_Update_to_Last_Account_Call_Date__c from account where id in :accountids];
        
        for (account thisaccount:accountlist){
            thisaccount.Last_Account_Call_Date__c = date.today();
            thisaccount.Last_Update_to_Last_Account_Call_Date__c = objName+' - '+ UserInfo.getName() + ' - ' + Datetime.now().format('dd/MM/yyyy HH:mm:ss');
        }
        Return accountlist;
    }
    //end CR2587 Changes
}