/*######################################################################################
PURPOSE
This is not currently in use. Proposed for customer interaction.

HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
public with sharing class CloudVoiceConfigCTRL {

public CloudVoiceConfigCTRL(ApexPages.StandardController controller) { }

private blob fileBody;
private integer filesize;
public String zUser{get; set;}
public String zNumber{get; set;}
public String zPhone{get; set;}

public void setFileBody(Blob fileBody)  {
            this.fileBody = fileBody;       
            setFileSize(this.fileBody.size());  
}   
public Blob getFileBody()   {
            return this.fileBody;   
}
public Integer getFileSize()    {       
    return this.fileSize;   }   
    
public void setFileSize(Integer fileSize)   {
    this.fileSize = fileSize;   
}   

public string CV= System.currentPageReference().getParameters().get('id');

    public List<String> getconfigUsers() {
        Cloud_Voice_Site__c ctUser = [Select Id, Total_Site_Licenses__c, zUserString__c FROM Cloud_Voice_Site__c WHERE Id = :CV] ;
        List<String> lUser= new List<String>();
        if(ctUser.zUserString__c ==Null){
            for(Integer a = 1; a < ctUser.Total_Site_Licenses__c+1; a++){
                lUser.add('Unspecified User '+ a);
            }
        }else{    
            for(String s: ctUser.zUserString__c.split(',')){
                lUser.add(s);
            }
        } 
        return lUser;
    } 
    public List<String> getconfigPhones() {
        Cloud_Voice_Site__c ctUser = [Select Id, Total_Site_Licenses__c, zPhoneString__c FROM Cloud_Voice_Site__c WHERE Id = :CV] ;
        List<String> lCPE = new List<String>();
        List<Cloud_Voice_Site_Product__c> sumCPE = [Select Id, Name, Product_Name__c, Quantity__c, Image__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :CV AND Type__c = 'CPE' AND Grouping__c = '' ORDER BY Sort_Order__c ASC] ;
            if(ctUser.zPhoneString__c ==Null){
                for(Cloud_Voice_Site_Product__c c:sumCPE){
                    for(Integer a = 0; a < c.Quantity__c; a++){
                        String cpeStr = c.Image__c +'~'+ c.Product_Name__c;
                        lCPE.add(cpeStr);
                    }
                }
            }else{
                for(String s: ctUser.zPhoneString__c.split(',')){
                    lCPE.add(s);
                }
            }  
        return lCPE;
    }
    public List<String> getconfigNumbers() {
        Cloud_Voice_Site__c ctUser = [Select Id, Total_Site_Licenses__c, zNumberString__c FROM Cloud_Voice_Site__c WHERE Id = :CV] ;
        List<String> lNum= new List<String>();
        if(ctUser.zNumberString__c ==Null){
            for(Integer a = 0; a < ctUser.Total_Site_Licenses__c; a++){
                lNum.add('0141 950 000'+ a);
            }
        }else{    
            for(String s: ctUser.zNumberString__c.split(',')){
                lNum.add(s);
            }
        } 
        return lNum;
    }

    public pagereference uploadUsers(){    
        Cloud_Voice_Site__c ctUser = [Select Id, Total_Site_Licenses__c, zUserString__c FROM Cloud_Voice_Site__c WHERE Id = :CV] ;

        if(this.fileSize>100000){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The file you have selected should be < 100kb.');
            ApexPages.addMessage(myMsg);
            return null;
        }

        if(this.filebody !=null){
             //Get the contents of the import file
            String all = filebody.tostring();
            String res = Null;
            system.debug('jmm all'+all);
            //Break it into separate lines
            String [] allrows = all.split('\r\n');

            //Only allow rows to match licenses
            if(allrows.size() != ctUser.Total_Site_Licenses__c){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You must have exactly '+ctUser.Total_Site_Licenses__c+' rows in the file. This file has ' + allrows.size());
                ApexPages.addMessage(myMsg);
                
                return null;
            }
            if(allrows.size() > 0){        
                Integer nCount = 0;
                for(String thisLine:allrows){
                    nCount++;
                    if(res == Null){
                        res = thisLine;
                    }else{
                        res = res + ',' + thisLine;
                    }    
               }  
               //update user lists
               ctUser.zUserString__c = res;
               //reset number/phone mappings
               ctUser.zNumberString__c = Null;
               ctUser.zPhoneString__c = Null;
               update ctUser; 
                
            }else{
                //Nothing in the file, return
                return null;
            } 
        }  
        return null;
    }

    public pageReference saveConfig(){
        Cloud_Voice_Site__c ctUser = [Select Id, zUserString__c, zNumberString__c, zPhoneString__c FROM Cloud_Voice_Site__c WHERE Id = :CV] ;
        ctUser.zUserString__c =  zUser;
        ctUser.zNumberString__c =  zNumber;
        ctUser.zPhoneString__c =  zPhone;
        update ctUser;
        return null;

    }        
}