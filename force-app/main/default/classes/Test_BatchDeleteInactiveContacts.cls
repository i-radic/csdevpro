@isTest
public class Test_BatchDeleteInactiveContacts {
  public static testMethod void TestDeleteBatch(){
      
      Database.QueryLocator QL;
      Database.BatchableContext BC;
      User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
      List<Contact> conlist = new List<Contact>();
      Contact c = new Contact();
      c.Salutation='Mr';
      c.FirstName='Test';
      c.LastName='Test';
      c.Job_Function__c='admin';
      c.Status__c='InActive';
      insert c;      
      conlist.add(c);      
      Batch_DeleteInactiveContacts condetail = new Batch_DeleteInactiveContacts();
      QL = condetail.start(BC);
      condetail.execute(BC, conlist);
      condetail.finish(BC);
   
  }
}