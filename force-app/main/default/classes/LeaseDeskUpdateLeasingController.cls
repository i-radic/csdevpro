public with sharing class LeaseDeskUpdateLeasingController {

    public Account account {get;set;}
    private String tempRAG {get;set;}
    public List<Leasing_History__c> salesForceLeasingHistory {get;set;}
    public List<LeaseDeskAPI.LeasingDetails> leaseDeskLeasingHistory {get;set;}
    
    public LeaseDeskUpdateLeasingController(ApexPages.StandardController controller) {

    }
    
    public PageReference OnLoad()
    {   
        String accountId = ApexPages.currentPage().getParameters().get('id');
        account = LeaseDeskUtil.getAccountDetails(accountId);
        User user = LeaseDeskUtil.getUserDetails();
        List<Leasing_History__c> leasingHistoryProposalsToUpsert = new List<Leasing_History__c>();
        List<Leasing_History__c> leasingHistoryAgreementsToUpsert = new List<Leasing_History__c>();
        Set<string> leasingHistoryProposalsToDelete = new Set<string>();
        List<Account> accountRAGStatusList = new List<Account>();
        Boolean recordMatch = false;
        Date ragDate = Date.today().addYears(-10);
            
        if (account.LeaseDeskID__c != null){
            LeaseDeskAPI leaseDeskAPI = new LeaseDeskAPI();
            salesForceLeasingHistory = LeaseDeskUtil.getLeasingHistoryList(account.Id);
            if(!system.Test.isRunningTest()){
                leaseDeskLeasingHistory = leaseDeskAPI.GetLeasingHistory(user.EIN__c, user.OUC__c, account.LeaseDeskID__c);
            }
            system.debug('#######################' + leaseDeskLeasingHistory);
            Set<string> leasingOpptyIDSet = new Set<string>();
            for (LeaseDeskAPI.LeasingDetails ld : leaseDeskLeasingHistory) {
                leasingOpptyIDSet.add (ld.salesforceOpptyID);
            }
            List<Opportunity> opptyList = [Select Id , Opportunity_Id__c from Opportunity where Opportunity_Id__c in :leasingOpptyIDSet];
            for(leaseDeskAPI.LeasingDetails ldHistory : leaseDeskLeasingHistory){                                                                 
                if(!system.Test.isRunningTest()){
                    if(ldHistory.salesforceOpptyID.startsWith('006')){
                        ldHistory.salesforceOID = ldHistory.salesforceOpptyID;
                    }
                    for(Opportunity oppty : opptyList){
                        if(ldHistory.salesforceOpptyID == oppty.Opportunity_Id__c){
                            ldHistory.salesforceOID = oppty.Id;
                        }
                    }
                }
            }   
            for(leaseDeskAPI.LeasingDetails ldHistory : leaseDeskLeasingHistory){               
                if (ldHistory.Error != null){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ldHistory.Error));
                    return null;
                }                   
                else{           
                    try{
                        Opportunity oppty = [SELECT Id FROM Opportunity WHERE Id =: ldHistory.salesforceOID];                                       
                        Leasing_History__c sfHistory = new Leasing_History__c();
                        sfHistory.Opportunity__c = ldHistory.salesforceOID;   
                        sfHistory.Account__c = ldHistory.salesforceAccountID;
                        sfHistory.AgreementID__c = ldHistory.leaseDeskProposalID;
                        sfHistory.LeaseDeskAgreementId__c = ldHistory.leaseDeskAgreementID;
                        if(ldHistory.status != null){
                            //------------To delete: funder status equals 'NotInUse' 
                            if(ldHistory.status == 'NotInUse'){ 
                                leasingHistoryProposalsToDelete.Add(ldHistory.leaseDeskProposalID);    
                            }
                            sfHistory.Status__c = ldHistory.status.replace('.', ': ');
                            sfHistory.clearance__c = StatusToRAGMapping(ldHistory.status);
                            if(IsLiveDeal(ldHistory.status)){
                                sfHistory.End_Date__c = null;
                            }
                            else{
                                sfHistory.End_Date__c = ldHistory.endDate;
                            }  
                        }        
                        if(ldHistory.settlement != null){
                            sfHistory.Settlement__c = ldHistory.settlement;
                        }        
                        if(ldHistory.periodsRemaining != null){   
                            sfHistory.Periods_Remaining__c = ldHistory.periodsRemaining;  
                        }    
                        if(ldHistory.product != null){    
                            sfHistory.Product__c = ldHistory.product;
                        }
                        if(ldHistory.leaseDeskCompanyName != null){   
                            sfHistory.Lease_Desk_Company_Name__c = ldHistory.leaseDeskCompanyName;
                        }
                        if(ldHistory.cugID != null){
                            sfHistory.CUG_Id__c = ldHistory.cugID;
                        }
                        if(ldHistory.funderCreditLimit != null){
                            sfHistory.Credit_limit__c = ldHistory.funderCreditLimit;
                        }
                        if(ldHistory.poValue != null){
                            sfHistory.PO_Value__c = ldHistory.poValue;
                        } 
                        if(ldHistory.funderURL != null){
                            sfHistory.Funder_URL__c = ldHistory.funderURL; 
                        }
                        if(ldHistory.leaseDeskURL != null){
                            sfHistory.Lease_Desk_Url__c = ldHistory.leaseDeskURL;
                        }
                        sfHistory.Funder__c = '-';
                        if(ldHistory.funder != null){
                            sfHistory.Funder__c = ldHistory.funder;
                        }
                        if(ldHistory.term != null){
                            sfHistory.Original_Term__c = String.valueOf(ldHistory.term) + ' Months';
                        }
                        if(ldHistory.rbo != null){
                            sfHistory.RBO__c = ldHistory.rbo; 
                        }
                        if(ldHistory.regularPayment != null){
                            sfHistory.Quarterly_Payment__c = QuarterlyPayment(ldHistory.frequency, ldHistory.regularPayment);
                        }
                        if(ldHistory.status == 'NotInUse' || ldHistory.status == 'History.Credit' || ldHistory.salesforceOID == null || ldHistory.salesforceOID == ''){
                            //do not add to insert list
                        }
                        else{
                            if(ldHistory.leaseDeskProposalID != null){
                                leasingHistoryProposalsToUpsert.Add(sfHistory);
                            }
                            else if(ldHistory.leaseDeskAgreementID != null){
                                leasingHistoryAgreementsToUpsert.Add(sfHistory);
                            }
                        }   
                        
                        //------------Update RAG status field on oppty detail page                                    
                     /**   if(StatusToRAGMapping(ldHistory.status) == 'Green'){
                            oppty.Finance_Available__c = StatusToRAGMapping(ldHistory.status);
                            tempRAG = 'Green';
                        }
                        if(StatusToRAGMapping(ldHistory.status) == 'Amber'){
                            if(tempRAG != 'Green'){
                                oppty.Finance_Available__c = StatusToRAGMapping(ldHistory.status);
                                tempRAG = 'Amber';
                            }
                        }
                        if(StatusToRAGMapping(ldHistory.status) == 'Red'){
                            if(tempRAG != 'Amber' && tempRAG != 'Green'){
                                oppty.Finance_Available__c = StatusToRAGMapping(ldHistory.status);
                                tempRAG = 'Red';
                            }
                        }   
                        update oppty;*/
                    
                        if(ldHistory.ragDate != null){
                            if(ldHistory.ragDate > ragDate && ldHistory.salesforceAccountID != null && ldHistory.ragStatus != null){
                                accountRAGStatusList.Clear();
                                Account a = new Account(Id = ldHistory.salesforceAccountID, Credit_Worthiness__c = ldHistory.ragStatus);
                                accountRAGStatusList.Add(a);
                                ragDate = ldHistory.ragDate;
                            }
                        }
                    }     
                    catch(Exception e){}    
                }
            }
            Integer upsertCount = leasingHistoryProposalsToUpsert.size() + leasingHistoryAgreementsToUpsert.size();
            //------------Update: Account RAG 
            if(accountRAGStatusList.Size() >= 1){
                update accountRAGStatusList;
            }
            //------------Upsert: Leasing Proposal Id records 
            if(leasingHistoryProposalsToUpsert.size() >= 1){
                upsert leasingHistoryProposalsToUpsert AgreementID__c;
            }   
            //------------Upsert: Leasing Agreement Id records 
            if(leasingHistoryAgreementsToUpsert.size() >= 1){
                upsert leasingHistoryAgreementsToUpsert LeaseDeskAgreementId__c;
            }      
            //------------Delete: Leasing status equals 'NotInUse', this prevents duplicate funders records
            if(leasingHistoryProposalsToDelete.size() >= 1){
                List<Leasing_History__c> lhProposals = [Select Id, AgreementID__c from Leasing_History__c where AgreementID__c in :leasingHistoryProposalsToDelete];
                Database.delete(lhProposals,false);
            }
            if(upsertCount >= 1){   
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, upsertCount + ' Leasing History record(s) successfully updated.'));             
                return null;
            }   
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Unable to update Leasing History, there are no records in the feed.'));
        return null;
    }
    
    public PageReference back()    
    {           
        return new ApexPages.StandardController(account).view();
    }
    
    public String StatusToRAGMapping(String status){
        if(status == 'Prospect.Preclearance' || status == 'Prospect.Proposed' || status == 'Prospect.Referral' || status == 'Prospect.MoreInformationRequired'){
            return 'Amber';
        }
        else if(status == 'Prospect.Acceptance' || status == 'Prospect.PurchaseOrder' || status == 'Prospect.Activation' || status == 'Prospect.DocsOut'){
            return 'Green';
        }
        else if(status == 'Prospect.Rejection' || status == 'Prospect.Dead'){
            return 'Red';
        }
        return null;
    }
    
    private Boolean IsLiveDeal(String status){
        if(status == 'Live' || status == 'Upgraded' || status == 'Settled'|| status == 'Arrears'|| status == 'EndOfTerm'|| status == 'SecondaryPeriod'|| status == 'Dead'){
            return true;
        }
        return false;
    }
    
    public Double QuarterlyPayment(String frequency, Double regularPayment){
        if(frequency != null && regularPayment != null){
            if(frequency == 'Monthly'){
                return regularPayment * 3;
            }
            if(frequency == 'Quarterly'){
                return regularPayment;
            }
            if(frequency == 'Annually'){
                return regularPayment / 4;
            }
            if(frequency == 'Two Monthly'){
                return (regularPayment * 6) / 4;
            }
            if(frequency == 'Four Monthly'){
                return (regularPayment * 3 ) / 4;
            }
            if(frequency == 'Six Monthly'){
                return (regularPayment * 2) / 4;
            }   
        }   
        return null;                
    }
}