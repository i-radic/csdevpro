public class batchJobs{
   
    public List<BatchJob> batchJobs;
    public Integer numberOfJobs {get; set;}

    public List<BatchJob> getBatchJobs(){
        //Create new list of BatchJobs, a wrapper class that includes the job and percent complete.
        batchJobs = new List<BatchJob>();
        Set<id> asynchset = new set<id>();
        
        //If number of jobs was not defined, default to 20
        if(numberOfJobs== null || numberofJobs <= 0){
            numberofJobs = 20;
        }
        String theId = ApexPages.currentPage().getParameters().get('id'); 
        system.debug('xxxxx' + theid);
    
        //Added by GS
        //Get a list of all of the asynch jobs
        // jb test List<AsyncApexJob> asynch = [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob order by CreatedDate desc limit :numberOfJobs];
        //List<AsyncApexJob> asynch = [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, CreatedDate, CreatedById, CompletedDate, ApexClassId, ApexClass.Name From AsyncApexJob order by CreatedDate desc limit :numberOfJobs];
        //List<AsyncApexJob> asynch = [select TotalJobItems, Status, NumberOfErrors, MethodName, JobType, JobItemsProcessed, Id, CreatedDate, CreatedById, CompletedDate From AsyncApexJob order by CreatedDate desc limit :numberOfJobs];
       
       	List<AsyncApexJob> asynch = [select TotalJobItems, JobItemsProcessed From AsyncApexJob order by CreatedDate desc limit :numberOfJobs];
         
        //List<AsyncApexJob> asynch = new List<AsyncApexJob>{};
        
        for (asyncApexJob thisasynch:asynch){
            asynchset.add(thisasynch.id);
        }
        
        //Get all of the batch jobs for the asynch jobs.
        List<bulk_campaign_run__c> bulkrunlist = new  List<bulk_campaign_run__c>([select id,Name,Dummy_Contacts_Created__c,ChildCampaignsCreated__c,Campaign_Members_Created__c,Bulk_Campaign_Batch__c,batchapexid__c,Campaign__c,Tasks_Created__c,Total_Errors_In_Batch__c,Total_Records_in_Batch__c,view_error_details__c From Bulk_Campaign_Run__c Where batchapexid__c in :asynchset]);
        Map<id,bulk_campaign_run__c> braynchmap = new Map<id,bulk_campaign_run__c>();
        
        //Create a map of batchapex ID's and campaign run records
        
        for(bulk_campaign_run__c bcr : bulkrunlist){
            braynchmap.put(bcr.batchapexid__c, bcr);
        }
        
         
        //END
        
        //Query the Batch apex jobs
        for(AsyncApexJob a : asynch){
            Double itemsProcessed = a.JobItemsProcessed;
            Double totalItems = a.TotalJobItems;
            bulk_campaign_run__c bulkrunrecord = new bulk_campaign_run__c();
            BatchJob j = new BatchJob();
            j.job = a;
            
            //Determine the pecent complete based on the number of batches complete
            if(totalItems == 0){
                //A little check here as we don't want to divide by 0.
                j.percentComplete = 0;
            }else{
                j.percentComplete = ((itemsProcessed  / totalItems) * 100.0).intValue();
            }
            //Added by GS
            //Get the latest success/failures for this batch.
            //Get the bulk run record
            //populate the batch job with appropriate values
            
            bulkrunrecord = braynchmap.get(a.id);
            if(bulkrunrecord !=null){
                j.runrecord = bulkrunrecord;
                //Only show jobs for the current campaign
                if(bulkrunrecord.campaign__c == theid){
                        batchJobs.add(j);    
                }
            }
             
           // batchJobs.add(j);    
        }
        return batchJobs;
    }
  
    //This is the wrapper class the includes the job itself and a value for the percent complete    
    public Class BatchJob{
        public AsyncApexJob job {get; set;}
        public Integer percentComplete {get; set;}
        public bulk_campaign_run__c runrecord {get;set;}
    }
    
    /*--------------------TEST METHOD------------------------*/
    static testMethod void batchStatusBarTest(){
        batchJobs controller = new batchJobs();
        controller.getBatchJobs();
    
    }

    
}