/**
 *  @author: Usha Panwar
 *  @date: 2015-07-14
 *  @description: Test Class for CS_NewProductBasketController
 *
 */

@isTest
private class CS_Test_NewProductBasketController {

   // private static variables

    private static Account testBasketAcc;
    private static cscfga__Product_Basket__c testBasket;
    private static cscfga__Product_Basket__c testBasketNew = new cscfga__Product_Basket__c();
   
    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        // create account
        testBasketAcc = new Account();
        testBasketAcc.Name = 'TestBasketAccount';
        testBasketAcc.NumberOfEmployees = 1;
        insert testBasketAcc;   
        
        // create opportunity
        Opportunity testResignOpp = new Opportunity();  
        testResignOpp.Name = 'Test Resign Opp: ' + testBasketAcc.Name + ':'+datetime.now();
        testResignOpp.AccountId = testBasketAcc.Id;
        testResignOpp.CloseDate = System.today();
        testResignOpp.StageName = 'Closed Won';   
        testResignOpp.TotalOpportunityQuantity = 0;
        insert testResignOpp;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;
        
        // create product basket
        testBasket = new cscfga__Product_Basket__c();
        Datetime d = system.now();
        String strDatetime = d.format('yyyy-MM-dd HH:mm:ss');
        testBasket.Name = 'Test Resign Basket ' + strDatetime;
        testBasket.cscfga__Opportunity__c = testResignOpp.Id;
        testBasket.ReSign__c = true;
        testBasket.Public_Sector__c = true;
        testBasket.ARPM_Current__c = 100.00;
        testBasket.Total_Buyout_Cost__c = 200.10;
        insert testBasket;
        
        
    }
    
    // Test the product basket Edit scenario

    private static testMethod void testEditProductBasket() {
    
        CreateTestData();
        
        Test.StartTest();
        
        PageReference pageRef = Page.CS_NewProductBasket;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',testBasket.id);
        
        CS_NewProductBasketController ctrl = new CS_NewProductBasketController(new ApexPages.StandardController(testBasket));
      
        ctrl.save();
                   
         List<cscfga__Product_Basket__c> basketList = [select Id,Name, ReSign__c, Public_Sector__c, ARPM_Current__c,Total_Buyout_Cost__c
             from cscfga__Product_Basket__c where id = :testBasket.id];
         
          // After Save, check the values
          System.assertEquals(true, basketList[0].ReSign__c);
          System.assertEquals(true,  basketList[0].Public_Sector__c);
          System.assertEquals(100.00,  basketList[0].ARPM_Current__c);
          System.assertEquals(200.10,  basketList[0].Total_Buyout_Cost__c);
         
          Test.StopTest();
  
     }
    
    private static testMethod void test_init() {
        CreateTestData();
        
        Test.StartTest();
        
        PageReference pageRef = Page.CS_NewProductBasket;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id',testBasket.id);
        
        CS_NewProductBasketController ctrl = new CS_NewProductBasketController(new ApexPages.StandardController(testBasket));
        ctrl.init();
    }
     
     // Test the product basket New scenario
     private static testMethod void testNewProductBasket() {
     
     Test.StartTest();
     
     CS_NewProductBasketController ctrl1 = new CS_NewProductBasketController(new ApexPages.StandardController(testBasketNew));
     ctrl1.save();
     
          // After Save, check the values
          System.assertEquals(false, ctrl1.getIsPublicSector());
          System.assertEquals(false, ctrl1.getIsResign());
       
     
      Test.StopTest();
     }



}