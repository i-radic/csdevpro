/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (seealldata = true)
private class Test_DeleteOppLineItem {

    static testMethod void myUnitTest() {
       
   
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2B123Profile13167cr@testemail.com',
                           EIN__c = '012305'
                           );
        insert u1;

        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 4';
        A.sac_code__c = 'testSAC1';
        A.Sector_code__c = 'CORP1';
        A.LOB_Code__c = 'LOB1';
        A.OwnerId = u1.Id;
        A.CUG__c='CugTest1';
        Database.SaveResult accountResult = Database.insert(A);
        
        Opportunity opp = new Opportunity();
        opp.AccountId = A.id;
        opp.Name = 'tst_oppty';
        //opp.NIBR_Year_2009_10__c = 1;
        //opp.NIBR_Year_2010_11__c = 1;
        //opp.NIBR_Year_2011_12__c = 1;
        opp.StageName = 'Created';
        opp.Sales_Stage_Detail__c = 'Appointment Made';
        opp.CloseDate = system.today();
        opp.Product__c='Mobile Self Fulfil';
        
        insert opp;
        
        
        Product2 Products = new Product2 (Name = 'Mobile Self Fulfil',ProductCode='BT-001',Family = 'Data Center');
        insert Products ;                
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];
        
        Pricebook2 PriceBook2 = new Pricebook2(IsActive=FALSE,Description='Custom Price Book',Name='BT');
        Insert PriceBook2;
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = Products.Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = PriceBook2.Id, Product2Id = Products.Id, UnitPrice = 10000, IsActive = true);
        insert pbe;
        
        PricebookEntry Pbe1 = [select id,ProductCode from PricebookEntry Where Id = '01u2000000XHrLW'];
        
         OpportunityLineItem opportunityLineItema = new OpportunityLineItem() ;  
        System.debug('eeeeeeeeeeeeeeeeee' + Pbe1.ProductCode);
         opportunityLineItema.OpportunityId = opp.Id;
                    opportunityLineItema.Billing_Cycle__c = 'Outright Sale';
                   // opportunityLineItema.Contract_Term__c = opp.Contract_Term__c;
         opportunityLineItema.Category__c = 'Main';
                    opportunityLineItema.UnitPrice = 100;
                    opportunityLineItema.Sales_Type__c = 'Mobile Self Fulfil';
                    opportunityLineItema.Initial_Cost__c = 0.0;
                    opportunityLineItema.PricebookEntryId = pbe1.id;
         //opportunityLineItema.Product__c=Products.id;
        insert opportunityLineItema; 
        
        opportunityLineItem oppLineItem = [select id from opportunityLineItem where id=:opportunityLineItema.id];
        Delete oppLineItem;
        
    }
}