@isTest(SeeAllData=true)
private class BTSportSite_Test {
testmethod static void Test_FlagIsSetExist(){
        Test_Factory.setProperty('IsTest', 'yes');
RecordType rt = [select id from RecordType where SobjectType='BT_Sport_Site__c' and DeveloperName ='Season3' limit 1];
Date uDate = date.today().addDays(7);
                
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BT Sport Test';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.Product_Family__c = 'BT Sport';
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty}); 
 
    Contact c1= Test_Factory.CreateContact();
        insert c1;
    
    OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId=c1.id;
        ocr.OpportunityId=oppResult[0].id;
        ocr.IsPrimary= true;
        insert ocr;

 BT_Sport_CL__c Header1 = new BT_Sport_CL__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport_CL__c [] {Header1});        

test.startTest();

BT_Sport_Site__c Site1 = new BT_Sport_Site__c();
        Site1.BT_Sport_CL__c = Header1Result[0].id; 
    	site1.RecordTypeId=rt.Id;
        Site1.Product__c = 'BT Sport Total';
        Site1.Site_Type__c = 'BT Sport Total Pub (GB)';
        Site1.Contract_Type__c = '12 month contract';
        Site1.Band__c = 'Band A - £0 - £5,000';
        Site1.Discounts__c = 'First Month FREE';
       // Site1.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
       // Site1.Site_Display_Name__c = 'TESTER';
       // Site1.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site1Result = Database.insert(new BT_Sport_Site__c [] {Site1});

/*
BT_Sport_Site__c Site2 = new BT_Sport_Site__c();
        Site2.BT_Sport_CL__c = Header1Result[0].id; 
        Site2.Product__c = 'BT Sport 1';
        Site2.Site_Type__c = 'BT Sport 1 Hotel Rooms';
        Site2.Contract_Type__c = '12 month contract';
        Site2.Band__c = '';
        Site2.Discounts__c = 'First Month FREE';
        Site2.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site2.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site2.Site_Display_Name__c = 'TESTER';
        Site2.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site2Result = Database.insert(new BT_Sport_Site__c [] {Site2});  
    */

BT_Sport_Site__c Site3 = new BT_Sport_Site__c();
        Site3.BT_Sport_CL__c = Header1Result[0].id; 
        Site3.Product__c = 'BT Sport Total';
        Site3.Site_Type__c = 'BT Sport Total General';
        Site3.Contract_Type__c = '14 month contract';
        Site3.Band__c = '';
        Site3.Discounts__c = '';
        Site3.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site3.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site3.Site_Display_Name__c = 'TESTER';
        Site3.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site3Result = Database.insert(new BT_Sport_Site__c [] {Site3});     


BT_Sport_Site__c Site4 = new BT_Sport_Site__c();
        Site4.BT_Sport_CL__c = Header1Result[0].id; 
        Site4.Product__c = 'BT Sport Total';
        Site4.Site_Type__c = 'BT Sport Total Pub (GB)';
        Site4.Contract_Type__c = '12 month contract';
        Site4.Rateable_Value__c = 60000;
        Site4.Discounts__c = 'First Month FREE';
        Site4.Hero__c = 'Yes';
       // Site4.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
       // Site4.Site_Display_Name__c = 'TESTER';
       // Site4.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site4Result = Database.insert(new BT_Sport_Site__c [] {Site4});            

StaticVariables.setTaskIsRun(false);
}
}