global class CS_LookupQueryForSolutionConsole implements cssmgnt.RemoteActionDataProvider   {

   global Map<String, Object> getData(Map<String, Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>(); 
        String module = String.valueOf(inputMap.get('module'));
        String grouping = String.valueOf(inputMap.get('grouping'));
        String contractTerm = String.valueOf(inputMap.get('contractTerm'));
        try{
            List<cspmb__Price_Item__c> priceItemList= [select id,Name from cspmb__Price_Item__c where Module__c =:module and Grouping__c =:grouping and cspmb__Contract_Term__c = :contractTerm ];
            if(!priceItemList.isEmpty()){
                returnMap.put ('PriceItemId', priceItemList[0].id);
                returnMap.put ('PriceItemName', priceItemList[0].Name);
                returnMap.put ('DataFound', true);
            }else{
                 returnMap.put ('DataFound', false);
            }
            
        }catch(Exception e){
            
        }
        return returnMap;
    }    
    
}