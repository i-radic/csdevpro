public class Test_Factory {
    /*  
public static void SetProperty(string name, string value){
List<TestContext__c> tcs = 
[SELECT 
id,
property__c,
value__c
FROM
TestContext__c
WHERE
property__c =:name];

if (tcs.IsEmpty()){
TestContext__c newTestContext = new TestContext__c();
newTestContext.property__c = name;
newTestContext.value__c = value;
insert newTestContext;
}
else{
tcs[0].value__c = value;
update tcs;
}        
}

public static string GetProperty(string name){
List<TestContext__c> tcs = 
[SELECT 
id,
property__c,
value__c
FROM
TestContext__c
WHERE
property__c =:name];

if (tcs.IsEmpty()){
return null;
}
else{
return tcs[0].value__c;
}        
}
*/
    private static Map<string,string> staticProperties;
    
    public static void SetProperty(string name, string value){
        if (staticProperties == null){
            staticProperties = new Map<string,string>();
        }
        
        staticProperties.put(name, value);
    }
    
    public static string GetProperty(string name){
        if (staticProperties == null){
            staticProperties = new Map<string,string>();
        }
        
        return staticProperties.get(name);
    }
    
    public static User CreateUser(){
        Profile p= [select id from profile where name='Field: Standard User'];
        
        User u;
        u= new User(alias='asd',email='testing@bt.it',EIN__c='87fgter',emailencodingkey='UTF-8',lastname='Testingasd', languagelocalekey='en_US',        
                    localesidkey='en_US',isActive=True,isActive2__c='True',Department='TestSupport', profileid = p.Id, timezonesidkey='Europe/London', username='testingasd@testemail.com');
        
        return u;
    }
    
    public static Account CreateAccount(){
        return CreateAccount(null);                              
    }
    
    public static Account CreateAccount(string accountId){
        Account retval = new Account(id = accountId);
        retval.Name = 'tst_Account';
        retval.LOB_Code__c = 'tst_lob';        
        retval.SAC_Code__c = 't_sac';
        retval.LE_Code__c = 'tst_le6789';
        retval.AM_EIN__c = '802537216';
        retval.Sector_code__c = 'CORP';
        return retval;                              
    }
    
    public static Account CreateAccountForDummy(){
        Account retval = CreateAccount(null);
        retval.sac_code__c = 'Dummy999';
        retval.LE_Code__c = null;
        return retval;                              
    }
    
    public static Contact CreateContactwithAccount(String AccountId) {
        Contact retval = new Contact();     
        retval.AccountId = AccountId;
        retval.SAC_code__c = 'tst_sac';
        retval.lastname = 'tst_developer';
        retval.FirstName='jon';
        retval.Salutation = 'Mr';
        retval.MobilePhone='07755648877';       
        retval.Phone='02555818000';
        return retval;       
        
    }
    
    public static Contact CreateContact() {
        Contact retval = new Contact();        
        retval.SAC_code__c = 'tst_sac';
        retval.lastname = 'tst_developer';
        retval.FirstName='jon';
        retval.Salutation = 'Mr';
        retval.MobilePhone='07755648877';       
        retval.Phone='02555818000';
        return retval;       
        
    }
    
    public static Asset_Line_Count__c CreateAssetLineCount() {
        Asset_Line_Count__c retval = new Asset_Line_Count__c();
        retval.SAC_Code__c = 'tst_alc';
        retval.Month__c = Date.today().addMonths(-0);
        retval.Featurenet_Embark__c = 1;
        retval.ISDN2__c = 2;
        retval.ISDN_30__c = 3;
        retval.PSTN__c = 4;
        retval.Total_Lines__c = 10;
        return retval;
    }
    
    public static AccountTeamMember CreateAccountTeamMember() {
        AccountTeamMember retval = new AccountTeamMember();
        retval.userId = Userinfo.getUserId();                
        retval.TeamMemberRole = 'TestScript';        
        return retval;
    }
    
    public static Bulk_Tasks__c CreateBulkTasks() {
        Bulk_Tasks__c retval = new Bulk_Tasks__c();
        retval.description__c = 'tst_description';
        retval.task_unique__c = 'tst_tunique';
        retval.Task_id__c = 'tst_tid';
        retval.Minimum_Lead_Time__c = 10;
        retval.Reminder_Days__c = 1;
        retval.Minimum_Repeat_Time__c = 20;
        retval.USER_Type__c = 'TestScript';
        retval.Task_Category__c = 'Account';
        retval.LOB_Code__c = 'tst_lob';
        return retval;
    }
    
    public static Service_Improvement_Plan__c CreateCustomerExperienceActionPlan(string accountid){
        Service_Improvement_Plan__c retval = new Service_Improvement_Plan__c();
        retval.Account__c = accountid;
        retval.RAG__c = 'Red';
        return retval;
    }
    
    public static BTLB_CSAT__c CreateCSAT(String accountId){
        BTLB_CSAT__c retval = new BTLB_CSAT__c();
        retval.Account__c = accountId;
        return retval;
    }
    
    public static Customer_Experience_Actions__c CreatecustomerExperience(String PlanId, String CSATConId, String UserId, String ContactId){
        Customer_Experience_Actions__c retval = new Customer_Experience_Actions__c();
        retval.Customer_Experience_Client_Plan__c =PlanId;
        retval.RAG_Status__c = 'Red';
        //retval.CSAT_Contact_Feedback_LINK__c = CSATConId;
        retval.Problem_Statement__c = 'Test';
        retval.Customer_Owner__c = ContactId;
        retval.BT_Owner_Lookup__c = UserId;
        retval.Agreed_Success_Criteria__c = 'Test';
        retval.Root_Cause_Area__c = 'Other';
        retval.Root_Cause_Detail__c = 'Test';
        retval.Target_Date__c = System.Today()+10;
        retval.Latest_Update__c = 'Test';
        retval.Open_Closed__c = 'Open';
        //retval.Business_Manager_Email__c = '';
        return retval;
    }
    
    public static CSAT_Contact_Feedback__c CreateCSATContactFeedback(string Contactid){
        CSAT_Contact_Feedback__c retval = new  CSAT_Contact_Feedback__c();
        retval.contact__c =  Contactid;
        retval.Review_meeting_planned_date__c = system.today();
        return retval;
    }
    
    public static Contract CreateContract(string accountid) {
        Contract retval = new Contract();
        retval.AccountId = accountid; 
        retval.Name = 'TESTCODE';
        retval.sac_code__c = 'tst_sac';
        retval.name__c = 'tst_contract';
        retval.startDate = Date.today().addMonths(4);
        retval.Contract_Term_years__c = 2;
        return retval;
    }
    
    public static Case CreateCase(string accountid, string contactid){
        Case retval = new Case();
        retval.ContactID = contactid;
        retval.AccountID = accountid;
        retval.Status = 'New';
        retval.Origin = 'Phone';
        return retval;
    }
    
    public static Task CreateTask() {
        Task retval = new Task();
        retval.subject = 'tst_subject';
        retval.Task_Category__c = 'Bulk Tasks';
        retval.Owner_EIN__c = '802537216';
        retval.reminder_days__c = 4;
        retval.ActivityDate = (Date.today()+28);
        return retval;
    }
    
    public static Account_Team_Contacts__c CreateAccountTeamContacts(string accountid) {
        Account_Team_Contacts__c retval = new Account_Team_Contacts__c();
        retval.Account__c = accountid;
        retval.Sales_Manager_Primary__c = UserInfo.getUserId();
        return retval;
    }
    
    public static Landscape_BTLB__c CreateLandscapeBTLB() {
        Landscape_BTLB__c retval = new Landscape_BTLB__c();
        return retval;
    }
    
    public static Landscape_BTLB__c CreateLandscapeBTLB2(string accountid) {
        Landscape_BTLB__c retval = new Landscape_BTLB__c();
        retval.Customer__c = accountid;
        return retval;
    }
    
    public static SCARS_BTLB__c CreateSCARS_BTLB(string accountid) {
        SCARS_BTLB__c retval = new SCARS_BTLB__c();
        retval.Account__c = accountid;
        return retval;
    }
    
    
    
    
    public static Opportunity CreateOpportunity(string accountid) {
        Opportunity retval = new Opportunity();
        retval.AccountId = accountid;
        retval.Name = 'tst_oppty';
        //retval.NIBR_Year_2009_10__c = 1;
        //retval.NIBR_Year_2010_11__c = 1;
        //retval.NIBR_Year_2011_12__c = 1;
        retval.StageName = 'Created';
        retval.Sales_Stage_Detail__c = 'Appointment Made';
        retval.CloseDate = system.today();
        return retval;
    }    
    
    public static OpportunityContactRole CreateOpportunityContactRole(String opportunityid, string contactId){
        OpportunityContactRole retval = new OpportunityContactRole();
        retval.OpportunityId = opportunityid;
        retval.IsPrimary = True;
        retval.ContactId = contactId;
        retval.Role = 'User';        
        return retval;
    }
    
    public static cscfga__Product_Basket__c CreateProductBasket(String OpportunityId){
        cscfga__Product_Basket__c retval = new cscfga__Product_Basket__c();
        retval.Name = 'Test Record';
        retval.cscfga__Opportunity__c = OpportunityId;
        return retval;
    }
    
    public static Event CreateEvent(string whoId) {
        Event retval = new Event();
        retval.Subject = 'tst_event'; 
        retval.startdatetime = datetime.newInstance((Date.today() +1).year(),(Date.today() +1).month(),(Date.today() +1).day(),8,0,0);
        retval.DurationInMinutes = 100;
        retval.OwnerId = UserInfo.getUserId(); 
        retval.WhoId = whoId;
        return retval;    
    }   
    
    public static validateLandscapeBTLB_RecordLimit CreateValidateLandscapeBTLB_RecordLimit(ApexPages.StandardController controller){
        validateLandscapeBTLB_RecordLimit retval = null;
        if (controller == null) {
            retval = new validateLandscapeBTLB_RecordLimit();
        }
        else {
            retval = new validateLandscapeBTLB_RecordLimit(controller);
        }
        return retval;       
    }
    
    public static Product2 CreateProduct() {
        Product2 retval = new Product2();
        retval.IsActive = true;
        retval.Name = 'tst_prod';
        retval.ProductCode = 'Atstprd';
        return retval;       
    }
    
    public static PricebookEntry CreatePricebookEntry(string pricebook2Id, string product2Id){
        PricebookEntry retval = new PricebookEntry();
        retval.Product2Id = product2Id;
        retval.Pricebook2Id = pricebook2Id;
        retval.IsActive = true;
        retval.UnitPrice = 100;
        return retval;
    }
    
    public static OpportunityLineItem CreateOpportunityLineItem(string pricebookEntryID, string opptyID){
        OpportunityLineItem retval = new OpportunityLineItem();
        retval.OpportunityId = opptyID;
        retval.PricebookEntryId = pricebookEntryID;
        retval.UnitPrice = 4100;
        retval.Quantity = 1;    
        retval.category__c = 'Main';
        retval.contract_term__c='24 Months';
        retval.billing_cycle__c='Monthly';
        retval.service_ready_date__c = system.today().addmonths(1);
        retval.first_bill_date__c = system.today().addmonths(2);
        retval.initial_cost__c = 4000;
        return retval;
    } 
    
    public static Project__c CreateProject(String OpportunityId){
        Project__c retval = new Project__c();
        //retval.Name = 'Unit Test Project';
        retval.Opportunity__c = OpportunityId;
        return retval;
    }
    
    public static Phase__c CreatePhase(String ProjectId){
        Phase__c retval = new Phase__c();
        retval.Project__c = ProjectId;
        retval.Name = 'Test Phase';
        return retval;
    }
    
}