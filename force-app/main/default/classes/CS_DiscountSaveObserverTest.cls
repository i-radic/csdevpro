/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 05/11/2020
   @author Maahaboob Basha

   @description Test class for CS_DiscountSaveObserver.

   @modifications
   
*/
@isTest(SeeAllData=FALSE)
private class CS_DiscountSaveObserverTest {

    /***********************************************************************************************
    * Method Name : executeTest
    * Description : Used to simulate and test the logic of execute method in CS_DiscountSaveObserver 
    * Parameters  : NA
    * Return      : NA                      
    ***********************************************************************************************/
    static testmethod void executeTest() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'Test Product Definition');
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config', basket);
        prodConfig.Calculations_Product_Group__c = 'Cloud Voice';
        INSERT prodConfig;
        Test.startTest();
            CS_DiscountSaveObserver discSaveObserver = new CS_DiscountSaveObserver();
            result = discSaveObserver.execute(new List<cscfga__Product_Configuration__c>{prodConfig});
        Test.stopTest();
        System.assertEquals('', result);
    }
    
    /***********************************************************************************************
    * Method Name : executeTest
    * Description : Used to simulate and test the logic of execute method in CS_DiscountSaveObserver 
    * Parameters  : NA
    * Return      : NA                      
    ***********************************************************************************************/
    static testmethod void executeTest2() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'Test Product Definition');
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config', basket);
        prodConfig.Calculations_Product_Group__c = 'BT Mobile';
        INSERT prodConfig;
        Test.startTest();
            CS_DiscountSaveObserver discSaveObserver = new CS_DiscountSaveObserver();
            result = discSaveObserver.execute(new List<cscfga__Product_Configuration__c>{prodConfig});
        Test.stopTest();
        System.assertEquals('', result);
    }
    
    /***********************************************************************************************
    * Method Name : loggerTest
    * Description : Used to simulate and test the logic of logger method in CS_DiscountSaveObserver 
    * Parameters  : NA
    * Return      : NA                      
    ***********************************************************************************************/
    static testmethod void loggerTest() {
        Test.startTest();
            CS_DiscountSaveObserver discSaveObserver = new CS_DiscountSaveObserver();
            discSaveObserver.logger('Test');
        Test.stopTest();
    }
    
    /***************************************************************************************************
    * Method Name : validateFCCTestl
    * Description : Used to simulate and test the logic of validateFCC method in CS_DiscountSaveObserver 
    * Parameters  : NA
    * Return      : NA                      
    ***************************************************************************************************/
    static testmethod void validateFCCTest1() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'User Group');
        List<cscfga__Attribute_Definition__c> attrDefinitions = CS_TestDataFactory.generateAttributeDefinitions(TRUE, CS_DiscountSaveObserver.mccRequiredAttributes, prodDefinition);
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config', basket);
        prodConfig.cscfga__Product_Definition__c = prodDefinition.Id;
        prodConfig.Calculations_Product_Group__c = 'Future Mobile';
        INSERT prodConfig;
        List<cscfga__Attribute__c> attributes = CS_TestDataFactory.generateAttributesForConfiguration(TRUE, attrDefinitions, prodConfig);
        Test.startTest();
            CS_DiscountSaveObserver discSaveObserver = new CS_DiscountSaveObserver();
            result = discSaveObserver.execute(new List<cscfga__Product_Configuration__c>{prodConfig});
        Test.stopTest();
        System.assertEquals('FCC on User Group Invalid, please update. FCC = 20 Maximum FCC = 0', result);
    }
    
    /***************************************************************************************************
    * Method Name : validateFCCTest2
    * Description : Used to simulate and test the logic of validateFCC method in CS_DiscountSaveObserver 
    * Parameters  : NA
    * Return      : NA                      
    ***************************************************************************************************/
    static testmethod void validateFCCTest2() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'User Group');
        List<cscfga__Attribute_Definition__c> attrDefinitions = CS_TestDataFactory.generateAttributeDefinitions(TRUE, CS_DiscountSaveObserver.mccRequiredAttributes, prodDefinition);
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config', basket);
        prodConfig.cscfga__Product_Definition__c = prodDefinition.Id;
        prodConfig.Calculations_Product_Group__c = 'Future Mobile';
        INSERT prodConfig;
        List<cscfga__Attribute__c> attributes = CS_TestDataFactory.generateAttributesForConfiguration(FALSE, attrDefinitions, prodConfig);
        for (cscfga__Attribute__c attr : attributes) {
            if (attr.Name == 'Service Plan Name') {
                attr.cscfga__Value__c = 'Shared';
            }
        }
        INSERT attributes;
        cscfga__Product_Definition__c prodDefinition1 = CS_TestDataFactory.generateProductDefinition(TRUE, 'Shared Access Fee');
        List<cscfga__Attribute_Definition__c> attrDefinitions1 = CS_TestDataFactory.generateAttributeDefinitions(FALSE, new List<String>{'Recurring Charge'}, prodDefinition1);
        attrDefinitions1[0].cscfga__Is_Line_Item__c = TRUE;
        INSERT attrDefinitions1;
        cscfga__Product_Configuration__c prodConfig1 = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Shared Access Fee', basket);
        prodConfig1.cscfga__Parent_Configuration__c = prodConfig.Id;
        prodConfig1.Calculations_Product_Group__c = 'Future Mobile';
        prodConfig1.Grouping__c = 'Access Fee';
        INSERT prodConfig1;
        List<cscfga__Attribute__c> attributes1 = CS_TestDataFactory.generateAttributesForConfiguration(FALSE, attrDefinitions1, prodConfig1);
        for (cscfga__Attribute__c attr : attributes1) {
            if (attr.Name == 'Recurring Charge') {
                attr.cscfga__Value__c = '10';
                attr.cscfga__is_active__c = TRUE;
                attr.cscfga__Is_Line_Item__c = TRUE;
                attr.cscfga__Recurring__c = TRUE;
            }
        }
        INSERT attributes1;
        Test.startTest();
            CS_DiscountSaveObserver discSaveObserver = new CS_DiscountSaveObserver();
            result = discSaveObserver.execute(new List<cscfga__Product_Configuration__c>{prodConfig, prodConfig1});
        Test.stopTest();
        System.assertEquals('FCC on User Group Invalid, please update. FCC = 20 Maximum FCC = 0', result);
    }
    
    /***************************************************************************************************
    * Method Name : validateFCCTest3
    * Description : Used to simulate and test the logic of validateFCC method in CS_DiscountSaveObserver 
    * Parameters  : NA
    * Return      : NA                      
    ***************************************************************************************************/
    static testmethod void validateFCCTest3() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(FALSE, 'Test Basket', opp);
        basket.Tenure__c = 24;
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        INSERT basket;
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'User Group');
        List<cscfga__Attribute_Definition__c> attrDefinitions = CS_TestDataFactory.generateAttributeDefinitions(TRUE, CS_DiscountSaveObserver.mccRequiredAttributes, prodDefinition);
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config', basket);
        prodConfig.cscfga__Product_Definition__c = prodDefinition.Id;
        prodConfig.Calculations_Product_Group__c = 'Future Mobile';
        prodConfig.cscfga__Quantity__c = 1;
        prodConfig.cscfga__Contract_Term__c = 24;
        INSERT prodConfig;
        List<cscfga__Attribute__c> attributes = CS_TestDataFactory.generateAttributesForConfiguration(FALSE, attrDefinitions, prodConfig);
        for (cscfga__Attribute__c attr : attributes) {
            if (attr.Name == 'Service Plan Name') {
                attr.cscfga__Value__c = 'Shared';
            }
        }
        INSERT attributes;
        cscfga__Product_Definition__c prodDefinition1 = CS_TestDataFactory.generateProductDefinition(TRUE, 'User Plans');
        List<cscfga__Attribute_Definition__c> attrDefinitions1 = CS_TestDataFactory.generateAttributeDefinitions(FALSE, new List<String>{'Recurring Charge'}, prodDefinition1);
        attrDefinitions1[0].cscfga__Is_Line_Item__c = TRUE;
        INSERT attrDefinitions1;
        cscfga__Product_Configuration__c prodConfig1 = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Unlimited Voice & Text', basket);
        prodConfig1.cscfga__Parent_Configuration__c = prodConfig.Id;
        prodConfig1.Grouping__c = 'Voice';
        prodConfig1.cscfga__Quantity__c = 1;
        prodConfig1.cscfga__Contract_Term__c = 24;
        prodConfig1.Calculations_Product_Group__c = 'Future Mobile';
        INSERT prodConfig1;
        List<cscfga__Attribute__c> attributes1 = CS_TestDataFactory.generateAttributesForConfiguration(FALSE, attrDefinitions1, prodConfig1);
        for (cscfga__Attribute__c attr : attributes1) {
            if (attr.Name == 'Recurring Charge') {
                attr.cscfga__Value__c = '10';
                attr.cscfga__is_active__c = TRUE;
                attr.cscfga__Is_Line_Item__c = TRUE;
                attr.cscfga__Recurring__c = TRUE;
            }
        }
        INSERT attributes1;
        cscfga__Product_Configuration__c prodConfig2 = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Unlimited Voice & Text', basket);
        prodConfig2.cscfga__Parent_Configuration__c = prodConfig.Id;
        prodConfig2.Grouping__c = 'Data';
        prodConfig2.cscfga__Quantity__c = 1;
        prodConfig2.cscfga__Contract_Term__c = 24;
        prodConfig2.Calculations_Product_Group__c = 'Future Mobile';
        INSERT prodConfig2;
        List<cscfga__Attribute__c> attributes2 = CS_TestDataFactory.generateAttributesForConfiguration(FALSE, attrDefinitions1, prodConfig2);
        for (cscfga__Attribute__c attr : attributes2) {
            if (attr.Name == 'Recurring Charge') {
                attr.cscfga__Value__c = '10';
                attr.cscfga__is_active__c = TRUE;
                attr.cscfga__Is_Line_Item__c = TRUE;
                attr.cscfga__Recurring__c = TRUE;
            }
        }
        INSERT attributes2;
        Test.startTest();
            CS_DiscountSaveObserver discSaveObserver = new CS_DiscountSaveObserver();
            result = discSaveObserver.execute(new List<cscfga__Product_Configuration__c>{prodConfig, prodConfig1});
        Test.stopTest();
        System.assertEquals('FCC on User Group Invalid, please update. FCC = 20 Maximum FCC = 0', result);
    }
    
    /***************************************************************************************************
    * Method Name : validateFCCTest4
    * Description : Used to simulate and test the logic of validateFCC method in CS_DiscountSaveObserver 
    * Parameters  : NA
    * Return      : NA                      
    ***************************************************************************************************/
    static testmethod void validateFCCTest4() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        basket.FM_Voice_ARPU__c = 2;
        basket.FM_Data_ARPU__c = 2;
        basket.FM_Voice_ARPU_Margin__c = 2;
        basket.FM_Data_ARPU_Margin__c = 2;
        update basket;
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'User Group');
        List<cscfga__Attribute_Definition__c> attrDefinitions = CS_TestDataFactory.generateAttributeDefinitions(TRUE, CS_DiscountSaveObserver.mccRequiredAttributes, prodDefinition);
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config', basket);
        prodConfig.cscfga__Product_Definition__c = prodDefinition.Id;
        prodConfig.Calculations_Product_Group__c = 'Future Mobile';
        INSERT prodConfig;
        List<cscfga__Attribute__c> attributes = CS_TestDataFactory.generateAttributesForConfiguration(FALSE, attrDefinitions, prodConfig);
        for (cscfga__Attribute__c attr : attributes) {
            if (attr.Name == 'Subscription Type Name') {
                attr.cscfga__Value__c = 'Data Only';
            }
        }
        INSERT attributes;
        Test.startTest();
            CS_DiscountSaveObserver discSaveObserver = new CS_DiscountSaveObserver();
            result = discSaveObserver.execute(new List<cscfga__Product_Configuration__c>{prodConfig});
        Test.stopTest();
        System.assertEquals('', result);
    }
}