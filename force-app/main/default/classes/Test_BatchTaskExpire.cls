@isTest
private class Test_BatchTaskExpire {

    static testMethod void testBatchTaskExpire() {
        //data setup - start
        Account a = Test_Factory.CreateAccount();
        insert a;
        //First contact
        Contact contact = Test_Factory.CreateContact();
        contact.Phone = '01234 567890';
        contact.AccountId = a.Id;
        insert contact;
        
        List<Campaign_Call_Status__c> ccsToInsert = new List<Campaign_Call_Status__c>();
        Campaign_Call_Status__c ccs1 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status1', Responded__c = true, Default__c=true);
        Campaign_Call_Status__c ccs2 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status2', Responded__c = false, Default__c=false);
        ccsToInsert.add(ccs1);
        ccsToInsert.add(ccs2);
        insert ccsToInsert;
        Campaign c;
        Date enddate = date.today() + 1;
        c = new Campaign(Name='TestCampaign', Campaign_Type__c='BTLB', Type='Telemarketing', Status='Planned', 
            Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, isActive=true, Data_Source__c='Self Generated');
        insert c;
        
        CampaignMember cm = new CampaignMember(CampaignId=c.Id, ContactId=contact.Id, Unsuccessful_Call_Attempts__c = 1);
        insert cm;
        
        //RecordType rt = [select id from RecordType where Id=:StaticVariables.campaign_task_record_type];
        RecordType rt = [select id from RecordType where DeveloperName='Campaign_Task'];
        System.assert(rt!= null);
        
        Task t1 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c.Id);
        insert t1;
        t1 = [select id, Status from Task where Id = :t1.id];
        System.assert(t1.Status == 'Not Started');
    
        Task t2 = new Task(RecordType = rt, Status = 'Completed', Call_Status__c='Sale Made', WhatId=c.Id, WhoId=contact.Id, X_Day_Rule_Copy__c= 3, Subject='Call', Type='Call', Description='blaaa');
        insert t2;
        t2 = [select id, X_Day_Rule_Copy__c, activitydate, WhatId, Status from Task where Id = :t2.id];
        System.assert(t2.Status == 'Completed');
        //data setup - end
        
        
        //Update the Campaign End date to simulate having reached the Campaign End date
        c = [select Id, EndDate from Campaign where Id =:c.Id];
        c.EndDate = date.today()-1;//Yesterday
        update c;
             
        Test.startTest(); 
        String query = 'SELECT Id FROM Task WHERE isClosed=false AND WhatId IN (SELECT Id FROM Campaign WHERE Id = \'' + c.Id + '\' AND EndDate <'+ String.valueof(date.today()) +')';
        BatchTaskExpire b = new BatchTaskExpire(query);
        ID batchprocessid = Database.executeBatch(b,200);
        Test.stopTest();
        
        //Only the 'Not Started' Task should have been set to expire as the other task was 'Completed' (i.e. closed) so should not have been updated.
        System.AssertEquals(database.countquery('SELECT count() FROM Task WHERE WhatId=\'' + c.Id + '\' AND Status=\'Expired\''), 1);
        t1 = [SELECT Id, Status FROM Task WHERE Id =: t1.Id];
        t2 = [SELECT Id, Status FROM Task WHERE Id =: t2.Id];
        System.AssertEquals(t1.Status, 'Expired');
        System.AssertEquals(t2.Status, 'Completed');
    }
    
    static testMethod void testScheduler() {
        String CRON_EXP = '0 0 0 3 9 ? 2089';
        Test.startTest();
            
        // Schedule the test job  
            String jobId = System.schedule('testScheduledApex',
                CRON_EXP, new BatchScheduleTaskExpire());
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same  
        System.assertEquals(CRON_EXP, ct.CronExpression);
        // Verify the job has not run      
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run  
        System.assertEquals('2089-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest(); 
    }
}