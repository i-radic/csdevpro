public class EmailTemBSClassicSLProduct {

    public String relatedTo { get; set; }
    public  List<BS_Classic_SL_Product__c> listBSClassicProduct;
    public  List<BS_Classic_SL_Product__c> listBSClassicEmailfooter;
    public  List<BS_Classic_SL_Product__c> listBSClassicProductfooter;
    public List<BSCLassicEmail__c>  bSCLassicEmail{get;set;}
    public List<BSCLassicEmail__c>  bSCLassicEmailfooter{get;set;}
    public string SLProductid{get;set;}
   
    public EmailTemBSClassicSLProduct () {
     
    }

    public List<BS_Classic_SL_Product__c> getBSClassicSLProduct() {
      Set<String> bscEmail=new Set<String>();  
      bSCLassicEmail=new List<BSCLassicEmail__c>();
      
       listBSClassicProduct=[select id,email_statements__c,Name,Product__c,Product_Type2__c,Quantity__c, period__c, Friendly_name__c from BS_Classic_SL_Product__c where Sales_Log__c =: SLProductid  and email_statements__c <> '' ];
   
        for (BS_Classic_SL_Product__c bsc :listBSClassicProduct){
           do {
                system.debug('222222222@'+listBSClassicProduct);
              if (bsc.email_statements__c.length() >= 4){
                  bscEmail.add(bsc.email_statements__c.substring(0,4)); 
              }
              if (bsc.email_statements__c.length() > 4){
                  bsc.email_statements__c= bsc.email_statements__c.substring(4,bsc.email_statements__c.length());
              }
              else{
                  bsc.email_statements__c= '';
              }
           }
           while  (bsc.email_statements__c.length() >= 4 );
        }
            
       if (bscEmail.size() >0){  
       system.debug('TEST12345'+bscEmail);
           bSCLassicEmail=[SELECT  Email_Text__c,Text_Format__c FROM BSCLassicEmail__c WHERE type__c ='email' and email_code__c in :bscEmail ORDER BY email_code__c];
            if(bSCLassicEmail.size() > 0)
            system.debug('TESTbody######'+bSCLassicEmail[0].Email_Text__c);
       }
       
        return listBSClassicProduct;
        
    }
    
    
    public List<BSCLassicEmail__c> getEmailFooter() {
        set <String> footer = new Set<String>();
         List<BS_Classic_SL_Product__c> footerCodes = [Select email_statements__c from BS_Classic_SL_Product__c where Sales_Log__c = :SLProductid and email_statements__c <> '' ];
             for (BS_Classic_SL_Product__c ft :footerCodes){
                         do {
                            if (ft.email_statements__c.length() >= 4){
                                footer.add(ft.email_statements__c.substring(0,4)); 
                            }
                                if (ft.email_statements__c.length() > 4){
                                ft.email_statements__c = ft.email_statements__c.substring(4,ft.email_statements__c.length());
                            }
                            else{
                                ft.email_statements__c = '';
                            }
                            }
                            while  (ft.email_statements__c.length() >= 4 );
                   }
            
                 if (footer.size() >0){  
                        return [SELECT  Email_Text__c,Text_Format__c FROM BSCLassicEmail__c WHERE type__c ='footer' and email_code__c in :footer ORDER BY email_code__c ];
                        }
                 else {
                    return null;
                 }
}
}