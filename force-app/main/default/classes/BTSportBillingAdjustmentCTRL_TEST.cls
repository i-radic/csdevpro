@isTest
public class BTSportBillingAdjustmentCTRL_TEST { 
    static testMethod void test1() {
        BT_Sport_MVOC__c mvoc;
        mvoc = new BT_Sport_MVOC__c (MSA_Contract_Reference__c= 'test1',Billing_Email__c='EmailTest@email.com');
        insert mvoc ;

        BT_Sport_Billing_Adjustment__c adj;
        adj= new BT_Sport_Billing_Adjustment__c (Account_Number__c= 'test1',BT_Sport_MVOC__c = mvoc.id,Bill_Adjustment_Version__c=0,Bill_Issue_Date__c=system.Today());
        insert adj;

        Test.setCurrentPageReference(new PageReference('Page.BTSportBillingAdjustment')); 

        Test.setCurrentPageReference(new PageReference('Page.BTSportBillingAdjustmentSave')); 

        System.currentPageReference().getParameters().put('action', 'createBill');         
        BTSportBillingAdjustmentCTRL stdCtrl = new BTSportBillingAdjustmentCTRL(new ApexPages.StandardController(adj));	
		stdCtrl.attachmentId = 'test';	
		stdCtrl.onLoadAction();

        System.currentPageReference().getParameters().put('action', 'sendEmail');
		stdCtrl.onLoadAction();
    }
}