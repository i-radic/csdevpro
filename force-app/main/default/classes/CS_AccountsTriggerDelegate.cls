public class CS_AccountsTriggerDelegate extends CS_TriggerHandler.DelegateBase {
     
	 Usage_Profile__c defaultUsageProfile=null;

    // do any preparation here - bulk loading of data etc
    public override void prepareBefore() {
 
		List<Usage_Profile__c> profiles=[Select Id FROM Usage_Profile__c WHERE Is_Default_Profile__c = true LIMIT 1];
		
		if (!profiles.isEmpty()){
			defaultUsageProfile=profiles[0];
		}
    }
    
    // do any preparation here - bulk loading of data etc
    public override void prepareAfter() {

    }
    
    // Apply before insert logic to this sObject. DO NOT do any SOQL
    // or DML here - store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeInsert(sObject o) {
		
		 Account a = (Account) o;

		 if(a.Usage_Profile__c==null && defaultUsageProfile!=null )
		 {
			a.Usage_Profile__c=defaultUsageProfile.Id;
		 }		
    }
    
    // Apply before update logic to this sObject. DO NOT do any SOQL
    // or DML here - store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeUpdate(sObject old, sObject o) {
        
		 Account a = (Account) o;
	 
		 if(a.Usage_Profile__c==null && defaultUsageProfile!=null)
		 {
			a.Usage_Profile__c=defaultUsageProfile.Id;
		 }
    }
    
    // Apply before delete logic to this sObject. DO NOT do any SOQL
    // or DML here - store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void beforeDelete(sObject o) {
        
    }

    // Apply after insert logic to this sObject. DO NOT do any SOQL
    // or DML here - store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterInsert(sObject o) {
       
    }

    // Apply after update logic to this sObject. DO NOT do any SOQL
    // or DML here - store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUpdate(sObject old, sObject o) {
      
    }

    // Apply after delete logic to this sObject. DO NOT do any SOQL
    // or DML here - store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterDelete(sObject o) {
        
    }

    // Apply after undelete logic to this sObject. DO NOT do any SOQL
    // or DML here - store records to be modified in an instance variable
    // which can be processed by the finish() method
    public override void afterUndelete(sObject o) {
        
    }

    // finish logic - process stored records and perform any dml action
    // updates product baskets if bundle is desynchronised
    public override void finish() {

        }

}