@isTest
private class Test_BattlePlanExtension{
 
static testMethod void Test_BattlePlanExtension() {
         User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Account a = Test_Factory.CreateAccount();
        a.Name = 'Test';
        a.ownerId = thisuser.id ;
        a.Base_Team_Assign_Date__c=System.today();
        insert a;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        Battleplan__c batPlan = new Battleplan__c(Opportunity_Name__c = o.Id);
        insert batPlan;
        
        ApexPages.StandardController stdcon = new ApexPages.StandardController(batPlan);
        battlePlanExtension becon = new battlePlanExtension(stdcon);
        System.debug(becon.getbpOpp());
        System.debug(becon.getbpAccnt());
        System.debug(becon.getbpOpActs());
        System.debug(becon.getbpActsHty());
        System.debug(becon.getbpConts());
        System.debug(becon.getbpStrats());
        System.debug(becon.getbpComps());
        System.debug(becon.getbpTResults());
        System.debug(becon.getbpNotes());
    }
}