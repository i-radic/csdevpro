@IsTest
public class CustomButtonPLNoNPVTest{
	testMethod static void testPLNoNPV() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'test def');
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		CS_TestDataFactory.generatePLReportConfigRecords();
		
		CustomButtonPLNoNPV cntrl = new CustomButtonPLNoNPV();
		String actionStr = cntrl.performAction(basket.Id);
	}
}