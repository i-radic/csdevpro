@isTest
private class CS_Test_CloneBasketController
{
    private static cscfga__Product_Basket__c testBasket;

    private static void createTestData()
    {
       	CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;

        testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;

       
        
        createProductConfigurations(testBasket.Id);
        
        
    
    }

    private static void createProductConfigurations(Id basketId)
    {
        
        cscfga__Product_Definition__c prodDef =
            new cscfga__Product_Definition__c
                ( Name = 'Fixed Line'
                , cscfga__Description__c = 'Test helper' );
        insert prodDef;

        cscfga__Product_Definition__c prodDefNew =
            new cscfga__Product_Definition__c
                ( Name = 'BB Test'
                , cscfga__Description__c = 'Test helper');
        insert prodDefNew;

        cscfga__Attribute_Definition__c attrDef =
            new cscfga__Attribute_Definition__c
                ( Name = 'Test'
                , cscfga__Product_Definition__c = prodDef.Id );
        insert attrDef;

        cscfga__Attribute_Definition__c childAttrDef =
            new cscfga__Attribute_Definition__c
                ( Name = 'Test child'
                , cscfga__Product_Definition__c = prodDefNew.Id );
        insert childAttrDef;

        cscfga__Product_Configuration__c config =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = testBasket.Id
                , cscfga__Product_Definition__c = prodDef.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12
                , Calculations_Product_Group__c = 'Cloud Voice');
        insert config;

        cscfga__Attribute__c attribute =
            new cscfga__Attribute__c
                ( Name = 'Test'
                , cscfga__Product_Configuration__c = config.Id
                , cscfga__Value__c = '1.0'
                , cscfga__Attribute_Definition__c = attrDef.Id );
        insert attribute;

        cscfga__Product_Configuration__c subConfig =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = basketId
                , cscfga__Product_Definition__c = prodDefNew.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12
                , cscfga__Root_Configuration__c = config.Id
                , cscfga__Parent_Configuration__c = config.Id
                , Calculations_Product_Group__c = 'Cloud Voice');
        insert subConfig;

        cscfga__Attribute__c childAttribute =
            new cscfga__Attribute__c
                ( Name = 'Test child'
                , cscfga__Product_Configuration__c = subConfig.Id
                , cscfga__Value__c = '1.0'
                , cscfga__Attribute_Definition__c = childAttrDef.Id );
    }

    private static testMethod void testCloneBasketController()
    {
        createTestData();
        
        
        CS_CloneBasketController clone = new CS_CloneBasketController
            ( new ApexPages.StandardController(testBasket) );

        try{
            List<Attachment> mapAttachments = CS_CloneBasketController.mapAttachments(null, null);
        }
        catch(Exception exc){}

        clone.cloneBasket();
        
        List<cscfga__Attribute__c> atts = new List<cscfga__Attribute__c>{
            new cscfga__Attribute__c ( Name = 'testAttr'),
            new cscfga__Attribute__c ( Name = 'testAttr2')
        };
        
        Map<String, Map<String, String>> attrmap = CS_CloneBasketController.mapAttributesMap(atts);
        
        List<CS_Node> cfgTree = CS_Node.createTree(new List<cscfga__Product_Configuration__c>{ });
        
        Map<String, Set<String>> defAttrs = new Map<String, Set<String>> {
          'testAttr'  => new Set<String>{ 'a', 'b'},
          'testAttr2'  => new Set<String>{ 'c', 'd'}
        };
        
        CS_CloneBasketController.createAttachments(cfgTree, defAttrs, attrmap);

        List<CS_Related_Product_Attributes__c> settings =
            new List<CS_Related_Product_Attributes__c>
                { new CS_Related_Product_Attributes__c
                    ( Name = 'Fixed Line'
                    , Attribute_Names__c = 'Test')
                , new CS_Related_Product_Attributes__c
                    ( Name = 'BB Test'
                    , Attribute_Names__c = 'Test child' )};
        insert settings;

        CS_AfterCopyEventHandler eventHandler =
            new CS_AfterCopyEventHandler();
        eventHandler.handleEvent
            ( 'calculateTotals_finished'
            , new Map<String, Object>
                { 'recalcualtedBasketsAndBundles'
                    => new Set<Id>{ testBasket.Id }});
    }

}