@IsTest
public class CS_BasketTotalsTriggerDelegateTest  {
   // private static Account acc;
	//private static Opportunity opp;
	//private static cscfga__Product_Basket__c basket;

	/*private static void createTestData() {
		User thisUser = [select id from User where id=:userinfo.getUserid()];
        
        System.runAs( thisUser ){
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            upsert settings TriggerDeactivating__c.Id;
        } 
        
		acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
	}*/
	@IsTest
	public static void testBeforeUpdate() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        //createTestData();
		Basket_Totals__c basketTotals = new Basket_Totals__c(Name = 'Test Basket Totals');
		INSERT basketTotals;
       // basket.Basket_Totals__c = basketTotals.id;
        //Update basket;
		basketTotals = [SELECT Id, Name,
				Total_Extra_Data__c,
				Total_Extra_Text__c,
				Total_Shared_Data__c,
				Total_Extra_Shared_Data__c,
				Total_Devices__c,
				Default_Text_Usage__c,
				Default_Voice_Usage__c,
				Default_Usage_Profile_Total__c,
				Total_International_Shared_Calling_Extra__c,
				Total_Shared_Data_Extra_Recurring_Charge__c,
				Daily_Usage_Factor__c,
				Voice_Subsidy__c,
				Data_Subsidy__c,
                Total_BuyOut_Amount__c,
				Total_BTOP_Voice_Revenue__c
			FROM Basket_Totals__c WHERE Id = :basketTotals.Id
		];

		List<String> products = new List<String>{
			'BT Mobile Extras',
			'Shared Data',
			'Sharer Subscription',
			'BT Mobile Hardware',
			'BT Mobile Broadband',
			'BT Mobile Flex',
			'BT Mobile Sharer',
			'BTOP Company Extras',
			'BTOP User Subscription',
            'Mobile Voice SME',
            'BTOP Subscription Level Add-On'
		};
      //  cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(true, 'Test PC', basket);
       // pc.Airtime_Fund__c = 12;
       // update PC;
		List<Product_Configuration_Totals__c> prodConfigTotalsList = new List<Product_Configuration_Totals__c>();
		for (Integer i = 0; i < products.size(); i++){
			Product_Configuration_Totals__c prodConfigTotals = new Product_Configuration_Totals__c(
						Name = products[i], Basket_Totals__c = basketTotals.Id, Product__c = products[i],
						Quantity__c = 10, BT_Hardware_Fund__c = 9, BT_Technology_Fund__c = 8, Total_Recurring_Charge__c = 6, Contract_Term_Period__c = 12);

			if(products[i] == 'BT Mobile Extras') {
				prodConfigTotals.Product_Type__c = 'Shared Data';
			}
			if(products[i] == 'BT Mobile Hardware') {
				prodConfigTotals.Product_Type__c = 'Handset';
			}
			if(products[i] == 'BTOP User Subscription') {
				prodConfigTotals.Product_Type__c = 'Mobile Worker – SIM Only';
			}
			if(products[i] == 'BT Mobile Sharer') {
				prodConfigTotals.Product_Name__c = 'Unlimited';
			}
            if(products[i] == 'BTOP Subscription Level Add-On') {
				//prodConfigTotals.Product_Name__c = 'Landline';
                prodConfigTotals.Product_Type__c = 'Landline';
                prodConfigTotals.Product_Name__c = 'New Landline';
                //prodConfigTotals.Product_Configuration__c =pc.id;
			}
			prodConfigTotalsList.add(prodConfigTotals);
		}
		INSERT prodConfigTotalsList;

		CS_BasketTotalsTriggerDelegate controller = new CS_BasketTotalsTriggerDelegate();
		Test.startTest();
        try{
		controller.configTotalsByBasketTotals.put(basketTotals.Id, prodConfigTotalsList);
		controller.beforeUpdate(basketTotals, basketTotals);
        controller.prepareAfter();
        controller.beforeInsert(basketTotals);
        Controller.afterInsert(basketTotals);
        Controller.afterUpdate(basketTotals, basketTotals);
        Controller.afterUndelete(basketTotals);
        Controller.afterdelete(basketTotals);
        Controller.beforeDelete(basketTotals);
        }
        Catch(exception e){
            
        }
		Test.stopTest();

		System.assertNotEquals(null, controller);
		System.assertEquals(10, basketTotals.Total_Devices__c);
		System.assertEquals(174.295, basketTotals.Default_Voice_Usage__c);
	}
}