public class CS_CreditFundListEditorController {
    public String productDefinitionName { get; set; }
    // public Id prodBasketId { get; private set; }
    public String prodBasketDisplayName { get { return prodBasket != null ? prodBasket.Name : null; } }
    public cscfga__Product_Basket__c prodBasket { get; set; }
    private String mleSource;
    private static final String screenFlowName = 'DefaultCreditFunds';
    private static final String resourceName = '/resource/mleHandler';

    public CS_CreditFundListEditorController(ApexPages.StandardController controller) {
        retrieveBasket();

        if (prodBasket == null)
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Product Basket not selected.'));

        productDefinitionName = 'Credit Funds';

        // from component controller's constructor
        hiddenAttributes = new List<cscfga.Attribute>();

        // from component
        prodDefName = productDefinitionName;
        isEditable = true;
    }
   
    public String getMLEIframeSource() {
        if (mleSource == null) {
            cscfga__Product_Definition__c productDefinition = [
                select Id, Name
                from cscfga__Product_Definition__c
                where Name = :prodDefName 
                and cscfga__Active__c = true
                limit 1
            ];
            PageReference pr = new PageReference('/apex/csmle__Editor?id=' + prodBasketId + '&productDefinitionId=' + productDefinition.id + '&screenFlowName=' + screenFlowName + '&scriptPlugin=' + resourceName);
            mleSource = pr.getUrl();
        }
        return mleSource;
    }
    
    public PageReference reRenderBasket() {
        retrieveBasket();
        createPCRs(prodBasketId);
        return null;
    }

    private Id retrieveBasket() {
        // prodBasketId = controller.getId();
        prodBasketId = ApexPages.currentPage().getParameters().get('id');

        if (prodBasketId != null)
            prodBasket = getProductBasket(prodBasketId);

        return prodBasketId;
    }

    @RemoteAction
    public static Boolean checkAsyncJobStatus(String basketId){
        Boolean jobCompleted = false;
        //String attachmentName = 'AsyncTaskJob-cscfga.PCAsyncDeleteTask-' + basketId + '%';
        String attachmentName = '%Async%';
        List<Attachment> attc = [SELECT Id, Name FROM Attachment where name like :attachmentName and parentId = :basketId];
        if (!attc.isEmpty()){
          jobCompleted = false;
        }
        else{
          jobCompleted = true;
          deletePCRs(basketId);
          createPCRs(basketId);
        }
        return jobCompleted;
    }

    public static void deletePCRs(String basketId){
        List<csbb__Product_Configuration_Request__c> pcrs = [SELECT Id FROM csbb__Product_Configuration_Request__c WHERE csbb__Product_Configuration__c = '' AND csbb__Product_Basket__c = :basketId];
        delete pcrs;
    }
    
    public static void createPCRs(String basketId) {
    	system.debug('SASA ' + basketId);
    	List<cscfga__Product_Configuration__c> creditFundConfigs = [SELECT Id, (Select Id from csbb__Product_Configuration_Requests__r) From cscfga__Product_Configuration__c WHERE cscfga__Product_Definition__r.Name = 'Credit Funds' AND cscfga__Product_Basket__c = :basketId];
    	system.debug('SASA ' + creditFundConfigs);
    	List<cscfga__Product_Configuration__c> cfgs = new List<cscfga__Product_Configuration__c>();
    	for(cscfga__Product_Configuration__c creditFundCfg : creditFundConfigs) {
    		if(creditFundCfg.csbb__Product_Configuration_Requests__r == null || creditFundCfg.csbb__Product_Configuration_Requests__r.isEmpty()) {
    			cfgs.add(creditFundCfg);
    		}
    	}
    	
    	List<csbb__Product_Configuration_Request__c> requests = csbb.API_v1.createPcrs(cfgs, false, true);
    	system.debug('SASA ' + requests);
    }

    @TestVisible
    private cscfga__Product_Basket__c getProductBasket(Id productBasketId) {
        if (productBasketId == null)
            return null;

        List<cscfga__Product_Basket__c> baskets = [
            SELECT
                Id, Name,
                Complex_VAS__c, Credit_fund_remaining__c, Credit_fund_total__c, Gross_Margin__c, Hardware_Charges__c,
                Investment_Ratio__c, Monthly_Recurring_Charges__c, Monthly_Recurring_Charges2__c,
                Monthly_Recurring_Charges_3__c, NPV_Buy_out_cheque__c, NPV_GOM_Before_Commission2__c,
                NPV_GOM_per_Sub_per_Annum__c, Gross_Operating_Margin__c, NPV_Staged_Air_Time__c, NPV_Tech_Fund__c,
                NPV_Unconditional_cheque__c, Net_Billed_ARPU__c, Payback_Month__c, Percentage_Secured__c,
                Rolling_Air_Time__c, Total_Charges__c, Total_Cheque__c, Total_Cost__c, Total_Rolling__c,
                Total_Rolling_Non_NPV__c, Total_Staged__c, Total_Tech_Fund__c, Unsecured_Revenue__c, VAS__c
            FROM cscfga__Product_Basket__c
            WHERE Id = :productBasketId
        ];

        if (baskets.size() == 1)
            return baskets[0];

        return null;
    }

    // from CS_ProductConfigEditorController - start
    private static final String DEFAULT_SECTION_NAME = 'General Information';

    @TestVisible
    public class AttributeTableRowItem {
        public cscfga.Attribute attribute { get; set; }
        public Integer sectionIndex { get; set; }
        public String sectionName { get; set; }
        public Integer row { get { return (Integer)attribute.getDefinition().cscfga__Row__c; } }
        public Integer column { get { return (Integer)attribute.getDefinition().cscfga__Column__c; } }
    }

    @TestVisible
    public class AttributeTableRow {
        public cscfga.Attribute leftAttribute { get; set; }
        public cscfga.Attribute rightAttribute { get; set; }
        public Integer sectionIndex { get; set; }
        public String sectionName { get; set; }
        public Integer row { get; set; }
    }

    private cscfga.API_1.ApiSession editorApiSession { get; set; }

    public List<cscfga.Attribute> hiddenAttributes { get; private set; }

    public String prodDefName {
        get { return prodDefName; }
        set {
            prodDefName = value;
            prodDef = String.isBlank(value) ? null : getProductDefinition(value);
        }
    }

    public cscfga__Product_Definition__c prodDef { get; set; }

    public Id prodConfigId {
        get { return prodConfigId; }
        set {
            prodConfigId = value;
            prodConfig = (value == null) ? null : getProductConfiguration(value);
            prodBasketId = (prodConfig == null) ? null : prodConfig.cscfga__Product_Basket__c;
            editorApiSession = getCurrentApiSession(prodConfig, prodDef);
        }
    }

    public cscfga__Product_Configuration__c prodConfig { get; set; }

    public Id prodConfigIdToDelete { get; set; }

    public List<cscfga__Product_Configuration__c> productConfigList {
        get { return getProductConfigurations(prodDef, prodBasketId); }
        set;
    }

    public Id prodBasketId { get; set; }

    public Boolean isEditable { get; set; }

    public String prodConfEditorTitle {
        get { return prodConfigId == null ? 'New ' + prodDefName : 'Currently editing: [' + prodConfig.Name + ']'; }
    }

    public String saveButtonText {
        get { return prodConfigId == null ? 'Save New ' + prodDefName : 'Save Changes'; }
    }

    public String cancelButtonText {
        get { return prodConfigId == null ? 'Clear Editor' : 'Cancel Changes'; }
    }

    public List<List<AttributeTableRow>> attributeTable {
        get {
            List<AttributeTableRowItem> attList = getAttributeMap(prodDef, prodConfig);
            return generateAttributeTable(attList);
        }

        private set;
    }

/*
    // component controller's constructor
    public CS_ProductConfigEditorController() {
        hiddenAttributes = new List<cscfga.Attribute>();
    }
*/

    private cscfga.API_1.ApiSession getCurrentApiSession(cscfga__Product_Configuration__c pc, cscfga__Product_Definition__c pd) {
        cscfga.API_1.ApiSession retval;

        if (pc != null) {
            retval = cscfga.Api_1.getApiSession(pc);
            //cscfga.ProductConfiguratorController.currentInstance = session.getController();
        }
        else if (pd != null) {
            retval = cscfga.Api_1.getApiSession(pd);
            //cscfga.ProductConfiguratorController.currentInstance = session.getController();
            retVal.getController().selectConfig();
            
        }
        else {
            retval = null;
        }

        return retval;
    }
    
	@TestVisible
    private List<AttributeTableRowItem> getAttributeMap(cscfga__Product_Definition__c prodDef, cscfga__Product_Configuration__c prodConfig) {
        List<AttributeTableRowItem> retval = new List<AttributeTableRowItem>();
        hiddenAttributes = new List<cscfga.Attribute>();

        if (editorApiSession == null)
            editorApiSession = getCurrentApiSession(prodConfig, prodDef);

        if (editorApiSession == null)
            return retval;

        cscfga.ProductConfiguration pc = editorApiSession.getRootConfiguration();
        List<cscfga.Attribute> attributes = pc.getAttributes();

        for (cscfga.Attribute att : attributes) {
            cscfga__Attribute_Definition__c attDef = att.getDefinition();
            att.presentationType = attDef.cscfga__Type__c;

            if (attDef.cscfga__Row__c != null && attDef.cscfga__Column__c != null) {
                AttributeTableRowItem atri = new AttributeTableRowItem();
                atri.attribute = att;
                retval.add(atri);
            }
            else {
                hiddenAttributes.add(att);
            }
        }

        Set<Id> ssIdList = new Set<Id>();
        for (AttributeTableRowItem atri : retval) {
            cscfga__Attribute_Definition__c attDef = atri.attribute.getDefinition();

            if (attDef.cscfga__Screen_Section__c != null)
                ssIdList.add(attDef.cscfga__Screen_Section__c);
        }

        Map<Id, cscfga__Screen_Section__c> ssMap = new Map<Id, cscfga__Screen_Section__c>([
            SELECT Id, Name, cscfga__Index__c, cscfga__Label__c
            FROM cscfga__Screen_Section__c
            WHERE Id IN :ssIdList
        ]);

        for (AttributeTableRowItem atri : retval) {
            cscfga__Attribute_Definition__c attDef = atri.attribute.getDefinition();

            if (attDef.cscfga__Screen_Section__c == null || !ssMap.containsKey(attDef.cscfga__Screen_Section__c)) {
                atri.sectionIndex = -1;
                atri.sectionName = DEFAULT_SECTION_NAME;
            }
            else {
                atri.sectionIndex = (Integer)ssMap.get(attDef.cscfga__Screen_Section__c).cscfga__Index__c;
                atri.sectionName = ssMap.get(attDef.cscfga__Screen_Section__c).Name;
            }
        }

        return retval;
    }

    @TestVisible
    private List<List<AttributeTableRow>> generateAttributeTable(List<AttributeTableRowItem> attributeList) {
        List<List<AttributeTableRow>> retval = new List<List<AttributeTableRow>>();

        Map<Integer, Map<Integer, AttributeTableRow>> tableSections = new Map<Integer, Map<Integer, AttributeTableRow>>();

        for (AttributeTableRowItem atri : attributeList) {
            Map<Integer, AttributeTableRow> atrMap = tableSections.get(atri.sectionIndex);
            if (atrMap == null) {
                atrMap = new Map<Integer, AttributeTableRow>();
                tableSections.put(atri.sectionIndex, atrMap);
            }

            AttributeTableRow atr;

            if (atrMap.containsKey(atri.row)) {
                atr = atrMap.get(atri.row);
            }
            else {
                atr = new AttributeTableRow();
                atr.sectionName = atri.sectionName;
                atr.sectionIndex = atri.sectionIndex;
                atr.row = atri.row;

                atrMap.put(atr.row, atr);
            }

            if (atri.column == 0) {
                atr.leftAttribute = atri.attribute;
                //if (atr.rightAttribute == null)
                //  atr.rightAttribute = new cscfga.Attribute(null, null, null);
            }
            else {
                atr.rightAttribute = atri.attribute;
                //if (atr.leftAttribute == null)
                //  atr.leftAttribute = new cscfga.Attribute(null, null, null);
            }
        }

        List<Integer> sortedSectionIndices = new List<Integer>(tableSections.keySet());
        sortedSectionIndices.sort();

        for (Integer sectIdx : sortedSectionIndices) {
            Map<Integer, AttributeTableRow> sectionMap = tableSections.get(sectIdx);
            List<AttributeTableRow> sortedSection = new List<AttributeTableRow>();

            List<Integer> sortedRowIndices = new List<Integer>(sectionMap.keySet());
            sortedRowIndices.sort();

            for (Integer rowIdx : sortedRowIndices)
                sortedSection.add(sectionMap.get(rowIdx));

            retval.add(sortedSection);
        }

        return retval;
    }

    public void edit() {
        editorApiSession = getCurrentApiSession(prodConfig, prodDef);
    }

    public PageReference saveProdConfig() {
        cscfga.Api_1.ApiSession apiSess = editorApiSession;

        cscfga.ProductConfiguration pc = apiSess.getConfiguration();

        if (prodConfig == null) {
            pc.getSObject().cscfga__Product_Basket__c = prodBasketId;
            //pc.setOppId(prodBasketId);
        }

        apiSess.updateConfig();
        cscfga.ValidationResult vr = apiSess.validateConfiguration();

        if (vr.getIsValid()) {
            apiSess.persistConfiguration(false);
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Record Saved. Product Configuration Id = [' + pc.getSObject().Id + ']'));
        }
        else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Invalid configuration.'));
            for (cscfga.FieldMessage fm : vr.getAllErrors())
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, formatFieldMessage(fm)));

            return null;
        }

/*
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'prodConfigId = ' + prodConfigId));
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'prodConfig.Id = ' + (prodConfig != null ? prodConfig.Id : null)));
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'pc.getSObject().Id = ' + pc.getSObject().Id));
*/

        if (prodConfig == null && prodBasketId != null) {
            cscfga__Product_Configuration__c newPC = pc.getSObject();
    
            if (newPC.cscfga__Product_Basket__c != prodBasketId) {
                cscfga__Product_Basket__c pbToDelete = new cscfga__Product_Basket__c(Id = newPC.cscfga__Product_Basket__c);
                newPC.cscfga__Product_Basket__c = prodBasketId;
    
                update newPC;
                delete pbToDelete;
            }
        }

        prodConfigId = null;
        editorApiSession = getCurrentApiSession(null, prodDef);

        retrieveBasket();

        //return getPageRefWithParams();
        return null;
    }

    public PageReference clearEditor() {
        prodConfigId = null;
        editorApiSession = getCurrentApiSession(null, prodDef);
        retrieveBasket();

        return null;
    }
/*
    public PageReference deleteOpenProdConfig() {
        return deleteProdConfig(prodConfigId);
    }

    public PageReference deleteProdConfig() {
        return deleteProdConfig(prodConfigIdToDelete);
    }

    private PageReference deleteProdConfig(Id prodConfigToDelete) {
        if (prodConfigId == prodConfigToDelete)
            prodConfigId = null;

        delete new cscfga__Product_Configuration__c(Id = prodConfigToDelete);
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Record deleted. Product Configuration Id = [' + prodConfigToDelete + ']'));

        editorApiSession = getCurrentApiSession(null, prodDef);
        retrieveBasket();

        return null;
    }
*/
    private cscfga__Product_Definition__c getProductDefinition(String prodDefName) {
        cscfga__Product_Definition__c productDefinition = [
            SELECT Id, Name
            FROM cscfga__Product_Definition__c
            WHERE
                Name = :prodDefName AND
                cscfga__Active__c = true
                LIMIT 1
        ];

        return productDefinition;
    }

    private cscfga__Product_Configuration__c getProductConfiguration(Id prodConfId) {
        cscfga__Product_Configuration__c retval = [
            SELECT
                Id, Name,
                cscfga__Product_Bundle__c,
                cscfga__Product_Definition__c,
                cscfga__Configuration_Offer__c,
                cscfga__Product_Basket__c,
                cscfga__Screen_Flow__r.cscfga__Landing_Page_Reference__c
            FROM cscfga__Product_Configuration__c
            WHERE Id = :prodConfId
        ];

        return retval;
    }

    private List<cscfga__Product_Configuration__c> getProductConfigurations(cscfga__Product_Definition__c productDefinition, Id productBasketId) {
        Id productDefinitionId = productDefinition.Id;

        String query =
            'SELECT Id, Name, Type__c, Credit_Amount__c ' +
            'FROM cscfga__Product_Configuration__c ' +
            'WHERE cscfga__Product_Definition__c = :productDefinitionId AND cscfga__Product_Basket__c = :productBasketId';

        return Database.query(query);
    }

    @TestVisible
    private String formatFieldMessage(cscfga.FieldMessage fm) {
        List<String> fillers = new List<String>{
            fm.fieldReference.substring(0, fm.fieldReference.length() - 2).replace('_', ' '),
            fm.message
        };

        return String.format('Error on field [{0}]: {1}', fillers);
    }
    // from CS_ProductConfigEditorController - end
}