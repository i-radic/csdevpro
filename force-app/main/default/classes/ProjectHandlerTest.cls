/**
* ProjectHandlerTest.cls
*
* Test methods for ProjectHandler.cls and ProjectTrigger.trigger
*
* @author Gary Marsh (westbrook)
* @date 2013-06-27
*/
@isTest
private class ProjectHandlerTest {
        
    private static final Id SYS_ADMIN_PROFILE_ID = MockUtils.getProfile('System Administrator').Id;
    
    static testMethod void updateOnHold() {
        
          TriggerDeactivating__c dt = new TriggerDeactivating__c();
            dt.Opportunity__c = True;
            insert dt;
        
        Project__c obj = (Project__c)Mock.one('Project__c', 
            new Map<String, Object>{
                'On_Hold__c' => false
            }, true);
        
        obj.On_Hold__c = true;
        obj.On_Hold_Reason__c = 'test';
        
        Test.startTest();
        update obj;
        Test.stopTest();
        
        System.assertEquals(1, [select count() from FeedItem where ParentId = :obj.Opportunity__c],
        'Expected 1 feed item related to project opportunity');
    }
    
    static testMethod void updateNotOnHold() {
        
          TriggerDeactivating__c dt = new TriggerDeactivating__c();
            dt.Opportunity__c = True;
            insert dt;
        
        Project__c obj = (Project__c)Mock.one('Project__c', 
            new Map<String, Object>{
                'On_Hold__c' => true,
                'On_Hold_Reason__c' => 'test'
            }, true);
    
        for(FeedItem fi : [select Id, Type, Body from FeedItem where ParentId = :obj.Opportunity__c]) {
            System.debug('~~Insert:' + fi);
        }
        
        obj.On_Hold__c = false;
        obj.On_Hold_Reason__c = null;
        
        Test.startTest();
        update obj;
        Test.stopTest();
        
        for(FeedItem fi : [select Id, Type, Body from FeedItem where ParentId = :obj.Opportunity__c]) {
            System.debug('~~Update:' + fi);
        }
        
        System.assertEquals(1, [select count() from FeedItem where ParentId = :obj.Opportunity__c],
        'Expected 1 feed item related to project opportunity');
    }
    
    static testMethod void updateStageAwaitAssign() {
        doUpdateWithTrackedStageTest('Awaiting Assignment to Tech. Delivery');
    }
    
    static testMethod void updateStageProjectAssign() {
        doUpdateWithTrackedStageTest('Project Assigned');
    }
    
    static testMethod void updateStageCustWelcomeComplete() {
        doUpdateWithTrackedStageTest('Customer Welcome Complete');
    }
    
    static testMethod void updateStageSystemCommissioned() {
        doUpdateWithTrackedStageTest('System Commissioned & Tested');
    }
    
    static testMethod void updateStageHandedOver() {
        doUpdateWithTrackedStageTest('Handed Over to Inlife');
    }
    
    static testMethod void updateStageSORApproved() {
        doUpdateWithTrackedStageTest('SOR Approved by Customer');
    }
    
    static testMethod void updateStageSORCommercial() {
        doUpdateWithTrackedStageTest('SOR Commercially Approved');
    }
    
    static testMethod void updateWithNonTrackedStage() {
        doUpdateWithNonTrackedStageTest('Project Under Review');
    }
    	
    static testMethod void updateProjectManagerTPS() {
    	doUpdateProjectManagerChangeTest(ProjectHandler.RT_TPS);
    }
    
    static void doUpdateWithTrackedStageTest(String stageValue) {
        
          TriggerDeactivating__c dt = new TriggerDeactivating__c();
            dt.Opportunity__c = True;
            insert dt;
        
        Project__c obj = (Project__c)Mock.one('Project__c', 
            new Map<String, Object>{
                'On_Hold__c' => false,
                'SCS_Added__c' => true,
                'SOR_Proposal_Added__c' => true
            }, true);
        
        obj.Stage__c = stageValue;
        
        Test.startTest();
        update obj;
        Test.stopTest();
        
        System.assertEquals(1, [select count() from FeedItem where ParentId = :obj.Opportunity__c],
        'Expected 1 feed item related to project opportunity');
    }
    
    static void doUpdateWithNonTrackedStageTest(String stageValue) {
          TriggerDeactivating__c dt = new TriggerDeactivating__c();
            dt.Opportunity__c = True;
            insert dt;
        
        Project__c obj = (Project__c)Mock.one('Project__c', 
            new Map<String, Object>{
                'On_Hold__c' => false,
                'SCS_Added__c' => true,
                'SOR_Proposal_Added__c' => true
            }, true);
        
        obj.Stage__c = stageValue;
        
        Test.startTest();
        update obj;
        Test.stopTest();
        
        System.assertEquals(0, [select count() from FeedItem where ParentId = :obj.Opportunity__c],
        'Expected 0 feed item related to project opportunity');
    }
    	
    static void doUpdateProjectManagerChangeTest(String rtDeveloperName) {
        String EIN = '358955256';
        String managerEIN = '468942839';
        String managerEIN2 = '463698828';
    	List<User> admins = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID ,'EIN__c' => EIN}, 1, true);
    	List<User> projectManagers = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID ,'EIN__c' => managerEIN }, 1, true);
        List<User> projectManagers2 = Mock.many('User', new Map<String, Object>{'ProfileId' => SYS_ADMIN_PROFILE_ID ,'EIN__c' =>managerEIN2}, 1, true);
    	
    	System.runAs(admins[0]) {
            
            TriggerDeactivating__c dt = new TriggerDeactivating__c();
            dt.Opportunity__c = True;
            insert dt;
            
    		Project__c obj = (Project__c)Mock.one('Project__c', 
            new Map<String, Object>{
                'On_Hold__c' => false,
                'SCS_Added__c' => true,
                'SOR_Proposal_Added__c' => true,
                'RecordTypeId' => MockUtils.getRecordType('Project__c', rtDeveloperName).Id,
                'Project_Manager__c' => projectManagers[0].Id
            }, true);
        
	        obj.Project_Manager__c = projectManagers2[0].Id;
	        
	        Test.startTest();
	        update obj;
	        Test.stopTest();
	        		
	        System.assertEquals(1, [select count() from FeedItem where ParentId = :obj.Opportunity__c],
        	'Expected 1 feed item related to project opportunity');
    	}
    }
}