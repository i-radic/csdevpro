@isTest
private class Test_convertToOrder {
    static testMethod void ConvertToOrderTest1() {
        Opportunity oppty = new Opportunity(name='test Opp', closedate=system.today(), stagename='Created');
        
        insert oppty;       
        convertToOrder cto = new convertToOrder(oppty);
        cto.opptyId = oppty.id;
        cto.OnLoad();
    } 
    
    static testMethod void ConvertToOrderTest2() {
        Opportunity oppty = new Opportunity(); 
        convertToOrder cto = new convertToOrder(oppty);
        cto.opptyId = '_TEST_';
        cto.OnLoad();
    }      
}