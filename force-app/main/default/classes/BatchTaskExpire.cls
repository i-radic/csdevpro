global class BatchTaskExpire implements Database.Batchable<SObject>{
 
	private String query;
	
	global BatchTaskExpire(String q){
		this.query = q;
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		List<Task> tasks = new List<Task>();
		for(Sobject t : scope){
			Task task = (Task)t;
			task.Status='Expired';
			tasks.add(task);
		} 
		update tasks;
	}

	global void finish(Database.BatchableContext BC){
	}
}