global class batchOpportunityForecastMonth implements Database.Batchable<SObject>{
     private String query;

    global batchOpportunityForecastMonth (String q){
        this.query = q;
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('JB batchOpportunityForecast Execute method size:' + scope.size());
        List<Opportunity> opps = new List<Opportunity>();
          for(Sobject c : scope){
            Opportunity opp = (Opportunity)c;
            system.debug('jb opportunity:' + opp);
            // copy this Months figures to last Months
          //opp.zLast_Month_Forecast_ACV__c = opp.zCurrent_Month_Forecast_ACV__c;
            opp.zLast_Month_Forecast_ACV__c = opp.Net_ACV__c;
          //opp.zLast_Month_Forecast_CY_NIBR__c = opp.zCurrent_Month_Forecast_CY_NIBR__c;
            opp.zLast_Month_Forecast_CY_NIBR__c = opp.SOV_GM__c;
            opp.zLast_Month_Forecast_NIBR__c = opp.zCurrent_Month_Forecast_NIBR__c;
            opp.zLast_Month_Forecast_Vol__c = opp.zCurrent_Month_Forecast_Vol__c;
            opp.Last_Month_Forecast_Amount__c = opp.Current_Month_Forecast_Amount__c;
            
            if(opp.Close_Date_Fiscal_Month__c == opp.zCurrent_Fiscal_Month__c) {
              //opp.zCurrent_Month_Forecast_ACV__c = opp.ACV_Calc__c;
                opp.zCurrent_Month_Forecast_ACV__c = opp.Net_ACV__c;                
                opp.zCurrent_Month_Forecast_NIBR__c = opp.NIBR_Next_Year__c;
                opp.zCurrent_Month_Forecast_Vol__c = 1;
               //opp.zCurrent_Month_Forecast_CY_NIBR__c = opp.NIBR_Current_Year__c;
                opp.zCurrent_Month_Forecast_CY_NIBR__c = opp.SOV_GM__c;
                opp.Current_Month_Forecast_Amount__c = opp.Amount;
                opp.ForecastUpdatedMonth__c = Date.today();
            }
            else {
                opp.zCurrent_Month_Forecast_ACV__c = 0;                       
                opp.zCurrent_Month_Forecast_NIBR__c = 0;
                opp.zCurrent_Month_Forecast_Vol__c = 0;
                opp.zCurrent_Month_Forecast_CY_NIBR__c = 0;
                opp.Current_Month_Forecast_Amount__c=0;
                opp.ForecastUpdatedMonth__c = Date.today();
            }
            
            
            
            opps.add(opp);
        } 
        update opps;
    }
 global void finish(Database.BatchableContext BC){
    }
}