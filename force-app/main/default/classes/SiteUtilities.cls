global with sharing class SiteUtilities {
	public static final String HASH = 'hash';
    public static final String EMAIL = 'email';
    public static final String TIMESTAMP = 'timestamp';
    public static final String SITE_URL = 'https://csdevpro-btbusiness.cs82.force.com/pinaccess';
    public static final String UTF8 = 'UTF-8';
    public static final String ENC = 'encrypted';
    public static final String SPID = 'sitePINid';
    public static final String ENC_TYPE = 'AES128';
    public static final String IS_VALID = 'isValid';
    public static final String V_HASH = 'vHash';
    public static final String PIN_SENT = 'pinSent';
    public static final Integer ENC_SIZE = 128;
    public static final String OE = 'oe';
    public static final String BASKET = 'basket';
    public static final String BASKETID = 'basketId';
    public static final String SOLUTION = 'solution';

    public static String decryptString(Blob key, Blob inputString){
        Blob decrypted = Crypto.decryptWithManagedIV(ENC_TYPE, key, inputString);
        return decrypted.toString();
    }

    public static Blob encryptString(Blob bKey, String inputString){
        Blob bString = Blob.valueOf(inputString);
        Blob encryptedString = Crypto.encryptWithManagedIV(ENC_TYPE, bKey, bString);
        return encryptedString;
    }
    
    public static String blob2String(Blob toString){
        String r = JSON.serialize(toString);
        return EncodingUtil.urlEncode(r.substring(1, r.length()-1), UTF8); 
    }
    
    public static String encodeURI(Map<String, Blob> toEncode){
        String uri = '?';
        String p = JSON.serialize(toEncode.get(EMAIL));
        p = EncodingUtil.urlEncode(p.substring(1, p.length()-1), UTF8);           
        String w = JSON.serialize(toEncode.get(SPID));
        w = EncodingUtil.urlEncode(w.substring(1, w.length()-1), UTF8); 
        uri += 'p=' + p + '&t=' + EncodingUtil.base64Encode(toEncode.get(TIMESTAMP)) + '&w=' + w;
        return uri;
    }
    
}