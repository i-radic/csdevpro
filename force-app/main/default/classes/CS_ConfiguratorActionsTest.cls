@isTest
private class CS_ConfiguratorActionsTest {
	
	@isTest static void test_method_one() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account acc = CS_TestDataFactory.generateAccount(True,'TestingAccount');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(True, 'TestOpp',acc);
        Usage_Profile__c usProf = CS_TestDataFactory.generateUsageProfile(True,acc);
        usProf.Is_Default_Profile__c = true;
        update usProf;
        cscfga__Product_Basket__c bskt = CS_TestDataFactory.generateProductBasket(True, 'TestBskt',opp);
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(True,'BT Mobile');
        
         cscfga__Attribute_Definition__c attrDef1 =
            new cscfga__Attribute_Definition__c
                ( Name = 'ProductActionConditions'
                , cscfga__Product_Definition__c = prodDef.Id );
        insert attrDef1;

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false,'BT Mobile Flex', bskt);
        pc.cscfga__Key__c = 'ProductActionConditions';
        insert pc;

        cscfga__Attribute__c attr1 = new cscfga__Attribute__c(Name = 'ProductActionConditions', cscfga__Value__c = acc.id, cscfga__Attribute_Definition__c = attrDef1.ID,
                cscfga__Product_Configuration__c = pc.ID, cscfga__is_active__c = true);  
        insert attr1;

        Map<String, cscfga__Attribute__c> attrMap = new Map<String, cscfga__Attribute__c>{'ProductActionConditions' => attr1};
        Map<String, Map<String, cscfga__Attribute__c>> parameterMap = new Map<String, Map<String, cscfga__Attribute__c>>{ 'ProductActionConditions' => attrMap};
        Map<Id, cscfga__Product_Definition__c> prodDefMap = new Map<Id, cscfga__Product_Definition__c>{prodDef.Id => prodDef};

    	Map<String, Set<Id>> mapa = CS_ConfiguratorActions.getConfigsByActions();
    	CS_ConfiguratorActions.executeActions(new Map<Id, cscfga__Product_Configuration__c>{pc.Id => pc});
    	Map<Id, String> idCFGMap = CS_ConfiguratorActions.configActionsMap;
    	idCFGMap = CS_ConfiguratorActions.configActionsMap;
    	Map<String, CS_ConfiguratorActions.ProductAction> actionSttng = CS_ConfiguratorActions.actionSettingsFromJSON;
    	Map<String, Set<Id>> returnmap = CS_ConfiguratorActions.getConfigsByDef(new List<cscfga__Product_Configuration__c>{pc});
    	returnmap = CS_ConfiguratorActions.getConfigsByDefFromConfigList(new List<cscfga__Product_Configuration__c>{pc});

    	CS_ConfiguratorActions controller = new CS_ConfiguratorActions();
    	CS_ConfiguratorActions.setProductConfigurationRequests(new Set<Id>{pc.Id}, 'valid');
    	
    	CS_ConfiguratorActions.setPCRequests(new Set<Id>{pc.Id}, 'valid');

    	CS_ConfiguratorActions.updateConditionAttribute(attrMap, new Map<String, String>{'ProductActionConditions' => 'test'});
    	List<cscfga__Attribute__c> attrList = CS_ConfiguratorActions.getAttributeListFromConfiguration(pc.Id);
    	List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>{pc};

    	String returnString = CS_ConfiguratorActions.getAction('currentAction', 'newAction');
    	Map<String, String> oldValuesMap = CS_ConfiguratorActions.getOldValues(new Map<String, cscfga__Attribute__c>{'ProductActionConditions' => attr1});

    	try{
    		Map<Id, String> returnmapActions = CS_ConfiguratorActions.getActions(parameterMap, pcList, prodDefMap, 'update'); 
    	}
    	catch(Exception exc){}

    	CS_ConfiguratorActions.ProductAction action = new CS_ConfiguratorActions.ProductAction();
    	CS_ConfiguratorActions.ActionConditions actionCondition = new CS_ConfiguratorActions.ActionConditions();
    	actionCondition.name = 'test';
    	actionCondition.action = 'test';
    	actionCondition.value = 'test';
    	actionCondition.event = 'test';
    	action.product = 'test';
    	action.dependentProducts = new List<String>{'test'};
    	action.conditions = new List<CS_ConfiguratorActions.ActionConditions>{actionCondition};

    	Boolean responseBool = actionCondition.isValid('test1', 'test', 'test1');
    	actionCondition.event = 'update';
    	responseBool = actionCondition.isValid('test1', 'test', 'update');
    	actionCondition.event = 'delete';
    	responseBool = actionCondition.isValid('test1', 'test', 'delete');
    	actionCondition.event = 'change';
    	responseBool = actionCondition.isValid('test1', 'test', 'change');
	}
}