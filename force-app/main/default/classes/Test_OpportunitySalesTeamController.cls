@isTest
private class Test_OpportunitySalesTeamController {

    static testMethod void myUnitTest() {
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99993';
        insert a;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        o.OwnerId = UserInfo.getUserId();
        insert o; 
        
        PageReference pageRef=Page.OpportunitySalesTeam;
        Test.setCurrentPageReference(pageRef);
        ApexPages.currentPage().getParameters().put('id', o.Id);
        OpportunitySalesTeamController myController = new OpportunitySalesTeamController();
        myController.onLoad();
    }
}