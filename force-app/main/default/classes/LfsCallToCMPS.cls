global class LfsCallToCMPS {
	public static String lfsCugId {get; set;}
    
    @future(callout=true)
    //public static void getCUGId(String lfsTelNumber,ID opporId)  
    public static void getCUGId(String lfsTelNumber,String opporId, String lfsCUG) {
        try{
            //SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();
            
            //service.timeout_x = 60000;
            Opportunity opp = [Select LFS_CUG__c from Opportunity where id =: opporId limit 1];
           	if(!Test.isRunningTest()){
            	lfsCugId = SalesforceCRMService.GetCUGId(lfsTelNumber);//service.GetCUGId(lfsTelNumber);
            	system.debug('lfs cugid future callout ' + lfsCugId);
            	system.debug('lfs cug from old opp ' + lfsCUG);
           	}
           	if(opp != null || Test.isRunningTest()){
                if(lfsCugId != null){
                	opp.LFS_CUG__c = lfsCugId;
                } else {
                	opp.LFS_CUG__c = lfsCUG;
                }
                update opp;
            }
        }
        catch(Exception ex){
            system.debug('exception ' + ex);
        }
    }
}