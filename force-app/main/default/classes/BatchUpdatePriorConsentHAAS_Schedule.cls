global class BatchUpdatePriorConsentHAAS_Schedule implements Schedulable {

    global void execute (SchedulableContext ctx) {
    
        String Query =  'SELECT ID,PriorStatus__c,status__c, Email_Consent__c,DL_Contact_Sector__c,Email, Phone_Consent__c, Mobile_Consent__c,EmailPriorConsent__c,SMSPriorConsent__c,PostalPriorConsent__c,' +
            'Address_Consent__c, Email_Consent_Date__c,emailHidden_2nd__c,Email_3rd__c,emailHidden_3rd__c,Email_2nd__c, emailHidden__c,Phone_Consent_Date__c,VoicePriorConsent__c,PostalPriorConsentDate__c,' +
            'Address_Consent_Date__c, Mobile_Consent_Date__c,EmailPriorConsentDate__c,VoicePriorConsentDate__c,SMSPriorConsentDate__c ' +              
            'FROM Contact Where  LastModifiedDate = Today ANd LastModifiedBYId = \'0050J000007Ywtj\'';

            BatchUpdatePriorConsentHAAS HAASContactBatchObject = new BatchUpdatePriorConsentHAAS(Query); 
            Database.executeBatch(HAASContactBatchObject,2000);

        
    }

}