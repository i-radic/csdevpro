/******************************************************************************************************
Name : QuestionnaireRecordtypeSelectclass
Description : Controller for page QuestionnaireRecordtypeSelectpage
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    1/12/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public class QuestionnaireRecordtypeSelectclass
{
    public String opptyid;
    public String recordtype{get;set;}
    public Questionnaire__c qt;
    public List<Questionnaire__c> qustList ;
    public QuestionnaireRecordtypeSelectclass(ApexPages.StandardController controller) 
    {
        qt=(Questionnaire__c)controller.getRecord();
        opptyid=qt.Opportunity__c;
        qustList = new List<Questionnaire__c>();
    }
    public List<SelectOption> opts{get;set;}
    public List<SelectOption> getmyOptions() 
    {
        opts = new List<SelectOption>();
        opts.add(new SelectOption('select','--Please Select Record Type --'));
        for(RecordType rts : [Select Id,DeveloperName,Name From RecordType Where SObjectType = 'Questionnaire__c'])
        {
            opts.add(new SelectOption(rts.name,rts.name));
        }
        return opts;
    }
    public Pagereference gotonewpage()
    {     
        String gateName;
        Opportunity opptyRecord = [Select id, StageName,CSC_Call_Pass__c,RecordType.DeveloperName from Opportunity where id = :qt.Opportunity__c];
        if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_PRE_QUALIFICATION ))
        {
            gateName='Make Pursuit';
        }
        else if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_QUALIFICATION))
        {
            gateName='Sign On';
        }
        else if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_DEVELOP_SOLUTION ))
        {
            if(opptyRecord.CSC_Call_Pass__c)
            {
                gateName='Sign Off';
            }
            else
            {
                gateName='CSC Call';
            }
        }
        else if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_PRESENT_NEGOTIATE ))
        {
            gateName ='Contract Sign Off';
        }
        if(opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_CLOSE_WON)||opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_CLOSED_LOST)||opptyRecord.StageName.EqualsIgnorecase(GlobalConstants.OPPORTUNITY_STAGE_CLOSED_CANCELLED))
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.Questionnaire_closed_opportunity_error));
            return null;
        }
        else if(recordtype.equalsignorecase(GlobalConstants.QUESTIONARRE_QUALIFICATION_RECORDTYPE_LABEL ))
        {
          
          //  if(opptyRecord.RecordType.DeveloperName.Equalsignorecase(GlobalConstants.OPPORTUNITY_STANDARD_MCC_RECORDTYPE  ))
           // {
           //     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.Questionnairequalificationstndard_error));
            //   return null;
           // }
            qustList = [select id,Gate__c,Opportunity__c from Questionnaire__c where Opportunity__c =:qt.Opportunity__c and Gate__c = :gateName];
            if(!qustList.isEmpty() && qustList[0].Gate__c == gateName)
            {
                PageReference pageRef = Page.Questionnairepageedit;
                pageRef.getParameters().put('qt',qustList[0].id);
                return PageRef;
            }
            else
            {
                PageReference pageRef = Page.ViewQuestions;
                pageRef.getParameters().put('opp',opptyid);
                return PageRef;
            }
        }
        else if(recordtype.equalsignorecase(GlobalConstants.QUESTIONARRE_PROFILER_RECORDTYPE_LABEL))
        {
            qustList = [select id,RecordType.Name,Opportunity__c from Questionnaire__c where Opportunity__c =:qt.Opportunity__c and RecordType.Name = :GlobalConstants.QUESTIONARRE_PROFILER_RECORDTYPE_LABEL];
            if(!qustList.isEmpty())
            {
                PageReference pageRef = Page.OpportunityProfileredit;
                pageRef.getParameters().put('qt',qustList[0].id);
                return PageRef;
            }
            else
            {
                PageReference pageRef = Page.OpportunityProfiler;
                pageRef.getParameters().put('opp',opptyid);
                return PageRef;
            }
        }
        else
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,label.QuestionnaireRecordtypeSelectError));
        }
        return null;
    }
    public Pagereference cancel()
    {     
        PageReference orderPage = new PageReference('/' + opptyid);
        orderPage.setRedirect(true);
        return orderPage;
    }
}