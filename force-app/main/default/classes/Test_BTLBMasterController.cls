@isTest
private class Test_BTLBMasterController{
    static testMethod void myUnitTest(){
    
    Profile p= [select id from profile where name='System Administrator'];
    
    User u;
    u= new User(alias='asd',email='testing@bt.it',emailencodingkey='UTF-8',lastname='Testingasd', languagelocalekey='en_US',        
                localesidkey='en_US',EIN__c='213test',isActive=True,Department='TestSupport', profileid = p.Id, timezonesidkey='Europe/London', username='testingasd@bt.it');                
    insert u;
    
    
    
    BTLB_Master__c BM = new BTLB_Master__c();
    BM.Name='TestSupport';
    BM.BTLB_Name_ExtLink__c='TestSupport';
    BM.Email_Signature__c='Regards,Test Support Team';
    BM.Company_Name__c='Test Company';
    BM.Company_Registration_No__c='12345';
    BM.Company_Registered_Address__c='Address';
    insert BM;
    
            
            
    BTLBMasterController myController = new BTLBMasterController(new ApexPages.StandardController(u));
    //String SelectedName='OK';
    //ApexPages.currentPage().getParameters().put('SelectedName','BM.Name');
    myController.SelectedName= BM.Name;
    myController.getBTLBnames();
    myController.UpdateUser();
    }
    
    
}