/*
*  This controller generates email template body for the visualforce component "MACOrderConfirmation" 
* 
*  Author                 Date               Description
*  Sridevi Bayyapuneedi   03/10/2014         Initial development
*/


public with sharing class MACOrderConfirmationController{
   
    public Id custCotId{get;set;}
    public  String MACTable{get;set;}
    public Id contactId{ get; set; }
    public Contact cont{ get; set; }  
    public String custName{get;set;}
    
    public MACOrderConfirmationController() {
    }  
      
       
   public void getTemplateDet(){
   
       if(custCotId != null){
           
           //system.debug('qqqqqqqqqgetMACCodeMethod   '+custCotId);
           contactId = [Select Contact__c from CustomerOptions__c where Id=:custCotId].Contact__c;
           cont=[Select Email,Name,Mac_code__c from Contact where Id=:contactId ]; 
           //system.debug('oooooooooo');
           //system.debug('nnnnnnnnnnnnnnn'+cont.Name);
           custName = cont.Name;
           String temp = generateMACtable(getMacCodes());
           //System.debug('teeeeemp  '+temp);
           MacTable = temp; 
       
       }
       else{
       
       //Is left blank so that the template preview does not throw an error.
       
       }   
   
   }
      
    public List<BTLB_Customer_Option_Product__c> getMacCodes(){  
        List<BTLB_Customer_Option_Product__c> ls_cop=[select MAC_Code__c,Telephone_Number__c, BTLB_Customer_Option_record__c from BTLB_Customer_Option_Product__c where BTLB_Customer_Option_record__c=:custCotId and Order_Type__c='mac'];
        return ls_cop;          
    }
   
    public String generateMACtable(List<BTLB_Customer_Option_Product__c> lst)  {
       String str = '' ;
       for(BTLB_Customer_Option_Product__c cs : lst)
       {
           str += '<tr><td>'+ cs.Telephone_Number__c +'</td>'+'<td>'+ cs.MAC_Code__c +'</td>'+'</tr>' ;
       }
       str = str.replace('null' , '') ;
       String finalStr = '' ;
       finalStr = '<table border="1" cellpadding="5" cellspacing="0"><td> Telephone Number </td><td> MAC Code</td> ' + str +'</table>' ;
       return finalStr ;
    }
}