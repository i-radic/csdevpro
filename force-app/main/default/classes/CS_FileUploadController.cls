public class CS_FileUploadController{
    
    public String nameFile {get; set;}
    public Blob contentFile {get; set;}
    private String[] lines = new String[]{};
    private String[] mobileNumbers = new List<String>();
    private Attachment csvFile {get; set;}
    static final String COLUMN_NAME = 'MSISDN';

    public CS_FileUploadController(){}

    public PageReference Upload(){
    	String configId = ApexPages.currentPage().getParameters().get('configId');
    	String result = '';

	    if(configId != '' || configId != null){
	    	try {
	    		nameFile = blobToString(contentFile, 'ISO-8859-1');
	    		lines = nameFile.split('\n');

	    		String[] header = lines[0].split(',');
	    		Integer index = header.indexOf(COLUMN_NAME);

		    	if(index > -1){
		    		for(Integer i = 1; i < lines.size(); i++){
		    			String[] line = lines[i].split(',');
		    			mobileNumbers.add(line[index]);
		    		}

		    		csvFile = new Attachment();
		    		csvFile.Name = COLUMN_NAME;
		    		csvFile.ParentId = configId;
		    		csvFile.Body = Blob.valueOf(JSON.serialize(mobileNumbers));
		    		insert csvFile;
		    		result = '&success=true';
		    	}
	    	}
	    	catch(Exception e){
	    		ApexPages.addMessages(e);
	    		result = '&success=false';
	    	}
	    }

	    PageReference ref = new PageReference('/apex/CS_FileUpload?configId=' + configId + result);
    	return ref.setRedirect(true);
    }

    public static String blobToString(Blob input, String inCharset){
        String hex = EncodingUtil.convertToHex(input);
        System.assertEquals(0, hex.length() & 1);
        final Integer bytesCount = hex.length() >> 1;
        String[] bytes = new String[bytesCount];
        for(Integer i = 0; i < bytesCount; ++i)
            bytes[i] =  hex.mid(i << 1, 2);
        return EncodingUtil.urlDecode('%' + String.join(bytes, '%'), inCharset);
    }  
}