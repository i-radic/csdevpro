@IsTest
public class CS_OpportunityTriggerDelegateTest  {
	private static Account acc;
	private static Opportunity opp;
	private static cscfga__Product_Basket__c basket;
	private static OLI_Sync__c oliSetting;
    private static CS_Solution_Calculations__c csSolCalc;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
		oliSetting = CS_TestDataFactory.generateOLISync(true);
        csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
	}
	
	@IsTest
	public static void testPrepareBefore() {
		createTestData();

		basket.csbb__Synchronised_With_Opportunity__c = true;
		UPDATE basket;
		Test.startTest();
		CS_OpportunityTriggerDelegate controller = new CS_OpportunityTriggerDelegate();
		controller.prepareBefore();
		Test.stopTest();

		System.assertNotEquals(null, controller);
		System.assertNotEquals(null, controller.closedWonOpportunities);
	}

	@IsTest
	public static void testAfterUpdate() {
		createTestData();

		opp.StageName = 'Won';
		UPDATE opp;

		Opportunity opp2 = CS_TestDataFactory.generateOpportunity(false, 'Test Opportunity 2', acc);
		opp2.StageName = 'Prospecting';
		INSERT opp2;

		List<Opportunity> oppsList = [SELECT Id, Name, StageName, IsClosed, IsWon FROM Opportunity WHERE Id IN (:opp.Id, :opp2.Id)];

		Test.startTest();
		CS_OpportunityTriggerDelegate controller = new CS_OpportunityTriggerDelegate();
		controller.prepareAfter();
		System.assert(controller.closedWonOpportunities.size() == 0);
        controller.beforeUpdate(oppsList[1], oppsList[0]);
		controller.afterUpdate(oppsList[1], oppsList[0]);
        controller.finish();
		Test.stopTest();

		System.assert(controller.closedWonOpportunities.size() > 0);
	}
}