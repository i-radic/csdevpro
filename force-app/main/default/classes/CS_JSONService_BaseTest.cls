@IsTest
public class CS_JSONService_BaseTest  {
	private static CS_JSONService_Base jsonServiceBase;
	private static CS_JSON_Schema jsonSchema;
	private static CS_JSON_Schema.CS_JSON_Section jsonSection;
	private static CS_JSON_Schema.CS_JSON_Setting jsonSetting;
	private static CS_JSON_Schema.CS_JSON_Object jsonObject;
	private static Account acc;
	private static Opportunity opp;
	private static cscfga__Product_Basket__c basket;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		jsonSchema = (CS_JSON_Schema) JSON.deserialize(CS_JSONExportTestFactory.getJSON(), CS_JSON_Schema.class);
		jsonSection = jsonSchema.getSection('Company Products');
		jsonSetting = jsonSchema.getSetting('productBasket');
		jsonObject = jsonSetting.getDefinition('BT Mobile Sharer');
		acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
	}

	@IsTest
	public static void testGetValuesWithRecord() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();
		basket = [SELECT Id, Name, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
		
		//create dummy record for getValues method
		Map<String, cscfga__Product_Basket__c> records = new Map<String, cscfga__Product_Basket__c>();
		records.put('cscfga__Product_Basket__c', basket);

		Test.startTest();
		CS_JSONService_Base_Dummy controller = new CS_JSONService_Base_Dummy();
		controller.init(jsonSetting);
		Id testId = (Id)controller.getResult(basket.Id);
		Map<String, Object> valuesCheck = controller.getValues(jsonSetting.definitions.get(0), records.get('cscfga__Product_Basket__c'));
		Test.stopTest();

		System.assertEquals(basket.Name, (String)valuesCheck.get('Basket name')); 

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testGetValuesWithUntypedInstance() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();
		basket = [SELECT Id, Name, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
		
		Map<String, Object> untypedInstance = new Map<String, Object>{ 'Name' => basket.Name, 'Opportunity ID' => basket.cscfga__Opportunity__c };

		Test.startTest();
		CS_JSONService_Base_Dummy controller = new CS_JSONService_Base_Dummy();
		controller.init(jsonSetting);

		Map<String, Object> valuesCheck = controller.getValues(jsonSetting.definitions.get(0), untypedInstance);
		Test.stopTest();

		System.assertEquals(basket.Name, (String)valuesCheck.get('Basket name')); 

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	@IsTest
	public static void testEscapeId() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();

		Test.startTest();
		CS_JSONService_Base_Dummy controller = new CS_JSONService_Base_Dummy();
		controller.init(jsonSetting);
		String testId = controller.escapeId(basket.Id);
		Test.stopTest();

		System.assertEquals('\''+ basket.Id + '\'', testId);

		notriggers.Flag__c = false;
		DELETE notriggers;
	}

	//dummy class that extends CS_JSONService_Base to cover abstract class
	public class CS_JSONService_Base_Dummy extends CS_JSONService_Base {
		public CS_JSONService_Base_Dummy() {
			super();
		}

		public override Object getResult(Id id) {
			return id;
		}
	}
}