global class LeaseDeskUtil {
    /******************************************************************************************************************
     Name:  LeaseDeskUtil.class()
     Copyright © 2012  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    Utility class for LeaseDesk development
                                                           
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR                 DATE              DETAIL                           FEATURES
    1.0 -    Rita Opoku-Serebuoh    29/02/2012        INITIAL DEVELOPMENT              Initial Build: 
             Adam Soobroy
    ******************************************************************************************************************/
    
    global static String getLeaseDeskId(Id oppty)
    {
        if(Test_Factory.GetProperty('IsTest') != null){
            return 'TEST';
        }
       /** else{
            Account acct = [SELECT LeaseDeskID__c
                FROM Account
                WHERE  id =: oppty];
        
        return acct.LeaseDeskID__c;
        }*/
        return '';
        
    }
    
    global static Boolean isOutrightSale(Id opptyId)
    {
        List<OpportunityLineItem> lineitem = [SELECT Id,Billing_Cycle__c 
                  FROM OpportunityLineItem 
                 WHERE Opportunity.Id =: opptyId and Billing_Cycle__c IN ('Outright Sale','Lease','Leasing') limit 1];
        
        if (lineitem.size() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    global static Boolean isOutrightSaleCORP(Id opptyId)
    {
        List<OpportunityLineItem> lineitem = [SELECT Id,Billing_Cycle__c 
                  FROM OpportunityLineItem 
                 WHERE Opportunity.Id =: opptyId limit 1];
        
        if (lineitem.size() > 0){
            return true;
        }else{
            return false;
        }
    }
    
    global static List<OpportunityLineItem> getOutrightSaleItems(Id opptyId)
    {
        List<OpportunityLineItem> lineitems = [SELECT Id, Description, Contract_Term__c,Billing_Cycle__c,Qty__c,TotalPrice,UnitPrice,ProdName__c 
                  FROM OpportunityLineItem 
                 WHERE Opportunity.Id =: opptyId and Billing_Cycle__c IN ('Outright Sale','Lease','Leasing') limit 1];
        
        return lineitems;
    }
    
    global static Decimal getTotalOutrightSaleCost(Id opptyId)
    {
        Decimal totalCost = 0;
        
        List<OpportunityLineItem> lineitems = [SELECT Id, TotalPrice 
                  FROM OpportunityLineItem 
                 WHERE Opportunity.Id =: opptyId and Billing_Cycle__c IN ('Outright Sale','Lease','Leasing')];
        
        if (lineitems != null){
            for (OpportunityLineItem oli :lineitems){
                   totalCost = totalCost + oli.TotalPrice;
            }
        }
        
        return totalCost;
    }
    
    global static Decimal getTotalOutrightSaleCostCORP(Id opptyId)
    {
        Decimal totalCost = 0;
        
        List<OpportunityLineItem> lineitems = [SELECT Id, Initial_Cost__c 
                  FROM OpportunityLineItem 
                 WHERE Opportunity.Id =: opptyId];
        
        if (lineitems != null){
            for (OpportunityLineItem oli :lineitems){
                   totalCost = totalCost + oli.Initial_Cost__c;
            }
        }
        
        return totalCost;
    }
    
    global static Account getAccountDetails(Id accountId)
    {
        return [SELECT Id, Name, Postcode__c, SAC_Code__c,LeaseDeskID__c  
                FROM Account
                WHERE  id =: accountId];
    }
    
    global static Opportunity getOpportunityDetails(Id opptyId)
    {
        return [SELECT Id, AccountId, Opportunity_Id__c
              FROM Opportunity 
              WHERE id =: opptyId];
    }
    
    global static List<String> getExistingFunders(Id accountId)
    {
        List<Leasing_History__c> leasingHistory;
        List<String> funders = new List<String>();
        
        leasingHistory = [SELECT Id,Funder__c 
                   FROM Leasing_History__c
                   WHERE Account__c =: accountId];
        
        if (leasingHistory != null){
            for (Leasing_History__c lh : leasingHistory){
                funders.add(lh.Funder__c);
            }
        }
        
        return funders;
    }
    
    global static String[] getFunders(Id accountId)
    {   
        if(Test_Factory.GetProperty('IsTest') == 'Yes'){
            List<String> ls = new List<String>();
            ls.add('Shire');
            return ls;
        }
        else{
            try{
                // query account object to get common name
                Account account = [SELECT Id, BTLB_Common_Name__c FROM Account WHERE Id =: accountId]; 
                
                // query the btlb master object using the common name
                BTLB_Master__c btlb = [SELECT Id,Funders__c FROM BTLB_Master__c WHERE Name =: account.BTLB_Common_Name__c];
                                      
                String[] funders = btlb.Funders__c.Split(';',0);   
                return funders;
            }   
            catch(Exception e){
                String[] noFunders = new String[]{'GE','Shire','CIT','DLL'};
                //String[] noFunders = new String[]{'Shire','CIT','DLL'};
                return noFunders;
            }
        } 
    }
    
    global static Boolean isBTLBAccount(Id accountId)
    {
        if(Test_Factory.GetProperty('IsTest') != null){
            return true;
        }
        else{
            try{
                Account acct = [SELECT Sector__c FROM Account WHERE  id =: accountId];
                if(acct.Sector__c.Contains('Corp') || acct.Sector__c.Contains('Corporate')){
                    return false;
                }
                else{
                    return true;
                }
            }
            catch(Exception e){
                return false;
            }
        }    
    }
    
    global static String getDepartment()
    {
        User user = [SELECT Name, Department
                             FROM User
                            WHERE Id =: UserInfo.getUserId()];
        

        
        return user.Department;
    }
   
    global static List<Leasing_History__c> getLeasingHistoryList(String accountId)
    {
        List<Leasing_History__c> leasingHistory = [SELECT Id, Account__c, AgreementID__c, LeaseDeskAgreementId__c, Credit_limit__c, Opportunity__c,Funder_URL__c , clearance__c, LE_Code__c, Lease_Desk_Company_Name__c, CUG_Id__c, RBO__c, 
             End_Date__c, Status__c, Funder__c, PO_Number__c, AX_reference__c, Monthly_Payment__c, Original_Term__c, Periods_Remaining__c, 
             Settlement__c, Residual_Value__c, Product__c, Lease_Desk_Url__c
              FROM Leasing_History__c 
              WHERE Account__c =: accountId];
        
        return leasingHistory;      
    }
    
    global static Leasing_History__c getLeasingHistory(Id leasingHistoryId)
    {
        
        if(Test_Factory.GetProperty('IsTest') != null){
            Leasing_History__c lh = new Leasing_History__c();
            return lh;
        }else{
        return [SELECT Id, Account__c, AgreementID__c, Credit_limit__c, Opportunity__c,Funder_URL__c , clearance__c, LE_Code__c, Lease_Desk_Company_Name__c, CUG_Id__c, RBO__c, 
             End_Date__c, Status__c, Funder__c, PO_Number__c, AX_reference__c, Monthly_Payment__c, Original_Term__c, Periods_Remaining__c, 
             Settlement__c, Residual_Value__c, Product__c, Lease_Desk_Url__c
              FROM Leasing_History__c 
              WHERE id =: leasingHistoryId];    
        }
    }
    
    global static User getUserDetails()
    {
        return [SELECT Leasing_iManage_Username__c, OUC__c, EIN__c
                             FROM User
                            WHERE Id =: UserInfo.getUserId()];  
    }
    
}