public with sharing class CaseContractApprovalExtension {
  public final Artemis_Contract__c aContract;  
  public List<Signatory> signatories = new List<Signatory>();
  public ID OwnerID {get;set;}
  
  // Page refresh?
  public Boolean refreshPage{
    get; set;
  }
  
  // Selected action
  public String action{
    get; set;
  }  
  
  // Comments
  public String comments{
    get; set;
  }

  // Constructor
  public CaseContractApprovalExtension( ApexPages.StandardController aStandardController ){
    
    if(Test.isRunningTest()){
        this.aContract = [Select ID,Name,Opportunity__c,Opportunity__r.AccountId from Artemis_Contract__c Limit 1].get(0); 
    } else {
        this.aContract = (Artemis_Contract__c)aStandardController.getRecord();
    }
    
    OwnerID = [SELECT ID,OwnerID FROM Opportunity WHERE ID = :this.aContract.Opportunity__c].OwnerID;
    this.action = 'Pre-validate and Send';
    this.refreshPage = false; 
  }
  
  // Get subject
  private String subject{
    get{
      // return 'Contract number: ' + this.aContract.Name + ' for opportunity ' + this.aContract.Opportunity__r.Name + ' ' + this.action;          
      return 'Contract number: ' + this.aContract.Name;          
    }
  }
  
  // Get select options for available actions
  public List<SelectOption> actions{
    get {
      List<SelectOption> actions = new List<SelectOption>();
      actions.add( new SelectOption( 'Pre-validate and Send', 'Pre-validate and Send' ) );
      actions.add( new SelectOption( 'Pre-validate and Hold', 'Pre-validate and Hold' ) );
      return actions;
    }
  }
  
  // Get contacts for account
  public List<signatory> getSignatories(){    
    if( signatories.size() <= 0 ){ 
      for(Contact aContact : [SELECT Name FROM Contact WHERE AccountId = :this.aContract.Opportunity__r.AccountId ORDER BY Name] ){
        signatories.add( new signatory( aContact ) );
      }
    }
    return signatories; 
  }
  
  // Get selected signatories
  private List<Contact> getSelectedSignatories(){
    List<Contact> contacts = new List<Contact>();
    for( Signatory aSignatory : signatories ){
      if( aSignatory.selected == true ){
        contacts.add( aSignatory.contact );
      }
    }
    return contacts;
  }
  
  // Get case description
  private String getDescription(){
    // Get selected signatories
    List<Contact> contacts = this.getSelectedSignatories();    
    String requiredSignatories;
    if( contacts.size() == 1 ){
      requiredSignatories = 'Required signatory: ' + contacts[0].Name;
    } else if( contacts.size() == 2 ){
      requiredSignatories = 'Required signatories: ' + contacts[0].Name + ' & ' + contacts[1].Name;
    } else {        
      throw new SignatoryException();
    }      
    
    if( this.comments != null && this.comments.length() > 0 ){
      return requiredSignatories + '.  COMMENTS: ' + this.comments;        
    } else {
      return requiredSignatories;  
    }              
  }
  
  
  
  private Id getQueueId(){
    // Create set of queue names to get ids for
    Set<String> queueNames = new Set<String>();
    queueNames.add('Contracts Validation');
    
    // Create map of queue ids
    Map<String, Id> queueIds = new Map<String, Id>();
    for(Group aQueue : [SELECT Id, Name FROM Group WHERE Name IN :queueNames AND Type = 'Queue'] ){
      queueIds.put( aQueue.Name, aQueue.Id );
    }      
    
    //Get correct queueu id from map
    id queueId = queueIds.get('Contracts Validation') ;
          
    return queueId;      
  }
  

  // Update Artemis Contract Status
  private void updateArtemisContractStatus(){
    
      // update 
      this.aContract.Status__c = 'Sent to Validation';
        
      try {
          update this.aContract;
      } catch ( DmlException e ){
          ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.WARNING, 'The Status for Agreement Contract could not be set to - Sent to Validation' ) );
      }
  }
      
  // Save case
  public void saveCase(){                
    // Get record type
    Id contractApprovalRecordType = [SELECT Id 
      FROM RecordType 
      WHERE SObjectType = 'Case' AND DeveloperName = 'Contract_Validations' AND IsActive = TRUE LIMIT 1].Id;
     
     // Create and insert case
    try{
      Case aCase = new Case( Description = this.getDescription(), 
        Subject = this.subject, 
        Type = 'Pre-Validation', 
        Priority = '3',
        Company__c = this.aContract.Opportunity__r.AccountId,
        Opportunity__c=this.aContract.Opportunity__r.ID,
      //Contract_Category__c='',  
        Pre_Validation__c=this.aContract.ID,
        Contact__c=OwnerID,
        RecordTypeId = contractApprovalRecordType,
        OwnerId = this.getQueueId(), 
        Status = 'Awaiting Allocation',
        AccountId = this.aContract.Opportunity__r.AccountId );
      insert aCase;
      ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO, 'Case saved' ) );
      
      // Update Artemis Contract Status 
      this.updateArtemisContractStatus();
                
      this.refreshPage = true;      
    } catch ( BrandException ex ){
      ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'The opportunity must have a brand identified or be bid managed.  Go back to the opportunity and select a brand or set as bid managed.' ) );
    } catch ( SignatoryException ex ){
      ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, 'You must select at least one signatory and not more than two.  Please review your selection.' ) );      
    }        
  }
  
  // Signatory sub-class
  public class Signatory{
    public Boolean selected{ get; set; }
    public Contact contact{ get; set; }
    
    public signatory( Contact aContact ){
      this.contact = aContact;
      this.selected = false;
    }
  }
  
  // Signatory exception class
  public class SignatoryException extends Exception {}
  
  // Brand exception class
  public class BrandException extends Exception {}    
  
  // Unit tests
/*  
  static testMethod void testRefreshPage(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage );
    insert aOpportunity;
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract; 
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    System.assert( aCaseContractApprovalExtension.refreshPage == false, 'Expected constructor to set to false!' );
    aCaseContractApprovalExtension.refreshPage = true;
    System.assert( aCaseContractApprovalExtension.refreshPage == true, 'Changed to true but returned false!' );    
  }
  
  static testMethod void testAction(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage );
    insert aOpportunity;
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract; 
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    System.assert( aCaseContractApprovalExtension.action == 'Pre-validate and Send', 'Was expecting "Pre-validate and Send" to be set by constructor' );
    aCaseContractApprovalExtension.action = 'TEST';
    System.assert( aCaseContractApprovalExtension.action == 'TEST', 'Was expecting "TEST"' );            
  }
  
  static testMethod void testComments(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage );
    insert aOpportunity;
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract; 
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );    
    aCaseContractApprovalExtension.comments = 'TEST';
    System.assert( aCaseContractApprovalExtension.comments == 'TEST', 'Was expecting "TEST"' );    
  }
  
  static testMethod void testActions(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage );
    insert aOpportunity;
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract; 
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    List<SelectOption> selectOptions = aCaseContractApprovalExtension.actions;
    System.assert( selectOptions.size() == 2, 'Expected 2 actions received ' + selectOptions.size() );
    Map<String, String> selectOptionsMap = new Map<String, String>();
    for( SelectOption aSelectOption : aCaseContractApprovalExtension.actions ){
      selectOptionsMap.put( aSelectOption.getValue(), aSelectOption.getLabel() );
    }
    System.assert( selectOptionsMap.get('Pre-validate and Send') ==  'Pre-validate and Send', 'List of actions did not contain "Pre-validate and Send"' );
    System.assert( selectOptionsMap.get('Pre-validate and Hold') ==  'Pre-validate and Hold', 'List of actions did not contain "Pre-validate and Hold"' );
  }
  
  static testMethod void testGetSignatories(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    Contact aContact = new Contact( AccountId = aAccount.id, FirstName = 'A Test', LastName = 'Contact' );
    insert aContact;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage );
    insert aOpportunity;
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract;
    aArtemisContract.Opportunity__r = aOpportunity;      
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    List<Signatory> singatories = aCaseContractApprovalExtension.getSignatories(); 
    System.assert( singatories.size() == 1, 'Expecting 1 signatory received ' + singatories.size() );
    System.assert( singatories[0].contact.Name == 'A Test Contact', 'Contact name not as expected.  Expecting "A Test Contact" received ' + singatories[0].contact.Name );    
    System.assert( singatories[0].selected == false, 'Signatory selected not set to false' );
  }
  */
/*  
  static testMethod void testSaveCaseOrange(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    Contact aContact = new Contact( AccountId = aAccount.id, FirstName = 'A Test', LastName = 'Contact' );
    insert aContact;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage, Brand__c = 'Orange' );
    insert aOpportunity;    
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract;
    aArtemisContract.Opportunity__r = aOpportunity;      
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    List<Signatory> signatories = aCaseContractApprovalExtension.getSignatories();
    signatories[0].selected = true;
    aCaseContractApprovalExtension.signatories = signatories;
    aCaseContractApprovalExtension.action = 'Pre-validate and Send';
    aCaseContractApprovalExtension.comments = 'Test';
    aCaseContractApprovalExtension.saveCase();
    
    Case aCase = [SELECT Id, Subject, Type, Description, Priority, OwnerId FROM Case WHERE AccountId = :aArtemisContract.Opportunity__r.AccountId];
    System.assert( aCase.description == 'Required signatory: A Test Contact.  COMMENTS: Test', 'Description not as expected' );
    System.assert( aCase.subject == 'Contract number: ' + aArtemisContract.Name, 'Subject not as expected' );
    System.assert( aCase.priority == 'Medium' );
    
    // Get queue
    Id queueId = [SELECT Id FROM Group WHERE Name = 'Orange Contract Pre-validation' AND Type = 'Queue'].Id;
    System.assert( aCase.OwnerId == queueId, 'Queue not set correctly' );
  }
  
  static testMethod void testSaveCaseEE(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    Contact aContact = new Contact( AccountId = aAccount.id, FirstName = 'A Test', LastName = 'Contact' );
    insert aContact;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage, Brand__c = 'EE' );
    insert aOpportunity;    
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract;
    aArtemisContract.Opportunity__r = aOpportunity;      
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    List<Signatory> signatories = aCaseContractApprovalExtension.getSignatories();
    signatories[0].selected = true;
    aCaseContractApprovalExtension.signatories = signatories;
    aCaseContractApprovalExtension.action = 'Pre-validate and Send';
    aCaseContractApprovalExtension.comments = 'Test';
    aCaseContractApprovalExtension.saveCase();
    
    Case aCase = [SELECT Id, Subject, Type, Description, Priority, OwnerId FROM Case WHERE AccountId = :aArtemisContract.Opportunity__r.AccountId];
    System.assert( aCase.description == 'Required signatory: A Test Contact.  COMMENTS: Test', 'Description not as expected' );
    System.assert( aCase.subject == 'Contract number: ' + aArtemisContract.Name, 'Subject not as expected' );
    System.assert( aCase.priority == 'Medium' );
    
    // Get queue
    Id queueId = [SELECT Id FROM Group WHERE Name = 'EE Contract Pre-validation' AND Type = 'Queue'].Id;
    System.assert( aCase.OwnerId == queueId, 'Queue not set correctly' );
  }
  
  
  static testMethod void testSaveCaseTMobile(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    Contact aContact = new Contact( AccountId = aAccount.id, FirstName = 'A Test', LastName = 'Contact' );
    insert aContact;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage, Brand__c = 'T-Mobile' );
    insert aOpportunity;    
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract;
    aArtemisContract.Opportunity__r = aOpportunity;      
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    List<Signatory> signatories = aCaseContractApprovalExtension.getSignatories();
    signatories[0].selected = true;
    aCaseContractApprovalExtension.signatories = signatories;
    aCaseContractApprovalExtension.action = 'Pre-validate and Send';
    aCaseContractApprovalExtension.comments = 'Test';
    aCaseContractApprovalExtension.saveCase();
    
    Case aCase = [SELECT Id, Subject, Type, Description, Priority, OwnerId FROM Case WHERE AccountId = :aArtemisContract.Opportunity__r.AccountId];
    
    // Get queue
    Id queueId = [SELECT Id FROM Group WHERE Name = 'T-Mobile Contract Pre-validation' AND Type = 'Queue'].Id;
    System.assert( aCase.OwnerId == queueId, 'Queue not set correctly' );    
  }
  
  static testMethod void testSaveCaseBidManaged(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    Contact aContact = new Contact( AccountId = aAccount.id, FirstName = 'A Test', LastName = 'Contact' );
    insert aContact;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage, Bid_Managed__c = TRUE );
    insert aOpportunity;    
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract;
    aArtemisContract.Opportunity__r = aOpportunity;      
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    List<Signatory> signatories = aCaseContractApprovalExtension.getSignatories();
    signatories[0].selected = true;
    aCaseContractApprovalExtension.signatories = signatories;
    aCaseContractApprovalExtension.action = 'Pre-validate and Send';
    aCaseContractApprovalExtension.comments = 'Test';
    aCaseContractApprovalExtension.saveCase();
    
    Case aCase = [SELECT Id, Subject, Type, Description, Priority, OwnerId FROM Case WHERE AccountId = :aArtemisContract.Opportunity__r.AccountId];
    
    // Get queue
    Id queueId = [SELECT Id FROM Group WHERE Name = 'Bid Managed Contract Pre-validation' AND Type = 'Queue'].Id;
    System.assert( aCase.OwnerId == queueId, 'Queue not set correctly' );    
  }
*/
/*  
  static testMethod void testSaveCaseSignatoryError(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage, Bid_Managed__c = TRUE );
    insert aOpportunity;    
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract;
    aArtemisContract.Opportunity__r = aOpportunity;      
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );    
    aCaseContractApprovalExtension.action = 'Pre-validate and Send';
    aCaseContractApprovalExtension.comments = 'Test';
    
    // No case should be saved
    aCaseContractApprovalExtension.saveCase();
    
    List<Case> cases = [SELECT Id FROM Case WHERE AccountId = :aAccount.id];
    System.assert( cases.size() == 0, 'Was expecting 0 cases receeived ' + cases.size() );
  }  
  
  static testMethod void testSaveCaseBrandError(){
    Account aAccount = new Account( Name = 'Test Account' );
    insert aAccount;
    
    Contact aContact = new Contact( AccountId = aAccount.id, FirstName = 'A Test', LastName = 'Contact' );
    insert aContact;
    
    String stage = [SELECT MasterLabel FROM OpportunityStage WHERE IsActive = TRUE AND IsClosed = FALSE LIMIT 1].MasterLabel;    
    Opportunity aOpportunity = new Opportunity( AccountId = aAccount.id, Name = 'Test Opportunity', CloseDate = Date.today(), StageName = stage );
    insert aOpportunity;    
    
    Artemis_Contract__c aArtemisContract = new Artemis_Contract__c( Opportunity__c = aOpportunity.id );
    insert aArtemisContract;
    aArtemisContract.Opportunity__r = aOpportunity;      
    
    ApexPages.StandardController aStandardController = new ApexPages.StandardController( aArtemisContract );
    CaseContractApprovalExtension aCaseContractApprovalExtension = new CaseContractApprovalExtension( aStandardController );
    List<Signatory> signatories = aCaseContractApprovalExtension.getSignatories();
    signatories[0].selected = true;
    aCaseContractApprovalExtension.signatories = signatories;
    aCaseContractApprovalExtension.action = 'Pre-validate and Send';
    aCaseContractApprovalExtension.comments = 'Test';
    aCaseContractApprovalExtension.saveCase();
    
    List<Case> cases = [SELECT Id, Subject, Type, Description, Priority, OwnerId FROM Case WHERE AccountId = :aArtemisContract.Opportunity__r.AccountId];
    
    System.assert( cases.size() == 0, 'Expected 0 cases but received ' + cases.size() );
  }  */  
}