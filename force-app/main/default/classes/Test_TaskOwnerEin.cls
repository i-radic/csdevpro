@isTest
private class Test_TaskOwnerEin{

    static testMethod void testTaskCreationAndUpdate(){     
        Task t1 = Test_Factory.CreateTask();
        insert t1;
        
        // Update task
        t1.reminder_days__c = 10;
        update t1;

    }

}