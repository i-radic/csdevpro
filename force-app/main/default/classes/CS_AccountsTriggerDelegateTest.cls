@isTest
private class CS_AccountsTriggerDelegateTest{
    @testSetup static void setupData(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account acc = CS_TestDataFactory.generateAccount(true, 'test account');
        Usage_Profile__c usageProfile = CS_TestDataFactory.generateUsageProfile(true, acc);
        usageProfile.Is_Default_Profile__c = true;
        update usageProfile;
    }
    
    @isTest static void testInsertAndUpdateAccount() {
        Account acc = [SELECT Name FROM Account WHERE Name = 'test account'];
        acc.Name = 'test account new';
        update acc;
        Account accNew =  CS_TestDataFactory.generateAccount(true, 'new account');
        system.assert(accNew.ID != null);
    }
}