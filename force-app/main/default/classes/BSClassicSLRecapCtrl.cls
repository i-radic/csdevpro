public class BSClassicSLRecapCtrl{
    
    public string SalesLogId = System.currentPageReference().getParameters().get('id');
    public string vVerbalRecap = Null;
    public Boolean flag{get;set;}
    public String estimatedSpeed {get;set;}
    public BS_Classic_Sales_log__c bcsl {get;set;}
    public Boolean reasonFlag {get;set;}
    private set<String> proTypes= new set<String>();
 
 public BSClassicSLRecapCtrl(ApexPages.StandardController controller) {
     bcsl = new BS_Classic_Sales_log__c();
     BS_Classic_Sales_log__c  SalesLog =(BS_Classic_Sales_log__c) controller.getRecord();
     bcsl=[Select Id,Name,ROSITA_Ref__c,Order_Number__c,Recap_Text_Advised_no_email__c,Broken_Period_Rental__c,Reason_for_Not_Informed_about_Speeds__c,Estimated_Speed_Confirmation__c,Contact_Email__c,BT_Cloud_Phone_Present__c,Contract_Term__c,Product_Count__c,Recap_Text_Advised__c from BS_Classic_Sales_log__c where Id =: SalesLogId];
     system.debug('original Record==='+bcsl+'  '+bcsl.Estimated_Speed_Confirmation__c+'    '+bcsl.Recap_Text_Advised_no_email__c);
     flag=false;
     for(BS_Classic_SL_Product__c bscslProduct:[SELECT  Product__c, Quantity__c, Friendly_name__c ,Product_Type2__c FROM BS_Classic_SL_Product__c WHERE Sales_Log__c = :SalesLogId])
     {
         //if(bscslProduct.Product_Type2__c == 'Broadband' && (bscslProduct.Product_Type2__c == 'Broadband Re-grade' || bscslProduct.Product_Type2__c == 'Mover'))
         //{
             proTypes.add(bscslProduct.Product_Type2__c);           
         //}
     }
     if(proTypes.contains('Broadband') || (proTypes.contains('Broadband Re-grade') || proTypes.contains('Mover')))
     {
     	flag=true;
     }
     reasonFlag = false;
  }

public void reasonCapture()
{
    system.debug('value===='+bcsl+'  '+bcsl.Estimated_Speed_Confirmation__c+'    '+bcsl.Recap_Text_Advised_no_email__c);
    if(bcsl.Estimated_Speed_Confirmation__c == 'No')
    {
        reasonFlag =true;   
    }
    else
    {
        reasonFlag =false;
    }
 
}
 
public List<BS_Classic_SL_Product__c> getProductList() {
   return [SELECT  Product__c, Quantity__c, Friendly_name__c  FROM BS_Classic_SL_Product__c WHERE Sales_Log__c = :SalesLogId and Friendly_name__c <> ''];
   }
                 
public List<BS_Classic_SL_Product__c> getProductList2() {
   return [SELECT  Product__c, Quantity__c, Friendly_name__c  FROM BS_Classic_SL_Product__c WHERE Sales_Log__c = :SalesLogId and Friendly_name__c = ''];
   
}

    public pagereference save(){
        update bcsl;        
        return new pagereference('/'+bcsl.id);
        //return null;
    }
    



}