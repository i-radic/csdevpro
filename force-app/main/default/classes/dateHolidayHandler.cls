public with sharing class dateHolidayHandler {
/**********************************************************************
	 Name:  dateHolidayHandler.class
	 Copyright © 2010  BT.
	 ======================================================
	======================================================
	Purpose:                                                            
	-------       
	
	This class has three methods :
	
	1. adddayswithholidays:
	Used to add days to an input date and return a calculated
	date that takes into account business working days and national holidays in calculating 
	a return date.
	
	
	2.getbusinessdays
	Takes two dates and returns the number of business days (not a weekend or a holiday)
	
	3.isdaybusinessday
	Takes a date and if it is a weekend or a holiday, returns the next business day.
	
	
	These methods are called from the two Sales Journey triggers (tgrCreateSalesJourney.trigger) and
	(tgrRecalcICTDueDates.trigger).
	
	
	======================================================
	======================================================
	History                                                            
	-------                                                            
	VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
	1.0 -    Greg Scarrott    22/02/2010        INITIAL DEVELOPMENT       				Initial Build: 
	
	***********************************************************************/
	public static Boolean bholidayrun = false;
	public static list<holiday> holidaylist = new list<holiday>();
	
	
	
	
	public static date adddayswithholidays(date indate,integer noofdays,boolean reverse){ 
		//Create a set for the holiday dates
		set<date> holidayset = new set<date>();
		boolean bstop = false;
		
		//Get the holiday dates
		If (!bholidayrun){
			holidaylist=[select activitydate from holiday];
			bholidayrun =true;
		}
		
		//Setup the holiday set
		If (holidaylist !=null){
			for (holiday h:holidaylist){
				holidayset.add(h.activitydate);
			}
		} 
		for(Integer i=0;i < noofdays;i++){
				//If this date is in the holiday list
				//Add a day. Repeat until not a holiday.
				bstop =false;
				If(holidayset !=null){
					while(bstop ==false){
						if(holidayset.contains(indate)){
							//Add a day
							indate = indate.adddays(1);
						}else{
							bstop = true;
						}
					}
				}
				//Add the next day to the indate
				If(reverse){
					indate =indate.adddays(-1);
				}else{
					indate =indate.adddays(1);
				}
				
				Datetime dt = datetime.newInstance(indate.year(), indate.month(),indate.day());
				String weekday = dt.format('E'); 
				
				if (weekday == 'Sat'){
					system.debug('found a saturday' + indate);
					If(reverse){
						indate =indate.adddays(-1);
					}else{
						indate =indate.adddays(2);
					}
				}
				if (weekday =='Sun'){
					system.debug('found a sunday' + indate);
					If(reverse){
						indate =indate.adddays(-2);
					}else{
						indate =indate.adddays(1);
					}
				}
				
				//Check for holidays again before sending back
				bstop =false;
				If(holidayset !=null){
					while(bstop ==false){
						if(holidayset.contains(indate)){
							//Add a day
							If(reverse){
								indate =indate.adddays(-1);
							}else{
								indate =indate.adddays(1);
							}
							
						}else{
							bstop = true;
						}
					}
				}
		}
		return indate;
	}
	
	public static integer getbusinessdays(date fromdate,date todate){ 
		Integer daycount = 0;
		boolean bstop = false;
		boolean bholdstop = false;
		
		//Get the holiday dates
		If (!bholidayrun){
			holidaylist=[select activitydate from holiday];
			bholidayrun =true;
		}
		
		set<date> holidayset = new set<date>();
		
		//Setup the holiday set
		If (holidaylist !=null){
			for (holiday h:holidaylist){
				holidayset.add(h.activitydate);
			}
		}
		
		If (todate > fromdate){
			Datetime dt;
			while(bstop==false){
				dt = datetime.newInstance(fromdate.year(), fromdate.month(),fromdate.day());
				String weekday = dt.format('E'); 
				
				if (weekday == 'Sat'){
					fromdate = fromdate.adddays(2);
				}else if (weekday =='Sun'){
						fromdate = fromdate.adddays(1);
						
				}else if (holidayset !=null && holidayset.contains(fromdate)){
							fromdate = fromdate.adddays(1);	
				}else{
					//Its a business day, increase the counter
					daycount++;
					fromdate = fromdate.adddays(1);
				}
				
				if(fromdate >= todate){
					bstop=true;
				}
			}
			
		}
		return daycount;
	}
	
	

	
	public static date isdaybusinessday(date thisdate,boolean reverse){ 
		boolean bstop = false;		
		boolean bchanged=false;
				//Get the holiday dates
				If (!bholidayrun){
					holidaylist=[select activitydate from holiday];
					bholidayrun =true;
				}
				set<date> holidayset = new set<date>();
				
				//Setup the holiday set
				If (holidaylist !=null){
					for (holiday h:holidaylist){
						holidayset.add(h.activitydate);
					}
				}
				
				while(bstop ==false){
					Datetime dt = datetime.newInstance(thisdate.year(), thisdate.month(),thisdate.day());
				
					//Is it a holiday ?
					If(holidayset !=null && holidayset.contains(thisdate)){ 
						//Add a day
						If(reverse){
							thisdate =thisdate.adddays(-1);
						}else{
							thisdate =thisdate.adddays(1);
						}
						bchanged = true;	
					}else{
						//Is it a weekend ?
						String weekday = dt.format('E'); 
						if (weekday == 'Sat'){
							If(reverse){
								thisdate =thisdate.adddays(-1);
							}else{
								thisdate =thisdate.adddays(2);
							}
							bchanged=true;
						}else if(weekday =='Sun'){
							If(reverse){
								thisdate =thisdate.adddays(-2);
							}else{
								thisdate =thisdate.adddays(1);
							}
							bchanged=true;					
						}
					}
					if(bchanged==false){
						bstop=true;
					}else{
						bchanged=false;
					}
				}	
				return thisdate;	
		}
		
}