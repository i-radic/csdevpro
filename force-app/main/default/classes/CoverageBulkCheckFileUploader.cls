public class CoverageBulkCheckFileUploader
{
    public string fileName{get;set;}
     public string attachmentName{get;set;}
    public Blob fileContent{get;set;}
    String[] filelines = new String[]{};
    public String Response {get; set;}
    Coverage_Check__c coverage;
    List<Coverage_Check_Site__c> ccsitestoupload;
    List<Coverage_Check_Site__c> ccsitestoupload2;
     List<Coverage_Check_Site__c> ccsitestoinsert;
    public static boolean isApexTest = false;
    public static String testResponse='';
    public Boolean refreshPage {get; set;}
    public Boolean showContinue{get; set;}
    public Id recordId  {    get;set;    }  
    
     public String requestStatus { get; set; }
     public String requestMore { get; set; }
     public Integer requestNumber { get; set; }
     public Integer requestCurrent { get; set; }
     public Integer startFilelines{ get; set; }
    
    public CoverageBulkCheckFileUploader(ApexPages.StandardController stdController) {
    
        recordId  = stdController.getRecord().Id;              
        coverage= [select id  from Coverage_Check__c
                       where id = :ApexPages.currentPage().getParameters().get('id')];
        requestStatus = '';
        requestMore = '';
         requestCurrent = 0;  
        startFilelines =1;
    }
    public PageReference uploadInputFile(String attachmentName)  
    {  
        PageReference pr;  
        if(fileName!= null && fileContent!= null)  
        {  
              Attachment myAttachment  = new Attachment();  
              myAttachment.Body = fileContent;  
              myAttachment.Name = attachmentName;  
              myAttachment.ParentId = recordId  ;  
              insert myAttachment;               
              pr = new PageReference('/' + myAttachment.Id);               
              update coverage;              
        }  
        return null;  
    }      
    public Pagereference ReadFile()
    {  
        attachmentName= fileName; 
        try 
        {
            fileName=fileContent.toString();
            filelines = fileName.split('\n');
        }
        catch(Exception e)
        {
             ApexPages.Message errorMsg= new ApexPages.Message(ApexPages.Severity.ERROR,'Error reading input file.Please ensure you have attached a CSV file before trying to upload again.');
             ApexPages.addMessage(errorMsg);            
        }
        ccsitestoupload = new List<Coverage_Check_Site__c>();
        ccsitestoinsert = new List<Coverage_Check_Site__c>();
        System.debug('Number of rows in the csv file = ' + filelines.size());
        if( filelines.size() <= 100)
        {
            for (Integer i=1;i<filelines.size();i++)
            {
                String[] inputvalues = new String[]{};
                inputvalues = filelines[i].split(',');            
                Coverage_Check_Site__c ccSite = new Coverage_Check_Site__c();
                ccSite.Coverage_Check__c = coverage.id;
                ccSite.Post_Code__c = inputvalues[0];                 
                System.debug('Number_of_Handsets_at_this_location__c='+inputvalues[2]); 
                System.debug('Number_of_Handsets_at_this_location__c='+inputvalues[2].indexOf('\r'));
                if(inputvalues[2].trim().length()!=0)
                    ccSite.Number_of_Handsets_at_this_location__c=inputvalues[2];
                else
                    ccSite.Number_of_Handsets_at_this_location__c ='0';
                System.debug('Number_of_Handsets_at_this_location__c is='+ ccSite.Number_of_Handsets_at_this_location__c); 
                //getandUpdateCoverageInformationRequest(ccSite,inputvalues[0]);
                getandUpdateCoverageInformationRequestSPG(ccSite,inputvalues[0]);
                if (inputvalues[1].containsIgnoreCase('Commercial'))
                {
                    ccSite.Commercial__c =true;                  
                } 
                ccsitestoupload.add(ccSite);            
            }
            try
            {
                insert ccsitestoupload;   
                System.debug('ccsitestoupload complete'+ccsitestoupload.size());
                System.debug('ccsitestoupload ='+ccsitestoupload.size());                  
                uploadInputFile(attachmentName);
                uploadResultsFile(ccsitestoupload,attachmentName);
                refreshPage=true;
            }
            catch (Exception e)
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template or try again later');
                ApexPages.addMessage(errormsg);
            } 
        }
        else
        {
           //ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Please ensure the input file does not contain more than 100 postocodes.If there more than 100 postcodes please split them into multiples of 100 and upload each file seperately');
           //ApexPages.addMessage(errormsg);
           //New code
           requestNumber = filelines.size()-1;
           handleMorethanHundredPostcodes(); 
       }   
       return null;
    }
    public void handleMorethanHundredPostcodes()
    {
        ccsitestoupload2 = new List<Coverage_Check_Site__c>();
        System.debug('handleMorethanHundredPostcodes'); 
        System.debug('filelines'+filelines.size()); 
        System.debug('requestMore '+requestMore ); 
        System.debug('requestCurrent '+requestCurrent ); 
        System.debug('requestNumber'+requestNumber);        
        System.debug('startFilelines'+startFilelines); 
        if (requestCurrent <= requestNumber) 
        {   
           for (Integer startPoint = requestCurrent ; ((startFilelines < filelines.size()) && ((requestCurrent <= requestNumber)) && ((requestCurrent - startPoint) < 100)); requestCurrent++)
           {
                String[] inputvalues = new String[]{};
                inputvalues = filelines[startFilelines].split(',');            
                Coverage_Check_Site__c ccSite = new Coverage_Check_Site__c();
                ccSite.Coverage_Check__c = coverage.id;
                ccSite.Post_Code__c = inputvalues[0]; 
                if (inputvalues[1].contains('Commercial'))
                {
                    ccSite.Commercial__c =true;
                }  
                if(inputvalues[2].trim().length()!=0)
                    ccSite.Number_of_Handsets_at_this_location__c=inputvalues[2];
                else
                    ccSite.Number_of_Handsets_at_this_location__c ='0';
                //getandUpdateCoverageInformationRequest(ccSite,inputvalues[0]);
                getandUpdateCoverageInformationRequestSPG(ccSite,inputvalues[0]);
                ccsitestoupload.add(ccSite); 
                startFilelines++;    
           }
           requestStatus = requestCurrent + ' out of ' + requestNumber + ' processed.';
        }
        if (requestCurrent > = requestNumber)
        {
           requestMore = 'false';   
           System.debug('ccsitestoupload'+ccsitestoupload.size());  
           try
            {
                insert ccsitestoupload;    
                System.debug('attachmentName='+attachmentName);                                    
                uploadInputFile(attachmentName);
                uploadResultsFile(ccsitestoupload,attachmentName);
                refreshPage=true;
            }
            catch (Exception e)
            {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template or try again later');
                ApexPages.addMessage(errormsg);
            }                                 
        }
        else
        {
           requestMore = 'true';
           showContinue = true;   
        } 
    }
    public PageReference UploadResultsFile(List<Coverage_Check_Site__c> ccsites,String attachmentName)  
    {  
            PageReference pr;  
            System.debug('========UploadResultsFile ====');
            System.debug('ccsites.size()='+ccsites.size());
            attachmentName =attachmentName.substring(0,attachmentName.indexOf('.csv'))+'Results.csv';
            if(fileName!= null && fileContent!= null)  
            { 
              String csvStr='PostCode, Residential/Commercial, No. of Connections,Predicted 2G,Predicted 3G,Predicted 4G';
              List<Coverage_Check_Site__c> insertedCCsites = [SELECT id,post_code__c,Commercial__c ,Number_of_Handsets_at_this_location__c,Coverage_2G__c,Coverage_3G__c,Coverage_4G__c,Coverage_2G_Description__c,Coverage_3G_Description__c,Coverage_4G_Description__c from Coverage_Check_Site__c WHERE id IN :ccsites];
              System.debug('========insertedCCsites  ===='+insertedCCsites.size());
              for (Coverage_Check_Site__c ccsite: insertedCCsites )
              {
               
                 String buildType='Residential';
                 if(ccSite.Commercial__c ==true)  buildType='Commercial' ;  
                 if  ( ccSite.Number_of_Handsets_at_this_location__c !='0'     && ccSite.Number_of_Handsets_at_this_location__c.indexOf('\r') !=-1 )
                   ccSite.Number_of_Handsets_at_this_location__c =  ccSite.Number_of_Handsets_at_this_location__c.substring(0,ccSite.Number_of_Handsets_at_this_location__c.indexOf('\r'))  ;          
                   csvStr+='\n' + ccsite.post_code__c + ',' + buildType + ','+  ccSite.Number_of_Handsets_at_this_location__c +',' + ccsite.Coverage_2G__c+' ('+ccsite.Coverage_2G_Description__c +')'+ ',' +  ccsite.Coverage_3G__c + ' ('+ccsite.Coverage_3G_Description__c+')'+ ',' + ccsite.Coverage_4G__c + ' ('+ccsite.Coverage_4G_Description__c+')';
              } 
              Attachment myAttachment  = new Attachment();  
              myAttachment.Body = Blob.valueOf(csvStr);
              myAttachment.Name = attachmentName;  
              myAttachment.ParentId = recordId  ;  
              insert myAttachment;  
             
              pr = new PageReference('/' + myAttachment.Id);               
              update coverage;
              
            }  
          
            return null;  
    }      
    public void getandUpdateCoverageInformationRequestSPG(Coverage_Check_Site__c ccs,String postCode) 
    {
        List<CoverageChecker__c> cc = CoverageChecker__c.getall().values();
        System.HttpRequest request = new System.HttpRequest();
        request.setMethod('GET');     
        try
        {
            if (postCode  != null) 
            {
                System.debug('postCode  ='+ postCode );
                System.debug('CoverageChecker__c='+ cc.size());
                String coverageCheckURL  = cc[0].CoverageCheckerSPGEndpointURL__c;
                //http://coverage.ee.co.uk/rest/v1/coverage/locations/
                String endpoint= coverageCheckURL   + EncodingUtil.urlEncode(postCode, 'UTF-8');          
                request.setEndpoint(endpoint);
                System.debug('EndPoint===='+coverageCheckURL );
                String guidString = generateGuid();
                request.setHeader('X-EE-EL-Tracking-Header', 'EE-SALESFORCE-CRM-'+guidString );
                if(!test.isRunningTest())  // shiva added this to skip the certificate on 08-02-2018
                request.setClientCertificateName('Salesforce_SPG_CSR');
                System.HttpResponse response ;               
                response = new System.Http().send(request);
                this.Response = response.getBody();
                System.debug('Response from coverage checker ===='+ this.Response);
                System.debug('X-EE-EL-Tracking-Header===='+ response.getHeader('X-EE-EL-Tracking-Header'));
                CoverageBulkCheckerSPGJSONParser parsedData =new CoverageBulkCheckerSPGJSONParser ();
                parsedData = CoverageBulkCheckerSPGJSONParser.parse(this.Response);
                System.debug('parsedData===='+ parsedData);        
                if ((parsedData!= null) && (parsedData.CoverageResource != null) && (parsedData.CoverageResource.Locations != null) && (parsedData.CoverageResource.Locations.Coverage.size() == 4))
                {
                   CoverageBulkCheckerSPGJSONParser.Coverage[] coverage= parsedData.CoverageResource.Locations.Coverage;
                   for(Integer i = 0; i < coverage.size(); i++)
                   {
                       System.debug('Cov type ='+coverage[i].type+'  Cov Str='+coverage[i].Strength);
                       if( coverage[i].type.equalsIgnoreCase('2G') )
                       {
                          ccs.Coverage_2G__c = String.valueOf(coverage[i].Strength);                                                     
                       }   
                       else if ( coverage[i].type.equalsIgnoreCase('3G') )
                       {
                           ccs.Coverage_3G__c = String.valueOf(coverage[i].Strength);                                          
                       }
                       else if ( coverage[i].type.equalsIgnoreCase('4G') )
                       {
                          ccs.Coverage_4G__c = String.valueOf(coverage[i].Strength);                                           
                       }      
                    }
                       
               }
               else
               {
                   ccs.Coverage_2G__c = '0';
                   ccs.Coverage_3G__c = '0';
                   ccs.Coverage_4G__c = '0';         
               }                  
             }
             else
             {
                 ccs.Coverage_2G__c = '0';
                 ccs.Coverage_3G__c = '0';
                 ccs.Coverage_4G__c = '0';                        
             }
         }
         catch(System.JSONException e)
         {
              System.debug('JSONException============'+e);
              ccs.Coverage_2G__c = '0';
              ccs.Coverage_3G__c = '0';
              ccs.Coverage_4G__c = '0';         
         }
    }    
    /*public void getandUpdateCoverageInformationRequest(Coverage_Check_Site__c ccs,String postCode) 
    {
       List<CoverageChecker__c> cc = CoverageChecker__c.getall().values();       
       System.HttpRequest request = new System.HttpRequest();
       request.setMethod('GET');         
       try
       {
             if (postCode  != null) 
             {
                System.debug('postCode  ='+ postCode );
                System.debug('CoverageChecker__c='+ cc.size());
                String coverageCheckURL  =cc[0].CoverageCheckerEndpointURL__c;
                //http://coverage.ee.co.uk/rest/v1/coverage/locations/
                String endpoint= coverageCheckURL   + EncodingUtil.urlEncode(postCode, 'UTF-8');          
                request.setEndpoint(endpoint);
                System.debug('EndPoint===='+coverageCheckURL );
                String guidString = generateGuid();
                request.setHeader('X-EE-EL-Tracking-Header','EE-SALESFORCE-CRM-'+guidString );
                System.HttpResponse response ;
                response = new System.Http().send(request);
                this.Response = response.getBody();
                System.debug('Response from the coverage checker='+ this.Response);
                System.debug('X-EE-EL-Tracking-Header===='+ response.getHeader('X-EE-EL-Tracking-Header'));                
                CoverageIndicatorJSONParser  parsedData =new CoverageIndicatorJSONParser();
                parsedData = CoverageIndicatorJSONParser.parse(this.Response);
                System.debug('parsedData.Locations.size===='+ parsedData.Locations.size());
                if (parsedData.Locations.size() == 1)
                {
                   CoverageIndicatorJSONParser.Locations loc = parsedData.locations[0];
                   for(Integer i = 0; i < loc.coverage.size(); i++)
                   {
                       System.debug('Cov type ='+loc.coverage[i].type+'  Cov Str='+loc.coverage[i].Strength);
                       if( loc.coverage[i].type.equals('2G') )
                       {
                          ccs.Coverage_2G__c = loc.coverage[i].Strength; 
                       }   
                       else if ( loc.coverage[i].type.equals('3G') )
                       {
                           ccs.Coverage_3G__c = loc.coverage[i].Strength;                                          
                       }
                       else if ( loc.coverage[i].type.equals('4G') )
                       {
                          ccs.Coverage_4G__c = loc.coverage[i].Strength;                                           
                       }      
                    }                       
               }
               else
               {
                   ccs.Coverage_2G__c = '0';
                   ccs.Coverage_3G__c = '0';
                   ccs.Coverage_4G__c = '0';          
               }
            }
            else
            {
                ccs.Coverage_2G__c = '0';
                ccs.Coverage_3G__c = '0';
                ccs.Coverage_4G__c = '0';
            }
        }
        catch(System.JSONException e)
        {
            System.debug('JSONException============'+e);
            ccs.Coverage_2G__c = '0';
            ccs.Coverage_3G__c = '0';
            ccs.Coverage_4G__c = '0';              
        }
    }*/
    public String generateGuid()
    {   
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String guid = h.SubString(0,8)+ '-' + h.SubString(8,12) + '-' + h.SubString(12,16) + '-' + h.SubString(16,20) + '-' + h.substring(20);
        return guid;
    }
}