public without sharing class CS_BasketViewerCtrl {
    public transient Map<Id, List<Product>> spaceList { get; set; }
    public transient Map<Id, cscfga__Product_Configuration__c> pcRootList { get; set; }
    public transient Map<Id, String> definitionMap { get; set; }
    public transient List<Id> orderedDefinitions { get; set; }
    public Id basketId { get; set; }
    public Id currProduct { get; set; }
    public Boolean isSMEUser {get; set;}
    public cscfga__Product_Basket__c currBasket { get; set; }
    public string currProductName { get; set; }
    public Decimal currProductTotalRevenue { get; set; }
    public Decimal currProductGrossMargin { get; set; }
    public Decimal currProductTenure { get; set; }
    public Decimal currProductFCC { get; set; }
    public Boolean currOpportunityClosed { get; set; }
    public Boolean basketApprovalInProgress { get; set; }

    public transient Map<Id, String> subDefinitionMap { get; set; }
    public transient List<Id> orderedSubDefinitions { get; set; }
    public transient Map<Id, List<Product>> subProducts { get; set; }

    public CS_BasketViewerCtrl(ApexPages.StandardController controller) {
        try {
            basketId = controller.getId();
            loadData();
            
            Id profileId=userinfo.getProfileId();
            String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
            system.debug('ProfileName'+profileName);
            if(ProfileName == 'SME BTLB Sales' || ProfileName == 'SME System Admin' ||
               ProfileName.contains('BTLB') || ProfileName == 'BPS Partner User' || ProfileName == 'BPS Internal User'){
                isSMEUser = true;
            }else{
                isSMEUser = false;
            }
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }
    }

    public boolean getBasketApprovalInProgress() {
        return this.basketApprovalInProgress;
    }
    
    public PageReference init() {
        if(isSMEUser) {
        	List<cscfga__Product_Configuration__c> configs = [Select Id, cscfga__Product_Definition__r.cscfga__runtime_version__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId];
        	for(cscfga__Product_Configuration__c cfg : configs) {
        		if(cfg.cscfga__Product_Definition__r.cscfga__runtime_version__c == 'v1') return null;
        	}
        	
            return new PageReference('/apex/csbb__CSBasketRedirect?id=' + basketId);
        }
        else {
            return new PageReference('/apex/csbb__CSBasketRedirect?id=' + basketId);
        }
    }

    public void loadData() {
        List<cscfga__Product_Basket__c> basketList = [
            SELECT
                Id,
                Approval_Status__c,
                cscfga__Basket_Status__c,
                cscfga__Opportunity__c,
                cscfga__Opportunity__r.IsClosed,
                Cloning_in_Progress__c
            FROM cscfga__Product_Basket__c
            WHERE Id = :basketId
        ];

        if (basketList.isEmpty())
            throw new CS_Exception('Basket not found!');

        currBasket = basketList[0];

        if (currBasket.Cloning_in_Progress__c)
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Cloning in progress. Please refresh page after one minute.'));

        currOpportunityClosed = currBasket.cscfga__Opportunity__r.IsClosed;

        Map<Id, cscfga__Product_Configuration__c> pcMap = new Map<Id, cscfga__Product_Configuration__c>([
            SELECT
                Name,
                cscfga__Configuration_Status__c,
                cscfga__Product_Definition__c,
                cscfga__Product_Definition__r.Name,
                cscfga__Root_Configuration__c,
                cscfga__Validation_Message__c,
                FCC_Airtime__c,
                FCC_Hardware__c,
                Gross_Margin__c,
                NPV_GOM_per_annum__c,
                Number_of_users__c,
                Percentage_Secured__c,
                Tenure__c,
                Total_Revenue__c
            FROM cscfga__Product_Configuration__c
            WHERE cscfga__Product_Basket__c = :basketId
        ]);

        Map<Id, List<cscfga__Product_Configuration__c>> pcMapList = new Map<Id, List<cscfga__Product_Configuration__c>>();

        pcRootList = new Map<Id, cscfga__Product_Configuration__c>();
        List<cscfga__Product_Configuration__c> tmpList;
        Id tmpId;

        for (cscfga__Product_Configuration__c pc : pcMap.values()) {
            tmpId = pc.cscfga__Root_Configuration__c == null ? pc.Id : pc.cscfga__Root_Configuration__c;

            tmpList = pcMapList.get(tmpId);
            if (tmpList == null)
                tmpList = new List<cscfga__Product_Configuration__c>();

            tmpList.add(pc);

            try {
                pcMapList.put(tmpId, tmpList);
            }
            catch (Exception ex) {
            }

            if (pc.cscfga__Root_Configuration__c == null)
                pcRootList.put(pc.Id, pc);
        }

        spaceList = new Map<Id, List<Product>>();
        definitionMap = new Map<Id, String>();

        List<Product> tmpProds;

        for (cscfga__Product_Configuration__c root : pcRootList.values()) {
            definitionMap.put(root.cscfga__Product_Definition__c, root.cscfga__Product_Definition__r.Name);
            tmpProds = spaceList.get(root.cscfga__Product_Definition__c);

            if (tmpProds == null)
                tmpProds = new List<Product>();

            tmpProds.add(new Product(root));

            try {
                spaceList.put(root.cscfga__Product_Definition__c, tmpProds);
            }
            catch (Exception ex) {
            }
        }

        orderByProductDefinition('main');

        List<ProcessInstance> processInstances = [
            SELECT Id
            FROM ProcessInstance
            WHERE
                TargetObjectId = :basketId AND
                Status = 'Pending'
        ];

        basketApprovalInProgress = (!processInstances.isEmpty() || currBasket.Approval_Status__c == 'Approved');
    }

    public PageReference loadSubProducts() {
        try {
            loadData();

            if (pcRootList.containsKey(currProduct)) {
                currProductName = pcRootList.get(currProduct).Name; // + ' - ' + pcRootList.get(currProduct).Product_Name__c;
                currProductTotalRevenue = pcRootList.get(currProduct).Total_Revenue__c;
                currProductGrossMargin = pcRootList.get(currProduct).Gross_Margin__c;
                currProductTenure = pcRootList.get(currProduct).Tenure__c;
            }

            List<cscfga__Product_Configuration__c> pcList = [
                SELECT
                    Name,
                    cscfga__Root_Configuration__c,
                    cscfga__Parent_Configuration__c,
                    cscfga__Product_Definition__c,
                    FCC_Airtime__c, FCC_Hardware__c,
                    cscfga__Configuration_Status__c,
                    cscfga__Product_Definition__r.Name,
                    cscfga__Validation_Message__c,
                    Gross_Margin__c,
                    Number_of_users__c,
                    Tenure__c,
                    Total_Revenue__c,
                    Percentage_Secured__c,
                    NPV_GOM_per_annum__c
                FROM cscfga__Product_Configuration__c
                WHERE
                    cscfga__Root_Configuration__c = :currProduct AND
                    cscfga__Product_Basket__c = :basketId
            ];

            Map<Id, cscfga__Product_Configuration__c> pcMap = new Map<Id, cscfga__Product_Configuration__c>();

            for (cscfga__Product_Configuration__c pc : pcList)
                pcMap.put(pc.Id, pc);

            subDefinitionMap = new Map<Id, String>();
            subProducts = new Map<Id, List<Product>>();
            Map<Id, Product> tmpSubProducts = new Map<Id, Product>();
            List<Product> tmpProds;
            Product subProd;

            for (cscfga__Product_Configuration__c pc : pcList) {
                subProd = new Product(pc);

                if (pc.cscfga__Root_Configuration__c == pc.cscfga__Parent_Configuration__c) {
                    tmpProds = subProducts.get(pc.cscfga__Product_Definition__c);

                    if (tmpProds == null)
                        tmpProds = new List<Product>();

                    tmpProds.add(subProd);
                    subProducts.put(pc.cscfga__Product_Definition__c, tmpProds);
                    tmpSubProducts.put(pc.Id, subProd);

                    if (!subDefinitionMap.containsKey(pc.cscfga__Product_Definition__c))
                        subDefinitionMap.put(pc.cscfga__Product_Definition__c, pc.cscfga__Product_Definition__r.Name);
                }
                else if (tmpSubProducts.containsKey(pc.cscfga__Parent_Configuration__c)) {
                    tmpSubProducts.get(pc.cscfga__Parent_Configuration__c).addSubProduct(subProd);
                }
            }

            orderByProductDefinition('sub');
        }
        catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }

        return null;
    }

    public PageReference cancelSubProducts() {
        loadData();
        subProducts = null;
        currProduct = null;

        return null;
    }

    public PageReference delProduct() {
        try {
            List<cscfga__Product_Configuration__c> pcList = [
                SELECT Id, cscfga__Product_Basket__c
                FROM cscfga__Product_Configuration__c
                WHERE
                    Id = :currProduct OR
                    cscfga__root_configuration__c = :currProduct
            ];

            for (cscfga__Product_Configuration__c pc : pcList)
                pc.cscfga__Product_Basket__c = null;

            delete pcList;
            update currBasket;
            loadData();
        }
        catch (DmlException ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getDmlMessage(0)));
        }
        catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, ex.getMessage()));
        }

        return null;
    }

    private void orderByProductDefinition(String mapType) {
        Map<Id, String> defMap;

        // two different product definition presentations
        defMap = mapType == 'sub' ? subDefinitionMap : definitionMap;

        // defReversed is map with reversed key and value, list for key is used because one value could have more keys
        Map<String, List<Id>> defReversed = new Map<String, List<Id>>();

        for (Id key : defMap.keySet()) {
            List<Id> currKeys = defReversed.get(defMap.get(key));
            if (currKeys == null)
                currKeys = new List<Id>();

            currKeys.add(key);
            defReversed.put(defMap.get(key), currKeys);
        }

        List<String> pos = new List<String>();
        for (TableDefinition tableDefinition : getTableDefinitions())
            pos.add(tableDefinition.definitionName);

        List<Id> newDefOrder = new List<Id>();
        Map<Id, String> newDefMap = new Map<Id, String>();

        // for new display order use ordering from custom settings
        for (String po : pos) {
            List<Id> keys = defReversed.get(po);

            if (keys != null) {
                for (Id key : keys) {
                    newDefOrder.add(key);
                    newDefMap.put(key, po);
                }
            }
        }

        // if some values are not in custom settings they will be put at the end in the same order
        for (Id key : defMap.keySet())
            if (newDefMap.get(key) == null)
                newDefOrder.add(key);

        if (mapType == 'sub')
            orderedSubDefinitions = newDefOrder;
        else
            orderedDefinitions = newDefOrder;
    }

    public virtual class Product implements Comparable {
        public String name { get; set; }
        public Decimal numberOfUsers { get; set; }
        public Decimal totalExpectedRevenue { get; set; }
        public Decimal percentageSecured { get; set; }
        public Decimal grossMargin { get; set; }
        public Decimal NPVPerSubPerAnnum { get; set; }
        public Decimal tenure { get; set; }
        public Decimal fcc { get; set; }
        public String defName { get; set; }

        public Id recordId { get; set; }
        public String productType  { get; set; }
        public String validationMessage { get; set; }
        public String status { get; set; }

        public Product subProduct { get; set; }
        public List<Product> subProducts { get; set;}

        public Decimal hierachyLvl { get; set; }

        public Boolean getIsUserGroup() {
            return defName == 'User Group' || defName == 'Mobile Broadband User Group';
        }

        public Product(cscfga__Product_Configuration__c pcRecord) {
            recordId = pcRecord.Id;
            validationMessage = pcRecord.cscfga__Validation_Message__c;
            status = pcRecord.cscfga__Configuration_Status__c;
            subProducts = new List<Product>();

            name = pcRecord.Name;
            defName = pcRecord.cscfga__Product_Definition__r.Name;
            numberOfUsers = pcRecord.Number_of_users__c;
            totalExpectedRevenue = pcRecord.Total_Revenue__c;
            percentageSecured = pcRecord.Percentage_Secured__c;
            grossMargin = pcRecord.Gross_Margin__c;
            NPVPerSubPerAnnum = pcRecord.NPV_GOM_per_annum__c;
            tenure = pcRecord.Tenure__c;

            if (pcRecord.FCC_Airtime__c != null && pcRecord.FCC_Airtime__c > 0)
                fcc = pcRecord.FCC_Airtime__c;
            else if (pcRecord.FCC_Hardware__c != null && pcRecord.FCC_Hardware__c > 0)
                fcc = pcRecord.FCC_Hardware__c;
            else
                fcc = 0;
        }

        public void addSubProduct(Product subProd) {
            subProducts.add(subProd);
            subProduct = subProd;
        }

        public integer CompareTo(object other) {
            Product otherProd = (Product) other;

            return Integer.valueOf(hierachyLvl - otherProd.hierachyLvl);
        }
    }

    //--------------------------------------------------------------------------
    // New stuff
    //--------------------------------------------------------------------------
    public static final String JSON_RESOURCE = 'CS_BasketSummaryJSON';

    transient List<TableDefinition> tableDefinitions;

    public List<TableDefinition> getTableDefinitions() {
        if (tableDefinitions == null) {
            String jsonResource = loadStaticResource(JSON_RESOURCE).toString();
            tableDefinitions = (List<TableDefinition>)JSON.deserialize(jsonResource, List<TableDefinition>.class);
        }

        return tableDefinitions;
    }

    transient Map<String, TableEval> tableEvalMap;

    public Map<String, TableEval> getTableEvalMap() {
        if (tableEvalMap == null) {
            Integer order = 0;
            tableEvalMap = new Map<String, TableEval>();

            for (TableDefinition tableDefinition : getTableDefinitions()) {
                List<ColumnEval> columns = new List<ColumnEval>();

                for (ColumnDefinition columnDefinition : tableDefinition.columns) {
                    columns.add(new ColumnEval(
                        columnDefinition.name,
                        columnDefinition.headerStyle,
                        columnDefinition.style,
                        columnDefinition.format,
                        CS_Expression.parseExpression(columnDefinition.expression)));
                }

                TableEval table = new TableEval(tableDefinition.tableName, order++, columns);
                tableEvalMap.put(tableDefinition.definitionName, table);
            }
        }

        return tableEvalMap;
    }

    transient Set<String> fields;

    public Set<String> getFields() {
        if (fields == null) {
            fields = new Set<String>();

            for (TableEval table : getTableEvalMap().values())
                for (ColumnEval column : table.columns)
                    fields.addAll(column.syntaxTree.fields());
        }

        return fields;
    }

    transient List<cscfga__Product_Configuration__c> configurations;

    public List<cscfga__Product_Configuration__c> getConfigurations() {
        if (configurations == null) {
            configurations = new List<cscfga__Product_Configuration__c>();

            String dynamicSoql =
                'SELECT Id, Name, cscfga__Product_Definition__r.Name, cscfga__Configuration_Status__c, cscfga__Parent_Configuration__c';

            dynamicSoql += getFields().isEmpty() ? '' : ', ' + String.join(new List<String>(getFields()), ', ');
            dynamicSoql += ' FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId';

            List<SObject> queryResult = Database.query(dynamicSoql);
            for (SObject element : queryResult)
                configurations.add((cscfga__Product_Configuration__c)element);
        }

        return configurations;
    }

    private List<CS_Node> configurationTree;

    public List<CS_Node> filteredProdConfigTree {
        get {
            return CS_Node.filterList(getConfigurationTree(), new CS_Node.FilterConfigId(currProduct));
        }
    }

    public List<CS_Node> getConfigurationTree() {
        if (configurationTree == null)
            configurationTree = CS_Node.createTree(getConfigurations());

        return configurationTree;
    }

    List<table> tables;

    public List<Table> getTables() {
        if (tables == null)
            tables = createTableTree(getConfigurationTree());

        return tables;
    }

    private List<Table> createTableTree(List<CS_Node> configurationTree) {
        List<Table> returnValue = new List<Table>();
        Map<String, Table> tableMap = new Map<String, Table>();

        for (CS_Node node : configurationTree) {
            // Check if table exists if doesn't create from TableEval
            String definitionName = node.getConfig().cscfga__Product_Definition__r.Name;
            String tableDefinitionName = definitionName;
            TableEval tableEval = getTableEvalMap().get(tableDefinitionName);

            if (tableEval == null) {
                tableDefinitionName = '';
                tableEval = getTableEvalMap().get(tableDefinitionName);
            }

            Table table = tableMap.get(tableDefinitionName);
            if (table == null) {
                table = new Table(tableEval.name, tableEval.order, createColumns(tableEval.columns), new List<Row>());
                tableMap.put(tableDefinitionName, table);
            }

            table.rows.add(createRow(node, tableEval.columns, table.columns));
        }

        returnValue = tableMap.values();
        returnValue.sort();

        return returnValue;
    }

    private static List<Column> createColumns(List<ColumnEval> columnEvals) {
        List<Column> returnValue = new List<Column>();

        for (ColumnEval columnEval : columnEvals) {
            returnValue.add(new Column(
                columnEval.name,
                columnEval.headerStyle,
                columnEval.style,
                'String',
                columnEval.format));
        }

        return returnValue;
    }

    private Row createRow(CS_Node node, List<ColumnEval> columnsEval, List<Column> columns) {
        cscfga__Product_Configuration__c config = node.getConfig();
        Map<String, Data> fields = new Map<String, Data>();
        Iterator<ColumnEval> evalIterator = columnsEval.iterator();
        Iterator<Column> columnIterator = columns.iterator();

        while (evalIterator.hasNext() && columnIterator.hasNext()) {
            ColumnEval columnEval = evalIterator.next();
            Column column = columnIterator.next();
            Object value = columnEval.syntaxTree.eval(config);

            if (value != null && value instanceOf Decimal)
                column.type = 'Decimal';
            else if (value != null && value instanceOf Boolean)
                column.type = 'Boolean';

            fields.put(column.name, new Data(value));
        }

        return new Row(
            config.Id,
            config.Name,
            config.cscfga__Configuration_Status__c,
            fields,
            createTableTree(node.getChildren()));
    }

    public class Column {
        public String name { get; set; }
        public String headerStyle { get; set; }
        public String style { get; set; }
        public String type { get; set; }
        public String format { get; set; }

        public Column(String name, String headerStyle, String style, String type, String format) {
            this.name = name;
            this.headerStyle = headerStyle;
            this.style = style;
            this.type = type;
            this.format = format;
        }
    }

    public class Row {
        public String recordId { get; set; }
        public String name { get; set; }
        public String status { get; set; }
        public Map<String, Data> fields { get; set; }
        public List<Table> tables { get; set; }

        public Row(String recordId, String name, String status, Map<String, Data> fields, List<Table> tables) {
            this.recordId = recordId;
            this.name = name;
            this.status = status;
            this.fields = fields;
            this.tables = tables;
        }
    }

    public class Table implements Comparable {
        public String name { get; set; }
        public Integer order { get; set; }
        public List<Column> columns { get; set; }
        public List<Row> rows { get; set; }

        public Table(String name, Integer order, List<Column> columns, List<Row> rows) {
            this.name = name;
            this.order = order;
            this.columns = columns;
            this.rows = rows;
        }

        public Integer compareTo(Object table) {
            Table other = (Table)table;

            if (this.order == null && other.order != null)
                return 1;
            else if (this.order != null && other.order == null)
                return -1;
            else if (this.order == other.order)
                return this.name.compareTo(other.name);
            else
                return this.order - other.order;
        }
    }

    public class TableDefinition {
        String definitionName;
        String tableName;
        List<ColumnDefinition> columns;

        public TableDefinition(String definitionName, String tableName, List<ColumnDefinition> columns) {
            this.definitionName = definitionName;
            this.tableName = tableName;
            this.columns = columns;
        }
    }

    public class ColumnDefinition {
        String name;
        String headerStyle;
        String style;
        String format;
        String expression;

        public ColumnDefinition(String name, String headerStyle, String style, String format, String expression) {
            this.name = name;
            this.headerStyle = headerStyle;
            this.style = style;
            this.format = format;
            this.expression = expression;
        }
    }

    public class TableEval {
        String name;
        Integer order;
        List<ColumnEval> columns;

        public TableEval(String name, Integer order, List<ColumnEval> columns) {
            this.name = name;
            this.order = order;
            this.columns = columns;
        }
    }

    public class ColumnEval {
        String name;
        String headerStyle;
        String style;
        String format;
        CS_Expression.Eval syntaxTree;

        public ColumnEval(String name, String headerStyle, String style, String format, CS_Expression.Eval syntaxTree) {
            this.name = name;
            this.headerStyle = headerStyle;
            this.style = style;
            this.format = format;
            this.syntaxTree = syntaxTree;
        }
    }

    public class Data {
        public Object data { get; set; }

        public Data(Object data) {
            this.data = data;
        }
    }

    private static Blob loadStaticResource(String resourceName) {
        return [ SELECT Body FROM StaticResource WHERE Name = :resourceName ].Body;
    }
}