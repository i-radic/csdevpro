global class CreateAllTikitKPIs implements Schedulable
{
      global void execute(SchedulableContext sc)  
      {   
      database.executeBatch(new BatchCreateWeightedPipelineKPI() );
      //database.executeBatch(new BatchCreateGrossMarginKPI() );
      database.executeBatch(new BatchCreateWinRatioByValueKPI('MONTH') );
      database.executeBatch(new BatchCreateWinRatioByValueKPI('FISCAL_QUARTER') );
      database.executeBatch(new BatchCreateWinRatioByValueKPI('FISCAL_YEAR') );
      }
  

}