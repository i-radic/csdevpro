@isTest
private class CS_UsageProfileCalculatorControllerTest {

    static {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account a = new Account(
            Name = 'Test',
            NumberOfEmployees = 1
        );
        insert a;
        
        Associated_LEs__c le = new Associated_LEs__c(
        	name = 'test',
        	Account__c = a.Id
        );
        insert le;
        
        Opportunity o = new Opportunity(
            Name = 'Test',
            AccountId = a.Id,
            CloseDate = System.Today(),
            StageName = 'Closed Won',
            TotalOpportunityQuantity = 0
        );
        
        insert o;
        
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
        Usage_Profile__c up1 = new Usage_Profile__c(
            Active__c = true,
            Account__c = a.Id,
            Expected_SMS__c = 20,
            Expected_Voice__c = 10,
            Expected_Data__c = 20,
            Total_Number_of_Connections__c = 2,
            Data_Only_Connections__c = 1,
            AssociatedLE__c = le.Id
        );
        
        insert up1;
        
        cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
            Name = 'Test',
            Default_Usage_Profile__c = up1.Id
        );
        insert pi1;
        
        cspmb__Rate_Card__c rc1 = new cspmb__Rate_Card__c(
            Name = 'Test'
        );
        insert rc1;
        
        cspmb__Rate_Card_Line__c rlVoice = new cspmb__Rate_Card_Line__c(
            Name = 'Voice',
            cspmb__Rate_Card__c = rc1.Id
        );
        insert rlVoice;
        
        cspmb__Rate_Card_Line__c rlSMS = new cspmb__Rate_Card_Line__c(
            Name = 'SMS',
            cspmb__Rate_Card__c = rc1.Id
        );
        insert rlSMS;
        
        cspmb__Rate_Card_Line__c rlData = new cspmb__Rate_Card_Line__c(
            Name = 'Data',
            cspmb__Rate_Card__c = rc1.Id
        );
        insert rlData;
        
        cspmb__Price_Item_Rate_Card_Association__c assoc = new cspmb__Price_Item_Rate_Card_Association__c(
            cspmb__Rate_Card__c = rc1.Id,
            cspmb__Price_Item__c = pi1.Id
        );
        insert assoc;
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c(
            Name = 'Mobile Voice',
            cscfga__Description__c = 'Pd1'
        );
        insert pd1;
        
        cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
            Name = 'Ad1',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'Related Product',
            cscfga__Product_Definition__c = pd1.Id,
            cscfga__high_volume__c = true
        );
        insert ad1;
        
        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
            Name = 'User Group',
            cscfga__Description__c = 'Pd2'
        );
        insert pd2;
        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
            Name = 'Ad2',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'User Input',
            cscfga__Product_Definition__c = pd2.Id
        );
        insert ad2;
        
        cscfga__Available_Product_Option__c apo = new cscfga__Available_Product_Option__c(
            cscfga__Attribute_Definition__c = ad1.Id,
            cscfga__Product_Definition__c = pd2.Id
        );
        insert apo;
        
        CS_Related_Products__c rp = new CS_Related_Products__c(
            Name = 'Pd1',
            Related_Products__c = 'Pd2'
        );
        insert rp;
        
        CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
            Name = 'Pd2',
            Attribute_Names__c = 'Ad2',
            List_View_Attribute_Names__c = 'Ad2',
            Total_Attribute_Names__c = 'Ad2'
        );
        insert rpa;
        
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
            Name = 'Test',
            cscfga__Opportunity__c = o.Id
        );
        insert pb;
        
        cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
            Name = 'Root',
            cscfga__Product_Basket__c = pb.Id,
            cscfga__Product_Definition__c = pd1.Id,
            Service_Plan__c = pi1.Id,
            Tenure__c = 24
        );
        insert pc1;
        
        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
            Name = 'Child',
            cscfga__Root_Configuration__c = pc1.Id,
            cscfga__Parent_Configuration__c = pc1.Id,
            cscfga__Product_Basket__c = pb.Id,
            cscfga__Product_Definition__c = pd2.Id,
            cscfga__Attribute_Name__c = 'Ad1',
            Tenure__c = 1
        );
        insert pc2;
        
        cscfga__Attribute__c at1 = new cscfga__Attribute__c(
            Name = 'Ad1',
            cscfga__Attribute_Definition__c = ad1.Id,
            cscfga__Value__c = pc2.Id,
            cscfga__Product_Configuration__c = pc1.Id
        );
        insert at1;
        
        cscfga__Attribute__c at2 = new cscfga__Attribute__c(
            Name = 'Ad2',
            cscfga__Attribute_Definition__c = ad2.Id,
            cscfga__Value__c = 'Test',
            cscfga__Product_Configuration__c = pc2.Id
        );
        insert at2;
        
        
    }
    
    private static testMethod void testCalculations() {
        Account a = [select Id from Account limit 1];
        cspmb__Price_Item__c pi = [select id from cspmb__Price_Item__c limit 1];
        cscfga__Product_Basket__c pb = [select id from cscfga__Product_Basket__c limit 1];
        
        String usageTotalsJSON = CS_UsageTotalsCalculatorController.initiateCalculations(null, a.Id, pi.Id, pb.Id, false);
        CS_UsageTotalsCalculatorController cont = new CS_UsageTotalsCalculatorController();
        cont = new CS_UsageTotalsCalculatorController(pb.Id, a.Id, pi.Id);
        Map<String, List<cspmb__Rate_Card_Line__c>> referenceLine = cont.referenceRateCardLines;
        Usage_Profile__c referenceProfile = cont.referenceUsageProfile;
        Map<String, List<String>> rclMap = cont.getRclFields();
        cont.setUsageProfileValid(true);
        cont.setUsageProfileChanged(true);
        // code coverage...
        cont.getSectionPermission();
        cont.getUsageReadPermission();
        cont.getUsageEditPermission();
        cont.getRateEditPermission();
        cont.getRateReadPermission();
        cont.getBasketEdit();
        cont.getUsageProfiles();
        cont.returnToBasket();

        cont.getUsageProfileChanged();
        cont.getUsageProfileValid();
        
         Test.startTest();
                
        //String jobId = CS_UsageTotalsCalculatorController.refreshAllConfigurationsInBasket(pb.Id);
        //String res = CS_UsageTotalsCalculatorController.checkIfBatchJobIsRunning(jobId);
        cscfga__Product_Configuration__c pc = [select Id from cscfga__Product_Configuration__c limit 1];
        CS_RecalculateConfigurationTotals rc = new CS_RecalculateConfigurationTotals(pc.Id);

        Test.stopTest();
        
        List<Usage_Profile__c> upList = [Select Id, Name from Usage_Profile__c];
        update upList;
        delete upList;
        
    }
    
    private static testMethod void testRecalculations() {
        Account a = [select Id from Account limit 1];
        cspmb__Price_Item__c pi = [select id from cspmb__Price_Item__c limit 1];
        cscfga__Product_Basket__c pb = [select id from cscfga__Product_Basket__c limit 1];
        
        String usageTotalsJSON = CS_UsageTotalsCalculatorController.initiateCalculations(null, a.Id, pi.Id, pb.Id, false);
        CS_UsageTotalsCalculatorController cont = new CS_UsageTotalsCalculatorController();
        cont = new CS_UsageTotalsCalculatorController(pb.Id, a.Id, pi.Id);
        Map<String, List<cspmb__Rate_Card_Line__c>> referenceLine = cont.referenceRateCardLines;
        Usage_Profile__c referenceProfile = cont.referenceUsageProfile;
        Map<String, List<String>> rclMap = cont.getRclFields();
        cont.setUsageProfileValid(true);
        cont.setUsageProfileChanged(true);
        // code coverage...
        cont.getSectionPermission();
        cont.getUsageReadPermission();
        cont.getUsageEditPermission();
        cont.getRateEditPermission();
        cont.getRateReadPermission();
        cont.getBasketEdit();
        cont.getUsageProfiles();
        cont.returnToBasket();
        cont.saveAndRecalculateTotals();
        cont.resetToBasketDefault();
        
        Test.startTest();
        
        cont.getReferenceUsageProfileJSON();
        cont.getReferenceRateCardJSON();
        cont.refreshCalculations();
        cont.resetToDefault();
        cont.getUsageProfile();
        cont.getUsageProfileJSON();
        
         
         
        CS_UsageTotalsCalculatorController.getUsageProfilesJSON(null, a.Id, pi.Id, pb.Id, false);
         
        cont.getRateCardLines();
        cont.getRateCardLinesJSON();
        
        List<Usage_Profile__c> upList = [Select Id, Name from Usage_Profile__c];
        update upList;
        delete upList;
        
        Test.stopTest();  
    }
}