@isTest
private class Test_BatchCampaignSplitEloqua {
    
    static String type='BTLB Split';  
     static String CRON_EXP = '0 0 0 15 3 ? 2022';
       static testMethod void myUnitTest1() {
        
           
        Test_Factory.SetProperty('IsTest', 'yes');
        
        User user1 = new User (id=UserInfo.getUserId(), Apex_Trigger_Account__c=true, Run_Apex_Triggers__c=true );
            user1.Division='BTLB';
            user1.Department='BTLB StJamesPark Gtr London SE';
            update user1;
			
             System.runAs( user1 ){
                 
                 TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
                 settings.Account__c = FALSE;
                 settings.Contact__c = FALSE;
                 settings.Opportunity__c = FALSE;
                 settings.OpportunitylineItem__c = FALSE;
                 settings.Task__c = FALSE;
                 settings.Event__c = FALSE;
                 
                 upsert settings TriggerDeactivating__c.Id;
             }
           
        //create test BTLB Master Record  
        BTLB_Master__c testBTLB = new BTLB_Master__c( name = 'TEST',  Account_Owner__c = UserInfo.getUserId(), BTLB_Name_ExtLink__c ='TEST');
           Database.SaveResult[] BTLBResult = Database.insert(new BTLB_Master__c[] {testBTLB});     
        
           
        Account acc = Test_Factory.CreateAccount(); // a LE Level Account to link to
            acc.Sector__c = 'BT Local Business';
            acc.LOB_Code__c = 'lob999';
            acc.SAC_Code__c = 'ET234';
            acc.LE_Code__c = 'TEST3';
            acc.AM_EIN__c = '802537216';
            acc.Sub_Sector__c = 'ssTest';
            acc.Account_Owner_Department__c = 'BTLB StJamesPark Gtr London SE';
            acc.Account_Owner_Division__c = 'BTLB';
            insert acc;
           
           Account acc1 = Test_Factory.CreateAccount(); // a LE Level Account to link to
            acc1.Sector__c = 'BT Local Business';
            acc1.LOB_Code__c = 'lob999';
            acc1.SAC_Code__c = 'ET234';
            acc1.LE_Code__c = 'TEST3';
            acc1.AM_EIN__c = '802537217';
            acc1.Sub_Sector__c = 'ssTest';
            acc1.Account_Owner_Department__c = 'BTLB Lancs and Cumbria';
            acc1.Account_Owner_Division__c = 'BTLB';
            insert acc1;
           
           
           Account acc2 = Test_Factory.CreateAccount(); // a LE Level Account to link to
            acc2.Sector__c = 'BT Local Business';
            acc2.LOB_Code__c = 'lob999';
            acc2.SAC_Code__c = 'ET234';
            acc2.LE_Code__c = 'TEST3';
            acc2.AM_EIN__c = '802537218';
            acc2.Sub_Sector__c = 'ssTest';
            acc2.Account_Owner_Department__c = 'LEM Desk Calls and Lines';
            acc2.Account_Owner_Division__c = 'BTLB';
            insert acc2;
    
           Contact Con=new Contact (firstname='ABC',lastname='DEF',SAC_Code__c='ET234',AccountId=acc.Id);
           insert Con;
               
           Contact Con1=new Contact (firstname='ABC1',lastname='DEF1',AccountId=acc1.Id);
           insert Con1;
           
           Contact Con2=new Contact (firstname='ABC2',lastname='DEF2',AccountId=acc2.Id);
           insert Con2;
           
           //Create a campaign record
           Campaign btlbCampaign = new Campaign(name='BTLBCampaignTestTestTestTestTestTestTestTestTestTest001',isActive=true,X_Day_Rule__c=1,campaign_type__c='BTLB Split', Data_Source__c='Self Generated');
           btlbCampaign.Campaign_Owner_Department__c='BTLB StJamesPark Gtr London SE';
           btlbCampaign.StartDate=Date.valueOf('2016-03-09');
           insert btlbCampaign;
           
           Campaign btlbCampaign2 = new Campaign(name='BTLBCampaignTestTestTestTestTestTestTestTestTestTestTest002',isActive=true,X_Day_Rule__c=1,campaign_type__c='BTLB Split', Data_Source__c='Self Generated'); 
            btlbCampaign2.ParentId=btlbCampaign.Id;   
           btlbCampaign2.Campaign_Owner_Department__c='BTLB Lancs and Cumbria';
           btlbCampaign2.StartDate=Date.valueOf('2016-03-09');
           insert btlbCampaign2;
           
           Campaign btlbCampaign3 = new Campaign(name='BTLBCampaignTest003',isActive=true,X_Day_Rule__c=1,campaign_type__c='BTLB Split', Data_Source__c='Self Generated'); 
           btlbCampaign3.ParentId=btlbCampaign2.Id;
           btlbCampaign3.Campaign_Owner_Department__c='BTLB London East';
           btlbCampaign3.StartDate=Date.valueOf('2016-03-09');
           insert btlbCampaign3;
               
           campaignmember CM=new campaignmember(Campaignid=btlbCampaign.id,Contactid=Con.id);
           insert CM;
           
           campaignmember CM1=new campaignmember(Campaignid=btlbCampaign.id,Contactid=Con1.id);
           insert CM1;
               
           campaignmember CM2=new campaignmember(Campaignid=btlbCampaign2.id,Contactid=Con1.id);
           insert CM2;
           
           campaignmember CM3=new campaignmember(Campaignid=btlbCampaign3.id,Contactid=Con2.id);
           Test.startTest();
           insert CM3;
        
           List<Campaign> camps= new List<Campaign>();
           //Batch_ExectionController.executeBatchForCampaignSplit(btlbCampaign.Id,'NotusingSAC');
           String query = 'Select Id,contactId,contact.SAC_Code__c,Status,Unsuccessful_Call_Attempts__c,Campaign_Member_Notes__c,Campaign.Id,Campaign.Name from CampaignMember where Campaign.StartDate = 2016-03-09 AND contact.SAC_Code__c!=null AND Campaign.Campaign_Type__c=\''+type+'\'';
           
           //Test.startTest();
           BatchCampaignSplitEloqua bcs= new BatchCampaignSplitEloqua(query);
           Database.executeBatch(bcs);
           Test.stopTest();         

       }
      
    
    
    
      static testMethod void myUnitTest2() {
        
           
        Test_Factory.SetProperty('IsTest', 'yes');
        
        User user1 = new User (id=UserInfo.getUserId(), Apex_Trigger_Account__c=true, Run_Apex_Triggers__c=true );
            user1.Division='BTLB';
            user1.Department='LEM Desk Calls and Lines';
            update user1;
			System.runAs( user1 ){
                 
                 TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
                 settings.Account__c = FALSE;
                 settings.Contact__c = FALSE;
                 settings.Opportunity__c = FALSE;
                 settings.OpportunitylineItem__c = FALSE;
                 settings.Task__c = FALSE;
                 settings.Event__c = FALSE;
                 
                 upsert settings TriggerDeactivating__c.Id;
             }	

        //create test BTLB    
        BTLB_Master__c testBTLB = new BTLB_Master__c( name = 'TEST',  Account_Owner__c = UserInfo.getUserId(), BTLB_Name_ExtLink__c ='TEST');
           Database.SaveResult[] BTLBResult = Database.insert(new BTLB_Master__c[] {testBTLB});     
           system.debug('ADJ TestBTLB ' + BTLBResult[0] ); 
               
           
        Account acc = Test_Factory.CreateAccount(); // a LE Level Account to link to
            acc.Sector__c = 'BT Local Business';
            acc.LOB_Code__c = 'lob999';
            acc.SAC_Code__c = 'ET234';
            acc.LE_Code__c = 'TEST3';
            acc.AM_EIN__c = '802537216';
            acc.Sub_Sector__c = 'ssTest';
            acc.Account_Owner_Department__c = 'LEM Desk Calls and Lines';
            acc.Account_Owner_Division__c = 'BTLB';
            insert acc;
    
           Contact Con=new Contact (firstname='ABC',lastname='DEF',SAC_Code__c='ET234',AccountId=acc.Id);
           insert Con;
               
           Contact Con1=new Contact (firstname='ABC',lastname='DEF',AccountId=acc.Id);
           insert Con1;
           
           //Create a campaign record
          Campaign btlbCampaign = new Campaign(name='BTLBCampaignTestTestTestTestTestTestTestTestTestTest001',isActive=true,X_Day_Rule__c=1,campaign_type__c='BTLB Split', Data_Source__c='Self Generated');
           btlbCampaign.Campaign_Owner_Department__c='BTLB London East';
           btlbCampaign.StartDate=Date.today();
           insert btlbCampaign;
           
           Campaign btlbCampaign2 = new Campaign(name='BTLBCampaignTest002',isActive=true,X_Day_Rule__c=1,campaign_type__c='BTLB Split', Data_Source__c='Self Generated'); 
           // btlbCampaign2.ParentId=btlbCampaign.Id;   
           btlbCampaign2.Campaign_Owner_Department__c='BTLB Lancs and Cumbria';
           btlbCampaign2.StartDate=Date.today();
           insert btlbCampaign2;
               
           campaignmember CM=new campaignmember(Campaignid=btlbCampaign2.id,Contactid=Con.id);
           insert CM;
               
           campaignmember CM2=new campaignmember(Campaignid=btlbCampaign2.id,Contactid=Con1.id);
           insert CM2;
        
           List<Campaign> camps= new List<Campaign>();
          
           String query = 'Select Id,contactId,contact.SAC_Code__c,Status,Unsuccessful_Call_Attempts__c,Campaign_Member_Notes__c,Campaign.Id,Campaign.Name from CampaignMember where Campaign.StartDate > YESTERDAY AND contact.SAC_Code__c!=null AND Campaign.Campaign_Type__c=\''+type+'\'';
           
           Test.startTest();
           BatchCampaignSplitEloqua bcs= new BatchCampaignSplitEloqua(query);
           Database.executeBatch(bcs);
           Test.stopTest();   
          
          //Test.startTest();

          // Schedule the test job
          //String jobId = System.schedule('Test Schedule Batch Apex',CRON_EXP,new Scheduler_CampaignSplitEloqua());

         //Test.stopTest();   
       }
       
  
}