/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = TRUE)
public class Test_OppDateapprovedUpdate {
    static testMethod void myUnitTest() {
        user thisUser = [select id from user where id=: userinfo.getUserId()];
        system.runAs(thisUser){
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr7@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B12',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2B3Profile13167cr@bt.com',
                           EIN__c = '012365'
                           );
        insert u1;
        
        
        RecordType rt;
        
        rt = [select id from RecordType where Name='Standard' AND SobjectType = 'Task'];
        System.assert(rt!= null);
        
        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 3';
        A.sac_code__c = 'testSAC1';
        A.Sector_code__c = 'CORP1';
        A.LOB_Code__c = 'LOB1';
        A.OwnerId = u1.Id;
        A.CUG__c='CugTest1';
        Database.SaveResult accountResult = Database.insert(A);
        
        Opportunity opp = new Opportunity();
        opp.AccountId = A.id;
        opp.Name = 'tst_oppty';
        //opp.NIBR_Year_2009_10__c = 1;
        //opp.NIBR_Year_2010_11__c = 1;
        //opp.NIBR_Year_2011_12__c = 1;
        opp.StageName = 'Created';
        opp.Sales_Stage_Detail__c = 'Appointment Made';
        opp.CloseDate = system.today();
        
        
        insert opp;
        
        Task T1 = new Task(RecordType = rt, Status = 'In Progress', WhatId=Opp.Id,Type='Call',subject = 'Bespoke Pricing Request',Description = 'Completed' );
        Insert T1;        
        
        T1.Status = 'Completed';
        T1.Description = 'Task Completed';
        
        Update T1;
        
        }
    }
}