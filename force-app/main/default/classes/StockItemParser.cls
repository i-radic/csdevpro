/*
###########################################################################
# File..................: StockItemParser
# Created by............: Sridhar Aluru
# Created Date..........: 16-May-2016
# Last Modified by......:
# Last Modified Date....: 
# Description...........: Parser class used inside StockItemsController 
# Change Log............:
# Test Class............: TestStockItemsController
###########################################################################
*/

public with sharing class StockItemParser {
    
    private boolean orphanRecords;
    
    public StockItemParser ()
    { 
       orphanRecords = true;
    }
    
    
    public List<Stock_Item__c> parseFile (List<List<String>> flinesList)
    {
        try
        {
            Stock_Item__c obj=null;
            Decimal amt=0.0;
            String transType ='',bno='';
            
            //Remove Header
            List<String> flines1 = flinesList[0];
            system.debug('--->' +flines1);

            string headerLine = flines1[0];
            string month = flines1[4];
            system.debug('-----Header Line--->' +headerLine);

            flinesList.remove(0);
            
            List<Stock_Item__c> stocklist = new List<Stock_Item__c>();
            for(List<String> flines : flinesList )
            {   
                obj = new Stock_Item__c ();
                System.debug('-----------flines-------'+flines);
                for(Integer k=0;k<flines.size();k++)
                {   
                    System.debug('-----------flines ------- ' + k + ' ---> ' + flines.get(k));
                    if (!String.isEmpty(flines.get(k)))
                    {                       
                        if (k==0) 
                        {
                            if (flines.get(k) != null)
                            {
                                System.debug('SKU ID Value >>>>>>>>>>>>>'+String.valueOf(flines.get(k)));
                                obj.SKU__c = flines.get(k);
                                orphanRecords = false;
                            }
                        }
                        else if (k==1 && !String.isEmpty(flines.get(k)))  
                            obj.Identification_Number__c  = String.valueOf(flines.get(k));
                        else if (k==2)  
                            obj.CTN__c = String.valueOf(flines.get(k));
                        else if (k==3) 
                            obj.Status__c = String.valueOf(flines.get(k));
                        else if (k==4) 
                            obj.Identification_Method__c = String.valueOf(flines.get(k));                        
                    }
                }//for
                stocklist.add(obj);     
            }//for
            system.debug(' Stock Items ---> ' + stocklist);
            return stocklist;
        }
        catch (exception e) 
        {throw e;}
    }   
    

    public class stockitemException extends Exception {}
}