global class CS_CustomLookupPlanSharer extends cscfga.ALookupSearch{

    public override String getRequiredAttributes(){
        return '[]';
    }

    public override Object[] doLookupSearch(Map<String, String>
			searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset,
			Integer pageLimit){

    	final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
		final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
		Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;

		String searchTerm = searchFields.get('searchValue');
        if (string.isEmpty(searchTerm)) {
            searchTerm = '%';
        } else {
            searchTerm = '%' + searchTerm + '%';
        }
			
		List<cspmb__Price_Item__c> priceItemList = [
			SELECT 		Id,
						Name,
						cspmb__One_Off_Charge__c,
						cspmb__Recurring_Charge__c,
						cspmb__Is_Active__c,
						cspmb__Product_Definition_Name__c,
						BT_Mobile_Proposition__c,
						Record_Type_Name__c,
						Commission_Cost__c,
						BT_Hardware_Fund__c,
						BT_Technology_Fund__c,
						Discountable__c,
						Maximum_Users__c,
						cspmb__Recurring_Cost__c,
						Minimum_Recurring_Price__c,
						Recurring_Discount_Values__c,
						One_Off_Discount_Values__c,
						Product_Code__c,
						cspmb__Price_Item_Code__c
				FROM	cspmb__Price_Item__c
				WHERE	BT_Mobile_Proposition__r.Name = 'Mobile Sharer'
					AND	cspmb__Is_Active__c = true
					AND	cspmb__Product_Definition_Name__c = 'BT Mobile'
					AND Record_Type_Name__c = 'BT Mobile Plan'
					AND Name LIKE :searchTerm
				ORDER BY Sequence__c
				LIMIT 	:SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT 
				OFFSET 	:recordOffset
		];

		return priceItemList;
    }
}