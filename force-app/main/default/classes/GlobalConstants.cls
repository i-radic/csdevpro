/******************************************************************************************************
Name : GlobalConstants
Description : This class will serve all constant values. So no hardcoding will be requried in any of classes 
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    21/11/2015              Abhishek Jagtap                         Class created         
*********************************************************************************************************/
global class GlobalConstants
{
    //Opportunity run control
    public static boolean opportunityran = true;
    
    // Opportnity Record Types ...
    public static String OPPORTUNITY_CLOSE_NONSTANDARD_RECORDTYPE = 'Opp_ClosedNonStandardComplex';
  //  public static String OPPORTUNITY_CLOSE_STANDARD_MCC_RECORDTYPE = 'Opp_ClosedStandard_MCC';
  //  public static String OPPORTUNITY_STANDARD_MCC_RECORDTYPE = 'Opp_Standard_MCC';
    public static String OPPORTUNITY_NONSTANDARD_MCC_RECORDTYPE = 'Complex';

    //Opportunity tier     
    public static String OPPORTUNITY_TIER='Standard';
      
   //Questionaire QuestionnaireQuestion Record types ...
    public static String QUESTIONARRE_QUESTIONNAIRE_PROFILER_RECORDTYPE = 'QuestionnaireQuestion_Profiler';
    public static String QUESTIONARRE_QUESTION_QUALIFICATION_RECORDTYPE = 'QuestionnaireQuestion_Qualification';    
    
      //Questionaire QuestionnaireAnswer Record types ...
    public static String QUESTIONARRE_ANSWER_PROFILER_RECORDTYPE = 'QuestionnaireAnswer_Profiler';
    public static String QUESTIONARRE_ANSWER_QUALIFICATION_RECORDTYPE = 'QuestionnaireAnswer_Qualification';

  
    //Questionaire Record types...
    public static String QUESTIONNAIRE_PROFILER_RECORDTYPE = 'Questionnaire_Profiler';
    public static String QUESTIONARRE_QUALIFICATION_RECORDTYPE = 'Questionnaire_Qualification';
    public static String QUESTIONARRE_QUALIFICATION_RECORDTYPE_LABEL = 'Qualification'; 
    public static String QUESTIONARRE_PROFILER_RECORDTYPE_LABEL='Profiler';
    
    //Questionaire QuestionReply Record types ...
    public static String QUESTIONNAIRE_REPLY_PROFILER_RECORDTYPE = 'QuestionnaireReply_Profiler';
    public static String QUESTIONARRE_REPLY_QUALIFICATION_RECORDTYPE = 'QuestionnaireReply_Qualification';
    
    //Questionaire opportunity error check
    public static String CUSTOM_VALIDATION_ERROR ='FIELD_CUSTOM_VALIDATION_EXCEPTION,';
    public static String ERROR_MESSAGE_END =': []';
    
    // Opportunity All stages ...
    public static String OPPORTUNITY_STAGE_CLOSE_WON = 'Won';
    public static String OPPORTUNITY_STAGE_NON_CLOSED = 'Non Closed';
    public static String OPPORTUNITY_STAGE_PRE_QUALIFICATION = 'Pre-Qualification';
    public static String OPPORTUNITY_STAGE_QUALIFICATION = 'Qualification';
    public static String OPPORTUNITY_STAGE_DEVELOP_SOLUTION = 'Develop Solution';
    public static String OPPORTUNITY_STAGE_PRESENT_NEGOTIATE = 'Present / Negotiate';
    public static String OPPORTUNITY_STAGE_CLOSED_LOST = 'Lost';
    public static String OPPORTUNITY_STAGE_CLOSED_CANCELLED ='Cancelled';
    
    // Opportunity DLD status ...
    public static String DLD_HIGHRIKNOSERVICEALLOWED = 'High Risk - No Service Allowed';
    
    //Queue Name...
    public static String QUEUE_CREDIT_VETTING = 'Credit Vetting Queue';
    
    // VETTING status...
    public static String STATUS_VETTING_REQUESTED = 'Requested';
    public static String REQUEST_OBJECT = 'Request__c';
    public static String REQUEST_CREDIT_VETTING = 'Credit Vetting';
     
    //Contact Roles..
    public static String CONTACT_ROLES_DECISION_MAKER = 'Decision Maker';
    
    //Opportunity and OLI calculation Constants
    public static String IRV = 'IRV';
    public static String NIER = 'NIER';
    public static String FISCAL_YEAR = 'Fiscal Year';
    public static Decimal NIER_NUMBER_OF_DAYS = 365.24219879;
    
    //Questionnaire Question Topic Area values Req No-239
    public static String QUESTIONNAIRE_QUESTION_WINNABILITY='Winnability';
    public static String QUESTIONNAIRE_QUESTION_DESIRABILITY='Desirability';
    public static String QUESTIONNAIRE_QUESTION_DELIVERABILITY='Deliverability';
    
    //Asset Status for req no-329
    Public static String ASSET_STATUS_SUBSTITUTED ='Substituted';
    Public static String ASSET_STATUS_ACTIVE ='Active';
    
    //Deal Card 
    Public static String BETTER_THAN_BT ='Better than BT';
    Public static String WORSE_THAN_BT ='Worse than BT';
    Public static String Questionnaire = 'Questionnaire__c';
    Public static String QuestionnaireReply = 'QuestionnaireReply__c';
    Public static String QuestionnaireQuestion = 'QuestionnaireQuestion__c';
    
    //Limit Variables
    Public static Integer DMLROWS = Limits.getLimitDmlRows();
    Public static Integer Limit_One =1;
    
    //EMPTY CHECK
    Public static String EMPTYSTRING = '';
    
    //Comma Check
    Public static String COMMASTRING= ',';
    
    // Target Period
    Public static String TARGETPERIODYEAR='Year';
    Public static String TARGETPERIODQUARTER='Quarter';
    
    //Target Type
    Public static String TARGETTYPEMARKETUNIT ='Market Unit';
    Public static String TARGETTYPEWORLDKEY ='Global World Key';
    Public static String TARGETTYPEORGLEVEL2='Organization Level 2';
    Public static String TARGETTYPEALLGS='All GS';
    
    //Questionnaire gate name
    Public static String GATENAME_Make_Pursuit ='Make Pursuit';
    Public static String GATENAME_Sign_On ='Sign On';
    Public static String GATENAME_Sign_Off ='Sign Off';
    Public static String GATENAME_CSC_Call ='CSC Call'; 
    Public static String GATENAME_Contract_Sign_Off ='Contract Sign Off';
    
    //hardcoded url
    Public static String URL_HARD_CODE ='/';
    Public static String HYPHEN ='-';
    Public static String FINANCIAL_YEAR ='FY';
    Public static String STARTYEAR_STARTDATE='01/04/';
    Public static String ENDYEAR_STARTDATE='31/03/';
    Public static String PROFILER_LABEL ='System.Label.Opportunity_Profile';
}