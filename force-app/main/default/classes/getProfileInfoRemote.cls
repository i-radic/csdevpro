global with sharing class getProfileInfoRemote implements cssmgnt.RemoteActionDataProvider {
    @RemoteAction
    global static Map<String, Object> getData(Map<String, Object> inputMap) {
        System.debug('Input parameters received ' + inputMap);
        Map<String, Object> returnMap= new Map<String, Object>();
        try{
            String basketId=(String) inputMap.get('basketID');
            List<cscfga__Product_Basket__c> baskets = [select id,Owner.Profile.name from cscfga__Product_Basket__c where id=:basketId];
            System.debug(baskets);
            System.debug(baskets[0]);

            returnMap.put('config',baskets[0]);
            returnMap.put('profile',baskets[0].Owner.Profile.name);
        }
        catch (exception e) {
            returnMap.put('exception', e.getMessage());
        }
        System.debug(returnMap);
        return returnMap;
    }
}