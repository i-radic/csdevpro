global class Scheduler_CampaignSplitEloqua implements Schedulable {
        
       global void execute(SchedulableContext sctx) {
        
          // The query used by the batch job.
          String type='BTLB Split';         
          String query = 'Select Id,contactId,contact.SAC_Code__c,Status,Unsuccessful_Call_Attempts__c,Campaign_Member_Notes__c,Campaign.Id,Campaign.Name from CampaignMember where Campaign.StartDate > YESTERDAY AND  contact.SAC_Code__c!=null AND Campaign.Campaign_Type__c=\''+type+'\'';

          BatchCampaignSplitEloqua bcs= new BatchCampaignSplitEloqua(query);
          Id batchProcessId = Database.executeBatch(bcs,200);         
          System.debug('Returned batch process ID:@@@@@@@@@@@@@@@@@@ ' + batchProcessId);      
       }  
}