@isTest(SeeAllData=true)
public class CS_ManageSolutionsControllerExt_Test {
        
    static csord__Solution__c CreateSolution(Account a){
        csord__Solution__c newSol = new csord__Solution__c();
        newSol.csord__Account__c = a.Id;
        newSol.Name = 'Test Solution';
        newSol.csord__Status__c = 'Completed';
        newSol.csord__Identification__c = 'TestIdentification';
        return newSol;
    }
    
    static csord__Order__c CreateOrder(){
        csord__Order__c newOrd = new csord__Order__c();
        newOrd.csord__Status2__c = 'TestStatus Order';
        newOrd.csord__Identification__c = 'TestIdentification';
        return newOrd;
    }
    
    static csord__Subscription__c CreateSubscription(csord__Solution__c s, csord__Order__c o){
        csord__Subscription__c newSub = new csord__Subscription__c();
        newSub.cssdm__solution_association__c = s.Id;
        newSub.csord__Order__c = o.Id;
        newSub.csord__Identification__c = 'TestIdentification';
        return newSub;
    }
    
    static csord__Service__c CreateService(csord__Solution__c s, csord__Subscription__c ss){
        csord__Service__c newSer = new csord__Service__c();
        newSer.csord__Status__c = 'TestStatus Service';
        newSer.csord__Identification__c = 'TestIdentification';
        newSer.cssdm__solution_association__c = s.Id;
        newSer.csord__Subscription__c = ss.Id;
        return newSer;
    }
    
    static testmethod void UnitTest(){
        Test.startTest();
        Account account = Test_Factory.CreateAccount();     
        account.CUG__c = 'cugCV1';
        account.OwnerId = UserInfo.GetUserId();
        insert account;
        csord__Solution__c solution = CreateSolution(account);
        insert solution;
        csord__Order__c order = CreateOrder();
        insert order;
        csord__Subscription__c subscription = CreateSubscription(solution, order);
        insert subscription;
        csord__Service__c service = CreateService(solution, subscription);
		insert service;
        ApexPages.StandardController sc = new ApexPages.StandardController(account);
        CS_ManageSolutionsControllerExtension ctrl = new CS_ManageSolutionsControllerExtension(sc);
        ctrl.getSolutions();
        ctrl.activateSolution();
        ctrl.solutions.get(0).selected = true;
        ctrl.activateSolution();
        PageReference modify = ctrl.modifySolution();
        ctrl.solutions.get(0).selected = true;
        modify = ctrl.modifySolution();
        PageReference back = ctrl.backToAccount();
        Test.stopTest();
    }

}