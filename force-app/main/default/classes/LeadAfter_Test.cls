@isTest

private class LeadAfter_Test{
    static testMethod void runPositiveTestCases() {
   
    StaticVariables.setAccountDontRun(False);  
    Account acc = Test_Factory.CreateAccount();  
    acc.OwnerId = UserInfo.getUserId(); 
    acc.CUG__c = 'leadCUG188';
    acc.le_code__c = 'JMMCUG188';
    acc.Sector_code__c = 'JMM88';
    acc.Sector__c = 'JMM';
    acc.SAC_Code__c = 'JMM67';
    //insert acc;
    Database.SaveResult[] acc1Result = Database.insert(new Account[] {acc});
                    
    Contact con = Test_Factory.CreateContact();
    con.AccountCUG__c =  acc.Id;
    con.AccountId = acc.Id;
    con.SAC_Code__c = acc.SAC_Code__c;
    insert con;     
    system.debug('JMM con: ' + con);

    Lead l = New Lead();
    l.Title = acc.Name;
    l.LastName = 'Landscape Lead: ' + acc.Name;
    l.Company = acc.Name;
    l.Account__c = acc.Id; //con.AccountCUG__c;
    l.Contact__c = con.Id; //con.Id;
    l.Description = 'Test Landscape to Lead';
    l.LeadSource = 'BT Sport';
    Test.startTest();    
    insert l;     
    //convert lead
    l.Status = 'No Opportunity';
    update l;
    
    l.LeadSource = 'BT Sport';
    l.Status = 'Opportunity Identified';
    update l;
    
    
    //lead for controller test
    Lead l2 = New Lead();
    l2.Title = acc.Name;
    l2.LastName = 'Landscape Lead: ' + acc.Name;
    l2.Company = acc.Name;
    l2.Account__c = NULL; //con.AccountCUG__c;
    l2.Contact__c = NUll; //con.Id;
    l2.Description = 'Test Landscape to Lead';      
    insert l2;        
    
        
    // test contact controller
    PageReference pageRef = Page.LeadContact;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('Id', l2.Id);    
    ApexPages.StandardController stc = new ApexPages.StandardController(l2);
    
    LeadContactCtrl ctrl = New LeadContactCtrl(stc);
    Test.stopTest();
    ctrl.viewAccs();    
    ctrl.save(); 
    ctrl.selAcc();
    ctrl.selCon();
    ctrl.selectedID = con.Id;
    
    /*test converting desk lead
    l2.Status = 'Opportunity Identified';
    l2.LeadSource = 'Desk Landscape Alert';
    update l2;
    */
    
    }    
}