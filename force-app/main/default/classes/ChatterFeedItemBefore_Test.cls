@isTest
private class ChatterFeedItemBefore_Test {

   static testmethod void validateChatterFeed(){
   Test.startTest();
   CollaborationGroup g = New CollaborationGroup();
   g.Name = 'Apex Class Test JMM';
   g.CollaborationType = 'Public';
   insert g;
   
   FeedItem f = New FeedItem();
   f.ParentID = g.Id; 
   f.Body = 'Test';
   insert f;
   Test.stopTest();       
   }
}