/*
###########################################################################
# File..................: TestStockItemsController 
# Created by............: Sridhar Aluru
# Created Date..........: 13-Jun-2016
# Last Modified by......:
# Last Modified Date....: 
# Description...........: Test Class created for Apex class: StockItemsController 
# Change Log............:
###########################################################################
*/

@isTest
private class TestStockItemsController {

    public static testMethod void testStockItemsController  () {
        test.starttest(); 
        
        List<SKU__c> skuLst = new List<SKU__c>();
        SKU__c sku = Test_Utils.populateSKU('SKU','EEBTestSKU',0.00,true,'EE 4G Nano','SIM');
        SKU__c skuDefault = Test_Utils.populateSKU ('SKUDefault','EEBTestSKUDefault',0.00,true,'EE 4G Nano','SIM');      
        skuLst.add(sku);
        skuLst.add(skuDefault); 
        insert skuLst;
        
        System_Defaults__c cs = new System_Defaults__c ();
        cs.Name='Stock Item Default SKU';
        cs.Record_Id__c = skuDefault.Id;
        insert cs;
        
        String header = 'SKU ID, EMEI, CTN, Status, ID Method, Currency \n';
        String finalStr = header;
        string recordString = '"'+sku.id+'","8944122575500659466","07896411716","Active","SIM","GBP"\n';
        string recordString1 = '"'+sku.id+'","8944122575500659467","07896411710","Active","SIM","GBP"\n';
        string recordString2 = '" ","8944122575500659466","07896411711","Active","SIM","GBP"\n';
        string recordString3 = '"'+sku.id+'","8944122575500659468","07896411716","Active","SIM","GBP"\n';
        string recordString4 = '"a2g7E000000000","8944122575500659468","07896411716","Active","SIM","GBP"\n';
               
        finalstr = finalStr +recordString;
        
        Blob csvBlob = Blob.valueOf(finalStr);
        String csvname= 'StockItem.csv';
        
        StockItemsController SICtrl = new StockItemsController ();
        SICtrl.contentFile = csvBlob;
        
        SICtrl.readFile();
        SICtrl.uploadStockItems();
        SICtrl.getStockItems();
        SICtrl.refresh();
        
        finalstr = finalStr + recordString + recordString1 + recordString2 + recordString3 + recordString4;
        Blob csvBlob1 = Blob.valueOf(finalStr);
        SICtrl.contentFile = csvBlob1;
        
        SICtrl.readFile();
        SICtrl.uploadStockItems();
        SICtrl.getStockItems();
        SICtrl.refresh();
        
        test.stoptest();
    } 
}