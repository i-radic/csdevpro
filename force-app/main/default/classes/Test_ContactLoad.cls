@isTest

private class Test_ContactLoad{
 
     static testMethod void runPositiveTestCases() {
     	Test_Factory.SetProperty('IsTest', 'yes');
 
 // set user to non Admin / Dataloader     
 		Profile prof = [SELECT Id FROM Profile WHERE Name = 'BT: Standard User']; 
 		
 		User U1 = new User();
        u1.Username = 'test98745@bt.com';
        u1.Ein__c = '987654321';
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.ProfileId = prof.Id;
        u1.TimeZoneSidKey = 'Europe/London'; 
        u1.LocaleSidKey = 'en_GB';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'en_US';
        u1.Apex_Trigger_Account__c = false; 
        u1.Run_Apex_Triggers__c = false;
        insert U1;

Test.StartTest();
System.RunAs(u1)
{
       
    //    Account dummyAccount = Test_Factory.CreateAccountForDummy();
    //    insert dummyAccount;
               
		StaticVariables.setAccountDontRun(False);  
		Account acc1 = Test_Factory.CreateAccount(); // a LOB Level Account to link to        
		acc1.Name = 'DELETE ME - Unit Test Account';        
		acc1.cug__c = 'cugAlan1';     
		insert acc1;
		
        StaticVariables.setContactIsRunBefore(False);             
        Contact con1 = Test_Factory.CreateContact();
        con1.Cug__c = 'cugAlan1';
        con1.Contact_Post_Code__c = 'WR5 3RL';
        insert con1;    
        
 // update record 
        StaticVariables.setContactIsRunBefore(False);             
        con1.Contact_Post_Code__c = 'WR5 3RL';
        update con1;    
        
}        
Test.StopTest();          
       
       
    }
}