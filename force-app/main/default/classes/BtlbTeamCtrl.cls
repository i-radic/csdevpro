public class BtlbTeamCtrl { 
   
public BtlbTeamCtrl(ApexPages.StandardController controller) {
}
 
ID AccountId = ApexPages.currentPage().getParameters().get('id');
List<Account> BTLBname = [ select BTLB_Common_Name__c from Account where id = :AccountId];
string BTLBname2 = BTLBname[0].BTLB_Common_Name__c;

public List<Account_Team_Contact_BTLB__c> getSpecialist() {   

return [select Name__c, Specialist_Name__c, Specialist_Role__c, Tel_no__c, Mobile__c, email__c from Account_Team_Contact_BTLB__c where btlb__r.name = :BTLBname2];
    }
 }