/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2013-06-24 13:09:50 
 *  @description:
 *      #806 - set Opportunity Team Members to automatically 'Follow' the
 *      Opportunity on Chatter when they are added to the team.
 *  
 *  Version History :   
 *    
 */
public without sharing class OpportunityTeamMemberHandler {
  private static OpportunityTeamMemberHandler handler;

  public static OpportunityTeamMemberHandler getHandler() {
    if (null == handler) {
      handler = new OpportunityTeamMemberHandler();
    }
    return handler;
  }

  /*
  public void beforeInsert () {
    //nothing currently
  }

  public void beforeUpdate () {
    //nothing currently
  }

  public void beforeDelete () {
    //nothing currently
  }
  */

  public void afterInsert () {
    chatterFollowOpportunity((List<OpportunityTeamMember>)Trigger.new);
  }

  public void afterUpdate () {
    //nothing currently
  }

  public void afterDelete () {
    chatterUnFollowOpportunity((List<OpportunityTeamMember>)Trigger.old);
  }

  public void afterUnDelete () {
    //nothing currently
  }


  /**
   * #806 - set Opportunity Team Members to automatically 'Follow' the
   * Opportunity on Chatter when they are added to the team
   */
  private void chatterFollowOpportunity(List<OpportunityTeamMember> members) {
    final Map<String, EntitySubscription> esMap = new Map<String, EntitySubscription>();
    final Set<Id> oppIds = new Set<Id>();
    Set<Id> RecoppIds = new Set<Id>();
    final Set<Id> userIds = new Set<Id>();
      //added to code to filter Opportunity standard rec type
      For(OpportunityTeamMember optm:members){
          RecoppIds.add(optm.OpportunityId);
      }
      Set<id> OppStandardRectypeid = new set<id>();
      for(Opportunity op:[select id,name,recordtype.name from Opportunity where id In:RecoppIds]){
         if(op.recordtype.name=='Standard') 
            OppStandardRectypeid.add(op.id);
      }
      
      for(OpportunityTeamMember m : members) {
      EntitySubscription e = new EntitySubscription();
      e.subscriberId = m.UserId;
      e.parentId = m.OpportunityId;
      String key = m.UserId + '' + m.OpportunityId;
      oppIds.add(m.OpportunityId);
      userIds.add(m.UserId);
      If(OppStandardRectypeid.contains(m.OpportunityId))
          esMap.put(key, e);
    }
    
    //remove users who already follows given opportunities
    for(EntitySubscription es : [select Id, subscriberId, parentId from EntitySubscription where subscriberId in: userIds and parentId in: oppIds ]) {
      String key = es.subscriberId + '' + es.parentId;
      esMap.remove(key);
    }  
    Database.insert(esMap.values());
  }
  /**
   * #806 - Unfollow opportunity if a User is removed from Opportunity Team   
   */
  private void chatterUnFollowOpportunity(List<OpportunityTeamMember> members) {
    final Set<Id> userIds = new Set<Id>();
    final Set<Id> parentIds = new Set<Id>();
    for(OpportunityTeamMember m : members) {
      userIds.add(m.UserId);
      parentIds.add(m.OpportunityId);
    }
    EntitySubscription[] es = [select Id from EntitySubscription where subscriberId in: userIds and parentId in: parentIds];
    Database.delete(es);
  }

}