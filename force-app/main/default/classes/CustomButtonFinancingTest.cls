/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 04/11/2020
   @author Maahaboob Basha

   @description Test class for CustomButtonFinancing.

   @modifications
   
*/
@isTest(SeeAllData=FALSE)
private class CustomButtonFinancingTest {

    /***************************************************************************************************
    * Method Name : performActionNegativeTest
    * Description : Used to simulate and test the logic of performAction method in CustomButtonFinancing 
    * Parameters  : NA
    * Return      : NA                      
    ***************************************************************************************************/
    static testmethod void performActionNegativeTest() {
        String result;
        String baseUrl = Label.CS_DiscountRedirect_Org_Url;
        String basketId = 'Test';
        String redirectURL = baseUrl + '/apex/CloudVoiceItemList?basketId=' + basketId;
        Test.startTest();
            CustomButtonFinancing cbFinancing = new CustomButtonFinancing();
            result = cbFinancing.performAction(basketId);
        Test.stopTest();
        System.assertNotEquals(NULL, result);
        System.assertEquals('{"status":"ok","redirectURL":"' + redirectURL + '"}', result);
    }
    
    /***************************************************************************************************
    * Method Name : performActionPositiveTest
    * Description : Used to simulate and test the logic of performAction method in CustomButtonFinancing 
    * Parameters  : NA
    * Return      : NA                      
    ***************************************************************************************************/
    static testmethod void performActionPositiveTest() {
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        String baseUrl = Label.CS_DiscountRedirect_Org_Url;
        String redirectURL = baseUrl + '/apex/CloudVoiceItemList?basketId=' + basket.Id;
        Test.startTest();
            CustomButtonFinancing cbFinancing = new CustomButtonFinancing();
            result = cbFinancing.performAction(basket.Id);
        Test.stopTest();
        System.assertNotEquals(NULL, result);
        System.assertEquals('{"status":"ok","redirectURL":"' + redirectURL + '"}', result);
    }
}