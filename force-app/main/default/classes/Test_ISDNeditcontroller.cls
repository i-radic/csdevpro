@isTest
private class Test_ISDNeditcontroller {

    static testMethod void runPositiveTestCases() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        CRF__c crf = new CRF__C();
        ISDN_CRF__c isdn = new ISDN_CRF__c();
        channels__c channel = new channels__c();
        opportunity o = new opportunity();
        Id MoversId;
        Id NSOId;
        Id BroadbandId;
        Id DDId;
        Id AXId;
        Id WLR3Id;
        Id ResignId;
        Id NPId;
        Id NCId;
        Id ISDNId;
        Id NCPId;
        List<RecordType> RTList = [Select Id,DeveloperName from RecordType where SobjectType=:'CRF__c'];
        if(RTList.size()>0){
            for(RecordType RT:RTList){
                if(RT.DeveloperName=='Movers')
                    MoversId = RT.Id;
                else if(RT.DeveloperName=='NSO_BS')
                    NSOId = RT.Id;
                else if(RT.DeveloperName=='Broadband')
                    BroadbandId = RT.Id;
                else if(RT.DeveloperName=='Direct_Debit_CRF')
                    DDId = RT.Id;
                else if(RT.DeveloperName=='AX_Form')
                    AXId = RT.Id;
                else if(RT.DeveloperName=='WLR3_Authorisation')
                    WLR3Id = RT.Id;
                else if(RT.DeveloperName=='Resign')
                    ResignId = RT.Id;
                else if(RT.DeveloperName=='Number_Port')
                    NPId = RT.Id;
                else if(RT.DeveloperName=='Name_Change_CRF')
                    NCId = RT.Id;
                else if(RT.DeveloperName=='ISDN30')
                    ISDNId = RT.Id;
                else if(RT.DeveloperName=='NCP')
                    NCPId = RT.Id;

            }
        }
        o.name='test';
        o.StageName='FOS_Stage';
        insert o;
        crf.Opportunity__c = o.Id;
        crf.RecordTypeId = ISDNId;
        insert crf;
        isdn.Related_to_CRF__c = crf.Id;
        isdn.Room_No_where_NTE_to_be_fitted__c = '111';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '1';
        isdn.Is_this_a_new_customer_to_BT__c = 'Yes';
        isdn.Tel_no_A_c_no__c = '1111';
        isdn.DASS_2__c = '111';
        isdn.ISDN_30e__c = '2';
        isdn.Total_No_of_DDI_Channels_required__c = '5';
        isdn.Total_No_of_non_DDI_Channels_required__c = '5';
        isdn.VP_Number__c = '14';
        isdn.No_of_DDI_s__c = '15';
        isdn.Room_No_where_NTE_to_be_fitted__c = '123';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '123';
        isdn.New_BLDG_No_BT_Lines_Or_Services__c = '123';
        isdn.Is_this_a_new_customer_to_BT__c = 'Yes';
        isdn.Dual_Parenting__c = false;
        isdn.LOP_ref__c = '123';
        isdn.Secondary_exchange_name_and_code__c = '123';
        isdn.Diverse_Routing__c = false;
		isdn.LOP_ref_DR__c = '123';
        isdn.Remote_exchange_name_and_code_DR__c = '123';
		isdn.One_Bill__c = false;
		isdn.Discount_Plan__c = false;
		isdn.Which_Discount_Plan__c = '123';
		isdn.Site_Assurance__c = true;
		isdn.Option1_ACCD__c = true;
		isdn.Option2__c = true;
        insert isdn;
        channel.Related_to_CRF__c = crf.Id;
        insert channel;

        PageReference PageRef = Page.VF_ISDN_Edit;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id',crf.Id);
        ApexPages.currentPage().getParameters().put('channelId',channel.Id);
        ApexPages.currentPage().getParameters().put('isdncrfId',isdn.Id);

        ApexPages.StandardController ctrl = new ApexPages.StandardController(crf);
        ISDNEditController isdnedit = new ISDNEditController(ctrl);
        isdnedit.OnLoad();
        isdnedit.Submit();
 		isdnedit.cancel1();
        isdnedit.getISDNcrf();
        isdnedit.getStandbyBattery();
        isdnedit.getservicetype();

		crf = new CRF__C();
        isdn = new ISDN_CRF__c();
        channel = new channels__c();
		crf.Opportunity__c = o.Id;
        crf.RecordTypeId = ISDNId;
        insert crf;
        isdn.Related_to_CRF__c = crf.Id;
        isdn.Room_No_where_NTE_to_be_fitted__c = '111';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '1';
        isdn.Is_this_a_new_customer_to_BT__c = 'Yes';
        isdn.Tel_no_A_c_no__c = '1111';
        isdn.DASS_2__c = '111';
        isdn.ISDN_30e__c = '2';
        isdn.Total_No_of_DDI_Channels_required__c = '5';
        isdn.Total_No_of_non_DDI_Channels_required__c = '5';
        isdn.VP_Number__c = '14';
        isdn.No_of_DDI_s__c = '15';
        isdn.Room_No_where_NTE_to_be_fitted__c = '';
        isdn.Floor_No_where_NTE_to_be_fitted__c = '';
        isdn.Is_this_a_new_customer_to_BT__c = '-None-';
        isdn.Dual_Parenting__c = true;
        isdn.LOP_ref__c = '';
        isdn.Secondary_exchange_name_and_code__c = '';
        isdn.Diverse_Routing__c = true;
		isdn.LOP_ref_DR__c = '';
        isdn.Remote_exchange_name_and_code_DR__c = '';
		isdn.One_Bill__c = true;
		isdn.Discount_Plan__c = true;
		isdn.Which_Discount_Plan__c = '';
		isdn.Site_Assurance__c = true;
		isdn.Option1_ACCD__c = false;
		isdn.Option2__c = false;
        insert isdn;
        channel.Related_to_CRF__c = crf.Id;
        insert channel;

        PageReference PageRef2 = Page.VF_ISDN_Edit;
        test.setCurrentPage(PageRef2);
        ApexPages.currentPage().getParameters().put('id',crf.Id);
        ApexPages.currentPage().getParameters().put('channelId',null);
        ApexPages.currentPage().getParameters().put('isdncrfId',isdn.Id);

        ApexPages.StandardController ctrl2 = new ApexPages.StandardController(crf);
        ISDNEditController isdnedit2 = new ISDNEditController(ctrl2);

        isdnedit2.Submit();
        isdnedit2.cancel1();
    }
}