@isTest
public class Test_eContactCardListButtonCon {
    static testMethod void TesteContactMethod(){
     User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
         
     }
    
    Account testAcc = Test_Factory.CreateAccount();
    insert testAcc;
    
    List<Contact> insertContacts = new List<Contact>();    
    Contact testCon = Test_Factory.CreateContact();
        testCon.accountId = testAcc.id;
        testCon.Status__c ='Active';
        testCon.Email = 'test@test.com';        
        insertContacts.add(testCon);
        insert insertContacts;
        
        ApexPages.CurrentPage().getParameters().put('id',testAcc.id) ;
        
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(insertContacts);
        eContactCardListButtonCon testContactListButton = new eContactCardListButtonCon(sc);
        testContactListButton.ShowPanel= False;
        testContactListButton.SendeCard();
        testContactListButton.Cancel();
         
            
}
}