/*######################################################################################
PURPOSE
Controller for the output quote/order pdf generator.
Includes some elements of validation before either doc can be produced.

HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
public with sharing class CloudVoiceQuoteCTRL {
    public Cloud_Voice_Site_Product__c cvspRec;
    public Cloud_Voice__c cvRec;
    ApexPages.StandardController Gcontroller ;
    ApexPages.StandardSetController setCtrl ;

    public CloudVoiceQuoteCTRL(ApexPages.StandardController controller) {
        Gcontroller = controller;
        if(!Test.isRunningTest()){
            controller.addFields(new List<String>{'Quote_Ready__c'});
            controller.addFields(new List<String>{'Service_Hub_Status__c'});          
            controller.addFields(new List<String>{'SharerPlanID__c'});
            controller.addFields(new List<String>{'ConnectionChargeID__c'});
            controller.addFields(new List<String>{'InternationalSharerPlanID__c'});
            controller.addFields(new List<String>{'Purchasing_Option__c'});
            controller.addFields(new List<String>{'Finance_Provider__c'});
            controller.addFields(new List<String>{'zCheckMainSite__c'});
            controller.addFields(new List<String>{'RecordType.DeveloperName'});
            controller.addFields(new List<String>{'Total_Sites__c'});
            controller.addFields(new List<String>{'UK_Sharer_PAYG_Flex__c'});
            controller.addFields(new List<String>{'UK_Sharer_PAYG_Flex_Authorised__c'});
        }
        this.cvRec= (Cloud_Voice__c)controller.getRecord();
    }

    public string qReady= System.currentPageReference().getParameters().get('quoteready');
    public string CV= System.currentPageReference().getParameters().get('id');
    public Set<Id> CVS= new Set<Id>();
    public Set<Id> CVSP= new Set<Id>();
    public List<Cloud_Voice_Site__c> CVSPs= new List<Cloud_Voice_Site__c>();
    public List<Number_Port_Line__c> CVNumPortLines = new List<Number_Port_Line__c>();
    Public String Sadd {get; set;}
    Public String Badd {get; set;}
    Public String Dadd {get; set;}
    public string objID {get; set;}
    public string errMessage {get; set;}
    
 
    public PageReference checkProposalReady(){
        if(cvRec.RecordType.DeveloperName == 'Cloud_Phone'){
            AttachCPOrderToOppty();
            PageReference retPg = new PageReference('/apex/CloudPhoneOrder?id=' + CV + '&type=Proposal');
            return retpg.setRedirect(true); 
        }        
        errMessage = '';
        if(cvRec.Quote_Ready__c != 0){
          errMessage = '<br/>&nbsp;&nbsp; - Site(s) listed below need to be checked for validation errors';
        }
        if(cvRec.Total_Sites__c == 0){
          errMessage = errMessage + '<br/>&nbsp;&nbsp; - You must add at least one site.';
        }      
        if(cvRec.RecordType.DeveloperName != 'Cloud_Voice_Modify'){
            if(cvRec.SharerPlanID__c == Null || cvRec.InternationalSharerPlanID__c == Null){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select both a Uk and International sharer plan';
            }
            if(cvRec.ConnectionChargeID__c == Null ){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select a Service Connection Charge';
            }
            if(cvRec.Purchasing_Option__c == 'Finance' && (cvRec.Finance_Provider__c =='' || cvRec.Finance_Provider__c == Null)){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select a finance supplier on the finance option page, or change the Purchasing Option to Outright Sale.';
            }
            if(cvRec.zCheckMainSite__c == 0){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - You cannot place an order with only homeworker sites.';
            }
        }
        if(errMessage != ''){
            return null;
        }
        AttachQuoteToOppty();
        PageReference retPg = new PageReference('/apex/CloudVoiceQuote?id=' + CV + '&type=Proposal');
        return retpg.setRedirect(true);
    }

    public PageReference checkOrderReady(){
        errMessage = '';

        if(cvRec.RecordType.DeveloperName == 'Cloud_Phone'){
            if(errMessage != ''){
                return null;
            }
            AttachCPOrderToOppty();
            PageReference retPg = new PageReference('/apex/CloudPhoneOrder?id=' + CV + '&type=Order');
            return retpg.setRedirect(true); 
        }
        if(cvRec.Quote_Ready__c != 0){
          errMessage = '<br/>&nbsp;&nbsp; - Site(s) listed below need to be checked for validation errors';
        }
        if(cvRec.Total_Sites__c == 0){
          errMessage = errMessage + '<br/>&nbsp;&nbsp; - You must add at least one site.';
        }
        if(cvRec.UK_Sharer_PAYG_Flex__c != 'No Flex' && cvRec.UK_Sharer_PAYG_Flex_Authorised__c == false){
          errMessage = errMessage + '<br/>&nbsp;&nbsp; - PAYG Flex Price has not been authorised.';
        }
		if(cvRec.RecordType.DeveloperName != 'Cloud_Voice_Modify'){
            if(cvRec.SharerPlanID__c == Null || cvRec.InternationalSharerPlanID__c == Null){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select both a Uk and International sharer plan';
            }
            if(cvRec.ConnectionChargeID__c == Null ){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select a Service Connection Charge';
            }
            if(cvRec.Purchasing_Option__c == 'Finance' && (cvRec.Finance_Provider__c =='' || cvRec.Finance_Provider__c == Null)){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - Select a finance supplier on the finance option page, or change the Purchasing Option to Outright Sale.';
            }
            if(cvRec.zCheckMainSite__c == 0){
              errMessage = errMessage + '<br/>&nbsp;&nbsp; - You cannot place an order with only homeworker sites.';
            }
            if(errMessage != ''){
                return null;
            }
        }
        if(errMessage != ''){
            return null;
        }
        AttachOrderToOppty();
        PageReference retPg = new PageReference('/apex/CloudVoiceQuote?id=' + CV + '&type=Order');
        return retpg.setRedirect(true); 
    }

    public List<Cloud_Voice_Site__c> getCVSPerr() {
        CVSPs = [SELECT ID, Name, Site_Name__c, Validated__c FROM Cloud_Voice_Site__c WHERE CV__c = :CV AND (zComplete__c = 'problem' OR zComplete__c = 'notstarted') ];
        return CVSPs;
    }
    
    public List<Cloud_Voice_Site__c> getCVSP() {
        List<Cloud_Voice_Site__c> CVSites = [SELECT ID, Site_name__c, Site_Contact__r.Name, Site_Contact__r.Phone, Site_Contact__r.Email, Site_address__c, Total_Site_Recurring_Cost__c, Total_Site_One_Off_Cost__c, Total_Site_Licenses__c, Users__c, 
        Structured_Cabling_Summary__c,LAN_Summary__c, Billing_address__c, Delivery_address__c,Access_Concurrent_Calls__c,Access_Codec__c,Access_Codec_Name__c,Site_Discounts_Initial__c,Site_Discounts_Recurring__c,Access_product_decision__c, Access_product_price__c,
        Access_Sale__c,Access_Cease__c,Access_Data_Traffic__c,Access_Max_UL__c,Access_Speed_UL__c,Access_Product__c,Access_Hub_Upgrade__c,Access_firewall__c,CPE_Multiple_Delivery_Addresses__c,Total_CPE__c,
        Server_Administrator__c,Total_CV_Basic_Licenses__c ,Total_CV_Connect_Licenses__c,Total_CV_Collaborate_Licenses__c,Total_Lines__c,Total_New_Lines__c,LAN_solution_provider__c, zCablingOCS__c,zCPEInstall__c ,
        Existing_LAN_Support_PoE__c,LAN_Peripheral_Device_Ports__c,Customer_Required_Date__c,Power__c,Structured_cabling_provider__c,LAN_Max_Ports__c,SC_Installation_Date__c,zCVConfig__c, Total_Discount__c,Total_Site_1st_Year_Cost__c, RecordType.Name,zAccessProvision__c, Total_Selected_Finance__c ,zSoftPhone__c,zPhysicalPhones__c,Add_Modify_Site_Name__c,RecordType.DeveloperName,
            (SELECT ID, Product_name__c, Quantity__c, Unit_Cost__c, Total_Recurring_Cost__c, Total_One_Off_Cost__c, Cloud_Voice_Site__c, Cloud_Voice_Site__r.Site_name__c, Type__c,Image__c,Purchasing_Option__c,Discount_Initial__c, Number_Info__c,Finance_Option__c,rcSTD__c,rcDirectory__c FROM Site_Products__r ORDER BY Type__c, Product_Name__c) 
        FROM Cloud_Voice_Site__c 
        WHERE CV__c = :CV AND RecordType.DeveloperName != 'Cloud_Voice_Modify_Service_Only'
        ORDER BY Site_Name__c];

        for (Cloud_Voice_Site__c c:CVSites){
            system.debug('JMM Site_address__c1: ' + c.Site_address__c);
            if(c.Site_address__c != Null){
                Sadd = c.Site_address__c.replaceAll('\n','<br/>');
            }
        }
        return CVSites;
    }

    public List<Number_Port_Line__c> getCVNumPortLines() {
        CVNumPortLines = [SELECT ID, Cloud_Voice__c, Name, Company_Name__c, Account_Number__c, Registered_Address__c, Registered_Number__c, Requesters_Name__c, Requesters_Job_Title__c, Requesters_Contact_Details__c, Site_Info__c, CreatedDate, Current_Service_Post_Code__c, Product__c, Full_Range__c,Losing_CP__c, Finish_Number__c, Start_Number__c FROM Number_Port_Line__c WHERE Cloud_Voice__c = :CV ORDER BY Id ASC];
        return CVNumPortLines;
    }

    public void AttachQuoteToOppty(){
        Cloud_Voice__c cv = [Select Id, Opportunity__c, Name FROM Cloud_Voice__c WHERE Id =: cvRec.Id];
        List<Attachment> attachments = [select Name FROM Attachment WHERE parentid =: cv.Opportunity__c];
        for(Attachment attachment : attachments){
            if(attachment.Name.Contains('Cloud Voice Proposal')){
                delete attachment;
            }
        }
        pageReference pdf = Page.CloudVoiceQuote;
        pdf.getParameters().put('id',cv.Id);
        pdf.getParameters().put('type','Proposal');
        Attachment attach = new Attachment();
        Blob body;
        if(!Test.isRunningTest()){
            body = pdf.getContent();
        }
        attach.Body = body;
        attach.Name = 'Cloud Voice Proposal '+cv.Name+'.pdf';
        attach.IsPrivate = false;
        attach.ParentId = cv.Opportunity__c;
        if(!Test.isRunningTest()){
            insert attach;
        }        
    }

    public void AttachOrderToOppty(){
        Cloud_Voice__c cv = [Select Id, Opportunity__c, Name FROM Cloud_Voice__c WHERE Id =: cvRec.Id];
        Opportunity oppty = [Select Id, stagename FROM Opportunity WHERE Id =: cv.Opportunity__c];
        if(oppty.stagename != 'Won'){
            List<Attachment> attachments = [select Name FROM Attachment WHERE parentid =: cv.Opportunity__c];
            for(Attachment attachment : attachments){
                if(attachment.Name.Contains('Cloud Voice Order')){
                    delete attachment;
                }
            }
            pageReference pdf = Page.CloudVoiceQuote;
            pdf.getParameters().put('id',cv.Id);
            pdf.getParameters().put('type','Order');
            Attachment attach = new Attachment();
            Blob body;
            if(!Test.isRunningTest()){
                body = pdf.getContent();
            }
            attach.Body = body;
            attach.Name = 'Cloud Voice Order '+cv.Name+'.pdf';
            attach.IsPrivate = false;
            attach.ParentId = cv.Opportunity__c;
            if(!Test.isRunningTest()){
                insert attach;
            }        
        }
    }

    public void AttachCPOrderToOppty(){
        Cloud_Voice__c cv = [Select Id, Opportunity__c, Name FROM Cloud_Voice__c WHERE Id =: cvRec.Id];
        Opportunity oppty = [Select Id, stagename FROM Opportunity WHERE Id =: cv.Opportunity__c];
        if(oppty.stagename != 'Won'){
            List<Attachment> attachments = [select Name FROM Attachment WHERE parentid =: cv.Opportunity__c];
            for(Attachment attachment : attachments){
                if(attachment.Name.Contains('Cloud Phone Order')){
                    delete attachment;
                }
            }
            pageReference pdf = Page.CloudPhoneOrder;
            pdf.getParameters().put('id',cv.Id);
            Attachment attach = new Attachment();
            Blob body;
            if(!Test.isRunningTest()){
                body = pdf.getContent();
            }
            attach.Body = body;
            attach.Name = 'Cloud Phone Order '+cv.Name+'.pdf';
            attach.IsPrivate = false;
            attach.ParentId = cv.Opportunity__c;
            if(!Test.isRunningTest()){
                insert attach;
            }        
        }
    }
}