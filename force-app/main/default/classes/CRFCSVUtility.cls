global class CRFCSVUtility {
    
    public  List<BTNetCRMCVSWrapper> btNetDataList {get; set;}
    public class BTNetCRMCVSWrapper implements Comparable {
        public Integer cloumnOrder{get;set;}
        public String column1{get;set;}
        public String column2{get;set;}// will serve as unique column in case of BT Net CRF CSV
        public String valuecolumn1{get;set;}
        
        public Integer compareTo(Object objToCompare) {
            BTNetCRMCVSWrapper compareToStud = (BTNetCRMCVSWrapper)ObjToCompare;
            if (cloumnOrder == compareToStud.cloumnOrder) return 0;
            if (cloumnOrder > compareToStud.cloumnOrder) return 1;
            return -1;
        }
    }
    public CRFCSVUtility (ApexPages.StandardController stdController) {
          //getBTNetCRMinfo();
    }
    public CRFCSVUtility () {
          //getBTNetCRMinfo();
    }
     public static void getBTNetCRMinfo(){
         String crfRecordId = ApexPages.currentPage().getParameters().get('Id');
         //String crfRecordId = 'aBx0E000000CdIt';
         String transactionType = 'BTNet CRF Mapping';
         Map<String,String> dataSetMap = new Map<String,String>();
         List<BTNetCRMCVSWrapper> btNetDataList = new List<BTNetCRMCVSWrapper>();
         
         BTNet_Customer_Details__c btNetCRM = [select id, Opportunity__c  from BTNet_Customer_Details__c where id = :crfRecordId limit 1];
         Set<String> objectIdSet = new Set<String>();
         objectIdSet.add(crfRecordId);
         if(btNetCRM!=null){
              objectIdSet.add(btNetCRM.Opportunity__c);
         }
        
         dataSetMap = getMappingDataForCSV(objectIdSet,transactionType);
         System.debug('objectIdSet>>>'+objectIdSet);
         System.debug('dataSetMap>>>'+dataSetMap);
         if(!dataSetMap.isEmpty()){
             List<Data_Mapping__c> dataMappingList= [select Active__c,Column_Sequence__c,Display_Column_1__c,Display_Column_2__c,Transaction_Type__c
                                     from Data_Mapping__c where  Active__c = true and Transaction_Type__c includes (:transactionType)];
             if(!dataMappingList.isEmpty()){
                 for(Data_Mapping__c dataMappingObj : dataMappingList){
                     BTNetCRMCVSWrapper btNetCRMCVSWrapperObj = new BTNetCRMCVSWrapper();
                     btNetCRMCVSWrapperObj.cloumnOrder = Integer.valueOf(dataMappingObj.Column_Sequence__c);
                     btNetCRMCVSWrapperObj.column1 = dataMappingObj.Display_Column_1__c;
                     btNetCRMCVSWrapperObj.column2 = dataMappingObj.Display_Column_2__c;
                     btNetCRMCVSWrapperObj.valuecolumn1 = dataSetMap.get(dataMappingObj.Display_Column_2__c.toLowerCase());
                     btNetDataList.add(btNetCRMCVSWrapperObj);
                 }
             }
         }
         btNetDataList.sort();
         System.debug('btNetDataList>>>'+JSON.serializePretty(btNetDataList));
     }
     public static Map<String,String> getMappingDataForCSV(Set<String> objectIdSet, String transactionType){
         return DataMappingUtility.CreateParametersValueMap(objectIdSet, transactionType);
     }
     
     webservice static void geneateCFRAttachments(ID crfRecordId,String mode, boolean ECCRequired){
        try{
             List<CRF_Attachment__c> existingAttachmentList = [select id from CRF_Attachment__c where Customer_Requirements_Form__c = :crfRecordId ];
             if(!existingAttachmentList.isEmpty()){
                 delete existingAttachmentList;
             }
             Map<String,String> dataSetMap = new Map<String,String>();
             List<BTNetCRMCVSWrapper> btNetDataList = new List<BTNetCRMCVSWrapper>();  
             BTNet_Customer_Details__c btNetCRM = [select id,CRF_Complete__c,Product_Basket__r.Basket_Sync_Counter__c,Account__c, Opportunity__c,Product_Configuration__c,BTNET_ECC_Required__c, Product_Configuration__r.BT_Net_Primary__c,Product_Configuration__r.BT_Net_Secondary__c  from BTNet_Customer_Details__c where id = :crfRecordId limit 1];
             Set<String> objectIdSet = new Set<String>();
             objectIdSet.add(crfRecordId);
             if(btNetCRM!=null){
                  String optyId= btNetCRM.Opportunity__c;
                  String customerName= btNetCRM.Account__c;
                  Integer basnketSyncCounter = Integer.valueOf(btNetCRM.Product_Basket__r.Basket_Sync_Counter__c);
                  objectIdSet.add(btNetCRM.Opportunity__c);
                  if(btNetCRM.Product_Configuration__c !=null){
                       objectIdSet.add(btNetCRM.Product_Configuration__c);
                  }
                 // getting file for ECC
                 
                 String transactionType = 'BTNet CRM ECC Mapping';
                 if(btNetCRM.BTNET_ECC_Required__c){
                     System.debug('objectIdSet>>>'+objectIdSet);
                     String csvFileStr= geneateCSVForTracsaction(objectIdSet,transactionType); 
                     if(String.isNotBlank(csvFileStr)){
                         // create attachment record for ECC
                         String fileName = optyId+'_'+basnketSyncCounter+'_'+customerName+'_ECC';
                         createAttachementRecord(optyId,transactionType,crfRecordId,fileName,csvFileStr);
                     }
                 }
                 List<Site__c> siteList = [select id,Site_Type__c from Site__c where BTNet_Customer_Details__c = : crfRecordId];
                 Map<String,String> siteIDMap = new Map<String,String>();
                 if(!siteList.isEmpty()){
                     for(site__c siteObj: siteList){
                         //if(String.valueOf(siteOb.Site_Type__c).equalsIgnoreCase('Primary')){
                             siteIDMap.put(siteObj.Site_Type__c,siteObj.id);
                         //}
                     }
                 }
                 System.debug('siteIDMap>>>'+siteIDMap);
                 // getting file for Primary
                 if(btNetCRM.Product_Configuration__r.BT_Net_Primary__c !=null){
                     transactionType = 'BTNet CRF Mapping Primary';
                     Set<String> objectIdSetPrimary = new Set<String>();
                     objectIdSetPrimary.addAll(objectIdSet);
                     if(!siteIDMap.isEmpty() && siteIDMap.containsKey('Primary')){
                         objectIdSetPrimary.add(siteIDMap.get('Primary'));
                     }
                     String csvFileStr= geneateCSVForTracsaction(objectIdSetPrimary,transactionType); 
                     if(String.isNotBlank(csvFileStr)){
                         // create attachment record for ECC
                         String fileName =  optyId+'_'+basnketSyncCounter+'_'+customerName+'_MS';
                         createAttachementRecord(optyId,transactionType,crfRecordId,fileName,csvFileStr);
                     }
                 }
                 // getting file for Secondary
                  if(btNetCRM.Product_Configuration__r.BT_Net_Secondary__c !=null){
                     transactionType = 'BTNet CRF Mapping Secondary';
                     Set<String> objectIdSetSec= new Set<String>();
                     objectIdSetSec.addAll(objectIdSet);
                     if(!siteIDMap.isEmpty() && siteIDMap.containsKey('Secondary')){
                         objectIdSetSec.add(siteIDMap.get('Secondary'));
                     }
                     String csvFileStr= geneateCSVForTracsaction(objectIdSetSec,transactionType); 
                     if(String.isNotBlank(csvFileStr)){
                         // create attachment record for ECC
                         String fileName =  optyId+'_'+basnketSyncCounter+'_'+customerName+'_SV';
                         createAttachementRecord(optyId,transactionType,crfRecordId,fileName,csvFileStr);
                     }
                 }
				 btNetCRM.CRF_Complete__c =true;		
				 update btNetCRM;
             }
             
        }catch(Exception e){
            
        }
         
     }
     
     
     public static void createAttachementRecord(String optyId, String selectedType , String crfObjectID, String csvFileName,String csvFileStr){
         try{
            CRF_Attachment__c obj = new CRF_Attachment__c();
            obj.Customer_Requirements_Form__c = crfObjectID; 
            obj.description__c = 'Auto Created';
            obj.Attachment_Type__c = selectedType;
            obj.active__c = true;
            
            Id rtid= Schema.SObjectType.CRF_Attachment__c.getRecordTypeInfosByName().get(selectedType).getRecordTypeId();
            System.debug('rtid>>'+rtid);
            if(rtid!=null){
                obj.recordTypeID = rtid;
            }
            insert obj;
            
            Database.SaveResult attachmentResult = saveStandardAttachment(obj.id, csvFileName,csvFileStr);
        
            if (attachmentResult != null && attachmentResult.isSuccess()) {
                // update the custom attachment record with some attachment info
                CRF_Attachment__c customAttachment = [select id from CRF_Attachment__c where id = :obj.id];
                customAttachment.name = csvFileName;
                customAttachment.Attachment__c = attachmentResult.getId();
                update customAttachment;
            }
             
         }catch(Exception e){
             
         }
         
     }
     
      // create an actual Attachment record with the CRF_Attachment__c as parent
     public static Database.SaveResult saveStandardAttachment(Id parentId, String csvFileName,String csvFileStr) {
            Database.SaveResult result;
            
            Attachment attachment = new Attachment();
            attachment.body = Blob.valueOf(csvFileStr);
            attachment.name = csvFileName +'_'+ System.now().format('yyyy_MM_dd_hh_mm_ss') + '.csv';
            attachment.parentId = parentId;
            attachment.ContentType = 'CS CSV';
            // inser the attahcment
            result = Database.insert(attachment);
            // reset the file for the view state
            //fileBody = Blob.valueOf(' ');
            return result;
     }
      webservice static  void getBTNetCRMinfoECC(ID crfRecordId,String mode){
        // String crfRecordId = ApexPages.currentPage().getParameters().get('Id');
         String transactionType = 'BTNet CRM ECC Mapping';
         Map<String,String> dataSetMap = new Map<String,String>();
         List<BTNetCRMCVSWrapper> btNetDataList = new List<BTNetCRMCVSWrapper>();  
         BTNet_Customer_Details__c btNetCRM = [select id, Opportunity__c  from BTNet_Customer_Details__c where id = :crfRecordId limit 1];
         Set<String> objectIdSet = new Set<String>();
         objectIdSet.add(crfRecordId);
         if(btNetCRM!=null){
              objectIdSet.add(btNetCRM.Opportunity__c);
         }  
         String csvFileStr= geneateCSVForTracsaction(objectIdSet,transactionType); 
         System.debug('csvFileStr111>>>'+JSON.serializePretty(csvFileStr));
         Attachment att = new Attachment();
         att.OwnerId = UserInfo.getUserId();
         att.ParentId = crfRecordId; // the record the file is attached to
         att.Body=Blob.valueOf(csvFileStr);
         att.ContentType = 'CS CSV';
         att.Name='BTNET_ECC_' + System.now().format('yyyy_MM_dd_hh_mm_ss') + '.csv';
 
         att.IsPrivate = false;
         try {
          insert att;
         } catch (DMLException e) {
         // ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
          //return null;
         } finally {
          att = new Attachment(); 
         }
        
      
         //sendAttachmentinEmail(csvFileStr,'ECC_CSV.csv');   
      }
     public static String geneateCSVForTracsaction(Set<String> objectIdSet,String transactionType){
         String csvFileStr='';
         List<BTNetCRMCVSWrapper> btNetDataList = new List<BTNetCRMCVSWrapper>();  
         Map<String,String> dataSetMap = new Map<String,String>();
         try{
             dataSetMap = getMappingDataForCSV(objectIdSet,transactionType);
             System.debug('objectIdSet>>>'+objectIdSet);
            //  /System.debug('geneateCSVForTracsaction transactionType>>>'+transactionType);
             System.debug('dataSetMap>>>'+dataSetMap);
             if(!dataSetMap.isEmpty()){
             List<Data_Mapping__c> dataMappingList= [select Active__c,Column_Sequence__c,Display_Column_1__c,Display_Column_2__c,Transaction_Type__c
                                     from Data_Mapping__c where  Active__c = true and Transaction_Type__c includes (:transactionType)];
                 if(!dataMappingList.isEmpty()){
                     for(Data_Mapping__c dataMappingObj : dataMappingList){
                         BTNetCRMCVSWrapper btNetCRMCVSWrapperObj = new BTNetCRMCVSWrapper();
                         btNetCRMCVSWrapperObj.cloumnOrder = Integer.valueOf(dataMappingObj.Column_Sequence__c);
                         btNetCRMCVSWrapperObj.column1 = dataMappingObj.Display_Column_1__c;
                         btNetCRMCVSWrapperObj.column2 = dataMappingObj.Display_Column_2__c;
                         btNetCRMCVSWrapperObj.valuecolumn1 = dataSetMap.get(dataMappingObj.Display_Column_2__c.toLowerCase());
                         btNetDataList.add(btNetCRMCVSWrapperObj);
                     }
                 }
             }
             btNetDataList.sort();
             System.debug('btNetDataList>>>'+JSON.serializePretty(btNetDataList));
             csvFileStr +=getHeaderForTrasction(transactionType);
             for(BTNetCRMCVSWrapper btNetDataObj: btNetDataList){
                String fileRow = '';
                fileRow += btNetDataObj.column1==null ? '' : btNetDataObj.column1;
                fileRow += ',';
                fileRow += btNetDataObj.column2==null ? '' : btNetDataObj.column2;
                fileRow += ',';
                fileRow += btNetDataObj.valueColumn1==null ? '' : btNetDataObj.valueColumn1;
                System.debug('geneateCSVForTracsaction fileRow>>>'+fileRow);
               // fileRow = fileRow.replaceFirst(',','');
                csvFileStr = csvFileStr + fileRow + '\n';
            }
            System.debug('geneateCSVForTracsaction transactionType>>>'+transactionType);
            System.debug('btNetDataList>>>'+JSON.serializePretty(btNetDataList));
            System.debug('csvFileStr>>>'+JSON.serializePretty(csvFileStr));
             
         }catch(Exception e){
             
         }
         return csvFileStr;
    }
     
     public static String getHeaderForTrasction(String transactionType){
         String headerStr='';
         if(transactionType.equalsIgnoreCase('BTNet CRM ECC Mapping')){
            headerStr +='Pre-Order Survey Request'+ '\n';                          
            headerStr +='All field are mandatory unless otherwise indicated and must be populated. Failure to do so will result in order being rejected.'+ '\n\n';                     
         }                       
         return headerStr;
         
     }
     
     webservice static void sendECCCRFAttachments(String crfrecordID){
         try{
             List<CRF_Attachment__c> eccAttachmentlist = [select id,Name,Customer_Requirements_Form__r.Opportunity__r.Owner.Name ,customer_Requirements_Form__r.Opportunity__r.Owner.email, Customer_Requirements_Form__r.Opportunity__r.Opportunity_Id__c, Customer_Requirements_Form__r.Account__c,Customer_Requirements_Form__c, description__c, active__c,Attachment_Type__c from CRF_Attachment__c where Attachment_Type__c='BTNet CRM ECC Mapping' and Customer_Requirements_Form__c= :crfrecordID ];
             if(!eccAttachmentlist.isEmpty()){
                List<Attachment> existingattachmentlist = [select id,name,body from Attachment where parentId = :eccAttachmentlist[0].id];
                if(!existingattachmentlist.isEmpty()){
                    sendAttachmentinEmail(existingattachmentlist[0].body,existingattachmentlist[0].name,eccAttachmentlist[0].Customer_Requirements_Form__r.Opportunity__r.Owner.email, eccAttachmentlist[0].Customer_Requirements_Form__r.Opportunity__r.Owner.Name, eccAttachmentlist[0].Customer_Requirements_Form__r.Opportunity__r.Opportunity_Id__c, eccAttachmentlist[0].Customer_Requirements_Form__r.Account__c);
                }
             }
         }catch(Exception e){
                 
         }
         
     }
      public static void sendAttachmentinEmail(Blob attchmentCSV,String csvName, String ownerEmail, String ownerName,String OID, String AccountName){      
         
        Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
        //Blob csvBlob = blob.valueOf(attchmentCSV);
        //String csvName = 'BasketData1.csv';
        csvAttachment.setFileName(csvName);
        csvAttachment.setBody(attchmentCSV);
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{System.Label.BTNet_ESS_Email_Recipient};
        String[] ccAddresses = new String[] {ownerEmail};
        String subject = 'BTnet CPQ – OSAS Pre Order Survey Request ('+OID+')';
        email.setSubject(subject);
        email.setToAddresses(toAddresses);
        email.setCCAddresses(ccAddresses);
        email.setHtmlBody(getEmailBody(ownerName,OID,AccountName));
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment});
        Messaging.SendEmailResult[] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[]{email});         
         
     }
     
     public static String getEmailBody(String ownerName, String OID, String AccountName){
         String bodyStr= '<p>'+OID+'</p>'+
        '<p>Submitting Sales Name: '+ownerName+'</p>'+
        '<p>Account Name: '+AccountName+'</p>'+
        '<p>&nbsp;</p>'+
        '<p>Hi UK Ethernet Ordering Centre of Excellence (CoE),</p>'+
        '<p>&nbsp;</p>'+
        '<p>Please find attached the OSAS Pre Order Survey for the following BTnet order from CPQ.</p>'+
        '<p>&nbsp;</p>'+
        '<p>Kind Regards,</p>'+
        '<p>Salesforce Admin</p>';
        return bodyStr;
     }
     
}