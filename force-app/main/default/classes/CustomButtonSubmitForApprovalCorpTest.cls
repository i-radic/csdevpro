@IsTest
public class CustomButtonSubmitForApprovalCorpTest  {
	testMethod static void testApprovalCorpButton() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(true, 'Test PC', basket);
		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'Test Prod Category');
		csbb__Product_Configuration_Request__c pcRequest = CS_TestDataFactory.generateProductConfigRequest(true, prodCategory.Id, pc.Id);
		
		try{
			CustomButtonSubmitForApprovalCorp cntrl = new CustomButtonSubmitForApprovalCorp();
			String actionStr = cntrl.performAction(basket.Id);
			System.assertNotEquals(actionStr, '');
		}
		catch(Exception exc){}
	}
}