/**
 * @name CS_Utl_Array
 * @description Utility class for Array
 * @revision
 *
 */
public class CS_Utl_Array {
    /**
     * Join list of Strings to String separated by given separator
     * @param  values
     * @param  separator
     * @return String
     */
	public static String join(List<String> values, String separator) {
        String ret = '';

        if(values != null){
        	for(String s : values) {
            	ret += s + separator;
        	}
        }

        return (String.isBlank(ret)) ? ret : ret.substring(0, ret.length() - separator.length());
    }

    /**
     * Join list of Strings to String separated by given separator
     * @param  values
     * @param  separator
     * @return String
     */   
    public static String join(Set<String> values, String separator) {
    	return join(new List<String>(values), separator);
    }
}