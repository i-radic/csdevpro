@isTest
private class CampaignXdayEndateUpdateController_Test {

    static Campaign c;
    static Task t1;
    static Task t2;
    static User uUser;
    static Contact contact;
    static Account a;
    
    static{
        //Data setup - start
        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User thisUser = [select id from User where id=:userinfo.getUserid()];
	     System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
        
        a = Test_Factory.CreateAccount();
        a.name = 'TESTCODE 2';
        a.sac_code__c = 'testSAC';
        a.Sector_code__c = 'CORP';
        a.LOB_Code__c = 'LOB';
        a.OwnerId = u1.Id;
        a.CUG__c='CugTest';
        insert a;
         }
        contact = Test_Factory.CreateContact();
        contact.Phone = '01234 567890';
        contact.AccountId = a.Id;
        insert contact;
        
        List<Campaign_Call_Status__c> ccsToInsert = new List<Campaign_Call_Status__c>();
        Campaign_Call_Status__c ccs1 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status1', Responded__c = true, Default__c=true);
        Campaign_Call_Status__c ccs2 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status2', Responded__c = false, Default__c=false);
        ccsToInsert.add(ccs1);
        ccsToInsert.add(ccs2);
        insert ccsToInsert;
        
        //Insert Test
        Date enddate = date.today() + 1;
        c = new Campaign(Name='TestCampaign', Type='Telemarketing', Status='Planned', 
            Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, isActive=true, Data_Source__c='Self Generated');
        insert c;
        
        CampaignMember cm = new CampaignMember(CampaignId=c.Id, ContactId=contact.Id);
        insert cm;
  
        //RecordType rt = [select id from RecordType where Id=:StaticVariables.campaign_task_record_type];
        RecordType rt = [select id from RecordType where DeveloperName='Campaign_Task'];
        System.assert(rt!= null);
        
        t1 = new Task(RecordType = rt, Status = 'Not Started', WhatId=c.Id,Type='Call', WhoId=contact.Id);
        insert t1;
        t1 = [select id, X_Day_Rule_Copy__c, activitydate from Task where Id = :t1.id];
        System.assert(t1 != null && t1.X_Day_Rule_Copy__c == 3 && t1.activitydate == date.today() + 1);
        
        t2 = new Task(RecordType = rt, Status = 'Completed', WhatId=c.Id, X_Day_Rule_Copy__c= 3, Subject='Call', Type='Call', WhoId=contact.Id, Call_Status__c='Sale Made', Description='blaaaa');
        insert t2;
        t2 = [select id, X_Day_Rule_Copy__c, activitydate, WhatId, Status from Task where Id = :t2.id];
        System.assert(t2.X_Day_Rule_Copy__c == 3);
        
        //Create User and when Campaign Update check their X Day rule value is updated
        Profile pProfile = [select id from profile where name='Standard User'];
        uUser = new User(alias = 'tst2', email='tst287@bt.it', 
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US', 
            localesidkey='en_US', profileid = pProfile.Id, timezonesidkey='Europe/London', username='tst2@testemail.com',
            EIN__c='tst2');
            
        insert uUser;   
        //Data Setup - END
    }
    
    static testMethod void updateCampaignCheck(){
               
        c.X_Day_Rule__c = 33;
        c.EndDate = date.today() + 3;
        update c;
        
        Test.startTest();   
        PageReference pageRef = Page.CampaignXdayEndDateUpdate;
        Test.setCurrentPage(pageRef);
        
        
        
        //####### TEST 1 ######
        //Test invalid Campaign Id in the page.
        ApexPages.currentPage().getParameters().put('id', 'xxxxxxxxxxxxxx');
        CampaignXdayEndateUpdateController controller = new CampaignXdayEndateUpdateController();
        controller.runBatchJobs();
        //Should find ERROR message
        System.assert(ApexPages.hasMessages(ApexPages.Severity.ERROR));
        
        //####### TEST 2 ######
        //Check batch jobs have run and updated relevant Users and Tasks - with altered Campaign X day and Enddate
        ApexPages.currentPage().getParameters().put('id', c.Id);
        controller = new CampaignXdayEndateUpdateController();
        controller.runBatchJobs();
        //System.assert(ApexPages.hasMessages(ApexPages.Severity.INFO));
        Test.stopTest();
    /*    
        t1 = [select Id, X_Day_Rule_Copy__c, activitydate from Task where Id = :t1.Id];
        //Task should have been updated as Open
        System.assert(t1.X_Day_Rule_Copy__c == 33 && t1.activitydate == date.today() + 3);
        t2 = [select Id, X_Day_Rule_Copy__c, activitydate from Task where Id = :t2.Id];
        //Task should NOT have been updated as Closed
        System.assert(t2.X_Day_Rule_Copy__c == 3);
    */
               
    }
}