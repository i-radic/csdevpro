global class AgedDebtDeleteBatch implements Database.Batchable<sobject>{

 public String query;
 public String rowslmt;
 
 global AgedDebtDeleteBatch (String lmt){
   rowslmt = lmt;
 }
 
  global AgedDebtDeleteBatch (){}
 
 global Database.QueryLocator start(Database.BatchableContext BC){
    Date todayDate=System.now().date();
    Date DT35=todayDate-35;
    DateTime LastModDateForDT35=datetime.newInstance(DT35.Year(), DT35.month(), DT35.day(), 23, 59, 59);
    query = 'Select Id from Aged_Debt__c where LastModifiedDate < : LastModDateForDT35';
    if(rowslmt != null)
      query+= ' limit '+ rowslmt;
       

    return Database.getQueryLocator(query);
  }
  
 global void execute(Database.BatchableContext BC,List<Aged_Debt__c> AD){ 
    Delete AD;
 }
 
 global void finish(Database.BatchableContext BC){
 }
 
}