@IsTest
public class CS_BeforeSendDiscountObserverTest {
	private static testMethod void testBeforeSendDiscountObserverTest() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'test', opp);
		cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

		CS_DiscountJSON dJSON = new CS_DiscountJSON();
		CS_DiscountJSON.MemberDiscount memberDisc = new CS_DiscountJSON.MemberDiscount();

		memberDisc.amount = '5';
		memberDisc.chargeType = 'recurring';
		memberDisc.description = 'Test Discount';
		memberDisc.discountCharge = 'recurring';
		memberDisc.source = 'Test Source';
		memberDisc.type = 'absolute';

		dJSON.discounts = new List<CS_DiscountJSON.Discount>();

		CS_DiscountJSON.Discount disc = new CS_DiscountJSON.Discount();
		disc.memberDiscounts = new List<CS_DiscountJSON.MemberDiscount>();
		disc.memberDiscounts.add(memberDisc);

		dJSON.discounts.add(disc);
		system.debug(dJSON);
		String jsonString = JSON.serialize(dJSON);


		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
		pc.cscfga__Product_Definition__c = pd.ID;
		pc.cscfga__Product_Family__c = 'BT Mobile Sharer';
		//pc.cscfga__discounts__c = jsonString;
		pc.Volume__c = 10;
        pc.Calculations_Product_Group__c = 'Future Mobile';
		INSERT pc;


		csdiscounts.ConfigurationQueryResult.Discount configQueryDiscounts = new csdiscounts.ConfigurationQueryResult.Discount(
			'absolute', 'test','test','test', 'test', 40.5, pc.Id
		);

		csdiscounts.FieldDetails fieldDetailsVolume = new csdiscounts.FieldDetails('Volume__c', 'Volume__c');
		fieldDetailsVolume.value = '10';
		fieldDetailsVolume.name = 'Volume__c';
		csdiscounts.FieldDetails fieldDetailsQueryResult = new csdiscounts.FieldDetails('Volume__c', 'Volume__c');
		fieldDetailsQueryResult.value = '10';
		fieldDetailsQueryResult.name = 'Volume__c';


		csdiscounts.ObjectDetails objectDetail = new csdiscounts.ObjectDetails(new List<csdiscounts.FieldDetails>{
				fieldDetailsVolume
		}, pc);


		csdiscounts.ConfigurationQueryResult configurationQueryResult = new csdiscounts.ConfigurationQueryResult(
				new List<csdiscounts.FieldDetails>{
						fieldDetailsQueryResult
				},
				new List<csdiscounts.ObjectDetails>{
						objectDetail
				},
				null);

		List<csdiscounts.ConfigurationQueryResult.ConfigurationObjectDetails> discObjectDetails = new List<csdiscounts.ConfigurationQueryResult.ConfigurationObjectDetails>();
		discObjectDetails =  configurationQueryResult.objectDetails;

		discObjectDetails.get(0).discounts = new List<csdiscounts.ConfigurationQueryResult.Discount>{configQueryDiscounts};

		CS_BeforeSendDiscountObserver cmtrl = new CS_BeforeSendDiscountObserver();
		Test.startTest();
		csdiscounts.ConfigurationQueryResult queryResult = cmtrl.execute(basket.Id, configurationQueryResult);
		Test.stopTest();
	}

	private static testMethod void testBeforeSendDiscountObserverTest2() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();

		Account acc = CS_TestDataFactory.generateAccount(true, 'test');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'test', opp);
		cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');

		cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'test', basket);
		pc.cscfga__Product_Definition__c = pd.ID;
		pc.cscfga__Product_Family__c = 'Future Mobile';
		pc.Volume__c = 10;
        pc.Calculations_Product_Group__c = 'Future Mobile';
		INSERT pc;

		CS_BeforeSendDiscountObserver cmtrl = new CS_BeforeSendDiscountObserver();
		Test.startTest();
		cmtrl.execute(basket.Id, null);
		Test.stopTest();
	}
}