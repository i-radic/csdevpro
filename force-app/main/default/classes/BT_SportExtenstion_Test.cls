@isTest(SeeAllData=true)
private class BT_SportExtenstion_Test {
	
	testmethod static void Test_BT_SportExtenstion(){
        Test_Factory.setProperty('IsTest', 'yes');

        StaticVariables.setContactIsRunBefore(False);     
        
        RecordType BTSOppyType = [select id from RecordType where SobjectType='Opportunity' and name ='BT Sport' limit 1];
        RecordType BTSHeaderType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='Header' limit 1];
        RecordType BTSNonMSAType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='non MSA' limit 1];
        RecordType BTSMSAType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='MSA' limit 1];
        
        Date uDate = date.today().addDays(7);
                
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BTS';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.RecordTypeId = BTSOppyType.Id;
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});
        
        Contact cnt = new Contact();
        cnt.LastName = 'Test LastName';
        Database.SaveResult[] cntResult = Database.insert(new Contact [] {cnt});
        
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cntResult[0].Id;
        ocr.OpportunityId = oppResult[0].Id;
        ocr.Role = 'Deleloper';
        Database.SaveResult[] ocrResult = Database.insert(new OpportunityContactRole [] {ocr});   
              
        BT_Sport__c Header1 = new BT_Sport__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Header1.RecordTypeId = BTSHeaderType.id;
        Header1.BT_Sport_MSA_Oppy__c = 'No';
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport__c [] {Header1});        
              
        BT_Sport__c Site1 = new BT_Sport__c();
        Site1.BT_Sport_Header__c = Header1Result[0].id; 
        Site1.RecordTypeId = BTSNonMSAType.id;
        Site1.Site_Type__c = 'BT Sport Pack Pub (GB)';
        Site1.Contract_Type__c = '12 month contract';
        Site1.Band__c = 'Band A - £0 - £5,000';
        Site1.Discounts__c = '';// '10 percent discount special offer';
        Site1.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site1.Site_Display_Name__c = 'TESTER';
        Site1.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site1Result = Database.insert(new BT_Sport__c [] {Site1});
        /*
        BT_Sport__c Site1a = new BT_Sport__c();
        Site1a.BT_Sport_Header__c = Header1Result[0].id; 
        Site1a.RecordTypeId = BTSNonMSAType.id;
        Site1a.Site_Type__c = 'BT Sport Pack Hotel Rooms';
        Site1a.Contract_Type__c = '12 month contract';
        Site1a.Band__c = '';
        Site1a.Discounts__c = '';
        Site1a.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site1a.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site1a.Site_Display_Name__c = 'TESTER';
        Site1a.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site1aResult = Database.insert(new BT_Sport__c [] {Site1a});  
        */
        BT_Sport__c Site1b = new BT_Sport__c();
        Site1b.BT_Sport_Header__c = Header1Result[0].id; 
        Site1b.RecordTypeId = BTSNonMSAType.id;
        Site1b.Site_Type__c = 'BT Sport Pack General';
        Site1b.Contract_Type__c = '12 month contract';
        Site1b.Band__c = '';
        Site1b.Discounts__c = '';
        Site1b.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site1b.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site1b.Site_Display_Name__c = 'TESTER';
        Site1b.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site1bResult = Database.insert(new BT_Sport__c [] {Site1b});  
        
        BT_Sport__c Header2 = new BT_Sport__c();
        Header2.Opportunity__c = oppResult[0].id; 
        Header2.RecordTypeId = BTSHeaderType.id;
        Header2.BT_Sport_MSA_Oppy__c = 'No';
        Header2.BT_Sport_General_MSA_Discount__c = '10';
        Header2.BT_Sport_Cat_2_Pub_MSA_Discount__c = '20';
        Header2.BT_Sport_General_MSA_Term__c = '1 Year';
        Header2.BT_Sport_Cat_2_MSA_Term__c = '1 Year';
        Database.SaveResult[] Header2Result = Database.insert(new BT_Sport__c [] {Header2});        
              
        BT_Sport__c Site2 = new BT_Sport__c();
        Site2.BT_Sport_Header__c = Header2Result[0].id; 
        Site2.RecordTypeId = BTSNonMSAType.id;
        Site2.Site_Type__c = 'BT Sport Pack Pub (GB)';
        Site2.Contract_Type__c = '12 month contract';
        Site2.Band__c = 'Band A - £0 - £5,000';
        Site2.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site2.Site_Display_Name__c = 'TESTER';
        Site2.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site2Result = Database.insert(new BT_Sport__c [] {Site2});  
                 
        BT_Sport__c Site3 = new BT_Sport__c();
        Site3.BT_Sport_Header__c = Header2Result[0].id; 
        Site3.RecordTypeId = BTSNonMSAType.id;
        Site3.Site_Type__c = 'BT Sport Pack Pub (GB)';
        Site3.Contract_Type__c = '12 month contract';
        Site3.Band__c = 'Band A - £0 - £5,000';
        Site3.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site3.Site_Display_Name__c = 'TESTER';
        Site3.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site3Result = Database.insert(new BT_Sport__c [] {Site3});  
       /* 
        BT_Sport__c Site4 = new BT_Sport__c();
        Site4.BT_Sport_Header__c = Header2Result[0].id; 
        Site4.RecordTypeId = BTSNonMSAType.id;
        Site4.Site_Type__c = 'BT Sport Pack Hotel Bar';
        Site4.Contract_Type__c = '12 month contract';
        Site4.Band__c = 'Band A - 1 rooms - 10 rooms';
        Site4.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site4.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site4.Site_Display_Name__c = 'TESTER';
        Site4.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site4Result = Database.insert(new BT_Sport__c [] {Site4});  
*/
        
        BT_Sport__c Header3 = new BT_Sport__c();
        Header3.Opportunity__c = oppResult[0].id; 
        Header3.RecordTypeId = BTSHeaderType.id;
        Header3.BT_Sport_MSA_Oppy__c = 'Yes';
        Header3.BT_Sport_General_MSA_Discount__c = '0';
        Header3.BT_Sport_Cat_2_Pub_MSA_Discount__c = '0';
        Header3.BT_Sport_General_MSA_Term__c = '1 Year';
        Header3.BT_Sport_Cat_2_MSA_Term__c = '1 Year';
        Database.SaveResult[] Header3Result = Database.insert(new BT_Sport__c [] {Header3});        
              
        BT_Sport__c Site3a = new BT_Sport__c();
        Site3a.BT_Sport_Header__c = Header3Result[0].id; 
        Site3a.RecordTypeId = BTSNonMSAType.id;
        Site3a.Site_Type__c = 'BT Sport Pack Pub (GB)';
        Site3a.Contract_Type__c = '12 month contract';
        Site3a.Band__c = 'Band A - £0 - £5,000';
        Site3a.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site3a.Site_Display_Name__c = 'TESTER';
        Site3a.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site3aResult = Database.insert(new BT_Sport__c [] {Site3a});
        /*
        BT_Sport_Viewing_Card__c s1vc1 = new BT_Sport_Viewing_Card__c();
        s1vc1.BT_Sport__c = Site1aResult[0].Id;
        s1vc1.Customer_has_Sky_Viewing_Card__c = 'Yes';
        s1vc1.is_this_part_of_a_SMATV_installation__c = 'Yes';
        s1vc1.Sky_viewing_card_number__c = '123456789';
        s1vc1.Telephone_number__c = '1234567890';
        s1vc1.Type_of_sky__c = 'Sky';
        Database.SaveResult[] s1vc1Result = Database.insert(new BT_Sport_Viewing_Card__c [] {s1vc1});
        
        
        BT_Sport_Viewing_Card__c s1vc2 = new BT_Sport_Viewing_Card__c();
        s1vc2.BT_Sport__c = Site1aResult[0].Id;
        s1vc2.Customer_has_Sky_Viewing_Card__c = 'Yes';
        s1vc2.is_this_part_of_a_SMATV_installation__c = 'Yes';
        s1vc2.Sky_viewing_card_number__c = '123456789';
        s1vc2.Telephone_number__c = '1234567890';
        s1vc2.Type_of_sky__c = 'Sky';
        Database.SaveResult[] s1vc2Result = Database.insert(new BT_Sport_Viewing_Card__c [] {s1vc2});
        
        BT_Sport_Viewing_Card__c s1vc3 = new BT_Sport_Viewing_Card__c();
        s1vc3.BT_Sport__c = Site1aResult[0].Id;
        s1vc3.Customer_has_Sky_Viewing_Card__c = 'Yes';
        s1vc3.is_this_part_of_a_SMATV_installation__c = 'Yes';
        s1vc3.Sky_viewing_card_number__c = '123456789';
        s1vc3.Telephone_number__c = '1234567890';
        s1vc3.Type_of_sky__c = 'Sky';
        Database.SaveResult[] s1vc3Result = Database.insert(new BT_Sport_Viewing_Card__c [] {s1vc3});
        */     
        BT_Sport_Viewing_Card__c vc1 = new BT_Sport_Viewing_Card__c();
        vc1.BT_Sport__c = Site3aResult[0].Id;
        vc1.Customer_has_Sky_Viewing_Card__c = 'Yes';
        vc1.is_this_part_of_a_SMATV_installation__c = 'Yes';
        vc1.Sky_viewing_card_number__c = '123456789';
        vc1.Telephone_number__c = '1234567890';
        vc1.Type_of_sky__c = 'Sky';
        Database.SaveResult[] vc1Result = Database.insert(new BT_Sport_Viewing_Card__c [] {vc1});
        
        BT_Sport_Viewing_Card__c vc2 = new BT_Sport_Viewing_Card__c();
        vc2.BT_Sport__c = Site3aResult[0].Id;
        vc2.Customer_has_Sky_Viewing_Card__c = 'Yes';
        vc2.is_this_part_of_a_SMATV_installation__c = 'Yes';
        vc2.Sky_viewing_card_number__c = '123456789';
        vc2.Telephone_number__c = '1234567890';
        vc2.Type_of_sky__c = 'Sky';
        Database.SaveResult[] vc2Result = Database.insert(new BT_Sport_Viewing_Card__c [] {vc2});
        
        BT_Sport_Viewing_Card__c vc3 = new BT_Sport_Viewing_Card__c();
        vc3.BT_Sport__c = Site3aResult[0].Id;
        vc3.Customer_has_Sky_Viewing_Card__c = 'Yes';
        vc3.is_this_part_of_a_SMATV_installation__c = 'Yes';
        vc3.Sky_viewing_card_number__c = '123456789';
        vc3.Telephone_number__c = '1234567890';
        vc3.Type_of_sky__c = 'Sky';
        Database.SaveResult[] vc3Result = Database.insert(new BT_Sport_Viewing_Card__c [] {vc3});
        
        ApexPages.StandardController sc = new ApexPages.standardController(Header1);
        BT_SportExtenstion objBT_SportExtenstion = new BT_SportExtenstion(sc);
   }
}