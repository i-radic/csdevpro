public class OVFormatter {
    public static string FormatTitle(string title){
        return title == 'Mr' ? title + '.' : (title == 'Mrs' ? title + '.' : (title == 'Professor' ? title + '.' : (title == 'Dr' ? title + '.' : title))); 
    }
    
    public static string FormatTelNo(string telNo) {
        if (telNo == '' || telNo == null){
            return telNo;
        }
        telNo = telNo.Replace('(0)', '0');
        telNo = telNo.Replace(' ', '');
        
        if (telNo.StartsWith('+440')){
            return telNo;
        }
        
        if (telNo.StartsWith('+44')){
            return telNo.Replace('+44', '+440');
        }
        
        if (telNo.StartsWith('0')){
            system.debug('**********'+telNo);
           // return '+44' + telNo;
            return telNo;
            
        } else {
            system.debug('**@@@@@@@@@*****'+telNo);
            return '+440' + telNo;
        }
    }
    public static string FormatConsent(string value) {
        return value == 'MPS' ? 'No' : (value == 'TPS' ? 'No' : (value == 'FPS' ? 'No' : value)); 
    }
    
    public static string FormatPreferredContactChannel(string value) {
        return value == 'Phone' ? 'Work' : (value == 'Letter' ? 'Alternate' : (value == 'Mail' ? 'Alternate' : value));
    }
    
    public static string FormatContactStatus(string value){
        if (value == 'InActive'){
            value = 'Inactive';
        }
        return value;
    }   
}