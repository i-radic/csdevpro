/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_SalesforceServicesBuildRequestXML {

    static testMethod void myUnitTest() {
        
        SalesforceServicesBuildRequestXML BuildRequestXML = new SalesforceServicesBuildRequestXML();
          
        SalesforceCRMService.CRMAddress address = new SalesforceCRMService.CRMAddress();
        address.IdentifierId = 'test';
        address.IdentifierName = 'test';
        address.IdentifierValue = 'test';
        address.Country = 'test';
        address.County = 'test';
        address.Name = 'test';
        address.POBox = '12345';
        address.BuildingNumber = '1';
        address.Street = 'test';
        address.Locality = 'test';
        address.DoubleDependentLocality = 'test';
        address.PostCode = '12345';
        address.Town = 'test';
        address.SubBuilding = 'test';
        address.ExchangeGroupCode = '12';
                 
        String result = SalesforceServicesBuildRequestXML.buildXMLCreateNADAddress(address);
        
        String result1 = SalesforceServicesBuildRequestXML.buildXMLAddressSearch(address.PostCode, 'OR');
        
        String result2 = SalesforceServicesBuildRequestXML.buildXMLCreateAddress(address);
        
        SalesforceCRMService.CRMContact contact = new SalesforceCRMService.CRMContact();
        contact.CUG = '0123456';
        contact.AccountManagerContact = '';        
        contact.ActiveFlag = 'Y';        
        contact.Comment = '';        
        contact.Type_x = '';        
        contact.ExternalId = '';        
        contact.Title = 'Mr';        
        contact.FirstName = 'FirstName';        
        contact.MiddleName = 'MiddleName';        
        contact.LastName = 'LastName';        
        contact.AliasName = 'AliasName';        
        contact.ContactSource = 'Salesforce';       
        contact.JobSeniority = '';        
        contact.Profession = '';        
        contact.DateOfBirth = DateTime.now();        
        contact.MotherMaidenName = '';        
        contact.Status = 'Active';        
        contact.JobTitle = '';        
        contact.KeyDecisionMaker = '';        
        contact.FaxNumber = '';        
        //contact.Organization = '';        
        contact.Department = '';        
        contact.RightToContact = '';        
        contact.PreferredContactChannel = '';        
        contact.SecondaryPreferredContactChannel = '';        
        contact.EmailAddress = 'test@crmtest.com';        
        contact.EmailAddressId = '3';  
        contact.EmailAddress1 = 'test@crmtest.com';        
        contact.Email1IntegrationId = '3';
        contact.EmailAddress2 = 'test@crmtest.com';        
        contact.Email2IntegrationId = '3';
        contact.EmailAddress3 = 'test@crmtest.com';        
        contact.Email3IntegrationId = '3';
        contact.Voice1 = '3';        
        contact.Voice1Id = '3';
        contact.Voice2 = '3';        
        contact.Voice2Id = '3';
        contact.Voice3 = '3';        
        contact.Voice3Id = '3';
        contact.VoiceType1 = 'Mobile';
        contact.VoiceType2 = 'Mobile';
        contact.VoiceType3 = 'Mobile';
        contact.EmailConsentPermission = '';        
        contact.EmailConsentDate = DateTime.now();        
        contact.EmailConsentType = '';        
        contact.HomePhoneId = '';        
        contact.HomePhone = '';        
        contact.HomePhoneConsentPermission = '';        
        contact.HomePhoneConsentDate = DateTime.now();        
        contact.HomePhoneConsentType = '';        
        contact.MobilePhoneId = '';        
        contact.MobilePhone = '0123456789';        
        contact.MobilePhoneConsentPermission = '';
		contact.MobilePhoneSMSConsentPermission = '';        
        contact.MobilePhoneConsentDate = DateTime.now();        
        contact.MobilePhoneConsentType = '';        
        contact.FaxId = '';        
        contact.Fax = '0123456789';        
        contact.FaxConsentPermission = '';        
        contact.FaxConsentDate = DateTime.now();        
        contact.FaxConsentType = '';        
        contact.WorkPhoneId = '';        
        contact.WorkPhone = '0123456789';        
        contact.WorkPhoneConsentPermission = '';        
        contact.WorkPhoneConsentDate = DateTime.now();        
        contact.WorkPhoneConsentType = '';        
        contact.AddressId = '';        
        contact.AddressNumber = '';        
        contact.AddressStreet = '';        
        contact.AddressLocality = '';        
        contact.AddressPostCode = '';        
        contact.AddressPostTown = '';        
        contact.AddressSector = '';        
        contact.AddressSubBuilding = '';        
        contact.AddressZipCode = '';        
        contact.AddressConsentPermission = '';        
        contact.AddressConsentDate = DateTime.now();        
        contact.AddressTimeAt = '';        
        contact.AddressConsentType = '';
        contact.PostConsent = 'Not Asked';
        contact.EmailConsent = 'Not Asked';
        contact.PhoneConsent= 'Not Asked';
        contact.SMSConsent= 'Not Asked';
        contact.AutomatedPhonecallsConsent= 'Not Asked';
        contact.PostConsent= 'Not Asked'; 
        contact.ProfileConsent= 'Not Asked';
        contact.DigitalMarketingConsent= 'Not Asked';
        
        contact.AutomatedPhonecallsConsentDate=DateTime.Now();       
        contact.ProfilingConsentDate=DateTime.Now();
        contact.DigitalMarketingConsentDate=DateTime.Now();
        contact.Email1Delete = 'Not Asked';
        contact.Email2Delete = 'Not Asked';
        contact.Email3Delete = 'Not Asked';
        contact.Voice1Delete = 'Not Asked';
        contact.Email1IntegrationId = '012345678912';
        contact.Email2IntegrationId = '012345678913';
        contact.Email3IntegrationId = '012345678914';
        contact.Email1DeleteIntegrationId = '012345678912';
        contact.Email2DeleteIntegrationId = '012345678913';
        contact.Email3DeleteIntegrationId = '012345678914';
        contact.VoiceType1Delete = 'close';
        contact.Voice1DeleteIntegrationID = '012345678915';
        
        String result3 = SalesforceServicesBuildRequestXML.buildXMLManageContact(contact);
        
        SalesforceCRMService.BTLBEventInteraction btlbEvent = null;
        btlbEvent = new SalesforceCRMService.BTLBEventInteraction();
        btlbEvent.EventId = '';
        btlbEvent.CUG = '';
        btlbEvent.RecordId = '';
        btlbEvent.agentEIN = '';
        btlbEvent.interactionIntegrationId = '';
        
        String result4 = SalesforceServicesBuildRequestXML.buildXMLEventInteraction(btlbEvent);
        
        String result5 = SalesforceServicesBuildRequestXML.buildXMLLinkContactToAccount(contact);
        
        String result6 = SalesforceServicesBuildRequestXML.buildXMLUpdateContact(contact);
        
        String result7 = SalesforceServicesBuildRequestXML.MPA1_NL_xml('SW19 1AA','','','');
        String result8 = SalesforceServicesBuildRequestXML.MPA2_ProvLineQlty_xml('SW19 1AA','','','GEA');
        String result9 = SalesforceServicesBuildRequestXML.MPA2_ProvLineQlty_xml('SW19 1AA','','','');
        String result10 = SalesforceServicesBuildRequestXML.MPA3_GetECC_xml('','');
    }
    
  
}