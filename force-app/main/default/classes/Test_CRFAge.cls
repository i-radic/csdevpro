@isTest
private class Test_CRFAge {
    static testMethod void myUnitTest() {
        //  set up data
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99991';
        insert a;
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o; 
        CRF__c crf = new CRF__c(Opportunity__c = o.Id);
        crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Movers'].Id;
        crf.Contact__c = c.Id;
        crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
        insert crf;
        crf.Back_Office_Status__c = 'Order Issued';
        update crf;        
        crf.Back_Office_Status__c = 'In progress';
        update crf;
        
        }
        
        
}