/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(Seealldata=True)
private class Test_EvEV_Upload_ArtemisController {

    static testMethod void myUnitTest() {
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
            
        Account acct = new Account(name='test account');
        insert acct;
        Opportunity op = new Opportunity(AccountId=acct.Id,StageName='Prospecting',Brand__c='',name='test Opportunity',CloseDate=date.newinstance(2009,6,6));
        insert op;
        
        Artemis_Deal__c deal= new Artemis_Deal__c(Brand__c='Orange',Opportunity__c=op.id,Primary_Deal__c=false,Process_Status__c='Processed',Voice_Users__c=10,Blackberry_DO_Users__c=20,Mobile_Broadband_DO_Users__c=30,Tablet_DO_Users__c=40,Fleetlink_Users__c=50,Total_M2M_Users__c=60);
        //Artemis_Deal__c deal= new Artemis_Deal__c(Opportunity__c=op.id,Primary_Deal__c=false,Process_Status__c='Processed',Voice_Users__c=10,Blackberry_DO_Users__c=20,Mobile_Broadband_DO_Users__c=30,Tablet_DO_Users__c=40,Fleetlink_Users__c=50,Total_M2M_Users__c=60);
        deal.Mobile_Outgoing_Voice_Revenue__c=100;
        deal.Data_Only_Blackberry_Revenue__c=200;
        deal.Data_Only_Mobile_Broadband_Revenue__c=300;
        deal.Data_Only_Tablet_Revenue__c=400;
        deal.Fleetlink_Revenue__c=500;
        deal.M2M_Revenue__c=600;
        //insert deal;
     
        Test.startTest();
        PageReference requestPage = new PageReference('/apex/EvEv_Upload_Artemis_Deal');
        Test.setCurrentPage(requestPage);
        ApexPages.StandardController stdController = new ApexPages.StandardController(op);
        EvEv_Upload_Artemis_Controller uploadController=new EvEv_Upload_Artemis_Controller(stdController);
        System.assert(uploadController.oldPrimaryDeal=='');
        
        
        uploadController.upload();
        uploadController.strMyDealId='testDeal';
        uploadController.upload();
        uploadController.document=Blob.valueOf('This is the content');
        uploadController.filename='TestFile.xls';
        uploadController.upload();
        
        
        
        insert deal;
        uploadController.updatePrimaryDeal();
        uploadController.primaryDeal=deal.Id;
        uploadController.updatePrimaryDeal();
        
        System.assert(uploadController.oldPrimaryDeal==deal.Id);
        
        uploadController.goToDealDetailPage();
        uploadController.backToOpportunity();
        
        uploadController.strMyDealId='testDeal2';
        uploadController.document=Blob.valueOf('This is the another content');
        uploadController.filename='TestFile2.xlsx';
        uploadController.upload();
        
        System.assert(uploadController.oldPrimaryDeal!='');
        
        Test.stopTest();
        
    }
}