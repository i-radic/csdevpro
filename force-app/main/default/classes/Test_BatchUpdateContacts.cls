@isTest
private class Test_BatchUpdateContacts{

  static testMethod void myUnitTest() {
      User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
      Test.startTest();
      Profile p= [select id from profile where name='Field: Standard User'];
      User u1=[select id from User where name='OTHER Default Account Owner'];
      User u;  
      u= new User(alias='asd',email='testing@bt.it',EIN__c='87fgter',emailencodingkey='UTF-8',lastname='Testingasd', languagelocalekey='en_US',                        localesidkey='en_US',isActive=True,isActive2__c='True',Department='TestSupport', profileid = p.Id, timezonesidkey='Europe/London', username='testingasd@bt.it');                  
      insert u;
      Account retval = new Account(id = null);        
      retval.Name = 'tst_Account';        
      retval.LOB_Code__c = 'tst_lob';                
      retval.SAC_Code__c = 't_sac';        
      retval.LE_Code__c = 'tst_le6789';        
      retval.AM_EIN__c = '802537216';        
      retval.Sector_code__c = 'CORP';
      retval.OwnerId=u.Id;       
      retval.Base_Team_Assign_Date__c=System.today();
      insert retval;
      Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',AccountId=retval.id,Email='test.mail@bt.it');
      insert myContact;
      String query = 'Select Id From Contact Limit 200';
      BatchUpdateContacts b=new BatchUpdateContacts(query);
      Database.executeBatch(b,200);
      Test.stopTest();
      
  }
  }