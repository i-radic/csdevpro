@isTest
private class Test_TaskContactId{

    static testMethod void testCreateUpdateTaskContactId(){
        Task t1 = Test_Factory.CreateTask();
        insert t1;
        
        // Update task
        t1.reminder_days__c = 10;
        update t1;
    }
}