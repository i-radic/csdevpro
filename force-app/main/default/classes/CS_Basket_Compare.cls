public with sharing class CS_Basket_Compare {
    
    public string removeBasket {get; set;}
    
	public boolean showExportButton {get; set;}
    
    public set<string> basketParameter{get; set;}
    
    public cscfga__Product_Basket__c basket {get; set;}
    
    public integer noOfBasketFields {get; set;}
    public integer noOfConfgFields {get; set;}
    
    public map<string, string> basketFieldLabel {get; set;}
    
    public list<string> basketFieldsLst {get; set;} /* List of Product Basket fields need to be displayed as columns*/
    public list<string> prodConfgsLst {get; set;}/* List of Product Configuration fields need to be displayed as columns*/
    
    public string basketId;
    
    public list<selectOption> basketsList {get; set;}/*list of baskets displayed in multiselect picklist, when opportunity is changed*/
    public list<string> selectedBaskets {get; set;} /*selected basket ids from multiselect picklist*/
	
	public map<string, list<cscfga__Product_Configuration__c>> productConfigurations {get; set;}/*Basket id as key and, list of Product Configurations as values*/
	
	public map<string, set<cscfga__Product_Configuration__c>> firstLevelprodConfg {get; set;}
	public map<string, set<cscfga__Product_Configuration__c>> secondLevelprodConfg {get; set;}
	
    public list<cscfga__Product_Basket__c> basketsToCompare {get; set;} 
    
    public boolean noRecords;
    
    public boolean getnoRecords(){
    	if(basketsToCompare.size() > 0){
    		return true;
    	}else{
    		return false;
    	}
    }
    
    string oppId;
    string basketqry = 'select ';
    string prodConfgqry = 'select ';
    public CS_Basket_Compare(apexPages.standardController sc){
    	basketParameter = new set<string>();
    	basketFieldLabel = new map<string, string>(); 
    	
    	noRecords = false;
        basket = new cscfga__Product_Basket__c();
        basketsToCompare = new list<cscfga__Product_Basket__c>();
        productConfigurations = new map<string, list<cscfga__Product_Configuration__c>>();
        
        firstLevelprodConfg = new map<string, set<cscfga__Product_Configuration__c>>();
        secondLevelprodConfg = new map<string, set<cscfga__Product_Configuration__c>>();
        
        basketFieldsLst = new list<string>();
        prodConfgsLst = new list<string>();
        list<Schema.FieldSetMember> basketFields = sObjectType.cscfga__Product_Basket__c.FieldSets.cs_Fields_displayed_on_Basket_Compare.getFields();
        if(!basketFields.isEmpty()){
        	for(Schema.FieldSetMember fs : basketFields){
        		basketqry = basketqry + fs.getFieldPath() + ',';
        		basketFieldsLst.add(fs.getFieldPath());
        		basketFieldLabel.put(fs.getFieldPath(),fs.getLabel());
        	}
        	basketqry = basketqry + 'id,name from cscfga__Product_Basket__c where id=:selectedBaskets';
        }
        noOfBasketFields = basketFieldsLst.size() + 3;
        
        list<Schema.FieldSetMember> prodConfgsfields = sObjectType.cscfga__Product_Configuration__c.FieldSets.cs_Fields_displayed_on_Basket_Compare.getFields();
        if(!prodConfgsfields.isEmpty()){
        	for(Schema.FieldSetMember fs : prodConfgsfields){
        		prodConfgqry = prodConfgqry + fs.getFieldPath() + ',';
        		prodConfgsLst.add(fs.getFieldPath());
        	}
        }
        if(!prodConfgqry.contains('cscfga__Product_Basket__c')){
        	prodConfgqry = prodConfgqry + 'cscfga__Product_Basket__c,';
        }
        prodConfgqry = prodConfgqry + 'id,name,cscfga__Parent_Configuration__c,cscfga__Root_Configuration__c,cscfga__Root_Configuration__r.cscfga__Product_Basket__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c=:selectedBaskets';
        
        noOfConfgFields = prodConfgsLst.size() + 3;
        
        /*Pre Populating Opportunity Id and Basket Options, when Page loded*/
        if(apexPages.currentPage().getParameters().get('id') != null){
        	oppId = apexPages.currentPage().getParameters().get('id');
        	basket.cscfga__Opportunity__c = oppId;
        	updateBasketOptions();
        	
        }
        /*Pre Populating Basket*/
        if(apexPages.currentPage().getParameters().get('basketId') != null){
        	basketId = apexPages.currentPage().getParameters().get('basketId');
        	selectedBaskets = new list<string>();
        	selectedBaskets.add(basketId);
        	compareBaskets();
        }
        
    }


	/*Prepares list of basket options, when opportunity is selected from opportunity lookup*/
    public pageReference updateBasketOptions(){
        basketsList = new list<selectOption>();
        if(basket.cscfga__Opportunity__c != null){
            for(cscfga__Product_Basket__c bs : [select id,name from cscfga__Product_Basket__c where cscfga__Opportunity__c=:basket.cscfga__Opportunity__c]){
                basketsList.add(new selectOption(bs.id, bs.name));
            }
        }
        return null;
    }
    
    
    /* Displays list of baskets, when add selected baskets clicked*/
    public pageReference compareBaskets(){
    	if(!selectedBaskets.isEmpty()){
    		basketParameter.addAll(selectedBaskets);
    		set<string> tmpselectedBaskets = new set<string>();
	    	tmpselectedBaskets.addAll(selectedBaskets);
	    	selectedBaskets.clear();
    		
    		for(cscfga__Product_Basket__c p : basketsToCompare){
    			if(tmpselectedBaskets.contains(p.id)){
    				tmpselectedBaskets.remove(p.id);
    			}
    		}
    		selectedBaskets.addAll(tmpselectedBaskets);
    		
    		/*Preaparing Baskets for selcted basket ids*/
    		list<cscfga__Product_Basket__c> tempBaskets = new list<cscfga__Product_Basket__c>();
    		tempBaskets = database.query(basketqry);
    		basketsToCompare.addAll(tempBaskets);
    		
    		/*Removing duplicate Baskets*/
    		set<cscfga__Product_Basket__c> removeDuplicates = new set<cscfga__Product_Basket__c>();
    		removeDuplicates.addAll(basketsToCompare);
    		basketsToCompare.clear();
    		basketsToCompare.addAll(removeDuplicates);
    		
    		
    		/*preparing Product configuration list for each Product basket*/
    		list<cscfga__Product_Configuration__c> tempProdConfg = new list<cscfga__Product_Configuration__c>();
    		set<id> rootConfigs = new set<id>();
    		string confgqry = prodConfgqry + ' and cscfga__Parent_Configuration__c= null and cscfga__Root_Configuration__c = null';
    		tempProdConfg = database.query(confgqry);
    		for(cscfga__Product_Configuration__c sobj : tempProdConfg){
    			rootConfigs.add(sobj.id);
    			list<cscfga__Product_Configuration__c> tmp = new list<cscfga__Product_Configuration__c>();
    			tmp.add(sobj);
    			if(productConfigurations.get(sobj.cscfga__Product_Basket__c) != null){
    				tmp.addAll(productConfigurations.get(sobj.cscfga__Product_Basket__c));
    			}
    			productConfigurations.put(sobj.cscfga__Product_Basket__c,tmp);
    		}
    		
    		for(cscfga__Product_Basket__c bskt: basketsToCompare){
    			if(productConfigurations.get(bskt.id) == null){
    				productConfigurations.put(bskt.id,new list<cscfga__Product_Configuration__c>());
    			}
    		}
    		
    		if(!rootConfigs.isEmpty()){
    			confgqry = prodConfgqry + ' and cscfga__Parent_Configuration__c= :rootConfigs and cscfga__Root_Configuration__c = :rootConfigs';
    			list<cscfga__Product_Configuration__c> firstLevelConfg = new list<cscfga__Product_Configuration__c>();
    			firstLevelConfg = dataBase.query(confgqry);
    			set<id> firstLevelIds = new set<id>();
				for(cscfga__Product_Configuration__c sobj : firstLevelConfg){
					firstLevelIds.add(sobj.id);
					set<cscfga__Product_Configuration__c> tmp = new set<cscfga__Product_Configuration__c>();
					tmp.add(sobj);
					if(firstLevelprodConfg.get(sobj.cscfga__Root_Configuration__c) != null){
						tmp.addAll(firstLevelprodConfg.get(sobj.cscfga__Root_Configuration__c));
					}
					firstLevelprodConfg.put(sobj.cscfga__Root_Configuration__c,tmp);
				}
				for(Id rootId: 	rootConfigs){
					if(firstLevelprodConfg.get(rootId) == null){
						firstLevelprodConfg.put(rootId,new set<cscfga__Product_Configuration__c>());
					}
				}
				
				confgqry = prodConfgqry + ' and cscfga__Parent_Configuration__c= :firstLevelIds and cscfga__Root_Configuration__c = :rootConfigs';
				
    			list<cscfga__Product_Configuration__c> secondLevelConfg = new list<cscfga__Product_Configuration__c>();
    			secondLevelConfg = dataBase.query(confgqry);
    			set<id> secondLevelIds = new set<id>();
				for(cscfga__Product_Configuration__c sobj : secondLevelConfg){
					secondLevelIds.add(sobj.id);
					set<cscfga__Product_Configuration__c> tmp = new set<cscfga__Product_Configuration__c>();
					tmp.add(sobj);
					if(secondLevelprodConfg.get(sobj.cscfga__Parent_Configuration__c) != null){
						tmp.addAll(secondLevelprodConfg.get(sobj.cscfga__Parent_Configuration__c));
					}
					secondLevelprodConfg.put(sobj.cscfga__Parent_Configuration__c,tmp);
				}
				for(Id rootId: 	firstLevelIds){
					if(secondLevelprodConfg.get(rootId) == null){
						secondLevelprodConfg.put(rootId,new set<cscfga__Product_Configuration__c>());
					}
				} 
    		}
    	} 
    	return null;
    }
    
    public pageReference removeBasket(){
		list<string> tmpselectedBaskets = new list<string>();
    	tmpselectedBaskets.addAll(selectedBaskets);
    	selectedBaskets.clear();
    	for(string s : tmpselectedBaskets){
    		if(s != removeBasket){
    			selectedBaskets.add(s);
    		}
    	}    	

    	basketParameter.remove(removeBasket);
    	map<string, list<cscfga__Product_Configuration__c>> tmpproductConfigurations =  new map<string, list<cscfga__Product_Configuration__c>>();
   		list<cscfga__Product_Basket__c> tmpbasketsToCompare = new list<cscfga__Product_Basket__c>();
   		tmpproductConfigurations.putAll(productConfigurations);
    	productConfigurations.clear();
    	tmpbasketsToCompare.addAll(basketsToCompare);
    	basketsToCompare.clear();
    	for(cscfga__Product_Basket__c bskt: tmpbasketsToCompare){
    		if(removeBasket != bskt.id){
    			basketsToCompare.add(bskt);
				if(tmpproductConfigurations.get(bskt.id) != null){
					productConfigurations.put(bskt.id,tmpproductConfigurations.get(bskt.id));
				}
    		}
		}
    	removeBasket = '';
    	return null;
    }
    
    public void removeAllBaskets(){
    	selectedBaskets.clear();
    	basketParameter.clear();
    	productConfigurations.clear();
    	basketsToCompare.clear();
    	firstLevelprodConfg.clear();
    	secondLevelprodConfg.clear();
    }
    
    
}