@isTest
Private class Test_BatchScheduleUVSTaskExpire{

    static testMethod void testScheduler() {
        String CRON_EXP = '0 0 0 3 9 ? 2089';
        Test.startTest();
            
        // Schedule the test job  
            String jobId = System.schedule('testScheduledApex',
                CRON_EXP, new BatchScheduleUVSTaskExpire());
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
        // Verify the expressions are the same  
        System.assertEquals(CRON_EXP, ct.CronExpression);
        // Verify the job has not run      
        System.assertEquals(0, ct.TimesTriggered);
        // Verify the next time the job will run  
        System.assertEquals('2089-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        Test.stopTest(); 
    }

}