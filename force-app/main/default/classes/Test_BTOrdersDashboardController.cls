@isTest
private class Test_BTOrdersDashboardController {
	
    static testMethod void myUnitTest() {
        PageReference pageRef=Page.BTOrdersDashboard;
        Test.setCurrentPageReference(pageRef);
        BTB_Order__c btb = new BTB_Order__c();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(btb);
        BTOrdersDashboardController myController = new BTOrdersDashboardController(sc);
        myController.t_start.ActivityDate = System.today()-365;
        myController.t_end.ActivityDate = System.today();
        
        //   ensure coverage attained
        myController.SUBSTATUS_ORDER_CREATED_r=0;
		myController.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS_r=0;
		myController.SUBSTATUS_CREDIT_VET_DISCREPANCY_r=0;
		myController.SUBSTATUS_CREDIT_REFERRAL_PENDING_r=0;
		myController.SUBSTATUS_MANUAL_FULFILLMENT_IN_PROGRESS_r=0;
		myController.SUBSTATUS_AWAITING_SUBMISSION_r=0;
		myController.SUBSTATUS_MANUAL_FULFILLMENT_PENDING_r=0;
		myController.SUBSTATUS_AUTOMATED_r=0;
		myController.SUBSTATUS_ORDER_DISCREPANCY_FULL_r=0;
		myController.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL_r=0;
		myController.SUBSTATUS_WAITING_BT_r=0;
		myController.SUBSTATUS_WAITING_CUSTOMER_INFO_r=0;
		myController.SUBSTATUS_ENGINEERS_AND_CONTRACT_r=0;
		myController.SUBSTATUS_ERROR_ON_CLOSURE_r=0;
		myController.SUBSTATUS_CLOSED_r=0;
		myController.SUBSTATUS_CANCELLED_r=0;
		myController.SUBSTATUS_WITH_ENGINEERS_r=0;
		myController.SUBSTATUS_JOB_COMPLETED_r=0;
		myController.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT_r=0;
		myController.SUBSTATUS_ENGINEERS_AND_DEPOSIT_r=0;
		myController.SUBSTATUS_WAITING_ARCHIVE_r=0;
		myController.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT_r=0;
		myController.SUBSTATUS_WAITING_CANCELLATION_r=0;
		myController.SUBSTATUS_WAITING_CONTRACT_RETURN_r=0;
		myController.SUBSTATUS_WAITING_DEPOSIT_r=0;
		myController.SUBSTATUS_AWAITING_KCI_0_r=0;
		myController.SUBSTATUS_AWAITING_KCI_1_r=0;
		myController.SUBSTATUS_AWAITING_KCI_2_r=0;
		myController.SUBSTATUS_AWAITING_KCI_3_r=0;
		myController.SUBSTATUS_ORDER_CREATED_a=0;
		myController.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS_a=0;
		myController.SUBSTATUS_CREDIT_VET_DISCREPANCY_a=0;
		myController.SUBSTATUS_CREDIT_REFERRAL_PENDING_a=0;
		myController.SUBSTATUS_AWAITING_SUBMISSION_a=0;
		myController.SUBSTATUS_MANUAL_FULFILLMENT_PENDING_a=0;
		myController.SUBSTATUS_AUTOMATED_a=0;
		myController.SUBSTATUS_ORDER_DISCREPANCY_FULL_a=0;
		myController.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL_a=0;
		myController.SUBSTATUS_WAITING_BT_a=0;
		myController.SUBSTATUS_WAITING_CUSTOMER_INFO_a=0;
		myController.SUBSTATUS_ENGINEERS_AND_CONTRACT_a=0;
		myController.SUBSTATUS_ERROR_ON_CLOSURE_a=0;
		myController.SUBSTATUS_CLOSED_a=0;
		myController.SUBSTATUS_CANCELLED_a=0;
		myController.SUBSTATUS_WITH_ENGINEERS_a=0;
		myController.SUBSTATUS_JOB_COMPLETED_a=0;
		myController.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT_a=0;
		myController.SUBSTATUS_ENGINEERS_AND_DEPOSIT_a=0;
		myController.SUBSTATUS_WAITING_ARCHIVE_a=0;
		myController.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT_a=0;
		myController.SUBSTATUS_WAITING_CANCELLATION_a=0;
		myController.SUBSTATUS_WAITING_CONTRACT_RETURN_a=0;
		myController.SUBSTATUS_WAITING_DEPOSIT_a=0;
		myController.SUBSTATUS_AWAITING_KCI_0_a=0;
		myController.SUBSTATUS_AWAITING_KCI_1_a=0;
		myController.SUBSTATUS_AWAITING_KCI_2_a=0;
		myController.SUBSTATUS_AWAITING_KCI_3_a=0;
		myController.SUBSTATUS_ORDER_CREATED_g=0;
		myController.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS_g=0;
		myController.SUBSTATUS_CREDIT_VET_DISCREPANCY_g=0;
		myController.SUBSTATUS_CREDIT_REFERRAL_PENDING_g=0;
		myController.SUBSTATUS_MANUAL_FULFILLMENT_IN_PROGRESS_g=0;
		myController.SUBSTATUS_AWAITING_SUBMISSION_g=0;
		myController.SUBSTATUS_MANUAL_FULFILLMENT_PENDING_g=0;
		myController.SUBSTATUS_AUTOMATED_g=0;
		myController.SUBSTATUS_ORDER_DISCREPANCY_FULL_g=0;
		myController.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL_g=0;
		myController.SUBSTATUS_WAITING_BT_g=0;
		myController.SUBSTATUS_WAITING_CUSTOMER_INFO_g=0;
		myController.SUBSTATUS_ENGINEERS_AND_CONTRACT_g=0;
		myController.SUBSTATUS_ERROR_ON_CLOSURE_g=0;
		myController.SUBSTATUS_CLOSED_g=0;
		myController.SUBSTATUS_CANCELLED_g=0;
		myController.SUBSTATUS_WITH_ENGINEERS_g=0;
		myController.SUBSTATUS_JOB_COMPLETED_g=0;
		myController.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT_g=0;
		myController.SUBSTATUS_ENGINEERS_AND_DEPOSIT_g=0;
		myController.SUBSTATUS_WAITING_ARCHIVE_g=0;
		myController.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT_g=0;
		myController.SUBSTATUS_WAITING_CANCELLATION_g=0;
		myController.SUBSTATUS_WAITING_CONTRACT_RETURN_g=0;
		myController.SUBSTATUS_WAITING_DEPOSIT_g=0;
		myController.SUBSTATUS_AWAITING_KCI_0_g=0;
		myController.SUBSTATUS_AWAITING_KCI_1_g=0;
		myController.SUBSTATUS_AWAITING_KCI_2_g=0;
		myController.SUBSTATUS_AWAITING_KCI_3_g=0;
		
		myController.SUBSTATUS_ERROR_SUBMISSION_g = 0;
		myController.SUBSTATUS_ERROR_ORDER_g = 0;
		myController.SUBSTATUS_COMPLETE_g = 0;
		myController.SUBSTATUS_ERROR_SUBMISSION_r = 0;
		myController.SUBSTATUS_ERROR_ORDER_r = 0;
		myController.SUBSTATUS_COMPLETE_r = 0;
		myController.SUBSTATUS_ERROR_SUBMISSION_a = 0;
		myController.SUBSTATUS_ERROR_ORDER_a = 0;
		myController.SUBSTATUS_COMPLETE_a = 0;
		        
        Integer x;
        x=myController.SUBSTATUS_ORDER_CREATED_r;
		x=myController.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS_r;
		x=myController.SUBSTATUS_CREDIT_VET_DISCREPANCY_r;
		x=myController.SUBSTATUS_CREDIT_REFERRAL_PENDING_r;
		x=myController.SUBSTATUS_MANUAL_FULFILLMENT_IN_PROGRESS_r;
		x=myController.SUBSTATUS_AWAITING_SUBMISSION_r;
		x=myController.SUBSTATUS_MANUAL_FULFILLMENT_PENDING_r;
		x=myController.SUBSTATUS_AUTOMATED_r;
		x=myController.SUBSTATUS_ORDER_DISCREPANCY_FULL_r;
		x=myController.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL_r;
		x=myController.SUBSTATUS_WAITING_BT_r;
		x=myController.SUBSTATUS_WAITING_CUSTOMER_INFO_r;
		x=myController.SUBSTATUS_ENGINEERS_AND_CONTRACT_r;
		x=myController.SUBSTATUS_ERROR_ON_CLOSURE_r;
		x=myController.SUBSTATUS_CLOSED_r;
		x=myController.SUBSTATUS_CANCELLED_r;
		x=myController.SUBSTATUS_WITH_ENGINEERS_r;
		x=myController.SUBSTATUS_JOB_COMPLETED_r;
		x=myController.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT_r;
		x=myController.SUBSTATUS_ENGINEERS_AND_DEPOSIT_r;
		x=myController.SUBSTATUS_WAITING_ARCHIVE_r;
		x=myController.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT_r;
		x=myController.SUBSTATUS_WAITING_CANCELLATION_r;
		x=myController.SUBSTATUS_WAITING_CONTRACT_RETURN_r;
		x=myController.SUBSTATUS_WAITING_DEPOSIT_r;
		x=myController.SUBSTATUS_AWAITING_KCI_0_r;
		x=myController.SUBSTATUS_AWAITING_KCI_1_r;
		x=myController.SUBSTATUS_AWAITING_KCI_2_r;
		x=myController.SUBSTATUS_AWAITING_KCI_3_r;
		x=myController.SUBSTATUS_ORDER_CREATED_a;
		x=myController.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS_a;
		x=myController.SUBSTATUS_CREDIT_VET_DISCREPANCY_a;
		x=myController.SUBSTATUS_CREDIT_REFERRAL_PENDING_a;
		x=myController.SUBSTATUS_AWAITING_SUBMISSION_a;
		x=myController.SUBSTATUS_MANUAL_FULFILLMENT_PENDING_a;
		x=myController.SUBSTATUS_AUTOMATED_a;
		x=myController.SUBSTATUS_ORDER_DISCREPANCY_FULL_a;
		x=myController.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL_a;
		x=myController.SUBSTATUS_WAITING_BT_a;
		x=myController.SUBSTATUS_WAITING_CUSTOMER_INFO_a;
		x=myController.SUBSTATUS_ENGINEERS_AND_CONTRACT_a;
		x=myController.SUBSTATUS_ERROR_ON_CLOSURE_a;
		x=myController.SUBSTATUS_CLOSED_a;
		x=myController.SUBSTATUS_CANCELLED_a;
		x=myController.SUBSTATUS_WITH_ENGINEERS_a;
		x=myController.SUBSTATUS_JOB_COMPLETED_a;
		x=myController.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT_a;
		x=myController.SUBSTATUS_ENGINEERS_AND_DEPOSIT_a;
		x=myController.SUBSTATUS_WAITING_ARCHIVE_a;
		x=myController.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT_a;
		x=myController.SUBSTATUS_WAITING_CANCELLATION_a;
		x=myController.SUBSTATUS_WAITING_CONTRACT_RETURN_a;
		x=myController.SUBSTATUS_WAITING_DEPOSIT_a;
		x=myController.SUBSTATUS_AWAITING_KCI_0_a;
		x=myController.SUBSTATUS_AWAITING_KCI_1_a;
		x=myController.SUBSTATUS_AWAITING_KCI_2_a;
		x=myController.SUBSTATUS_AWAITING_KCI_3_a;
		x=myController.SUBSTATUS_ORDER_CREATED_g;
		x=myController.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS_g;
		x=myController.SUBSTATUS_CREDIT_VET_DISCREPANCY_g;
		x=myController.SUBSTATUS_CREDIT_REFERRAL_PENDING_g;
		x=myController.SUBSTATUS_MANUAL_FULFILLMENT_IN_PROGRESS_g;
		x=myController.SUBSTATUS_AWAITING_SUBMISSION_g;
		x=myController.SUBSTATUS_MANUAL_FULFILLMENT_PENDING_g;
		x=myController.SUBSTATUS_AUTOMATED_g;
		x=myController.SUBSTATUS_ORDER_DISCREPANCY_FULL_g;
		x=myController.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL_g;
		x=myController.SUBSTATUS_WAITING_BT_g;
		x=myController.SUBSTATUS_WAITING_CUSTOMER_INFO_g;
		x=myController.SUBSTATUS_ENGINEERS_AND_CONTRACT_g;
		x=myController.SUBSTATUS_ERROR_ON_CLOSURE_g;
		x=myController.SUBSTATUS_CLOSED_g;
		x=myController.SUBSTATUS_CANCELLED_g;
		x=myController.SUBSTATUS_WITH_ENGINEERS_g;
		x=myController.SUBSTATUS_JOB_COMPLETED_g;
		x=myController.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT_g;
		x=myController.SUBSTATUS_ENGINEERS_AND_DEPOSIT_g;
		x=myController.SUBSTATUS_WAITING_ARCHIVE_g;
		x=myController.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT_g;
		x=myController.SUBSTATUS_WAITING_CANCELLATION_g;
		x=myController.SUBSTATUS_WAITING_CONTRACT_RETURN_g;
		x=myController.SUBSTATUS_WAITING_DEPOSIT_g;
		x=myController.SUBSTATUS_AWAITING_KCI_0_g;
		x=myController.SUBSTATUS_AWAITING_KCI_1_g;
		x=myController.SUBSTATUS_AWAITING_KCI_2_g;
		x=myController.SUBSTATUS_AWAITING_KCI_3_g;
		
		//x=myController.SUBSTATUS_ERROR_SUBMISSION;
		x=myController.SUBSTATUS_ERROR_SUBMISSION_g;
		x=myController.SUBSTATUS_ERROR_ORDER_g;
		x=myController.SUBSTATUS_COMPLETE_g;
		x=myController.SUBSTATUS_ERROR_SUBMISSION_r;
		x=myController.SUBSTATUS_ERROR_ORDER_r;
		x=myController.SUBSTATUS_COMPLETE_r;
		x=myController.SUBSTATUS_ERROR_SUBMISSION_a;
		x=myController.SUBSTATUS_ERROR_ORDER_a;
		x=myController.SUBSTATUS_COMPLETE_a;
		
		String str;
		str=myController.SUBSTATUS_CREDIT_REFERRAL_IN_PROGRESS;
		str=myController.SUBSTATUS_CREDIT_VET_DISCREPANCY;
		str=myController.SUBSTATUS_CREDIT_REFERRAL_PENDING;
		str=myController.SUBSTATUS_AWAITING_SUBMISSION;
		str=myController.SUBSTATUS_MANUAL_FULFILLMENT_IN_PROGRESS;
		str=myController.SUBSTATUS_SUBMITTED;
		str=myController.SUBSTATUS_MANUAL_FULFILLMENT_PENDING;
		str=myController.SUBSTATUS_AUTOMATED;
		str=myController.SUBSTATUS_ORDER_DISCREPANCY_FULL;
		str=myController.SUBSTATUS_ORDER_DISCREPANCY_PARTIAL;
		str=myController.SUBSTATUS_WAITING_BT;
		str=myController.SUBSTATUS_WAITING_CUSTOMER_INFO;
		str=myController.SUBSTATUS_ENGINEERS_AND_CONTRACT;
		str=myController.SUBSTATUS_ERROR_ON_CLOSURE;
		str=myController.SUBSTATUS_WITH_ENGINEERS;
		str=myController.SUBSTATUS_JOB_COMPLETED;
		str=myController.SUBSTATUS_ENGINEERS_CONTRACT_AND_DEPOSIT;
		str=myController.SUBSTATUS_ENGINEERS_AND_DEPOSIT;
		str=myController.SUBSTATUS_WAITING_ARCHIVE;
		str=myController.SUBSTATUS_WAITING_CONTRACT_AND_DEPOSIT;
		str=myController.SUBSTATUS_WAITING_CANCELLATION;
		str=myController.SUBSTATUS_WAITING_CONTRACT_RETURN;
		str=myController.SUBSTATUS_WAITING_DEPOSIT;
		str=myController.SUBSTATUS_AWAITING_KCI_0;
		str=myController.SUBSTATUS_AWAITING_KCI_1;
		str=myController.SUBSTATUS_AWAITING_KCI_2;
		str=myController.SUBSTATUS_AWAITING_KCI_3;
		
		str=myController.SUBSTATUS_ERROR_SUBMISSION;
		str=myController.SUBSTATUS_ERROR_ORDER;
		str=myController.SUBSTATUS_COMPLETE;

		str=myController.report_startdate;
		str=myController.report_enddate;
		
		/*	
		Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
        
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99999';
        insert a;
        
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        
        Opportunity o1;
        o1 =  [Select Id, Opportunity_Id__c from  Opportunity where Name =: 'Dummy Opp For EDW'];
        
        //   CVD OR ODF COVERAGE
        List<User> thisUser = [Select id, Run_Apex_Triggers__c, Manager_EIN__c from User where id = :UserInfo.getUserId() Limit 1];
    	thisUser[0].Run_Apex_Triggers__c = False;
    	update thisUser[0];	
        
		User uM = new User();
		uM.Username = '999999000@bt.com';
		uM.Ein__c = '999999000';
		uM.LastName = 'TestLastname';
		uM.FirstName = 'TestFirstname';
		uM.MobilePhone = '07918672032';
		uM.Phone = '02085878834';
		uM.Title='What i do';
		uM.OUC__c = 'DKW';
		uM.Manager_EIN__c = '123456789';
		uM.Email = 'no.reply@bt.com';
		uM.Alias = 'boatid01';
		uM.TIMEZONESIDKEY = 'Europe/London';
		uM.LOCALESIDKEY  = 'en_GB';
		uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
		uM.PROFILEID = profileId;
		uM.LANGUAGELOCALEKEY = 'en_US';	      
		uM.email = 'no.reply@bt.com';
		Database.SaveResult[] uMResult = Database.insert(new User [] {uM});

		User uGM = new User();
		uGM.Username = '999999001@bt.com';
		uGM.Ein__c = '999999001';
		uGM.LastName = 'TestLastname';
		uGM.FirstName = 'TestFirstname';
		uGM.MobilePhone = '07918672032';
		uGM.Phone = '02085878834';
		uGM.Title='What i do';
		uGM.OUC__c = 'DKW';
		uGM.Manager_EIN__c = '999999000';
		uGM.ManagerId = uMResult[0].id;
		uGM.Email = 'no.reply@bt.com';
		uGM.Alias = 'boatid01';
		uGM.TIMEZONESIDKEY = 'Europe/London';
		uGM.LOCALESIDKEY  = 'en_GB';
		uGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
		uGM.PROFILEID = profileId;
		uGM.LANGUAGELOCALEKEY = 'en_US';
		uGM.email = 'no.reply@bt.com';
		Database.SaveResult[] uGMResult = Database.insert(new User [] {uGM});
 
		User uDGM = new User();
		uDGM.Username = '999999002@bt.com';
		uDGM.Ein__c = '999999002';
		uDGM.LastName = 'TestLastname';
		uDGM.FirstName = 'TestFirstname';
		uDGM.MobilePhone = '07918672032';
		uDGM.Phone = '02085878834';
		uDGM.Title='What i do';
		uDGM.OUC__c = 'DKW';
		uDGM.Manager_EIN__c = '999999001';
		uDGM.ManagerId = uGMResult[0].id;
		uDGM.Email = 'no.reply@bt.com';
		uDGM.Alias = 'boatid01';
		uDGM.managerID = uGM.Id;
		uDGM.TIMEZONESIDKEY = 'Europe/London';
		uDGM.LOCALESIDKEY  = 'en_GB';
		uDGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
		uDGM.PROFILEID = profileId;
		uDGM.LANGUAGELOCALEKEY = 'en_US';
		uDGM.email = 'no.reply@bt.com';
		Database.SaveResult[] uDGMResult = Database.insert(new User [] {uDGM});
        
		User uSM = new User();
		uSM.Username = '999999003@bt.com';
		uSM.Ein__c = '999999003';
		uSM.LastName = 'TestLastname';
		uSM.FirstName = 'TestFirstname';
		uSM.MobilePhone = '07918672032';
		uSM.Phone = '02085878834';
		uSM.Title='What i do';
		uSM.OUC__c = 'DKW';
		uSM.Manager_EIN__c = '999999002';
		uSM.ManagerId = uDGMResult[0].id;
		uSM.Email = 'no.reply@bt.com';
		uSM.Alias = 'boatid01';
		uSM.managerID = uDGM.Id;
		uSM.TIMEZONESIDKEY = 'Europe/London';
		uSM.LOCALESIDKEY  = 'en_GB';
		uSM.EMAILENCODINGKEY = 'ISO-8859-1';                               
		uSM.PROFILEID = profileId;
		uSM.LANGUAGELOCALEKEY = 'en_US';
		uSM.email = 'no.reply@bt.com';
		insert uSM;
      
		//link user running test to hierachy created above
		thisUser[0].ManagerId = uSM.ID;
		update thisUser[0];
                
        RecordType rt = [select id from RecordType where SobjectType='Case' and name ='Order Discrepancy' limit 1];
    	RecordType rt_creditvet = [select id from RecordType where SobjectType='Case' and name ='Credit Vet' limit 1];

		Case c1 = new case();
        c1.Vol_Reference__c = 'VOL011-43406041842';
        c1.Status = 'Assigned to Front Office';
        c1.Reason = 'test';
        c1.Front_Office_ein__c = '999999003';
        c1.RecordTypeId = rt.Id;
        insert c1;
        
        Case c2 = new case();
        c2.Vol_Reference__c = 'VOL011-43406041842';
        c2.Status = 'Assigned to Front Office';
        c2.Reason = 'test';
        c2.Front_Office_ein__c = '999999003';
        c2.RecordTypeId = rt_creditvet.Id;
        insert c2;
        
		List<BTB_Order__c> bo_ls=new List<BTB_Order__c>();
		BTB_Order__c bo9=new BTB_Order__c(
		OWNERID = UserInfo.getUserId(), 
		NAME = 'VOL011-43406041842',
		BACKOFFICE_QUEUE_ENTER_DT__C = Datetime.newInstance(2010,12,17,03,11,47), 
		BACKOFFICE_QUEUE_NAME__C = 'CS_WLR3_BTB_ACQ', 
		CRED_CREATE_DT__C = Datetime.newInstance(2010,12,17,03,11,01),
		EDW_UPDATE_DT__C = Datetime.newInstance(2011,01,31,17,51,39), 
		EIN__c = uSM.Ein__c,
		LE_CODE__C = '3145472', 
		ONEVIEW_ORDER_SUB_STATUS__C = 'Supplier Orders Issued', 
		ONEVIEW_STATUS__C = 'Open', 
		ORDERKEY__C = '1004', 
		ORDER_DISCREPANCY__C= 'N', 
		OV_ORDER_NUM__C = 'TEST_VOL011-43406041842', 
		SHOPKEY__C = 'SK03862959', 
		COMMENT_LONG__C = 'BVJ992lw; BVJ917lw **07.01.11 BTBOP549523 applied', 
		ORDER_CREATED_DT__C = Datetime.newInstance(2010,12,16,18,30,0), 
		INTERNAL_CREDIT_CHECK_STATUS__C = 'Closed',
		INTERNAL_CREDIT_CHECK_OUTCOME__C = 'Green', 
		EVT_STAT_CD__C = 'Done',
		ORDER_SUBMITTED_DATE__C = Datetime.newInstance(2010,12,17,03,11,46), 
		ICC_LAST_UPDATED_DT__C = Datetime.newInstance(2010,12,17,03,11,01), 
		SHOP_CHILD_CSS_ORDERS_EXIST__C = TRUE, 
		INTERNAL_CREDIT_CHECK_ACTIVITY__C = TRUE, 
		CV_DISCREPANCY_ACTIVITY__C = FALSE,
		BO_OPEN_ACTIVITY_EXISTS__C = FALSE,
		SFDC_STATUS__c='Order Created', 
		SFDC_Discrepancy__c='Red');
		bo_ls.add(bo9);
		
		insert bo_ls;
		
		List<BTB_Order_Product__c> bop_ls=new List<BTB_Order_Product__c>();
		BTB_Order_Product__c bop4=new BTB_Order_Product__c(
		BTB_ORDER__C = bo9.id,
		PRODUCT_NAME_OV__C = 'Remove CPS/Wholesale Calls',
		EDW_PRODUCT_NAME__C = 'CPS Winback',
		PROD_GROUP__C = 'Calls and Lines Winback',
		BTB_OV_ORDER_KEY__C = 'test_BTB_OV_ORDER_KEY__C_1'
		);

		bop_ls.add(bop4);

		insert bop_ls;
		
		BTB_Order_Product__c op = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '123', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Calls and Lines New provide', EDW_PRODUCT_NAME__c = 'Calls and Lines New provide', PROD_GROUP__c = 'Calls and Lines New provide');
		insert op;
		BTB_Order_Product__c op1 = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '124', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Calls and Lines SIM New provide', EDW_PRODUCT_NAME__c = 'Calls and Lines SIM New provide', PROD_GROUP__c = 'Calls and Lines SIM New provide');
		insert op1;
		BTB_Order_Product__c op2 = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '125', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Calls and Lines Resign', EDW_PRODUCT_NAME__c = 'Calls and Lines Resign', PROD_GROUP__c = 'Calls and Lines Resign');
		insert op2;
		BTB_Order_Product__c op3 = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '126', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Calls and Lines Winback', EDW_PRODUCT_NAME__c = 'Calls and Lines Winback', PROD_GROUP__c = 'Calls and Lines Winback');
		insert op3;
		BTB_Order_Product__c op4 = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '127', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Featureline provide', EDW_PRODUCT_NAME__c = 'Featureline provide', PROD_GROUP__c = 'Featureline provide');		
		insert op4;
		BTB_Order_Product__c op5 = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '128', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Featureline cease', EDW_PRODUCT_NAME__c = 'Featureline cease', PROD_GROUP__c = 'Featureline cease');		
		insert op5;
		BTB_Order_Product__c op6 = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '129', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Featureline modify', EDW_PRODUCT_NAME__c = 'Featureline modify', PROD_GROUP__c = 'Featureline modify');		
		insert op6;
		BTB_Order_Product__c op7 = new BTB_Order_Product__c(BTB_OV_ORDER_KEY__c = '130', BTB_Order__c = bo9.Id, PRODUCT_NAME_OV__c = 'Mobility', EDW_PRODUCT_NAME__c = 'Mobility', PROD_GROUP__c = 'Mobility');		
		insert op7;

		List<BTB_Order_Line__c> bol_ls=new List<BTB_Order_Line__c>();
		BTB_Order_Line__c bol1=new BTB_Order_Line__c(
		NAME='test_BTB_Order_Line__Name_1',
		BTB_ORDER__C=bo9.id,
		COMPLETION_DATE__C=Datetime.newInstance(2011,1,17,03,11,01),
		CSS_ORD_ISSUED_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		CSS_STATUS__C='Closed',
		CUSTOMER_TYPE__C='RV',
		CUST_AGREED_DT__C=Datetime.newInstance(2011,1,1,03,11,01),
		DAYS_BTW_CSS_ORD_CLOSED__C=15.69166667,
		EDW_UPDATE_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI1_RECEIVED_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI2_RECEIVED_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI3_RECEIVED_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI4_RECEIVED_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		SFDC_STATUS__C='Order Closed',
		CSS_ORDER_DATA_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		CUSTOMER_REQ_DATE__C=Datetime.newInstance(2010,12,17,03,11,01),
		CSS_ORD_STATUS__C='PL',
		CSS_ORD_STATUS_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI0_REQUIRED_BY_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI0_STATUS__C='COM',
		KCI1_REQUIRED_BY_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI1_STATUS__C='COM',
		KCI2_REQUIRED_BY_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI2_STATUS__C='COM',
		KCI3_REQUIRED_BY_DT__C=Datetime.newInstance(2010,12,17,03,11,01),
		KCI3_STATUS__C='COM',
		BTB_CSS_ORDER_NO__c='test_BTB_CSS_ORDER_KEY__C_1'
		);

		bol_ls.add(bol1);

		insert bol_ls;
        
        BTB_Order_Line_Product__c olp = new BTB_Order_Line_Product__c(BTB_CSS_ORDER_KEY__c = '123', BTB_Order_Line__c = bol1.Id, Product_Name_on_CSS__c = 'Calls and Lines New provide', PROD_GROUP__c = 'Calls and Lines New provide');
		insert olp;
		BTB_Order_Line_Product__c olp1 = new BTB_Order_Line_Product__c(BTB_CSS_ORDER_KEY__c = '124', BTB_Order_Line__c = bol1.Id, Product_Name_on_CSS__c = 'Calls and Lines SIM New provide', PROD_GROUP__c = 'Calls and Lines SIM New provide');
		insert olp1;
		BTB_Order_Line_Product__c olp2 = new BTB_Order_Line_Product__c(BTB_CSS_ORDER_KEY__c = '125', BTB_Order_Line__c = bol1.Id, Product_Name_on_CSS__c = 'Calls and Lines Resign', PROD_GROUP__c = 'Calls and Lines Resign');
		insert olp2;
		BTB_Order_Line_Product__c olp3 = new BTB_Order_Line_Product__c(BTB_CSS_ORDER_KEY__c = '126', BTB_Order_Line__c = bol1.Id, Product_Name_on_CSS__c = 'Calls and Lines Winback', PROD_GROUP__c = 'Calls and Lines Winback');
		insert olp3;
		BTB_Order_Line_Product__c olp4 = new BTB_Order_Line_Product__c(BTB_CSS_ORDER_KEY__c = '127', BTB_Order_Line__c = bol1.Id, Product_Name_on_CSS__c = 'Featureline provide', PROD_GROUP__c = 'Featureline provide');		
		insert olp4;
		BTB_Order_Line_Product__c olp5 = new BTB_Order_Line_Product__c(BTB_CSS_ORDER_KEY__c = '128', BTB_Order_Line__c = bol1.Id, Product_Name_on_CSS__c = 'Featureline cease', PROD_GROUP__c = 'Featureline cease');		
		insert olp5;
		BTB_Order_Line_Product__c olp6 = new BTB_Order_Line_Product__c(BTB_CSS_ORDER_KEY__c = '129', BTB_Order_Line__c = bol1.Id, Product_Name_on_CSS__c = 'Featureline modify', PROD_GROUP__c = 'Featureline modify');		
		insert olp6; */
        
        //Start the Test Coverage
 		Test.startTest();
 		List<SelectOption> options = myController.getItems(); 
        myController.t_start.ActivityDate = Date.today()-25;
        myController.t_end.ActivityDate = Date.today();
        myController.view_orders = 'Winback';
        myController.go();
        myController.view_orders = 'All Available Orders';
        myController.go();
        Test.stopTest(); 
		//End the Test Coverage
    }
}