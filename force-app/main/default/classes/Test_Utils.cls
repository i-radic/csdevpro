@isTest
public class Test_Utils {
    
    /**
    * <P>Helper method to prepare Account.</P>
    */
    public static Account createAccount(string accName)
    {
      Account acc = new Account(name=accName);
      acc.BillingStreet = 'Street 1';
      acc.BillingCity = 'City 1';
      acc.BillingState = 'State 1';
      acc.BillingCountry = 'Country 1';
      acc.BillingPostalCode = 'Code 1';
      insert acc;
      
      system.assertNotEquals(acc.id,null);
      return acc;
    }
    
        
    /**
    * <P>Helper method to prepare Contact.</P>
    */
    public static Contact createContact(string firstName, string lastName, string accId , string email)
    { 
      if(firstName == null){
         firstName = 'FName';
      }
      if(lastName == null){
         lastName = 'LName';
      }
      if(email == null){
         email = 'abc@test.com';
      }     
      Contact con=new Contact(FirstName=firstName, LastName=lastName, AccountId=accId, Email=email);
      //con.Comms_Type__c = 'Events';
      insert con;
      system.assertNotEquals(con,null);
      
      return con;
    }
    
    
    /**
    * <P>Helper method to prepare Contact.</P>
    */
    public static Lead createLead(string recordtypename)
    { 
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Lead; 
        Map<String,Schema.RecordTypeInfo> rtLeadInfo = cfrSchema.getRecordTypeInfosByName(); 
        
        if (string.isEmpty(recordtypename))
            recordtypename = 'Small Sales';
                    
        string rtLeadId = rtLeadInfo.get(recordtypename).getRecordTypeId();
        
        Lead ld=new Lead(FirstName='firstName', LastName='lastName', Company='Test Company', 
                        RecordtypeId = rtLeadId, Email='abc@test.com');
        insert ld;
        system.assertNotEquals(ld,null);
      
        return ld;
    }
    
    /**
    * <P>Helper method to prepare Opportunity.</P>
    */
    public static Opportunity createOpp(string oppName,string oppStageName, date oppCloseDate, Id oppAccId)
    { 
      
      if(null == oppName){
         oppName = 'Test Opportunity';
      }
      if(null == oppStageName){
         oppStageName = 'Test Stage';
      }
            
      Opportunity opp=new Opportunity(name=oppName, LeadSource='BCS', AccountId=oppAccId, Company_Details_Validated__c=true, StageName='Changes over Time',
                                      CloseDate=(System.today()+15), ForecastCategoryName='Pipeline',Next_Action_Date__c=(System.today()+2),Incumbent__c='Vodafone',
                                      Competitors__c='Vodafone',Customer_Status__c='New To Orange', //BigMachines_User_Approver_Level__c=1.35,
                                      OBA_Activated_Date__c=(System.today()),/*DealApprovedinBigMachines__c='Yes',*/ Type='Win');
      insert opp;
      system.assertNotEquals(opp.id,null);
      return opp;
    }
    
    /**
    * <P>Helper method to prepare Campaign.</P>
    */
    /*public static Campaign createCampaign(string camptype)
    {
      Campaign camp = new Campaign();
      camp.Name = 'Test';
      camp.Status = 'Planned';
      camp.Comms_Type__c = 'Events';
      camp.StartDate = Date.today();
      sbm__EmailTemplate__c temp = new sbm__EmailTemplate__c(Name = 'Test Template', sbm__sbx_EmailBodyHTML__c = 'test email body');
      insert temp;
      
      sbm__AWS_SES_Sender__c sender = new sbm__AWS_SES_Sender__c(Name = 'test@test.com', sbm__sbx_AwsSesSenderName__c = 'Test');
      insert sender;
      
      camp.sbm__sbx_EmailSender__c = sender.id;
      camp.sbm__sbx_EmailTemplate__c = temp.id;
      camp.sbm__sbx_EmailSubject__c = 'Hello';
      camp.RecordTypeId = Test_Utils.getRecordTypeID(new Campaign(), camptype);
      insert camp;
      
      system.assertNotEquals(camp.id,null);
      return camp;
    }*/
    
    
    /**
    * <P>Helper method to prepare Campaign Member.</P>
    */
    /*public static CampaignMember createCampaignMember(string campId, string contactId, string Status)
    {
      CampaignMember campMem = new CampaignMember();
      campMem.CampaignId = campId;
      campMem.ContactId = contactId;
      campMem.Status = Status;
      //campMem.RecordTypeId = Test_Utils.getRecordTypeID(new CampaignMember(), cmtype);
      insert campMem;
      
      system.assertNotEquals(campMem.id,null);
      return campMem;
    }*/
    
    /**
    * <P>Helper method to prepare Contact Subscription</P>
    */
   /* public static Contact_Subscription__c createContactSubscription(string campId, string contactId, string commsType)
    {
      Contact_Subscription__c cs = new Contact_Subscription__c();
      cs.Campaign__c = campId;
      cs.Contact__c = contactId;
      cs.Communications_Type__c = commsType;
      cs.Unsubscription_Date__c = Date.Today().addYears(-1);
      insert cs;
      
      system.assertNotEquals(cs.id,null);
      return cs;
    }*/
    
    
    /**
    * <P>Helper method to prepare Trial.</P>
    */
    public static Trial__c createTrial(Opportunity objOpp, Contact cnt)
    {
        Trial__c objTrial = new Trial__c();
        objTrial.Opportunity__c = objOpp.Id; 
        objTrial.Recipient_Name__c = cnt.Id; 
        objTrial.Status__c = 'Draft';
        insert objTrial; 
        
        system.assertNotEquals(objTrial.id,null);       
        return objTrial;
    }
    
    
    /**
    * <P>Helper method to prepare Trial Collection Courier.</P>
    */
    public static void createTrialCourier()
    {
        Trial_Collection_Courier__c objCourier = new Trial_Collection_Courier__c();
        objCourier.Name = 'abc test';
        objCourier.Email_Addresses__c = 'abctest@gmail.com';
        objCourier.Active__c = true;
        insert objCourier;
        system.assertNotEquals(objCourier,null);
    
    }
    
    
    /**
    * <P>Helper method to prepare Trial Collection.</P>
    */
    public static Trial_Collection__c createTrialCollection(Trial__c objTrial)
    {
        system.debug('********objTrial'+objTrial.Id);
        Trial_Collection__c objTrialColl = new Trial_Collection__c();
        objTrialColl.Trial__c = objTrial.Id;
        objTrialColl.Contact_Name__c = 'test1';
        objTrialColl.Contact_Telephone_Number__c ='23423423';
        objTrialColl.Opening_Hours__c = '12';
        objTrialColl.Special_Instructions__c = 'XYZ';
        objTrialColl.Collection_Address__c = 'abc';
        objTrialColl.Collection_City__c ='AZ';
        objTrialColl.Collection_Postcode__c='345345';
        insert objTrialColl;
        system.assertNotEquals(objTrialColl.id,null);
        
        return objTrialColl;
    }
    
    
    /**
    * <P>Helper method to populate SKU.</P>
    */
    public static SKU__c populateSKU(string sName, string sNameC, decimal price, boolean isTabletDevice, string simType, string deviceType) {
                
        SKU__c sku = new SKU__c(Name = sName, Name__c = sNameC, Price__c = price , Tablet_Device__c = isTabletDevice,
                                SIM_Type__c = simType, Type__c = deviceType); 
        system.assertNotEquals(sku,null);
        return sku; 
            
    }
    
    
    /**
    * <P>Helper method to populate Stock Item.</P>
    */
    public static Stock_Item__c populateStockItem(SKU__c sku, string status, string identificationNumber,String ctn) {
        
        Stock_Item__c stockItem = new Stock_Item__c(SKU__c = sku.Id,Status__c=status,Identification_Number__c=identificationNumber,CTN__c=ctn); 
        system.assertNotEquals(stockItem,null);
        return stockItem;   
            
    }
    
    
    /**
    * <P>Helper method to prepare Trial Equipment.</P>
    */
    public static Trial_Line_Item__c createTrialLineItem(Id trialId, Id skuId, decimal quantity) {
        
        Trial_Line_Item__c trialLineItem = new Trial_Line_Item__c(Trial__c = trialId, SKU__c = skuId, Quantity__c = quantity); 
        insert trialLineItem;
        system.assertNotEquals(trialLineItem.id,null);
        
        return trialLineItem;   
            
    }
    
    /**
    * <P>Helper method to populate Trial Stock Item.</P>
    */
     public static Trial_Stock_Item__c populateTrialStockItem(Trial__c objTrial,Stock_Item__c stkItem, string status)
    {
        Trial_Stock_Item__c objTrialStockItem = new Trial_Stock_Item__c(Stock_Item__c = stkItem.Id, Trial__c = objTrial.Id, Status__c = status);
        system.assertNotEquals(objTrialStockItem, null);
        return objTrialStockItem;
    }
    
    /**
    * <P>Helper method to create Echo Sign Aggrement.</P>
    */
     public static echosign_dev1__SIGN_Agreement__c createEchoSignAgreement(Trial__c trial, string status)
    {   
        echosign_dev1__SIGN_Agreement__c objEchoSignAgreement = new echosign_dev1__SIGN_Agreement__c(Trial__c = trial.id, echosign_dev1__Status__c = status);
        insert objEchoSignAgreement;
        system.assertNotEquals(objEchoSignAgreement.Id, null);
        return objEchoSignAgreement;
    }
    
    /**
    * <P>Helper method to create Project</P>
    */
    public static Project__c createProject(Opportunity opp)
    {   
        Project__c p = new Project__c();
        p.Project_Name__c = 'Unit Test Project';
        p.Opportunity__c = opp.Id;
        p.Notify_Project_Manager__c = FALSE;
        insert p;
        system.assertNotEquals(p.Id, null);
        return p;
    }
    
    /**
    * <P>Helper method to create Product</P>
    */
    public static Product2 createProduct()
    {   
        Product2 prod = new Product2(Name='Test Product',Family='family',ProductCode='Test PRD001',/*CurrencyIsoCode='GBP',*/IsActive=true);
        insert prod;
        system.assertNotEquals(prod.Id, null);
        return prod;
    }
    
    /**
    * <P>Helper method to create Product</P>
    */
    /*public static M2M_Product__c createM2MProduct(string oppId, string product2Id, string duration)
    {   
        M2M_Product__c mprod = new M2M_Product__c();
        mprod.Opportunity__c = oppId;
        mprod.Product__c = product2Id;
        if (duration == null || duration == '')
            mprod.Duration__c = '12m';
        else
            mprod.Duration__c = duration;   
        mprod.Total_Connection_Opportunity__c = 10;
        mprod.ARPU__c = 10;
        mprod.Est_Con_Discon__c = Date.Today().addDays(5);
        insert mprod;
        system.assertNotEquals(mprod.Id, null);
        return mprod;
    }*/
    
    /**
    * <P>Helper method to create User.</P>
    */
    public static User createUserRecord(string userName,string lastName,string alias,String email,
                                   string emailCodingKey,string langLocaleKey,string localIdKey,string profileId,string timeZonesIdKey,
                                   string division, string department, string companyName){
        User user;
        System.runAs(new User(Id=UserInfo.getUserId())){
            user = new User(Username = userName,LastName = lastName,Alias = alias,Email = email,
                            Emailencodingkey = emailCodingKey,LanguageLocaleKey = langLocaleKey ,LocalesIdKey = localIdKey,
                            ProfileId = profileId,TimeZonesIdKey = timeZonesIdKey,Division = division,Department = department,CompanyName = companyName);
            insert user;
        }           
        system.assertNotEquals(user.Id, null);
        return user;
    }
    
    /**
    * <P>Helper method to create Case.</P>
    */
    public static Case createCaseRecord(Id accountID, Id OwnerID, Id RecordTypeID, String Descrip){
        Case caseObj = new Case(accountId = accountID, ownerId = ownerID, recordtypeId = recordTypeID, Description = Descrip);
        insert caseObj;
        system.assertNotEquals(caseObj.Id, null);
        return caseObj;
    }
    
    /**
    * <P>Helper method to create Case.</P>
    */
    public static Approval.ProcessResult approvalWorkItemRequest(String setComments, String setAction, ID setWorkitemId){ 
        Approval.ProcessWorkItemRequest processWorkItemRecord = new Approval.ProcessWorkItemRequest();
        processWorkItemRecord.setComments(setComments);
        processWorkItemRecord.setAction(setAction);
        processWorkItemRecord.setWorkitemId(setWorkitemId);
        Approval.ProcessResult approvalResult = Approval.process(processWorkItemRecord);
        // Verify the results
        return approvalResult;
    }
    
    
    /**
    * <P>Helper method to create Queue.</P>
    */
    public static QueueSobject createQueue(string sObjectType)
    { 
        Group testGroup = new Group(Name = 'Queue', Type = 'Queue');
        insert testGroup;
    
        QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = sObjectType);
        insert testQueue;
        
        return testQueue;
    }
    
    public static ID getRecordTypeID(SObject sobjectName, string recordTypeName){
        Schema.DescribeSObjectResult describeSobjectSchema = sobjectName.getSObjectType().getDescribe(); 
        Map<String,Schema.RecordTypeInfo> recordTypeInfo = describeSobjectSchema.getRecordTypeInfosByName();
        return recordTypeInfo.get(recordTypeName).getRecordTypeId();
    }
    
    /*
    * @author           : Puneet Khosla
    * @description      : Method to create Business Academy Course with some default values
    * @param  cName     : Course Name
    * @param  doInsert  : Whether you wish to save the record or not
    * @return Object    : Return the course object
    */
    /*public static Course__c CreateCourse(string cName, boolean doInsert)
    {
        BuildGlobalSettingsCS();
        Course__c cs = new Course__c();
        cs.Name = cName;
        cs.Course_Summary__c = 'This is a TestClass Course';
        cs.Dress_Code__c = 'Casual';
        cs.Delivered_By__c ='QA';
        cs.Start_Date_Time__c = DateTime.now().addDays(1);
        cs.Finish_Date_Time__c = DateTime.now().addDays(1).addHours(1);
        cs.Location__c = 'Other';
        cs.Full_Address__c = 'Test Address';
        cs.Room_Details__c = '1';
        cs.Status__c = 'Scheduled';
        cs.Maximum_Participants__c = 45;
        if(doInsert)
            insert cs;
        return cs;
    }*/
    
    /*
    * @author           : Puneet Khosla
    * @description      : Method to create User Training record
    * @param  cs        : Course Record
    * @param  trainee   : User as Trainee
    * @param  doInsert  : Whether you wish to save the record or not
    * @return Object    : Return the User Training object
    */
    /*public static User_Training__c CreateUserTraining(Course__c cs, User trainee, boolean doInsert)
    {
        User_Training__c userTraining = new User_Training__c();
        userTraining.Trainee__c = trainee.id;
        userTraining.Course__c = (cs==null)?CreateCourse('TestClassCourse',true).id:cs.id;
        if(doInsert)
            insert userTraining;
        return userTraining;
    }*/
    
    /*
    * @author           : Puneet Khosla
    * @description      : Method to Build the Business Academy Location Details Custom Setting
    */
   /* public static void BuildBusinessAcademyLocationDetails()
    {
        list<BA_Location_Details__c> bldList = new list<BA_Location_Details__c>();
        bldList.add(new BA_Location_Details__c(Name='Bristol - Aztec', Location_Full_Address__c='Bristol - Aztec, Full Address'));
        bldList.add(new BA_Location_Details__c(Name='Bristol - Capricorn', Location_Full_Address__c='Bristol - Capricorn, Full Address'));
        insert bldList;
        
    }*/
    
    /*
    * @author           : Puneet Khosla
    * @description      : Method to Build the Global Settings Custom Setting
    */
   /* public static void BuildGlobalSettingsCS()
    {
        Global_Settings__c gs = Global_Settings__c.getInstance(UserInfo.getOrganizationId() );
        if(gs==null || gs.id==null)
        {
            gs.BA_Maximum_Participant__c = 50;
            gs.BA_Threshold_Limit__c = 75;
            insert gs;
        }
        
        
    }*/
    
    /*
    * @author           : Puneet Khosla
    * @descripition     : To create Users
    * @param  uLastName : Last Name of the user
    * @param  profileId : Profile Id of the user
    * @param  managerid : Id for the Manager
    * @param userIdset  : Set of ids to be avoided
    * @param  doInsert  : Whether you want to insert the user or just return the instance
    * @return Object    : Return the User record
    */
    public static User CreateUser(string uLastName, id profileId, id managerId, set<id> userIdset, boolean doInsert)
    {
        if(userIdSet == null)
            userIdSet = new set<id>();
        userIdSet.add(UserInfo.getUserId());
        
        list<User> usList = [Select id,alias,email,lastName, userName, managerId, name , ProfileId from User Where ProfileId =:profileId and IsActive = true 
                            and email <> '' and id not in :userIdset and managerId = :managerId LIMIT 1];
        User u;
        if(usList !=null && usList.size() > 0)
            u=usList.get(0);
        else
        {
            u= new User(Alias = uLastName.substring(5) , Email=uLastName + '@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName=uLastName, LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId, TimeZoneSidKey='America/Los_Angeles',
                            UserName= uLastName + '@testorg.com',managerId=managerId, IsActive=true);
            if(doInsert)
                insert u;
        }
        
        return u;    
    }
    
    /*
    * @author               : Puneet Khosla
    * @descripition         : To Retrieve Profile Id
    * @param  profileName   : Name of the profile
    * @return Object        : Profile record
    */
    public static Profile SelectProfile(string profileName)
    {
        return ([Select id From Profile where name = :profileName LIMIT 1].get(0));
    }
    
 
    /**
    * <P>Helper method to create Case of type Contract Validation.</P>
    * @author               : Sridhar Aluru
    * @descripition         : To create Case with RecordType "Contract Validations"
    */
    public static Case createCaseRecordforContractValidation (){
        String CaseRecordType = Label.ContractValidationRecordType;
        
        Case cs = new Case();
        ID recordtypeID = getRecordTypeID(cs,CaseRecordType);
        System.debug('Contract Validation Record Type ID >>>>>>>>>>>>>>>>>>>>>>>>'+recordtypeID);
              
        ID queueID = [Select ID,Name,Type,DeveloperName from Group where DeveloperName = 'Contracts_Validation' and Type='Queue'].ID;
        Profile profile = SelectProfile('Major and Public Sales');
     //   Profile profile1 = SelectProfile('Finance - Deal Review');
        Profile profile1 = SelectProfile('System Administrator');
        Case caseContractVal;
        Case caseContractVal1;
        Case caseContractVal2;
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){ 
        User usr= new User(Alias = 'tstusr', Email='testSalesUser@bt.com', 
                            EmailEncodingKey='UTF-8', LastName='testSalesUser', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles',
                            UserName='testSalesUser@bt.com', IsActive=true, EIN__c = '609701688');
        insert usr;  
        
        User usr1= new User(Alias = 'tstusr1', Email='testSalesUser1@bt.com', 
                            EmailEncodingKey='UTF-8', LastName='testSalesUser1', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profile.Id, TimeZoneSidKey='America/Los_Angeles',
                            UserName='testSalesUser1@bt.com', IsActive=true ,EIN__c = '609700088');
        insert usr1;
        
        User usr2= new User(Alias = 'tstusr1', Email='testUser2@bt.com', 
                            EmailEncodingKey='UTF-8', LastName='testUser2', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profile1.Id, TimeZoneSidKey='America/Los_Angeles',
                            UserName='testUser2@bt.com', IsActive=true ,EIN__c = '609709088');
        insert usr2;
         //call to custom settings
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
        
         Account acct = createAccount ('TestAccount');
        Opportunity opp = createOpp ('TestOpp','Test Stage',System.today()+15,acct.ID);                                       
         caseContractVal = new Case(Company__c = acct.ID, ownerId = queueID, Opportunity__c = opp.ID,recordtypeId = recordTypeID, Description = 'Test Description',
                       Status ='Awaiting Allocation',Type = 'Pre-Validation',Contract_Category__c = 'Lighthouse',Contact__c=usr.id,
                       X1st_Check__c=System.Today()-5,Subsequent_Check_1__c=System.Today()-4,Subsequent_Check_2__c=System.Today()-3,
                       Completion_of_Checklist__c=System.Today()-2);
        insert caseContractVal;
        
         caseContractVal1 = new Case(Company__c = acct.ID, ownerId = queueID, Opportunity__c = opp.ID,recordtypeId = recordTypeID, Description = 'Test Description',
                       Status ='Awaiting Allocation',Type = 'Pre-Validation',Contract_Category__c = 'GENERAL QUERIES',Contact__c=usr.id,
                       X1st_Check__c=System.Today()-5,Subsequent_Check_1__c=System.Today()-4,Subsequent_Check_2__c=System.Today()-3,
                       Completion_of_Checklist__c=System.Today()-2);
        insert caseContractVal1;
        
         caseContractVal2 = new Case(Company__c = acct.ID, ownerId = queueID, Opportunity__c = opp.ID,recordtypeId = recordTypeID, Description = 'Test Description',
                       Status ='Awaiting Allocation',Type = 'Pre-Validation',Contract_Category__c = 'ARTEMIS - BID',Contact__c=usr.id,
                       X1st_Check__c=System.Today()-5,Subsequent_Check_1__c=System.Today()-4,Subsequent_Check_2__c=System.Today()-3,
                       Completion_of_Checklist__c=System.Today()-2);
        insert caseContractVal2;
        
        system.assertNotEquals(caseContractVal.Id, null);
         
       }
     
         return caseContractVal;
     
    }
    
     /**
    * <P>Helper method to create 'Contract Validation Outcome' for a Case of type Contract Validation.</P>
    * @author               : Sridhar Aluru
    * @descripition         : To create a "Contract Validation Outcome" record
    */
   
    public static Contract_Validation_Outcome__c createContValidOutcome() {
        Case cs = createCaseRecordforContractValidation ();
        Contract_Validation_Outcome__c convalOutcome = new Contract_Validation_Outcome__c(Case__c=cs.id,
                    Reason_for_Failure__c='No approved commercials (or expired);Resign Pack missing/expired;Incorrect Entity type;Missing contact details');
        insert convalOutcome; 
        system.assertNotEquals(convalOutcome.Id, null);
        return convalOutcome;    
    }
}