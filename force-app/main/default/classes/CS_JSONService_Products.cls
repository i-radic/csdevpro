/**
 * @name CS_JSONService_Products
 * @description Base service for Products
 * @revision
 *
 */
public virtual class CS_JSONService_Products extends CS_JSONService_Base {
    /**
     * Class Constructor
     * @return  CS_JSONService_Products
     */
    public CS_JSONService_Products() {
        super();
    }
    
    public virtual override Object getResult(Id id) {
        Map<Id, ProductConfiguration> configs = getConfigurations(id);
        
        return getValues(configs);
    }
    
    @TestVisible
    protected virtual Object getValues(Map<Id, ProductConfiguration> configs) {
        List<Object> result = new List<Object>();
        
        for(ProductConfiguration cfg : configs.values()) {
            CS_JSON_Schema.CS_JSON_Object spec = this.setting.getDefinition(cfg.config.Product_Definition_Name__c);
            if(spec != null) {
                result.add(setConfig(cfg, spec));
                 System.debug('spec>>>'+JSON.serializePretty(spec));
                if(spec.relatedObjects != null && !spec.relatedObjects.isEmpty()){
                    for(CS_JSON_Schema.CS_JSON_Object obj : this.setting.definitions.get(0).relatedObjects) {
                        Id parentId = String.valueOf(cfg.config.get(obj.parentField));
                        System.debug(' CS_JSONService_Products parentId>>>'+parentId);
                        List<SObject> relatedRecords = Database.query(getQuery(obj, parentId));
                         System.debug('parentId>>>'+parentId);
                        if(!relatedRecords.isEmpty()) {
                            result.add(getValues(obj, relatedRecords.get(0)));
                        }
                    }
                }
            }
        }
        
        return result;
    }

    @TestVisible
    protected Map<String, Object> setConfig(ProductConfiguration cfg, CS_JSON_Schema.CS_JSON_Object spec) {
        Map<String, Object> cfgObj = new Map<String, Object>();
        
        cfgObj.putAll(getValues(spec, cfg.config));
        cfgObj.putAll(getAttributeValues(spec, cfg));
        
        if(!cfg.relatedProducts.isEmpty()) {
            List<Object> relatedProducts = new List<Object>();
            
            for(ProductConfiguration rCfg : cfg.relatedProducts.values()) {
                CS_JSON_Schema.CS_JSON_Object relatedSpec = this.setting.getDefinition(rCfg.config.Product_Definition_Name__c);
                if(relatedSpec != null) {
                    relatedProducts.add(setConfig(rCfg, relatedSpec));
                }
            }
            
            cfgObj.put(spec.relatedList, relatedProducts);
        }
        
        return cfgObj;
    }
    
    @TestVisible
    protected Map<String, Object> getAttributeValues(CS_JSON_Schema.CS_JSON_Object spec, ProductConfiguration cfg) {
        Map<String, Object> result = new Map<String, Object>();
        
        for(CS_JSON_Schema.CS_JSON_Field att : spec.attributes) {
            result.put(att.label, cfg.getAttributeValue(att));
        }
        
        return result;
    }
    
    @TestVisible
    protected virtual Set<Id> getConfigIds(Id id) {
        return new Map<Id, cscfga__Product_Configuration__c>((List<cscfga__Product_Configuration__c>)Database.query(getQuery(id))).keySet();
    }
    
    @TestVisible
    private String getQuery(Id id) {
        String queryStr =  String.format(QueryTemplate, new List<String>{
            'Id',
            'cscfga__Product_Configuration__c',
            getFilter(id)
        });
        System.debug('queryStr>>>'+queryStr);
        return queryStr;
    }
    @TestVisible
    private String getQuery(CS_JSON_Schema.CS_JSON_Object def, Id id) {
        return String.format(QueryTemplate, new List<String>{
            getFields(def.fields),
            def.type,
            getFilter(def.filter, id)
        });
    }
    @TestVisible
    private String getFields(List<CS_JSON_Schema.CS_JSON_Field> fieldsList) {
        return CS_Utl_Array.join(getFieldNames(fieldsList), ',');
    }
    
    @TestVisible
    private Set<String> getFieldNames(List<CS_JSON_Schema.CS_JSON_Field> fieldsList) {
        Set<String> fieldNames  = new Set<String>();
        for(CS_JSON_Schema.CS_JSON_Field field : fieldsList) {
            fieldNames.add(field.Name);
        }
        
        return fieldNames;
    }
    
    @TestVisible
    private String getFilter(String filter, Id id) {
        return String.format(filter, new List<String>{escapeId(id)});
    }
    
    @TestVisible
    private String getFilter(Id id) {
        System.debug('(this.setting.filter >>>>'+this.setting.filter );
        return String.format(this.setting.filter + ' AND cscfga__Product_Basket__c = {0}', new List<String>{escapeId(id)});
    }
    
    @TestVisible
    protected Map<String, Object> getConfigData(Set<Id> configIds) {
        return cscfga.API_1.getProductConfigurations(new List<Id>(configIds));
    }
    
    @TestVisible
    protected Map<Id, ProductConfiguration> getConfigurations(Id id) {
        Map<Id, ProductConfiguration> configs = new Map<Id, ProductConfiguration>();
        Map<String, Object> configData = getConfigData(getConfigIds(id));
        for(String configId : configData.keySet()) {
            if(configData.get(configId) instanceof cscfga__Product_Configuration__c) {
                ProductConfiguration cfg = new ProductConfiguration((cscfga__Product_Configuration__c) configData.get(configId),
                                                                    (List<cscfga__Attribute__c>) configData.get(configId + CS_JSON_Constants.Attributes_Suffix));
                configs.put(cfg.config.Id, cfg);
            }
        }
        
        Map<Id, ProductConfiguration> result = new Map<Id, ProductConfiguration>();
        for(ProductConfiguration cfg : configs.values()) {
            if(cfg.config.cscfga__Parent_Configuration__c != null && configs.get(cfg.config.cscfga__Parent_Configuration__c) != null
            && configs.get(cfg.config.cscfga__Parent_Configuration__c).relatedProducts != null && cfg.config.Id != null) {
                configs.get(cfg.config.cscfga__Parent_Configuration__c).relatedProducts.put(cfg.config.Id, cfg);
            }
            else {
                result.put(cfg.config.Id, cfg);
            }
        }
        return result;
    }
    
    public class ProductConfiguration {
        public cscfga__Product_Configuration__c config {get; set;}
        
        private Map<String, cscfga__Attribute__c> attributes {get; set;}
        
        public Map<Id, ProductConfiguration> relatedProducts {get; set;}
        
        public ProductConfiguration(cscfga__Product_Configuration__c cfg, List<cscfga__Attribute__c> atts) {
            this.config = cfg;
            this.attributes = new Map<String, cscfga__Attribute__c>();
            this.relatedProducts = new Map<Id, ProductConfiguration>();
            
            if(atts != null) {
                for(cscfga__Attribute__c att : atts) {
                    this.attributes.put(att.Name, att);
                }
            }
        }
        
        public cscfga__Attribute__c getAttribute(String name) {
            return attributes.get(name);
        }
        
        public Object getAttributeValue(CS_JSON_Schema.CS_JSON_Field att) {
            if(getAttribute(att.getAttributeName()) != null) {
                cscfga__Attribute__c attObject = getAttribute(att.getAttributeName());
                if(att.getType() == CS_JSON_Constants.FieldType_JSON) {
                    return JSON.deserializeUntyped(String.valueOf(attObject.get(att.getAttributeField())));
                }
                else {
                    return attObject.get(att.getAttributeField());
                }
            }
            
            return null;
        }
    }
}