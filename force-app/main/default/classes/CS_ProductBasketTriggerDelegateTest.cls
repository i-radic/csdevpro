@isTest
private class CS_ProductBasketTriggerDelegateTest {
    private static Account acct;
    private static Account acct1;
    private static Opportunity opp;
    private static Opportunity opp1;
    private static Contact con;
    private static Contact agent;
	
    static testMethod void createTestData() {
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;

        acct = new Account(name='test account');
        insert acct;
        
        opp = new Opportunity(AccountId=acct.Id,StageName='Prospecting',CloseDate=date.today(),NextStep='Test',Next_Action_Date__c=date.today(),name='test OpportunityCI' ,
              TotalOpportunityQuantity = 0);
        insert opp;
        
        Usage_Profile__c testUsageProfiles = new Usage_Profile__c(
          Active__c = true,
          Account__c = acct.Id,
          Expected_SMS__c = 10,
          Expected_Voice__c = 5,
          Expected_Data__c = 10
        );
    
        insert testUsageProfiles;
        
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
          Name = 'Test Basket Trigger',
          cscfga__Opportunity__c = opp.Id,
          ReSign__c = true
        );
        insert basket;
        
        basket.Approval_Status__c = 'Commercially Approved';
       
        basket.Has_Special_Conditions__c = false;
        
          
          basket.Competitive_Benchmarking__c = false;
          basket.Unlocking_Fees__c = false;
         basket.Total_Contract_Guarantee__c = false;
         basket.Total_Revenue_Guarantee__c = false;
         basket.Competitive_Clause__c = false;
         basket.Disconnect_Allowances__c = false;
         basket.Averaging_Co_Terminus__c = false;
         basket.Full_Co_Terminus__c = false;
         basket.Special_Conditions_Id__c = null;
         basket.Tenure__c = 24;
         basket.Care_Selected__c = false;
         basket.Payment_Terms__c = '';
         basket.Monthly_Recurring_Charges__c = 10;
         Integer pbMonth = (Integer) basket.Tenure__c;
       
         basket.Total_Charges__c=90;
         basket.Total_Cost__c=40;
         //basket.Number_of_users__c = 10;
         basket.Credit_fund_total__c=100;
         
         update basket;
        
        cscfga__Product_Basket__c basket1 = new cscfga__Product_Basket__c(
          Name = 'Test Basket Trigger1',
          
          cscfga__Opportunity__c = opp.Id
          
        );
        insert basket1;
        
        
        
         
        CS_Approval_Level__c testapproval = new CS_Approval_Level__c(
        
         Type__c='Acquisition',
         Is_Active__c= true,
         KPI__c='Gross Margin',
         Approval_Level__c =6
        );
        
        insert testapproval;
        
         CS_Approval_Level__c testapproval1 = new CS_Approval_Level__c(
        
        
         Type__c='Re-sign',
         Is_Active__c= true,
         KPI__c='Revenue Dilution',
         Approval_Level__c =5
        );
        
        insert testapproval1;
        
        CS_Approval_Level__c testapproval2 = new CS_Approval_Level__c(
        
         Type__c='SME',
         Is_Active__c= true,
         KPI__c='Investment Pot Usage Percentage',
         Approval_Level__c =7
        );
        insert testapproval2;
        
        
       cscfga__Product_Definition__c proddef1 = new cscfga__Product_Definition__c(
          Name = 'proddef1',
          cscfga__Description__c = 'prod description1'
        );
        insert proddef1;
    
        cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
          Name = 'Ad1',
          cscfga__Data_Type__c = 'String',
          cscfga__Type__c = 'Related Product',
          cscfga__Product_Definition__c = proddef1.Id,
          cscfga__high_volume__c = true
        );
        insert ad1;
        
      
        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
          Name = 'Pd2',
          cscfga__Description__c = 'Pd2'
        );
        insert pd2;
        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
          Name = 'Ad2',
          cscfga__Data_Type__c = 'String',
          cscfga__Type__c = 'User Input',
          cscfga__Product_Definition__c = pd2.Id
        );
        insert ad2;
        
        
        
    
        CS_Related_Products__c rp = new CS_Related_Products__c(
          Name = 'Pd1',
          Related_Products__c = 'Pd2'
        );
        insert rp;
        
        CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
          Name = 'Pd2',
          Attribute_Names__c = 'Ad2',
          List_View_Attribute_Names__c = 'Ad2',
          Total_Attribute_Names__c = 'Ad2'
        );
        insert rpa;
    
    
    
        cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
          Name = 'Root',
          cscfga__Product_Basket__c = basket.Id,
          cscfga__Product_Definition__c = proddef1.Id
        );
        pc1.Calculations_Product_Group__c = 'Future Mobile';
        insert pc1;
    
        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
          Name = 'Child',
          cscfga__Root_Configuration__c = pc1.Id,
          cscfga__Parent_Configuration__c = pc1.Id,
          cscfga__Product_Basket__c = basket.Id,
          cscfga__Product_Definition__c = proddef1.Id
        );
        pc2.Calculations_Product_Group__c = 'Future Mobile';
        insert pc2;
    
    
          cscfga__Product_Configuration__c pc3 = new cscfga__Product_Configuration__c(
          Name = 'Test Lone Worker',
          cscfga__Product_Basket__c = basket1.Id,
          cscfga__Product_Definition__c = proddef1.Id,
          cscfga__Configuration_Status__c = 'Valid',
          cscfga__Unit_Price__c = 10,
          cscfga__Quantity__c = 1,
          cscfga__Recurrence_Frequency__c = 12,
          tenure__c=36,
          Care__c= 'yes',
          Total_Charges__c=100.00,
          Total_Cost__c=200.00, 
          Number_of_users__c=20,
          Data_Recurring_Charge__c=100.00,
          Data_One_Off_Charge__c=100.00,
          Data_One_Off_Cost__c=100.00,
          Data_Recurring_Cost__c=50.00,
          SMS_Recurring_Charge__c=5.00,
          SMS_One_Off_Charge__c = 5.00,
          SMS_Recurring_Cost__c=5.00,
          SMS_One_Off_Cost__c = 5.00,
          Mobile_Voice_One_Off_Cost__c =5.00,
          Mobile_Voice_Recurring_Cost__c=5.00,
          Fixed_One_Off_Charge__c =5.00,
          Fixed_Recurring_Charge__c =5.00,
          Incoming_Recurring_Charge__c=5.00,
          Service_Recurring_Cost__c =5.00,
          OPEX_Recurring_Cost__c=5.00,
          Warning_Summary__c='Test',         
          Mobile_Voice_One_Off_Charge__c =5.00,
          Mobile_Voice_Recurring_Charge__c=5.00,
          Gross_Margin__c=85.00,
          SpecCon_Competitive_Clause_Cost__c=40.00,
          SpecCon_Disconnection_Allowance_Cost__c=22.00,
          Credit_fund_total__c=17.00,
          SpecCon_Full_Co_terminus_Cost__c=18.00
        );
        pc3.Calculations_Product_Group__c = 'Future Mobile';
        insert pc3;
        
         cscfga__Product_Configuration__c pc4 = new cscfga__Product_Configuration__c(
          Name = 'Special Conditions',
          cscfga__Product_Basket__c = basket1.Id,
          cscfga__Product_Definition__c = proddef1.Id,
          cscfga__Configuration_Status__c = 'Valid',
          cscfga__Unit_Price__c = 10,
          cscfga__Quantity__c = 1,
          cscfga__Recurrence_Frequency__c = 12,
          tenure__c=36,
          SpecCon_Averaging_co_terminus__c = true,
          SpecCon_Competitive_Benchmarking__c= true,
          SpecCon_Competitive_Clause__c=true,
          SpecCon_Competitive_Clause_Cost__c = 10.00,
          SpecCon_Disconnection_Allowance__c = true,
          SpecCon_Full_co_terminus__c= true,
          SpecCon_Full_Co_terminus_Cost__c= 34.40,
          Payment_Terms__c='45',
          SpecCon_Total_Contract_Guarantee__c= true,
          SpecCon_Total_Revenue_Guarantee__c=true,
          SpecCon_Unlocking_Fees_Cost__c=44.44,
          SPecCon_Unlocking_Fees__c= true
        );
        pc4.Calculations_Product_Group__c = 'Future Mobile';
        insert pc4;

         basket1.Approval_Status__c = 'Approved';
       
        basket1.Has_Special_Conditions__c = true;
        
          
          basket1.Competitive_Benchmarking__c = true;
          basket1.Unlocking_Fees__c = true;
         basket1.Total_Contract_Guarantee__c = true;
         basket1.Total_Revenue_Guarantee__c = true;
         basket1.Competitive_Clause__c = true;
         basket1.Disconnect_Allowances__c = true;
         basket1.Averaging_Co_Terminus__c = true;
         basket1.Full_Co_Terminus__c = true;
         basket1.Special_Conditions_Id__c =pc4.id;
         basket1.Tenure__c = 24;
         basket1.Care_Selected__c = true;
         basket1.Payment_Terms__c = '';
         basket1.Monthly_Recurring_Charges__c = 10;
         basket1.Total_Charges__c=90;
         basket1.Total_Cost__c=40;
        
         basket1.Credit_fund_total__c=100;
         
         update basket1;
        
        
        
    }
    
     static testMethod void createIndirectAccTestData() 
     {    
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
         
         OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;
        
        acct1 = new Account(name='Indirect test account');
        acct1.Staging_Area_Classification__c ='Non Managed Operational Account';
        insert acct1;
        
        opp1 = new Opportunity(AccountId=acct1.Id,StageName='Prospecting',CloseDate=date.today(),NextStep='Test',Next_Action_Date__c=date.today(),name='test OpportunityCI' ,
              TotalOpportunityQuantity = 0);
        insert opp1;
        
        Usage_Profile__c testUsageProfiles = new Usage_Profile__c(
          Active__c = true,
          Account__c = acct1.Id,
          Expected_SMS__c = 10,
          Expected_Voice__c = 5,
          Expected_Data__c = 10
        );
    
        insert testUsageProfiles;
        
        
        cscfga__Product_Basket__c basket1 = new cscfga__Product_Basket__c(
          Name = 'Test Basket Trigger1',
          cscfga__Opportunity__c = opp1.Id,
          ReSign__c = true

        );
        insert basket1;        
        basket1.Approval_Status__c = 'Commercially Approved';
        basket1.Has_Special_Conditions__c = false;
        basket1.Competitive_Benchmarking__c = false;
        basket1.Unlocking_Fees__c = false;
        basket1.Total_Contract_Guarantee__c = false;
        basket1.Total_Revenue_Guarantee__c = false;
        basket1.Competitive_Clause__c = false;
        basket1.Disconnect_Allowances__c = false;
        basket1.Averaging_Co_Terminus__c = false;
        basket1.Full_Co_Terminus__c = false;
        basket1.Special_Conditions_Id__c = null;
        basket1.Tenure__c = 24;
        basket1.Care_Selected__c = false;
        basket1.Payment_Terms__c = '';
        basket1.Monthly_Recurring_Charges__c = 10;
        basket1.Total_Charges__c=90;
        basket1.Total_Cost__c=40;       
         //basket.Number_of_users__c = 10;
        basket1.Credit_fund_total__c=100;         
        update basket1;
    }
}