global class BatchScheduleContractInActive implements Schedulable{
   global void execute(SchedulableContext sc) {
        Date myDate = (Date.today()- 22);
        String sDate = String.valueOf(myDate);
        BatchContractInActive b = new BatchContractInActive('SELECT Id FROM Contract WHERE Load_Date__c < '+ sDate +' ');
        database.executebatch(b, 200);
    }
}