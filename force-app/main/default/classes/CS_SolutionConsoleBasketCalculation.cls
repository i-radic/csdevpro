global class CS_SolutionConsoleBasketCalculation implements cssmgnt.RemoteActionDataProvider   {
    
    global Map<String, Object> getData(Map<String, Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>(); 
        String basketId = String.valueOf(inputMap.get('basketId'));
        Boolean asyncMode = false;
        Id asyncJobId;
        Map<Id, cscfga__Product_Configuration__c> configs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>
        {new cscfga__Product_Basket__c(Id = basketId)});
        
        cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
        System.debug('test:::'+CS_CR_Basket_Calculations__c.getInstance().Sync_Basket_Size_Limit__c+':::'+configs.size());
        
        /*if (configs.size() <= CS_CR_Basket_Calculations__c.getInstance().Sync_Basket_Size_Limit__c) {
            asyncMode = false;
            CS_ProductBasketService.prepareTechFund(basket, configs, 'Tech_Fund__c');
            try{
            CS_Util.upsertReportConfigurationAttachment(configs.keySet());}catch(Exception exc){}
            try{
            CS_ProductBasketService.calculateBasketTotals(configs, basket);}catch(Exception exc){}
            CS_ProductBasketService.setSpecialConditions(configs, basket);
            update basket;
            returnMap.put ('DataFound', true);
        } else {
            
            asyncMode = true;
            asyncJobId = System.enqueueJob(new CS_CustomRedirectQueueable(basket, configs,'Configration'));
            returnMap.put ('DataFound', true);
        }*/
        
        asyncJobId = System.enqueueJob(new CS_CustomRedirectQueueable(basket, configs,'Configration'));
        returnMap.put ('DataFound', true);
        return returnMap;
        
    }

}