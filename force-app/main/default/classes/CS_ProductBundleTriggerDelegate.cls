/**
 * Trigger delegate implementation for Product Bundle trigger logic
 */
public with sharing class CS_ProductBundleTriggerDelegate extends CS_TriggerHandler.DelegateBase {
	
	// do any preparation here – bulk loading of data etc
	public override void prepareBefore() {
	}
	
	// do any preparation here - bulk loading of data etc
	public override void prepareAfter() {
		
	}
	
	// Apply before insert logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	public override void beforeInsert(sObject o) {
		
	}
	
	// Apply before update logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	public override void beforeUpdate(sObject old, sObject o) {
		
	}

	// Apply after insert logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	public override void afterInsert(sObject o) {
		
	}

	// Apply after update logic to this sObject. DO NOT do any SOQL
	// or DML here – store records to be modified in an instance variable
	// which can be processed by the finish() method
	public override void afterUpdate(sObject old, sObject o) {
		
	}

	// finish logic - process stored records and perform any dml action
	// updates product baskets if bundle is desynchronised
	public override void finish() {
	
	}
}