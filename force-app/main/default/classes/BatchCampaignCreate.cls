global class BatchCampaignCreate implements Database.Batchable<sobject>{ 
     /**********************************************************************
     Name:  BatchCampaignCreate.class()
     Copyright © 2010  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    This Class is a Batch Apex class used to bulk load campaigns into Salesforce.
    It is called from the CreateBulkCampaigns class which in turn is called from the BulkCampaignCreate 
    VisualForce page.
    
    This class implements the BatchableContext interface and contains 3 key methods for bulk Apex operations
    These are the start execute and finish methods.
    The calling class sets the querylocator and then calls the execute method.
    This then repeatedly calls the execute methods with a list of records from the bulkcampaignstaging table.
    The execute methods hands off to a function called CreateMembers.
    This takes a batch of staging records and performs all of the logic of creating child campaigns
    and campaign member records.
    Because this is batch apex the usual governor limits do not apply.
    Thus as long as a single call to the execute method does not exceed any governor limits, it is possible
    to process as many bulkcampaignstaging records as we like.
    
    The CreateMembers function works slightly differently depending whether the parent campaign (necessary to initiate a bulk load)
    is a BTLB or a Corporate Campaign Type.
    
    For BTLB child campaigns are created for every new BTLB in the batch.
    For Corporate child campaigns are never created; all campaign members are created under the parent campaign.
    
    There is also an additional Campaign type which was added later "BTLB(non split)" which behaves in the same way as
    Corporate, i.e all members are created under a single parent campaign.
    
                                                          
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION    AUTHOR           DATE            DETAIL                        FEATURES
    1.0        Greg Scarrtott   2/05/2010       INITIAL DEVELOPMENT           Initial Build: 
    1.1        John McGovern    21/10/10        Rework on Name and Contact    Bypass field size limitations on campaign name and default contact name
    1.2        John McGovern    28/10/10        Add in Warrington options     Allow warrington campaigns to be created excluded from BTLB ones. RKW annotations to changes
    1.21       John McGovern    3/11/10         Change warrington to LEM Base
    1.22       Dan Measures     20/01/11        CR2039 - corp do not want inactive Contacts added as a campaign member
    1.3        John McGovern    29/11/12        CR3367 - Adapted to accept CUG as part of the input data in additiona to SACs amd LEs
    ***********************************************************************/
     
    
    public String query;
    public String email;
    public Bulk_Campaign_Run__c runrecord;
    
    //Constants
    final string CAMPAIGN_TYPE_CORPORATE_MARKETING='Marketing';
    final string CAMPAIGN_TYPE_CORPORATE_BTLB='BTLB';
    //RKW001
    final string CAMPAIGN_TYPE_LEM='LEM Base';
    //RKW001-END
    global database.querylocator start(Database.BatchableContext BC){
    
    return Database.getQueryLocator(query);}
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<bulkcampaignstaging__c> stageblocklist = (List<bulkcampaignstaging__c>)scope;
        //Get the latest run record
        Bulk_Campaign_Run__c thisRun = [select Delete_source_data_after_load__c,Total_Records_in_Batch__c, Total_Errors_In_Batch__c, Tasks_Created__c,Notes__c,New_Notes__c, Name,Dummy_Contacts_Created__c,ChildCampaignsCreated__c,Campaign__c,Campaign_Members_Created__c,Bulk_Campaign_Batch__c,BatchApexID__c From Bulk_Campaign_Run__c where batchapexid__c = :BC.getJobId()];
        runrecord = thisRun;

        system.debug('eachexecute: '+ runrecord.Total_Records_in_Batch__c);
        
        Bulk_Campaign_Run__c latestrunrecord = createmembers(stageblocklist);
        
    }

    global void finish(Database.BatchableContext BC){
        system.debug('point3');
    
    }
    
    public Bulk_Campaign_Run__c createmembers (list<bulkcampaignstaging__c> stagelist){
        //Takes a bulk campaign run record and a list of staging records. Performs updates/inserts to campaigns + campaign members
        //and returns the updated runrecord
        
        //Create a map of id's from the incoming list.
        //We will default the string element to "none" and will
        //populate with Account or Contact later.
        //If it remains "none" we will know that the id was invalid
        
        boolean bIsBTLB = false;
         //RKW001
        boolean bIsLEM = false;
        //RKW001-END        
        system.debug('CalledCreateMembers');
        system.debug('runrecord is now :' + runrecord);
        
        Map<string,String> accmainmap = new Map<string,String>();
        Map<string,bulkcampaignstaging__c> stagingmap = new Map<string,bulkcampaignstaging__c>();
        Set <string> einset = new Set<string>();
        Set<String> accountids = new Set<string>();
        Map<String,String> leadlocatormap = new Map<String,String>();
        
        campaign parentcampaign = [select Campaign_Type__c,name,x_day_rule__c,chapter__c from campaign where id = :runrecord.Campaign__c];
        system.debug('*******parentcampaign'+parentcampaign); 
        if(parentcampaign.Campaign_Type__c == CAMPAIGN_TYPE_CORPORATE_MARKETING || parentcampaign.Campaign_Type__c==CAMPAIGN_TYPE_CORPORATE_BTLB){
            bIsBTLB = True;
        }
        //RKW001
        if(parentcampaign.Campaign_Type__c == CAMPAIGN_TYPE_LEM ){
            bIsLEM = True;
        }
        //RKW001-END
                
        Set<string> accmainset = new set<string>();
        
        //Set up a map to store the le or sac code followed by a "not found". We will populate this with a "found" value if the query finds
        //an account with this le or SAC code and use it to report errors back to the users.
        Set <string> saccodes = new Set<string>();
        Set <string> lecodes = new Set<string>();
        Set <string> cugcodes = new Set<string>(); //CR3367
        
        for (bulkcampaignstaging__c stagerec:stagelist){
            system.debug('incomingrec:'+stagerec);
                if(stagerec.le_or_sac_code__c.length() >= 3 && stagerec.le_or_sac_code__c.length() <= 5){  //  CR2039 - sac codes can be 3, 4 or 5 in length
                    system.debug('xxx:adding a sac');
                    saccodes.add(stagerec.le_or_sac_code__c);
                }else if(stagerec.le_or_sac_code__c.length()==10){
                    lecodes.add(stagerec.le_or_sac_code__c);
                }//CR3367
                else if(stagerec.le_or_sac_code__c.length()>10){
                    cugcodes.add(stagerec.le_or_sac_code__c);
                    
                }
                system.debug('**********cugcodes'+cugcodes.size());
                accmainmap.put(stagerec.le_or_sac_code__c,'not found');
                stagingmap.put(stagerec.le_or_sac_code__c,stagerec);
                if(stagerec.ein__c !=null){
                    einset.add(stagerec.ein__c);
                }
                if(stagerec.lead_locator_role__c !=null){
                    leadlocatormap.put(stagerec.le_or_sac_code__c,stagerec.lead_locator_role__c);
                }
        }
        
        //Set the total records counter
            If (runrecord.Total_Records_in_Batch__c == null){
                runrecord.Total_Records_in_Batch__c = stagelist.size();
            }else{
                runrecord.Total_Records_in_Batch__c = runrecord.Total_Records_in_Batch__c + stagelist.size();
            }
            
        //Get all user records for the EINs in the input file
        
        List<user> usereinlist = new List<user>([select name,ein__c from user where ein__c in :einset]);
        //put these in a map
        Map<string,user> einusermap = new Map<string,user>();
        for (user userrec:usereinlist){
            einusermap.put(userrec.ein__c,userrec);
        }
        
        //Load the map id's into a set, which we can use in our query
        accmainset = accmainmap.keyset();
        
        //*** IS THIS A BTLB CAMPAIGN LOAD OR A CORP LOAD ?
        List<Account> accountcontactlist = new List<Account>();
        List<contact> thisacccontacts = new list<contact>();
        
        //BTLB
        system.debug('JMM debug: lecodes '+ lecodes + 'saccodes' + saccodes + 'cugcodes' + cugcodes);
        Boolean bIsUserMismatch = false;
        //RKW001
        //if(bIsBTLB){
        if(bIsBTLB || bIsLEM){
        //RKW001END        
            accountcontactlist = [select id,name,btlb_name__c,sac_code__c,le_code__c,CUG__c,ownerid,phone,(select contact_key_decision_maker__c,kdm_priority__c,LastModifiedDate from contacts) from Account where (le_code__c in :lecodes) or (sac_code__c in :saccodes) or (CUG__c in :cugcodes)]; 
            //GS
            system.debug('********accountcontactlist'+accountcontactlist.size());
            Set<String> owneridset = new Set<String>();
            for (Account accountrec:accountcontactlist){
                owneridset.add(accountrec.ownerid);
            }
            Map<id,user> roleUserMap = new Map<id,user>([select id,userroleid from user where id in :owneridset]); 
            //END
            
            system.debug('runningBTLB' + lecodes+'&&&&&&&&'+roleUserMap);
            //Need to remove the le + sacs from this list if the owners of the accounts are not the same as the running user
            //these also need to be reported as errors
            if(parentcampaign.Campaign_Type__c==CAMPAIGN_TYPE_CORPORATE_BTLB){
                for (Account accountrec:accountcontactlist){
                    system.debug('******userroleid@@@@@@@'+roleUserMap.get(accountrec.ownerid).userroleid);
                    if(roleUserMap.get(accountrec.ownerid).userroleid != userinfo.getuserroleid()){
                        //remove the sac and/or the le from the sets
                        system.debug('******userroleid-----'+bIsUserMismatch);
                        bIsUserMismatch = true;
                        lecodes.remove(accountrec.le_code__c);
                        saccodes.remove(accountrec.sac_code__c);
                    }
                }
                if(bIsUserMismatch){
                    //Run the query again. It will only have the accounts where the userid and owner id match
                    accountcontactlist = [select id,name,btlb_name__c,sac_code__c,le_code__c,CUG__c,ownerid,phone,(select contact_key_decision_maker__c,kdm_priority__c,LastModifiedDate from contacts) from Account where (le_code__c in :lecodes) or (sac_code__c in :saccodes) or (CUG__c in :cugcodes)]; 
                	system.debug('****%%%%%accountcontactlist'+accountcontactlist.size());
                }
            }
        }else{
        //CORP
            system.debug('runningCorp' + saccodes);
          //  accountcontactlist = [select id,name,btlb_name__c,sac_code__c,le_code__c,ownerid,phone,(select contact_key_decision_maker__c,kdm_priority__c,LastModifiedDate from contacts) from Account where sac_code__c in :saccodes]; 
            //  DM 20/01/2011 - CR2039, corp do not want inactive contacts assigned as campaign members
            accountcontactlist = [select id,name,btlb_name__c,sac_code__c,le_code__c,ownerid,phone,(select contact_key_decision_maker__c,kdm_priority__c,LastModifiedDate from contacts where Status__c = 'Active') from Account where sac_code__c in :saccodes];
        }
        
        //Now we have a definitive list , accountcontactlist of all accounts and contacts.
        //Next create any dummy contacts, where accounts don't have any
        List<accountteammember> accteammember = new List<accountteammember>();
        accteammember = [select teammemberrole,accountid,userid from accountteammember where accountid in :accountcontactlist];
        List<contact> dummyContactList = new List<contact>();
        Map<id,contact> dummyMap = new Map<id,contact>();
        
        //Create a map of account id and accountteamember reocrd
        system.debug('thisaccountcontlist' + accountcontactlist.size());
            
        //Go through the accounts and create dummy contacts where needed    
        for (Account accountrec:accountcontactlist){
        
            //Update the map which stores whether we successfully found the le or sac code or not
            if((accountrec.le_code__c !=null & lecodes.contains(accountrec.le_code__c))){
                system.debug('found lecode '+ accountrec.le_code__c);
                accmainmap.put(accountrec.le_code__c,'Found');
            }else if(accountrec.SAC_Code__c !=null & saccodes.contains(accountrec.sac_code__c)){
                system.debug('found sac code '+ accountrec.sac_code__c);
                accmainmap.put(accountrec.sac_code__c,'Found');
            }else if(accountrec.CUG__c !=null & cugcodes.contains(accountrec.CUG__c)){
                system.debug('found CUG code '+ accountrec.CUG__c);
                accmainmap.put(accountrec.CUG__c,'Found');
            }
    		
            system.debug('accountrec.contacts'+accountrec.contacts.size());
            thisacccontacts = accountrec.contacts;
            
            //Create a dummy if there aren't any
            Contact dummycontact = new contact();
            if(thisacccontacts.size() ==0){
                system.debug('Create dummy contact');
                dummycontact.firstname = 'BT DEFAULT';
                //change by JMM 18/10 to reduce account characters and errors
                
                String jmmString = accountrec.name;
                Integer jmmResult = jmmString .length();
                if(jmmResult > 60){
                    jmmResult = 60;
                }                
                dummycontact.lastname = 'CONTACT:'+accountrec.name.substring(0,jmmResult );
                
             // dummycontact.lastname = 'CONTACT:'+accountrec.name;
                dummycontact.salutation='Mr';
                //change by JMM 17/9
                //dummycontact.phone ='0800654321';
                dummycontact.phone ='0800800152';
                if (accountrec.phone !=null){
                    dummycontact.phone =accountrec.phone;
                }
                dummycontact.job_function__c = 'ADMIN';
                dummycontact.phone_consent__c = 'Yes';
                dummycontact.email = 'default@default.com';
                dummycontact.address_consent__c='Yes';
                dummycontact.email_consent__c='Yes';
                dummycontact.SAC_Code__c = '1234';
                dummycontact.accountid = accountrec.id;
                dummycontact.ownerid = accountrec.ownerid;
                dummyContactList.add(dummycontact);
                
                //Ad this contact to the acc list so we have a single list to iterate
                thisacccontacts.add(dummycontact);
                accountrec.contacts.add(dummycontact); 
                dummyMap.put(accountrec.id,dummycontact);
                
                If (runrecord.dummy_contacts_created__c == null){
                    runrecord.dummy_contacts_created__c = 1;
                }else{
                    runrecord.dummy_contacts_created__c = runrecord.dummy_contacts_created__c + 1;
                }
                
            }   
            //Add this account id to a set so we can query the account contacts records later
            accountids.add(accountrec.id);
        }
        system.debug('****@@@@@@accountids'+accountids.size());
        //Create dummy contacts
        try{
        system.debug('****@@@@@@dummyContactList'+dummyContactList.size());
        Database.insert(dummyContactList,false);
        }catch(exception ex){
        system.debug('Exception :'+ex);    
        }
        //We now know which input id's we don't have records for.
        //Report these as errors on the page
        List<string> inputerrors = new List<string>();
        inputerrors = accmainmap.values();
        
        //Loop through the list and count the errors
        Integer errorcount = 0;
        for (string errorstring:inputerrors){
            if(errorstring=='not found'){
                errorcount = errorcount+1;
            }
        }
        
        //Produce a string to write to the log containing unfound id's
        //Get a keyset of the mainmap
        Set<String> mainKeys = accmainmap.keyset();
        String errorOutputString = '';
        system.debug('gps'+mainKeys);
 
        
        for (string thisKey:mainKeys){
            system.debug('keyval'+accmainmap.get(thisKey));
            if(accmainmap.get(thisKey)=='not found'){
                errorOutputString = errorOutputString+thiskey + ',';
            }
        }
        system.debug('gpserrstring' + errorOutputString);
        
        //Write the string back to the log
        runrecord.error_details__c = runrecord.error_details__c + errorOutputString;
        
        If (runrecord.Total_Errors_In_Batch__c == null){
            runrecord.Total_Errors_In_Batch__c = errorcount;
        }else{
            runrecord.Total_Errors_In_Batch__c = runrecord.Total_Errors_In_Batch__c + errorcount;
        }
        //Now all of our accounts have at least one contact.
        system.debug('gpsfinal' + runrecord.Total_Errors_In_Batch__c);
        //The next section creates the campaign members and child campaigns(for BTLB)
        //Need to branch the logic as it is quite different for BTLB and Corp
        
        
        List<campaignmember> campmemberlist = new list<campaignmember>();
        Boolean bAlreadyHaveChildCampaign = false;
        Double bestyet = 0;
        Datetime bestmodified;
        Campaignmember newCampMem;
        
        //Create list to store corp tasks
        List<task> newtasklist = new list<task>();
        String campaignMemberNotes ='';
        system.debug('got to break with ' + bIsbtlb);               
        //BTLB 
        
        //RKW001
        //if(bIsBTLB){
        if(bIsBTLB || bIsLEM){
        //RKW001END
            //Get a list of all child campaigns created so far.
            List<Campaign> childcamplist = [select name from campaign where parentid = :runrecord.campaign__c];
            //Go through all of the accounts again.
            Campaign btlbchildcampaign ;
            system.debug('about to enter btlb contact loop'+childcamplist.size()+'&&&&&&&&'+accountcontactlist.size());
            for (Account accountrec:accountcontactlist){
                //change by JMM 18/10 to reduce BTLB name and errors
                /*
                String myString = accountrec.btlb_name__c;
                Integer result = myString.length();
                if(result > 28){
                    String newString = accountrec.btlb_name__c.substring(27);
                    if(newString  == ' '){
                            result = 26;
                        }else {
                            result = 27;
                        }
                }
                */
                
                List<contact> acccontacts = accountrec.contacts;
                system.debug('GPS:Processing new BTLB account'+acccontacts.size());
                newCampMem = new Campaignmember();
                
                //Do we already have a campaign for the current BTLB ?
                //RKW001 - don't need child campaigns if LEM
                if (bIsBTLB) { //implies not LEM
                //RKW001-END                 
                bAlreadyHaveChildCampaign = false;
                for (campaign thiscampaign:childcamplist){
                        //change by JMM 18/10 to reduce BTLB name and errors
                        //if (thiscampaign.name == parentcampaign.name + ':' + accountrec.btlb_name__c.substring(5,result)){
                        if (thiscampaign.name == parentcampaign.name + ':' + accountrec.btlb_name__c){
                                //Already have one
                                btlbchildcampaign = new campaign();
                                btlbchildcampaign = thiscampaign;
                                btlbchildcampaign.ownerid=accountrec.ownerid;
                                bAlreadyHaveChildCampaign = true;
                        }
                }   
                //RKW001
                }
                //RKW001-END
                             
                //If not create one(if the standard BTLB campaign type is used and not the "non split" option).
                //RKW001               
                //if(parentcampaign.Campaign_Type__c==CAMPAIGN_TYPE_CORPORATE_BTLB){
                if(parentcampaign.Campaign_Type__c==CAMPAIGN_TYPE_CORPORATE_BTLB ||
                   parentcampaign.Campaign_Type__c==CAMPAIGN_TYPE_LEM){
                //RKW001-END                
                    //Set the child campaign as the parent 
                    bAlreadyHaveChildCampaign = true;
                    btlbchildcampaign = parentcampaign; 
                }
                
                If(!bAlreadyHaveChildCampaign){
                        //Create a child campaign of the current parent
                        system.debug('GPS:Creating new BTLB child campaign');
                        btlbchildcampaign = new campaign();
                        btlbchildcampaign.type = 'Advertisement';
                        btlbchildcampaign.status = 'Planned';
                        //change by JMM 18/10 to reduce BTLB name and errors
                     
                        //btlbchildcampaign.name = parentcampaign.name+':' + accountrec.btlb_name__c.substring(5,result );
                        btlbchildcampaign.name = parentcampaign.name+':' + accountrec.btlb_name__c;
                        btlbchildcampaign.isActive = True;
                        btlbchildcampaign.x_day_rule__c=parentcampaign.x_day_rule__c;
                        //Added by GS 21-03-2011 Need to take the Chapter value down to the child campaign
                        btlbchildcampaign.chapter__c = parentcampaign.chapter__c;
                        //End of change
                        btlbchildcampaign.parentid = runrecord.campaign__c;
                        btlbchildcampaign.ownerid=accountrec.ownerid;
                    try{
                        Database.insert(btlbchildcampaign,false);
                        
                        //Also add it to our list - for checking for existing children.
                        childcamplist.add(btlbchildcampaign);
                        
                        If (runrecord.childcampaignscreated__c == null){
                            runrecord.childcampaignscreated__c = 1; 
                        }else{
                            
                            runrecord.childcampaignscreated__c = runrecord.childcampaignscreated__c + 1;
                        }
                    }catch(exception ex){
        				system.debug('Exception :'+ex);    
                    }
                } 
                 
                //Now create the campaign member record with the correct campaign.
                
                //Get the best contact to become the campaign member
                bestyet = 0;
                bestmodified = datetime.newInstance(1);
                system.debug('xxx' + acccontacts.size());
                
                //Add the dummy contact to acccontacts if appropriate
                if (acccontacts.size()==0){
                    system.debug('adddumm');
                    acccontacts.add(dummyMap.get(accountrec.id));
                }
                
                for(contact thiscontact:acccontacts){
                    system.debug('GPS:about to create member');
                    //Is this a higher priority contact than the previous ?
                    //If(thiscontact.contact_key_decision_maker__c !=null){
                    system.debug('why' + thiscontact.kdm_priority__c +bestyet +thiscontact.lastmodifieddate + bestmodified+'***'+acccontacts.size());
                    
                     if (thiscontact.kdm_priority__c > bestyet || bestmodified ==null ||(thiscontact.kdm_priority__c==bestyet && thiscontact.lastmodifieddate > bestmodified)||acccontacts.size()==1){   
                        newCampMem = new campaignmember();
                        system.debug('Creating campaign member');
                        system.debug('campidis'+ btlbchildcampaign.id);
                        newCampMem.campaignid =btlbchildcampaign.id;
                        newCampMem.contactid=thiscontact.id;
                        bestyet = thiscontact.kdm_priority__c;
                        bestmodified = thiscontact.lastmodifieddate;
                        
                     }
                }
                if(newCampMem.CampaignId !=null){
                        //If this staging record has notes, add them to the campaign member record
                        if(stagingmap.get(accountrec.sac_code__c)!=null){
                            if(stagingmap.get(accountrec.sac_code__c).campaign_member_notes__c !=null){
                                campaignMemberNotes = stagingmap.get(accountrec.sac_code__c).campaign_member_notes__c;
                            }
                        }else if (stagingmap.get(accountrec.le_code__c)!=null){
                            if(stagingmap.get(accountrec.le_code__c).campaign_member_notes__c !=null){
                                campaignMemberNotes = stagingmap.get(accountrec.le_code__c).campaign_member_notes__c;
                            }
                        }
                        
                        if(campaignMemberNotes !=''){
                            newCampMem.campaign_member_notes__c = campaignMemberNotes;
                        }
                        campaignMemberNotes ='';
                        campmemberlist.add(newCampMem);
                        system.debug('xyz'+ newcampmem.CampaignId);
                        If (runrecord.campaign_members_created__c == null){
                            runrecord.campaign_members_created__c = 1;
                        }else{
                            system.debug('************campaign_members_created__c'+runrecord.campaign_members_created__c);
                            runrecord.campaign_members_created__c = runrecord.campaign_members_created__c + 1;
                        }
            }
            
        
            }
            
            
        }else{
        //CORP
            
            Id assigntome;
            
            List<Account_Team_Contacts__c> accountteam  = new List<Account_Team_Contacts__c>();
            Map<id,Account_Team_Contacts__c> accountteammap = new Map<id,Account_Team_Contacts__c>();
            accountteam = [select Account_Owner_Primary__c,account__c from Account_Team_Contacts__c where Account__c in :accountids];
            Date corpDueDate;
            Integer corpReminder = 0;
            String corpSubject ='';
            String corpDetails = '';
            String corpPrimaryCampaign = '';
            String corpCollatUrl = '';
            String corpCollaText = '';
            String corpSystemText = '';
            String corpSystemUrl = '';
            Boolean bIsReminderSet = false;
            
            for (Account_Team_Contacts__c teammemrec:accountteam){
                accountteammap.put(teammemrec.account__c,teammemrec);
            }
            
            Set<id> contactIdSet = new Set<id>();
                
            for (Account accountrec:accountcontactlist){
                List<contact> acccontacts = accountrec.contacts;
                system.debug('corpContCount' + acccontacts.size());
                
                //****************
                //Get the best contact to become the campaign member
                bestyet = 0;
                bestmodified = datetime.newInstance(1);
                
                    //Add the dummy contact to acccontacts if appropriate
                    if (acccontacts.size()==0){
                        system.debug('adddumm');
                        acccontacts.add(dummyMap.get(accountrec.id));
                        system.debug('addedHere');
                    }
                    for(contact thiscontact:acccontacts){
                        //Is this a higher priority contact than the previous ?
                        //If(thiscontact.contact_key_decision_maker__c !=null){
                         if (thiscontact.kdm_priority__c > bestyet || bestmodified ==null ||(thiscontact.kdm_priority__c==bestyet && thiscontact.lastmodifieddate > bestmodified)||acccontacts.size()==1){   
                            newCampMem = new campaignmember();
                            system.debug('Creating campaign member');
                            system.debug('campidis'+ runrecord.Campaign__c);
                            newCampMem.campaignid =runrecord.Campaign__c;
                            newCampMem.contactid=thiscontact.id;
                            bestyet = thiscontact.kdm_priority__c;
                            bestmodified = thiscontact.lastmodifieddate;
                            
                         }
                    }
                    
                    if(newCampMem.CampaignId !=null){
                        //If this staging record has notes, add them to the campaign member record
                        if(stagingmap.get(accountrec.sac_code__c).campaign_member_notes__c !=null){
                            campaignMemberNotes = stagingmap.get(accountrec.sac_code__c).campaign_member_notes__c;
                            newCampMem.campaign_member_notes__c = campaignMemberNotes;
                        }
                        //Only add this contact if not already present
                        if(!contactIdSet.contains(newCampmem.contactid)){
                            campmemberlist.add(newCampMem);
                            If (runrecord.campaign_members_created__c == null){
                                runrecord.campaign_members_created__c = 1;
                            }else{
                                runrecord.campaign_members_created__c = runrecord.campaign_members_created__c + 1;
                            }
                            contactIdSet.add(newCampmem.contactid);     
                        }
                    }
                    campaignMemberNotes ='';
                    
                    
                    //*******
                    //Have now created the Corporate campaign member.
                    
                    //Now create a task and assign it to the account manager unless otherwise specified.
                    
                    //How are we doing to assign the task to ? Account Manager or EIN or Lead Locator ?
                    
                    //Has the EIN been provided ?
                    
                    //Found out if this account had the lead locator provided and if so what is it ?
                    
                    Boolean bIsLeadlocator = false;
                    String leadlocatoruser = '';
                    for(accountteammember thismember:accteammember){
                        system.debug('Processing next member');
                        if(thismember.accountid == accountrec.id && thismember.teammemberrole == leadlocatormap.get(accountrec.sac_code__c)){
                            system.debug('Got lead locator');
                            bIsLeadLocator =true;
                            leadlocatoruser = thismember.userid;
                        }
                    }
                    
                    
                    if(stagingmap.get(accountrec.sac_code__c).ein__c !=null){
                        //Use the EIN in the input file to assign 
                        assigntome = einusermap.get(stagingmap.get(accountrec.sac_code__c).ein__c).id;
                    }else if(bIsLeadLocator){
                        assigntome = leadlocatoruser;
                    }else if(accountteammap.get(accountrec.id) !=null){
                        if(accountteammap.get(accountrec.id).Account_Owner_Primary__c !=null){
                        //Assign it to the Account Manager if it is specified in the Account Team contacts.
                            assigntome = accountteammap.get(accountrec.id).Account_Owner_Primary__c;
                        }else{
                        //Assign to the account record owner
                            assigntome = accountrec.ownerid;
                        }       
                    }else{
                        //Assign to the account record owner
                        assigntome = accountrec.ownerid;
                    } 
                    
                    //Create the task and populate with fields from the input file if present.
                    corpSystemUrl = stagingmap.get(accountrec.sac_code__c).Corp_System_Url__c;
                    corpSystemText =stagingmap.get(accountrec.sac_code__c).Corp_System_Text__c;
                    corpPrimaryCampaign = stagingmap.get(accountrec.sac_code__c).Corp_Primary_Campaign_Source__c;
                    corpDetails=stagingmap.get(accountrec.sac_code__c).Corp_Details__c;
                    corpCollatUrl =stagingmap.get(accountrec.sac_code__c).Corp_Collateral_Url__c;
                    corpCollaText = stagingmap.get(accountrec.sac_code__c).Corp_Collateral_Text__c;
                    corpSubject = stagingmap.get(accountrec.sac_code__c).Corp_Subject__c;
                    if(stagingmap.get(accountrec.sac_code__c).Corp_Reminder_Days__c!=null){
                        corpReminder = stagingmap.get(accountrec.sac_code__c).Corp_Reminder_Days__c.intvalue();
                    }
                    corpDueDate = stagingmap.get(accountrec.sac_code__c).Corp_Due_Date__c;
                    if(corpReminder >0){
                        bIsReminderSet =true;
                    }
                    
                    if(corpDueDate ==null){
                        corpDueDate = System.today();
                    } 
                    if(corpSubject==''){ 
                        corpSubject = 'Campaign Task: For '+parentcampaign.name;
                    }
                    system.debug('outputting due date' + corpDueDate);
                    
                    Task task1 = new Task(Subject=corpSubject,ActivityDate=corpDueDate,status = 'Not Started',Priority='Normal',whoid=newCampMem.contactid,whatid=parentcampaign.id,ownerid = assigntome,recordtypeid=StaticVariables.corp_campaign_task_record_type,CampaignDefaultOwner__c=accountrec.ownerid,system_url__c=corpSystemUrl,system_text__c=corpSystemText,details__c=corpDetails,collateral_url__c = corpCollatUrl,collateral_text__c=corpCollaText,primary_campaign_source__c =corpPrimaryCampaign,reminderdatetime=system.today()+corpReminder,isReminderSet=bIsReminderSet);
                    system.debug('gsassigntome' + assigntome);
                    
                    newtasklist.add(task1);
                    
                    //Set the task counter
                    If (runrecord.Tasks_Created__c == null){
                        runrecord.Tasks_Created__c = 1;
                    }else{
                        runrecord.Tasks_Created__c = runrecord.Tasks_Created__c + 1;
                    }
                //***************
            }
        }   
            
        
        //Insert the corporate tasks
        //Need to query all of the owner records and see if any are inactive. If they are revert to the default owner (account owner) as the assignee of the task.AccountId
        Set<id> ownerIdSet = new Set<id>();
        for(task thistask:newtasklist){
            ownerIdSet.add(thistask.ownerid);   
            ownerIdSet.add(thistask.CampaignDefaultOwner__c);
        }
        
        List<user> userlist = new List<user>([select id,isactive from user where id in :ownerIdSet]);
        Set<id> inActiveUsers = new Set<id>();
        
        for(user thisuser:userlist){
            if(thisuser.isactive ==false){
                system.debug('GPSfoundinactive'+thisuser.id);
                inActiveUsers.add(thisuser.id);
            }
        }
        
        List<task> finaltasklist = new list<task>();
        Integer nCount =0;
        Set<integer> remSet = new Set<integer>();
        
        for(task thistask:newtasklist){
            
            //Test if the current owner is active
            if(inActiveUsers.contains(thistask.ownerid)){
                if(!inActiveUsers.contains(thistask.CampaignDefaultOwner__c)){
                    thistask.ownerid = thistask.CampaignDefaultOwner__c;
                    //This task is now good. Add to the list
                    finaltasklist.add(thistask);
                    
                    system.debug('GSChangedOwnerTo:'+thistask.CampaignDefaultOwner__c + ' phoning :'+thistask.whoid);   
                }else{
                    //Both owners are inactive - report as an error
                    system.debug('acknowledged error');
                    //Report it as an error
                    //Set the task counter
                    runrecord.Tasks_Created__c = runrecord.Tasks_Created__c -1;
                    runrecord.Total_Errors_In_Batch__c=runrecord.Total_Errors_In_Batch__c+1;
                }
            }else{
            //The current owner is good. Add them to the new list   
                finaltasklist.add(thistask);
            }   
        }
        
        
        //Write the campaign members to the db
        system.debug('dumpingcampmembers' + campmemberlist.size());
        try{
        database.insert(campmemberlist,false);
        }catch(exception ex){
        system.debug('Exception :'+ex);    
        }
        system.debug('fulltasklist' + finaltasklist);
        
        Database.SaveResult[] srl = Database.insert(finaltasklist);
        // Iterate through the Save Results
        for(Database.SaveResult sr:srl){
            if(!sr.isSuccess()){
                Database.Error err = sr.getErrors()[0];
                system.debug('ERR:' + err);
            }
        }
         
        //Delete the staging records if the user specified
        if(runrecord.Delete_source_data_after_load__c==true){
            delete stagelist;
        }
        
        //Update the runrecord
        system.debug('updatingrunrecord');
        update runrecord;
        return runrecord;
        
        
    }
}