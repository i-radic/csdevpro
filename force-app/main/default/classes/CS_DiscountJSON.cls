public class CS_DiscountJSON{
    
    public List<Discount> discounts { get; set; }
    
    
    public class Discount
    {
        public List<MemberDiscount> memberDiscounts { get; set; }
        public string evaluationOrder { get; set; }
        public string recordType { get; set; }
        public string version { get; set; }
        
        
        
    }
    public class MemberDiscount
    {
        public string chargeType { get; set; }
        public string recordType { get; set; }
        public string type { get; set; }
        public string source { get; set; }
        public string discountCharge { get; set; }
        public string description { get; set; }
        public string amount { get; set; }
        public string version { get; set; }
    }
    
}