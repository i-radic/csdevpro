/******************************************************************************************************
Name : BTOnePhoneHelper 
Description : Helper class of BT_One_Phone__c Trigger

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    01/02/2016                 Praveen	                             Class created         
*********************************************************************************************************/

public class BTOnePhoneHelper{
    
    Public static void updateBTOnePhoneDetailsBeforeUpdate(map<Id,BT_One_Phone__c> newBTOnePhoneMap ,map<Id,BT_One_Phone__c> oldBTOnePhoneMap){
        for (Id BTOPId: newBTOnePhoneMap.KeySet()){            
            if((newBTOnePhoneMap.get(BTOPId).Status__c != oldBTOnePhoneMap.get(BTOPId).Status__c) && newBTOnePhoneMap.get(BTOPId).Status__c == 'Completed order sent to BTOP')
                newBTOnePhoneMap.get(BTOPId).Sent_To_BTOP_On__c = DateTime.now().date();                
            if((newBTOnePhoneMap.get(BTOPId).Status__c != oldBTOnePhoneMap.get(BTOPId).Status__c) && newBTOnePhoneMap.get(BTOPId).Status__c == 'Order Rejected')
                newBTOnePhoneMap.get(BTOPId).Order_Rejected_On__c = DateTime.now();
            if((newBTOnePhoneMap.get(BTOPId).BTOP_PM_Hand_Over_Discussion_Call__c != oldBTOnePhoneMap.get(BTOPId).BTOP_PM_Hand_Over_Discussion_Call__c) && newBTOnePhoneMap.get(BTOPId).BTOP_PM_Hand_Over_Discussion_Call__c == True)
                newBTOnePhoneMap.get(BTOPId).BTOP_acceptance_date__c = DateTime.now().date();
            if((newBTOnePhoneMap.get(BTOPId).Status__c != oldBTOnePhoneMap.get(BTOPId).Status__c) && newBTOnePhoneMap.get(BTOPId).Status__c == 'Site evaluation / survey requested')
                newBTOnePhoneMap.get(BTOPId).BTOP_Site_Survey_Requsted_Date__c = DateTime.now(); 
            //CR10933
            if((newBTOnePhoneMap.get(BTOPId).Status__c != oldBTOnePhoneMap.get(BTOPId).Status__c) && newBTOnePhoneMap.get(BTOPId).Status__c == 'Sales handover call completed')
                newBTOnePhoneMap.get(BTOPId).Sales_handover_call_completed_Date__c = DateTime.now().date(); 
        }
    }
     // commented as part of CR9917 -start
    /*public static void updateContractDurationonBTOnePhoneSites(map<Id,BT_One_Phone__c> newBTOnePhoneMap ,map<Id,BT_One_Phone__c> oldBTOnePhoneMap){
        
        List<BT_One_Phone_Site__c> lstBTOnePhoneSiteUpdate = new List<BT_One_Phone_Site__c>();
        Set<Id> btOnePhoneUpdateIds = new Set<Id>();
        
        for (Id btOnePhoneId: newBTOnePhoneMap.keySet()){
            if ((newBTOnePhoneMap.get(btOnePhoneId).Contract_Duration__c != oldBTOnePhoneMap.get(btOnePhoneId).Contract_Duration__c)) {
                btOnePhoneUpdateIds.add(btOnePhoneId);
            }                
        }
        // Fetch the corresponding BT one phone sites
        for(BT_One_Phone_Site__c btonephonesite : [SELECT id, name, Contract_Duration__c,BT_One_Phone__r.id
                                                   FROM BT_One_Phone_Site__c WHERE BT_One_Phone__r.Id In : btOnePhoneUpdateIds]){
             btonephonesite.Contract_Duration__c = newBTOnePhoneMap.get(btonephonesite.BT_One_Phone__r.id).Contract_Duration__c;
             lstBTOnePhoneSiteUpdate.add(btonephonesite);                                          
        }        
        if (lstBTOnePhoneSiteUpdate.size() > 0 ){
            update lstBTOnePhoneSiteUpdate; 
        }
    } */  
     // commented as part of CR9917 -end
}