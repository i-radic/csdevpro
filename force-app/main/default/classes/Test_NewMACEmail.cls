@isTest
private class Test_NewMACEmail{
    
    static testMethod void myUnitTest(){
    
    Id UId = UserInfo.getUserId();
    String uName=[Select Name from User where Id=:UId].Name;
    
    
    
    /*OrgWideEmailAddress owd= new OrgWideEmailAddress ();
    owd.Address ='noreply.btbusiness@bt.com';
    owd.DisplayName='BT Business No Reply';
    insert owd;*/
    Id fromaddr=[select Id,Address from OrgWideEmailAddress where DisplayName=:'BT Business No Reply'].Id; 
    
    Account newAccount = new Account (name='XYZ Organization');
    insert newAccount;

    Contact myContact = new Contact (FirstName='Joe',LastName='Schmoe',AccountId=newAccount.id,Email='test.mail@test.com');
    insert myContact;
    
    CustomerOptions__c cot=new CustomerOptions__c(Account__c=newAccount.Id, Telephone_Number__c='0123456789', Account_Number__c='AB12345678', Contact__c=myContact.Id);
    insert cot;
    
    BTLB_Customer_Option_Product__c cop=new BTLB_Customer_Option_Product__c(Order_Type__c='mac',BTLB_Customer_Option_record__c=cot.Id, Mac_code__c='ABCD12345678/EF12G',Telephone_Number__c='0123456789',Quantity__c=1);
    insert cop;
    
    PageReference pageRef = Page.New_MAC_Transfer_Page2;
    Test.setCurrentPage(pageRef);
    ApexPages.currentPage().getParameters().put('cotid',cot.Id);
    
    ApexPages.StandardController stc = new ApexPages.StandardController(cot);
    
    NewMACEmail MacCtrl = new NewMACEmail(stc);

    
    MacCtrl.sendEmail();
    MacCtrl.custContact.Email = 'test@test.com';
    MacCtrl.sendEmail();
    MacCtrl.custContact.Email = 'test';
    MacCtrl.sendEmail();
    MacCtrl.custContact.Email = '';
    MacCtrl.sendEmail();
    //MacCtrl.getcallnew();
    
    
   
    //MacCtrl.showTemplate();
    
    //MacCtrl.actionDo();
    //MacCtrl.getactionBody();
    
    }
}