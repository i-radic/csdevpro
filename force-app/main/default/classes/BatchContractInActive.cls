global class BatchContractInActive implements Database.Batchable<SObject>{
     private String query;
    global BatchContractInActive(String q){
        this.query = q;
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Contract> contracts = new List<Contract>();
        for(Sobject c : scope){
            Contract contract = (Contract)c;
            contract.Adder_Status__c ='Inactive';
            contracts.add(contract);
        } 
        update contracts;
    }
 global void finish(Database.BatchableContext BC){
    }
}