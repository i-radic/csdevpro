public class CS_CloudVoice_SpeedTestEmailCtrl {

	public Contact contactObj { get; set; }
	public String infoMessage { get; set; }
	public Boolean displaySendEmailSection { get; set; }
	public String configGuid { get; set; }
	public class applicationException extends Exception {}
	
	public CS_CloudVoice_SpeedTestEmailCtrl() { 
		String basketId = System.currentPageReference().getParameters().get('basketId');
		configGuid = System.currentPageReference().getParameters().get('configGuid');
		cscfga__Product_Basket__c basket = [select id, Customer_Contact__c from cscfga__Product_Basket__c where id = :basketId];

		try {
			contactObj = [select Id, Name, FirstName, LastName, Email from Contact where Id = :basket.Customer_Contact__c];
		}
		catch(Exception e) {
			throw new applicationException('Customer_Contact__c field on the basket is missing. Please fill it in before trying again');
		}

		displaySendEmailSection = true; 	
	}

	public PageReference sendEmail() {
		String accessType = System.currentPageReference().getParameters().get('accessType');
		String sendEmailAction = accessType; // 'Existing' OR 'OTT'
		String sendEmailAddress = contactObj.Email;
		String sendEmailName = contactObj.FirstName;
		String emlSubject = '';
		String emlPlainTextBody = '';
		String emlHtmlBody = '';
		String emlFrom = '';
		if (sendEmailAction == 'Existing') {
			emlSubject = 'BT Cloud Voice Line Speed Test';
			emlPlainTextBody = 'THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY\n\n'
			+ 'Dear ' + sendEmailName + '\n'
			+ 'Please navigate to the link below to run the BT Wholesale Broadband Performance Test.\n'
			+ 'http://www.speedtest.btwholesale.com\n'
			+ 'When complete let the BT Sales Person know the results.\n\n'
			+ 'Regards\n'
			+ 'BT Cloud Voice Team';
			emlHtmlBody = '<span style="font-family:Arial;font-size:13px">'
			+ '<span style="font-weight:bold;color:red;">THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY<br/><br/></span>'
			+ 'Dear ' + sendEmailName + '<br/>'
			+ 'Please navigate to the link below to run the BT Wholesale Broadband Performance Test.<br/>'
			+ '<a href="http://www.speedtest.btwholesale.com">Speed Test Site</a><br/>'
			+ 'When complete let the BT Sales Person know the results.<br/><br/>'
			+ 'Regards<br/>'
			+ 'BT Cloud Voice Team'
			+ '</span>';
		}
		else if (sendEmailAction == 'OTT') {
			emlSubject = 'Please run a test on your internet connection';
			emlPlainTextBody = 'THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY\n\n'
			+ 'Dear ' + sendEmailName + '\n'
			+ 'As we do not supply your internet connection, we need your help to check that it meets the minimum requirements for BT Cloud Voice.\n\n'
			+ 'Go to our network assesment tool [www.bt.com/btcloudvoice/networkassessment] and follow the instructions. It only takes a few minutes.\n\n'
			+ 'Make sure you run this test while on the connection that you plan to use with BT Cloud Voice at a time when you would typically use it. Please make a note of the test session ID as we will need it to check your connection. You will need to run the connection test and record the test session ID for each site where you are planning to use BT Cloud Voice.\n\n'
			+ 'Once we have got the results, we will let you know if we have got any concerns with your internet connection for running BT Cloud Voice.\nn'
			+ 'Thanks,\n\n'
			+ 'Your BT Team\n';
			emlHtmlBody = '<span style="font-family:Arial;font-size:13px">'
			+ '<span style="font-weight:bold;color:red;">THIS HAS BEEN SENT FROM AN UNMONITORED EMAIL ACCOUNT. PLEASE DO NOT REPLY<br/><br/></span>'
			+ 'Dear ' + sendEmailName + '<br/>'
			+ 'As we do not supply your internet connection, we need your help to check that it meets the minimum requirements for BT Cloud Voice.<br/><br/>'
			+ 'Go to our <a href="http://www.bt.com/btcloudvoice/networkassessment">network assesment tool</a> and follow the instructions. It only takes a few minutes.<br/><br/>'
			+ 'Make sure you run this test while on the connection that you plan to use with BT Cloud Voice at a time when you would typically use it. Please make a note of the test session ID as we will need it to check your connection. You will need to run the connection test and record the test session ID for each site where you are planning to use BT Cloud Voice.<br/><br/>'
			+ 'Once we have got the results, we will let you know if we have got any concerns with your internet connection for running BT Cloud Voice.<br/><br/>'
			+ 'Thanks,<br/><br/>'
			+ 'Your BT Team<br/>';
		}
		else {
			infoMessage = 'Access Type must be Existing or OTT!';
			displaySendEmailSection = false;
			return null;
		}

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] { sendEmailAddress };

		OrgWideEmailAddress[] owea;

		try {
			owea = [select Id from OrgWideEmailAddress where Address = 'noreply.btbusiness@bt.com'];

			mail.setOrgWideEmailAddressId(owea.get(0).Id);
			mail.setToAddresses(toAddresses);
			mail.setUseSignature(false);
			mail.setSubject(emlSubject);
			mail.setPlainTextBody(emlPlainTextBody);
			mail.setHtmlBody(emlHtmlBody);
			try {
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
				infoMessage = 'Email Sent!';
			}catch(Exception e) {
				infoMessage = e.getMessage();
			}
		}
		catch(Exception e) {
			throw new applicationException('noreply.btbusiness@bt.com not found is salesforce!!!');
		}
		
		displaySendEmailSection = false;
		return null;
	}
}