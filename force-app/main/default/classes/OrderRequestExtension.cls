public without sharing class OrderRequestExtension {

    private final string CONST_ERROR_ID_INIT_ORDER_REQUEST_MSG 		= 'No Order Request Id was detected, please make sure you reached this page using the \'Send Email\' button on the Order Request page.';
    private final string CONST_ERROR_ID_INIT_ORDER_ITEM_MSG 		= 'No Order Item Id was detected, please contact Sales Enablers';
    private final string CONST_ERROR_ID_INIT_ORDER_MODE_MSG 		= 'No Mode querystring was detected, please contact Sales Enablers';
    
    private final string CONST_ERROR_NO_LOCATIONS_MSG 	= 'No Location Details Specified';
    private final string CONST_ERROR_WELCOME_DAY_MISSING_LOCATIONS_MSG = 'Not all Welcome Days have Location Details Specified';
    private final string CONST_ERROR_MISSING_ATTACHMENTS_MSG 		= 'No Attachments have been found';
    private final string CONST_ERROR_STATUS_INVALID_SEND_MSG 		= 'You can only send the email if the Status is at Draft';
    private final string CONST_ERROR_STATUS_INVALID_CHANGE_MSG		= 'You cannot change the Status when it is at Status: ';

    private final string CONST_ERROR_STATUS_INVALID_SUBMIT_MSG 		= 'You can only Submit the Order Request if the Status is at Draft';

    // Check that the Proforma has been generated
	// Case 00098105 PGoodey May 2016
    private final string CONST_ERROR_MISSING_PROFORMA_MSG 			= 'Missing Proforma : Please return to the Order Request and click the \'Generate Document\' button';
    private final string CONST_PICKLIST_YES							= 'Yes';
    
	private final string CONST_STATUS_SENT_TO_SUPPLIER 	= 'Sent to Supplier';
	private final string CONST_STATUS_COMPLETE 			= 'Complete';
	private final string CONST_STATUS_CANCELLED 		= 'Cancelled';
	
	private final string CONST_STATUS_DRAFT 		= 'Draft';

	private final string CONST_MODE_QUERYSTRING				= 'mode';
	private final string CONST_MODE_SEND_ORDER_REQUEST 		= 'sendrequest';
	private final string CONST_MODE_CANCEL_ORDER_REQUEST 	= 'cancelrequest';

	private final string CONST_MODE_SUBMIT_ORDER_REQUEST 	= 'submitrequest';

    public Order_Request__c oreq { get; set;}
    public OrderItem oitm { get; set;}
    private List<AttachmentWrapper> attWrappers = new List<AttachmentWrapper>();
    private List<Attachment> selectedAttachments = new List<Attachment>();

	// Querystring Parameters 
    public Map<string,string> mMapQsParams {get;set;}
    public Map<string,string> savedparams {get;set;}

	// Mode Property
    public String mode { get; set;}
    
    // This sets the vf page to show the save button
    public Boolean saveStatusEnabled { get; set;}
    public Boolean showEmailSection { get; set;}
    
        
	// Email Properties
    public String toAddr { get; set;}
    public String ccAddr { get; set;}
    public String subject { get; set;}
    public String body { get; set;}

	// Visualforce properties
    public Boolean sendEnabled { get; set;}
    public Integer step { get; set;}
    public Boolean sendSuccess { get; set;}
    public Boolean sendIncomplete { get; set;}
    public Boolean submitInvalid { get; set;}
    public Boolean submitWarning { get; set;}
        
    public OrderRequestExtension (ApexPages.StandardController stdController){
    	
    	// Initialise
    	step = 1;
    	sendEnabled = false;
        sendSuccess = false;
        sendIncomplete = false;
        saveStatusEnabled = false;
        showEmailSection = false;

        // Get the Order Request details from the passed standard Controller Order Request Id
        oreq = (Order_Request__c)stdController.getRecord();

		// retrieve and set the Primary object records
        setPrimaryRecords();
        
        // Check the mode and process accordingly
        if( mode == CONST_MODE_SEND_ORDER_REQUEST ){

	        // Check the Status
	        if(oreq.Status__c == CONST_STATUS_DRAFT){
	        
		        // initialise all variables and settings
		        init();
		        
	        }else{
	        	// Only send the email if the status is at Draft
	            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_STATUS_INVALID_SEND_MSG ));
	            sendIncomplete = true;
	        }
                	
        }else if( mode == CONST_MODE_CANCEL_ORDER_REQUEST ){
        	
			// Check the Current Status
	        //if( (oreq.Status__c != CONST_STATUS_COMPLETE) && (oreq.Status__c != CONST_STATUS_CANCELLED) ) {
	        // Check the Status
	        if(oreq.Status__c == CONST_STATUS_SENT_TO_SUPPLIER){	        	
	        	// initialise all variables and settings
	        	init_cancel();
	        	//saveStatusEnabled = true;
	        	
	        }else{
	        
	        	// Only update the status if the status is not already Complete or Cancelled
	            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_STATUS_INVALID_CHANGE_MSG + oreq.Status__c ));
	            
	        }
        	
        }
        else if( mode == CONST_MODE_SUBMIT_ORDER_REQUEST ){
        	
        	// Submit loads a new visualforce form which uses validation rules to control the logic

/*
	        // Check the Status
	        if(oreq.Status__c != 'Draft'){
	        
	        	// Only send the email if the status is at Draft
	            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_STATUS_INVALID_SUBMIT_MSG ));
	            submitInvalid = true;
	        }else{
	        	
	        	// Check whether the Hotstaging form has been attached and warn the user
	        	if(oreq.Hotstaging_Completed_Form_Attached__c != 'Yes'){
	        		submitWarning = true;
	        	}
	        }
*/	        
	        
        }
    }
    

   /*
    * @name         getSelectAttachments
    * @description  Method to return the selection of attachments
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */    
    public List<Attachment> getSelectAttachments(){
        return selectedAttachments;
    }


   /*
    * @name         getSelected
    * @description  Method to handle the selection of attachments
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    public PageReference getSelected() {
        selectedAttachments.clear();
        for(AttachmentWrapper attWrapper: attWrappers) {
            if(attWrapper.selected == true) {
                //attWrapper.isChanged = true;
                selectedAttachments.add(attWrapper.att);  
            }                    
        }      
        return null;
    }

   /*
    * @name         AttachmentWrapper
    * @description  Attachment Wrapper Class to aid the selection of attachments
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    public class AttachmentWrapper {
        public Attachment att{get; set;}
        public Boolean selected{get; set;}
        public AttachmentWrapper(Attachment a) {
            att = a;
            // Automatically set all attchments as selected
            selected = true;
        }
    }
    
    
   /*
    * @name         setPrimaryRecords
    * @description  sets the details for the Order Request and Order Item primary records
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */    
	public void setPrimaryRecords() {

		// Get the Querystring Parameters
        mMapQsParams = ApexPages.currentPage().getParameters();

		// Check that there is an id in the parameters
        if(mMapQsParams.containsKey('id')){
            String lsOrId = mMapQsParams.get('id');
            /*
            oreq = [SELECT Id, Name, Status__c, System_Order_Item__c, Order__c, RecordTypeId, Welcome_Day_Quantity__c, Order_Quantity__c, Hotstaging_Completed_Form_Attached__c, Submit_Action__c 
            		FROM Order_Request__c
            		WHERE Id = :lsOrId LIMIT 1];

            oreq = [SELECT Id, Name, Status__c, System_Order_Item__c, Order__c, RecordTypeId, Order_Quantity__c, Hotstaging_Completed_Form_Attached__c, Submit_Action__c 
            		FROM Order_Request__c
            		WHERE Id = :lsOrId LIMIT 1];            
            
			*/

			/*
			oreq = [SELECT Id, Name, Status__c, System_Order_Item__c, Order__c, RecordTypeId, Booked_Staff_Quantity__c, Order_Quantity__c, Hotstaging_Completed_Form_Attached__c, Submit_Action__c 
            		FROM Order_Request__c
            		WHERE Id = :lsOrId LIMIT 1];
			*/            		          		          		          

			// Case 00092417 Dec 2015 - PG Include Company Name
			oreq = [SELECT Id, Name, Status__c, System_Order_Item__c, Order__c, RecordTypeId, Booked_Staff_Quantity__c, Order_Quantity__c, 
						Hotstaging_Completed_Form_Attached__c, Submit_Action__c, Company__r.Name, Proforma_Attached__c
            		FROM Order_Request__c
            		WHERE Id = :lsOrId LIMIT 1];            		          		          

            		
	       	// Retrieve the associated Order Item for the Order Request
	    	List<OrderItem> lListOrderItem = new List<OrderItem>();
	    	lListOrderItem = [SELECT Id, Status__c, PriceBookEntry.Product2.Name, OrderId FROM OrderItem WHERE Id = :oreq.System_Order_Item__c LIMIT 1];
	        
	        // Set the Order Item for the Order Request and raise an error if there is no Order Item
	        if( !lListOrderItem.isEmpty() ){
	
				// Assign the Order Item
				oitm = lListOrderItem[0];
				
				// Retrieve the Mode - this is either Send the Order Request email or Cancel the Order Request
				String lsMode = mMapQsParams.get(CONST_MODE_QUERYSTRING);
				
				// Check the mode
				//if( (lsMode == CONST_MODE_SEND_ORDER_REQUEST) || (lsMode == CONST_MODE_CANCEL_ORDER_REQUEST) ){
				if( (lsMode != null) || (lsMode == '') ){
					// Set the mode
					mode = lsMode;
				}else{
		        	// No Mode has been retrieved
		        	ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_ID_INIT_ORDER_MODE_MSG ));
		        	sendIncomplete = true;					
				}
	        }else{
	        	// No Order Item has been retrieved
	        	ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_ID_INIT_ORDER_ITEM_MSG ));
	        }            		
        } else {
			// No Order Request Id has been passed
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_ID_INIT_ORDER_REQUEST_MSG ));
        }        	
	}


   /*
    * @name         init
    * @description  initialises and validates the data required for the email sending
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */ 
    public void init() {
    	
        // Set the Subject for the Send Email
        /*
        subject = 'EE' + ' ' + oreq.Name + ' ' + getSubjectEmailSend();
        */
        // Case 00092417 Dec 2015 - PG Include Company Name
        subject = 'EE' + ' ' + oreq.Name + ' ' + getSubjectEmailSend() + ' for ' + oreq.Company__r.Name;
        
        // Set the Body text for the Send Email
        /* 
        body = getBodyEmailSend();
        */
        // Case 00092417 Dec 2015 - PG Include Company Name
        body = getBodyEmailSend() + ' for ' + oreq.Company__r.Name;
        
        
        
        // Get the User Id
        Id userId = UserInfo.getUserId();
        User currentUser = [ SELECT Email FROM User WHERE Id = :userId];
        ccAddr = currentUser.Email;
        //step = 1;
        
        
        Double ldOrderRequestLocationCount = (double)oreq.get('Booked_Staff_Quantity__c');
		// Validate Welcome Days Status and State
		//if( getRecordTypeName() == 'Welcome Day' ){
		// This needs to be dynamic
		// Welcome Days, Device Training Days need to have locations
		// But Billing Reports etc do not
		if( getIsLocationRequired(oitm.PriceBookEntry.Product2.Name) == true ){

			// Check that the rolled up number of Order Request Locations is not zero		 	
			if( ldOrderRequestLocationCount == 0 ){
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_NO_LOCATIONS_MSG ));
				sendIncomplete = true;
			}else if( ldOrderRequestLocationCount != oreq.Order_Quantity__c ){
				ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_WELCOME_DAY_MISSING_LOCATIONS_MSG ));
				sendIncomplete = true;				
			}
		}
		
		// Continue and Validate Attachments
		//if( sendIncomplete == false ){
		if( getIsAttachmentRequired(oitm.PriceBookEntry.Product2.Name) == true ){
			// set up the attachments that are to be sent 
	        for(Attachment att: [SELECT Id, Name, CreatedDate, ContentType, Body FROM Attachment WHERE parentid =: oreq.Id ORDER BY createdDate DESC]){
	            attWrappers.add(new AttachmentWrapper(att));
	        }
	        // Check that there are attachments to send
	        if( attWrappers.isEmpty() ){
	        	ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_MISSING_ATTACHMENTS_MSG ));
	        	sendIncomplete = true;
	        }
	        
	        // Check that the Proforma has been generated
	        // Case 00098105 PGoodey May 2016
	        if( (getIsProformaRequired(oitm.PriceBookEntry.Product2.Name) == true) && (oreq.Proforma_Attached__c != CONST_PICKLIST_YES) ){
	        	ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.ERROR, CONST_ERROR_MISSING_PROFORMA_MSG ));
	        	sendIncomplete = true;	        	
	        } 
	        			
		}

		// Continue and set the Supplier Email
		if( sendIncomplete == false ){
            toAddr  = getSupplierEmail();
        }
    }


   /*
    * @name         getIsLocationRequired
    * @description  Gets the Email Location Requirement for the Order Request based on the Order Item Product
    * @param		Product Name    
    * @author       P Goodey
    * @date         Jan 2015
    * @see			WR
    */
    private Boolean getIsLocationRequired(string psProductName){
		Boolean lbIsRequired = false;
 		Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            if (orRecordType.Product_Name__c == psProductName) {
            	lbIsRequired = orRecordType.Supplier_Email_Is_Location_Required__c;
            	break;
            }
        }
		return lbIsRequired;
	}

   /*
    * @name         getIsAttachmentRequired
    * @description  Gets the Email Attachment Requirement for the Order Request based on the Order Item Product
    * @param		Product Name    
    * @author       P Goodey
    * @date         Jan 2015
    * @see			WR
    */
    private Boolean getIsAttachmentRequired(string psProductName){
		Boolean lbIsRequired = false;
 		Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            if (orRecordType.Product_Name__c == psProductName) {
            	lbIsRequired = orRecordType.Supplier_Email_Is_Attachment_Required__c;
            	break;
            }
        }
		return lbIsRequired;
	}

   /*
    * @name         getIsActivityRequired
    * @description  Gets the Email Activity History Requirement for the Order Request based on the Order Item Product
    * @param		Product Name    
    * @author       P Goodey
    * @date         Jan 2015
    * @see			WR
    */
    private Boolean getIsActivityRequired(string psProductName){
		Boolean lbIsRequired = false;
 		Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            if (orRecordType.Product_Name__c == psProductName) {
            	lbIsRequired = orRecordType.Supplier_Email_Is_Activity_Required__c;
            	break;
            }
        }
		return lbIsRequired;
	}


   /*
    * @name         getIsProformaRequired
    * @description  Gets the Proforma Attachment Requirement for the Order Request based on the Order Item Product
    * @param		Product Name    
    * @author       P Goodey
    * @date         May 2016
    * @see			Case 00098105
    */
    private Boolean getIsProformaRequired(string psProductName){
		Boolean lbIsRequired = false;
 		Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            if (orRecordType.Product_Name__c == psProductName) {
            	lbIsRequired = orRecordType.Supplier_Email_Is_Proforma_Required__c;
            	break;
            }
        }
		return lbIsRequired;
	}
	
	    

   /*
    * @name         init_cancel
    * @description  initialises and validates the data required for the email sending cancellation
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */ 
    public void init_cancel() {
    	
    	saveStatusEnabled = true;
    	
        // Set the Subject for the Cancel Email
        //subject = getSubjectEmailCancel();
        subject = 'EE' + ' ' + oreq.Name + ' ' + getSubjectEmailCancel();
        
        // Set the Body text for the Cancel Email
        body = getBodyEmailCancel();
        
        // Get the User Id
        Id userId = UserInfo.getUserId();
        User currentUser = [ SELECT Email FROM User WHERE Id = :userId];
        ccAddr = currentUser.Email;

        // Continue and set the Supplier Email
		if( sendIncomplete == false ){
            toAddr  = getSupplierEmail();
        }
    }
    

    public PageReference toggleStatusEmail() {
    	if( oreq.Status__c == 'Cancelled' ){
    		showEmailSection = true;
    	}else{
    		showEmailSection = false;
    	}
		return null;
    }
    
   /*
    * @name         getAttachments
    * @description  initialises and validates the data required for the email sending
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */ 
    public List<Attachment> getAttachments(){
        
        List<Attachment> allAttachment = [SELECT Id, Name, ContentType, Body FROM Attachment WHERE parentid =: oreq.Id];
        return allAttachment;
    }    

    public List<AttachmentWrapper> getAttachmentWrappers() {
        return attWrappers;
    }
    
    
    public Integer getSelectedAttachmentCount(){
        return(selectedAttachments.size());
    }
    

    public PageReference nextStep(){
        step++;
        if(step==2){
            getSelected();
        }
        return null;   
    }

    public PageReference backStep(){
        
        ApexPages.getMessages().clear();
        step--;
        return null;   
    }



   /*
    * @name         onSubmit
    * @description  This is called from the vf page when a user presses the "Submit" button (used to proces the Order Request)
    * @author       P Goodey
    * @date         Aug 2014
    * @see 
    */ 
    public PageReference onSubmit() {

		PageReference pageRef = null;
		try{

			// Update the Status for the Order Item - this will cause to the trigger to update the order request & order level records
			//setOrderItemStatus(oreq.Status__c);
			//oreq.Date_Order_Request_Submitted__c = system.today();
			
			// Using a Submit Action on the vf page OrderRequestSubmit to make the update more generic
			update oreq;
    		pageRef = new PageReference('/' + oreq.Id);	
/*
		}catch(DMLException e){
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getDmlMessage(0)));        					
		}
*/			
        } catch(Exception e) {
        	if(e.getTypeName()=='System.DmlException'){
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,e.getDmlMessage(0)));        		
        	}else{
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR  ,e.getTypeName() + ' Saving record failed.' +e.getMessage()));
        	}  
        } 
        return pageRef;
    }
    
    

   /*
    * @name         onSave
    * @description  This is called from the vf page when a user presses the "Save" button (used to cancel or complete)
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */ 
    public PageReference onSave() {

		PageReference pageRef = null;
        
		try{

	        if( oreq.Status__c == CONST_STATUS_CANCELLED ){

				Messaging.SingleEmailMessage theMessage = new Messaging.SingleEmailMessage();
		        //theMessage.setWhatId(oreq.Id);

				// Check if ok to proceed
				if( toAddr != null ){
	
		            // Set the to address(es)
		            List<String> toAddresses = new List<String>();
		            if(toAddr.trim().length() > 0 ){
		            	
		            	// Check if there are multiple address
		            	if(toAddr.contains(';')){
		                	toAddresses = toAddr.split(';');	            		
		            	}else{
		            		toAddresses.add(toAddr);         
		            	}
		            	theMessage.setToAddresses(toAddresses);	 
		            }
		            
		            //List<String> toAddresses = new List<String>();
		            //toAddresses.add(toAddr);
		            
		            theMessage.setToAddresses(toAddresses);
		            if(ccAddr.trim().length() > 0 ){
		                //String[] ccAddresses = ccAddr.split(',', 0);
		                String[] ccAddresses = ccAddr.split(',');
		                theMessage.setCcAddresses(ccAddresses);
		            }
		            	            	            
		            theMessage.setSubject(subject);
		            theMessage.setHtmlBody(body);
		            
		            try{
		            	
			            // Sends the email
			            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {theMessage}); 
			            if(r[0].isSuccess()){
			                sendSuccess = true;
			                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO, 'Message Sent Successfully!'));
			                
							// Update the Status for the Order Item - this will cause to the trigger to update the order request & order level records
							setOrderItemStatus(oreq.Status__c);
				    		//pageRef = new PageReference('/' + oreq.Id);						
			            }else{
			            	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR  ,'Not Successfully Sent'));
			            }
			        } catch(emailException e) {         
			        	ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR  ,'Send an email failed.' +e.getMessage()));
			        } 			
				}else{
					ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR,'No Supplier email found'));
				}
				
	        }else{

				// Update the Status for the Order Item - this will cause to the trigger to update the order request & order level records
				setOrderItemStatus(oreq.Status__c);
				
				// Add an Email Activity History record
				if(getIsActivityRequired(oitm.PriceBookEntry.Product2.Name) == true){
					insertEmailActivityHistory(oreq.Status__c);
				}
				
	    		pageRef = new PageReference('/' + oreq.Id);	
	        	
	        }
				
        } catch(Exception e) {         
			ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR  ,'Saving record failed.' +e.getMessage()));
        } 
        return pageRef;
    }




   /*
    * @name         onSend
    * @description  This is called from the vf page when a user presses the "Send" button
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */ 
    public PageReference onSend() {
    
    	// Check that an attachment has been specified - needed for the PDF of order request details
        //if( (!selectedAttachments.isEmpty()) || (Test.isRunningTest())  ){

	        Messaging.SingleEmailMessage theMessage = new Messaging.SingleEmailMessage();

	        try{
	        	
				// Check if sending an attachment is required
				if( (getIsAttachmentRequired(oitm.PriceBookEntry.Product2.Name) == true ) || (Test.isRunningTest()) ) {
	        	
		            // Create the email attachment
		            List<Messaging.Emailfileattachment> lListEfa = new List<Messaging.Emailfileattachment>();
		            for( Attachment a : selectedAttachments) {
		                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
		                efa.setFileName(a.Name);
		                efa.setBody(a.Body);
		                efa.setContentType(a.ContentType);
		                lListEfa.add(efa);
		            }
		            theMessage.setFileAttachments(lListEfa);         
	            
				}
	            
	            // Set the to address(es)
	            List<String> toAddresses = new List<String>();
	            if(toAddr.trim().length() > 0 ){
	            	
	            	// Check if there are multiple address
	            	if(toAddr.contains(';')){
	                	toAddresses = toAddr.split(';');	            		
	            	}else{
	            		toAddresses.add(toAddr);         
	            	}
	            	theMessage.setToAddresses(toAddresses);	 
	            }
	            
	            //List<String> toAddresses = new List<String>();
	            //toAddresses.add(toAddr);         
	            //theMessage.setToAddresses(toAddresses);
	            
	            // Set the cc address(es)
	            if(ccAddr.trim().length() > 0){
	                //String[] ccAddresses = ccAddr.split(',', 0);
	                String[] ccAddresses = ccAddr.split(',');
	                theMessage.setCcAddresses(ccAddresses); 
	            }
	            

	            theMessage.setSubject(subject);
	            theMessage.setHtmlBody(body);
	            
	        	// Store as an activity           
	            //theMessage.setWhatId(oitm.OrderId);
	            //theMessage.setTargetObjectId(UserInfo.getUserId());
	            //theMessage.setsaveAsActivity(true);
	            

	            
	            
	        	// Do not store as an activity	            
	            //theMessage.setsaveAsActivity(false);
	            
	            // Sends the email
	            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {theMessage}); 
	            if(r[0].isSuccess()){
	                sendSuccess = true;
	                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.INFO, 'Message Sent Successfully!'));
	                
	                // Update the Status
					setOrderItemStatus(CONST_STATUS_SENT_TO_SUPPLIER);
					
					// Add an Email Activity History record
					if(getIsActivityRequired(oitm.PriceBookEntry.Product2.Name) == true){
						insertEmailActivityHistory(CONST_STATUS_SENT_TO_SUPPLIER);						
					}
	            }
	        } catch(emailException e) {
				ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR  ,'Sending email failed.' +e.getMessage()));
	        }        	
//        }else{
    	
//    		ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.ERROR  ,'Please select an attachment to send'));
//    	}
        
        return null;
    }
    
    

   /*
    * @name         setOrderItemStatus
    * @description  updates the Order Item with the Status - this will update the order request using the upsert trigger
    * @author       P Goodey
    * @date         April 2014
    * @see 
    */
    private void insertEmailActivityHistory(String psStatus){
        task t = new task();
        t.WhatId 		= oreq.Id;
        t.type 			='Email';
        t.ActivityDate 	= system.today();
        t.Status 		= 'Completed';
        t.Subject 		= oreq.Name + ' ' + psStatus;
        t.OwnerId 		= UserInfo.getUserId();
        insert t;    	
    }



   /*
    * @name         setOrderItemStatus
    * @description  updates the Order Item with the Status - this will update the order request using the upsert trigger
    * @author       P Goodey
    * @date         April 2014
    * @see 
    */
    private void setOrderItemStatus(String psStatus){
    	oitm.Status__c = psStatus;
    	update oitm;
    }
    


   /*
    * @name         getRecordTypeName
    * @description  Return the Record Type Name for the Order Request Record Type
    * @author       P Goodey
    * @date         April 2014
    * @see 
    */
    public String getRecordTypeName(){
        String sReturn = '';
        // Get the Record Type Name
        Schema.SObjectType token = oreq.Id.getSObjectType();
        Schema.DescribeSObjectResult dr = token.getDescribe();
        String lsRecordTypeName = getRecordTypeName(dr.getName(), oreq.RecordTypeId);
        sReturn = lsRecordTypeName;
        return sReturn;
    }
    

   /*
    * @name         getSupplierEmail
    * @description  Return the Supplier Email set for the Order Request Record Type
    * @author       P Goodey
    * @date         April 2014
    * @see 
    */
    public String getSupplierEmail(){
        String sReturn;
        // Get the Record Type Name
        //Schema.SObjectType token = oreq.Id.getSObjectType();
        //Schema.DescribeSObjectResult dr = token.getDescribe();
        //String lsRecordTypeName = getRecordTypeName(dr.getName(), oreq.RecordTypeId);
        //sReturn = lsRecordTypeName;
        Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        // Retrieve the Supplier Email set for the Order Request Record Type
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            // Handle Device Training Day Changes Jan 2015
            //if (orRecordType.Name == lsRecordTypeName) {
            //if (orRecordType.Record_Type_Label__c == lsRecordTypeName) {

            // Compare the name of the Product to get the Email Details
            // Jan 2015
            if( oitm.PriceBookEntry.Product2.Name == orRecordType.Product_Name__c ){

                sReturn = (string)orRecordType.get('Supplier_Email__c');
                break;
            }




        }
        return sReturn;
    }


   /*
    * @name         getSubjectEmailSend
    * @description  Return the Send Subject Email text set for the Order Request Record Type
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    public String getSubjectEmailSend(){
        String sReturn;
        
        // Get the Record Type Name
        //Schema.SObjectType token = oreq.Id.getSObjectType();
        //Schema.DescribeSObjectResult dr = token.getDescribe();
        //String lsRecordTypeName = getRecordTypeName(dr.getName(), oreq.RecordTypeId);
        
        //sReturn = lsRecordTypeName;
        Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        // Retrieve the Subject Email Send text for the Order Request Record Type
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            //if (orRecordType.Name == lsRecordTypeName) {
            // Handle Device Training Day Changes Jan 2015
            //if (orRecordType.Record_Type_Label__c == lsRecordTypeName) {
            
            // Compare the name of the Product to get the Email Details
            // Jan 2015
            if( oitm.PriceBookEntry.Product2.Name == orRecordType.Product_Name__c ){
                sReturn = orRecordType.Supplier_Email_Subject_Send__c;
                break;
            }
        }
        return sReturn;
    }


   /*
    * @name         getSubjectEmailCancel
    * @description  Return the Cancel Subject Email text set for the Order Request Record Type
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    public String getSubjectEmailCancel(){
        String sReturn;
        // Get the Record Type Name
        //Schema.SObjectType token = oreq.Id.getSObjectType();
        //Schema.DescribeSObjectResult dr = token.getDescribe();
        //String lsRecordTypeName = getRecordTypeName(dr.getName(), oreq.RecordTypeId);
        //sReturn = lsRecordTypeName;
        Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        // Retrieve the Subject Email Cancel text for the Order Request Record Type
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            //if (orRecordType.Name == lsRecordTypeName) {
            // Handle Device Training Day Changes Jan 2015
            //if (orRecordType.Record_Type_Label__c == lsRecordTypeName) {            
            // Compare the name of the Product to get the Email Details
            // Jan 2015
            if( oitm.PriceBookEntry.Product2.Name == orRecordType.Product_Name__c ){
                sReturn = orRecordType.Supplier_Email_Subject_Cancel__c;
                break;
            }
        }
        return sReturn;
    }


   /*
    * @name         getBodyEmailSend
    * @description  Return the Send Body Email text set for the Order Request Record Type
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    public String getBodyEmailSend(){
        String sReturn;
        // Get the Record Type Name
        //Schema.SObjectType token = oreq.Id.getSObjectType();
        //Schema.DescribeSObjectResult dr = token.getDescribe();
        //String lsRecordTypeName = getRecordTypeName(dr.getName(), oreq.RecordTypeId);
        //sReturn = lsRecordTypeName;
        Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        // Retrieve the Body Email Send text for the Order Request Record Type
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            //if (orRecordType.Name == lsRecordTypeName) {
            // Handle Device Training Day Changes Jan 2015
            //if (orRecordType.Record_Type_Label__c == lsRecordTypeName) {            
            // Compare the name of the Product to get the Email Details
            // Jan 2015
            if( oitm.PriceBookEntry.Product2.Name == orRecordType.Product_Name__c ){
                sReturn = orRecordType.Supplier_Email_Body_Send__c;
                break;
            }
        }
        return sReturn;
    }



   /*
    * @name         getBodyEmailCancel
    * @description  Return the Cancel Body Email text set for the Order Request Record Type
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    public String getBodyEmailCancel(){
        String sReturn;
        // Get the Record Type Name
        //Schema.SObjectType token = oreq.Id.getSObjectType();
        //Schema.DescribeSObjectResult dr = token.getDescribe();
        //String lsRecordTypeName = getRecordTypeName(dr.getName(), oreq.RecordTypeId);
        //sReturn = lsRecordTypeName;
        Map<String, OrderRequest_RecordTypeSettings__c> orAllRecordTypes = OrderRequest_RecordTypeSettings__c.getAll();
        // Retrieve the Body Email Cancel text for the Order Request Record Type
        for(OrderRequest_RecordTypeSettings__c orRecordType : orAllRecordTypes.values()) {
            //if (orRecordType.Name == lsRecordTypeName) {
            // Compare the name of the Product to get the Email Details
            // Jan 2015
            if( oitm.PriceBookEntry.Product2.Name == orRecordType.Product_Name__c ){
                sReturn = orRecordType.Supplier_Email_Body_Cancel__c;
                break;
            }
        }
        return sReturn;
    }


   /*
    * @name         getRecordTypeName
    * @description  get the record type name for the passed RecordType Id 
    *               - This is a workaround due to Force.com bug.  See https://success.salesforce.com/issues_view?id=a1p30000000T1zGAAS
    * @author       P Goodey
    * @date         April 2014
    * @see 
    */
    private String getRecordTypeName(String psSobject, Id pIdRecordTypeId){
        String[] types = new String[]{psSobject}; 
        List<Schema.DescribeSobjectResult> results = Schema.describeSObjects(types); 
        Map<Id, String> rmap = new Map<Id, String> (); 
        for (Schema.RecordTypeInfo ri: results[0].getRecordTypeInfos()) { 
            rmap.put(ri.getRecordTypeId(), ri.getName());
        }
        return(rmap.get(pIdRecordTypeId));
    }
    
        

    
}