/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CountCallVisitofOpp {

     static testMethod void myUnitTest() {
        // TO DO: implement unit test
        //Test_Factory.SetProperty('IsTest', 'yes');
        //Account a = Test_Factory.CreateAccount();
        //a.Name = 'Test';
        //insert a;
        
        Account a = new Account(Name='Test Account to test trig');
        insert a;
        
        
        System.debug('inserted account');
        
        Opportunity o1 = new Opportunity(AccountId=a.id, Name='Test Opp1 to test trig', Product_Family__c='BROADBAND PRODUCTS', StageName='Created',CloseDate=Date.today()+7);
        Opportunity o2 = new Opportunity(AccountId=a.id, Name='Test Opp2 to test trig', Product_Family__c='BROADBAND PRODUCTS', StageName='Created',CloseDate=Date.today()+7);        
        List<Opportunity> opls=new List<Opportunity>();
        opls.add(o1);
        opls.add(o2);
        
        insert opls;
        System.debug('inserted o1, o2');
        
        /*
        Corp Customer Audio / Call Outbound - DMNA
		Corp Customer Audio / Customer Conference Call
		Corp Customer Audio / Healing Call
		Corp Customer Audio / Sales Genius Follow Up Call
		Corp Customer Inbound Call / Call - Inbound
		Corp Non Customer Facing / Conference call
		
		Corp Customer Visit/Vist
		BT User BTLB Site Visit/Scheduled Visit
		DBAM Visit - Half Day
		DBAM Visit - Full Day
        */
         
         Contact c=new Contact(Status__c='Active', Salutation='Mr', Preferred_Contact_Channel__c='Email', Phone_Consent__c='No', Phone_Consent_Date__c=Datetime.now(), Phone='0123456789', LastName='testln', Job_Title__c='OTHER', Job_Function__c='OTHER', FirstName='testfn', Email_Consent__c='No', Email_Consent_Date__c=Datetime.now(), Email='test@testanilcontact.com', Address_POBox__c='IP4 1NX', Address_Consent__c='No', Address_Consent_Date__c=Datetime.now(), AccountId= a.Id);
         insert c;
        
        Event e1= new event(whatid=o1.id, whoid=c.id, Subject='call_opp1', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Call Outbound',Contact_Id__c=c.id); 
       
        
        
        Event e2= new event(whatid=o1.id, whoid=c.id, Subject='visit_opp1', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Visit',Activity_type__c='Visit',Contact_Id__c=c.id);
        Event e3= new event(whatid=o1.id, whoid=c.id, Subject='other_opp1', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Advocacy Healthcheck',Contact_Id__c=c.id);
        
              
        Event e4= new event(whatid=o2.id, whoid=c.id,Subject='call_opp12', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Call Outbound - DMNA',Contact_Id__c=c.id); 
        Event e5= new event(whatid=o2.id, whoid=c.id,Subject='visit_opp2', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='BT User BTLB Site Visit',Activity_type__c='Scheduled Visit',Contact_Id__c=c.id);
        Event e6= new event(whatid=o2.id, whoid=c.id,Subject='other_opp2', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Advocacy Healthcheck',Contact_Id__c=c.id);
        
       
        
        Event e7= new event(whatid=o1.id, whoid=c.id,Subject='call_opp1', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Customer Conference Call',Contact_Id__c=c.id); 
        Event e8= new event(whatid=o1.id, whoid=c.id,Subject='visit_opp1', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='DBAM Visit - Half Day',Contact_Id__c=c.id);
        Event e9= new event(whatid=o1.id, whoid=c.id,Subject='other_opp1', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Advocacy Healthcheck',Contact_Id__c=c.id);
        
        
        Event e10= new event(whatid=o2.id, whoid=c.id,Subject='call_opp2', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Healing Call',Contact_Id__c=c.id); 
        Event e11= new event(whatid=o2.id, whoid=c.id,Subject='visit_opp2', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='DBAM Visit - Full Day',Contact_Id__c=c.id);
        Event e12= new event(whatid=o2.id, whoid=c.id,Subject='other_opp2', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Audio',Activity_type__c='Advocacy Healthcheck',Contact_Id__c=c.id);
        Event e13= new event(whatid=o2.id, whoid=c.id,Subject='call_opp2', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Customer Inbound Call',Activity_type__c='Call - Inbound',Contact_Id__c=c.id);
        Event e14= new event(whatid=o2.id, whoid=c.id,Subject='call_opp2', StartDatetime=Datetime.now(), EndDateTime=Datetime.now().addMinutes(120), Assigned_User_Id__c=UserInfo.getUserId(), Activity_Area__c='Corp Non Customer Facing',Activity_type__c='Conference call',Contact_Id__c=c.id);
        
        
        List<Event> evls=new List<Event>();
        evls.add(e1);
        evls.add(e2);
        evls.add(e3);
        evls.add(e4);
        evls.add(e5);
        evls.add(e6);
        evls.add(e7);
        evls.add(e8);
        evls.add(e9);
        evls.add(e10);
        evls.add(e11);
        evls.add(e12);
        evls.add(e13);
        evls.add(e14);
        
        insert evls;
        System.debug('events');
        
        e1.Subject='call_opp1_to_visit_opp1';
        e1.Activity_Area__c='Corp Customer Visit';
        e1.Activity_type__c='Visit';
        
        e2.Subject='visit_opp1_to_call_opp1';
        e2.Activity_Area__c='Corp Customer Audio';
        e2.Activity_type__c='Sales Genius Follow Up Call';
        
        e3.Subject='other_opp1_to_visit_opp1';
        e3.Activity_Area__c='Corp Customer Visit';
        e3.Activity_type__c='Visit';
        
        e4.Subject='call_opp2_to_visit_opp12';
        e4.Activity_Area__c='BT User BTLB Site Visit';
        e4.Activity_type__c='Scheduled Visit';
        
        e5.Subject='visit_opp2_to_other_opp2';
        e5.Activity_Area__c='Corp Customer Audio';
        e5.Activity_type__c='Advocacy Healthcheck';
        
        e6.Subject='other_opp2_to_call_opp2';
        e6.Activity_Area__c='Corp Customer Audio';
        e6.Activity_type__c='Call Outbound - DMNA';    
        
        
        List<Event> u_evls=new List<Event>();
        
        u_evls.add(e1);
        u_evls.add(e2);
        u_evls.add(e3);
        u_evls.add(e4);
        u_evls.add(e5);
        u_evls.add(e6);
        
        update evls;
        System.debug('updated events');
        
        List<event> d_evls=new List<Event>();
        d_evls.add(e1);
        d_evls.add(e2);
        d_evls.add(e3);
        d_evls.add(e4);
        d_evls.add(e5);
        d_evls.add(e6);
        
        delete d_evls;
        System.debug('deleted events');
    }
}