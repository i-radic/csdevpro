@isTest(seeAllData = true)
private class addressResultsCRF_Test {
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.Address_Type__c='CeaseAddress';
        
        PageReference PageRef = Page.AddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Address_Type__c);
        
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    
    static testMethod void runPositiveTestCases5() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.Address_Type__c='ProvideAddress';
        
        PageReference PageRef = Page.AddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Address_Type__c);
        
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    
    static testMethod void runPositiveTestCases4() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.Address_Type__c='AlternateBillingAddress';
        
        PageReference PageRef = Page.AddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Address_Type__c);
        
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    
    
    static testMethod void runPositiveTestCases2() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'update';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect');
        addResultsCRF.Back();
        system.debug('Back');      
    }
    static testMethod void runPositiveTestCases6() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.AX_Address_Type__c='CorrespondenceAddress';
        
        PageReference PageRef = Page.AddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.AX_Address_Type__c);
        
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    static testMethod void runPositiveTestCases7() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.AX_Address_Type__c='DeliveryAddress';
        
        PageReference PageRef = Page.AddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.AX_Address_Type__c);
        
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    static testMethod void runPositiveTestCases8() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.AX_Address_Type__c='BillingAddress';
        
        PageReference PageRef = Page.AddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.AX_Address_Type__c);
        
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    static testMethod void runPositiveTestCases9() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Address_Details__c addressDet = new Address_Details__c();
        addressDet.Address_Type_NSO__c='ExistingAddress';
        
        PageReference PageRef = Page.AddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Address_Type_NSO__c);
        
        addressResultsCRF addResultsCRF = new addressResultsCRF(addressDet);
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    
}