public with sharing class addressSearchResults {

    public Contact contact;  
    public Boolean test= true;
    public string refferalPage = ApexPages.currentPage().getParameters().get('refPage');
    public string postcode = ApexPages.currentPage().getParameters().get('p');
    List<Addresswrapper> ResultsList = new List<Addresswrapper>();
    List<Address> selectedAddress = new List<Address>(); //singhd62
    
    
    public Boolean getTest(){
        return test;
    }
    
    public addressSearchResults(Contact contact_edit){
         contact = contact_edit;
    }
    
    public PageReference CreateAddress(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }
        PageReference PageRef = new PageReference('/apex/createAddress?id='+contact.Id+'&refPage='+refferalPage);
        PageRef.setRedirect(true);      
        return PageRef;                 
    }
    
    public PageReference Back(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
        if (refferalPage == 'new'){
            PageReference PageRef = new PageReference('/apex/createcontact?id='+contact.Id);
            PageRef.setRedirect(true);
                       
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }    
            return PageRef;                       
        }
        if (refferalPage == 'update'){
            PageReference PageRef = new PageReference('/apex/updatecontact?id='+contact.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }   
            return PageRef;    
        }
        if (refferalPage == 'detail'){
            PageReference PageRef = new PageReference('/'+contact.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }      
            return PageRef;  
        }
        return null;
    }
     
    public addressSearchResults(ApexPages.StandardController stdController) {

         contact = (Contact)stdController.getRecord();
    }
    
    public List<Addresswrapper> pullAddr {
        get {
          //  system.debug('KKKKKKK KKKKKK KKKKKK '+pullAddresses());
            return pullAddresses();
        }
        
    }
    
    
    public List<Addresswrapper> pullAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceCRMService.CRMAddressList NADResponse = null; 
            
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceCRMService.CRMAddressList();
            NADResponse.Addresses = new SalesforceCRMService.CRMArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceCRMService.CRMAddress[0];
                    
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            a.BuildingName = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                system.debug('POSTCODE : '+postCode);
                NADResponse = SalesforceCRMService.GetNADAddress(postCode);
                system.debug('nadresponse ' + NADResponse);
              
                if(NADResponse.Addresses == null) {
                    return null;
               }
               
           }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;
            }                                              
        } 
        
                                            
        
             
        for(SalesforceCRMService.CRMAddress sfsAddress : NADResponse.Addresses.Address){
            
            system.debug('LLLLLLLLL '+sfsAddress );
            
            Address a = new Address();
                    
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
            a.BuildingName = sfsAddress.BuildingName;
            
            ResultsList.add(new Addresswrapper(a));
            
                                                                     
        }
        if(ResultsList.size()>=1){
            
            system.debug('ResultsList :'+ResultsList);
                
            return ResultsList;
            
            
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please \'Create Address\'.'));          
             return null;
        }
    }
    
    // singhd62   
     /*
    public List<Addresswrapper> getAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceServicesCRM.AddressList NADResponse = null; 
        SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();            
            
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceServicesCRM.AddressList();
            NADResponse.Addresses = new SalesforceServicesCRM.ArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceServicesCRM.Address[0];
                    
            SalesforceServicesCRM.Address a = new SalesforceServicesCRM.Address();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                service.timeout_x = 60000;
                NADResponse = Endpoints.SFCRMGateway().GetNADAddress('', '', postCode);
            }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;
            }                                              
        }                                       
            
        for(SalesforceServicesCRM.Address sfsAddress : NADResponse.Addresses.Address){
            
            Address a = new Address();
                    
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
                
            ResultsList.add(new Addresswrapper(a));                                                         
        }
        if(ResultsList.size()>1){        
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please \'Create Address\'.'));          
             return null;
        }
    }
    */
    public PageReference getSelected()
    {
        selectedAddress.clear();
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            Address a = new Address(); //singhd62
            
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            a.BuildingName = '_TEST_';
            a.IdentifierId='_TEST_';
            a.IdentifierName='_TEST_';
                
            Addresswrapper wrapper = new Addresswrapper(a);
            wrapper.selected = true;
            ResultsList.add(wrapper);                        
        }        
        
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                          
                    contact.MailingStreet = accwrapper.acc.Street;
                    contact.MailingState = accwrapper.acc.County;
                    contact.MailingPostalCode = accwrapper.acc.PostCode;
                    contact.MailingCountry = accwrapper.acc.Country;
                    contact.MailingCity = accwrapper.acc.Town;
                
                    contact.OtherStreet = accwrapper.acc.Street;
                    contact.OtherState = accwrapper.acc.County;
                    contact.OtherPostalCode = accwrapper.acc.PostCode;
                    contact.OtherCountry = accwrapper.acc.Country;
                    contact.OtherCity = accwrapper.acc.Town;                
                               
                    //Address Key#########
                    contact.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                    contact.Address_Country__c = accwrapper.acc.Country;
                    contact.Address_County__c = accwrapper.acc.County;
                    contact.Contact_Building__c = accwrapper.acc.Name;
                    contact.Address_POBox__c = accwrapper.acc.POBox;
                    contact.Contact_Address_Number__c = accwrapper.acc.BuildingNumber;
                    contact.Contact_Address_Street__c = accwrapper.acc.Street;
                    contact.Contact_Locality__c = accwrapper.acc.Locality;
                    contact.Contact_Post_Code__c = accwrapper.acc.PostCode;
                    contact.Contact_Post_Town__c = accwrapper.acc.Town;
                    contact.Contact_Sub_Building__c = accwrapper.acc.SubBuilding;
                    contact.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality;
            		contact.Contact_Building__c = accwrapper.acc.BuildingName;
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update contact;
                        Return ReDirect();
                    }    
        }    
        return null;
    }
    
    public List<Address> getselectedAddress() // singhd62
    {
        if(selectedAddress.size()>0){
            return selectedAddress;
        }
        else
        return null;
    }    
    
    public with sharing class Addresswrapper
    {
        public Address acc {get; set;} //singhd62
        public Boolean selected {get; set;}
        public Addresswrapper(Address a) //singhd62
        {
            acc = a;
            selected = false;
        }
    }
    
    public class Address {
        public String IdentifierId {get; set;}
        public String IdentifierName {get; set;}
        public String IdentifierValue {get; set;}        
        public String Country {get; set;}
        public String County {get; set;}
        public String Name {get; set;}
        public String POBox {get; set;}
        public String BuildingNumber {get; set;}
        public String Street {get; set;}
        public String Locality {get; set;}
        public String DoubleDependentLocality {get; set;}
        public String PostCode {get; set;}
        public String Town {get; set;}
        public String SubBuilding {get; set;}
        public String BuildingName {get; set;}
    }    
 
   
}