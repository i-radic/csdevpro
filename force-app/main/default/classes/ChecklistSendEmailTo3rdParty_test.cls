/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class ChecklistSendEmailTo3rdParty_test {
    
    private static Map<String, Id> multiRecordTypeMap;
    private static Account mAccount;
    private static Opportunity mOpportunity;
    private static Company_Implementation__c mCompanyImplementation;
    private static Checklist__c mChecklist;	
    
    static{
        
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
        }
        
        // Store the record types for the objects of interest
        if (multiRecordTypeMap == null) {
            multiRecordTypeMap = new Map<String, Id>(); 
            for( RecordType rct : [SELECT Id, Name, DeveloperName
                                   FROM RecordType 
                                   WHERE SobjectType='Opportunity'
                                   OR SobjectType='Checklist__c']){
                                       
                                       multiRecordTypeMap.put(rct.DeveloperName, rct.Id);
                                   }
        }
        
        // Create a single test account. false = no incremental count for the Company Name
        mAccount = EE_TestClass.createStaticAccount('Test Company', false);
        insert mAccount;
        
        // Create a Test Opportunity
        mOpportunity = new Opportunity();
        mOpportunity.Name 		= 'Test';
        mOpportunity.AccountId	= mAccount.Id;
        mOpportunity.CloseDate	= system.today().addYears(10);
        mOpportunity.StageName	= 'Decision';
      //  mOpportunity.RecordTypeId = multiRecordTypeMap.get('Standard');
        insert mOpportunity;
        
        // Create a Test Company Implementation record (manually)
        mCompanyImplementation = new Company_Implementation__c();
        mCompanyImplementation.Name = 'Test';
        mCompanyImplementation.Opportunity__c = mOpportunity.Id;
        mCompanyImplementation.Company__c = mAccount.Id;
        insert mCompanyImplementation;			
        
        // Create a Test Checklist record
        mChecklist = new Checklist__c();
       // mChecklist.RecordTypeId = multiRecordTypeMap.get('Contract_Setup_3rd_Party_Solutions');
        mChecklist.Company_Implementation__c = mCompanyImplementation.Id;
        mChecklist.Email_3rd_Party__c = 'test@test.com';
        mChecklist.Contact_Email__c = 'test@test.com';
        mChecklist.Contact_Name__c = 'Mr Test';
        mChecklist.Contact_Phone__c = '123456789';
        mChecklist.Delivery_Address__c = '123 HighStreet';
        mChecklist.Status__c = 'In Progress';
        mchecklist.Order_User__c = userinfo.getUserid();
        insert mChecklist;
        
    }
    
    /*
* @name         testSendEmailWithAttachment
* @description	Send Email Test - Although cannot assert so used only for code coverage
* @author       P Goodey
* @date         Sep 2015
* @see 
*/
    static testmethod void testSendEmailWithAttachment() {    
        
        // Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Checklist Id      
        PageReference pageRef = Page.ChecklistSendEmailTo3rdParty;
        pageRef.getParameters().put('Id', mChecklist.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        ChecklistSendEmailTo3rdParty chkExt = new ChecklistSendEmailTo3rdParty(new ApexPages.StandardController(mChecklist));
        
        // Set the Attachment
        Blob b = Blob.valueOf('Test Data');  
        Attachment attachment = new Attachment();  
        attachment.ParentId = mOpportunity.Id;  
        attachment.Name = Label.Checklist_Product_Filename;  
        attachment.Body = b;  
        insert(attachment);         
        
        // Click the Send Email Button
        chkExt.SendEmail();
        
        // Stop Tests
        Test.StopTest();
        
    } 
    
    
    /*
* @name         testAttachmentMissing
* @description	Attachment Test
* @author       P Goodey
* @date         Sep 2015
* @see 
*/
    static testmethod void testAttachmentMissing() {    
        
        // Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Checklist Id      
        PageReference pageRef = Page.ChecklistSendEmailTo3rdParty;
        pageRef.getParameters().put('Id', mChecklist.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        ChecklistSendEmailTo3rdParty chkExt = new ChecklistSendEmailTo3rdParty(new ApexPages.StandardController(mChecklist));        
        
        // Execute the Check for Attachments
        boolean bAttachmentExists = chkExt.getAttachmentExists();
        
        // Stop Tests
        Test.StopTest();
        
        // Assert that he Check for Attachments is that no attachment exists
        system.AssertEquals( false, bAttachmentExists );   
    }
    
    
    /*
* @name         testAttachmentFound
* @description	Attachment Test
* @author       P Goodey
* @date         Sep 2015
* @see 
*/
    static testmethod void testAttachmentFound() {    
        
        // Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Checklist Id      
        PageReference pageRef = Page.ChecklistSendEmailTo3rdParty;
        pageRef.getParameters().put('Id', mChecklist.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        ChecklistSendEmailTo3rdParty chkExt = new ChecklistSendEmailTo3rdParty(new ApexPages.StandardController(mChecklist));
        
        // Set the Attachment
        Blob b = Blob.valueOf('Test Data');  
        Attachment attachment = new Attachment();  
        attachment.ParentId = mOpportunity.Id;  
        attachment.Name = Label.Checklist_Product_Filename;  
        attachment.Body = b;  
        insert(attachment);        
        
        // Execute the Check for Attachments
        boolean bAttachmentExists = chkExt.getAttachmentExists();
        
        // Stop Tests
        Test.StopTest();
        
        // Assert that he Check for Attachments is that the attachment exists
        system.AssertEquals( true, bAttachmentExists );   
    }
    
    
    /*
* @name         testEmailSent
* @description	Email Sent Test
* @author       P Goodey
* @date         Sep 2015
* @see 
*/
    static testmethod void testEmailSent() {    
        
        // Start Tests	        
        Test.StartTest();
        
        // Load the Page using the Checklist Id      
        PageReference pageRef = Page.ChecklistSendEmailTo3rdParty;
        pageRef.getParameters().put('Id', mChecklist.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Load the Controller Extension
        ChecklistSendEmailTo3rdParty chkExt = new ChecklistSendEmailTo3rdParty(new ApexPages.StandardController(mChecklist));        
        
        // Execute the Check for Email Sent
        chkExt.getEmailSent();
        
        // Stop Tests
        Test.StopTest();
        
    }    
    
    
}