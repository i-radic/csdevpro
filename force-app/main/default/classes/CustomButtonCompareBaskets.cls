global with sharing class CustomButtonCompareBaskets extends csbb.CustomButtonExt {
    
    public String performAction (String basketId) {
        cscfga__Product_Basket__c pb = [select cscfga__Opportunity__c from cscfga__Product_Basket__c where id = :basketId];
        String newUrl = '/apex/c__CS_Basket_Compare?scontrolCaching=1&id='+pb.cscfga__Opportunity__c+'&basketId='+basketId;
        
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    } 
}