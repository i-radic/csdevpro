global with sharing class CustomButtonImplementationForm extends csbb.CustomButtonExt {
  public String performAction (String basketId) {
    String configs = getCoreConfigIds(basketId);
    if(configs == ''){
        return '{"status":"error","title":"Error", "text":"Please make sure at least one valid core proposition is in the Product Basket"}';
      }else{
        String newUrl = '/apex/c__ImplementationForm?configs=' + configs + '&basketId=' + basketId;
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
      }
  }
  @TestVisible
  String performAction (String basketId, String pcrIds) {
    String configs = getCoreConfigIds(basketId);
    if(configs == ''){
        return '{"status":"error","title":"Error", "text":"Please make sure at least one valid core proposition is in the Product Basket"}';
      }else{
        String newUrl = '/apex/c__ImplementationForm?configs=' + configs + '&basketId=' + basketId;
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
      }  
  }

  private String getCoreConfigIds(String basketId){
  	List<cscfga__Product_Configuration__c> pcs = [SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :basketId AND Product_Definition_Name__c IN ('BT Mobile Sharer', 'BT Mobile Flex', 'BTOP CORE') AND cscfga__Configuration_Status__c = 'Valid' ];  
  	List<Map<String, String>> configs = new List<Map<String, String>>();

    if(pcs.size() > 0){
      for(cscfga__Product_Configuration__c pc : pcs){
          configs.add(new Map<String, String>{ pc.Name.replace('&', 'and') => pc.Id });
      }
      return JSON.serialize(configs).escapeEcmaScript();
    } else {
      return '';
    }
  }


}