public with sharing class addressResultsCRF {

    public Address_Details__c addressDet;  
    public Boolean test= true;
    //public String ExchangeGroupCode;

    
    
    public string refferalPage = ApexPages.currentPage().getParameters().get('refPage');
    public string postcode = ApexPages.currentPage().getParameters().get('p');


    public string selectedAddr = ApexPages.currentPage().getParameters().get('AddType');
    List<Addresswrapper> ResultsList = new List<Addresswrapper>();
    
    List<SalesforceCRMService.CRMAddress> selectedAddress = new List<SalesforceCRMService.CRMAddress>(); // singhd62 
    
    public Boolean getTest(){
        return test;
    }
  
    public addressResultsCRF(Address_Details__c address_edit){
         addressDet = address_edit;
    }
    
    public PageReference CreateAddress(){  System.debug('abccd'+selectedAddr);                                   
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }
        PageReference PageRef = new PageReference('/apex/createCRFAddress?id='+addressDet.Id+'&refPage='+refferalPage+'&AddType1='+selectedAddr);
        PageRef.setRedirect(true);      
        return PageRef;                 
    }
    
    public PageReference Back(){                                     
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            return null;
        }      
        return ReDirect();                       
    }
    
    public PageReference ReDirect(){              
        if (refferalPage == 'new'){
            PageReference PageRef = new PageReference('/apex/CRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
                       
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }    
            return PageRef;                       
        }
        if (refferalPage == 'update'){
            PageReference PageRef = new PageReference('/apex/CRFAddress?id='+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }   
            return PageRef;    
        }
       /* if (refferalPage == 'detail'){
            PageReference PageRef = new PageReference('/'+addressDet.Id);
            PageRef.setRedirect(true);
            if(Test_Factory.GetProperty('IsTest') == 'yes') {
                return null;
            }      
            return PageRef;  
        }*/
        return null;
    }
     
    public addressResultsCRF(ApexPages.StandardController controller) {
     addressDet = (Address_Details__c)controller.getRecord();
     
    }
    
    public List<Addresswrapper> getAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceCRMService.CRMAddressList NADResponse = null;        
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceCRMService.CRMAddressList();
            NADResponse.Addresses = new SalesforceCRMService.CRMArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceCRMService.CRMAddress[0];
                                     
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            //a.ExchangeGroupCode = '_TEST_';
            //a.IdentifierId = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                
                NADResponse = SalesforceCRMService.GetNADAddress(postCode);
            }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;

            }                                             
        }                                       
              
        for(SalesforceCRMService.CRMAddress sfsAddress : NADResponse.Addresses.Address){
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();            
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
            //a.ExchangeGroupCode = sfsAddress.ExchangeGroupCode;
            
            ResultsList.add(new Addresswrapper(a));                                                         
        }

        if(ResultsList.size()>1){       
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please \'Create Address\'.'));          
             return null;
        }
    }
    
    /* -- singhd62 
    public List<Addresswrapper> getAddresses()
    {                      
        ResultsList.Clear(); 
            
        SalesforceServicesCRM.AddressList NADResponse = null;        
        SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway(); 
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            NADResponse = new SalesforceServicesCRM.AddressList();
            NADResponse.Addresses = new SalesforceServicesCRM.ArrayOfAddress();
            NADResponse.Addresses.Address = new SalesforceServicesCRM.Address[0];
                                     
            SalesforceServicesCRM.Address a = new SalesforceServicesCRM.Address();                
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            //a.ExchangeGroupCode = '_TEST_';
            //a.IdentifierId = '_TEST_';
                    
            NADResponse.Addresses.Address.Add(a);
            return null;               
        }
        else{
            try {
                service.timeout_x = 60000;
                NADResponse = Endpoints.SFCRMGateway().GetNADAddress('', '', postCode);
            }
            catch (System.CalloutException ex){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Address-match time out occurred please press back button and try again (NAD matched addresses are a requirement however if you receive 3 failed attempts you can create the address manually)'));
                return null;

            }                                             
        }                                       
              
        for(SalesforceServicesCRM.Address sfsAddress : NADResponse.Addresses.Address){
            Address a = new Address();            
            a.IdentifierValue = sfsAddress.IdentifierValue;
            a.Country = sfsAddress.Country;
            a.County = sfsAddress.County;
            a.Name = sfsAddress.Name;
            a.POBox = sfsAddress.POBox;
            a.BuildingNumber = sfsAddress.BuildingNumber;
            a.Street = sfsAddress.Street;
            a.Locality = sfsAddress.Locality;
            a.DoubleDependentLocality = sfsAddress.DoubleDependentLocality;
            a.PostCode = sfsAddress.PostCode;
            a.Town = sfsAddress.Town;
            a.SubBuilding = sfsAddress.SubBuilding;
            //a.ExchangeGroupCode = sfsAddress.ExchangeGroupCode;
            
            ResultsList.add(new Addresswrapper(a));                                                         
        }

        if(ResultsList.size()>1){       
            return ResultsList;
        } 
        else {
             ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Address Not Found! Please \'Create Address\'.'));          
             return null;
        }
    }
    */
    
        
    public PageReference getSelected()
    {
        selectedAddress.clear();
        
        if(Test_Factory.GetProperty('IsTest') == 'yes') {
            
            SalesforceCRMService.CRMAddress a = new SalesforceCRMService.CRMAddress();
            
            a.IdentifierValue = '_TEST_';
            a.Country = '_TEST_';
            a.County = '_TEST_';
            a.Name = '_TEST_';
            a.POBox = '_TEST_';
            a.BuildingNumber = '_TEST_';
            a.Street = '_TEST_';
            a.Locality = '_TEST_';
            a.DoubleDependentLocality = '_TEST_';
            a.PostCode = '_TEST_';
            a.Town = '_TEST_';
            a.SubBuilding = '_TEST_';
            //a.ExchangeGroupCode = '_TEST_';
                
            Addresswrapper wrapper = new Addresswrapper(a);
            wrapper.selected = true;
            ResultsList.add(wrapper);                        
        }
        
        if(selectedAddr=='ExistingAddress'){
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                   addressDet.Existing_Country__c = accwrapper.acc.Country;
                    addressDet.Existing_County__c = accwrapper.acc.County;
                    addressDet.Existing_Premises_Building_Name__c = accwrapper.acc.Name;
                   // addressDet.Address_POBox__c = accwrapper.acc.POBox;
                    addressDet.Existing_Thoroughfare_No_Street_Number__c = accwrapper.acc.BuildingNumber;
                    addressDet.Existing_Thoroughfare_Name_Street_Name__c = accwrapper.acc.Street;
                    addressDet.Existing_Locality__c = accwrapper.acc.Locality;
                    addressDet.Existing_Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Existing_Post_Town__c = accwrapper.acc.Town;
                    addressDet.Existing_Sub_Premises__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Existing__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality;   
                    
                                                             
                    //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    
                    //System.debug('NADID-->'+accwrapper.acc.IdentifierValue);
                    //System.debug('ExchangeGroupCode-->'+ExchangeGroupCode); 
                    
                    //if(ExchangeGroupCode!=null)
                    //addressDet.Existing_Exchange_Group_Code__c = ExchangeGroupCode;

                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        }    
        }
                
        if(selectedAddr=='CeaseAddress'){
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
            
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                   addressDet.Cease_Country__c = accwrapper.acc.Country;
                    addressDet.Cease_County__c = accwrapper.acc.County;
                    addressDet.Cease_Premises_Building_Name__c = accwrapper.acc.Name;
                   // addressDet.Address_POBox__c = accwrapper.acc.POBox;
                    addressDet.Cease_Thoroughfare_Number_Street_Number__c = accwrapper.acc.BuildingNumber;
                    addressDet.Cease_Thoroughfare_Name_Street_Name__c = accwrapper.acc.Street;
                    addressDet.Cease_Locality__c = accwrapper.acc.Locality;
                    addressDet.Cease_Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Cease_Post_Town__c = accwrapper.acc.Town;
                    addressDet.Cease_Sub_Premises__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Cease__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality; 
                   //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    //if(ExchangeGroupCode!=null)                                             
                    //addressDet.Cease_Exchange_Group_Code__c = ExchangeGroupCode;
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        }    
        }
        if(selectedAddr=='ProvideAddress'){
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                          
                                  
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                   addressDet.Provide_Country__c = accwrapper.acc.Country;
                    addressDet.Provide_County__c = accwrapper.acc.County;
                    addressDet.Provide_Premises_Building_Name__c = accwrapper.acc.Name;
                   // addressDet.Address_POBox__c = accwrapper.acc.POBox;
                    addressDet.Provide_Thoroughfare_Number_Street_Numb__c = accwrapper.acc.BuildingNumber;
                    addressDet.Provide_Thoroughfare_Name_Street_Name__c = accwrapper.acc.Street;
                    addressDet.Provide_Locality__c = accwrapper.acc.Locality;
                    addressDet.Provide_Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Provide_Post_Town__c = accwrapper.acc.Town;
                    addressDet.Provide_Sub_Premises__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Provide__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality;  
                   //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    //if(ExchangeGroupCode!=null)                                            
                    //addressDet.Provide_Exchange_Group_Code__c = ExchangeGroupCode;
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        }    
        }
        if(selectedAddr=='AlternateBillingAddress'){
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                          
                          
                              
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                   addressDet.Billing_Country__c = accwrapper.acc.Country;
                    addressDet.Billing_County__c = accwrapper.acc.County;
                    addressDet.Billing_Premises_Building_Name__c = accwrapper.acc.Name;
                   // addressDet.Address_POBox__c = accwrapper.acc.POBox;
                    addressDet.Billing_Thoroughfare_Number_Street_Numb__c = accwrapper.acc.BuildingNumber;
                    addressDet.Billing_Thoroughfare_Name_Street_Name__c = accwrapper.acc.Street;
                    addressDet.Billing_Locality__c = accwrapper.acc.Locality;
                    addressDet.Billing_Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Billing_Post_Town__c = accwrapper.acc.Town;
                    addressDet.Billing_Sub_Premises__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Billing__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality;   
                   //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    //if(ExchangeGroupCode!=null)                                           
                    //addressDet.Billing_Exchange_Group_Code__c = ExchangeGroupCode;
                  
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        } 
        } 
        
        
        if(selectedAddr=='DeliveryAddress'){
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                          
                                  
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                   addressDet.Provide_Country__c = accwrapper.acc.Country;
                    addressDet.Provide_County__c = accwrapper.acc.County;
                    addressDet.Provide_Premises_Building_Name__c = accwrapper.acc.Name;
                    addressDet.Provide_PO_Box__c = accwrapper.acc.POBox;
                    addressDet.Provide_Thoroughfare_Number_Street_Numb__c = accwrapper.acc.BuildingNumber;
                    addressDet.Provide_Thoroughfare_Name_Street_Name__c = accwrapper.acc.Street;
                    addressDet.Provide_Locality__c = accwrapper.acc.Locality;
                    addressDet.Provide_Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Provide_Post_Town__c = accwrapper.acc.Town;
                    addressDet.Provide_Sub_Premises__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Provide__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality; 
                   //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    //if(ExchangeGroupCode!=null)                                             
                    //addressDet.Provide_Exchange_Group_Code__c = ExchangeGroupCode;
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        }    
        }  
        if(selectedAddr=='CorrespondenceAddress'){
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                          
                                  
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                    addressDet.Legal_Country__c = accwrapper.acc.Country;
                    addressDet.Legal_County__c = accwrapper.acc.County;
                    addressDet.Legal_Premises_Building_Name__c = accwrapper.acc.Name;
                    addressDet.Legal_PO_Box__c = accwrapper.acc.POBox;
                    addressDet.LegalThoroughfare_Number_StreetNumber__c = accwrapper.acc.BuildingNumber;
                    addressDet.Legal_Thoroughfare_Name_Street_Name__c = accwrapper.acc.Street;
                    addressDet.Legal_Locality__c = accwrapper.acc.Locality;
                    addressDet.Legal_Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Legal_Post_Town__c = accwrapper.acc.Town;
                    addressDet.Legal_Sub_Premises__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Legal__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality; 
                   //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    //if(ExchangeGroupCode!=null)                                             
                    //addressDet.Legal_Exchange_Group_Code__c = ExchangeGroupCode;
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        }    
        }
        if(selectedAddr=='BillingAddress'){
        for(Addresswrapper accwrapper : ResultsList)
             
        if(accwrapper.selected == true){
                          
                                  
                               
                    //Address Key#########
                    //addressDet.Address_Id__c = accwrapper.acc.IdentifierValue;
                    //####################                    
                    addressDet.Billing_Country__c = accwrapper.acc.Country;
                    addressDet.Billing_County__c = accwrapper.acc.County;
                    addressDet.Billing_Premises_Building_Name__c = accwrapper.acc.Name;
                    addressDet.Billing_PO_Box__c = accwrapper.acc.POBox;
                    addressDet.Billing_Thoroughfare_Number_Street_Numb__c = accwrapper.acc.BuildingNumber;
                    addressDet.Billing_Thoroughfare_Name_Street_Name__c = accwrapper.acc.Street;
                    addressDet.Billing_Locality__c = accwrapper.acc.Locality;
                    addressDet.Billing_Post_Code__c = accwrapper.acc.PostCode;
                    addressDet.Billing_Post_Town__c = accwrapper.acc.Town;
                    addressDet.Billing_Sub_Premises__c = accwrapper.acc.SubBuilding;
                    //addressDet.NAD_ID_Billing__c = accwrapper.acc.IdentifierValue;
                   // addressDet.Double_Dependent_Locality__c = accwrapper.acc.DoubleDependentLocality;  
                   //ExchangeGroupCode= Endpoints.SFCRMGateway().GetNADExchangeGroupCode('', '',accwrapper.acc.IdentifierValue);
                    //if(ExchangeGroupCode!=null)                                            
                   // addressDet.Billing_Exchange_Group_Code__c = ExchangeGroupCode;
                    
                    if(Test_Factory.GetProperty('IsTest') == 'yes') {
                        return null;                      
                    }
                    else {
                       
                        update addressDet;
                        Return ReDirect();
                    }    
        } 
        }
        
        
        
        return null;
    
    }
    
    public List<SalesforceCRMService.CRMAddress> getselectedAddress()
    {
        if(selectedAddress.size()>0){
            return selectedAddress;
        }
        else
        return null;
    }    
    

     
    public with sharing class Addresswrapper
    {
        public SalesforceCRMService.CRMAddress acc {get; set;} //singhd62
        public Boolean selected {get; set;}
        public Addresswrapper(SalesforceCRMService.CRMAddress a) //singhd62
        {
            acc = a;
            selected = false;
        }
    }
}