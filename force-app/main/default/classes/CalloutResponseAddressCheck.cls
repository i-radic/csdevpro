global class CalloutResponseAddressCheck extends csbb.CalloutResponseManagerExt {
// We must implement this class. However for simple address check there is no pre/post check 
// processing of data so a skeleton suffices.

	global CalloutResponseAddressCheck (Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {
		// Standard constructor
		this.setData(mapCR, productCategory, productResponse);
	}
	global CalloutResponseAddressCheck () {
		// Standard constructor
	}
	global void setData(Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {
		// Skeleton method, does nothing in this case
	}
	global Map<String, Object> getDynamicRequestParameters (Map<String, Object> inputMap) {
		// Skeleton method, does nothing in this case, just pass raw input data on
		return new Map<String, Object>();
	}
	global Map<String, Object> processResponseRaw (Map<String, Object> inputMap) {
		// Skeleton method, does nothing, just pass raw output data on
		return new Map<String, Object>();
	}
	global void runBusinessRules (String categoryIndicator) {
		// Skeleton method, does nothing
	}
	global csbb.Result canOffer (Map<String, String> attMap, Map<String, String> responseFields, csbb.CalloutProduct.ProductResponse productResponse) {
		// Skeleton method, does nothing
		return null;
	}
}