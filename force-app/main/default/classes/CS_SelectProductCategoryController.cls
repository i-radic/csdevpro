public class CS_SelectProductCategoryController {

    public Id basketId { get; set; }
    public Id categoryId { get; set; }
    public Id definitionId { get; set; }
    
    public string paramRetURL { get; set; }
    public string containerType { get; set; }

    public string buttonClass { get { return 'btn'; } }
    public string cancelURL { 
        get { 
            return '/' + (this.basketId == null ? '#' : this.basketId); 
        } 
    }
    public string buttonStyle { get { return 'padding: 4px 3px; text-decoration: none'; } }
    public string selcat_Cancel { get { return 'Cancel'; } }

    public Map<Id, cscfga__Product_Category__c> categoriesMap { get; set; }
    public cscfga__Product_Category__c category { get; set; }
    
    public List<cscfga__Product_Category__c> categories {
        get {
            if (this.categoriesMap == null) {
                return new List<cscfga__Product_Category__c>();
            }
            else {
                return this.categoriesMap.values();
            }
        }
    }

    public Map<Id, cscfga__Product_Definition__c> productsMap { get; set; }
    public cscfga__Product_Definition__c product { get; set; }
    
    public List<cscfga__Product_Definition__c> products {
        get {
            if (this.productsMap == null) {
                return new List<cscfga__Product_Definition__c>();
            }
            else {
                return this.productsMap.values();
            }
        }
    }

    public Map<Id, cscfga__Configuration_Offer__c> offersMap { get; set; }
    public cscfga__Configuration_Offer__c offer { get; set; }
    
    public List<cscfga__Configuration_Offer__c> offers {
        get {
            if (this.offersMap == null) {
                return new List<cscfga__Configuration_Offer__c>();
            }
            else {
                return this.offersMap.values();
            }
        }
    }

    /*public Map<Id, cscfga__Product_Definition__c> packagesMap { get; set; }
    public cscfga__Product_Definition__c cpackage { get; set; }
    
    public List<cscfga__Product_Definition__c> packages {
        get {
            if (this.packagesMap == null) {
                return new List<cscfga__Product_Definition__c>();
            }
            else {
                return this.packagesMap.values();
            }
        }
    }*/

    public Boolean opportunityIsClosed { get; set; }
    public Boolean showLabel { get; set; }
    public Boolean isFromOffer { get; set; }
    public Boolean isForPackageSlot { get; set; }

    public integer numCategories { get; set; }
    public integer numProducts { get; set; }
    public integer numOffers { get; set; }
    public integer numOfPackages { get; set; }
    
    private cscfga__Product_Basket__c productBasket { get; set; }
    @TestVisible
    private cscfga.SessionManager.SessionInfo session { get; set; }
    
    public string returnUrlFull { get; set; }
    public string baseURL { 
        get {
            return URL.getSalesforceBaseUrl().toExternalForm();
        
        }
    }
    
    public string PageURL { 
        get {
            return ApexPages.currentPage().getUrl();
        
        }
    }
    
    public string redirectTo { get; set; }

    public CS_SelectProductCategoryController() {
        
        if (String.isEmpty(this.basketId)) {
            this.basketId = ApexPages.currentPage().getParameters().get('linkedId');
        }
        
        if (String.isEmpty(this.paramRetURL)) {
            this.paramRetURL = ApexPages.currentPage().getParameters().get('retURL');
        }

        if (String.isEmpty(this.containerType)) {
            this.containerType = ApexPages.currentPage().getParameters().get('containerType');
        }

        this.returnUrlFull = this.baseURL + this.PageURL;

        opportunityIsClosed = false;
        showLabel = true;
        isFromOffer = false;
        isForPackageSlot = false;
    }

    public void init() {

        this.productBasket = new cscfga__Product_Basket__c(id = this.basketId);
        this.session = cscfga.SessionManager.getSessionInfo('', productBasket);
        
        InitCategories();
        InitProducts();
        InitOffers();
        
        // Commented out for now Packages will be needed in the future
        // InitPackages();
        this.numOfPackages = 0;
    }

    public PageReference onSelectProductCategory() {
        InitProducts(this.categoryId);
        
        if (this.categoryId != null) {
            
            this.category = categoriesMap.get(this.categoryId);
            
            // Hide categories 
            this.categoriesMap = new Map<Id, cscfga__Product_Category__c>();
            this.numCategories = 0;
            
            InitOffers(this.categoryId);
        }
        
        return null;
    }
    
    public PageReference onGoToConfigurator() {

        if(this.categoryId != null) {
            this.redirectTo = String.format('/apex/cscfga__ConfigureProduct?categoryId={0}&containerType={1}&definitionId={2}&linkedId=%2F{3}&retURL=%2F{3}',
                                        new string[] { this.categoryId, this.containerType, this.definitionId, this.BasketId });
        } 
        else {
            this.redirectTo = String.format('/apex/cscfga__ConfigureProduct?containerType={0}&definitionId={1}&linkedId=%2F{2}&retURL=%2F{2}',
                            new string[] { this.containerType, this.definitionId, this.BasketId });
        }

        return new PageReference(this.redirectTo);
    }
    
    public PageReference onGoBack() {
        this.categoryId = null;
        this.category = null;

        return new PageReference(this.returnUrlFull);
    }
    
    public PageReference onCancel() {
        return new PageReference('/' + this.BasketId);
    }
    
    public void InitCategories() {
        
        this.categoriesMap = cscfga.API_1.getEligibleCategories(new cscfga__Product_Category__c(), this.session);
        SetupCategories();
    }

    private void SetupCategories() {
        if (this.categories.size() > 0) {
            // this.category = this.categories[0];
            
            this.numCategories = this.categories.size();
        }
        else {
            this.numCategories = 0;
        }
    }
    
    public void InitProducts() {

        this.productsMap = cscfga.API_1.getEligibleProductDefinitions(new cscfga__Product_Category__c(), this.session);
        SetupProducts();
    }
    
    public void InitProducts(Id catId) {

        this.productsMap = cscfga.API_1.getEligibleProductDefinitions(new cscfga__Product_Category__c(Id = catId), this.session);
        SetupProducts();
    }
    
    private void SetupProducts() {
        if (this.products.size() > 0) {
            // this.product = this.products[0];
            
            this.numProducts = this.products.size();
        }
        else {
            this.numProducts = 0;
        }
    }
    
    public void InitOffers() {

        this.offersMap = cscfga.API_1.getEligibleOffers(new cscfga__Product_Category__c(), this.session);
        SetupOffers();
    }
    
    public void InitOffers(Id catId) {

        this.offersMap = cscfga.API_1.getEligibleOffers(new cscfga__Product_Category__c(Id = catId), this.session);
        SetupOffers();
    }
    
    private void SetupOffers() {
        if (this.offers.size() > 0) {
            this.offer = this.offers[0];
            
            this.numOffers = this.offers.size();
        }
        else {
            this.numOffers = 0;
        }
    }
    
    /*public void InitPackages() {

        this.packagesMap = cscfga.API_1.getEligibleProductDefinitions(new cscfga__Product_Category__c(), this.session);
        
        if (this.packages.size() > 0) {
            this.cpackage = this.packages[0];
            
            this.numOfPackages = this.packages.size();
        }
        else {
            this.numOfPackages = 0;
        }
    }*/
}