/**************************************************************************************************************************************************
Class Name : CreateMultipleActionRecordsApex
VisualForce Page : CreateMultipleActionRecords
Description : Code to Create multiple Customer Experience Action records on the go.
Version : V0.1
Created By Author Name : BALAJI MS
Date : 09/02/2018
Modified Date : 09/02/2018
 *************************************************************************************************************************************************/
@isTest(seealldata=true)
private class CreateMultipleActionRecordsApexTest {

    static testMethod void CreateMultipleActionTest(){
         User thisUser = [select id from User where id=:userinfo.getUserid()];
	     System.runAs( thisUser ){    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
         }
        User userData = Test_Factory.CreateUser();
        userData.Username = 'abc@bt.it';
        insert userData;
        
        Account acc = Test_Factory.CreateAccount();
        Insert acc;
        
        Contact contactData = Test_Factory.CreateContactwithAccount(acc.Id);
        Insert ContactData;
        
        Service_Improvement_Plan__c SIP= Test_Factory.CreateCustomerExperienceActionPlan(acc.Id);
        Insert SIP;
        
        CSAT_Contact_Feedback__c CSAT = Test_Factory.CreateCSATContactFeedback(ContactData.Id);
        CSAT.RecordTypeId = Schema.SObjectType.CSAT_Contact_Feedback__c.getRecordTypeInfosByName().get('MPS Deep Insight').getRecordTypeId();
        Insert CSAT;
        
        Customer_Experience_Actions__c CEA = Test_Factory.CreatecustomerExperience(SIP.Id, CSAT.Id, userData.Id, ContactData.Id);
        insert CEA;
        
        PageReference pageRef = Page.CreateMultipleActionRecords;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(SIP);
        ApexPages.currentPage().getParameters().put('Aid',acc.id);
        ApexPages.currentPage().getParameters().put('Sid',CSAT.id);
              
        CreateMultipleActionRecordsApex.wrapperClass testWrap= new CreateMultipleActionRecordsApex.wrapperClass(25);
        testWrap.CEA = CEA;
        testWrap.checkbox=true;
        testWrap.PlanRecord= SIP;
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(CEA);
        CreateMultipleActionRecordsApex CMAR = new CreateMultipleActionRecordsApex(sc1);
        CMAR.selectedRowIndex= '0';
        CMAR.count= 1;
        CMAR.errorFlag= true;
        CMAR.CsatRec = null;
        CMAR.Add();
        CMAR.addMoreRows();
        CMAR.Cancel();
        CMAR.Del(); 
        CMAR.Save();
              
    }
}