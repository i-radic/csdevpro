@isTest

private class CRController_TEST{
    static testMethod void runPositiveTestCases() {
        // test general feebdack      
        Change_Request__c fbk = new Change_Request__c (Feedback_Title__c = 'Test Feedback', Requested_Priority__c = 'low', Type__c = 'Issue',Description__c = 'Testing' ); 
        Database.Saveresult fbkResult = Database.insert(fbk);    
        // test attachment
        Blob attachBody = Blob.valueOf('attachment body');
        Attachment attach = new Attachment (OwnerId = UserInfo.getUserId(), ParentId = fbk.ID, isPrivate = false, body = attachBody, Name = 'test');
        Database.Saveresult attachmentResult = Database.insert(attach);    
        
        // invoke controller actions               
        CRController controller = new CRController(null);
        controller.addNote();  
        controller.upload();  
        controller.CATcancel();
        controller.CATno();
        controller.CATyes();
        controller.getAtt();
        controller.getNoteList();
    }    
}