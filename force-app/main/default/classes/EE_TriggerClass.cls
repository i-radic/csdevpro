global with sharing class EE_TriggerClass {

    global static Boolean isTriggerEnabled(String psTriggerName) {
        
        Boolean lbReturn = false;
        
        final Map<String,TriggerEnabler__c> lMapTriggerEnablerAll = TriggerEnabler__c.getAll();
        
        if (psTriggerName != null && psTriggerName.trim() != null && psTriggerName.trim().length() > 0 && 
        		lMapTriggerEnablerAll != null && lMapTriggerEnablerAll.get(psTriggerName) != null) {
        	
        	if (lMapTriggerEnablerAll.get(psTriggerName).TriggerEnabled__c == true) {
                    lbReturn = true;
            }
        
        }
        
        return lbReturn ;
    }
}