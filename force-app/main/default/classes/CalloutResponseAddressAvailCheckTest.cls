@IsTest
public class CalloutResponseAddressAvailCheckTest  {
	private static Object jsonObj;
	private static Map<String, Object> testMap;

	private static void createTestData() {
		String jsonString = '{"context":{"calloutServiceMethodType":"availability","calloutServiceMethodName":"AddressAvailabilityCheck"},"httpStatusCode":"200","availabilityResponseRaw":"Test Response Body"}';
		jsonObj = JSON.deserializeUntyped(jsonString);

		testMap = new Map<String, Object>{'Test Map' => jsonObj};
	}

	@IsTest
	public static void testProcessResponseRaw() {
		createTestData();

		CalloutResponseAddressAvailabilityCheck controller = new CalloutResponseAddressAvailabilityCheck();
		Test.startTest();
		Map<String, Object> processedMap = controller.processResponseRaw(testMap);
		Test.stopTest();

		System.assert(processedMap != null);
	}

	@IsTest
	public static void testGetDynamicRequestParameters() {
		createTestData();

		CalloutResponseAddressAvailabilityCheck controller = new CalloutResponseAddressAvailabilityCheck();
		Test.startTest();
		Map<String, Object> processedMap = controller.getDynamicRequestParameters(testMap);
		Test.stopTest();

		System.assert(processedMap != null);
	}

	@IsTest
	public static void testSetData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'Test Category');

		csbb.ProductCategory productCategory = new csbb.ProductCategory(prodCategory.Id);

		csbb.CalloutResponse calloutResponse = new csbb.CalloutResponse();
		Map<String, csbb.CalloutResponse> mapCR = new Map<String, csbb.CalloutResponse> {'Test Map' => calloutResponse};
		
		csbb.CalloutProduct.ProductResponse productResponse = new csbb.CalloutProduct.ProductResponse();
		productResponse.displayMessage = 'Test Message';
		productResponse.available = 'true';

		CalloutResponseAddressAvailabilityCheck controller = new CalloutResponseAddressAvailabilityCheck();
		Test.startTest();
		controller.setData(mapCR, productCategory, productResponse);
		Test.stopTest();
	}

	@IsTest
	public static void testRunBusinessRules() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'Test Category');

		csbb.ProductCategory productCategory = new csbb.ProductCategory(prodCategory.Id);

		csbb.CalloutResponse calloutResponse = new csbb.CalloutResponse();
		Map<String, csbb.CalloutResponse> mapCR = new Map<String, csbb.CalloutResponse> {'Test Map' => calloutResponse};
		
		csbb.CalloutProduct.ProductResponse productResponse = new csbb.CalloutProduct.ProductResponse();
		productResponse.displayMessage = 'Test Message';
		productResponse.available = 'true';

		CalloutResponseAddressAvailabilityCheck controller = new CalloutResponseAddressAvailabilityCheck();
		Test.startTest();
		controller.setData(mapCR, productCategory, productResponse);

		try{
			controller.runBusinessRules('Test Indicator');
		}
		catch(Exception exc){}
		Test.stopTest();
	}

	@IsTest
	public static void testCanOffer() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'Test Category');

		csbb.ProductCategory productCategory = new csbb.ProductCategory(prodCategory.Id);

		csbb.CalloutResponse calloutResponse = new csbb.CalloutResponse();
		Map<String, csbb.CalloutResponse> mapCR = new Map<String, csbb.CalloutResponse> {'Test Map' => calloutResponse};
		
		csbb.CalloutProduct.ProductResponse productResponse = new csbb.CalloutProduct.ProductResponse();
		productResponse.displayMessage = 'Test Message';
		productResponse.available = 'true';

		CalloutResponseAddressAvailabilityCheck controller = new CalloutResponseAddressAvailabilityCheck(mapCR, productCategory, productResponse);
		Test.startTest();
		Map<String, String> attMap = new Map<String, String>();
		Map<String, String> responseFields = new Map<String, String>();
		
		try{
			controller.canOffer(attMap, responseFields, productResponse);
		}
		catch(Exception exc){}
		Test.stopTest();
	}


}