public class SitePINValidationController {

    public static String p { get; set; }
    public static String t { get; set; }
    public static String w { get; set; }
    public static String s { get; set; }

    public SitePINValidationController(){
        p = System.currentPagereference().getParameters().get('p');
        t = System.currentPagereference().getParameters().get('t');
        w = System.currentPagereference().getParameters().get('w');
    }
    
    public PageReference onLoad(){
        Boolean isValid = validateRequest();
        if(!isValid){
            PageReference retURL = new PageReference('http://www.google.com'); // error page redirect
            retURL.setRedirect(true);
            return retURL;
        }
        return null;
    }

    public Boolean validateRequest(){
        CS_Site_PIN__c found = null;
        List<CS_Site_PIN__c> sps = [SELECT Id, Name, Timestamp__c, Hash__c, Target_Email__c FROM CS_Site_PIN__c WHERE Timestamp__c = :EncodingUtil.base64Decode(t).toString()];
        for(CS_Site_PIN__c sp: sps){
            Blob bKey = EncodingUtil.base64Decode(sp.Hash__c);
            String pd = SiteUtilities.decryptString(bKey, EncodingUtil.base64Decode(p));
            String wd = SiteUtilities.decryptString(bKey, EncodingUtil.base64Decode(w));
            if(pd == sp.Target_Email__c && wd == sp.Id){
                found = sp;
                s = EncodingUtil.base64Encode(Blob.valueOf(wd));
            }
        }            
        return found == null ? false : true;
    }
    
    
    @RemoteAction
    public static Map<String, String> requestPIN(String p, String t, String w, String s){
        Blob spId = EncodingUtil.base64Decode(s);
        String PIN = generatePIN();
        CS_Site_PIN__c sp = [SELECT Id, PIN__C, Target_Email__c FROM CS_Site_PIN__c WHERE Id = :spId.toString()];
        sp.PIN__c = PIN;
        update sp;
        // send PIN
        Boolean pinSent = sendPIN(sp);
        return new Map<String, String> { SiteUtilities.PIN_SENT => String.valueOf(pinSent), SiteUtilities.EMAIL =>  sp.Target_Email__c};
    }

    private static String generatePIN(){
        List<Long> PIN = new List<Long>();
        for(Integer i = 0; i < 4; i++){  // 4 digit PIN
            Double random = Math.random()*10;
            Long digit = Decimal.valueOf(random).round(System.RoundingMode.FLOOR);
            PIN.add(digit);
        }
        return String.join(PIN, '');
    }

    private static Boolean sendPIN(CS_Site_PIN__c sp){
        String body = '';
        body += 'Your PIN is: <b>' + sp.PIN__c + '</b>.';
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] { sp.Target_Email__c });
        mail.setHtmlBody(body);
        try {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            return true;
        }catch (Exception e){
            return false;
        }
    }
    
    @RemoteAction
    public static Map<String, String> validatePIN(String pin, String s){
        Map<String, String> returnMap = new Map<String, String>();
        Blob spId = EncodingUtil.base64Decode(s);
        CS_Site_PIN__c sp = [SELECT Id, PIN__C, Target_Email__c, Hash__c, VHash__c, Timestamp__c, Order__c FROM CS_Site_PIN__c WHERE Id = :spId.toString()];
        if(pin == sp.PIN__c){
            returnMap.put(SiteUtilities.IS_VALID, String.valueOf(true));
            List<csord__Service__c> ser = [SELECT Id, csordtelcoa__Product_Basket__c FROM csord__Service__c WHERE csord__Order__c =:sp.Order__c];
            Blob bKey = EncodingUtil.base64Decode(sp.Hash__c);
            Blob vHash = Crypto.generateAesKey(SiteUtilities.ENC_SIZE);
            String validationHash = EncodingUtil.base64Encode(vHash);
            Blob vHashEnc = SiteUtilities.encryptString(bKey, validationHash);
            returnMap.put(SiteUtilities.V_HASH, SiteUtilities.blob2String(vHashEnc));
            returnMap.put(SiteUtilities.TIMESTAMP, EncodingUtil.base64Encode(Blob.valueOf(sp.Timestamp__c)));
            returnMap.put(SiteUtilities.BASKETID, ser.get(0).csordtelcoa__Product_Basket__c);
            sp.VHash__c = validationHash;
            update sp;
        }else{
            returnMap.put(SiteUtilities.IS_VALID, String.valueOf(false));
        }
        
        return returnMap;
    }

}