public class AttributeSyncJSONParser {

    public AttributeSyncJSONParserDataList[] attributeSyncJSONParserDataList;
	public class AttributeSyncJSONParserDataList {
		public String isactive;	//true
		public String attname;	//Contract Term
		public String objectname;	//BT_One_Phone__c
		public String mode;	//AttrbuteSync
		public String fieldname;	//Contract_Term__c
		public String sourcefielddatatype;	//double
		public String lookupFieldonObject;	//Opportunity__c
		public String relationship;	//indirect
		public String relatedObject;	//Opptunity
		public String attlookupForRelatedObject;	//cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c
		public String filedlookupForRelatedObject;	//Opptunity
	}

}