@IsTest
global class CS_BTNetPortSpeedCustomLookup_Test {

    private static void getRequiredAttributes_Test() {
      String result;
      Test.startTest();
      CS_BTNetPortSpeedCustomLookup appDirectProductLookup = new CS_BTNetPortSpeedCustomLookup();
      result = appDirectProductLookup.getRequiredAttributes();
      Test.stopTest();
      System.assertEquals('["Contract Term","Geography","Bearer Speed"]', result);
  }
    
     static testMethod void CustomAddOnLookupTest() {
      Map<String, String> searchFieldsMap = new Map<String, String>();
      searchFieldsMap.put('Bearer Speed', '1000');
      searchFieldsMap.put('Contract Term', '36');
      searchFieldsMap.put('Geography', 'Standard');
      String prodDefinitionID;
      Id[] excludeIds;
      Integer pageOffset;
      Integer pageLimit=1;

      CS_TestDataFactory.insertTriggerDeactivatingSetting();

        Test.StartTest();
        CS_BTNetPortSpeedCustomLookup customLookup = new CS_BTNetPortSpeedCustomLookup ();
        customLookup.getRequiredAttributes();
        Object[] addOnsResults = customLookup.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);

        System.assertNotEquals(null, addOnsResults);    
        Test.StopTest();
     }
    
}