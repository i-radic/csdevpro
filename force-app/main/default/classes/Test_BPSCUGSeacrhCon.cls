/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = True)
public class Test_BPSCUGSeacrhCon {
	
    public static testMethod void UnitTest1(){
         
        //Profile P =[select id from Profile where Name = 'BPS Standard User'];
        
        User U = new User();
        U.LastName='test';
        U.FirstName='test1';
        U.Email='test@bt.it';
        U.UserName='test@btconnect.com';
        U.Alias='tes1';
        U.Division='BPS';
        U.Department='Partner1';
        U.TimeZoneSidKey = 'Europe/London';
        U.LocaleSidKey = 'en_GB';
        U.ProfileId ='00e200000015zm6AAA'; //p.id;
        U.EmailEncodingKey='UTF-8';
        U.LanguageLocaleKey='en_US';
        U.Work_Area__c = 'Central';
        U.EIN__c = 'einpart1';
        U.CommunityNickname = 'testabc';
        
        Insert U;
        
        System.RunAs(U){
            
            Account A = Test_Factory.CreateAccount();
            A.Name = 'test2'; 
            A.Sector__c = U.Division;
            A.Sub_sector__c = U.Department; 
            A.OwnerId = U.id;
            A.CUG__c = 'test';
            A.Phone = '12345';
            Insert  A;
           
                       
            BPSCUGSeacrhCon BPS = new BPSCUGSeacrhCon();
           
            BPS.RetriveCUG();
            BPS.AccountTel.Phone = '1234';
            BPS.RetriveCUG();
            BPS.CUGID = A.CUG__c;
            BPS.RetriveCUG();
            BPS.CancelAccount();
            Account AA = Test_Factory.CreateAccount();
            AA.Name = 'test2'; 
            AA.Sector__c = U.Division;
            AA.Sub_sector__c = U.Department; 
            AA.OwnerId = U.id;
            AA.CUG__c = 'CUG097563';
            AA.Phone = '12345';
            BPS.AccountTel = AA;
			//Insert BPS.AccountTel;            
            BPS.SaveAccount();            
            
          
           
        }
    }
       
}