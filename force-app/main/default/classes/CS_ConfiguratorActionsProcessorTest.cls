@isTest
private class CS_ConfiguratorActionsProcessorTest {
	
	@isTest static void test_method_one() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.setupNoTriggersFlag();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account acc = CS_TestDataFactory.generateAccount(True,'TestingAccount');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(True, 'TestOpp',acc);
        cscfga__Product_Basket__c bskt = CS_TestDataFactory.generateProductBasket(True, 'TestBskt',opp);
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(True,'BT Mobile');
        
         cscfga__Attribute_Definition__c attrDef1 =
            new cscfga__Attribute_Definition__c
                ( Name = 'ProductActionConditions'
                , cscfga__Product_Definition__c = prodDef.Id );
        insert attrDef1;

        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false,'BT Mobile Flex', bskt);
        pc.cscfga__Product_Definition__c = prodDef.Id;
        pc.cscfga__Configuration_Status__c = 'Valid';
        pc.Calculations_Product_Group__c = 'Future Mobile';
        insert pc;

        Map<String, Set<Id>> configsByActions = new Map<String, Set<Id>>{
        	'invalidate' => new Set<Id>{pc.Id}, 
        	'none' => new Set<Id>(), 
        	'revalidate' => new Set<Id>()
        };
		System.enqueueJob(new CS_ConfiguratorActionsProcessor(configsByActions));
	}
}