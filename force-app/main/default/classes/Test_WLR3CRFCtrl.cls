@isTest
private class Test_WLR3CRFCtrl {
    static testMethod void myUnitTest() {
        //  set up data
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99991';
        insert a;
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o; 
        CRF__c crf = new CRF__c(Opportunity__c = o.Id);
        crf.RecordTypeId = [Select Id from RecordType where DeveloperName='WLR3_Authorisation'].Id;
        crf.Contact__c = c.Id;
        insert crf;
        
        Numbers_to_be_Transferred__c pd = new Numbers_to_be_Transferred__c();
        pd.Related_To_CRF__c = crf.id;
        insert pd;
        WLR3CRFCtrl myController = new WLR3CRFCtrl(new ApexPages.StandardController(pd));
        myController.getProdList();
        myController.addProd();
        
    }

}