public with sharing class updateCampaignLastContacted {  

    public list<Campaign> updateCampaign(set<id> campaignids){    
        //Take a list of a account id's and return a list of accounts    
        List<Campaign> campaignlist = new List<Campaign>();    
        campaignlist = [select Last_Outcome_Date__c from Campaign where id in :campaignids];        
        for (Campaign c:campaignlist){      
            c.Last_Outcome_Date__c = date.today();    
        }    
        Return campaignlist;  
    }
}