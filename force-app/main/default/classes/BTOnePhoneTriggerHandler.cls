/******************************************************************************************************
Name : BTOnePhoneTriggerHandler 
Description : Trigger Handler of BT_One_Phone__c Object

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    01/02/2016                 Praveen	                        Class created         
*********************************************************************************************************/
public class BTOnePhoneTriggerHandler {
    
    public static boolean BTOnePhoneTriggerAfterHandleRecusion = True;
    public static boolean BTOnePhoneTriggerBeforeHandleRecusion = True;
    
    public static void beforeUpdate(map<Id,BT_One_Phone__c> newBTOnePhoneMap ,map<Id,BT_One_Phone__c> oldBTOnePhoneMap){
        BTOnePhoneHelper.updateBTOnePhoneDetailsBeforeUpdate(newBTOnePhoneMap, oldBTOnePhoneMap);
    }
    // commented as part of CR9917 -start
    /* public static void afterUpdate(map<Id,BT_One_Phone__c> newBTOnePhoneMap ,map<Id,BT_One_Phone__c> oldBTOnePhoneMap){
         BTOnePhoneHelper.updateContractDurationonBTOnePhoneSites(newBTOnePhoneMap, oldBTOnePhoneMap);
    } */
    // commented as part of CR9917 -end
}