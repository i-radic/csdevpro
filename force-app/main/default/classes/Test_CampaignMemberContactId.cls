@isTest

private class Test_CampaignMemberContactId{

    static testMethod void testCMContactId(){
        // Create Account
        Account a = Test_Factory.CreateAccount();
        insert a;
        
        // Create Contact
        Contact contact = Test_Factory.CreateContact();
        contact.Phone = '01234 567890';
        contact.AccountId = a.Id;
        insert contact;
        
        Campaign c;
        c = new Campaign(Name='TestCampaign', Campaign_Type__c='BTLB', Type='Telemarketing', Status='Planned',X_Day_Rule__c = 3,
         isActive=true, Data_Source__c='Self Generated');
        insert c;
        
        CampaignMember cm = new CampaignMember(CampaignId=c.Id, ContactId=contact.Id, Unsuccessful_Call_Attempts__c = 1);
    insert cm;
    
        cm.Unsuccessful_Call_Attempts__c = 2;
        update cm;
    }
}