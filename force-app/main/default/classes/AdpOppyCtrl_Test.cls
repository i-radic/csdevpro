@isTest
private class AdpOppyCtrl_Test {

    static testMethod void firstTest() {
        Test_Factory.SetProperty('IsTest', 'yes');
		USer thisUser = [select id from User where id=:Userinfo.getUserId()];
        system.runAs(thisUser){
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
            
		 TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
     
     upsert settings TriggerDeactivating__c.Id;
        Account accnt = Test_Factory.CreateAccount();
        accnt.name = 'TESTCODE 2';
        accnt.sac_code__c = 'testSAC';
        accnt.Sector_code__c = 'CORP';
        accnt.LOB_Code__c = 'LOB';
        accnt.OwnerId = u1.Id;
        accnt.CUG__c='CugTest';
        Database.SaveResult accountResult = Database.insert(accnt);

        ADP__c thisADP = new ADP__c(Customer__c = accountResult.Id);
        Database.SaveResult[] adpRes = Database.insert(new ADP__c[] {thisADP});

		Opportunity thisOpp = Test_Factory.CreateOpportunity(accountResult.Id);
        Database.SaveResult[] oppRes = Database.insert(new Opportunity[] {thisOpp});

        PageReference pageRef = Page.ADPOppertunities;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', thisADP.Id);

        ApexPages.StandardController stc = new ApexPages.StandardController(thisADP);
        AdpOppyCtrl ADPOpp = new AdpOppyCtrl(stc);

        ADPOpp.getOpportunities();
    }
    }
}