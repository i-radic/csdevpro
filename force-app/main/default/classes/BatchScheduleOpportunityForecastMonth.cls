global class BatchScheduleOpportunityForecastMonth implements Schedulable{
   global void execute(SchedulableContext sc) {
    Date myDate = (Date.today()- 3);
    String sDate = String.valueOf(myDate);
        String qString = 'select Close_Date_Fiscal_Month__c,Net_Acv__c,SOV_GM__c, zCurrent_Fiscal_Month__c, zCurrent_Month_Forecast_ACV__c, ACV_Calc__c, zCurrent_Month_Forecast_NIBR__c, NIBR_Next_Year__c, zCurrent_Month_Forecast_Vol__c, zCurrent_Month_Forecast_CY_NIBR__c, NIBR_Current_Year__c, Current_Month_Forecast_Amount__c, Amount, ForecastUpdated__c from opportunity where zToProcessInForecastBatchMonth__c = \'PROCESS\'and ( ForecastUpdatedMonth__c < '+ sDate +' or ForecastUpdatedMonth__c  = null) ';
        batchOpportunityForecastMonth b = new batchOpportunityForecastMonth(qString);
        database.executebatch(b, 50);
    }
}