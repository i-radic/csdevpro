/**
 * Project Name..........: Everything Everywhere
 * File..................: TrialCollectionHelper.cls
 * Version...............: 1 
 * Created by............: Martin Eley
 * Created Date..........: 20th September 2012
 * Last Modified by......:    
 * Last Modified Date....:   
 * Description...........: Provides methods for processing Trial Collections
 */
public with sharing class TrialCollectionHelper {
	/**
	 * Method to send email to delivery company to arrange trial collection
	 * 
	 * @param List<Trial_Collection__c> List of Trial Collections to be processed
	 */
	public void SendCollectionRequestEmail(List<Trial_Collection__c> trialCollections){
		//Get recipient email address from custom setting
		String emailAddress = [SELECT Email_Addresses__c
			FROM Trial_Collection_Courier__c
			WHERE Active__c = TRUE LIMIT 1].Email_Addresses__c;
		
		string[] toEmailAddresses = emailAddress.replace(' ', '').split(','); 
				
		//Reserve email capacity
	 	Messaging.reserveSingleEmailCapacity(trialCollections.size());
	 	
	 	//List of messages to send
	 	List<Messaging.SingleEmailMessage> singleEmailMessages = new List<Messaging.SingleEmailMessage>();
		
		//Create email for each
		for(Trial_Collection__c aTrialCollection : trialCollections){
			Messaging.SingleEmailMessage aSingleEmailMessage = new Messaging.SingleEmailMessage();
			
			//Add CC address
			String[] ccAddresses = new String[] {'trial.request@ee.co.uk.cat'};
			aSingleEmailMessage.setCcAddresses(ccAddresses);
						
			//Add to address
			//String[] toAddress = new String[] {emailAddress};
			aSingleEmailMessage.setToAddresses(toEmailAddresses);			
			
			Trial__c trialTemp = [Select name from Trial__c where id =: aTrialCollection.Trial__c limit 1];
			
			//Set body and subject - Can't use template as sending to unknown contact
			aSingleEmailMessage.setSubject('EE - Collection Request - ' + trialTemp.Name);
			
			String body = 'Trial Collection Request \r\n'
				+ 'Contact Name:              ' + aTrialCollection.Contact_Name__c + '\r\n';
			
			if (aTrialCollection.Contact_Telephone_Number__c != null)	
				body += 'Contact Telephone Number: ' + aTrialCollection.Contact_Telephone_Number__c + '\r\n';
			else
				body += 'Contact Telephone Number: ' + '\r\n';
			
			if (aTrialCollection.Opening_Hours__c != null)
				body += 	+ 'Opening Hours:             ' + aTrialCollection.Opening_Hours__c + '\r\n';
			else
				body += 	+ 'Opening Hours:             ' + aTrialCollection.Opening_Hours__c + '\r\n';

			body += 'Special Instructions:        ' + aTrialCollection.Special_Instructions__c + '\r\n'
				+ 'Collection Address:         ' + aTrialCollection.Collection_Address__c + '\r\n';
			
			if (aTrialCollection.Collection_City__c != null)
				body += 'Collection City:            ' + aTrialCollection.Collection_City__c + '\r\n';
			else
				body += 'Collection City:            ' + '\r\n';
			
			body += 'Collection Postcode:        ' + aTrialCollection.Collection_Postcode__c;

			aSingleEmailMessage.setPlainTextBody(body);
			
			//Add to list for sending
			singleEmailMessages.add(aSingleEmailMessage);
		}
		
		//Send
		Messaging.sendEmail(singleEmailMessages);
	}
}