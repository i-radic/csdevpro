@isTest(SeeAllData=true)
private class Test_LeaseDeskAccountMatchController {
    static testMethod void UnitTest() {
        RunTest('Yes', '01');
        RunTest('No', '02'); 
    }
    
    static void RunTest(String isTest, String uniqueVal) {
        Test_Factory.SetProperty('IsTest', isTest);
        
        system.debug('-------------------------- Test_LeaseDeskAccountMatchController --------------');

        // create user
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'BTLB: Admin User']; 
        
        User u1 = new User();
        //System.RunAs(u1){
        u1.Username = 'test9874' + uniqueVal + '@bt.com';
        u1.Ein__c = '9876543' + uniqueVal;
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.ProfileId = prof.Id;
        u1.TimeZoneSidKey = 'Europe/London'; 
        u1.LocaleSidKey = 'en_GB';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'en_US';
        u1.Apex_Trigger_Account__c = false; 
        u1.Run_Apex_Triggers__c = false;
        insert u1; 

        // create an account
        Account a1 = Test_Factory.CreateAccount();
        a1.Name = 'LD Account Test';
        a1.OwnerId = U1.Id;
        a1.Postcode__c = 'n16 9al';
        //a1.LeaseDeskID__c = 'TT12345';
        a1.BTLB_Common_Name__c = 'BTLB Bath and Bristol';
        a1.LOB__c = 'BTLB Bath and Bristol';
        a1.Sector__c = 'BT Local Business';
        a1.Sub_Sector__c = 'BTLB Bath and Bristol';
        a1.Sector_Code__c = 'BTLB';
        insert a1;
        
        
        // create a standard opportunity
        Opportunity opp = Test_Factory.CreateOpportunity(a1.Id);
        opp.RecordTypeId = '01220000000PjDu';       // Standard record type
        opp.Type ='Sale';
        upsert opp; 
       
        //get standard pricebook
        Pricebook2  standardPb = [select id, name, isActive from Pricebook2 where IsStandard = true limit 1];

        Pricebook2 pbk1 = new Pricebook2 (Name='Test Pricebook Entry 1',Description='Test Pricebook Entry 1', isActive=true);
        insert pbk1;

        Product2 prd1 = new Product2 (Name='Test Product Entry 1',Description='Test Product Entry 1',productCode = 'ABC', isActive = true);
        insert prd1;


        PricebookEntry pbe1 = new PricebookEntry (Product2ID=prd1.id,Pricebook2ID=standardPb.id,UnitPrice=50, isActive=true);
        insert pbe1;
        
        // add outright product to oppty
        OpportunityLineItem oli = Test_Factory.CreateOpportunityLineItem(pbe1.Id, opp.Id);
        oli.Sales_Type__c = 'New Sale';
        oli.Contract_Term__c = '0 Months';
        oli.Billing_Cycle__c = 'Outright Sale';
        insert oli;
        a1.Sub_Sector_Code__c = 'LD';
        a1.LOB_Code__c = 'LD';
        update a1;
      
        //Test.StartTest();
        PageReference pageRef = Page.LeaseDeskAccountMatch;
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController stdController = new ApexPages.StandardController(opp);
        ApexPages.currentPage().getParameters().put('id', opp.Id);
        LeaseDeskAccountMatchController controller = new LeaseDeskAccountMatchController(stdController);
        try{PageReference Submit = controller.Submit();}catch (Exception e){System.assertEquals('Unable to retrieve object' ,e.getMessage());}
        PageReference back = controller.back();
        //Test.StopTest();
       //}
    }
}