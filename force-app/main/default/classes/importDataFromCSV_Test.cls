@isTest
public with sharing class importDataFromCSV_Test {

    private static testMethod void testSuccessfulImport() {

        importDataFromCSVController controller = new importDataFromCSVController();
        controller.csvFileBody= Blob.valueOf('Quantity,Description,SC,Initial charges,Recurring charges,Discount,CC,Min Period months,Product Code (BT),Category Type\n1,IPO R11 4 x 6 Digital (BRI4) Essential PRM,X,1.362.01,,0,,,B50438,Switch');
        controller.importCSVFile();
        system.assertEquals(controller.returnMapString!=null, true);
    }
}