@isTest(SeeAllData=true)
private class Test_LeaseDeskUpdateLeasingController {

    static testMethod void UnitTest() {
        RunTest('01', '1');
    }
    
    static void RunTest(String uniqueVal, String leaseDeskId) {
        
        // create user
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'BTLB: Admin User']; 
        
        User u1 = new User();
        u1.Username = 'test9874' + uniqueVal + '@bt.com';
        u1.Ein__c = '9876543' + uniqueVal;
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.ProfileId = prof.Id;
        u1.TimeZoneSidKey = 'Europe/London'; 
        u1.LocaleSidKey = 'en_GB';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.LanguageLocaleKey = 'en_US';
        u1.Apex_Trigger_Account__c = false; 
        u1.Run_Apex_Triggers__c = false;
        insert u1; 

        // create an account
        Account a1 = Test_Factory.CreateAccount();
        a1.Name = 'LD Account Test ' + uniqueVal;
        a1.OwnerId = U1.Id;
        a1.Postcode__c = 'n16 9al';
        a1.BTLB_Common_Name__c = 'BTLB Bath and Bristol';
        a1.LOB__c = 'BTLB Bath and Bristol';
        a1.Sector__c = 'BT Local Business';
        a1.Sub_Sector__c = 'BTLB Bath and Bristol';
        a1.Sector_Code__c = 'BTLB';
        //a1.LeaseDeskID__c = leaseDeskId;
        insert a1;
        
        // create a standard opportunity
        Opportunity opp = Test_Factory.CreateOpportunity(a1.Id);
        opp.RecordTypeId = '01220000000PjDu';       // Standard record type
        opp.Type ='Sale';
        upsert opp; 
        
        Leasing_History__c lhistory = new Leasing_History__c();
        //lhistory.AgreementID__c = '1';
        lhistory.Funder__c = 'TEST';
        lhistory.Opportunity__c = opp.Id;
        insert lhistory;
        
        LeaseDeskAPI.LeasingDetails lDetails = new LeaseDeskAPI.LeasingDetails();
        lDetails.leaseDeskProposalID = '1';
        lDetails.salesforceOpptyID = opp.Id;
        lDetails.salesforceOID = opp.Id;
        lDetails.status = 'Live';
        lDetails.periodsRemaining = 1;
        lDetails.settlement = 100;
        lDetails.product = 'TEST';
        lDetails.leaseDeskCompanyName = 'TEST';
        lDetails.cugID = 'TEST';
        lDetails.funderCreditLimit = 200;
        lDetails.poValue = 500;
        lDetails.funderURL = 'TEST';
        lDetails.leaseDeskURL = 'TEST';
        lDetails.funder = 'TEST';
        lDetails.term = 2;
        lDetails.rbo = 2;
        lDetails.regularPayment = 500;
        lDetails.frequency = 'Monthly';
        lDetails.ragDate = Date.ValueOf('2013-11-27');
        lDetails.ragStatus = 'Amber';
        
        List<LeaseDeskAPI.LeasingDetails> ldLeasingHistoryList = new List<LeaseDeskAPI.LeasingDetails>();
        ldLeasingHistoryList.Add(lDetails);
              
        List<Leasing_History__c> sfLeasingHistoryList = new List<Leasing_History__c>();
        sfLeasingHistoryList.Add(lHistory);
        
        ApexPages.currentPage().getParameters().put('id', a1.Id);        
        ApexPages.StandardController stdController = new ApexPages.StandardController(a1);
        LeaseDeskUpdateLeasingController updateLeasing = new LeaseDeskUpdateLeasingController(stdController);
        updateLeasing.salesForceLeasingHistory = sfLeasingHistoryList;
        updateLeasing.leaseDeskLeasingHistory = ldLeasingHistoryList;
        updateLeasing.account = a1;
        updateLeasing.OnLoad();
        updateLeasing.back();        
        updateLeasing.QuarterlyPayment('Quarterly', 500);
        updateLeasing.QuarterlyPayment('Annually', 500);
        updateLeasing.QuarterlyPayment('Two Monthly', 500);
        updateLeasing.QuarterlyPayment('Four Monthly', 500);
        updateLeasing.QuarterlyPayment('Six Monthly', 500);
        updateLeasing.StatusToRAGMapping('Prospect.Preclearance');
        updateLeasing.StatusToRAGMapping('Prospect.Acceptance');
        updateLeasing.StatusToRAGMapping('Prospect.Rejection');

    }
}