/* *************************************************************************************************************    
Class Name    : CustomObjectFieldSyncToAttribute
Description   : Utility Call for Attribte Sync                     
CreatedDate   : 14-OCt-2018       
Version       : 1.0     
Author        : TCS(Sunny Kashyap)
-----------------------  Revision History -------------------

Sno      Version      Modification Done     Modified By    Modified Date                  
 1.
 2.
*****************************************************************************************************************/
public class CustomObjectFieldSyncToAttribute {
     /************************************************************************************************
     * CR10478
    Method Name            : doObjectAttributeSync
    Input Parameters       : list of attobject Id where reverseSyncrequired is true
    Return Type            : void
    Method Description     : This Method will be called frorm Process Builder on any object witch fileds value needs yo be updated on Attribute
    ***********************************************************************************************/
    @invocableMETHOD
    public static void doObjectAttributeSync(List<id> sObjectIdList){
        String sObjectName = (sObjectIdList[0].getSObjectType().getDescribe().getName()).tolowercase();
        //String JSON_SRC='{\r\n\"attributeSyncJSONParserDataList\":[{\r\n\"isactive\":\"true\",\r\n\"attname\": \"Contract Term\",\r\n\"objectname\": \"BT_One_Phone__c\",\r\n\"mode\": \"FieldSync\",\r\n\"fieldname\": \"Contract_Term__c\",\r\n\"sourcefielddatatype\": \"double\",\r\n\"lookupFieldonObject\": \"Opportunity__c\",\r\n\"relationship\":\"indirect\",\r\n\"relatedObject\": \"Opptunity\",\r\n\"attlookupForRelatedObject\": \"cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c\",\r\n\"filedlookupForRelatedObject\": \"Opptunity\"\r\n},\r\n{\r\n\"isactive\":\"true\",\r\n\"attname\": \"Contract Term\",\r\n\"objectname\": \"Opportunity\",\r\n\"mode\": \"FieldSync\",\r\n\"fieldname\": \"Contract_Number__c \",\r\n\"sourcefielddatatype\": \"string\",\r\n\"lookupFieldonObject\": \"id\",\r\n\"relationship\":\"direct\",\r\n\"relatedObject\": \"Opptunity\",\r\n\"attlookupForRelatedObject\": \"cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c\",\r\n\"filedlookupForRelatedObject\": \"Opptunity\"\r\n}]\r\n}';
        String JSON_SRC=AttributeRevSyncUtility.getStaticResourceByName('AttSyncJson');
        Map<string, string> attNameLookupForRelatedObjectMap = new Map<string, string>();
        Set<String> attlookupFieldForRelatedObjectSet = new Set<String>();
        Set<String> attNameSet = new Set<String>();
        Map<String,String> ojectNameAttLookupFieldMap = new Map<String,String>();
        
        Set<String> fieldNameSet = new Set<String>();
        
        Map<String,String> attNameFieldNameMap = new Map<String,String>();
        Map<String,String> filedNamedataTypeMap = new Map<String,String>();
        Map<String,String> ojectNameLookupFieldMap = new Map<String,String>();
        
        Map<string, sobject> relatedObjectIDSObjectDatamap = new Map<string, sobject>();
        try{
            
            AttributeSyncJSONParser attributeSyncJSONParser =  (AttributeSyncJSONParser)System.JSON.deserialize(JSON_SRC, AttributeSyncJSONParser.class);
            
            List <AttributeSyncJSONParser.AttributeSyncJSONParserDataList> attributeSyncJSONParserDataList = attributeSyncJSONParser.attributeSyncJSONParserDataList;
            System.debug('attributeSyncJSONParserDataList>>>'+attributeSyncJSONParserDataList);
            System.debug('sObjectName>>>'+sObjectName);
           
            for(AttributeSyncJSONParser.AttributeSyncJSONParserDataList attributeSyncJSONParserObj : attributeSyncJSONParserDataList){
                 System.debug('attributeSyncJSONParserObj.objectname>>>'+attributeSyncJSONParserObj.objectname);
                  System.debug('attributeSyncJSONParserObj.mode>>>'+attributeSyncJSONParserObj.mode);
                if(attributeSyncJSONParserObj.isactive.equalsIgnoreCase('true') && attributeSyncJSONParserObj.objectname.equalsIgnoreCase(sObjectName) && attributeSyncJSONParserObj.mode.equalsIgnoreCase('FieldSync')){
                    //attlookupFieldForRelatedObjectSet.add(attributeSyncJSONParserObj.attlookupForRelatedObject.trim().tolowercase());
                    attNameSet.add(attributeSyncJSONParserObj.attname.trim().tolowercase());
                    ojectNameAttLookupFieldMap.put(attributeSyncJSONParserObj.objectname.trim().tolowercase(),attributeSyncJSONParserObj.attlookupForRelatedObject.trim().tolowercase());
                    attNameFieldNameMap.put(attributeSyncJSONParserObj.attname.trim().tolowercase(),attributeSyncJSONParserObj.fieldname.trim().tolowercase());  
                    
                    filedNamedataTypeMap.put(attributeSyncJSONParserObj.fieldname.trim().tolowercase(),attributeSyncJSONParserObj.sourcefielddatatype.trim().tolowercase());  
                    fieldNameSet.add(attributeSyncJSONParserObj.fieldname.trim().tolowercase());
                    fieldNameSet.add(attributeSyncJSONParserObj.fieldname.trim().tolowercase());
                    
                    ojectNameLookupFieldMap.put(attributeSyncJSONParserObj.objectname.trim().tolowercase(),attributeSyncJSONParserObj.lookupFieldonObject.trim().tolowercase());
                     
                }
                
                System.debug('attlookupFieldForRelatedObjectSet>>>'+attlookupFieldForRelatedObjectSet);
                System.debug('attNameSet>>>'+attNameSet);
                System.debug('attNameFieldNameMap>>>'+attNameFieldNameMap);
                System.debug('filedNamedataTypeMap>>>'+filedNamedataTypeMap);
                System.debug('fieldNameSet>>>'+fieldNameSet);
                System.debug('ojectNameLookupFieldMap>>>'+ojectNameLookupFieldMap);
            }
            
            relatedObjectIDSObjectDatamap = getRelatedObjecSObjectDataMaps(sObjectName, sObjectIdList, fieldNameSet , ojectNameLookupFieldMap);
            
            updateAttributeDetails(sObjectName,attNameSet,filedNamedataTypeMap,relatedObjectIDSObjectDatamap, attNameFieldNameMap, ojectNameAttLookupFieldMap);
        }catch(Exception e){
            System.debug('Exception accrued:::::'+e);
        }
       
       
    }
    
     public static Map<String,sobject> getRelatedObjecSObjectDataMaps(String sObjectName,List<id> sObjectIdList,Set<String> fieldNameSet, Map<String,String> ojectNameLookupFieldMap){
        Map<String,String> attNameObjectIdMap = new Map<String,String> ();
        Map<string, sobject> relatedObjectIDSObjectDatamap = new Map<string, sobject>();
        String soqlfields ='', soqlquery='',filedName='';
        try{    
           
            if(!ojectNameLookupFieldMap.isEmpty()){
                fieldNameSet.add(ojectNameLookupFieldMap.get(sObjectName.toLowerCase()));
               
                fieldNameSet.add('id');
                fieldNameSet.add('Name');
                soqlfields = AttributeRevSyncUtility.convertsettostring(fieldNameSet);
                soqlquery = 'select '+soqlfields+' from '+sObjectName+' where id in : sObjectIdList';
                System.debug('soqlquery>>'+soqlquery);
                list<sobject> QueryResult = Database.query(soqlquery);
                System.debug('QueryResult>>'+QueryResult);
                if(!QueryResult.isEmpty()){
                    for(sObject obj : QueryResult){
                        String temapKey = AttributeRevSyncUtility.returnFieldValue(obj, ojectNameLookupFieldMap.get(sObjectName.toLowerCase()));
                        System.debug('temapKey::::'+temapKey);
                        relatedObjectIDSObjectDatamap.put(temapKey,obj);
                    }
                }
                System.debug('relatedObjectIDSObjectDatamap>>'+relatedObjectIDSObjectDatamap);
            }
        }catch(Exception e){
            System.debug('Exception accrued:::::'+e);
        }
        return relatedObjectIDSObjectDatamap;
    }
    
     public static Map<String,String> updateAttributeDetails(String sObjectName,Set<String> attNameSet,Map<String, String>filedNamedataTypeMap,Map<string, sobject> relatedObjectIDSObjectDatamap ,Map<String,String> attNameFieldNameMap, Map<String,String> ojectNameAttLookupFieldMap){
        Map<String,String> attNameObjectIdMap = new Map<String,String> ();
        String soqlfields ='', soqlquery='',filedName='';
        try{    
           
            if(!relatedObjectIDSObjectDatamap.isEmpty()){
                //soqlfields = convertsettostring(attlookupFieldForRelatedObjectSet);
                String attLookupField = ojectNameAttLookupFieldMap.get(sObjectName);
                Set<String> relatedObjectIdSet =  relatedObjectIDSObjectDatamap.keySet();
                System.debug('relatedObjectIdSet>>'+relatedObjectIdSet);
                soqlquery = 'select '+attLookupField+',id,Name,cscfga__Display_Value__c,cscfga__Value__c  from cscfga__Attribute__c where '+attLookupField+' in :relatedObjectIdSet and name in : attNameSet';
                System.debug('soqlquery>>'+soqlquery);
                List<cscfga__Attribute__c> attbutedataList = (List<cscfga__Attribute__c>)Database.query(soqlquery);
                List<sObject> sObjectListtoUpdate =  new List<sObject>();
                System.debug('attbutedataList>>'+attbutedataList);
                if(!attbutedataList.isEmpty()){
                    for(cscfga__Attribute__c attObj : attbutedataList){
                        String attrelatedObjectid = AttributeRevSyncUtility.returnFieldValue(attObj, attLookupField);
                        sObject associatedSObject = relatedObjectIDSObjectDatamap.get(attrelatedObjectid);
                        String attValue = AttributeRevSyncUtility.returnFieldValue(associatedSObject, attNameFieldNameMap.get(attObj.Name.toLowerCase()));
                        if(String.isNotBlank(attValue)){
                            attObj.cscfga__Value__c = attValue;
                            attObj.cscfga__Display_Value__c = attValue;
                            System.debug('attObj>>'+attObj);
                            sObjectListtoUpdate.add(attObj);
                        }else if(String.isNotBlank(attObj.cscfga__Value__c)){
                            if(String.isNotBLank(filedNamedataTypeMap.get(attNameFieldNameMap.get(attObj.Name.toLowerCase()))) && filedNamedataTypeMap.get(attNameFieldNameMap.get(attObj.Name.toLowerCase())).equalsIgnoreCase('double')){
                                associatedSObject.put(attNameFieldNameMap.get(attObj.Name.toLowerCase()), decimal.valueOf(attObj.cscfga__Value__c));
                            }else{
                                associatedSObject.put(attNameFieldNameMap.get(attObj.Name.toLowerCase()), attObj.cscfga__Value__c);
                            }
                            sObjectListtoUpdate.add(associatedSObject);
                            System.debug('associatedSObject>>'+associatedSObject);
                        }
                    }
                    if(!sObjectListtoUpdate.isEmpty()){
                        update sObjectListtoUpdate;
                    }
                }
                
            }
        }catch(Exception e){
            System.debug('Exception accrued:::::'+e);
        }
        return null;
    }

}