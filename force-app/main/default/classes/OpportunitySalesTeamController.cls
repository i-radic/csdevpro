//
// History
//
// Version      Date            Author          Comments
// 1.0.0        14-02-2011      Dan Measures    Initial version - CR2627
//

//
// Comments
//
// 1. This controller creates a Task (and also sends an Email notification) to the Opportunity owner in response to clicking
//		the 'Request Sales Team Access' custom button on an Opportunity.  
//		Users would do this to request write access from the Opportunity owner.
//
public with sharing class OpportunitySalesTeamController {
	public final Opportunity opp;
	private User replyToUser = [select email from User where Id = :UserInfo.getUserId()];
	private String subject = System.Label.OST_subject;
	private String body;

	public OpportunitySalesTeamController() {
		List<Opportunity> opp_list = [select id, Owner.Email, Name, OwnerId, Sales_Team_Access_Requested__c, Opportunity_Id__c from Opportunity where id =:ApexPages.currentPage().getParameters().get('id')];
		if(!opp_list.isEmpty()){
			this.opp = opp_list[0];
			this.body = UserInfo.getName() + ' ' + System.Label.OST_body + ' ' + opp.Name + ' - ' + opp.Opportunity_Id__c + '\n' + '\n';
			this.body += System.Label.OST_body1 + ' ' + System.Label.OST_URL + opp.Id + '\n' + '\n';
			this.body += System.Label.OST_body3 + ' ' + replyToUser.Email + '\n' + '\n';
			this.body += System.Label.OST_body4;
		}
	}
	
	public PageReference onLoad(){
		if(this.opp != null){
			//  create task
			Task t = new Task();
			t.WhatId = opp.Id;
			t.subject = subject;
			t.ActivityDate = System.today() + 1;	//  set due date to 24 hours hence
			t.OwnerId = opp.OwnerId;				//  this is a delegated Task
			t.Description = body;
			insert t;
			
			//update the opportunity - flag set purely for reporting purposes.  We only update the opp on the first request.
			if(!this.opp.Sales_Team_Access_Requested__c && this.opp != null){
				OpportunitySalesTeam.markOppSalesTeamAccessRequested(this.opp);			//  this class is 'without sharing'
			}
			
			//  send email- also exclude from test
			if(!Test.isRunningTest())
				sendEmail();
			
			//  redirect back to Opportunity detail page
			PageReference pr = new ApexPages.StandardController(opp).view();
			pr.setRedirect(true);
			return  pr;
		}
		return null;
	}
	
	public void sendEmail(){
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		String[] toAddresses = new String[] {opp.Owner.email};
		mail.setToAddresses(toAddresses);
		if(replyToUser != null)
			mail.setReplyTo(replyToUser.email);
		mail.setSubject(subject);
		mail.setBccSender(false);
		mail.setPlainTextBody(body);
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}