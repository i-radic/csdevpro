@isTest
public class Test_CreateLeadsFromServiceCon {
    public static testMethod void LFS_LeadtestMethod(){
        TriggerDeactivating__c dt = new TriggerDeactivating__c();
        dt.Account__c = False;
        dt.Opportunity__c = False;
        insert dt;
        
        Account A = new Account();
        A.CUG__c = 'CUGTest12345';
        A.Name = 'Test-Account';
        insert A;
        
        Lead L = new Lead();
        L.Company = 'TestCompany';
        L.LastName = 'LFSTestLead';
        L.Status = 'Not Started';
        L.RecordTypeId = [select Id from RecordType Where Name = 'Leads From Service' AND SobjectType = 'Lead'].Id;
        L.Product_Group__c = 'Broadband';
        L.Product_Group_Type__c = 'Broadband new';
        L.LFS_CUG__c = 'CUGTest12345';
        //L.Lead_Due_Date__c = DateTime.Now()+2;
        L.LFS_Originator_Email__c = 'test@test.com';
        L.LFS_EIN__c = '608291848';
        //L.preSales_User_EIN__c = '608291848';
        L.LeadSource = 'Leads From Service';
        
        Insert L;
                       
        ApexPages.StandardController controller= new ApexPages.StandardController(L);
        CreateLeadsFromServiceCon LFS = new CreateLeadsFromServiceCon(controller);
       
        LFS.getTarrifValues();
        LFS.saveLead();
        LFS.CreateNewLead();
        LFS.setCookie();
        
    }
    
        public static testMethod void LFS_OpptestMethod(){
            
        TriggerDeactivating__c dt = new TriggerDeactivating__c();
        dt.Account__c = False;
        dt.Opportunity__c = False;
        insert dt;    
        
            
            
        Lead L = new Lead();
        L.Company = 'TestCompany';
        L.LastName = 'LFSTestLead';
        L.Status = 'Not Started';
        L.RecordTypeId = [select Id from RecordType Where Name = 'Leads From Service' AND SobjectType = 'Lead'].Id;
        L.Product_Group__c = 'Service Self-fulfil';
        L.Product_Group_Type__c = 'Handset only';
        L.LFS_CUG__c = 'CUGTest12345';
        //L.Lead_Due_Date__c = DateTime.Now()+2;
        L.LFS_Originator_Email__c = 'test@test.com';
        L.LFS_EIN__c = '608291848';
        L.preSales_User_EIN__c = '608291848';
        L.LeadSource = 'Leads From Service';
        
        Insert L;
                       
        ApexPages.StandardController controller= new ApexPages.StandardController(L);
        CreateLeadsFromServiceCon LFS = new CreateLeadsFromServiceCon(controller);
       
        LFS.getTarrifValues();
        LFS.saveLead();
        LFS.CreateNewLead();
        LFS.setCookie();
        
        }
}