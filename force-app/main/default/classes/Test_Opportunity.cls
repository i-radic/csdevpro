@istest

private class Test_Opportunity {

    testmethod static void Test_FlagIsSetExist(){
        Test_Factory.setProperty('IsTest', 'yes');

        StaticVariables.setContactIsRunBefore(False);             
        Contact con1 = Test_Factory.CreateContact();
        con1.Cug__c = 'cugAlan1';
        con1.Contact_Post_Code__c = 'WR5 3RL';
        insert con1;    
        
        insert Test_Factory.CreateAccountForDummy();
        Account account = Test_Factory.CreateAccount();        
        Database.SaveResult accountResult = Database.insert(account);
      
        Opportunity oppS2S = Test_Factory.CreateOpportunity(accountResult.getid());
        oppS2S.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(oppS2S);  

        Product2 opplineprod = Test_Factory.CreateProduct();
        Database.SaveResult opplineprodResult = Database.insert(opplineprod);
        Pricebook2 stdPb = [select Id from Pricebook2 where isStandard=true limit 1] ;// standard pricebook
        PricebookEntry opplinepb = Test_Factory.CreatePricebookEntry(stdPb.Id, opplineprod.Id);
        Database.SaveResult opplinepbResult = Database.insert(opplinepb);
        OpportunityLineItem oppline = Test_Factory.CreateOpportunityLineItem(opplinepb.Id,oppS2S.Id );
        Database.SaveResult opplineResult = Database.insert(oppline);
        
        oppS2S.closedate = system.today().addmonths(3);
        update oppS2S;
        
        oppline.service_ready_date__c = Null;
        oppline.first_bill_date__c = Null;
        update oppline;
       
       
        OpportunityContactRole oppCon = new OpportunityContactRole();
        oppCon.ContactId = con1.Id;
        oppCon.OpportunityId = oppS2S.Id;
        oppCon.IsPrimary = TRUE;
        Database.SaveResult oppConResult = Database.insert(oppCon);  
                     
        oppS2S.Product_Family__c = 'ENGAGE IT';
        oppS2S.Routing_Product__c = 'Engage IT';
        update oppS2S;
                 
        s2s_Opportunities__c s2srec = new s2s_Opportunities__c();
        s2srec.s2s_title__c = 'TEST';
        s2srec.s2s_Owner_Name__c = 'TEST';
        s2srec.s2s_status__c = 'TEST';
        s2srec.s2s_description__c = 'TEST';
        s2srec.s2s_Owner_Phone__c = 'TEST';
        s2srec.s2s_Owner_Email__c = 'TEST';
        
        Database.SaveResult s2sResult = Database.insert(s2srec); 
        s2srec.s2s_title__c = 'TEST1';
        s2srec.s2s_Owner_Name__c = 'TEST1';
        s2srec.s2s_status__c = 'TEST1';
        s2srec.s2s_description__c = 'TEST1';
        s2srec.s2s_Owner_Phone__c = 'TEST1';
        s2srec.s2s_Owner_Email__c = 'TEST1';
        //update s2srec;      
    }
}