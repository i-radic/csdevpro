global class CoverageCheckApprovalProcess{
    WebService static String submitAndProcessSalesRequest(String id) 
    {
        Coverage_Check_Site__c ccs= [Select Location_Name2__c,Post_Code__c,CER_Reason__c,Site_Contact_Name__c,Telephone_Number__c,
                                     Floorplan_Attached__c,Which_floors_require_enhancement__c,Building_Type__c,No_of_Buildings_Requiring_Enhancements__c,No_of_floors_in_this_building__c from Coverage_Check_Site__c where Id = :id ];
        String salesResult = '';
        boolean submitError = false;
        salesResult ='Special Projects submission error.Please check the following fields and submit again.\n';
        if ((ccs.Location_Name2__c == null )) 
        {
            salesResult = salesResult + '\tPlease enter the site address.\n';
            submitError = true;
        } 
        if ((ccs.Post_Code__c == null )) 
        {
            salesResult = salesResult + '\tPlease enter the post code.\n';
            submitError = true;
        } 
        if ((ccs.CER_Reason__c == null )) 
        {
            salesResult = salesResult + '\tPlease select atleast one CER reason.\n';
            submitError = true;
        } 
        if ((ccs.Site_Contact_Name__c == null )) 
        {
            salesResult = salesResult + '\tPlease enter the site contact name.\n';
            submitError = true;
        } 
        if ((ccs.Telephone_Number__c == null )) 
        {
            salesResult = salesResult + '\tPlease enter the site contact number.\n';
            submitError = true;
        }
        if ((ccs.Floorplan_Attached__c == false)) 
        {
            salesResult = salesResult + '\tPlease attach the floor plan.\n';
            submitError = true;
        } 
        if ((ccs.No_of_Buildings_Requiring_Enhancements__c == null )) 
        {
            salesResult = salesResult + '\tPlease enter the number of buildings requiring enhancements.\n';
            submitError = true;
        } 
        if ((ccs.No_of_floors_in_this_building__c == null)) 
        {
            salesResult = salesResult + '\tPlease enter the number of floors in this bulding.\n';
            submitError =true;
        } 
        if ((ccs.Which_floors_require_enhancement__c == null)) 
        {
            salesResult = salesResult + '\tPlease enter which floors require enhancement.\n';
            submitError =true;
        }               
        if ( submitError == false) 
        {
            ccs.Status__c ='Submitted to Special Projects';
            update ccs;
            salesResult =  'Successfully submitted to Special projects.\n';
        }
        return salesResult ;         
   }    
}