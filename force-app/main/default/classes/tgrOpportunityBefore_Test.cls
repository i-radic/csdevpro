/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class tgrOpportunityBefore_Test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        User thisUser = [select id from user where id=:Userinfo.getuserid()];
        system.runAs(thisUser){
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@bt.com',
                           EIN__c = '6erg'
                           );
        insert u1;
        
        User u3 = new User(
                           alias = 'B2B1', email = 'B2B1@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BPcr@bt.com',
                           EIN__c = '6erg2'
                           );
        insert u3;
                
          Profile prof2=[SELECT Id FROM Profile WHERE Name = 'BTLB: Standard Team'];
         User u2= new User(
                           alias = 'B2B1', email = 'B2cr@bt.it',
                          emailencodingkey = 'UTF-8', lastname = 'Testing B21',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof2.Id,
                           timezonesidkey = 'Europe/London', username = 'cr@bt.com',
                           EIN__c = '6erg1' ,ManagerId= u3.Id
                           );
        insert u2;
        
        
                
        Account a = new Account(Name='Test Account to test trig');
        a.OwnerId = u1.Id;
        insert a;
        Date uDate1 = date.today().addDays(15);
        
         Account a1 = new Account(Name='Test Account to test trig 2');
        a1.OwnerId = u2.Id;
        insert a1;
        //Date uDate2 = date.today().addDays(15);
        
        
        System.debug('inserted account');
        
        Opportunity o1 = new Opportunity(AccountId=a.id, Name='Test Opp1 to test trig', Product_Family__c='BROADBAND PRODUCTS', StageName='Created',CloseDate=Date.today()+7);
        Opportunity o2 = new Opportunity(AccountId=a.id, Name='Test Opp2 to test trig', Product_Family__c='BROADBAND PRODUCTS', StageName='Created',CloseDate=Date.today()+7);   
        Opportunity o3 = new Opportunity(AccountId=a.id, Name='Test Opp3 to test trig', Product_Family__c='BROADBAND PRODUCTS', StageName='Created',CloseDate=Date.today()+7,RecordTypeId='01220000000AIB4',Lead_Source__c = 'BTNI BRT'); 
        Opportunity o4 = new Opportunity(AccountId=a.id, Name='Test Opp4 to test trig', Product_Family__c='ENGAGE IT', StageName='Created',CloseDate=Date.today()+7);  
        Opportunity o5 = new Opportunity(AccountId=a.id, Name='Test Opp5 to test trig', Product_Family__c='BUSINESS DIRECT', StageName='Created',CloseDate=Date.today()+7);  
        Opportunity o6 = new Opportunity(AccountId=a.id, Name='Test Opp6 to test trig', Product_Family__c='INET', StageName='Created',CloseDate=Date.today()+7); 
        Opportunity o7 = new Opportunity(AccountId=a.id, Name='Test Opp7 to test trig', Sales_Agent_EIN__c = '6erg', StageName='Created',CloseDate=Date.today()+7 ,Auto_Assign_Owner__c = true);  
		Opportunity o8 = new Opportunity(AccountId=a.id, Name='Test Opp8 to test trig', Product_Family__c='IT SERVICES', StageName='Created',CloseDate=Date.today()+7);         
        
        List<Opportunity> opls=new List<Opportunity>();
        opls.add(o1);
        opls.add(o2);
        opls.add(o3);
        opls.add(o4);
        opls.add(o5);
        opls.add(o6);
        opls.add(o7);
        opls.add(o8);
        
        insert opls;
        System.debug('inserted o1, o2');
        
       o1.StageName='Cancelled';
       o1.CloseDate = uDate1;
       
       o2.StageName='Cancelled';
       o2.CloseDate = uDate1;//Qty__c, ProdCode__c, ProdName__c, FFA_Supplier__c, Category__c, First_Bill_Date__c, TotalPrice, OpportunityId, Description
       update opls;
        
        
        Product2 p2 = new Product2();
        p2.IsActive = true;
        p2.Name = 'Test Product';
        p2.ProductCode = 'testprod';
        //p2.Part_Number__c = 'MVNRV';
        
        insert p2;
       PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = '01s20000000HXVd', Product2Id = p2.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false );
           
      insert standardPrice;
        
      
      //Create Price Book
           
           PriceBook2 pb=new PriceBook2();
           pb.name='BTLB';
           
           insert pb;
        
    //Create Price Book Entry
        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = p2.id;
        pbe.Pricebook2Id = pb.id;
        pbe.IsActive = true;
        pbe.UnitPrice = 100;
        pbe.USESTANDARDPRICE=false;

        insert pbe;

          TriggerDeactivating__c customsetting=new TriggerDeactivating__c();
            customsetting.SetupOwnerId = '00520000001C3Fw';
            insert customsetting;
            
      //Create Oppty Line Item      
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = o1.id;
        oli.PricebookEntryId = pbe.id;
        oli.UnitPrice = 4100;
        oli.Quantity = 1;    
        oli.category__c = 'Main';
        oli.contract_term__c='24 Months';
        oli.billing_cycle__c='Monthly';
        oli.service_ready_date__c = system.today().addmonths(1);
        oli.first_bill_date__c = system.today().addmonths(2);
        oli.initial_cost__c = 4000;
            
            
       
        
        test.startTest();
        insert oli;    
           
            
            Contact btops1Contact = Test_Factory.CreateContact();
        insert btops1Contact;
        
        //Create BTOP record
        BT_One_Phone__c btop1 = new BT_One_Phone__c();
        btop1.Opportunity__c = o2.id; 
        btop1.Status__c = 'Pre Clearance Check Requested';
        btop1.Volume_of_Data_SIMs__c = 5;
        btop1.Contract_Duration__c = '24 Months';
        btop1.Contract_Value__c = 10;
        btop1.OnePhone_Product_Type__c = 'BT One Phone';
        btop1.Existing_Customer_Resign__c  = 'Y'; 
        btop1.Company_Registered_Number__c = '0001'; 
        btop1.Post_Code__c ='test';    
        Database.SaveResult[] btop1Result = Database.insert(new BT_One_Phone__c [] {btop1});  
         
        BT_One_Phone_Site__c btops1 = new BT_One_Phone_Site__c();
        btops1.BT_One_Phone__c = btop1Result[0].id;
        btops1.Site_Telephone_Number_Landline__c ='01234567890';
        btops1.Number_of_users__c = 1;
        btops1.Number_Concurrent_Calls_Expected_at_Site__c = 10;
        btops1.Number_of_Users_in_Parallel_Hunt_Group__c = '5 or Less';
        btops1.mobilecoverageResults__c = 'Excellent Indoor & Outdoor';
        btops1.Post_Code__c ='WR5 3RL';
        btops1.Site_Diagram_or_Sketch_Attached__c = true;
        btops1.Contract_Duration__c = '24 Months';      
        
        btops1.Customer_Contact__c = btops1Contact.Id;
        btops1.No_Onsite_network__c =true;
        Database.SaveResult[] btops1Result = Database.insert(new BT_One_Phone_Site__c [] {btops1});
        
       o2.StageName='Lost';
o2.Sales_Stage_Detail__c = 'Customer Changed Mind';
       update o2;
        //
       
        
     /* system.runAs(u2)
        {
             Opportunity o8 = new Opportunity(AccountId=a1.id, Name='Test Opp8 to test trig', StageName='Created',CloseDate=Date.today()+7,ownerId = U2.id  );  
            insert o8;
         system.debug('divya123' + o8.Account_OwnerID__c +u2.Id);
            
        }*/
     test.stopTest(); 
}
    }
}