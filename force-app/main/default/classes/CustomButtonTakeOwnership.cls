global with sharing class CustomButtonTakeOwnership extends csbb.CustomButtonExt{
    public String performAction (String basketId) {
        
        String newUrl = CustomButtonTakeOwnership.TakeOwnership(basketId);
        String successMsg = 'Service Owner updated successfully';
        //return '{"status":"ok","redirectURL":"' + newUrl + '"}';
        return '{"status":"ok","redirectURL":"' + newUrl+'","text":"'+ successMsg + '"}';

    } 
     public static String TakeOwnership(String basketId){
        cscfga__Product_Basket__c pb = Database.query('SELECT Id, Name, Service_Owner__c FROM cscfga__Product_Basket__c WHERE Id=\'' + basketId + '\'');
        String firstName = UserInfo.getFirstName();
		String lastName = UserInfo.getLastName();
        //String serviceOwner = firstName +' '+lastName;
        if(pb != null){
            pb.Service_Owner__c = UserInfo.getUserId();
            update pb;
        }
        return 'apex/BasketbuilderApp?id=' + basketId;
     }
}