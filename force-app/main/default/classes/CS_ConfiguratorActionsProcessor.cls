public class CS_ConfiguratorActionsProcessor implements Queueable {
	@TestVisible
    protected Map<String, Set<Id>> configsByActions {get; set;}
    protected Set<Id> allConfigs {get; set;}
    protected Map<Id, cscfga__Product_Configuration__c> configMap {get; set;}
    protected cscfga__Product_Basket__c basket {get; set;}
    
    public CS_ConfiguratorActionsProcessor(Map<String, Set<Id>> configsByActions) {
        this.allConfigs = new Set<Id>();
        this.configsByActions = configsByActions;
        for(Set<Id> cfgIds : configsByActions.values()) {
            this.allConfigs.addAll(cfgIds);
        }
        
        this.configMap = getConfigs(allConfigs);
        this.basket = getBasket(configMap.values().get(0));
    }
    
    public void execute(QueueableContext context) {
        for(String action : CS_ConfiguratorActions.ACTION_PRIORITY.keySet()) {
            Type t = Type.forName('CS_ConfiguratorActionsProcessor', 'CS_ConfiguratorAction' + action.capitalize());
            
            if(t != null) {
            	CS_ConfiguratorAction instance = (CS_ConfiguratorAction) t.newInstance();
            	instance.execute(getConfigsByAction(action), basket);
            }
        }
        
        finish();
    }
    
    public Map<Id, cscfga__Product_Configuration__c> getConfigsByAction(String action) {
        Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>();
        if(configsByActions != null){
          for(Id configId : configsByActions.get(action)) {
              configs.put(configId, this.configMap.get(configId));
          }
        }
        
        return configs;
    }
    
    public cscfga__Product_Basket__c getBasket(cscfga__Product_Configuration__c config) {
        return [SELECT
            		Id, Name, Basket_Totals__r.BT_Products_in_Basket__c
                FROM
               		cscfga__Product_Basket__c
                WHERE
               		Id = :config.cscfga__Product_Basket__c];
    }
    
    public Map<Id, cscfga__Product_Configuration__c> getConfigs(Set<Id> configIds) {
        return new Map<Id, cscfga__Product_Configuration__c>([Select
                                                              Id, Name, cscfga__Product_Definition__r.Name, cscfga__Product_Definition__c, cscfga__Recurrence_Frequency__c, Charge_Type__c,
                                                              cscfga__Product_Family__c, Product_Type__c, Product_Name__c, Product_Definition_Name__c, Deal_Type__c, Total_Subscribers__c,
                                                              Volume__c, Voice_Subscribers__c, Data_Subscribers__c, cscfga__Recurring_Charge__c, cscfga__One_Off_Charge__c, Usage_Profile_Voice_Revenue__c,
                                                              Total_Cost__c, BT_Hardware_Fund__c, BT_Technology_Fund__c, Total_Subsidy_Available__c, Voice_Subsidy__c, Calculations_Product_Group__c,
                                                              BT_Total_Commission__c, Shared_Data_Users__c, Shared_Data_Extra_Total__c, Calculated_device_fund__c, One_Off_Cost__c,
                                                              Device_payment_made_by_customer__c, cscfga__Contract_Term__c, MBB_Number_Of_Devices__c, BT_UK_Minutes_Extra__c, 
                                                              Calling_Extra__c, Rest_of_the_World__c, Tenure__c, cscfga__Configuration_Status__c, cscfga__Product_Basket__c,
                                                              cscfga__Root_Configuration__c, cscfga__Parent_Configuration__c, Total_Charges__c, Custom_Profile_Used__c, EE_Flag__c, BT_Flag__c,
                                                              Number_of_devices__c, Number_of_users__c, SpecCon_Competitive_Clause_Cost__c, SpecCon_Disconnection_Allowance_Cost__c,
                                                              SpecCon_Full_Co_terminus_Cost__c, SpecCon_Unlocking_Fees_Cost__c, Credit_fund_total__c, Complex_VAS__c, VAS__c, CF_Recurring_Cost__c,
                                                              Payment_Flag__c, CF_One_Off_Cost__c, Unconditional_cheque__c,  Buy_out_cheque__c, Staged_Air_Time__c, Rolling_Air_Time__c, Tech_Fund__c,
                                                              FCC__c, FCC_Actual__c, FCC_Airtime__c, FCC_Hardware__c, FCC_Flag__c, Hardware_Charges__c, NPV_Tech_Fund__c, NPV_Staged_Air_Time__c,
                                                              NPV_Unconditional_cheque__c, NPV_Buy_out_cheque__c, Unsecured_Revenue__c, Disconnection_allowance__c,  Disconnection_Allowance_Percentage__c,
                                                              Sales_Commision_One_Off_Cost__c, MNC_Discount__c, Early_Resign__c, Monthly_Charges__c, Monthly_Charges_after_Discount__c, Line_Rental__c,
                                                              Line_Rental_after_Discount__c, Total_Charges_after_Discount__c, Total_Device_Charges__c, Total_Investment_Pot__c, Total_Investment_Remaining__c,
                                                              Total_Device_Contribution__c, Investment_Pot_Usage__c, Care__c, Care_Insurance__c, Other_Recurring_Charge_Discounted__c,
                                                              Leader_Fee__c, Sharer_Fee__c, Type__c, cspl__Month_Term__c, Tech_Fund_Total__c, Product_Code__c,
                                                              Mobile_Voice_Recurring_Charge_Discounted__c, SMS_Recurring_Charge_Discounted__c, SMS_Recurring_Cost__c, SMS_One_Off_Charge_Discounted__c,
                                                              SMS_One_Off_Cost__c, Data_One_Off_Charge_Discounted__c, Data_One_Off_cost__c, Data_Recurring_Charge_Discounted__c, Data_Recurring_Cost__c,
                                                              Mobile_Voice_One_Off_Charge_Discounted__c, Mobile_Voice_One_Off_Cost__c, Mobile_Voice_Recurring_Cost__c, Revenue_Type__c, Other_One_Off_Charge_Discounted__c,
                                                              Fixed_One_Off_Charge_Discounted__c, Fixed_Recurring_Charge_Discounted__c, Incoming_Recurring_Charge__c, Service_Recurring_Cost__c,
                                                              OPEX_Recurring_Cost__c, Warning_Summary__c, Proposition_Name__c, Gross_Margin__c , Approval_Status__c, Approval_Level_1__c,
                                                              Approval_Level_2__c, Approval_Level_3__c, cscfga__one_off_charge_product_discount_value__c, cscfga__recurring_charge_product_discount_value__c
                                                              From
                                                              cscfga__Product_Configuration__c
                                                              WHERE
                                                              Id = :configIds]);
    }
    
    public void finish() {
        CS_ConfiguratorActions.setProductConfigurationRequests(this.allConfigs, '');
    }
    
    public abstract class CS_ConfiguratorAction {
        public abstract void execute(Map<Id, cscfga__Product_Configuration__c> configs, cscfga__Product_Basket__c basket);
    }
    
    public class CS_ConfiguratorActionRevalidate extends CS_ConfiguratorAction {
        public override void execute(Map<Id, cscfga__Product_Configuration__c> configs, cscfga__Product_Basket__c basket) {
            if(!configs.isEmpty()) {
            	cscfga.ProductConfigurationBulkActions.revalidateConfigurations(configs.keySet());

                CS_ProductConfigurationService.createProductConfigurationTotals(configs);
            }
        }
    }
    
     public class CS_ConfiguratorActionInvalidate extends CS_ConfiguratorAction {
        public override void execute(Map<Id, cscfga__Product_Configuration__c> configs, cscfga__Product_Basket__c basket) {
            if(!configs.isEmpty()) {
                for(cscfga__Product_Configuration__c cfg : configs.values()) {
                    cfg.cscfga__Configuration_Status__c = 'Invalid';
                }
                
                update configs.values();
            }
        }
    }
}