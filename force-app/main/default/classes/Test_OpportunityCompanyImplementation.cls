@isTest(SeeAllData=True)
//Can see all data as requires insertion of PRojects. Because we cannot insert Workspaces then we need to use this
private class Test_OpportunityCompanyImplementation {

    public static Account acc;
    public static Contact con;
    public static Opportunity opp;
    public static Company_Implementation__c ci;
    public static User u;
    
    public static void prepareTestData() 
    {
        u = [Select id from user where firstname = 'Marc' and lastname = 'Francis'];
        //u.Primary_Sales_Function__c = 'Acquisition';
        u.Orange_Business_Unit__c = 'Medium Sales';
        u.Super_Sector_Region__c = 'North';
        //u.Sector_Sales_Team__c = 'North';
        update u;
        
        acc = Test_Utils.createAccount('Test Account');
        acc.Number_of_subs_on_account__c = 10;
        acc.T_Mobile_Connections_in_Company__c = 10;
        acc.ownerId = u.Id;
        update acc;

        con = Test_Utils.createContact('Test','Contact', acc.Id, 'testcontact@email.com');
        
        System.runAs(u)
        {
            opp = Test_Utils.createOpp('Test Opp', 'Prospecting' , Date.Today(), acc.Id);
            Project__c p = Test_Utils.createProject(opp);
        }
    }
    
    static testMethod void companyImplementationTest() 
    {
        test.startTest();
        prepareTestData();
            
        /*BST_Deal_Credits__c bstDeal = new BST_Deal_Credits__c(Name='Test Deal', Opportunity__c = opp.Id);
        insert bstDeal;*/
        
        //Get the Company Implementation Id and create Credits record
        ci = new Company_Implementation__c();
        //ci = [Select id, Company__c from Company_Implementation__c where Opportunity__c =: opp.Id limit 1];
        ci.Name = acc.Name + ' - Implementation';
            
        ci.Company__c = opp.AccountId;
        ci.Opportunity__c = opp.Id;
        ci.Overview_Stage__c = 'New';
        ci.Credit_Status__c = 'New';
           
        insert ci;
            
        //Checklists
        List<RecordType> lstRT = new List<RecordType>([Select id, name from Recordtype where 
                                    IsActive = true and SObjectType = 'Checklist__c']);
        for (RecordType rt : lstRT)
        {
            if (rt.Name == 'Validation')
            {
                Checklist__c chkRecord = new Checklist__c(Credits__c = true, RecordtypeId=rt.Id, Company_Implementation__c = ci.Id);
                insert chkRecord;
            }
            else if (rt.Name.Contains('Data Setup'))
            {
                Checklist__c chkRecord1 = new Checklist__c(Credits__c = true, RecordtypeId=rt.Id, Company_Implementation__c = ci.Id);
                insert chkRecord1;
            }   
            else if (rt.Name == 'Contract Setup')
            {
                Checklist__c chkRecord2 = new Checklist__c(Order_User__c = u.Id, Credits__c = true, RecordtypeId=rt.Id, Company_Implementation__c = ci.Id);
                insert chkRecord2;  
            }
            else if (rt.Name == 'Contract Setup Change Note')
            {
                Checklist__c chkRecord3 = new Checklist__c(Credits__c = true, RecordtypeId=rt.Id, 
                Company_Implementation__c = ci.Id, Manual_CCN__c='test',Document_Type__c = 'Contract Change Note');
                insert chkRecord3;  
            }
        }
        
        opp.Acquisition_Status_post_approval__c = 'Processed';
        opp.EEBA_Status__c = 'Fully Signed';
        update opp;
        //Credits
        List<RecordType> rt = new List<RecordType>([Select id from Recordtype where name = 'Staged Equipment with Cheque' 
                and SObjectType = 'Credit__c']);
                
        Credit__c cr = new Credit__c();
        cr.Company__c = ci.Company__c;
        cr.Company_Implementation__c = ci.Id;
        cr.RecordtypeId = rt[0].Id;
        insert cr;   
            
        
        /*Notes__c nt = new Notes__c();
        nt.Name = 'Test';
        nt.Company_Implementation__c = ci.Id;
        nt.Team__c = 'Validation';
        insert nt;
        
        nt.description__c = 'Description';
        update nt;
        
        delete nt;*/
        test.stopTest();        
    }
    
    
    static testMethod void companyImplementationTrigger() 
    {
        
        u = [Select id from user where firstname = 'Marc' and lastname = 'Francis'];
        
        //u.Primary_Sales_Function__c = 'Acquisition';
        u.Orange_Business_Unit__c = 'Medium Sales';
        u.Super_Sector_Region__c = null;
        //u.Sector_Sales_Team__c = null;
        update u;
        
        acc = Test_Utils.createAccount('Test Account');
        acc.Number_of_subs_on_account__c = 10;
        acc.T_Mobile_Connections_in_Company__c = 10;
        acc.ownerId = u.Id;
        update acc;
        
        System.runAs(u)
        {
            opp = Test_Utils.createOpp('Test Opp', 'Prospecting' , Date.Today(), acc.Id);
            Project__c p = Test_Utils.createProject(opp);

            ci = new Company_Implementation__c();
            ci.Name = acc.Name + ' - Implementation';
            ci.Company__c = opp.AccountId;
            ci.Opportunity__c = opp.Id;
            ci.Overview_Stage__c = 'New';
            ci.Credit_Status__c = 'New';
            ci.Bill_Date__c = '26th';
            insert ci;
        }

        //u.Primary_Sales_Function__c = 'Retention';
        u.Orange_Business_Unit__c = 'Medium Sales';
        u.Super_Sector_Region__c = null;
        //u.Sector_Sales_Team__c = null;
        update u;
        
        System.runAs(u)
        {
            ci = new Company_Implementation__c();
            ci.Name = acc.Name + ' - Implementation';
            ci.Company__c = opp.AccountId;
            ci.Opportunity__c = opp.Id;
            ci.Overview_Stage__c = 'New';
            ci.Credit_Status__c = 'New';
            insert ci;
        }

        //u.Primary_Sales_Function__c = 'Retention';
        u.Orange_Business_Unit__c = 'Medium Sales';
        u.Super_Sector_Region__c = 'Public Sector';
        //u.Sector_Sales_Team__c = 'North Ireland';
        update u;
        
        System.runAs(u)
        {
            /*opp.Expected_Interconnect_Revenue__c = 80000;
            opp.Expected_Agreement_tenure__c = 12;
            update opp;*/

            
            ci = new Company_Implementation__c();
            ci.Name = acc.Name + ' - Implementation';
            ci.Company__c = opp.AccountId;
            ci.Opportunity__c = opp.Id;
            ci.Overview_Stage__c = 'New';
            ci.Credit_Status__c = 'New';
            insert ci;
        }
        
        
        //u.Primary_Sales_Function__c = 'Acquisition';
        u.Orange_Business_Unit__c = 'Medium Sales';
        u.Super_Sector_Region__c = 'North';
        //u.Sector_Sales_Team__c = 'North';
        update u;
        
        

        //con = Test_Utils.createContact('Test','Contact', acc.Id, 'testcontact@email.com');
        
        System.runAs(u)
        {
           /* opp.Expected_Interconnect_Revenue__c = 41000;
            opp.Expected_Agreement_tenure__c = 12;
            update opp;*/
            
            ci = new Company_Implementation__c();
            ci.Name = acc.Name + ' - Implementation';
            ci.Company__c = opp.AccountId;
            ci.Opportunity__c = opp.Id;
            ci.Overview_Stage__c = 'New';
            ci.Credit_Status__c = 'New';
            insert ci;
        }
          
        //u.Primary_Sales_Function__c = 'Acquisition';
        u.Orange_Business_Unit__c = 'Medium Sales';
        u.Super_Sector_Region__c = 'North';
        //u.Sector_Sales_Team__c = 'North';
        update u;
        
        test.startTest();
        System.runAs(u)
        {
            /*opp.Expected_Interconnect_Revenue__c = 20000;
            opp.Expected_Agreement_tenure__c = 12;
            update opp;*/

            ci = new Company_Implementation__c();
            ci.Name = acc.Name + ' - Implementation';
            ci.Company__c = opp.AccountId;
            ci.Opportunity__c = opp.Id;
            ci.Overview_Stage__c = 'New';
            ci.Credit_Status__c = 'New';
            ci.Bill_Date__c = '26th';
            insert ci;
            
            ci.Bill_Date__c = '1st';
            update ci;
            ci.Bill_Date__c = '3rd';
            update ci;
            ci.Bill_Date__c = '5th';
            update ci;
            ci.Bill_Date__c = '8th';
            update ci;
            ci.Bill_Date__c = '10th';
            update ci;
            ci.Bill_Date__c = '12th';
            update ci;
            ci.Bill_Date__c = '15th';
            update ci;
            ci.Bill_Date__c = '17th';
            update ci;
            ci.Bill_Date__c = '19th';
            update ci;
            ci.Bill_Date__c = '22nd';
            update ci;
            ci.Bill_Date__c = '24th';
            update ci;
        }
        test.stopTest();        
    }
}