public class ADPSWOTCtrl { 
   
public ADPSWOTCtrl(ApexPages.StandardController controller) {   
 }
ID APD_Id = ApexPages.currentPage().getParameters().get('id');

public List<ADP_BT_SWOT__c> getBT() {          
return [SELECT id, Status__c, Show_in_PDF__c, What_is_the_progress__c,What_needs_to_be_done__c,When_will_it_be_achieved__c,Who_is_going_to_do_it__c from ADP_BT_SWOT__c
WHERE ADP__c = :APD_Id
ORDER BY Status__c, When_will_it_be_achieved__c DESC];
    }
public List<ADP_Client_SWOT__c> getClient() {          
return [SELECT id, Status__c, Show_in_PDF__c, What_is_the_progress__c,What_needs_to_be_done__c,When_will_it_be_achieved__c,Who_is_going_to_do_it__c from ADP_Client_SWOT__c
WHERE ADP__c = :APD_Id
ORDER BY Status__c, When_will_it_be_achieved__c DESC];
    }
public List<ADP_BT_SWOT__c> getBTBoth() {          
return [SELECT id, Status__c, Show_in_PDF__c, What_is_the_progress__c,What_needs_to_be_done__c,When_will_it_be_achieved__c,Who_is_going_to_do_it__c from ADP_BT_SWOT__c
WHERE ADP__c = :APD_Id
AND Show_in_PDF__c = 'Both'
ORDER BY Status__c, When_will_it_be_achieved__c DESC];
    }
public List<ADP_Client_SWOT__c> getClientBoth() {          
return [SELECT id, Status__c, Show_in_PDF__c, What_is_the_progress__c,What_needs_to_be_done__c,When_will_it_be_achieved__c,Who_is_going_to_do_it__c from ADP_Client_SWOT__c
WHERE ADP__c = :APD_Id
AND Show_in_PDF__c = 'Both'
ORDER BY Status__c, When_will_it_be_achieved__c DESC];
    }   
 }