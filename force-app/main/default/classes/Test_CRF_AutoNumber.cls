@isTest

    private class Test_CRF_AutoNumber{
    
    static testMethod void myUnitTest(){
    
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    
    
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    insert o;
    
    Id MoversId;
    Id NSOId;
    Id BroadbandId;
    Id DDId;
    Id AXId;
    Id WLR3Id;
    Id ResignId;
    Id NPId;
    Id NCId;  
    Id ISDNId;
    Id NCPId;
    Id BCId;
    Id OEId;
                
                
    List<RecordType> RTList = [Select Id,DeveloperName from RecordType where SobjectType=:'CRF__c'];
    if(RTList.size()>0){
        for(RecordType RT:RTList){
            if(RT.DeveloperName=='Movers')
                MoversId = RT.Id;
            else if(RT.DeveloperName=='NSO_BS')
                NSOId = RT.Id;
            else if(RT.DeveloperName=='Broadband')
                BroadbandId = RT.Id;
            else if(RT.DeveloperName=='Direct_Debit_CRF')
                DDId = RT.Id;
            else if(RT.DeveloperName=='AX_Form')
                AXId = RT.Id;
            else if(RT.DeveloperName=='WLR3_Authorisation')
                WLR3Id = RT.Id;
            else if(RT.DeveloperName=='Resign')
                ResignId = RT.Id;
            else if(RT.DeveloperName=='Number_Port')
                NPId = RT.Id;
            else if(RT.DeveloperName=='Name_Change_CRF')
                NCId = RT.Id;
            else if(RT.DeveloperName=='ISDN30')                
                ISDNId = RT.Id;
            else if(RT.DeveloperName=='NCP')                
                NCPId = RT.Id;
            else if(RT.DeveloperName=='Business_Complete')                
                BCId = RT.Id;
            else if(RT.DeveloperName=='Offer_Exception')                
                OEId = RT.Id;
        }
    }
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = MoversId; 
    crf.Opportunity__c = o.Id;
    crf.Contact__c = c.Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf;
    
    CRF__c crf3 = new CRF__c();
    crf3.RecordTypeId = MoversId;
    crf3.Opportunity__c = o.Id;
    crf3.Contact__c = c.Id;
    crf3.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf3;
    
    
    
    CRF__c crf1 = new CRF__c();
    crf1.RecordTypeId = NSOId ;
    crf1.Opportunity__c = o.Id;
    crf1.Contact__c = c.Id;
    crf1.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf1;
    
    //crf1.Name='2';
    //update crf1;
    
    CRF__c crf4 = new CRF__c();
    crf4.RecordTypeId = NSOId ;
    crf4.Opportunity__c = o.Id;
    crf4.Contact__c = c.Id;
    crf4.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf4;
    
    delete crf4;


    CRF__c crf2 = new CRF__c();
    crf2.RecordTypeId = BroadbandId ;
    crf2.Opportunity__c = o.Id;
    crf2.Order_type__c = 'Overlapping Mover';
    crf2.Payment_Method__c = 'DD details not available send mandate';
    crf2.Alt_contact_phone_number__c = '0123456789';
    crf2.password__c='asdf@12345';
    crf2.Tel_number_for_Broadband_Service__c='02154879632';
    crf2.Your_sales_channel_ID__c='B77';
    crf2.Contact__c = c.Id;
    crf2.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf2;
    
    CRF__c crf5 = new CRF__c();
    crf5.RecordTypeId = BroadbandId ;
    crf5.Opportunity__c = o.Id;
    crf5.Order_type__c = 'Overlapping Mover';
    crf5.Payment_Method__c = 'DD details not available send mandate';
    crf5.Alt_contact_phone_number__c = '0123456789';
    crf5.password__c='asdf@12345';    
    crf5.Tel_number_for_Broadband_Service__c='02154879632'; 
    crf5.Your_sales_channel_ID__c='B77'; 
    crf5.Contact__c = c.Id;
    crf5.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';  
    insert crf5;
    
    CRF__c crf6 = new CRF__c();
    crf6.RecordTypeId = DDId ;
    crf6.Opportunity__c = o.Id;
    crf6.Contact__c = c.Id;
    crf6.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf6;
    
    CRF__c crf7= new CRF__c();
    crf7.RecordTypeId = DDId ;
    crf7.Opportunity__c = o.Id;
    crf7.Contact__c = c.Id;
    crf7.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf7;
    
        CRF__c crf8 = new CRF__c();
    crf8.RecordTypeId = AXId ;
    crf8.Opportunity__c = o.Id;
    crf8.Contact__c = c.Id;
    crf8.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf8;
    
    CRF__c crf9= new CRF__c();
    crf9.RecordTypeId = AXId ;
    crf9.Opportunity__c = o.Id;
    crf9.Contact__c = c.Id;
    crf9.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf9;
       
    CRF__c crf14= new CRF__c();
    crf14.RecordTypeId = WLR3Id ;
    crf14.Opportunity__c = o.Id;
    crf14.Contact__c = c.Id;
    crf14.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf14;
    
    CRF__c crf15= new CRF__c();
    crf15.RecordTypeId = WLR3Id ;
    crf15.Opportunity__c = o.Id;
    crf15.Contact__c = c.Id;
    crf15.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf15;
    
    
    CRF__c crf16= new CRF__c();
    crf16.RecordTypeId = ResignId ;
    crf16.Opportunity__c = o.Id;
    crf16.Contact__c = c.Id;
    crf16.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf16;
    
    CRF__c crf17= new CRF__c();
    crf17.RecordTypeId = ResignId ;
    crf17.Opportunity__c = o.Id;
    crf17.Contact__c = c.Id;
    crf17.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf17;
    
    CRF__c crf12= new CRF__c();
    crf12.RecordTypeId = NPId ;
    crf12.Opportunity__c = o.Id;
    crf12.Vol_ref_Mover__c='VOL011-11111111111';
    crf12.Contact__c = c.Id;
    crf12.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf12;
    
    CRF__c crf13= new CRF__c();
    crf13.RecordTypeId = NPId ;
    crf13.Opportunity__c = o.Id;
    crf13.Vol_ref_Mover__c='VOL011-11111111111';
    crf13.Contact__c = c.Id;
    crf13.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf13;
    
    
    CRF__c crf10= new CRF__c();
    crf10.RecordTypeId = NCId ;
    crf10.Opportunity__c = o.Id;
    crf10.Contact__c = c.Id;
    crf10.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf10;
    
    CRF__c crf11= new CRF__c();
    crf11.RecordTypeId = NCId ;
    crf11.Opportunity__c = o.Id;
    crf11.Contact__c = c.Id;
    crf11.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf11;
    
    
    CRF__c crf18= new CRF__c();
    crf18.RecordTypeId = ISDNId ;
    crf18.Opportunity__c = o.Id;
    insert crf18;
    
    CRF__c crf19= new CRF__c();
    crf19.RecordTypeId = ISDNId;
    crf19.Opportunity__c = o.Id;
    insert crf19;
    
    
    CRF__c crf20= new CRF__c();
    crf20.RecordTypeId = NCPId;
    crf20.Opportunity__c = o.Id;
    crf20.Contact__c = c.Id;
    crf20.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf20;
    
    //crf20.Back_Office_Status__c='Assigned to NCP team';
    //update crf20;
    
    CRF__c crf21= new CRF__c();
    crf21.RecordTypeId = NCPId;
    crf21.Opportunity__c = o.Id;
    crf21.Contact__c = c.Id;
    crf21.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    insert crf21;
    
    CRF__c crf22= new CRF__c();
    crf22.RecordTypeId = BCId;
    crf22.Opportunity__c = o.Id;
    crf22.Contact__c = c.Id;
    crf22.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    crf22.Capped_Calls__c='Yes';
    crf22.OP_Access_Lines__c='New';
    insert crf22;
    
    CRF__c crf23= new CRF__c();
    crf23.RecordTypeId = OEId;
    crf23.Opportunity__c = o.Id;
    crf23.Contact__c = c.Id;
    crf23.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    crf23.Vol_ref_mover__c='VOL011-00000000000';
    crf23.VOL_ref__c='na';
    insert crf23;
    
    }
}