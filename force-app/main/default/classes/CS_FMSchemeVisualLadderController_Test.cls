@isTest
private class CS_FMSchemeVisualLadderController_Test {

    @isTest
    private static void constructor_Test() {
        cspmb__Price_Item__c teamVoicePlan = CS_TestDataFactory.generatePriceItem(FALSE, 'Unlimited Minutes & Texts');
        teamVoicePlan.cspmb__Is_Active__c = TRUE;
        teamVoicePlan.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamVoicePlan.Grouping__c = 'Voice';
        teamVoicePlan.Module__c = 'Team';
        teamVoicePlan.cspmb__Recurring_Charge__c = 10;
        INSERT teamVoicePlan;
        cspmb__Price_Item__c teamVoiceScheme = CS_TestDataFactory.generatePriceItem(FALSE, 'Scheme 3');
        teamVoiceScheme.cspmb__Is_Active__c = TRUE;
        teamVoiceScheme.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamVoiceScheme.Grouping__c = 'Voice';
        teamVoiceScheme.Module__c = 'Scheme';
        teamVoiceScheme.cspmb__Recurring_Charge__c = -3;
        teamVoiceScheme.cspmb__Master_Price_item__c = teamVoicePlan.Id;
        INSERT teamVoiceScheme;
        
        cspmb__Price_Item__c teamDataPlan = CS_TestDataFactory.generatePriceItem(FALSE, 'Team Data 1GB Contribution');
        teamDataPlan.cspmb__Is_Active__c = TRUE;
        teamDataPlan.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamDataPlan.Grouping__c = 'Data';
        teamDataPlan.Module__c = 'Team';
        teamDataPlan.cspmb__Recurring_Charge__c = 6;
        INSERT teamDataPlan;
        cspmb__Price_Item__c teamDataScheme = CS_TestDataFactory.generatePriceItem(FALSE, 'Scheme 1');
        teamDataScheme.cspmb__Is_Active__c = TRUE;
        teamDataScheme.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamDataScheme.Grouping__c = 'Data';
        teamDataScheme.Module__c = 'Scheme';
        teamDataScheme.cspmb__Recurring_Charge__c = -1;
        teamDataScheme.cspmb__Master_Price_item__c = teamVoicePlan.Id;
        INSERT teamDataScheme;

        Test.startTest();
            PageReference pageRef = Page.CS_FMSchemeVisualLadder;
            Test.setCurrentPage(pageRef);
            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('teamVoicePlan', 'Unlimited Minutes & Texts');
            ApexPages.currentPage().getParameters().put('teamVoiceScheme', 'Scheme 3');
            ApexPages.currentPage().getParameters().put('teamDataPlan', 'Team Data 1GB Contribution');
            ApexPages.currentPage().getParameters().put('teamDataScheme', 'Scheme 1');
            CS_FMSchemeVisualLadderController smSchLadder = new CS_FMSchemeVisualLadderController();
        Test.stopTest();
        
        System.assertEquals(1, smSchLadder.voiceSchemeLadderData.size());
        System.assertEquals(teamVoicePlan.Name, smSchLadder.voiceSchemeLadderData[0].name);
        System.assertEquals(teamVoicePlan.cspmb__Recurring_Charge__c, smSchLadder.voiceSchemeLadderData[0].price);
        System.assertEquals(teamVoicePlan.cspmb__Recurring_Charge__c + teamVoiceScheme.cspmb__Recurring_Charge__c, smSchLadder.voiceSchemeLadderData[0].sch3DiscountedPrice);
        System.assertEquals(TRUE, smSchLadder.voiceSchemeLadderData[0].sch3Selected);
        
        System.assertEquals(1, smSchLadder.dataSchemeLadderData.size());
        System.assertEquals(teamDataPlan.Name, smSchLadder.dataSchemeLadderData[0].name);
        System.assertEquals(teamDataPlan.cspmb__Recurring_Charge__c, smSchLadder.dataSchemeLadderData[0].price);
        System.assertEquals(teamDataPlan.cspmb__Recurring_Charge__c + teamDataScheme.cspmb__Recurring_Charge__c, smSchLadder.dataSchemeLadderData[0].sch1DiscountedPrice);
        System.assertEquals(TRUE, smSchLadder.dataSchemeLadderData[0].sch1Selected);
    }
    
    @isTest
    private static void constructor_Test2() {
        cspmb__Price_Item__c teamVoicePlan = CS_TestDataFactory.generatePriceItem(FALSE, 'Unlimited Minutes & Texts');
        teamVoicePlan.cspmb__Is_Active__c = TRUE;
        teamVoicePlan.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamVoicePlan.Grouping__c = 'Voice';
        teamVoicePlan.Module__c = 'Team';
        teamVoicePlan.cspmb__Recurring_Charge__c = 10;
        INSERT teamVoicePlan;
        cspmb__Price_Item__c teamVoiceScheme = CS_TestDataFactory.generatePriceItem(FALSE, 'Scheme 3');
        teamVoiceScheme.cspmb__Is_Active__c = TRUE;
        teamVoiceScheme.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamVoiceScheme.Grouping__c = 'Voice';
        teamVoiceScheme.Module__c = 'Scheme';
        teamVoiceScheme.cspmb__Recurring_Charge__c = -3;
        teamVoiceScheme.cspmb__Master_Price_item__c = teamVoicePlan.Id;
        INSERT teamVoiceScheme;
        
        cspmb__Price_Item__c teamDataPlan = CS_TestDataFactory.generatePriceItem(FALSE, 'Team Data 1GB Contribution');
        teamDataPlan.cspmb__Is_Active__c = TRUE;
        teamDataPlan.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamDataPlan.Grouping__c = 'Data';
        teamDataPlan.Module__c = 'Team';
        teamDataPlan.cspmb__Recurring_Charge__c = 6;
        INSERT teamDataPlan;
        cspmb__Price_Item__c teamDataScheme = CS_TestDataFactory.generatePriceItem(FALSE, 'Scheme 1');
        teamDataScheme.cspmb__Is_Active__c = TRUE;
        teamDataScheme.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamDataScheme.Grouping__c = 'Data';
        teamDataScheme.Module__c = 'Scheme';
        teamDataScheme.cspmb__Recurring_Charge__c = -1;
        teamDataScheme.cspmb__Master_Price_item__c = teamVoicePlan.Id;
        INSERT teamDataScheme;

        Test.startTest();
            PageReference pageRef = Page.CS_FMSchemeVisualLadder;
            Test.setCurrentPage(pageRef);
            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('teamVoicePlan', 'Unlimited Minutes & Texts');
            ApexPages.currentPage().getParameters().put('teamVoiceScheme', 'Scheme 1');
            ApexPages.currentPage().getParameters().put('teamDataPlan', 'Team Data 1GB Contribution');
            ApexPages.currentPage().getParameters().put('teamDataScheme', 'Scheme 3');
            CS_FMSchemeVisualLadderController smSchLadder = new CS_FMSchemeVisualLadderController();
        Test.stopTest();
        
        System.assertEquals(1, smSchLadder.voiceSchemeLadderData.size());
        System.assertEquals(teamVoicePlan.Name, smSchLadder.voiceSchemeLadderData[0].name);
        System.assertEquals(teamVoicePlan.cspmb__Recurring_Charge__c, smSchLadder.voiceSchemeLadderData[0].price);
        System.assertEquals(teamVoicePlan.cspmb__Recurring_Charge__c + teamVoiceScheme.cspmb__Recurring_Charge__c, smSchLadder.voiceSchemeLadderData[0].sch3DiscountedPrice);
        System.assertEquals(FALSE, smSchLadder.voiceSchemeLadderData[0].sch3Selected);
        
        System.assertEquals(1, smSchLadder.dataSchemeLadderData.size());
        System.assertEquals(teamDataPlan.Name, smSchLadder.dataSchemeLadderData[0].name);
        System.assertEquals(teamDataPlan.cspmb__Recurring_Charge__c, smSchLadder.dataSchemeLadderData[0].price);
        System.assertEquals(teamDataPlan.cspmb__Recurring_Charge__c + teamDataScheme.cspmb__Recurring_Charge__c, smSchLadder.dataSchemeLadderData[0].sch1DiscountedPrice);
        System.assertEquals(FALSE, smSchLadder.dataSchemeLadderData[0].sch1Selected);
    }
    
    @isTest
    private static void constructor_Test3() {
        cspmb__Price_Item__c teamVoicePlan = CS_TestDataFactory.generatePriceItem(FALSE, 'Unlimited Minutes & Texts');
        teamVoicePlan.cspmb__Is_Active__c = TRUE;
        teamVoicePlan.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamVoicePlan.Grouping__c = 'Voice';
        teamVoicePlan.Module__c = 'Team';
        teamVoicePlan.cspmb__Recurring_Charge__c = 10;
        INSERT teamVoicePlan;
        cspmb__Price_Item__c teamVoiceScheme = CS_TestDataFactory.generatePriceItem(FALSE, 'Scheme 1');
        teamVoiceScheme.cspmb__Is_Active__c = TRUE;
        teamVoiceScheme.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamVoiceScheme.Grouping__c = 'Voice';
        teamVoiceScheme.Module__c = 'Scheme';
        teamVoiceScheme.cspmb__Recurring_Charge__c = -3;
        teamVoiceScheme.cspmb__Master_Price_item__c = teamVoicePlan.Id;
        INSERT teamVoiceScheme;
        
        cspmb__Price_Item__c teamDataPlan = CS_TestDataFactory.generatePriceItem(FALSE, 'Team Data 1GB Contribution');
        teamDataPlan.cspmb__Is_Active__c = TRUE;
        teamDataPlan.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamDataPlan.Grouping__c = 'Data';
        teamDataPlan.Module__c = 'Team';
        teamDataPlan.cspmb__Recurring_Charge__c = 6;
        INSERT teamDataPlan;
        cspmb__Price_Item__c teamDataScheme = CS_TestDataFactory.generatePriceItem(FALSE, 'Scheme 1');
        teamDataScheme.cspmb__Is_Active__c = TRUE;
        teamDataScheme.cspmb__Product_Definition_Name__c = 'Service Plan';
        teamDataScheme.Grouping__c = 'Data';
        teamDataScheme.Module__c = 'Scheme';
        teamDataScheme.cspmb__Recurring_Charge__c = -1;
        teamDataScheme.cspmb__Master_Price_item__c = teamVoicePlan.Id;
        INSERT teamDataScheme;

        Test.startTest();
            PageReference pageRef = Page.CS_FMSchemeVisualLadder;
            Test.setCurrentPage(pageRef);
            // Add parameters to page URL
            ApexPages.currentPage().getParameters().put('teamVoicePlan', 'Unlimited Minutes & Texts');
            ApexPages.currentPage().getParameters().put('teamVoiceScheme', 'Scheme 1');
            ApexPages.currentPage().getParameters().put('teamDataPlan', 'Team Data 1GB Contribution');
            ApexPages.currentPage().getParameters().put('teamDataScheme', 'Scheme 3');
            CS_FMSchemeVisualLadderController smSchLadder = new CS_FMSchemeVisualLadderController();
            
            teamVoiceScheme.Name = 'Scheme 2';
            teamDataScheme.Name = 'Scheme 2';
            smSchLadder = new CS_FMSchemeVisualLadderController();
        Test.stopTest();

    }
}