@isTest

private class Test_ContractLoad{
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
    
        insert Test_Factory.CreateAccountForDummy();
        //Test a Corp (LOB Level Task)
        User thisUser = new User (id=UserInfo.getUserId(), Run_Apex_Triggers__c = true, Apex_Trigger_Account__c=false );
        update thisUser;    
        
        String sacname = 'TestName';
        String LOB='FIELDM';
           
        List<Contract> allContract = new List<Contract>();
           
        Account testAcc = Test_Factory.CreateAccount();
        testAcc.name = 'TESTCODE';
        testAcc.LE_Code__c = null;
        testAcc.Sector_code__c='CORP';
        testAcc.LOB_Code__c=LOB;
        Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc});  
           
        // insert Contract to be allocated against DummyID as SAC is Unknown
        Contract con1 = Test_Factory.CreateContract(accResult[0].getId());
        con1.Adder_unique__c = '1';
        con1.Status = 'Draft';
        con1.Contract_Term_years__c = 2;
        con1.StartDate = Date.today().addMonths(-1);
        con1.Extract_Date__c = Date.today();        
        allContract.add(con1);
                                 
        // insert £200 REd Tier & Commitment TFL
        Contract con2 = Test_Factory.CreateContract(accResult[0].getId());
        con2.Adder_unique__c = '2';
        con2.Status = 'Draft';
        con2.Contract_Term_years__c = 2;
        con2.StartDate = (Date.today()-400);
        con2.Current_Monitor_Spend_Date__c = Date.today();
        con2.Extract_Date__c = Date.today();
        allContract.add(con2); 

        // insert £250 AMBER Tier & Commitment TFL
        Contract con3 = Test_Factory.CreateContract(accResult[0].getId());
        con3.Adder_unique__c = '3';
        con3.Status = 'Draft';
        con3.Contract_Term_years__c = 1;
        con3.StartDate = Date.today().addMonths(-7);
        con3.Extract_Date__c = Date.today();
        allContract.add(con3);          
        
        // insert £750 Committed Spend = ZERO
        Contract con5 = Test_Factory.CreateContract(accResult[0].getId());
        con5.Adder_unique__c = '5';
        con5.Status = 'Draft';
        con5.Contract_Term_years__c = 2;
        con5.StartDate = Date.today().addMonths(-10);
        con5.Tier_Value__c = '£750';
        con5.Committed_Spend__c = 0;
        con5.Current_Monitor_Spend_Date__c = con5.StartDate.addMonths(+6);
        con5.Current_Monitor_Spend__c = 251;
        con5.Extract_Date__c = Date.today();
        allContract.add(con5);
        // insert £5K Committed Spend = NULL

        Contract con6 = Test_Factory.CreateContract(accResult[0].getId());
        con6.Adder_unique__c = '6';
        con6.Status = 'Draft';
        con6.Contract_Term_years__c = 2;
        con6.StartDate = Date.today().addMonths(-14);
        con6.Tier_Value__c = '£5K';
        con6.Committed_Spend__c = null;
        con6.Current_Monitor_Spend_Date__c = con6.StartDate.addMonths(+6);
        con6.Current_Monitor_Spend__c = 251;
        con6.Extract_Date__c = Date.today();
        allContract.add(con6);
        
        //STANDARD CONTRACTS        
        Contract con1001 = Test_Factory.CreateContract(accResult[0].getId());
        con1001.Adder_unique__c = '1';
        con1001.Status = 'Draft';
        con1001.Contract_Term_years__c = 2;
        con1001.StartDate = Date.today().addMonths(-3);
        con1001.Extract_Date__c = Date.today().addMonths(-7);
        con1001.Tier_Value__c = null;
        con1001.Committed_Spend__c = 500;
        con1001.Current_Monitor_Spend_Date__c = con1001.StartDate.addMonths(+1);
        con1001.Current_Monitor_Spend__c = 1;
        // insert con1001;
        allContract.add(con1001);      
        
        // (c.Current_Monitor_Spend__c < 0.01 || c.Current_Monitor_Spend__c == null){
        Contract con1002 = Test_Factory.CreateContract(accResult[0].getId());
        con1002.Adder_unique__c = '2';
        con1002.Tier_Value__c = '£200';
        con1002.Status = 'Draft';
        con1002.Contract_Term_years__c = 1;
        con1002.StartDate = (Date.today()-363);
        con1002.Extract_Date__c = Date.today();
        con1002.Current_Monitor_Spend_Date__c = con1002.StartDate.addMonths(+1);
        con1002.Current_Monitor_Spend__c = 0;
        allContract.add(con1002);
        
        //  if (c.Last_Rollover_Date__c > c.StartDate)
        Contract con1004 = Test_Factory.CreateContract(accResult[0].getId());
        con1004.Adder_unique__c = '4';
        con1004.Status = 'Draft';
        con1004.Contract_Term_years__c = 1;
        con1004.StartDate = Date.today().addMonths(-6);
        con1004.Last_Rollover_Date__c = Date.today().addMonths(-5);
        con1004.Tier_Value__c = '£750';
        con1004.Committed_Spend__c = 1;
        con1004.Current_Monitor_Spend_Date__c = con1004.StartDate.addMonths(+3);
        con1004.Current_Monitor_Spend__c = 180;
        con1004.Extract_Date__c = Date.today();
        allContract.add(con1004);
        
        
        // insert £5K RED TFL
        Contract con1005 = Test_Factory.CreateContract(accResult[0].getId());
        con1005.Adder_unique__c = '5';
        con1005.Status = 'Draft';
        con1005.Contract_Term_years__c = 2;
        con1005.StartDate = Date.today().addMonths(-6);
        con1005.Tier_Value__c = '£5K';
        con1005.Committed_Spend__c = 1;
        con1005.Current_Monitor_Spend_Date__c = con1005.StartDate.addMonths(+3);
        con1005.Current_Monitor_Spend__c = 100;
        con1005.Extract_Date__c = Date.today();
        allContract.add(con1005);        

        //Amber TFL 
        Contract con1006 = Test_Factory.CreateContract(accResult[0].getId());
        con1006.Adder_unique__c = '6';
        con1006.Status = 'Draft';
        con1006.Contract_Term_years__c = 2;
        con1006.StartDate = Date.today().addMonths(-18);
        con1006.Tier_Value__c = '£5K';
        con1006.Committed_Spend__c = null;
        con1006.Current_Monitor_Spend_Date__c = con1006.StartDate.addMonths(+15);
        con1006.Current_Monitor_Spend__c = 1200;
        con1006.Extract_Date__c = Date.today();
        allContract.add(con1006);
        
        // insert £30K Current_Monitor_Spend = NULL
        Contract con1008 = Test_Factory.CreateContract(accResult[0].getId());
        con1008.Adder_unique__c = '8';
        con1008.Status = 'Draft';
        con1008.Contract_Term_years__c = 2;
        con1008.StartDate = Date.today().addMonths(-1);
        con1008.Tier_Value__c = '£30K';
        con1008.Committed_Spend__c = 500;
        con1008.Current_Monitor_Spend_Date__c = con1008.StartDate.addMonths(+6);
        con1008.Current_Monitor_Spend__c = null;
        con1008.Extract_Date__c = Date.today();
        allContract.add(con1008);

        Contract conT1 = Test_Factory.CreateContract(accResult[0].getId());
        conT1.Adder_unique__c = '8';
        conT1.Status = 'Draft';
        conT1.Contract_Term_years__c = 2;
        conT1.StartDate = Date.today().addMonths(-1);
        conT1.Tier_Value__c = '£10K';
        conT1.Committed_Spend__c = 500;
        conT1.Current_Monitor_Spend_Date__c = con1008.StartDate.addMonths(+6);
        conT1.Current_Monitor_Spend__c = null;
        conT1.Extract_Date__c = Date.today();
        allContract.add(conT1);

        Contract conT2 = Test_Factory.CreateContract(accResult[0].getId());
        conT2.Adder_unique__c = '8';
        conT2.Status = 'Draft';
        conT2.Contract_Term_years__c = 2;
        conT2.StartDate = Date.today().addMonths(-1);
        conT2.Tier_Value__c = '£10K';
        conT2.Committed_Spend__c = 500;
        conT2.Current_Monitor_Spend_Date__c = con1008.StartDate.addMonths(+6);
        conT2.Current_Monitor_Spend__c = null;
        conT2.Extract_Date__c = Date.today();
        allContract.add(conT2);       

        // insert contract to cover tiers missing         
        Contract MT1 = Test_Factory.CreateContract(accResult[0].getId());
        MT1.Status = 'Draft';
        MT1.StartDate = Date.today().addMonths(-1);
        MT1.Tier_Value__c = '£0K';        
        allContract.add(MT1); 

        Contract MT2 = Test_Factory.CreateContract(accResult[0].getId());
        MT2.Status = 'Draft';
        MT2.StartDate = Date.today().addMonths(-1);
        MT2.Tier_Value__c = '£250';
        allContract.add(MT2);
        
        Contract MT3 = Test_Factory.CreateContract(accResult[0].getId());
        MT3.Status = 'Draft';
        MT3.StartDate = Date.today().addMonths(-1);
        MT3.Tier_Value__c = '£15K';
        allContract.add(MT3);
        
        Contract MT4 = Test_Factory.CreateContract(accResult[0].getId());
        MT4.Status = 'Draft';
        MT4.StartDate = Date.today().addMonths(-1);
        MT4.Tier_Value__c = '£30,000';
        allContract.add(MT4);
        
        Contract MT5 = Test_Factory.CreateContract(accResult[0].getId());
        MT5.Status = 'Draft';
        MT5.StartDate = Date.today().addMonths(-1);
        MT5.Tier_Value__c = '£50K';
        allContract.add(MT5);
        
        Contract MT6 = Test_Factory.CreateContract(accResult[0].getId());
        MT6.Status = 'Draft';
        MT6.StartDate = Date.today().addMonths(-1);
        MT6.Tier_Value__c = '£100K';
        allContract.add(MT6);
        
        Contract MT7 = Test_Factory.CreateContract(accResult[0].getId());
        MT7.Status = 'Draft';
        MT7.StartDate = Date.today().addMonths(-1);
        MT7.Tier_Value__c = '£200K';
        allContract.add(MT7);
        
        Contract MT8 = Test_Factory.CreateContract(accResult[0].getId());
        MT8.Status = 'Draft';
        MT8.StartDate = Date.today().addMonths(-1);
        MT8.Tier_Value__c = '£250K';
        allContract.add(MT8);

        Contract MT9 = Test_Factory.CreateContract(accResult[0].getId());
        MT9.Status = 'Draft';
        MT9.StartDate = Date.today().addMonths(-1);
        MT9.Tier_Value__c = '£400K';
        allContract.add(MT9);    
         
        Integer x = 0;
        for(Contract c:allContract){
        	x += 1;
        	System.debug(x);
        	c.SAC_Code__c = testAcc.sac_code__c;
        	//insert c;
        }
        
        insert allContract;   
    }   
}