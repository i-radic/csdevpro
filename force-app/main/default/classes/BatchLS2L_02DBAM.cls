global class BatchLS2L_02DBAM implements Database.Batchable<sobject>{
private String query;
public Integer i = 0;
   
    global BatchLS2L_02DBAM(String q){
        this.query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Lead> L2L = new List<Lead>();
        Set<id> acc = new Set<id>();
        for(Sobject ls : scope){
            Landscape_DBAM__c lsrec= (Landscape_DBAM__c)ls;
            String DescStr = 'The alert has been generated based on a landscape with an upcoming contract expiry date in the following area(s):\n';
            if (lsrec.xLeads_Flag_Calls__c == 'True'){
                  DescStr = DescStr + '\n - Calls' ;
            } 
            if (lsrec.xLeads_Flag_Lines__c == 'True'){
                  DescStr = DescStr + '\n - Lines';
            } 
            if (lsrec.xLeads_Flag_Mobile__c == 'True'){
                  DescStr = DescStr + '\n - Mobile';
            } 
            if (lsrec.xLeads_Flag_WAN__c == 'True'){
                  DescStr = DescStr + '\n - WAN';
            } 
            /*
            if (lsrec.xLeads_Flag_BB__c == 'True'){
                  DescStr = DescStr + '\n Broadband';
            }    
            if (lsrec.xLeads_Flag_Switch__c == 'True'){
                  DescStr = DescStr + '\n Switch';
            } 
            if (lsrec.xLeads_Flag_LAN__c == 'True'){
                  DescStr = DescStr + '\n LAN';
            } 
            */
   
            system.debug('JMM lsrec.Id' + lsrec.Id);
            Lead L2Lrec = new Lead();
            //check customer name length and trim to 60 chars
            i = lsrec.Account__r.Name.length();
            if (i > 60) {
                i = 60;
            }              
            L2Lrec.Title = lsrec.Account__r.Name;
            L2Lrec.LastName = 'Landscape Lead: '+ lsrec.Account__r.Name.substring(0,i);
            L2Lrec.Company = lsrec.Account__r.Name;
            L2Lrec.Owner_Department__c = lsrec.Account__r.Owner.Department;
            L2Lrec.Account__c = lsrec.Account__c;
            L2Lrec.Description = DescStr;
            L2Lrec.Landscape_DBAM__c = lsrec.Id;
            L2Lrec.LeadSource= 'Desk Landscape Alert';
            L2Lrec.OwnerId = lsrec.Account__r.OwnerId;
            //L2Lrec.Contact__c = '003P000000XjJvk';
            L2L.add(L2Lrec);
            
            acc.add(lsrec.Account__c);
            
        }       
        insert L2L;
        
        Map<Id, Account> acctsToUpdate = new Map<ID, Account>([select Id, xLead_Last_Created__c FROM Account WHERE Id in :acc]);    
        for (Account acct : acctsToUpdate.values()) {
            acct.xLead_Last_Created__c = System.Today();
        }
        StaticVariables.setAccountAfterDontRun(True); 
        update acctsToUpdate.values();
    }
    
global void finish(Database.BatchableContext BC){
    //create next batch
    if(!Test.isRunningTest()){  
        BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
    
        DateTime n = datetime.now().addMinutes(2);
        String cron = '';
    
        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();
    
        b.scheduled_id3__c = System.schedule('LS2L Batch 3', cron, new BatchLS2L_Schedule03());
    
        update b; 
    }            
}
}