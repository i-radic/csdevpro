@isTest(seeAllData = true)
private class addressResultsFlowCRF_Test {
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        addressDet.Type__c='CeaseAddress';
        
        PageReference PageRef = Page.FlowAddressResultsCRF;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Type__c);
        
        addressResultsFlowCRF addResultsCRF = new addressResultsFlowCRF(addressDet);
        
        addResultsCRF.refferalPage = 'new';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    static testMethod void runPositiveTestCases1() {
        Test_Factory.SetProperty('IsTest', 'yes');
        
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();                
        
        addressDet.Type__c='CeaseAddress';
        
        
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Type__c);
        addressResultsFlowCRF addResultsCRF = new addressResultsFlowCRF(addressDet);
        
        addResultsCRF.refferalPage = 'update';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    static testMethod void runPositiveTestCases2() {
        Test_Factory.SetProperty('IsTest', 'yes');
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        addressDet.Type__c='CeaseAddress';
        
        
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Type__c);
        
        addressResultsFlowCRF addResultsCRF = new addressResultsFlowCRF(addressDet);
        addResultsCRF.refferalPage = 'detail';
        addResultsCRF.postcode = 'GU195QT';
        addResultsCRF.getTest();
        system.debug('getTest');
        addResultsCRF.getAddresses();
        system.debug('getAddresses');
        addResultsCRF.getSelected();
        system.debug('getSelected');
        addResultsCRF.getselectedAddress(); 
        system.debug('getselectedAddress');        
        addResultsCRF.CreateAddress();
        system.debug('CreateAddress');
        addResultsCRF.ReDirect();
        system.debug('ReDirect'); 
        addResultsCRF.Back();
        system.debug('Back');
    }
    
    static testMethod void runPositiveTestCases4() {
               
        Flow_Address_Details__c addressDet = new Flow_Address_Details__c();
        addressDet.Type__c='CeaseAddress';
        ApexPages.currentPage().getParameters().put('AddType',addressDet.Type__c);
        ApexPages.StandardController con = new ApexPages.StandardController(addressDet ); 
        addressResultsFlowCRF addSearchResults = new addressResultsFlowCRF (con);
        
        Test_Factory.SetProperty('IsTest', 'No');
        addSearchResults.Back();        
        system.debug('Back');
        addSearchResults.ReDirect();        
        system.debug('ReDirect');
        addSearchResults.CreateAddress();
       
    }
    
    
    
}