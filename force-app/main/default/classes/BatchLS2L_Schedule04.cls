global class BatchLS2L_Schedule04 implements Schedulable{
        
global void execute(SchedulableContext sc) {
    //batch for chatter alert
    BatchLS2L_04AlertChatter lCountC = new BatchLS2L_04AlertChatter();
    lCountC.query = 'SELECT Id, Email, Name, zLeadsOwned__c FROM User WHERE isActive = TRUE AND zChannel__c = \'BTLB\' AND zLeadsOwned__c > 0';
    database.executeBatch(lCountC, 200);              
}
}