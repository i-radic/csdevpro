@isTEST

public class EmailServiceHandlerTest {

    static testMethod void myTestMethod()
    {
            Messaging.InboundEmail email = new Messaging.InboundEmail();
            Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
            Messaging.InboundEmail.BinaryAttachment AttFile = new Messaging.InboundEmail.BinaryAttachment();
            String [] FileLines = new List<String>();
            email.subject = 'Test Mail';
            env.fromAddress = 'shruthi.ragula@bt.com';                   
            AttFile.fileName = 'Copy of CPQ.csv';
            AttFile.mimeTypeSubType = 'text/csv';
        
            String Header = 'ID,	Date Ran,	EOL ID,	Number of Bills	,Bill Date Range	,Bill Days	,Bill Type	Account	,Total Number of Voice Connections	,Expected Voice per User	,Expected Voice	,Answer phone	,Calls to EE	,Calls to other UK mobile networks	,Closed user group	,Incoming roaming in EU	,Incoming roaming in ROW	,International calls	Landline	,Other	,Outgoing roaming from EU	,Outgoing roaming from ROW	,Freephone	,Expected SMS	,Expected SMS per User	,Closed User Group SMS	,International numbers text messaging	,MMS	,Other SMS	,Outgoing Roaming SMS	,SMS to EE	,SMS to Other UK Networks	,Expected Data	,Expected Data per User	,Data	,Roaming data in EU	,Roaming data in ROW	,MMS Roaming	,Data Only Connections	,Net Active Voice and Data Connections	,Number of Inactive Voice Connections\n82	,03/04/2018 17:26:35	,eo000127611,	1	,03/01/2018 To 26/01/2018	,24	,EDW 	,RECTICEL LTD	,108	,173.01	,18684.95	,5.11	,0.04	,0	,73.01	,1.07	,0.74	,0.07	,19.53	,0.4,	0	,0	,0.04	,2850	,26	,15.4	,1.47	,2.67	,0.32	,0.18	,27.05	,52.91	,73.98	,0.76	,99.07	,0.93	,0	,0	,18	,126	,0 ';               
            FileLines.add(Header);          
            string csvAsString = FileLines[0];
            csvAsString.split('[\n]');                      
            Blob csvFileBody = blob.valueOf(csvAsString);                       
            AttFile.body = csvFileBody;
            email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {AttFile };        	
            EmailServiceHandlerClass MailHandler = new EmailServiceHandlerClass();
            MailHandler.handleInboundEmail(email, env);
        
        

    }
}