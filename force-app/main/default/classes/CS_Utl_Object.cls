/**
 * @name CS_Utl_Object
 * @description Utility class to manipulate Object
 * @revision
 *
 */
public class CS_Utl_Object {
	public static Map<String, Object> getUntyped(Object obj) {
		try{	        
			return (Map<String, Object>) JSON.deserializeUntyped(Json.serialize(obj));
		}
		catch (System.TypeException ex){
			System.debug('CS_Utl_Object.getUntyped(Object obj) exception: ' + ex.getMessage());
			return null;
		}
	}
}