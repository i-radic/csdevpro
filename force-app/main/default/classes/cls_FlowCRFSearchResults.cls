public class cls_FlowCRFSearchResults {
    
    public List<CRF__c> crfs {get; set;}
    public List<Flow_CRF__c> flowcrfs {get; set;}
    public Boolean flag {get;set;}
    
    public cls_FlowCRFSearchResults()
    {
        flag=true;
        String st= ApexPages.currentPage().getParameters().get('vol');
        crfs = new List<CRF__c>();
        crfs= [select Id,Name,Vol_ref_Mover__c,RecordType.Name,CreatedBy.Name,Opportunity__r.Opportunity_Id__c,Back_Office_Status__c from CRF__c where Vol_ref_Mover__c =: st];
        flowcrfs =  new List<Flow_CRF__c>();
        flowcrfs =[select Id,Name,VOL_BT_Number__c,RecordType.Name,CreatedBy.Name,Opportunity__r.Opportunity_Id__c,Back_Office_Status__c from Flow_CRF__c where VOL_BT_Number__c =: st];        
        if(crfs.size()==0 && flowcrfs.size()==0)
        {
            flag=false;
        }
        system.debug('result list'+crfs +'  ' +flowcrfs);
    }
}