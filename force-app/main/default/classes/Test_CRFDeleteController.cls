@isTest
private class Test_CRFDeleteController {
static testMethod void MyUnitTest() {

 //  set up data
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
        
        Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99991';
        a.Name = 'Test';
        a.ownerId = thisUser.id ;
        a.Base_Team_Assign_Date__c=System.today();
        insert a;
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o; 

        String RTId = [Select Id from RecordType where DeveloperName=:'One_Plan'].Id;
        //  insert CRF - ensure oneplan product is selected to attain coverage
        CRF__c crf = new CRF__c(Opportunity__c = o.Id, 
                                    Contact__c = c.Id, 
                                    Account_number__c = 'ls3456789',
                                    Customer_bank_account_number__c = '111122223',
                                    //Customer_Email__c = 'test@test456.com',
                                    //Customer_phone_number__c = '9632587410',
                                    Alt_contact_phone_number__c = '7854123690',
                                    Order_type__c = 'Linked Delivery', 
                                    Payment_Method__c = 'DD details entered',
                                    DD_Confirm__c = True,
                                    Password__c = 'qwerty12',
                                    Sort_Code__c='123123',
                                    Override_Account_Name__c='Simon',
                                    Your_sales_channel_ID__c='B101',
                                    How_Many_Employees_are_in_your_company__c='10 or less',
                                    OP_Which_One_Plan_is_required__c = CRFController.BT_BUSINESS_ONE_PLAN
                                    );
                                    
        insert crf; 
        
     ApexPages.StandardController stdctr = new ApexPages.StandardController(crf);
     CRFDeleteController ctr=new CRFDeleteController(stdctr);
     PageReference pr=ctr.step1();
     CRFDeleteController.DeleteCRF(crf.id);

}
}