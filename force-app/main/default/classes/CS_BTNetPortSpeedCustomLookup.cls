global class CS_BTNetPortSpeedCustomLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["Contract Term","Geography","Bearer Speed"]'; 
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
        Decimal speed = Decimal.valueOf(searchFields.get('Bearer Speed'));
        List<cspmb__Price_Item__c> uniqueProducts = [SELECT Id, Name, cspmb__One_Off_Charge__c, cspmb__Price_Item_Code__c, cspmb__Price_Item_Description__c, Module__c, Grouping__c, cspmb__Contract_Term__c, Record_Type_Name__c, cspmb__Is_Active__c, cspmb__One_Off_Cost__c, cspmb__Recurring_Charge__c, cspmb__Recurring_Cost__c, PL_Mapping_Revenue__c, PL_Mapping_Cost__c, Bearer_Speed__c, cspmb__Master_Price_item__c, cspmb__Billing_Frequency__c, Product_Name__c, B_Code__c, Geography__c, Access_Type__c, Bandwidth__c FROM cspmb__Price_Item__c WHERE Module__c = 'Port Speed' AND cspmb__Is_Active__c = TRUE AND Record_Type_Name__c = 'BTnet' AND Bearer_Speed__c = :speed AND cspmb__Contract_Term__c = :searchFields.get('Contract Term') AND Geography__c = :searchFields.get('Geography') ORDER By Bandwidth__c];
        
        return uniqueProducts;
    }
}