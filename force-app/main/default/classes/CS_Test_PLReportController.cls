@isTest
private class CS_Test_PLReportController
{
    private static cscfga__Product_Basket__c testBasket;

    private static void createTestData()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account testAcc = new Account
            ( Name = 'TestAccount'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
            , AccountId = testAcc.Id
            , CloseDate = System.today()
            , StageName = 'Closed Won'
            , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;

        cscfga__Product_Definition__c prodDef =
            new cscfga__Product_Definition__c
                ( Name = 'Fixed Line'
                , cscfga__Description__c = 'Test helper' );
        insert prodDef;

        cscfga__Product_Definition__c prodDefNew =
            new cscfga__Product_Definition__c
                ( Name = 'BB Test'
                , cscfga__Description__c = 'Test helper' );
        insert prodDefNew;

        cscfga__Product_Configuration__c config =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = testBasket.Id
                , cscfga__Product_Definition__c = prodDef.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12 );
        insert config;

        cscfga__Product_Configuration__c subConfig =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = testBasket.Id
                , cscfga__Product_Definition__c = prodDefNew.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12
                , cscfga__Root_Configuration__c = config.Id
                , cscfga__Parent_Configuration__c = config.Id );
        insert subConfig;
    }

    private static testMethod void testPLReportController()
    {
        createTestData();

        Test.startTest();

        Test.setCurrentPageReference
            ( new PageReference
                ( '/apex/CS_PLReport?id=' + testBasket.Id ));
        CS_PLReportController ctrl = new CS_PLReportController();
        //ctrl.exportToCSV();
        List<CS_PLReportController.Row> table = ctrl.table;
        List<CS_PLReportController.Data> data = ctrl.data;

        Test.stopTest();
    }
}