/**********************************************************************
    History                                                            
    -------                                                            
           AUTHOR            DATE                                  DETAILS                                 
       Venkata Srinivas    03/09/2014        Change for CR5979-Deleted the code related to Advocacy_DBAM__c.
          Maddirala
*/
global class UKBService{ 
    public class NoMatchingAccountException extends Exception{}
    
    global class AccountSummary {
        public AccountSummary (){
            Opportunities = new List<OpportunitySummary>();
            Landscapes = new List<LandscapeSummary >();
            Campaigns = new List<CampaignSummary>();
            Activities = new List<ActivitySummary>();
            
        }
        
        webservice string AccountId;     
        webservice string CUG;     
        webservice List<OpportunitySummary> Opportunities;
        webservice List<LandscapeSummary > Landscapes;
        webservice List<CampaignSummary> Campaigns;
        webservice List<ActivitySummary> Activities;
    }

    global class OpportunitySummary {
        webservice string Id;
        webservice string OID;
        webservice string OwnerEIN;
        webservice string Description;
        webservice string Stage;
        webservice string Name;
        webservice DateTime CloseDate;
    }
    
    global class LandscapeSummary {
        webservice string Id;
        webservice string TypeOfLandscape;
        webservice string LastModifiedByEIN;
        webservice DateTime LastModifiedDate;
    }
    
    global class CampaignSummary {
        webservice string Id;
        webservice string TypeOfCampaign;
        webservice string Name;
    }
    
    global class ActivitySummary {
        webservice string Id;
        webservice string ActivityType;
        webservice string Title;
        webservice string Status;
        webservice DateTime DueDate;
        webservice DateTime ClosedDate;
    }
    
    //static webservice List<Activity2> GetActivities(string cug){
    //    List<Activity2> retval = new List<Activity2>();
    //    
    //    return retval;
    //}

    static webservice AccountSummary GetAccountSummary(string cug){
        AccountSummary retval = new AccountSummary();
        retval.CUG = cug;
        retval.accountId = GetAccountIdFromCUG(cug);
    
        // Opportunities
        for(Opportunity o : [SELECT id,Opportunity_Id__c,name,stagename,description,closedate  FROM Opportunity WHERE AccountID=:retval.accountId]){
            OpportunitySummary os = new OpportunitySummary();
            os.id = o.id;
            os.oid = o.Opportunity_Id__c;
            os.name = o.name;
            os.stage = o.StageName;
            os.description = o.description;
            os.closedate = o.closedate;
            retval.Opportunities.add(os);
        }

        // Campaigns
        string Account15 = retval.accountID.SubString(0,15);
        for(CampaignMember cm : [SELECT id, Campaign.id, Campaign.Name, Campaign.Campaign_Type__c  FROM CampaignMember WHERE Account__c= :Account15]){
            CampaignSummary cs = new CampaignSummary();
            cs.id = cm.id;
            cs.TypeOfCampaign = cm.Campaign.Campaign_Type__c;
            cs.Name = cm.Campaign.Name;
            retval.Campaigns.add(cs);
        }

        // Landscapes

        for(ADP_Landscape__c adp : [SELECT id, LastModifiedBy.EIN__c, LastModifiedDate FROM ADP_Landscape__c WHERE Account__c=:retval.accountId]){
            LandscapeSummary ls = new LandscapeSummary();
            ls.id = adp .id;
            ls.TypeOfLandscape = 'ADP';
            ls.LastModifiedDate  = adp.LastModifiedDate;
            ls.LastModifiedByEIN = adp.LastModifiedBy.ein__c;
            retval.Landscapes.add(ls);
        }

        for(Landscape_BTLB__c btlb : [SELECT id, LastModifiedBy.EIN__c, LastModifiedDate FROM Landscape_BTLB__c WHERE Customer__c=:retval.accountId]){
            LandscapeSummary ls = new LandscapeSummary();
            ls.id = btlb.id;
            ls.TypeOfLandscape = 'BTLB';
            ls.LastModifiedDate  = btlb.LastModifiedDate;
            ls.LastModifiedByEIN = btlb.LastModifiedBy.ein__c;
            retval.Landscapes.add(ls);
        }

        for(Landscape_DBAM__c dbam : [SELECT id, LastModifiedBy.EIN__c, LastModifiedDate FROM Landscape_DBAM__c WHERE Account__c=:retval.accountId]){
            LandscapeSummary ls = new LandscapeSummary();
            ls.id = dbam.id;
            ls.TypeOfLandscape = 'DBAM';
            ls.LastModifiedDate  = dbam.LastModifiedDate;
            ls.LastModifiedByEIN = dbam.LastModifiedBy.ein__c;
            retval.Landscapes.add(ls);
        }
        
        
        for( Task tsk: [SELECT id, subject, ActivityDate, status  FROM Task WHERE AccountID=:retval.accountId]){
            ActivitySummary ts = new ActivitySummary();
            ts.id = tsk.id;
            ts.ActivityType = 'Task';
            ts.Title = tsk.subject;
            ts.Status  = tsk.Status;
            ts.DueDate = tsk.ActivityDate;
            retval.Activities.add(ts);
        }
        
        for(Event evt: [SELECT  id, subject, ActivityDate, event_status__C FROM Event WHERE AccountID=:retval.accountId]){
            ActivitySummary es = new ActivitySummary();
            es.id = evt.id;
            es.ActivityType = 'Event';
            es.Title = evt.subject;
            es.Status  = evt.event_Status__c;
            es.DueDate = evt.ActivityDate;
            retval.Activities.add(es);
        }
    
        
        return retval;
    }
    
    static webservice OpportunitySummary CreateOpporunity(string CUG, string description, string stage, string ownerEIN, string name, Date closeDate){
        List<Account> accounts = [SELECT id FROM Account WHERE CUG__c=:cug];
        List<User> users = [SELECT id FROM User WHERE EIN__c=:ownerEIN];
        
        Opportunity newOppty = new Opportunity();
        newOppty.Account = accounts[0];
        newOppty.Description = description;
        newOppty.StageName = stage;
        newOppty.name = Name;
        newOppty.CloseDate = CloseDate;
        newOppty.Owner = users[0];        newOppty.Auto_Assign_Owner__c = false;
        
        Database.SaveResult resultOppty  = Database.Insert(newOppty);
        newOppty = [SELECT id,Opportunity_Id__c,name,stagename,description,closedate,Owner.EIN__c  FROM Opportunity WHERE id=:resultOppty.getid() LIMIT 1];
 
        OpportunitySummary os = new OpportunitySummary();
        os.id = newOppty.id;
        os.oid = newOppty.Opportunity_Id__c;
        os.name = newOppty.name;
        os.stage = newOppty.StageName;
        os.description = newOppty.description;
        os.closedate = newOppty.closedate;
        os.OwnerEIN = newOppty.Owner.EIN__c;
        return os;
    }
    
    private static string GetAccountIdFromCUG(string cug){
        List<Account> accounts = [SELECT id FROM Account WHERE CUG__c=:cug];
        if (accounts.isEmpty()){
            throw new NoMatchingAccountException();
        }
        return accounts[0].id;
    }
}