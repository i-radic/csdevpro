public class CS_CloudVoice_CableQualifiCriteriaCtrl {

	public static List<SelectOption> selOptions;
	public String cablingTypes { get; private set; }
	public String infoMessage { get; set; }
	public Boolean displaySendEmailSection { get; set; }
	public Account accountName { get; set; }
	public String siteInstallationAddress { get; set; }
	public String siteContactName { get; set; }
	public String siteContactEmail { get; set; }
	public String siteContactPhone { get; set; }
	public Integer requiredOutlets { get; set; }
	public String surveyClearanceRequired { get; set; }
	public String surveyYear { get; set; }
	public String surveyCablingCategory { get; set; }
	public String surveyExternalCabling { get; set; }
	public String surveyOutOfHoursInstall { get; set; }
	public String surveyNotes { get; set; }

	public List<SelectOption> getSiteVisits() {
		selOptions = new List<SelectOption> ();
		selOptions.add(new SelectOption('', '--None--'));
		selOptions.add(new SelectOption('Yes', 'Yes'));
		selOptions.add(new SelectOption('No', 'No'));
		return selOptions;
	}

	public List<SelectOption> getCablingCategory() {
		selOptions = new List<SelectOption> ();
		selOptions.add(new SelectOption('', '--None--'));
		selOptions.add(new SelectOption('Cat 5', 'Cat 5'));
		selOptions.add(new SelectOption('Cat 6', 'Cat 6'));
		return selOptions;
	}

	public CS_CloudVoice_CableQualifiCriteriaCtrl() {
		String basketId = System.currentPageReference().getParameters().get('basketId');
		String contactName = System.currentPageReference().getParameters().get('contactName');
		String contactEmail = System.currentPageReference().getParameters().get('contactEmail');
		String contactMobile = System.currentPageReference().getParameters().get('contactMobile');
		System.debug(System.currentPageReference().getParameters());
		cscfga__Product_Basket__c basket = [select id, Customer_Contact__c, csbb__Account__c from cscfga__Product_Basket__c where id = :basketId];
		accountName = [select Name from Account where Id = :basket.csbb__Account__c];
		//Contact contactObj = [select Id, Name, Email, Phone from Contact where Id = :basket.Customer_Contact__c];
		//siteContactName = contactObj.Name;
		//siteContactEmail = contactObj.Email;
		//siteContactPhone = contactObj.Phone;
		siteContactName = contactName;
		siteContactEmail = contactEmail;
		siteContactPhone = contactMobile;
		cablingTypes = JSON.serialize([select id, name from cspmb__Price_Item__c where Module__c ='Cabling']);
		displaySendEmailSection = true;
	}

	public PageReference sendEmail() {
		String sendEmailAddress = 'btschelpdesk@ngbailey.co.uk';
		String sendEmailName =  'BT Helpdesk';
		String emlSubject = '';
		String emlPlainTextBody = '';
		String emlHtmlBody = '';
		String emlFrom = '';
		String firstName = UserInfo.getFirstName();
		String lastName = UserInfo.getLastName();
		String userEmail = UserInfo.getUserEmail();
		String sendEmailCompanyName = accountName.Name;


		emlSubject = 'BT Cloud Voice Cabling Survey Request';
		emlHtmlBody = 'SURVEY REQUEST: PLEASE REPLY TO THIS EMAIL WITH THE SURVEY QUOTE.<br/><br/>'
		+ 'Dear ' + sendEmailName + ',<br/><br/>'
		+ '<b>Company Name:&nbsp;&nbsp;</b> ' + sendEmailCompanyName + '<br/>'
		+ '<b>Site Installation Address:&nbsp;&nbsp;</b> ' + siteInstallationAddress + '<br/>'
		+ '<b>Site Contact Name:&nbsp;&nbsp;</b> ' + siteContactName + '<br/>'
		+ '<b>Site Contact Email:&nbsp;&nbsp;</b> ' + siteContactEmail + '<br/>'
		+ '<b>Site Contact Phone:&nbsp;&nbsp;</b> ' + siteContactPhone + '<br/>'
		+ '<b>Required Outlets:&nbsp;&nbsp;</b> ' + requiredOutlets + '<br/>'
		+ '<b>Clearance Required:&nbsp;&nbsp;</b>' + surveyClearanceRequired + '<br/>'
		+ '<b>20 years or older:&nbsp;&nbsp;</b>' + surveyYear + '<br/>'
		+ '<b>Cabling Category:&nbsp;&nbsp;</b>' + surveyCablingCategory + '<br/>'
		+ '<b>External Cabling:&nbsp;&nbsp;</b>' + surveyExternalCabling + '<br/>'
		+ '<b>Out of Hours Install:&nbsp;&nbsp;</b>' + surveyOutOfHoursInstall + '<br/>'
		+ '<b>Notes:&nbsp;&nbsp;</b>' + surveyNotes + '<br/><br/>'
		+ 'Regards<br/>'
		+ firstName + ' ' + lastName + '<br/>'
		+ 'BT Cloud Voice Team<br/>';

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new String[] { sendEmailAddress };
		mail.setToAddresses(toAddresses);
		mail.setUseSignature(false);
		mail.setSubject(emlSubject);
		mail.setPlainTextBody(emlPlainTextBody);
		mail.setHtmlBody(emlHtmlBody);
		mail.setReplyTo(userEmail);
		mail.setSenderDisplayName(firstName);
		try {
			Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			infoMessage = 'Email has been sent to NG Bailey who will contact the customer within 2 working days, survey within 5 working days and return to you a quote within 10 working days. Once you have the survey emailed to you, please upload against the Opportunity and enter pricing in the One Off Charge field on the Cabling screen.';
		} catch(Exception e) {
			infoMessage = e.getMessage();
		}
		displaySendEmailSection = false;
		return null;
	}
}