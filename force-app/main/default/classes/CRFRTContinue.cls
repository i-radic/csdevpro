public with sharing class CRFRTContinue {
    
    public String recordTypes,opptyName,opptyId,opptyAccntName,opptyAccntId, bbLineOf;
    CRF__c crf;
    //Public String returl;
    public CRFRTContinue(ApexPages.StandardController controller) {
    crf = (CRF__c)controller.getRecord();
    opptyName=Apexpages.currentPage().getParameters().get('CF00N20000002o7Kh');
    opptyId=Apexpages.currentPage().getParameters().get('CF00N20000002o7Kh_lkid');
    opptyAccntName=Apexpages.currentPage().getParameters().get('CF00N20000002oGth');
    opptyAccntId=Apexpages.currentPage().getParameters().get('CF00N20000002oGth_lkid');
    bbLineOf=Apexpages.currentPage().getParameters().get('CF00N20000002oGtq');
    //returl = Apexpages.currentPage().getParameters().get('retURL');
    }
    
    public PageReference Continue1(){
    //String crfnewId = returl.substring(1);
    List<RecordType> RC=[Select Id,Name,SOBJECTTYPE,DeveloperName From RecordType where Id=:crf.RecordTypeId];
    
    /*
    List<CRF__c> crfList = [SELECT Id,Name,Opportunity__r.Name,Opportunity__r.AccountId,Opportunity__r.Account.Name,Account_Name_ForFilter__r.Name,Opportunity__c,Account_Name_ForFilter__c FROM CRF__c WHERE Id=:crfnewId];
    if(returl!=null){    
        if(opptyName == null || opptyAccntName== null){
            opptyName = crfList[0].Opportunity__r.Name;
            opptyId = crfList[0].Opportunity__c;
            opptyAccntName = crfList[0].Opportunity__r.Account.Name;
            opptyAccntId = crfList[0].Opportunity__r.AccountId;
        }   
    } 
    */   
    PageReference NextPage;
    if(RC[0].Name=='AX Form')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
        //NextPage = new PageReference('/a2w/e?CF00N200000098SzU='+opptyName+'&CF00N200000098SzU_lkid='+opptyId+'&retURL=/'+opptyId+'&CF00N200000099nt7='+opptyAccntName+'&CF00N200000099nt7_lkid='+opptyAccntId+'&nooverride=1&RecordType=012g00000000iJxAAI&00Ng0000000uCtA=Auto+Number');//This is for Flow CRF 

    if(RC[0].Name=='Broadband')
        NextPage = new PageReference('/a1l/e?CF00N20000002oGtq='+bbLineOf+'&CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='Broadband Flow'){
        if(opptyName.length() >= 79){
            opptyName = opptyName.substring(0,79) + '~';
        }        
        //NextPage = new PageReference('/apex/CRFBroadband?CF00N20000002oGtq='+bbLineOf+'&OppyId='+opptyId+'&OppyName='+opptyName+'&AccountName='+opptyAccntName+'&AccountId='+opptyAccntId);
        //Added As Part of the CR CR4574
        //NextPage = new PageReference('/apex/CRFBroadband?CF00N20000002oGtq='+bbLineOf+'&OppyId='+opptyId+'&OppyName='+opptyName+'&AccountName='+opptyAccntName+'&AccountId='+opptyAccntId + '&profid=' + UserInfo.getProfileId());
        NextPage = new PageReference('/apex/FlowBroadbandCRF?CF00N20000002oGtq='+bbLineOf+'&OppyId='+opptyId+'&OppyName='+opptyName+'&AccountName='+opptyAccntName+'&AccountId='+opptyAccntId + '&profid=' + UserInfo.getProfileId());        
    }
    if(RC[0].Name=='Direct Debit')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    
     if(RC[0].Name=='Business Mover'){
       NextPage = new PageReference('/apex/CRFBusinessMovers?OppyId='+opptyId+'&OppyName='+opptyName+'&AccountName='+opptyAccntName+'&AccountId='+opptyAccntId);
    }
    
    if(RC[0].Name=='ISDN30'){
        CRF__c crf1 = new CRF__c();
        crf1.RecordTypeId = RC[0].Id;
        crf1.Opportunity__c=opptyId;
        crf1.Account_Name_ForFilter__c=opptyAccntId;
        crf1.Name='Auto Number';
        insert crf1;
        
        ISDN_CRF__c ISDNCRF = new ISDN_CRF__c();
        ISDNCRF.Related_to_CRF__c = crf1.Id;
        insert ISDNCRF;
        
        Channels__c channel = new Channels__c();
        channel.Related_to_CRF__c = crf1.Id;
        insert channel;
        
        
        //NextPage = new PageReference('/apex/VF_ISDN_New?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+ISDNRTId);
        NextPage = new PageReference('/apex/VF_ISDN_Edit?id='+crf1.Id+'&isdncrfId='+ISDNCRF.Id+'&channelId='+channel.Id);
    }
    if(RC[0].Name=='Mover with existing BB')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);    
    if(RC[0].Name=='Movers')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='Name Change')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='NSO')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='NSO Flow')
        NextPage = new PageReference('/apex/NSO_FLow?00N200000098SzU='+opptyId+'&OppyName='+opptyName+'&AccountName='+opptyAccntName+'&AccountId='+opptyAccntId);     
    if(RC[0].Name=='Number Port')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='Olympics')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='One Plan')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='One Plan and Broadband')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='Resign')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='WLR3 Authorisation')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);
    if(RC[0].Name=='NCP')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);    
    if(RC[0].Name=='Business Complete')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);    
    if(RC[0].Name=='Offer Exception')
        NextPage = new PageReference('/a1l/e?CF00N20000002o7Kh='+opptyName+'&CF00N20000002o7Kh_lkid='+opptyId+'&Name=Auto+Number&retURL=/'+opptyId+'&CF00N20000002oGth='+opptyAccntName+'&CF00N20000002oGth_lkid='+opptyAccntId+'&nooverride=1&RecordType='+crf.RecordTypeId);    
    if(RC[0].Name=='Calling Plans/Packages')
        NextPage = new PageReference('/apex/CallingPlansFlowCRF?00N200000098SzU='+opptyId+'&OppyName='+opptyName+'&AccountName='+opptyAccntName+'&AccountId='+opptyAccntId + '&profid=' + UserInfo.getProfileId());    
    
    NextPage.setRedirect(true);
    return NextPage;
    }
    
    }