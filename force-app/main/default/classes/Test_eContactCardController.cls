@isTest
private class Test_eContactCardController {
    
     static testMethod void myUnitTest() {
        
        Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
        
        Account a = Test_Factory.CreateAccount();
        a.Name = 'Testing';
        a.LE_CODE__c = 'T-99999';
        insert a;
        
        Contact con = Test_Factory.CreateContact();
        con.AccountId = a.Id;
        con.Cug__c = 'cugAlan1';
        con.Contact_Post_Code__c = 'WR5 3RL';
        insert con;
        
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        
        User u1 = new User();
        u1.Username = '999999000@bt.com';
        u1.Ein__c = '999999000';
        u1.LastName = 'TestLastname';
        u1.FirstName = 'TestFirstname';
        u1.MobilePhone = '07918672032';
        u1.Phone = '02085878834';
        u1.Title='What i do';
        u1.OUC__c = 'DKW';
        u1.Manager_EIN__c = '123456789';
        u1.Email = 'no.reply@bt.com';
        u1.Alias = 'boatid01';
        u1.TIMEZONESIDKEY = 'Europe/London';
        u1.LOCALESIDKEY  = 'en_GB';
        u1.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u1.PROFILEID = profileId;
        u1.LANGUAGELOCALEKEY = 'en_US';       
        u1.email = 'no.reply@bt.com';
        insert u1;

        User u2 = new User();
        u2.Username = '999999001@bt.com';
        u2.Ein__c = '999999001';
        u2.LastName = 'TestLastname';
        u2.FirstName = 'TestFirstname';
        u2.MobilePhone = '07918672032';
        u2.Phone = '02085878834';
        u2.Title='What i do';
        u2.OUC__c = 'DKW';
        u2.Manager_EIN__c = '999999000';
        u2.ManagerId = u1.id;
        u2.Email = 'no.reply@bt.com';
        u2.Alias = 'boatid01';
        u2.TIMEZONESIDKEY = 'Europe/London';
        u2.LOCALESIDKEY  = 'en_GB';
        u2.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u2.PROFILEID = profileId;
        u2.LANGUAGELOCALEKEY = 'en_US';
        u2.email = 'no.reply@bt.com';
        insert u2;
 
        User u3 = new User();
        u3.Username = '999999002@bt.com';
        u3.Ein__c = '999999002';
        u3.LastName = 'TestLastname';
        u3.FirstName = 'TestFirstname';
        u3.MobilePhone = '07918672032';
        u3.Phone = '02085878834';
        u3.Title='What i do';
        u3.OUC__c = 'DKW';
        u3.Manager_EIN__c = '999999001';
        u3.ManagerId = u2.id;
        u3.Email = 'no.reply@bt.com';
        u3.Alias = 'boatid01';
        u3.managerID = u2.Id;
        u3.TIMEZONESIDKEY = 'Europe/London';
        u3.LOCALESIDKEY  = 'en_GB';
        u3.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u3.PROFILEID = profileId;
        u3.LANGUAGELOCALEKEY = 'en_US';
        u3.email = 'no.reply@bt.com';
        insert u3;
        
        User u4 = new User();
        u4.Username = '999999003@bt.com';
        u4.Ein__c = '999999003';
        u4.LastName = 'TestLastname';
        u4.FirstName = 'TestFirstname';
        u4.MobilePhone = '07918672032';
        u4.Phone = '02085878834';
        u4.Title='What i do';
        u4.OUC__c = 'DKW';
        u4.Manager_EIN__c = '999999002';
        u4.ManagerId = u3.id;
        u4.Email = 'no.reply@bt.com';
        u4.Alias = 'boatid01';
        u4.managerID = u3.id;
        u4.TIMEZONESIDKEY = 'Europe/London';
        u4.LOCALESIDKEY  = 'en_GB';
        u4.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u4.PROFILEID = profileId;
        u4.LANGUAGELOCALEKEY = 'en_US';
        u4.email = 'no.reply@bt.com';
        insert u4;
        
        User u5 = new User();
        u5.Username = '999999004@bt.com';
        u5.Ein__c = '999999004';
        u5.LastName = 'TestLastname';
        u5.FirstName = 'TestFirstname';
        u5.MobilePhone = '07918672032';
        u5.Phone = '02085878834';
        u5.Title='What i do';
        u5.OUC__c = 'DKW';
        u5.Manager_EIN__c = '999999002';
        u5.ManagerId = u4.Id;
        u5.Email = 'no.reply@bt.com';
        u5.Alias = 'boatid01';
        u5.managerID = u4.Id;
        u5.TIMEZONESIDKEY = 'Europe/London';
        u5.LOCALESIDKEY  = 'en_GB';
        u5.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u5.PROFILEID = profileId;
        u5.LANGUAGELOCALEKEY = 'en_US';
        u5.email = 'no.reply@bt.com';
        insert u5;
        
        User u6 = new User();
        u6.Username = '999999005@bt.com';
        u6.Ein__c = '999999005';
        u6.LastName = 'TestLastname';
        u6.FirstName = 'TestFirstname';
        u6.MobilePhone = '07918672032';
        u6.Phone = '02085878834';
        u6.Title='What i do';
        u6.OUC__c = 'DKW';
        u6.Manager_EIN__c = '123456789';
        u6.ManagerId = u5.Id;
        u6.Email = 'no.reply@bt.com';
        u6.Alias = 'boatid01';
        u6.TIMEZONESIDKEY = 'Europe/London';
        u6.LOCALESIDKEY  = 'en_GB';
        u6.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u6.PROFILEID = profileId;
        u6.LANGUAGELOCALEKEY = 'en_US';       
        u6.email = 'no.reply@bt.com';
        Database.SaveResult[] u6Result = Database.insert(new User [] {u6});

        User u7 = new User();
        u7.Username = '999999006@bt.com';
        u7.Ein__c = '999999006';
        u7.LastName = 'TestLastname';
        u7.FirstName = 'TestFirstname';
        u7.MobilePhone = '07918672032';
        u7.Phone = '02085878834';
        u7.Title='What i do';
        u7.OUC__c = 'DKW';
        u7.Manager_EIN__c = '999999000';
        u7.ManagerId = u6Result[0].id;
        u7.Email = 'no.reply@bt.com';
        u7.Alias = 'boatid01';
        u7.TIMEZONESIDKEY = 'Europe/London';
        u7.LOCALESIDKEY  = 'en_GB';
        u7.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u7.PROFILEID = profileId;
        u7.LANGUAGELOCALEKEY = 'en_US';
        u7.email = 'no.reply@bt.com';
        Database.SaveResult[] u7Result = Database.insert(new User [] {u7});
 
        User u8 = new User();
        u8.Username = '999999007@bt.com';
        u8.Ein__c = '999999007';
        u8.LastName = 'TestLastname';
        u8.FirstName = 'TestFirstname';
        u8.MobilePhone = '07918672032';
        u8.Phone = '02085878834';
        u8.Title='What i do';
        u8.OUC__c = 'DKW';
        u8.Manager_EIN__c = '999999001';
        u8.ManagerId = u7Result[0].id;
        u8.Email = 'no.reply@bt.com';
        u8.Alias = 'boatid01';
        u8.managerID = u7Result[0].Id;
        u8.TIMEZONESIDKEY = 'Europe/London';
        u8.LOCALESIDKEY  = 'en_GB';
        u8.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u8.PROFILEID = profileId;
        u8.LANGUAGELOCALEKEY = 'en_US';
        u8.email = 'no.reply@bt.com';
        Database.SaveResult[] u8Result = Database.insert(new User [] {u8});
        
        User u9 = new User();
        u9.Username = '999999008@bt.com';
        u9.Ein__c = '999999008';
        u9.LastName = 'TestLastname';
        u9.FirstName = 'TestFirstname';
        u9.MobilePhone = '07918672032';
        u9.Phone = '02085878834';
        u9.Title='What i do';
        u9.OUC__c = 'DKW';
        u9.Manager_EIN__c = '999999002';
        u9.ManagerId = u8Result[0].id;
        u9.Email = 'no.reply@bt.com';
        u9.Alias = 'boatid01';
        u9.managerID = u8Result[0].id;
        u9.TIMEZONESIDKEY = 'Europe/London';
        u9.LOCALESIDKEY  = 'en_GB';
        u9.EMAILENCODINGKEY = 'ISO-8859-1';                               
        u9.PROFILEID = profileId;
        u9.LANGUAGELOCALEKEY = 'en_US';
        u9.email = 'no.reply@bt.com';
        insert u9;
        
        Lead_Locator__c lc1 = new Lead_Locator__c();
        lc1.Account__c = a.Id;
        lc1.Person_Role__c = 'Account Owner (Primary)';
        lc1.User__c = u1.Id;
        insert lc1;
        
        
        Test.startTest();
        eContactCardController objE = new eContactCardController();
        objE.eId = a.Id;        
        objE.getgenerateHTMLBody();
        objE.eId = o.Id;        
        objE.getgenerateHTMLBody();
        objE.eId = con.Id;      
        objE.getgenerateHTMLBody();     
        Test.stopTest();
        
     }
}