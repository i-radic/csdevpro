@IsTest
public class CS_JSONService_SObjectTest  {
	private static CS_JSON_Schema jsonSchema;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		jsonSchema = new CS_JSON_Schema();
		jsonSchema = (CS_JSON_Schema) JSON.deserialize(CS_JSONExportTestFactory.getJSON(), CS_JSON_Schema.class);
	}

	@IsTest
	private static void testGetResult() {
		No_Triggers__c notriggers =  new No_Triggers__c();
		notriggers.Flag__c = true;
		INSERT notriggers;

		createTestData();
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Acc');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opportunity', acc);
		cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);
		
		Test.startTest();
		CS_JSONService_SObject controller = new CS_JSONService_SObject();
		controller.init(jsonSchema.settings.get(0));
		Map<String, Object> objCheck = (Map<String, Object>)controller.getResult(basket.Id);
		Test.stopTest();

		System.assertEquals(basket.Name, (String)objCheck.get('Basket name'));
		System.assertEquals(opp.Id, (String)objCheck.get('Opportunity ID'));

		notriggers.Flag__c = false;
		DELETE notriggers;
	}
}