@isTest

    private class Test_UpdateBBBackOfficeStatus{
    
    static TestMethod void myUnitTest(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
    opportunity o = new opportunity();
    o.name='test';
    o.StageName='FOS_Stage';
    o.CloseDate = system.today();
    insert o;
      
    CRF__c crf = new CRF__c();
    crf.RecordTypeId = [Select Id from RecordType where DeveloperName='NSO_BS'].Id;
    crf.Opportunity__c = o.Id;
    crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    crf.Contact__c = c.Id;
    insert crf;
    
    CRF__c BBcrf = new CRF__c();
    BBcrf.RecordTypeId = [Select Id from RecordType where DeveloperName='One_Plan_and_Broadband'].Id;
    BBcrf.Opportunity__c = o.Id;
    BBcrf.Related_to_NSO_CRF__c=crf.Id;
    BBcrf.Order_type__c = 'Res to Bus Conversion';
    BBcrf.Payment_Method__c = 'DD details not available send mandate';
    BBcrf.Alt_contact_phone_number__c = '0123456789';
    BBcrf.password__c='asdf@12345';
    BBcrf.Tel_number_for_Broadband_Service__c='02154879632';
    BBcrf.Your_sales_channel_ID__c='B77';
    BBcrf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
    BBcrf.Contact__c = c.Id;
    insert BBcrf;
    
    crf.Back_Office_Status__c='In progress';
    update crf;
    
    }    
    }