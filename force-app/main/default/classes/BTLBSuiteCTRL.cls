public  with sharing class  BTLBSuiteCTRL {
    ApexPages.StandardController Gcontroller;
    public BTLB_Suite__c thisRec;    

    public BTLBSuiteCTRL() { }
	public String pageName= ApexPages.CurrentPage().getUrl();

    public BTLBSuiteCTRL(ApexPages.StandardController controller) {
        if(!pageName.contains('BTLBSuiteChange')){
			Gcontroller = controller;
	        if (!Test.isRunningTest()) {
	            controller.addFields(new List<String>{'Opportunity__c'});
	            controller.addFields(new List<String>{'zOppAmount__c'});
	        }
	        this.thisRec= (BTLB_Suite__c)controller.getRecord();    
		}
    }
    private List<SelectOption> selOptions; 
    
    public String oppName {get;set;}
    public String accName {get;set;}
    public String accBTLB {get;set;}
    public String ownerName {get;set;}
    public String ownerId {get;set;}
    public String referrer {get;set;}
    public String OID {get;set;}
    public Decimal oppAmount {get;set;}
    public String accId {get;set;}
    public String oliUpdate {get;set;}
    public String textColor {get;set;}
    public String records {get;set;}
    public String existingRef {get;set;}
	
    public String showData {get;set;}

    public Decimal countRecords {get;set;}

	public String userName {
        get {return System.currentPageReference().getParameters().get('userName');}
        set;
    }
	public String filterUser {
        get {return System.currentPageReference().getParameters().get('filterUser');}
        set;
    }
	public String filterDate {
        get {return System.currentPageReference().getParameters().get('filterDate');}
        set;
    }
	public String filterStatus {
        get {return System.currentPageReference().getParameters().get('filterStatus');}
        set;
    }
	public String filterBTStatus {
        get {return System.currentPageReference().getParameters().get('filterBTStatus');}
        set;
    }
	public String filterSch5 {
        get {return System.currentPageReference().getParameters().get('filterSch5');}
        set;
    }
	public String filterSOV {
        get {return System.currentPageReference().getParameters().get('filterSOV');}
        set;
    }
	public String filterComm {
        get {return System.currentPageReference().getParameters().get('filterComm');}
        set;
    }
/*
	public String sortUser {
        get {return System.currentPageReference().getParameters().get('sortUser');}
        set;
    }
	public String sortDate {
        get {return System.currentPageReference().getParameters().get('sortDate');}
        set;
    }
	public String sortStatus {
        get {return System.currentPageReference().getParameters().get('sortStatus');}
        set;
    }
	public String sortBTStatus {
        get {return System.currentPageReference().getParameters().get('sortBTStatus');}
        set;
    }
	public String sortSch5 {
        get {return System.currentPageReference().getParameters().get('sortSch5');}
        set;
    }
	public String sortSOV {
        get {return System.currentPageReference().getParameters().get('sortSOV');}
        set;
    }
	public String sortComm {
        get {return System.currentPageReference().getParameters().get('sortComm');}
        set;
    }
*/
    public String orderDescr {get;set;}
   
    public String orderRef0 {get;set;}public String orderRef1 {get;set;}public String orderRef2 {get;set;}public String orderRef3 {get;set;}public String orderRef4 {get;set;}public String orderRef5 {get;set;}public String orderRef6 {get;set;}public String orderRef7 {get;set;}public String orderRef8 {get;set;}public String orderRef9 {get;set;}public String orderRef10 {get;set;}public String orderRef11 {get;set;}
    public Decimal orderSOV0 {get;set;}public Decimal orderSOV1 {get;set;}public Decimal orderSOV2 {get;set;}public Decimal orderSOV3 {get;set;}public Decimal orderSOV4 {get;set;}public Decimal orderSOV5 {get;set;}public Decimal orderSOV6 {get;set;}public Decimal orderSOV7 {get;set;}public Decimal orderSOV8 {get;set;}public Decimal orderSOV9 {get;set;}public Decimal orderSOV10 {get;set;}public Decimal orderSOV11 {get;set;}
    public String orderType0 {get;set;}public String orderType1 {get;set;}public String orderType2 {get;set;}public String orderType3 {get;set;}public String orderType4 {get;set;}public String orderType5 {get;set;}public String orderType6 {get;set;}public String orderType7 {get;set;}public String orderType8 {get;set;}public String orderType9 {get;set;}public String orderType10 {get;set;}public String orderType11 {get;set;}
    public Decimal orderQty0 {get;set;}public Decimal orderQty1 {get;set;}public Decimal orderQty2 {get;set;}public Decimal orderQty3 {get;set;}public Decimal orderQty4 {get;set;}public Decimal orderQty5 {get;set;}public Decimal orderQty6 {get;set;}public Decimal orderQty7 {get;set;}public Decimal orderQty8 {get;set;}public Decimal orderQty9 {get;set;}public Decimal orderQty10 {get;set;}public Decimal orderQty11 {get;set;}
    
    public Date orderCRD0 {get;set;}public Date orderCRD1 {get;set;}public Date orderCRD2 {get;set;}public Date orderCRD3 {get;set;}public Date orderCRD4 {get;set;}public Date orderCRD5 {get;set;}public Date orderCRD6 {get;set;}public Date orderCRD7 {get;set;}public Date orderCRD8 {get;set;}public Date orderCRD9 {get;set;}public Date orderCRD10 {get;set;}public Date orderCRD11 {get;set;}
    public Date orderDate0 {get;set;}public Date orderDate1 {get;set;}public Date orderDate2 {get;set;}public Date orderDate3 {get;set;}public Date orderDate4 {get;set;}public Date orderDate5 {get;set;}public Date orderDate6 {get;set;}public Date orderDate7 {get;set;}public Date orderDate8 {get;set;}public Date orderDate9 {get;set;}public Date orderDate10 {get;set;}public Date orderDate11 {get;set;}
    
    public String commAgent1 {get;set;}public String commAgent2 {get;set;}public String commAgent3 {get;set;}public String commAgent4 {get;set;}
    public Decimal commSplit1 {get;set;}public Decimal commSplit2 {get;set;}public Decimal commSplit3 {get;set;}public Decimal commSplit4 {get;set;}
    public Decimal sovSplit1 {get;set;}public Decimal sovSplit2 {get;set;}public Decimal sovSplit3 {get;set;}public Decimal sovSplit4 {get;set;}
    public Decimal qtySplit1 {get;set;}public Decimal qtySplit2 {get;set;}public Decimal qtySplit3 {get;set;}public Decimal qtySplit4 {get;set;}
    
    public String oppCheck {get;set;}

    public String oppId {
        get {return System.currentPageReference().getParameters().get('oppId');}
        set;
    }
    
    public String onLoad(){
        oppCheck = 'notReady';
        if (oppId == '' || oppId == null){
            oppCheck = 'noOpp';
            return null;
        }
        Opportunity o = [SELECT Id, Name, isWon, Amount, Opportunity_Id__c, AccountId, Account.Name, OwnerId, CreatedById, Owner.Name,BTLB_Common_Name__c FROM Opportunity WHERE Id = :oppId];
        accId = o.AccountId;
        oppName = o.Name;
        accName = o.Account.Name;
        ownerName = o.Owner.Name;
        ownerId = o.OwnerId;
        referrer = o.CreatedById;
        OID = o.Opportunity_Id__c;
        oppAmount = o.Amount;
        accBTLB = o.BTLB_Common_Name__c;

        if (o.IsWon && o.Amount > 0){
            oppCheck = 'ready';
        }
        return Null;
    }   
    public List<SelectOption> getPCTs(){
        selOptions= new List<SelectOption>();
        //selOptions.add(new SelectOption('', '0'));selOptions.add(new SelectOption('1', '1%'));selOptions.add(new SelectOption('2', '2%'));selOptions.add(new SelectOption('3', '3%'));selOptions.add(new SelectOption('4', '4%'));selOptions.add(new SelectOption('5', '5%'));selOptions.add(new SelectOption('6', '6%'));selOptions.add(new SelectOption('7', '7%'));selOptions.add(new SelectOption('8', '8%'));selOptions.add(new SelectOption('9', '9%'));selOptions.add(new SelectOption('10', '10%'));selOptions.add(new SelectOption('11', '11%'));selOptions.add(new SelectOption('12', '12%'));selOptions.add(new SelectOption('13', '13%'));selOptions.add(new SelectOption('14', '14%'));selOptions.add(new SelectOption('15', '15%'));selOptions.add(new SelectOption('16', '16%'));selOptions.add(new SelectOption('17', '17%'));selOptions.add(new SelectOption('18', '18%'));selOptions.add(new SelectOption('19', '19%'));selOptions.add(new SelectOption('20', '20%'));selOptions.add(new SelectOption('21', '21%'));selOptions.add(new SelectOption('22', '22%'));selOptions.add(new SelectOption('23', '23%'));selOptions.add(new SelectOption('24', '24%'));selOptions.add(new SelectOption('25', '25%'));selOptions.add(new SelectOption('26', '26%'));selOptions.add(new SelectOption('27', '27%'));selOptions.add(new SelectOption('28', '28%'));selOptions.add(new SelectOption('29', '29%'));selOptions.add(new SelectOption('30', '30%'));selOptions.add(new SelectOption('31', '31%'));selOptions.add(new SelectOption('32', '32%'));selOptions.add(new SelectOption('33', '33%'));selOptions.add(new SelectOption('34', '34%'));selOptions.add(new SelectOption('35', '35%'));selOptions.add(new SelectOption('36', '36%'));selOptions.add(new SelectOption('37', '37%'));selOptions.add(new SelectOption('38', '38%'));selOptions.add(new SelectOption('39', '39%'));selOptions.add(new SelectOption('40', '40%'));selOptions.add(new SelectOption('41', '41%'));selOptions.add(new SelectOption('42', '42%'));selOptions.add(new SelectOption('43', '43%'));selOptions.add(new SelectOption('44', '44%'));selOptions.add(new SelectOption('45', '45%'));selOptions.add(new SelectOption('46', '46%'));selOptions.add(new SelectOption('47', '47%'));selOptions.add(new SelectOption('48', '48%'));selOptions.add(new SelectOption('49', '49%'));selOptions.add(new SelectOption('50', '50%'));selOptions.add(new SelectOption('51', '51%'));selOptions.add(new SelectOption('52', '52%'));selOptions.add(new SelectOption('53', '53%'));selOptions.add(new SelectOption('54', '54%'));selOptions.add(new SelectOption('55', '55%'));selOptions.add(new SelectOption('56', '56%'));selOptions.add(new SelectOption('57', '57%'));selOptions.add(new SelectOption('58', '58%'));selOptions.add(new SelectOption('59', '59%'));selOptions.add(new SelectOption('60', '60%'));selOptions.add(new SelectOption('61', '61%'));selOptions.add(new SelectOption('62', '62%'));selOptions.add(new SelectOption('63', '63%'));selOptions.add(new SelectOption('64', '64%'));selOptions.add(new SelectOption('65', '65%'));selOptions.add(new SelectOption('66', '66%'));selOptions.add(new SelectOption('67', '67%'));selOptions.add(new SelectOption('68', '68%'));selOptions.add(new SelectOption('69', '69%'));selOptions.add(new SelectOption('70', '70%'));selOptions.add(new SelectOption('71', '71%'));selOptions.add(new SelectOption('72', '72%'));selOptions.add(new SelectOption('73', '73%'));selOptions.add(new SelectOption('74', '74%'));selOptions.add(new SelectOption('75', '75%'));selOptions.add(new SelectOption('76', '76%'));selOptions.add(new SelectOption('77', '77%'));selOptions.add(new SelectOption('78', '78%'));selOptions.add(new SelectOption('79', '79%'));selOptions.add(new SelectOption('80', '80%'));selOptions.add(new SelectOption('81', '81%'));selOptions.add(new SelectOption('82', '82%'));selOptions.add(new SelectOption('83', '83%'));selOptions.add(new SelectOption('84', '84%'));selOptions.add(new SelectOption('85', '85%'));selOptions.add(new SelectOption('86', '86%'));selOptions.add(new SelectOption('87', '87%'));selOptions.add(new SelectOption('88', '88%'));selOptions.add(new SelectOption('89', '89%'));selOptions.add(new SelectOption('90', '90%'));selOptions.add(new SelectOption('91', '91%'));selOptions.add(new SelectOption('92', '92%'));selOptions.add(new SelectOption('93', '93%'));selOptions.add(new SelectOption('94', '94%'));selOptions.add(new SelectOption('95', '95%'));selOptions.add(new SelectOption('96', '96%'));selOptions.add(new SelectOption('97', '97%'));selOptions.add(new SelectOption('98', '98%'));selOptions.add(new SelectOption('99', '99%'));selOptions.add(new SelectOption('100', '100%'));
        selOptions.add(new SelectOption('', '0'));selOptions.add(new SelectOption('5', '5'));selOptions.add(new SelectOption('10', '10'));selOptions.add(new SelectOption('15', '15'));selOptions.add(new SelectOption('20', '20'));selOptions.add(new SelectOption('25', '25'));selOptions.add(new SelectOption('30', '30'));selOptions.add(new SelectOption('35', '35'));selOptions.add(new SelectOption('40', '40'));selOptions.add(new SelectOption('45', '45'));selOptions.add(new SelectOption('50', '50'));selOptions.add(new SelectOption('55', '55'));selOptions.add(new SelectOption('60', '60'));selOptions.add(new SelectOption('65', '65'));selOptions.add(new SelectOption('70', '70'));selOptions.add(new SelectOption('75', '75'));selOptions.add(new SelectOption('80', '80'));selOptions.add(new SelectOption('85', '85'));selOptions.add(new SelectOption('90', '90'));selOptions.add(new SelectOption('95', '95'));selOptions.add(new SelectOption('100', '100'));
        return selOptions;
    }  

    public List<SelectOption> getRateCardProducts(){
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('', '--None--'));
        List<BTLB_Suite_Rate_Card__c> optList = [SELECT Id, Product_Description__c FROM BTLB_Suite_Rate_Card__c WHERE Active__c = true ORDER BY Product_Description__c];
        for (BTLB_Suite_Rate_Card__c v:optList){
            //selOptions.add(new SelectOption(v.Id,v.Product_Description__c));
            selOptions.add(new SelectOption(v.Product_Description__c,v.Product_Description__c));
        }
        selOptions.add(new SelectOption('Other','Other (please specify)'));
        return selOptions;
    } 

    public List<SelectOption> getPeople(){
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('', '--None--'));
        List<User> uActive = [SELECT Id, Name, Department, EIN__c FROM User WHERE Department = :accBTLB AND isActive = true ORDER BY Name LIMIT 80];
        for (User u:uActive){
            selOptions.add(new SelectOption(u.Id,u.Name));
        }
        List<User> uInactive = [SELECT Id, Name, Department, EIN__c FROM User WHERE Department = :accBTLB AND isActive = false ORDER BY Name LIMIT 120];
        selOptions.add(new SelectOption('', '', true));
        selOptions.add(new SelectOption('', 'HISTORIC', true));
        for (User u:uInactive){
            selOptions.add(new SelectOption(u.Id,u.Name));
        }
        return selOptions;
    }  

    public PageReference saveRecord(){
        List<BTLB_Suite__c> newRec = New List<BTLB_Suite__c>(); 
        List<BTLB_Suite_Order_Lines__c> newRecOL = New List<BTLB_Suite_Order_Lines__c>(); 
        //add orders
		if (existingRef == '' || existingRef == null){
			if(orderRef0 != '' && orderRef0 != Null){newRec.add(addToList(orderRef0,orderType0,orderCRD0,orderSOV0,orderDate0,orderQty0));}
			if(orderRef1 != '' && orderRef1 != Null && orderRef1 != orderRef0){newRec.add(addToList(orderRef1,orderType1,orderCRD1,orderSOV1,orderDate1,orderQty1));}
			if(orderRef2 != '' && orderRef2 != Null && orderRef2 != orderRef1){newRec.add(addToList(orderRef2,orderType2,orderCRD2,orderSOV2,orderDate2,orderQty2));}
			if(orderRef3 != '' && orderRef3 != Null && orderRef3 != orderRef2){newRec.add(addToList(orderRef3,orderType3,orderCRD3,orderSOV3,orderDate3,orderQty3));}
			if(orderRef4 != '' && orderRef4 != Null && orderRef4 != orderRef3){newRec.add(addToList(orderRef4,orderType4,orderCRD4,orderSOV4,orderDate4,orderQty4));}
			if(orderRef5 != '' && orderRef5 != Null && orderRef5 != orderRef4){newRec.add(addToList(orderRef5,orderType5,orderCRD5,orderSOV5,orderDate5,orderQty5));}
			if(orderRef6 != '' && orderRef6 != Null && orderRef6 != orderRef5){newRec.add(addToList(orderRef6,orderType6,orderCRD6,orderSOV6,orderDate6,orderQty6));}
			if(orderRef7 != '' && orderRef7 != Null && orderRef7 != orderRef6){newRec.add(addToList(orderRef7,orderType7,orderCRD7,orderSOV7,orderDate7,orderQty7));}
			if(orderRef8 != '' && orderRef8 != Null && orderRef8 != orderRef7){newRec.add(addToList(orderRef8,orderType8,orderCRD8,orderSOV8,orderDate8,orderQty8));}
			if(orderRef9 != '' && orderRef9 != Null && orderRef9 != orderRef8){newRec.add(addToList(orderRef9,orderType9,orderCRD9,orderSOV9,orderDate9,orderQty9));}
			if(orderRef10 != '' && orderRef10 != Null && orderRef10 != orderRef9){newRec.add(addToList(orderRef10,orderType10,orderCRD10,orderSOV10,orderDate10,orderQty10));}
			if(orderRef11 != '' && orderRef11 != Null && orderRef11 != orderRef10){newRec.add(addToList(orderRef11,orderType11,orderCRD11,orderSOV11,orderDate11,orderQty11));}
		}
        try{
            if (!Test.isRunningTest()) {
                insert newRec;
            }
            //add related order line items
            List<RecordType> RTs = [Select Id,DeveloperName FROM RecordType WHERE SobjectType ='BTLB_Suite_Order_Lines__c'];
            String rtId = '';
            for (RecordType rt:RTs){
                if (rt.DeveloperName == 'BTLB_Suite_BTLB_Entered'){
                    rtId = rt.Id;
                }
                if (rt.DeveloperName == 'BTLB_Suite_BT_Entered'){
                    //rtId = rt.Id;
                }

            }

            newRecOL.add(addToListOL(orderRef0,orderType0,orderCRD0,orderSOV0,orderDate0,orderQty0,rtId));  
            if(orderRef1 != ''){newRecOL.add(addToListOL(orderRef1,orderType1,orderCRD1,orderSOV1,orderDate1,orderQty1,rtId));}     
            if(orderRef2 != ''){newRecOL.add(addToListOL(orderRef2,orderType2,orderCRD2,orderSOV2,orderDate2,orderQty2,rtId));}     
            if(orderRef3 != ''){newRecOL.add(addToListOL(orderRef3,orderType3,orderCRD3,orderSOV3,orderDate3,orderQty3,rtId));}     
            if(orderRef4 != ''){newRecOL.add(addToListOL(orderRef4,orderType4,orderCRD4,orderSOV4,orderDate4,orderQty4,rtId));}     
            if(orderRef5 != ''){newRecOL.add(addToListOL(orderRef5,orderType5,orderCRD5,orderSOV5,orderDate5,orderQty5,rtId));}     
            if(orderRef6 != ''){newRecOL.add(addToListOL(orderRef6,orderType6,orderCRD6,orderSOV6,orderDate6,orderQty6,rtId));}     
            if(orderRef7 != ''){newRecOL.add(addToListOL(orderRef7,orderType7,orderCRD7,orderSOV7,orderDate7,orderQty7,rtId));}     
            if(orderRef8 != ''){newRecOL.add(addToListOL(orderRef8,orderType8,orderCRD8,orderSOV8,orderDate8,orderQty8,rtId));}     
            if(orderRef9 != ''){newRecOL.add(addToListOL(orderRef9,orderType9,orderCRD9,orderSOV9,orderDate9,orderQty9,rtId));}     
            if(orderRef10 != ''){newRecOL.add(addToListOL(orderRef10,orderType10,orderCRD10,orderSOV10,orderDate10,orderQty10,rtId));}      
            if(orderRef11 != ''){newRecOL.add(addToListOL(orderRef11,orderType11,orderCRD11,orderSOV11,orderDate11,orderQty11,rtId));}  

            if(newRecOL.size()>0){
                insert newRecOL;
            }
            PageReference retPg = new PageReference('/'+oppId);
			if (existingRef != ''){
				BTLB_Suite__c checkParent = [SELECT Id FROM BTLB_Suite__c WHERE Name = :orderRef0];
				retPg = new PageReference('/'+checkParent.Id);
			}
            return retpg.setRedirect(true);

        }catch (DMLException e){
            String errMsg = '';String errId = '';
            for (Integer i = 0; i < e.getNumDml(); i++) {
                if(String.valueOf(e.getDmlType(i)) == 'DUPLICATE_VALUE'){
                    errId = e.getDmlMessage(i).replace('duplicate value found: Order_Reference__c duplicates value on record with id: ','');
                    List<BTLB_Suite__c> dupeRecord = [SELECT Order_Reference__c, Opportunity__r.Opportunity_Id__c, Opportunity__r.Owner.Name, Opportunity__r.BTLB_Common_Name__c FROM BTLB_Suite__c Where ID = :errId];
                    if(dupeRecord.size()> 0){
                        errMsg += dupeRecord[0].Order_Reference__c+' is already linked to an existing opportunity '+dupeRecord[0].Opportunity__r.Opportunity_Id__c+' owned by '+dupeRecord[0].Opportunity__r.Owner.Name+' in '+dupeRecord[0].Opportunity__r.BTLB_Common_Name__c+'<br/>';
                    }else{
                        errMsg += 'Check that all Order References are unique<br/>';
                    }
                }
            }
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL,errMsg);
            ApexPages.addMessage(msg);
        }

        return null;
    }
	
    public PageReference cancelRecord(){
		PageReference retPg = new PageReference('/'+oppId);
        return retpg.setRedirect(true);
    }
 
    public BTLB_Suite__c addToList(String orderRefX, String orderTypeX, Date orderCRDX, Decimal orderSOVX, Date orderDateX, Decimal orderQtyX ){
        BTLB_Suite__c newRecRel = New BTLB_Suite__c();
        newRecRel = New BTLB_Suite__c();

        newRecRel.Opportunity__c = oppId;
        newRecRel.Account__c = accId;
        newRecRel.Order_Description__c = orderDescr;
        newRecRel.Order_Date__c = orderDate0;
        newRecRel.BT_Status__c = 'Pending';
        
        newRecRel.Name = orderRefX;
        newRecRel.Order_Reference__c = orderRefX;
        newRecRel.Order_Type__c = orderTypeX;
        newRecRel.BTLB_CRD__c = orderCRDX;
        newRecRel.BTLB_Status__c = 'Issued';
        newRecRel.BTLB_Quantity__c = orderQtyX;
        
        if(orderDateX != null){
            newRecRel.Order_Date__c = orderDateX;
        }

        return newRecRel;
    }   

    public BTLB_Suite_Order_Lines__c addToListOL(String orderRefX, String orderTypeX, Date orderCRDX, Decimal orderSOVX, Date orderDateX, Decimal orderQtyX, String rtId){
        BTLB_Suite__c checkParent = [SELECT Id FROM BTLB_Suite__c WHERE Name = :orderRefX];
        BTLB_Suite_Order_Lines__c newRecRelOL = New BTLB_Suite_Order_Lines__c();
        newRecRelOL = New BTLB_Suite_Order_Lines__c();

        newRecRelOL.Order_Reference__c = checkParent.Id;
        newRecRelOL.Order_Line_Type__c = 'BTLB Entered';
        newRecRelOL.RecordTypeId = rtId;
        newRecRelOL.Product_Description__c = orderTypeX;
        newRecRelOL.SOV__c = orderSOVX;
        newRecRelOL.Volume__c = orderQtyX;
        newRecRelOL.Status__c = 'BTLB Entered';
        newRecRelOL.Order_Date__c = orderDate0;
        newRecRelOL.CRD__c = orderCRDX;
        newRecRelOL.Commission_Agent_1__c = commAgent1;newRecRelOL.Commission_Agent_2__c = commAgent2;newRecRelOL.Commission_Agent_3__c = commAgent3;newRecRelOL.Commission_Agent_4__c = commAgent4;
        newRecRelOL.Commission_Split_1__c = commSplit1;newRecRelOL.Commission_Split_2__c = commSplit2;newRecRelOL.Commission_Split_3__c = commSplit3;newRecRelOL.Commission_Split_4__c = commSplit4;
        newRecRelOL.SOV_Split_1__c = sovSplit1;newRecRelOL.SOV_Split_2__c = sovSplit2;newRecRelOL.SOV_Split_3__c = sovSplit3;newRecRelOL.SOV_Split_4__c = sovSplit4;
        newRecRelOL.Quantity_Split_1__c = qtySplit1;newRecRelOL.Quantity_Split_2__c = qtySplit2;newRecRelOL.Quantity_Split_3__c = qtySplit3;newRecRelOL.Quantity_Split_4__c = qtySplit4;

        newRecRelOL.Referrer__c = referrer;
        if(orderDateX != null){
            newRecRelOL.Order_Date__c = orderDateX;
        }

        return newRecRelOL;
    }
    
    public List<OpportunityLineItem> getOppLineItems(){
        List<OpportunityLineItem> oli = new List<OpportunityLineItem>();
        oli = [SELECT ID, Description, Category__c, Billing_Cycle__c, Sales_Type__c, Qty__c, ProdName__c,Portfolio_Area__c, Service_Ready_Date__c, schedule_5_Non_Schedule_5__c, UnitPrice, TotalPrice 
        FROM OpportunityLineItem WHERE OpportunityId = :oppId];
        return oli;
    }

    public List<OpportunityLineItem> getOppLineItems2(){
        List<OpportunityLineItem> oli = new List<OpportunityLineItem>();
        oli = [SELECT ID, Description, Category__c, Billing_Cycle__c, Sales_Type__c, Qty__c, ProdName__c,Portfolio_Area__c, Service_Ready_Date__c, schedule_5_Non_Schedule_5__c, UnitPrice, TotalPrice 
        FROM OpportunityLineItem WHERE OpportunityId = :thisRec.Opportunity__c];
        return oli;
    }

    public List<BTLB_Suite_Order_Lines__c> getOrderLineItems(){
        return [SELECT Id, RecordType.DeveloperName, Product_Description__c, Order_Date__c, CRD__c, Status__c, Cancel_Date__c, Closed_Date__c,Order_Line_Type__c,SOV__c,Volume__c,Cobra_Commission_Paid__c, Cobra_Period__c FROM BTLB_Suite_Order_Lines__c WHERE Order_Reference__c = :thisRec.Id];
    }

    public List<BTLB_Suite__c> getOppOrders(){
        List<BTLB_Suite__c> oli = new List<BTLB_Suite__c>();
        oli = [SELECT ID, Name FROM BTLB_Suite__c WHERE Opportunity__c = :oppId LIMIT 10000];
        return oli;
    }
	
    public PageReference goLoad(){
		getOrders();
		return null;
	}
    public List<BTLB_Suite__c> getOrders(){
		Decimal startInt = 0;
		Decimal endInt = 0;
		String queryString ='SELECT ID, Name,Order_Date__c, BTLB_Status__c, BTLB_SOV__c, BT_SOV__c, BTLB_Commission_Value__c,BT_Commission_Value_Cobra__c, ';
		queryString +='BTLB_Commission_Sacrifice__c, Account__r.Name, On_Cobra__c, SOV_Gap__c,Commission_Gap__c,SOV_GAP_PCT__c,Commission_Gap_PCT__c,BT_Status__c ';
		queryString +='FROM BTLB_Suite__c ';
		queryString +='WHERE Id != null ';
		String filterString = '';
		String orderString = '';

		if(filterDate != null && filterDate != ''){
			filterString +='AND Order_Date__c = '+filterDate +' ';
		}
		if(filterStatus != null && filterStatus != ''){
			filterString +='AND BTLB_Status__c = :filterStatus ';
		}
		if(filterBTStatus != null && filterBTStatus != ''){
			filterString +='AND BT_Status__c = :filterBTStatus ';
		}
		if(filterSOV != null && filterSOV != ''){
			startInt = Decimal.valueOf(filterSOV.subString(0,3));
			endInt = Decimal.valueOf(filterSOV.subString(4,7));
			filterString +='AND SOV_GAP_PCT__c >= :startInt AND SOV_GAP_PCT__c <= :endInt ';
		}
		if(filterComm != null && filterComm != ''){
			startInt = Decimal.valueOf(filterComm.subString(0,3));
			endInt = Decimal.valueOf(filterComm.subString(4,7));
			filterString +='AND Commission_Gap_PCT__c >= :startInt AND Commission_Gap_PCT__c <= :endInt ';
		}
		queryString = queryString + filterString+' LIMIT 500';

		//do a record count of totals
		String queryStringCount = 'SELECT COUNT(Id) i FROM BTLB_Suite__c WHERE Id != null ';
        queryStringCount += filterString;
		AggregateResult[] checkTotal = Database.query(queryStringCount);  
		countRecords = (Decimal) checkTotal[0].get('i');

		List<BTLB_Suite__c> block1 = database.Query(queryString);
system.debug('JMM DEBUG block1:'+block1);	 
		//return database.Query(queryString);
		return block1;
    }

    public PageReference goDownloadAll(){
		getCommissions();
        PageReference retPg = new PageReference('/apex/BTLBSuiteExport');
        return retpg.setRedirect(false);
    }
    public PageReference goDownloadUser(){
		getCommissions();
        PageReference retPg = new PageReference('/apex/BTLBSuiteExport');
        return retpg.setRedirect(false);
    }
	
    public List<BTLB_Suite_Commission__c> getCommissions(){
		String queryString ='SELECT Id, Agent__r.Name, Indicative_Commission_Value__c,Indicative_Quantity_Value__c,Indicative_SOV_Value__c, BTLB_Suite_Order_Lines__c, ';
		queryString +='BTLB_Suite_Order_Lines__r.Order_Reference__c, BTLB_Suite_Order_Lines__r.Order_Reference__r.Name, BTLB_Suite_Order_Lines__r.Product_Description__c, ';
		queryString +='BTLB_Suite_Order_Lines__r.Order_Date__c,BTLB_Suite_Order_Lines__r.zSchedule5__c,Referral__c,BTLB_Suite_Order_Lines__r.Order_Reference__r.BTLB_Status__c,BTLB_Suite_Order_Lines__r.Order_Reference__r.On_Cobra__c, ';
		queryString +='BTLB_Suite_Order_Lines__r.Order_Reference__r.Opportunity__r.Opportunity_Id__c, BTLB_Suite_Order_Lines__r.Order_Reference__r.Opportunity__r.Account.Name ';
		queryString +='FROM BTLB_Suite_Commission__c ';
		queryString +='WHERE Id != null ';
		if(filterUser != null && filterUser != ''){
			 queryString +='AND Agent__c = :filterUser ';
		}
		if(filterDate != null && filterDate != ''){
			 queryString +='AND BTLB_Suite_Order_Lines__r.Order_Date__c = '+filterDate +' ';
		}
		if(filterStatus != null && filterStatus != ''){
			 queryString +='AND BTLB_Suite_Order_Lines__r.Order_Reference__r.BTLB_Status__c = :filterStatus ';
		}
		if(filterSch5 != null && filterSch5 != ''){
			 queryString +='AND BTLB_Suite_Order_Lines__r.Schedule_5_Non_Schedule_5__c = :filterSch5 ';
		}
        queryString +=' LIMIT 10000';
		return database.Query(queryString);
    }
	
    public List<AggregateResult> getCommissionsAgg(){
 		String queryString ='SELECT Agent__c commId, Agent__r.Name commName, COUNT(Id) countR, SUM(Referral__c) sumRef, SUM(Indicative_Commission_Value__c) sumC, SUM(Indicative_Quantity_Value__c) sumQ, SUM(Indicative_SOV_Value__c) sumS ';
        queryString +='FROM BTLB_Suite_Commission__c ';
		queryString +='WHERE Id != null ';
		if(filterUser != null && filterUser != ''){
			// queryString +='AND Agent__c = :filterUser ';
		}
		if(filterDate != null && filterDate != ''){
			 queryString +='AND BTLB_Suite_Order_Lines__r.Order_Date__c = '+filterDate +' ';
		}
		if(filterStatus != null && filterStatus != ''){
			 queryString +='AND BTLB_Suite_Order_Lines__r.Order_Reference__r.BTLB_Status__c = :filterStatus ';
		}
		if(filterSch5 != null && filterSch5 != ''){
			 queryString +='AND BTLB_Suite_Order_Lines__r.Schedule_5_Non_Schedule_5__c = :filterSch5 ';
		}
        queryString +='GROUP BY Agent__r.Name, Agent__c ';
        queryString +='ORDER BY Agent__r.Name LIMIT 10000';
		return database.Query(queryString);
    }	
	
    public List<AggregateResult> getStatusAgg(){
        String queryString ='SELECT BTLB_Status__c val ';
        queryString +='FROM BTLB_Suite__c ';
        queryString +='GROUP BY BTLB_Status__c ';
        queryString +='ORDER BY BTLB_Status__c LIMIT 10000';
		return database.Query(queryString);
    }

    public List<AggregateResult> getBTStatusAgg(){
        String queryString ='SELECT BT_Status__c val ';
        queryString +='FROM BTLB_Suite__c ';
        queryString +='GROUP BY BT_Status__c ';
        queryString +='ORDER BY BT_Status__c LIMIT 10000';
		return database.Query(queryString);
    }

    public List<AggregateResult> getStatusSch5(){
        String queryString ='SELECT Schedule_5_Non_Schedule_5__c val ';
        queryString +='FROM BTLB_Suite_Order_Lines__c ';
        queryString +='GROUP BY Schedule_5_Non_Schedule_5__c ';
		queryString +='ORDER BY Schedule_5_Non_Schedule_5__c LIMIT 10000';
		return database.Query(queryString);
    }

	public PageReference noAction() {
		getCommissions();
		return null;
	}

	public String getAllStatus() {
		Schema.DescribeFieldResult F = BTLB_Suite__c.BTLB_Status__c.getDescribe();
		List<Schema.PicklistEntry> PL = F.getPicklistValues();
        String str= '<option value="">--None--</option>';
		for(Schema.PicklistEntry p:PL){
			str +='<option value="'+p.getValue()+'">'+p.getValue()+'</option>';
		}
	    return str;
	}

	public PageReference saveRecords() {
        Set<Id> setIds = new Set<Id>();
		List<String> csvList = records.Split(',');
		for(String s : csvList){   
			String[] csvVals = s.Split(':');
            setIds.add(csvVals[0]);
        }
        List<BTLB_Suite__c> recs = [SELECT Id, BTLB_Status__c FROM BTLB_Suite__c WHERE Id = :setIds];
		for (BTLB_Suite__c r:recs){
			for(String s : csvList){   
				String[] csvVals = s.Split(':');
			    if(r.Id == csvVals[0]){
					r.BTLB_Status__c = csvVals[1];
				}
			}
		}
		update recs;	
        PageReference retPg = new PageReference('/apex/BTLBSuiteView');
        return retpg.setRedirect(true);
	}
	
    public PageReference oliUpdate(){
        Set<Id> oliIds = New Set<Id>();
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();
        Map<Id, String> oliMap = new Map<Id, String>();
        if(oliUpdate !=''){
            List<String> csvList = oliUpdate.Split(',');
            for(String s : csvList){
                String[] csvVals = s.Split(':');
                oliIds.add(csvVals[0]);
                oliMap.put(csvVals[0], csvVals[1]);
            }
            oliList = [SELECT ID, TotalPrice, Opportunity.Amount FROM OpportunityLineItem WHERE Id = :oliIds]; 
            
            for ( OpportunityLineItem oli : oliList){
                if (oliMap.containsKey(oli.Id)){
                    oli.TotalPrice = decimal.valueof(oliMap.get(oli.Id));
                }
 
            }
            update oliList;
        }
        
        if(thisRec.Id == null){
            Opportunity o = [SELECT Id, Name, isWon, Amount, Opportunity_Id__c, AccountId, Account.Name, Owner.Name FROM Opportunity WHERE Id = :oppId];
            oppAmount = o.Amount;
            return null;
        }else{
            PageReference retPg = new PageReference('/'+thisRec.Id);
            retPg = new PageReference('/'+thisRec.Id);
            return retpg.setRedirect(true);
        }
    }
}