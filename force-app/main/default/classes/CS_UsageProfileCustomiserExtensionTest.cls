@isTest
private class CS_UsageProfileCustomiserExtensionTest {
	
	static {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		Account a = new Account(
			Name = 'Test',
			NumberOfEmployees = 1
		);
		insert a;
		
		Associated_LEs__c le = new Associated_LEs__c(Account__c = a.Id);
		insert le;
		
		OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
		Opportunity o = new Opportunity(
			Name = 'Test',
			AccountId = a.Id,
			CloseDate = System.Today(),
			StageName = 'Closed Won',
			TotalOpportunityQuantity = 0,
			AssociatedLE__c = le.Id
		);
		
		insert o;
		
		Usage_Profile__c up1 = new Usage_Profile__c(
			Active__c = true,
			Account__c = a.Id,
			Expected_SMS__c = 20,
			Expected_Voice__c = 10,
			Expected_Data__c = 20,
			AssociatedLE__c = le.Id
		);
		
		insert up1;
		
		cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
			Name = 'Test',
			Default_Usage_Profile__c = up1.Id
		);
		insert pi1;
		
		cspmb__Rate_Card__c rc1 = new cspmb__Rate_Card__c(
			Name = 'Test'
		);
		insert rc1;
		
		cspmb__Rate_Card_Line__c rlVoice = new cspmb__Rate_Card_Line__c(
			Name = 'Voice',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlVoice;
		
		cspmb__Rate_Card_Line__c rlSMS = new cspmb__Rate_Card_Line__c(
			Name = 'SMS',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlSMS;
		
		cspmb__Rate_Card_Line__c rlData = new cspmb__Rate_Card_Line__c(
			Name = 'Data',
			cspmb__Rate_Card__c = rc1.Id
		);
		insert rlData;
		
		cspmb__Price_Item_Rate_Card_Association__c assoc = new cspmb__Price_Item_Rate_Card_Association__c(
			cspmb__Rate_Card__c = rc1.Id,
			cspmb__Price_Item__c = pi1.Id
		);
		insert assoc;
		
		cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c(
			Name = 'Pd1',
			cscfga__Description__c = 'Pd1'
		);
		insert pd1;
		
		cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
			Name = 'Ad1',
			cscfga__Data_Type__c = 'String',
			cscfga__Type__c = 'Related Product',
			cscfga__Product_Definition__c = pd1.Id,
			cscfga__high_volume__c = true
		);
		insert ad1;
		
		cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
			Name = 'Pd2',
			cscfga__Description__c = 'Pd2'
		);
		insert pd2;
		cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
			Name = 'Ad2',
			cscfga__Data_Type__c = 'String',
			cscfga__Type__c = 'User Input',
			cscfga__Product_Definition__c = pd2.Id
		);
		insert ad2;
		
		cscfga__Available_Product_Option__c apo = new cscfga__Available_Product_Option__c(
			cscfga__Attribute_Definition__c = ad1.Id,
			cscfga__Product_Definition__c = pd2.Id
		);
		insert apo;
		
		CS_Related_Products__c rp = new CS_Related_Products__c(
			Name = 'Pd1',
			Related_Products__c = 'Pd2'
		);
		insert rp;
		
		CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
			Name = 'Pd2',
			Attribute_Names__c = 'Ad2',
			List_View_Attribute_Names__c = 'Ad2',
			Total_Attribute_Names__c = 'Ad2'
		);
		insert rpa;
		
		cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c(
			Name = 'Test',
			cscfga__Opportunity__c = o.Id
		);
		insert pb;
		
		cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
			Name = 'Root',
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Product_Definition__c = pd1.Id,
			Service_Plan__c = pi1.Id
		);
		insert pc1;
		
		cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(
			Name = 'Child',
			cscfga__Root_Configuration__c = pc1.Id,
			cscfga__Parent_Configuration__c = pc1.Id,
			cscfga__Product_Basket__c = pb.Id,
			cscfga__Product_Definition__c = pd2.Id,
			cscfga__Attribute_Name__c = 'Ad1'
		);
		insert pc2;
		
		cscfga__Attribute__c at1 = new cscfga__Attribute__c(
			Name = 'Ad1',
			cscfga__Attribute_Definition__c = ad1.Id,
			cscfga__Value__c = pc2.Id,
			cscfga__Product_Configuration__c = pc1.Id
		);
		insert at1;
		
		cscfga__Attribute__c at2 = new cscfga__Attribute__c(
			Name = 'Ad2',
			cscfga__Attribute_Definition__c = ad2.Id,
			cscfga__Value__c = 'Test',
			cscfga__Product_Configuration__c = pc2.Id
		);
		insert at2;
		
		
	}

	private static testMethod void testGetTopComponentHTML() {
		List<cscfga__Product_Configuration__c> prodConfigs = [Select Id, Name, cscfga__Product_Basket__c, 
			cscfga__Product_Bundle__c, cscfga__Configuration_Offer__c,
			cscfga__Product_Bundle__r.cscfga__Opportunity__c,
			cscfga__Root_Configuration__c, cscfga__Parent_Configuration__c
			from cscfga__Product_Configuration__c
			where cscfga__Root_Configuration__c != null];
		if (!prodConfigs.isEmpty()) {
			Map<String, String> params = new Map<String, String>();
			params.put('configId', prodConfigs[0].Id);
			params.put('retURL', '//' + prodConfigs[0].cscfga__Product_Basket__c);
			cscfga.ProductConfiguratorController pc = new cscfga.API_1.ApiSession(prodConfigs[0]).getController();
			CS_UsageProfileCustomiserExtension ext = new CS_UsageProfileCustomiserExtension();
			String html = ext.getTopComponentHtml(pc);
			ext.afterSave(pc);
			CS_Customiser customiser = new CS_Customiser();
			String html2 = customiser.getTopComponentHtml(pc);
			customiser.afterSave(pc);
		}
	}
	private static testMethod void testGetTopComponentHTM2L() {
		List<cscfga__Product_Configuration__c> prodConfigs = [Select Id, Name, cscfga__Product_Basket__c, 
			cscfga__Product_Bundle__c, cscfga__Configuration_Offer__c,
			cscfga__Product_Bundle__r.cscfga__Opportunity__c,
			cscfga__Root_Configuration__c, cscfga__Parent_Configuration__c
			from cscfga__Product_Configuration__c
			where cscfga__Root_Configuration__c = null];
		if (!prodConfigs.isEmpty()) {
			Map<String, String> params = new Map<String, String>();
			params.put('configId', prodConfigs[0].Id);
			params.put('retURL', '//' + prodConfigs[0].cscfga__Product_Basket__c);
			cscfga.ProductConfiguratorController pc = new cscfga.API_1.ApiSession(prodConfigs[0]).getController();
			CS_UsageProfileCustomiserExtension ext = new CS_UsageProfileCustomiserExtension();
			String html = ext.getTopComponentHtml(pc);
			ext.afterSave(pc);
			CS_Customiser customiser = new CS_Customiser();
			String html2 = customiser.getTopComponentHtml(pc);
			customiser.afterSave(pc);
		}
		
	}
	
	private static testMethod void testGetTopComponentHTM3L() {
		CS_UsageProfileCustomiser upc = new CS_UsageProfileCustomiser();
		upc.coverage();
		List<cscfga__Product_Configuration__c> prodConfigs = [Select Id, Name, cscfga__Product_Basket__c, 
			cscfga__Product_Bundle__c, cscfga__Configuration_Offer__c,
			cscfga__Product_Bundle__r.cscfga__Opportunity__c,
			cscfga__Root_Configuration__c, cscfga__Parent_Configuration__c
			from cscfga__Product_Configuration__c
			where cscfga__Root_Configuration__c = null];
		List<cscfga__Product_Configuration__c> prodConfigs2 = [Select Id, Name, cscfga__Product_Basket__c, 
			cscfga__Product_Bundle__c, cscfga__Configuration_Offer__c,
			cscfga__Product_Bundle__r.cscfga__Opportunity__c,
			cscfga__Root_Configuration__c, cscfga__Parent_Configuration__c
			from cscfga__Product_Configuration__c
			where cscfga__Root_Configuration__c != null];
		
		String childId = (String) prodConfigs2[0].Id;
		Map<String, Map<String, String>> attMap = new Map<String, Map<String, String>>();
		attMap.put(childId, new Map<String, String>());
		attMap.get(childId).put('Ad2', 'Test');
		Attachment att = new Attachment(
			Name = 'Pd2',
			ParentId = prodConfigs[0].Id,
			Body = Blob.valueOf(JSON.serialize(attMap))
		);
		insert att;
		
		if (!prodConfigs.isEmpty()) {
			Map<String, String> params = new Map<String, String>();
			params.put('configId', prodConfigs[0].Id);
			params.put('retURL', '//' + prodConfigs[0].cscfga__Product_Basket__c);
			cscfga.ProductConfiguratorController pc = new cscfga.API_1.ApiSession(prodConfigs[0]).getController();
			CS_UsageProfileCustomiserExtension ext = new CS_UsageProfileCustomiserExtension();
			String html = ext.getTopComponentHtml(pc);
			ext.afterSave(pc);
			CS_Customiser customiser = new CS_Customiser();
			String html2 = customiser.getTopComponentHtml(pc);
			customiser.afterSave(pc);
		}
		
	}

}