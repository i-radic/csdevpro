@isTest(SeeAllData=true)

public class UsageProfileSearch_Test{

    static Account a {get; set;} 
    static Account a2 {get; set;} 
    static Opportunity o {get; set;} 
    static Usage_Profile__c up {get; set;} 
    static Usage_Profile__c up2 {get; set;} 
    static Usage_Profile__c up3 {get; set;} 

    public static testMethod void testUsageProfileSearchController(){            
        a = Test_Factory.CreateAccount();     a.CUG__c = 'UsageProf1';a.OwnerId = UserInfo.GetUserId() ;Database.SaveResult aR = Database.insert(a); 
        a2 = Test_Factory.CreateAccount();     a2.CUG__c = 'UsageProf2';a2.OwnerId = UserInfo.GetUserId() ;Database.SaveResult aR2 = Database.insert(a2); 
        o = Test_Factory.CreateOpportunity(a.Id);o.closedate = system.today();Database.SaveResult opptResult = Database.insert(o); 

        up = new Usage_Profile__c (Usage_Profile_External_ID__c = 'TEST-JMM-1',Active__c = true,Expected_SMS__c = 20,Expected_Voice__c = 10,Expected_Data__c = 20,Calls_to_other_UK_mobile_networks__c = 20,Total_Number_of_Connections__c = 2,Data_Only_Connections__c = 2); insert up;
        up2 = new Usage_Profile__c (Usage_Profile_External_ID__c = 'TEST-JMM-2', Account__c = a.Id,Active__c = true,Expected_SMS__c = 20,Expected_Voice__c = 10,Expected_Data__c = 20,Calls_to_other_UK_mobile_networks__c = 20,Total_Number_of_Connections__c = 2,Data_Only_Connections__c = 2); insert up2;
        up3 = new Usage_Profile__c (Usage_Profile_External_ID__c = 'TEST-JMM-3', Account__c = a2.Id,Active__c = true,Expected_SMS__c = 20,Expected_Voice__c = 10,Expected_Data__c = 20,Calls_to_other_UK_mobile_networks__c = 20,Total_Number_of_Connections__c = 2,Data_Only_Connections__c = 2); insert up3;
		

        Test.startTest(); 
			UsageProfileSearch upCtrl= new UsageProfileSearch(); 
			upCtrl = new UsageProfileSearch(new ApexPages.StandardController(o)); 
			upCtrl.getEOLlist();

			UsageProfileSearch uProf= new UsageProfileSearch(); 
			upCtrl.textEOL = 'test';  
			upCtrl.resultEOLText = 'test';  
			upCtrl.oppAccountId = a.Id; 
			upCtrl.resultEOL = true; 
			upCtrl.linkedEOL = true; 
	
			upCtrl.searchEOL = up.Usage_Profile_External_ID__c;  
			upCtrl.recordEOL = up.Id; 	
			upCtrl.searchEOL();
			upCtrl.searchEOL = up2.Usage_Profile_External_ID__c;   
			upCtrl.recordEOL = up2.Id; 
			upCtrl.searchEOL();
			upCtrl.searchEOL = up3.Usage_Profile_External_ID__c;   
			upCtrl.recordEOL = up3.Id; 
			upCtrl.searchEOL();
			
			upCtrl.recordEOL = up.Id; 
			upCtrl.confirmLink();
        Test.stopTest();
	}
}