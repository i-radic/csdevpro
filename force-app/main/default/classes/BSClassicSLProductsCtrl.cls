public with sharing class BSClassicSLProductsCtrl{

    public BSClassicSLProductsCtrl(ApexPages.StandardController controller) {

    }

public string SLID = System.currentPageReference().getParameters().get('id');
  
public BS_Classic_SL_Product__c newProd {
  get {
      if (newProd == null)
        newProd = new BS_Classic_SL_Product__c();
      return newProd ;
    }
  set;
  }

public List<BS_Classic_SL_Product__c> getProdList() {
      return [SELECT ID, Product_Type2__c, Product__c, Quantity__c, CreatedByID, CreatedBy.Name, CreatedDate, Sales_Log__r.Work_Area__c FROM BS_Classic_SL_Product__c WHERE Sales_Log__c = :SLID ORDER BY CreatedDate DESC];
}

public PageReference addProd() {
    newProd.Sales_Log__c = SLID ; // the record the file is attached to

    if (newProd.Product_Type2__c== null) {
        newProd.adderror('Please select the Product Type.'); 
        return null;
    }
    if (newProd.Product__c== null) {
        newProd.adderror('Please select the Product.'); 
        return null;
    }    
    if (newProd.Quantity__c == null) {
        newProd.adderror('Please enter the Quantity.'); 
        return null;
    }

    try {
      insert newProd;
    } catch (DMLException e) {
      ApexPages.addMessages(e);
    } finally {
      newProd= new BS_Classic_SL_Product__c(); 
    }

    return null;
}
 
}