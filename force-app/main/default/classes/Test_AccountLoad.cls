@isTest
 
private class Test_AccountLoad{
 
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');

// ensure user has privaledges to run trigger
		User thisUser = new User (id=UserInfo.getUserId(), Apex_Trigger_Account__c=true, Run_Apex_Triggers__c=true );
       update thisUser;
        
//create new user to improve code coverage
	User U1 = new User();
      u1.Username = 'jaovohoauh124@bt.com';
      u1.Ein__c = 'test12345';
      u1.LastName = 'TestLastname';
      u1.FirstName = 'TestFirstname';
      u1.MobilePhone = '07918672032';
      u1.Phone = '02085878834';
      u1.Title='What i do';
      u1.OUC__c = 'DKW';
      u1.Manager_EIN__c = 'test12346';
      u1.Email = 'no.reply@bt.com';
      u1.Alias = 'boatid01';
        insert U1;    
        
//create test BTLB    
 BTLB_Master__c testBTLB = new BTLB_Master__c( name = 'TEST',  Account_Owner__c = UserInfo.getUserId(), BTLB_Name_ExtLink__c ='TEST');
   Database.SaveResult[] BTLBResult = Database.insert(new BTLB_Master__c[] {testBTLB});     
   system.debug('ADJ TestBTLB ' + BTLBResult[0] ); 
       
// create dummy Market sacs for test BTLB
BTLB_Market_SAC__c testMktSac = new BTLB_Market_SAC__c();
        testMktSac.BTLB_Name__c = BTLBResult[0].id;
        testMktSac.SAC_Code__c = 'aSac999';
        testMktSac.ExtId__c = '999';
        insert testMktSac;
system.debug('ADJ TestMktSAC ' + testMktSac );

// create dummy LOB etc for test BTLB      
BTLB_CCAT__c testLOB = new BTLB_CCAT__c();
        testLOB.BTLB_Name__c = BTLBResult[0].id;
        testLOB.LOB_Code__c = 'lob999';
        testLOB.LOB_Name__c = 'BTLBTest';
        testLOB.Sub_Sector_Code__c = 'ssc999';
        testLOB.Sub_Sector__c ='ssTest';
        testLOB.ExtId__c = '999';
        insert testLOB;  
  system.debug('ADJ TestLOB ' + testLOB );  

//create test BTLB 2   
 BTLB_Master__c testBTLB2 = new BTLB_Master__c( name = 'TEST2',  Account_Owner__c = UserInfo.getUserId(), BTLB_Name_ExtLink__c ='TEST2');
   Database.SaveResult[] BTLBResult2 = Database.insert(new BTLB_Master__c[] {testBTLB2});     
   system.debug('ADJ TestBTLB2 ' + BTLBResult[0] ); 
       
// create dummy Market sacs for test BTLB2
BTLB_Market_SAC__c testMktSac2 = new BTLB_Market_SAC__c();
        testMktSac2.BTLB_Name__c = BTLBResult2[0].id;
        testMktSac2.SAC_Code__c = 'aSac99z';
        testMktSac2.ExtId__c = '999z';
        insert testMktSac2;
system.debug('ADJ TestMktSAC ' + testMktSac );

// create dummy LOB etc for test BTLB2      
BTLB_CCAT__c testLOB2 = new BTLB_CCAT__c();
        testLOB2.BTLB_Name__c = BTLBResult[0].id;
        testLOB2.LOB_Code__c = 'lob992';
        testLOB2.LOB_Name__c = 'BTLBTest2';
        testLOB2.Sub_Sector_Code__c = 'ssc9992';
        testLOB2.Sub_Sector__c ='ssTest2';
        testLOB2.ExtId__c = '9992';
        insert testLOB2;  
  system.debug('ADJ TestLOB2 ' + testLOB );      
        
//create BTLB owning SAC
        Account acc3 = Test_Factory.CreateAccount(); // a LE Level Account to link to
        acc3.Sector__c = 'BT Local Business';
        acc3.LOB_Code__c = 'lob999';
        acc3.SAC_Code__c = 'bSac999';
        acc3.LE_Code__c = 'TEST3';
        acc3.AM_EIN__c = '802537216';
        acc3.Sub_Sector__c = 'ssTest';
        insert acc3;

// create account non CORP or BTLB
		StaticVariables.setAccountDontRun(False);  
        Account acc1 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc1.Sector__c = 'other';
        acc1.LOB_Code__c = 'TEST';
        acc1.SAC_Code__c = 'LATO1231';
        acc1.LE_Code__c = null;
        //acc1.AM_EIN__c = '802537216';
        acc1.postcode__c = 'WR5 3RL';
        acc1.Base_Team_Assign_Date__c = Date.today();
        insert acc1;
        
//create BTLB Market SAC
        StaticVariables.setAccountDontRun(False);  
        Account acc2 = Test_Factory.CreateAccount(); // a SAC Level Account to link to
        acc2.Sector__c = 'BT Local Business';
        acc2.LOB_Code__c = 'lob999';
        acc2.SAC_Code__c = 'aSac999';
        acc2.LE_Code__c = 'TEST2';
        acc2.AM_EIN__c = '802537216';
        acc2.Sub_Sector__c = 'ssTest';
        insert acc2;
        
//Move BTLB Market SAC customer
  		StaticVariables.setAccountDontRun(False);  
	    acc2.Sub_Sector__c = 'ssc9992';   
        update acc2;

   
//Corporate  
   		StaticVariables.setAccountDontRun(False);  
   		Account acc4 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc4.Sector__c = 'Corporate';
        acc4.LOB_Code__c = 'TEST';
        acc4.SAC_Code__c = 'LATO1232';
        acc4.LE_Code__c = null;
        acc4.AM_EIN__c = '802537216';
        insert acc4;
        
        StaticVariables.setAccountDontRun(False);  
        Account acc5 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc5.Sector__c = 'Corporate';
        acc5.LOB_Code__c = 'TEST';
        acc5.SAC_Code__c = 'LATO1233';
        acc5.LE_Code__c = null;
        acc5.AM_EIN__c = '99999';
        insert acc5;
   

//Major and Public Sector  
      StaticVariables.setAccountDontRun(False);  
      Account acc6 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc6.Sector__c = 'Major and Public Sector';
        acc6.LOB_Code__c = 'TEST';
        acc6.SAC_Code__c = 'LATO1232';
        acc6.LE_Code__c = null;
        acc6.AM_EIN__c = '802537216';
        insert acc6;
        
        StaticVariables.setAccountDontRun(False);  
        Account acc7 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc7.Sector__c = 'Major and Public Sector';
        acc7.LOB_Code__c = 'TEST';
        acc7.SAC_Code__c = 'LATO1233';
        acc7.LE_Code__c = null;
        acc7.AM_EIN__c = '99999';
        insert acc7;


//NI Enterprise
      StaticVariables.setAccountDontRun(False);  
      Account acc8 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc8.Sector__c = 'NI Enterprise';
        acc8.LOB_Code__c = 'TEST';
        acc8.SAC_Code__c = 'LATO1232';
        acc8.LE_Code__c = null;
        acc8.AM_EIN__c = '802537216';
        insert acc8;
        
        StaticVariables.setAccountDontRun(False);  
        Account acc9 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc9.Sector__c = 'NI Enterprise';
        acc9.LOB_Code__c = 'TEST';
        acc9.SAC_Code__c = 'LATO1233';
        acc9.LE_Code__c = null;
        acc9.AM_EIN__c = '99999';
        insert acc9;
   

   
    }
}