public class FormattedDatetime {
	
	public FormattedDatetime() {
		
	}
	
	//returns the properly formatted datetime value
	public static String getTimeZoneValue(DateTime date_time) {
		Map<String, String> mappedValues = new Map<String, String>(); //map for holding locale to datetime format
		mappedValues.put('ar', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('ar_AE', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('ar_BH', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('ar_JO', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('ar_KW', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('ar_LB', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('ar_SA', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('bg_BG', 'yyyy-M-d H:mm');
		mappedValues.put('ca', 'dd/MM/yyyy HH:mm');
		mappedValues.put('ca_ES', 'dd/MM/yyyy HH:mm');
		mappedValues.put('ca_ES_EURO', 'dd/MM/yyyy HH:mm');
		mappedValues.put('cs', 'd.M.yyyy H:mm');
		mappedValues.put('cs_CZ', 'd.M.yyyy H:mm');
		mappedValues.put('da', 'dd-MM-yyyy HH:mm');
		mappedValues.put('da_DK', 'dd-MM-yyyy HH:mm');
		mappedValues.put('de', 'dd.MM.yyyy HH:mm');
		mappedValues.put('de_AT', 'dd.MM.yyyy HH:mm');
		mappedValues.put('de_AT_EURO', 'dd.MM.yyyy HH:mm');
		mappedValues.put('de_CH', 'dd.MM.yyyy HH:mm');
		mappedValues.put('de_DE', 'dd.MM.yyyy HH:mm');
		mappedValues.put('de_DE_EURO', 'dd.MM.yyyy HH:mm');
		mappedValues.put('de_LU', 'dd.MM.yyyy HH:mm');
		mappedValues.put('de_LU_EURO', 'dd.MM.yyyy HH:mm');
		mappedValues.put('el_GR', 'd/M/yyyy h:mm a');
		mappedValues.put('en_AU', 'd/MM/yyyy HH:mm');
		mappedValues.put('en_B', 'M/d/yyyy h:mm a');
		mappedValues.put('en_BM', 'M/d/yyyy h:mm a');
		mappedValues.put('en_CA', 'dd/MM/yyyy h:mm a');
		mappedValues.put('en_GB', 'dd/MM/yyyy HH:mm');
		mappedValues.put('en_GH', 'M/d/yyyy h:mm a');
		mappedValues.put('en_ID', 'M/d/yyyy h:mm a');
		mappedValues.put('en_IE', 'dd/MM/yyyy HH:mm');
		mappedValues.put('en_IE_EURO', 'dd/MM/yyyy HH:mm');
		mappedValues.put('en_NZ', 'd/MM/yyyy HH:mm');
		mappedValues.put('en_SG', 'M/d/yyyy h:mm a');
		mappedValues.put('en_US', 'M/d/yyyy h:mm a');
		mappedValues.put('en_ZA', 'yyyy/MM/dd hh:mm a');
		mappedValues.put('es', 'd/MM/yyyy H:mm');
		mappedValues.put('es_AR', 'dd/MM/yyyy HH:mm');
		mappedValues.put('es_BO', 'dd-MM-yyyy hh:mm a');
		mappedValues.put('es_CL', 'dd-MM-yyyy hh:mm a');
		mappedValues.put('es_CO', 'd/MM/yyyy hh:mm a');
		mappedValues.put('es_CR', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('es_EC', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('es_ES', 'd/MM/yyyy H:mm');
		mappedValues.put('es_ES_EURO', 'd/MM/yyyy H:mm');
		mappedValues.put('es_GT', 'd/MM/yyyy hh:mm a');
		mappedValues.put('es_HN', 'MM-dd-yyyy hh:mm a');
		mappedValues.put('es_MX', 'd/MM/yyyy hh:mm a');
		mappedValues.put('es_PE', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('es_PR', 'MM-dd-yyyy hh:mm a');
		mappedValues.put('es_PY', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('es_SV', 'MM-dd-yyyy hh:mm a');
		mappedValues.put('es_UY', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('es_VE', 'dd/MM/yyyy hh:mm a');
		mappedValues.put('et_EE', 'd.MM.yyyy H:mm');
		mappedValues.put('fi', 'd.M.yyyy H:mm');
		mappedValues.put('fi_FI', 'd.M.yyyy H:mm');
		mappedValues.put('fi_FI_EURO', 'd.M.yyyy H:mm');
		mappedValues.put('fr', 'dd/MM/yyyy HH:mm');
		mappedValues.put('fr_BE', 'd/MM/yyyy H:mm');
		mappedValues.put('fr_CA', 'yyyy-MM-dd HH:mm');
		mappedValues.put('fr_CH', 'dd.MM.yyyy HH:mm');
		mappedValues.put('fr_FR', 'dd/MM/yyyy HH:mm');
		mappedValues.put('fr_FR_EURO', 'dd/MM/yyyy HH:mm');
		mappedValues.put('fr_LU', 'dd/MM/yyyy HH:mm');
		mappedValues.put('fr_MC', 'dd/MM/yyyy HH:mm');
		mappedValues.put('hr_HR', 'yyyy.MM.dd HH:mm');
		mappedValues.put('hu', 'yyyy.MM.dd. H:mm');
		mappedValues.put('hy_AM', 'M/d/yyyy h:mm a');
		mappedValues.put('is_IS', 'd.M.yyyy HH:mm');
		mappedValues.put('it', 'dd/MM/yyyy H.mm');
		mappedValues.put('it_CH', 'dd.MM.yyyy HH:mm');
		mappedValues.put('it_IT', 'dd/MM/yyyy H.mm');
		mappedValues.put('iw', 'HH:mm dd/MM/yyyy');
		mappedValues.put('iw_IL', 'HH:mm dd/MM/yyyy');
		mappedValues.put('ja', 'yyyy/MM/dd H:mm');
		mappedValues.put('ja_JP', 'yyyy/MM/dd H:mm');
		mappedValues.put('kk_KZ', 'M/d/yyyy h:mm a');
		mappedValues.put('km_KH', 'M/d/yyyy h:mm a');
		mappedValues.put('ko', 'yyyy. M. d a h:mm');
		mappedValues.put('ko_KR', 'yyyy. M. d a h:mm');
		mappedValues.put('lt_LT', 'yyyy.M.d HH.mm');
		mappedValues.put('lv_LV', 'yyyy.d.M HH:mm');
		mappedValues.put('ms_MY', 'dd/MM/yyyy h:mm a');
		mappedValues.put('nl', 'd-M-yyyy H:mm');
		mappedValues.put('nl_BE', 'd/MM/yyyy H:mm');
		mappedValues.put('nl_NL', 'd-M-yyyy H:mm');
		mappedValues.put('nl_SR', 'd-M-yyyy H:mm');
		mappedValues.put('no', 'dd.MM.yyyy HH:mm');
		mappedValues.put('no_NO', 'dd.MM.yyyy HH:mm');
		mappedValues.put('pl', 'yyyy-MM-dd HH:mm');
		mappedValues.put('pt', 'dd-MM-yyyy H:mm');
		mappedValues.put('pt_AO', 'dd-MM-yyyy H:mm');
		mappedValues.put('pt_BR', 'dd/MM/yyyy HH:mm');
		mappedValues.put('pt_PT', 'dd-MM-yyyy H:mm');
		mappedValues.put('ro_RO', 'dd.MM.yyyy HH:mm');
		mappedValues.put('ru', 'dd.MM.yyyy H:mm');
		mappedValues.put('sk_SK', 'd.M.yyyy H:mm');
		mappedValues.put('sl_SI', 'd.M.y H:mm');
		mappedValues.put('sv', 'yyyy-MM-dd HH:mm');
		mappedValues.put('sv_SE', 'yyyy-MM-dd HH:mm');
		mappedValues.put('th', 'M/d/yyyy h:mm a');
		mappedValues.put('th_TH', 'd/M/yyyy, H:mm ?.');
		mappedValues.put('tr', 'dd.MM.yyyy HH:mm');
		mappedValues.put('ur_PK', 'M/d/yyyy h:mm a');
		mappedValues.put('vi_VN', 'HH:mm dd/MM/yyyy');
		mappedValues.put('zh', 'yyyy-M-d ah:mm');
		mappedValues.put('zh_CN', 'yyyy-M-d ah:mm');
		mappedValues.put('zh_HK', 'yyyy-M-d ah:mm');
		mappedValues.put('zh_TW', 'yyyy/M/d a h:mm');
		String user_locale = UserInfo.getLocale(); //grab the locale of the user
		String datetime_format = 'M/d/yyyy h:mm a'; //variable for the datetime format defaulted to the US format
		if (mappedValues.containsKey(user_locale)) { //if the map contains the correct datetime format
			datetime_format = mappedValues.get(user_locale); //grab the datetime format for the locale
		}
		String locale_formatted_date_time_value = date_time.format(datetime_format); //create a string with the proper format
		return locale_formatted_date_time_value; //return the string
	}
}