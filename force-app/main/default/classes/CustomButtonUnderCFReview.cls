global with sharing class CustomButtonUnderCFReview extends csbb.CustomButtonExt {
    
    public String performAction (String basketId) {
        cscfga__Product_Basket__c pb = [select CF_Review_Status__c from cscfga__Product_Basket__c where id = :basketId];
        String status = 'nok';
        String newUrl = '';
        
        if(pb.CF_Review_Status__c != 'Under Review'){
            pb.CF_Review_Status__c = 'Under Review';
            update pb;
            status = 'ok';
        }
        return '{"status":"'+status+'","redirectURL":"' + newUrl + '"}';
    }
}