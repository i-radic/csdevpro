@isTest
private class CS_Test_Expression {
	public static testmethod void test_sub() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(FCC_Actual__c = 50.0, FCC_Airtime__c = 25.0);
		CS_Expression.Field f1 = new CS_Expression.Field('FCC_Actual__c');
		CS_Expression.Field f2 = new CS_Expression.Field('FCC_Airtime__c');
		CS_Expression.Sub sub = new CS_Expression.Sub(f1, f2);
		sub.eval(basket);
	}
	
	public static testmethod void test_mul() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(FCC_Actual__c = 50.0, FCC_Airtime__c = 25.0);
		CS_Expression.Field f1 = new CS_Expression.Field('FCC_Actual__c');
		CS_Expression.Field f2 = new CS_Expression.Field('FCC_Airtime__c');
		CS_Expression.Mul mul = new CS_Expression.Mul(f1, f2);
		mul.eval(basket);
	}
}