global class AddressAvailabilityCheck{
    webservice static String doWork (String siteId) {
        Map<String, String> retMap = new Map<String, String>{'siteid' => siteId};
        return JSON.serialize(retMap);
    }
}