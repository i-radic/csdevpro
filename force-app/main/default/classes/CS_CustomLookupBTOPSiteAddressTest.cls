@IsTest
public class CS_CustomLookupBTOPSiteAddressTest {
	private static Map<String, String> searchFieldsMap = new Map<String, String>();    
    private static String prodDefinitionID;
    private static Id[] excludeIds;
    private static Integer pageOffset;
    private static Integer pageLimit;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
    	pageOffset = 1;
        
        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;

       	Contact contact = CS_TestDataFactory.generateContact(True,'TestContact');

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;

        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;

        BT_One_Phone__c btOnePhone = CS_TestDataFactory.generateBTOnePhone(True, testOpp);
        BT_One_Phone_Site__c btOnePhoneSite = CS_TestDataFactory.generateBTOnePhoneSite(True, btOnePhone, contact);
        
        searchFieldsMap.put('Address', btOnePhoneSite.Id);
        searchFieldsMap.put('Term', '24');
       
    }

    private static testMethod void searchBTOPAddressOneOff() {
	    
	    createTestData();
	    Test.StartTest();
	    CS_CustomLookupBTOPSiteAddress lkp = new CS_CustomLookupBTOPSiteAddress();
	    searchFieldsMap.put('Pricing fee option', 'One-off Charge');
	    lkp.getRequiredAttributes();
	    Object[] result = lkp.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}

	private static testMethod void searchBTOPAddressRecurring() {

	    createTestData();
	    Test.StartTest();
	    CS_CustomLookupBTOPSiteAddress lkp = new CS_CustomLookupBTOPSiteAddress();
	    searchFieldsMap.put('Pricing fee option', 'Recurring over the term');
	    Object[] result = lkp.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}

	private static testMethod void searchBTOPAddressDynamic() {
	    
	    createTestData();
	    Test.StartTest();
	    CS_CustomLookupBTOPSiteAddress lkp = new CS_CustomLookupBTOPSiteAddress();
	    searchFieldsMap.put('Pricing fee option', 'One-off Charge');
	    lkp.getRequiredAttributes();
	    Object[] result = lkp.doDynamicLookupSearch(searchFieldsMap, prodDefinitionID);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}
}