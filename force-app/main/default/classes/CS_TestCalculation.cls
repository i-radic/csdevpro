@isTest
private class CS_TestCalculation
{
    static testMethod void testAddition()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation a = new CS_Calculation
            ( new List<Decimal> {1, 2} );
        CS_Calculation b = new CS_Calculation
            ( new List<Decimal> {1, 2, 3} );

        CS_Calculation c = a.add(b);

        System.assertEquals(3, c.size());
        System.assertEquals(2, c.getData()[0]);
        System.assertEquals(4, c.getData()[1]);
        System.assertEquals(3, c.getData()[2]);

        c = b.add(a);

        System.assertEquals(3, c.size());
        System.assertEquals(2, c.getData()[0]);
        System.assertEquals(4, c.getData()[1]);
        System.assertEquals(3, c.getData()[2]);
    }

    static testMethod void testMultiplication()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation a = new CS_Calculation
            ( new List<Decimal> {1, 2} );
        CS_Calculation b = new CS_Calculation
            ( new List<Decimal> {1, 2, 3} );

        CS_Calculation c = a.mul(b);

        System.assertEquals(2, c.size());
        System.assertEquals(1, c.getData()[0]);
        System.assertEquals(4, c.getData()[1]);

        c = b.mul(a);

        System.assertEquals(2, c.size());
        System.assertEquals(1, c.getData()[0]);
        System.assertEquals(4, c.getData()[1]);
    }

    static testMethod void testCumsum()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation a = new CS_Calculation
            ( new List<Decimal> {1, 2, 3, 4, 5} );

        CS_Calculation b = a.cumsum();

        System.assertEquals(5, b.size());
        System.assertEquals(1, b.getData()[0]);
        System.assertEquals(3, b.getData()[1]);
        System.assertEquals(6, b.getData()[2]);
        System.assertEquals(10, b.getData()[3]);
        System.assertEquals(15, b.getData()[4]);
    }

    static testMethod void testOneOffRecurring()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation calc =
            CS_Calculation.createOneOffRecurring(100.0, 10.0, 12);

        System.assertEquals(13, calc.size());
        System.assertEquals(100, calc.getData()[0]);
        System.assertEquals(10, calc.getData()[1]);
        System.assertEquals(10, calc.getData()[2]);
        System.assertEquals(10, calc.getData()[3]);
        System.assertEquals(10, calc.getData()[4]);
        System.assertEquals(10, calc.getData()[5]);
        System.assertEquals(10, calc.getData()[6]);
        System.assertEquals(10, calc.getData()[7]);
        System.assertEquals(10, calc.getData()[8]);
        System.assertEquals(10, calc.getData()[9]);
        System.assertEquals(10, calc.getData()[10]);
        System.assertEquals(10, calc.getData()[11]);
        System.assertEquals(10, calc.getData()[12]);
    }

    static testMethod void testOneOff()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation calc =
            CS_Calculation.createOneOff(100.0);

        System.assertEquals(1, calc.size());
        System.assertEquals(100, calc.getData()[0]);
    }

    static testMethod void testOneOffOnMonth()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation calc =
            CS_Calculation.createOneOffOnMonth(100.0, 1);

        System.assertEquals(2, calc.size());
        System.assertEquals(0, calc.getData()[0]);
        System.assertEquals(100, calc.getData()[1]);
    }

    static testMethod void testNPVFactors()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation calc =
            CS_Calculation.createNPVFactors(14, true);

        System.assertEquals(14, calc.getData().size());
        System.assertEquals(1, calc.getData()[0]);
        System.assertEquals
        	( (1 / CS_Calculation.NPV_FACTOR).setScale(4)
        	, calc.getData()[13].setScale(4));
    }

    static testMethod void testSumOnYears()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation calc = CS_Calculation.createOneOffRecurring(100, 10, 15);

        List<Decimal> yearTotals = calc.sumOnYears();

        System.assertEquals(2, yearTotals.size());
        System.assertEquals(220, yearTotals[0]);
        System.assertEquals(30, yearTotals[1]);

        calc = CS_Calculation.createOneOffRecurring(100, 10, 24);

        yearTotals = calc.sumOnYears();

        System.assertEquals(2, yearTotals.size());
        System.assertEquals(220, yearTotals[0]);
        System.assertEquals(120, yearTotals[1]);
    }

    static testMethod void testSum()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation a = new CS_Calculation(new List<Decimal>{ 1, 2, 3 });
        CS_Calculation b = new CS_Calculation(new List<Decimal>{ 1, 2 });

        CS_Calculation sum = CS_Calculation.sum(new List<CS_Calculation>{ a, b });
        List<Decimal> result = sum.getData();

        System.assertEquals(3, result.size());
        System.assertEquals(2, result[0]);
        System.assertEquals(4, result[1]);
        System.assertEquals(3, result[2]);
    }

    static testMethod void testNeg()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation a = new CS_Calculation(new List<Decimal>{ 1, 2, 3 });

        List<Decimal> result = a.neg().getData();

        System.assertEquals(3, result.size());
        System.assertEquals(-1, result[0]);
        System.assertEquals(-2, result[1]);
        System.assertEquals(-3, result[2]);
    }

    static testMethod void testSetScale()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        CS_Calculation a =
            new CS_Calculation(new List<Decimal>{ 1.45, 2.54, 3.56 });

        List<Decimal> result = a.setScale(1).getData();

        System.assertEquals(3, result.size());
        System.assertEquals(1.4, result[0]);
        System.assertEquals(2.5, result[1]);
        System.assertEquals(3.6, result[2]);
    }

    static testMethod void testSumBySign()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        List<CS_Calculation> a = CS_Calculation.sumBySign(
            new List<CS_Calculation>
                { new CS_Calculation(new List<Decimal>{ 1.6, -3, -4, 6 }) });


        List<Decimal> positive = a[0].getData();
        List<Decimal> negative = a[1].getData();

        System.assertEquals(4, positive.size());
        System.assertEquals(1.6, positive[0]);
        System.assertEquals(0, positive[1]);
        System.assertEquals(0, positive[2]);
        System.assertEquals(6, positive[3]);

        System.assertEquals(4, negative.size());
        System.assertEquals(0, negative[0]);
        System.assertEquals(-3, negative[1]);
        System.assertEquals(-4, negative[2]);
        System.assertEquals(0, negative[3]);
   }
}