public class importDataFromCSVController {

    public Blob csvFileBody{get;set;}
    public String csvAsString{get;set;}
    public String[] csvFileLines{get;set;}
    public String returnMapString{get;set;}
    public Boolean CSVready{get;set;}
    
    
    public importDataFromCSVController(){
        csvFileLines = new String[]{};
        returnMapString='';
        CSVready=False;
    }
    
    // public Map<integer,Map<String,String>> importCSVFile(){
    public void importCSVFile(){
        system.debug('in importCSVFile');
        try{
            csvAsString = csvFileBody.toString(); 
            csvFileLines = csvAsString.split('\n'); //↵
            system.debug('csvFileLines   '+csvFileLines.size()+'   ' +csvFileLines);
            String[] csvRecordLabels = csvFileLines[0].split(',');
            
            // Iterate CSV file lines and retrieve one column at a time.
            for(Integer i=1; i < csvFileLines.size(); i++){
                String[] csvRecordData = csvFileLines[i].split(',');
                for(Integer j = 0; j < csvRecordData.size(); j++ ) {
                    system.debug(j+'   '+i+'  '+csvRecordData[j]);
                }
                returnMapString+=(csvRecordData[0]+',');
                returnMapString+=(csvRecordData[1]+',');
                returnMapString+=(csvRecordData[2]+',');
                returnMapString+=(csvRecordData[3]+',');
                returnMapString+=(csvRecordData[4]+',');
                returnMapString+=(csvRecordData[5]+',');
                returnMapString+=(csvRecordData[6]+',');
                
                if(csvRecordData[7].length()>2) {
                    returnMapString+=(csvRecordData[7].substring(1, csvRecordData[7].length()-1)+',');
                }
                else {
                    returnMapString+=(csvRecordData[7]+',');
                }
                returnMapString+=(csvRecordData[8]+',');
                returnMapString+=(csvRecordData[9]);
            }
        }
        catch (Exception e) {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured while importing. Please make sure input csv file is correct');
            system.debug(errorMessage);
        }
        system.debug(returnMapString);
        for(Integer i=1; i < returnMapString.length(); i++) {
            system.debug(i+'  '+returnMapString.charAt(i)+'    '+String.fromCharArray(new Integer[]{returnMapString.charAt(i)}));
            if(returnMapString.charAt(i)==13) {
                system.debug('changing annoyance with comma');
                returnMapString=returnMapString.left(i)+','+returnMapString.right(returnMapString.length()-i-1);
            }
        }
        system.debug(returnMapString);
        if (returnMapString!='') CSVready=True;
        return;
    }
}