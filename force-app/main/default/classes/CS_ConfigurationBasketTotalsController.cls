/**
 * Controller for configuration basket totals page
 * 
 * @author Kristijan
 */
global class CS_ConfigurationBasketTotalsController {
    
    private String configId;
    private String basketId;
    private String rootId;
    private String parentId;
    private String basketDetails;
    private String configDetails;
    private String parentConfigDetails;
    private String rootConfigDetails;
    public boolean isSMEUser;
    
    
    /**
     * get isSMEUser
     * @return String
     **/
    public boolean getIsSMEUser(){
        
        return this.isSMEUser;
        
    }
    
    /**
     * get basket details
     * @return String
     */
    public String getBasketDetails() {
        return basketDetails;
    }
     
    /**
     * get config details
     * @return String
     */
    public String getConfigDetails() {
        return configDetails;
    }
    
    /**
     * get parent config details
     * @return String
     */
    public String getParentConfigDetails() {
        return parentConfigDetails;
    }
    
    /**
     * get root config details
     * @return String
     */
    public String getRootConfigDetails() {
        return rootConfigDetails;
    }
    
    /**
     * Class constructor - parses parameters and assigns them to 
     * local variables. Used to get values and define behavior 
     * 
     */
    public CS_ConfigurationBasketTotalsController() {
        if (ApexPages.currentPage().getParameters().get('configId') != null) {
            configId = ApexPages.currentPage().getParameters().get('configId');
        }
        if (ApexPages.currentPage().getParameters().get('basketId') != null) {
            basketId = ApexPages.currentPage().getParameters().get('basketId');
        }
        if (ApexPages.currentPage().getParameters().get('rootId') != null) {
            rootId = ApexPages.currentPage().getParameters().get('rootId');
        }
        if (ApexPages.currentPage().getParameters().get('parentId') != null) {
            parentId = ApexPages.currentPage().getParameters().get('parentId');
        }
        
        loadBasketTotals();
        loadCurrentConfig();
        loadParentConfig();
        loadRootConfig();
        
        //get user profile and set value of isSMEUser
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
		if(ProfileName == 'SME BTLB Sales' || ProfileName == 'SME System Admin' || ProfileName == 'SME Setup (Doxford)' || ProfileName.contains('BTLB')){
            isSMEUser = true;
        }else{
            isSMEUser = false;
        }
    }
    
    /**
     * Class constructor for testing - accepts basketId and 
     * other parameters
     * 
     * @param basketId  Id of current basket
     * @paran configId  Id of current config
     */
    public CS_ConfigurationBasketTotalsController(String basketId, String configId) {
        this.basketId = basketId;
        this.configId = configId;
        
        loadBasketTotals();
        loadCurrentConfig();
        system.debug(configDetails);
    }
    
    /**
     * Loads basket and configurations. Current configuration is kept as a 
     * separate object. This will be edited/changed when someone changes 
     * configuration in real time
     */
    private void loadBasketTotals() {
        if (basketId != null && basketId != '') {
            String basketQuery = 'select '
                + CS_Utils.getSobjectFields('cscfga__Product_Basket__c')
                + ', (select '
                + CS_Utils.getSobjectFields('cscfga__Product_Configuration__c')
                + ' from cscfga__Product_Configurations__r)'
                + ' from cscfga__Product_Basket__c'
                + ' where Id = :basketId';
            cscfga__Product_Basket__c basket = Database.query(basketQuery);
            basketDetails = JSON.serialize(basket);
        }
    }
    
    /**
     * Loads configuration details
     * @param configId
     * @return config serialized as JSON string
     */
    private String loadConfigurationDetails(String configId) {
        String configJSON;
        if (configId != null && configId != '') {
            String configurationQuery = 'select '
                + CS_Utils.getSobjectFields('cscfga__Product_Configuration__c')
                + ' from cscfga__Product_Configuration__c'
                + ' where id = :configId';
            cscfga__Product_Configuration__c config = Database.query(configurationQuery);
            configJSON = JSON.serialize(config);
        }
        return configJSON;
    }
    
    /**
     * Loads parent configuration details.
     */
    private void loadParentConfig() {
        if (parentId != null) {
            parentConfigDetails = loadConfigurationDetails(parentId);
        }
    }
    
    /**
     * Loads root configuration details.
     */
    private void loadRootConfig() {
        if (rootId != null) {
            rootConfigDetails = loadConfigurationDetails(rootId);
        }
    }
    
    /**
     * Loads current configuration details. used for calculating 
     * differences between new/old values. 
     */
    private void loadCurrentConfig() {
        configDetails = loadConfigurationDetails(configId);
    }
}