public class OrgChartACCController {

string ADPId = '';
string AccountId = '';
List<ADP__c> ADPs = new List<ADP__c>();
List<Contact> activeContacts = new List<Contact>();

public OrgChartACCController() {
        
        ADPId = ApexPages.CurrentPage().getParameters().get('ADPId');
        //string ADPId = 'a0iP00000009vMKIAY';
        AccountId = ApexPages.CurrentPage().getParameters().get('accId');       
        
        //find account from ADP
        
        IF(ADPId==''){
           List<ADP__c> adIds = new List<ADP__c>();
            adIds = [SELECT Customer__c,Id FROM ADP__c WHERE Customer__c=:AccountId LIMIT 1];
              if(adIds.size() > 0) {
                ADPs = [SELECT Customer__c FROM ADP__c WHERE Id = :adIds[0].Id LIMIT 1];
                System.debug('----null'+adIds[0].Id);
            }
            else {
               ApexPages.Message message = new ApexPages.message(ApexPages.severity.ERROR, '<font color="#CC0000"><h4>No ADP Record Found</h4></font>');
                ApexPages.addMessage(message);
            }
        }
        else{
            ADPs = [SELECT Customer__c FROM ADP__c WHERE Id = :ADPId LIMIT 1];
             System.debug('----notnull'+ADPId);
          }
        system.debug('ADPs Size is : ' + ADPs.size());
    }


    public OrgChartACCController(ApexPages.StandardController controller) {
    
    
}


    


    public String AccountName { get; set; }

//START CUSTOM LIST SORTER
public static void sortList(List<Sobject> items, String sortField, String order){
       List<Sobject> resultList = new List<Sobject>();
   
        //Create a map that can be used for sorting 
       Map<object, List<Sobject>> objectMap = new Map<object, List<Sobject>>();
           
       for(Sobject ob : items){
                if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
                    objectMap.put(ob.get(sortField), new List<Sobject>()); 
                }
                objectMap.get(ob.get(sortField)).add(ob);
        }       
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();
       
        for(object key : keys){ 
            resultList.addAll(objectMap.get(key)); 
        }
       
        //Apply the sorted values to the source list
        items.clear();
        if(order.toLowerCase() == 'asc'){
            for(Sobject ob : resultList){
                items.add(ob); 
            }
        }else if(order.toLowerCase() == 'desc'){
            for(integer i = resultList.size()-1; i >= 0; i--){
                items.add(resultList[i]);  
            }
        }
    }
//END CUSTOM LIST SORTER    

//variables
List<contact> ConOrg= new List<contact>(); 
List<contact> showCon= new List<contact>();
List<contact> showCon1= new List<contact>();
List<contact> showCon2= new List<contact>();
List<contact> showCon3= new List<contact>();
List<contact> showCon4= new List<contact>();
List<contact> showCon5= new List<contact>();
List<contact> showCon6= new List<contact>();
List<contact> showCon7= new List<contact>();
Set<Id> conSet = new Set<Id>();
Set<Id> conSet1 = new Set<Id>();
Set<Id> conSet2 = new Set<Id>();
Set<Id> conSet3 = new Set<Id>();
Set<Id> conSet4 = new Set<Id>();
Set<Id> conSet5 = new Set<Id>();
Set<Id> conSet6 = new Set<Id>();
Set<Id> conSet7 = new Set<Id>();
Set<Id> cAct = new Set<Id>();
Set<Id> conTask = new Set<Id>();
Set<Id> conEvent = new Set<Id>();
//ADP: a0iP00000009vMKIAY
//ACC: 001P000000VXO7ZIAX

//define the basic SOQL statement for re-use in the tiers
String defaultStr = 'SELECT Id, Name, ReportsToId, ReportsTo.Name, Status__c, Job_Function__c, Job_Title__c, zTouch__c, Phone, Contact_Key_Decision_Maker__c FROM Contact';
String defaultStr2 = ' ORDER BY ReportsTo.Name, Name';
String jmSOQL = Null;
String accStatus = 'Active';

public List<Contact> getContacts() {
           
        //top level contacts
        IF(ADPs.size() >0) {
            jmSOQL = defaultStr + ' WHERE AccountId = \'' + ADPs[0].customer__c + '\'' + ' AND ReportsToId = Null AND Top_of_Org_Chart__c = True' + defaultStr2 ;           
        }
        // fetching results for Account when ADP is null
        ELSE IF(ADPId == ''){
            jmSOQL = defaultStr + ' WHERE AccountId = \'' + AccountId + '\' AND ReportsToId = Null AND Top_of_Org_Chart__c = True ' + defaultStr2 ;
        }      
        
        List<Contact> AccCons = Database.query(jmSOQL);        
        jmSOQL = Null;
        
     IF(AccCons.size()>0) {
        
        for (Contact cons : AccCons){  
            if(cons.Status__c == 'Active'){         
                ConOrg.add(cons);
                showCon.add(cons);
                conSet.add(cons.ID);
            }            
        }    
        //2nd level contacts
        jmSOQL = defaultStr + ' WHERE ReportsToId IN :conSet ' + defaultStr2;
        List<Contact> Sub1 = Database.query(jmSOQL);  
        jmSOQL = Null;   
        for (Contact cons1 : Sub1){
            if(cons1.Status__c == 'Active'){
                ConOrg.add(cons1);
                showCon1.add(cons1);
                conSet1.add(cons1.ID);
            }
        }
        //3rd level contacts
        jmSOQL = defaultStr + ' WHERE ReportsToId IN :conSet1 ' + defaultStr2;
        List<Contact> Sub2 = Database.query(jmSOQL);  
        jmSOQL = Null;   
        for (Contact cons2 : Sub2){
            if(cons2.Status__c == 'Active'){
                ConOrg.add(cons2);
                showCon2.add(cons2);
                conSet2.add(cons2.ID);
        }        
        //4th level contacts
        jmSOQL = defaultStr + ' WHERE ReportsToId IN :conSet2 ' + defaultStr2;
        List<Contact> Sub3 = Database.query(jmSOQL);  
        jmSOQL = Null;   
        for (Contact cons3 : Sub3){
            if(cons3.Status__c == 'Active'){
                ConOrg.add(cons3);
                showCon3.add(cons3);
                conSet3.add(cons3.ID);
            }
        }
        //5th level contacts
        jmSOQL = defaultStr + ' WHERE ReportsToId IN :conSet3 ' + defaultStr2;
        List<Contact> Sub4 = Database.query(jmSOQL);  
        jmSOQL = Null;   
        for (Contact cons4 : Sub4){
            if(cons4.Status__c == 'Active'){
                ConOrg.add(cons4);
                showCon4.add(cons4);
                conSet4.add(cons4.ID);
            }
        }   
        //6th level contacts
        jmSOQL = defaultStr + ' WHERE ReportsToId IN :conSet4 ' + defaultStr2;
        List<Contact> Sub5 = Database.query(jmSOQL);  
        jmSOQL = Null;   
        for (Contact cons5 : Sub5){
            if(cons5.Status__c == 'Active'){
                ConOrg.add(cons5);
                showCon5.add(cons5);
                conSet5.add(cons5.ID);
            }
        }   
        //7th level contacts
        jmSOQL = defaultStr + ' WHERE ReportsToId IN :conSet5 ' + defaultStr2;
        List<Contact> Sub6 = Database.query(jmSOQL);  
        jmSOQL = Null;   
        for (Contact cons6 : Sub6){
            if(cons6.Status__c == 'Active'){
                ConOrg.add(cons6);
                showCon6.add(cons6);
                conSet6.add(cons6.ID);
            }
            }
        }       
        //8th level contacts
        jmSOQL = defaultStr + ' WHERE ReportsToId IN :conSet6 ' + defaultStr2;
        List<Contact> Sub7 = Database.query(jmSOQL);  
        jmSOQL = Null;   
        for (Contact cons7 : Sub7){
            if(cons7.Status__c == 'Active'){
                ConOrg.add(cons7);
                showCon7.add(cons7);
                conSet7.add(cons7.ID);
            }
        }               
        
        //trying to process in task info for recent contact flagging
        for (Contact Acons : ConOrg){
            if(acons.Status__c == 'Active'){
                cAct.add(Acons.Id);
            }
        }
        DateTime jmActive = DateTime.Now().addmonths(-6);
        //String myDate = jmActive.format('dd MMM yyyy');
        //List<Task> cTask = [SELECT WhoId FROM Task WHERE WhoId = :cAct];
        List<Event> cEvent = [SELECT WhoId FROM Event WHERE WhoId = :cAct AND Event_Status__c = 'Closed' AND EndDateTime > :jmActive ];
         
        //for (Task cT : cTask ){
        //    conTask.add(cT.WhoId);
        //}
        for (Event cE : cEvent){
            conEvent.add(cE.WhoId);
        }
        
            
        //sort th list as required and return to VF page
        sortList(ConOrg,'ReportsToId', 'ASC');
        for (Contact conL: ConOrg){
            //if ((conTask.contains(conL.Id) == True) || (conEvent.contains(conL.Id) == True)){
            if (conEvent.contains(conL.Id) == True && conL.Status__c == 'Active'){
                conL.zTouch__c = 'Active';
            }
        }
        
        // adding active contacts
        /*
        for(Contact con : ConOrg){
            If(con.Status__c == 'Active'){
                activeContacts.add(con);
            }   
        }*/
        
        return ConOrg;
     }
     else {
        return null; 
     }
  }

public class repeaterCon {

    public String[] collection { get; set; }
    
    public repeaterCon() {
        collection = new String[]{'ONE','TWO','THREE'};
    }
}
    
    public List<Contact> getCon() {       
        sortList(showCon,'ReportsToId', 'ASC');
        return showCon;
    }
    public List<Contact> getCon1() {       
        sortList(showCon1,'ReportsToId', 'ASC');
        return showCon1;
    }    
    public List<Contact> getCon2() {       
        sortList(showCon2,'ReportsToId', 'ASC');
        return showCon2;
    }   
    public List<Contact> getCon3() {       
        sortList(showCon3,'ReportsToId', 'ASC');
        return showCon3;
    }       
    public List<Contact> getCon4() {       
        sortList(showCon4,'ReportsToId', 'ASC');
        return showCon4;
    }       
    public List<Contact> getCon5() {       
        sortList(showCon5,'ReportsToId', 'ASC');
        return showCon5;
    }       
    public List<Contact> getCon6() {       
        sortList(showCon6,'ReportsToId', 'ASC');
        return showCon6;
    }              
    public List<Contact> getCon7() {       
        sortList(showCon7,'ReportsToId', 'ASC');
        return showCon7;
    }                   
    
    public Integer ConTotalSize{get {
        if(ConOrg!= null)
            return ConOrg.size();
        else
            return 0;}
        }
    public Integer ConSize{get {
        if(showCon!= null)
            return showCon.size();
        else
            return 0;}
        }        
    public Integer ConSize1{get {
        if(showCon1!= null)
            return showCon1.size();
        else
            return 0;}
        }
    public Integer ConSize2{get {
        if(showCon2!= null)
            return showCon2.size();
        else
            return 0;}
        }                
    public Integer ConSize3{get {
        if(showCon3!= null)
            return showCon3.size();
        else
            return 0;}
        }                   
    public Integer ConSize4{get {
        if(showCon4!= null)
            return showCon4.size();
        else
            return 0;}
        }                   
    public Integer ConSize5{get {
        if(showCon5!= null)
            return showCon5.size();
        else
            return 0;}
        }                   
    public Integer ConSize6{get {
        if(showCon6!= null)
            return showCon6.size();
        else
            return 0;}
        }                   
    public Integer ConSize7{get {
        if(showCon7!= null)
            return showCon7.size();
        else
            return 0;}
        }           
}