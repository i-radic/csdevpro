@isTest(SeeAllData=TRUE)
private class EinsteinAnalyticsPredictionSMETest {
    static testMethod void runPositiveTestCases() {
        ed_insights__SDDPredictionConfig__c  sddpred = new ed_insights__SDDPredictionConfig__c ();
        sddpred.Name = 'BBDiscoveryOutcome';        
        sddpred.ed_insights__prediction_definition_id__c = '0OR0J0000004C9mWAE';
        sddpred.ed_insights__Batch_Update_Query__c='';
        sddpred.ed_insights__Disable_Scoring__c = false;
        sddpred.ed_insights__object_api_name__c = 'Einstein_Analytics__c';
        sddpred.ed_insights__commentary_field_api_name__c = 'BB_Discovery_Explanation__c';
        sddpred.ed_insights__outcome_field_api_name__c = 'BB_Discovery_Outcome__c';
        sddpred.ed_insights__prescription_field_api_name__c = 'BB_Discovery_Prescription__c';
        sddpred.ed_insights__Hide_Warnings__c = false;
        //insert sddpred;
        ed_insights__SDDPredictionConfig__c  sddpred2 = new ed_insights__SDDPredictionConfig__c ();
        sddpred2.Name = 'CloudDiscoveryOutcome';        
        sddpred2.ed_insights__prediction_definition_id__c = '0OR250000000006GAA';
        sddpred2.ed_insights__Batch_Update_Query__c='';
        sddpred2.ed_insights__Disable_Scoring__c = false;
        sddpred2.ed_insights__object_api_name__c = 'Einstein_Analytics__c';
        sddpred2.ed_insights__commentary_field_api_name__c = 'Cloud_Discovery_Explanation__c';
        sddpred2.ed_insights__outcome_field_api_name__c = 'Cloud_Discovery_Outcome__c';
        sddpred2.ed_insights__prescription_field_api_name__c = 'Cloud_Discovery_Prescription__c';
        sddpred2.ed_insights__Hide_Warnings__c = false;
        //Upsert sddpred2 ed_insights__SDDPredictionConfig__c.Name;
        ed_insights__SDDPredictionConfig__c  sddpred3 = new ed_insights__SDDPredictionConfig__c ();
        sddpred3.Name = 'DNDiscoveryOutcome';        
        sddpred3.ed_insights__prediction_definition_id__c = '0OR0J0000004C9mWAE';
        sddpred3.ed_insights__Batch_Update_Query__c='';
        sddpred3.ed_insights__Disable_Scoring__c = false;
        sddpred3.ed_insights__object_api_name__c = 'Einstein_Analytics__c';
        sddpred3.ed_insights__commentary_field_api_name__c = 'DN_Discovery_Explanation__c';
        sddpred3.ed_insights__outcome_field_api_name__c = 'DN_Discovery_Outcome__c';
        sddpred3.ed_insights__prescription_field_api_name__c = 'DN_Discovery_Prescription__c';
        sddpred3.ed_insights__Hide_Warnings__c = false;
        //Upsert sddpred3 ed_insights__SDDPredictionConfig__c.Name;
        ed_insights__SDDPredictionConfig__c  sddpred4 = new ed_insights__SDDPredictionConfig__c ();
        sddpred4.Name = 'SWDiscoveryOutcome';        
        sddpred4.ed_insights__prediction_definition_id__c = '0OR0J0000004C9mWAE';//0OR250000000006GAA
        sddpred4.ed_insights__Batch_Update_Query__c='';
        sddpred4.ed_insights__Disable_Scoring__c = false;
        sddpred4.ed_insights__object_api_name__c = 'Einstein_Analytics__c';
        sddpred4.ed_insights__commentary_field_api_name__c = 'SW_Discovery_Explanation__c';
        sddpred4.ed_insights__outcome_field_api_name__c = 'SW_Discovery_Outcome__c';
        sddpred4.ed_insights__prescription_field_api_name__c = 'SW_Discovery_Prescription__c';
        sddpred4.ed_insights__Hide_Warnings__c = false;
        //Upsert sddpred4 ed_insights__SDDPredictionConfig__c.Name;
        ed_insights__SDDPredictionConfig__c  sddpred5 = new ed_insights__SDDPredictionConfig__c ();
        sddpred5.Name = 'BTOPDiscoveryOutcome';        
        sddpred5.ed_insights__prediction_definition_id__c = '0OR0J0000004C9mWAE';
        sddpred5.ed_insights__Batch_Update_Query__c='';
        sddpred5.ed_insights__Disable_Scoring__c = false;
        sddpred5.ed_insights__object_api_name__c = 'Einstein_Analytics__c';
        sddpred5.ed_insights__commentary_field_api_name__c = 'BTOP_Discovery_Explanation__c';
        sddpred5.ed_insights__outcome_field_api_name__c = 'BTOP_Discovery_Outcome__c';
        sddpred5.ed_insights__prescription_field_api_name__c = 'BTOP_Discovery_Prescription__c';
        sddpred5.ed_insights__Hide_Warnings__c = false;
        //Upsert sddpred5 ed_insights__SDDPredictionConfig__c.Name;
        ed_insights__SDDPredictionConfig__c  sddpred6 = new ed_insights__SDDPredictionConfig__c ();
        sddpred6.Name = 'MOBDiscoveryOutcome';        
        sddpred6.ed_insights__prediction_definition_id__c = '0OR0J0000004C9mWAE';
        sddpred6.ed_insights__Batch_Update_Query__c='';
        sddpred6.ed_insights__Disable_Scoring__c = false;
        sddpred6.ed_insights__object_api_name__c = 'Einstein_Analytics__c';
        sddpred6.ed_insights__commentary_field_api_name__c = 'MOB_Discovery_Explanation__c';
        sddpred6.ed_insights__outcome_field_api_name__c = 'MOB_Discovery_Outcome__c';
        sddpred6.ed_insights__prescription_field_api_name__c = 'MOB_Discovery_Prescription__c';
        sddpred6.ed_insights__Hide_Warnings__c = false;
        //Upsert sddpred6 ed_insights__SDDPredictionConfig__c.Name;
        
        Test_Factory.SetProperty('IsTest', 'yes');
        User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             settings.Einstein_Analytics__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
             
        Account testAcc = Test_Factory.CreateAccount();
        //testAcc.AM_ein__c = u1.EIN__c;
       //testAcc.Sub_Sector__c = 'true';
        testAcc.OwnerId = thisUser.Id; 
        testAcc.CUG__c='CugTest';
        insert testAcc;
        
        Einstein_Analytics__c ed = new Einstein_Analytics__c();
        ed.Name='TestDiscovery';
        ed.Account__c= testAcc.Id;
        insert ed;
        ed.Name = 'TestDiscovery1';
        update ed;
    }
}