global class Batch_DeleteInactiveContactsSchedule implements Schedulable {
   global void execute(SchedulableContext sc) {
      Batch_DeleteInactiveContacts b = new Batch_DeleteInactiveContacts(); 
      database.executebatch(b,1);
   }
}