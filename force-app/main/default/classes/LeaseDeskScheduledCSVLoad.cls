global class LeaseDeskScheduledCSVLoad Implements Schedulable {
    
    public static String url {get;set;}
    public static String csvRawTestData {get;set;}
      
    global void execute(SchedulableContext sc){
        ParseData();
    }
    
    @future (callout=true) //code that does HTTP callouts
    static void ParseData(){  	
    	LeaseDeskURL__c ldUrl = [select Name, Url__c, Username__c, Password__c, CSV_Override__c from LeaseDeskURL__c where Name = 'CSV'];       
        String[] csvRows = new String[]{};
        HttpRequest req = new HttpRequest();
        HttpResponse resp = new HttpResponse();
        Http http = new Http();
        DateTime d = datetime.now(); 
        string dateStr = d.format('yyyy-MM-dd');        
        if(ldUrl.CSV_Override__c != null){
        	url = ldUrl.CSV_Override__c;
        }
        else{
        	url = String.valueOf(ldUrl.Url__c) + '/' + dateStr + '-day.csv';
        }           	                  
        req.setEndpoint(url);
        req.setTimeout(80000);
        req.setMethod('POST');        
        String username = String.valueOf(ldUrl.Username__c);
        String password = String.valueOf(ldUrl.Password__c);
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +
        encodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);
        List<LeaseDeskAPI.LeasingDetails> leaseDeskLeasingHistory = new List<LeaseDeskAPI.LeasingDetails>();
        Set<string> leasingOpptyIDSet = new Set<string>();
        if(test.isrunningtest()){
        	
        }
        else{
        	resp = http.send(req);
        }        
        String csvRawData = null;
        if(test.isrunningtest()){
    		csvRawData = 'Account Name,Company Name,LE Code,SAC Code,SF Account ID,CUG ID,SF OpptyID,Lease Desk Agreement ID,Lease Desk Account ID,Lease Desk Proposal ID,RBO,End Date,Status,LD Agreement URL,Funder,Funder Agreement ID,Funder URL,Funder Credit Limit,PO Number,PO Value,AX Number,Regular Payment,Frequency,Term,Periods remaining,Settlement,Residual Value,Product \n Ashton Healthcare Limited,Ashton Healthcare Limited,,,0012000000SiKjm,,OID-01597631,1429,LDC1908,,10051.22,01/05/2014,Live,https://btdev1.lease-desk.com/agreement/view/1429/,Shire,,,,,19489.65,,636.59,Monthly,36,15,9438.43,0,Mitel Switch';                  	
    	}
    	else{
    		csvRawData = resp.getBody();
    	}
        csvRows = csvRawData.split('\n');
        try{
	        for (Integer i = 1; i < csvRows.size(); i++)
	        {
	            String[] csvValues = new String[]{};
	            csvValues = csvRows[i].split(',');              
	            LeaseDeskAPI.LeasingDetails leasingDetails = new LeaseDeskAPI.LeasingDetails();
	            leasingDetails.salesforceAccountName = csvValues[0];
	            leasingDetails.leaseDeskCompanyName = csvValues[1];
	            leasingDetails.leCode = csvValues[2];
	            //##SAC Code not mapped on leasing object
	            leasingDetails.salesforceAccountID = csvValues[4];              
	            leasingDetails.cugID = csvValues[5];
	            leasingDetails.salesforceOpptyID = csvValues[6];
	            leasingDetails.leaseDeskAgreementID = csvValues[7];         
	            leasingDetails.leaseDeskAccountID = csvValues[8];               
	            leasingDetails.leaseDeskProposalID = csvValues[9];
	            if(csvValues[9]==''){
	                leasingDetails.leaseDeskProposalID = null;
	            }
	            try{leasingDetails.rbo = Double.ValueOf(csvValues[10]);} 
	            catch(Exception e){}
	            try{leasingDetails.endDate = Date.ValueOf(csvValues[11]);}
	            catch(Exception e){}
	            leasingDetails.status = csvValues[12];
	            leasingDetails.leaseDeskURL = csvValues[13];                
	            leasingDetails.funder = csvValues[14];
	            //##funder agreement Id not mapped on leasing object
	            leasingDetails.funderURL = csvValues[16];
	            try{leasingDetails.funderCreditLimit = Double.ValueOf(csvValues[17]);}
	            catch(Exception e){}
	            leasingDetails.poNumber = csvValues[18];
	            try{leasingDetails.poValue = Double.ValueOf(csvValues[19]);}
	            catch(Exception e){}
	            leasingDetails.axReference = csvValues[20];
	            try{leasingDetails.regularPayment = Double.ValueOf(csvValues[21]);}
	            catch(Exception e){}
	            leasingDetails.frequency = csvValues[22];
	            try{leasingDetails.term = Integer.ValueOf(csvValues[23]);}
	            catch(Exception e){}
	            try{leasingDetails.periodsRemaining = Integer.ValueOf(csvValues[24]);}
	            catch(Exception e){}
	            try{leasingDetails.settlement = Double.ValueOf(csvValues[25]);}
	            catch(Exception e){}
	            try{leasingDetails.residualValue = Double.ValueOf(csvValues[26]);}
	            catch(Exception e){}
	            try{leasingDetails.product = csvValues[27];}
	            catch(Exception e){}
	            try{leasingDetails.ragStatus = csvValues[28];}
	            catch(Exception e){}
	            try{leasingDetails.ragDate = Date.ValueOf(csvValues[29]);}
	            catch(Exception e){}
	            leaseDeskLeasingHistory.Add(leasingDetails);
	            leasingOpptyIDSet.add (leasingDetails.salesforceOpptyID);
	        }                      
			List<Opportunity> opptyList = [Select Id , Opportunity_Id__c from Opportunity where Opportunity_Id__c in :leasingOpptyIDSet];
	        for(leaseDeskAPI.LeasingDetails ldHistory : leaseDeskLeasingHistory){                                                                 
	        	if(!system.Test.isRunningTest()){
		        	if(ldHistory.salesforceOpptyID.startsWith('006')){
		        		ldHistory.salesforceOID = ldHistory.salesforceOpptyID;
		        	}
		        	for(Opportunity oppty : opptyList){
		        		if(ldHistory.salesforceOpptyID == oppty.Opportunity_Id__c){
		        			ldHistory.salesforceOID = oppty.Id;
		        		}
		        	}
	        	}
	        } 
        }
        catch(Exception e){
       		//--------Logging
        	Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'LeaseDeskScheduledCSVLoad', Request__c = 'ParseData: ' + url, Response__c = 'Error: ' + e);
        	insert log;                   
        }   
        LoadData(leaseDeskLeasingHistory);
    }
    
    static void LoadData(List<LeaseDeskAPI.LeasingDetails> leaseDeskLeasingHistory){ 
    	List<Leasing_History__c> leasingHistoryProposalToUpsert = new List<Leasing_History__c>();
		List<Leasing_History__c> leasingHistoryAgreementToUpsert = new List<Leasing_History__c>();
    	Set<Account> accountRAGStatusSet = new Set<Account>();
    	Set<string> leasingHistoryProposalsToDelete = new Set<string>();
    	
    	try{
	    	for(leaseDeskAPI.LeasingDetails ldHistory : leaseDeskLeasingHistory){ 
	    		Leasing_History__c sfHistory = new Leasing_History__c();
	    		sfHistory.Opportunity__c = ldHistory.salesforceOID;   
	            sfHistory.Account__c = ldHistory.salesforceAccountID;
	            sfHistory.AgreementID__c = ldHistory.leaseDeskProposalID;
	            sfHistory.LeaseDeskAgreementId__c = ldHistory.leaseDeskAgreementID;
	    		if(ldHistory.status != null){
	    			//------------To delete: funder status equals 'NotInUse' 
                    if(ldHistory.status == 'NotInUse'){ 
                        leasingHistoryProposalsToDelete.Add(ldHistory.leaseDeskProposalID);    
                    }
	            	sfHistory.Status__c = ldHistory.status.replace('.', ': ');
	            	sfHistory.clearance__c = StatusToRAGMapping(ldHistory.status);
	            	if(IsLiveDeal(ldHistory.status)){
	                	sfHistory.End_Date__c = null;
	            	}
	            	else{
	                	sfHistory.End_Date__c = ldHistory.endDate;
	            	}  
	            }        
	            if(ldHistory.settlement != null){
	            	sfHistory.Settlement__c = ldHistory.settlement;
	            }        
	            if(ldHistory.periodsRemaining != null){   
	            	sfHistory.Periods_Remaining__c = ldHistory.periodsRemaining;  
	            }    
	            if(ldHistory.product != null){    
	            	sfHistory.Product__c = ldHistory.product;
	            }
	            if(ldHistory.leaseDeskCompanyName != null){   
	            	sfHistory.Lease_Desk_Company_Name__c = ldHistory.leaseDeskCompanyName;
	            }
	            if(ldHistory.cugID != null){
	            	sfHistory.CUG_Id__c = ldHistory.cugID;
	            }
	            if(ldHistory.funderCreditLimit != null){
	            	sfHistory.Credit_limit__c = ldHistory.funderCreditLimit;
	            }
	            if(ldHistory.poValue != null){
	            	sfHistory.PO_Value__c = ldHistory.poValue;
	            } 
	            if(ldHistory.funderURL != null){
	            	sfHistory.Funder_URL__c = ldHistory.funderURL; 
	            }
	            if(ldHistory.leaseDeskURL != null){
	            	sfHistory.Lease_Desk_Url__c = ldHistory.leaseDeskURL;
	            }
	            sfHistory.Funder__c = '-';
	            if(ldHistory.funder.Length() >= 1){
	                sfHistory.Funder__c = ldHistory.funder;
	            }
	            if(ldHistory.term != null){
	            	sfHistory.Original_Term__c = String.valueOf(ldHistory.term) + ' Months';
	            }
	            if(ldHistory.rbo != null){
	            	sfHistory.RBO__c = ldHistory.rbo; 
	            }
	            if(ldHistory.regularPayment != null){
	            	sfHistory.Quarterly_Payment__c = QuarterlyPayment(ldHistory.frequency, ldHistory.regularPayment);
	            }
	            if(ldHistory.status == 'NotInUse' || ldHistory.salesforceOID == null || ldHistory.salesforceOID == '' || ldHistory.status == 'History.Credit'){
	            	//do not add to insert list
	            }
	            else{
	            	if(ldHistory.leaseDeskProposalID != null){
	            		leasingHistoryProposalToUpsert.Add(sfHistory);
	            	}
	            	else if(ldHistory.leaseDeskAgreementID != null){
	            		leasingHistoryAgreementToUpsert.Add(sfHistory);
	            	}
	            }
	            if(ldHistory.salesforceAccountID != null && ldHistory.ragStatus != null){
					Account a = new Account(Id = ldHistory.salesforceAccountID, Credit_Worthiness__c = ldHistory.ragStatus);
					accountRAGStatusSet.Add(a);
				}  
	        }
    	}
    	catch(Exception e){
       		//--------Logging
        	Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'LeaseDeskScheduledCSVLoad', Request__c = 'LoadData: ' + url, Response__c = 'Error: ' + e);
        	insert log;                   
        }
        Integer upsertCount = leasingHistoryProposalToUpsert.size() + leasingHistoryAgreementToUpsert.size();
        if(system.Test.isRunningTest() || accountRAGStatusSet.Size() >= 1){
        	List<Account> accountRAGStatusList = new List<Account>();
        	accountRAGStatusList.addAll(accountRAGStatusSet);
			update accountRAGStatusList;
		}
        if(system.Test.isRunningTest() || leasingHistoryProposalToUpsert.size() >= 1){
            try{
              	Schema.Sobjectfield externalId = Leasing_History__c.Fields.AgreementID__c;
				Database.upsert(leasingHistoryProposalToUpsert,externalId,false);
            	//--------Logging
            	Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'LeaseDeskScheduledCSVLoad', Request__c = 'To Upsert(leasingHistoryProposalToUpsert): ' + url, Response__c = 'Upsert Success: ' + leasingHistoryProposalToUpsert.size());
            	insert log; 
            }
            catch(Exception e){
           		//--------Logging
            	Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'LeaseDeskScheduledCSVLoad', Request__c = 'To Upsert(leasingHistoryProposalToUpsert): ' + url, Response__c = 'Upsert Error: ' + e);
            	insert log;                   
            }
    	}
    	if(system.Test.isRunningTest() || leasingHistoryAgreementToUpsert.size() >= 1){
            try{
             	Schema.Sobjectfield externalId = Leasing_History__c.Fields.LeaseDeskAgreementId__c;
				Database.upsert(leasingHistoryAgreementToUpsert,externalId,false);
            	//--------Logging
            	Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'LeaseDeskScheduledCSVLoad', Request__c = 'To Upsert(leasingHistoryAgreementToUpsert): ' + url, Response__c = 'Upsert Success: ' + leasingHistoryAgreementToUpsert.size());
            	insert log; 
            }
            catch(Exception e){
           		//--------Logging
            	Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'LeaseDeskScheduledCSVLoad', Request__c = 'To Upsert(leasingHistoryAgreementToUpsert): ' + url, Response__c = 'Upsert Error: ' + e);
            	insert log;                   
            }
    	}
    	//------------Delete: Leasing status equals 'NotInUse', this prevents duplicate funders records
        if(leasingHistoryProposalsToDelete.size() >= 1){
            List<Leasing_History__c> lhProposals = [Select Id, AgreementID__c from Leasing_History__c where AgreementID__c in :leasingHistoryProposalsToDelete];
            Database.delete(lhProposals,false);
        }  
    }    
        
    public static String StatusToRAGMapping(String status){
        if(status == 'Prospect.Preclearance' || status == 'Prospect.Proposed' || status == 'Prospect.Referral'){
            return 'Amber';
        }
        else if(status == 'Prospect.Acceptance' || status == 'Prospect.PurchaseOrder' || status == 'Prospect.Activation'){
            return 'Green';
        }
        else if(status == 'Prospect.Rejection' || status == 'Prospect.Dead'){
            return 'Red';
        }
        return null;
    }
    
    static Boolean IsLiveDeal(String status){
        if(status == 'Live' || status == 'Upgraded' || status == 'Settled'|| status == 'Arrears'|| status == 'EndOfTerm'|| status == 'SecondaryPeriod'|| status == 'Dead'){
            return true;
        }
        return false;
    }
    
    public static Double QuarterlyPayment(String frequency, Double regularPayment){
        if(frequency != null && regularPayment != null){
            if(frequency == 'Monthly'){
                return regularPayment * 3;
            }
            if(frequency == 'Quarterly'){
                return regularPayment;
            }
            if(frequency == 'Annually'){
                return regularPayment / 4;
            }
            if(frequency == 'Two Monthly'){
                return (regularPayment * 6) / 4;
            }
            if(frequency == 'Four Monthly'){
                return (regularPayment * 3 ) / 4;
            }
            if(frequency == 'Six Monthly'){
                return (regularPayment * 2) / 4;
            }   
        }   
        return null;                
    }   
}