/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 04/11/2020
   @author Maahaboob Basha

   @description Test class for CustomButtonSendToService.

   @modifications
   
*/
@isTest(SeeAllData=FALSE)
private class CustomButtonSendToServiceTest {

    /*******************************************************************************************************
    * Method Name : performActionPositiveTest
    * Description : Used to simulate and test the logic of performAction method in CustomButtonSendToService 
    * Parameters  : NA
    * Return      : NA                      
    *******************************************************************************************************/
    static testmethod void performActionPositiveTest() {
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
        INSERT oliSync;
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        String baseUrl = Label.CS_DiscountRedirect_Org_Url;
        String redirectURL = 'apex/BasketbuilderApp?id=' + basket.Id;
        String successMsg = 'Opportunity Stage is moved to Order Validation';
        Test.startTest();
            CustomButtonSendToService cbService = new CustomButtonSendToService();
            result = cbService.performAction(basket.Id);
        Test.stopTest();
        System.assertNotEquals(NULL, result);
        System.assertEquals('{"status":"ok","redirectURL":"' + redirectURL +'","text":"'+ successMsg + '"}', result);
    }
}