@isTest

private class Test_iDepotController{
    static testMethod void runPositiveTestCases() {
        // test general feebdack  
               iDepot__c  fbk = new iDepot__c  (Confirm__c = 'True', Title__c = 'Test', Commission_Due__c = 10, Status__c = 'Open', Product_Area__c = 'Broadband', Year_Covered__c = '10/11', From__c = Date.today(), To__c = Date.today(), Order_Reference__c = 'VOL011-123456789',Wsx_Commission_Level__c =  'Level 1' ); 
        Database.Saveresult fbkResult = Database.insert(fbk);                
        // test attachment
        Blob attachBody = Blob.valueOf('attachment body');
        Attachment attach = new Attachment (OwnerId = UserInfo.getUserId(), ParentId = fbk.ID, isPrivate = false, body = attachBody, Name = 'test');
        Database.Saveresult attachmentResult = Database.insert(attach);    
        
        // invoke controller actions
        System.currentPageReference().getParameters().put('id',fbk.id);               
        iDepotController controller = new iDepotController (null);
        controller.addNote();  
        controller.upload();  
        controller.getAtt();
        controller.getNoteList();
    }    
}