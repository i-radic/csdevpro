@isTest
public class TestUsageProfileFMController {
    
    static testMethod void createTestData(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account acc = new Account();
        acc.Name = 'Test Acc';
        acc.Sector__c = 'Corporate';
        insert acc;
        
        Usage_Profile__c up = new Usage_Profile__c();
        up.Name = 'UsageProfile1';
        up.Account__c = acc.Id;
        up.Active__c = true;
        insert up;
       
    }
    
    static testMethod void testcase(){
        
        createTestData();
	    Test.StartTest();
	    UsageProfileFMController upf = new UsageProfileFMController();
        upf.SearchUsageProfile();
        upf.getProfile();
        Test.StopTest();
    }

}