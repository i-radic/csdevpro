public class CS_JSON_SendEmail {
    public CS_JSON_Schema.CS_JSON_Email setting {get; set;}
    public Map<String, Object> structure {get; set;}
    
    public static String PROPERTY_TEMPLATE = '\'{\'{0}\'}\'';
    
	/**
	 * Class Constructor
	 * @return 	CS_JSON_SendEmail
	 */
	public CS_JSON_SendEmail(CS_JSON_Schema.CS_JSON_Email emailSetting, Map<String, Object> jsonResult) {
		this.setting = emailSetting;
        this.structure = jsonResult;
	}
    
    public Map<String, String> sendEmail() {
        if(isValid(this.setting.condition, this.structure)) {
            return prepareEmail();
        }
        else {
            return new Map<String, String>();
        }
    }
    
    public Boolean isValid(String condition, Map<String, Object> structure) {
        return getProperty(structure, condition) != null;
    }
    
    public EmailTemplate getTemplate(String templateName) {
        List<EmailTemplate> templates = [Select Id, Name, Body, Subject From EmailTemplate Where DeveloperName = :templateName];
        return templates.size() == 1 ? templates.get(0) : null;
    }
    
    public Map<String, String> prepareEmail() {
        EmailTemplate template = getTemplate(this.setting.template);
        String body = getBody(template.Body);

        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
        email.setToAddresses(new List<String>{this.setting.recipient});
        email.setSubject(template.Subject);
        email.setPlainTextBody(body);

        Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {email});
        
        return new Map<String, String>{template.Subject => body};
    }
    
    public String getBody(String body) {
        for(CS_JSON_Schema.CS_JSON_MergeField mField : setting.mergeFields) {
            String propertyValue = String.valueOf(getProperty(this.structure, mField.path));
            propertyValue = propertyValue != null ? propertyValue : '';
            body = body.replace(String.format(PROPERTY_TEMPLATE, new List<String>{mField.name}), propertyValue);
        }
        
        return body;
    }
    
    public Object getProperty(Map<String, Object> structure, CS_Json_Schema.CS_JSON_MergeFieldPath path) {
        if(path.filters != null && !path.filters.isEmpty()) {
            List<Object> objects = (List<Object>) structure.get(path.value);
            for(Object obj : objects) {
                if(path.evaluate((Map<String, Object>) obj)) {
                    return getProperty((Map<String, Object>) obj, path.subpath);
                }
            }
        }
        else {
            return getProperty(structure, path.value);
        }
        
        return null;
    }
    
    public Object getProperty(Map<String, Object> structure, String propertyName) {
        if(structure == null) {
			return null;
		}
		
		try {
			if(propertyName.contains('.')) {
				List<String> separatedField = propertyName.split('\\.', 2);
				return getProperty((Map<String, Object> ) structure.get(separatedField.get(0)), separatedField.get(1));
			}
			else {
				return structure.get(propertyName);
			}
		}

		catch(SObjectException ex) {
			return null;
		}
    }
}