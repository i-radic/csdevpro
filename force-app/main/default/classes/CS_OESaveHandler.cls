global class CS_OESaveHandler implements csoe.IRemoteAction {
    global Map<String, Object> execute(Map<String, Object> inputMap){
        String configId = (String)inputMap.get('configId');
        String action = (String)inputMap.get('action');
        String basketId = (String)inputMap.get('basketId');
        String definitionName = (String)inputMap.get('definitionName');
        Map<String, Object> outputMap = new Map<String, Object>();

        if(action == 'ImplementationForm'){

            Boolean valid = false;
            Decimal invalidCount = (Decimal)inputMap.get('invalidCount');

            cscfga__Product_Basket__c basket = [SELECT Id, Basket_Totals__r.Id FROM cscfga__Product_Basket__c WHERE Id = :basketId LIMIT 1];
            Basket_Totals__c bt = [SELECT Id, OE_BTM_IF_Valid__c, OE_BTOP_IF_Valid__c FROM Basket_Totals__c WHERE Id = :basket.Basket_Totals__r.Id LIMIT 1];

            if(invalidCount == 0){
                valid = true;
            }
            
            if(definitionName == 'BTMobile'){
                bt.OE_BTM_IF_Valid__c = valid;    
            }

            if(definitionName == 'BTOP'){
                bt.OE_BTOP_IF_Valid__c = valid;    
            }

            outputMap.put('basketTotals', updateObject(bt));
        }

        if(action == 'AccountAdmins'){
            Boolean valid = false;
            Decimal invalidCount = (Decimal)inputMap.get('invalidCount');

            cscfga__Product_Basket__c basket = [SELECT Id, Basket_Totals__r.Id FROM cscfga__Product_Basket__c WHERE Id = :basketId LIMIT 1];
            Basket_Totals__c bt = [SELECT Id, OE_BTM_AA_Valid__c FROM Basket_Totals__c WHERE Id = :basket.Basket_Totals__r.Id LIMIT 1];

            if(invalidCount == 0){
                valid = true;
            }

            bt.OE_BTM_AA_Valid__c = valid;

            outputMap.put('basketTotals', updateObject(bt));
        }

        if(action == 'AppleDEP'){
            String customerAppleId = (String)inputMap.get('customerAppleId');
            Boolean optOut = (Boolean)inputMap.get('optOut');

            cscfga__Product_Configuration__c config = [SELECT Id, cscfga__Product_Basket__c, AccountId__c FROM cscfga__Product_Configuration__c WHERE Id = :configId LIMIT 1];
            cscfga__Product_Basket__c basket = [SELECT Id, csbb__Account__c, Customer_Apple_Id__c, Basket_Totals__r.Id FROM cscfga__Product_Basket__c WHERE Id = :config.cscfga__Product_Basket__c LIMIT 1];
            Basket_Totals__c bt = [SELECT Id, OE_BTM_AD_Valid__c FROM Basket_Totals__c WHERE Id = :basket.Basket_Totals__r.Id LIMIT 1];

            basket.Customer_Apple_Id__c = customerAppleId;
            
            if(customerAppleId == ''){
                bt.OE_BTM_AD_Valid__c = false;
            }else{
                bt.OE_BTM_AD_Valid__c = true;
            }

            if(optOut){
                bt.OE_BTM_AD_Valid__c = true;
            }

            outputMap.put('basket', updateObject(basket));

            if(basket.csbb__Account__c != null){
                Account account = new Account( Id = basket.csbb__Account__c, Customer_Apple_Id__c = customerAppleId );
                outputMap.put('account', updateObject(account));
            }

            outputMap.put('basketTotals', updateObject(bt));
        }

        outputMap.put('OEValidity', validateBasket(definitionName, basketId));
        
        return outputMap;
    }

    private Boolean validateBasket(String definitionName, String basketId){

        cscfga__Product_Basket__c basket = [SELECT Id, Implementation_Form_Valid__c, Basket_Totals__r.OE_BTM_IF_Valid__c, Basket_Totals__r.OE_BTM_AA_Valid__c, Basket_Totals__r.OE_BTM_AD_Valid__c, Basket_Totals__r.OE_BTOP_IF_Valid__c FROM cscfga__Product_Basket__c WHERE Id = :basketId LIMIT 1];
       
        if(definitionName == 'BTMobile'){
            basket.Implementation_Form_Valid__c = basket.Basket_Totals__r.OE_BTM_IF_Valid__c && basket.Basket_Totals__r.OE_BTM_AA_Valid__c && basket.Basket_Totals__r.OE_BTM_AD_Valid__c;
        }

        if(definitionName == 'BTOP'){
            basket.Implementation_Form_Valid__c = basket.Basket_Totals__r.OE_BTOP_IF_Valid__c;
        }
        
        try {
            update basket;
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    private Boolean updateObject(sObject obj){

        try {
            update obj;
            return true;
        } catch(Exception e){
            return false;
        }     

    }
}