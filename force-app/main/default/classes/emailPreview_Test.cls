@isTest
private class emailPreview_Test{
    
    static testMethod void myUnitTest() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        User uM = new User();
        User uM1 = new User();
		System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        
        Profile p = [select id from profile where name='BTLB FOS Warrington'];
        String profileId = p.Id;
                
        
        uM.Username = '9999990001@bt.com';
        uM.Ein__c = 'test123';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = profileId;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});
     
        Profile p1 = [select id from profile where name='BTNI: Standard Team'];
        String profileId1 = p1.Id;
                
        
        uM1.Username = '9999990012@bt.com';
        uM1.Ein__c = 'test345';
        uM1.LastName = 'TestLastname';
        uM1.FirstName = 'TestFirstname';
        uM1.MobilePhone = '07918672032';
        uM1.Phone = '02085878834';
        uM1.Title='What i do';
        uM1.OUC__c = 'DKW';
        uM1.Manager_EIN__c = '123456789';
        uM1.Email = 'no.reply1@bt.com';
        uM1.Alias = 'botid011';
        uM1.TIMEZONESIDKEY = 'Europe/London';
        uM1.LOCALESIDKEY  = 'en_GB';
        uM1.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM1.PROFILEID = profileId1;
        uM1.LANGUAGELOCALEKEY = 'en_US';       
        uM1.email = 'no.reply1@bt.com';
        Database.SaveResult[] uMResult1 = Database.insert(new User [] {uM1});
        }    
        
        contact con = new contact();  
        con.Contact_Address_Street__c = '';
        con.Address_County__c = '';
        con.Contact_Post_Code__c = '';
        con.Address_Country__c = '';
        con.Contact_Post_Town__c = '';                
        con.Contact_Address_Street__c = '';
        con.Address_County__c = '';
        con.Contact_Post_Code__c = '';
        con.Address_Country__c = '';
        con.Contact_Post_Town__c = '';       
        con.Active__c = '';
        con.Comment__c = '';
        con.Type__c = '';
        con.Salutation = '';
        con.FirstName = 'Alan';
        con.MiddleName__c = '';
        con.LastName = 'Jackson';
        con.AliasName__c = '';
        con.ContactSource__c = '';
        con.JobSeniority__c = '';
        con.Profession__c = '';
        con.Birthdate = null;
        con.MotherMaidenName__c = '';
        con.Status__c = 'InActive';
        con.Job_Title__c = '';
        con.Key_Decision_Maker__c = '';
        con.Fax = '';
        con.Organization__c = '';
        con.Department = '';
        con.Preferred_Contact_Channel__c = '';
        con.Secondary_Preferred_Contact_Channel__c = '';
        con.Email_Id__c = '';       
        con.Email_Id__c = '';
        con.Email = 'test@bt.com.com';
        con.Email_Consent__c = '';
        con.Email_Consent_Date__c = null;
        con.Mobile_Id__c = '';
        con.MobilePhone = '';
        con.Mobile_Consent__c = '';
        con.Mobile_Consent_Date__c = null;    
        con.Phone_Id__c = '';
        con.Phone = '0123456789';
        con.Phone_Consent__c = '';
        con.Phone_Consent_Date__c = null;
        con.Fax_Id__c = '';
        con.Fax = '';
        con.Fax_Consent__c = '';
        con.Fax_Consent_Date__c = null;
        con.Contact_Address_Number__c = '';
        con.Address_Id__c = '0000';
        con.Contact_Address_Street__c = '';
        con.Contact_Locality__c = '';
        con.Contact_Locality__c = '';
        con.Address_County__c = '';
        con.Address_Country__c = '';
        con.Contact_Post_Code__c = 'WR5 3RL';
        con.Contact_Post_Town__c = '';
        con.Contact_Sector__c = '';
        con.Address_Name__c = '';
        con.Contact_Zip_Code__c = '';
        con.Address_Consent__c = '';
        con.Address_Consent_Date__c = null; 
        insert con;
        
        
        Campaign c = new Campaign(Name='Second Chance - Mobile', Campaign_Type__c='BTLB', Type='Telemarketing', Status='Planned',X_Day_Rule__c = 3,
         isActive=true, Data_Source__c='Self Generated');
        insert c;
        
        
        
        PageReference emailPreview = Page.emailPreview;
        Test.setCurrentPageReference(emailPreview);
        ApexPages.currentPage().getParameters().put('id', con.id);
     
       emailPreview ep = new emailPreview();
   
        ep.reset();
        ep.test();
        ep.getBtlbMailOptions();
        ep.BTLBmailValue = '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2a&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail';
        ep.sendMail();
        ep.BTLBmailValue = '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Z&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail';
        ep.sendMail();
        ep.BTLBmailValue = '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2Y&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail';
        ep.sendMail();
        ep.BTLBmailValue = '/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IP&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail';
        ep.sendMail();
        ep.BTLBmailValue = '/p/email/template/EmailTemplateEditorUi/s?id=00X20000001y2IO&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail';
        ep.sendMail();
        ep.BTLBmailValue = '';
        ep.sendMail();
        
        system.runAs(uM){
            contact con1 = new contact();  
        con1.Contact_Address_Street__c = '';
        con1.Address_County__c = '';
        con1.Contact_Post_Code__c = '';
        con1.Address_Country__c = '';
        con1.Contact_Post_Town__c = '';                
        con1.Contact_Address_Street__c = '';
        con1.Address_County__c = '';
        con1.Contact_Post_Code__c = '';
        con1.Address_Country__c = '';
        con1.Contact_Post_Town__c = '';       
        con1.Active__c = '';
        con1.Comment__c = '';
        con1.Type__c = '';
        con1.Salutation = '';
        con1.FirstName = 'Alan';
        con1.MiddleName__c = '';
        con1.LastName = 'Jackson';
        con1.AliasName__c = '';
        con1.ContactSource__c = '';
        con1.JobSeniority__c = '';
        con1.Profession__c = '';
        con1.Birthdate = null;
        con1.MotherMaidenName__c = '';
        con1.Status__c = 'InActive';
        con1.Job_Title__c = '';
        con1.Key_Decision_Maker__c = '';
        con1.Fax = '';
        con1.Organization__c = '';
        con1.Department = '';
        con1.Preferred_Contact_Channel__c = '';
        con1.Secondary_Preferred_Contact_Channel__c = '';
        con1.Email_Id__c = '';       
        con1.Email_Id__c = '';
        con1.Email = 'test@bt.com.com';
        con1.Email_Consent__c = '';
        con1.Email_Consent_Date__c = null;
        con1.Mobile_Id__c = '';
        con1.MobilePhone = '';
        con1.Mobile_Consent__c = '';
        con1.Mobile_Consent_Date__c = null;    
        con1.Phone_Id__c = '';
        con1.Phone = '0123456789';
        con1.Phone_Consent__c = '';
        con1.Phone_Consent_Date__c = null;
        con1.Fax_Id__c = '';
        con1.Fax = '';
        con1.Fax_Consent__c = '';
        con1.Fax_Consent_Date__c = null;
        con1.Contact_Address_Number__c = '';
        con1.Address_Id__c = '0000';
        con1.Contact_Address_Street__c = '';
        con1.Contact_Locality__c = '';
        con1.Contact_Locality__c = '';
        con1.Address_County__c = '';
        con1.Address_Country__c = '';
        con1.Contact_Post_Code__c = 'WR5 3RL';
        con1.Contact_Post_Town__c = '';
        con1.Contact_Sector__c = '';
        con1.Address_Name__c = '';
        con1.Contact_Zip_Code__c = '';
        con1.Address_Consent__c = '';
        con1.Address_Consent_Date__c = null; 
        insert con1;
        
        PageReference emailPreview1 = Page.emailPreview;
        Test.setCurrentPageReference(emailPreview1);
        ApexPages.currentPage().getParameters().put('id', con1.id);
     
         emailPreview ep1 = new emailPreview();
            ep1.getBtlbMailOptions();
        ep1.sendMail();}
  
       system.runAs(uM1){
            contact con2 = new contact();  
        con2.Contact_Address_Street__c = '';
        con2.Address_County__c = '';
        con2.Contact_Post_Code__c = '';
        con2.Address_Country__c = '';
        con2.Contact_Post_Town__c = '';                
        con2.Contact_Address_Street__c = '';
        con2.Address_County__c = '';
        con2.Contact_Post_Code__c = '';
        con2.Address_Country__c = '';
        con2.Contact_Post_Town__c = '';       
        con2.Active__c = '';
        con2.Comment__c = '';
        con2.Type__c = '';
        con2.Salutation = '';
        con2.FirstName = 'Alan';
        con2.MiddleName__c = '';
        con2.LastName = 'Jackson';
        con2.AliasName__c = '';
        con2.ContactSource__c = '';
        con2.JobSeniority__c = '';
        con2.Profession__c = '';
        con2.Birthdate = null;
        con2.MotherMaidenName__c = '';
        con2.Status__c = 'InActive';
        con2.Job_Title__c = '';
        con2.Key_Decision_Maker__c = '';
        con2.Fax = '';
        con2.Organization__c = '';
        con2.Department = '';
        con2.Preferred_Contact_Channel__c = '';
        con2.Secondary_Preferred_Contact_Channel__c = '';
        con2.Email_Id__c = '';       
        con2.Email_Id__c = '';
        con2.Email = 'test@bt.com.com';
        con2.Email_Consent__c = '';
        con2.Email_Consent_Date__c = null;
        con2.Mobile_Id__c = '';
        con2.MobilePhone = '';
        con2.Mobile_Consent__c = '';
        con2.Mobile_Consent_Date__c = null;    
        con2.Phone_Id__c = '';
        con2.Phone = '0123456789';
        con2.Phone_Consent__c = '';
        con2.Phone_Consent_Date__c = null;
        con2.Fax_Id__c = '';
        con2.Fax = '';
        con2.Fax_Consent__c = '';
        con2.Fax_Consent_Date__c = null;
        con2.Contact_Address_Number__c = '';
        con2.Address_Id__c = '0000';
        con2.Contact_Address_Street__c = '';
        con2.Contact_Locality__c = '';
        con2.Contact_Locality__c = '';
        con2.Address_County__c = '';
        con2.Address_Country__c = '';
        con2.Contact_Post_Code__c = 'WR5 3RL';
        con2.Contact_Post_Town__c = '';
        con2.Contact_Sector__c = '';
        con2.Address_Name__c = '';
        con2.Contact_Zip_Code__c = '';
        con2.Address_Consent__c = '';
        con2.Address_Consent_Date__c = null; 
        insert con2;
           
        Campaign c1 = new Campaign(Name='BTNI Second Chance - Packages', Campaign_Type__c=' LEM Base', Type='Telemarketing', Status='Planned',X_Day_Rule__c = 3,
         isActive=true, Data_Source__c='Self Generated');
        insert c1;
        
        PageReference emailPreview2 = Page.emailPreview;
        Test.setCurrentPageReference(emailPreview2);
        ApexPages.currentPage().getParameters().put('id', con2.id);
     
         emailPreview ep2 = new emailPreview();
            ep2.getBtlbMailOptions();
           ep2.BTLBmailValue = '/p/email/template/EmailTemplateEditorUi/s?id=00X200000014q2a&TemplateType=2&TemplateStyle=0&setupid=CommunicationTemplatesEmail';
        ep2.sendMail();
    }
}
    
}