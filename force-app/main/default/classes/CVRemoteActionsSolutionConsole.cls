global class CVRemoteActionsSolutionConsole implements cssmgnt.RemoteActionDataProvider {
    global static Map<String, Object> getData(Map<String, Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>();
        
        if(String.valueOf(inputMap.get('action'))=='CS_SendToService') {
            String basketId = String.valueOf(inputMap.get('basketId'));
            try {
                Id oppId = [SELECT cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId].cscfga__Opportunity__c;
                Opportunity opp = [Select Order_Validation_Start_Date__c FROM Opportunity WHERE Id = :oppId];
                opp.Order_Validation_Start_Date__c  = System.today();
                update opp;
                
                returnMap.put('status', 'ok');
                returnMap.put('message', 'Sent to Service');
            }catch(Exception ex) {
                returnMap.put('status', 'error');    
                returnMap.put('message', 'Send to Service Failed '+ex);
            }
        }
        
        if(String.valueOf(inputMap.get('action'))=='CS_TakeOwnership') {
            String basketId = String.valueOf(inputMap.get('basketId'));
            try {
                cscfga__Product_Basket__c  basket = [SELECT Service_Owner__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
                basket.Service_Owner__c  = UserInfo.getUserId();
                update basket;
                
                returnMap.put('status', 'ok');
                returnMap.put('message', 'Took Ownership');
            }catch(Exception ex) {
                returnMap.put('status', 'error');
                returnMap.put('message', 'Take Ownsership Failed '+ex);
            }
        }

        if(String.valueOf(inputMap.get('action'))=='CS_SaveTSV') {
            String configId = String.valueOf(inputMap.get('configId'));
            try {
                cscfga__Product_Configuration__c  mainConfig = [SELECT Total_Solution_Value__c FROM cscfga__Product_Configuration__c WHERE Id = :configId];
                mainConfig.Total_Solution_Value__c  = Decimal.valueOf((Double) inputMap.get('Total Solution Value'));
                update mainConfig;

                csord__Solution__c sol = [SELECT cssdm__total_one_off_charge__c, cssdm__total_recurring_charge__c FROM csord__Solution__c WHERE Id = :String.valueOf((String) inputMap.get('solutionId'))];
                sol.cssdm__total_one_off_charge__c = Decimal.valueOf((Double) inputMap.get('Total One Off Value'));
                sol.cssdm__total_recurring_charge__c = Decimal.valueOf((Double) inputMap.get('Total Recurring Value'));

                update sol;

                returnMap.put('status', 'ok');
            }catch(Exception ex) {
                returnMap.put('status', 'error');
            }
            returnMap.put('status', 'ok');
        }
        
        if(String.valueOf(inputMap.get('action'))=='CS_SynchronizeWithOpportunity') {
            String basketId = String.valueOf(inputMap.get('basketId'));
            try {
                returnMap.put('status', new CustomButtonSyncWithOpp().performAction(basketId));
            }catch(Exception ex) {}
        }
        
        if(String.valueOf(inputMap.get('action'))=='CS_LookupOpportunitySolutionConsole') {
            String basketId = String.valueOf(inputMap.get('basketId'));
            
            cscfga__Product_Basket__c basket =  [SELECT cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
            returnMap.put('opportunityId', [SELECT cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId].cscfga__Opportunity__c);
            returnMap.put('oppStage', [SELECT StageName FROM Opportunity WHERE Id = :basket.cscfga__Opportunity__c].StageName);
         }
    
        if(String.valueOf(inputMap.get('action'))=='getConfigInfoRemote') {
            try{
                String configId=(String) inputMap.get('configID');
                List<cscfga__Product_Configuration__c> configs = [select id,BTnet_Primary_Port_Speed__c 
                                                                from cscfga__Product_Configuration__c 
                                                                where id=:configId];
                System.debug(configs);
                System.debug(configs[0]);
    
                returnMap.put('config',configs[0]);
                returnMap.put('speed',configs[0].BTnet_Primary_Port_Speed__c);
            }
            catch (exception e) {
                returnMap.put('exception', e.getMessage());
            }
        }

        if(String.valueOf(inputMap.get('action'))=='CS_LookupQueryForAccountDetails') {
            String basketId = String.valueOf(inputMap.get('basketId'));
            try{
                List<cscfga__Product_Basket__c> productBasketList= [select id,Name,csbb__Account__c from cscfga__Product_Basket__c where id =:basketId];
                if(!productBasketList.isEmpty() && basketId!=null){
                    returnMap.put ('AccountId', productBasketList[0].csbb__Account__c);
                    returnMap.put ('DataFound', true);
                }else{
                     returnMap.put ('DataFound', false);
                }
            }catch(Exception e){}
        }

        if(String.valueOf(inputMap.get('action'))=='CS_SolutionConsoleBasketCalculation') {
            try {
                String basketId = String.valueOf(inputMap.get('basketId'));
                cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
                basket.Synchronised_with_Opportunity__c = false;
                basket.csbb__Synchronised_With_Opportunity__c = false;
                basket.csordtelcoa__Synchronised_with_Opportunity__c = false;
                CS_SolutionDealCalculations dealCalculations = new CS_SolutionDealCalculations(basket);
                dealCalculations.calculateBasketAttributes();
                dealCalculations.calculateBasketApprovals();
                update basket;
                returnMap.put ('DataFound', true);
            } catch(Exception exc) {
                returnMap.put('exception', exc.getMessage());
            }
        }

        if(String.valueOf(inputMap.get('action'))=='CS_SolutionConsoleDesyncBasket') {
            String basketId = String.valueOf(inputMap.get('basketId'));
            cscfga__Product_Basket__c basket = [SELECT Synchronised_with_Opportunity__c, csbb__Synchronised_With_Opportunity__c, csordtelcoa__Synchronised_with_Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
            basket.Synchronised_with_Opportunity__c = false;
            basket.csbb__Synchronised_With_Opportunity__c = false;
            basket.csordtelcoa__Synchronised_with_Opportunity__c = false;
            update basket;
        }

        if(String.valueOf(inputMap.get('action'))=='CS_LookupQueryForSolutionConsole') {
            String module = String.valueOf(inputMap.get('module'));
            String grouping = String.valueOf(inputMap.get('grouping'));
            String contractTerm = String.valueOf(inputMap.get('contractTerm'));
            try{
                List<cspmb__Price_Item__c> priceItemList= [select id,Name from cspmb__Price_Item__c where Module__c =:module and Grouping__c =:grouping and cspmb__Contract_Term__c = :contractTerm ];
                if(!priceItemList.isEmpty()){
                    returnMap.put ('PriceItemId', priceItemList[0].id);
                    returnMap.put ('PriceItemName', priceItemList[0].Name);
                    returnMap.put ('DataFound', true);
                }else{
                     returnMap.put ('DataFound', false);
                }
                
            }catch(Exception e){}
        } 
        
        if(String.valueOf(inputMap.get('action'))=='CS_BTNetLookupQueryForSolutionConsole') {
            Integer accessType = Integer.valueOf(inputMap.get('accessType'));
            String contractTerm = String.valueOf(inputMap.get('contractTerm'));
            String geography = String.valueOf(inputMap.get('geography'));
            String standardLA = String.valueOf(inputMap.get('standardLA'));
            try{
                List<cspmb__Price_Item__c> priceItemList= [select id,Name from cspmb__Price_Item__c where Access_Type__c =:standardLA and Bearer_Speed__c =:accessType and cspmb__Contract_Term__c = :contractTerm and Geography__c =:geography and cspmb__Is_Active__c = TRUE and Grouping__c = 'Bearer' and Module__c = 'Access' and Record_Type_Name__c = 'BTnet'];
                if(!priceItemList.isEmpty()){
                    returnMap.put ('PriceItemId', priceItemList[0].id);
                    returnMap.put ('PriceItemName', priceItemList[0].Name);
                    returnMap.put ('DataFound', true);
                }else{
                     returnMap.put ('DataFound', false);
                }
                
            }catch(Exception e){}
        }
        
        if(String.valueOf(inputMap.get('action'))=='CS_BTNetBandwidthForSolutionConsole') {
            String piId = String.valueOf(inputMap.get('piId'));
            try{
                List<cspmb__Price_Item__c> priceItemList= [select id,Bandwidth__c from cspmb__Price_Item__c where Id = :piId];
                if(!priceItemList.isEmpty()){
                    returnMap.put ('Bandwidth', priceItemList[0].Bandwidth__c);
                    returnMap.put ('DataFound', true);
                }else{
                     returnMap.put ('DataFound', false);
                }
                
            }catch(Exception e){}
        }
        
        if(String.valueOf(inputMap.get('action'))=='getProfileInfoRemote') {
            try{
                String basketId=(String) inputMap.get('basketID');
                List<cscfga__Product_Basket__c> baskets = [select id,Owner.Profile.name from cscfga__Product_Basket__c where id=:basketId];
                System.debug(baskets);
                System.debug(baskets[0]);
                returnMap.put('config',baskets[0]);
                returnMap.put('profile',baskets[0].Owner.Profile.name);
            }
            catch (exception e) {
                returnMap.put('exception', e.getMessage());
            }
        }

        if(String.valueOf(inputMap.get('action'))=='isProfileQueuedRemote') {
            try{
                String goodProfile = 'false';
                Id userID = UserInfo.getUserId();
                id id1 = userinfo.getProfileId();
                profile userProfile = [select Name from profile where id = :id1];

                system.debug(userProfile);

                if(userProfile.name=='System Administrator') goodProfile='true';
                else {
                    List<Group> groups = [select Id,name from Group where Type = 'Queue' and name = 'Pricing Support Unit'];
                    List<GroupMember> users = [Select UserOrGroupId from GroupMember where GroupId = :groups[0].Id];
                    for (GroupMember oneUser : users) {
                        if (oneUser.UserOrGroupId == userID) goodProfile = 'true';
                    }
                }
                returnMap.put('goodProfile',goodProfile);
            }
            catch (exception e) {
                returnMap.put('exception', e.getMessage());
            }
        }
        
        if(String.valueOf(inputMap.get('action'))=='getSharedVAS') {
            String servicePlanId = String.valueOf(inputMap.get('servicePlanId'));
            String addOnType = String.valueOf(inputMap.get('addOnType'));
            String addOngroup = String.valueOf(inputMap.get('addOngroup'));
            Integer noOfUsers = Integer.valueOf(inputMap.get('noOfUsers'));
            try {
                List<cspmb__Price_Item_Add_On_Price_Item_Association__c> commPrdAddOnAssocs = [SELECT Id, Name, cspmb__Group__c, Add_On_Name__c, cspmb__Add_On_Price_Item__r.Name, cspmb__Add_On_Price_Item__r.Grouping__c, cspmb__Add_On_Price_Item__r.Minimum_Connections__c, cspmb__Add_On_Price_Item__r.Maximum_Connections__c, cspmb__Add_On_Price_Item__r.Contract_Type__c, cspmb__Add_On_Price_Item__r.cspmb__Recurring_Charge__c, cspmb__Add_On_Price_Item__r.cspmb__One_Off_Charge__c, cspmb__Add_On_Price_Item__r.cspmb__Billing_Frequency__c, cspmb__Add_On_Price_Item__r.cspmb__Contract_Term__c FROM cspmb__Price_Item_Add_On_Price_Item_Association__c WHERE cspmb__Price_Item__c = :servicePlanId AND cspmb__Add_On_Price_Item__r.cspmb__Is_Active__c = TRUE AND Module__c = 'Shared VAS' AND Add_On_Type__c = :addOnType AND cspmb__Group__c = :addOngroup];
                for (cspmb__Price_Item_Add_On_Price_Item_Association__c commPrdAddOnAssoc : commPrdAddOnAssocs) {
                    if (commPrdAddOnAssoc.cspmb__Group__c == 'Account Bill' || (noOfUsers >= commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.Minimum_Connections__c && noOfUsers <= commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.Maximum_Connections__c)) {
                        returnMap.put('Group', commPrdAddOnAssoc.Id);
                        returnMap.put('GroupName', commPrdAddOnAssoc.cspmb__Group__c);
                        returnMap.put('Product', commPrdAddOnAssoc.Id);
                        returnMap.put('ProductName', commPrdAddOnAssoc.Add_On_Name__c);
                        returnMap.put('RecurringCharge', commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Charge__c);
                        returnMap.put('OneOffCharge', commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Charge__c);
                        returnMap.put('ProductBillingFrequency', commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.cspmb__Billing_Frequency__c);
                        returnMap.put('MinimumConnections', commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.Minimum_Connections__c);	
                        returnMap.put('MaximumConnections', commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.Maximum_Connections__c);	
                        returnMap.put('ContractType', commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.Contract_Type__c);
                        returnMap.put('MinContractTerm', commPrdAddOnAssoc.cspmb__Add_On_Price_Item__r.cspmb__Contract_Term__c);
                    }
                }
            }catch(Exception ex) {
                returnMap.put('status', 'error');
            }
        }
    system.debug(returnMap);
    return returnMap;
    }
}