/************************************************************************************************************************
 * 			Name		:		PhaseListApex
 * 			Test Class	:		Test_PhaseListApex
 * 			VF Page		:		PhaseListSLDS
 * 			Description	:		Added a VF Page related list to edit Phase records related to Project objects on the go.
 * 			Author		:		BALAJI MS
 * 			Date		:		15-11-2017
 * 			Version		:		v1.0
 ***********************************************************************************************************************/
public class PhaseListApex {
    public List<Phase__c> PhaseRecordList;
	public static String errorMessage{get; set;}
    
    public PhaseListApex(ApexPages.StandardController controller){
        PhaseRecordList = new List<Phase__c>();
        PhaseRecordList = [Select Name, Milestones__c, RAG_Status__c, DB_Received_Date__c, Hot_Staging_Date__c, Hot_Staging_Reference__c, Handover_Date__c, Porting_Start_Date__c, Porting_End_Date__c FROM Phase__c where Project__c =: ApexPages.currentPage().getParameters().get('id')];
    }
    
    public List<Phase__c> getPhaseRecords(){
        return PhaseRecordList;
    }
    
    public pagereference Save(){
        errorMessage = null;
        try {
            update PhaseRecordList;
            Apexpages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Stages Updated Successfully'));
        } catch (Dmlexception e) {
            errorMessage = 'Problem whilst saving, please ensure you have entered all the required data for stage ' + [Select Name FROM Phase__c WHERE Id = :e.getDmlId(0)].Name + '. ' + e.getMessage().substring(e.getMessage().indexOf(',')+1);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,errorMessage));
            return null;
        }
        return null;
    }
    
    public pagereference Cancel(){
        return null;
    }
}