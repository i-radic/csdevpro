/*
###########################################################################
# File..................: TestContractOutcomeValidationTrigger
# Created by............: Sridhar Aluru
# Created Date..........: 30-Apr-2016
# Last Modified by......: Ashok Kumar Varma
# Last Modified Date....: 31-May-2018
# Description...........: Test class for validating the behavior of Apex Trigger : ContractOutcomeValidationTrigger
# Change Log............: Migrated as a part of EE Migration.
###########################################################################
*/
@isTest
private class TestContractOutcomeValidationTrigger {

    static testMethod void runContractOutcomeValidationTrigger () {
        test.starttest();
        Contract_Validation_Outcome__c convalout = Test_Utils.createContValidOutcome();   
        System.assertNotEquals(convalout.Id, null);   
        convalout.Reason_for_Failure__c='Resign Schedule missing/incomplete;Credit Check unsigned;Devices - incorrect pricing'; 
        update convalout;
        convalout.Reason_for_Failure__c='Devices - incorrect pricing'; 
        update convalout;
        convalout.Reason_for_Failure__c=''; 
        update convalout;
        test.stoptest();
    } 
}