public with sharing class ADPLandscapeViewController {

	private static String reportName;
	private static String specialistReportName;

	private Report mainReport;
	private Report specialistReport;

	private ADP_Landscape__c landscape;

    private List<SelectOption> recordTypes;
    private List<SelectOption> types;
    private List<SelectOption> rollingContract;

    public String reportURL {get; set;}
    public List<String> selectedRecordTypes {get; set;}
    public String selectedType {get; set;}
    public String selectedContract {get;set;}
    public String ownerOrManagerName {get; set;}

    public ADPLandscapeViewController(ApexPages.StandardController controller) {
    	landscape = (ADP_Landscape__c) controller.getRecord();
    	ownerOrManagerName = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
    	reportURL = '';
    	try {
    	    mainReport = [Select Id From Report Where Name = 'ADP Landscape'];
    	    specialistReport = [Select Id From Report Where Name = 'ADP Landscape - Specialist'];
    	} catch (Exception e) {
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'The Visualforce page was unable to locate a source ADP Landscape report. Please inform your Salesforce administrator.'));
    	}
    }

    public List<SelectOption> getRecordTypes() {
    	recordTypes = new List<SelectOption>();
    	for (RecordType rt : [Select Id, Name
    	                      From RecordType
    	                      Where SobjectType = 'ADP_Landscape__c'
    	                      And Name <> 'SIP'
    	                      Order By Name ASC]) {
    		recordTypes.add(new SelectOption(rt.Name, rt.Name));
    	}
    	return recordTypes;
    }

    public List<SelectOption> getTypes() {
    	types = new List<SelectOption>();
    	types.add(new SelectOption('Both', 'Both'));
    	types.add(new SelectOption('Acquisition', 'Acquisition'));
    	types.add(new SelectOption('Defence', 'Defence'));
    	return types;
    }


    public List<SelectOption> getRollingContract(){
    	rollingContract = new List<SelectOption>();
    	rollingContract.add(new SelectOption('True','True'));
    	rollingContract.add(new SelectOption('False','False'));
    	return rollingContract;
    }

    public PageReference updateReportURL() {
        // Report ID
        reportURL = '/' + mainReport.Id;
        reportURL = reportURL + getReportParams(false);
        return null;

    }

    public PageReference updateReportURLSpecialist() {
        // Report ID
        reportURL = '/' + specialistReport.Id;
        reportURL = reportURL + getReportParams(true);
        return null;
    }

    public String getReportParams(Boolean isSpecialist) {

        String sReportURL = '?pv0=';

        // Record types
        for (String s : selectedRecordTypes) {
        	sReportURL = sReportURL + EncodingUtil.urlEncode(s, 'UTF-8') + ',';
        }
        if (sReportURL.endsWith(',')) {
        	sReportURL = sReportURL.substring(0, sReportURL.length() - 1);
        }

        // Contract start and end dates
        Date dt1 = landscape.Contract_Expiry_Date__c;
        Date dt2 = landscape.Maintenance_Expiry_Date__c;

        if (dt1 != null) {
            sReportURL = sReportURL + '&pv1=' + dt1.month() + '/' + dt1.day() + '/' + dt1.year();
        }

        if (dt2 != null) {
            sReportURL = sReportURL + '&pv2=' + dt2.month() + '/' + dt2.day() + '/' + dt2.year();
        }

        sReportURL = sReportURL + '&pv3=1';

        if (selectedType.equals('Defence')) {
        	sReportURL = sReportURL + '&pv4=' + selectedType;
        	sReportURL = sReportURL + '&pv5=' + 'NOVALUE';
        }

        if (selectedType.equals('Acquisition')) {
        	sReportURL = sReportURL + '&pv4=' + 'NOVALUE';
        	sReportURL = sReportURL + '&pv5=' + selectedType;
        }

        if (landscape.Customer__c == null) {
        	sReportURL = sReportURL + '&pv6=001';
        } else {
        	sReportURL = sReportURL + '&pv6=' + landscape.Customer__c;
        }

        if (landscape.Annual_Rental_Cost__c != null) {
        	sReportURL = sReportURL + '&pv7=' + landscape.Annual_Rental_Cost__c;
        }

        if (landscape.Annual_Maintenance_Cost__c != null) {
        	sReportURL = sReportURL + '&pv8=' + landscape.Annual_Maintenance_Cost__c;
        }

        if ((ownerOrManagerName != null) && ( ! isSpecialist )){
        	sReportURL = sReportURL + '&pv9=' + ownerOrManagerName;
        }

        //sReportURL = sReportURL + '&isdtp=mn';
        sReportURL = sReportURL + '&isdtp=nv';

        System.debug('sReportURL: ' + sReportURL);
        return sReportURL;
    }

}