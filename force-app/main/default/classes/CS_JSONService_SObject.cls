/**
 * @name CS_JSONService_SObject
 * @description 
 * @revision
 *
 */
public class CS_JSONService_SObject extends CS_JSONService_Base {
	/**
	 * Class Constructor
	 * @return 	CS_JSONService_SObject
	 */
	public CS_JSONService_SObject() {
		super();
	}
	
	public override Object getResult(Id id) {
		List<SObject> records = Database.query(getQuery(this.setting.definitions.get(0), id));
		Map<String, Object> result = getValues(this.setting.definitions.get(0), records.get(0));
		
		if(this.setting.definitions.get(0).relatedObjects != null && !this.setting.definitions.get(0).relatedObjects.isEmpty()) {
			for(CS_JSON_Schema.CS_JSON_Object obj : this.setting.definitions.get(0).relatedObjects) {
				Id parentId = String.valueOf(records.get(0).get(obj.parentField));
				List<SObject> relatedRecords = Database.query(getQuery(obj, parentId));
				if(!relatedRecords.isEmpty()) {
					result.putAll(getValues(obj, relatedRecords.get(0)));
				}
			}
		}
		
		return result;
	}
	
	@TestVisible
	private String getQuery(CS_JSON_Schema.CS_JSON_Object def, Id id) {
		return String.format(QueryTemplate, new List<String>{
			getFields(def.fields),
			def.type,
			getFilter(def.filter, id)
		});
	}
	
	@TestVisible
	private String getFields(List<CS_JSON_Schema.CS_JSON_Field> fieldsList) {
		return CS_Utl_Array.join(getFieldNames(fieldsList), ',');
	}
	
	@TestVisible
	private Set<String> getFieldNames(List<CS_JSON_Schema.CS_JSON_Field> fieldsList) {
		Set<String> fieldNames  = new Set<String>();
		for(CS_JSON_Schema.CS_JSON_Field field : fieldsList) {
			fieldNames.add(field.Name);
		}
		
		return fieldNames;
	}
	
	@TestVisible
	private String getFilter(String filter, Id id) {
		return String.format(filter, new List<String>{escapeId(id)});
	}
}