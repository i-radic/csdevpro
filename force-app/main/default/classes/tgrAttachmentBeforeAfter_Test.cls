@isTest
private class tgrAttachmentBeforeAfter_Test {
  
  static testMethod void myUnitTest() {
    User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        
    Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
    User u1 = new User(
                       alias = 'B2B1', email = 'B2B13cr@bt.com',
                       emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                       languagelocalekey = 'en_US',
                       localesidkey = 'en_US', ProfileId=prof.Id,
                       timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@bt.com',
                       EIN__c = 'OTHER_def'
                       );
    insert u1;   
            
    Account a = Test_Factory.CreateAccount();
        a.name = 'TESTCODE 2';
        a.sac_code__c = 'testSAC';
        a.Sector_code__c = 'CORP';
        a.LOB_Code__c = 'LOB';
        a.OwnerId = u1.Id;
        insert a;
         Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        Battleplan__c batPlan = new Battleplan__c(Opportunity_Name__c = o.Id);
      insert batPlan;
            
      RecordType rt_BSP = [select id from RecordType where SobjectType='Customer_Sales_Support__c' and name = 'Bespoke Pricing' limit 1];
      
      Customer_Sales_Support__c css = new Customer_Sales_Support__c(Opportunity_Name__c = o.Id, RecordTypeId = rt_BSP.Id, Level_2_Attached__c = true, Full_Evaluation_Attached__c = true);
      insert css;
      //Support Request
      
      Support_Request__c sup = new support_Request__c(Company__c=a.id,Opportunity__c = o.Id, Expected_Data_Connections__c=50.00,Expected_Voice_Connections__c=60.00,Sales_Team__c='CAM', Support_Required_From_Date__c = Date.newInstance(2019 , 10 ,10), Supporting_Information_Details__c='test',What_is_complex_bespoke__c='test');
      insert sup;
      
      Blob bodyBlob5=Blob.valueOf('Unit Test5 Attachment Body');
      
      Attachment attachment5=new Attachment(); 
      attachment5.Name='An attachment 51';
      attachment5.body=bodyBlob5; 
      attachment5.parentId=sup.id;
    insert attachment5;
      
      //
      
      Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
      
      Attachment attachment=new Attachment(); 
      attachment.Name='An attachment 1';
      attachment.body=bodyBlob; 
      attachment.parentId=css.id;
    insert attachment;
    
    Attachment attachment1=new Attachment(); 
      attachment1.Name='An attachment 2';
      attachment1.body=bodyBlob; 
      attachment1.parentId=css.id;
    insert attachment1;
    
    Attachment attachment2=new Attachment(); 
      attachment2.Name='An attachment 3';
      attachment2.body=bodyBlob; 
      attachment2.parentId=css.id;
    insert attachment2;
           
      Attachment attachment3=new Attachment(); 
      attachment3.Name='An attachment 4';
      attachment3.body=bodyBlob; 
      attachment3.parentId=a.id;
    insert attachment3;
    
    Attachment attachment4=new Attachment(); 
      attachment4.Name='An attachment 5';
      attachment4.body=bodyBlob; 
      attachment4.parentId=o.id;      
    insert attachment4;
    
    update attachment1;
    
    update attachment2;
    
    delete attachment1;
    
    delete attachment2;
    
    update attachment5;
    
    delete attachment5;
      
  }
  }

}