@istest

private class Test_OrderBinding{
    static testmethod void RunTest(){
       Test_Factory.SetProperty('IsTest', 'yes');
        insert Test_Factory.CreateAccountForDummy();
        
        Account account = Test_Factory.CreateAccount();
        Database.SaveResult resultaccount  = Database.Insert(account);
        
        Opportunity oppty = Test_Factory.CreateOpportunity(resultaccount.getId());
        Database.SaveResult resultOppty = Database.Insert(oppty);
        oppty = [SELECT Opportunity_Id__c FROM Opportunity WHERE id=:resultOppty.getID() LIMIT 1];
        
        OrderBinding.BindOrderToObject(oppty.Opportunity_Id__c, 'testReference', 'ein');
        OrderBinding.BindOrderToObject(oppty.Opportunity_Id__c, 'testReferenceResubmitTest', 'ein');
        OrderBinding.BindOrderToObject(oppty.Opportunity_Id__c, 'testReferenceResubmitTest', 'ein');
    }
}