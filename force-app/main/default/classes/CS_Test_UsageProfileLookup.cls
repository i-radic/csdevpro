@isTest
private class CS_Test_UsageProfileLookup {

	private static Map<String, String> searchFieldsMap = new Map<String, String>();    
    private static String prodDefinitionID;
    private static Id[] excludeIds;
    private static Integer pageOffset;
    private static Integer pageLimit;
    
    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;

        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;
        
        Usage_Profile__c up1 = new Usage_Profile__c(
            Active__c = true,
            Account__c = testAcc.Id,
            Expected_SMS__c = 20,
            Expected_Voice__c = 10,
            Expected_Data__c = 20,
            Total_Number_of_Connections__c = 2,
            Data_Only_Connections__c = 1
        );
        
        insert up1;
        
        cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
            Name = 'Test',
            Default_Usage_Profile__c = up1.Id
        );
        insert pi1;
        
        List<Usage_Profile__c> lup = new List<Usage_Profile__c>();
        lup.add(up1);
        
        Attachment att = new Attachment(
                Name = 'UsageProfile',
                ParentId = testBasket.Id,
                Body = Blob.valueOf(JSON.serialize(lup))
            );
        insert att;
        
        searchFieldsMap.put('Product Basket Id', testBasket.Id);
        searchFieldsMap.put('Service Plan Level', pi1.Id);
        
    }
    
    private static void createTestData2() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;

        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;
        
        Usage_Profile__c up1 = new Usage_Profile__c(
            Active__c = true,
            Account__c = testAcc.Id,
            Expected_SMS__c = 20,
            Expected_Voice__c = 10,
            Expected_Data__c = 20,
            Total_Number_of_Connections__c = 2,
            Data_Only_Connections__c = 1
        );
        
        insert up1;
        
        cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
            Name = 'Test',
            Default_Usage_Profile__c = up1.Id
        );
        insert pi1;
        
        searchFieldsMap.put('Service Plan Level', pi1.Id);
        
    }

	private static testMethod void fromAttachment() {
	    
	    createTestData();
	    Test.StartTest();
	    CS_UsageProfileLookup lkp = new CS_UsageProfileLookup();
	    Object[] result = lkp.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}
	
	private static testMethod void fromPriceItem() {
	    
	    createTestData2();
	    Test.StartTest();
	    CS_UsageProfileLookup lkp = new CS_UsageProfileLookup();
	    Object[] result = lkp.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}

}