@isTest
public class MobileCaseTgrTest {
    
    //Method to create user with inactive manager
    private static User createUser(){
      
        id userProfileId = [select id,Name from Profile where Name='System Administrator'].id; 
        id mgr = [select id from user where profileid !=: userProfileId limit 1].id;

        user inactiveMgr = new user(title = 'Mr', CompanyName = 'BT', division = 'TSO', EIN__c = '112121011',Alias = 'mertgr',Email='standarduserManger@testorg9.com',EmailEncodingKey='UTF-8',LastName='TestingThemanger', LanguageLocaleKey='en_US', LocaleSidKey='en_US',ProfileId = userProfileId,TimeZoneSidKey='America/Los_Angeles',
                         UserName='GJstandardusmanager@testorg2.com', MobilePhone='+12345673322', ManagerId = mgr, isActive = false);              
        insert inactiveMgr;
            
        user u = new user(title = 'Mr', CompanyName = 'BT', division = 'TSO', EIN__c = '11144421',Alias = 'bigjoe',Email='standarduser211@testorg2.com',EmailEncodingKey='UTF-8',LastName='TestingThis', LanguageLocaleKey='en_US', LocaleSidKey='en_US',ProfileId = userProfileId,TimeZoneSidKey='America/Los_Angeles',
                         UserName='GJstandarduser111@testorg2.com', MobilePhone='+12345678910', SF_Mobile_Number__c = '+447979733379', ManagerID = inactiveMgr.id);   
        insert u;           
        return u;
        }

    //Method to create user with active manager
    private static User createUserThree(){
            
        //Inserting test User
        id userProfileId = [select id,Name from Profile where Name='System Administrator'].id; 
        id mgr = [select id from user where profileid !=: userProfileId limit 1].id;

        user u= new user(title = 'Mr', CompanyName = 'BT', division = 'TSO', EIN__c  = '1114', Alias = 'standtb', Email='standarduser336@testorg2.co', EmailEncodingKey='UTF-8', LastName='Testinggg', LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = userProfileId, TimeZoneSidKey='America/Los_Angeles', 
                         UserName='BTstandarduser333@testorg2.com', MobilePhone='+12345678900', SF_Mobile_Number__c = '+447979790001', ManagerId = mgr);
            
        insert u;         
        return u;
    }
    
    //Method to raise case
    private static case createCase(){
        case c = new case();
        c.RecordTypeId = [select id from recordType where DeveloperName = 'mobile_app'].id;
        c.status = 'new'; 
        c.Mobile_App_Number__c = '+440505050505';
        insert c;
        return c;
    }
    
    static testmethod void mobileCaseTest(){
        user userRecord = createUser();
        user userThree = createUserThree();
        
        //Run for iOS user
        system.runAs(userRecord){
            //Create case and send for approval
            case caseRecord = createCase();
        
            //Find approval and approve
            id pi = [SELECT Id FROM ProcessInstance where targetobjectid =: caseRecord.id ORDER BY CreatedDate DESC limit 1].id;
            id pwi = [select id from processinstanceworkitem where processinstanceid =: pi order by createdDate Desc limit 1].id;
            Approval.ProcessWorkitemRequest pwr = new Approval.ProcessWorkitemRequest();
            pwr.setWorkitemId(pwi);
            pwr.setAction('Approve');
            Approval.ProcessResult result =  Approval.process(pwr);

                
    }
    
}
}