public with sharing class MACEmail{
    public string msg='';
    public Contact cont{ get; set; }
    public string ContURL { get; set; }
    Id ConId;  
    public MACEmail(ApexPages.StandardController Controller) {
    ConId=Apexpages.currentPage().getParameters().get('Contactid');
    cont=[Select Email,Name,Mac_code__c from Contact where Id=:ConId]; 
       
    }
    Id UId = UserInfo.getUserId();
    String uName=[Select Name from User where Id=:UId].Name;
    String email=[Select Email from User where Id=:UId].Email;
    
    
    public void actionDo(){
    try{
    Id fromaddr=[select Id,Address from OrgWideEmailAddress where DisplayName=:'BT Business No Reply'].Id;  
    String EmailTemplateSubject= [Select Subject From EmailTemplate where developername =:'MAC_eMail'].Subject;
    
    EmailTemplate EmailTemplateBody= [Select body From EmailTemplate where developername =:'MAC_eMail'];
    String tempbody=EmailTemplateBody.body;
    
    //tempbody=tempbody.replace('{!Contact.LastName},','{!Contact.LastName},');
    tempbody=tempbody.replace('{!Contact.FirstName} {!Contact.LastName},',cont.Name+',');
    //tempbody=tempbody.replace('We’re sorry that you’re leaving us.','We’re sorry that you’re leaving us.<br/>');
    //tempbody=tempbody.replace('Service Provider to migrate your Broadband Service.','Service Provider to migrate your Broadband Service.<br/><br/>');
    //tempbody=tempbody.replace('please contact us on 0800 800 152','please contact us on 0800 800 152.<br/><br/>');
    //tempbody=tempbody.replace('We’re here to listen.','We’re here to listen.<br/><br/>');
    //tempbody=tempbody.replace('BT for your Broadband service in the near future.','BT for your Broadband service in the near future.<br/><br/>');
    //tempbody=tempbody.replace('We’re sorry that you’re leaving us.','We’re sorry that you’re leaving us.<br/><br/>');
    
    if(cont.Mac_code__c!=null)
    tempbody=tempbody.replace('{!Contact.Mac_code__c}',cont.Mac_code__c);
    else
    tempbody=tempbody.replace('{!Contact.Mac_code__c}','');
    
    //tempbody=tempbody.replace('Yours Sincerely','Yours Sincerely<br/><br/>');
    tempbody=tempbody.replace('{!User.Name}',uName);
    
          
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       
    mail.setPlainTextBody(tempbody);
    mail.setSubject(EmailTemplateSubject);
    
    //mail.setTemplateId(EmailTemplateId);
    //mail.setTargetObjectId(cont.Id);
    //mail.setSaveAsActivity(False);
    mail.setToAddresses(new String[]{cont.Email});
    mail.setBccAddresses(new String[]{'venkat.maddipati@bt.com',email});
    mail.setOrgWideEmailAddressId(fromaddr);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    msg='Email sent successfully';
    
    }
    catch(Exception e){
    ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a valid email address.'));
    //msg='Error';
    //return msg;
    }
    
  }
  public string getcallnew(){
  
  return msg;
  }
  
  public String getactionBody(){
  
    EmailTemplate EmailTemplateBody= [Select body From EmailTemplate where developername =:'MAC_eMail'];
    String tempbody=EmailTemplateBody.body;
    
    //tempbody=tempbody.replace('{!Contact.LastName},','{!Contact.LastName}, <br/><br/>');
    tempbody=tempbody.replace('{!Contact.FirstName} {!Contact.LastName},',cont.Name+',<br/><br/>');
    tempbody=tempbody.replace('We’re sorry that you’re leaving us.','We’re sorry that you’re leaving us.<br/>');
    tempbody=tempbody.replace('Service Provider to migrate your Broadband Service.','Service Provider to migrate your Broadband Service.<br/><br/>');
    tempbody=tempbody.replace('please contact us on 0800 800 152','please contact us on 0800 800 152.<br/><br/>');
    tempbody=tempbody.replace('We’re here to listen.','We’re here to listen.<br/><br/>');
    tempbody=tempbody.replace('BT for your Broadband service in the near future.','BT for your Broadband service in the near future.<br/><br/>');
    tempbody=tempbody.replace('We’re sorry that you’re leaving us.','We’re sorry that you’re leaving us.<br/><br/>');
    
    if(cont.Mac_code__c!=null)
    tempbody=tempbody.replace('{!Contact.Mac_code__c}',cont.Mac_code__c+'<br/><br/>');
    else
    tempbody=tempbody.replace('{!Contact.Mac_code__c}','<br/><br/>');
    
    tempbody=tempbody.replace('Yours Sincerely','Yours Sincerely<br/><br/>');
    tempbody=tempbody.replace('{!User.Name}',uName+'<br/><br/>');
     
    return  tempbody;
    
  }
  
  }