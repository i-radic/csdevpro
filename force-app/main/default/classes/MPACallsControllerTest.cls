/*******************************************************************************************************************
* @description Test class for CS_BTnet_QuickCreate page and MPACallsController class
*				this page is used inside the BT Net Product Configurator (CloudSense CPQ)
*
* @creation
* @category  CloudSense - BT Net
* @author
* @copyright BT
* @license   BT
* @link      https://www.bt.com/
*******************************************************************************************************************/
@isTest
public class MPACallsControllerTest {
        
    // test disabled because we only support "1000 Mbps Ethernet" Access Bearer for now
    @isTest
    public static void TestMPACallsController() {
        
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    	Account testAcc = CS_TestDataFactory.generateAccount(true, 'Test');
        Opportunity o = CS_TestDataFactory.generateOpportunity(true, 'Test', testAcc);
        cscfga__Product_Basket__c pb =  CS_TestDataFactory.generateProductBasket(true, 'BT Net', o);
        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinitionResign(true, 'BT Mobile', true);
        cscfga__Product_Configuration__c pc1 = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', pb);
		pc1.cscfga__Product_Definition__c = prodDef.Id;
		INSERT pc1;
        
        VordelAdapterService__c vordelAdptrEndpoint =new  VordelAdapterService__c();
        vordelAdptrEndpoint.Name ='NADAddress';
        vordelAdptrEndpoint.Vordel_Endpoint__c ='https://www.wse-rt-robt.bt.co.uk:53080/btr/ManagePlaceProvider';
        insert vordelAdptrEndpoint;
        VordelAdapterService__c vordelAdptrEndpoint1 =new  VordelAdapterService__c();
        vordelAdptrEndpoint1.Name ='MPA_url';
        vordelAdptrEndpoint1.Vordel_Endpoint__c ='https://www.wse-rt-robt.bt.co.uk:53080/btr/Simplify/ManagePlaceAvailability';
        insert vordelAdptrEndpoint1;
        
        
        BT_Net__c btNetPrimaryObject =new BT_Net__c();
        
        BT_Net__c btnet = new BT_Net__c();
        btnet.Service_Variant__c = 'Standard';
        btnet.Access_Bearer__c = '1000 Mbps Ethernet';
        btnet.Post_Code__c = 'WA15 7SU';
        btnet.Identifier_Node_Value__c ='Excg12345-2123456-30';
        insert btnet;
        
        BT_Net__c btnet1 = new BT_Net__c();
        btnet1.Service_Variant__c = 'Standard';
        btnet1.Access_Bearer__c = '1000 Mbps Ethernet';
        btnet1.Post_Code__c = 'WA15 7SU';
        insert btnet1;
        
        /*BT_Net__c btnet2 = new BT_Net__c();
        btnet2.Service_Variant__c = 'Failover';
        btnet2.Access_Bearer__c = '1000 Mbps Ethernet';
        btnet2.Post_Code__c = 'WA15 7SU';
        insert btnet2;*/

        CS_Nodes__c node= new CS_Nodes__c();
        Node.Name = 'test';
        insert Node;
        
        SalesforceCRMService.PAInstance PaInst = new SalesforceCRMService.PAInstance();
        List<SalesforceCRMService.PAInstance> PaInstlist = new List<SalesforceCRMService.PAInstance>();
        //PaInstlist.add(PaInst);
        
        PaInst.ProductName = 'FTTC';
        PaInst.OHPExchangeName ='TestExchange';
        PaInst.OHPExchangeCode= '131TV';
        PaInst.AvailabilityFlag ='true';
        PaInst.EthernetUpstream= '100';
        PaInst.EthernetDownstream = '1';
        PaInst.EtherwayUpstream='500Mbit/s';
        PaInst.EtherwayDownstream ='1';
        PaInst.BandwidthPerMPFPair ='12';
        PaInst.FTTPAvailable= 'Yes';    
        PaInst.UpstreamSpeed='120';
        PaInst.DownstreamSpeed='12';
        //ecc
        PaInst.IndicativeECCOutSideThreshold ='10';
        PaInst.IndicativeECCTariff='12';
        PaInst.IndicativeOrderCategory='testCat';
        PaInst.UpperIndicativeECC ='132';
        PaInst.LowerIndicativeECC= '12';        
        PaInstlist.add(PaInst);
        
        SalesforceCRMService.PAInstance PaInst1 = new SalesforceCRMService.PAInstance();
        
        PaInst1.ProductName = 'FTTP';
        PaInst1.OHPExchangeName ='TestExchange1';
        PaInst1.OHPExchangeCode= '131TV';
        PaInst1.AvailabilityFlag ='true';
        PaInst1.EthernetUpstream= '100';
        PaInst1.EthernetDownstream = '1';
        PaInst1.EtherwayUpstream='500Mbit/s';
        PaInst1.EtherwayDownstream ='1';
        PaInst1.BandwidthPerMPFPair ='12';
        PaInst1.FTTPAvailable= 'Yes';    
        PaInst1.FTTPExistingONTAvailable = 'Yes';
        PaInst1.FTTPCPTransferOrWLTOAvailable = 'Yes';
        PaInst1.UpstreamSpeed='120';
        PaInst1.DownstreamSpeed='12';
        PaInst1.FTTPExistingONTAvailableCOMB ='green';
        PaInst1.L2Sid= 'BAABBP';
        PaInst1.FTTPCPTransferOrWLTOAvailable='Y';
        PaInst1.FTTPExistingONTAvailable='N';
        PaInst1.FTTPNewONTAvailable='p';
        //ecc
        PaInst1.IndicativeECCOutSideThreshold ='10';
        PaInst1.IndicativeECCTariff='12';
        PaInst1.IndicativeOrderCategory='testCat';
        PaInst1.UpperIndicativeECC ='132';
        PaInst1.LowerIndicativeECC= '12';
        PaInstlist.add(PaInst1);
        
        SalesforceCRMService.PAInstance PaInst2 = new SalesforceCRMService.PAInstance();
        
        PaInst2.ProductName = 'EOC';
        PaInst2.OHPExchangeName ='TestExchange1';
        PaInst2.OHPExchangeCode= '131TV';
        PaInst2.AvailabilityFlag ='true';
        PaInst2.EthernetUpstream= '100';
        PaInst2.EthernetDownstream = '1';
        PaInst2.EtherwayUpstream='500Mbit/s';
        PaInst2.EtherwayDownstream ='1';
        PaInst2.BandwidthPerMPFPair ='12';
        PaInst2.FTTPAvailable= 'Yes';    
        PaInst2.FTTPExistingONTAvailable = 'Yes';
        PaInst2.FTTPCPTransferOrWLTOAvailable = 'Yes';
        PaInst2.UpstreamSpeed='120';
        PaInst2.DownstreamSpeed='12';
        PaInst2.FTTPExistingONTAvailableCOMB ='green';
        PaInst2.L2Sid= 'BAABBP';
        //ecc
        PaInst2.IndicativeECCOutSideThreshold ='10';
        PaInst2.IndicativeECCTariff='12';
        PaInst2.IndicativeOrderCategory='testCat';
        PaInst2.UpperIndicativeECC ='132';
        PaInst2.LowerIndicativeECC= '12';
        PaInstlist.add(PaInst2);
        
        SalesforceCRMService.PAInstance PaInst3 = new SalesforceCRMService.PAInstance();
        PaInst3.ProductName = 'SOGEA';
        PaInstlist.add(PaInst3);
        
        SalesforceCRMService.CRMAddress address = new SalesforceCRMService.CRMAddress();
        address.IdentifierId = 'test';
        address.IdentifierName = 'test';
        address.IdentifierValue = 'Test12';
        address.Country = 'test';
        address.County = 'test';
        address.Name = 'test';
        address.POBox = '242';
        address.BuildingNumber = '23';
        address.Street = 'test';
        address.Locality = 'test';
        address.DoubleDependentLocality = '';
        address.PostCode = 'WA15 7SU';
        address.Town = '';
        address.SubBuilding = '';
        address.ExchangeGroupCode = '';
        address.MainLinkDistance = '30';
        address.PAInstance = PaInstlist;
        
        List<SalesforceCRMService.CRMAddress> addressList=new List<SalesforceCRMService.CRMAddress>();
        addressList.add(address);
        SalesforceCRMService.CRMArrayOfAddress CRMarray = new SalesforceCRMService.CRMArrayOfAddress();
        CRMarray.Address=addressList;
        //SalesforceCRMService.CRMAddress address1 = new SalesforceCRMService.CRMAddress();
        //address1 = Test_SalesforceCRMService.CreateAddressForTest();
        
        Test.startTest();
        
        ApexPages.StandardController sc = new ApexPages.StandardController(btnet);
        MPACallsController testBTNet = new MPACallsController(sc);
        MPACallsController mpscontroll = new MPACallsController(btnet);
        PageReference pageRef = Page.CS_BTnet_QuickCreate;
        pageRef.getParameters().put('OpportunityId', String.valueOf(o.Id));
        pageRef.getParameters().put('BasketId', String.valueOf(pb.Id));
        pageRef.getParameters().put('ProductId', String.valueOf(pc1.Id));
        pageRef.getParameters().put('Id', btnet.Id);
        pageRef.getParameters().put('cs_product', 'btnet');
        pageRef.getParameters().put('isSecondary', 'true');
        pageRef.getParameters().put('BTnetPrimaryId', btnet.Id);
        //pageRef.getParameters().put('BTNetSecondaryId', btnet2.Id);
        pageRef.getParameters().put('selectedExchange', 'Test');
        Test.setCurrentPage(pageRef);
        
        List<SelectOption> serviceVarianteOptions = testBTNet.getServiceVariantPickListValues();
        
        testBTNet.btNetObject.Service_Variant__c ='Back Up';
        
        List<SelectOption> accessBearerOptions = testBTNet.getAccessBearerPickListValues();
        
        testBTNet.btNetObject.Access_Bearer__c='1000 Mbps Ethernet';
        testBTNet.btNetObject.Post_Code__c = 'WA15 7SU';
        testBTNet.BTnetId = btnet.id;
        testBTNet.ServVar ='test12';
        testBTNet.isshow = false;
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorBtnet()); 
        // testBTNet.crmAddressListWrapper.responseStatus = 'GREEN';
        testBTNet.crmAddressListWrapper = SalesforceCRMService.GetORNADAddress('WA15 7SU',true);
        //MPACallsController.AddressDataWrapper selectedAddressObj1 = new MPACallsController.AddressDataWrapper(address);
        // testBTNet.selectedAddressObj = selectedAddressObj1;
        
        testBTNet.crmAddressListMPA2Wrapper = null;
        testBTnet.crmAddressListMPA1Wrapper = null;
        testBTnet.crmAddressListWrapperGetECC = new SalesforceCRMService.CRMAddressList();
        
        testBTnet.inputPostCode = 'WA15 7SU';
        // testBTnet.nadAddress = accessBearerOptions;
        testBTnet.selectedExchangeNode =null;
        testBtnet.selectedECC = null;
        testBTnet.mpaRresponseDataWrapperList = null;
        
        testBTnet.mpaRresponseDataWrapperListECC = null;
        // testBTnet.btNetSecondaryObject = btnet2;
        
        //  Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
        SalesforceCRMService.CRMAddressList crmaddresslist=new SalesforceCRMService.CRMAddressList();
        crmaddresslist.responseStatus ='GREEN';
        CRMAddressList.Addresses =  CRMarray; 
        testBTNet.crmAddressListWrapper = crmaddresslist;
        
        testBTNet.getAddressDeatils();
        testBTNet.getMap2DetailsForEXpress();
        testBTNet.getMap1DetailsForEthernet();
        System.debug('testBTNet.crmAddressListWrapper>>'+testBTNet.crmAddressListWrapper);
        System.debug('crmaddresslist>>'+crmaddresslist);
        
        testBTNet.isSecondary = true;
        testBTNet.BTnetPrimaryId = btnet.id;
        testBTnet.crmAddressListWrapperGetECC=crmaddresslist;
        testBTNet.getNodeDetailsMPA2(crmaddresslist);
        testBTNet.getNodeDetailsMPA1(crmaddresslist);        
        
        testBTNet.getAllNodeDataMap();
        testBTNet.updateSelectedAddress();
        //testBTNet.updateNodeDetailsToCRF();
        
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(btnet1);
        MPACallsController testBTNet1 = new MPACallsController(sc1);
        MPACallsController.AddressDataWrapper selectedAddressObj = new MPACallsController.AddressDataWrapper(address);
        testBTNet1.selectedAddressObj = selectedAddressObj;
        
        MPACallsController.MPARresponseDataWrapper MPACallsCont= new MPACallsController.MPARresponseDataWrapper(true,'test','TestCode','TestNode','10','test','test','100MBPS','50MBPS','test2132','Test132','10','10','Tst','1000','10','test','test','Test');
        MPACallsCont.MainLinkDistance = '20.20';   
        MPACallsCont.ExchangeName = 'Excg12345';
        MPACallsCont.ExchangeCode = '2123456';
        MPACallsCont.RadialDistance = '30';
        testBTnet.selectedExchangeNode = MPACallsCont;
        
        MPACallsController.MPARresponseDataWrapper MPACallsCont1=new MPACallsController.MPARresponseDataWrapper();
        MPACallsCont1.MainLinkDistance = '10.12';   
        MPACallsCont1.ExchangeName = 'Excg123';
        MPACallsCont1.ExchangeCode = '123456';
        MPACallsCont1.RadialDistance = '20';
        testBTnet.mpaRresponseDataWrapperList = new List <MPACallsController.MPARresponseDataWrapper>{MPACallsCont,MPACallsCont1};
            
            testBTNet.updateSelectedNodeMPA1();
        
        integer mapval= MPACallsCont1.compareTo(MPACallsCont);
        MPACallsCont1.MainLinkDistance = '15';            
        integer mapval1= MPACallsCont1.compareTo(MPACallsCont);
        MPACallsCont1.MainLinkDistance = '5';            
        integer mapval2= MPACallsCont1.compareTo(MPACallsCont);
        PageReference pageRef1 = Page.CS_BTnet_QuickCreate;
        pageRef1.getParameters().put('OpportunityId', String.valueOf(o.Id));
        pageRef1.getParameters().put('BasketId', String.valueOf(pb.Id));
        pageRef1.getParameters().put('ProductId', String.valueOf(pc1.Id));
        pageRef1.getParameters().put('Id', btnet.Id);
        pageRef1.getParameters().put('Id', String.valueOf(btnet.Id));
        pageRef1.getParameters().put('isSecondary', 'true');
        
        Test.setCurrentPage(pageRef1);
        
        List<SelectOption> serviceVarianteOptions1 = testBTNet1.getServiceVariantPickListValues();
        
        testBTNet1.btNetObject.Service_Variant__c ='Standard';
        
        List<SelectOption> accessBearerOptions1 = testBTNet1.getAccessBearerPickListValues();
        
        testBTNet1.btNetObject.Access_Bearer__c='1000 Mbps Ethernet';
        testBTNet1.btNetObject.Post_Code__c = 'WA15 7SU';
        testBTNet1.isSecondary = true;
        Test_Factory.SetProperty('IsTest', 'yes');
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorBtnet()); 
        testBTNet1.crmAddressListWrapper = SalesforceCRMService.GetORNADAddress('WA15 7SU',true);
        testBTNet1.getAddressDeatils();
        testBTNet1.getMap2DetailsForEXpress();
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator1()); 
        //  testBTNet1.getMap1DetailsForEthernet();
        // testBTNet.getNodeDetailsMPA2(testBTNet.crmAddressListWrapper);
        // testBTNet.updateSelectedNodeMPA2();
        testBTNet1.getAllNodeDataMap();
        testBTNet1.updateSelectedAddress();
        //testBTNet1.updateSelectedECC();
        //testBTNet1.updateServiceVariant();
        testBTNet1.checkAccessNodeDistance(MPACallsCont);
        testBTNet1.saveAndReturnToBasket();
        testBTNet1.BTnetPrimaryId = btnet1.id;
        //btNetPrimaryObject = [select id,Service_Variant__c,Access_Bearer__c, Post_Code__c, Identifier_Value__c, Identifier_Node_Value__c from BT_Net__c where id = :btnet1.id limit 1];
        //btNetPrimaryObject.Service_Variant__c='Loadbalanced';
        testBTNet.updateSecondaryNode();
        testBTNet1.getMap1DetailsForEthernet();
        testBTnet1.mpaRresponseDataWrapperList = new List <MPACallsController.MPARresponseDataWrapper>{MPACallsCont,MPACallsCont1};
        //testBTNet1.createOrUpdateSecoundraNode(btnet1,btnet2.id);
        Test.stopTest();
    }

    @isTest
    public static void TestMPACallsController1() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    	Account testAcc = CS_TestDataFactory.generateAccount(true, 'Test');
        Opportunity o = CS_TestDataFactory.generateOpportunity(true, 'Test', testAcc);
        cscfga__Product_Basket__c pb =  CS_TestDataFactory.generateProductBasket(true, 'BT Net', o);
        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinitionResign(true, 'BT Mobile', true);
        cscfga__Product_Configuration__c pc1 = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', pb);
		pc1.cscfga__Product_Definition__c = prodDef.Id;
		INSERT pc1;
        
        BT_Net__c btnet = new BT_Net__c();
        btnet.Service_Variant__c = 'Standard';
        btnet.Access_Bearer__c = '1000 Mbps Ethernet';
        btnet.Post_Code__c = 'SK9 7SD';
        btnet.Identifier_Node_Value__c ='test';
        insert btnet;
        BT_Net__c btnet1 = new BT_Net__c();
        btnet1.Service_Variant__c = 'Standard';
        btnet1.Access_Bearer__c = '1000 Mbps Ethernet';
        btnet1.Post_Code__c = 'SK9 7SD';
        btnet1.Identifier_Node_Value__c ='test';
        insert btnet1;
        SalesforceCRMService.PAInstance PaInst = new SalesforceCRMService.PAInstance();
        List<SalesforceCRMService.PAInstance> PaInstlist = new List<SalesforceCRMService.PAInstance>();
        PaInstlist.add(PaInst);
        
        PaInst.ProductName = 'TestProd';
        PaInst.OHPExchangeName ='TestExchange';
        PaInst.OHPExchangeCode= '131TV';
        PaInst.AvailabilityFlag ='true';
        PaInst.EthernetUpstream= '100';
        PaInst.EthernetDownstream = '1';
        PaInst.EtherwayUpstream='500Mbit/s';
        PaInst.EtherwayDownstream ='1';
        PaInst.BandwidthPerMPFPair ='12';
        PaInst.FTTPAvailable= 'Yes';    
        PaInst.UpstreamSpeed='120';
        PaInst.DownstreamSpeed='12';
        //ecc
        PaInst.IndicativeECCOutSideThreshold ='10';
        PaInst.IndicativeECCTariff='12';
        PaInst.IndicativeOrderCategory='testCat';
        PaInst.UpperIndicativeECC ='132';
        PaInst.LowerIndicativeECC= '12';
        
        SalesforceCRMService.CRMAddress address = new SalesforceCRMService.CRMAddress();
        address.IdentifierId = 'test';
        address.IdentifierName = 'test';
        address.IdentifierValue = 'Test12';
        address.Country = 'test';
        address.County = 'test';
        address.Name = 'test';
        address.POBox = '242';
        address.BuildingNumber = '23';
        address.Street = 'test';
        address.Locality = 'test';
        address.DoubleDependentLocality = '';
        address.PostCode = 'WA15 7SU';
        address.Town = '';
        address.SubBuilding = '';
        address.ExchangeGroupCode = '';
        address.PAInstance = PaInstlist;
        
        List<SalesforceCRMService.CRMAddress> addressList=new List<SalesforceCRMService.CRMAddress>();
        addressList.add(address);
        SalesforceCRMService.CRMArrayOfAddress CRMarray = new SalesforceCRMService.CRMArrayOfAddress();
        CRMarray.Address=addressList;
        
        PageReference pageRef = Page.CS_BTnet_QuickCreate;
        pageRef.getParameters().put('OpportunityId', String.valueOf(o.Id));
        pageRef.getParameters().put('BasketId', String.valueOf(pb.Id));
        pageRef.getParameters().put('ProductId', String.valueOf(pc1.Id));
        pageRef.getParameters().put('isSecondary', 'true');
        pageRef.getParameters().put('BTnetPrimaryId', btnet.Id);
        pageRef.getParameters().put('BTNetSecondaryId', btnet1.Id);
        pageRef.getParameters().put('Id', null);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(btnet);
        MPACallsController testBTNet = new MPACallsController(sc);
        Test.setCurrentPage(pageRef);
        MPACallsController.AddressDataWrapper selectedAddressObj = new MPACallsController.AddressDataWrapper(address);
        testBTNet.selectedAddressObj = selectedAddressObj;
        //testBTNet.updateSelectedNodeMPA1andGetECC();
        try{
            SalesforceCRMService.CRMAddressList crmaddresslist=new SalesforceCRMService.CRMAddressList();
            crmaddresslist.responseStatus ='GREEN';
            CRMAddressList.Addresses =  CRMarray; 
            
            testBTNet.disableSaveButton();
            testBTNet.closeInfoBox();
            testBTNet.updateServiceVariant();
            testBTNet.getECCDetails();
            testBTNet.crmAddressListWrapperGetECC = crmaddresslist;
            testBTNet.getECCDetailsSecondryNode('Exch1234');
            testBTNet.updateSelectedECC();
            
            //testBTNet.updateSelectedNodeMPA1andGetECC();
            MPACallsController.updateNodeDetailsToCRF(o.id);
            
            
        }
        Catch(exception e){}
        
    }
    //Extra for vordeladapterservice
    
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        CustomerOptions__c testcallLog = new CustomerOptions__c();
        
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        
        Account a = new Account(
            Name = 'Test'
        );
        insert a;
        testcallLog.Account__c = a.id;
        testcallLog.Telephone_Number__c = '0123456789';
        testcallLog.Account_Number__c = 'ab22222222';
        testcallLog.Call_Originator__c = 'BTLB';
        
        testcallLog.Reason_for_alleged_mis_sale__c = 'Did not order it';
        testcallLog.EIN_of_the_agent_who_placed_the_order__c = 604218245;
        testcallLog.Site__c = 'BTLB';
        testcallLog.Line_Manager_of_above__c = '604218245';
        
        testcallLog.Order_placed_date__c = DateTime.now();
        testcallLog.Free_Notes__c = 'Test Event';
        
        
        insert testcallLog;
        
    }
    
    @isTest
    public static void mojtest2() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    	Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        cscfga__Product_Basket__c basket =  CS_TestDataFactory.generateProductBasket(true, 'test basket', testOpp);
        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinitionResign(true, 'BT Mobile', true);
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);
		prodConfig.cscfga__Product_Definition__c = prodDef.Id;
		prodConfig.Calculations_Product_Group__c = 'Future Mobile';
		INSERT prodConfig;
    }
}