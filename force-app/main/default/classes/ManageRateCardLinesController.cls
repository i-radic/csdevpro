/**
 * Created by Robert on 8/7/2020.
 */

public with sharing class ManageRateCardLinesController {

    public class rateCardLineWrapper {
        public Decimal estimatedCharge { get; set; }
        public Decimal estimatedUsage { get; set; }
        public Decimal estimatedCost { get; set; }
        public Decimal expectedVoice { get; set; }
        public Decimal percentageFM { get; set; }
        public cspmb__Rate_Card_Line__c rateCardLine { get; set; }
        public List<SelectOption> discounts { get; set; }
        public Decimal selectedDiscount { get; set; }
    }
    public class rateCardWrapper {
        public Id id { get; set; }
        public String Name { get; set; }
        public cspmb__Rate_Card__c rateCard { get; set; }
        public List<rateCardLineWrapper> rateCardLines { get; set; }
        public Decimal totalCost { get; set; }
        public Decimal totalCharge { get; set; }
    }

    public List<cspmb__Rate_Card__c> rateCards {get; set;}
    public List<cspmb__Rate_Card_Line__c> rateCardLines {get; set;}
    public Usage_Profile__c usageProfile {get; set;}
    public Map<String,double> usageProfileMap {get; set;}
    public List<rateCardWrapper> containerOnUI {get; set;}
    public List<rateCardLineWrapper> listOnUI {get; set;}
    public Map<String,String> discountCodeMap {get; set;}
    public Boolean existedBefore {get; set;}
    public Id accountId {get; set;}
    public Id basketId {get; set;}
    public String rateCardId {get; set;}
    public class applicationException extends Exception {}

    public ManageRateCardLinesController() {
        accountId = apexpages.currentpage().getparameters().get('accountId');
        String RCstring = apexpages.currentpage().getparameters().get('rateCardId');
        String Bstring = apexpages.currentpage().getparameters().get('basketId');
        if(accountId==null) {
            throw new applicationException('Account Id is missing. Please close the Page and try again');
        }
        discountCodeMap= new Map<String,String>();
        if(RCstring=='') rateCardId=null;
        else rateCardId=Id.valueOf(RCstring);
        if(Bstring=='') basketId=null;
        else basketId=Id.valueOf(Bstring);
        try {
            usageProfile = [
                    select id, Expected_Voice__c, Expected_SMS__c, Account__c,
                            FM_UK_Landlines__c,FM_Receiving_call_in_Zone_A__c,FM_Calls_to_EE__c,FM_Receiving_call_in_Zone_B__c,FM_Calls_to_other_UK_mobile_networks__c,FM_Receiving_call_in_Zone_C__c,
                            FM_IDD_Zone_A__c,FM_IDD_Zone_B__c,FM_MMS__c,FM_IDD_Zone_C__c,FM_Roaming_SMS_Zone_A__c,FM_Non_Geographic_Calls__c,FM_Roaming_SMS_Zone_B__c,FM_SMS_to_EE__c,
                            FM_Roaming_SMS_Zone_C__c,FM_SMS_to_other_network__c,FM_IDD_SMS_Zone_A__c,FM_Voice_calls_to_Zone_A__c,FM_IDD_SMS_Zone_B__c,FM_Voice_calls_to_Zone_B__c,FM_IDD_SMS_Zone_C__c,
                            FM_Voice_calls_to_Zone_C__c,FM_Voice_calls_back_to_UK_from_Zone_A__c,FM_Voice_calls_back_to_UK_from_Zone_B__c,FM_Voice_calls_back_to_UK_from_Zone_C__c,FM_IDD_SMS_to_EU__c,FM_IDD_Voice_to_EU__c
                    from Usage_Profile__c
                    where Account__c = :accountId and Active__c = true
                    limit 1
            ];
        } catch(exception e) {throw new applicationException('Account has no related Usage Profile object. Please add one and try again');}
        System.debug(usageProfile);
        if (usageProfile != null) {
            Map<String, Object> fieldsToValue = usageProfile.getPopulatedFieldsAsMap();
            System.debug('fieldsToValue '+fieldsToValue);
            usageProfileMap = new Map<String, double>();
            for (String fieldName : fieldsToValue.keySet()) {
                if (fieldName.startsWith('FM_')) {
                    System.debug(fieldsToValue.get(fieldName));
                    if(fieldName=='FM_MMS__c') {
                        usageProfileMap.put('MMS Messaging',double.valueOf(fieldsToValue.get(fieldName)));
                    }
                    else {
                        usageProfileMap.put(fieldName.left(fieldName.length() - 3).right(fieldName.length() - 6).replace('_', ' '), double.valueOf(fieldsToValue.get(fieldName)));
                    }
                }
            }
            System.debug(usageProfileMap);
        }

        List<cspmb__Discount_Level__c> discountLevels;
        existedBefore=rateCardId==null ? false : true;
        system.debug('existedBefore '+existedBefore);
        system.debug('rateCardId '+rateCardId);
        if(existedBefore) { //if the rate card ID was passed, search that rate card in particular
            rateCards = [
                select name,id,cspmb__Rate_Card_Code__c,cspmb__Is_Active__c,cspmb__Is_Authorization_Required__c,Product_Category__c,cspmb__Effective_Start_Date__c,
                cspmb__Effective_End_Date__c,Is_Template__c,cspmb__Account__c
                from cspmb__Rate_Card__c where id =:rateCardId and Is_Template__c=false
            ];
            cspmb__Rate_Card__c templateRateCard=[select id from cspmb__Rate_Card__c where Is_Template__c=true limit 1];
            if(rateCards.size()>0) {
                rateCardLines = [
                        select name,id,cspmb__Is_Active__c,cspmb__Rate_Card_Line_Unit__c,Standard_Rate_Value__c,cspmb__rate_value__c,
                        cspmb__Rate_Card__c,Discount_Percentage__c,Standard_Rate_Cost__c,cspmb__rate_card_line_code__c
                        from cspmb__Rate_Card_Line__c where cspmb__Rate_Card__c = :rateCardId order by Name];
                system.debug('rateCardLines size '+rateCardLines.size());
                discountLevels = [select id,name,cspmb__Discount_Values__c,cspmb__Discount_Level_Code__c,Rate_Card_Line__c,cspmb__Discount__c
                                    from cspmb__Discount_Level__c where Rate_Card_Line__r.cspmb__Rate_Card__c =:templateRateCard.Id
                                    order by Rate_Card_Line__r.name,cspmb__Discount__c ];
                system.debug('discountLevels size '+discountLevels.size());
            }
            else existedBefore=false;
        }
        if(!existedBefore) {//if the rate card ID was not passed or was not found, search all rate cards
            rateCards = [
                select name,id,cspmb__Rate_Card_Code__c,cspmb__Is_Active__c,cspmb__Is_Authorization_Required__c,Product_Category__c,cspmb__Effective_Start_Date__c,
                        cspmb__Effective_End_Date__c,Is_Template__c,cspmb__Account__c
                from cspmb__Rate_Card__c
                where Name = 'Custom Caller' and Is_Template__c=true
            ];

            rateCardLines = [ select name,cspmb__Is_Active__c,cspmb__Rate_Card_Line_Unit__c,Standard_Rate_Value__c,
                            cspmb__rate_value__c,cspmb__Rate_Card__c,Discount_Percentage__c,Standard_Rate_Cost__c,cspmb__rate_card_line_code__c,Group__c
                            from cspmb__Rate_Card_Line__c where cspmb__Rate_Card__c = :rateCards[0].Id order by Name];
            discountLevels = [select id,name,cspmb__Discount_Values__c,cspmb__Discount_Level_Code__c,Rate_Card_Line__c,cspmb__Discount__c
                                from cspmb__Discount_Level__c where Rate_Card_Line__r.cspmb__Rate_Card__c =:rateCards[0].Id
                                order by Rate_Card_Line__r.name,cspmb__Discount__c ];

            rateCards[0].Is_Template__c = false;
            rateCards[0].Id = null;
            rateCards[0].cspmb__Account__c=usageProfile.Account__c;

        }

        //at this point we have all rate card lines which can be changed
        listOnUI=new List<rateCardLineWrapper>();
        containerOnUI=new List<rateCardWrapper>();

        rateCardWrapper oneRC = new rateCardWrapper();
        oneRC.Id = rateCards[0].Id;
        oneRC.Name = rateCards[0].Name;
        oneRC.rateCard=rateCards[0];
        oneRC.totalCost=0;

        for(cspmb__Rate_Card_Line__c oneRCL : rateCardLines) {
            rateCardLineWrapper oneWrapper = new rateCardLineWrapper();
            oneWrapper.discounts = new List<SelectOption>();
            oneWrapper.discounts.add(new SelectOption('0','0'));
            for(cspmb__Discount_Level__c oneDisc : discountLevels) {
                discountCodeMap.put(oneDisc.name,oneDisc.cspmb__Discount_Level_Code__c);
                if(oneDisc.name.contains(oneRCL.Name)) {
                    oneWrapper.discounts.add(new SelectOption(String.valueOf(Integer.valueOf(oneDisc.cspmb__Discount__c)),String.valueOf(Integer.valueOf(oneDisc.cspmb__Discount__c))));
                }
            }
            oneWrapper.selectedDiscount=oneRCL.Discount_Percentage__c==null ? 0 : oneRCL.Discount_Percentage__c;
            oneWrapper.percentageFM=usageProfileMap.get(oneRCL.Name.replace('-', ' '))==null ? 0 : usageProfileMap.get(oneRCL.Name.replace('-', ' '));
            oneWrapper.expectedVoice=(usageProfile.Expected_Voice__c == null ? 0 : usageProfile.Expected_Voice__c).setscale(2);
            oneWrapper.estimatedUsage=(oneWrapper.percentageFM*oneWrapper.expectedVoice/100).setscale(2);
            oneWrapper.estimatedCharge=(oneWrapper.estimatedUsage*oneRCL.cspmb__rate_value__c).setscale(2);
            oneWrapper.estimatedCost=(oneWrapper.estimatedUsage*oneRCL.Standard_Rate_Cost__c).setscale(2);
            oneRC.totalCost+=oneWrapper.estimatedCost;
            oneWrapper.rateCardLine=oneRCL;
            listOnUI.add(oneWrapper);
        }

        oneRC.rateCardLines = listOnUI;
        containerOnUI.add(oneRC);
    }
    public PageReference saveChanges() {
        List<cspmb__Rate_Card_Line__c> rateCardLinesToSave = new List<cspmb__Rate_Card_Line__c>();
        containerOnUI[0].totalCharge=0;
        containerOnUI[0].totalCost=0;

        if(existedBefore) { //if the rate card related to account or config existed before
            for(rateCardLineWrapper oneW : listOnUI) {
                containerOnUI[0].totalCharge+=(oneW.estimatedCharge);
                containerOnUI[0].totalCost+=(oneW.estimatedCost);
                rateCardLinesToSave.add(new cspmb__Rate_Card_Line__c(Id=oneW.rateCardLine.Id,
                                                                    cspmb__rate_value__c=oneW.rateCardLine.Standard_Rate_Value__c*(100-oneW.selectedDiscount)/100,
                                                                    Discount_Code__c=discountCodeMap.get('FM-Recurring-Percentage-'+oneW.rateCardLine.Name+'-'+oneW.selectedDiscount),
                                                                    Discount_Percentage__c=oneW.selectedDiscount,
                                                                    Estimated_Charge__c=oneW.estimatedUsage*oneW.rateCardLine.Standard_Rate_Value__c*(100-oneW.selectedDiscount)/100,
                                                                    Estimated_Cost__c=oneW.estimatedCost,
                                                                    cspmb__rate_card_line_code__c=oneW.rateCardLine.cspmb__rate_card_line_code__c));
            }
            update rateCardLinesToSave;
        }
        else { //if this is created just from a template
            insert rateCards[0];
            cscfga__Product_Basket__c oneBasket = [select id, Rate_Card__c from cscfga__Product_Basket__c where Id=:basketId limit 1];
            oneBasket.Rate_Card__c=rateCards[0].Id;
            update oneBasket;

            for(rateCardLineWrapper oneW : listOnUI) {
                containerOnUI[0].totalCharge+=oneW.estimatedCharge;
                containerOnUI[0].totalCost+=oneW.estimatedCost;
                oneW.rateCardLine.cspmb__Rate_Card__c=rateCards[0].Id;
                oneW.rateCardLine.Id=null;
                oneW.rateCardLine.cspmb__rate_value__c=oneW.rateCardLine.Standard_Rate_Value__c*(100-oneW.selectedDiscount)/100;
                oneW.rateCardLine.Discount_Code__c=discountCodeMap.get('FM-Recurring-Percentage-'+oneW.rateCardLine.Name+'-'+oneW.selectedDiscount);
                oneW.rateCardLine.Discount_Percentage__c=oneW.selectedDiscount;
                oneW.rateCardLine.Estimated_Charge__c=oneW.estimatedUsage*oneW.rateCardLine.Standard_Rate_Value__c*(100-oneW.selectedDiscount)/100;
                oneW.rateCardLine.Estimated_Cost__c=oneW.estimatedCost;
                rateCardLinesToSave.add(oneW.rateCardLine);
            }
            insert rateCardLinesToSave;
        }
        system.debug(containerOnUI[0].totalCost);
        return null;
    }
}