global with sharing class CustomButtonSynchronizeWithOpportunity extends csbb.CustomButtonExt {
    public static final String BASKET_STATUS_APPROVED = 'Approved';
    public static String action;
    public static String errorMsg;
    public static Boolean validBasket;
    private static Set<String> approvalFields = new Set<String> {
        'Approval_Level_1__c',
        'Approval_Level_2__c',
        'Approval_Level_3__c',
        'Approval_Level_4__c'    
    };

    public String performAction (String basketId) {
        validBasket = true;
        try {
            return CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketId);
        }
        catch (Exception e) {
            return '{"status":"error","title":"Error","text":"' + String.escapeSingleQuotes(e.getMessage()) + '"}';
        }
    }

    public static String syncWithOpportunity (String basketId) {
        Map<Id, cscfga__Product_Configuration__c> configs = CS_Util.getConfigurationsInBasket(basketId, 'CustomButtonSynchronizeWithOpportunity');
        cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
        CS_ProductConfigurationService.calculateApprovals(configs, basket, 'CustomButtonSynchronizeWithOpportunity', false);
        Boolean smeDeal = false;
        String productCode = '';

        for (cscfga__Product_Configuration__c pc : configs.values()) {          
            if (pc.cscfga__Configuration_Status__c != 'Valid') {
                validBasket = false;
                errorMsg = '{"status":"error","title":"Error","text":"' + Label.BT_NoConfigurationsInBasket + '"}';
                return errorMsg;
            }
            else if(pc.Calculations_Product_Group__c == 'BT Mobile' || pc.Calculations_Product_Group__c == 'BT Mobile Broadband' || pc.Calculations_Product_Group__c == 'EE SME' || pc.Calculations_Product_Group__c == 'BT NET') {
                smeDeal = true;
                if(String.isNotBlank(pc.Approval_Status__c) && !pc.Approval_Status__c.equalsIgnoreCase('Approved') && 
                   (String.isNotBlank(pc.Approval_Level_1__c) || String.isNotBlank(pc.Approval_Level_2__c) || String.isNotBlank(pc.Approval_Level_3__c) || String.isNotBlank(pc.Approval_Level_4__c))) {
                    validBasket = false;
                    Map<Id, User> userMap = new Map<Id, User>([
                        SELECT Id, FirstName, LastName
                        FROM User
                        WHERE Id = :pc.Approval_Level_1__c OR Id = :pc.Approval_Level_2__c OR Id = :pc.Approval_Level_3__c OR Id = :pc.Approval_Level_4__c
                    ]);
                    String approverNames = '';
                    for(String fName : approvalFields) {
                        approverNames += String.isNotBlank(String.valueOf(pc.get(fName))) ? getName(userMap.get(String.valueOf(pc.get(fName)))) + ', ' : '';
                    }
                    approverNames = approverNames.removeEnd(', ');
                    errorMsg = '{"status":"error","title":"Error","text":"' + Label.BT_BasketNeedsApprovalForSync + ' ' + approverNames + '"}';
                    return errorMsg;
                }
            }
            
            if(pc.Product_Definition_Name__c == 'Cloud Phone')
                productCode = pc.Product_Code__c;
        }
        
        basket.Synchronised_with_Opportunity__c = true;
        basket.csbb__Synchronised_with_Opportunity__c = true;
        if(basket.Basket_Sync_Counter__c!=null){
            basket.Basket_Sync_Counter__c =  basket.Basket_Sync_Counter__c+1;
        }else{
            basket.Basket_Sync_Counter__c =1;
        }
        update basket;
        
        SyncFields(basket, smeDeal, productCode);
        syncCRFForBTNet(basket,configs);

        PageReference oppPage = new ApexPages.StandardController(new Opportunity(Id = basket.cscfga__opportunity__c)).view();
        stampRateCardLinesToOpportunity(basketId, basket.cscfga__opportunity__c);

        return '{"status":"ok","redirectURL":"' + oppPage.getUrl() + '","title":"Success","text":"' + Label.BT_BasketSuccessfullySyncedWithOpp + '"}';
    }
    
    public static void syncCRFForBTNet(cscfga__Product_Basket__c basket, Map<Id, cscfga__Product_Configuration__c> configs){
        List<cscfga__Product_Configuration__c> btNetPCList = new List<cscfga__Product_Configuration__c>();
        List<sObject> listToUpsert = new List<sObject>();
        try{
            if(!configs.isEmpty()){
                for(Id pcid : configs.keySet()){
                    cscfga__Product_Configuration__c pc = configs.get(pcid);
                    if(String.valueOf(pc.Product_Definition_Name__c).equalsIgnoreCase('BTNet')){
                        btNetPCList.add(pc);
                    }
                }
            }
            if(!btNetPCList.isEmpty()){
                List<BTNet_Customer_Details__c> btNetCustDtlList = new List<BTNet_Customer_Details__c>();
                System.debug('btNetCustDtlList>>'+btNetCustDtlList);
                for(cscfga__Product_Configuration__c pc  : btNetPCList){
                    boolean addNewRecord = true;
                    List<BTNet_Customer_Details__c> existingCRFList = [select id,CRF_Complete__c from BTNet_Customer_Details__c where Product_Configuration__c =: pc.id];
                    if(!existingCRFList.isEmpty()){
                        if(existingCRFList[0].CRF_Complete__c){
                            addNewRecord = false;
                        }else{
                            delete existingCRFList;
                        }                       
                    }
                    if(addNewRecord){
                        BTNet_Customer_Details__c btNetCustDtl = new BTNet_Customer_Details__c();
                        btNetCustDtl.Opportunity__c = basket.cscfga__opportunity__c;
                        btNetCustDtl.Product_Basket__c = basket.id;
                        btNetCustDtl.Product_Configuration__c = pc.id;
                        btNetCustDtl.Unique_Ref_For_Site__c = pc.id+':'+basket.Name+':'+pc.Name;
                        Id btNetCustDtlrtid= Schema.SObjectType.BTNet_Customer_Details__c.getRecordTypeInfosByName().get('BT Net').getRecordTypeId();
                        if(btNetCustDtlrtid!=null){
                            btNetCustDtl.RecordTypeId = btNetCustDtlrtid;
                        }
                        //getting ecordtype ofr site
                        List<String> btnetIdList = new List<String>();
                        if(pc.BT_Net_Primary__c != null){
                            btnetIdList.add(pc.BT_Net_Primary__c);
                        }
                        if(pc.BT_Net_Secondary__c != null){
                            btnetIdList.add(pc.BT_Net_Secondary__c);
                        }
                        Map<String,String> accessBeareMap = new Map<String,String>();
                        if(!btnetIdList.isEmpty()){
                           for (BT_Net__c btnetObj : [select id,Primary__c,CS_Access_Bearer__r.Name from BT_Net__c where id in :btnetIdList]){
                               if(btnetObj.Primary__c!=null){
                                   accessBeareMap.put('secondary',btnetObj.CS_Access_Bearer__r.Name);
                               }else if(btnetObj.Primary__c ==null){
                                   accessBeareMap.put('primary',btnetObj.CS_Access_Bearer__r.Name);
                               }
                           }
                        }
                       System.debug('accessBeareMap>>>'+accessBeareMap);
                        listToUpsert.add(btNetCustDtl);
                        if(pc.BT_Net_Primary__c != null){
                            Site__c  primarySite = new Site__c();
                            primarySite.Product_Configuration__c = pc.id;
                            primarySite.BT_Net__c = pc.BT_Net_Primary__c;
                            primarySite.isPrimary__c = true;
                            primarySite.Site_Type__c = 'Primary';
                            System.debug('ppp'+accessBeareMap.get('primary') );
                            String recordTypename = accessBeareMap.get('primary');
                            if(String.isNotBlank(accessBeareMap.get('primary')) && (accessBeareMap.get('primary').containsignorecase('FTTP') || accessBeareMap.get('primary').containsignorecase('FTTC')))
                            {
                                recordTypename ='BTnet Express (GEA Access)';
                            }
                            Id rtid= Schema.SObjectType.Site__c.getRecordTypeInfosByName().get(recordTypename).getRecordTypeId();
                            if(rtid!=null){
                                System.debug('rtid'+rtid);
                                primarySite.RecordTypeId = rtid;
                                BTNet_Customer_Details__c   tempCustDlt = new BTNet_Customer_Details__c( Unique_Ref_For_Site__c= pc.id+':'+basket.Name+':'+pc.Name);
                                primarySite.BTNet_Customer_Details__r= tempCustDlt;
                                listToUpsert.add(primarySite);
                            }
                        }
                        if(pc.BT_Net_Secondary__c != null){
                            Site__c  secondarySite = new Site__c();
                            secondarySite.Product_Configuration__c = pc.id;
                            secondarySite.BT_Net__c = pc.BT_Net_Secondary__c;
                            secondarySite.isPrimary__c = false;
                            secondarySite.Site_Type__c = 'Secondary';
                            String recordTypenameSec = accessBeareMap.get('secondary');
                            if(String.isNotBlank(accessBeareMap.get('secondary')) && (accessBeareMap.get('secondary').containsignorecase('FTTP') || accessBeareMap.get('secondary').containsignorecase('FTTC')))
                            {
                                recordTypenameSec ='BTnet Express (GEA Access)';
                            }
                            Id rtid= Schema.SObjectType.Site__c.getRecordTypeInfosByName().get(recordTypenameSec).getRecordTypeId();
                            if(rtid!=null){
                                System.debug('rtid11'+rtid);
                                secondarySite.RecordTypeId = rtid;
                                BTNet_Customer_Details__c   tempCustDlt = new BTNet_Customer_Details__c( Unique_Ref_For_Site__c= pc.id+':'+basket.Name+':'+pc.Name);
                                secondarySite.BTNet_Customer_Details__r= tempCustDlt;
                                listToUpsert.add(secondarySite);
                            }
                        }
                    }
                    
                }
                if(!listToUpsert.isEmpty()){
                    System.debug('listToUpsert>>'+JSON.serializePretty(listToUpsert));
                    insert listToUpsert;
                }
            }
            
        }catch(Exception e){
            System.debug('Error::::'+e.getLineNumber()+' >>> '+e);
        }
        
    }

    // Get a comma separated SObject Field list
    public static String getSobjectFields (String so) {
        String fieldString;
        SObjectType sot = Schema.getGlobalDescribe().get(so);
        List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();
        fieldString = fields[0].getDescribe().LocalName; 
        for (Integer i = 1; i < fields.size(); i++) {
            fieldString += ',' + fields[i].getDescribe().LocalName;
        }
        return fieldString;
    }

    Public static void SyncFields(cscfga__Product_Basket__c productBasket, Boolean smeDeal, String productCode) {
        List<Schema.FieldSetMember> basketFields = SObjectType.cscfga__Product_Basket__c.FieldSets.Opportunity_Sync.getFields();
        Opportunity currOpp = new Opportunity(Id = productBasket.cscfga__Opportunity__c);
        
        for (Schema.FieldSetMember field : basketFields) {
            System.debug('field>>>'+field);
            System.debug('field.getFieldPath()>>>'+field.getFieldPath());
            System.debug('productBasket.get(field.getFieldPath())>>>'+productBasket.get(field.getFieldPath()));
            currOpp.put(field.getFieldPath(), productBasket.get(field.getFieldPath()));
        }
        currOpp.Basket_Name__c = productBasket.Name;
        currOpp.ChequeFlag__c = 'No';
        currOpp.FCC_2__c = false;
        currOpp.Total_Revenue_Guarantee__c = productBasket.Total_Revenue_Guarantee__c;
        currOpp.Competitive_Clause__c = productBasket.Competitive_Clause__c;
        currOpp.Averaging_Co_Terminus__c = productBasket.Averaging_Co_Terminus__c;
        currOpp.Full_Co_Terminus__c = productBasket.Full_Co_Terminus__c;
        currOpp.Unlocking_Fees__c = productBasket.Unlocking_Fees__c;
        currOpp.Total_Revenue_Contract__c = productBasket.Total_Contract_Guarantee__c;
        currOpp.Disconnect_Allowances__c = productBasket.Disconnect_Allowances__c;
        
        currOpp.RollingAirtimeFlag__c = 'No';
        if (productBasket.Rolling_Air_Time__c != null && productBasket.Rolling_Air_Time__c > 0)
            currOpp.RollingAirtimeFlag__c = 'Yes';
        
        currOpp.StagedAirtimeFlag__c = 'No';
        if (productBasket.Staged_Air_Time__c != null && productBasket.Staged_Air_Time__c > 0)
            currOpp.StagedAirtimeFlag__c = 'Yes';
        
        currOpp.TechFundFlag__c = 'No'; 
        if (productBasket.Tech_Fund__c != null && productBasket.Tech_Fund__c > 0)
            currOpp.TechFundFlag__c = 'Yes';

        currOpp.FCC__c = productBasket.FCC__c;
        if (currOpp.FCC__c != null && currOpp.FCC__c > 0)
            currOpp.FCC_2__c = true;

        currOpp.Unconditional_cheque__c = productBasket.Unconditional_cheque__c;
        currOpp.Unconditional_cheque_2__c = 'No';
        currOpp.ChequeFlag__c = 'No';
        if (productBasket.Unconditional_cheque__c != null && productBasket.Unconditional_cheque__c > 0) {
            currOpp.Unconditional_cheque_2__c = 'Yes';
            currOpp.ChequeFlag__c = 'Yes';
        }

        currOpp.Buy_out_Cheque__c = productBasket.Buy_out_cheque__c;
        currOpp.Buy_out_cheque_2__c = 'No';
        if (productBasket.Buy_out_cheque__c != null && productBasket.Buy_out_cheque__c > 0) {
            currOpp.Buy_out_cheque_2__c = 'Yes';
            currOpp.ChequeFlag__c = 'Yes';
        } 
        else if(currOpp.ChequeFlag__c == 'Yes')
            currOpp.ChequeFlag__c = 'No';
        
        currOpp.Payment_Terms__c = '';
        if (productBasket.Payment_Terms__c == '45' || productBasket.Payment_Terms__c == '60')
            currOpp.Payment_Terms__c = productBasket.Payment_Terms__c;

        currOpp.Deal_Approved_in_CloudSense__c = 'No';
        if(productBasket.Approval_Status__c == 'Approved' || smeDeal)
            currOpp.Deal_Approved_in_CloudSense__c = 'Yes';

        currOpp.TRC_Value__c = productBasket.TRC_TRG_Value__c;
        currOpp.TRG_Value__c = productBasket.TRC_TRG_Value__c;
        
        currOpp.Product_Code__c = productCode;
        UPDATE currOpp;
     }

    private static void stampRateCardLinesToOpportunity(Id basketId, Id opportunityId){
        List<cspmb__Rate_Card_Line__c> rateCardLinesToDelete = [
            SELECT  Id
            FROM    cspmb__Rate_Card_Line__c
            WHERE   Opportunity__c = :opportunityId 
        ];

        if(rateCardLinesToDelete != null && !rateCardLinesToDelete.isEmpty())
            DELETE rateCardLinesToDelete;

        List<cscfga__Attribute__c> rateCardLineAttribute = [
            SELECT  Id,
                    cscfga__Value__c
            FROM    cscfga__Attribute__c
            WHERE   cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId  
            AND     Name = 'RateCardLine'
        ];

        if(rateCardLineAttribute != null && !rateCardLineAttribute.isEmpty() && !String.isBlank(rateCardLineAttribute[0].cscfga__Value__c)) {
            String rateCardJson = rateCardLineAttribute[0].cscfga__Value__c;
            WrapperRateCardWithLineItems rateCardWithLineItems = (WrapperRateCardWithLineItems)json.deserialize(
                rateCardJson, 
                WrapperRateCardWithLineItems.class);
            List<cspmb__Rate_Card_Line__c> rateCardLinesToInsert = new List<cspmb__Rate_Card_Line__c>();
            Map<String, cspmb__Rate_Card_Line__c> rateCardLinesByName = new Map<String, cspmb__Rate_Card_Line__c>();
            
            for (cspmb__Rate_Card_Line__c current : rateCardWithLineItems.rateCardLines) {
                rateCardLinesByName.put(current.Name, current);
                if(current.Display_in_Contract__c) {
                    current.Id = null;
                    current.cspmb__rate_card__c = null;
                    current.Opportunity__c = opportunityId;
                    rateCardLinesToInsert.add(current);
                }
            }
            
            for (cspmb__Rate_Card_Line__c current : rateCardWithLineItems.rateCardLines) {
                if(String.isNotBlank(current.Group__c)) {
                    cspmb__Rate_Card_Line__c groupRateCardLine = rateCardLinesByName.get(current.Group__c);
                    if(groupRateCardLine != null) {
                        if(!current.Group__c.contains('Voice Roaming'))
                            current.Quote_Price__c = groupRateCardLine.Quote_Price__c;
                        current.Quote_Price_Percentage_Discount_Value__c = groupRateCardLine.Quote_Price_Percentage_Discount_Value__c;
                    }
                }
            }           

            INSERT rateCardLinesToInsert;
        }
    }
                   
    private static String getName(User usr) {
        return usr.FirstName + ' ' + usr.LastName;        
    }

    public class WrapperRateCardWithLineItems {
        public cspmb__Price_Item_Rate_Card_Association__c rateCard {get; set;}
        public List<cspmb__Rate_Card_Line__c> rateCardLines {get; set;}

        public WrapperRateCardWithLineItems(){
            rateCard = new cspmb__Price_Item_Rate_Card_Association__c();
            rateCardLines = new List<cspmb__Rate_Card_Line__c>();
        } 
    }
}