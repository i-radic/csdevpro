/**
 * Created by PuneetGosain on 20/05/2020.
 */
public class CS_ManageSolutionsControllerExtension {

    public Account acct;
    public List<CS_ManageSolutionsControllerExtension.SolutionListWrapper> solutions {get; set;}
    public Map<Id, csord__Order__c> solutionOrderMap;

    public CS_ManageSolutionsControllerExtension(ApexPages.StandardController sc) {
        this.acct = (Account) sc.getRecord();
    }

    public void getSolutions(){
        solutionOrderMap = new Map<Id, csord__Order__c>();
        Map<Id, Id> solutionOrderIdMap = new Map<Id,Id>();
        solutions = new List<CS_ManageSolutionsControllerExtension.SolutionListWrapper>();

        Map<Id, csord__Solution__c> accountSolutions = new Map<Id, csord__Solution__c>([
                            SELECT
                                    Id,
                                    Name,
                                    CreatedBy.Name,
                                    CreatedDate
                            FROM csord__Solution__c
                            WHERE csord__Account__c = :acct.Id
                            AND csord__Status__c IN ('Completed', 'Solution Activated')]);

        if(accountSolutions == null) {
            return;
        }

        Map<Id, csord__Subscription__c> solutionSubscriptions =
                new Map<Id, csord__Subscription__c>([SELECT
                        cssdm__solution_association__c,
                        csord__Order__c
                FROM csord__Subscription__c
                WHERE cssdm__solution_association__c IN :accountSolutions.keySet()
                ]);

        if (solutionSubscriptions == null) {
            return;
        }

        for (csord__Subscription__c sub : solutionSubscriptions.values()) {
            solutionOrderIdMap.put(sub.cssdm__solution_association__c, sub.csord__Order__c);
        }

        if (solutionOrderIdMap.isEmpty()) {
            return;
        }

        Map<Id, csord__Order__c> solutionOrders = new Map<Id, csord__Order__c>([
                                SELECT
                                    Id,
                                    csord__Status2__c
                                FROM csord__Order__c
                                WHERE Id IN :solutionOrderIdMap.values()
                                ]);

        if (solutionOrders == null) {
            return;
        }

        for (csord__Solution__c sol : accountSolutions.values()) {
            solutionOrderMap.put(sol.Id, solutionOrders.get(solutionOrderIdMap.get(sol.Id)));
        }

        if (solutionOrderMap.isEmpty()) {
            return;
        }

        for (csord__Solution__c sol : accountSolutions.values()) {
            CS_ManageSolutionsControllerExtension.SolutionListWrapper solWrapper =
                    new CS_ManageSolutionsControllerExtension.SolutionListWrapper();

            solWrapper.solutionId = sol.Id;
            solWrapper.solutionName = sol.Name;
            solWrapper.createdDate = String.valueOf(sol.CreatedDate);
            solWrapper.createdBy = sol.CreatedBy.Name;
            solWrapper.orderStatus = solutionOrderMap.get(sol.Id).csord__Status2__c;
            solWrapper.orderId = solutionOrderMap.get(sol.Id).Id;
            solWrapper.selected = false;

            solutions.add(solWrapper);
        }

    }

    public void activateSolution() {

        Id selectedSolutionId;

        for (CS_ManageSolutionsControllerExtension.SolutionListWrapper sol : solutions) {
            if (sol.selected == true) {
                selectedSolutionId = sol.solutionId;
                sol.selected = false;
                break;
            }
        }

        if (selectedSolutionId == null) {
            return;
        }

        csord__Order__c selectedOrder = solutionOrderMap.get(selectedSolutionId);
        getSolutions();
        selectedOrder.csord__Status2__c = 'Order Activated';
        update selectedOrder;

        csord__Solution__c sol = [
                SELECT
                        csord__Status__c
                FROM csord__Solution__c
                WHERE Id = :selectedSolutionId
        ];

        sol.csord__Status__c = 'Solution Activated';

        update sol;

        List<cscfga__Product_Configuration__c> productConfigs = [
                SELECT
                        Activation_Date__c
                FROM  cscfga__Product_Configuration__c
                WHERE cssdm__solution_association__c = :selectedSolutionId
        ];

        if (productConfigs == null) {
            return;
        }

        for (cscfga__Product_Configuration__c config : productConfigs) {
            config.Activation_Date__c = System.today();
        }

        update productConfigs;

        List<csord__Subscription__c> subs = [
            SELECT
                    csord__Status__c
            FROM csord__Subscription__c
            WHERE cssdm__solution_association__c = :selectedSolutionId
        ];

        if (subs == null) {
            return;
        }

        for (csord__Subscription__c sub : subs) {
            sub.csord__Status__c = 'Subscription Activated';
        }

        update subs;

        List<csord__Service__c > services = [
          SELECT
                  csord__Status__c,
                  csord__Activation_Date__c
          FROM csord__Service__c
          WHERE cssdm__solution_association__c = :selectedSolutionId
        ];

        if (services == null) {
            return;
        }

        for (csord__Service__c service : services) {
            service.csord__Status__c = 'Service Activated';
            service.csord__Activation_Date__c = System.today();
        }

        update services;
    }


    public PageReference modifySolution() {

        Id selectedSolId;

        for (CS_ManageSolutionsControllerExtension.SolutionListWrapper sol : solutions) {
            if(sol.selected == true) {
                selectedSolId = sol.solutionId;
                break;
            }
        }

        if (selectedSolId == null) {
            return null;
        }

        PageReference newPage;
        newPage = Page.cssmgnt__SolutionChange;
        newPage.getParameters().put('id', selectedSolId);

        return newPage;
    }

    public PageReference backToAccount() {
        PageReference acctPage = new ApexPages.StandardController(acct).view();
        acctPage.setRedirect(true);
        return acctPage;
    }

    public class SolutionListWrapper {
        public Id solutionId {get; set;}
        public String solutionName {get; set;}
        public String orderStatus {get; set;}
        public Id orderId {get; set;}
        public Boolean selected {get; set;}
        public String createdDate {get; set;}
        public String createdBy {get; set;}
    }
}