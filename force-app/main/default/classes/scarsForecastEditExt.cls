/*
Client      : BT
Author      : Krupakar J
Date        : 16/11/2011 (dd/mm/yyyy)
Description : This class will manipulate scars forecast values for all
all the targeted streams.
*/

public class scarsForecastEditExt {
	
	public Account acc {get;set;}
	public Account accWithScars {get;set;}	
	public List<SCARS__c> acScars {get;set;}
	
	// Decimal Total Target Variable For Forecast
	public Decimal tfP1 {get;set;} public Decimal tfP2 {get;set;} public Decimal tfP3 {get;set;} public Decimal tfP4 {get;set;}
	public Decimal tfP5 {get;set;} public Decimal tfP6 {get;set;} public Decimal tfP7 {get;set;} public Decimal tfP8 {get;set;}
	public Decimal tfP9 {get;set;} public Decimal tfP10 {get;set;} public Decimal tfP11 {get;set;} public Decimal tfP12 {get;set;}
	// Date Variable For Disable Feature
	public Date dP1 {get;set;} public Date dP2 {get;set;} public Date dP3 {get;set;} public Date dP4 {get;set;}
	public Date dP5 {get;set;} public Date dP6 {get;set;} public Date dP7 {get;set;} public Date dP8 {get;set;}
	public Date dP9 {get;set;} public Date dP10 {get;set;} public Date dP11 {get;set;} public Date dP12 {get;set;}
	private Integer FYStartYear {get;set;}
	private Integer FYEndYear {get;set;}
	
	// Boolean Variable For Disable Feature
	public Boolean bP1 {get;set;} public Boolean bP2 {get;set;} public Boolean bP3 {get;set;} public Boolean bP4 {get;set;}
	public Boolean bP5 {get;set;} public Boolean bP6 {get;set;} public Boolean bP7 {get;set;} public Boolean bP8 {get;set;}
	public Boolean bP9 {get;set;} public Boolean bP10 {get;set;} public Boolean bP11 {get;set;} public Boolean bP12 {get;set;}
	// Controller to Initiate and get Battle Plan Record
    public scarsForecastEditExt(ApexPages.StandardController con) {
    	//Call to Initialize Variables
    	initVariables();
    	
    	//Populate Account Record   
    	acc = (Account)con.getRecord();
    	
    	//Populate Account with Related Scars
       accWithScarsRecs(acc.Id);
    }
	
	//Method to Initialize Variables
	public void initVariables() {
    	acc = null;
    	accWithScars = null;
    	acScars = null;
    	// Default the Integer variable For Total Targeted
    	tfP1 = 0; tfP2 = 0;	tfP3 = 0; tfP4 = 0;	tfP5 = 0; tfP6 = 0; tfP7 = 0; tfP8 = 0; tfP9 = 0; tfP10 = 0; tfP11 = 0; tfP12 = 0;    	
    	// Default the date variables with Default Value to Validate against
    	Integer FiscalYearStartMonth = [select FiscalYearStartMonth from Organization where id=:Userinfo.getOrganizationId()].FiscalYearStartMonth;
    	if(date.today().month() < FiscalYearStartMonth) {
    		FYStartYear = Date.today().year()-1;
    		FYEndYear = Date.today().year();
    	}
    	else {
    		FYStartYear = Date.today().year();
    		FYEndYear = Date.today().year()+1;
    	}
    	dP1 = date.newinstance(FYStartYear, 5, 13); dP2 = date.newinstance(FYStartYear, 6, 13);
    	dP3 = date.newinstance(FYStartYear, 7, 13); dP4 = date.newinstance(FYStartYear, 8, 11);
    	dP5 = date.newinstance(FYStartYear, 9, 13); dP6 = date.newinstance(FYStartYear, 10, 13);
    	dP7 = date.newinstance(FYStartYear, 11, 11); dP8 = date.newinstance(FYStartYear, 12, 13);
    	dP9 = date.newinstance(FYEndYear, 1, 13); dP10 = date.newinstance(FYEndYear, 2, 13);
    	dP11 = date.newinstance(FYEndYear, 3, 13); dP12 = date.newinstance(FYEndYear, 4, 16);
    	/*dP1 = date.newinstance(Date.today().year(), 5, 13); dP2 = date.newinstance(Date.today().year(), 6, 13);
    	dP3 = date.newinstance(Date.today().year(), 7, 13); dP4 = date.newinstance(Date.today().year(), 8, 11);
    	dP5 = date.newinstance(Date.today().year(), 9, 13); dP6 = date.newinstance(Date.today().year(), 10, 13);
    	dP7 = date.newinstance(Date.today().year(), 11, 11); dP8 = date.newinstance(Date.today().year(), 12, 13);
    	dP9 = date.newinstance(Date.today().year() + 1, 1, 13); dP10 = date.newinstance(Date.today().year() + 1, 2, 13);
    	dP11 = date.newinstance(Date.today().year() + 1, 3, 13); dP12 = date.newinstance(Date.today().year() + 1, 4, 16);*/
    	// Default the boolean variable to show
    	bP1 = false; bP2 = false; bP3 = false; bP4 = false;	bP5 = false; bP6 = false; bP7 = false; bP8 = false;	bP9 = false; bP10 = false; bP11 = false; bP12 = false;
    	// Update the boolean variable to disable based on date field
    	if(Date.today() >= dP1) bP1 = true; if(Date.today() >= dP2) bP2 = true;
    	if(Date.today() >= dP3) bP3 = true; if(Date.today() >= dP4) bP4 = true;
    	if(Date.today() >= dP5) bP5 = true; if(Date.today() >= dP6) bP6 = true;
    	if(Date.today() >= dP7) bP7 = true; if(Date.today() >= dP8) bP8 = true;
    	if(Date.today() >= dP9) bP9 = true; if(Date.today() >= dP10) bP10 = true;
    	if(Date.today() >= dP11) bP11 = true; if(Date.today() >= dP12) bP12 = true;
    }
	
	//Default Query to Get SCARS for the Account
	public void accWithScarsRecs (Id Id) {
		accWithScars = [	Select a.Name, a.Id, 
							(Select Id, Name, ConnectionReceivedId, ConnectionSentId, Account__c, Last_Year_P10__c, Last_Year_P11__c, Last_Year_P12__c, Last_Year_P1__c, Last_Year_P2__c, Last_Year_P3__c, Last_Year_P4__c, Last_Year_P5__c, Last_Year_P6__c, Last_Year_P7__c, Last_Year_P8__c, Last_Year_P9__c, Last_Year_Q1__c, Last_Year_Q2__c, Last_Year_Q3__c, Last_Year_Q4__c, Last_Year_To_Date__c, Last_Year__c, Revenue_Stream__c, SAC_Code__c, SCARSPeriod__c, SCARS_LineID__c, This_Year_P10__c, This_Year_P11__c, This_Year_P12__c, This_Year_P1__c, This_Year_P2__c, This_Year_P3__c, This_Year_P4__c, This_Year_P5__c, This_Year_P6__c, This_Year_P7__c, This_Year_P8__c, This_Year_P9__c, This_Year_Q1__c, This_Year_Q2__c, This_Year_Q3__c, This_Year_Q4__c, This_Year__c, This_Year_Forecast_P10__c, This_Year_Forecast_P11__c, This_Year_Forecast_P12__c, This_Year_Forecast_P1__c, This_Year_Forecast_P2__c, This_Year_Forecast_P3__c, This_Year_Forecast_P4__c, This_Year_Forecast_P5__c, This_Year_Forecast_P6__c, This_Year_Forecast_P7__c, This_Year_Forecast_P8__c, This_Year_Forecast_P9__c, This_Year_Forecast_Q1__c, This_Year_Forecast_Q2__c, This_Year_Forecast_Q3__c, This_Year_Forecast_Q4__c, This_Year_Forecast__c, Forecast_Variance_P1__c, Forecast_Variance_P2__c, Forecast_Variance_P3__c, Forecast_Variance_P4__c, Forecast_Variance_P5__c, Forecast_Variance_P6__c, Forecast_Variance_P7__c, Forecast_Variance_P8__c, Forecast_Variance_P9__c, Forecast_Variance_P10__c, Forecast_Variance_P11__c, Forecast_Variance_P12__c, Order__c From SCARS__r ORDER BY Order__c ASC) 
							From Account a WHERE a.Id =:Id];
		acScars = accWithScars.SCARS__r;
		
		//Update for Calculation. 
		UpdateValues(acScars);
			
	}
		
	//SCARS List to Populate UI
	public List<SCARS__c> getaccScars() {
    	return acScars;
    }
    
    //SCARS List Populate Updated Values.
    public void UpdateValues(List<SCARS__c> acScars2) {
    	Integer intPosition = 0;
    	for(Integer i=0; i< acScars2.size(); i++) {
			if(acScars2[i].Revenue_Stream__c == 'Total Targeted') {
				intPosition = i;
			}
			else {
				// Calculate Total Targeted
				if(acScars2[i].This_Year_Forecast_P1__c != null) tfP1 = tfP1 + acScars2[i].This_Year_Forecast_P1__c;
				if(acScars2[i].This_Year_Forecast_P2__c != null) tfP2 = tfP2 + acScars2[i].This_Year_Forecast_P2__c;
				if(acScars2[i].This_Year_Forecast_P3__c != null) tfP3 = tfP3 + acScars2[i].This_Year_Forecast_P3__c;
				if(acScars2[i].This_Year_Forecast_P4__c != null) tfP4 = tfP4 + acScars2[i].This_Year_Forecast_P4__c;
				if(acScars2[i].This_Year_Forecast_P5__c != null) tfP5 = tfP5 + acScars2[i].This_Year_Forecast_P5__c;
				if(acScars2[i].This_Year_Forecast_P6__c != null) tfP6 = tfP6 + acScars2[i].This_Year_Forecast_P6__c;
				if(acScars2[i].This_Year_Forecast_P7__c != null) tfP7 = tfP7 + acScars2[i].This_Year_Forecast_P7__c;
				if(acScars2[i].This_Year_Forecast_P8__c != null) tfP8 = tfP8 + acScars2[i].This_Year_Forecast_P8__c;
				if(acScars2[i].This_Year_Forecast_P9__c != null) tfP9 = tfP9 + acScars2[i].This_Year_Forecast_P9__c;
				if(acScars2[i].This_Year_Forecast_P10__c != null) tfP10 = tfP10 + acScars2[i].This_Year_Forecast_P10__c;
				if(acScars2[i].This_Year_Forecast_P11__c != null) tfP11 = tfP11 + acScars2[i].This_Year_Forecast_P11__c;
				if(acScars2[i].This_Year_Forecast_P12__c != null) tfP12 = tfP12 + acScars2[i].This_Year_Forecast_P12__c;
			}			 
		}
		//Update Total Scars Value
		SCARS__c totTargeted =  acScars2.get(intPosition);
		if(tfP1 != 0) totTargeted.This_Year_Forecast_P1__c = tfP1; 
		if(tfP2 != 0) totTargeted.This_Year_Forecast_P2__c = tfP2;
		if(tfP3 != 0) totTargeted.This_Year_Forecast_P3__c = tfP3; 
		if(tfP4 != 0) totTargeted.This_Year_Forecast_P4__c = tfP4;
		if(tfP5 != 0) totTargeted.This_Year_Forecast_P5__c = tfP5; 
		if(tfP6 != 0) totTargeted.This_Year_Forecast_P6__c = tfP6;
		if(tfP7 != 0) totTargeted.This_Year_Forecast_P7__c = tfP7; 
		if(tfP8 != 0) totTargeted.This_Year_Forecast_P8__c = tfP8;
		if(tfP9 != 0) totTargeted.This_Year_Forecast_P9__c = tfP9; 
		if(tfP10 != 0) totTargeted.This_Year_Forecast_P10__c = tfP10;
		if(tfP11 != 0) totTargeted.This_Year_Forecast_P11__c = tfP11; 
		if(tfP12 != 0) totTargeted.This_Year_Forecast_P12__c = tfP12;
		if(intPosition != 0) {
			acScars2.remove(intPosition);
			acScars2.add(0, totTargeted);
		}
    }	
	
	//update Forecast Values and show updated values
	public PageReference save() {
		PageReference page = new PageReference('/apex/scarsForecastEdit?id=' + acc.Id);
		try {
			update acScars;
		}
		catch(DmlException ex){
			ApexPages.addMessages(ex);
		}
		page.setRedirect(true);
		return page;
	}
	
	//update Forecast Values and retrun to Account Page
	public PageReference saveclose() {
		tfP1 = 0; tfP2 = 0;	tfP3 = 0; tfP4 = 0;	tfP5 = 0; tfP6 = 0; tfP7 = 0; tfP8 = 0; tfP9 = 0; tfP10 = 0; tfP11 = 0; tfP12 = 0;
		UpdateValues(acScars);
		PageReference page = new PageReference('/' + acc.Id);
		try {
			update acScars;
		}
		catch(DmlException ex){
			ApexPages.addMessages(ex);
		}
		page.setRedirect(true);
		return page;
	}
	
	//Cancel Forecast Updation
	public PageReference cancel() {
		PageReference page = new PageReference('/' + acc.Id); 
		return page;
	}
}