Global class CRFNameChangeEmailTemplate {

    Webservice Static String SendEmail(String id){
    String Msg = 'Email Sent Successfully.';
    
    Id EmailTemplateId= [Select Id,DeveloperName From EmailTemplate where developername =:'NameChangeCRF'].Id;
    Id UId = UserInfo.getUserId();
    
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    mail.setToAddresses(new String[]{'business-billing@bt.com'});
    mail.setWhatId(id);
    mail.setTemplateId(EmailTemplateId);
    mail.setTargetObjectId(UId);
    mail.setSaveAsActivity(False);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         
    Email_Status__c ES = new Email_Status__c();
    ES.Email_Sent_By__c=UId;
    ES.Related_to_CRF__c=id;
    ES.Email_Sent_Date__c=System.now();
    ES.Name='Delivered';
    insert ES;

    return Msg;    
    
    }
    
    /* //Number Port Email Service
    Webservice Static String SendEmailNumberPort(String id){
    String Msg = 'Email Sent Successfully.';
    String Msg2 = 'You must enter "number port check" in the Vol ref field. Only use this button to trigger a stage 1 number port check. Stage 2 will be triggered by entering a valid Vol ref in the VOL ref field.';
    
    Id EmailTemplateId= [Select Id,DeveloperName From EmailTemplate where developername =:'NumberPortCRF'].Id;
    Id UId = UserInfo.getUserId();
    
    String VolRef = [Select Vol_ref_Mover__c from CRF__c where Id=:id].Vol_ref_Mover__c;
    if(VolRef=='number port check'){    
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();    
    //mail.setToAddresses(new String[]{'number.port.team.btb@bt.com'});
    mail.setToAddresses(new String[]{'manish.thaduri@bt.com'});
    mail.setWhatId(id);
    mail.setTemplateId(EmailTemplateId);
    mail.setTargetObjectId(UId);
    mail.setSaveAsActivity(False);
    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
         
    Email_Status__c ES = new Email_Status__c();
    ES.Email_Sent_By__c=UId;
    ES.Related_to_CRF__c=id;
    ES.Email_Sent_Date__c=System.now();
    ES.Name='Delivered';
    insert ES;

    return Msg;    
    }
    else
    return Msg2;
    
    
    }
    */
    
}