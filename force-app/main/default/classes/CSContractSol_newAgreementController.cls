public class CSContractSol_newAgreementController {

    Public string oppoId {get;set;}
    Public string oppoName {get;set;}
    private opportunity selectedOpportunity;
    Public string basket {get;set;}
    Public double tenure {get;set;}
    private cscfga__Product_Basket__c synchedBasked;
    Public string basePropositionName {get;set;}
    
    Public boolean isAssociatedLEPopulated{get;set;}
    Public boolean isPrimaryContactPopulated{get;set;}
    Public boolean isCompanySignatoryPopulated{get;set;}
    
    
    Public CSContractSol_newAgreementController(ApexPages.StandardController controller) {
        selectedOpportunity = [select id,Name,Basket_ID__c, Tenure__c, AssociatedLE__c, Primary_Contact__c, Company_Signatory1__c from opportunity where id=:ApexPages.CurrentPage().getparameters().get('id')];
            
        // (Opportunity) ApexPages.CurrentPage().getparameters().get('id');
        oppoID = selectedOpportunity.Id;
        tenure = selectedOpportunity.tenure__c;
        basket = selectedOpportunity.Basket_ID__c;
        oppoName = selectedOpportunity.Name;
        
        if (selectedOpportunity.AssociatedLE__c==null) {
            isAssociatedLEPopulated=false;
        } else {
            isAssociatedLEPopulated=true;
        }
        
        // get the basket that we are interested in, the one written in Basket_ID__c field on Opportunity
        synchedBasked = fetchBasket(basket);
        basePropositionName = synchedBasked.Base_Proposition_Name__c;
        
        //boolean uspjeh = createAgreement();
        
    }
    
    Public cscfga__Product_Basket__c fetchBasket (string basketId) {
        cscfga__Product_Basket__c a = [select id, Base_Proposition_Name__c from cscfga__Product_Basket__c where Id=:basketId];
        return a;
    }
    
    // composes an Agreement name on formula "OpportunityName_EEBA"
    /*
    private string createAgreementName (string oppoName) {
        return oppoName + '_EEBA';
        
    }
    */
    
    Public PageReference createAgreement() {
        
        // code section to determine which Document Template (CLM record we need)
        // for now, we only use one - "Full EEBA Template", so we need to query the database and fetch it's Id - for code to be usable after deployment to another org
        
        string templateId = [select Id from csclm__Document_Template__c where Name ='Full EEBA Template'].Id;
        
        csclm__Agreement__c newAgreement = new csclm__Agreement__c();
        newAgreement.Name = oppoName + '_EEBA';
        newAgreement.csclm__Opportunity__c = oppoID;
        newAgreement.csclm__Document_Template__c = templateId;
        
        if(!isAssociatedLEPopulated) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Associated Legal Entity field on Opportunity is not populated!'));
            return null;
        } else {
            insert newAgreement;
            return new PageReference ('/'+newAgreement.Id);
        }
        
        //string newlyCreatedAgreementId = [select Id from csclm__Agreement__c where Name ='mojpokusniAgreement'].Id;
        
        //return new PageReference ('/'+newAgreement.Id);
        
        
        
    }
    
}