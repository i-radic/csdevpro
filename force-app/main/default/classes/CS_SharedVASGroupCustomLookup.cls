global class CS_SharedVASGroupCustomLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["Service Plan"]'; 
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
        
        Map<String, cspmb__Price_Item_Add_On_Price_Item_Association__c> uniqueGroupsMap = new Map<String, cspmb__Price_Item_Add_On_Price_Item_Association__c>();
        System.debug('Service Plan Id : ' + searchFields.get('Service Plan'));
        for (cspmb__Price_Item_Add_On_Price_Item_Association__c commPrdAddOnAssoc : [SELECT Id, Name, cspmb__Price_Item__r.Name, cspmb__Add_On_Price_Item__c, cspmb__add_on_price_item__r.cspmb__Add_On_Price_Item_Code__c, cspmb__add_on_price_item__r.cspmb__add_on_price_item_description__c, Add_On_Name__c, cspmb__add_on_price_item__r.name, Module__c, cspmb__Group__c, cspmb__add_on_price_item__r.cspmb__one_off_cost__c, cspmb__add_on_price_item__r.cspmb__one_off_charge__c, cspmb__add_on_price_item__r.cspmb__recurring_charge__c, cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c,  cspmb__Min__c, cspmb__add_on_price_item__r.cspmb__product_definition_name__c, cspmb__Max__c, cspmb__Price_Item__c, cspmb__add_on_price_item__r.id, cspmb__Price_Item__r.id, cspmb__add_on_price_item__r.GM_Recurring_Cost__c, cspmb__add_on_price_item__r.GM_Upfront_Cost__c, cspmb__add_on_price_item__r.cspmb__Is_Active__c, cspmb__Add_On_Price_Item__r.Grouping__c, Add_On_Type__c, cspmb__add_on_price_item__r.manufacturer__c, cspmb__Add_On_Price_Item__r.cspmb__Billing_Frequency__c, cspmb__Add_On_Price_Item__r.Maximum_Connections__c, cspmb__Add_On_Price_Item__r.Minimum_Connections__c, cspmb__Add_On_Price_Item__r.Contract_Type__c, cspmb__Add_On_Price_Item__r.Data__c, cspmb__Add_On_Price_Item__r.cspmb__Contract_Term__c FROM cspmb__Price_Item_Add_On_Price_Item_Association__c WHERE Module__c = 'Shared VAS' AND cspmb__add_on_price_item__r.cspmb__Is_Active__c = TRUE AND cspmb__Price_Item__c = :searchFields.get('Service Plan') ORDER By cspmb__Group__c]) {
            if (String.isNotEmpty(commPrdAddOnAssoc.cspmb__Group__c) && !uniqueGroupsMap.containsKey(commPrdAddOnAssoc.cspmb__Group__c)) {
                uniqueGroupsMap.put(commPrdAddOnAssoc.cspmb__Group__c, commPrdAddOnAssoc);
            }
        }
        

        return uniqueGroupsMap.values();
    }
}