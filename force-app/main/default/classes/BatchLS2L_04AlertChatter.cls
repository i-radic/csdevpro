global class BatchLS2L_04AlertChatter implements Database.Batchable<sobject>{
/*
###################################################################################################
To calculate the number of leads created by owner by a user and post chatter message or email
account owners from being made inactive automatically.

24/01/12    John McGovern    Intial Build

###################################################################################################
*/

public String query;

global BatchLS2L_04AlertChatter() {

}

global database.querylocator start(Database.BatchableContext BC){
    return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List<sObject> scope){
    //set leads to zero   
    List<FeedItem> NewChat = new List<FeedItem>();   
    for(sObject s : scope) {
        User u = (User)s;
        NewChat.add(new FeedItem(CreatedById ='00520000001C3Fw', ParentId= u.ID, Body = 'You have ' + u.zLeadsOwned__c.format() + ' new #LandscapeLeads created.', LinkURl = '/00Q?fcf=00B200000069Gns', Title = 'My New Landscape Alerts'));         
    }
    insert NewChat;
}

global void finish(Database.BatchableContext BC){  
    //create next batch
    if(!Test.isRunningTest()){  
        BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
        DateTime n = datetime.now().addMinutes(2);
        String cron = '';
    
        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();
    
        b.scheduled_id5__c = System.schedule('LS2L Batch 5', cron, new BatchLS2L_Schedule05());
    
        update b;    
    }                
}

}