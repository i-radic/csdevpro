@istest
public class Test_FilterOptionsCon {
    @istest
    static void FilterOptionTestMethod(){
        User thisUser = [select id from User where id=:userinfo.getUserid()];
        System.runAs( thisUser ){
            
            TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
            settings.Account__c = FALSE;
            settings.Contact__c = FALSE;
            settings.Opportunity__c = FALSE;
            settings.OpportunitylineItem__c = FALSE;
            settings.Task__c = FALSE;
            settings.Event__c = FALSE;
            
            upsert settings TriggerDeactivating__c.Id;
            
            Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
            User u1 = new User(
                alias = 'B2B18', email = 'B2B138cr@bt.com',
                emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                languagelocalekey = 'en_US',
                localesidkey = 'en_US', ProfileId=prof.Id,
                timezonesidkey = 'Europe/London', username = 'B2B28Profile13167cr@testemail.com',
                EIN__c = 'OTHER_def'
            );
            insert u1;
            
            Account A  = Test_Factory.CreateAccount();
            A.name = 'TESTCODE 2';
            A.sac_code__c = 'testSAC';
            A.Sector_code__c = 'CORP';
            A.LOB_Code__c = 'LOB';
            A.OwnerId = u1.Id;
            A.CUG__c='CugTest';
            Database.SaveResult accountResult = Database.insert(A);
            
            Contact c = new Contact();
            c.LastName = 'TestLName';
            c.accountId=accountResult.id;
            Insert c;
            
            Service_Improvement_Plan__c CustomerExpPlanRec = Test_Factory.CreateCustomerExperienceActionPlan(accountResult.Id);
            insert CustomerExpPlanRec; 
            Customer_Experience_Actions__c CustomerExpActionRec = new Customer_Experience_Actions__c(Customer_Experience_Client_Plan__c =CustomerExpPlanRec.id );
            insert CustomerExpActionRec;
            ApexPages.currentPage().getParameters().put('id',CustomerExpPlanRec.id);
            FilterOptionsCon StandardFoptionsCon = new FilterOptionsCon(new ApexPages.StandardController(CustomerExpPlanRec));
            StandardFoptionsCon.RedirectToPDF();
            new FilterOptionsCon.innerClass(c);
            StandardFoptionsCon.GeneratePDF();
            StandardFoptionsCon.SearchTextValue = 'Test';
            StandardFoptionsCon.ShowActiveContacts();
            StandardFoptionsCon.ShowRootCauseArea();
            StandardFoptionsCon.rightSelected = new List<string>();
            StandardFoptionsCon.leftSelected  = new List<string>();
            StandardFoptionsCon.SelectedPickList = '';
            StandardFoptionsCon.getSelect();
            StandardFoptionsCon.getDeselect();
            StandardFoptionsCon.getDeselectedValues();
            StandardFoptionsCon.getSelectedValues();
            
        }
    }
}