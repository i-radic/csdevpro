@IsTest
public class CustomButtonSubmitForApprovalTest  {

	static testMethod void testConfiguraitons(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        No_Triggers__c notriggers =  new No_Triggers__c();
        notriggers.Flag__c = true;
        INSERT notriggers;

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'basketName', opp);

        cscfga__Product_Configuration__c pc1 = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);

        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Sharer');
        pc1.cscfga__Product_Definition__c = pd.Id;
        pc1.cscfga__Product_Family__c = 'BT Mobile Sharer';
        pc1.Approval_Status__c = 'Approval needed';
        pc1.Approval_Level_1__c = UserInfo.getUserId();

        INSERT pc1;

        Test.startTest();

        CustomButtonSubmitForApproval submitForApproval = new CustomButtonSubmitForApproval();
		submitForApproval.performAction(basket.Id);        

        Test.stopTest();
    }

    static testMethod void testConfiguraitons1(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        
        No_Triggers__c notriggers =  new No_Triggers__c();
        notriggers.Flag__c = true;
        INSERT notriggers;

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account acc = CS_TestDataFactory.generateAccount(true, 'test');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'test', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(true, 'basketName', opp);

        cscfga__Product_Configuration__c pc1 = CS_TestDataFactory.generateProductConfiguration(true, 'BT Mobile', basket);

        Test.startTest();

        CustomButtonSubmitForApproval submitForApproval = new CustomButtonSubmitForApproval();
		submitForApproval.performAction(basket.Id);        

        Test.stopTest();
    }
}