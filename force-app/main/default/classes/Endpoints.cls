public class Endpoints{
    
    public static SalesforceServicesCRM.CRMServiceSoap SFCRMGateway (){
        List<BTSalesforceServicesURL__c> urls = BTSalesforceServicesURL__c.getAll().values();
        
        SalesforceServicesCRM.CRMServiceSoap service = new SalesforceServicesCRM.CRMServiceSoap();
        
        system.debug(urls.get(0).Url__c);
        service.endpoint_x = urls.get(0).Url__c;
        service.clientCertName_x = 'SFServicesCRMgateway';
        service.timeout_x = 80000;              
        return service;
    }   
}