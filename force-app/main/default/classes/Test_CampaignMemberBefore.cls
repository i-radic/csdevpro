@isTest
private class Test_CampaignMemberBefore {
    static Campaign c;
    static Contact contact;
    static Account a;
    
    static{
        //Data setup - START
        List<Campaign_Call_Status__c> ccsToInsert = new List<Campaign_Call_Status__c>();
        Campaign_Call_Status__c ccs1 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status1', Responded__c = true, Default__c=true);
        Campaign_Call_Status__c ccs2 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status2', Responded__c = false, Default__c=false);
        ccsToInsert.add(ccs1);
        ccsToInsert.add(ccs2);
        insert ccsToInsert;
  
        Date enddate = date.today() + 1;
        c = new Campaign(Name='TestCampaign', Campaign_Type__c='BTLB', Type='Telemarketing', Status='Planned', 
            Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, isActive=true, Data_Source__c='Self Generated');
        insert c;
        
        a = Test_Factory.CreateAccount();
        a.Sector__c='BT Local Business';
        a.Sub_Sector__c='Cambridge Gold Customers';
        insert a;
        contact = Test_Factory.CreateContact();
        contact.Phone = '01234 567890';
        contact.AccountId = a.Id;
        insert contact;
        //Data setup - END
    }

    static testMethod void checkContactAndAccountLookupsSet() {
        
        CampaignMember cm = new CampaignMember(CampaignId=c.Id, ContactId=contact.Id);
        insert cm;
        
        cm = [select Id, ContactId, CampaignId, Unique_Key__c from CampaignMember where id = :cm.Id];
        //Custom Lookups should have been populated by trigger
        /*System.assertEquals(cm.Contact__c, contact.Id);
        System.assertEquals(cm.Account__c, a.Id);*/
        System.assertEquals(cm.Unique_Key__c, cm.ContactId + '_' + cm.CampaignId);
        
        cm.Status='Business has Ceased';
        update cm;
    }
}