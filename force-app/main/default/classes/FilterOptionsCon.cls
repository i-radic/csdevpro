public class FilterOptionsCon {
    
    public Boolean OpenActions{get;set;}
    public Boolean ClosedActions{get;set;}
    public Boolean Closed3Months{get;set;}
    public Boolean rootcause {get;set;}
    public Boolean customerContact {get;set;}
    public string ActionId{get;set;}    
    public List<InnerClass> ConInnerList{get;set;}
    public List<Customer_Experience_Actions__c> ClosedWithin3Months{get;set;}
    public List<Customer_Experience_Actions__c> RootCauseArea{get;set;}
    public List<Customer_Experience_Actions__c> CustomerContactList{get;set;}
    Public List<SelectOption> options{get;set;}
    public String SelectedPickList{get;set;}
    Public List<String> leftSelected {get;set;}
    Public List<String> rightSelected {get;set;}
    Set<String> leftValues = new Set<String>();
    Set<String> rightValues = new Set<String>();
    public string SearchTextValue{get;set;}
    
    public FilterOptionsCon(ApexPages.StandardController stdController) {
        ConInnerList = new List<InnerClass>();
        ActionId =  ApexPages.currentPage().getParameters().get('id');
        
    }
    public Pagereference RedirectToPDF(){
        system.debug('**********ProfileId'+userinfo.getProfileId());//
        if( userinfo.getProfileId() != '00e20000001MX7zAAG' && userinfo.getProfileId() !='00e2000000163QyAAI' ){
            Pagereference Pg = new Pagereference('/apex/Customer_Experience_Pdf?id='+ActionId);
            pg.setRedirect(true);
            return pg;
        }else {
            return null;
        }            
    }
    public Pagereference GeneratePDF(){
        
        ClosedWithin3Months = new List<Customer_Experience_Actions__c>();
        ClosedWithin3Months = [select CreatedDate,Problem_Statement__c,Customer_Owner__r.Name,Agreed_Success_Criteria__c,
                               Customer_benefit_once_resolved__c,BT_benefit_once_resolved__c,Target_Date__c,
                               Latest_Update__c,RAG_Status__c,Open_Closed__c,BT_Owner_Lookup__r.Name
                               from Customer_Experience_Actions__c Where Close_Date__c >= Last_N_Months:3 AND 
                               Close_Date__c <=Today AND Customer_Experience_Client_Plan__c =: ActionId AND PDF__c ='Yes'];
        
        
        RootCauseArea = new List<Customer_Experience_Actions__c>();
        RootCauseArea = [select CreatedDate,Problem_Statement__c,Customer_Owner__r.Name,Agreed_Success_Criteria__c,
                         Customer_benefit_once_resolved__c,BT_benefit_once_resolved__c,Target_Date__c,
                         Latest_Update__c,RAG_Status__c,Open_Closed__c,BT_Owner_Lookup__r.Name
                         from Customer_Experience_Actions__c Where Root_Cause_Area__c IN:rightValues
                         AND Customer_Experience_Client_Plan__c =: ActionId AND PDF__c ='Yes'];
        
        Set<Id> ContactId = new set<Id>();
        
        if(ConInnerList.size()>0){
            for(Innerclass i:ConInnerList){
                if(i.SelectedContacts == TRUE){
                    ContactId.add(i.Contacts.id);    
                }    
            }
            CustomerContactList = new List<Customer_Experience_Actions__c>();
            CustomerContactList = [select CreatedDate,Problem_Statement__c,Customer_Owner__r.Name,Agreed_Success_Criteria__c,
                                   Customer_benefit_once_resolved__c,BT_benefit_once_resolved__c,Target_Date__c,
                                   Latest_Update__c,RAG_Status__c,Open_Closed__c,BT_Owner_Lookup__r.Name
                                   from Customer_Experience_Actions__c Where Customer_Owner__c  IN:ContactId
                                   AND Customer_Experience_Client_Plan__c =: ActionId AND PDF__c ='Yes'];
            
        }
        Pagereference Pg = new Pagereference('/apex/Customer_Experience_Pdf?id='+ActionId);
        //pg.setRedirect(false);
        return pg;       
    } 
    
    public void ShowActiveContacts(){
        set<Id> ConIDList = new set<Id>();
        List<contact> DConList = new List<contact>();
        system.debug('SearchTextValue****'+SearchTextValue);
        List<Service_Improvement_Plan__c > CustExpPlan = [select Account__c,(select id,Customer_Owner__r.id from Customer_Experience_Actions__r ) 
                                                          from Service_Improvement_Plan__c  
                                                         Where id =:ActionId ];
        for(Service_Improvement_Plan__c  SImprovPlan: CustExpPlan){
            for(Customer_Experience_Actions__c CExpActions:SImprovPlan.Customer_Experience_Actions__r){
            	ConIDList.add(CExpActions.Customer_Owner__r.id);    
            }    
        } 
        if(CustExpPlan.size()>0){
            String SearchFilter ='';
            if(SearchTextValue!=null){
            SearchFilter = 'AND (Email =\'' + String.escapeSingleQuotes(SearchTextValue) + '\' OR Name =\'' + String.escapeSingleQuotes(SearchTextValue) + '\' OR FirstName =\'' + String.escapeSingleQuotes(SearchTextValue) + '\' OR Phone=\'' + String.escapeSingleQuotes(SearchTextValue) + '\' OR Job_title__c =\'' + String.escapeSingleQuotes(SearchTextValue)+'\' OR'+
                					' Job_Function__c =\'' + String.escapeSingleQuotes(SearchTextValue)+'\')';
            }   
            string QueryString = 'select id,Name,Phone,Email,Job_Title__c,Job_Function__c'+
                       ' from Contact where (Status__c = \'' + String.escapeSingleQuotes('Active') + '\' AND AccountId =\'' + String.escapeSingleQuotes(CustExpPlan[0].Account__c) + '\' AND ID IN: ConIDList )'+ SearchFilter;
            system.debug('******QueryString'+QueryString);
            DConList = Database.query(QueryString);
            system.debug('DynamicQuery****'+DConList);
            ConInnerList = new List<InnerClass>();
            /**ConList = [select id,Name,Phone,Email,Job_Title__c,Job_Function__c  
                       from Contact where (Status__c = 'Active' AND 
                       AccountId =:CustExpPlan[0].Account__c )];*/
            for(Contact c : DConList){
                ConInnerList.add(new InnerClass(c));
            }
        }
    }
    public void ShowRootCauseArea(){
        Options = new List<SelectOption>();
        
        Schema.DescribeFieldResult fieldResult =
            Customer_Experience_Actions__c.Root_Cause_Area__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry f : ple)
        {
            //options.add(new SelectOption(f.getLabel(), f.getValue()));
            leftValues.add(f.getValue());            
        } 
        
    }
    public PageReference getSelect(){
        rightSelected.clear();
        for(String s : leftSelected){
            leftValues.remove(s);
            rightValues.add(s);
        }
        return null;
    }
    
    public PageReference getDeselect(){    
        leftSelected.clear();
        for(String s : rightSelected){
            rightValues.remove(s);
            leftValues.add(s);
        }
        return null;
    }
    public List<SelectOption> getDeselectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<String> objList = new List<String>();
        objList.addAll(leftValues);
        objList.sort();
        for(String s : objList){
            options.add(new SelectOption(s,s));
        }
        return options;
    }
    
    public List<SelectOption> getSelectedValues(){
        
        List<SelectOption> options = new List<SelectOption>();
        List<String> objList = new List<String>();
        objList.addAll(rightvalues);
        objList.sort();
        for(String s : objList){
            options.add(new SelectOption(s,s));
        }
        return options;
    }
    
    public class innerClass {
        public Contact Contacts {get;set;}
        public Boolean SelectedContacts{get;set;}
        public innerClass(Contact c){Contacts =c;}
    }
}