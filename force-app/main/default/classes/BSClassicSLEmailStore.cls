global class BSClassicSLEmailStore implements Messaging.InboundEmailHandler {

  global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
    Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
  
 //   String myPlainText= '';
 //   myPlainText = email.plainTextBody;
   
     try {
    
    Attachment[] newAttachment = new Attachment[0];
    
    string RefNo = email.subject.mid(email.subject.indexOf('SFSL-', 0),12);
    
    System.debug('ADJ :RefNo ' + RefNo );  
    list <BS_Classic_Sales_log__c> relatedCase = [select id, Contact__r.Email from BS_Classic_Sales_log__c where name = :RefNo limit 1]; 
 
      newAttachment.add(new Attachment(
           //name = 'To:'+ email.toAddresses + ': '+ email.subject + '.html',
           name = 'To:'+ relatedCase[0].Contact__r.Email + ': '+ email.subject + '.html',
           body = Blob.valueof(email.htmlBody),
           ParentID = relatedCase[0].id));
        insert newAttachment;    
      System.debug('Nehttp://marketplace.eclipse.org/marketplace-client-intro?mpc_install=1336w Task Object: ' + newAttachment );   
    }
       catch (QueryException e) {
       System.debug('Query Issue: ' + e);
   }
   
     result.success = true; 
  
    return result;
  
  
  }
  
}