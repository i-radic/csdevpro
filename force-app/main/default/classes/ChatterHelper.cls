public class ChatterHelper{

    //get all BTLB Group
    public static string getBTLBgroup() {
        string grp = '0F920000000PE37';
        return grp;
    }    
    public static string getLEMServicegroup() {
        //List<CollaborationGroup> grp = [Select Id from CollaborationGroup Where Id = '0F920000000PE37'];
        //return grp[0].Id;
        string grp = '0F920000000PETY';
        return grp;
    }        
    public static string getDeskgroup() {
        string grp = '0F920000000PE3q';
        return grp;
    }    
    /*
    public static string getFieldgroup() {
        List<CollaborationGroup> grp = [Select Id from CollaborationGroup Where Id = 'Chatter Launch - Field'];
        return grp[0].Id;
    }            
*/
    public static string getCSgroup() {
        string grp = '0F920000000PE4j';
        return grp;
    }    

/* informatica has replaced this functionality    
    //add BTLB Group
    @future
    public static void SendBTLB(Set<ID> addBTLB){
    List<CollaborationGroupMember> NewMembers=new List<CollaborationGroupMember>();
    List<EntitySubscription> NewFollowers=new List<EntitySubscription>();
        for (User u: [SELECT Id FROM User Where Id IN :addBTLB]){
            CollaborationGroupMember NewMember = new CollaborationGroupMember(MemberId = u.Id, CollaborationGroupId = getBTLBgroup());
            NewMembers.add(NewMember);
            //add BTLB Following Danny 00520000001U9k7 & Ricky 00520000001U8uY
            EntitySubscription NewFollower1 = new EntitySubscription(SUBSCRIBERID= u.Id, PARENTID = '00520000001U9k7');
            NewFollowers.add(NewFollower1);
            EntitySubscription NewFollower2 = new EntitySubscription(SUBSCRIBERID= u.Id, PARENTID = '00520000001U8uY');
            NewFollowers.add(NewFollower2);
        }          
    upsert NewMembers;
    upsert NewFollowers;
    }
    //add Desk
    @future
    public static void SendLEMServ(Set<ID> addLEMServ){   
    List<CollaborationGroupMember> NewMembers=new List<CollaborationGroupMember>();
        for (User u: [SELECT Id FROM User Where Id IN :addLEMServ]){
            CollaborationGroupMember NewMember = new CollaborationGroupMember(MemberId = u.Id, CollaborationGroupId = getLEMServicegroup());
            NewMembers.add(NewMember);
        }          
    upsert NewMembers;
    }         

    //add Desk
    @future
    public static void SendDesk(Set<ID> addDesk){   
    List<CollaborationGroupMember> NewMembers=new List<CollaborationGroupMember>();
        for (User u: [SELECT Id FROM User Where Id IN :addDesk]){
            CollaborationGroupMember NewMember = new CollaborationGroupMember(MemberId = u.Id, CollaborationGroupId = getDeskgroup());
            NewMembers.add(NewMember);
        }
    upsert NewMembers;
    }
    //add Field
    @future
    public static void SendField(Set<ID> addField){   
    List<CollaborationGroupMember> NewMembers=new List<CollaborationGroupMember>();
        for (User u: [SELECT Id FROM User Where Id IN :addField]){
            CollaborationGroupMember NewMember = new CollaborationGroupMember(MemberId = u.Id, CollaborationGroupId = getFieldgroup());
            NewMembers.add(NewMember);
        }          
    upsert NewMembers;
    }    
        
    //add Desk
    @future
    public static void SendCS(Set<ID> addCS){   
    List<CollaborationGroupMember> NewMembers=new List<CollaborationGroupMember>();
        for (User u: [SELECT Id FROM User Where Id IN :addCS]){
            CollaborationGroupMember NewMember = new CollaborationGroupMember(MemberId = u.Id, CollaborationGroupId = getCSgroup());
            NewMembers.add(NewMember);
        }          
    upsert NewMembers;
    }        
*/   
}