@isTest
class ScheduleTestClass
{
    static testmethod void test()
    {

         Test.StartTest();
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c; 
        String CRON_EXP = '0 0 0 3 9 ? 2022';
       
        CRF__c CF=new CRF__c();
        CF.Customer_bank_account_number__c='';
        CF.sort_code__c='';
        CF.DD_Confirm__c=False;
        CF.Override_Account_Name__c='Simon';
        //CF.Customer_Email_1_del__c = 'test@test.com';
        //CF.Customer_phone_number__c = '9876543210';
        CF.Your_sales_channel_ID__c='B101';
        CF.Customer_bank_account_number__c='123654789';
        CF.Order_type__c = 'Overlapping Mover';
        CF.Payment_Method__c = 'DD details entered';
        CF.Alt_contact_phone_number__c = '0123456789';
        CF.password__c='asdf@12345';
        CF.Tel_number_for_Broadband_Service__c='02154879632';  
        CF.DD_Confirm__c=true;     
        CF.Sort_Code__c='123654';
        CF.Contact__c = c.Id;
        CF.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
        Account Ac = new Account(Name='TestAc');
        insert Ac;
        
        Opportunity OP = new Opportunity(Name='Test',StageName='Created',Product_Family__c='iNET');
        OP.CloseDate=System.today();
        OP.AccountId = Ac.Id;
        insert OP;
        CF.Opportunity__c = OP.Id;
        
        
        Insert CF;
         
         String jobId = System.schedule('testBasicScheduledApex',CRON_EXP,
        new BatchScheduleDeleteCRFSensitiveData());
         
        
         BatchDeleteCRFSensitiveData reassign = new BatchDeleteCRFSensitiveData('SELECT Id, Customer_bank_account_number__c, Sort_code__c FROM CRF__c Limit 200');
         
         Datetime myDate = System.now() - 30;
         String sDate = String.valueOf(myDate);
   
         ID batchprocessid = Database.executeBatch(reassign);
         
         Test.stopTest();
    
    }

}