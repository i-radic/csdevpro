/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (SeeAllData=true) // Exceptional use (Needed for the accessing of Standard Pricebook)
private class OrderItemTriggerHandler_test {

	private static Account mAccount;
	private static Order mOrder;
	
	private static Map<String, Id> orderRequestRecordTypeMap;

    private static Product2 testProductWelcomeDay;
	private static Pricebook2 testStandardPricebook, testServicesPricebook;
	
	private static PricebookEntry testPbeWelcomeDay;
	
	static{
		
		// Just create a single test account. false = no incremental count for the Company Name
		mAccount = EE_TestClass.createStaticAccount('Test Company', false);
		insert mAccount;
		
        mOrder = new Order();
		mOrder.AccountId 		= mAccount.Id;
		mOrder.EffectiveDate	= system.today().addYears(1);
		mOrder.Status			= 'Draft';
		mOrder.Pricebook2Id		= getPricebookId('Services Price Book');
		mOrder.Type				= 'Included Services';
		insert mOrder;

        // Build up details of the record types for the order requests
        if (orderRequestRecordTypeMap == null) {
            orderRequestRecordTypeMap = new Map<String, Id>(); 
            for( RecordType rct : [SELECT Id, Name, DeveloperName
                                FROM RecordType 
                                WHERE SobjectType='Order_Request__c']){
        
                orderRequestRecordTypeMap.put(rct.DeveloperName, rct.Id);
            }
        }
        
        // Create Custom Settings
        createCustomSettings();
		
	}

   /*
    * @name         testCreateOrderItem
    * @description  Creates an Order Request for every Inserted OrderItem (using upsert)
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    static testmethod void testCreateOrderItem() {    

        OrderItem lOrderItem = new OrderItem();
		lOrderItem.OrderId 			= mOrder.Id;
		lOrderItem.PricebookEntryId =  getPricebookEntryId('Welcome Day', 'Services Price Book');
		lOrderItem.Quantity 		= 5;
		lOrderItem.UnitPrice		= 0;
		        
        Test.StartTest();
        
        // insert the order item
        insert lOrderItem;
        
        Test.StopTest();

        // Check that the OrderItem insert trigger has created an OrderRequest record
        List<Order_Request__c> lListOrderRequests = [ SELECT Id, Order_Quantity__c FROM Order_Request__c WHERE System_Order_Item__c = :lOrderItem.Id LIMIT 1];
        System.AssertEquals( 5, lListOrderRequests[0].Order_Quantity__c );
    
    }


   /*
    * @name         testDeleteOrderItem
    * @description  Deletes the associated Order Request when OrderItem record are deleted
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    static testmethod void testDeleteOrderItem() {    

        OrderItem lOrderItem = new OrderItem();
		lOrderItem.OrderId 			= mOrder.Id;
		lOrderItem.PricebookEntryId =  getPricebookEntryId('Welcome Day', 'Services Price Book');
		lOrderItem.Quantity 		= 1;
		lOrderItem.UnitPrice		= 0;

        insert lOrderItem;
        
        // Retrieve the Order Item
        List<OrderItem> lListOrderItem = [ SELECT Id FROM OrderItem WHERE OrderId = :mOrder.Id];
        
        // Check that the OrderItem insert trigger has created an OrderRequest record
        List<Order_Request__c> lListOrderRequests = [ SELECT Id, Order_Quantity__c FROM Order_Request__c WHERE System_Order_Item__c = :lOrderItem.Id LIMIT 1];
        System.AssertEquals( 1, lListOrderRequests[0].Order_Quantity__c );
                       		        
        Test.StartTest();
        
        // Delete the Order item
        delete lListOrderItem;
        
        Test.StopTest();
        
        // Check that the OrderItem delete trigger has deleted the OrderRequest record
        lListOrderRequests = [ SELECT Id, Order_Quantity__c FROM Order_Request__c WHERE System_Order_Item__c = :lOrderItem.Id LIMIT 1];
        System.Assert(lListOrderRequests.isEmpty());

    }

   /*
    * @name         testCreateOrderItemMultipleStamp
    * @description  Creates multiple Order Request for every Inserted OrderItem (using upsert) and checks the product stamp on the order
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    static testmethod void testCreateOrderItemMultipleStamp() {    


        OrderItem lOrderItem1 = new OrderItem();
		lOrderItem1.OrderId 			= mOrder.Id;
		lOrderItem1.PricebookEntryId =  getPricebookEntryId('Welcome Day', 'Services Price Book');
		lOrderItem1.Quantity 		= 5;
		lOrderItem1.UnitPrice		= 0;
		insert lOrderItem1;
		
		OrderItemTriggerHandler.cbIsFirstRun = true;
		
		OrderItem lOrderItem2 = new OrderItem();
		lOrderItem2.OrderId 			= mOrder.Id;
		lOrderItem2.PricebookEntryId =  getPricebookEntryId('Device Customisation', 'Services Price Book');
		lOrderItem2.Quantity 		= 1;
		lOrderItem2.UnitPrice		= 0;
		insert lOrderItem2;
       
        Test.StartTest();

		// Check that the product stamps are on the Order Multiselect picklist
		Order o = [SELECT Id, Order_Item_Products__c FROM Order WHERE Id = :mOrder.Id];

        Test.StopTest();

        // Check that the OrderItem insert trigger has created an OrderRequest record
        System.AssertEquals( 'Welcome Day;Device Customisation', o.Order_Item_Products__c );
    
    }

   /*
    * @name         createCustomSettings
    * @description  create Custom Settings
    *				requires the use of (SeeAllData=true)
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private static void createCustomSettings() {
    	
    	// Get the Trigger Enabler Custom Setting for Order Item
        TriggerEnabler__c trgEnablerOrderItem = [SELECT Id, Name, TriggerEnabled__c FROM TriggerEnabler__c WHERE Name='OrderItem' LIMIT 1 ]; 
    	if(trgEnablerOrderItem==null){
    		TriggerEnabler__c trgEnabler = EE_TestClass.createTriggerEnabler('OrderItem', true);
			insert trgEnabler;    		
    	}

    	// Get the Order Request RecordType Custom Setting for Welcome Day Order Requests
        OrderRequest_RecordTypeSettings__c orRctWelcomeDay = [SELECT Id FROM OrderRequest_RecordTypeSettings__c WHERE Name='Welcome Day' LIMIT 1 ]; 
    	if(orRctWelcomeDay==null){
	    	// Create a Custom Setting for setting Order Request Items
	        OrderRequest_RecordTypeSettings__c lOrRct = new OrderRequest_RecordTypeSettings__c();
	        lOrRct.Name 				= 'Welcome Day';
	        lOrRct.Record_Type_Name__c 	= 'Welcome_Day';
	        lOrRct.put('Supplier_Email__c', 'sales.enablers.co.uk');
	        lOrRct.Product_Name__c		= 'Welcome Day';
	        lOrRct.Record_Type_Label__c	= 'Welcome Day';
			insert lOrRct;  		
    	}
    }

   /*
    * @name         getPricebookEntryId
    * @description  returns the PricebookEntry Id for the passed product and pricebook
    *				requires the use of (SeeAllData=true)
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private static Id getPricebookEntryId(String psProductName, String psPricebook) {
                 
        // Get the Product
        List<Product2> lListProducts = [SELECT Id, Name, isActive FROM Product2 WHERE Name=:psProductName AND isActive=true LIMIT 1 ];        

        // Get the Pricebook
        List<Pricebook2> lListPricebooks = [SELECT Id, Name, isActive FROM Pricebook2 WHERE Name=:psPricebook AND isActive=true LIMIT 1 ];
 
        // Get the PricebookEntryId
        List<PricebookEntry> lListPricebookEntries = [SELECT Id, Name, isActive FROM PricebookEntry WHERE Product2Id = :lListProducts[0].Id AND Pricebook2Id =:lListPricebooks[0].Id AND isActive=true LIMIT 1 ];

        return lListPricebookEntries[0].Id;
        
	}


   /*
    * @name         getPricebookId
    * @description  returns the Pricebook Id for the passed pricebook
    *				requires the use of (SeeAllData=true)
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private static Id getPricebookId(String psPricebook) {
        // Get the Pricebook
        List<Pricebook2> lListPricebooks = [SELECT Id, Name, isActive FROM Pricebook2 WHERE Name=:psPricebook AND isActive=true LIMIT 1 ];
        return lListPricebooks[0].Id;
	}

    
/*
    private static void createProducts() {
                 
        // Standard Price Book - requiers the use of (SeeAllData=true)
        testStandardPricebook	= [SELECT Id, Name, isActive FROM Pricebook2 WHERE isStandard = true LIMIT 1];
        
        if (!testStandardPricebook.isActive) {
        	testStandardPricebook.isActive = true;
        	update testStandardPricebook;
    	}
            
        // Create Pricebooks
        List<Pricebook2> lListPricebooks 	= new List<Pricebook2>();
                     
        testServicesPricebook 				= new Pricebook2();
        testServicesPricebook.Name			= 'Services Price Book';
        testServicesPricebook.IsActive		= true;     
        lListPricebooks.add(testServicesPricebook);
        
        insert lListPricebooks;

		// Create Products
        List<Product2> lListProducts 	= new List<Product2>();
        testProductWelcomeDay					= new Product2();
        testProductWelcomeDay.Name				= 'Welcome Day';
        testProductWelcomeDay.IsActive			= true;
        lListProducts.add(testProductWelcomeDay);
        
        insert lListProducts;
            
		// Create PricebookEntries
		List<PricebookEntry> lListStandardPricebookEntries 		= new List<PricebookEntry>();
		
		PricebookEntry testPbeStdWelcomeDay		= new PricebookEntry();
		testPbeStdWelcomeDay.UnitPrice			= 1500.00;
		testPbeStdWelcomeDay.Product2Id			= testProductWelcomeDay.Id;
		testPbeStdWelcomeDay.Pricebook2Id		= testStandardPricebook.Id;//testServicesPricebook.Id;
		testPbeStdWelcomeDay.IsActive			= true;

		lListStandardPricebookEntries.add( testPbeStdWelcomeDay );
		
		testPbeWelcomeDay					= new PricebookEntry();
		testPbeWelcomeDay.UnitPrice			= 0.00;
		testPbeWelcomeDay.Product2Id		= testProductWelcomeDay.Id;
		testPbeWelcomeDay.Pricebook2Id		= testServicesPricebook.Id;
		testPbeWelcomeDay.IsActive			= true;
		
		lListStandardPricebookEntries.add( testPbeWelcomeDay );
		
		insert lListStandardPricebookEntries;


	}
*/   
    
    
}