global class CreateAllTikitKPIs3 implements Schedulable
{
      global void execute(SchedulableContext sc)   
      {   
      database.executeBatch(new BatchCreateActiveAccountsKPI('FISCAL_YEAR') );
      database.executeBatch(new BatchCreateSalesOrderValueKPI() );     
      database.executeBatch(new BatchCreateKeyDealsKPI() );
      }
    

}