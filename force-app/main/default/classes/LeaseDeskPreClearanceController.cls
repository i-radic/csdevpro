public class LeaseDeskPreClearanceController {
        
    public Opportunity oppty {get;set;}
    private static List<Leasing_History__c> insertLeasingHistory = new List<Leasing_History__c>();
    private static List<String[]> funderErrors = new List<String[]>();
    public List<LeaseDeskAPI.QuickDecisionResponse> quickDecisionResponseList {get;set;}
    public String opptyId {get;set;}
    public Account acct {get;set;}
    public Boolean displayPreClearance {get;set;}
    public String totalCost {get;set;}
    public Boolean displayBackButton {get;set;}
    public String selectedContractTerm {set;get;}
    public User user {get;set;}
    private String tempRAG {get;set;}
    public LeaseDeskAPI.ProfileType profileType {get;set;}
    
    
    public String getSelectedContractTerm()
    {
        return selectedContractTerm;
    }
    
    public void setSelectedContractTerm(String value)
    {
        this.selectedContractTerm = value;
    }
    
    public Boolean getDisplayBackButton()
    {
        return displayBackButton;    
    } 
     
    public void setDisplayBackButton(Boolean value)
    {
        this.displayBackButton = value;
    }    
    
    public String getTotalCost()
    {
        return totalCost;    
    } 
     
    public void setTotalCost(String value)
    {
        this.totalCost = value;
    }
    
    public void LeaseDeskPreClearanceController()        
    {
        displayPreClearance = false;
        //displayMessages = false;
        displayBackButton = false;
        totalCost = '';
    }
    
    public LeaseDeskPreClearanceController(ApexPages.StandardController stdController)
    {   
        opptyId = ApexPages.currentPage().getParameters().get('opptyid');
        oppty = LeaseDeskUtil.getOpportunityDetails(opptyId); 
        acct = LeaseDeskUtil.getAccountDetails(oppty.AccountId); 
        user = LeaseDeskUtil.getUserDetails(); 
        displayPreClearance();    
    }
    
    public void displayPreClearance()
    {
        if(LeaseDeskUtil.isBTLBAccount(acct.Id)){
            if(LeaseDeskUtil.isOutrightSale(oppty.Id)){
                totalCost = String.valueOf(LeaseDeskUtil.getTotalOutrightSaleCost(oppty.Id));
                System.debug('############################################# totalCost ' + totalCost);
                displayPreClearance = true;
            }else{
                displayPreClearance = false;
                displayBackButton = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There are no Outright Sale or Lease products against this opportunity.'));
            }
        }
        else{
            if(LeaseDeskUtil.isOutrightSaleCORP(oppty.Id)){
                totalCost = String.valueOf(LeaseDeskUtil.getTotalOutrightSaleCostCORP(opptyId));
                System.debug('############################################# totalCost ' + totalCost);
                displayPreClearance = true;
            }else{
                displayPreClearance = false;
                displayBackButton = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There are no products against this opportunity.'));
            }
        }     
    }
    
    public PageReference performQuickDecision()
    {   
        List<String> existingFunders = new List<String>();
        List<String[]> fundersToRetry = new List<String[]>();
        profileType = LeaseDeskAPI.ProfileType.LB;        
        Integer contractMonths = Integer.valueof(selectedContractTerm) * 12;
            
        //------------Perform validation
        if(!isImanageUsernameOk()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'iManage username is blank, please complete via your Salesforce User Detail page!!'));
            return null;
        }
        if(!isPreClearFirstTime()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'This Opportunity has already had a finance request submitted, check the leasing indicator for funder results!!'));
            return null;
        }
        if((totalCost == null) || (totalCost == '')){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'Please enter a value!!'));
            return null;
        }
        if(Decimal.ValueOf(totalCost) >= 100000){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Warning, 'The total is over £100,000.00, please contact the DLL Account Manager.'));
            return null;
        }
        
        //------------CORP Holding Proposal   
        if(Test_Factory.GetProperty('IsTest') != null || !LeaseDeskUtil.isBTLBAccount(acct.Id)){                        
            profileType = LeaseDeskAPI.ProfileType.Corp;
            LeaseDeskAPI api = new LeaseDeskAPI();
            LeaseDeskAPI.HoldingProposalResponse holdingProposalResponse = new LeaseDeskAPI.HoldingProposalResponse();
            holdingProposalResponse = api.CreateHoldingProposal(user.EIN__c, user.OUC__c, acct.leaseDeskId__c, opptyId, Double.valueOf(totalCost), Integer.valueof(selectedContractTerm));
        
            if(holdingProposalResponse.Error ==  null || Test_Factory.GetProperty('IsTest') == 'yes'){
                Leasing_History__c leasingHistory = new Leasing_History__c();
                leasingHistory.Opportunity__c = oppty.Id;
                leasingHistory.Account__c = oppty.AccountId;
                leasingHistory.Funder_URL__c = holdingProposalResponse.holdingURL;
                leasingHistory.AgreementID__c = holdingProposalResponse.proposalID;
                leasingHistory.Funder__c = 'Holding Proposal';
                insertLeasingHistory.add(leasingHistory);                                
            }
            else{
                //------------Insert callout logs
                if(LeaseDeskAPI.logList.Size() > 0){
                    if(!system.Test.isRunningTest()){
                        insert LeaseDeskAPI.logList;
                    }
                }               
                String holdingPropError = holdingProposalResponse.Error;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Holding Proposal' + ', ' + holdingPropError));
                return null;
            }
        }       
        
        if(acct.LeaseDeskID__c == null){
            PageReference accountMatchPage = new PageReference('/apex/LeaseDeskAccountMatch?opptyid=' + oppty.Id);
            accountMatchPage.setRedirect(true);       
            return accountMatchPage;   
        }
        else {                                  
            existingFunders = LeaseDeskUtil.getFunders(oppty.AccountId);
            
            //------------Callout to LeaseDeskAPI MultipleQuickDecision
            //List<LeaseDeskAPI.QuickDecisionResponse> quickDecisionResponseList = new List<LeaseDeskAPI.QuickDecisionResponse>();
            LeaseDeskAPI api = new LeaseDeskAPI();
            if(!system.Test.isRunningTest()){
                quickDecisionResponseList = api.MultipleQuickDecision(user.EIN__c, user.OUC__c,acct.LeaseDeskID__c,oppty.Opportunity_Id__c,Double.valueOf(totalCost),Integer.valueof(selectedContractTerm), existingFunders, user.Leasing_iManage_Username__c, profileType);                     
            }
            for (LeaseDeskAPI.QuickDecisionResponse quickDecisionResponse : quickDecisionResponseList){
                ProcessLeasingData(quickDecisionResponse, quickDecisionResponse.Funder, contractMonths);
            }            
            //------------Check response for ErrorType "Internal-Retryable"                                                                        
            if(funderErrors.Size() > 0){                
                for(String[] funderError : funderErrors){
                    if(funderError[1] == 'Internal-Retryable'){
                        fundersToRetry.Add(funderError);
                    }
                }
            }
            //------------Callout to LeaseDeskAPI QuickDecision for funders with ErrorType "Internal-Retryable"    
            if(fundersToRetry.Size() > 0){   
                for (String[] funder : fundersToRetry){
                    LeaseDeskAPI.Funder currentFunder;
                        
                    if(funder[0] == 'GE'){
                        currentFunder = LeaseDeskAPI.Funder.GE;
                    }
                    if(funder[0] == 'Shire'){
                        currentFunder = LeaseDeskAPI.Funder.Shire;
                    }
                    if(funder[0] == 'CIT'){
                        currentFunder = LeaseDeskAPI.Funder.CIT;
                    }
                    if(funder[0] == 'DLL'){
                        currentFunder = LeaseDeskAPI.Funder.DLL;
                    }
                            
                    LeaseDeskAPI.QuickDecisionResponse quickDecisionResponse = new LeaseDeskAPI.QuickDecisionResponse();        
                    quickDecisionResponse = api.QuickDecision(user.EIN__c, user.OUC__c,acct.LeaseDeskID__c,oppty.Opportunity_Id__c,Double.valueOf(totalCost),Integer.valueof(selectedContractTerm), currentFunder, user.Leasing_iManage_Username__c, profileType);
                    ProcessLeasingData(quickDecisionResponse, funder[0], contractMonths);             
                }
            }
        }  
        //------------Insert leasing records
        if(insertLeasingHistory.Size() > 0){
            insert insertLeasingHistory;
        }           
        //------------Update Opportunity RAG
        update oppty;                       
        //------------Insert callout logs
        if(LeaseDeskAPI.logList.Size() > 0){
            insert LeaseDeskAPI.logList;
        }           
        //------------Show any funder errors
        if(funderErrors.Size() > 0){
            for(String[]funderError: funderErrors){
                if(funderError[1] != 'Internal-Retryable'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, funderError[0] + ', ' + funderError[3]));
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'Please click back to the opportunity to view other funder responses'));
                }
             }
            return null;
        }          
        return new ApexPages.StandardController(oppty).view();                 
    }
    
    private void ProcessLeasingData(LeaseDeskAPI.QuickDecisionResponse qDecisionResponse, String funder, Integer contractMonths)
    {
        Leasing_History__c existingFundersResponse = new Leasing_History__c();
        existingFundersResponse.Opportunity__c = oppty.Id;
        existingFundersResponse.Account__c = oppty.AccountId;
        DateTime endDate = DateTime.Now().addDays(90);
        existingFundersResponse.End_Date__c = endDate.date();
        existingFundersResponse.Status__c = 'Prospect: Preclearance';
        existingFundersResponse.clearance__c = qDecisionResponse.ragStatus;
        existingFundersResponse.AgreementID__c = qDecisionResponse.proposalID;
        existingFundersResponse.Credit_limit__c = qDecisionResponse.creditLimit;
        existingFundersResponse.Funder__c = funder;
        existingFundersResponse.Original_Term__c =  String.valueOf(contractMonths) + ' Months';
               
        if(qDecisionResponse.ErrorText != null){
            funderErrors.add(new String[]{funder, qDecisionResponse.ErrorType, qDecisionResponse.ErrorCode, qDecisionResponse.ErrorText});
        } 
        else{
            insertLeasingHistory.add(existingFundersResponse);
        }
              
        //------------Update RAG status field on oppty                                   
      /**  if(qDecisionResponse.ragStatus == 'Green'){
            oppty.Finance_Available__c = qDecisionResponse.ragStatus;
            tempRAG = 'Green';
        }
        if(qDecisionResponse.ragStatus == 'Amber'){
            if(tempRAG != 'Green'){
                oppty.Finance_Available__c = qDecisionResponse.ragStatus;
                tempRAG = 'Amber';
            }
        }
        if(qDecisionResponse.ragStatus == 'Red'){
            if(tempRAG != 'Amber' && tempRAG != 'Green'){
                oppty.Finance_Available__c = qDecisionResponse.ragStatus;
                tempRAG = 'Red';
            }
        } 
        */                       
    }
    
    public PageReference back(){           
        return new ApexPages.StandardController(oppty).view();
    } 
    
    private Boolean isImanageUsernameOk(){
        if(user.Leasing_iManage_Username__c == null || user.Leasing_iManage_Username__c == ''){
            return false;
        }
        else{
            return true;
        }
    }   
    
    private Boolean isPreClearFirstTime(){
        List<Leasing_History__c> leasingHistoryList = [Select ID, Opportunity__c from Leasing_History__c WHERE Opportunity__c =: oppty.Id];
        if(leasingHistoryList.Size() > 0){
            return false;
        }
        else{
            return true;
        }
    } 
}