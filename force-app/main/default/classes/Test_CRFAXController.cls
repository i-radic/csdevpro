@isTest
private class Test_CRFAXController{
    static testMethod void myUnitTest() {
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
        //  set up data
        Opportunity o = new Opportunity();
        o.name='test';
        o.StageName='FOS_Stage';
        insert o;
        
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        
        CRF__c crf = new CRF__c(Opportunity__c = o.Id);
        crf.RecordTypeId = [Select Id from RecordType where DeveloperName='AX_Form'].Id;  
        crf.Contact__c = c.Id;      
        insert crf;
        
        //Provide_Line_Details__c pd = new Provide_Line_Details__c();
        //pd.name = 'abcd';
        //pd.Related_To_CRF__c = crf.id;
        //insert pd;
        
        List<Provide_Line_Details__c> PDList= new List<Provide_Line_Details__c>{};
           
        for(Integer i = 0; i < 200; i++){
          Provide_Line_Details__c p = new Provide_Line_Details__c();
          p.Name = 'Test PD' + i;
          p.Related_To_CRF__c = crf.id;
          p.Serial_No__c = i;
          PDList.add(p);
        }        
        System.debug('---->'+PDList.size());           
        insert PDList;       
                
        List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,Serial_No__c from Provide_Line_Details__c where related_to_CRF__c=:crf.Id AND (Serial_No__c>10 AND Serial_No__c<=20) ORDER BY Serial_No__c];
        System.debug('---->'+ProvideLines.size()); 
                
        CRFAXController myController = new CRFAXController(new ApexPages.StandardController(crf));
        myController.getProvideLines();
        myController.getProvideLines1();
        myController.getProvideLines2();
        myController.getProvideLines3();
        myController.getProvideLines4();
        myController.getProvideLines5();
        myController.getProvideLines6();
        myController.getProvideLines7();
        myController.getProvideLines8();
        myController.getProvideLines9();
        myController.getProvideLines10();
        
        myController.getShowTable();
        myController.getShowTable1();
        myController.getShowTable2();
        myController.getShowTable3();
        myController.getShowTable4();
        myController.getShowTable5();
        myController.getShowTable6();
        myController.getShowTable7();
        myController.getShowTable8();
        myController.getShowTable9();
        myController.getShowTable10();
        
        myController.getDivision();
        myController.getDivision1();
        myController.getDivision2();
        myController.getDivision3();
        myController.getDivision4();
        myController.getDivision5();
        myController.getDivision6();
        myController.getDivision7();
        myController.getDivision8();
        myController.getDivision9();
        myController.getDivision10();
    }
    
    static testMethod void myUnitTest1() {
        
        //  set up data
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     	}
        Opportunity o = new Opportunity();
        o.name='test';
        o.StageName='FOS_Stage';
        insert o;
        
        Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        
        CRF__c crf = new CRF__c(Opportunity__c = o.Id);
        crf.RecordTypeId = [Select Id from RecordType where DeveloperName='AX_Form'].Id;  
        crf.Contact__c = c.Id;      
        insert crf;
        
        /*Provide_Line_Details__c pd = new Provide_Line_Details__c();
        pd.name = 'abcd';
        pd.Related_To_CRF__c = crf.id;
        insert pd;
        
        List<Provide_Line_Details__c> PDList= new List<Provide_Line_Details__c>{};
           
        for(Integer i = 0; i < 200; i++){
          Provide_Line_Details__c p = new Provide_Line_Details__c();
          p.Name = 'Test PD' + i;
          p.Related_To_CRF__c = crf.id;
          p.Serial_No__c = i;
          PDList.add(p);
        }        
        System.debug('---->'+PDList.size());           
        insert PDList;       
                
        List<Provide_Line_Details__c> ProvideLines = [select Name, Quantity__c, Description_AX__c,Supply_Code__c,Initial_charges__c,Initial_Charge_Code__c,Recurring_charges__c,Recurring_Charge_Code__c,Rental_Term__c,Product_code__c,Serial_No__c from Provide_Line_Details__c where related_to_CRF__c=:crf.Id AND (Serial_No__c>10 AND Serial_No__c<=20) ORDER BY Serial_No__c];
        System.debug('---->'+ProvideLines.size()); 
               
        */       
                
        CRFAXController myController = new CRFAXController(new ApexPages.StandardController(crf));
        myController.getProvideLines();
        myController.getProvideLines1();
        myController.getProvideLines2();
        myController.getProvideLines3();
        myController.getProvideLines4();
        myController.getProvideLines5();
        myController.getProvideLines6();
        myController.getProvideLines7();
        myController.getProvideLines8();
        myController.getProvideLines9();
        myController.getProvideLines10();
        
        myController.getShowTable();
        myController.getShowTable1();
        myController.getShowTable2();
        myController.getShowTable3();
        myController.getShowTable4();
        myController.getShowTable5();
        myController.getShowTable6();
        myController.getShowTable7();
        myController.getShowTable8();
        myController.getShowTable9();
        myController.getShowTable10();
        
        myController.getDivision();
        myController.getDivision1();
        myController.getDivision2();
        myController.getDivision3();
        myController.getDivision4();
        myController.getDivision5();
        myController.getDivision6();
        myController.getDivision7();
        myController.getDivision8();
        myController.getDivision9();
        myController.getDivision10();
    }

}