global class BTSportSiteAttCont {
    
    WebService static void createAttachment(String siteId,String sName)
    {
        
        Integer incr= 123;
        String siteName='';
        BT_Sport_Site__c bt= new BT_Sport_Site__c();
        bt= [Select Id,Name,Site_Display_Name__c,type__c from BT_Sport_Site__c where Id=:siteId]; 
        String st='';      
        system.debug('ssssssss'+bt+' sdfsdfsdf  '+st);
        PageReference pdf = Page.BTSportSiteTemplate;
        pdf.getParameters().put('id',bt.Id);
        pdf.getParameters().put('type',sName);
        
        
        //system.debug('rrrrrrrrr'+pdf.getContent());
        Attachment attach = new Attachment();
        Blob body;
        
        try {
        
        // returns the output of the page as a PDF
        body = pdf.getContentAsPDF();   
        } catch (VisualforceException e) {
        body = Blob.valueOf('Some Text');
        }     
        attach.Body = body;
        //attach.body =pdf.getContentAsPDF();   
        DateTime dt = DateTime.now();
        String formattedDt = dt.format('yyyy-MM-dd\'_\'kk-mm-ss');
        system.debug('$$$$$'+formattedDt);
        if(bt.Site_Display_Name__c != null && (bt.Site_Display_Name__c.contains('Quote') || bt.Site_Display_Name__c.contains('Order'))){
            st=bt.Site_Display_Name__c.substring(5, bt.Site_Display_Name__c.length());
            if(sName.contains('Quote'))
             {                     
                  attach.Name = 'Quote_'+bt.Name+'_'+formattedDt+'.pdf';
                  siteName='Quote';
                  bt.type__c='Quote';
             }
            else
            {                
                 attach.Name = 'Order_'+bt.Name+'_'+formattedDt+'.pdf';
                 siteName='Order';
                 bt.type__c='Order';
            }       
        }
        else
        {
            st='0';
            if(sName.contains('Quote'))
             {                     
                  attach.Name = 'Quote_'+bt.Name+'_'+formattedDt+'.pdf';
                  siteName='Quote';
                  bt.type__c='Quote';
             }
            else
            {                
                 attach.Name = 'Order_'+bt.Name+'_'+formattedDt+'.pdf';
                 siteName='Order';
                 bt.type__c='Order';
            }     
        }  
        attach.IsPrivate = false;
        attach.ContentType='application/PDF';
        // attach the pdf to the account
        attach.ParentId = bt.Id;
        insert attach;
        
        incr=Integer.valueOf(st);
        system.debug('nnnnnnnnnnn'+incr);
        incr++;
        system.debug('iiiiiiiiiiii'+incr);      
        bt.Site_Display_Name__c=siteName+incr;      
        update bt;
        /*pagereference pg= new Pagereference('/'+testid);
        pg.setredirect(true);
        return pg;*/
    
    }


}