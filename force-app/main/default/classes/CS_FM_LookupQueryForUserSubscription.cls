global class CS_FM_LookupQueryForUserSubscription implements cssmgnt.RemoteActionDataProvider   {

   global Map<String, Object> getData(Map<String, Object> inputMap) {
        Map<String, Object> returnMap = new Map<String, Object>(); 
        String module = String.valueOf(inputMap.get('module'));
        String grouping = String.valueOf(inputMap.get('grouping'));
        String productDefination = String.valueOf(inputMap.get('productDefination'));
        String scheme = inputMap.containsKey('scheme')?String.valueOf(inputMap.get('scheme')):'';
        String name = String.valueOf(inputMap.get('name'));
        System.debug('inputMap>>>'+inputMap);
        try{
            List<cspmb__Price_Item__c> priceItemList= [select id,Name, cspmb__recurring_charge__c , cspmb__recurring_cost__c,cspmb__one_off_charge__c, 
            cspmb__one_off_cost__c 
            from cspmb__Price_Item__c where Module__c =:module and Grouping__c =:grouping 
            and cspmb__Product_Definition_Name__c = :productDefination and  Scheme__c = : scheme and Name = : name];
            if(!priceItemList.isEmpty()){
                System.debug('priceItemList>>>'+priceItemList);
                returnMap.put ('PriceItemId', priceItemList[0].id);
                returnMap.put ('PriceItemName', priceItemList[0].Name);
                
                returnMap.put ('recurringCharge', priceItemList[0].cspmb__recurring_charge__c);
                returnMap.put ('recurringCost', priceItemList[0].cspmb__recurring_cost__c);
                returnMap.put ('oneOffCharge', priceItemList[0].cspmb__one_off_charge__c);
                returnMap.put ('oneOffCost', priceItemList[0].cspmb__one_off_cost__c);
                
                returnMap.put ('DataFound', true);
            }else{
                 returnMap.put ('DataFound', false);
            }
            
        }catch(Exception e){
            
        }
        return returnMap;
    }    
    
}