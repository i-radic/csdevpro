public with sharing class ContactOpportunityController {
    public ContactOpportunityController(ApexPages.StandardController controller) {
   
    }
public string conID = System.currentPageReference().getParameters().get('ID');

List<Contact> ConAccID = [SELECT AccountId FROM Contact WHERE ID = :conID LIMIT 1 ];
Id accID = ConAccID[0].AccountId;

public List<Opportunity> getAccOppHist(){
  
return [Select Id,Opportunity_Id__c,Name,StageName,Owner__c,Product_Family__c,Amount,CloseDate,LastModifiedDate,(SELECT Contact.Name,ContactId FROM Opportunity.OpportunityContactRoles where IsPrimary =TRUE) from Opportunity where AccountID =:accID
              ORDER BY CreatedDate DESC];
}

  
}