global class CreateAllTikitKPIs2 implements Schedulable
{
      global void execute(SchedulableContext sc)   
      {   
      database.executeBatch(new BatchCreateWinRatioKPI('MONTH') );
      database.executeBatch(new BatchCreateWinRatioKPI('FISCAL_QUARTER') );
      database.executeBatch(new BatchCreateWinRatioKPI('FISCAL_YEAR') );
      database.executeBatch(new BatchCreateActiveAccountsKPI('MONTH') );
      database.executeBatch(new BatchCreateActiveAccountsKPI('FISCAL_QUARTER') );
      }
    

}