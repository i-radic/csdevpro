global with sharing class CustomButtonCreditFunds extends csbb.CustomButtonExt {
    
	public String performAction (String basketId) {
        Boolean isValid = true;
        
        Profile currentProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        CustomConsoleFiltering.ProductConfigurationStructure pcs = CustomConsoleFiltering.getJSONResource();
        
        cscfga__Product_Basket__c basket = [SELECT
                                        Id, Sector__c, 
                                            (SELECT Id, cscfga__Product_Definition__r.Name, cscfga__originating_offer__r.Id,
                                                    cscfga__originating_offer__r.Product_Definitions__c,
                                                    cscfga__Product_Definition__r.cscfga__Product_Category__r.Name
                                                FROM cscfga__Product_Configurations__r)
                                    FROM
                                        cscfga__Product_Basket__c
                                    WHERE
                                        Id = :basketId];
        
        List<cscfga__Offer_Category_Association__c> ocaList = 
            [ 
                SELECT Id, cscfga__Configuration_Offer__c, 
                    cscfga__Configuration_Offer__r.Name,
                    cscfga__Configuration_Offer__r.Id,
                    cscfga__Configuration_Offer__r.Product_Definitions__c,
                    cscfga__Configuration_Offer__r.cscfga__Description__c,
                    cscfga__Configuration_Offer__r.Parent_Offer_Name__c,
                    cscfga__Product_Category__c,
                    cscfga__Product_Category__r.Name,
                    cscfga__Product_Category__r.cscfga__Parent_Category__r.Name
                FROM cscfga__Offer_Category_Association__c 
                WHERE cscfga__Configuration_Offer__r.Name = 'Credit Funds'
            ];
        
        for(cscfga__Offer_Category_Association__c oca : ocaList){
            if(pcs.isInvalid(basket, currentProfile.Name, oca)) {
                isValid = false;
            }
            if(!pcs.isProductRulesValid(basket.cscfga__Product_Configurations__r, oca)) {
                isValid = false;   
            }
        }
        
        return isValid ? '{"status":"ok","redirectURL":"' + '/apex/c__CS_CreditFundListEditor?id='+basketId + '"}' : '{"status":"error","title":"Error","text":"Credit Funds are only available with Corporate products."}';
    } 
}