/**************************************************************************************************************************************************
Class Name : CreateMultipleActionRecordsApex
Test Class Name : CreateMultipleActionRecordsApexTest
VisualForce Page : CreateMultipleActionRecords
Description : Code to Create multiple Customer Experience Action records on the go.
Version : V0.1
Created By Author Name : BALAJI MS
Date : 09/02/2018
Modified Date : 09/02/2018
*************************************************************************************************************************************************/

public class CreateMultipleActionRecordsApex {
    public list<Customer_Experience_Actions__c> CEAlist = new list<Customer_Experience_Actions__c>();
    public List<wrapperClass> lstWrapper{get;set;}
    public String selectedRowIndex{get;set;}
    public Integer count = 1;    
    public Boolean errorFlag {get; set;}
    public CSAT_Contact_Feedback__c CsatRec{get; set;}
    public CreateMultipleActionRecordsApex(ApexPages.StandardController controller){
        errorFlag = True;
        CSAT_Contact_Feedback__c CsatRec = [Select Id, Action_required__c, Name from CSAT_Contact_Feedback__c where id =: apexpages.currentpage().getparameters().get('Sid')];
        if(CsatRec.Action_required__c == Null)
            errorFlag = False;
        lstWrapper = new List<wrapperClass>();
        addMoreRows();
        selectedRowIndex = '0';
    }    
    public PageReference Save(){
        try{
            PageReference pr = new PageReference('/'+apexpages.currentpage().getparameters().get('Sid')); 
            for(Integer j = 0;j<lstWrapper.size();j++)
                CEAlist.add(lstWrapper[j].CEA);
            if(CEAlist.size() > 0)
                insert CEAlist;
            pr.setRedirect(True);
            return pr;
        }catch(Exception Ex){
            return null;
        }
    }
    public PageReference Cancel(){
        return new PageReference('/'+apexpages.currentpage().getparameters().get('Sid'));
    }
    public void Add(){   
        count = count+1;
        addMoreRows();      
    }
    public void addMoreRows(){      
        wrapperClass objInnerClass = new wrapperClass(count);
        lstWrapper.add(objInnerClass);          
    }
    public void Del(){ 
        List<wrapperClass> TemporaryWrapper = new List<wrapperClass>();
        for(wrapperClass wc : lstWrapper){
            if(wc.checkbox != True)
                TemporaryWrapper.add(wc);
        }
        lstWrapper = TemporaryWrapper;
    }
    public class wrapperClass{
        public Service_Improvement_Plan__c PlanRecord;
        public String recCount{get;set;}
        public boolean checkbox {get; set;}
        public Customer_Experience_Actions__c CEA {get; set;}
        public wrapperClass(Integer intCount){
            PlanRecord = new Service_Improvement_Plan__c();
            PlanRecord = [Select Id, Name from Service_Improvement_Plan__c where Account__c =: apexpages.currentpage().getparameters().get('Aid')];
            recCount = String.valueOf(intCount);
            CEA = new Customer_Experience_Actions__c();
            CEA.CSAT_Contact_Feedback_LINK__c = apexpages.currentpage().getparameters().get('Sid');
            CEA.Customer_Experience_Client_Plan__c = PlanRecord.Id;
            CEA.Customer_Owner__c = apexpages.currentpage().getparameters().get('Cid');
        }
    }
    
}