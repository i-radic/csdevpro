@isTest
private class tgrRecalcICTDueDates_Test{
static testMethod void UnitTest() {
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        }
        date tdy = system.Today();
        Account ALL = Test_Factory.CreateAccount();
        ALL.Name='Test LL';
        ALL.SAC_Code__c='SACLL11';
        ALL.Base_Team_Assign_Date__c = tdy+7;
        ALL.Sector__c = 'BT Local Business';
        ALL.BTLB_Common_Name__c = 'abcds';
        Insert ALL;
        
        Opportunity o = Test_Factory.CreateOpportunity(ALL.id);
        o.Name='TEST';
        o.RecordTypeId = '01220000000PjDu';
        o.stageName = 'Created';
        o.CloseDate = tdy;
        o.Expected_Close_Date__c = o.CloseDate;
        o.zCurrent_Week_Forecast_Vol__c = 1;
        o.Current_Week_Forecast_Amount__c = 200;
        o.Win_Rating_Definition_pl__c = 'BT has advantage on Price / Product / Strategy';
        insert o;
    
        ict_sales_journey__c isj1 = new ict_sales_journey__c (Opportunity_c__c = o.Id,propose_Due_Date__c = tdy, qualify_Due_Date__c = tdy.addDays(1), sell_Due_Date__c = tdy.addDays(2), Closure_Due_Date__c = tdy.addDays(3),Lead_Due_Date__c = tdy.addDays(4));
        insert isj1;
        date updt = tdy.addDays(4);
        date downdt = tdy.addDays(-2);
        isj1.Lead_Due_Date__c = updt;
        update isj1;
        isj1.Lead_Due_Date__c = downdt; 
        update isj1;
        isj1.qualify_Due_Date__c = updt;
        update isj1;
        isj1.qualify_Due_Date__c = downdt;
        update isj1;
        isj1.sell_Due_Date__c = updt;
        update isj1;
        isj1.sell_Due_Date__c = downdt;
        update isj1;
        isj1.propose_Due_Date__c = updt;
        update isj1;
        isj1.propose_Due_Date__c = downdt;
        update isj1;
    }
}