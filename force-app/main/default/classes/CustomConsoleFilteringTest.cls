@isTest
public with sharing class CustomConsoleFilteringTest {
    private static CustomConsoleFiltering ccf = null;
    private static cscfga__Product_Basket__c basket = null;

    private static void crateTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');

        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        
        basket =  CS_TestDataFactory.generateProductBasket(true, 'test basket', testOpp);

        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');

        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile');

        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);
        prodConfig.cscfga__Product_Definition__c = prodDef.Id;
        prodConfig.cscfga__Configuration_Offer__c = testOffer.ID;

        insert prodConfig;

        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'BT Mobile');
        cscfga__Product_Category__c btopCat = CS_TestDataFactory.generateProductCategory(true, 'BTOP');

        cscfga__Offer_Category_Association__c oca = CS_TestDataFactory.generateCategoryDefinitionAssoc(true, prodCategory, testOffer);
        btopCat.cscfga__Parent_Category__c = prodCategory.Id;
        update btopCat;
    }
 
    private static void crateTestDataEESME() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');

        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        
        basket =  CS_TestDataFactory.generateProductBasket(true, 'test basket', testOpp);

        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BT Mobile');

        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile');

        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(false, 'BT Mobile', basket);
        prodConfig.cscfga__Product_Definition__c = prodDef.Id;
        prodConfig.cscfga__Configuration_Offer__c = testOffer.Id;

        insert prodConfig;

        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'EE Corp');
        cscfga__Product_Category__c btopCat = CS_TestDataFactory.generateProductCategory(true, 'EE Corp');

        cscfga__Offer_Category_Association__c oca = CS_TestDataFactory.generateCategoryDefinitionAssoc(true, prodCategory, testOffer);
        btopCat.cscfga__Parent_Category__c = prodCategory.Id;
        update btopCat;
    }

    private static void crateTestDataTESTPHONE() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');

        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        
        basket =  CS_TestDataFactory.generateProductBasket(true, 'test basket', testOpp);

        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'TEST PHONE');

        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'TEST PHONE');

        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(false, 'TEST PHONE', basket);
        prodConfig.cscfga__Product_Definition__c = prodDef.Id;
        prodConfig.cscfga__Configuration_Offer__c = testOffer.Id;

        insert prodConfig;

        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'TEST PHONE');
        cscfga__Product_Category__c btopCat = CS_TestDataFactory.generateProductCategory(true, 'TEST');

        cscfga__Offer_Category_Association__c oca = CS_TestDataFactory.generateCategoryDefinitionAssoc(true, prodCategory, testOffer);
        btopCat.cscfga__Parent_Category__c = prodCategory.Id;
        update btopCat;
    }
     private static void crateTestDataBTOP() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Account testAcc = CS_TestDataFactory.generateAccount(true, 'test');

        Opportunity testOpp = CS_TestDataFactory.generateOpportunity(true, 'test', testAcc);
        
        basket =  CS_TestDataFactory.generateProductBasket(true, 'BTOP CORE', testOpp);
      
         
        cscfga__Configuration_Offer__c testOffer =  CS_TestDataFactory.generateOffer(true, 'BTOP CORE');

        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'BTOP CORE');

        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(false, 'BTOP CORE', basket);
        prodConfig.cscfga__Product_Definition__c = prodDef.Id;
        prodConfig.cscfga__Configuration_Offer__c = testOffer.ID;
        //ProdConfig..cscfga__Product_Basket__c = 

        insert prodConfig;

        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'BTOP CORE');
        cscfga__Product_Category__c btopCat = CS_TestDataFactory.generateProductCategory(true, 'BTOP Devices');

        cscfga__Offer_Category_Association__c oca = CS_TestDataFactory.generateCategoryDefinitionAssoc(true, btopCat, testOffer);
        btopCat.cscfga__Parent_Category__c = prodCategory.Id;
        update btopCat;
    }

    @isTest
    public static void testController(){
        crateTestData();
        ccf = new CustomConsoleFiltering();
        List<Id> lista = ccf.getApprovedIds(basket.Id);
        System.assertNotEquals(null, lista);
    }

    @isTest
    public static void testController1(){
        crateTestDataEESME();
        ccf = new CustomConsoleFiltering();
        List<Id> lista = ccf.getApprovedIds(basket.Id);
        System.assertNotEquals(null, lista);
    }

    @isTest
    public static void testController3(){
        crateTestDataTESTPHONE();
        ccf = new CustomConsoleFiltering();
        List<Id> lista = ccf.getApprovedIds(basket.Id);
        System.assertNotEquals(null, lista);
    }
    @isTest
    public static void testController4(){
        
        crateTestDataBTOP();
        ccf = new CustomConsoleFiltering();
        List<Id> lista = ccf.getApprovedIds(basket.Id);
        System.assertNotEquals(null, lista);
    }

    @IsTest
    public static void testInvoke() {
        crateTestData();

        Test.startTest();
        ccf = new CustomConsoleFiltering();
        Object approvedIds = ccf.invoke(basket.Id);
        Test.stopTest();

        System.assertNotEquals(null, approvedIds);
    }
    
    @IsTest
    public static void testCustomClass() {
        crateTestData();

        Test.startTest();
        ccf = new CustomConsoleFiltering();
        
        CustomConsoleFiltering.ProductConfigurationStructure pcs = new CustomConsoleFiltering.ProductConfigurationStructure();
        Map<String, Set<String>> profileMap = new Map<String, Set<String>>{'System administrator' => new Set<String>{'test'}};
        pcs.Profiles = profileMap;
        Boolean checkIfValid = pcs.isProfilePDsValid('System administrator', new cscfga__Offer_Category_Association__c());
        checkIfValid = pcs.isCategoryValid(new List<cscfga__Product_Configuration__c>(), new cscfga__Offer_Category_Association__c());
        
        Test.stopTest();
    }
    @IsTest
    public static void testWithSpgDeyToKeep() {
        crateTestData();
        cscfga__Product_Definition__c prodDef = CS_TestDataFactory.generateProductDefinition(true, 'BT Mobile Flex');
        cscfga__Product_Configuration__c pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
        pc.cscfga__Product_Definition__c = prodDef.Id;
        INSERT pc;

        Test.startTest();
        ccf = new CustomConsoleFiltering();
        List<Id> lista = ccf.getApprovedIds(basket.Id);
        Test.stopTest();

        System.assertNotEquals(null, lista);
    }
    
}