/**
 *	@author: Andrey Gavrikov (westbrook)
 *	@date: 2013-06-17 10:45:33 
 *	@description:
 *	    #801 - attach a file to specified Opportunity
 *
 *	@changes (latest first):
 *		Date			Author		Description
 *		----			------		-----------
 *		Dec 2015		P Goodey	Case 00089563 Searching for SCS files in SFDC - 
 *									Modified JSON System.Label.Document_Type_Map to set the name of the file on the project in text field for searching
 *		Mar 2015		P Goodey	WR 00073054 - TPS&D Workflow Phase 2 - 
 *									Change Filename of Attachment to Type | Project Reference | Company Name | VAS Product(from TPS&D Project) | Date / Time uploaded
 */
public without sharing class AttachToOpportunityController {

	private final ApexPages.StandardController stdCon;
	
	private Project__c project;
	private final Id projectId;
	public String userDefinedDocType {get; set;}

	public String documentType {get; set;}
	public List<SelectOption> documentTypeList {get; private set;}
	final Map<String, String> fieldByLabel = new Map<String, String>();

	// Case 00089563 Searching for SCS files in SFDC 
	final Map<String, String> fieldFilenameByLabel = new Map<String, String>();

	
	public Attachment attachment {
		get {
			if (attachment == null)
				attachment = new Attachment();
			return attachment;
		}
		set;
	}

	public AttachToOpportunityController (ApexPages.StandardController stdCon) {
		if (!Test.isRunningTest()) {
		  stdCon.addFields(new String[] {'Opportunity__c', 'Name','Project_Number__c'});
		}
		this.stdCon = stdCon;
		project = (Project__c)stdCon.getRecord();
        system.debug('Project Number: '+project.Project_Number__c);
		projectId = project.Id;
		if (null == projectId) {
			addError(System.Label.Project_Id_URL_parameter_is_required);
			return;
		}

		if (null == project.Opportunity__c) {
			addError(System.Label.Failed_to_identify_Opportunity_for_given_project_Id);
		}

		//load Document Type picklist values
		documentTypeList = new List<SelectOption>();
		documentTypeList.add(new SelectOption('', System.Label.NONE));

		/*
			{ 
			"Document_Type_Options" :[ 
			{"label": "TPS Discovery Writeup", "field":"TPS_Url__c"}, 
			{"label": "PID", "field":"PID_Url__c"}, 
			{"label": "SCS", "field":"SCS _Url__c"}, 
			{"label": "Inlife Handover Doc", "field":"Inlife_Url__c"} 
			]}
		 */
		 
		// Case 00089563 Searching for SCS files in SFDC 		 
		/*
			{ 
			"Document_Type_Options" :[ 
			{"label": "TPS Discovery Writeup", "field":"TPS_Url__c", "filename field":""}, 
			{"label": "PID", "field":"PID_Url__c", "filename field":""}, 
			{"label": "SCS", "field":"SCS _Url__c", "filename field":"SCS_Filename__c"}, 
			{"label": "Inlife Handover Doc", "field":"Inlife_Url__c", "filename field":""} 
			]}
		 */

		 
        Map<String, Object> documentTypeListMap = (Map<String, Object>)JSON.deserializeUntyped(System.Label.Document_Type_Map);
		List<Object> docTypeOptions = (List<Object>)documentTypeListMap.get('Document_Type_Options');

		for(Object optionObj : docTypeOptions) {
			Map<String, Object> oneOptionMap = (Map<String, Object>)optionObj;
			String label = (String) oneOptionMap.get('label');
			String fName = (String) oneOptionMap.get('field');
			fieldByLabel.put(label, fname);
            System.debug('## label=' + label);
            System.debug('## field=' + fName);
			documentTypeList.add(new SelectOption(label, label));

			// Case 00089563 Searching for SCS files in SFDC 		 
			String filenameField = (String) oneOptionMap.get('filename_field');
			fieldFilenameByLabel.put(label, filenameField);
            System.debug('## filename_field=' + filenameField);
			
		}

		
	}
	private static void addError(final String txt) {
		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, txt));
	}
	public Boolean getHasErrors() {
		return ApexPages.hasMessages(ApexPages.Severity.ERROR);
	}
	public Boolean getShowForm() {
		return null == project.Opportunity__c || null == projectId;
	}


	public PageReference attach() {
		if (null == attachment.body) {
			addError(System.Label.Please_Attach_a_document);
		}
		if (String.isBlank(documentType)) {
			addError(System.Label.Document_Type_Required);
		}
		if (getAllowUserDefinedDocType() && String.isBlank(userDefinedDocType)) {
			addError(System.Label.User_Defined_Document_Type_Required);
		}
		if (getHasErrors()) {
			return null;
		}
		attachment.ParentId = project.Opportunity__c;
		attachment.OwnerId = UserInfo.getUserId();
		Date today = System.today();

		String docTypeToUse = documentType;
		if (getAllowUserDefinedDocType() && !String.isBlank(userDefinedDocType)) {
			docTypeToUse = userDefinedDocType;
		}
		Integer extensionStart = attachment.Name.lastIndexOf('.');
		String extension = '';
		if (extensionStart > 0) {
			extension = attachment.Name.substring(extensionStart);
		}
		//attachment.Name = docTypeToUse + ' ' + project.Name + ' ' + today.day() + '/' + today.month() + '/' + today.year() + extension;
		// WR 00073054 - TPS&D Workflow Phase 2
		attachment.Name = getAttachmentFilename(today, docTypeToUse, extension);
		
		Savepoint sp = Database.setSavepoint();
		try {
			Database.insert(attachment);
			//delete old attachment of current document type
			String nameLike = documentType + ' ' + project.Project_Number__c + '%';
			final List<Attachment> oldAttachments = [select Id from Attachment where ParentId =: project.Opportunity__c and Name like: nameLike and Id <>: attachment.Id];
			Database.delete(oldAttachments);

			//add document URL to Project
			//https://c.cs13.content.force.com/servlet/servlet.FileDownload?file=00PW0000000ZUDt
			String url = System.URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=';
			project.put(fieldByLabel.get(documentType), url + attachment.Id);

			// Case 00089563 Searching for SCS files in SFDC 			
			String sFilenameField = fieldFilenameByLabel.get(documentType);
			System.debug('## sFilenameField=' + sFilenameField);
			System.debug('## attachment.Name=' + attachment.Name);
			if( sFilenameField != '' ){
				project.put(sFilenameField, attachment.Name);
			}
			
			Database.update(project);
		} catch (Exception e) {
			addError('Failed to save attachment. ' + e);
			Database.rollback(sp);
			return null;
		} finally {
			attachment = new Attachment(); 
		}

		return this.stdCon.cancel();
	}
	/**
	 * when Document Type = 'Other' is selected - give user an option to overwrite the file name
	 */
	public Boolean getAllowUserDefinedDocType() {
		System.debug('agX System.Label.Document_Type_Other == documentType ==' + (System.Label.Document_Type_Other == documentType));
		return System.Label.Document_Type_Other == documentType;

	}
	

   /*
    * @name         getAttachmentFilename
    * @description  returns the details required for setting the attachment filename
    *				File Type | Project Reference | Company Name | VAS Product(from TPS&D Project) | Date / Time uploaded 
	*				For example: SCS P004770 White Packaging Limited Data VPN - IPSEC 6/3/2014.xlsx
    * @author       P Goodey
    * @date         Mar 2015
    * @see			WR 00073054
    */
	private String getAttachmentFilename(Date ldtToday, String psDocTypeToUse, String psExtension){

		String lsFilename;
		
		// Retrieve the Company Name and VAS Product
		Project__c proj = [select Id,Project_Number__c, Opportunity__r.Account.Name, VAS_Product__c from Project__c where Id =: project.Id limit 1 ];
		
		// Set the filename string
		lsFilename = psDocTypeToUse + ' ' + project.Project_Number__c + ' ' + proj.Opportunity__r.Account.Name + ' ' + proj.VAS_Product__c + ' ' + ldtToday.day() + '/' + ldtToday.month() + '/' + ldtToday.year() + psExtension;
		
		// retrun the string
		return lsFilename;

	}	
	
	
	
	
}