@isTest
private class CS_Test_CustomAddOnLookup {
    
    private static cscfga__Product_Definition__c prodDefinition;
 
    private static Map<String, String> searchFieldsMap = new Map<String, String>();    
    private static String prodDefinitionID;
    private static Id[] excludeIds;
    private static Integer pageOffset;
    private static Integer pageLimit;
    
    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        RecordType recordType = [SELECT Id, Name from RecordType where Name = 'Shared Add-On'];
        
        // Service Plan 
        cspmb__Price_Item__c servicePlan = new cspmb__Price_Item__c ();
        servicePlan.Name =  'Tailored';
        insert servicePlan;
        
        //AddOn PriceItem 
        cspmb__Add_On_Price_Item__c addOn = new cspmb__Add_On_Price_Item__c();
        addOn.Name = 'Super Secure 2GB Starter';
        insert addOn;
        
        // addOn Association-
        cspmb__Price_Item_Add_On_Price_Item_Association__c association = new cspmb__Price_Item_Add_On_Price_Item_Association__c ();
        association.cspmb__Add_On_Price_Item__c = addOn.Id;
        association.cspmb__Price_Item__c = servicePlan.Id;
        insert association;
                
		searchFieldsMap.put('Service Plan Parent', servicePlan.Id);
      	searchFieldsMap.put('Type', 'Voice');
		searchFieldsMap.put('Exclude Type','Data,Voice');
		searchFieldsMap.put('Record Type','Shared Add On');
		searchFieldsMap.put('Disable Roaming','');
        
        // code added 2020-12-11 for increased code coverage in CS_CustomAddOnLookup class
        searchFieldsMap.put('Number of users','1001');
        
    }
    
     static testMethod void CustomAddOnLookupTest() {
     	createTestData();
     	Test.StartTest();
     	CS_CustomAddOnLookup customLookup = new CS_CustomAddOnLookup ();
        customLookup.getRequiredAttributes();
     	pageOffset = 1;
     	Object[] addOnsResult = customLookup.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
		System.assertNotEquals(null, addOnsResult);  	
     	Test.StopTest();
     }
    
}