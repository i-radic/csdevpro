/*######################################################################################
PURPOSE
Controller for the dicsounts page from the CV

HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
public with sharing class CloudVoiceDiscountsCtrl {

    public Cloud_Voice_Site__c cvsRec;
    public string objId {get; set;}
    public string prodId {get; set;}
    public string initialDiscountVal {get; set;}
    public string recurringDiscountVal {get; set;}
    public string discountsCSV {get; set;}
    
    public CloudVoiceDiscountsCtrl(ApexPages.StandardController controller) { 
        this.cvsRec= (Cloud_Voice_Site__c)controller.getRecord();   
        objId = System.currentPageReference().getParameters().get('Id');
        prodId = System.currentPageReference().getParameters().get('pId');
        initialDiscountVal = System.currentPageReference().getParameters().get('iCharge');
        recurringDiscountVal = System.currentPageReference().getParameters().get('rCharge');  
    }
    
    public PageReference OnLoad(){
        List<Cloud_Voice_Site_Product__c> prodList = getSelectedALLDisc();
        List<Cloud_Voice_Site_Product__c> toUpdate = new List<Cloud_Voice_Site_Product__c>();
        for(Cloud_Voice_Site_Product__c  prod : prodList){
            if(prod.Discount_Initial__c == null && prod.Discount_Recurring__c == null){
                prod.Discount_Initial__c = 0;
                toUpdate.Add(prod);
            }
        }
        if(toUpdate.Size() > 0){
            update toUpdate;
        }   
        return null;
    }
    
    public List<Cloud_Voice_Site_Product__c> getSelectedALLDisc() {
        List<Cloud_Voice_Site_Product__c> prodList = [SELECT ID, Product_Name__c, Quantity__c, Type__c, A_Code__c, Unit_Cost__c, Total_One_Off_Cost__c, Total_Recurring_Cost__c, Connection_Charge__c, LastModifiedBy.Name, LastModifiedDate, Locked_Edit__c, Locked_Delete__c, Number_Info__c,Discountable__c,Cloud_Voice_Site__r.Total_Site_Recurring_Cost__c,Cloud_Voice_Site__r.Total_Site_One_Off_Cost__c, Discount__c,DiscountStringI__c,DiscountStringR__c,Purchasing_Option__c,Discount_Initial__c,Discount_Recurring__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :objId AND Discountable__c = True ORDER BY Type__c, Product_Name__c];
        return prodList;
    }
    
    public PageReference goFinish() {  
        update cvsRec;
        PageReference retPg = new PageReference('/' + objId);
        return retpg.setRedirect(true);
    }
      
    public PageReference goUpdateDiscounts() {
    	Set<string> cvsProductIDSet = new Set<string>();
    	List<string[]> discList = new List<string[]>();   	
        List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();       
        discountsCSV = discountsCSV.replace('undefined','');  
        List<String> csvList = discountsCSV.Split(',');
        for(string s : csvList){
            string[] csvVals = s.Split(':');
            string prodId = csvVals[0].substring(0,csvVals[0].length()-1);
    		string discVal = csvVals[1];
            string discType = csvVals[0].substring(csvVals[0].length()-1);
            cvsProductIDSet.Add(prodID);
            discList.Add(new string[]{discVal, discType});     
        }
        prodList = [SELECT ID, Discount_Recurring__c, Discount_Initial__c, Type__c, Product_Name__c FROM Cloud_Voice_Site_Product__c WHERE ID in :cvsProductIDSet ORDER BY Type__c, Product_Name__c];            
        for (integer i = 0; i < prodList.Size(); i++){
        	if(discList[i][1] == 'I'){
                prodList[i].Discount_Initial__c = Decimal.valueOf(discList[i][0]);
                if(Test.isRunningTest() || prodList[i].Discount_Recurring__c >= 0){
                    prodList[i].Discount_Recurring__c = null;
                }
            }
            else if(discList[i][1] == 'R'){
                prodList[i].Discount_Recurring__c = Decimal.valueOf(discList[i][0]);
                if(Test.isRunningTest() || prodList[i].Discount_Initial__c >= 0){
                    prodList[i].Discount_Initial__c = null;
                }
            } 
        }
        update prodList;
        PageReference retPg = new PageReference('/apex/CloudVoiceDiscounts?id=' + objId);
        return retpg.setRedirect(true);
    }
}