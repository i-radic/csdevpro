public class HelperRollUpSummary{

    Public Static Set<Id> cvId = New Set<Id>();

    //Set variable to stop re-processing other triggeres
    public static boolean CVSiteStop= false;
    public static boolean getCVSiteStop(){
        return CVSiteStop;
    }    
    public static void setCVSiteStop(boolean b){
        CVSiteStop = b;
    }
    //set variable for alternative product
    public static boolean CValt= false;
    public static boolean getCValt(){
        return CValt;
    }    
    public static void setCValt(boolean b){
        CValt= b;
    }    
        
    public enum Method {COUNT, SUM, MIN, MAX, AVG} 
     
    private string sobjectParent, 
                   relationName, 
                   formulaParent, 
                   sobjectChild, 
                   parentfield, 
                   fieldChild,
                   criteria;

    public HelperRollUpSummary( string myformulaParent, string myCriteria, string mysobjectParent, string myrelationName,
                             string mysobjectChild, string myparentfield, string myfieldChild){
        sobjectParent = mysobjectParent;
        relationName = myrelationName;
        formulaParent = myformulaParent;
        sobjectChild = mysobjectChild;
        parentfield = myparentfield;
        fieldChild = myfieldChild;
        criteria= myCriteria;
    }
     
    public void Calculate(Method calculation, List<sobject> childList){
        set<Id> parentIdSet = new set<Id>();
        for(sobject sobj : childList)
        parentIdSet.add((Id) sobj.get(parentfield));
        string soqlParent = 'select id, (select ' + fieldChild + ' from ' + relationName + ' WHERE ' + criteria + ') from ' + sobjectParent + '';
        system.debug('###################soqlParent:' + soqlParent);
        List<sobject> parentList = Database.query(soqlParent);
        for(sobject parent : parentList){
            List<sobject> children = parent.getSObjects(relationName);
            if(children == null)
                children = new List<sobject>();
            Decimal counter = (mustSum(calculation))? 0 : null;
            if(calculation == Method.COUNT)
                counter = children.size();
            for(sobject child : children){
                Decimal value = (Decimal) child.get(fieldChild);
                if(mustSum(calculation) && value != null)
                    counter += value;
                else if(calculation == Method.MIN && (counter == null || value < counter))
                    counter = value;
                else if(calculation == Method.MAX && (counter == null || value > counter))
                    counter = value;
            }
            if(calculation == Method.AVG && children.size() > 0)
                counter = counter / children.size();
            parent.put(formulaParent, counter);
        }
        HelperRollUpSummary.setCVSiteStop(true);
        update parentList;
    }
     
    private boolean mustSum(Method calculation){
        return (calculation == Method.SUM || calculation == Method.AVG);
    }
     
}