public class NADServiceLibrary {
    public class CRMAddressList {
        public List<Address> addresses {get; set;}
    }
    
    public class Address {
        public string IdentifierId { get; set; }
        public string IdentifierName { get; set; }
        public string IdentifierValue { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string Name { get; set; }
        public string POBox { get; set; }
        public string BuildingNumber { get; set; }
        public string Street { get; set; }
        public string Locality { get; set; }
        public string DoubleDependentLocality { get; set; }
        public string PostCode { get; set; }
        public string Town { get; set; }
        public string SubBuilding { get; set; }
        public string ExchangeGroupCode { get; set; }
    }
    
    /*
    public class sfAddress{
        public string IdentifierId { get; set; }
        public string IdentifierName { get; set; }
        public string IdentifierValue { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string Name { get; set; }
        public string POBox { get; set; }
        public string BuildingNumber { get; set; }
        public string Street { get; set; }
        public string Locality { get; set; }
        public string DoubleDependentLocality { get; set; } 
        public string PostCode { get; set; }
        public string Town { get; set; }
        public string SubBuilding { get; set; }
        public string ExchangeGroupCode { get; set; }
    }
    
    public String createNADAddress(SalesforceCRMService.CRMAddress salesforceAddress){
        List<sfAddress> addressList = new List<sfAddress>();

        ManagePlace.ManagePlace_ProviderPort mPlace = new ManagePlace.ManagePlace_ProviderPort();

        ManagePlace.StandardHeaderBlock header = new ManagePlace.StandardHeaderBlock();

        ManagePlace.E2E e2e = new ManagePlace.E2E();
        e2e.E2EDATA = 'E2E';
        header.e2e = e2e;

        ManagePlace.ServiceState serviceState = new ManagePlace.ServiceState();
        
        serviceState.stateCode = 'OK';
        serviceState.errorCode = '';
        serviceState.errorText = '';
        serviceState.errorDesc = '';
        serviceState.errorTrace = '';
        serviceState.resendIndicator = false;
        serviceState.retriesRemaining = 2;
        serviceState.retryInterval = 5;
        header.serviceState = serviceState;
        
        ManagePlace.ServiceAddressing serviceAddressing = new ManagePlace.ServiceAddressing();
        serviceAddressing.from_x = 'http://sf.services.salesforce.com';
        ManagePlace.AddressReference addressReference = new ManagePlace.AddressReference();
        addressReference.address = 'http://capabilities.nat.bt.com/CMF';
        serviceAddressing.to = addressReference;
        ManagePlace.ContextItem contextItem = new ManagePlace.ContextItem();
        contextItem.contextName = 'Destination';
        contextItem.Value = 'ROBT'; 
        ManagePlace.ContextItem[] contextItemArray = new ManagePlace.ContextItem[]{ contextItem };
        
        //new lines - start
        ManagePlace.ContextItemList contextItemlist = new ManagePlace.ContextItemList();
        contextItemlist.contextItem = contextItemArray;
        addressReference.contextItemList = contextItemlist;
        
        // new lines - end
        
        addressReference.address = 'http://sf.services.salesforce.com';
        serviceAddressing.replyTo = addressReference;
        serviceAddressing.relatesTo = '';
        serviceAddressing.faultTo = addressReference;
        serviceAddressing.messageId = '54321';
        serviceAddressing.serviceName = 'http://capabilities.nat.bt.com/ManagePlace_Provider';
        serviceAddressing.action = 'http://capabilities.nat.bt.com/AddressMatch';
        header.serviceAddressing = serviceAddressing;

        ManagePlace.ServiceProperties serviceProperties = new ManagePlace.ServiceProperties();
        ManagePlace.MessageExpiry messageExpiry = new ManagePlace.MessageExpiry();
        messageExpiry.expiryTime = '';
        messageExpiry.expiryAction = '';
        serviceProperties.messageExpiry = messageExpiry;
        ManagePlace.MessageDelivery messageDelivery = new ManagePlace.MessageDelivery();
        messageDelivery.messagePersistence = '';
        messageDelivery.messageRetries = '';
        messageDelivery.messageRetryInterval = '';
        messageDelivery.messageQoS = '';
        serviceProperties.messageDelivery = messageDelivery;
        header.serviceProperties = serviceProperties;

        ManagePlace.ServiceSpecification serviceSpecification = new ManagePlace.ServiceSpecification();
        serviceSpecification.payloadFormat = '';
        serviceSpecification.version = '';
        serviceSpecification.revision = '';
        header.serviceSpecification = serviceSpecification;

        ManagePlace.ServiceSecurity serviceSecurity = new ManagePlace.ServiceSecurity();
        serviceSecurity.id = 'saasSalesforce';
        serviceSecurity.role = '';
        serviceSecurity.type_x = ''; 
        serviceSecurity.authenticationLevel = '';
        serviceSecurity.authenticationToken = 'Pa$$4w0rd';
        serviceSecurity.userEntitlements = '';
        serviceSecurity.tokenExpiry = '';
        serviceSecurity.callingApplication = '';
        serviceSecurity.callingApplicationCredentials = '';
        header.serviceSecurity = serviceSecurity;

        ManagePlace.CreateAddressRequest createAddressRequest = new ManagePlace.CreateAddressRequest();
        createAddressRequest.standardHeader = header;
        createAddressRequest.createAddressReq = new ManagePlace.CreateAddressReq();
        createAddressRequest.createAddressReq.identifier = '227015716';

        ManagePlace.Location location = new ManagePlace.Location();
        location.type_x = '';
        location.hazardCode = 999;
        location.hazardText = '';
        ManagePlace.Address address = new ManagePlace.Address();
        address.format = 'UKPAF';
        address.qualifier = '';
        address.qualityInd = '';
        
        ManagePlace.CountryAddressCoverageLevel country = new ManagePlace.CountryAddressCoverageLevel();
        country.name = salesforceAddress.Country;
        address.country = country;
        
        return null;
    }
    
    public String GetExchangeGroupCode(string nadId){
        ManagePlace.ManagePlace_ProviderPort mPlace = new ManagePlace.ManagePlace_ProviderPort();

        ManagePlace.StandardHeaderBlock header = new ManagePlace.StandardHeaderBlock();
        
        ManagePlace.E2E e2e = new ManagePlace.E2E();
        e2e.E2EDATA = 'E2E';
        header.e2e = e2e;

        ManagePlace.ServiceState serviceState = new ManagePlace.ServiceState();
        serviceState.stateCode = 'OK';
        serviceState.errorCode = '';
        serviceState.errorText = '';
        serviceState.errorDesc = '';
        serviceState.errorTrace = '';
        serviceState.resendIndicator = false;
        serviceState.retriesRemaining = 2;
        serviceState.retryInterval = 5;
        header.serviceState = serviceState;

        ManagePlace.ServiceAddressing serviceAddressing = new ManagePlace.ServiceAddressing();
        serviceAddressing.from_x = 'http://sf.services.salesforce.com';
        ManagePlace.AddressReference addressReference = new ManagePlace.AddressReference();
        addressReference.address = 'http://capabilities.nat.bt.com/CMF';
        serviceAddressing.to = addressReference;
        ManagePlace.ContextItem contextItem = new ManagePlace.ContextItem();
        contextItem.contextName = 'Destination';
        //contextItem.Value = 'ROBT';
        ManagePlace.ContextItem[] contextItemArray = new ManagePlace.ContextItem[]{contextItem};
        //addressReference.contextItemList = contextItemArray;

        addressReference.address = 'http://sf.services.salesforce.com';
        serviceAddressing.replyTo = addressReference;
        serviceAddressing.relatesTo = '';
        serviceAddressing.faultTo = addressReference;
        serviceAddressing.messageId = '54321';
        serviceAddressing.serviceName = 'http://capabilities.nat.bt.com/ManagePlace_Provider';
        serviceAddressing.action = 'http://capabilities.nat.bt.com/AddressMatch';
        header.serviceAddressing = serviceAddressing;

        ManagePlace.ServiceProperties serviceProperties = new ManagePlace.ServiceProperties();
        ManagePlace.MessageExpiry messageExpiry = new ManagePlace.MessageExpiry();
        messageExpiry.expiryTime = '';
        messageExpiry.expiryAction = '';
        serviceProperties.messageExpiry = messageExpiry;
        ManagePlace.MessageDelivery messageDelivery = new ManagePlace.MessageDelivery();
        messageDelivery.messagePersistence = '';
        messageDelivery.messageRetries = '';
        messageDelivery.messageRetryInterval = '';
        messageDelivery.messageQoS = '';
        serviceProperties.messageDelivery = messageDelivery;
        header.serviceProperties = serviceProperties;

        ManagePlace.ServiceSpecification serviceSpecification = new ManagePlace.ServiceSpecification();
        serviceSpecification.payloadFormat = '';
        serviceSpecification.version = '';
        serviceSpecification.revision = '';
        header.serviceSpecification = serviceSpecification;

        ManagePlace.ServiceSecurity serviceSecurity = new ManagePlace.ServiceSecurity();
        serviceSecurity.id = 'saasSalesforce';
        serviceSecurity.role = '';
        serviceSecurity.type_x = '';
        serviceSecurity.authenticationLevel = '';
        serviceSecurity.authenticationToken = 'Pa$$4w0rd';
        serviceSecurity.userEntitlements = '';
        serviceSecurity.tokenExpiry = '';
        serviceSecurity.callingApplication = '';
        serviceSecurity.callingApplicationCredentials = '';
        header.serviceSecurity = serviceSecurity;

        ManagePlace.GetAddressRequest getAddressRequest = new ManagePlace.GetAddressRequest();
        getAddressRequest.standardHeader = header;
        ManagePlace.GetAddressReq getAddressReq = new ManagePlace.GetAddressReq();
        getAddressReq.identifier = '522695198';
        getAddressReq.sourceType = 'NBG';
      
        return null;
    }
    
    public SalesforceCRMService.CRMAddressList addressMatch(string postCode){
        List<sfAddress> addressList = new List<sfAddress>();
        ManagePlace.ManagePlace_ProviderPort mPlace = new ManagePlace.ManagePlace_ProviderPort();
        
        ManagePlace.StandardHeaderBlock header = new ManagePlace.StandardHeaderBlock();
        
        ManagePlace.E2E e2e = new ManagePlace.E2E(); 
        e2e.E2EDATA = 'E2E';
        header.e2e = e2e;           
        
        ManagePlace.ServiceState serviceState = new ManagePlace.ServiceState();
        serviceState.stateCode = 'OK';
        serviceState.errorCode = '';
        serviceState.errorText = '';
        serviceState.errorDesc = '';
        serviceState.errorTrace = '';
        serviceState.resendIndicator = false;
        serviceState.retriesRemaining = 2;
        serviceState.retryInterval = 5; 
        header.serviceState = serviceState;
        
        ManagePlace.ServiceAddressing serviceAddressing = new ManagePlace.ServiceAddressing();
        serviceAddressing.from_x = 'http://sf.services.salesforce.com';
        ManagePlace.AddressReference addressReference = new ManagePlace.AddressReference();
        addressReference.address = 'http://capabilities.nat.bt.com/CMF';
        serviceAddressing.to = addressReference;
        ManagePlace.ContextItem contextItem = new ManagePlace.ContextItem();
        contextItem.contextName = 'Destination';  
        contextItem.Value = 'ROBT';
        contextItem.contextId = ''; 
        ManagePlace.ContextItem[] contextItemArray = new ManagePlace.ContextItem[]{contextItem};
        ManagePlace.ContextItemList contextItemList = new ManagePlace.ContextItemList();
        contextItemList.contextItem = contextItemArray;
        addressReference.contextItemList = contextItemList;
        
        
        addressReference.address = 'http://sf.services.salesforce.com';
        serviceAddressing.replyTo = addressReference;
        serviceAddressing.relatesTo = '';
        serviceAddressing.faultTo = addressReference;
        serviceAddressing.messageId = '54321';
        serviceAddressing.serviceName = 'http://capabilities.nat.bt.com/ManagePlace_Provider';
        serviceAddressing.action = 'http://capabilities.nat.bt.com/AddressMatch'; // Check url later while testing
        header.serviceAddressing = serviceAddressing;

        ManagePlace.ServiceProperties serviceProperties = new ManagePlace.ServiceProperties();
        ManagePlace.MessageExpiry messageExpiry = new ManagePlace.MessageExpiry();
        messageExpiry.expiryTime = '';
        messageExpiry.expiryAction = '';
        serviceProperties.messageExpiry = messageExpiry;
        ManagePlace.MessageDelivery messageDelivery = new ManagePlace.MessageDelivery();
        messageDelivery.messagePersistence = '';
        messageDelivery.messageRetries = '';
        messageDelivery.messageRetryInterval = '';
        messageDelivery.messageQoS = '';
        serviceProperties.messageDelivery = messageDelivery;
        header.serviceProperties = serviceProperties;

        ManagePlace.ServiceSpecification serviceSpecification = new ManagePlace.ServiceSpecification();
        serviceSpecification.payloadFormat = '';
        serviceSpecification.version = '';
        serviceSpecification.revision = '';
        header.serviceSpecification = serviceSpecification;

        ManagePlace.ServiceSecurity serviceSecurity = new ManagePlace.ServiceSecurity();
        serviceSecurity.id = 'saasSalesforce';
        serviceSecurity.role = '';
        serviceSecurity.type_x = '';
        serviceSecurity.authenticationLevel = '';
        serviceSecurity.authenticationToken = 'Pa$$4w0rd';
        serviceSecurity.userEntitlements = '';
        serviceSecurity.tokenExpiry = '';
        serviceSecurity.callingApplication = '';
        serviceSecurity.callingApplicationCredentials = '';
        header.serviceSecurity = serviceSecurity;
        
        // addr match request
        ManagePlace.AddressMatchRequest addressMatchRequest = new ManagePlace.AddressMatchRequest();
        addressMatchRequest.standardHeader = header;
        addressMatchRequest.addressMatchReq = new ManagePlace.AddressMatchReq();
        addressMatchRequest.addressMatchReq.identifier = '227015716'; // This will be acting as DUNS ID
        addressMatchRequest.addressMatchReq.searchType = 'Search';
        addressMatchRequest.addressMatchReq.sourceType = 'NBA'; 
        
        ManagePlace.Location location = new ManagePlace.Location(); 
        location.type_x = '';
        location.hazardCode = 999;
        location.hazardText = '';
        //location.ListOfTechnology = ;
        
        ManagePlace.Address address = new ManagePlace.Address();
        address.format = 'UKPAF';
        address.qualifier = ''; 
        address.qualityInd = '';
        
        ManagePlace.CountryAddressCoverageLevel countryAddressCoverageLevel = new ManagePlace.CountryAddressCoverageLevel();
        countryAddressCoverageLevel.name = 'UK';
        countryAddressCoverageLevel.iSOAlpha2Code = '';
        countryAddressCoverageLevel.iSOAlpha3Code = '';
        address.country = countryAddressCoverageLevel;
        
        ManagePlace.PostalAddress addressDetails = new ManagePlace.PostalAddress();
        addressDetails.city = '';
        addressDetails.postCode = postCode;
        addressDetails.building = '';
        addressDetails.SubStreet = '';
        addressDetails.Street = '';
        addressDetails.subLocality = '';
        addressDetails.countyStateProvince = '';
        addressDetails.stateCode = '';
        addressDetails.poBox = '';
        addressDetails.postInCode = '';
        addressDetails.postOutCode = '';
        addressDetails.postalOrganisation = '';
        addressDetails.BuildingNumber = '';
        addressDetails.SubBuilding = '';
        addressDetails.deliveryPoint = '';
        addressDetails.locality = '';
        addressDetails.subCountyStateProvince = ''; // NA for UK Address
        addressDetails.subPostCode = ''; // NA for UK Address
        addressDetails.addressLine1 = '';
        addressDetails.addressLine2 = '';
        addressDetails.addressLine3 = '';
        addressDetails.addressLine4 = '';
        addressDetails.addressLine5 = '';
        addressDetails.addressLine6 = '';
        addressDetails.addressLine7 = '';
        addressDetails.addressLine8 = '';
        
        
        ManagePlace.PAFLabelAddress pafLabelAddress = new ManagePlace.PAFLabelAddress();
        pafLabelAddress.bestAddressLine1 = '';
        pafLabelAddress.bestAddressLine2 = '';
        pafLabelAddress.bestAddressLine3 = '';
        pafLabelAddress.bestAddressLine4 = '';
        addressDetails.pafLabelAddress = pafLabelAddress;
        
        
        address.addressDetails = addressDetails;
        location.address = address;
        
        ManagePlace.AddressIdentifier addressIdentifier = new ManagePlace.AddressIdentifier();
        addressIdentifier.name = 'NADALK';
        addressIdentifier.value = '';
        addressIdentifier.id = '';
        location.addressIdentifier = addressIdentifier;
        
        
        ManagePlace.GeodeticCoordinate geodeticCoordinate = new ManagePlace.GeodeticCoordinate();
        geodeticCoordinate.latitude = '';
        geodeticCoordinate.longitude = '';
        geodeticCoordinate.ellipoidalHeight = '';
        
        ManagePlace.LocalGridCoordinateSystem localGridCoorDinateSystem = new ManagePlace.LocalGridCoordinateSystem();
        localGridCoorDinateSystem.x = 2;
        localGridCoorDinateSystem.y = 3;
        localGridCoorDinateSystem.z = 4;
        
        ManagePlace.OsNationalGridCoordinate osNationalGridCoordinate = new ManagePlace.OsNationalGridCoordinate();
        osNationalGridCoordinate.northing = '';
        osNationalGridCoordinate.easting = '';
        osNationalGridCoordinate.orthometricHeight = '';
        
        ManagePlace.Position position = new ManagePlace.Position();
        position.geodeticCoordinate = geodeticCoordinate;
        position.localGridCoorDinateSystem = localGridCoorDinateSystem;
        position.osNationalGridCoordinate = osNationalGridCoordinate;
        location.position = position;
        
        addressMatchRequest.addressMatchReq.location = location;
        
        
        
        mPlace.endpoint_x = 'https://www.wsd-rtref.robt.bt.co.uk:53080/BTR/dev/ManagePlaceProvider';
        mPlace.timeout_x = 60000;
        mPlace.clientCertName_x = 'SFServicesCRMgateway003';
        ManagePlace.AddressMatchResponse addressMatchResponse = mPlace.addressMatch(header,addressMatchRequest.addressMatchReq);
        
        return null;
    }
    
    private string CheckForNull(String value){
        if (value == '' || value == null){
            return '0';
        }
        else return value;
    }
    */
}