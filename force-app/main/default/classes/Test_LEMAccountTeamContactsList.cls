@isTest
private class Test_LEMAccountTeamContactsList {
	static testMethod void myUnitTest() {
						        
	    Profile p = [select id from profile where name='System Administrator'];
        String profileId = p.Id;
        
	    User uM = new User();
        uM.Username = '999999000@bt.com';
        uM.Ein__c = '999999000';
        uM.LastName = 'TestLastname';
        uM.FirstName = 'TestFirstname';
        uM.MobilePhone = '07918672032';
        uM.Phone = '02085878834';
        uM.Title='What i do';
        uM.OUC__c = 'DKW';
        uM.Manager_EIN__c = '123456789';
        uM.Email = 'no.reply@bt.com';
        uM.Alias = 'boatid01';
        uM.TIMEZONESIDKEY = 'Europe/London';
        uM.LOCALESIDKEY  = 'en_GB';
        uM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uM.PROFILEID = profileId;
        uM.LANGUAGELOCALEKEY = 'en_US';       
        uM.email = 'no.reply@bt.com';
        Database.SaveResult[] uMResult = Database.insert(new User [] {uM});

        User uGM = new User();
        uGM.Username = '999999001@bt.com';
        uGM.Ein__c = '999999001';
        uGM.LastName = 'TestLastname';
        uGM.FirstName = 'TestFirstname';
        uGM.MobilePhone = '07918672032';
        uGM.Phone = '02085878834';
        uGM.Title='What i do';
        uGM.OUC__c = 'DKW';
        uGM.Manager_EIN__c = '999999000';
        uGM.ManagerId = uMResult[0].id;
        uGM.Email = 'no.reply@bt.com';
        uGM.Alias = 'boatid01';
        uGM.TIMEZONESIDKEY = 'Europe/London';
        uGM.LOCALESIDKEY  = 'en_GB';
        uGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uGM.PROFILEID = profileId;
        uGM.LANGUAGELOCALEKEY = 'en_US';
        uGM.email = 'no.reply@bt.com';
        Database.SaveResult[] uGMResult = Database.insert(new User [] {uGM});
 
        User uDGM = new User();
        uDGM.Username = '999999002@bt.com';
        uDGM.Ein__c = '999999002';
        uDGM.LastName = 'TestLastname';
        uDGM.FirstName = 'TestFirstname';
        uDGM.MobilePhone = '07918672032';
        uDGM.Phone = '02085878834';
        uDGM.Title='What i do';
        uDGM.OUC__c = 'DKW';
        uDGM.Manager_EIN__c = '999999001';
        uDGM.ManagerId = uGMResult[0].id;
        uDGM.Email = 'no.reply@bt.com';
        uDGM.Alias = 'boatid01';
        uDGM.managerID = uGM.Id;
        uDGM.TIMEZONESIDKEY = 'Europe/London';
        uDGM.LOCALESIDKEY  = 'en_GB';
        uDGM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uDGM.PROFILEID = profileId;
        uDGM.LANGUAGELOCALEKEY = 'en_US';
        uDGM.email = 'no.reply@bt.com';
        Database.SaveResult[] uDGMResult = Database.insert(new User [] {uDGM});
        
        User uSM = new User();
        uSM.Username = '999999003@bt.com';
        uSM.Ein__c = '999999003';
        uSM.LastName = 'TestLastname';
        uSM.FirstName = 'TestFirstname';
        uSM.MobilePhone = '07918672032';
        uSM.Phone = '02085878834';
        uSM.Title='What i do';
        uSM.OUC__c = 'DKW';
        uSM.Manager_EIN__c = '999999002';
        uSM.ManagerId = uDGMResult[0].id;
        uSM.Email = 'no.reply@bt.com';
        uSM.Alias = 'boatid01';
        uSM.managerID = uDGM.Id;
        uSM.TIMEZONESIDKEY = 'Europe/London';
        uSM.LOCALESIDKEY  = 'en_GB';
        uSM.EMAILENCODINGKEY = 'ISO-8859-1';                               
        uSM.PROFILEID = profileId;
        uSM.LANGUAGELOCALEKEY = 'en_US';
        uSM.email = 'no.reply@bt.com';
        insert uSM;
        
	    BTLB_Master__c BM = new BTLB_Master__c();
	    BM.Name='TestSupport';
	    BM.BTLB_Name_ExtLink__c='TestSupport';
	    BM.Email_Signature__c='Regards,Test Support Team';
	    BM.Company_Name__c='Test Company';
	    BM.Company_Registration_No__c='12345';
	    BM.Company_Registered_Address__c='Address';
	    insert BM;
	    
	    LEM_Review__c lr = new LEM_Review__c();
		lr.iNET_FY_Target__c = 770943;
		lr.WLR_Lines_FY_Target__c = 717;
		lr.Switch_Volume_FY_Target__c = 311;
		lr.Switch_Value_FY_Target__c =1143661;
		lr.RD_Name__c = 'Chris Freeney';
		lr.PSM_Name__c = 'Malcolm Davey';
		lr.OwnerId =  uMResult[0].Id;
		lr.Name = 'BTLB Bath and Bristol';
		lr.Mobile_FY_Target__c = 689;
		lr.MD_Name__c = 'Chris Freeney';
		lr.Financial_Year__c = '2012/13';
		lr.Data_Networks_FY_Target__c = 2469525;
		lr.Calls_FY_Target__c = 812781;
		lr.Broadband_FY_Target__c = 660;
		lr.BTLB_Master_Object__c = BM.Id;
		insert lr;
	    
	    Account_Team_Contact_BTLB__c atc = new Account_Team_Contact_BTLB__c();
	    atc.Specialist_Role__c = 'PSM';
	    atc.Name__c = uMResult[0].Id;
	    atc.ExtId__c = '123';
	    atc.BTLB__c = BM.Id;
	    insert atc;	
	    
	    Account_Team_Contact_BTLB__c atc2 = new Account_Team_Contact_BTLB__c();
	    atc2.Specialist_Role__c = 'RD';
	    atc2.Name__c = uGMResult[0].Id;
	    atc2.ExtId__c = '124';
	    atc2.BTLB__c = BM.Id;
	    insert atc2;
	    
	    Test.startTest();
		PageReference pageRef = new PageReference('apex/LEMAccountTeamContactsList?id=' + lr.Id);
		Test.setCurrentPageReference(pageRef);
		ApexPages.Standardcontroller sc = New ApexPages.StandardController(lr); 
		LEMAccountTeamContactsList objList = new LEMAccountTeamContactsList(sc);
		List<Account_Team_Contact_BTLB__c> myList = objList.getLEMATContacts();
		objList.sortExpression = 'Specialist_Role__c';
		objList.ViewData();
		objList.setSortDirection('DESC');
		objList.getSortDirection();
		objList.ViewData();
		Id myId = objList.getLrId();
		objList.sortExpression = '';
		String MyDir = objList.getSortDirection();
		Test.stopTest();
	}
}