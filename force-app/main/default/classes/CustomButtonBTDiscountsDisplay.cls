/**
 * Used for the Product Basket new custom button 'Discounts'
 * Button opens the P&L VF page.
 *
 * @author Cloudsense
 */
global with sharing class CustomButtonBTDiscountsDisplay extends csbb.CustomButtonExt {

	public static final String errorMsg = '{"status":"error","title":"Error",'
									  + '"text":"' + Label.BT_NoConfigurationsInBasket + '"}';
	public String performAction (String basketId) {
		String action = '';
		List<cscfga__Product_Configuration__c> pcList = [
			SELECT Id, cscfga__Configuration_Status__c 
			FROM cscfga__Product_Configuration__c 
			WHERE cscfga__Product_Basket__c = :basketId
		];
		Boolean validBasket = true;
		system.debug('CustomButtonBTDiscountsDisplay.pcList = ' + pcList.size());
		for(cscfga__Product_Configuration__c pc : pcList){
			system.debug('CustomButtonBTDiscountsDisplay.pc.cscfga__Configuration_Status__c = ' + pc.cscfga__Configuration_Status__c + ' id = ' + pc.Id);
			if(pc.cscfga__Configuration_Status__c.toLowerCase() != 'valid')
				validBasket = false;
		}
		system.debug('CustomButtonBTDiscountsDisplay.validBasket = ' + validBasket);
		if(!validBasket)
			action = errorMsg;
		else
			action =  '{"status":"ok","redirectURL":"' + Label.BTSFOrganizationLink + '/apex/CS_CustomDiscounting?basketId=' + basketId + '"}';
		return action;
	}
}