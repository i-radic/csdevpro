/**
 *  @author: Andrey Gavrikov (westbrook)
 *  @date: 2013-05-08 13:10:25 
 *  @description:
 *      main handler for all Support_Request__c trigger events
 *  
 *  @author Gary Marsh (westbrook)
 *  @date 2013-06-04
 *  @desc Added new methods setAssignee and setForApproval. Removed
 *  addOpportunityMember on insert record as it goes to approval first
 *  
 *  @author Gary Marsh (westbrook)
 *  @date 2013-08-07
 *  @desc Added new method ApproveOnAssignment which approves a bid contract 
 *  record when it is assigned
 */
public without sharing class SupportRequestHandler {

    public static final String TEAM_ROLE_TPS = 'Technical Pre-Sales';
    public static final String TEAM_ROLE_SS = 'Solution Sales';
    public static final String TEAM_ROLE_BCS = 'Bid & Contract Support';
    
    public static final String RT_TPS = 'Technical_Presales_Support';
    public static final String RT_SS = 'Solution_Sales_Support';
    public static final String RT_BCSC = 'Bid_Contract_Support_Complex';
    public static final String RT_BCSS = 'Bid_Contract_support_Other';

    final Map<String, String> OPPORTUNITY_TEAM_ROLE_BY_RT = new Map<String, String>{
        RT_BCSC => TEAM_ROLE_BCS,
        RT_BCSS => TEAM_ROLE_BCS,
        RT_TPS => TEAM_ROLE_TPS,
        RT_SS => TEAM_ROLE_SS
    };

    private static SupportRequestHandler handler;
    
    private SupportRequestHandler () {
    }
    
    public static SupportRequestHandler getHandler() {
        if (null == handler) {
            handler = new SupportRequestHandler();
        }
        return handler;
    
    }
    
    public void beforeInsert () {
        setAssignee((Support_Request__c[])Trigger.new);
    }
    /*
    public void beforeUpdate () {
        //nothing currently
    }
    
    public void beforeDelete () {
        //nothing currently
    }
    */
    public void afterInsert () {
        //addOpportunityTeamMember((List<Support_Request__c>)Trigger.new, (Map<Id, Support_Request__c>)null);
        
        setForApproval((Support_Request__c[])Trigger.new);
        
        
        approveOnAssignment((Support_Request__c[])Trigger.new);
    }
    
    public void afterUpdate () {
        addOpportunityTeamMember((List<Support_Request__c>)Trigger.new, (Map<Id, Support_Request__c>)Trigger.oldMap);
        readjustApproval((Support_Request__c[])Trigger.new, (Map<Id, Support_Request__c>)Trigger.oldMap);
        approveOnAssignment((Support_Request__c[])Trigger.new, (Map<Id, Support_Request__c>)Trigger.oldMap);
        updateChatterForOpportunity((Support_Request__c[])Trigger.new, (Map<Id, Support_Request__c>)Trigger.oldMap);
    }
    public void afterDelete () {
        //nothing currently
        removeOpportunityTeamMember(Trigger.old);
    }
    
    public void afterUnDelete () {
        addOpportunityTeamMember((List<Support_Request__c>)Trigger.new, null);
    }

    /**
     * #840 - posts a chatter feed item to the related opportunity
     */
    private void updateChatterForOpportunity(Support_Request__c[] recs, Map<Id, Support_Request__c> oldMap) {
        if (TriggerUtils.hasSkipReason('SupportRequestHandler.updateChatterForOpportunity')) { return; }
        
        final Support_Request__c[] affectedRecs = new Support_Request__c[]{};
        
        //load Record Type map
        final Map<Id, String> rtMap = new Map<Id, String>();
        for (String rtName : OPPORTUNITY_TEAM_ROLE_BY_RT.keySet()) {
            RecordType rt = MockUtils.getRecordType('Support_Request__c', rtName);
            if (null != rt) {
                rtMap.put(rt.Id, rtName);
            }
        }
        
        for(Support_Request__c rec : recs) {
            String rtName = rtMap.get(rec.RecordTypeId);
            
            System.debug('~~rtName='+rtName);
            System.debug('~~oldAssignee='+oldMap.get(rec.Id).Assignee__c);
            System.debug('~~Assignee='+rec.Assignee__c);
            System.debug('~~Opp='+rec.Opportunity__c);
            
            if((rtName == RT_TPS || rtName == RT_SS) &&
               (oldMap.get(rec.Id).Assignee__c != rec.Assignee__c) &&
               rec.Opportunity__c != null) {
                    affectedRecs.add(rec);
            }
        }
        
        if(affectedRecs.isEmpty()) { return; }
        
        FeedItem[] feedItems = new FeedItem[]{};
        
        Support_Request__c[] allSupportRequests = new Support_Request__c[]{};
        allSupportRequests.addAll(recs);
        allSupportRequests.addAll(oldMap.values());
        Map<Id, User> assigneeMap = getAssigneeMap(allSupportRequests);
        
        for(Support_Request__c rec : affectedRecs) {
            String body = Label.Support_Request_Reassignment;
            body = body.replace('{!Name}', rec.Name);
            body = body.replace('{!Customer_Name}', getNullAsString(rec.Customer_Name__c));
            body = body.replace('{!Assignee_Old}', assigneeMap.get(oldMap.get(rec.Id).Assignee__c).Name);
            body = body.replace('{!Assignee}', assigneeMap.get(rec.Assignee__c).Name);
            
            feedItems.add(new FeedItem(
                ParentId = rec.Opportunity__c,
                Type = 'LinkPost',
                Title = 'Support Request reassignment',
                LinkUrl = '/' + rec.Id,
                Body = body
            ));
        }
        
        insert feedItems;
        TriggerUtils.setSkipReason('SupportRequestHandler.updateChatterForOpportunity', 
        'Feed items already added so skipping');
    }

    /**
     * #744 - generate OpportunityTeamMember records based on Support_Request__c
     */
    private void addOpportunityTeamMember(List<Support_Request__c> newRecs, Map<Id, Support_Request__c> oldRecsMap) {
        if (TriggerUtils.hasSkipReason('addOpportunityTeamMember')) {
            return;
        }
        //load Record Type map
        final Map<Id, String> rtMap = new Map<Id, String>();
        for (String rtName : OPPORTUNITY_TEAM_ROLE_BY_RT.keySet()) {
            RecordType rt = MockUtils.getRecordType('Support_Request__c', rtName);
            if (null != rt) {
                rtMap.put(rt.Id, rtName);
            }
        }

        final Set<Id> oppIds = getOpportunityIds(newRecs);
        
        //load existing OpportunityTeamMember-s
        final Map<Id, Map<Id, OpportunityTeamMember>> membersMap = loadExistingMembers(oppIds);

        final Set<Id> memberIdsToDelete = new Set<Id>();
        final List<OpportunityTeamMember> membersToInsert = new List<OpportunityTeamMember>();
        final List<Support_Request__c> requestsToUpdate = new List<Support_Request__c>();
        
        
        for (Support_Request__c obj : newRecs) {
            Map<Id, OpportunityTeamMember> members = membersMap.get(obj.Opportunity__c);
            if (Trigger.isUpdate && null != members && true != obj.Keep_existing_assignee_in_team__c) {
                //check if there was a change
                Support_Request__c oldObj = oldRecsMap.get(obj.Id);
                if ('Approved' == oldObj.Status__c && null != oldObj.Assignee__c &&
                        (oldObj.Assignee__c != obj.Assignee__c) || ('Approved' != obj.Status__c)) {
                    //user has changed, need to delete current OpportunityTeamMember record
                    OpportunityTeamMember m = members.get(oldObj.Assignee__c);
                    if (null != m) {
                        memberIdsToDelete.add(m.Id);
                    }
                }
            }
            if ('Approved' == obj.Status__c && null != obj.Assignee__c) {
                String rtName = rtMap.get(obj.RecordTypeId);
                String role = OPPORTUNITY_TEAM_ROLE_BY_RT.get(rtName);
                if (null != role) {
                    OpportunityTeamMember m = new OpportunityTeamMember();
                    m.OpportunityId = obj.Opportunity__c;
                    m.UserId = obj.Assignee__c;
                    m.TeamMemberRole = role;
                    membersToInsert.add(m);

                }
            }
            //reset Keep_existing_assignee_in_team__c back to OFF
            if (true == obj.Keep_existing_assignee_in_team__c) {
                requestsToUpdate.add(new Support_Request__c(Id = obj.Id, Keep_existing_assignee_in_team__c = false));
            }

        }
        Database.delete(new List<Id> (memberIdsToDelete));
        Database.insert(membersToInsert);
        TriggerUtils.setSkipReason('addOpportunityTeamMember', 'no need to execute addOpportunityTeamMember second time while resetting Keep_existing_assignee_in_team__c');
        Database.update(requestsToUpdate);
        TriggerUtils.removeSkipReason('addOpportunityTeamMember');

        /*
        //sort out sharing rules
        // get all of the team members' sharing records
        List<OpportunityShare> shares = [select Id, OpportunityAccessLevel, RowCause from OpportunityShare 
                                            where OpportunityId in :oppIds and RowCause = 'Team' and TeamMemberRole in: ];

        // set all team members access to read/write
        for (OpportunityShare share : shares) {
            share.OpportunityAccessLevel = 'Read Only';
        }

        update shares;
        */
    }

    /**
     * #744 - delete OpportunityTeamMember records when relevant Support_Request__c record is deleted
     */
    private void removeOpportunityTeamMember(List<Support_Request__c> oldRecs) {
        final Set<Id> oppIds = getOpportunityIds(oldRecs);
        
        final Map<Id, Map<Id, OpportunityTeamMember>> membersMap = loadExistingMembers(oppIds);

        final Set<Id> memberIdsToDelete = new Set<Id>();
        
        for (Support_Request__c obj : oldRecs) {
            Map<Id, OpportunityTeamMember> members = membersMap.get(obj.Opportunity__c);
            if (null != obj.Assignee__c && null != members) {
                OpportunityTeamMember m = members.get(obj.Assignee__c);
                if (null != m) {
                    memberIdsToDelete.add(m.Id);
                }
            }
        }
        Database.delete(new List<Id> (memberIdsToDelete));
    }
    
    /**
    * #781 - Set assignee based on record type
    * @param recs Records to set assignee for
    */
    private void setAssignee(Support_Request__c[] recs) {
        final Support_Request__c[] affectedRecs = new Support_Request__c[]{};
            
        //load Record Type map
        final Map<Id, String> rtMap = new Map<Id, String>();
        for (String rtName : OPPORTUNITY_TEAM_ROLE_BY_RT.keySet()) {
            RecordType rt = MockUtils.getRecordType('Support_Request__c', rtName);
            if (null != rt) {
                rtMap.put(rt.Id, rtName);
            }
        }
        
        // get opportunities
        final Set<Id> oppIds = getOpportunityIds(recs);
        final Map<Id, Opportunity> oppMap = loadOppOwners(oppIds);
        
        Set<Id> ownerIds = new Set<Id>();
        
        // separate the records that are affected
        for(Support_Request__c rec : recs) {
            String rtName = rtMap.get(rec.RecordTypeId);
            if((rtName == RT_TPS || rtName == RT_SS) &&
               null==rec.Assignee__c) {
                    affectedRecs.add(rec);
                    ownerIds.add(oppMap.get(rec.Opportunity__c).OwnerId);
            }
        }
        
        System.debug('~~OwnerIds:' + OwnerIds);
        
        final Map<Id, Map<String, UserTeamMember>> defaultTeamMembers = getDefaultTeamMembers(ownerIds);
        final Default_Assignee__c defaults = Default_Assignee__c.getInstance('default');
        final Map<Id, User> userMap = getUserDetailMap(ownerIds);
        final Map<Id, User> defaultAssigneeUserMap = defaults!=null ? getDefaultAssigneeUserMap(defaults) : null;
        
        // set assignees based on record type
        for(Support_Request__c rec : affectedRecs) {
            Id oppOwner = oppMap.get(rec.Opportunity__c).OwnerId;
            String rtName = rtMap.get(rec.RecordTypeId);
                    
            // if rt is technical presales support
            if(rtName == RT_TPS) {
                if(defaultTeamMembers.containsKey(oppOwner) && 
                defaultTeamMembers.get(oppOwner).containsKey(TEAM_ROLE_TPS)) {
                    
                    UserTeamMember utm = defaultTeamMembers.get(oppOwner).get(TEAM_ROLE_TPS);
                    if(null!=utm.User.DelegatedApproverId) {
                        rec.Assignee__c = utm.User.DelegatedApproverId;
                        rec.Delegated_Assignee__c = true;
                    } else {
                        rec.Assignee__c = utm.UserId;
                    }   
                    continue;
                }
                if(null!=defaults) {
                    User defaultAssigneeUser = defaultAssigneeUserMap.get(defaults.Technical_Pre_Sales__c);
                            
                    if(null!=defaultAssigneeUser && null!=defaultAssigneeUser.DelegatedApproverId) {
                        rec.Assignee__c = defaultAssigneeUser.DelegatedApproverId;
                        rec.Delegated_Assignee__c = true;
                    } else {
                        rec.Assignee__c = defaults.Technical_Pre_Sales__c;
                    }
                    continue;
                }
            }
            
            // if rt is solution sales support
            if(rtName == RT_SS) {
                if(defaultTeamMembers.containsKey(oppOwner) && 
                defaultTeamMembers.get(oppOwner).containsKey(TEAM_ROLE_SS)) {
                    
                    UserTeamMember utm = defaultTeamMembers.get(oppOwner).get(TEAM_ROLE_SS);
                    if(null!=utm.User.DelegatedApproverId) {
                        rec.Assignee__c = utm.User.DelegatedApproverId;
                        rec.Delegated_Assignee__c = true;
                    } else {
                        rec.Assignee__c = utm.UserId;
                    }   
                    continue;
                }
                if(null!=defaults) {
                    User defaultAssigneeUser = defaultAssigneeUserMap.get(defaults.Solution_Sales__c);
                            
                    if(null!=defaultAssigneeUser && null!=defaultAssigneeUser.DelegatedApproverId) {
                        rec.Assignee__c = defaultAssigneeUser.DelegatedApproverId;
                        rec.Delegated_Assignee__c = true;
                    } else {
                        rec.Assignee__c = defaults.Solution_Sales__c;
                    }
                    continue;
                }
            }
            
            // add an error if it ever gets to this stage, this is when the
            // record does not have an assignee and it cannot be resolved
            // to any sort of default
            rec.addError(System.Label.Assignee_Unresolved);
        }
    }
    
    /**
    * #781 - Set for approval
    * @param recs Records to set approval for
    * @remarks Assignee is handled in the before insert trigger and will error
    * there before it gets to this method
    */
    private void setForApproval(Support_Request__c[] recs) {
        Approval.ProcessRequest[] processRequests = new Approval.ProcessSubmitRequest[]{};  
        
        for(Support_Request__c rec : recs) {
            if(!rec.Is_Cloned__c && rec.Request_Type__c !='Surgery - Internal Only'){ //EV: changed for cloned SR's
                processRequests.add(createProcessRequest(rec.Id));
            }
        }
        if(processRequests.size()>0)
        	Approval.process(processRequests);
    }
        
    /**
     * #781 - Readjust approval
     * @param recs Records to check for assignee changes
     * @param oldMap old record map to check for assignee changes
     */
    private void readjustApproval(Support_Request__c[] recs, Map<Id, Support_Request__c> oldMap) {
        if (TriggerUtils.hasSkipReason('SupportRequestHandler.readjustApproval')) { return; }
        
        final Support_Request__c[] affectedRecs = new Support_Request__c[]{};
            
        //load Record Type map
        final Map<Id, String> rtMap = new Map<Id, String>();
        for (String rtName : OPPORTUNITY_TEAM_ROLE_BY_RT.keySet()) {
            RecordType rt = MockUtils.getRecordType('Support_Request__c', rtName);
            if (null != rt) {
                rtMap.put(rt.Id, rtName);
            }
        }
        
        // separate the records that are affected
        for(Support_Request__c rec : recs) {
            String rtName = rtMap.get(rec.RecordTypeId);
            if((rtName == RT_TPS || rtName == RT_SS) &&
               oldMap.get(rec.Id).Assignee__c != rec.Assignee__c) {
                    affectedRecs.add(rec);
            }
        }
        
        if(affectedRecs.isEmpty()) { return; }
                
        Approval.ProcessRequest[] processWorkitemRequests= new Approval.ProcessWorkitemRequest[]{};
        Approval.ProcessRequest[] processSubmitRequests = new Approval.ProcessSubmitRequest[]{};
        Map<Id, ProcessInstanceWorkitem> processInstanceWorkitemMap = getProcessInstanceWorkItemMap(affectedRecs); 
        ProcessInstanceWorkitem[] updates = new ProcessInstanceWorkitem[]{};
                
        for(Support_Request__c rec : affectedRecs) {
            if(null==rec.Assignee__c) { continue; }
            
            ProcessInstanceWorkitem piw = processInstanceWorkitemMap.get(rec.Id);
            if(null!=piw) {
                piw.ActorId = rec.Assignee__c;
                updates.add(piw);
            }
        }
            
        update updates;
        TriggerUtils.setSkipReason('SupportRequestHandler.readjustApproval', 'Approval process already readjusted.');
    }
    
    /**
     * #838 - Approve on Assignment
     * @param recs Records to set approval for
     * @remark Used on after insert, checks to see if assignee has been populated
     */
    private void approveOnAssignment(Support_Request__c[] recs) {
        final Support_Request__c[] affectedRecs = new Support_Request__c[]{};
            
        //load Record Type map
        final Map<Id, String> rtMap = new Map<Id, String>();
        for (String rtName : OPPORTUNITY_TEAM_ROLE_BY_RT.keySet()) {
            RecordType rt = MockUtils.getRecordType('Support_Request__c', rtName);
            if (null != rt) {
                rtMap.put(rt.Id, rtName);
            }
        }
        
        // separate the records that are affected
        for(Support_Request__c rec : recs) {
            String rtName = rtMap.get(rec.RecordTypeId);
            if((rtName == RT_BCSS || rtName == RT_BCSC) &&
               null!=rec.Assignee__c) {
                    affectedRecs.add(rec);
            }
        }
        
        // if there are no affected records, exit method
        if(affectedRecs.isEmpty()) { return; }
        
        approveSupportRequests(affectedRecs);
    }
        
    /**
     * #838 - Approve on Assignment
     * @param recs Records to set approval for
     * @param oldMap Map of previous record values
     * @remark Used on after update, checks to see if assignee has been populated
     * and if the record previously had no assignee
     */
    private void approveOnAssignment(Support_Request__c[] recs, Map<Id, Support_Request__c> oldMap) {
        final Support_Request__c[] affectedRecs = new Support_Request__c[]{};
            
        //load Record Type map
        final Map<Id, String> rtMap = new Map<Id, String>();
        for (String rtName : OPPORTUNITY_TEAM_ROLE_BY_RT.keySet()) {
            RecordType rt = MockUtils.getRecordType('Support_Request__c', rtName);
            if (null != rt) {
                rtMap.put(rt.Id, rtName);
            }
        }
        
        // separate the records that are affected
        for(Support_Request__c rec : recs) {
            String rtName = rtMap.get(rec.RecordTypeId);
            if((rtName == RT_BCSS || rtName == RT_BCSC) &&
               null!=rec.Assignee__c && null==oldMap.get(rec.Id).Assignee__c) {
                    affectedRecs.add(rec);
            }
        }
        
        // if there are no affected records, exit method
        if(affectedRecs.isEmpty()) { return; }
        
        approveSupportRequests(affectedRecs);
    }
    
    /**
     * Approves support requests if they are in the pending state
     */
    private static void approveSupportRequests (Support_Request__c[] recs) {
        Map<Id, ProcessInstanceWorkitem> processInstanceWorkitemMap = getProcessInstanceWorkItemMap(recs);
        Approval.ProcessRequest[] processRequests = new Approval.ProcessRequest[]{};
        
        // find all pending approvals and create new process requests
        for(Support_Request__c rec : recs) {
            ProcessInstanceWorkitem piwi = processInstanceWorkitemMap.get(rec.Id);
            if(null!=piwi) {
                processRequests.add(createProcessWorkItemRequest(piwi.Id, 'Approve'));
            }
        }
        
        // process approved requests
        Approval.process(processRequests);
    }
        
    private static Approval.ProcessRequest createProcessRequest(Id objId) {
        Approval.ProcessSubmitRequest psr = new Approval.ProcessSubmitRequest();
        psr.setObjectId(objId);
        return psr;
    }
        
    private static Approval.ProcessRequest createProcessWorkItemRequest(Id objId, String action) {
        Approval.ProcessWorkItemRequest pwir = new Approval.ProcessWorkItemRequest();
        pwir.setWorkItemId(objId);
        pwir.setAction(action);
        return pwir;
    }
    
    private static Map<Id, ProcessInstanceWorkitem> getProcessInstanceWorkitemMap(
    final List<Support_Request__c> recs) {
        Map<Id, ProcessInstanceWorkitem> processInstanceWorkitemMap = new Map<Id, ProcessInstanceWorkitem>();
            
        for(ProcessInstanceWorkitem piw : [select Id, ProcessInstance.TargetObjectId, ActorId from ProcessInstanceWorkItem
            where ProcessInstance.TargetObjectId in :recs and ProcessInstance.Status = 'Pending']) {
                Id targetObjectId = piw.ProcessInstance.TargetObjectId;
                if(!processInstanceWorkitemMap.containsKey(targetObjectId) || 
                (processInstanceWorkitemMap.containsKey(targetObjectId) && piw.ActorId==UserInfo.getUserId())) {
                    processInstanceWorkitemMap.put(targetObjectId, piw);
                }
        }
            
        return processInstanceWorkitemMap;
    }
    
    private static Map<Id, Map<String, UserTeamMember>> getDefaultTeamMembers(final Set<Id> ownerIds) {
        // ownerId -> Map<TeamMemberRole, UserTeamMember>
        final Map<Id, Map<String, UserTeamMember>> membersMap = new Map<Id, Map<String, UserTeamMember>>();
        
        for(UserTeamMember m : [select Id, OwnerId, TeamMemberRole, 
        UserId, User.DelegatedApproverId from UserTeamMember where OwnerId in :ownerIds]) {
            System.debug(m);
            
            Map<String, UserTeamMember> members = membersMap.get(m.OwnerId);
            if(null==members) {
                members = new Map<String, UserTeamMember>();
                membersMap.put(m.OwnerId, members);
            }
            members.put(m.TeamMemberRole, m);
        }
        
        System.debug('~~MembersMap:' + membersMap);
        return membersMap;
    }

    private static Set<Id> getOpportunityIds(List<Support_Request__c> recs) {
        final Set<Id> oppIds = new Set<Id>();
        
        for (Support_Request__c obj : recs) {
            oppIds.add(obj.Opportunity__c);
        }
        return oppIds;
    }
    
    private static Map<Id, Opportunity> loadOppOwners(final Set<Id> oppIds) {
        return new Map<Id, Opportunity>([select Id, OwnerId from Opportunity where Id in :oppIds]);
    }
    
    private static Map<Id, User> getDefaultAssigneeUserMap(final Default_Assignee__c defaults) {
        final Map<Id, User> userMap = new Map<Id, User>();
        
        User[] defaultUsers = [select Id, DelegatedApproverId from User where Id = :defaults.Technical_Pre_Sales__c
        or Id = :defaults.Solution_Sales__c];
        
        for(User u : defaultUsers) {
            if(u.Id==defaults.Technical_Pre_Sales__c) {
                userMap.put(defaults.Technical_Pre_Sales__c, u);
                continue;
            }
            if(u.Id==defaults.Solution_Sales__c) {
                userMap.put(defaults.Solution_Sales__c, u);
                continue;
            }
        }
        
        return userMap;
    }

    private static Map<Id, Map<Id, OpportunityTeamMember>> loadExistingMembers(final Set<Id> oppIds) {
        final Map<Id, Map<Id, OpportunityTeamMember>> membersMap = new Map<Id, Map<Id, OpportunityTeamMember>>();//opp.Id -> Map(User.Id -> Member)
        for (OpportunityTeamMember m : [select Id, OpportunityId, OpportunityAccessLevel, TeamMemberRole, UserId from OpportunityTeamMember where OpportunityId in :oppIds ]) {
            Map<Id, OpportunityTeamMember> members = membersMap.get(m.OpportunityId);
            if (null == members) {
                members = new Map<Id, OpportunityTeamMember>();
                membersMap.put(m.OpportunityId, members);
            }
            members.put(m.UserId, m);
        }
        return membersMap;
    }
    
    private static Map<Id, User> getUserDetailMap(final Set<Id> userIds) {
        return new Map<Id, User>([select Id, DelegatedApproverId from User where Id in :userIds]);
    }
    
    private static Map<Id, User> getAssigneeMap(Support_Request__c[] recs) {
        Set<Id> assigneeIds = new Set<Id>();
        for(Support_Request__c rec : recs) { assigneeIds.add(rec.Assignee__c); }
        return new Map<Id, User>([select Id, Name from User where Id in :assigneeIds]);
    }
    
    private String getNullAsString(String s) {
        return s == null ? 'null' : s;
    }
}