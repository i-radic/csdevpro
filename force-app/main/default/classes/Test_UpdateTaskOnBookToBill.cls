@isTest
private class Test_UpdateTaskOnBookToBill {
    static testMethod void myUnitTest1() {
    
    Map<string,string> RCMap = new Map<string,string>();  
    Map<String,String> MapProfiles=new Map<String,String>();
    Map<String,String> MapSSProfiles=new Map<String,String>();
    List<Profile> SS;
    
    User B2BUser1,B2BUser2;
    Profile B2BProfile1 = [select id from profile where name='Field: Standard User'];
    Profile B2BProfile2 = [select id from profile where name='Field: BooktoBill Sales Manager'];
    
        B2BUser1= new User(alias = 'B2B1', email='B2B13cr@bt.com', 
        emailencodingkey='UTF-8', lastname='Testing B2B1', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = B2BProfile1.Id, timezonesidkey='Europe/London', username='B2BProfile131cr@bt.com',
        EIN__c='B2B3cr');
        insert B2BUser1;
        
        B2BUser2= new User(alias = 'B2B2', email='B2B23cr@bt.com', 
        emailencodingkey='UTF-8', lastname='Testing B2B2', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = B2BProfile2.Id, timezonesidkey='Europe/London', username='B2BProfile1312cr@bt.com',
        EIN__c='B2B32cr');
        insert B2BUser2;
      test.startTest();
    Opportunity o=new Opportunity();
    o.Name='Book2Bill';
    o.StageName='Created';
    o.ownerid=B2BUser1.id;
    o.CloseDate = system.today();
    insert o;
    
      
    BookToBill__c b=new BookToBill__c();
    b.SE_SM_Feedback__c='Test1';
    b.FeedbackNotes__c='Test2';
    b.Measurement_Status__c='Feedback Required';
    b.Revenue_Assurance_Status__c='Audit';
    String RevenueAssuranceICTId='01220000000AGRn';
    b.RecordTypeId=RevenueAssuranceICTId;//[Select Id from RecordType where DeveloperName=:'Revenue_Assurance_ICT'].Id;
    b.Opportunity__c=o.Id;
    insert b;
    test.stopTest();
   Task t=new Task();
   
    t.whatId=b.Id;
    t.status='Audit';
    t.priority='Normal';
    t.Description='Test 1';
    String Book2BllTaskId='01220000000AGRq';
    t.recordtypeid=Book2BllTaskId;//lect Id from RecordType where DeveloperName='Book2Bill_Task'].Id;
   
   insert T;
      
    
    BookToBill__c b1=new BookToBill__c();
    b1.FeedbackNotes__c='Test4';
    b1.Measurement_Status__c='Feedback Required';
    b1.Opportunity__c=o.Id;
    //insert b1;
    
 /* system.runas(B2BUser1){
  
  t.Description='Test 4546';
   Update t;
  }*/

    }
   
 }