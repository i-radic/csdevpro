public class qbLineCheckerCTRL {

public qbLineCheckerCTRL(qbLineCheckerCTRL controller) { }
public qbLineCheckerCTRL() {}
public qbLineCheckerCTRL(ApexPages.StandardController controller) {}

public HttpResponse httpRespTest {get; set;} 

public List<SelectOption> selOptions {get;set;}

public String siteId = System.currentPageReference().getParameters().get('id');    

public String lineChange = System.currentPageReference().getParameters().get('line');    
public String pcChange = System.currentPageReference().getParameters().get('pc');      
public String nameNumChange = System.currentPageReference().getParameters().get('namenum');
public String fullAdd = System.currentPageReference().getParameters().get('full');     
public String checkType = System.currentPageReference().getParameters().get('checkType');     

public String multiAddress {get; set;} 
public String jmAddress = '';
public String jmSubPrem = '';
public String jmPrem = '';
public String jmSt = '';
public String jmLoc = '';
public String jmTown = '';
public String jmDist = '';
public String jmPC {get; set;}
public String jmNum {get; set;}

public Boolean showAddresses{get; set;}

public String lineCheck(string checkNumber, string chkType){
    //setup wholesale checker details
    String uname = 'btbsfdc';
    String pword = 'Btwhs1';
    String sysVer = '39';

    String telNum = checkNumber;
    String pc = ''; 
    String nameNum = ''; 
    String epURL = Null;
    showAddresses = False; 

    String MAX = Null; String ADSL2 = Null; String WBC = Null; String FTTC = Null; String FTTP = Null; String FOD = Null;
    String dlMAX = Null; String dlADSL2 = Null; String dlWBC = Null; String dlFTTC = Null; String dlFTTP = Null; String dlFOD = Null;
    
    Http h = new Http();
    HttpRequest req = new HttpRequest();
    
    if(chkType == 'tel'){
        req.setEndpoint('https://www.dslchecker.bt.com/adsl/adslchecker_xml.telno?username='+uname+'&password='+pword+'&version='+sysVer+'&input='+telNum);
    }
    if(chkType == 'pc'){
        pc = telNum.replace(' ','%20');
        if(nameNumChange != null){
            nameNum = nameNumChange.replace(' ','%20');
        }
        req.setEndpoint('https://www.dslchecker.bt.com/adsl/adslchecker_xml.address?username='+uname+'&password='+pword+'&version='+sysVer+'&ThoroughfareNumber='+nameNum+'&PremiseName='+nameNum+'&PostCode='+pc);
        telNum = '';
    }

    if(chkType == 'full'){
        showAddresses = False; 
        String[] lAddress = telnum.Split('~');
        jmSubPrem = lAddress[0];
        jmPrem = lAddress[1];
        jmNum = lAddress[2];
        jmSt = lAddress[3];
        jmLoc = lAddress[4];
        jmTown = lAddress[5];
        jmPC = lAddress[6];
        jmDist = lAddress[7];

        epURL = 'SubPremises='+jmSubPrem+'&PremiseName='+jmPrem+'&ThoroughfareNumber='+jmNum+'&ThoroughfareName='+jmSt+'&Locality='+jmLoc+'&Posttown='+jmTown+'&Postcode='+jmPC+'&DistrictID='+jmDist;

        epURL = epURL.replace(' ','%20');
        req.setEndpoint('https://www.dslchecker.bt.com/adsl/adslchecker_xml.address?username='+uname+'&password='+pword+'&version='+sysVer+'&'+epURL);
        telNum = '';pc = jmPC;nameNum = jmNum;
    }
    req.setMethod('GET');
    req.setTimeout(120000);

    String errText = Null;
    String addPass = Null;
    HttpResponse res;

    if(!Test.isRunningTest()){
        for (Integer i = 1; i < 10; i++) {
            try {
                res = h.send(req);
                i = 25;
            } catch (System.CalloutException e) {
                System.debug('JMM error: ' + e + ' retry: '+ i);
            } 
        }   
    }
    else{
        res = httpRespTest;
    }

    String chkError= '';
    String fullXML = '';

    if(res == null){
        errText = 'No response from server, please try again.';
        fullXML = '<timeoutError>True</timeoutError>';

    }else{
        Dom.Document doc = res.getBodyDocument();   
        Dom.XMLNode access = doc.getRootElement();
        //return error messages
        chkError= access.getChildElement('ERRORID', null).getText();
        if(chkType == 'tel'){
            if(chkError == '1'){errText = 'Telephone number not found in the BT database or the number belongs to a LLU operator';
            }else if(chkError == '2'){errText = 'Postcode not found in database';
            }else if(chkError == '3'){errText = 'Telephone number is invalid. Telephone numbers must be 10-11 characters in length and start with either 01 or 02';
            }else if(chkError == '4'){errText = 'Postcode is invalid. Postcodes must be between 5 and 7 characters in length';
            }else if(chkError == '5'){errText = 'Telephone Number Exported to an Other Licensed Operator therefore we cannot qualify this line';
            }else if(chkError == '6'){errText = 'Telephone Number Ceased, therefore the telephone number no longer exists';
            }else if(chkError == '7'){errText = 'Error processing request. Please try again later';
            }else if(chkError == '8'){errText = 'Authentication failed. Username and password combination incorrect';
            }else if(chkError == '9'){errText = 'Not executed via HTTPS';
            }else if(chkError == '99'){errText = 'Description of MAC Validation Request Failure ';
            }
        }
        if(chkType == 'pc' || chkType == 'full'){
            if(chkError == '1'){errText = 'Address Not Found (Unmatched Address)';
            }else if(chkError == '2'){errText = 'Too Many Addresses Found';
            }else if(chkError == '3'){errText = 'Qualification not available for this address';
            }else if(chkError == '4'){errText = 'Mandatory Postcode field not entered';
            }else if(chkError == '5'){errText = 'Mandatory Thoroughfare Number or Premise Name not entered';
            }else if(chkError == '6'){errText = 'Insufficient Address Elements';
            }else if(chkError == '7'){errText = 'Conflict between Locality, Posttown and Postcode';
            }else if(chkError == '8'){errText = 'Posttown or Locality not unique';
            }else if(chkError == '9'){errText = 'An Error Has Occurred';
            }else if(chkError == '10'){errText = 'Authentication failed. Username and password combination incorrect';
            }else if(chkError == '11'){errText = 'Not executed via HTTPS. The XML version of the checker must be sent over HTTPS as it returns customer information';
            }else if(chkError == '12'){errText = 'Incomplete information available';
            }else if(chkError == '13'){errText = 'The address does not map to Gold NAD key, please proceed with postcode qualification';
            }else if(chkError == '14'){errText = 'Invalid NAD Key Format';
            }
        }    
        //address list
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('','-- Select Correct Address --'));

        for(Dom.XMLNode child : access.getChildElements()) {
            for(Dom.XMLNode c1: child.getChildElements()) {
                if(child.getName() == 'ADDRESS_DETAILS' && c1.getName() =='ADDRESS_MATCHED' && c1.getText().substring(1,6) == 'FALSE' && chkError =='0' ){
                showAddresses = True;
                }
                for(Dom.XMLNode c2: c1.getChildElements()) {
                    for(Dom.XMLNode c3: c2.getChildElements()) {    
                        jmAddress = '';
                        if(c3.getName() =='SUBPREMISES'){jmSubPrem = c3.getText();}
                        if(c3.getName() =='PREMISENAME'){jmPrem = c3.getText();}
                        if(c3.getName() =='THOROUGHFARENUMBER'){jmNum = c3.getText();}
                        if(c3.getName() =='THOROUGHFARENAME'){jmSt = c3.getText();}
                        if(c3.getName() =='LOCALITY'){jmLoc = c3.getText();}
                        if(c3.getName() =='POSTTOWN'){jmTown = c3.getText();}
                        if(c3.getName() =='POSTCODE'){jmPC = c3.getText();}
                        if(c3.getName() =='DISTRICTID'){jmDist = c3.getText();}
                        if(jmSubPrem !=''){jmAddress=jmSubPrem+' ';}
                        if(jmPrem !=''){jmAddress=jmAddress+jmPrem+' ';}
                        if(jmNum !=''){jmAddress=jmAddress+jmNum+' ';}
                        if(jmSt !=''){jmAddress=jmAddress+jmSt+' ';}
                        if(jmLoc !=''){jmAddress=jmAddress+jmLoc+' ';}
                        if(jmTown !=''){jmAddress=jmAddress+jmTown+' ';}
                        if(jmPC !=''){jmAddress=jmAddress+jmPC+' ';}
                        //if(jmDist !=''){jmAddress=jmAddress+jmDist+' ';}
                    }
                    selOptions.add(new SelectOption(jmSubPrem+'~'+jmPrem+'~'+jmNum+'~'+jmSt+'~'+jmLoc+'~'+jmTown+'~'+jmPC+'~'+jmDist, jmAddress));
                    selOptions.sort();
                }            
            }
        }

        if(chkError == '0'){
            String retText = Null;
            MAX = 'Unavailable';ADSL2= 'Unavailable';WBC= 'Unavailable';FTTC= 'Unavailable';FTTP= 'Unavailable';FOD= 'Unavailable';
            dlMAX = '-';dlADSL2= '-';dlWBC= '-';dlFTTC= '-';dlFTTP= '-';dlFOD= '-';
            for(Dom.XMLNode child : access.getChildElements()) {
                //availabiity
                for(Dom.XMLNode c1: child.getChildElements()) {
                    fullXML += '<'+child.getName()+'_'+c1.getName()+'>'+c1.getText()+'</'+child.getName()+'_'+c1.getName()+'>';
                    if(child.getName() == 'MAX' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){MAX = 'Available';}
                    if(child.getName() == 'ADSL2' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){ADSL2= 'Available';}
                    if(child.getName() == 'WBC' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){WBC= 'Available';}
                    if(child.getName() == 'WBCFTTC' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){FTTC= 'Available';}
                    if(child.getName() == 'WBCFTTP' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){FTTP= 'Available';}
                    if(child.getName() == 'FTTPONDEMAND' && c1.getName() =='EXCHSTATE' && c1.getText() =='E'){FOD= 'Available';}
                }
                //speeds
                for(Dom.XMLNode c1: child.getChildElements()) {
                    if(child.getName() == 'WBC' && c1.getName() =='ANNEXMUPSPEED' && c1.getText()!=''){WBC= c1.getText();}
                    if(child.getName() == 'WBCFTTC' && c1.getName() =='UPSPEED' && c1.getText()!=''){FTTC= c1.getText();}
                    if(child.getName() == 'WBCFTTP' && c1.getName() =='UPSPEED' && c1.getText()!=''){FTTP= c1.getText();}
                    if(child.getName() == 'FTTPONDEMAND' && c1.getName() =='UPSPEED' && c1.getText()!=''){FOD= c1.getText();}

                    if(child.getName() == 'MAX' && c1.getName() =='SPEED' && c1.getText()!=''){dlMAX= c1.getText();}
                    if(child.getName() == 'ADSL2' && c1.getName() =='SPEED' && c1.getText()!=''){dlADSL2= c1.getText();}
                    if(child.getName() == 'WBC' && c1.getName() =='SPEED' && c1.getText()!=''){dlWBC= c1.getText();}
                    if(child.getName() == 'WBCFTTC' && c1.getName() =='DOWNSPEED' && c1.getText()!=''){dlFTTC= c1.getText();}
                    if(child.getName() == 'WBCFTTP' && c1.getName() =='DOWNSPEED' && c1.getText()!=''){dlFTTP= c1.getText();}
                    if(child.getName() == 'FTTPONDEMAND' && c1.getName() =='DOWNSPEED' && c1.getText()!=''){dlFOD= c1.getText();}
                }          
            } 
        }else{
            MAX = Null; ADSL2 = Null; WBC = Null; FTTC = Null; FTTP = Null; FOD = Null;
        }
    }
    String xmlStr='<DSLchecker>';
    xmlStr += '<linecheckRun>'+system.Now()+'</linecheckRun>';
    xmlStr += '<acLineCheckNumber>'+telNum+'</acLineCheckNumber><acLineCheckNameNum>'+nameNum+'</acLineCheckNameNum><acLineCheckPostCode>'+pc+'</acLineCheckPostCode>';
    xmlStr += '<MAX>'+MAX+'</MAX><ADSL2>'+ADSL2+'</ADSL2><WBC>'+WBC+'</WBC><FTTC>'+FTTC+'</FTTC><FTTP>'+FTTP+'</FTTP><FOD>'+FOD+'</FOD><dlMAX>'+dlMAX+'</dlMAX>';
    xmlStr += '<dlADSL2>'+dlADSL2+'</dlADSL2><dlWBC>'+dlWBC+'</dlWBC><dlFTTC>'+dlFTTC+'</dlFTTC><dlFTTP>'+dlFTTP+'</dlFTTP><dlFOD>'+dlFOD+'</dlFOD>';
    xmlStr += fullXML;
    xmlStr += '<errText>'+errText+'</errText>';
    xmlStr += '</DSLchecker>';
    return xmlStr;
}
public PageReference lineResults() {
    String telNum = Null; 
    String chkType = Null; 
    String MAX = Null; String ADSL2 = Null; String WBC = Null; String FTTC = Null; String FTTP = Null; String FOD = Null;
    String dlMAX = Null; String dlADSL2 = Null; String dlWBC = Null; String dlFTTC = Null; String dlFTTP = Null; String dlFOD = Null;

    Order_Site__c siteRec = [SELECT ID, zXMLInfo__c FROM Order_Site__c WHERE Id = :siteId LIMIT 1];

    if(checkType == 'tel'){
        telNum = lineChange;
        pcChange = Null;
        nameNumChange = Null;
    }else if(checkType == 'pc'){
        telNum = pcChange;
        lineChange = Null;
    }else if(checkType == 'full'){
        telNum = fullAdd;
        pcChange = '1'+jmPC;    
        nameNumChange = jmNum;          
    }

    String tRes = lineCheck(telNum, checkType);
	tRes = tRes.replaceAll('\\n', '');
    String origXML = qbOrderCTRL.writeApexXML(siteRec.zXMLInfo__c, 'DSLchecker');
    siteRec.zXMLInfo__c =  origXML +tRes;

    update siteRec;

    return null; 
}

}