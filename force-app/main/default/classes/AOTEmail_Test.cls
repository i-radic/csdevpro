@isTest
private class AOTEmail_Test {

    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        User thisUser = [select id from User where id=:userinfo.getUserid()];
		System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
       
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;

        Account accnt = Test_Factory.CreateAccount();
        accnt.name = 'TESTCODE 2';
        accnt.sac_code__c = 'testSAC';
        accnt.Sector_code__c = 'CORP';
        accnt.LOB_Code__c = 'LOB';
        accnt.OwnerId = u1.Id;
        accnt.CUG__c='CugTest';
        insert accnt;
    
        Opportunity opp1 = Test_Factory.CreateOpportunity(accnt.id);
        opp1.closedate = system.today();
        Database.SaveResult opptResult = Database.insert(opp1);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(opp1);
        AOTEmail aotEmail = new AOTEmail(sc);
        aotEmail.actionDo();
    }
    } 
}