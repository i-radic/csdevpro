@isTest

private class Test_emailCompliance{
    
    static testmethod void myUnitTest() {
        User u;
        Profile p= [select id from profile where name=:'BTLB: Standard Team'];
        u= new User(alias='asd',EIN__c='sfd234',email='testing@bt.com',emailencodingkey='UTF-8',lastname='Testingasd', languagelocalekey='en_US',localesidkey='en_US',isActive=True,Department='TestSupport', profileId = p.Id, timezonesidkey='Europe/London', username='testingasd@bt.com',Email_Signature__c = 'Regards');
        insert u;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.htmlBody = 'Here is my htmlText body of the email';
        email.plaintextBody = 'Here is my plainText body of the email';
        email.fromAddress ='testing@testemail.com';
        email.subject = 'Here is my plainText body of the email';
        emailCompliance EC= new emailCompliance();
        EC.handleInboundEmail(email, env);
        
        
        
        
    }
    
    static testmethod void myUnitTest1() {
        User u1;
        Profile p1= [select id from profile where name=:'BTLB: Standard Team'];
        u1= new User(alias='asd',EIN__c='sfm234',email='testing@bt.com',Email_BCC__c = true,emailencodingkey='UTF-8',lastname='Testingasd', languagelocalekey='en_US',localesidkey='en_US',isActive=True,Department='TestSupport', profileId = p1.Id, timezonesidkey='Europe/London', username='testingasd@testemail.com',Email_Signature__c = 'Regards');
        insert u1;
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        email.htmlBody = 'Here is my plainText body of the email';
        email.plaintextBody = 'Here is my plainText body of the email';
        email.fromAddress ='testing@bt.com';
        emailCompliance EC= new emailCompliance();
        EC.handleInboundEmail(email,env);
        

        
        
    }
    
}