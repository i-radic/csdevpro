@isTest
private class CS_ProductConfTriggerDelegateTest
{
    private static cscfga__Product_Configuration__c config;
    private static cscfga__Product_Configuration__c subConfig;
    private static cscfga__Product_Configuration__c config3;
	private static cscfga__Product_Configuration__c flexPc;
	private static cscfga__Product_Configuration__c sharerPc;
	private static cscfga__Product_Configuration__c broadbandPC;
	private static cscfga__Product_Configuration__c hardwarePc;
    
    private static void createTestData()
    {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();

        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;

        createProductConfigurations(testBasket.Id);
        CS_Customisations cs = new CS_Customisations();
        String test = cs.getTopComponentHtml();
    }

    private static void createProductConfigurations(Id basketId)
    {
        List<cscfga__Product_Definition__c> definitions = new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c prodDef =
            new cscfga__Product_Definition__c
                ( Name = 'Fixed Line'
                , cscfga__Description__c = 'Test helper' );
        definitions.add(prodDef);
        cscfga__Product_Definition__c prodDefNew =
            new cscfga__Product_Definition__c
                ( Name = 'BB Test'
                , cscfga__Description__c = 'Test helper' );
        definitions.add(prodDefNew);
        cscfga__Product_Definition__c prodDef3 =
            new cscfga__Product_Definition__c
                ( Name = 'Credit Funds'
                , cscfga__Description__c = 'Test helper' );
        definitions.add(prodDef3);

		cscfga__Product_Definition__c broadbandPd =
            new cscfga__Product_Definition__c
                ( Name = 'BT Mobile Broadband'
                , cscfga__Description__c = 'Test helper' );
		definitions.add(broadbandPd);
		cscfga__Product_Definition__c flexPd =
            new cscfga__Product_Definition__c
                ( Name = 'BT Mobile Flex'
                , cscfga__Description__c = 'Test helper' );
		definitions.add(flexPd);
		cscfga__Product_Definition__c sharerPd =
            new cscfga__Product_Definition__c
                ( Name = 'BT Mobile Sharer'
                , cscfga__Description__c = 'Test helper' );
		definitions.add(sharerPd);
		cscfga__Product_Definition__c hardwarePd =
            new cscfga__Product_Definition__c
                ( Name = 'BT Mobile Hardware'
                , cscfga__Description__c = 'Test helper' );
		definitions.add(hardwarePd);

        insert definitions;
        
        List<cscfga__Attribute_Definition__c> attDefs = new List<cscfga__Attribute_Definition__c>();
        cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
            Name = 'No of users',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'User Input',
            cscfga__Product_Definition__c = prodDef.Id
        );
        attDefs.add(ad1);
        
        cscfga__Attribute_Definition__c ad6 = new cscfga__Attribute_Definition__c(
            Name = 'Number of users',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'User Input',
            cscfga__Product_Definition__c = prodDefNew.Id
        );
        attDefs.add(ad6);
        
        cscfga__Attribute_Definition__c ad2 = new cscfga__Attribute_Definition__c(
            Name = 'Ad2',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'Related Product',
            cscfga__Product_Definition__c = prodDef.Id,
            cscfga__high_volume__c = true
        );
        attDefs.add(ad2);
        
        cscfga__Attribute_Definition__c ad3 = new cscfga__Attribute_Definition__c(
            Name = 'Customiser Processed',
            cscfga__Data_Type__c = 'Boolean',
            cscfga__Type__c = 'User Input',
            cscfga__Product_Definition__c = prodDef.Id
        );
        attDefs.add(ad3);
        
        cscfga__Attribute_Definition__c ad4 = new cscfga__Attribute_Definition__c(
            Name = 'total_Investment_Pot_0',
            cscfga__Data_Type__c = 'String',
            cscfga__Type__c = 'User Input',
            cscfga__Product_Definition__c = prodDef3.Id
        );
        attDefs.add(ad4);
        
        cscfga__Attribute_Definition__c ad8 = new cscfga__Attribute_Definition__c(
            Name = 'test',
            cscfga__Product_Definition__c = prodDef3.Id
        );
        attDefs.add(ad8);
        insert attDefs;
        
        cscfga__Available_Product_Option__c apo = new cscfga__Available_Product_Option__c(
            cscfga__Attribute_Definition__c = ad2.Id,
            cscfga__Product_Definition__c = prodDefNew.Id
        );
        insert apo;
        
        CS_Related_Products__c rp = new CS_Related_Products__c(
            Name = 'Fixed line',
            Related_Products__c = 'BB Test'
        );
        insert rp;
        
        List<CS_Related_Product_Attributes__c> rpAtts = new List<CS_Related_Product_Attributes__c>();
        CS_Related_Product_Attributes__c rpa = new CS_Related_Product_Attributes__c(
            Name = 'BB Test',
            Attribute_Names__c = 'Ad6',
            List_View_Attribute_Names__c = 'Ad6',
            Total_Attribute_Names__c = 'Ad6',
            Total_Attribute_Names_2__c = 'Ad6',
            Attribute_Names_2__c = 'Ad6'
        );
        rpAtts.add(rpa);
        
        CS_Related_Product_Attributes__c rpa1 = new CS_Related_Product_Attributes__c(
            Name = 'Credit Funds',
            Attribute_Names__c = 'Ad4',
            List_View_Attribute_Names__c = 'Ad4',
            Total_Attribute_Names__c = 'Ad4',
            Total_Attribute_Names_2__c = 'Ad4',
            Attribute_Names_2__c = 'Ad4'
        );
        rpAtts.add(rpa1);

        
        List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>();
        config = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = prodDef.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12
            );
        configs.add(config);
        
        config3 = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = prodDef3.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12);
        configs.add(config3);

		flexPc = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = flexPd.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12
			, Volume__c = 8);
        configs.add(flexPc);

		sharerPc = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = sharerPd.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12
			, Volume__c = 9);
        configs.add(sharerPc);

		broadbandPC = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = broadbandPd.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12
			, Volume__c = 10);
        configs.add(broadbandPC);

		hardwarePc = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = basketId
            , cscfga__Product_Definition__c = hardwarePd.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12
			, Volume__c = 11);
        configs.add(hardwarePc);

        insert configs;
        
        List<cscfga__Attribute__c> atts = new List<cscfga__Attribute__c>();
        cscfga__Attribute__c at1 = new cscfga__Attribute__c(
            Name = 'No of users',
            cscfga__Attribute_Definition__c = ad1.Id,
            cscfga__Value__c = '2',
            cscfga__Product_Configuration__c = config.Id
        );
        atts.add(at1);

        cscfga__Attribute__c at3 = new cscfga__Attribute__c(
            Name = 'Customiser Processed',
            cscfga__Attribute_Definition__c = ad3.Id,
            cscfga__Value__c = 'No',
            cscfga__Product_Configuration__c = config.Id
        );
        atts.add(at3);
                
        subConfig =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = basketId
                , cscfga__Product_Definition__c = prodDefNew.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12
                , cscfga__Root_Configuration__c = config.Id
                , cscfga__Parent_Configuration__c = config.Id );
        insert subConfig;
        
        cscfga__Attribute__c at6 = new cscfga__Attribute__c(
            Name = 'Number of users',
            cscfga__Attribute_Definition__c = ad6.Id,
            cscfga__Value__c = '2',
            cscfga__Product_Configuration__c = subConfig.Id
        );
        atts.add(at6);
        
         cscfga__Attribute__c at5 = new cscfga__Attribute__c(
            Name = 'Ad2',
            cscfga__Attribute_Definition__c = ad2.Id,
            cscfga__Value__c =  subConfig.id,
            cscfga__Product_Configuration__c = config.Id
        );
        atts.add(at5);
        
        cscfga__Attribute__c at2 = new cscfga__Attribute__c(
            Name = 'total_Investment_Pot_0',
            cscfga__Attribute_Definition__c = ad4.Id,
            cscfga__Value__c = '2',
            cscfga__Product_Configuration__c = config3.Id
        );
        atts.add(at2);
        insert atts;
        CS_Customiser_Extension__c prodDefinition = new CS_Customiser_Extension__c(Name = 'Fixed line',Class_Name__c = 'CS_UsageProfileCustomiserExtension');
        insert prodDefinition;
        
        CS_Customisations__c settings = new CS_Customisations__c(
            Classes__c = 'CS_Customiser,CS_RollupCustomiser'
         );
         insert settings;
         
         List<Attachment> attachments = new List<Attachment>();
         Attachment attach1=new Attachment();
         attach1.Name='BB Test';
         String jsonString = '{"' + config.id + '":{"No of users":"5"}}';
         Blob bodyBlob=Blob.valueOf(jsonString);
         attach1.body=bodyBlob;
         attach1.parentId=config.id;
         attachments.add(attach1);
         
         Attachment attach2=new Attachment();    
         attach2.Name='prodDef3';
         String jsonString2 = '{"' + config3.id + '":{"total_Investment_Pot_0":"5"}}';
         Blob bodyBlob2 = Blob.valueOf(jsonString2);
         attach2.body = bodyBlob2;
         attach2.parentId =config3.id;
         attachments.add(attach2);
         
         Attachment attach3=new Attachment();    
         attach3.Name='UsageProfile-reparent';
         String jsonString3 = '{"' + config3.id + '":{"total_Investment_Pot_0":"5"}}';
         Blob bodyBlob3 = Blob.valueOf(jsonString2);
         attach3.body = bodyBlob3;
         attach3.parentId = basketId;
         attachments.add(attach3);
         
         insert attachments;
    }

    private static testMethod void testConfigurations()
    {
        createTestData();
        config.Name = 'New test name';
        subConfig.Name = 'New test name';
        List<cscfga__Product_Configuration__c> configsToUpdate = new List<cscfga__Product_Configuration__c>();
        configsToUpdate.add(config);
        configsToUpdate.add(subConfig);
        update configsToUpdate;
        system.assert(config.Name == 'New test name');
        system.assert(subConfig.Name == 'New test name');
        delete subConfig;
        delete config;
        cscfga__Product_Configuration__c tempConfig = [SELECT ID, IsDeleted FROM cscfga__Product_Configuration__c WHERE ID = :config.ID ALL ROWS];
        system.assertEquals(tempConfig.IsDeleted, true);
    }

	@IsTest
	public static void testConfigurationsBeforeUpdate() {
		createTestData();
		List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>();
		flexPc.Name = 'New Flex Name';
		sharerPc.Name = 'New Sharer Name';
		broadbandPc.Name = 'New Broadband Name';
		hardwarePc.Name = 'New Hardware Name';
		pcList.add(flexPc);
		pcList.add(sharerPc);
		pcList.add(broadbandPc);
		pcList.add(hardwarePc);
		UPDATE pcList;

		cscfga__Product_Configuration__c flexCheck = [SELECT Id, Name, Voice_Revenue_inc_SMS__c, Data_Revenue__c, Volume__c, Incremental_Overheads__c FROM cscfga__Product_Configuration__c WHERE Id = :flexPc.Id];
		System.assertEquals((0.04 * (flexCheck.Voice_Revenue_inc_SMS__c + flexCheck.Data_Revenue__c)) + (0.33 * flexCheck.Volume__c), flexCheck.Incremental_Overheads__c);

	}
}