global class BatchCreateSalesOrderValueKPI implements Database.Batchable<sObject>, Database.Stateful
{
    global map<date, decimal> dateMap = new map<date, decimal>(); //holds final results
    
    GlobalKPISettings__c gs = GlobalKPISettings__c.getInstance();
    GlobalKPI_Helper gh = new GlobalKPI_Helper();
    public string businessName  = gs.Business_Unit__c; 
    public string busCode       = gs.Business_Unit_Code__c; 
        
    public string theQuery;
    public string thisPeriod; 
    global List<Global_KPI__c> gkpiUpdList = new List<Global_KPI__c>();      
    global BatchCreateSalesOrderValueKPI ()
    {
        
        
        theQuery = 'select closeDate, Amount from Opportunity where Account_Sub_Sector__c =  \'TIKIT\' and isClosed = true and isWon = true and (closeDate = LAST_FISCAL_YEAR Or closeDate=THIS_FISCAL_YEAR) and closeDate<=TODAY';
        
        Date map_start_date     = [Select startDate From Period Where type = 'Year' and StartDate = LAST_FISCAL_YEAR].startDate ;
        Date map_end_date       = date.valueOf([Select endDate From Period Where type = 'Month' and StartDate = THIS_MONTH].endDate );
        
        system.debug(map_start_date);
        system.debug(map_end_date);
    
        for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(1)  )
        {
            dateMap.put(d, 0);  
        }
        system.debug(dateMap);
                
        system.debug(theQuery);
        system.debug ('*****************************BatchCreateSalesOrderValueKPI'); 
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug ('*****************************BatchCreateSalesOrderValueKPI QueryLocator() method');      
        return Database.getQueryLocator(theQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.debug ('*****************************BatchCreateSalesOrderValueKPI Execute() method');   
        
        for(sObject s : scope) //loop through rows of scope list
        {
            Opportunity o = (Opportunity) s;
            if (o.Amount == null) 
            {
                //do nothing
            } 
            else 
            {   
                system.debug(o.closeDate.toStartOfMonth());
                system.debug(dateMap.get(o.closeDate.toStartOfMonth()));
                dateMap.put(o.closeDate.toStartOfMonth(), dateMap.get(o.closeDate.toStartOfMonth()) + o.Amount);
                system.debug(dateMap.get(o.closeDate.toStartOfMonth()));
            }
                
        }
        system.debug(dateMap);
        
    }    
    
        
    global void finish(Database.BatchableContext BC)
    {  
        
        system.debug ('*****************************BatchCreateSalesOrderValueKPI Finish() method');        
        
        string dtFrom;
        string dtTo;
        string periodCode;
        
        periodCode='MN';
        for (date d : dateMap.keySet())
        {
            Global_KPI__c kpi           = new Global_KPI__c();
            kpi.business_unit__c        = businessName;
            kpi.Measure_Name__c         = 'Sales Order Value';
            kpi.Unit_of_Measure__c      = 'Currency';
            kpi.Date_From__c            = d;
            dtFrom                      = string.valueOf(d);
            kpi.Date_To__c              = [Select endDate From Period Where type = 'Month' and StartDate = :d].endDate;
            dtTo                        = string.valueOf(kpi.Date_To__c) ;
            kpi.uniqueKey__c = busCode + ':' + 'SOV' + ':' + periodCode + ':' + dtFrom.replace('-','') + ':' + dtTo.replace('-','');  
            kpi.Period_Type__c          = 'Month';
            kpi.result__c               = dateMap.get(d);
            
            //insert or update it using external ID field uniqueKey__c as the key
            system.debug(kpi);
            upsert kpi uniqueKey__c;
        }
            
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                         FROM AsyncApexJob WHERE Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.     
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BatchCreateSalesOrderValueKPI ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }


}