public with sharing class KCIRecordTypeSelection {

    Id KCIRTId;
    Public String RecordTypeName,recordTypes;
    
    KCI__c kci; 
    
    public KCIRecordTypeSelection(ApexPages.StandardController controller) {
        kci = (KCI__c)controller.getRecord();        
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Validation Call','Validation Call'));
        options.add(new SelectOption('Confirmation Call','Confirmation Call'));
        return options;
    }
    
    public String getrecordTypes() {
        return recordTypes;
    }
            
    public void setrecordTypes(String recordTypes) {
        this.recordTypes = recordTypes;
    }
        
    public PageReference Continue1(){
        PageReference NextPage;
                
        List<RecordType> RT=[Select Id,Name,SOBJECTTYPE,DeveloperName From RecordType where Name=:recordTypes limit 1];
        
        if(recordTypes=='Validation Call')
        {  
            //CAT
            //NextPage=new PageReference('/apex/KCIEditPage_Validation?retURL=/a2P/o&RecordType='+RT[0].Id+'&nooverride=1&ent=01IR0000000DRcA');
            //Live
            NextPage=new PageReference('/apex/KCIEditPage_Validation?retURL=/a2P/o&RecordType='+RT[0].Id+'&nooverride=1&ent=01I200000007YdI');
        }  
         
        if(recordTypes=='Confirmation Call')
        { 
            //CAT
            //NextPage=new PageReference('/apex/KCIEditPage_Confirmation?retURL=/a2P/o&RecordType='+RT[0].Id+'&nooverride=1&ent=01IR0000000DRcA');
            //Live
            NextPage=new PageReference('/apex/KCIEditPage_Confirmation?retURL=/a2P/o&RecordType='+RT[0].Id+'&nooverride=1&ent=01I200000007YdI');
        }
        
        
        NextPage.setRedirect(true);
        return NextPage;    
    }
    
    Public PageReference Cancel1(){
        
        //CAT
        PageReference CancelPage = new PageReference('/a2P/o'); 
           
        CancelPage.setRedirect(true);
        return CancelPage;
    }    
    
      
    
    }