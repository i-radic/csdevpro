public with sharing class HoldingReportWrapper {
	//we cannot use group as variable identifier since it is an apex reserved word
	public String 	groupField;
	public String 	corporate;
	public String 	account;
	public String 	subscription;
	public String 	tariff;
	public String 	costCentre;
	public String 	userName;
	public String 	mobileNumber;
	public String 	connectedDate;
	public Integer 	termOfContract;
	public String 	contractStartDate;
	public String 	networkCode;
	public String 	status;
	public String 	ced;
	public Integer 	daysCed;
	public String 	inImplementationForm;
	public Integer 	termMTHSRemaining;
	public String 	exclusions;

	public transient DateTime connectedDateField;
	public transient DateTime contractStartDateField;
	public transient DateTime cedDate;

	private static String DDMMYYYY_FORMAT = 'dd/MM/yyyy'; 

	public HoldingReportWrapper() {}

	public HoldingReportWrapper( String csvLine ) {	

		if( String.IsNotBlank( csvLine ) ) {
			String[] csvElements = csvLine.split(',');
			System.debug('....size()::::'+csvElements.size());
			if( csvElements != null && csvElements.size() >= 12) {
				this.groupField 			= 	csvElements[0];
				this.corporate 				= 	csvElements[1];
				System.debug('corporate::::'+csvElements[1]);
				this.account 				= 	csvElements[2];
				this.subscription 			= 	csvElements[3];
				this.tariff 				= 	csvElements[4];
				this.costCentre 			= 	csvElements[5];
				this.userName 				= 	csvElements[6];
				this.mobileNumber 			= 	csvElements[7];	
				this.connectedDateField 	= 	parseDate( csvElements[8] );
				this.termOfContract			= 	Integer.valueOf( csvElements[9] );
				System.debug('termOfContract::::'+csvElements[9] );
				this.contractStartDateField = 	parseDate( csvElements[10] );
				this.networkCode			= 	csvElements[11];
				//this.status	 				= 	csvElements[12];				

				this.contractStartDate 		= 	this.contractStartDateField.format( DDMMYYYY_FORMAT );
				this.connectedDate 			= 	this.connectedDateField.format( DDMMYYYY_FORMAT );				
			}
		}
	}

	public void calculateCed() {
	    system.debug(LoggingLevel.WARN, '>>contractStartDateField = ' + contractStartDateField);
		cedDate = contractStartDateField.addDays(( termOfContract / 12 ) * 365 );
		
 		system.debug(LoggingLevel.WARN, 'RRR cedDate: ' + cedDate);
 		
		ced = cedDate.format( DDMMYYYY_FORMAT );
		system.debug(LoggingLevel.WARN, 'RRR ced: ' + ced);
	}

	public void calculateCedDays() {
		Date lastDayOfMonth = Date.today().addMonths(1).toStartofMonth().addDays(-1);
		system.debug(LoggingLevel.WARN, 'RRR lastDayOfMonth: ' + lastDayOfMonth);

        //17/05/2018 - ISSUE38791 - Darko switched daysbetween formula, before it was from past date until today, now it is from today until date from report
 		daysCed = lastDayOfMonth.daysBetween( cedDate.date() );	
 		if(daysCed < 0)
 			daysCed = 0;
 		system.debug(LoggingLevel.WARN, 'RRR daysCed: ' + daysCed);
	}

	public void calculateTermMTHS(){
		termMTHSRemaining = Integer.valueOf(Math.ceil(Math.ceil(daysCed * 12) / 365));
		system.debug(LoggingLevel.WARN, 'calculateTermMTHS.termMTHSRemaining: ' + termMTHSRemaining);
	}

	public DateTime parseDate(String strDate){
		DateTime returnDate = null;
		System.debug(LoggingLevel.WARN, 'strDate = ' + strDate);		
		String[] strDateArray = String.IsNotBlank( strDate ) ? strDate.split('/') : null;

		if( strDateArray != null && strDateArray.size() == 3 ){
			String month 	= 	strDateArray[1];
			String day 		= 	strDateArray[0];
			String year 	= 	strDateArray[2];
			returnDate = DateTime.newInstance( Integer.valueOf( year ), Integer.valueOf( month ), Integer.valueOf( day ) );			
		}
		return returnDate;
	}
}