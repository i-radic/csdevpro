/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class trgAfterContract_Test {

    static testMethod void contractAfterTest() {
    		User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        
         Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@bt.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
        
        //
        Account account = Test_Factory.CreateAccount();
        account.name = 'TESTCODE 2';
        account.sac_code__c = 'Dummy999';
        account.Sector_code__c = 'CORP';
        account.LOB_Code__c = 'LOB';
        account.OwnerId = u1.Id;
        account.CUG__c='CugTest';
        insert account;
        
        Contract cn = new Contract(Name__c='TESTESTCN', Status='Draft', Accountid=account.Id, Adder_Status__c = 'Draft');
        insert cn;
        
        Contract cn1 = new Contract(Name__c='TESTESTCN1', Status='Draft', Accountid=account.Id, Adder_Status__c = 'Active');
        insert cn1;
      
        Contract cn2 = new Contract(Name__c='TESTESTCN1', Status='Draft', Accountid=account.Id, Adder_Status__c = 'Draft');
        Contract cn3 = new Contract(Name__c='TESTESTCN1', Status='Draft', Accountid=account.Id, Adder_Status__c = 'Active');
        

        insert new List<Contract>{cn2, cn3};
    }
    }
}