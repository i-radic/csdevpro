/*  ======================================================
    History                                                            
    -------                                                            
VERSION  AUTHOR             DATE              DETAIL                                 FEATURES
1.00     Dan Measures       13/06/2011        Retrieve all snapshot records created YESTERDAY (via the analytical snapshot), these records will have their owner set.

*/
global class BatchSchedulePortfolioAreaSnapshotOwner implements Schedulable{
   	global void execute(SchedulableContext sc) {
		String qString = 'select id, BTLB_Name__c from Forecasting_Portfolio_Area_Snapshot__c where CreatedDate  = YESTERDAY';
        BatchPortfolioAreaSnapshotOwner b = new BatchPortfolioAreaSnapshotOwner(qString);
    	database.executebatch(b, 200);
    }
}