@isTest
private class Test_BookToBillBefore {

static testMethod void runPositiveTestCases() {

Date TestDate1 = date.today();
Date TestDate2 = date.today()+1;
		User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         }
        Account acc1 = Test_Factory.CreateAccount(); // a LOB Level Account to link to
        acc1.Sector__c = 'other';
        acc1.LOB_Code__c = 'TEST';
        acc1.SAC_Code__c = 'JMMO1231';
        acc1.LE_Code__c = null;
        acc1.AM_EIN__c = '803268119';
        acc1.postcode__c = 'WR5 3RL';
        acc1.Base_Team_Assign_Date__c = Date.today();
        insert acc1;
        
        /*Account a = Test_Factory.CreateAccount();
        a.LE_CODE__c = 'T-99992';
        insert a;*/
        
        Opportunity o = Test_Factory.CreateOpportunity(acc1.id);
        insert o; 
        
        BookToBill__c LS1 = new BookToBill__c();     
        LS1.Opportunity__c = o.id;   
        LS1.RecordTypeId=[Select Id from RecordType where Name=:'ICT Book to Bill'].Id;
        LS1.UV1_Correct_Sales_Type_Selected__c='Yes';
        LS1.UV2_Mobius_Evolution_report_attached__c='Yes';
        LS1.UV3_Contract_attached__c='Yes';
        LS1.UV4_CCC_Schedule_attached__c='Yes';
        LS1.UV5_Customer_email_for_rollover__c='Yes';
        LS1.UV6_Contract_Signed_Dated__c='Yes';
        LS1.UV7_Contract_Number_recorded_on_SFDC__c='Yes';
        LS1.UV8_Network_Build_attached__c='Yes';
        LS1.UV9_Product_Match__c='Yes';
        LS1.UV10_Accurate_Product_Lines__c='Yes';
        LS1.M1_Product_Match__c='Yes';
        LS1.M2_Product_Term_Match__c='Yes';
        LS1.M3_Volumes_Correct_Resign_or_Upsell_o__c='Yes';
        LS1.DC1__c='Yes';
        LS1.DC2__c='Yes';
        LS1.DC3__c='Yes';
        LS1.DC4__c='Yes';
        LS1.DC5__c='Yes';
        LS1.DC6__c='Yes';
        LS1.DC7__c='Yes';
        LS1.DC8__c='Yes';
        LS1.DC9__c='Yes';
        LS1.DC10__c='Yes';
        LS1.DC11__c='Yes';
        LS1.DC12__c=TestDate1;
        LS1.SC4__c=10;
        LS1.DC14_Total_Contract_Value_Recorded_FF__c='Yes';
        LS1.SC1__c='Yes';
        LS1.SC2__c='Yes';
        LS1.SC3__c='Yes';
        LS1.SC5__c='Yes';
        LS1.SC5_Net_ACV_Calculation_Correct__c='Yes';
        LS1.FeedbackNotes__c='Text 1';
        LS1.Task_Comments_Measurement__c='Text 2';
        LS1.Project_Status__c='Text 3';
        LS1.Revenue_Assurance_Status__c='Text 4';
        LS1.Status_Date__c=TestDate1;
        LS1.Status__c='Awaiting More Data';

insert LS1;

//check where startin value is null, dates only
        //LS1.DC12__c=null;
        //LS1.Status_Date__c=null;
       
//update LS1;

//check changes to values
LS1.UV1_Correct_Sales_Type_Selected__c='No';
        LS1.UV2_Mobius_Evolution_report_attached__c='No';
        LS1.UV3_Contract_attached__c='No';
        LS1.UV4_CCC_Schedule_attached__c='No';
        LS1.UV5_Customer_email_for_rollover__c='No';
        LS1.UV6_Contract_Signed_Dated__c='No';
        LS1.UV7_Contract_Number_recorded_on_SFDC__c='No';
        LS1.UV8_Network_Build_attached__c='No';
        LS1.UV9_Product_Match__c='No';
        LS1.UV10_Accurate_Product_Lines__c='No';
        LS1.M1_Product_Match__c='No';
        LS1.M2_Product_Term_Match__c='No';
        LS1.M3_Volumes_Correct_Resign_or_Upsell_o__c='No';
        LS1.DC1__c='No';
        LS1.DC2__c='No';
        LS1.DC3__c='No';
        LS1.DC4__c='No';
        LS1.DC5__c='No';
        LS1.DC6__c='No';
        LS1.DC7__c='No';
        LS1.DC8__c='No';
        LS1.DC9__c='No';
        LS1.DC10__c='No';
        LS1.DC11__c='No';
        LS1.DC12__c=TestDate2;
        LS1.SC4__c=20;
        LS1.DC14_Total_Contract_Value_Recorded_FF__c='No';
        LS1.SC1__c='No';
        LS1.SC2__c='No';
        LS1.SC3__c='No';
        LS1.SC5__c='No';
        LS1.SC5_Net_ACV_Calculation_Correct__c='No';
        LS1.FeedbackNotes__c='Text 5';
        LS1.Task_Comments_Measurement__c='Text 6';
        LS1.Project_Status__c='Text 7';
        LS1.Revenue_Assurance_Status__c='Text 8';
        LS1.Status_Date__c=TestDate2;
        LS1.Status__c='Project in Progress';
update LS1;


}

}