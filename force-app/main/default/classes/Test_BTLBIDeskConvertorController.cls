@isTest
private class Test_BTLBIDeskConvertorController {
    
    static testMethod void runPositiveTestCases() {


        string URLtoConvert = 'http://www.google.co.uk';
        // invoke controller actions               
        BTLBIDeskConvertorController controller = new BTLBIDeskConvertorController();
        
        controller.URLtoConvert = 'http://www.google.co.uk:8080/subdir/page.html?var=1';  
        controller.ConvertURL();
        controller.getConvertURL();

        controller.URLtoConvert = 'https://www.google.co.uk:8080/subdir/page.html?var=1';  
        controller.ConvertURL();
        controller.getConvertURL();   
        
        controller.URLtoConvert = 'https://www.google.co.uk/';  
        controller.ConvertURL();
        controller.getConvertURL();
        
        controller.URLtoConvert = '';  
        controller.ConvertURL();
        controller.getConvertURL();   
    }
    
}