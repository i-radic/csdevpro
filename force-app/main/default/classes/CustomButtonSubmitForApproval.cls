global with sharing class CustomButtonSubmitForApproval implements csdiscounts.ICustomButton{ 
    private static String CONTROLLER_NAME = 'CustomButtonSubmitForApproval';

    global String performAction(String basketId){
        Map<Id, cscfga__Product_Configuration__c> pcMap = CS_Util.getConfigurationsInBasket(basketId, CONTROLLER_NAME);
        System.debug(LoggingLevel.WARN, 'CustomButtonSubmitForApproval.pcMap = ' + pcMap.values());

        List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
        Approval.ProcessResult[] processResults = null;
        Id currentUser = UserInfo.getUserId();
        System.debug(LoggingLevel.WARN, 'CustomButtonSubmitForApproval.currentUser = ' + currentUser);

        for(cscfga__Product_Configuration__c pc : pcMap.values()){
            System.debug(LoggingLevel.WARN, 'CustomButtonSubmitForApproval.pc = ' + pc);
            if(pc.Approval_Status__c != null && pc.Approval_Status__c != '' && pc.Approval_Status__c.equalsIgnoreCase(Label.CS_DiscountApprovalStatusInitialMessage)){
                Approval.ProcessSubmitRequest approvalRequest = new Approval.ProcessSubmitRequest();
                approvalRequest.setComments('Submitting request for approval');
                approvalRequest.setObjectId(pc.Id);
                approvalRequest.setSubmitterId(currentUser);
                if(pc.Approval_Level_1__c != null || pc.Approval_Level_2__c != null || pc.Approval_Level_3__c != null || pc.Approval_Level_4__c != null)
                    approvalRequest.setProcessDefinitionNameOrId('BT_Product_Approval'); 
                else
                    approvalRequest.setProcessDefinitionNameOrId('BT_Product_Lock_Children'); 
                approvalRequest.setSkipEntryCriteria(true);
                requests.add(approvalRequest);
            }
        }

        try {
            if(requests != null && requests.size() > 0){
                System.debug(LoggingLevel.WARN, 'CustomButtonSubmitForApproval.requests = ' + requests);
                processResults = Approval.process(requests, true);
                System.debug(LoggingLevel.WARN, 'CustomButtonSubmitForApproval.requests = ' + requests);
                cscfga__Product_Basket__c basket = [
                    SELECT Id, BT_Basket_Approval_Status__c
                    FROM cscfga__Product_Basket__c
                    WHERE Id = :basketId
                    LIMIT 1
                ];
                basket.BT_Basket_Approval_Status__c = 'In approval process';
                System.debug(LoggingLevel.WARN, 'CustomButtonSubmitForApproval.basket = ' + basket);

                update basket;
            }
            else
                return '{ "message":"' + Label.CS_DiscountApprovalNoProductsForApprovalMessage + '", "status":200}';
        }
        catch (System.DmlException e) {
            System.debug(LoggingLevel.WARN, 'CustomButtonSubmitForApproval.Exception = ' + e.getMessage());
            return '{ "message":"' + Label.CS_DiscountApprovalSubmitErrorMessage + ' = ' + e.getMessage() + '", "status":200}';
        }

        return '{ "message":"' + Label.CS_DiscountApprovalSubmitMessage + '", "status":200}';
    }
}