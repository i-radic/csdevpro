public with sharing class CS_ProductConfigurationService {
    public static void updateDiscounts(cscfga__Product_Configuration__c config, cscfga__Product_Configuration__c oldConfig) {
        System.debug('Product configuration service');
        //CS_DiscountJSON dJSON = (CS_DiscountJSON) JSON.deserializeStrict(config.cscfga__discounts__c,  CS_DiscountJSON.class);
        //cscfga.ProductConfiguration.ProductDiscount dJSON = (cscfga.ProductConfiguration.ProductDiscount)JSON.deserializeStrict(config.cscfga__discounts__c, cscfga.ProductConfiguration.ProductDiscount.class);
        Map<String, Object> dJSON = (Map<String, Object>) JSON.deserializeUntyped(config.cscfga__discounts__c);
        System.debug('mpk JSON Value ' +dJSON );
        config.Discount_Type__c ='£0';
        List<Object> disList = (List<Object>) dJSON.get('discounts');
        if(disList.size() > 0){
            Map<String, Object> M2 = (Map<String, Object>)disList[0];
            List<Object> mDisList = (List<Object>) M2.get('memberDiscounts');
            if(mDisList != null && mDisList.size() > 0){
                System.debug('Member Discount List :' +mDisList);
                Map<String, Object> dis = (Map<String, Object>)mDisList[0];
                system.debug('Memeber Discounts : '+dis);
                if(dis.get('type')=='percentage'){
                    if(dis.get('amount')!=null)
                    config.Discount_Type__c = String.valueOf(dis.get('amount'))+'%';
                }
                else{
                    if(dis.get('amount')!= null){
                    if(config.Product_Definition_Name__c!=null && (config.Product_Definition_Name__c=='Cloud Work' || config.Product_Definition_Name__c=='BT Engineer Install')&&config.Number_of_users__c >0)
                    config.Discount_Type__c = '£'+String.valueOf(double.valueOf(dis.get('amount'))/config.Number_of_users__c);
                    else{
                     if(config.Group_No_of_Users__c > 0)
                     config.Discount_Type__c = '£'+String.valueOf(double.valueOf(dis.get('amount'))/config.Group_No_of_Users__c);   
                      }
                    }
                } 
                if(dis.get('type') == 'absolute') {
                    Decimal volume = 1;
                    Decimal oldVolume = 1;
                    if(config.Volume__c != null && config.Volume__c != 0){
                        volume = config.Volume__c;
                    }
                    if(oldConfig.Volume__c != null && oldConfig.Volume__c != 0){
                        oldVolume = oldConfig.Volume__c;
                    }
                    System.debug('dis.amount 00>>'+dis.get('amount'));
                    decimal amt =   Double.valueOf(dis.get('amount'));
                    amt  =  (amt / oldVolume * volume);
                    dis.put('amount',amt);//amt.toPlainString();
                    System.debug('Final Amount >>'+dis.get('amount'));           
                    
                    config.cscfga__discounts__c = JSON.serialize(dJSON);
                }  
                if(String.valueOf(dis.get('source')).contains('Airtime Fund')){
                    config.Airtime_Fund__c = (-1)*Double.valueOf(dis.get('amount'));
                    config.Rolling_Air_Time__c = (-1)*Double.valueOf(dis.get('amount'));                
                }
                if(String.valueOf(dis.get('source')).contains('Hardware Fund')){
                    config.Tech_Fund_Total__c = (-1)*Double.valueOf(dis.get('amount'));
                }
                
                // change for future mobile custom caller
                /*System.debug('config.cscfga__Product_Family__c>>>>'+config.cscfga__Product_Family__c);
if(config.cscfga__Product_Family__c == 'Future Mobile'){
Double unitChargeCustomCaller =0.0;
if(dis.get('type')=='percentage'){
if(dis.get('amount')!=null && Double.valueOf(dis.get('amount')) > 0 ){
if(config.Custom_Caller_Unit_Charge__c!=null){
config.Custom_Caller_Discounted_Unit_Charge__c = config.Custom_Caller_Unit_Charge__c * ((Double.valueOf(dis.get('amount')))/100);
}
}                      
}                   
}
System.debug('config.Custom_Caller_Unit_Charge__c>>>>'+config.Custom_Caller_Unit_Charge__c);
System.debug('config.Custom_Caller_Discounted_Unit_Charge__c>>>>'+config.Custom_Caller_Unit_Charge__c);   */
                
            }
        }
    }
    
    public static void deleteProdConfigTotals(List<cscfga__Product_Configuration__c> newConfigs){
        Set<Id> prodConfigIds = new Set<Id>();
        for (cscfga__Product_Configuration__c config : newConfigs) {
            prodConfigIds.add(config.Id);
        }
        List<Product_Configuration_Totals__c> prodTotals = [
            SELECT id,Product_Configuration__c 
            FROM Product_Configuration_Totals__c 
            WHERE Product_Configuration__c 
            IN :prodConfigIds 
        ];
        
        delete prodTotals;
    }
    
    public static void createProductConfigurationTotals(Map<Id, cscfga__Product_Configuration__c> configs) {
        deleteProdConfigTotals(configs.values());
        
        List<Product_Configuration_Totals__c> totalsToInsert = new List<Product_Configuration_Totals__c>();
        Set<Id> basketIds = new Set<Id>();
        for(cscfga__Product_Configuration__c cfg : configs.values()) {
            basketIds.add(cfg.cscfga__Product_Basket__c);   
        }
        try{
            Map<Id, cscfga__product_basket__c> baskets = new Map<Id, cscfga__product_basket__c>([
                SELECT Id, Basket_Totals__c
                FROM cscfga__product_basket__c
                WHERE Id IN :basketIds FOR UPDATE  // added FOR UPDATE
            ]);
            
            
            for(cscfga__Product_Configuration__c pc : configs.values()) {
                cscfga__product_basket__c bskt = baskets.get(pc.cscfga__Product_Basket__c);
                Product_Configuration_Totals__c pt = new Product_Configuration_Totals__c();
                decimal usageProfileRevenue = pc.Usage_Profile_Voice_Revenue__c != null ? pc.Usage_Profile_Voice_Revenue__c : 0;
                pt.Product_Configuration__c = pc.id;
                pt.Product__c = pc.Product_Definition_Name__c;
                pt.Product_Type__c = pc.Product_Type__c;
                pt.Product_Name__c = pc.Product_Name__c;
                pt.Quantity__c = pc.Volume__c;
                pt.Voice_Subscribers__c = pc.Voice_Subscribers__c;
                pt.Data_Subscribers__c = pc.Data_Subscribers__c;
                pt.Total_Recurring_Charge__c = pc.Revenue_Type__c == 'Usage Based' || pc.cscfga__Recurrence_Frequency__c != 12 ? 0 : pc.cscfga__Recurring_Charge__c + usageProfileRevenue;
                pt.Total_One_Off_Charge__c = pc.Product_Definition_Name__c != 'BT Mobile Hardware' ? pc.cscfga__One_Off_Charge__c : 0;
                pt.Calculated_device_fund__c = pc.Product_Definition_Name__c == 'BT Mobile Hardware' ? pc.One_Off_Cost__c : 0;
                pt.Charge_Type__c = pc.Charge_Type__c;
                if(pc.Calculations_Product_Group__c == 'BT OnePhone'){
                    pt.BT_Hardware_Fund__c = 0;
                    pt.BT_Technology_Fund__c = 0;
                    pt.BTOP_Voice_Subscribers__c = pc.Voice_Subscribers__c;
                    pt.BTOP_Data_Subscribers__c = pc.Data_Subscribers__c;
                    pt.BTOP_Deal_Type__c = pc.Deal_Type__c != null ? pc.Deal_Type__c : '';
                }
                else{
                    pt.BT_Hardware_Fund__c = pc.BT_Hardware_Fund__c != null ? pc.BT_Hardware_Fund__c : 0;
                    pt.BT_Technology_Fund__c = pc.BT_Technology_Fund__c != null ? pc.BT_Technology_Fund__c : 0;   
                    pt.Voice_Subscribers__c = pc.Voice_Subscribers__c;
                    pt.Data_Subscribers__c = pc.Data_Subscribers__c;
                    pt.Deal_Type__c = pc.Deal_Type__c != null ? pc.Deal_Type__c : '';
                }
                pt.Total_Cost__c = pc.Total_Cost__c;
                pt.Total_Subsidy_Available__c = pc.Total_Subsidy_Available__c;
                pt.Voice_Subsidy__c = pc.Voice_Subsidy__c;
                //pt.BT_Total_Commission__c = pc.BT_Total_Commission__c;
                pt.BT_Total_Commission__c = pc.Line_Item_Commission__c;                                            
                pt.Total_Shared_Data_Users__c = pc.Shared_Data_Users__c;
                pt.Total_Extra_Shared_Data__c = pc.Shared_Data_Extra_Total__c;
                pt.Device_payment_made_by_customer__c = pc.Device_payment_made_by_customer__c;
                pt.Contract_Term_Period__c = pc.cscfga__Contract_Term__c;
                pt.BT_UK_Minutes_Extra__c = pc.BT_UK_Minutes_Extra__c;
                pt.Total_Calling_Extra__c = pc.Calling_Extra__c;
                pt.Rest_of_the_World__c = pc.Rest_of_the_World__c;
                 if(pc.Calculations_Product_Group__c =='Pronto'){
                Decimal totalrecc=0;
                if(pc.Total_Recurring_Discount__c!=null && pc.cscfga__Contract_Term__c!=null)
                    totalrecc = pc.Total_Recurring_Discount__c*pc.cscfga__Contract_Term__c;
                pt.Total_discount_against_list_price__c  = totalrecc+pc.Total_One_Off_Discount__c;
            }
                if(bskt.Basket_Totals__c != null) {
                    pt.Basket_Totals__c = bskt.Basket_Totals__c;
                    totalsToInsert.add(pt);
                }
            }
        
            if(!totalsToInsert.isEmpty()){
                insert totalsToInsert; 
            }
        }catch(exception e){}    
    }
    
    public static Map<Id, cscfga__Product_Configuration__c> updateCreditFunds(Map<Id, cscfga__Product_Basket__c> baskets) {
        Map<Id, cscfga__Product_Configuration__c> configsToUpdate = new Map<Id, cscfga__Product_Configuration__c>();
        for(cscfga__Product_Basket__c basket : baskets.values()) {
            Decimal Tenure = 0;
            for(cscfga__Product_Configuration__c cfg : basket.cscfga__Product_Configurations__r) {
                if(cfg.Tenure__c != null && cfg.Tenure__c > Tenure) {
                    Tenure = cfg.Tenure__c;
                }
            }
            
            for(cscfga__Product_Configuration__c cfg : basket.cscfga__Product_Configurations__r) {
                if(cfg.cspl__Month_Term__c != null && cfg.cspl__Month_Term__c > Tenure) {
                    cfg.cscfga__Configuration_Status__c = 'Incomplete';
                    configsToUpdate.put(cfg.Id, cfg);
                }
            }
        }
        
        return configsToUpdate;
    }
    
    public static String calculateApprovals(Map<Id, cscfga__Product_Configuration__c> productConfigurationsMap, cscfga__Product_Basket__c basket, String CONTROLLER_NAME, Boolean refreshAtt) {
        String returnMessage = '';
        if(productConfigurationsMap != null && productConfigurationsMap.size() > 0) {
            if(refreshAtt) {
                createProductConfigurationTotals(productConfigurationsMap);
                CS_Util.upsertReportConfigurationAttachment(productConfigurationsMap.keySet());
            }
            Set<Id> basketIdsSet = CS_Util.fillBasketIdSet(productConfigurationsMap, CONTROLLER_NAME);
            Map<String, Map<String, Object>> productFamilyJSONMap = CS_Util.getProductFamiliesJSONMap(CONTROLLER_NAME); //soql 3
            if(refreshAtt) {
                CS_Util.updateBasketTotals(productConfigurationsMap, basketIdsSet, basket);
            }
                                      
            
                                                                                                                                        
            
                                            
            Profile userProfile = [
                SELECT Name 
                FROM Profile 
                WHERE Id = :UserInfo.getProfileId() 
                LIMIT 1
            ];
            
            List<String> marketProfiles = CS_Util.getProductFamilyProductsListByCategoryName('Mid Market Profile', 'profileName', productFamilyJSONMap);
            
            if(!productFamilyJSONMap.isEmpty() && userProfile.Name != marketProfiles[0]){
                Map<Id, CS_BasketConfigurationWrapper> basketConfigsMap = CS_Util.fillBasketConfigsMap(basketIdsSet); //soql 4
                CS_Util.fillProductConfigurationsSetInBasketConfigsMap(productConfigurationsMap, basketConfigsMap, productFamilyJSONMap);
                List<BT_User_Group_Member__c> userGroupMemberList = CS_Util.getUserGroupMember(CONTROLLER_NAME); //soql 5
                List<CS_Approval_Paths__c> approvalPathList = CS_Util.getApprovalPaths(userGroupMemberList, CONTROLLER_NAME); //soql 6
                List<CS_Approval_Conditions__c> approvalConditionsList = CS_Util.getApprovalConditions(approvalPathList, CONTROLLER_NAME); //soql 7
                
                if(approvalPathList.size() > 0 && approvalConditionsList.size() > 0){
                    basketConfigsMap = CS_Util.calculateThresholdsForFamilies(basketConfigsMap); 
                    Map<Id, cscfga__Product_Configuration__c> configsToUpdate = CS_Util.checkApprovalProcessThresholds(basketConfigsMap, productFamilyJSONMap, approvalPathList, approvalConditionsList); //soql 7
                    for(cscfga__Product_Configuration__c pc : configsToUpdate.values()){
                        if(pc.Approval_Level_1__c != null || pc.Approval_Level_2__c != null || pc.Approval_Level_3__c != null)
                            returnMessage = Label.BT_DiscountApprovalMessage;
                    }
                    if(configsToUpdate != null && !configsToUpdate.isEmpty())
                        UPDATE configsToUpdate.values(); //soql 8
                }
            }
               
        }
        
        return returnMessage;
    }
    
    public static void updateUsageProfileCalcs(List<cscfga__Attribute__c> attributes, List<cscfga__Product_Configuration__c> configs,
                                               Map<Id, cscfga__Product_Definition__c> definitions, Map<Id, cscfga__Attribute_Definition__c> attDefs) {
                                                   Set<Id> usageProfileIds = new Set<Id>();
                                                   for(cscfga__Attribute__c att : attributes) {
                                                       if(att.Name == 'Usage Profile' && attDefs.get(att.cscfga__Attribute_Definition__c).cscfga__Type__c == 'Lookup') {
                                                           usageProfileIds.add(att.cscfga__Value__c);
                                                       }
                                                   }
                                                   
                                                   if(!usageProfileIds.isEmpty()) {
                                                       Map<Id, Usage_Profile__c> usageProfiles = new Map<Id, Usage_Profile__c>([
                                                           Select
                                                           Id,
                                                           IDD_Zone_1__c,
                                                           IDD_Zone_2__c,
                                                           IDD_Zone_3__c,
                                                           IDD_Zone_4__c,
                                                           IDD_Zone_5__c,
                                                           Freephone__c,
                                                           EOLBillMonths__c,
                                                           Domestic_Text__c,
                                                           UK_Fixed_Line_Numbers__c,
                                                           IDD_Zone_6__c,
                                                           Voice_Roaming_EU__c,
                                                           Voice_Roaming_Zone_6_RoW__c,
                                                           Voice_Roaming_Zone_5__c,
                                                           Voice_Roaming_Zone_4_USA_Canada__c,
                                                           Voice_Roaming_Zone_3_RoW__c,
                                                           Own_Account_Fixed_Line_Numbers__c,
                                                           Own_Account_Mobiles__c,
                                                           Closed_user_group__c ,
                                                           Other__c ,
                                                           Calls_to_other_UK_mobile_networks__c ,
                                                           Other_BT_Minutes_Total__c,
                                                           Other_BT_Mobiles__c,
                                                           Calls_to_EE__c,
                                                           Other_UK_Mobile_Operators__c,
                                                           Voicemail__c,
                                                           Non_Geographic__c,
                                                           FreePhone_BT__c,
                                                           International_Calls_BT__c,
                                                           Voice_Roaming__c,
                                                           Roaming_Inter_Country__c,
                                                           Expected_Data_per_User__c,
                                                           Answer_phone__c,
                                                           Landline__c
                                                           From Usage_Profile__c
                                                           Where Id IN :usageProfileIds
                                                       ]);
                                                       
                                                       for(cscfga__Product_Configuration__c pc : configs) {
                                                           if(definitions.get(pc.cscfga__Product_Definition__c).Name == 'BT Mobile Flex') {
                                                               String rateCardJson;
                                                               String usageProfileId;
                                                               Integer volume = 0;
                                                               
                                                               for(cscfga__Attribute__c att : attributes) {
                                                                   if(att.Name == 'RateCardLine' && att.cscfga__Product_Configuration__r.cscfga__Key__c == pc.cscfga__Key__c) {
                                                                       rateCardJson = att.cscfga__Value__c;
                                                                   }
                                                                   
                                                                   if(att.Name == 'Usage Profile' && attDefs.get(att.cscfga__Attribute_Definition__c).cscfga__Type__c == 'Lookup' &&
                                                                      att.cscfga__Product_Configuration__r.cscfga__Key__c == pc.cscfga__Key__c) {
                                                                          usageProfileId = att.cscfga__Value__c;
                                                                      }
                                                                   
                                                                   if('Volume__c' == attDefs.get(att.cscfga__Attribute_Definition__c).cscfga__configuration_output_mapping__c
                                                                      && att.cscfga__Product_Configuration__r.cscfga__Key__c == pc.cscfga__Key__c && String.isNotBlank(att.cscfga__Value__c)) {
                                                                          volume = Integer.valueOf(att.cscfga__Value__c);
                                                                      }
                                                               }
                                                               
                                                               if(String.isNotBlank(rateCardJson)) {
                                                                   Map<String,Object> jsonResult = (Map<String,Object>) JSON.deserializeUntyped(rateCardJson);
                                                                   List<Object> rateLines = (List<Object>) jsonResult.get('rateCardLines');
                                                                   Decimal sumVoiceRevenue = 0;
                                                                   Decimal sumDataRevenue = 0;
                                                                   Decimal sumVoiceCost = 0;
                                                                   Decimal sumDataCost = 0;
                                                                   
                                                                   for(Object rateLineObj : rateLines){
                                                                       Map<String,Object> rateLine = (Map<String,Object>) rateLineObj;
                                                                       string rateLineName = String.valueOf(rateLine.get('name'));
                                                                       string quotePriceString = String.valueOf(rateLine.get('quote_price__c'));   
                                                                       string qouteCostString = String.valueOf(rateLine.get('quote_cost__c'));
                                                                       string quotePercentageDiscount = String.valueOf(rateLine.get('quote_price_percentage_discount_value__c'));
                                                                       boolean profitloss = Boolean.valueOf(rateLine.get('profit_loss__c') != null ? rateLine.get('profit_loss__c') : 'false');
                                                                       Decimal quotePrice = 0;
                                                                       Decimal qouteCost = 0;
                                                                       Decimal quotePercentage;
                                                                       system.debug(quotePriceString);
                                                                       if(quotePercentageDiscount != null && quotePercentageDiscount != ''){
                                                                           quotePercentage = Decimal.valueOf(quotePercentageDiscount) / 100;
                                                                           quotePrice = Decimal.valueOf(quotePriceString) - Decimal.valueOf(quotePriceString) * quotePercentage;
                                                                       }
                                                                       else
                                                                           quotePrice = Decimal.valueOf(quotePriceString);
                                                                       if(qouteCostString != null)
                                                                           qouteCost = Decimal.valueOf(qouteCostString);
                                                                       Usage_Profile__c usageProfile = usageProfiles.get(usageProfileId);
                                                                       if(profitloss) {    
                                                                           WrapperRevenueCost wrapper = GetRevenueForCallType(rateLineName, quotePrice, qouteCost, usageProfile);
                                                                           sumVoiceRevenue = sumVoiceRevenue + wrapper.revenue;
                                                                           sumVoiceCost = sumVoiceCost + wrapper.cost;
                                                                       }       
                                                                       else if(usageProfile != null && usageProfile.Expected_Data_per_User__c != null) {
                                                                           //sumDataRevenue = sumDataRevenue + usageProfile.Expected_Data_per_User__c * (quotePrice * 0.01);
                                                                           //sumDataCost = sumDataCost + usageProfile.Expected_Data_per_User__c * qouteCost;
                                                                       }
                                                                   }
                                                                   
                                                                   pc.Usage_Profile_Data_Cost__c = sumDataCost * volume;
                                                                   pc.Usage_Profile_Data_Revenue__c = sumDataRevenue * volume;
                                                                   pc.Usage_Profile_Voice_Cost__c = sumVoiceCost * volume;
                                                                   pc.Usage_Profile_Voice_Revenue__c = sumVoiceRevenue * volume;
                                                               }
                                                           }
                                                       }
                                                   }
                                               }
    
    public static WrapperRevenueCost GetRevenueForCallType(string rateLineName, decimal quotePrice, decimal quoteCost, Usage_Profile__c usageProfile) {
        WrapperRevenueCost wrapper = new WrapperRevenueCost();
        wrapper.cost = 0;
        wrapper.revenue = 0;
        decimal callTypeMinutesPerUserPerMonth = 0;
        decimal quoteFactor = 0.01;
        decimal reallocation = 0;
        decimal voiceRoamingEU = usageProfile.Voice_Roaming_EU__c != null ? usageProfile.Voice_Roaming_EU__c : 0;
        decimal otherBTMinutesTotal = usageProfile.Other_BT_Minutes_Total__c != 0 && usageProfile.Other_BT_Minutes_Total__c != null ? usageProfile.Other_BT_Minutes_Total__c : 1;
        if(rateLineName != ''){
            if(rateLineName == 'Data Usage (MB)')
                callTypeMinutesPerUserPerMonth = usageProfile.Expected_Data_per_User__c;
            if(rateLineName == 'Domestic Text')
                callTypeMinutesPerUserPerMonth = usageProfile.Domestic_Text__c;
            if(rateLineName == 'UK Fixed Line Numbers') {
                callTypeMinutesPerUserPerMonth = usageProfile.UK_Fixed_Line_Numbers__c;
                quoteFactor *= 1.12;
                reallocation = ((usageProfile.UK_Fixed_Line_Numbers__c != null ? usageProfile.UK_Fixed_Line_Numbers__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8);
            }
            if(rateLineName == 'FreePhone') {
                callTypeMinutesPerUserPerMonth = usageProfile.FreePhone_BT__c;
                quoteFactor *= 1;
                reallocation = ((usageProfile.FreePhone_BT__c != null ? usageProfile.FreePhone_BT__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8);
            }
            if(rateLineName.contains('IDD')){
                if(rateLineName.contains('Zone 1')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.IDD_Zone_1__c;
                    quoteFactor *= 1.12;
                }
                if(rateLineName.contains('Zone 2')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.IDD_Zone_2__c;
                    quoteFactor *= 1.08;
                }
                if(rateLineName.contains('Zone 3')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.IDD_Zone_3__c;
                    quoteFactor *= 1.08;
                }
                if(rateLineName.contains('Zone 4')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.IDD_Zone_4__c;
                    quoteFactor *= 1.06;
                }
                if(rateLineName.contains('Zone 5')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.IDD_Zone_5__c;
                    quoteFactor *= 1.07;
                }
                if(rateLineName.contains('Zone 6')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.IDD_Zone_6__c;
                    quoteFactor *= 1.08;
                }
            }
            if(rateLineName.contains('Voice Roaming')){
                if(rateLineName.contains('Voice Roaming - EU')) {
                    callTypeMinutesPerUserPerMonth = (usageProfile.Voice_Roaming_EU__c != null ? usageProfile.Voice_Roaming_EU__c : 0) * 0.2 * 0.2;
                    quoteFactor *= 1.03;
                }
                if(rateLineName.contains('Zone 3')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.Voice_Roaming_Zone_3_RoW__c;
                    quoteFactor *= 1.05;
                }
                if(rateLineName.contains('Zone 4')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.Voice_Roaming_Zone_4_USA_Canada__c;
                    quoteFactor *= 1.04;
                }
                if(rateLineName.contains('Zone 5')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.Voice_Roaming_Zone_5__c;
                    quoteFactor *= 1.03;
                }
                if(rateLineName.contains('Zone 6')) {
                    callTypeMinutesPerUserPerMonth = usageProfile.Voice_Roaming_Zone_6_RoW__c;
                    quoteFactor *= 1.06;
                }
            }
            if(rateLineName == 'Own Account Fixed Line Numbers') {
                callTypeMinutesPerUserPerMonth = usageProfile.Own_Account_Fixed_Line_Numbers__c;
                quoteFactor *= 1.12;
                reallocation = ((usageProfile.Own_Account_Fixed_Line_Numbers__c != null ? usageProfile.Own_Account_Fixed_Line_Numbers__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8);
            }
            if(rateLineName == 'Own Account Mobiles') {
                callTypeMinutesPerUserPerMonth = usageProfile.Own_Account_Mobiles__c;
                quoteFactor *= 1.2;
                reallocation = ((usageProfile.Own_Account_Mobiles__c != null ? usageProfile.Own_Account_Mobiles__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8);
            }
            if(rateLineName == 'Other BT Mobiles') {
                callTypeMinutesPerUserPerMonth = usageProfile.Other_BT_Mobiles__c;
                quoteFactor *= 1.2;
                reallocation = ((usageProfile.Other_BT_Mobiles__c != null ? usageProfile.Other_BT_Mobiles__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8);
            }
            if(rateLineName == 'Other UK Mobile Operators') {
                callTypeMinutesPerUserPerMonth = usageProfile.Other_UK_Mobile_Operators__c;
                quoteFactor *= 1.22;
                reallocation = ((usageProfile.Other_UK_Mobile_Operators__c != null ? usageProfile.Other_UK_Mobile_Operators__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8) + voiceRoamingEU * 0.8 * 0.2;             
            }
            if(rateLineName == 'Non Geographic') {
                callTypeMinutesPerUserPerMonth = usageProfile.Non_Geographic__c;
                quoteFactor *= 1.11;
                reallocation = ((usageProfile.Non_Geographic__c != null ? usageProfile.Non_Geographic__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8);        
            }
            if(rateLineName.contains('Roaming Inter-Country')) {
                callTypeMinutesPerUserPerMonth = usageProfile.Roaming_Inter_Country__c;
                quoteFactor *= 1;
            }
            if(rateLineName.contains('Voicemail')) {
                callTypeMinutesPerUserPerMonth = usageProfile.Voicemail__c;
                quoteFactor *= 1.41;
                reallocation = ((usageProfile.Voicemail__c != null ? usageProfile.Voicemail__c : 0) / otherBTMinutesTotal * voiceRoamingEU * 0.8);      
            }
            
            if(callTypeMinutesPerUserPerMonth == null) callTypeMinutesPerUserPerMonth = 0;
            
            wrapper.revenue = (callTypeMinutesPerUserPerMonth + reallocation) * (quotePrice * quoteFactor);
            wrapper.cost = (callTypeMinutesPerUserPerMonth + reallocation) * quoteCost;
        }
        
        return wrapper;
    }
    
    public class WrapperRevenueCost {
        @TestVisible
        Decimal cost;
        @TestVisible
        Decimal revenue;
    }
}