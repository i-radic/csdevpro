public with sharing class CrfFlowController {
    List < Lines_to_Move__c > ltm;

	//Added As Part of the CR CR4574
	public String vfprofid { get; set; }
    public string opId;

    public CrfFlowController(ApexPages.StandardController controller) {
        opId = Apexpages.CurrentPage().getParameters().get('OppyId');
        //Added As Part of the CR CR4574
        vfprofid = Apexpages.CurrentPage().getParameters().get('profid');
    }


    public Flow.Interview.BusinessMovers myFlow { get; set; }
    public Flow.Interview.Broadband broadband { get; set; }
        
    public string VfOppyName;
    public string VfConId;
    public String dcid { get; set; }
     
    public List < Lines_to_Move__c > getLinesToMoveList() {
        if ((myFlow != null)) {
            ltm = [SELECT Id, name, Provide_crd__c, Cease_crd__c, Product_Type__c, CreatedDate FROM Lines_to_Move__c WHERE Related_to_CRF__c = : myFlow.crf_id ORDER BY CreatedDate Desc];
            return ltm;
        } else {
            return null;
        }

    }


    public String getVfOppyname() {
        string opName = Apexpages.CurrentPage().getParameters().get('OppyName');
        if (opName != null) {
            return opName;
        } else {
            return 'No Opportunity Name';
        }
    }

    public String getVfOppyId() {

        if (opId != null) {
            return opId;
        } else {
            return 'No Opportunity ID';
        }
    }

    public String getVfConId() {
        List < OpportunityContactRole > opConRole = [Select ContactId from OpportunityContactRole where OpportunityId = : opId LIMIT 1];
        if (opConRole.size() > 0) {
            String ConId = opConRole[0].ContactId;
            return ConId;
        } else {
            return null;
        }
    }

    public String getVfAccountName() {
        string AcctName = Apexpages.CurrentPage().getParameters().get('AccountName');
        if (AcctName != null) {
            return AcctName;
        } else {
            return 'No Account Name';
        }
    }

    public String getVfAccountId() {
        string AcctId = Apexpages.CurrentPage().getParameters().get('AccountId');
        if (AcctId != null) {
            return AcctId;
        } else {
            return 'No Account ID';
        }
    }
    
    public String getVfBBLineOf() {
        String BBLineOfName = Apexpages.CurrentPage().getParameters().get('CF00N20000002oGtq');
        if (BBLineOfName != null) {
        	try{
        		CRF__c crf = [SELECT Id, Name FROM CRF__c WHERE Name =: BBLineOfName];
            	return crf.Id;
        	}
        	catch(Exception e){
        		return null;
        	}
        } else {
            return null;
        }
    }

    public String getmyID() {
        if (myFlow == null) return 'na';
        else return myFlow.crf_id;
    }

    public String getBroadbandID() {
        if (broadband == null) return opId;
        else return broadband.crf_id;
    }

    public PageReference getCRFid() {
        PageReference p = new PageReference('/' + getmyID());
        p.setRedirect(true);
        system.debug('ADJ NewCRF Page' + p);
        return p;
    }

    public PageReference getBroadbandCRFid() {
        PageReference p = new PageReference('/' + getBroadbandID());
        p.setRedirect(true);
        return p;
    }

    public pagereference deleteCon() {
        List < Lines_to_move__c > lstacc1 = [Select id, Name from Lines_to_Move__c where id = : dcid];
        delete lstacc1;
        return null;
    }


}