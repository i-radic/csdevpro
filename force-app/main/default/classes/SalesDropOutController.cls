public class SalesDropOutController{

    /******************************************************************************************************************
     Name:  SalesDropOutController.class()
     Copyright © 2013  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    This class is used to retrieve the details of Sales Drop Out lead to be used by a component 
    for sending out emails to customers using a visualforce email template.
                                                           
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR                 DATE              DETAIL                           FEATURES
    1.0 -    Rita Opoku-Serebuoh    30/10/2013        INITIAL DEVELOPMENT              Initial Build: 
    
    ******************************************************************************************************************/
    
    public String sdoLeadId{get;set;}
    private Lead lead;
    
    public SalesDropOutController(){}
    
    public Lead getLead()
    {      
        if (lead== null && sdoLeadId != null){
            lead= [SELECT Is_Lead_Active__c, First_Email_Sent__c, Product_Group__c, Package_Name__c, Product_Group_Type__c, Contract__c, Email, Order_Completion_URL__c, SDOEmailTemplate__c FROM Lead WHERE Id =: sdoLeadId];
        } 
           
        return lead;     
    }
     
}