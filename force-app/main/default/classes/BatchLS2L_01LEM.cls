global class BatchLS2L_01LEM implements Database.Batchable<sobject>{
private String query;
Public Integer i = 0;


    global BatchLS2L_01LEM(String q){
        this.query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Lead> L2L = new List<Lead>();
        List<Lead> L2LHist = new List<Lead>();
        Set<id> acc = new Set<id>();
        Set<id> LSIds = new Set<id>();
        for(Sobject ls : scope){
            //create the lead
            Landscape_BTLB__c lsrec= (Landscape_BTLB__c)ls;
            String CallsHist = null;
            String LinesHist = null;
            String BBHist = null;
            String MobileHist = null;
            String SwitchHist = null;
            String LANHist = null;
            String WANHist = null;
            LSIds.add(lsrec.Id); //add all landscape id into list for history check
            String DescStr = 'The alert has been generated based on a landscape with an upcoming contract expiry date in the following area(s):';
            if (lsrec.xLeads_Flag_Calls__c == 'True'){
                  DescStr = DescStr + '\n Calls ';
                  CallsHist = 'True';
            } 
            if (lsrec.xLeads_Flag_Lines__c == 'True'){
                  DescStr = DescStr + '\n Lines';
                  LinesHist = 'True';
            } 
            if (lsrec.xLeads_Flag_BB__c == 'True'){
                  DescStr = DescStr + '\n Broadband';
                  BBHist = 'True';
            } 
            if (lsrec.xLeads_Flag_Mobile__c == 'True'){
                  DescStr = DescStr + '\n Mobile';
                  MobileHist= 'True';
            } 
            if (lsrec.xLeads_Flag_Switch__c == 'True'){
                  DescStr = DescStr + '\n Switch';
                  SwitchHist = 'True';
            } 
            if (lsrec.xLeads_Flag_LAN__c == 'True'){
                  DescStr = DescStr + '\n LAN';
                  WANHist = 'True';
            } 
            if (lsrec.xLeads_Flag_WAN__c == 'True'){
                  DescStr = DescStr + '\n WAN';
                  WANHist = 'True';
            }                              
            system.debug('JMM LS: ' + lsrec);
            Lead L2Lrec = new Lead();
            //check customer name length and trim to 60 chars
            i = lsrec.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }         
            L2Lrec.Title = lsrec.Customer__r.Name.substring(0,i);
            L2Lrec.LastName = 'Landscape Lead: ' + lsrec.Customer__r.Name.substring(0,i);
            L2Lrec.Company = lsrec.Customer__r.Name;
            L2Lrec.Account__c = lsrec.Customer__c;
            L2Lrec.Description = DescStr ;
            L2Lrec.Landscape_BTLB__c = lsrec.Id;
            L2Lrec.LeadSource= 'LEM Landscape';
            L2Lrec.OwnerId = lsrec.Customer__r.OwnerId;
            L2Lrec.Owner_Department__c = lsrec.Customer__r.Owner.Department;
            L2Lrec.Calls_Last_Updated_Hist__c = CallsHist ;
            L2Lrec.Lines_Last_Updated_Hist__c = LinesHist ;
            L2Lrec.BB_Last_Updated_Hist__c = BBHist ;
            L2Lrec.Mobile_Last_Updated_Hist__c = MobileHist ;            
            L2Lrec.Switch_Last_Updated_Hist__c = SwitchHist ;
            L2Lrec.LAN_Last_Updated_Hist__c = LANHist ;            
            L2Lrec.WAN_Last_Updated_Hist__c = WANHist ;
            //check for an LEM customer before adding
            if(lsrec.Customer__r.Sector__c == 'BT Local Business'){
                L2L.add(L2Lrec);
            }
            //add account id in for date stamping
            acc.add(lsrec.Customer__c);
            
        }
        
     
        //run through the histroy records for the landscape to find who last updated that value that has triggered the lead creation.
        //stamp those detail onto the lead, and determine ownership based on division        
        List<Landscape_BTLB_History__c> LSHistCalls = [SELECT Id, Landscape_BTLB__r.Customer__r.Name, Landscape_BTLB__c, Question__c, New_Value__c, Div__c, Dept__c, CreatedBy.Name, CreatedDate FROM Landscape_BTLB_History__c WHERE Landscape_BTLB__c = :LSIds AND QuestionIdentifier__c = 'MKTG_BTLB:Q20' ORDER BY CreatedDate DESC LIMIT 1];
        List<Landscape_BTLB_History__c> LSHistLines = [SELECT Id, Landscape_BTLB__r.Customer__r.Name, Landscape_BTLB__c, Question__c, New_Value__c, Div__c, Dept__c, CreatedBy.Name, CreatedDate FROM Landscape_BTLB_History__c WHERE Landscape_BTLB__c = :LSIds AND QuestionIdentifier__c = 'MKTG_BTLB:Q25' ORDER BY CreatedDate DESC LIMIT 1];
        List<Landscape_BTLB_History__c> LSHistBB = [SELECT Id, Landscape_BTLB__r.Customer__r.Name, Landscape_BTLB__c, Question__c, New_Value__c, Div__c, Dept__c, CreatedBy.Name, CreatedDate FROM Landscape_BTLB_History__c WHERE Landscape_BTLB__c = :LSIds AND QuestionIdentifier__c = 'MKTG_BTLB:Q8' ORDER BY CreatedDate DESC LIMIT 1];
        List<Landscape_BTLB_History__c> LSHistMobile = [SELECT Id, Landscape_BTLB__r.Customer__r.Name, Landscape_BTLB__c, Question__c, New_Value__c, Div__c, Dept__c, CreatedBy.Name, CreatedDate FROM Landscape_BTLB_History__c WHERE Landscape_BTLB__c = :LSIds AND QuestionIdentifier__c = 'MKTG_BTLB:Q17' ORDER BY CreatedDate DESC LIMIT 1];
        List<Landscape_BTLB_History__c> LSHistSwitch = [SELECT Id, Landscape_BTLB__r.Customer__r.Name, Landscape_BTLB__c, Question__c, New_Value__c, Div__c, Dept__c, CreatedBy.Name, CreatedDate FROM Landscape_BTLB_History__c WHERE Landscape_BTLB__c = :LSIds AND QuestionIdentifier__c = 'MKTG_BTLB:Q14.2' ORDER BY CreatedDate DESC LIMIT 1];
        List<Landscape_BTLB_History__c> LSHistLAN = [SELECT Id, Landscape_BTLB__r.Customer__r.Name, Landscape_BTLB__c, Question__c, New_Value__c, Div__c, Dept__c, CreatedBy.Name, CreatedDate FROM Landscape_BTLB_History__c WHERE Landscape_BTLB__c = :LSIds AND QuestionIdentifier__c = 'MKTG_BTLB:Q10.3' ORDER BY CreatedDate DESC LIMIT 1];
        List<Landscape_BTLB_History__c> LSHistWAN = [SELECT Id, Landscape_BTLB__r.Customer__r.Name, Landscape_BTLB__c, Question__c, New_Value__c, Div__c, Dept__c, CreatedBy.Name, CreatedDate FROM Landscape_BTLB_History__c WHERE Landscape_BTLB__c = :LSIds AND QuestionIdentifier__c = 'MKTG_BTLB:Q4.3' ORDER BY CreatedDate DESC LIMIT 1];

        for (Lead l:L2L){
            string jmOwner = null;
            for (Landscape_BTLB_History__c LSH:LSHistCalls ){
            //check customer name length and trim to 60 chars
            i = LSH.Landscape_BTLB__r.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }               
                if(l.Landscape_BTLB__c == LSH.Landscape_BTLB__c && l.Calls_Last_Updated_Hist__c == 'True'){
                    l.Calls_Last_Updated_Hist__c = LSH.Div__c+': '+LSH.CreatedBy.Name+', '+LSH.CreatedDate.format();
                    //based on division make changes to the lead
                    if(LSH.Div__c.contains('COT') && jmOwner != 'BTLB'){jmOwner = 'COT';}
                    else if(LSH.Div__c == 'BS Classic' && jmOwner != 'BTLB'){jmOwner = 'BS';}
                    else {jmOwner = 'BTLB';} 
                        
                    if(jmOwner == 'COT'){
                        l.OwnerId = '00520000001U8s0'; //Adam Rowe-Bland
                        l.LeadSource='COT Landscape';
                        l.LastName = 'COT Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }
                    if(jmOwner == 'BS'){
                        l.LeadSource='BS Classic Landscape';
                        l.LastName = 'BS Classic Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }   
                    if(jmOwner == 'BTLB'){
                        l.LeadSource='LEM Landscape';
                        l.LastName = 'Landscape Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }                                
                }
            }
            for (Landscape_BTLB_History__c LSH:LSHistLines ){
            //check customer name length and trim to 60 chars
            i = LSH.Landscape_BTLB__r.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }                   
                if(l.Landscape_BTLB__c == LSH.Landscape_BTLB__c && l.Lines_Last_Updated_Hist__c == 'True'){
                    l.Lines_Last_Updated_Hist__c = LSH.Div__c+': '+LSH.CreatedBy.Name+', '+LSH.CreatedDate.format();
                    //based on division make changes to the lead
                    if(LSH.Div__c.contains('COT') && jmOwner != 'BTLB'){jmOwner = 'COT';}
                    else if(LSH.Div__c == 'BS Classic' && jmOwner != 'BTLB'){jmOwner = 'BS';}
                    else {jmOwner = 'BTLB';} 
                        
                    if(jmOwner == 'COT'){
                        l.OwnerId = '00520000001U8s0'; //Adam Rowe-Bland
                        l.LeadSource='COT Landscape';
                        l.LastName = 'COT Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }
                    if(jmOwner == 'BS'){
                        l.LeadSource='BS Classic Landscape';
                        l.LastName = 'BS Classic Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }   
                    if(jmOwner == 'BTLB'){
                        l.LeadSource='LEM Landscape';
                        l.LastName = 'Landscape Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }     
                }
            }
            for (Landscape_BTLB_History__c LSH:LSHistBB ){
            //check customer name length and trim to 60 chars
            i = LSH.Landscape_BTLB__r.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }                   
                if(l.Landscape_BTLB__c == LSH.Landscape_BTLB__c && l.BB_Last_Updated_Hist__c == 'True'){
                    l.BB_Last_Updated_Hist__c = LSH.Div__c+': '+LSH.CreatedBy.Name+', '+LSH.CreatedDate.format();
                    //based on division make changes to the lead
                    if(LSH.Div__c.contains('COT') && jmOwner != 'BTLB'){jmOwner = 'COT';}
                    else if(LSH.Div__c == 'BS Classic' && jmOwner != 'BTLB'){jmOwner = 'BS';}
                    else {jmOwner = 'BTLB';} 
                        
                    if(jmOwner == 'COT'){
                        l.OwnerId = '00520000001U8s0'; //Adam Rowe-Bland
                        l.LeadSource='COT Landscape';
                        l.LastName = 'COT Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }
                    if(jmOwner == 'BS'){
                        l.LeadSource='BS Classic Landscape';
                        l.LastName = 'BS Classic Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }   
                    if(jmOwner == 'BTLB'){
                        l.LeadSource='LEM Landscape';
                        l.LastName = 'Landscape Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }     
                }
            }
            for (Landscape_BTLB_History__c LSH:LSHistMobile ){
            //check customer name length and trim to 60 chars
            i = LSH.Landscape_BTLB__r.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }                   
                if(l.Landscape_BTLB__c == LSH.Landscape_BTLB__c && l.Mobile_Last_Updated_Hist__c == 'True'){
                    l.Mobile_Last_Updated_Hist__c = LSH.Div__c+': '+LSH.CreatedBy.Name+', '+LSH.CreatedDate.format();
                    //based on division make changes to the lead
                    if(LSH.Div__c.contains('COT') && jmOwner != 'BTLB'){jmOwner = 'COT';}
                    else if(LSH.Div__c == 'BS Classic' && jmOwner != 'BTLB'){jmOwner = 'BS';}
                    else {jmOwner = 'BTLB';} 
                        
                    if(jmOwner == 'COT'){
                        l.OwnerId = '00520000001U8s0'; //Adam Rowe-Bland
                        l.LeadSource='COT Landscape';
                        l.LastName = 'COT Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }
                    if(jmOwner == 'BS'){
                        l.LeadSource='BS Classic Landscape';
                        l.LastName = 'BS Classic Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }   
                    if(jmOwner == 'BTLB'){
                        l.LeadSource='LEM Landscape';
                        l.LastName = 'Landscape Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }     
                }
            }
            for (Landscape_BTLB_History__c LSH:LSHistSwitch ){
            //check customer name length and trim to 60 chars
            i = LSH.Landscape_BTLB__r.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }                   
                if(l.Landscape_BTLB__c == LSH.Landscape_BTLB__c && l.Switch_Last_Updated_Hist__c == 'True'){
                    l.Switch_Last_Updated_Hist__c = LSH.Div__c+': '+LSH.CreatedBy.Name+', '+LSH.CreatedDate.format();
                    //based on division make changes to the lead
                    if(LSH.Div__c.contains('COT') && jmOwner != 'BTLB'){jmOwner = 'COT';}
                    else if(LSH.Div__c == 'BS Classic' && jmOwner != 'BTLB'){jmOwner = 'BS';}
                    else {jmOwner = 'BTLB';} 
                        
                    if(jmOwner == 'COT'){
                        l.OwnerId = '00520000001U8s0'; //Adam Rowe-Bland
                        l.LeadSource='COT Landscape';
                        l.LastName = 'COT Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }
                    if(jmOwner == 'BS'){
                        l.LeadSource='BS Classic Landscape';
                        l.LastName = 'BS Classic Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }   
                    if(jmOwner == 'BTLB'){
                        l.LeadSource='LEM Landscape';
                        l.LastName = 'Landscape Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }     
                }
            }
            for (Landscape_BTLB_History__c LSH:LSHistLAN ){
            //check customer name length and trim to 60 chars
            i = LSH.Landscape_BTLB__r.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }                   
                if(l.Landscape_BTLB__c == LSH.Landscape_BTLB__c && l.LAN_Last_Updated_Hist__c == 'True'){
                    l.LAN_Last_Updated_Hist__c = LSH.Div__c+': '+LSH.CreatedBy.Name+', '+LSH.CreatedDate.format();
                    //based on division make changes to the lead
                    if(LSH.Div__c.contains('COT') && jmOwner != 'BTLB'){jmOwner = 'COT';}
                    else if(LSH.Div__c == 'BS Classic' && jmOwner != 'BTLB'){jmOwner = 'BS';}
                    else {jmOwner = 'BTLB';} 
                        
                    if(jmOwner == 'COT'){
                        l.OwnerId = '00520000001U8s0'; //Adam Rowe-Bland
                        l.LeadSource='COT Landscape';
                        l.LastName = 'COT Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }
                    if(jmOwner == 'BS'){
                        l.LeadSource='BS Classic Landscape';
                        l.LastName = 'BS Classic Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }   
                    if(jmOwner == 'BTLB'){
                        l.LeadSource='LEM Landscape';
                        l.LastName = 'Landscape Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }     
                }
            }
            for (Landscape_BTLB_History__c LSH:LSHistWAN ){
            //check customer name length and trim to 60 chars
            i = LSH.Landscape_BTLB__r.Customer__r.Name.length();
            if (i > 60) {
                i = 60;
            }                   
                if(l.Landscape_BTLB__c == LSH.Landscape_BTLB__c && l.WAN_Last_Updated_Hist__c == 'True'){
                    l.WAN_Last_Updated_Hist__c = LSH.Div__c+': '+LSH.CreatedBy.Name+', '+LSH.CreatedDate.format();
                    //based on division make changes to the lead
                    if(LSH.Div__c.contains('COT') && jmOwner != 'BTLB'){jmOwner = 'COT';}
                    else if(LSH.Div__c == 'BS Classic' && jmOwner != 'BTLB'){jmOwner = 'BS';}
                    else {jmOwner = 'BTLB';} 
                        
                    if(jmOwner == 'COT'){
                        l.OwnerId = '00520000001U8s0'; //Adam Rowe-Bland
                        l.LeadSource='COT Landscape';
                        l.LastName = 'COT Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }
                    if(jmOwner == 'BS'){
                        l.LeadSource='BS Classic Landscape';
                        l.LastName = 'BS Classic Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }   
                    if(jmOwner == 'BTLB'){
                        l.LeadSource='LEM Landscape';
                        l.LastName = 'Landscape Lead: ' + LSH.Landscape_BTLB__r.Customer__r.Name.substring(0,i);
                    }     
                }
            }
        insert l;
        }
        
        //set date on account to prevent recreation of leads, referenced in batch creation
        Map<Id, Account> acctsToUpdate = new Map<ID, Account>([select Id, xLead_Last_Created__c FROM Account WHERE Id in :acc]); 
        for (Account acct : acctsToUpdate.values()) {
            acct.xLead_Last_Created__c = System.Today();
        }
        StaticVariables.setAccountAfterDontRun(True); 
        update acctsToUpdate.values();
    }
    
global void finish(Database.BatchableContext BC){
    //create next batch to run
    if(!Test.isRunningTest()){  
        BatchSchedule__c b = BatchSchedule__c.getOrgDefaults();
    
        DateTime n = datetime.now().addMinutes(2);
        String cron = '';
    
        cron += n.second();
        cron += ' ' + n.minute();
        cron += ' ' + n.hour();
        cron += ' ' + n.day();
        cron += ' ' + n.month();
        cron += ' ' + '?';
        cron += ' ' + n.year();
    
        b.scheduled_id3__c = System.schedule('LS2L Batch 3', cron, new BatchLS2L_Schedule03());
    
        update b; 
    } 

}
}