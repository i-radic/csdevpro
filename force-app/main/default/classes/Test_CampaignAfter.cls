@isTest
private class Test_CampaignAfter {
    static Campaign c;
    
    static{
        List<Campaign_Call_Status__c> ccsToInsert = new List<Campaign_Call_Status__c>();
        Campaign_Call_Status__c ccs1 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status1', Responded__c = true, Default__c=true);
        Campaign_Call_Status__c ccs2 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status2', Responded__c = false, Default__c=false);
        Campaign_Call_Status__c ccs3 = new Campaign_Call_Status__c(Campaign_Type__c='default_test', Status__c='Test_Status3', Responded__c = false, Default__c=false);
        ccsToInsert.add(ccs1);
        ccsToInsert.add(ccs2);
        ccsToInsert.add(ccs3);
        insert ccsToInsert;
        
        //Insert Test
        Date enddate = date.today() + 1;
        c = new Campaign(Name='TestCampaign', Campaign_Type__c='BTLB', Type='Telemarketing', Status='Planned', 
            Campaign_Type_For_Status__c = 'default_test', X_Day_Rule__c = 3, EndDate = enddate, isActive=true, Data_Source__c='Self Generated');
        insert c;
    }

    static testMethod void insertPostCheckCampaign() {
        Test.startTest();
        List<CampaignMemberStatus> listCampaignMemberStatuses = [select id from CampaignMemberStatus where CampaignId = :c.Id];
        System.assertEquals(listCampaignMemberStatuses.size(), 3);//there are 2 x test_statuses
        Test.stopTest();
    } 
}