public class CreateBulkCampaigns {
 /**********************************************************************
     Name:  CreateBulkCampaigns.class()
     Copyright © 2010  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    This Class is called from the Bulk Campaign Create and acts as a wrapper to the BatchApex methods
    which process campaignstaging records and turn them into campaigns and Campaign members.
    This class is the custom controller for the VisualForce page where users can import campaign 
    data either using a browse button to pick up the CSV file(for BTLB) or using a pre-loaded batch
    loaded in using the Apex dataloader.
    
    Corporate users will not have the ability to browse for CSV files as their source files exceed the supported
    file size (500kb).
    
    Once a number of validation checks are done, the batch apex routines are called and the raw data is processed
    into child campaigns(for BTLB) and campaign members (for Corporate and BTLB).
                                                     
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
    1.0 -    Greg Scarrtott    2/05/2010        INITIAL DEVELOPMENT              Initial Build: 
    
    ***********************************************************************/
    
    public Bulk_Campaign_Run__c bulkcamprun {get;private set;}    
    public list<Bulk_Campaign_Run__c> listcamprun {get;set;}
    public string campaignid = '';
    public string campaignname {get;private set;}
    private blob filebody;
    private integer filesize;
    public boolean isBTLB {get;private set;}
    
    
    //Constants
    final string CAMPAIGN_TYPE_CORPORATE='Corporate';
  
    public void setFileBody(Blob fileBody)  {
                this.fileBody = fileBody;       
                setFileSize(this.fileBody.size());  
    }   
    
    public Blob getFileBody()   {
                return this.fileBody;   
    }
    
    public Integer getFileSize()    {       
        return this.fileSize;   }   
        
    public void setFileSize(Integer fileSize)   {
        this.fileSize = fileSize;   
    }   
    
    public PageReference save() { 
        return null;
    }
    public PageReference cancel() {
        return null;
    }
    public CreateBulkCampaigns(){
         
        //listcamprun = [select campaign__c,bulk_campaign_batch__c,createddate,campaign_Members_Created__c,ChildCampaignsCreated__c,Dummy_Contacts_Created__c,Tasks_Created__c,Total_Errors_In_Batch__c,Total_Records_in_Batch__c from Bulk_Campaign_Run__c order by createddate desc];
        String theId = ApexPages.currentPage().getParameters().get('id'); 
        bulkcamprun = new Bulk_Campaign_Run__c();
        bulkcamprun.campaign__c = theid;
        campaignid = theid;
        
        //Get the campaign name
        campaign camprecord = [select name,campaign_type__c from campaign where id = :theid];
        campaignname = camprecord.Name;
        if(camprecord.Campaign_Type__c==CAMPAIGN_TYPE_CORPORATE){
            this.isBTLB = false;
        }else{
            this.isBTLB = true;
        }
    }
    
    
    public pagereference docampaigncreate(){
        
        //First identify if the run is from a previously loaded batch
        //or the user is loading the data now from a CSV.
        if((bulkcamprun.Bulk_Campaign_Batch__c!=null && this.filebody !=null) || (bulkcamprun.Bulk_Campaign_Batch__c==null && this.filebody ==null)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'You must select EITHER an import file or a pre-loaded batch.');
            ApexPages.addMessage(myMsg);
            return null;
        }
        if(this.fileSize>400000){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The file you have selected should be < 400kb.');
            ApexPages.addMessage(myMsg);
            system.debug('filsizeint'+this.filesize);
            return null;
        }
        
        //If the file is an import file, create a new batch and run the load as usual.
        if(this.filebody !=null){
            //Get the contents of the import file
            String all = filebody.tostring();
            //Break it into separate lines
            String [] allrows = all.split('\n');
            List<bulkcampaignstaging__c> staginglist = new List<bulkcampaignstaging__c>();
            Bulkcampaignstaging__c stagingrec;
            
            //Only allow 4000 rows
            if(allrows.size() > 4000){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Maxium number of rows (4000) has been exceeded. You passed in ' + allrows.size());
                ApexPages.addMessage(myMsg);
                
                return null;
            }
            if(allrows.size() > 0){
                //Create a new batch record
                Bulk_campaign_batch__c newBatch = new Bulk_campaign_batch__c();
                newbatch.batch_name__c = userinfo.getname() + ':' + datetime.now();
                insert newBatch;
        
                //Build a list of our staging records
                Integer nCount = 0;
                for(String thisLine:allrows){
                    nCount++;
                    stagingrec= new Bulkcampaignstaging__c();
                    stagingrec.LE_or_SAC_Code__c = thisline;
                    stagingrec.Related_Bulk_Campaign_Batch__c = newBatch.id; 
                    stagingrec.Campaign_Member_Notes__c = bulkcamprun.New_Notes__c;
                    staginglist.add(stagingrec);
                    if(nCount == 999){
                        //This insert is OK and will only currently execute a maximum of 4 times (row limit is 4000)
                        insert staginglist;
                        staginglist.clear();
                        nCount = 0;
                    }   
                }
                
                //insert the staging records
                insert staginglist;
    
                //Now we can let the standard code run for loading a pre-loaded batch.
                
                //Just set the public batch variable to our new batch..
                bulkcamprun.Bulk_Campaign_Batch__c = newbatch.id;
                
            }else{
                //Nothing in the file, return
                return null;
            }
                
        }   
        //Remind the users about the refresh button
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'You can refresh the status of the load by pressing the "Refresh Bulk Campaign Progress" button.');
        ApexPages.addMessage(myMsg);
            
        //Create a new batch run record and put this in a table
        system.debug('campvalue is ' + bulkcamprun);
    
        Bulk_Campaign_Run__c newCampaignRun = new Bulk_Campaign_Run__c ();
        
        newcampaignrun.Campaign__c = campaignid;
        newcampaignrun.Bulk_Campaign_Batch__c = bulkcamprun.Bulk_Campaign_Batch__c;
        newcampaignrun.Delete_source_data_after_load__c = bulkcamprun.Delete_source_data_after_load__c;
        //Need to insert here to get the id to pass to the batch job
        
        insert newcampaignrun;
        system.debug('gsbatchid'+bulkcamprun.Bulk_Campaign_Batch__c);
        BatchCampaignCreate docampaigns = new BatchCampaignCreate();
        
        system.debug('GSSELECT Id, legalentityid__c,sac_code__c FROM bulkcampaignstaging__c where related_bulk_campaign_batch__c = \'' + bulkcamprun.Bulk_Campaign_Batch__c + '\'');
        
        docampaigns.query='SELECT Id, le_or_sac_code__c,ein__c,lead_locator_role__c,campaign_member_notes__c,Corp_System_Url__c,Corp_System_Text__c,Corp_Subject__c,Corp_Reminder_Days__c,Corp_Primary_Campaign_Source__c,Corp_Due_Date__c,Corp_Details__c,Corp_Collateral_Url__c,Corp_Collateral_Text__c FROM bulkcampaignstaging__c where related_bulk_campaign_batch__c = \'' + newcampaignrun.Bulk_Campaign_Batch__c + '\'';
        
        docampaigns.runrecord = newcampaignrun;
        ID batchprocessid = Database.executeBatch(docampaigns,50);  
        system.debug('ok:batch id is '+ batchprocessid);
        
        //Write the run record and include the batcjid
        newcampaignrun = docampaigns.runrecord;
        newcampaignrun.batchapexid__c = batchprocessid;
        
        update newcampaignrun;
        
        return null;
    }
    
    
}