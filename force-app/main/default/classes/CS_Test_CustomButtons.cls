@isTest
private class CS_Test_CustomButtons {
	private static cscfga__Product_Basket__c createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_TestDataFactory.generatePLReportConfigRecords();
		OLI_Sync__c os = new OLI_Sync__c();
		os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
		insert os;
		
		CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
		
        Account acct = new Account(name='test account');
        insert acct;
        
        Opportunity opp = new Opportunity(AccountId=acct.Id,StageName='Prospecting',CloseDate=date.today(),NextStep='Test',Next_Action_Date__c=date.today(),name='test OpportunityCI' ,
              TotalOpportunityQuantity = 0);
        insert opp;
        
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
          Name = 'Test Basket Trigger',
          cscfga__Opportunity__c = opp.Id,
          ReSign__c = true
        );
        insert basket;
        
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c(Name='BB Test', cscfga__Description__c = 'Test helper');
        insert prodDef;
        
        cscfga__attribute_definition__c attDef = new cscfga__attribute_definition__c(Name = 'RateCardLine', cscfga__product_definition__c = prodDef.Id);
        insert attDef;

        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c();
        config.cscfga__Product_Basket__c = basket.Id;
        config.cscfga__Product_Definition__c = prodDef.Id;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.Calculations_Product_Group__c = 'Future Mobile';
        insert config;
        
        cspmb__Price_Item_Rate_Card_Association__c rateCard = new cspmb__Price_Item_Rate_Card_Association__c();
        cspmb__Rate_Card_Line__c rateCardLine1 = new cspmb__Rate_Card_Line__c(
            Group__c = 'Test Group',
            Display_in_Contract__c = true
        );
        cspmb__Rate_Card_Line__c rateCardLine2 = new cspmb__Rate_Card_Line__c(
            Name = 'Test Group',
            Display_in_Contract__c = true
        );
        
        CustomButtonSynchronizeWithOpportunity.WrapperRateCardWithLineItems wrapper = new CustomButtonSynchronizeWithOpportunity.WrapperRateCardWithLineItems();
        wrapper.rateCard = rateCard;
        wrapper.rateCardLines.add(rateCardLine1);
        wrapper.rateCardLines.add(rateCardLine2);
        
        cscfga__attribute__c att = new cscfga__attribute__c(
            Name = 'RateCardLine',
            cscfga__Product_Configuration__c = config.Id,
            cscfga__Attribute_definition__c = attDef.Id,
            cscfga__Value__c = JSON.serialize(wrapper)
        );
        insert att;
        
        return basket;
	}
	private static testmethod void test_CustomButtonBasketApproval() {       
        cscfga__Product_Basket__c basket = createTestData();
		
		CustomButtonBasketApproval button = new CustomButtonBasketApproval();
		button.performAction(basket.Id);
	}
	
	private static testmethod void test_CustomButtonCloneBasket() {
        cscfga__Product_Basket__c basket = createTestData();
		
		CustomButtonCloneBasket button = new CustomButtonCloneBasket();
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_CustomButtonCompareBaskets() {
        cscfga__Product_Basket__c basket = createTestData();
		
		CustomButtonCompareBaskets button = new CustomButtonCompareBaskets();
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_CustomButtonCreditFunds() {
        cscfga__Product_Basket__c basket = createTestData();
		
		CustomButtonCreditFunds button = new CustomButtonCreditFunds();
		button.performAction(basket.Id);
	}
	
	private static testmethod void test_CustomButtonPLReport() {
        cscfga__Product_Basket__c basket = createTestData();
		
		CustomButtonPLReport button = new CustomButtonPLReport();
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_CustomButtonSynchronizeWithOpportunity() {
        cscfga__Product_Basket__c basket = createTestData();
       	CustomButtonSynchronizeWithOpportunity button = new CustomButtonSynchronizeWithOpportunity();
		button.performAction(basket.Id);
		
        basket.cscfga__Basket_Status__c = 'Valid';
        basket.csbb__Synchronised_with_Opportunity__c = true;
        update basket;
		
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_CustomButtonSyncWithOpp() {
        cscfga__Product_Basket__c basket = createTestData();
        basket.cscfga__Basket_Status__c = 'Valid';
        update basket;
		
		CustomButtonSyncWithOpp button = new CustomButtonSyncWithOpp();
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_CustomButtonUnderCFReview() {
        cscfga__Product_Basket__c basket = createTestData();
		
		CustomButtonUnderCFReview button = new CustomButtonUnderCFReview();
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_CustomButtonUnlock() {
        cscfga__Product_Basket__c basket = createTestData();
        basket.isBasketReadonly__c = true;
        update basket;
		
		CustomButtonUnlock button = new CustomButtonUnlock();
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_NoSolutions() {
        cscfga__Product_Basket__c basket = createTestData();
        cscfga__Product_Configuration__c pc = [SELECT Id, Calculations_Product_Group__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c =: basket.Id LIMIT 1];
        pc.Calculations_Product_Group__c = 'BT Net';
        update pc;
        
        CustomButtonBasketApproval button = new CustomButtonBasketApproval();
		button.performAction(basket.Id);		
	}
	
	private static testmethod void test_CustomButtonViewUsageProfile() {
        cscfga__Product_Basket__c basket = createTestData();
		
		CustomButtonViewUsageProfile button = new CustomButtonViewUsageProfile();
		button.performAction(basket.Id);		
	}
}