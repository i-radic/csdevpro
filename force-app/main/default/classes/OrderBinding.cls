global class OrderBinding{

    static webservice void BindOrderToObject(string objectId, string orderReference, string ein){
        List<Opportunity_Submitted_Order__c> existingBinds = 
            [SELECT 
                id, 
                order_reference__c, 
                Opportunity__c,
                Submitted_Count__c,
                Submitted_By_EIN__c
            FROM
                Opportunity_Submitted_Order__c
            WHERE 
                Opportunity__r.Opportunity_Id__c =: objectId AND
                order_reference__c =: orderReference];
                
        if (existingBinds.IsEmpty()){
            Opportunity_Submitted_Order__c newBind = new Opportunity_Submitted_Order__c();
            Opportunity oppty = [SELECT id FROM Opportunity WHERE Opportunity_Id__c=:objectId LIMIT 1];
            newBind.Opportunity__c = oppty.id;
            newBind.order_reference__c = orderReference;
            newBind.Submitted_Count__c = 1;
            newBind.Submitted_By_EIN__c = ein;
            insert newBind;            
        }
        else{
            Opportunity_Submitted_Order__c existingBind = existingBinds.get(0);
            existingBind.Submitted_Count__c += 1;
            existingBind.Submitted_By_EIN__c = ein;
            upsert existingBind;
        }            
    }
}