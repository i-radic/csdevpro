/**************************************************************************************************************************************************
Test Class Name : Test_BatchBTUserGroupMember
Class Name : BatchBTUserGroupMember
Description : This job will test the funcionality of BatchBTUserGroupMember class.
Version : V0.1
Created By Author Name : Phanikanth P
Date : 15/02/2019
Modified Date : 
*************************************************************************************************************************************************/

@isTest
public class Test_BatchBTUserGroupMember {
    @testSetup 
    static void setup() {
        List<User> userList=new List<User>();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        for(Integer i=0;i<10;i++){
            User u=new user();
            u.Alias='btUsr'+i;
            u.Email='testuser@bt.com'+i;
            u.IsActive=true;
            u.CommunityNickname='btEmployee'+i;
            u.EmailEncodingKey='UTF-8';
            u.LastName='Testing'+i;
            u.LanguageLocaleKey='en_US';
            u.LocaleSidKey='en_US';
            u.ProfileId = p.Id;
            u.EIN__c='123654'+i;
            u.TimeZoneSidKey='America/Los_Angeles';
            u.UserName='standarduser@testorg.com'+i;
            if(i<5){
                u.Department='BTLB Glasgow';
            }
            userList.add(u);
        }
        Database.upsert(userList); 
        
        BT_User_Group__c btUGroup=new BT_User_Group__c();
        btUGroup.Name='BTLB Glasgow';
        btUGroup.Active__c=true;
        insert btUGroup;
        
        List<user> uList=[select id from user where lastname like 'Testing%' limit 1];
        BT_User_Group_Member__c btUGM=new BT_User_Group_Member__c();
        btUGM.user__c=uList[0].id;
        btUGM.BT_User_Group__c=btUGroup.id;
        insert btUGM;
        
    }
    static testmethod void test() {
        test.startTest();
        BatchBTUserGroupMember bt=new BatchBTUserGroupMember();
        ID batchID=Database.executeBatch(bt);
        String sch = '0 0 2 * * ?';
        system.schedule('Test BT User Group Member Check', sch, bt); 
        test.stopTest();
        
    }
    
}