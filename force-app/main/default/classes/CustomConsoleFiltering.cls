global class CustomConsoleFiltering extends csbb.PluginManager.BasePlugin {
    
    /* CODE BEFORE REFACTORING - mpk
    global override Object invoke (Object basketId) {
        return this.getApprovedIds((String) basketId);
    }
   */

    global override Object invoke (Object input) {
        Map<String, String> inputMap = (Map<String, String>) input;
        String basketId = inputMap.get('basketId');
        return this.getApprovedIds((String) basketId);
    }

    public List<Id> getApprovedIds (String basketId) {
        Profile currentProfile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
		
        system.debug('::BP1::');
        
        ProductConfigurationStructure pcs = getJSONResource();
        
        system.debug('::BP2::');
        
        /* Get all offer category  associations */
        List<cscfga__Offer_Category_Association__c> ocaList = 
            [ 
                SELECT Id, cscfga__Configuration_Offer__c, 
                    cscfga__Configuration_Offer__r.Name,
                    cscfga__Configuration_Offer__r.Product_Definitions__c,
                    cscfga__Configuration_Offer__r.cscfga__Description__c,
                    cscfga__Configuration_Offer__r.Parent_Offer_Name__c,
                    cscfga__Product_Category__c,
                    cscfga__Product_Category__r.Name,
                    cscfga__Product_Category__r.cscfga__Parent_Category__r.Name
                FROM cscfga__Offer_Category_Association__c 
            ];
		
        system.debug('::BP2.5::');
        
        /* Get Basket */
        cscfga__Product_Basket__c basket = [SELECT
                                                Id, Sector__c, 
                                                    (SELECT Id, cscfga__Product_Definition__r.Name, 
                                                            cscfga__Product_Definition__r.cscfga__Product_Category__r.Name
                                                        FROM cscfga__Product_Configurations__r)
                                            FROM
                                                cscfga__Product_Basket__c
                                            WHERE
                                                Id = :basketId];
        System.debug('basket>>>'+basket);
        /* Go through all ocas and check for Sector and Profile */
        Set<Id> allIds = new Set<Id>();
        Set<Id> throwOutIds = new Set<Id>();
		system.debug('::BP3::');
        
        for(cscfga__Offer_Category_Association__c oca : ocaList){
            allIds.add(oca.cscfga__Product_Category__c); 
            allIds.add(oca.cscfga__Configuration_Offer__c); 

            if(pcs.isInvalid(basket, currentProfile.Name, oca)) {
                throwOutIds.add(oca.cscfga__Product_Category__c);
            }
            if(!pcs.isProductRulesValid(basket.cscfga__Product_Configurations__r, oca)) {
                throwOutIds.add(oca.cscfga__Configuration_Offer__c);   
            }
            if(!pcs.isDependencyExist(basket.cscfga__Product_Configurations__r,oca.cscfga__Configuration_Offer__r.Product_Definitions__c, oca)) {
                System.debug('>>11oca.cscfga__Configuration_Offer__r.Product_Definitions__c'+oca.cscfga__Configuration_Offer__r.Product_Definitions__c);
                 System.debug('11oca.cscfga__Product_Category__c>>>'+oca.cscfga__Product_Category__c);
                throwOutIds.add(oca.cscfga__Configuration_Offer__c);   
            }
            if(!pcs.isDependencyExist(basket.cscfga__Product_Configurations__r,oca.cscfga__Product_Category__r.Name, oca)) {
                System.debug('>>oca.cscfga__Product_Category__r.Name>>>'+oca.cscfga__Product_Category__r.Name);
                 System.debug('oca.cscfga__Product_Category__c>>>'+oca.cscfga__Product_Category__c);
                throwOutIds.add(oca.cscfga__Product_Category__c);   
            }
        }
               
        allIds.removeAll(throwOutIds);
	
        system.debug('::BP4::');
        
        return new List<Id>(allIds);
    }

    public static ProductConfigurationStructure getJSONResource(){
        Blob resource = CS_Utils.loadStaticResource('ProductConfigurationStructureJSON');
        System.debug('resource>>>'+Json.serializePretty(resource.toString()));
        ProductConfigurationStructure pcsObj = (ProductConfigurationStructure) JSON.deserialize(resource.toString(), ProductConfigurationStructure.class);
        System.debug('pcsObj>>>'+pcsObj);
        return pcsObj;
    }

    public class ProductConfigurationStructure {
        public Map<String, Set<String>> Sectors {get; set;}
        public Map<String, Set<String>> Profiles {get; set;}
        public Map<String, Set<String>> ProductRules {get; set;}
        public Map<String, Set<String>> DependencyRule{get; set;}
        

        public boolean isInvalid(cscfga__Product_Basket__c basket, String profileName, cscfga__Offer_Category_Association__c oca) {
            return (!isSectorPDsValid(basket.Sector__c, oca) || !isProfilePDsValid(profileName, oca) 
                        || !isCategoryValid(basket.cscfga__Product_Configurations__r, oca));
        }

        public boolean isSectorPDsValid(String sector, cscfga__Offer_Category_Association__c oca) {
            sector = String.isBlank(sector) ? '' : sector;
            Set<String> sectorPDs = Sectors.get(sector.toUpperCase());
            return sectorPDs != null && (sectorPDs.contains(oca.cscfga__Product_Category__r.Name) 
                    || sectorPDs.contains(oca.cscfga__Product_Category__r.cscfga__Parent_Category__r.Name)
                    || sectorPDs.isEmpty());
        }

        public boolean isProfilePDsValid(String profileName, cscfga__Offer_Category_Association__c oca) {
            Set<String> profilePDs = Profiles.get(profileName);
            return profilePDs != null && (profilePDs.contains(oca.cscfga__Product_Category__r.Name) 
                    || profilePDs.contains(oca.cscfga__Product_Category__r.cscfga__Parent_Category__r.Name)
                    || profilePDs.isEmpty());
        }

        public boolean isCategoryValid(List<cscfga__Product_Configuration__c> configs, cscfga__Offer_Category_Association__c oca) {
            for(cscfga__Product_Configuration__c product : configs) {
                Set<String> exclude = ProductRules.get(product.cscfga__Product_Definition__r.Name) != null ?
                                        ProductRules.get(product.cscfga__Product_Definition__r.Name) : ProductRules.get(product.cscfga__Product_Definition__r.cscfga__Product_Category__r.Name);
                System.debug('isCategoryValid exclude>>'+exclude);
                if(exclude != null && (exclude.contains(oca.cscfga__Product_Category__r.Name) ||
                    exclude.contains(oca.cscfga__Product_Category__r.cscfga__Parent_Category__r.Name)))
                    return false;
            }
            
            return true; 
        }

        public boolean isProductRulesValid(List<cscfga__Product_Configuration__c> configs, cscfga__Offer_Category_Association__c oca) {
            for(cscfga__Product_Configuration__c product : configs) {
                Set<String> exclude = ProductRules.get(product.cscfga__Product_Definition__r.Name) != null ?
                                        ProductRules.get(product.cscfga__Product_Definition__r.Name) : ProductRules.get(product.cscfga__Product_Definition__r.cscfga__Product_Category__r.Name);
                System.debug('isProductRulesValid exclude>>'+exclude);
                if(exclude != null && exclude.contains(formatName(oca.cscfga__Configuration_Offer__r.Product_Definitions__c)))
                    return false;
            }
            
            return true;
        }
        
        public boolean isDependencyExist(List<cscfga__Product_Configuration__c> configs, String dependentName, cscfga__Offer_Category_Association__c oca){
            dependentName = String.isNotBlank(dependentName) && dependentName.indexOf('[')>=0?dependentName.replace('[',''):dependentName;
            dependentName = String.isNotBlank(dependentName) && dependentName.indexOf(']')>=0?dependentName.replace(']',''):dependentName;
            if(DependencyRule!= null && !DependencyRule.isEmpty()){
               if(DependencyRule.keySet().contains(dependentName)){
                    Set<String> dependencySet = DependencyRule.get(dependentName);
                    if(!dependencySet.isEmpty()){
                        if(!configs.isEmpty()){
                            boolean parentFound= false;
                            for(cscfga__Product_Configuration__c pc : configs){
                                if(dependencySet.contains(pc.cscfga__Product_Definition__r.Name)){
                                    parentFound = true;
                                    break;
                                }
                            }
                            return parentFound;
                        }else{
                            return false;
                        }
                    }
                    
                }
            }
            return true;
        }
        public String formatName(String product) {
            return String.isNotBlank(product) ? product.substring(1, product.length()-1) : '';
        }
    }
}