@isTest
private class AddressBTSportSearch_Test {

     static testMethod void runPositiveTestCases() {
         
        Test_Factory.setProperty('IsTest', 'yes');

        StaticVariables.setContactIsRunBefore(False);
         User thisUser = [select id from User where id=:Userinfo.getUserId()];
         System.runAs( thisUser ){
    
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
    	}
        
        RecordType BTSOppyType = [select id from RecordType where SobjectType='Opportunity' and name ='BT Sport' limit 1];
        RecordType BTSHeaderType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='Header' limit 1];
        
        Date uDate = date.today().addDays(7);
                
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BTS';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.RecordTypeId = BTSOppyType.Id;
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});
        
        Contact cnt = new Contact();
        cnt.LastName = 'Test LastName';
        Database.SaveResult[] cntResult = Database.insert(new Contact [] {cnt});
        
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = cntResult[0].Id;
        ocr.OpportunityId = oppResult[0].Id;
        ocr.Role = 'Deleloper';
        Database.SaveResult[] ocrResult = Database.insert(new OpportunityContactRole [] {ocr});   
              
        BT_Sport__c Header1 = new BT_Sport__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Header1.RecordTypeId = BTSHeaderType.id;
        Header1.BT_Sport_MSA_Oppy__c = 'No';
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport__c [] {Header1});
         
        BTSalesforceServicesURL__c btsurl = new BTSalesforceServicesURL__c();
    btsurl.Name = 'CRMServices';
    btsurl.Url__c = 'https://eric-gotham-salesforceservices.bt.com/SF.CRMGateway/CRMService.asmx';
    insert btsurl;

        PageReference PageRef = Page.addressBtSportResults;
        test.setCurrentPage(PageRef);
        ApexPages.currentPage().getParameters().put('id',Header1.Id);
        ApexPages.currentPage().getParameters().put('Pc','w13 8qd');

        ApexPages.StandardController ctrl = new ApexPages.StandardController(Header1);
        AddressBTSportSearch abss = new AddressBTSportSearch(ctrl);
        abss.getTest();
        abss.CreateAddress();
        abss.Back();
        abss.ReDirect();
        abss.getAddresses();
        abss.getSelected();
        abss.getselectedAddress();
        
        BT_Sport__c Header2 = new BT_Sport__c();
        Header2.Opportunity__c = oppResult[0].id; 
        Header2.RecordTypeId = BTSHeaderType.id;
        Header2.BT_Sport_MSA_Oppy__c = 'No';
        
        
        Test_Factory.setProperty('IsTest', 'no');
        abss.CreateAddress();
        abss.Back();
        abss.ReDirect();
        abss.getSelected();
        
         
        AddressBTSportSearch abss2 = new AddressBTSportSearch(Header2); 
         
     }
     
}