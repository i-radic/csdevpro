/**
 * Created by puneetgosain on 09/12/2020.
 */

public class CS_PLLoadObserver implements cspl.OnLoadObserver {
    public Map<String, cspl.Calculation> execute(cscfga__Product_Basket__c basketDetails, Map<Id, cspl.ProductConfiguration> configDetails, Map<String, cspl.Calculation> calculations){
        System.debug('-----calculations--------'+calculations);
        System.debug('----basketDetails----'+JSON.serializePretty(basketDetails));
        System.debug('----configDetails----'+JSON.serializePretty(configDetails));

        return calculations;
    }
}