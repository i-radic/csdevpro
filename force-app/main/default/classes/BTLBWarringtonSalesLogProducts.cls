public with sharing class BTLBWarringtonSalesLogProducts {
    public BTLB_Warrington_Sales_Log_Products__c btlbWarringtonSalesLogProduct {get; set; }
    public string warringtonProduct {get;set;}
     public string btlbRecType{get; set;}
     public string recId {get; set;}
     public Id recName {get; set;}
     // public   BTLB_Warrington_Sales_Log__c a {get; set;}
     public Id recTypeId {get; set;}
    
    public BTLBWarringtonSalesLogProducts(ApexPages.StandardController stdController) {      
        btlbWarringtonSalesLogProduct = (BTLB_Warrington_Sales_Log_Products__c)stdController.getRecord();
        recTypeId = ApexPages.currentPage().getParameters().get('RecordType');
        warringtonProduct = ApexPages.currentPage().getParameters().get('WrngtonProduct');        
        recId= btlbWarringtonSalesLogProduct.RecordTypeId;
        
        if(recId == null || recId == '') {
            recId = ApexPages.currentPage().getParameters().get('recId');
        }
        if(Test_Factory.GetProperty('IsTest') != 'yes'){
            List<RecordType> bList = [Select Name, Id, DeveloperName From RecordType where id =: recId];
                
            btlbRecType = bList[0].Name;
            
            try{
                BTLB_Warrington_Sales_Log__c a = [SELECT Id,Name FROM BTLB_Warrington_Sales_Log__c WHERE Id =: warringtonProduct LIMIT 1];
                btlbWarringtonSalesLogProduct.Warrington_Sales_Log__c = a.Id;
                recName = a.Name;
            }
            catch (Exception ex){
            
            }
        }
        if(Test_Factory.GetProperty('IsTest') == 'yes'){
            btlbRecType = 'test';
            //recName = 'test';
        }
    }
    
    public BTLBWarringtonSalesLogProducts (BTLB_Warrington_Sales_Log_Products__c prod) {
        btlbWarringtonSalesLogProduct = prod;
        
    }
    
    public PageReference Save() {
        if(Test_Factory.GetProperty('IsTest') != 'yes'){
            upsert btlbWarringtonSalesLogProduct;
         } 

        ApexPages.addMessage(new  ApexPages.Message(ApexPages.Severity.Info,'id1'+btlbWarringtonSalesLogProduct.Id));
        PageReference retPageRef = new PageReference('/'+btlbWarringtonSalesLogProduct.Id);
        return retPageRef;
    }
    
    public PageReference OnLoad() {
        
        btlbWarringtonSalesLogProduct.Product_Group_Mobile__c = '';
        btlbWarringtonSalesLogProduct.Sale__c = '';
        btlbWarringtonSalesLogProduct.qQTY__c = null;
        btlbWarringtonSalesLogProduct.Product__c = '';
        btlbWarringtonSalesLogProduct.Term__c = '';
        btlbWarringtonSalesLogProduct.Save_Type__c = '';
        btlbWarringtonSalesLogProduct.VOL_Reference__c = '';
        
        return null;
    }
    
    public PageReference SaveNnew() {
        if(Test_Factory.GetProperty('IsTest') != 'yes'){
            upsert btlbWarringtonSalesLogProduct;
         }

        PageReference retPageRef = new PageReference('/apex/BTLBWarringtonSalesLogProduct?WrngtonProduct='+btlbWarringtonSalesLogProduct.Warrington_Sales_Log__c+'&recId='+recId);
       
        retPageRef.SetRedirect(false);      
        return retPageRef;
    }
}