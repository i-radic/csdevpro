global class BatchUpdateContacts implements Database.Batchable<SObject>{
	private String query;
	
	global BatchUpdateContacts(String q){
		this.query = q;
	}
	global Database.QueryLocator start(Database.BatchableContext BC){
		return Database.getQueryLocator(query);
	}
	global void execute(Database.BatchableContext BC, List<sObject> scope){
		system.debug('GS Execute method:' + scope.size());
		List<Contact> contacts = new List<Contact>();
		//Fire update trigger for these contacts
		update contacts;
	}
	
	global void finish(Database.BatchableContext BC){
	}
}