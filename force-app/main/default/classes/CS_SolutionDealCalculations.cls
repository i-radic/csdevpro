public class CS_SolutionDealCalculations {
    public Map<Id, cscfga__Product_Configuration__c> configs;
    public cscfga__Product_Basket__c basket;
    public Decimal minimumConnectionPeriod;
    public Decimal rebateValue;
    
    public CS_SolutionDealCalculations(cscfga__Product_Basket__c basket) {
        this.basket = basket;
        this.configs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{basket});

        Map<Id,cscfga__Product_Configuration__c> configsWithPrices = new Map<Id, cscfga__Product_Configuration__c>([
                SELECT
                    Mobile_Voice_One_Off_Charge__c,
                    Mobile_Voice_One_Off_Charge_Discounted__c, 
                    Mobile_Voice_Recurring_Charge__c,
                    Mobile_Voice_Recurring_Charge_Discounted__c,
                    Mobile_Voice_Recurring_Cost__c,
                    Mobile_Voice_One_Off_Cost__c,
                    Minimum_Connection_Period__c,
                    FCC_Total__c,
                    FM_Data__c,
                    Grouping__c,
                    cscfga__Parent_Configuration__r.cscfga__Quantity__c,
                    Number_of_Channels__c,
                    Solution_Monthly_ARPU__c,
                    Solution_NPV_GOM_PUPA__c,
                    Solution_Total_One_Off_Price__c,
                    Solution_Total_Recurring_Cost__c,
                    Solution_Total_Recurring_Price__c,
                    Solution_Total_One_Off_Cost__c,
                    Rebate_Value__c
                FROM cscfga__Product_Configuration__c
                WHERE Id IN :configs.keySet()
        ]);

        for (Id configId : configs.keySet()) {
            cscfga__Product_Configuration__c config = configs.get(configId);
            
            config.Minimum_Connection_Period__c = configsWithPrices.get(configId).Minimum_Connection_Period__c;
            config.FCC_Total__c = configsWithPrices.get(configId).FCC_Total__c;
            config.FM_Data__c = configsWithPrices.get(configId).FM_Data__c;
            config.Grouping__c = configsWithPrices.get(configId).Grouping__c;
            config.Number_of_Channels__c = configsWithPrices.get(configId).cscfga__Parent_Configuration__r.cscfga__Quantity__c;
            config.FixedLine_Mobile_Monthly_Charges__c = configsWithPrices.get(configId).Solution_Monthly_ARPU__c;
            config.NPV_GOM_per_annum__c = configsWithPrices.get(configId).Solution_NPV_GOM_PUPA__c;
            config.Mobile_Voice_One_Off_Charge__c = configsWithPrices.get(configId).Solution_Total_One_Off_Price__c;
            config.Mobile_Voice_Recurring_Charge__c = configsWithPrices.get(configId).Solution_Total_Recurring_Price__c;
            config.Mobile_Voice_Recurring_Cost__c = configsWithPrices.get(configId).Solution_Total_Recurring_Cost__c;
            config.Mobile_Voice_One_Off_Cost__c = configsWithPrices.get(configId).Solution_Total_One_Off_Cost__c;
            config.Rebate_Value__c = configsWithPrices.get(configId).Rebate_Value__c;
        }
    }

    public void calculateBasketAttributes() {
        Map<String, CS_SolutionDealCalculations.BasketRollUpSummary>  rollUps = new Map<String, CS_SolutionDealCalculations.BasketRollUpSummary>();

        //Basket summary roll-ups
        rollUps.put('Total Monthly Revenue', createBasketRollUp('Mobile_Voice_Recurring_Charge__c','Decimal',new Map<String, String>{'No Condition' => ''}, 'Add'));
        rollUps.put('Total One Off Revenue', createBasketRollUp('Mobile_Voice_One_Off_Charge__c','Decimal',new Map<String, String>{'No Condition' => ''}, 'Add'));
        rollUps.put('Tenure', createBasketRollUp('Tenure__c','Integer',new Map<String, String>{'is_Main_Component__c' => 'true'}, 'Max'));
        rollUps.put('No Of Users',createBasketRollUp('Number_of_users__c','Integer',new Map<String, String>{'is_Main_Component__c' => 'true'}, 'Add'));
        rollUps.put('Mobile One Off Cost', createBasketRollUp('Mobile_Voice_One_Off_Cost__c','Decimal',new Map<String, String>{'Calculations_Product_Group__c' => 'Future Mobile'}, 'Add'));
        rollUps.put('Mobile Recurring Cost', createBasketRollUp('Mobile_Voice_Recurring_Cost__c','Decimal',new Map<String, String>{'Calculations_Product_Group__c' => 'Future Mobile'}, 'Add'));
        rollUps.put('GM Upfront Cost',createBasketRollUp('GM_Upfront_Cost__c','Decimal',new Map<String, String>{'Calculations_Product_Group__c' => 'Cloud Voice'}, 'Add'));
        rollUps.put('GM Recurring Cost',createBasketRollUp('GM_Recurring_Cost__c','Decimal',new Map<String, String>{'Calculations_Product_Group__c' => 'Cloud Voice'}, 'Add'));
        rollUps.put('Rolling Airtime Fund', createBasketRollUp('Rolling_Air_Time__c','Decimal',new Map<String, String>{'Product_Definition_Name__c' => 'Future Mobile Credit Funds'}, 'Add'));
        rollUps.put('Staged Airtime Fund', createBasketRollUp('Staged_Air_Time__c','Decimal',new Map<String, String>{'Product_Definition_Name__c' => 'Future Mobile Credit Funds'}, 'Add'));
        rollUps.put('TechFund', createBasketRollUp('Tech_Fund__c','Decimal',new Map<String, String>{'Product_Definition_Name__c' => 'Future Mobile Credit Funds'}, 'Add'));
        rollUps.put('Product Definition Names', createBasketRollUp('Product_Definition_Name__c', 'String',new Map<String, String>{'No Condition' => ''}, 'ConcatenateUnique'));
        rollUps.put('FCC Total', createBasketRollUp('FCC_Total__c','Decimal',new Map<String, String>{'No Condition' => ''}, 'Add'));
        rollUps.put('Main Service Name', createBasketRollUp('Name', 'String',new Map<String, String>{'is_Main_Component__c' => 'true'}, 'ConcatenateUnique'));
        rollUps.put('Total Data', createBasketRollUp('FM_Data__c', 'Decimal',new Map<String, String>{'Grouping__c' => 'Data'}, 'Add'));
        rollUps.put('Total Data Charge', createBasketRollUp('Mobile_Voice_Recurring_Charge__c', 'Decimal',new Map<String, String>{'Grouping__c' => 'Data'}, 'Add'));
        rollUps.put('Total Data Cost', createBasketRollUp('Mobile_Voice_Recurring_Cost__c', 'Decimal',new Map<String, String>{'Grouping__c' => 'Data'}, 'Add'));
        rollUps.put('Total Voice Charge', createBasketRollUp('Mobile_Voice_Recurring_Charge__c', 'Decimal',new Map<String, String>{'Grouping__c' => 'Voice'}, 'Add'));
        rollUps.put('Total Voice Cost', createBasketRollUp('Mobile_Voice_Recurring_Cost__c', 'Decimal',new Map<String, String>{'Grouping__c' => 'Voice'}, 'Add'));
        rollUps.put('Voice Users',createBasketRollUp('Number_of_Channels__c', 'Decimal',new Map<String, String>{'Grouping__c' => 'Voice'}, 'Add')); 
        rollUps.put('Data Users',createBasketRollUp('Number_of_Channels__c', 'Decimal',new Map<String, String>{'Grouping__c' => 'Data'}, 'Add')); 
        rollUps.put('Config NPV GOM PUPA',createBasketRollUp('NPV_GOM_per_annum__c','Decimal',new Map<String, String>{'No Condition' => ''}, 'Add'));
        rollUps.put('Config ARPU',createBasketRollUp('FixedLine_Mobile_Monthly_Charges__c','Decimal',new Map<String, String>{'No Condition' => ''}, 'Add'));
        rollUps.put('Count Voice Configs',createBasketRollUp('','Decimal',new Map<String, String>{'Grouping__c' => 'Voice'}, 'Count'));
        rollUps.put('Count Data Configs',createBasketRollUp('','Decimal',new Map<String, String>{'Grouping__c' => 'Data'}, 'Count'));
        rollUps.put('Rebate Value',createBasketRollUp('Rebate_Value__c','Decimal',new Map<String, String>{'No Condition' => ''}, 'Add'));

        //Approval roll ups
        rollUps.put('Full Co Terminus', createBasketRollUp('SpecCon_Full_co_terminus__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Averaging Co Terminus', createBasketRollUp('SpecCon_Averaging_co_terminus__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Disconnect Allowance', createBasketRollUp('SpecCon_Disconnection_Allowance__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Competitive Clause', createBasketRollUp('SpecCon_Competitive_Clause__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Total Revenue Guarantee', createBasketRollUp('SpecCon_Total_Revenue_Guarantee__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Total Contract Guarantee', createBasketRollUp('SpecCon_Total_Contract_Guarantee__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Unlocking Fees', createBasketRollUp('SPecCon_Unlocking_Fees__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Competitive Benchmarking', createBasketRollUp('SpecCon_Competitive_Benchmarking__c', 'Boolean',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Logic'));
        rollUps.put('Payment Terms',  createBasketRollUp('Payment_Terms__c', 'String',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'ConcatenateUnique'));
        rollUps.put('Minimum Connection Period', createBasketRollUp('Minimum_Connection_Period__c', 'Decimal',new Map<String, String>{'Product_Definition_Name__c' => 'Special Conditions'}, 'Add'));

        calculateRollups(configs.values(), rollUps);

        minimumConnectionPeriod = (Decimal) rollUps.get('Minimum Connection Period').basketRollUpValue;
        rebateValue = (Decimal) rollUps.get('Rebate Value').basketRollUpValue;
        basket.Total_Charges__c = basket.cscfga__total_contract_value__c;
        basket.Tenure__c = (Decimal) rollUps.get('Tenure').basketRollUpValue;
        basket.Total_Billable_Revenue__c = ((Decimal) rollUps.get('Total Monthly Revenue').basketRollUpValue) + (Decimal) rollUps.get('Total One Off Revenue').basketRollUpValue;
        basket.Number_of_Users__c = (Decimal) rollUps.get('No Of Users').basketRollUpValue;
        basket.Product_Definitions__c = (String) rollUps.get('Product Definition Names').basketRollUpValue;
        basket.NPV_Tech_Fund__c = (Decimal) rollUps.get('TechFund').basketRollUpValue;
        basket.NPV_Staged_Air_Time__c = (Decimal) rollUps.get('Staged Airtime Fund').basketRollUpValue;
        basket.Rolling_Air_Time__c = (Decimal) rollUps.get('Rolling Airtime Fund').basketRollUpValue * basket.Tenure__c;
        basket.CS_Total_One_Off_Charge__c = (Decimal) rollUps.get('Total One Off Revenue').basketRollUpValue;
        basket.CS_Total_Recurring_Charge__c = (Decimal) rollUps.get('Total Monthly Revenue').basketRollUpValue; 
        
        if ((Decimal) rollUps.get('FCC Total').basketRollUpValue > 0) {
            basket.FCC_Flag__c = 1;
        }
        
        if (basket.Product_Definitions__c.contains('Cloud Voice')) {
            basket.Total_Cost__c = (Decimal) rollUps.get('GM Upfront Cost').basketRollUpValue + (Decimal) rollUps.get('GM Recurring Cost').basketRollUpValue * basket.Tenure__c;
        }
        
        if (basket.Product_Definitions__c.contains('Future Mobile')) {
            basket.Total_Cost__c = (Decimal) rollUps.get('Mobile One Off Cost').basketRollUpValue + (Decimal) rollUps.get('Mobile Recurring Cost').basketRollUpValue;
        }
        
        if (basket.Product_Definitions__c.contains('Future Mobile')) {
            basket.NPV_GOM_per_Sub_per_Annum__c = ((Decimal) rollUps.get('Config NPV GOM PUPA').basketRollUpValue) + (((basket.NPV_Tech_Fund__c + basket.NPV_Staged_Air_Time__c + basket.Rolling_Air_Time__c) / (basket.Number_of_Users__c)) / (basket.Tenure__c/12));
            basket.Net_Billed_ARPU__c = ((Decimal) rollUps.get('Config ARPU').basketRollUpValue) + (basket.NPV_Staged_Air_Time__c + (basket.Rolling_Air_Time__c/basket.Tenure__c));
            basket.Investment_Ratio__c = (basket.NPV_Tech_Fund__c + basket.NPV_Staged_Air_Time__c + basket.Rolling_Air_Time__c) / basket.Total_Billable_Revenue__c;
        }

        basket.Full_Co_Terminus__c = (Boolean) rollUps.get('Full Co Terminus').basketRollUpValue;
        basket.Averaging_Co_Terminus__c = (Boolean) rollUps.get('Averaging Co Terminus').basketRollUpValue;
        basket.Disconnect_Allowances__c = (Boolean) rollUps.get('Disconnect Allowance').basketRollUpValue;
        basket.Competitive_Clause__c = (Boolean) rollUps.get('Competitive Clause').basketRollUpValue;
        basket.Total_Revenue_Guarantee__c = (Boolean) rollUps.get('Total Revenue Guarantee').basketRollUpValue;
        basket.Total_Contract_Guarantee__c = (Boolean) rollUps.get('Total Contract Guarantee').basketRollUpValue;
        basket.Unlocking_Fees__c = (Boolean) rollUps.get('Unlocking Fees').basketRollUpValue;
        basket.Competitive_Benchmarking__c = (Boolean) rollUps.get('Competitive Benchmarking').basketRollUpValue;
        basket.Payment_Terms__c = (String) rollUps.get('Payment Terms').basketRollUpValue;

        //ARPU Calculations     
        // iradic: try catch here because of System.MathException: Divide by 0
        try {
            String fmMainServiceName = (String) rollUps.get('Main Service Name').basketRollUpValue;
            Decimal dataPerUser = (Decimal) rollUps.get('Total Data').basketRollUpValue / basket.Number_of_Users__c;
            basket.Data_Per_User__c = dataPerUser < 1 ? 1 : dataPerUser; // minimum Data Per User is 1 flat
            if (basket.Number_of_Users__c != 0 && basket.Tenure__c!=0) {
                basket.FM_Total_Monthly_Investement__c = ((basket.NPV_Tech_Fund__c + basket.NPV_Staged_Air_Time__c + basket.Rolling_Air_Time__c)/basket.Tenure__c)/basket.Number_of_Users__c;
                basket.FM_Voice_ARPU__c = ((Decimal) rollUps.get('Total Voice Charge').basketRollUpValue / basket.Tenure__c)/(basket.Number_of_Users__c);
                basket.FM_Data_ARPU__c = ((Decimal) rollUps.get('Total Data Charge').basketRollUpValue / basket.Tenure__c)/(basket.Number_of_Users__c);
                basket.FM_Voice_ARPU_Margin__c = (((Decimal) rollUps.get('Total Voice Charge').basketRollUpValue + (Decimal) rollUps.get('Total Voice Cost').basketRollUpValue) / basket.Tenure__c)/(basket.Number_of_Users__c);
                basket.FM_Data_ARPU_Margin__c = (((Decimal) rollUps.get('Total Data Charge').basketRollUpValue + (Decimal) rollUps.get('Total Data Cost').basketRollUpValue) / basket.Tenure__c)/(basket.Number_of_Users__c);
            }
        } catch (Exception e){
        }        
    }

    public void calculateBasketApprovals() {
        this.basket = CS_ProductBasketService.setSpecialConditionFromConfigurations(configs, basket);
        String basketType = '';
        String dealType = '';
        Boolean techFund = false;
        List<FM_ARPU_Margin__c> fmARPUMargins = new List<FM_ARPU_Margin__c>();
        
        //start - replacing old code with new - in case of null - exception would be thrown
        Decimal fmVoiceArpu = basket.FM_Voice_ARPU__c != null ? basket.FM_Voice_ARPU__c : 0;
        Decimal fmDataArpu = basket.FM_Data_ARPU__c != null ? basket.FM_Data_ARPU__c : 0;
        Decimal fmVoiceArpuMargin = basket.FM_Voice_ARPU_Margin__c != null ? basket.FM_Voice_ARPU_Margin__c : 0;
        Decimal fmDataArpuMargin = basket.FM_Data_ARPU_Margin__c != null ? basket.FM_Data_ARPU_Margin__c : 0;
        Decimal floorARPU = fmVoiceArpu + fmDataArpu;
        Decimal floorARPUMargin = fmVoiceArpuMargin + fmDataArpuMargin;
        Decimal arpuPercentage = 0;
        if (floorARPUMargin > 0)
            arpuPercentage = (floorARPUMargin/floorARPU) * 100;
            
        Decimal rollingAirTime = basket.Rolling_Air_Time__c != null ? basket.Rolling_Air_Time__c : 0;
        Decimal stagedAirTime = basket.Staged_Air_Time__c != null ? basket.Staged_Air_Time__c : 0;
        Decimal funds = rollingAirTime + stagedAirTime;
        //end
        
        //old code start
        //Decimal floorARPU = basket.FM_Voice_ARPU__c + basket.FM_Data_ARPU__c;
        //Decimal floorARPUMargin = basket.FM_Voice_ARPU_Margin__c + basket.FM_Data_ARPU_Margin__c;
        //Decimal arpuPercentage = (floorARPUMargin/floorARPU) * 100;
        //Decimal funds = basket.Rolling_Air_Time__c + basket.Staged_Air_Time__c == null ? 0 : basket.Rolling_Air_Time__c + basket.Staged_Air_Time__c;
        //old code end

        List<CS_Approval_Level__c> approvalLevels = [
                SELECT
                        Id, Name, Subs_From__c, Subs_To__c,
                        Approval_Level__c, Type__c, KPI__c,
                        Band_To__c, Band_From__c, Is_Active__c
                FROM CS_Approval_Level__c
                WHERE Is_Active__c = TRUE
        ];

        Map<String, List<CS_Approval_Level__c>> approvalLevelsByType = new Map<String, List<CS_Approval_Level__c>>();

        for (CS_Approval_Level__c al : approvalLevels) {
            if (approvalLevelsByType.get(al.Type__c) == null) {
                approvalLevelsByType.put(al.Type__c, new List<CS_Approval_Level__c>());
            }
            approvalLevelsByType.get(al.Type__c).add(al);
        }

        if (basket.ReSign__c) {
            basketType = 'Re-sign';
        } else {
            basketType = 'Acquisition';
        }

        if (basketType == 'Re-sign')
            dealType = 'Resign';
        if (basketType == 'Acquisition')
            dealType = 'New';   
            
        if (basket.NPV_Tech_Fund__c > 0)
            techFund = true;    
        
        fmARPUMargins = [
                        SELECT      
                            Id 
                        FROM FM_ARPU_Margin__c
                        WHERE 
                        Deal_Type__c = :dealType AND
                        Data_Per_User__c = :basket.Data_Per_User__c  AND
                        Tech_Fund__c = :techFund AND
                        Min_Connections__c <= :basket.Number_of_Users__c AND
                        Max_Connections__c >= :basket.Number_of_Users__c AND
                        Floor_ARPU__c <= :floorARPU AND
                        ARPU_Margin__c <= :floorARPUMargin AND
                        ARPU_Margin_Percentage__c <= :arpuPercentage
        ];

        
        if (fmARPUMargins.size() == 0) {
            basket.Approval_Level_GOM_pu_pa__c = 5;
        }
        
        else if ((basket.Payment_Terms__c != null && basket.Payment_Terms__c != '30 Days') || (minimumConnectionPeriod == 30) || (funds !=0) || (rebateValue >= 8.0)) {
            basket.Approval_Level_GOM_pu_pa__c = 5;
        }

        else if (approvalLevels != null || approvalLevelsByType.get(basketType) != null) {
            for (CS_Approval_Level__c al : approvalLevelsByType.get(basketType)) {
                if (al.KPI__c == 'NPV GOM per sub per annum') {
                    if (al.Band_To__c == null && al.Band_From__c <= basket.NPV_GOM_per_Sub_per_Annum__c && basket.Number_of_Users__c >= al.Subs_From__c && basket.Number_of_Users__c <= al.Subs_To__c) {
                        basket.Approval_Level_GOM_pu_pa__c = al.Approval_Level__c; 
                    }    
                    else if (basket.NPV_GOM_per_Sub_per_Annum__c >= al.Band_From__c && basket.NPV_GOM_per_Sub_per_Annum__c < al.Band_To__c && basket.Number_of_Users__c >= al.Subs_From__c && basket.Number_of_Users__c <= al.Subs_To__c) {
                        basket.Approval_Level_GOM_pu_pa__c = al.Approval_Level__c;
                    }
                }
            }
        }
    }

    public CS_SolutionDealCalculations.BasketRollUpSummary createBasketRollUp(
            String configRollUpFieldsName,
            String dataType,
            Map<String, String> configConditionalValues,
            String rollUpType
    ) {
        CS_SolutionDealCalculations.BasketRollUpSummary rollUp =
                new CS_SolutionDealCalculations.BasketRollUpSummary();

        rollUp.configRollUpFieldsName = configRollUpFieldsName;
        rollUp.dataType = dataType;
        rollUp.configConditionValues = configConditionalValues;
        rollUp.rollUpType = rollUpType;

        return rollUp;
    }

    public void calculateRollups(List<cscfga__Product_Configuration__c> configs,
            Map<String, CS_SolutionDealCalculations.BasketRollUpSummary> rollUps) {

        for (cscfga__Product_Configuration__c config : configs) {
            for (String rollupName : rollUps.keySet()) {
                CS_SolutionDealCalculations.BasketRollUpSummary rollUp = rollUps.get(rollupName);
                Boolean conditionsVerified = true;

                if (rollUp.basketRollUpValue == null) {
                    switch on rollUp.dataType {
                        when 'Decimal' {
                            rollUp.basketRollUpValue = 0.0;
                        }

                        when 'Integer' {
                            rollUp.basketRollUpValue = 0;
                        }

                        when 'String' {
                            rollUp.basketRollUpValue = '';
                        }

                        when 'Boolean' {
                            rollUp.basketRollUpValue = false;
                        }
                    }
                }

                for (String configField : rollUp.configConditionValues.keySet()) {
                    if (configField == 'No Condition') {
                        break;
                    }

                    if (rollUp.configConditionValues.get(configField) == 'true' || rollUp.configConditionValues.get(configField) == 'false') {
                        if (config.get(configField) != Boolean.valueOf(rollUp.configConditionValues.get(configField))) {
                            conditionsVerified = false;
                            break;
                        }
                    } else {
                        if (config.get(configField) != rollUp.configConditionValues.get(configField)) {
                            conditionsVerified = false;
                            break;
                        }
                    }
                }

                if (!conditionsVerified) {
                    continue;
                }

                if (rollUp.rollUpType == 'Add') {
                    if (rollUp.configRollUpFieldsName == 'FM_Data__c') {
                        if (config.get(rollUp.configRollUpFieldsName) == null) continue;
                        rollUp.basketRollUpValue = (Decimal) rollUp.basketRollUpValue + Decimal.valueOf((String) config.get(rollUp.configRollUpFieldsName));
                    } else {
                        if (config.get(rollUp.configRollUpFieldsName) == null) continue;
                        rollUp.basketRollUpValue = (Decimal) rollUp.basketRollUpValue + (Decimal) config.get(rollUp.configRollUpFieldsName);
                    }
                }

                if (rollUp.rollUpType == 'Max') {
                    if (config.get(rollUp.configRollUpFieldsName) == null) continue;
                    rollUp.basketRollUpValue = (Decimal) rollUp.basketRollUpValue > (Decimal) config.get(rollUp.configRollUpFieldsName)
                            ? (Decimal) rollUp.basketRollUpValue : (Decimal) config.get(rollUp.configRollUpFieldsName);
                }

                if (rollUp.rollUpType == 'Min') {
                    if (config.get(rollUp.configRollUpFieldsName) == null) continue;
                    rollUp.basketRollUpValue = (Decimal) rollUp.basketRollUpValue < (Decimal) config.get(rollUp.configRollUpFieldsName)
                            ? (Decimal) rollUp.basketRollUpValue : (Decimal) config.get(rollUp.configRollUpFieldsName);
                }

                if (rollUp.rollUpType == 'ConcatenateUnique') {
                    if (config.get(rollUp.configRollUpFieldsName) == null) continue;
                    String rollUpStringValue = (String) rollUp.basketRollUpValue;
                    rollUp.basketRollUpValue = rollUpStringValue.contains((String) config.get(rollUp.configRollUpFieldsName))
                            ? rollUpStringValue
                            : rollUpStringValue == '' ? config.get(rollUp.configRollUpFieldsName) : rollUpStringValue + ',' + config.get(rollUp.configRollUpFieldsName);
                }

                if (rollUp.rollUpType == 'Logic') {
                    Boolean rollUpBooleanValue = (Boolean) rollUp.basketRollUpValue;
                    rollUp.basketRollUpValue = rollUpBooleanValue && (Boolean) config.get(rollUp.configRollUpFieldsName);
                }

                if (rollUp.rollUpType == 'Count') {
                    if (rollUp.basketRollUpValue == null) {
                        rollUp.basketRollUpValue = 1; 
                    } else {
                        rollUp.basketRollUpValue = (Decimal) rollUp.basketRollUpValue + 1;
                    }         
                }
            }
        }
    }

    public class BasketRollUpSummary {
        public String configRollUpFieldsName;
        public Object basketRollUpValue;
        public String dataType;
        public Map<String, String> configConditionValues;
        public String rollUpType;
    }
}