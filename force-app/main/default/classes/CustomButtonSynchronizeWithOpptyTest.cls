@IsTest
private class CustomButtonSynchronizeWithOpptyTest {
  static cscfga__Product_Basket__c testBasket;
  static cscfga__Product_Configuration__c pc1;
  static cscfga__Product_Configuration__c pc2;
  
  static void createTestData(){
    CS_TestDataFactory.insertTriggerDeactivatingSetting();
    OLI_Sync__c os = new OLI_Sync__c();
    os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
    os.SetupOwnerId = UserInfo.getUserId();
    insert os;
    CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
    CS_TestDataFactory.generatePLReportConfigRecords();  
    
    Account testAcc = new Account();
    testAcc.Name = 'TestAccount';
    testAcc.NumberOfEmployees = 1;
    insert testAcc; 

    Id pricebookId = Test.getStandardPricebookId();

    id RecordTypeIdOpp = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Standard').getRecordTypeId();
    Opportunity testOpp = new Opportunity();    
    testOpp.Name = 'TestOppty';
    testOpp.recordtypeid = RecordTypeIdOpp; 
    testOpp.AccountId = testAcc.Id;
    testOpp.CloseDate = System.today();
    testOpp.StageName = 'Changes Over Time';  
    testOpp.TotalOpportunityQuantity = 0; 
    testOpp.Pricebook2Id = pricebookId;

    insert testOpp;

    testBasket = new cscfga__Product_Basket__c();
    Datetime d = system.now();
    String strDatetime = d.format('yyyy-MM-dd HH:mm:ss');
    testBasket.Name = 'Test Order ' + strDatetime;
    testBasket.cscfga__Opportunity__c = testOpp.Id;
	testBasket.Rolling_Air_Time__c = 10;
	testBasket.Staged_Air_Time__c = 5;
	testBasket.Tech_Fund__c = 100;
	testBasket.Unconditional_cheque__c = 200;

    insert testBasket;

    cscfga__Product_Definition__c proddef1 = new cscfga__Product_Definition__c(
    Name = 'BT Mobile',
    cscfga__Description__c = 'prod description1'
    );
    insert proddef1;

    cscfga__Attribute_Definition__c ad1 = new cscfga__Attribute_Definition__c(
    Name = 'Total Price',
    cscfga__Data_Type__c = 'Decimal',
    cscfga__Type__c = 'User Input',
    cscfga__Product_Definition__c = proddef1.Id,
    cscfga__Is_Line_Item__c = true,
    cscfga__line_item_description__c = 'BT Mobile',
    cscfga__Configuration_Output_Mapping__c = 'cscfga__Total_Price__c'
    );
    insert ad1;

    pc1 = new cscfga__Product_Configuration__c(
    cscfga__Contract_Term__c = 24,
    cscfga__Recurrence_Frequency__c = 12,
    Name = 'BT Mobile',
	Calculations_Product_Group__c = 'BT Mobile',
    cscfga__Product_Basket__c = testBasket.Id,
    cscfga__Product_Definition__c = proddef1.Id,
    cscfga__Configuration_Status__c = 'Valid',
    Approval_Status__c = 'Approved'
    );
    insert pc1;

    cscfga__Attribute__c at1 = new cscfga__Attribute__c(
    Name = 'Total Price',
    cscfga__Attribute_Definition__c = ad1.Id,
    cscfga__Value__c = '24',
    cscfga__Product_Configuration__c = pc1.Id,
    cscfga__Line_Item_Description__c = 'BT Mobile',
    cscfga__Is_Line_Item__c = true
    );
    insert at1;

    product2 prod = new product2(name='BT Mobile',Family='Monthly', isActive=true,Part_Number__c = 'tst067');
    insert prod;

    PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = prod.Id,UnitPrice = 10000, IsActive = true);
    insert standardPrice;
  }

  static testMethod void testBasketSync(){
    createTestData();
    Test.startTest();
    CustomButtonSynchronizeWithOpportunity syncBaskt = new CustomButtonSynchronizeWithOpportunity();
    syncBaskt.performAction(testBasket.id);
    String flds = CustomButtonSynchronizeWithOpportunity.getSobjectFields('cscfga__Product_Configuration__c');
    system.assertNotEquals(flds, '');

    Test.stopTest();
  }

  static testMethod void testBasketSync1(){
    createTestData();
    Test.startTest();
    
    testBasket.Approval_Level_1__c = UserInfo.getUserId();
    testBasket.BT_Basket_Approval_Status__c = 'Rejected';
    update testBasket;
    CustomButtonSynchronizeWithOpportunity syncBaskt = new CustomButtonSynchronizeWithOpportunity();
    syncBaskt.performAction(testBasket.id);

    Test.stopTest();
  }

  static testMethod void testBasketSync2(){
    createTestData();
    Test.startTest();

    pc1.cscfga__Configuration_Status__c = 'test valid';
    update pc1;
    String returnstr = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(testBasket.id);
    system.assertNotEquals(returnstr, '');

    Test.stopTest();
  }

  static testMethod void testBasketSync3(){
    createTestData();

    Test.startTest();

    String returnstr = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(testBasket.id);
    system.assertNotEquals(returnstr, '');
    Test.stopTest();
  }

  @IsTest
  public static void testApprove() {
	createTestData();

	Test.startTest();
	pc1.Approval_Status__c = 'Rejected';
	pc1.Approval_Level_1__c = UserInfo.getUserId();
	update pc1;
	String returnstr = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(testBasket.id);
	system.assertNotEquals(returnstr, '');

	Test.stopTest();
  }

  @IsTest
  public static void testRateCardWrapper() {
	cspmb__Rate_Card__c	rateCard = new cspmb__Rate_Card__c(Name = 'Test Rate Card');
	insert rateCard;

	cspmb__Rate_Card_Line__c rcl = new cspmb__Rate_Card_Line__c(
		Name = 'Test Rate Card Line',
		cspmb__Rate_Card__c = rateCard.Id
	);
	insert rcl;

	cspmb__Price_Item__c priceItem = CS_TestDataFactory.generatePriceItem(true, 'Test price item');
	
	cspmb__Price_Item_Rate_Card_Association__c rateCardAssoc = new cspmb__Price_Item_Rate_Card_Association__c(
		cspmb__Price_Item__c = priceItem.Id,
		cspmb__Rate_Card__c = rateCard.id
	);
	insert rateCardAssoc;

	Test.startTest();
	CustomButtonSynchronizeWithOpportunity.WrapperRateCardWithLineItems testWrapper = new CustomButtonSynchronizeWithOpportunity.WrapperRateCardWithLineItems();
	testWrapper.rateCard = rateCardAssoc;
	testWrapper.rateCardLines = new List<cspmb__Rate_Card_Line__c>{ rcl };
	Test.stopTest();
  }
}