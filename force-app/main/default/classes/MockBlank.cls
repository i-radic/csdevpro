/**
* Mock class which provides as much of a clean slate as possible
*
* @author Gary Marsh
* @date 2013-06-03
* @extends MockDataStandard
*/
public with sharing class MockBlank extends MockDataStandard {
	private static Map<String, Map<String, Object>> DEFAULT_VALUES_BY_TYPE = new Map<String, Map<String, Object>>();
	
	public static Map<String, Object> SUPPORT_REQUEST_VALUES = new Map<String, Object> {
		Mock.DEPENDS_ON => new Map<String, String> {'Opportunity__c' => 'Opportunity'}
	};
	
	static {
		//all fixture maps specified above need to be added here
		DEFAULT_VALUES_BY_TYPE.put('Support_Request__c', SUPPORT_REQUEST_VALUES);
	}

	public override Map<String, Object> getValueMap(final String objApiName, final Map<String, Object> fieldValues) {
		return MockBlank.DEFAULT_VALUES_BY_TYPE.get(objApiName);
	}
	
}