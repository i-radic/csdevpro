public class AdpOppyCtrl { 
   
public AdpOppyCtrl(ApexPages.StandardController controller) {   
 }
ID APD_Id = ApexPages.currentPage().getParameters().get('id');
List<ADP__c> AccountID1 = [ select customer__r.id from ADP__c where id = :APD_Id];
  
public List<Opportunity> getOpportunities() {        
//return [select id, name, StageName, Amount, CreatedDate, ACV_Calc__c, NIBR_Calc__c, Expected_Close_Date__c, Win_Rate__c, Main_Competitor__c from Opportunity                
return [select id, name, Win_Rating_ff__c, Product_Family__c, NIBR_Calc__c, CloseDate, Net_ACV__c from Opportunity       
where accountId = :AccountID1[0].customer__r.id AND StageName = 'In Progress'];
    }
 }