public class CovSiteIndicatorControllerExtention 
{
    private Coverage_Check_Site__c covchecksite;
    public String Response {get; set;}
    public static boolean isApexTest = false;
    public static String testResponse='';
    public Boolean refreshPage {get; set;}
    public string URLForPage{get;set;}
    public Integer count{get; set;}
   
    public CovSiteIndicatorControllerExtention (ApexPages.StandardController stdController) 
    {
        ////this.acct = (Account)stdController.getRecord();
        covchecksite = [select id,  Post_Code__c,Coverage_2G__c,Coverage_3G__c,Coverage_4G__c  from Coverage_Check_Site__c
                       where id = :ApexPages.currentPage().getParameters().get('id')];
        URLForPage = ApexPages.currentPage().getHeaders().get('Host');
        refreshPage=false;
        
    }
    public void doRequest() 
    { 
        List<CoverageChecker__c> cc = CoverageChecker__c.getall().values();
        System.debug('CALLING COVERAGE CHECKER');
        System.HttpRequest request = new System.HttpRequest();
        request.setMethod('GET');

        try
        {
            if (covchecksite.Post_Code__c  != null) 
            {
                String postCode = covchecksite.Post_Code__c;
                System.debug('postCode  ='+ postCode );  
                String coverageCheckURL;
                If(!isApexTest)
                 coverageCheckURL  =cc[0].CoverageCheckerEndpointURL__c;
                else
                  coverageCheckURL=''; 
                //http://coverage.ee.co.uk/rest/v1/coverage/locations/
                String endpoint= coverageCheckURL   + EncodingUtil.urlEncode(covchecksite.Post_Code__c, 'UTF-8');          
                request.setEndpoint(endpoint);
                System.debug('EndPoint===='+coverageCheckURL );
                String timestamp = Datetime.now().format('dd/MM/yyyy HH:mm:ss');
                request.setHeader('X-EE-EL-Tracking-Header', 'eesfdc-'+  postCode +'-'+ timestamp );
                System.HttpResponse response ;
                if (!isApexTest)
                {
                    response = new System.Http().send(request);
                    this.Response = response.getBody();
                    System.debug('Response from the coverage checker='+ this.Response);
                    System.debug('X-EE-EL-Tracking-Header===='+ response.getHeader('X-EE-EL-Tracking-Header'));
                }
                else
                {
                    this.Response=testResponse;
                }          
                CoverageIndicatorJSONParser  parsedData =new CoverageIndicatorJSONParser();
                parsedData = CoverageIndicatorJSONParser.parse(this.Response);
                System.debug('parsedData.Locations.size===='+ parsedData.Locations.size());
                if (parsedData.Locations.size() == 1)
                {
                   CoverageIndicatorJSONParser.Locations loc = parsedData.locations[0];
                   for(Integer i = 0; i < loc.coverage.size(); i++)
                   {
                       System.debug('Cov type ='+loc.coverage[i].type+'  Cov Str='+loc.coverage[i].Strength);
                       if( loc.coverage[i].type.equals('2G') )
                       {
                          covchecksite.Coverage_2G__c = loc.coverage[i].Strength;                                     
                       }   
                       else if ( loc.coverage[i].type.equals('3G') )
                       {
                           covchecksite.Coverage_3G__c = loc.coverage[i].Strength;                                          
                       }
                       else if ( loc.coverage[i].type.equals('4G') )
                       {
                          covchecksite.Coverage_4G__c = loc.coverage[i].Strength;                                           
                       }      
                    }                       
               }
               else
               {
                   covchecksite.Coverage_2G__c = '0';
                   covchecksite.Coverage_3G__c = '0';
                   covchecksite.Coverage_4G__c = '0';          
               }
                 
                   update covchecksite;              
                    refreshPage = true;   
                 
                   
             }        
             else
             {
                   covchecksite.Coverage_2G__c = '0';
                   covchecksite.Coverage_3G__c = '0';
                   covchecksite.Coverage_4G__c = '0';
                   update covchecksite;        
             }
             
        }
        catch(System.JSONException e)
        {
            System.debug('JSONException============'+e);
            covchecksite.Coverage_2G__c = '0';
            covchecksite.Coverage_3G__c = '0';
            covchecksite.Coverage_4G__c = '0';
            update covchecksite;        
        }
      
   }
   public void forTest(String response)
   {
         isApexTest = true;
         testResponse=response;
         doRequest();
   }   
}