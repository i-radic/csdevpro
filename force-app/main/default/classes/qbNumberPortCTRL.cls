public with sharing class qbNumberPortCTRL {
    ApexPages.StandardController Gcontroller;
    public Number_Port_Line__c thisRec;   
    private List<SelectOption> selOptions; 
    
    public String headerId = System.currentPageReference().getParameters().get('qbId');
    public String orderId = System.currentPageReference().getParameters().get('Id');
    public String attachPDF = System.currentPageReference().getParameters().get('attachPDF');
    
    public String orderName {get; set;}
    public String cloneId {get; set;}
    public String fullRange {get; set;}
    public String rangeHolder {get; set;}
    public String companyName {get; set;}
    public String accountNumber {get; set;}
    public String selLineAction{get; set;}
    public String registeredAddress {get; set;}
    public String registeredNumber {get; set;}
    public String requestersName {get; set;}
    public String requestersJobTitle {get; set;}
    public String requestersContactDetails {get; set;}
    public String associatedNumbersP {get; set;}
    public String associatedNumbersC {get; set;}
    public String associatedNumbersR {get; set;}
    public String otherNumbersP {get; set;}
    public String otherNumbersC {get; set;}
    public String otherNumbersR {get; set;}
    public String addInfo {get; set;}
    public String CP {get; set;}   
    public String billingAcc{get;set;}
    public String selPortType {get; set;}
    public String cCSV {get; set;}
    public String pCSV {get; set;}
    public String rCSV {get; set;}
    public String prodTel {get; set;}
    public String selectedProducts {get; set;}
    public String serviceView {get; set;}
    
    public Long StartNum {get; set;}
    public Long FinishNum {get; set;}
    
    public Integer pStartNum {get; set;}
    public Integer pFinishNum {get; set;}
    public Integer rStartNum {get; set;}
    public Integer rFinishNum {get; set;}
    public Integer cStartNum {get; set;}
    public Integer cFinishNum {get; set;}
    public Integer totalInRange {get; set;}
    public Integer totalAllocated {get; set;}
    public Integer LineQty{get; set;}
    public Integer Qty {get; set;}   
    
    public Boolean quoteRef = False;
    public Boolean refreshPage {get; set;}
    public Boolean prodNonBT {get; set;}
    public Boolean prodBT {get; set;}
    public Boolean prodOther {get; set;}
    public Boolean detailMissing{get; set;}
    public Boolean opId{get; set;}
    Public List<Number_Port_Line__c> numPortDetail {get; set;}
    
    public qbNumberPortCTRL() {
        System.debug('mpk const1');
    }
    
    public qbNumberPortCTRL(ApexPages.StandardController controller) {
        Gcontroller = controller;
        System.debug('mpk const4');
        if(!test.isRunningTest())
        {
            controller.addFields(new List<String>{'Order__c'});
            controller.addFields(new List<String>{'Order__r.Opportunity__c'});
            controller.addFields(new List<String>{'Order__r.Name'});  
        }
        this.thisRec= (Number_Port_Line__c)controller.getRecord(); 
    }    
    
    public Static String j{
        get {         
            String j = qbOrderCTRL.j;
            return j;
        }
        set;
    } 
    
    public List<Number_Port_Reference__c> getCPs() {
        List<Number_Port_Reference__c> rlist = [Select Id, Type__c,Reference__c,Description__c,Area__c, EstablishedIP__c FROM Number_Port_Reference__c WHERE Type__c = 'CP' ORDER BY Reference__c LIMIT 1000 ];
        return rlist ;
    }
    
    public List<SelectOption> getPortProduct() {  
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('Single','Single Line'));
        selOptions.add(new SelectOption('Multi','Multi Line'));
        return selOptions;
    } 
    
    public List<SelectOption> getLineAction() {  
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('Port','Port Entire Range'));
        selOptions.add(new SelectOption('Partial','Port/Cease/Retain'));
        return selOptions;
    } 
    
    public List<Number_Port_Line__c> getNumberPortDetail() {
        if(headerId != '' && headerId != null){
            orderId = headerId;
        }
        List<Number_Port_Line__c> npList =
            [SELECT ID, Name, zStatus__c, Order__r.Id, Order__r.Name,Order__r.Opportunity__c, Losing_CP__c, Account_Number__c, Main_Billing_Account__c, Full_Range__c,
             Total_in_Range__c,Total_Lines__c,Start_Number__c,Finish_Number__c, Site__c, Site__r.Site_Name__c,Order__c, Total_Allocated__c,
             Company_Name__c, Registered_Address__c, Registered_Number__c, Requesters_Name__c, Requesters_Job_Title__c, Requesters_Contact_Details__c, Site_Info__c, 
             CreatedDate, Current_Service_Post_Code__c, Product__c 
             FROM Number_Port_Line__c WHERE Order__c = :orderId ORDER BY Name DESC];
        return npList;	
    }
    
    public List<SelectOption> getSiteList(){
        selOptions= new List<SelectOption>();
        selOptions.add(new SelectOption('', '--None--'));
        for (Order_Site__c rt : [SELECT ID, Site_Name__c FROM Order_Site__c WHERE Order_Header__c = :thisRec.Order__c AND Numbers_Port__c > 0 ORDER BY Site_Name__c]){
            selOptions.add(new SelectOption(rt.Id, rt.Site_Name__c));
        }
        return selOptions;
    }
    
    public PageReference goCancel() {  
        PageReference retPg = new PageReference('/apex/qbNumberPort?id='+thisRec.Order__c);
        return retpg.setRedirect(true);
    }
    
    public PageReference goCancelNew() {  
        PageReference retPg = new PageReference('/apex/qbNumberPort?id='+headerId);
        return retpg.setRedirect(true);
    }
    
    public PageReference goDelete(){
        delete thisRec;
        attachNumPortToOpp(thisRec.Order__r.Opportunity__c);
        PageReference retPg = new PageReference('/apex/qbNumberPort?id='+thisRec.Order__c);
        return retpg.setRedirect(true);
    }
     
	public PageReference goClone(){
        if(cloneId == null){
			cloneId = thisRec.Id;
		}	
		Number_Port_Line__c originalRec = [SELECT Id, Order__c, Order__r.Name, Port_Date__c,Full_Range__c,Cloud_Voice_Site__c,Cloud_Voice__c,Site__c,Registered_Number__c,
		Line_Type__c,Multi_Line_Action__c,Port_Type__c,Registered_Address__c,Requesters_Contact_Details__c,Account_Number__c,Company_Name__c,Requesters_Name__c,
		Current_Service_Number__c,Current_Service_Post_Code__c,Current_Service_Street__c,Current_Service_City__c,LinkedProductLine__c,New_Service_Number__c,
		New_Service_Post_Code__c,New_Service_Street__c,New_Service_City__c,Requesters_Job_Title__c,Gaining_CP__c,Losing_CP__c,Main_Billing_Account__c,Range_Holder__c
		FROM Number_Port_Line__c WHERE Id = :cloneId];
		originalRec.zStatusNotes__c = '<li>Cloned Record</li>';
		originalRec.zStatus__c = 'grey';
        Number_Port_Line__c cloneRec = originalRec.clone(false);
        insert cloneRec;
        PageReference retPg = new PageReference('/apex/qbNumberPortAdd?id='+cloneRec.Id+'&qbName='+cloneRec.Order__r.Name+'&qbId='+cloneRec.Order__c);
        return retpg.setRedirect(true);
    }
	    
    public PageReference addPortDetail() {  
        orderName = ''; 
        if (headerId != Null){ 
            thisRec.Order__c = headerId;
        }
        try{
            upsert thisRec;
        }catch (DMLException e){
            ApexPages.addMessages(e);
        } 
        
        Decimal totalPorts = 0;
        List<Number_Port_Line__c> npl = [SELECT ID, zStatus__c, Total_Allocated__c, Total_in_Range__c, Total_Lines__c, Total_Lines_Required__c,Order__r.Name 
                                         FROM Number_Port_Line__c WHERE Order__c = :headerId AND zStatus__c = 'green'];
        for(Number_Port_Line__c n:npl){
            orderName = n.Order__r.Name;
            totalPorts += qbSiteCTRL.nullToDecimal(n.Total_Allocated__c);
        }
        Order_Header__c oh = [SELECT ID, Total_Number_Ports_Completed__c,Opportunity__c FROM Order_Header__c WHERE Id = :headerId];
        Order_Site__c os = [SELECT ID FROM Order_Site__c WHERE Order_Header__c = :headerId LIMIT 1];
        oh.Total_Number_Ports_Completed__c = totalPorts;
        update oh;
        if (npl.size() >0){          
            orderId = oh.Id;
            qbOrderCTRL oc = new qbOrderCTRL(os.Id);
            oc.checkComplete();
        }        
        PageReference retPg = page.qbNumberPort;	
        retPg.getParameters().put('Id',headerId);
        retPg.getParameters().put('qbName',orderName);
        retPg.getParameters().put('attachPDF','y');
        retPg.getParameters().put('oppId',oh.Opportunity__c);
        return retPg.setRedirect(true);
    }  

    Public void attachNumPortToOpp2(){   
        String attachPDF = System.currentPageReference().getParameters().get('attachPDF');
        
        if(attachPDF!= null && attachPDF == 'y'){
            orderName = System.currentPageReference().getParameters().get('qbName');
            string oppId = System.currentPageReference().getParameters().get('oppId');
            attachNumPortToOpp(oppId);               
        }        
    }
    public void attachNumPortToOpp(String oId){        
        string pdfName = 'Number Port '+orderName+'.pdf';        
        
        List<Attachment> attachments = [select Name FROM Attachment WHERE parentid =: oId AND Name =: pdfName];
        if (attachments.size()>0) Delete attachments;
        
        pageReference pdf = Page.qbNumberPortLOA;        
        pdf.getParameters().put('qbId', orderId);
        Attachment attach = new Attachment();
        Blob body;
        if(!Test.isRunningTest()){
            body = pdf.getContent();
        }
        attach.Body = body;
        attach.Name = pdfName;
        attach.IsPrivate = false;
        attach.ParentId = oId;
        if(!Test.isRunningTest()){
            insert attach;
        }  
    }
}