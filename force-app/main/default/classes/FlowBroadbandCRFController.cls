public with sharing class FlowBroadbandCRFController {

	public String vfAccId { get; set; }
	public String vfAccName { get; set; }
	public String vfBBLineOf { get; set; }
	public String vfBBRecType { get; set; }
	public String vfConId { get; set; }
	public String vfOppId { get; set; }
	public String vfOppName { get; set; }
  	public String vfprofid { get; set; }

    public Flow.Interview.BB_Main_Flow broadband { get; set; }

    public FlowBroadbandCRFController(ApexPages.StandardController controller) {
    	initVariables();
    }


    public void initVariables() {
    	vfAccName = Apexpages.CurrentPage().getParameters().get('AccountName');
        vfAccId = Apexpages.CurrentPage().getParameters().get('AccountId');
        vfOppName = Apexpages.CurrentPage().getParameters().get('OppyName');
        vfOppId = Apexpages.CurrentPage().getParameters().get('OppyId');
        vfprofid = Apexpages.CurrentPage().getParameters().get('profid');
    	String BBLineOfName = Apexpages.CurrentPage().getParameters().get('CF00N20000002oGtq');
		vfBBRecType = BTUtilities.getRecordTypeIdByName('Flow_CRF__c', 'Broadband');

    	if(string.isBlank(vfAccName)) {
    		vfAccName = 'No Account Name';
    	}

    	if(string.isBlank(vfAccId)) {
    		vfAccId = 'No Account ID';
    	}

    	if(string.isBlank(vfOppName)) {
    		vfOppName = 'No Opportunity Name';
    	}

    	if(string.isBlank(vfOppId)) {
    		vfOppId = 'No Opportunity ID';
    	}

    	if(string.isBlank(vfprofid)) {
    		vfprofid = 'No Profile ID';
    	}
		
    	list<OpportunityContactRole> opConRole = [SELECT ContactId FROM OpportunityContactRole WHERE OpportunityId = : vfOppId LIMIT 1];
    	if (opConRole.size() > 0) {
    		vfConId = opConRole[0].ContactId;
    	}
    	else {
    		vfConId = 'No Contact ID';
    	}
	
    	if (BBLineOfName != null) {
    		list<CRF__c> crf = [SELECT Id, Name FROM CRF__c WHERE Name =: BBLineOfName];
    		if (crf.size() > 0) {
    			vfBBLineOf = crf[0].Id;
    		}
    		else {
    			vfBBLineOf = 'No BBLineOF ID';
    		}
    	}
		
    	if(string.isBlank(vfBBRecType)) {
    		vfBBRecType = 'No RecordType ID';
    	}

    }


    public String getBroadbandID() {
        if (broadband == null)
            return vfOppId;
        else return broadband.CRFId;
    }

    public PageReference getBroadbandCRF() {
        PageReference p = new PageReference('/' + getBroadbandID());
        p.setRedirect(true);
        return p;
    }

}