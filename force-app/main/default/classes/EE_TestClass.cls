/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class EE_TestClass {

    // Keep a count for test users
    private static integer mUserCount;
    private static integer mAccountCount;
    private static integer mContactCount;
    private static integer mLeadCount;
        
    //public static User mUserTest;
   
    static {
        mUserCount      = 1;
        mAccountCount   = 1;
        mContactCount   = 1;        
    }

/*
    public static TriggerEnabler__c createCustomSettingTriggerEnabler(){
    
        TriggerEnabler__c trgEnabler = new TriggerEnabler__c();
        trgEnabler.Name = 'User';
        trgEnabler.TriggerEnabled__c = true;
        //insert trgEnabler;
        return trgEnabler;

    }
*/

   /*
    * @name         createStaticUser
    * @description
    * @author       P Goodey
    * @date         Sep 2013
    * @see 
    */
    public static User createStaticUser(String psFirstName, String psLastName, String psEmail, String psProfile, 
        String psCompanyName, Boolean pbIsIncrementSuffix) {
        //if (EE_TestClass.mUserTest == null) {

        Profile lProfile = [SELECT Id FROM Profile WHERE Name = :psProfile LIMIT 1];
        User mUserTest = new User();
        mUserTest.FirstName         = psFirstName;
        
        if( pbIsIncrementSuffix ){
            mUserTest.LastName = psLastName + mUserCount.format();
            // Increment the user count
            mUserCount++;                       
        }else{
            mUserTest.LastName = psLastName; 
        }

        
        mUserTest.Alias    = 'Test';
       // mUserTest.Username = psFirstName + '.' +  mUserTest.LastName + '@ee.co.uk.testclass';
        mUserTest.Username = psFirstName + '.' +  mUserTest.LastName + '@bt.com';
        if( psEmail == null ){
            mUserTest.Email = mUserTest.Username;        
        }
        else{
            mUserTest.Email = psEmail;
        }

        mUserTest.CompanyName       = psCompanyName;
        mUserTest.EmailEncodingkey  = 'UTF-8';
        mUserTest.LanguageLocalekey = 'en_US';
        mUserTest.LocaleSidkey      = 'en_GB';
        mUserTest.ProfileId         = lProfile.id;
        mUserTest.Country           = 'GB';
        mUserTest.TimeZonesIdkey    = 'Europe/London';
        mUserTest.EIN__c            = '123456789';
        


        //}
        
        return mUserTest;
    }

    public static Account createStaticAccount(String psName, Boolean pbIsIncrementSuffix) {

        Account mAccountTest = new Account();
        
        if( pbIsIncrementSuffix ){
            mAccountTest.Name = psName + mAccountCount.format();
            // Increment the account count
            mAccountCount++;
        }else{
            mAccountTest.Name = psName;
        }
        

        return mAccountTest;
    }


    public static Contact createStaticContact(String psFirstName, String psLastName, String psEmail, Boolean pbIsIncrementSuffix) {

        Contact mContactTest = new Contact();

        mContactTest.FirstName = psFirstName;

        if( pbIsIncrementSuffix ){
            mContactTest.LastName = psLastName + mContactCount.format(); //mUserCount.format();
            // Increment the contact count
            mContactCount++;                        
        }else{
            mContactTest.LastName = psLastName; 
        }

        if( psEmail == null ){
            mContactTest.Email = psFirstName + '.' +  mContactTest.LastName + '@ee.co.uk.testclass';        
        }
        else{
            mContactTest.Email = psEmail;
        }
                    

        
        return mContactTest;
    }


    public static Lead createStaticLead(String psFirstName, String psLastName, String psEmail, Boolean pbIsIncrementSuffix) {

        Lead mLeadTest = new Lead();
        mLeadTest.FirstName = psFirstName;
        mLeadTest.status    = 'New';
        if( pbIsIncrementSuffix ){
            mLeadTest.LastName = psLastName + mLeadCount.format();
            // Increment the lead count
            mLeadCount++;                        
        }else{
            mLeadTest.LastName = psLastName; 
        }

        if( psEmail == null ){
            mLeadTest.Email = psFirstName + '.' +  mLeadTest.LastName + '@ee.co.uk.testclass';        
        }
        else{
            mLeadTest.Email = psEmail;
        }
        return mLeadTest;
    }


    // Create a Custom Setting for enabling Trigger
    public static TriggerEnabler__c createTriggerEnabler(String psTriggerName, Boolean pbIsEnabled) {

        TriggerEnabler__c lTriggerEnabler = new TriggerEnabler__c();
        lTriggerEnabler.Name = psTriggerName;
        lTriggerEnabler.TriggerEnabled__c = pbIsEnabled;
        return lTriggerEnabler;        
        
    }
    
    
    // Create a Custom Setting for setting User Contact
    /*
    public static UserContact_Settings__c createSettingUserContact(String psDefaultCompanyName, Boolean pbIsDefaultAccountNameRequired ) {    
    
        UserContact_Settings__c lUserContact = new UserContact_Settings__c();
        lUserContact.IsDefaultAccountNameRequired__c = pbIsDefaultAccountNameRequired;
        lUserContact.Default_Company_Name__c = psDefaultCompanyName;
        return lUserContact;
    
    }
    */
    

}