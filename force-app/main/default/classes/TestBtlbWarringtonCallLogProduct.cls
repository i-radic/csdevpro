@isTest
private class TestBtlbWarringtonCallLogProduct {
    static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');
        BTLB_Warrington_Sales_Log_Products__c salesprod = new BTLB_Warrington_Sales_Log_Products__c();
        salesprod.Product_Group_Mobile__c = '';
        salesprod.Sale__c = '';
        salesprod.qQTY__c = null;
        salesprod.Product__c = '';
        salesprod.Term__c = '';
        salesprod.Save_Type__c = '';
        salesprod.VOL_Reference__c = '';
       
        ApexPages.StandardController sc = new ApexPages.StandardController(salesprod);
        BTLBWarringtonSalesLogProducts salesP= new BTLBWarringtonSalesLogProducts(sc);
         
        BTLBWarringtonSalesLogProducts salesProduct = new BTLBWarringtonSalesLogProducts(salesprod);
        salesProduct.OnLoad();
        salesProduct.Save();
        salesProduct.SaveNnew();
    
    }

}