public with sharing class LfsManagement {

public Opportunity Opportunity = null;
 
public PageReference relink() {
	
	 if (this.Opportunity != null) {
                    Id oppyId = Opportunity.Id;
                    
//get oppy details
      
      List<Opportunity> opp = [select id, AccountId, Customer_Contact__c, Customer_Contact_Number__c, Email_Address__c from Opportunity where Id = :oppyId];
      system.debug('ADJ LFS CLASS Oppy :' + opp);
//get Contact & Account Details  
      List<Contact> contacts = [Select id, account.id, phone2__c, email2__c, account.ownerId from contact WHERE 
      							(phone2__c = :opp[0].Customer_Contact_Number__c or ((email2__c = :opp[0].Email_Address__c) and (email2__c <> 'unknown@bt.com'))) order by email2__c desc  ];    
      							                
       system.debug('ADJ LFS CLASS contacts :' + contacts);   
        
       
      if(contacts.size() >0){	
      	opp[0].AccountId = contacts[0].AccountId;
      	opp[0].Customer_Contact__c = contacts[0].Id;		
      	system.debug('ADJ LFS CLASS Oppy2 :' + opp);				                
       update opp[0];  
      }        


/*for (Opportunity o:Trigger.new){
		if ((o.RecordTypeId == '01220000000ABFB')) {
			if (o.Customer_Contact_Number__c != null){
			ContactWorkTel.add(o.Customer_Contact_Number__c);
			system.debug('ADJ o.Customer_Contact_Number__c :' + o.Customer_Contact_Number__c);

			}
			if ((o.Email_Address__c != null) && (o.Email_Address__c != 'unknown@bt.com')){ // added unknown@bt.com to allow dummy email address to be added but not linked to
			ContactEmail.add(o.Email_Address__c);
			system.debug('ADJ o.Email_Address__c :' + o.Email_Address__c);

			}
		system.debug('ADJ o.Customer_Contact_Number__c size :' + ContactWorkTel.size());
		system.debug('ADJ o.Email_Address__c size :' + ContactEmail.size());
 			  }
		}
		
		vif (AccountIdPhone.containsKey(o.Customer_Contact_Number__c) <> false){
	o.AccountId =  AccountIdPhone.get(o.Customer_Contact_Number__c);
	}
if (AccountIdEmail.containsKey(o.Email_Address__c) <> false){
	o.AccountId =  AccountIdEmail.get(o.Email_Address__c);
	}
if ((ContactIdPhone.containsKey(o.Customer_Contact_Number__c) <> false) && (AcountOwnerList.size() <2)){
	o.Customer_Contact__c =  ContactIdPhone.get(o.Customer_Contact_Number__c);
	}
if (ContactIdEmail.containsKey(o.Email_Address__c) <> false){
	o.Customer_Contact__c =  ContactIdEmail.get(o.Email_Address__c);
	}
		
	*/	
		
		



// go back to original page
                    PageReference pr = new PageReference('/'+oppyId);
                    pr.setRedirect(true);

                    return pr;

                    }

            return null;
}

        public LfsManagement (ApexPages.StandardController stdController) {
            Opportunity = (Opportunity)stdController.getRecord();
            }

        public LfsManagement () {}

        }