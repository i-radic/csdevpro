public with sharing class ipcCTRL{

    public ipcCTRL(ApexPages.StandardController controller) {
    }
    public ipcCTRL(){ }    
    public Integer licenses {get; set;}  

    public String licenseType {get; set;} 
    public String term {get; set;} 
    public String phone{get; set;} 
    public String product {get; set;}  
    public String speed {get; set;}  
    public String accPrice {get; set;}  
    public String bundleSTD {get; set;}  
    public String newAccess {get; set;}  
    public String newAccessRef {get; set;}  
    public String newLAN {get; set;}  
    public String newSC {get; set;}     
    public String ccFeatures {get; set;}      
    public String selProduct {get; set;}   
    public String LineCheckNumber {get; set;}
    public String tMax {get; set;}
    public String tADSL2 {get; set;}
    public String tWBC {get; set;}
    public String tFTTC {get; set;}
    public String tFTTP {get; set;}
    public String tFOD {get; set;}
    public String tErrText {get; set;}
    public String action {get; set;}
    public String sell {get; set;}

    public Boolean contactValid {get; set;}

    private List<SelectOption> selOptions; 

    public string aId = System.currentPageReference().getParameters().get('accId');
    public string cId = System.currentPageReference().getParameters().get('conId');
    public string oId = System.currentPageReference().getParameters().get('oppId');
    
    public String getAccName(){
        String aName = System.currentPageReference().getParameters().get('accName');
        return aName;
    }  
    public List<SelectOption> getYesNo() {
      selOptions= new List<SelectOption>();
      selOptions.add(new SelectOption('', 'Select'));
      selOptions.add(new SelectOption('No', 'No'));
      selOptions.add(new SelectOption('Yes', 'Yes'));
      return selOptions;
    }     
    public Opportunity newOpp {
        get {
        if (newOpp == null)
            newOpp = new Opportunity();
            return newOpp ;
        }
        set;
    }   
    public OpportunityContactRole newOppCon {
        get {
        if (newOppCon == null)
            newOppCon = new OpportunityContactRole();
            return newOppCon;
        }
        set;
    }             
    public Cloud_Voice__c newCV {
        get {
        if (newCV == null)
            newCV = new Cloud_Voice__c();
            return newCV ;
        }
        set;
    }      
    public Cloud_Voice_Site__c newSite {
        get {
        if (newSite == null)
            newSite = new Cloud_Voice_Site__c();
            return newSite ;
        }
        set;
    }             
    public Cloud_Voice_Site_Product__c newProd {
        get {
        if (newProd == null)
            newProd = new Cloud_Voice_Site_Product__c();
            return newProd ;
        }
        set;
    }      
    public PageReference createCoreData() {
        if(selProduct =='Cloud Phone'){
            action = 'RC';
        }else if(selProduct =='Cloud Voice'){
            action = 'CV';
        }    
        //--------Check existing oppty
        if(String.isNotBlank(oId)){
            //--------Check existing RCDeal_ID
            String cvID = '';
            String ipcRT = [Select Id From RecordType WHERE DeveloperName ='Cloud_Phone' and SobjectType ='Cloud_Voice__c' limit 1].Id;
            List<Cloud_Voice__c> cvObjList = [SELECT Id, RecordTypeId, Opportunity__c, RCDeal_ID__c FROM Cloud_Voice__c WHERE RecordTypeId =: ipcRT AND Opportunity__c =: oId];
            if(cvObjList.Size() > 0){
                List<Cloud_Voice_Site__c> cvsObjList = [SELECT Id FROM Cloud_Voice_Site__c WHERE CV__c =: cvObjList[0].Id];
                //#### HAS A DEAL REG ID ####
                if(String.isNotBlank(cvObjList[0].RCDeal_ID__c)){
                    if(cvsObjList.isEmpty()){
                        //--------Add CP site
                        newSite.CV__c = cvObjList[0].Id;
                        newSite.Site_Name__c = 'Main Site';
                        newSite.Use_Contact_Address_for_Site__c = True;
                        newSite.Use_Site_Address_for_Billing__c = True;
                        newSite.Use_Site_Address_for_Delivery__c = True;        
                        newSite.RecordTypeId = [Select Id From RecordType WHERE DeveloperName ='Cloud_Phone' and SobjectType ='Cloud_Voice_Site__c' limit 1].Id;
                        newSite.Site_contact__c = cId;
                        insert newSite;
                        //--------Make a second call to populate location code check
                        update newSite;                    
                        HelperRollUpSummary.setCVSiteStop(false);
                        StaticVariables.setCloudVoiceDontRun(false);
                        cvID = newSite.Id;
                    }
                    else{
                        cvID = cvsObjList[0].Id;
                    }
                    //--------Direct to CloudVoiceQuickStart
                    PageReference retPg = page.CloudVoiceQuickStart;
                    String uL = string.valueof(licenses);
                    retPg.getParameters().put('users',uL);
                    retPg.getParameters().put('telNum',lineCheckNumber);
                    retPg.getParameters().put('selProduct',selProduct);
                    retPg.getParameters().put('accName',getaccName());
                    retPg.getParameters().put('cvHeadID',cvObjList[0].Id);
                    retPg.getParameters().put('cvID',cvID);
                    retPg.getParameters().put('conID',cId);
                    return retPg;
                }
                //#### NO DEAL REG ID ####
                else{
                    if(cvsObjList.isEmpty()){
                        //--------Add CP site
                        newSite.CV__c = cvObjList[0].Id;
                        newSite.Site_Name__c = 'Main Site';
                        newSite.Use_Contact_Address_for_Site__c = True;
                        newSite.Use_Site_Address_for_Billing__c = True;
                        newSite.Use_Site_Address_for_Delivery__c = True;        
                        newSite.RecordTypeId = [Select Id From RecordType WHERE DeveloperName ='Cloud_Phone' and SobjectType ='Cloud_Voice_Site__c' limit 1].Id;
                        newSite.Site_contact__c = cId;
                        insert newSite;
                        //--------Make a second call to populate location code check
                        update newSite;                    
                        HelperRollUpSummary.setCVSiteStop(false);
                        StaticVariables.setCloudVoiceDontRun(false);                         
                        cvID = newSite.Id;
                    }
                    else{
                        cvID = cvsObjList[0].Id;
                    }
                    //--------Direct to RC Deal Reg
                    PageReference retPg = page.RingCentralDealReg;
                    String uL = string.valueof(licenses);
                    retPg.getParameters().put('users',uL);
                    retPg.getParameters().put('telNum',lineCheckNumber);
                    retPg.getParameters().put('selProduct',selProduct);
                    retPg.getParameters().put('accName',getaccName());
                    retPg.getParameters().put('cvHeadID',cvObjList[0].Id);
                    retPg.getParameters().put('cvID',cvID);
                    retPg.getParameters().put('conID',cId);
                    return retPg;
                }
            }
        }
        //set to stop opp triggeres running
        StaticVariables.setCloudVoiceDontRun(true);
        //stop rollup summaries
        HelperRollUpSummary.setCVSiteStop(true);
        
        //CSS_Products__c css = [SELECT ID, Term__c FROM CSS_Products__c WHERE Id = :licenseType LIMIT 1];
        List<Cloud_Voice_Site_Product__c> prodList = new List<Cloud_Voice_Site_Product__c>();
        
        //get RecordTypes
        String oppRT = '';
        String ipcRT = '';
        String ipcsRT = '';
        oppRT = [Select Id From RecordType WHERE DeveloperName ='Cloud_Voice' and SobjectType ='Opportunity' limit 1].Id;
        if(action == 'RC'){
            ipcRT = [Select Id From RecordType WHERE DeveloperName ='Cloud_Phone' and SobjectType ='Cloud_Voice__c' limit 1].Id;
            ipcsRT = [Select Id From RecordType WHERE DeveloperName ='Cloud_Phone' and SobjectType ='Cloud_Voice_Site__c' limit 1].Id;
        }else{
            ipcRT = [Select Id From RecordType WHERE DeveloperName ='Cloud_Voice' and SobjectType ='Cloud_Voice__c' limit 1].Id;
            ipcsRT = [Select Id From RecordType WHERE DeveloperName ='Standard' and SobjectType ='Cloud_Voice_Site__c' limit 1].Id;
        }

        String oppID = '';
        if(String.isNotBlank(oId)){
            oppID = oId;
        }
        else{
            //insert opp
            newOpp.Name = 'IP Comms: ' + getaccName();
            newOpp.LeadSource = 'IP Comms Decisioning';
            newOpp.StageName = 'Created';
            newOpp.AccountId = aId;
            newOpp.Routing_Product__c = selProduct;
            newOpp.Product_Family__c = selProduct;
            newOpp.RecordTypeId = oppRT;
            newOpp.CloseDate = system.today()+30;
            if(sell != 'true'){
                newOpp.Auto_Assign_Owner__c = True;
                newOpp.Description = 'Number of users: ' + licenses + '\nNew LAN Required: ' + newLAN + '\nNew Cabling Required: ' + newSC + '\nCall Centre features: ' + ccFeatures;
            }
            insert newOpp;

            //set opp contact as primary
            newOppCon.ContactID = cId;
            newOppCon.OpportunityId = newOpp.Id;
            newOppCon.IsPrimary = True;
            insert newOppCon;
            oppID = newOpp.Id;
        }

        //add CV header
        newCV.Opportunity__c = oppID;       
        newCV.RecordTypeId = ipcRT;
        newCV.Customer_Contact__c = cId;
        insert newCV;
        
        //add CV site
        newSite.CV__c = newCV.ID;
        newSite.Site_Name__c = 'Main Site';
        newSite.Use_Contact_Address_for_Site__c = True;
        newSite.Use_Site_Address_for_Billing__c = True;
        newSite.Use_Site_Address_for_Delivery__c = True;        
        newSite.RecordTypeId = ipcsRT;
        newSite.Site_contact__c = cId;
        insert newSite;
        //make a second call to populate location code check
        update newSite;
    
        HelperRollUpSummary.setCVSiteStop(false);
        StaticVariables.setCloudVoiceDontRun(false);

        PageReference retPg = Null;

        if(action == 'RC' && sell == 'true'){
            String uL = string.valueof(licenses);
            retPg = page.RingCentralDealReg;
            retPg.getParameters().put('users',uL);
            retPg.getParameters().put('telNum',lineCheckNumber);
            /*
            retPg.getParameters().put('MAX',tMAX);
            retPg.getParameters().put('ADSL2',tADSL2);
            retPg.getParameters().put('WBC',tWBC);
            retPg.getParameters().put('FTTC',tFTTC);
            retPg.getParameters().put('FTTP',tFTTP);
            retPg.getParameters().put('FOD',tFOD);
            */
            retPg.getParameters().put('selProduct',selProduct);
            retPg.getParameters().put('accName',getaccName());
            retPg.getParameters().put('cvHeadID',newCV.Id);
            retPg.getParameters().put('cvID',newSite.Id);
            retPg.getParameters().put('conID',cId);
        }else{
            retPg = new PageReference('/'+oppID);
        }
        return retPg;
    }    
    public PageReference sellProduct(){
        sell = 'true';
        return createCoreData();
    }
    public PageReference dealRegRetry(){
        PageReference retPg = page.RingCentralDealReg;
        retPg.getParameters().put('users',System.currentPageReference().getParameters().get('users'));
        retPg.getParameters().put('telNum',System.currentPageReference().getParameters().get('telNum'));
        retPg.getParameters().put('selProduct',System.currentPageReference().getParameters().get('selProduct'));
        retPg.getParameters().put('accName',System.currentPageReference().getParameters().get('accName'));
        retPg.getParameters().put('cvHeadID',System.currentPageReference().getParameters().get('cvHeadID'));
        retPg.getParameters().put('cvID',System.currentPageReference().getParameters().get('cvID'));
        retPg.getParameters().put('conID',System.currentPageReference().getParameters().get('conId'));
        return retPg;
    }
    public PageReference cancelOrder(){
        String cvHeadID = System.currentPageReference().getParameters().get('cvHeadID');
        String cvID = System.currentPageReference().getParameters().get('cvID');
        String conId = System.currentPageReference().getParameters().get('conId');
        Cloud_Voice__c cv = [Select Id, Opportunity__c From  Cloud_Voice__c Where Id =: cvHeadID];
        Opportunity oppty = [Select Id, StageName, Sales_Stage_Detail__c From Opportunity Where Id =: cv.Opportunity__c];
        oppty.StageName = 'Cancelled';
        oppty.Sales_Stage_Detail__c = 'Deal Reg Blocked';
        Update oppty;
        Delete cv;
        PageReference retPage = new PageReference('/' + conId);     
        retPage.setRedirect(true);                             
        return retPage;
    }
    public Void validateContact(){
        contactValid = true;
        if(String.isNotBlank(cId)){
            Contact contact = [SELECT Id, firstName, lastName, email, phone FROM Contact WHERE Id =: cId];
            if (contact.firstName == '' || contact.firstName == null){
                contactValid = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Contact FIRST NAME is missing or invalid.'));
            }
            if (contact.lastName == '' || contact.lastName == null){
                contactValid = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Contact LAST NAME is missing.'));
            }
            if (contact.phone == '' || contact.phone == null){
                contactValid = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Contact PHONE is missing.'));
            }  
            if (contact.email == '' || contact.email == null){
                contactValid = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Contact EMAIL is missing.'));
            }
            if (contact.email == 'donotcontact@bt.co.uk'){
                contactValid = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Contact EMAIL is invalid (donotcontact@bt.co.uk).'));
            }
            else if (checkEmail(contact.email)){
                contactValid = false;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,'Contact EMAIL is invalid.'));
            }
        }
    }
    public static Boolean checkEmail(String semail) {
        String inputString = semail;
        String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern p = Pattern.compile(emailRegex);
        Matcher m = p.matcher(inputString);
        if (!m.matches()) {
            return true;
        }
        else {
            return false;
        }
    }
   
}