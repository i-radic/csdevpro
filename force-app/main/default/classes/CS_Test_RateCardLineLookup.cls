@isTest
private class CS_Test_RateCardLineLookup {
    
    private static Map<String, String> searchFieldsMap = new Map<String, String>();    
    private static String prodDefinitionID;
    private static Id[] excludeIds;
    private static Integer pageOffset;
    private static Integer pageLimit;
    
    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        cspmb__Rate_Card__c rc = new cspmb__Rate_Card__c( Name='test' );
        insert rc;
        
        cspmb__Rate_Card_Line__c rcl = new cspmb__Rate_Card_Line__c( Name='test',cspmb__Rate_Card__c = rc.Id);
        insert rcl;
        
    }

	private static testMethod void test() {
	    
	    createTestData();
	    Test.StartTest();
	    CS_RateCardLineLookup lkp = new CS_RateCardLineLookup();
        lkp.getRequiredAttributes();
	    Object[] result = lkp.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}

}