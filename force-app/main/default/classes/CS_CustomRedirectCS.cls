public class CS_CustomRedirectCS  {
    private Boolean redirectToBBApp = true;
    private ApexPages.StandardController controller;
    private String configId = '';
    public Boolean asyncMode {get; set;}
    private Id asyncJobId;
    
    public CS_CustomRedirectCS(ApexPages.StandardController stdController) {
        this.controller = stdController;
        if (ApexPages.currentPage().getParameters().get('cfgfinish') != null)
            this.redirectToBBApp = false;
        if (ApexPages.currentPage().getParameters().get('configId') != null)
            configId = (String)ApexPages.currentPage().getParameters().get('configId');
    }
    public PageReference redirectToPage() {
        PageReference pr;
        String URL = System.URL.getSalesforceBaseURL().getHost();
        if (!redirectToBBApp)
            pr = new PageReference('https://' + URL + '/apex/csbb__csbasketredirect?id=' + controller.getId());
        else
            pr = new PageReference('https://' + URL + '/apex/CS_BasketViewer?id=' + controller.getId());
        if (asyncJobId != null) {
             AsyncApexJob asyncJob = [SELECT Status  FROM AsyncApexJob  WHERE Id = :asyncJobId];if (asyncJob.Status == 'Completed') { return pr; }else{ return null;}
        }else {
            return pr;
        }
     }
    public PageReference doBasketCalculations() {
        Map<Id, cscfga__Product_Configuration__c> configs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{new cscfga__Product_Basket__c(Id = controller.getId())});
        cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(controller.getId());
        System.debug('test:::'+CS_CR_Basket_Calculations__c.getInstance().Sync_Basket_Size_Limit__c+':::'+configs.size());
        if (configs.size() <= CS_CR_Basket_Calculations__c.getInstance().Sync_Basket_Size_Limit__c) {
            asyncMode = false;
            CS_ProductBasketService.prepareTechFund(basket, configs, 'Tech_Fund__c');
            try{
            CS_Util.upsertReportConfigurationAttachment(configs.keySet());}catch(Exception exc){}
            try{
            CS_ProductBasketService.calculateBasketTotals(configs, basket);}catch(Exception exc){}
            CS_ProductBasketService.setSpecialConditions(configs, basket);
            update basket;
            return redirectToPage();
        } else {
            
            asyncMode = true;
            asyncJobId = System.enqueueJob(new CS_CustomRedirectQueueable(basket, configs,'Configration'));
            return null;
        }
    }
}