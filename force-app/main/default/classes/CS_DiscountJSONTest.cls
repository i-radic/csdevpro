@IsTest
public class CS_DiscountJSONTest  {
    testMethod static void testDiscountSaveObserver(){
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        CS_DiscountJSON cdjson = new CS_DiscountJSON();
        CS_DiscountJSON.Discount discount = new CS_DiscountJSON.Discount();
        
        discount.evaluationOrder = 'test';
        discount.recordType = 'single';
        discount.version = '3-0-0';
        
        CS_DiscountJSON.MemberDiscount mD = new CS_DiscountJSON.MemberDiscount(); 
        mD.chargeType = 'Charge';
        mD.recordType = 'TEST Type';
        mD.type ='test';
        mD.source ='tests';
        mD.discountCharge = '30';
        mD.description = 'test';
        mD.amount = '100';
        mD.version ='test';
        
        List<CS_DiscountJSON.MemberDiscount> MDLIST = new List<CS_DiscountJSON.MemberDiscount>();
        MDLIST.add(mD);
        discount.memberDiscounts = MDLIST;
        List<CS_DiscountJSON.Discount> DiscList = new List<CS_DiscountJSON.Discount>();
        DiscList.add(discount);
        cdjson.discounts = DiscList;
        Test.startTest();
        System.assertEquals(cdjson.discounts.size(), 1);
        Test.stopTest();
    }
}