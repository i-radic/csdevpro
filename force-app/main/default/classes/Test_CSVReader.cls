@isTest
private class Test_CSVReader {
    static testMethod void runPositiveTestCases() {
        String data = 'RateCard,gci.complex.customers@bt.com,7756410042,7756410042,';
        String delim = '\t';
        String data2 = '"RateCard,gci.complex.customers@bt.com",7756410042,7756410042,';
        String data3 = '';
        CSVReader csv = new CSVReader(data,delim);
        CSVReader csv1 = new CSVReader(data);
        CSVReader csv2 = new CSVReader(data2,delim);
        CSVReader csv3 = new CSVReader(data3);
        csv.readLine();
        csv1.readLine();
        csv2.readLine();
        csv3.readLine();
    }
}