@isTest
private class Test_AuditOpportunityProduct {

    static testMethod void positiveTest() { 
   // Test.startTest();   
        //Get the standard pricebook
      Pricebook2 stdPb = [select Id from Pricebook2 where isStandard=true limit 1];
       
       //  Id PriceBookId = Test.getStandardPricebookId(); //Used to get the Standard Price Book Id directly  instead of Querying.
        
       //Create an Account
    Account account = new Account(name='Rita Test Account 1');
    insert account;
    
    //Create an Opportunity
    Opportunity opp = new Opportunity(name='Rita Test Opp1',
                                        first_bill_date__c = system.today().addmonths(2),
                                        service_ready_date__c = system.today().addmonths(1),
                                        //Opportunity_Id__c='3141592654',
                                        type='New Business',
                                        stagename='Created',
                                        closedate=system.today(),
                                        accountid=account.id);
    
    insert opp; 
    
    //Create Product 1
    Product2 p = new Product2(name='RitaTestProd1', 
                                Description = 'test product',
                                family ='ICT',
                                Part_Number__c='2401418');
    insert p;    
    
    //Create Product 2
    Product2 p2 = new Product2(name='RitaTestProd2', 
                                Description = 'test product2',
                                family ='ICT',
                                Part_Number__c='2401419');
    insert p2;    
    
    //Create pricebook entry 1
    PricebookEntry pbe =  new PricebookEntry(pricebook2id = stdPb.id, 
                                                product2id = p.id, 
                                                unitprice=1.0, 
                                                isActive=true);
    insert pbe;
    
    //Create pricebook entry 1
    PricebookEntry pbe2 =  new PricebookEntry(pricebook2id = stdPb.id, 
                                                product2id = p2.id, 
                                                unitprice=1.0, 
                                                isActive=true);
    insert pbe2;
    
     Test.startTest();  
    
    //Insert opportunity line item 1
    OpportunityLineItem oli = new OpportunityLineItem (OpportunityId = opp.Id, 
                                                        description = 'opp1',
                                                        PricebookEntryId = pbe.Id, 
                                                        Qty__c = 1, 
                                                        Quantity = 1,
                                                        UnitPrice = 20000,
                                                        category__c = 'Main',
                                                        contract_term__c='24 Months',
                                                        billing_cycle__c='Monthly',
                                                        service_ready_date__c = system.today().addmonths(1),
                                                        first_bill_date__c = system.today().addmonths(2),
                                                        initial_cost__c = 4000);
    insert oli;
    system.debug('oncreation' + oli);
    
    
    //Insert opportunity line item 2
    OpportunityLineItem oli2 = new OpportunityLineItem (OpportunityId = opp.Id, 
                                                            description = 'opp2', 
                                                            PricebookEntryId = pbe2.Id, 
                                                            Qty__c = 1, 
                                                            Quantity = 4,
                                                            UnitPrice = 20000,
                                                            category__c = 'Standard',
                                                            contract_term__c='24 Months',
                                                            billing_cycle__c='Monthly',
                                                            service_ready_date__c = system.today().addmonths(1),
                                                            first_bill_date__c = system.today().addmonths(2),
                                                            initial_cost__c = 5000);
    insert oli2;
   
    
    // Update opportunity line item
    
    OpportunityLineItem oliUpdate = [select id,Qty__c,UnitPrice, initial_cost__c, ServiceDate from OpportunityLineItem where description = 'opp2' LIMIT 1];
    oliUpdate.Qty__c = 4;
    oliUpdate.UnitPrice = 15000;
    oliUpdate.Initial_Cost__c = 2500;
    oliUpdate.ServiceDate = system.today().addMonths(2);
    oliUpdate.Service_Ready_Date__c = system.today().addMonths(2);
    oliUpdate.Billing_Cycle__c = 'Annually';
    oliUpdate.First_Bill_Date__c = system.today().addMonths(3);
    oliUpdate.Sales_Type__c ='upgrade';
    oliUpdate.Category__c ='additional';
    oliUpdate.Overlay_Specialist__c = UserInfo.getUserId();
    oliUpdate.Contract_Term__c = '36 Months';
    
    
        
    update oliUpdate;
     Test.stopTest();
   
    delete oli2;
    
     
               
    }
}