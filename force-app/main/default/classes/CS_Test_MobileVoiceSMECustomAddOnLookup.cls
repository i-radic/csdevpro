@isTest
private class CS_Test_MobileVoiceSMECustomAddOnLookup {
    
    private static cscfga__Product_Definition__c prodDefinition;
 
    private static Map<String, String> searchFieldsMap1 = new Map<String, String>();    
    private static Map<String, String> searchFieldsMap2 = new Map<String, String>();    
    private static String prodDefinitionID;
    private static Id[] excludeIds;
    private static Integer pageOffset = 0;
    private static Integer pageLimit;
    
    private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
        
        RecordType recordType = [SELECT Id, Name from RecordType where Name = 'Shared Add-On'];
        
        // Service Plan 
        cspmb__Price_Item__c servicePlan = new cspmb__Price_Item__c ();
        servicePlan.Name =  'Business Connect';
        servicePlan.Type__c = 'Data';
        insert servicePlan;
        
        //AddOn PriceItem 
        cspmb__Add_On_Price_Item__c addOn = new cspmb__Add_On_Price_Item__c();
        addOn.Name = 'BC 10GB';
        insert addOn;
        
        // addOn Association-
        cspmb__Price_Item_Add_On_Price_Item_Association__c association = new cspmb__Price_Item_Add_On_Price_Item_Association__c ();
        association.cspmb__Add_On_Price_Item__c = addOn.Id;
        association.cspmb__Price_Item__c = servicePlan.Id;
        insert association;
                
		searchFieldsMap1.put('Parent Service Plan Level', servicePlan.Id);
      	searchFieldsMap1.put('Type', 'Data');
		searchFieldsMap1.put('Record Type','Shared Add-On');
		searchFieldsMap1.put('Tenure','24');
		
		searchFieldsMap2.put('Parent Service Plan Level', servicePlan.Id);
      	searchFieldsMap2.put('Type', 'Data');
		searchFieldsMap2.put('Record Type','Single Add-On');
		searchFieldsMap2.put('Tenure','24');
		
    }
    
     static testMethod void CustomAddOnLookupTest() {
     	createTestData();
     	Test.StartTest();
     	CS_MobileVoiceSMECustomAddOnLookup customLookup = new CS_MobileVoiceSMECustomAddOnLookup ();
     	String attr = customLookup.getRequiredAttributes();
     	Object[] addOnsResult1 = customLookup.doLookupSearch(searchFieldsMap1, prodDefinitionID, excludeIds, pageOffset, pageLimit);
     	Object[] addOnsResult2 = customLookup.doLookupSearch(searchFieldsMap2, prodDefinitionID, excludeIds, pageOffset, pageLimit);
		System.Assert(true);    	
     	Test.StopTest();
     }
    
}