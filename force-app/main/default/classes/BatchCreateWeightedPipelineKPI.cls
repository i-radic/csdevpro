global class BatchCreateWeightedPipelineKPI implements Database.Batchable<sObject>, Database.Stateful
{
    global map<date, decimal> dateMap = new map<date, decimal>(); //holds final results
    
    GlobalKPISettings__c gs = GlobalKPISettings__c.getInstance();
    GlobalKPI_Helper gh = new GlobalKPI_Helper();
    public string businessName  = gs.Business_Unit__c; 
    public string busCode       = gs.Business_Unit_Code__c; 
        
    public string theQuery;
    public string thisPeriod; 
    global List<Global_KPI__c> gkpiUpdList = new List<Global_KPI__c>();      
    global BatchCreateWeightedPipelineKPI ()
    {
        
        
        theQuery = 'select closeDate, amount, probability from Opportunity where Account_Sub_Sector__c = \'TIKIT\' and isClosed=false and closeDate >= THIS_MONTH and (closeDate = THIS_FISCAL_YEAR or closeDate=NEXT_FISCAL_YEAR)';
        
        Date map_start_date = date.valueOf([Select startDate From Period Where type = 'Month' and StartDate = THIS_MONTH].startDate );
        Date map_end_date   = [Select endDate From Period Where type = 'Year' and StartDate = NEXT_FISCAL_YEAR].endDate ;
        
        system.debug(map_start_date);
        system.debug(map_end_date);
    
        for(Date d = map_start_date ; d < map_end_date.addDays(1) ; d=d.addMonths(1)  )
        {
            dateMap.put(d, 0);  
        }
        system.debug(dateMap);
                
        system.debug(theQuery);
        system.debug ('*****************************BatchCreateWeightedPipelineKPI'); 
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug ('*****************************BatchCreateWeightedPipelineKPI QueryLocator() method');     
        return Database.getQueryLocator(theQuery);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope)
    {
        system.debug ('*****************************BatchCreateWeightedPipelineKPI Execute() method');  
        
        for(sObject s : scope) //loop through rows of scope list
        {
            Opportunity o = (Opportunity) s;
            if (o.amount ==null || o.probability==null) 
            {
                //do nothing
            } 
            else 
            {   
                system.debug(o.closeDate.toStartOfMonth());
                system.debug(dateMap.get(o.closeDate.toStartOfMonth()));
                dateMap.put(o.closeDate.toStartOfMonth(), dateMap.get(o.closeDate.toStartOfMonth()) + (o.amount * (o.probability/100)));
                system.debug(dateMap.get(o.closeDate.toStartOfMonth()));
            }
                
        }
        system.debug(dateMap);
        
    }    
    
        
    global void finish(Database.BatchableContext BC)
    {  
        
        system.debug ('*****************************BatchCreateWeightedPipelineKPI Finish() method');       
        
        string dtFrom;
        string dtTo;
        string periodCode;
        
        periodCode='MN';
        for (date d : dateMap.keySet())
        {
            Global_KPI__c kpi           = new Global_KPI__c();
            kpi.business_unit__c        = businessName;
            kpi.Measure_Name__c         = 'Weighted Pipeline';
            kpi.Unit_of_Measure__c      = 'Currency';
            kpi.Date_From__c            = d;
            dtFrom                      = string.valueOf(d);
            kpi.Date_To__c              = [Select endDate From Period Where type = 'Month' and StartDate = :d].endDate;
            dtTo                        = string.valueOf(kpi.Date_To__c) ;
            kpi.uniqueKey__c = busCode + ':' + 'PIP' + ':' + periodCode + ':' + dtFrom.replace('-','') + ':' + dtTo.replace('-','');  
            kpi.Period_Type__c          = 'Month';
            kpi.result__c = dateMap.get(d);
            
            //insert or update it using external ID field uniqueKey__c as the key
            system.debug(kpi);
            upsert kpi uniqueKey__c;
        }
            
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,TotalJobItems, CreatedBy.Email
                         FROM AsyncApexJob WHERE Id =:BC.getJobId()];

        // Send an email to the Apex job's submitter notifying of job completion.     
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        mail.setToAddresses(toAddresses);
        mail.setSubject('BatchCreateWeightedPipelineKPI ' + a.Status);
        mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });  
    }


}