//M&Ps eContactCard  Class
public With Sharing class eContactCardListButtonCon {        
    public List<ConInnerClass> ContactInnerList {get;set;}
    public Boolean ShowPanel{get;set;}
    public string AccountId {get;set;}
    
    public eContactCardListButtonCon(ApexPages.StandardSetController controller) {
        AccountId = ApexPages.CurrentPage().getParameters().get('id');
        List<Contact> Con = [select id,Name,Email,Sent_Contact_eCard__c  From Contact where Accountid = : ApexPages.CurrentPage().getParameters().get('id') 
                            and Email !=Null and Status__c = 'Active' Order By Name];
        ContactInnerList = new List<ConInnerClass>();
        for(Contact c: Con){
            ContactInnerList.add(new ConInnerClass(c));    
        }    
    }   
    public class ConInnerClass{
        public Boolean Selected{get;set;}
        public Contact Con{get;set;}
        public ConInnerClass(Contact C){
            Selected = false;
            Con = c;        
        }
    }
    
    public PageReference SendeCard(){
        set<id> ContactId= new set<Id>();     
        List<Contact> UpdateContactsList = new List<contact>();
       
        for(ConInnerClass SeletedContacts:ContactInnerList){
            if(SeletedContacts.Selected == True){
                ContactId.add(SeletedContacts.Con.Id);    
            }
          }
            for( contact UpdateSentCheckBox : [select id,send_Contact_ecard__c,sent_Contact_ecard__c from Contact where id IN:ContactId]){
                UpdateSentCheckBox.send_Contact_ecard__c = True;
                UpdateContactsList.add(UpdateSentCheckBox );             
            }
           
            Database.Update(UpdateContactsList,false) ;
       
        pagereference Pg = new Pagereference('/'+apexPages.CurrentPage().getParameters().get('id'));
        pg.setRedirect(true);
        return Pg;
    }
     public PageReference Cancel(){
        pagereference Pg = new Pagereference('/'+apexPages.CurrentPage().getParameters().get('id'));
         pg.setRedirect(true);
        return Pg;
    }  
    
    public void Preview(){
        ShowPanel = TRUE;
    } 
    public void CancelPreview(){
        ShowPanel = False;
    }   
        
}