global class BatchDeleteCRFSensitiveData implements Database.Batchable<SObject>{
 
    private String query;
    
    global BatchDeleteCRFSensitiveData(String q){
        this.query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<CRF__c> crfs = new List<CRF__c>();
        for(Sobject crf : scope){
            CRF__c c = (CRF__c)crf;
            if(c.Customer_bank_account_number__c!=null)
            c.Customer_bank_account_number__c = '0000000000';
            if(c.Sort_code__c!=null)            
            c.Sort_code__c = '000000';
            crfs.add(c);
        } 
        if(!crfs.isEmpty())
            update crfs;
    }

    global void finish(Database.BatchableContext BC){
    }
}