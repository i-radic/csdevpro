@isTest(seeAllData=True)
private class Test_InternationalSupportBefore {
    static testMethod void runTest1(){
       
        Account A  = Test_Factory.CreateAccount();
        A.name = 'TESTCODE 3';
        A.sac_code__c = 'testSAC1';
        A.Sector_code__c = 'CORP1';
        A.LOB_Code__c = 'LOB1';
        A.OwnerId = UserInfo.getUserId();
        A.CUG__c='CugTest1';
        Database.SaveResult accountResult = Database.insert(A);
        
        Opportunity opp = new Opportunity();
        opp.AccountId = A.id;
        opp.Name = 'tst_oppty';
        //opp.NIBR_Year_2009_10__c = 1;
        //opp.NIBR_Year_2010_11__c = 1;
        //opp.NIBR_Year_2011_12__c = 1;
        opp.StageName = 'Created';
        opp.Sales_Stage_Detail__c = 'Appointment Made';
        opp.CloseDate = system.today();
        
        insert opp;
        
        List<International_Support__c> InsertList =new List<International_Support__c>();
        for(integer i =0;i<=10;i++){
        International_Support__c Int_sup=new International_Support__c()  ;
        if(i==0)
        Int_sup.Status__c='Rejected';
        if(i==1)
        Int_sup.Status__c='Assigned';
        if(i==2)
        Int_sup.Status__c='In Progress';
        if(i==3)
        Int_sup.Status__c='Created';
        if(i==4)
        Int_sup.Status__c='Deferred';
        if(i==5)
        Int_sup.Status__c='Vet and Validation';
        if(i==6)
        Int_sup.Status__c='Vet and Validation – PPR';
        if(i==7)
        Int_sup.Status__c='Sent to Workflow';
        if(i==8)
        Int_sup.Status__c='Waiting on Someone Else';
        if(i==9)
        Int_sup.Status__c='Completed';
        if(i==10)
        Int_sup.Status__c='Cancelled';
            
        Int_sup.Requestor_Name__c=UserInfo.getUserId();
        Int_sup.Request_Title__c='Test';
        Int_sup.Request_Type__c='In-flight Change';
        Int_sup.Pricing_System__c='APE';
        Int_sup.Contract_Term__c='1';
        Int_sup.No_Of_Connections__c=1;
        Int_sup.Required_Date__c=date.today();
        Int_sup.Description__c='Test';
        Int_sup.Status_Detail__c='Test';
        Int_sup.Opportunity__c=opp.id;
            
            InsertList.add(Int_sup);
        }
        
        Insert InsertList;
        
        International_Support__c Int_sup=new International_Support__c()  ;       
        Int_sup.Status__c='Rejected';        
        Int_sup.Requestor_Name__c=UserInfo.getUserId();
        Int_sup.Request_Title__c='Test';
        Int_sup.Request_Type__c='In-flight Change';
        Int_sup.Pricing_System__c='APE';
        Int_sup.Contract_Term__c='1';
        Int_sup.No_Of_Connections__c=1;
        Int_sup.Required_Date__c=date.today();
        Int_sup.Description__c='Test';
        Int_sup.Status_Detail__c='Test';
        Int_sup.Opportunity__c=opp.id;
        
       
        Insert Int_sup;
        
        
        Int_sup.Status__c='Assigned';
        Update Int_sup;
        
        Int_sup.Status__c='In Progress';
        Update Int_sup;
        
        Int_sup.Status__c='Deferred';
        Update Int_sup;
       
        Int_sup.Status__c='Waiting on Someone Else';
        Update Int_sup;
        
        Int_sup.Status__c='Cancelled';
        Update Int_sup;
        
        Int_sup.Status__c='Completed';
        Update Int_sup;
        
        Int_sup.Status__c='Sent to Workflow';
        Update Int_sup;
        
        Int_sup.Status__c='Created';
        Update Int_sup;
            
        Int_sup.Status__c='Vet and Validation – PPR';
        Update Int_sup;    
            
        Int_sup.Status__c='Vet and Validation';
        Update Int_sup;    
        
        Int_sup.Status__c='Assigned';
        Update Int_sup;
        
        Int_sup.Status__c='Rejected';
        Update Int_sup;

        }
}