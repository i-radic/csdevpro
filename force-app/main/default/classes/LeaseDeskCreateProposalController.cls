public with sharing class LeaseDeskCreateProposalController {
	/******************************************************************************************************************
     Name:  LeaseDeskCreateProposalController.class()
     Copyright © 2012  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    
    This class covers use case SF-003 Create Leasing proposal/agreement
    Another extension of “Raise Opportunity” – if the customer wants to go ahead with Leasing then SF will present 
    a weblink to the LeaseDesk portal. LeaseDesk will render a proposal form, partially populated with appropriate 
    data from SF.
                                                           
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR                 DATE              DETAIL                           FEATURES
    1.0 -    Rita Opoku-Serebuoh    20/03/2012        INITIAL DEVELOPMENT              Initial Build: 
             Adam Soobroy
    
    ******************************************************************************************************************/
    
    public Leasing_History__c leasingHistory {get;set;}
    public String leasingHistoryId {get;set;}
    public String opptyId {get;set;}
    public String oId {get;set;}
    public Account account {get;set;}
    public User user {get;set;}
    
    public LeaseDeskCreateProposalController(ApexPages.StandardController stdController) {
    }
    
    public PageReference OnLoad()
    {
        Decimal totalCost;
        leasingHistoryId = ApexPages.currentPage().getParameters().get('id');  
        oId = ApexPages.currentPage().getParameters().get('oid'); 
        opptyId = ApexPages.currentPage().getParameters().get('opptyid');  
        if(Test_Factory.GetProperty('IsTest') == 'yes' || Test_Factory.GetProperty('IsTest') == 'no'){
        	
        }
        else{
        	leasingHistory = LeaseDeskUtil.getLeasingHistory(leasingHistoryId);
        	account = LeaseDeskUtil.getAccountDetails(leasingHistory.Account__c);
        	user = LeaseDeskUtil.getUserDetails();
        }
        if(LeaseDeskUtil.isBTLBAccount(account.Id)){
         	totalCost = LeaseDeskUtil.getTotalOutrightSaleCost(opptyId);
        }
        else{
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'This functionality is not available for Corp accounts!'));
        	return null;
        	//totalCost = LeaseDeskUtil.getTotalOutrightSaleCostCORP(opptyId);
        }  
             
        if (leasingHistory != null){
            LeaseDeskAPI leaseDeskAPI = new LeaseDeskAPI();          
            String funderURL;
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
            	funderURL = 'TEST';
            }
            else{          	
            	funderURL = leaseDeskAPI.CreateProposal(user.EIN__c, user.OUC__c,leasingHistory.AgreementID__c, oId, Double.valueOf(totalCost));          	          	
            }           
            if (funderURL.contains('ERROR')){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, funderURL));             
                return null;
            }else{
                leasingHistory.Funder_URL__c = funderURL;
                
                System.debug('##################### RESPONSE FUNDER URL: ' + funderURL);
                update leasingHistory;
                
                // Redirect to funder portal
                PageReference funderPageRef = new PageReference(funderURL);
                funderPageRef.setRedirect(true);
                if(Test_Factory.GetProperty('IsTest') == 'yes'){
                	                	
                }
                else{
                	return funderPageRef;
                }              
            }              
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'There appears to be a problem. Unable to redirect to funder website'));
        return null;
    }
    
}