/*
###########################################################################
# File..................: EE_ActionUpdateTriggerHelper
# Version...............: 1
# Created by............: Etika Varma
# Created Date..........: 12-march-2013
# Last Modified by......: 
# Last Modified Date....: 
# Description...........: This Class is used as helper for EE_ActionUpdateAfterEvents trigger.     
# VF page...............:             
# Change Log:           
*/  
public with sharing class EE_ActionUpdateTriggerHelper {
	
	/**
    * <P> This method is used to update Progress update from Action update to parent.</P> 
    * @param1: Old Project update map
    * @param2: New Project update map
    * @param3: Trigger type identifier
    * @return:
    */
     public static void updateProgresscommentsOnTracker(Map<Id,Action_update__c> newActionMap,Map<Id,Action_update__c> oldActionMap, Integer triggerType){
     	
     	Set <Id> actionTrackerIds = new Set <Id> ();
     	List<Action_update__c> lstActionUpdates = new List<Action_update__c > ();
     	Map<Id,Action_update__c> trackerId_UpdateMap = new Map<Id,Action_update__c> ();
     	List<Action_Tracker__c> tobeUpdatedTrackers =new List<Action_Tracker__c> ();
     	
     	for(Action_update__c au: newActionMap.values()){
     		if(triggerType == 1 && au.Internal_Comments__c ==false && String.IsNotBlank(au.Progress_Update__c)){
     			actionTrackerIds.add(au.Action_Tracker__c);
     		}else{
     			//check the condition for trigger
                if(null!= oldActionMap && oldActionMap.containsKey(au.Id)){
                    system.debug('##oldActionMap.get(au.Action_Tracker__c)' + oldActionMap.get(au.Id));
                    if((au.Internal_Comments__c== false) && (null!= oldActionMap.get(au.Id).Action_Tracker__c) &&
                    	 (oldActionMap.get(au.Id).Progress_Update__c != newActionMap.get(au.Id).Progress_Update__c))                    	 
                    	 //add the Action Tracker id to be updated
                    	 actionTrackerIds.add(au.Action_Tracker__c);
                }
     		}
     	}
     	
     	
     	//Get The Action Update record for the Action Tracker     	
     	List<Action_update__c> lstallActionUpdates = [SELECT Id,Action_Tracker__c,Internal_Comments__c,Progress_Update__c,createddate FROM Action_update__c
     												 WHERE Action_Tracker__c IN : actionTrackerIds ];
     												 
     	system.debug('##lstActionUpdates:' + lstallActionUpdates);
     	
     	for(Action_update__c au : lstallActionUpdates){
     		if(trackerId_UpdateMap.containsKey(au.Action_Tracker__c)){
     			if(au.CreatedDate > trackerId_UpdateMap.get(au.Action_Tracker__c).createddate){//check for latest record
     				trackerId_UpdateMap.put(au.Action_Tracker__c,au);
     			}
     			
     		}else{
     			trackerId_UpdateMap.put(au.Action_Tracker__c,au);
     		}
     	}
     	
     	//update the parent Action tracker with latest action update.
     	if(null!= trackerId_UpdateMap && trackerId_UpdateMap.size() >0){
     		for(Id trackerId : trackerId_UpdateMap.keyset()){
     			Action_Tracker__c act = new Action_Tracker__c (Id=trackerId);
     			act.Progress_Update__c = (trackerId_UpdateMap.get(trackerId).createddate).format('dd/MM/yyyy') + ' - ' +
     									 trackerId_UpdateMap.get(trackerId).Progress_Update__c;//update Date+ Comments
     			tobeUpdatedTrackers.add(act);
     		}
     	}
     	
     	system.debug('##tobeUpdatedTrackers:' + tobeUpdatedTrackers);
     	if(null!=tobeUpdatedTrackers && tobeUpdatedTrackers.size() >0)
     	database.update (tobeUpdatedTrackers,false);										 
     	
     } 
	

}