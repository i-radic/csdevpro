public class createBulkTasksContract {
    public Contract contract = null;
    public Date sDate = null;
    public Date QtrStartDate = null;
    public Date QtrEndDate = null;
    public Date QtrHistoricStartDate = null;
    public Double cTerm = 0;
    public Integer qrtr = 0;
    public Double qrtr2 = 0;
    public Integer diff = 0;
    public Integer aSize = 0;
    public Integer tSize = 0;
    public Integer tUvsSize = 0;
    public Integer tUvsMSize = 0;
    public Integer tMSize = 0;
    public Integer prevActs = 0;
    public Integer currentActs = 0;
    public Integer testing = 0;
    public id loggedinID = UserInfo.getUserId();
    public string loggedinTeamFunction = null;
    public string taskRole = null;
    public string Sector = null;
    public string lob = null;

    public PageReference createContractTasks() {
        if (this.contract != null) {
            Id contractId = contract.Id;
            
            //Get contract & associated Account info
            List<Contract> contracts = [select account.id, account.lob_code__c, account.Sector_code__c,  Id, StartDate, ContractTerm, Last_Rollover_Date__c, Contract_Term_years__c, SAC_Code__c, LE_Code__c from Contract where Id = :contractId];
            contract = contracts[0];
            cTerm = contract.Contract_Term_years__c;
            Sector = contract.account.Sector_code__c;
            Lob = contract.account.lob_code__c;
            
            //get user team function to determin tasks to create
            List <AccountTeamMember> userRole = [Select a.AccountId, a.TeamMemberRole, a.UserId from AccountTeamMember a where a.UserId = :loggedinID and a.accountId = :contract.account.id];
            if (userRole.size() > 0) {
                taskRole = userRole[0].TeamMemberRole;
            }
    
            //get List of  existing open Tasks or are not due for presenting for completion again yet
            set <String> existTasks = new Set<String>();
            List<Task> existTasksQ = [Select Task_Id__c from Task where WhatId = :contractId and Task_Category__c = 'Contract' and (isClosed = false or Minimum_Repeat_Date__c > :Date.today())];
            for (Task et :existTasksQ) {
                existTasks.add(et.Task_Id__c);
            }

            if (Sector == 'BTLB' ){// get BTLB tasks else assume task are at Line of Business (LOB) level
                // get list of tasks to be created for BTLB user
                List<Bulk_Tasks__c> tasks1 = [Select q.Reminder_Days__c, q.Minimum_Repeat_Time__c, q.Minimum_Lead_Time__c, q.Details__c, q.Description__c, q.CreatedDate, q.Task_Id__c, Collateral_text__c, Collateral_url__c, Activity_area__c, Activity_type__c, System_text__c, System_url__c, Task_Category__c 
                                              From Bulk_Tasks__c q 
                                              where Task_Category__c = 'Contract' and Active_Task_Switch__c = true and sector_code__c = :Sector and USER_Type__c =:taskRole and Task_Id__c not in :existTasks];

                aSize = tasks1.size();
                
                //create tasks for identified BTLB accounts
                List<Task> newTasks = new List<Task>();
                for (Bulk_Tasks__c qa : tasks1) {
                    Task nt = new Task();
                    nt.ActivityDate = (Date.today()+ qa.Minimum_Lead_Time__c.intValue());
                    nt.Target_Completion_Date__c = nt.ActivityDate;
                    nt.ReminderDateTime = datetime.newInstance((nt.ActivityDate - qa.Reminder_Days__c.intValue()).year(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).month(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).day(),8,0,0);
                    if (qa.reminder_days__c != null){
                        nt.ISREMINDERSET = TRUE;
                    }
    
                    nt.OwnerId = UserInfo.getUserId();
                    nt.Subject = qa.Description__c;
                    nt.WhatId = contract.Id;
                    nt.Task_Id__c = qa.Task_Id__c;
                    //nt.Promote__c = qa.Promote__c;
                    nt.Details__c = qa.Details__c;
                    nt.Collateral_text__c = qa.Collateral_text__c;
                    nt.Collateral_url__c = qa.Collateral_url__c;
                    nt.System_text__c = qa.System_text__c;
                    nt.System_url__c = qa.System_url__c;
                    nt.Task_Category__c = qa.Task_Category__c;
                    nt.SAC_Code__c = contract.SAC_Code__c;
                    nt.LE_Code__c = contract.LE_Code__c;
                    
                    if (qa.Minimum_Repeat_Time__c != null){
                        nt.Minimum_Repeat_Date__c = (Date.today()+ qa.Minimum_Repeat_Time__c.intValue());
                    }
                    else{
                        nt.Minimum_Repeat_Date__c = (Date.today()-1);
                    }
                        newTasks.add(nt);
                    }
                    tSize = newTasks.size();
                    insert newTasks;
                }
                else{ // get corporate tasks
                    List<Bulk_Tasks__c> tasks2 = [Select q.Reminder_Days__c, q.Minimum_Repeat_Time__c, q.Minimum_Lead_Time__c, q.Details__c, q.Description__c, q.CreatedDate, q.Task_Id__c, Collateral_text__c, Collateral_url__c, Activity_area__c, Activity_type__c, System_text__c, System_url__c, Task_Category__c 
                                                From Bulk_Tasks__c q 
                                                where Task_Category__c = 'Contract' and Active_Task_Switch__c = true and LOB_code__c = :lob and USER_Type__c =:taskRole and Task_Id__c not in :existTasks];
                    aSize = tasks2.size();
                    //create tasks for identified non BTLB accounts
                    List<Task> newTasks = new List<Task>();

                    for (Bulk_Tasks__c qa : tasks2) {
                        Task nt = new Task();
                        nt.ActivityDate = (Date.today()+ qa.Minimum_Lead_Time__c.intValue());
                        nt.Target_Completion_Date__c = nt.ActivityDate;
                        nt.ReminderDateTime = datetime.newInstance((nt.ActivityDate - qa.Reminder_Days__c.intValue()).year(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).month(),(nt.ActivityDate - qa.Reminder_Days__c.intValue()).day(),8,0,0);
                        if (qa.reminder_days__c != null){
                            nt.ISREMINDERSET = TRUE;
                        }
                        nt.OwnerId = UserInfo.getUserId();
                        nt.Subject = qa.Description__c;
                        nt.WhatId = contract.Id;
                        nt.Task_Id__c = qa.Task_Id__c;
                        //nt.Promote__c = qa.Promote__c;
                        nt.Details__c = qa.Details__c;
                        nt.Collateral_text__c = qa.Collateral_text__c;
                        nt.Collateral_url__c = qa.Collateral_url__c;
                        nt.System_text__c = qa.System_text__c;
                        nt.System_url__c = qa.System_url__c;
                        nt.Task_Category__c = qa.Task_Category__c;
                        nt.SAC_Code__c = contract.SAC_Code__c;
                        nt.LE_Code__c = contract.LE_Code__c;
                        if (qa.Minimum_Repeat_Time__c != null){
                            nt.Minimum_Repeat_Date__c = (Date.today()+ qa.Minimum_Repeat_Time__c.intValue());
                        }
                        else{
                            nt.Minimum_Repeat_Date__c = (Date.today()-1);
                        }
                        newTasks.add(nt);
                    }
                    tSize = newTasks.size();
                    insert newTasks;
            }


            //insert UVS TASKS

            // use rollover date if greater that startdate
            if (contract.Last_Rollover_Date__c > contract.StartDate) {
                sDate = contract.Last_Rollover_Date__c;
            }
            else {
                sDate = contract.StartDate;
            }

            //calculate rather than code diff - note Diff is variable to hold number of months
            diff = ((Date.today().year() - sDate.year())*12)+ Date.today().month() - sDate.month();
            // account for day within month when calculating month differences. ie subtract 1 where start day is greater than current day
            if ((Date.today().day() - sDate.day())<0) {
            diff = diff -1;
        }
        
        // get rid of negative ( nb not sure there should be any !)
        diff = math.abs(diff);
        //calculate qrtr rather than code individual values
        qrtr2 = (diff / 3) + 1 ;
        qrtr = qrtr2.intValue();
        //Calculate Qtr Start & End Dates
        QtrStartDate = sDate.addMonths(3*(qrtr-1));
        QtrEndDate = sDate.addMonths(3*qrtr);
        QtrEndDate = QtrEndDate.addDays(-1);
        // the date deemed that task is more 2 qtr old
        QtrHistoricStartDate = QtrStartDate.addMonths(6);

        // calculate tasks as long as contract date is not in future
        if (qrtr > 0) {

            //get List of existing activities already present for current qtr
            set <String> exUvsTasks = new Set<String>();
            List<Task> exUvsTasksQ = [Select Task_Id__c from Task where Task_Category__c = 'UVS Defence Portal' and WhatId = :contractId and Activity_Quarter__c = :qrtr and status <>'Cancelled' ];
            for (Task eut :exUvsTasksQ) {
                exUvsTasks.add(eut.Task_Id__c);
            }

            // get list of tasks to be created for user
            List<Bulk_Tasks__c> UvsTasks1 = [Select q.Minimum_Lead_Time__c, q.SystemModstamp, q.Quarter__c, q.OwnerId, q.Name, q.LastModifiedDate, q.LastModifiedById, q.IsDeleted, q.Id, q.Details__c, q.Description__c, q.Days_Elapsed__c, q.CreatedDate, q.CreatedById, q.Task_Id__c, Collateral_text__c, Collateral_url__c, Activity_area__c, Activity_type__c, System_text__c, System_url__c, Task_Category__c 
            From Bulk_Tasks__c q 
            where  Task_Category__c = 'UVS Defence Portal' and q.Quarter__c = :qrtr and  Active_Task_Switch__c = true and Contract_Length__c  = :cTerm and lob_code__c = :Lob and USER_Type__c =:taskRole and Task_Id__c not in :exUvsTasks];
            tUvsSize = UvsTasks1.size();

            //create tasks for identified quarterly activities
            List<Task> newTasks = new List<Task>();
            for (Bulk_Tasks__c qa : UvsTasks1) {
                Task nt = new Task();
                if ((sDate + qa.Days_Elapsed__c.intValue()).addMonths(3*(qrtr-1))>= (Date.today()+ 7)) {
                    // If Activity Date is more 7 days into future, create activity as normal
                    nt.ActivityDate = (sDate + qa.Days_Elapsed__c.intValue()).addMonths(3*(qrtr-1));
                }
                else if ((Date.today()+ 7)<= QtrEndDate) {
                    // If Activity Date + 7 days does not go over Qtr end date create activity with 7 days lead time
                    nt.ActivityDate = (Date.today()+ 7);
                }
                else {
                    // create activity with due date of qtr end date
                    nt.ActivityDate = QtrEndDate;
                }
                nt.Target_Completion_Date__c = nt.ActivityDate;
                nt.ReminderDateTime = datetime.newInstance((nt.ActivityDate - 14).year(),(nt.ActivityDate - 14).month(),(nt.ActivityDate - 14).day(),8,0,0);
                nt.ISREMINDERSET = TRUE;
                nt.OwnerId = UserInfo.getUserId();
                nt.Subject = 'Q'+qrtr+' '+qa.Description__c;
                nt.WhatId = contract.Id;
                nt.Task_Id__c = qa.Task_Id__c;
                //nt.Mandatory_Activity__c = qa.Mandatory_Activity__C;
                nt.Activity_Quarter__c = qrtr;
                //nt.Promote__c = qa.Promote__c;
                nt.Details__c = qa.Details__c;
                nt.Activity_area__c = qa.Activity_area__c;
                nt.Activity_type__c = qa.Activity_type__c;
                nt.Collateral_text__c = qa.Collateral_text__c;
                nt.Collateral_url__c = qa.Collateral_url__c;
                nt.System_text__c = qa.System_text__c;
                nt.System_url__c = qa.System_url__c;
                nt.Task_Category__c = qa.Task_Category__c;
                nt.SAC_Code__c = contract.SAC_Code__c;
                //nt.Qtr_Start_Date__c = QtrStartDate;
                //nt.Qtr_End_Date__c = QtrEndDate;
                //nt.Qtr_Historic_Start_Date__c = QtrHistoricStartDate;
                newTasks.add(nt);
            }

            tUvsSize = newTasks.size();
            insert newTasks;

            // create tasks for missing mandatory activities

            //get List of existing mandatory activities already present against contract
            set <String> exUvsManTasks = new Set<String>();
            List<Task> exUvsManTasksQ = [Select Task_Id__c from Task where Task_Category__c = 'UVS Defence Portal' and WhatId = :contractId];
            for (Task emt :exUvsManTasksQ) {
                exUvsManTasks.add(emt.Task_Id__c);
            }

            //Get List of Mandatory tasks to create
            List<Bulk_Tasks__c> qMAct = [Select q.Minimum_Lead_Time__c, q.SystemModstamp, q.Quarter__c, q.OwnerId, q.Name, q.LastModifiedDate, q.LastModifiedById, q.IsDeleted, q.Id, q.Details__c, q.Description__c, q.Days_Elapsed__c, q.CreatedDate, q.CreatedById, q.Task_Id__c, Collateral_text__c, Collateral_url__c, Activity_area__c, Activity_type__c, System_text__c, System_url__c, Task_Category__c 
            From Bulk_Tasks__c q 
            where Task_Category__c = 'UVS Defence Portal' and q.Quarter__c <= :qrtr and  Active_Task_Switch__c = true and Contract_Length__c  = :cTerm and  Sector_code__c = :Sector and USER_Type__c =:taskRole and  Task_Id__c not in :exUvsManTasks];
            tUvsMSize = qMAct.size();
            //create tasks for identified Mandatory quarterly activities

            List<Task> newMTasks = new List<Task>();
            for (Bulk_Tasks__c qa : qMAct) {
                Task nt = new Task();
                if ((sDate + qa.Days_Elapsed__c.intValue()).addMonths(3*(qrtr-1))>= (Date.today()+ 7)) {
                    // If Activity Date is more 7 days into future, create activity as normal
                    nt.ActivityDate = (sDate + qa.Days_Elapsed__c.intValue()).addMonths(3*(qrtr-1));
                }
                else if ((Date.today()+ 7)<= QtrEndDate) {
                    // If Activity Date + 7 days does not go over Qtr end date create activity with 7 days lead time
                    nt.ActivityDate = (Date.today()+ 7);
                }
                else {
                    // create activity with due date of qtr end date
                    nt.ActivityDate = QtrEndDate;
                }
                nt.Target_Completion_Date__c = nt.ActivityDate;
                nt.ReminderDateTime = datetime.newInstance((nt.ActivityDate - 14).year(),(nt.ActivityDate - 14).month(),(nt.ActivityDate - 14).day(),8,0,0);
                nt.ISREMINDERSET = TRUE;
                nt.OwnerId = UserInfo.getUserId();
                nt.Subject = 'Q'+qrtr+' '+qa.Description__c;
                nt.WhatId = contract.Id;
                nt.Task_Id__c = qa.Task_Id__c;
                //nt.Mandatory_Activity__c = qa.Mandatory_Activity__C;
                nt.Activity_Quarter__c = qa.Quarter__c;
                nt.Collateral_text__c = qa.Collateral_text__c;
                nt.Collateral_url__c = qa.Collateral_url__c;
                //nt.Promote__c = qa.Promote__c;
                nt.Details__c = qa.Details__c;
                nt.Activity_area__c = qa.Activity_area__c;
                nt.Activity_type__c = qa.Activity_type__c;
                nt.Collateral_text__c = qa.Collateral_text__c;
                nt.Collateral_url__c = qa.Collateral_url__c;
                nt.System_text__c = qa.System_text__c;
                nt.System_url__c = qa.System_url__c;
                nt.Task_Category__c = qa.Task_Category__c;
                nt.SAC_Code__c = contract.SAC_Code__c;
                //nt.Qtr_Start_Date__c = QtrStartDate;
                //nt.Qtr_End_Date__c = QtrEndDate;
                //nt.Qtr_Historic_Start_Date__c = QtrHistoricStartDate;
                newMTasks.add(nt);
            }
            tUvsMSize = newMTasks.size();
            insert newMTasks;

            //tag contract and account with date of latest task creation plus  start & end date of qtr BUT only if UVS Tasks are Created
            /**if (tUvsMSize >0 || tUvsSize > 0 ){
                //contract.UVS_Latest_Task_Date__c = Date.today();
                //contract.UVS_Qtr_End_Date__c = QtrEndDate;
                //contract.UVS_Qtr_Historic_Start_Date__c = QtrHistoricStartDate;
                //contract.account.UVS_Latest_Task_Date__c = Date.today();
                //contract.account.UVS_Qtr_End_Date__c = QtrEndDate;
                //contract.account.UVS_Qtr_Historic_Start_Date__c = QtrHistoricStartDate;

                update contract;
                update contract.account;
            }*/
        }

        // go back to original page
        PageReference pr = new PageReference('/'+contractId);
        pr.setRedirect(true);

        return pr;

        }

        return null;
    }

    public createBulkTasksContract (ApexPages.StandardController stdController) {
        contract = (Contract)stdController.getRecord();
    }

    public createBulkTasksContract () 
    {
    }
}