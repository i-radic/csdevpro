global class batchOpportunityForecast implements Database.Batchable<SObject>{
     private String query;

    global batchOpportunityForecast(String q){
        this.query = q;
    }
   global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
   global void execute(Database.BatchableContext BC, List<sObject> scope){
        system.debug('JB batchOpportunityForecast Execute method size:' + scope.size());
        List<Opportunity> opps = new List<Opportunity>();
        Set<Id> oppIds = new Set<Id>();
         Set<Id> oppIdsClosingThisWeek = new Set<Id>();
          for(Sobject c : scope){
            Opportunity opp = (Opportunity)c;
            system.debug('jb opportunity:' + opp);
            if(opp.Account.Sector_Code__c != null && opp.Account.Sector_Code__c.equalsIgnoreCase('BTLB'))
              oppIds.add(opp.Id);
            
            // copy this weeks figures to last weeks
          //opp.zLast_Week_Forecast_ACV__c = opp.zCurrent_Week_Forecast_ACV__c;
            opp.zLast_Week_Forecast_ACV__c = opp.Net_ACV__c;
          //opp.zLast_Week_Forecast_CY_NIBR__c = opp.zCurrent_Week_Forecast_CY_NIBR__c;
            opp.zLast_Week_Forecast_CY_NIBR__c = opp.SOV_GM__c;
            opp.zLast_Week_Forecast_NIBR__c = opp.zCurrent_Week_Forecast_NIBR__c;
            opp.zLast_Week_Forecast_Vol__c = opp.zCurrent_Week_Forecast_Vol__c;
            opp.Last_Week_Forecast_Amount__c = opp.Current_Week_Forecast_Amount__c;
            
            if(opp.Close_Date_Fiscal_Week__c == opp.zCurrent_Fiscal_Week__c) {
              if(opp.Account.Sector_Code__c != null && opp.Account.Sector_Code__c.equalsIgnoreCase('BTLB')){
                oppIdsClosingThisWeek.add(opp.Id);
              }
              //opp.zCurrent_Week_Forecast_ACV__c = opp.ACV_Calc__c;
                opp.zCurrent_Week_Forecast_ACV__c = opp.Net_ACV__c;             
                opp.zCurrent_Week_Forecast_NIBR__c = opp.NIBR_Next_Year__c;
                opp.zCurrent_Week_Forecast_Vol__c = 1;
              //opp.zCurrent_Week_Forecast_CY_NIBR__c = opp.NIBR_Current_Year__c;
                opp.zCurrent_Week_Forecast_CY_NIBR__c = opp.SOV_GM__c;
                opp.Current_Week_Forecast_Amount__c = opp.Amount;
                opp.ForecastUpdated__c = Date.today();
            }
            else {
              opp.zCurrent_Week_Forecast_ACV__c = 0;                       
        opp.zCurrent_Week_Forecast_NIBR__c = 0;
        opp.zCurrent_Week_Forecast_Vol__c = 0;
        opp.zCurrent_Week_Forecast_CY_NIBR__c = 0;
        opp.Current_Week_Forecast_Amount__c=0;
        opp.ForecastUpdated__c = Date.today();
            }
            
            
            
            opps.add(opp);
        } 
        update opps;
        
        Map<String, Forecasting_Portfolio_Area__c> mPortfolioForecasts = new Map <String, Forecasting_Portfolio_Area__c>();
        
        //   get existing FPA records for all opps in this batch
        for(Forecasting_Portfolio_Area__c fpa : [Select f.id, f.Previous_Week_Forecast_Revenue_SOV__c, f.Previous_Week_Forecast_Qty__c, f.Previous_Week_Forecast_ACV__c, f.Portfolio_Area__c, f.Sales_Type__c, f.Opportunity__c, f.Current_Week_Forecast_Revenue_SOV__c, f.Current_Week_Forecast_Qty__c, f.Current_Week_Forecast_ACV__c, f.Opportunity__r.Close_Date_Fiscal_Week__c, f.Opportunity__r.zCurrent_Fiscal_Week__c From Forecasting_Portfolio_Area__c f where f.Opportunity__c in :oppIds]){
           fpa.Previous_Week_Forecast_Revenue_SOV__c = fpa.Current_Week_Forecast_Revenue_SOV__c;
          fpa.Previous_Week_Forecast_Qty__c = fpa.Current_Week_Forecast_Qty__c;
          fpa.Previous_Week_Forecast_ACV__c = fpa.Current_Week_Forecast_ACV__c;
                    
           //if(fpa.Opportunity__r.Close_Date_Fiscal_Week__c != fpa.Opportunity__r.zCurrent_Fiscal_Week__c){
             fpa.Current_Week_Forecast_Revenue_SOV__c = 0;
            fpa.Current_Week_Forecast_Qty__c = 0;
            fpa.Current_Week_Forecast_ACV__c = 0;
           //}
           mPortfolioForecasts.put(fpa.Opportunity__c + fpa.Portfolio_Area__c + fpa.Sales_Type__c, fpa);
        }
        
        // Weekly Porfolio Area Forecasting 
        for(OpportunityLineItem oli : [select Portfolio_Area__c, Sales_Type__c, UnitPrice, ACV_Calc__c, Qty__c, OpportunityId, Opportunity.Close_Date_Fiscal_Week__c, Opportunity.zCurrent_Fiscal_Week__c from OpportunityLineItem where OpportunityId in :oppIdsClosingThisWeek]){
          if(!mPortfolioForecasts.containsKey(oli.OpportunityId + oli.Portfolio_Area__c + oli.Sales_Type__c)){
            Forecasting_Portfolio_Area__c fpa = new Forecasting_Portfolio_Area__c(
                Portfolio_Area__c = oli.Portfolio_Area__c,
                Sales_Type__c = oli.Sales_Type__c,
                Current_Week_Forecast_Revenue_SOV__c = oli.UnitPrice, 
                Current_Week_Forecast_Qty__c = oli.Qty__c,
                Current_Week_Forecast_ACV__c = oli.ACV_Calc__c,
                Opportunity__c = oli.OpportunityId);
            mPortfolioForecasts.put(oli.OpportunityId + oli.Portfolio_Area__c + oli.Sales_Type__c, fpa);
          }else{
            Forecasting_Portfolio_Area__c fpa = mPortfolioForecasts.get(oli.OpportunityId + oli.Portfolio_Area__c + oli.Sales_Type__c);  
             
               fpa.Current_Week_Forecast_Revenue_SOV__c += oli.UnitPrice;
              fpa.Current_Week_Forecast_Qty__c += oli.Qty__c;
              fpa.Current_Week_Forecast_ACV__c += oli.ACV_Calc__c;
          }
        }
        if(!mPortfolioForecasts.isEmpty())
          upsert mPortfolioForecasts.values() id;
        
    }
 global void finish(Database.BatchableContext BC){
    }
}