@IsTest
public class CS_UtilsTest {
	static testMethod void testMethodsInClass(){
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		
		Test.startTest();

		String fields = CS_Utils.geCustomtSobjectFields('cscfga__Product_Definition__c');
		System.assertNotEquals(fields, '');

		fields = CS_Utils.getSobjectFields('cscfga__Product_Definition__c');
		System.assertNotEquals(fields, '');

		fields = CS_Utils.getSobjectFieldsExcludeFormulas('cscfga__Product_Definition__c');
		System.assertNotEquals(fields, '');

		Account acc = CS_TestDataFactory.generateAccount(false, 'test');
		List<Account> accList = new List<Account>{acc};
		fields = CS_Utils.convertListToString(accList);
		System.assertNotEquals(fields, '');

		List<String> stringList = new List<String>{'test'};
		fields = CS_Utils.convertStringListToString(stringList);
		System.assertNotEquals(fields, '');

		Blob resource = CS_Utils.loadStaticResource('ProductConfigurationStrutureJSON');

		Test.stopTest();
	}
}