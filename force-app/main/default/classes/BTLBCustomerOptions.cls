public with sharing class BTLBCustomerOptions {
    
    @future(callout=true)   
    public static void getCustomerOptionsData(string sfid) { //singhd62
        BTLBCustomerOptions callLog = new BTLBCustomerOptions();
        
        try {
            SalesforceCRMService.BTLBEventInteraction eventRequest = callLog.crmEvent(sfid);
            SalesforceCRMService.CreateEventInteraction(eventRequest);            
        }
        catch (System.CalloutException ex){
            system.debug('Callout Exception occured:' + ex); 
        }
        catch (Exception exep) {
            system.debug('Exception:' + exep); 
        } 
    }
    
     private SalesforceCRMService.BTLBEventInteraction crmEvent(string eveId) { //singhd62
        SalesforceCRMService.BTLBEventInteraction btlbEvent = new SalesforceCRMService.BTLBEventInteraction();
        List<CustomerOptions__c> btlbCustOptionsCallLog = new List<CustomerOptions__c>();
        CustomerOptions__c interacId= new CustomerOptions__c();
        btlbCustOptionsCallLog = [select LastModifiedBy.EIN__c, LastModifiedBy.Phone, LastModifiedById, Account__r.CUG__c,Account__c, Name from CustomerOptions__c where id =: eveId];
        
        interacId.Interaction_Integration_Id__c = eveId;
        
        btlbEvent.CUG = btlbCustOptionsCallLog[0].Account__r.CUG__c;
        btlbEvent.EventId = btlbCustOptionsCallLog[0].Name;
        btlbEvent.RecordId = btlbCustOptionsCallLog[0].Name;
        btlbEvent.agentEIN = btlbCustOptionsCallLog[0].LastModifiedBy.EIN__c;
        btlbEvent.interactionIntegrationId = interacId.Interaction_Integration_Id__c ;
        return btlbEvent;
    }
    
    /* singhd62
    @future(callout=true)   
    public static void getCustomerOptionsData(string sfid) {
        BTLBCustomerOptions callLog = new BTLBCustomerOptions();
        SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();
        service.timeout_x = 60000;
        try {
            SalesforceServicesCRM.BTLBEventInteraction eventResponse = new SalesforceServicesCRM.BTLBEventInteraction();
            eventResponse = callLog.crmEvent(sfid);
            service.CreateEventInteraction('','',eventResponse);            
        }
        catch (System.CalloutException ex){
            system.debug('Callout Exception occured:' + ex); 
        }
        catch (Exception exep) {
            system.debug('Exception:' + exep);
        }
    }
    
     private SalesforceServicesCRM.BTLBEventInteraction crmEvent(string eveId) {
        SalesforceServicesCRM.BTLBEventInteraction btlbEvent = new SalesforceServicesCRM.BTLBEventInteraction();
        List<CustomerOptions__c> btlbCustOptionsCallLog = new List<CustomerOptions__c>();
        CustomerOptions__c interacId= new CustomerOptions__c();
        btlbCustOptionsCallLog = [select LastModifiedBy.EIN__c, LastModifiedBy.Phone, LastModifiedById, Account__r.CUG__c,Account__c, Name from CustomerOptions__c where id =: eveId];
        
        interacId.Interaction_Integration_Id__c = eveId;
        
        btlbEvent.CUG = btlbCustOptionsCallLog[0].Account__r.CUG__c;
        btlbEvent.EventId = btlbCustOptionsCallLog[0].Name;
        btlbEvent.RecordId = btlbCustOptionsCallLog[0].Name;
        btlbEvent.agentEIN = btlbCustOptionsCallLog[0].LastModifiedBy.EIN__c;
        btlbEvent.interactionIntegrationId = interacId.Interaction_Integration_Id__c ;
        return btlbEvent;
    }
    */
}