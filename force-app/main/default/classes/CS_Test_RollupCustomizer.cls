@isTest
private class CS_Test_RollupCustomizer
{
    private static cscfga__Product_Configuration__c config;
    private static cscfga__Product_Configuration__c subConfig;

    private static void createTestData()
    {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;
        
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;

        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;
        cscfga__Product_Definition__c prodDef =
            new cscfga__Product_Definition__c
                ( Name = 'Fixed Line'
                , cscfga__Description__c = 'Test helper' );
        insert prodDef;

        cscfga__Product_Definition__c prodDefNew =
            new cscfga__Product_Definition__c
                ( Name = 'BB Test'
                , cscfga__Description__c = 'Test helper' );
        insert prodDefNew;

        List<String> attributeName = new List<String>{'Test Attribute'};

        List<cscfga__Attribute_Definition__c> attributeDefinitions = CS_TestDataFactory.generateAttributeDefinitions(true, attributeName, prodDef);

        config = new cscfga__Product_Configuration__c
            ( cscfga__Product_Basket__c = testBasket.Id
            , cscfga__Product_Definition__c = prodDef.Id
            , cscfga__Configuration_Status__c = 'Valid'
            , cscfga__Unit_Price__c = 10
            , cscfga__Quantity__c = 1
            , cscfga__Recurrence_Frequency__c = 12 );
        insert config;

        subConfig =
            new cscfga__Product_Configuration__c
                ( cscfga__Product_Basket__c = testBasket.Id
                , cscfga__Product_Definition__c = prodDefNew.Id
                , cscfga__Configuration_Status__c = 'Valid'
                , cscfga__Unit_Price__c = 10
                , cscfga__Quantity__c = 1
                , cscfga__Recurrence_Frequency__c = 12
                , cscfga__Root_Configuration__c = config.Id
                , cscfga__Parent_Configuration__c = config.Id
                , cscfga__Attribute_Name__c = 'Test Attribute' );
        insert subConfig;

        List<cscfga__Attribute__c> attributesForPC = CS_TestDataFactory.generateAttributesForConfiguration(false, attributeDefinitions, config);
        for(cscfga__Attribute__c attribute : attributesForPC) {
            if(attribute.Name == 'Test Attribute') {
                attribute.cscfga__Value__c = subConfig.Id;
            }
        }
        insert attributesForPC;
    }

    private static testMethod void testRollupCustomizer()
    {
        createTestData();

        CS_RollupCustomiser rollup = new CS_RollupCustomiser();
        cscfga.API_1.ApiSession session = cscfga.API_1.getApiSession(config);
        rollup.beforeSave(session.getController());
    }

    private static testMethod void testRollupCustomizerSubConfig()
    {
        createTestData();

        CS_RollupCustomiser rollup = new CS_RollupCustomiser();
        cscfga.API_1.ApiSession session = cscfga.API_1.getApiSession(subConfig);
        rollup.beforeSave(session.getController());
    }
}