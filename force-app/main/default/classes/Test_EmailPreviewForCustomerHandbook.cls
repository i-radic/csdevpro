/**************************************************************************************************************************************************
Class Name : EmailPreviewForCustomerHandbook
Test Class Name : Test_EmailPreviewForCustomerHandbook
Email Template : CustomerHandbookEmailTemplate
VisualForce Component : CustomerHandbookBTEmail
Description : To Send eContact Card Email.
Version : V0.1
Created By Author Name : Praveen Kumar Chada
Date : 13/02/2018
Modified Date : 
*************************************************************************************************************************************************/

@isTest
public class Test_EmailPreviewForCustomerHandbook { 
    
    static testmethod void testMethodforContactCard1(){
         User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
     
        User Managerusr= Test_Factory.CreateUser();
        Managerusr.email = 'econtact@bt.it';      
        Managerusr.Username = 'econtact@bt.com';
        Managerusr.EIN__c = '123456987';
        Managerusr.Phone = '+44 1234 789650';
        insert Managerusr;
        User usr= Test_Factory.CreateUser();
        usr.Username = 'econtact1@bt.it';
        usr.EIN__c = '123456897';
        usr.Phone = '+44 1234 789650';
        usr.ManagerId = Managerusr.Id;
        insert usr;
        Account acc= Test_Factory.CreateAccount();
        acc.Brand__c='BT';
        acc.Product__c ='Analyst Converge';
        acc.OwnerId = usr.Id;
        insert acc;
        Contact con= Test_Factory.CreateContactwithAccount(acc.id);        
        insert con;
        
        eContactCardDetails__c CS=new eContactCardDetails__c();
        CS.Area__c='service';
        CS.Brand__c ='EE';
        CS.End_User__c ='Default';
        CS.Name='eContactCardDetails';
        insert CS;        
        eContactCardDetails__c CS1=new eContactCardDetails__c();
        CS1.Area__c='faults';
        CS1.Brand__c ='EE';
        CS1.End_User__c ='Default';
        CS1.Name='eContactCardDetails';
        insert CS1; 
        
        EmailPreviewForCustomerHandbook emailpr=new EmailPreviewForCustomerHandbook();
        emailpr.getData();
        emailpr.ContactRecId = con.Id;
        acc.Brand__c='EE';
        update acc;
        emailpr.getData();
        emailpr.ContactRecId = con.Id;
        
        CS1.Area__c='Orders';
        update CS1;
        emailpr.getData();
        emailpr.ContactRecId = con.Id;
         }
    }    
    
    static testmethod void testMethodforContactCard2(){
         User thisUser = [select id from User where id=:userinfo.getUserid()];
         System.runAs( thisUser ){
             
             TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
             settings.Account__c = FALSE;
             settings.Contact__c = FALSE;
             settings.Opportunity__c = FALSE;
             settings.OpportunitylineItem__c = FALSE;
             settings.Task__c = FALSE;
             settings.Event__c = FALSE;
             
             upsert settings TriggerDeactivating__c.Id;
         
        User usr= Test_Factory.CreateUser();
        usr.Username = 'econtact2@bt.com';
        usr.EIN__c = '123456897';
        insert usr;
        Account acc= Test_Factory.CreateAccount();
        acc.Brand__c='BT';
        //  acc.Product__c ='Analyst Converge';
        insert acc;
        Contact con= Test_Factory.CreateContactwithAccount(acc.id);        
        insert con;
        
        eContactCardDetails__c CS=new eContactCardDetails__c();
        CS.Area__c='service';
        CS.Brand__c ='EE';
        CS.End_User__c ='Default';
        CS.Name='eContactCardDetails';
        insert CS;        
        eContactCardDetails__c CS1=new eContactCardDetails__c();
        CS1.Area__c='faults';
        CS1.Brand__c ='EE';
        CS1.End_User__c ='Default';
        CS1.Name='eContactCardDetails';
        insert CS1; 
        
        EmailPreviewForCustomerHandbook emailpr=new EmailPreviewForCustomerHandbook();
        emailpr.getData();
        emailpr.ContactRecId = con.Id;
        acc.Brand__c='EE';
        update acc;
        emailpr.getData();
        emailpr.ContactRecId = con.Id;
        
        CS1.Area__c='Orders';
        update CS1;
        emailpr.getData();
        emailpr.ContactRecId = con.Id;
    } 
    }
}