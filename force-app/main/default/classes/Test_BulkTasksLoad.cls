@isTest

private class Test_BulkTasksLoad{
  
  static testMethod void runPositiveTestCases() {
        Test_Factory.SetProperty('IsTest', 'yes');

        
        User thisUser = new User (id=UserInfo.getUserId(), Apex_Trigger_BulkLoadTasks__c=true );
        update thisUser;   
        
        insert Test_Factory.CreateAccountForDummy();
               
	    Account testAcc = Test_Factory.CreateAccount();
	    Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc});    
    
      	//add test user to allocate a task to Task
      	AccountTeamMember  ATM1 = Test_Factory.CreateAccountTeamMember();
		ATM1.AccountId = accResult[0].getId();
		ATM1.userId = Userinfo.getUserId();
		ATM1.TeamMemberRole = 'TestScript';
		insert ATM1;
		
    	//create tasks
      	Task Test1 = Test_Factory.CreateTask();
	    Test1.subject = 'D1test';
	    Test1.SAC_Code__c = testAcc.sac_code__c;
	    //Test1.Owner_EIN__c = '802537216';	    
	    Test1.Owner_EIN__c = 'SFADMIN';	    
	    Test1.reminder_days__c = 4;
        insert Test1;
        
      	Task Test2= Test_Factory.CreateTask();
        Test2.subject = 'D2test';
        Test2.SAC_Code__c = testAcc.sac_code__c;
        Test2.activity_role__c = 'Bulk Tasks';
        Test2.Reminder_Days__c = 1;
        Test2.Owner_EIN__c = 'SFADMIN';
	    insert Test2;
	      
  		Task Test3= Test_Factory.CreateTask();
        Test3.subject = 'D3test';
        Test3.Owner_EIN__c = 'SFADMIN';
        Test3.SAC_Code__c = testAcc.sac_code__c;
        Test3.Task_Category__c = 'Bulk Tasks';
        insert Test3;     
     }
 }