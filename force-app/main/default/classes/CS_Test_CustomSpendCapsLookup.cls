@isTest
private class CS_Test_CustomSpendCapsLookup{
    
    private static cscfga__Product_Definition__c prodDefinition;
 
    private static Map<String, String> searchFieldsMap = new Map<String, String>();    
    private static String prodDefinitionID;
    private static Id[] excludeIds;
    private static Integer pageOffset;
    private static Integer pageLimit;
    
    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        RecordType recordType = [SELECT Id, Name from RecordType where Name = 'BT Mobile Plan'];
        
        // Service Plan 
        cspmb__Price_Item__c servicePlan = new cspmb__Price_Item__c ();
        servicePlan.Name =  'Flex';
        insert servicePlan;
        
        //AddOn PriceItem 
        cspmb__Add_On_Price_Item__c addOn = new cspmb__Add_On_Price_Item__c();
        addOn.Name = 'Sharer Subscription';
        insert addOn;
        
        // addOn Association-
        cspmb__Price_Item_Add_On_Price_Item_Association__c association = new cspmb__Price_Item_Add_On_Price_Item_Association__c ();
        association.cspmb__Add_On_Price_Item__c = addOn.Id;
        association.cspmb__Price_Item__c = servicePlan.Id;
        insert association;
        
        Spend_Cap__c sc = new Spend_Cap__c();
        sc.Name = 'test';
        sc.Commercial_Product__c = servicePlan.Id;
        insert sc;
                
        searchFieldsMap.put('Spend Cap Lookup Id', servicePlan.Id);
    }
    
     static testMethod void CustomSpendCapLookupTest() {
        createTestData();
        
        Test.StartTest();
        CS_CustomSpendCapsLookup customLookup = new CS_CustomSpendCapsLookup();
        customLookup.getRequiredAttributes();
        pageOffset = 1;
        Object[] addOnsResult = customLookup.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
        System.assertNotEquals(null, addOnsResult);     
        Test.StopTest();
     }
    
}