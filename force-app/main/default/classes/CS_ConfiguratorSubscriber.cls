global class CS_ConfiguratorSubscriber extends csutil.ASubscriber {
    public static final String PRODUCTDEF_APPLEDEP = 'Apple DEP';
    public static final Set<String> IMPLEMENTATION_FORM_DEF = new Set<String> {
            'BT Mobile Sharer', 'BT Mobile Flex', 'BTOP CORE'
    };

    public CS_ConfiguratorSubscriber() {

    }

    public override void onMessage (String topic, String subject, Map<String, Object> payload) {
        System.debug(LoggingLevel.WARN, '**** Topic: ' + topic);
        System.debug(LoggingLevel.WARN, '**** Subject: ' + subject);
        System.debug(LoggingLevel.WARN, '**** Payload: ' + JSON.serialize(payload));


        if (subject.equals('AfterSaveV2')) {
            System.debug(LoggingLevel.WARN, '***** Entered AfterSaveV2');

            if(payload.get('Container') instanceOf cscfga__Product_Basket__c) {
                Id basketId = ((cscfga__Product_Basket__c)payload.get('Container')).Id;
                cscfga__Product_Basket__c basket =  CS_ProductBasketService.getBasket(basketId);
                Set<Id> configIds = (new Map<Id, cscfga__Product_Configuration__c>((List<cscfga__Product_Configuration__c>) payload.get('AllConfigs'))).keySet();
                Map<Id, cscfga__Product_Configuration__c> allConfigs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{basket});
                system.debug('CS_ConfiguratorSubscriber.onMessage.soqls issued = ' + Limits.getQueries());

                //Bypass the code if Cloud Voice or Future Mobile
                Boolean solutionConfigs = false;
                CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
                String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;

                for(cscfga__Product_Configuration__c config : allConfigs.values()) {
                    if (config.Calculations_Product_Group__c != null && calculateProductGroup.contains(config.Calculations_Product_Group__c)) {
                        solutionConfigs = true;
                        break;
                    }
                }

                if (solutionConfigs) {
                    CS_ProductBasketService.desyncBasket(new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket});
                    //update basket;
                    return;
                }
               
                CS_ProductBasketService.setSpecialConditions(allConfigs, basket);
                CS_ProductBasketService.desyncBasket(new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket});
                updateProductBasket(allConfigs, basket);
                updateProductConfigsInBasket(basket, allConfigs);
                updateCVRevenue(allConfigs, basket);
                system.debug(basket);
                update basket;

                if(!configIds.isEmpty())
                    createPCTotals(getCurrentConfigs(allConfigs, configIds));
                executeActions(allConfigs);
            }
        }

        if (subject.equals('BeforeSaveV2')) {
            System.debug(LoggingLevel.WARN, '***** Entered BeforeSaveV2');

            if(payload.get('Container') instanceOf cscfga__Product_Basket__c) {
                Id basketId = ((cscfga__Product_Basket__c)payload.get('Container')).Id;
                Map<Id, cscfga__Product_Definition__c> definitions = getProductDefinitions((List<cscfga__Product_Configuration__c>) payload.get('AllConfigs'));

                updateProductDefinitionName(payload, definitions);

                Boolean solutionDefinitions = false;
                CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
                String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;
                for (cscfga__Product_Definition__c def : definitions.values()) {
                    if (def.cscfga__Product_Category__r.Name != null && calculateProductGroup.contains(def.cscfga__Product_Category__r.Name)) {
                        solutionDefinitions = true;
                        break;
                    }
                }

                if (solutionDefinitions) {
                    return;
                }

                Map<String, Map<String, cscfga__Attribute__c>> attributesByConfig = getAttributes(payload);

                updateCalculationProductGroup(payload, definitions);
                updateUsageProfileCalcs(payload, definitions);
                updateConfigs(payload);
                getConfiguratorActions(attributesByConfig, payload, definitions);
                System.debug('Finished here*******************************');
            }
        }

        if (subject.equals('Deleted')) {
            System.debug(LoggingLevel.WARN, '***** Entered Deleted');

            List<cscfga__Product_Configuration__c> rootConfigs = new List<cscfga__Product_Configuration__c>();
            List<cscfga__Product_Configuration__c> configs = (List<cscfga__Product_Configuration__c>) payload.get('Configs');
            System.debug(LoggingLevel.WARN, configs);

            Id basketId;
            boolean foundPromo = false;//mpk CR12500
            for(cscfga__Product_Configuration__c cfg : configs) {
                if(cfg.cscfga__Root_Configuration__c == null) {
                    rootConfigs.add(cfg);
                    basketId = cfg.cscfga__Product_Basket__c;
                    if(cfg.IsPromoOffer__c)      //mpk CR12500
                        foundPromo = true;
                }
            }

                
            if(!rootConfigs.isEmpty() && basketId != null) {
                cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
                
                system.debug('mpk3 - '+ basket.PromoOffer__c +' foundPromo='+foundPromo);
                if(foundPromo && basket.PromoOffer__c) //mpk CR12500
                    basket.PromoOffer__c=false;

                Map<Id, cscfga__Product_Configuration__c> allConfigs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{basket});
                CS_ProductBasketService.calculateBasketTotals(allConfigs, basket);
                CS_ProductBasketService.setSpecialConditions(allConfigs, basket);
                CS_ProductBasketService.desyncBasket(new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket});
                updateProductBasket(allConfigs, basket);
                Map<Id, cscfga__Product_Configuration__c> configsToUpdate = CS_ProductConfigurationService.updateCreditFunds(new Map<Id, cscfga__Product_Basket__c>{basket.Id => basket});
                update basket;

                if(!configsToUpdate.isEmpty()) {
                    update configsToUpdate.values();
                }
            }
        }

        if(subject.equals('Cloned')) {

        }
    }

    @TestVisible
    private Map<Id, cscfga__Product_Definition__c> getProductDefinitions(List<cscfga__Product_Configuration__c> configs) {
        Set<Id> definitionIds = new Set<Id>();
        for(cscfga__Product_Configuration__c cfg : configs) {
            definitionIds.add(cfg.cscfga__Product_Definition__c);
        }

        return new Map<Id, cscfga__Product_Definition__c>([
                SELECT Id, Name, cscfga__Product_Category__r.Name
                FROM cscfga__Product_Definition__c
                WHERE ID IN :definitionIds
        ]);
    }

    @TestVisible
    private Map<Id, cscfga__Product_Configuration__c> getOldProductConfigurations(Map<Id, cscfga__Product_Configuration__c> configs) {
        return new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Customer_Apple_Id__c
        FROM cscfga__Product_Configuration__c
        WHERE Id IN :configs.keySet()]);
    }

    @TestVisible
    private void updateProductConfigsInBasket(cscfga__Product_Basket__c basket, Map<Id, cscfga__Product_Configuration__c> allConfigs) {
        String productConfigsInBasket = '';
        boolean Unlimitedprod =false;                             
        for(cscfga__Product_Configuration__c pc : allConfigs.values()){
            productConfigsInBasket = productConfigsInBasket + pc.Id + '|' + pc.Volume__c + ',';
            if(pc.Name=='5G Unlimited Data Plans_24M' || pc.Name=='5G Unlimited Data Plans_36M')
               Unlimitedprod = true;                                                                                                                
        }
        productConfigsInBasket = productConfigsInBasket.removeEnd(',');
        basket.Product_Configs_In_Basket__c = productConfigsInBasket;
        basket.is_Unlimited_basket__c = Unlimitedprod;                                            
        
    }

    @TestVisible
    private void updateProductBasket(Map<Id, cscfga__Product_Configuration__c> allConfigs, cscfga__Product_Basket__c productBasket) {
        productBasket.Implementation_Form_Valid__c = true;
        List<String> basePropositions = new List<String>();
        for(cscfga__Product_Configuration__c cfg : allConfigs.values()) {
            if(String.isNotBlank(cfg.Proposition_Name__c)) {
                basePropositions.add(cfg.Proposition_Name__c);
            }

            if(IMPLEMENTATION_FORM_DEF.contains(cfg.Product_Definition_Name__c)) {
                productBasket.Implementation_Form_Valid__c = false;
            }
        }

        if(basePropositions.isEmpty()) {
            productBasket.Base_Proposition_Name__c = '';
        }
        else {
            productBasket.Base_Proposition_Name__c = String.join(basePropositions, ',');
        }
    }

    @TestVisible
    private void createPCTotals(Map<Id, cscfga__Product_Configuration__c> allConfigsMap) {
        if(!allConfigsMap.isEmpty()) {
            CS_ProductConfigurationService.createProductConfigurationTotals(allConfigsMap);
        }
    }

    @TestVisible
    private void updateCalculationProductGroup(Map<String, Object> payload, Map<Id, cscfga__Product_Definition__c> definitions) {
        for(cscfga__Product_Configuration__c pc : (List<cscfga__Product_Configuration__c>) payload.get('AllConfigs')) {

            if(String.isBlank(pc.Calculations_Product_Group__c)) {
                if(definitions.get(pc.cscfga__Product_Definition__c).cscfga__Product_Category__r.Name == 'EE SME' ||
                        definitions.get(pc.cscfga__Product_Definition__c).cscfga__Product_Category__r.Name == 'EE Corp' ||
                        definitions.get(pc.cscfga__Product_Definition__c).cscfga__Product_Category__r.Name == 'Cloud Voice') {
                    pc.Calculations_Product_Group__c = definitions.get(pc.cscfga__Product_Definition__c).cscfga__Product_Category__r.Name;
                    if(definitions.get(pc.cscfga__Product_Definition__c).cscfga__Product_Category__r.Name == 'Cloud Voice'){
                        pc.Deal_Type__c ='New Connection';
                    }
                }
                if(definitions.get(pc.cscfga__Product_Definition__c).Name.contains('Cloud Work') ||
                        definitions.get(pc.cscfga__Product_Definition__c).Name.contains('Contact Centre')){
                    pc.Calculations_Product_Group__c = 'Cloud Work';
                }
            }
        }
    }
    
    @TestVisible
    private void updateCVRevenue(Map<Id, cscfga__Product_Configuration__c> allConfigs, cscfga__Product_Basket__c productBasket){
        cscfga__Product_Configuration__c cvpc = new cscfga__Product_Configuration__c();
        Decimal Commission =0.00;
        Decimal CVRecRevenue = 0.00;
        Decimal CVOneOffRevenue = 0.00;
        Decimal ContractTerm =0.00;
        for(cscfga__Product_Configuration__c pc : allConfigs.values()) {
            if(pc.Calculations_Product_Group__c == 'Cloud Voice') {
                CVRecRevenue = CVRecRevenue + pc.PC_Total_Recurring_Charge__c ;
                CVOneOffRevenue = CVOneOffRevenue + pc.PC_Total_One_Off_Charge__c;
            }
            if(pc.Cloud_Voice_Commission__c!=null){
                Commission = pc.Cloud_Voice_Commission__c;
                ContractTerm = pc.cscfga__Contract_Term__c;
            }
        }
        productBasket.BT_Deal_Level_Commission__c =(Commission*(CVRecRevenue*ContractTerm +CVOneOffRevenue))/100;
    }
    @TestVisible
    private void updateUsageProfileCalcs(Map<String, Object> payload, Map<Id, cscfga__Product_Definition__c> definitions) {
        List<cscfga__Attribute__c> attributes = (List<cscfga__Attribute__c>) payload.get('AllAttrs');
        List<cscfga__Product_Configuration__c> configs = (List<cscfga__Product_Configuration__c>) payload.get('AllConfigs');
        Map<Id, cscfga__Attribute_Definition__c> attDefs = (Map<Id, cscfga__Attribute_Definition__c>) payload.get('AttributeDefinitionsMap');

        CS_ProductConfigurationService.updateUsageProfileCalcs(attributes, configs, definitions, attDefs);
    }

    @TestVisible
    private void updateConfigs(Map<String, Object> payload) {
        List<cscfga__Product_Configuration__c> configs = (List<cscfga__Product_Configuration__c>) payload.get('AllConfigs');

        for(cscfga__Product_Configuration__c cfg : configs) {
            cfg.Name = cfg.Name.length() > 80 ? cfg.Name.substring(0, 80) : cfg.Name;
        }
    }

    @TestVisible
    private void updateProductDefinitionName(Map<String, Object> payload, Map<Id, cscfga__Product_Definition__c> definitions) {
        cscfga__Product_Configuration__c rootProductConfiguration = (cscfga__Product_Configuration__c) payload.get('RootConfig');
        rootProductConfiguration.Product_Definitions__c = '';

        Set<String> definitionNamesSet = new Set<String>();
        for(cscfga__Product_Configuration__c pc : (List<cscfga__Product_Configuration__c>) payload.get('AllConfigs')) {
            if(pc.Id != rootProductConfiguration.Id) {
                definitionNamesSet.add(definitions.get(pc.cscfga__Product_Definition__c).Name);
            }
        }

        for (String str : definitionNamesSet) {
            rootProductConfiguration.Product_Definitions__c += str + ',';
        }

        rootProductConfiguration.Product_Definitions__c = rootProductConfiguration.Product_Definitions__c.removeEndIgnoreCase(',');
    }

    @TestVisible
    private Map<Id, cscfga__Product_Configuration__c> getCurrentConfigs(Map<Id, cscfga__Product_Configuration__c> allConfigs, Set<Id> configIds) {
        Map<Id, cscfga__Product_Configuration__c> currentConfigs = new Map<Id, cscfga__Product_Configuration__c>();

        for(Id cfgId : configIds) {
            currentConfigs.put(cfgId, allConfigs.get(cfgId));
        }

        return currentConfigs;
    }

    @TestVisible
    private Map<String, Map<String, cscfga__Attribute__c>> getAttributes(Map<String, Object> payload) {
        Map<String, Map<String, cscfga__Attribute__c>> attributesByConfig = new Map<String, Map<String, cscfga__Attribute__c>>();
        List<cscfga__Attribute__c> attributes = (List<cscfga__Attribute__c>) payload.get('AllAttrs');
        for(cscfga__Attribute__c att : attributes) {
            if(!attributesByConfig.containsKey(att.cscfga__Product_Configuration__r.cscfga__Key__c)) {
                attributesByConfig.put(att.cscfga__Product_Configuration__r.cscfga__Key__c, new Map<String, cscfga__Attribute__c>());
            }

            attributesByConfig.get(att.cscfga__Product_Configuration__r.cscfga__Key__c).put(att.Name, att);
        }

        return attributesByConfig;
    }

    @TestVisible
    private void getConfiguratorActions(Map<String, Map<String, cscfga__Attribute__c>> attributes, Map<String, Object> payload, Map<Id, cscfga__Product_Definition__c> definitions) {
        CS_ConfiguratorActions.getActions(attributes, (List<cscfga__Product_Configuration__c>) payload.get('AllConfigs'), definitions, 'change');
    }

    @TestVisible
    private void executeActions(Map<Id, cscfga__Product_Configuration__c> allConfigs) {
        CS_ConfiguratorActions.executeActions(allConfigs);
    }
}