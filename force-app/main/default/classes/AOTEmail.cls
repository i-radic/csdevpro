public with sharing class AOTEmail{

    public Opportunity oppty {get; set;}
    public List<contact> contactID = new List<Contact>();
    public string oppURL { get; set; }
      
    public AOTEmail(ApexPages.StandardController stdController) {

        oppty = (Opportunity)stdController.getRecord();
    }
    
    public PageReference actionDo(){
        if(Test_Factory.GetProperty('IsTest') == 'yes'){
            oppURL = '/test';   
        } else {
            oppty = [Select id, Opportunity_Id__c, Account_CUG__c, AccountId From Opportunity where Id =: oppty.Id LIMIT 1];
            contactID = [Select Id,Email From Contact where AccountId =: oppty.AccountId and Target_Contact__c =: true];
            if(contactID.Size() > 0) {
                    oppURL = '/email/author/emailauthor.jsp?retURL=/' + oppty.Id + '&p2_lkid='+contactId[0].Id+'&template_id=00X2000000155NJ';
            } else {
                oppURL = '/email/author/emailauthor.jsp?retURL=/' + oppty.Id + '&template_id=00X2000000155NJ';
            }
        }
        PageReference PageRef = new PageReference(oppURL);
        PageRef.setRedirect(true);    
        return PageRef;
    } 
  }