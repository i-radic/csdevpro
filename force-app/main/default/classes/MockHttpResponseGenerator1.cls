@isTest
global class MockHttpResponseGenerator1 implements HttpCalloutMock {

        // Implement this interface method   
     global HTTPResponse respond(HTTPRequest req) {
        system.debug('rrrrrrrr'+req);
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><customerDetailsResponse xmlns="http://capabilities.nat.bt.com/xsd/crm/v10/Customer/Customer"><customerAccount><cak>CUG5600016529</cak><creditScore></creditScore><name></name><password></password><primaryContactId>820ZP09367</primaryContactId><Id>Test Id</Id><title>Test Id</title><firstName>Test</firstName><lastName>Test</lastName><jobTitle>Test</jobTitle><primaryRole>Test</primaryRole><telephoneNumber>Test</telephoneNumber><contactIntegrationId>Test</contactIntegrationId><type>Customer Group</type><customerClass>B</customerClass><accountNumber>5600016529</accountNumber><serviceStartDate>2012-07-12T15:55:27.000+01:00</serviceStartDate><accountStatus>Active</accountStatus><btCRFlag></btCRFlag><customerSummary></customerSummary><followupRating></followupRating><tos></tos><ocb></ocb><icb></icb><pwdflag>N</pwdflag><sector>Enterprise</sector><subSector>Enterprise Unsegmented</subSector><lineOfBusiness>Unsegmented</lineOfBusiness><customerGroupType></customerGroupType><sacCode>NLSBS</sacCode><wlr3Flag>N</wlr3Flag><strategicJourney>N</strategicJourney><organisationName>BT Volume</organisationName><sacName>BT BUS UNSEGMENTED AWAITING ALLOCATION</sacName><accountManagerName></accountManagerName><cpDunsId> </cpDunsId><organizationDunsId> </organizationDunsId><CMIG>N</CMIG><createdBy>920191222</createdBy><createdOn>2012-07-12T15:55:27.000+01:00</createdOn><lastModifiedBy>CMPS</lastModifiedBy><lastModifiedOn>2012-07-12T15:55:36.000+01:00</lastModifiedOn><accountManagerEIN></accountManagerEIN><transitionFlag></transitionFlag><cpDomainName></cpDomainName><movementStatus></movementStatus></customerAccount></customerDetailsResponse>');
         res.setStatusCode(200);
        return res; 
    }
        
    
    }