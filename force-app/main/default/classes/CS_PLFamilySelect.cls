public class CS_PLFamilySelect{

    public Id prodBasketId { get; set; }
    public cscfga__Product_Basket__c prodBasket { get; set; }
    public String allFamily {get; set; }
    Set<String> families = new Set<String>();
    
    public CS_PLFamilySelect(ApexPages.StandardController controller) {
        prodBasketId = ApexPages.currentPage().getParameters().get('basketId');
        
        if (prodBasketId == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Product Basket not selected.'));
        } else {
            prodBasket = getProductBasket(prodBasketId);
            getFamily(prodBasketId);
        }
    }
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Total', 'Total'));
        for (String str: families) {
            options.add(new SelectOption(str, str));
        }
        return options;
    }
    
    public void getFamily(Id pbId) {
        List<cscfga__Product_Configuration__c> pcList = [SELECT cscfga__Product_Family__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c = :pbId];
        for (cscfga__Product_Configuration__c pc : pcList) {
            if (pc.cscfga__Product_Family__c != null && pc.cscfga__Product_Family__c != '') {
                families.add(pc.cscfga__Product_Family__c);
            }
        }
    }
    
    private cscfga__Product_Basket__c getProductBasket(Id productBasketId) {
        List<cscfga__Product_Basket__c> baskets = [
            SELECT Id, Name, PL_Family_Group__c
            FROM cscfga__Product_Basket__c
            WHERE Id = :productBasketId
        ];

        if (baskets.size() == 1)
            return baskets[0];

        return null;
    }
    
    public PageReference saveAndRedirect() {
        prodBasket.PL_Family_Group__c = allFamily;
        update prodBasket;
        
        PageReference retPageRef = new PageReference('/apex/cspl__PNLEditor?basketId=' + prodBasketId + '&familyName=' + allFamily);
        return retPageRef;
    }
}