public with sharing class MailUtil {
    
    public static void sendMail(string message, string recipients, string subject, string displayName){
        
        Messaging.Singleemailmessage mail = new Messaging.Singleemailmessage();
        String[] toAddresses = new String[] {recipients};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setSenderDisplayName(displayName);
        
        mail.setUseSignature(false);
        mail.setHtmlBody(message);
        
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
    }


    public static testMethod void testSendMail(){
        sendMail('Test email message','rita.opokuserebuoh@bt.com','email test subject','Rita Opoku-Serebuoh');
    }
}