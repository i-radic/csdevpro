global with sharing class CS_OrderSpecification {
    public CS_OrderSpecification(String name, List<CS_SubscriptionSpecification> subscriptionSpecificationList, String ordId) {
        this.subscriptionName = name;
        this.subscriptionSpecificationList = subscriptionSpecificationList;
        this.orderId = ordId;
    }

    public String subscriptionName;
    public List<CS_SubscriptionSpecification> subscriptionSpecificationList;
    public String orderId;
}