@isTest
private class tgrADPAfter_Test {
 
    static testMethod void firstTest() {
        Test_Factory.SetProperty('IsTest', 'yes');
        User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     	
        
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@bt.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;   
        
        Account testAcc2 = Test_Factory.CreateAccount();
        testAcc2.name = 'TESTCODE 2';
        testAcc2.sac_code__c = 'testSAC';
        testAcc2.Sector_code__c = 'CORP';
        testAcc2.LOB_Code__c = 'LOB';
        testAcc2.OwnerId = u1.Id;
        Database.SaveResult[] accResult2 = Database.insert(new Account[] {testAcc2});
         
        ADP__c thisADP = new ADP__c(Customer__c = accResult2[0].Id);            
        Database.SaveResult[] adpRes = Database.insert(new ADP__c[] {thisADP});
        
        ADP_Landscape__c thisADPLS = new ADP_Landscape__c(ADP__c = adpRes[0].Id, RecordTypeID = '01220000000PirH');            
        Database.SaveResult[] adpLSRes = Database.insert(new ADP_Landscape__c[] {thisADPLS });
                
        
        PageReference pageRef = Page.ADPOppertunities;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', thisADP.Id);                      
        }        
    }
}