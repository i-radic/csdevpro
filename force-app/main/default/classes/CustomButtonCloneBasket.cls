global with sharing class CustomButtonCloneBasket extends csbb.CustomButtonExt {
	public String performAction (String basketId) {
    	String newUrl = '/apex/c__CS_CloneBasket?id='+basketId;
        
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
	}
}