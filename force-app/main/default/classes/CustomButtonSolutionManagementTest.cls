/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 04/11/2020
   @author Maahaboob Basha

   @description Test class for CustomButtonSolutionManagement.

   @modifications
   
*/
@isTest(SeeAllData=FALSE)
private class CustomButtonSolutionManagementTest {

    /************************************************************************************************************
    * Method Name : performActionPositiveTest
    * Description : Used to simulate and test the logic of performAction method in CustomButtonSolutionManagement 
    * Parameters  : NA
    * Return      : NA                      
    ************************************************************************************************************/
    static testmethod void performActionPositiveTest() {
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'Test Product Definition');
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(TRUE, 'Test Prod Config', basket);
        csbb__Product_Configuration_Request__c prodConfigRequest = CS_TestDataFactory.generateProductConfigRequest(TRUE, prodCategory.Id, prodConfig.Id);
        String pcrIds = '["' + prodConfigRequest.Id + '"]';
        String redirectURL = cssmgnt.API_1.getSolutionManagementURL() + '?basketId=' + basket.Id;
        Test.startTest();
            CustomButtonSolutionManagement cbSolMgmt = new CustomButtonSolutionManagement();
            result = cbSolMgmt.performAction(basket.Id, pcrIds);
        Test.stopTest();
        System.assertNotEquals(NULL, result);
        System.assertEquals('{"status":"ok", "title":"Nothing selected", "redirectURL":" ' + redirectURL + '" }', result);
    }
    
    /************************************************************************************************************
    * Method Name : performActionPositiveTest2
    * Description : Used to simulate and test the logic of performAction method in CustomButtonSolutionManagement 
    * Parameters  : NA
    * Return      : NA                      
    ************************************************************************************************************/
    static testmethod void performActionPositiveTest2() {
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'Test Product Definition');
        csord__Solution__c sol = new csord__Solution__c(Name = 'Test Solution', csord__Account__c = acc.Id, cssdm__product_basket__c = basket.Id, csord__Identification__c = '1111');
        INSERT sol;
        cscfga__Product_Configuration__c prodConfig = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config', basket);
        prodConfig.cssdm__solution_association__c = sol.Id;
        INSERT prodConfig;
        csbb__Product_Configuration_Request__c prodConfigRequest = CS_TestDataFactory.generateProductConfigRequest(TRUE, prodCategory.Id, prodConfig.Id);
        String pcrIds = '["' + prodConfigRequest.Id + '"]';
        String redirectURL = cssmgnt.API_1.getSolutionManagementURL() + '?basketId=' + basket.Id;
        Test.startTest();
            CustomButtonSolutionManagement cbSolMgmt = new CustomButtonSolutionManagement();
            result = cbSolMgmt.performAction(basket.Id, pcrIds);
        Test.stopTest();
        System.assertNotEquals(NULL, result);
        System.assertEquals('{"status":"ok", "redirectURL":" ' + redirectURL + '&selectedSolution=' + sol.Id + '"}', result);
    }
    
    /************************************************************************************************************
    * Method Name : performActionPositiveTest3
    * Description : Used to simulate and test the logic of performAction method in CustomButtonSolutionManagement 
    * Parameters  : NA
    * Return      : NA                      
    ************************************************************************************************************/
    static testmethod void performActionPositiveTest3() {
        String result;
        TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
        INSERT triggerSetting;
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        Account acc = CS_TestDataFactory.generateAccount(TRUE, 'Test Account');
        Opportunity opp = CS_TestDataFactory.generateOpportunity(TRUE, 'Test Opportunity', acc);
        cscfga__Product_Basket__c basket = CS_TestDataFactory.generateProductBasket(TRUE, 'Test Basket', opp);
        cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(TRUE, 'Test Product Category');
        cscfga__Product_Definition__c prodDefinition = CS_TestDataFactory.generateProductDefinition(TRUE, 'Test Product Definition');
        csord__Solution__c sol1 = new csord__Solution__c(Name = 'Test Solution1', csord__Account__c = acc.Id, cssdm__product_basket__c = basket.Id, csord__Identification__c = '1111');
        csord__Solution__c sol2 = new csord__Solution__c(Name = 'Test Solution2', csord__Account__c = acc.Id, cssdm__product_basket__c = basket.Id, csord__Identification__c = '2222');
        INSERT new List<csord__Solution__c>{sol1, sol2};
        cscfga__Product_Configuration__c prodConfig1 = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config1', basket);
        prodConfig1.cssdm__solution_association__c = sol1.Id;
        cscfga__Product_Configuration__c prodConfig2 = CS_TestDataFactory.generateProductConfiguration(FALSE, 'Test Prod Config2', basket);
        prodConfig2.cssdm__solution_association__c = sol2.Id;
        INSERT new List<cscfga__Product_Configuration__c>{prodConfig1, prodConfig2};
        csbb__Product_Configuration_Request__c prodConfigRequest1 = CS_TestDataFactory.generateProductConfigRequest(TRUE, prodCategory.Id, prodConfig1.Id);
        csbb__Product_Configuration_Request__c prodConfigRequest2 = CS_TestDataFactory.generateProductConfigRequest(TRUE, prodCategory.Id, prodConfig2.Id);
        String pcrIds = '["' + prodConfigRequest1.Id + '","' + prodConfigRequest2.Id + '"]';
        String redirectURL = cssmgnt.API_1.getSolutionManagementURL() + '?basketId=' + basket.Id;
        Test.startTest();
            CustomButtonSolutionManagement cbSolMgmt = new CustomButtonSolutionManagement();
            result = cbSolMgmt.performAction(basket.Id, pcrIds);
        Test.stopTest();
        System.assertNotEquals(NULL, result);
        System.assertEquals('{"status":"error","title":"More than one network is selected","text":""}', result);
    }
}