public class BTNorthernIrelandService {

//public string ServiceSite {get;set;}
//public string OriginatorEIN {get;set;}
public static Opportunity  Opty{get;set;}


public BTNorthernIrelandService(ApexPages.StandardController controller) {

    /*
Cookie Sitecookie = ApexPages.currentPage().getCookies().get('SiteName');
Cookie EINcookie = ApexPages.currentPage().getCookies().get('EIN');

if (Sitecookie != null) {
ServiceSite  = Sitecookie.getValue();
}

if (EINCookie != null) {
OriginatorEIN  = EINCookie.getValue();
}*/
}

@RemoteAction
public static Map<String,String> createOpportunity(Map<String,String> OptyMap ){
//system.debug('Input Data'+OptyMap);
Map<String,String> response=new Map<String,String>();

try
{      
//system.debug('****************entered'+OptyMap.get('Name')+'------------------');    
//Opty.RecordTypeId='01220000000AIB4AAO';//BTNI : Standard   and     BTNI BRT

Opty=new Opportunity();
Opty.Lead_Indicator__c='LFS Lead';
Opty.RecordTypeId='01220000000AIB4AAO';
Opty.Name = OptyMap.get('Name');
Opty.Lead_Source__c='BTNI BRT';
Opty.CloseDate= Date.Today();
Opty.StageName='Created';
Opty.Originator_Ein__c=OptyMap.get('Originator_EIN__c');
Opty.Originator_Email__c=OptyMap.get('Originator_Email__c');
Opty.Postcode__c=OptyMap.get('Postcode__c');
Opty.Product__c=OptyMap.get('Product__c');
Opty.Sales_Agent_EIN__c=OptyMap.get('Sales_Agent_EIN__c');
Opty.Sub_Category__c=OptyMap.get('Sub_Category__c');
Opty.Lead_Information__c=OptyMap.get('Lead_Information__c');
Opty.LFS_CUG__c=OptyMap.get('LFS_CUG__c');
Opty.LFS_OUC__c=OptyMap.get('LFS_OUC__c');
Opty.Customer_Contact_Salutation__c=OptyMap.get('Customer_Contact_Salutation__c');

//List<RecordType> RType =[select id from RecordType where DeveloperName = 'BTNI_Standard' AND SobjectType = 'Opportunity'];
//system.debug('**********'+RType );

//if(RType.Size() > 0){
//Opty.RecordTypeId = RType[0].id;
//}

if(Opty.id == null){
insert Opty;
}

List<Opportunity> Op = [select Name,Opportunity_Id__c,Originator_EIN__c,Service_site__c from Opportunity Where Id=:Opty.id];


if(op.size() >0){

Opty=Op[0];
response.put('message','Record Insert successfully -- ' + Op[0].id);
response.put('Opportunity_ID',Op[0].Opportunity_Id__c);
response.put('Opportunity_Name',Op[0].Name);
response.put('errorCode','200');
}   
else{
response.put('message', 'Opportunity is not created Please copy this id '+Opty.id+'for Support and Concerns');
response.put('errorCode','500');
}
}catch(Exception ex)
{
//system.debug('****************'+ex.getMessage()+'------------------');
response.put('message',ex.getMessage());
response.put('errorCode','500');
}
return response;
}

/*public Pagereference CreateNewOpp(){
setCookie();

//pagereference Pg= new Pagereference('/apex/LeadsFromService?SiteName='+Opty.Service_Site__c+'&EIN='+Opty.Originator_EIN__c);
pagereference Pg= new Pagereference ('/apex/LeadsFromService');
pg.setredirect(true);
return pg;
}


public void setCookie() {
Cookie SiteCookie= new Cookie('SiteName',ServiceSite , null, 315569260, false); //Here 315569260 represents cookie expiry date = 10 years. You can set this to what ever expiry date you want. Read apex docs for more details.
Cookie EINCookie = new Cookie('EIN',OriginatorEIN , null, 315569260, false); //Here 315569260 represents cookie expiry date = 10 years. You can set this to what ever expiry date you want. Read apex docs for more details.
system.debug('*************** '+EINCookie);
ApexPages.currentPage().setCookies(new Cookie[] {SiteCookie,EINCookie});
}*/
}