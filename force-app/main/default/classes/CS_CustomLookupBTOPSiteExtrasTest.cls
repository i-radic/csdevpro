@IsTest
public class CS_CustomLookupBTOPSiteExtrasTest  {
	private static Map<String, String> searchFieldsMap = new Map<String, String>();    
    private static String prodDefinitionID;
    private static Id[] excludeIds;
    private static Integer pageOffset;
    private static Integer pageLimit;

    private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
    	pageOffset = 1;
        
        Account testAcc = new Account
            ( Name = 'Test Account'
            , NumberOfEmployees = 1 );
        insert testAcc;

        Opportunity testOpp = new Opportunity
            ( Name = 'Online Order'
              , AccountId = testAcc.Id
              , CloseDate = System.today()
              , StageName = 'Closed Won'
              , TotalOpportunityQuantity = 0 );
        insert testOpp;
        
        OLI_Sync__c os = new OLI_Sync__c();
        os.Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c';
        insert os;

        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c
            ( Name = 'Test Order ' + system.now().format('yyyy-MM-dd HH:mm:ss')
            , cscfga__Opportunity__c = testOpp.Id );
        insert testBasket;

        BT_Extra_Product_Type__c productType = new BT_Extra_Product_Type__c(
        	Name = 'Test Product Type',
        	Product_Type__c = 'Site Extras'
        );
        insert productType;
        
        cspmb__Price_Item__c pi1 = new cspmb__Price_Item__c(
            Name = 'Test',
            Product_Type__c = productType.id
        );
        insert pi1;
        
        searchFieldsMap.put('Product', productType.Id);
        searchFieldsMap.put('Exclude extra from picklist', 't,test');
    }

    private static testMethod void searchCompanyExtras() {
	    
	    createTestData();
	    Test.StartTest();
	    CS_CustomLookupBTOPSiteExtras lkp = new CS_CustomLookupBTOPSiteExtras();
	    lkp.getRequiredAttributes();
	    Object[] result = lkp.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}

	private static testMethod void searchCompanyExtrasWithSearchTerm() {
	    searchFieldsMap.put('searchValue', 'searchValue');

	    createTestData();
	    Test.StartTest();
	    CS_CustomLookupBTOPSiteExtras lkp = new CS_CustomLookupBTOPSiteExtras();
	    Object[] result = lkp.doLookupSearch(searchFieldsMap, prodDefinitionID, excludeIds, pageOffset, pageLimit);
	    Test.StopTest();
	    
	    System.assertNotEquals(null, result);

	}
}