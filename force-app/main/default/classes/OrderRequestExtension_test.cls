/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest (seeAllData = true)
private class OrderRequestExtension_test {

	private static Account mAccount;
	private static Order_Request__c mOrderRequest;
	private static Order mOrder;
	
	private static OrderItem mOrderItem;
	
	private static Map<String, Id> orderRequestRecordTypeMap;

//	private final string CONST_MODE_QUERYSTRING				= 'mode';
//	private final string CONST_MODE_SEND_ORDER_REQUEST 		= 'sendrequest';
//	private final string CONST_MODE_CANCEL_ORDER_REQUEST 	= 'cancelrequest';

	
	static{
		
		// Create Custom Settings
		EE_OrderTestClass.createCustomSettings();

		// Store the record types for the order requests
        if (orderRequestRecordTypeMap == null) {
            orderRequestRecordTypeMap = new Map<String, Id>(); 
            for( RecordType rct : [SELECT Id, Name, DeveloperName
                                FROM RecordType 
                                WHERE SobjectType='Order_Request__c']){
        
                orderRequestRecordTypeMap.put(rct.Name, rct.Id);
            }
        }

		// Just create a single test account. false = no incremental count for the Company Name
		mAccount = EE_TestClass.createStaticAccount('Test Company', false);
		insert mAccount;
				
		// Create Order record
        mOrder = new Order();
		mOrder.AccountId 		= mAccount.Id;
		mOrder.EffectiveDate	= system.today().addYears(1);
		mOrder.Status			= 'Draft';
		mOrder.Type 			= 'Included Services';	
		mOrder.Pricebook2Id		= EE_OrderTestClass.getPricebookId('Services Price Book');
		insert mOrder;		
		
		// Create our Order Request record
/*		
        mOrderRequest = new Order_Request__c();
		mOrderRequest.Name				= 'Test';//oi.PriceBookEntry.Product2.Name + ' ' + date.valueOf(oi.Order.EffectiveDate);
    	mOrderRequest.RecordTypeId 		= orderRequestRecordTypeMap.get('Welcome Day');
    	mOrderRequest.Order__c 			= mOrder.Id;
    	mOrderRequest.Status__c			= 'Draft';
		insert mOrderRequest;
*/		
		// Create our Order Item record
		mOrderItem = new OrderItem();

		//OrderItem lOrderItem = new OrderItem();
		mOrderItem.OrderId 			= mOrder.Id;
		mOrderItem.PricebookEntryId = EE_OrderTestClass.getPricebookEntryId('Welcome Day', 'Services Price Book');
		mOrderItem.Quantity 		= 1;
		mOrderItem.UnitPrice		= 0;
		//lListOrderItems.add(lOrderItem2);
		insert mOrderItem;

        // Get the autocreated OrderRequest record
        mOrderRequest = [ SELECT Id, Name, Order__c, Order_Quantity__c, Status__c FROM Order_Request__c WHERE System_Order_Item__c = :mOrderItem.Id LIMIT 1];
        System.AssertEquals( 1, mOrderRequest.Order_Quantity__c );
		
		
		//EE_OrderTestClass.getPricebookEntryId(psProductName, psPricebook)
		
	}
	

   /*
    * @name         testOrderRequestExtensionMissingParamMode
    * @description	Ensure an error is reported when no mode is set
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    static testmethod void testOrderRequestExtensionMissingParamMode() {   	

        // Load the Page using the Order Request Id      
        PageReference pageRef = Page.OrderRequestEmail;
        pageRef.getParameters().put('Id', mOrderRequest.Id);
        Test.setCurrentPageReference(pageRef);
        
        // Start Tests	        
        Test.StartTest();
        
       	// Load the Controller Extension
        OrderRequestExtension oreExt = new OrderRequestExtension(new ApexPages.StandardController(mOrderRequest)); 
                
        // Assert that we have an error reported
        system.AssertEquals( true, oreExt.sendIncomplete );   

		// Stop Tests
        Test.StopTest();
        
    }

	
   /*
    * @name         testNewOrderRequestEmail
    * @description	Setup and send an email
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    static testmethod void testNewOrderRequestEmail() {   	

        // Load the Page using the Order Request Id      
        PageReference pageRef = Page.OrderRequestEmail;
        pageRef.getParameters().put('Id', mOrderRequest.Id);
        pageRef.getParameters().put('mode', 'sendrequest');
        Test.setCurrentPageReference(pageRef);
        
       	// Load the Controller Extension
        OrderRequestExtension oreExt = new OrderRequestExtension(new ApexPages.StandardController(mOrderRequest)); 
        
        // Initialise
        oreExt.init();
        
        // Set the email fields for sending
        oreExt.toAddr 	= 'sales.enablers@ee.co.uk.dev1';
        oreExt.subject 	= 'Test Subject';
        oreExt.body 	= 'This is Test to verify our request emailing system works';
       
        
        // Next Step
        oreExt.nextStep();
        
        // Back Step
        oreExt.backStep();

        // Next Step
        oreExt.nextStep();
        
        // Next Step
        oreExt.nextStep();

		// Send    
        try{          
        oreExt.onSend();
        }
        Catch(exception e){}

        
    }


   /*
    * @name         testNewOrderRequestStatusCancelled
    * @description	Sets the Status to Cancelled
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    static testmethod void testNewOrderRequestStatusCancelled() {   	

        // Load the Page using the Order Request Id      
        PageReference pageRef = Page.OrderRequestEmail;
        pageRef.getParameters().put('Id', mOrderRequest.Id);
        pageRef.getParameters().put('mode', 'cancelrequest');
        Test.setCurrentPageReference(pageRef);
        
       	// Load the Controller Extension
        OrderRequestExtension oreExt = new OrderRequestExtension(new ApexPages.StandardController(mOrderRequest)); 
        
		// Set the Status to Cancelled
		//mOrderRequest.Status__c = 'Cancelled';
		//update mOrderRequest;
        
        oreExt.oreq.Status__c = 'Cancelled';
        
        // Start Tests	        
        Test.StartTest();
   
        
        // Set the email fields for sending
        oreExt.toAddr 	= 'sales.enablers@ee.co.uk.dev1';
        oreExt.subject 	= 'Test Subject';
        oreExt.body 	= 'This is Test to verify our request emailing system works';
        
		// Save                
        oreExt.onSave();
        
		// Stop Tests
        Test.StopTest();
        

    }


   /*
    * @name         testNewOrderRequestStatusCancelled
    * @description	Sets the Status to Cancelled
    * @author       P Goodey
    * @date         May 2014
    * @see 
    */
    static testmethod void testNewOrderRequestStatusComplete() { 

        // Load the Page using the Order Request Id      
        PageReference pageRef = Page.OrderRequestEmail;
        pageRef.getParameters().put('Id', mOrderRequest.Id);
        pageRef.getParameters().put('mode', 'cancelrequest');
        Test.setCurrentPageReference(pageRef);
        
       	// Load the Controller Extension
        OrderRequestExtension oreExt = new OrderRequestExtension(new ApexPages.StandardController(mOrderRequest)); 
        
		// Set the Status to Cancelled
		//mOrderRequest.Status__c = 'Cancelled';
		//update mOrderRequest;
        
        oreExt.oreq.Status__c = 'Complete';
        
        // Start Tests	        
        Test.StartTest();
   
  		// Save                
        oreExt.onSave();
        
		// Stop Tests
        Test.StopTest();
        

    }



   /*
    * @name         testCodeCoverageMiscellaneous
    * @description	selects the Add product - for Welcome Days - in the Order Item selection screen
    * @author       P Goodey
    * @date         Apr 2014
    * @see 
    */
    static testmethod void testCodeCoverageMiscellaneous() {   	

        // Load the Page using the Order Request Id      
        PageReference pageRef = Page.OrderRequestEmail;
        pageRef.getParameters().put('Id', mOrderRequest.Id);
        Test.setCurrentPageReference(pageRef);
        
       	// Load the Controller Extension
        OrderRequestExtension oreExt = new OrderRequestExtension(new ApexPages.StandardController(mOrderRequest)); 
        
        // Initialise
        oreExt.init();

		// getSupplierEmail
		oreExt.getSupplierEmail();
		
		// getSubjectEmailCancel
		oreExt.getSubjectEmailCancel();
		
		// getSubjectEmailSend
		oreExt.getSubjectEmailSend();
		
		// getBodyEmailCancel
		oreExt.getBodyEmailCancel();
		
		// getBodyEmailSend
		oreExt.getBodyEmailSend();
		
		// Test the Attachment Code
		//create a new attachment
		Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body'); //controller.attachment.body=bodyBlob;
		Attachment att = new Attachment();
		att.Name	= 'Test';
		att.Body	= bodyBlob;
		OrderRequestExtension.AttachmentWrapper attWrapper = new OrderRequestExtension.AttachmentWrapper(att);
		
		// getSelectAttachments
		oreExt.getSelectAttachments();
		
		// getAttachments
		oreExt.getAttachments();
		
		// getAttachmentWrappers
		oreExt.getAttachmentWrappers();
		
		// getSelectedAttachmentCount
		oreExt.getSelectedAttachmentCount();
		
		// init_cancel
		oreExt.init_cancel();
		
		// onSubmit
		oreExt.onSubmit();

    }
    
 
    
}