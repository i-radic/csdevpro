@IsTest(SeeAllData=true)
public class CS_RPActionClassPriceItemTest  {

	public static cscfga__Product_Definition__c createTestData(){
		cspmb__Price_Item__c pi = CS_TestDataFactory.generatePriceItem(true, 'Test Price Item');
		RecordType rt = [SELECT Id, Name FROM RecordType Where Name = 'BT Broadband Plan'];
		pi.RecordType = rt;
		pi.cspmb__Is_Active__c = true;
		update pi;
		return CS_TestDataFactory.generateProductDefinition(true, 'Test');
	}

	public static testMethod void testRPAction(){
		cscfga__Product_Definition__c pd = createTestData();
		Test.startTest();

		RPActionClassPriceItem customObj = new RPActionClassPriceItem();
        Map<String, String> searchFields = new Map<String, String>();

        String res1 = customObj.getRequiredAttributes();
		Id[] excludeIds;
		Object[] res2 = customObj.doDynamicLookupSearch(searchFields, pd.Id);
        Object[] res3 = customObj.doLookupSearch(searchFields, pd.Id, excludeIds, 1, 1);

		Test.stopTest();
	}
}