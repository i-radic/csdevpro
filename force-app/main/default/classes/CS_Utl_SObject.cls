/**
 * @name CS_Utl_SObject
 * @description Utility class to manipulate SObjects
 * @revision
 *
 */
public class CS_Utl_SObject {
	/**
	 * Returns field value from a given object
	 * @param  obj       SObject
	 * @param  fieldName String
	 * @return           String
	 */
	public static String getFieldValue(SObject obj, String fieldName) {
		if(obj == null) {
			return null;
		}
		
		try {
			if(fieldName.contains('.')) {
				List<String> separatedField = fieldName.split('\\.', 2);
				return getFieldValue((SObject) obj.getSobject(separatedField.get(0)), separatedField.get(1));
			}
			else {
				return String.valueOf(obj.get(fieldName));
			}
		}

		catch(SObjectException ex) {
			return null;
		}
	}
}