/********************************************************************************************************************************
		Class Name	: BTSportMVOCHandler
		Test Class Name : Test_BTSportMVOC
		Description	: Code to Calculate RollUp Summary
		Version	: V0.1
		Created By Author Name : RAJI VUBA
		Date : 30/05/2018
 *********************************************************************************************************************************/
public class BTSportMVOCHandler {
    
   public static Boolean AfterUpdateRecurssionflag = True;
    
    public static void AfterInsertHandler(List<BT_Sport_SVOC__c> SVOCList){        
        BTSportMVOCHelper.AfterInsertMethod(SVOCList);       
    }    
    public static void AfterUpdateHandler(List<BT_Sport_SVOC__c> SVOCList){        
        BTSportMVOCHelper.AfterUpdateMethod(SVOCList);
    }    
    public static void AfterDeleteHandler(List<BT_Sport_SVOC__c> SVOCList){        
        BTSportMVOCHelper.AfterDeleteMethod(SVOCList);        
    }    
    public static void AfterUnDeleteHandler(List<BT_Sport_SVOC__c> SVOCList){ 
        BTSportMVOCHelper.AfterUnDeleteMethod(SVOCList);
    }
}