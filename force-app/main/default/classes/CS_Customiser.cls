/**
 * Configurator Customiser implementation
 * 
 * @author Kristijan Kosutic
 */
global class CS_Customiser extends cscfga.AConfigurationCustomisation {
	
	private static Blob body;
	
    //Added for SME implementation
    @TestVisible
	private boolean isSMEUser;
	    
	/**
	 * Prepare totals and values for interaction with child/parent configurations.
	 * Load attributes used for basket total calculations.
	 *
	 * @param ctrl
	 * @return String html to be inserted on configurator page
	 */
	global override String getTopComponentHtml(cscfga.ProductConfiguratorController ctrl) {
		cscfga.ProductConfiguration pc = ctrl.getConfig();
		system.debug(LoggingLevel.ERROR, pc.getRoot());
		cscfga.ProductConfiguration root = ctrl.getRootConfig();
		cscfga__Product_Basket__c basket = ctrl.basket;
		String html = 'null;';
		List<String> relatedProductNames = new List<String>();
		String definitionName = pc.getSObject().cscfga__Product_Definition__r.Name;
		Map<String, CS_Related_Products__c> relatedProducts = CS_Related_Products__c.getall();
		Map<String, CS_Customiser_Extension__c> customiserExtensions = CS_Customiser_Extension__c.getall();
		Map<String, CS_Related_Product_Attributes__c> relatedProductAttributes = CS_Related_Product_Attributes__c.getall();
		Map<String, Map<String, String>> relatedProductDetails = new Map<String, Map<String, String>>();
		
		// if there is no root Id -> first child or parent product -> no need to load any other html for it 
		if (root.getSObject().Id != null && relatedProducts.get(definitionName) != null) {
			relatedProductNames = relatedProducts.get(definitionName).Related_Products__c.split(',');
			List<Attachment> attList = [Select Id, Name, body from Attachment where ParentId = :root.getSObject().Id and Name in :relatedProductNames];
			Map<String, Map<String, String>> relatedProductValues = new Map<String, Map<String, String>>();
			for (Attachment attachment : attList) {
				Map<String, Map<String, String>> attBody = (Map<String, Map<String, String>>) JSON.deserialize(attachment.body.toString(), Map<String, Map<String, String>>.class);
				// relatedProductValues.putAll(attBody);
				if (relatedProductAttributes.get(attachment.Name) != null) {
					List<String> columnNames = new List<String>();
					if (relatedProductAttributes.get(attachment.Name).List_View_Attribute_Names__c != null) {
						columnNames = relatedProductAttributes.get(attachment.Name).List_View_Attribute_Names__c.split(',');
					}
					List<String> totalNames = new List<String>();
					if (relatedProductAttributes.get(attachment.Name).Total_Attribute_Names__c != null) {
						totalNames = relatedProductAttributes.get(attachment.Name).Total_Attribute_Names__c.split(',');
						if (relatedProductAttributes.get(attachment.Name).Total_Attribute_Names_2__c != null && relatedProductAttributes.get(attachment.Name).Total_Attribute_Names_2__c != '') {
							totalNames.addAll(relatedProductAttributes.get(attachment.Name).Total_Attribute_Names_2__c.split(','));
						}
					}
					for (String productId : attBody.keySet()) {
						Map<String, String> columnValues = new Map<String, String>();
						Map<String, String> totalValues = new Map<String, String>();
						for (String colName : columnNames) {
							if (attBody.get(productId).get(colName) != null) {
								columnValues.put(colName, attBody.get(productId).get(colName));
							} else {
								columnValues.put(colName, '');
							}
						}
						for (String totalName : totalNames) {
							if (attBody.get(productId).get(totalName) != null) {
								totalValues.put(totalName, attBody.get(productId).get(totalName));
							} else {
								totalValues.put(totalName, '');
							}
						}
						relatedProductDetails.put(productId, columnValues);
						relatedProductValues.put(productId, totalValues);
					}
				}
			}
			if (relatedProductValues != null) {
				html = JSON.serialize(relatedProductValues);
			}
		}
		html = '<script>relatedProducts = ' + html + ';';
		if (!relatedProductNames.isEmpty()) {
			html += 'relatedProductNames = ' + JSON.serialize(relatedProductNames) + ';';
		} else {
			html += 'relatedProductNames = null;';
		}
		if (!relatedProductDetails.isEmpty()) {
			html += 'relatedProductDetails = ' + JSON.serialize(relatedProductDetails) + ';';
		} else {
			html += 'relatedProductDetails = null;';
		}
		cscfga.ProductConfiguration parentConfig = pc.getParent();
		cscfga__Product_Configuration__c rootConfig;
		
		Id rootId;
		// Bug fix -> check if root is same as parent
		if (!pc.isRoot() && root.getSObject().Id != null) {
			cscfga__Product_Configuration__c helpConfig = [
				select id, cscfga__Root_Configuration__c
				from cscfga__Product_Configuration__c
				where id = :root.getSObject().Id
			];
			
			if (helpConfig != null && helpConfig.cscfga__Root_Configuration__c != null) {
				rootId = helpConfig.cscfga__Root_Configuration__c;
			} else {
				rootId = root.getSObject().Id;
			}
			// if there is pc -> set root
			/*
			if (pc.getSObject().Id != null) {
				String rootQuery = 'select ' + CS_Utils.getSobjectFields('cscfga__Product_Configuration__c');
				rootQuery += ', cscfga__Product_Definition__r.Name, (select ';
				rootQuery += CS_Utils.getSobjectFields('cscfga__Attribute__c');
				rootQuery += ' from cscfga__Attributes__r)';
				rootQuery += ' from cscfga__Product_Configuration__c';
				rootQuery += ' where id= :rootId';
				List<cscfga__Product_Configuration__c> pcs = Database.query(rootQuery);
				rootConfig = !pcs.isEmpty() ? pcs[0] : null;
			}
			*/
		}
		Boolean firstTime = false;
		// first time save
		if (!pc.isRoot() && root.getSObject().cscfga__Product_Definition__r.Name != parentConfig.getSObject().cscfga__Product_Definition__r.Name && root.getSObject().Id == null) {
			firstTime = true;
		}
		if (!pc.isRoot()) {
			// on edit -> parent is saved
			if (pc.getSObject().Id != null) {
				html += 'rootConfig = null;';
				html += 'parentConfig = null;';
			} else {
				if (root.getSObject().cscfga__Product_Definition__r.Name != parentConfig.getSObject().cscfga__Product_Definition__r.Name && parentConfig.getSObject().Id == null) {
					html += 'rootConfig = ' + getConfigurationMsgData(root.getAttributes(), root.getSObject().cscfga__Product_Definition__r.Name) + ';';
					html += 'parentConfig = ' + getConfigurationMsgData(parentConfig.getAttributes(), parentConfig.getSObject().cscfga__Product_Definition__r.Name) + ';';
				} else {
					html += 'parentConfig = null;';
					html += 'rootConfig = ' + getConfigurationMsgData(parentConfig.getAttributes(), parentConfig.getSObject().cscfga__Product_Definition__r.Name) + ';';
				}
			}
			/*
			if (rootConfig == null && !firstTime) {
				html += 'parentConfig = null;';
				if (parentConfig.getSObject().Id != null) {
					html += 'rootConfig = ' + getConfigurationMsgData(parentConfig.getSObject()) + ';';
				} else {
					html += 'rootConfig = ' + getConfigurationMsgData(parentConfig.getAttributes(), parentConfig.getSObject().cscfga__Product_Definition__r.Name) + ';';
				}
			} else {
				if (parentConfig.getSObject().Id != null) {
					html += 'parentConfig = ' + getConfigurationMsgData(parentConfig.getSObject()) + ';';
				} else {
					html += 'parentConfig = ' + getConfigurationMsgData(parentConfig.getAttributes(), parentConfig.getSObject().cscfga__Product_Definition__r.Name) + ';';
				}
				if (firstTime) {
					html += 'rootConfig = ' + getConfigurationMsgData(root.getAttributes(), root.getSObject().cscfga__Product_Definition__r.Name) + ';';
				} else {
					html += 'rootConfig = ' + getConfigurationMsgData(rootConfig) + ';';
				}
			}
			*/
		} else {
			html += 'rootConfig = null;';
			html += 'parentConfig = null;';
		}
		String attributes = getDefinitionAttributesJSON(definitionName);
		if (attributes != '') {
			html += 'attributes = ' + attributes + ';';
		} else {
			html += 'attributes = null;';
		}
		// Product definition specific code -> use CS_CustomiserExtension interface implementation
		if (customiserExtensions.get(definitionName) != null) {
			Type t = Type.forName(customiserExtensions.get(definitionName).Class_Name__c);
			CS_ICustomiserExtension customiserExtension = (CS_ICustomiserExtension) t.newInstance();
			html += customiserExtension.getTopComponentHtml(ctrl);
		}
		String urlString = URL.getSalesforceBaseUrl().toExternalForm();
		urlString += Page.CS_ConfigurationBasketTotals.getUrl();
		urlString += '?';
		if (basket != null && basket.Id != null) {
			urlString += 'basketId=' + basket.Id;
			urlString += '&';
		}
		if (pc != null && pc.getSObject().Id != null) {
			urlString += 'configId=' + pc.getSObject().Id;
			urlString += '&';
		}
		if (!pc.isRoot()) {
			if (rootConfig == null) {
				if (parentConfig.getSObject().Id != null) {
					urlString += 'rootId=' + parentConfig.getSObject().Id;
					urlString += '&';
				} else if (root.getSObject().Id != null) {
					urlString += 'rootId=' + root.getSObject().id;
					urlString += '&';
				}
			} else {
				if (parentConfig.getSObject().Id != null) {
					urlString += 'parentId=' + parentConfig.getSObject().Id;
					urlString += '&';
				}
				if (rootId != null) {
					urlString += 'rootId=' + rootId;
				} else if (root.getSObject().id != null) {
					urlString += 'rootId=' + root.getSObject().id;
				}
			}
		}
		urlString = urlString.replaceAll('cscfga', 'c');
		system.debug(LoggingLevel.ERROR, urlString);
		html += '</script>';
		html += '<iframe frameborder="0" width="100%" height="98" scrolling="no" id="frame3" src="' + urlString + '" ></iframe>';
		
		return html;
	}
	
	/**
	 * Method creates attachment on root configuration. 
	 *
	 * Child configurations values are stored in JSON format and will be used on parent to 
	 * populate some values or for validations when second child will be created. We will
	 * use Customiser on parent to access this and extract relevant data. Attachments are
	 * used to avoid limitation of salesforce long text area fields.
	 *
	 * @param ctrl
	 */	
	global override void afterSave(cscfga.ProductConfiguratorController ctrl) {
		cscfga.ProductConfiguration root = ctrl.getRootConfig();
		cscfga.ProductConfiguration pc = ctrl.getConfig();
		String definitionName = pc.getSObject().cscfga__Product_Definition__r.Name;
		cscfga__Product_Basket__c basket = ctrl.basket;
		
		// Collect only child configuration data
		if (!pc.isRoot()) {
			// bug fix
			cscfga__Product_Configuration__c helpConfig = [
				select id, cscfga__Root_Configuration__c
				from cscfga__Product_Configuration__c
				where id = :root.getSObject().Id
			];
			Id rootId;
			if (helpConfig != null && helpConfig.cscfga__Root_Configuration__c != null) {
				rootId = helpConfig.cscfga__Root_Configuration__c;
			} else {
				rootId = root.getSObject().Id;
			}
			Map<String, CS_Related_Product_Attributes__c> relatedProductAttributes = CS_Related_Product_Attributes__c.getall();
			Map<String, String> attributes = new Map<String, String>();
			
			if (relatedProductAttributes.get(definitionName) != null && relatedProductAttributes.get(definitionName).Attribute_Names__c != null) {
				List<String> attributeNames = relatedProductAttributes.get(definitionName).Attribute_Names__c.split(',');
				if (relatedProductAttributes.get(definitionName).Attribute_Names_2__c != null) {
					attributeNames.addAll(relatedProductAttributes.get(definitionName).Attribute_Names_2__c.split(','));
				}
				for (String attributeName : attributeNames) {
					if (pc.containsAttribute(attributeName)) {
						cscfga.Attribute att = pc.getAttribute(attributeName);
						attributes.put(att.getSObject().Name, att.getValue());
					}
				}
			}
			List<Attachment> attList = [Select Id, Name, body from Attachment where ParentId = :rootId and Name = :definitionName];
			Attachment attachment;
			if (!attList.isEmpty()) {
				attachment = attList[0];
			}
			if (attachment == null) {
				attachment = new Attachment(
					Name = definitionName,
					ParentId = rootId,
					body = Blob.valueOf('')
				);
			}
			if (attachment.body.toString() != null && attachment.body.toString() != '') {
				Map<String, Map<String, String>> attBody = (Map<String, Map<String, String>>) JSON.deserialize(attachment.body.toString(), Map<String, Map<String, String>>.class);
				attBody.put(pc.getSObject().Id, attributes);
				attachment.body = Blob.valueOf(JSON.serialize(attBody));
			} else {
				Map<String, Map<String, String>> attBody = new Map<String, Map<String, String>>();
				attBody.put(pc.getSObject().Id, attributes);
				attachment.body = Blob.valueOf(JSON.serialize(attBody));
			}
			upsert attachment;
		} else {
			cscfga__Product_Configuration__c rootConfig = root.getSObject();
			Id basketId;
			List<cscfga__Product_Configuration__c> descendants = new List<cscfga__Product_Configuration__c>();
			if (rootConfig.Id != null) {
				String configurationQuery = 'select '
					+ CS_Utils.getSobjectFields('cscfga__Product_Configuration__c')
					+ ', cscfga__Product_Definition__r.Name'
					+ ', cscfga__Parent_Configuration__r.Group_No_of_Users__c'
					+ ', cscfga__Product_Basket__r.Tenure__c'
					+ ' from cscfga__Product_Configuration__c'
					+ ' where Id = \'' + rootConfig.Id + '\''
					+ ' or cscfga__Root_Configuration__c = \'' + rootConfig.Id + '\'';
				descendants = Database.query(configurationQuery);
				Decimal Tenure = 0;
				Decimal BasketTenure = 0;
				Boolean requiresUpdate = false;
				for (cscfga__Product_Configuration__c pconf : descendants) {
					if (pconf.cscfga__Root_Configuration__c == null) {
						Tenure = pconf.Tenure__c;
					}
					basketId = pconf.cscfga__Product_Basket__c;
					BasketTenure = pconf.cscfga__Product_Basket__r.Tenure__c;
				}
				for (cscfga__Product_Configuration__c pconf : descendants) {
					if (definitionName == 'Mobile Voice') {
						if (pconf.Tenure__c != Tenure) {
							requiresUpdate = true;
						}
						if (pconf.cscfga__Product_Definition__r.Name == 'Single Add On' && pconf.Group_No_of_Users__c != pconf.cscfga__Parent_Configuration__r.Group_No_of_Users__c) {
							requiresUpdate = true;
						}
					} else if (definitionName == 'Fixed Line' || definitionName == 'Security' || definitionName == 'Data VPN' || definitionName == 'Coverage Solutions' 
							|| definitionName == 'Mobile Voice Bespoke' || definitionName == 'Mobile Data' || definitionName == 'Mobile Data for Tablets' || definitionName == 'M2M'
							|| definitionName == 'Mobile Voice VPN' || definitionName == 'Fixed Landline Bespoke') {
						if (pconf.Tenure__c != Tenure) {
							requiresUpdate = true;
						}
					}
				}
				if (requiresUpdate) {
					CS_RecalculateConfigurationTotals cs = new CS_RecalculateConfigurationTotals(rootConfig.Id);
				}
				if (Tenure > BasketTenure) {
					BasketTenure = Tenure;
				}
				List<cscfga__Product_Configuration__c> creditFunds = [
					select Id, Name, cscfga__Configuration_Status__c, Credit_fund_staged_Month__c, cscfga__Product_Basket__r.Tenure__c
					from cscfga__Product_Configuration__c
					where cscfga__Product_Basket__c = :basketId
					and cscfga__Product_Definition__r.Name = 'Credit Funds'
				];
				if (!creditFunds.isEmpty()) {
					List<cscfga__Product_Configuration__c> fundsToUpdate = new List<cscfga__Product_Configuration__c>();
					for (cscfga__Product_Configuration__c pconf : creditFunds) {
						if (pconf.Credit_fund_staged_Month__c != null && pconf.Credit_fund_staged_Month__c > pconf.cscfga__Product_Basket__r.Tenure__c) {
							pconf.cscfga__Configuration_Status__c = 'Incomplete';
							fundsToUpdate.add(pconf);
						}
					}
					if (!fundsToUpdate.isEmpty()) {
						update fundsToUpdate;
					}
				}
			}
		}
		List<cscfga__Attribute__c> attributesForUpdate = new List<cscfga__Attribute__c>();
		// reset customiser flags 
		if (pc.containsAttribute('Customiser Processed')) {
			cscfga.Attribute att = pc.getAttribute('Customiser Processed');
			att.setValue('No');
			attributesForUpdate.add(att.getSObject());
			if (!pc.isRoot()) {
				cscfga.ProductConfiguration parent = pc.getParent();
				if (parent.containsAttribute('Customiser Processed')) {
					cscfga.Attribute att2 = parent.getAttribute('Customiser Processed');
					att2.setValue('No');
					attributesForUpdate.add(att2.getSObject());
				}
			}
		}
		
		
		List<Attachment> attList = [
			select Id, Name, Body 
			from Attachment
			where ParentId = :basket.Id
			and (Name = 'UsageProfile-reparent'
			or Name = 'RateCard-reparent')
			
		];
		if (!attList.isEmpty()) {
			List<Attachment> newAttList = new List<Attachment>();
			for (Attachment att : attList) {
				Attachment att2 = new Attachment();
				att2.Name = att.Name.replaceAll('-reparent','');
				if (pc.isRoot()) {
					att2.ParentId = pc.getSObject().Id;
				} else {
					att2.ParentId = root.getSObject().Id;
				}
				att2.Body = att.Body;
				newAttList.add(att2);
			}
			delete attList;
			insert newAttList;
		}
		
		
		if (!attributesForUpdate.isEmpty()) {
			update attributesForUpdate;
		}
	}
	
	/**
	 * prepares javascript object used in configuration totals logic. Object
	 * tells configuration page which attributes to use for recurring, one off
	 * tenure and number of users values
	 * 
	 * @param definitionName
	 * @return JSON body
	 */
	private String getDefinitionAttributesJSON(String definitionName) {
	    //get user profile and set value of isSMEUser
        Id profileId=userinfo.getProfileId();
        String profileName=[Select Id,Name from Profile where Id=:profileId].Name;
        system.debug('ProfileName'+profileName);
        if(ProfileName == 'SME BTLB Sales' || ProfileName == 'SME System Admin' || ProfileName == 'SME Setup (Doxford)' || ProfileName.contains('BTLB')){
            isSMEUser = true;
        }else{
            isSMEUser = false;
        }
		if (body == null) {
		    if(isSMEUser){
			    body = CS_Utils.loadStaticResource('CS_AttributesSME');
		    }else{
		        body = CS_Utils.loadStaticResource('CS_ATTRIBUTES');
		    }
		}
		if (body != null) {
			Map<String, Object> attributeJSON = (Map<String, Object>) JSON.deserializeUntyped(body.toString());
			return attributeJSON.get(definitionName) != null ? JSON.serialize(attributeJSON.get(definitionName)) : '';
		} else {
			return '';
		}
	}
	
	/**
	 * Format configuration to represent msg data sent to basket total page. Iterates 
	 * over list of attributes stored in CS_ATTRIBUTES resource to find which values
	 * attribute to particular value
	 * 
	 * @param configAttributes 
	 * @return JSON object
	 */
	@TestVisible
	private String getConfigurationMsgData(List<cscfga.Attribute> configAttributes, String definitionName) {
		Map<String, Decimal> configValues = new Map<String, Decimal>();
		Map<String, String> attMap = new Map<String, String>();
		for (cscfga.Attribute attribute : configAttributes) {
			cscfga__Attribute__c att = attribute.getSObject();
			attMap.put(att.Name.replaceAll(' ', '_') + '_0', att.cscfga__Value__c);
		}
		configValues.put('numberOfUsers', 0);
		configValues.put('tenure', 0);
		configValues.put('totalCharges', 0);
		configValues.put('totalChargesNPV', 0);
		configValues.put('totalCost', 0);
		configValues.put('totalCostsNPV', 0);
		configValues.put('totalOneOffCharge', 0);
		configValues.put('totalOneOffCost', 0);
		configValues.put('totalRecurringCharge', 0);
		configValues.put('totalRecurringCost', 0);
		//SME Implementation
		configValues.put('totalInvestmentPot', 0);
		configValues.put('chargesPerCTN', 0);
		configValues.put('totalDeviceCharges', 0);
		String attributes = getDefinitionAttributesJSON(definitionName);
		if (attributes != '') {
			Map<String, Object> o = (Map<String, Object>) JSON.deserializeUntyped(attributes);
			String tenure = o.get('tenure') != null ? String.valueOf(o.get('tenure')) : null;
			String numberOfUsers = o.get('numberOfUsers') != null ? String.valueOf(o.get('numberOfUsers')) : null;
			List<Object> oneoffCharge = o.get('oneoffCharge') != null ? (List<Object>) o.get('oneoffCharge') : null;
			List<Object> oneoffCost = o.get('oneoffCost') != null ? (List<Object>) o.get('oneoffCost') : null;
			List<Object> recurringCharge = o.get('recurringCharge') != null ? (List<Object>) o.get('recurringCharge') : null;
			List<Object> recurringCost = o.get('recurringCost') != null ? (List<Object>) o.get('recurringCost') : null;
			if (tenure != null && attMap.get(tenure) != null) {
				configValues.put('tenure', Decimal.valueOf(attMap.get(tenure)));
			}
			if (numberOfUsers != null && attMap.get(numberOfUsers) != null) {
				configValues.put('numberOfUsers', Decimal.valueOf(attMap.get(numberOfUsers)));
			}
			//SME Implementation
		    if(isSMEUser){
		        List<Object> totalIP = o.get('totalInvestmentPot') != null ? (List<Object>) o.get('totalInvestmentPot') : null;
		        List<Object> chargesCTN = o.get('chargesPerCTN') != null ? (List<Object>) o.get('chargesPerCTN') : null;
		        List<Object> totalDC = o.get('totalDeviceCharges') != null ? (List<Object>) o.get('totalDeviceCharges') : null;
		        List<Object> tc = o.get('totalCharge') != null ? (List<Object>) o.get('totalCharge') : null;
		        if (totalIP != null) {
		            Decimal val = 0;
		            for (Object str : totalIP) {
		                val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
		            }
		            configValues.put('totalInvestmentPot',val);
    			}
    			if (totalDC != null) {
    			    Decimal val = 0;
    			    for (Object str : totalDC) {
    				    val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    			    }  
    			    configValues.put('totalDeviceCharges', val);
    			}
    			if (tc != null) {
    			    Decimal val = 0;
    			    for (Object str : tc) {
    				    val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    			    }   
    			    configValues.put('totalCharges',val);
    			}
    			if (chargesCTN != null) {
    			    Decimal val = 0;
    			    /*for (Object str : chargesCTN) {
    			        val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    			    }  */
    			    
    			    if(configValues.get('totalCharges') >0 && configValues.get('numberOfUsers') >0){
    			       val = configValues.get('totalCharges')/ configValues.get('numberOfUsers');
    			    }
    			     configValues.put('chargesPerCTN',val);
    			}
		        
		    }else{
    			if (oneoffCharge != null) {
    				Decimal total = 0;
    				for (Object str : oneOffCharge) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalOneOffCharge', total);
    			}
    			if (oneoffCost != null) {
    				Decimal total = 0;
    				for (Object str : oneoffCost) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalOneOffCost', total);
    			}
    			if (recurringCharge != null) {
    				Decimal total = 0;
    				for (Object str : recurringCharge) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalRecurringCharge', total);
    			}
    			if (recurringCost != null) {
    				Decimal total = 0;
    				for (Object str : recurringCost) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalRecurringCost', total);
    			}
    			configValues.put('totalCharges', configValues.get('tenure') * configValues.get('totalRecurringCharge') + configValues.get('totalOneOffCharge'));
    			configValues.put('totalCost', configValues.get('tenure') * configValues.get('totalRecurringCost') + configValues.get('totalOneOffCost'));
		    }
		}
		return JSON.serialize(configValues);
	}
	
	/**
	 * Format configuration to represent msg data sent to basket total page. Iterates 
	 * over list of attributes stored in CS_ATTRIBUTES resource to find which values
	 * attribute to particular value
	 * 
	 * @param productConfiguration 
	 * @return JSON object
	 */
	@TestVisible
	private String getConfigurationMsgData(cscfga__Product_Configuration__c productConfiguration) {
		Map<String, Decimal> configValues = new Map<String, Decimal>();
		Map<String, String> attMap = new Map<String, String>();
		for (cscfga__Attribute__c att : productConfiguration.cscfga__Attributes__r) {
			attMap.put(att.Name.replaceAll(' ', '_') + '_0', att.cscfga__Value__c);
		}
		configValues.put('numberOfUsers', 0);
		configValues.put('tenure', 0);
		configValues.put('totalCharges', 0);
		configValues.put('totalChargesNPV', 0);
		configValues.put('totalCost', 0);
		configValues.put('totalCostsNPV', 0);
		configValues.put('totalOneOffCharge', 0);
		configValues.put('totalOneOffCost', 0);
		configValues.put('totalRecurringCharge', 0);
		configValues.put('totalRecurringCost', 0);

		configValues.put('totalInvestmentPot', 0);
		configValues.put('chargesPerCTN', 0);
		configValues.put('totalDeviceCharges', 0);
		// add here new variables
		// instead of below load new static resource
		// create calculations for new values
		String attributes = getDefinitionAttributesJSON(productConfiguration.cscfga__Product_Definition__r.Name);
		if (attributes != '') {
		    Map<String, Object> o = (Map<String, Object>) JSON.deserializeUntyped(attributes);
		    String tenure = o.get('tenure') != null ? String.valueOf(o.get('tenure')) : null;
			String numberOfUsers = o.get('numberOfUsers') != null ? String.valueOf(o.get('numberOfUsers')) : null;
			List<Object> oneoffCharge = o.get('oneoffCharge') != null ? (List<Object>) o.get('oneoffCharge') : null;
			List<Object> oneoffCost = o.get('oneoffCost') != null ? (List<Object>) o.get('oneoffCost') : null;
			List<Object> recurringCharge = o.get('recurringCharge') != null ? (List<Object>) o.get('recurringCharge') : null;
			List<Object> recurringCost = o.get('recurringCost') != null ? (List<Object>) o.get('recurringCost') : null;
	
			if (tenure != null && attMap.get(tenure) != null) {
				configValues.put('tenure', Decimal.valueOf(attMap.get(tenure)));
			}
			if (numberOfUsers != null && attMap.get(numberOfUsers) != null) {
				configValues.put('numberOfUsers', Decimal.valueOf(attMap.get(numberOfUsers)));
			}
		    //SME Implementation
		    if(isSMEUser){
		        List<Object> totalIP = o.get('totalInvestmentPot') != null ? (List<Object>) o.get('totalInvestmentPot') : null;
		        List<Object> chargesCTN = o.get('chargesPerCTN') != null ? (List<Object>) o.get('chargesPerCTN') : null;
		        List<Object> totalDC = o.get('totalDeviceCharges') != null ? (List<Object>) o.get('totalDeviceCharges') : null;
		        List<Object> tc = o.get('totalCharge') != null ? (List<Object>) o.get('totalCharge') : null;
		        if (totalIP != null) {
		            Decimal val = 0;
		            for (Object str : totalIP) {
		                val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
		            }
		            configValues.put('totalInvestmentPot',val);
    			}
    			if (totalDC != null) {
    			    Decimal val = 0;
    			    for (Object str : totalDC) {
    				    val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    			    }  
    			    configValues.put('totalDeviceCharges', val);
    			}
    			if (tc != null) {
    			    Decimal val = 0;
    			    for (Object str : tc) {
    				    val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    			    }   
    			    configValues.put('totalCharges',val);
    			}
    			if (chargesCTN != null) {
    			    Decimal val = 0;
    			    /*for (Object str : chargesCTN) {
    			        val += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    			    }  */
    			    
    			    if(configValues.get('totalCharges') >0 && configValues.get('numberOfUsers') >0){
    			       val = configValues.get('totalCharges')/ configValues.get('numberOfUsers');
    			    }
    			    configValues.put('chargesPerCTN',val);
    			}
		        
		    }else{
    		    if (oneoffCharge != null) {
    				Decimal total = 0;
    				for (Object str : oneOffCharge) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalOneOffCharge', total);
    			}
    			if (oneoffCost != null) {
    				Decimal total = 0;
    				for (Object str : oneoffCost) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalOneOffCost', total);
    			}
    			if (recurringCharge != null) {
    				Decimal total = 0;
    				for (Object str : recurringCharge) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalRecurringCharge', total);
    			}
    			if (recurringCost != null) {
    				Decimal total = 0;
    				for (Object str : recurringCost) {
    					total += attMap.get(String.valueOf(str)) != null ? Decimal.valueOf(attMap.get(String.valueOf(str))) : 0;
    				}
    				configValues.put('totalRecurringCost', total);
    			}
    			configValues.put('totalCharges', configValues.get('tenure') * configValues.get('totalRecurringCharge') + configValues.get('totalOneOffCharge'));
    			configValues.put('totalCost', configValues.get('tenure') * configValues.get('totalRecurringCost') + configValues.get('totalOneOffCost'));
    		}
		}
		return JSON.serialize(configValues);
	}

}