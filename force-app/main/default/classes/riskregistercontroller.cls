public class riskregistercontroller { 
    Id AccId; 
    public riskregistercontroller(ApexPages.StandardController controller) {   
        ID rid = ApexPages.currentPage().getParameters().get('id');
        AccId = [SELECT Id,Account__r.Id FROM Risk_Register_DBAM__c WHERE Id = :rid].Account__r.Id;
    }
    
    
      
    public List<Contract> getContracts() {        
        return [SELECT Id,ContractNumber,Adder_Contract_ID__c,Base_Product__c,StartDate,EndDate,Tier_Value__c,Committed_Spend__c FROM Contract WHERE AccountId = :AccId AND Adder_Status__c =: 'Active' ORDER BY Committed_Spend__c DESC];
    }
}