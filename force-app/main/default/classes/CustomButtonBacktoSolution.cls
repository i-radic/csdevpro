global with sharing class CustomButtonBacktoSolution implements csdiscounts.ICustomButton{ 
	private static String CONTROLLER_NAME = 'CustomButtonBacktoSolution';

	global String performAction(String basketId){
		List<Approval.ProcessSubmitRequest> requests = new List<Approval.ProcessSubmitRequest>();
		Id currentUser = UserInfo.getUserId();

		String sfdcURL = Label.CS_SolutionRedirect_Org_Url;
        //String sfdcURL = 'https://btbusiness--csdevpro--cssmgnt.cs82.visual.force.com';
        System.debug('sfdcURL====>' +sfdcURL);  //sfdcURL+
        String newUrl = sfdcURL+'/apex/sceditor?basketId='+basketId;
        return '{"status":"All ok","status":200,"redirectURL":"' + newUrl + '"}';

		//return '{ "message":"All good", "status":200}';
		// MM added test class 2020-11-04
	}
}