public class LeaseDeskAPI {
	
	public static List<Leasing_API_Log__c> logList = new List<Leasing_API_Log__c>();
    
    public HttpRequest GetHttpRequest(){        
        HttpRequest req = new HttpRequest();
        req.setTimeout(120000);
        req.setMethod('POST');        
        LeaseDeskURL__c ldUrl = [select Name, Url__c, Username__c, Password__c from LeaseDeskURL__c where Name = 'API'];
        String url = String.valueOf(ldUrl.Url__c);
        req.setEndpoint(url);
        String username = String.valueOf(ldUrl.Username__c);
        String password = String.valueOf(ldUrl.Password__c);
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);     
        return req;
    }
      
    public List<Company> QueryCompanies(String agentEIN, String agentOUC, String companyName, String postCode, String companiesHouseReg, String companyType, String numMatches) {
        
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
                
        List<Company> companyList = new List<Company>();
        
        // set empty postcode string as null 
        if(postCode == null){
            postcode = '';
        }
              
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                            + '<QueryCompaniesRequest>'
                                + '<CompanyName>' + FormatXmlChars(companyName) + '</CompanyName>'
                                + '<PostCode>' + postCode + '</PostCode>'
                                + '<CompaniesHouseReg>' + companiesHouseReg + '</CompaniesHouseReg>'
                                + '<CompanyTypes>' + companyType + '</CompanyTypes>'
                                + '<NumMatches>' + numMatches + '</NumMatches>'
                            + '</QueryCompaniesRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }               
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'QueryCompaniesRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);
            insert logList;                        
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            Company company = new Company();
            company.Error = 'ERROR: ' + e;
            companyList.add(company);
            return companyList;
        }  
 
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
        
        // process the results
        XmlDom.Element[] elements = doc.getElementsByTagName('Header');
        String ErrorType = elements[0].getValue('ErrorType');
        String ErrorText = elements[0].getValue('ErrorText');         
        If(ErrorType != 'None' || Test_Factory.GetProperty('IsTest') == 'yes') {
            system.debug('##################### CALLOUT RESPONSE ERROR: ' + ErrorText);
            Company company = new Company();
            company.Error = 'ERROR: ' + ErrorText;
            companyList.add(company);
            return companyList;
        }
        else {
            elements = doc.getElementsByTagName('CompanyDetails');
            if (elements != null) {
            for (XmlDom.Element element : elements)
                companyList.add(ToCompany(element));
            }
            return companyList;
        }      
    }
    
    public String CreateCompany(String agentEIN, String agentOUC, String credSafeID, ContactType contactType, String leCode, String sac, String cug, String acctId, String acctName) {
                 
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
        
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                            + '<CreateCompanyRequest>'
                                + '<CreditSafeID>' + credSafeID + '</CreditSafeID>'
                                + '<Contact>'
                                    + '<Name>' + FormatXmlChars(contactType.name) + '</Name>'
                                    + '<PhoneNumber>' + contactType.phoneNumber + '</PhoneNumber>'
                                    + '<EmailAddress>' + contactType.emailAddress + '</EmailAddress>'
                                + '</Contact>'
                                + '<BT-LECode>' + leCode + '</BT-LECode>'
                                + '<BT-SACCode>' + sac + '</BT-SACCode>'
                                + '<BT-CUG>' + cug + '</BT-CUG>'
                                + '<SF-AcctID>' + acctId + '</SF-AcctID>'
                                + '<SF-AcctName>' + FormatXmlChars(acctName) + '</SF-AcctName>'
                            + '</CreateCompanyRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'CreateCompanyRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);
            insert logList;                                
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            return 'ERROR: ' + e;
        }  
 
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
              
        // process the results
        XmlDom.Element[] elements = doc.getElementsByTagName('Header');
        String ErrorType = elements[0].getValue('ErrorType');
        String ErrorText = elements[0].getValue('ErrorText');         
        If(ErrorType != 'None' || Test_Factory.GetProperty('IsTest') == 'yes') {
            system.debug('##################### CALLOUT RESPONSE ERROR: ' + ErrorText);
            return 'ERROR: ' + ErrorText;
        }
        else {
            elements = doc.getElementsByTagName('CreateCompanyResponse');
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                return 'TEST';
            }
            else{
                return elements[0].getValue('LeaseDeskID'); 
            }       
        }             
    } 
    
    public QuickDecisionResponse QuickDecision(String agentEIN, String agentOUC, String leaseDeskID, String salesforceOpptyID, Double amount, Integer term, Funder funder, String FunderUserIdentifier, ProfileType profile) {
                
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
        
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                            + '<QuickDecisionRequest>'
                                + '<LeaseDeskID>' + leaseDeskID + '</LeaseDeskID>'
                                + '<SalesforceOpptyID>' + salesforceOpptyID + '</SalesforceOpptyID>'
                                + '<Amount>' + amount + '</Amount>'
                                + '<Term>' + term + '</Term>'
                                + '<Funder>' + funder + '</Funder>'
                                + '<FunderUserIdentifier>' + funderUserIdentifier + '</FunderUserIdentifier>'
                            	+ '<Profile>' + profile + '</Profile>'
                            + '</QuickDecisionRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'QuickDecisionRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);                                           
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            QuickDecisionResponse quickDecisionResponse = new QuickDecisionResponse();
            quickDecisionResponse.ErrorText = 'ERROR: ' + e;
            return quickDecisionResponse;
        }  
 
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
        
        // process the results
        XmlDom.Element[] elements = doc.getElementsByTagName('Header');
        String ErrorType = elements[0].getValue('ErrorType');
        String ErrorText = elements[0].getValue('ErrorText');         
        If(ErrorType != 'None' || Test_Factory.GetProperty('IsTest') == 'yes') {
            system.debug('##################### CALLOUT RESPONSE ERROR: ' + ErrorText);
            QuickDecisionResponse quickDecisionResponse = new QuickDecisionResponse();
            quickDecisionResponse.ErrorText = 'ERROR: ' + ErrorText;
            return quickDecisionResponse;
        }
        else {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                QuickDecisionResponse quickDecisionResponse = new QuickDecisionResponse();
                return quickDecisionResponse;
            }
            else{
                elements = doc.getElementsByTagName('QuickDecisionResponse');      
                QuickDecisionResponse quickDecisionResponse = new QuickDecisionResponse();
                quickDecisionResponse.ragStatus = elements[0].getValue('RAGStatus');
                quickDecisionResponse.proposalID = elements[0].getValue('LeaseDeskProposalID');
                try{quickDecisionResponse.creditLimit = Double.ValueOf(elements[0].getValue('CreditLimit'));}      
                catch(Exception e){}       
                return quickDecisionResponse;
            }       
        }             
    }
    
    public List<QuickDecisionResponse> MultipleQuickDecision(String agentEIN, String agentOUC, String leaseDeskID, String salesforceOpptyID, Double amount, Integer term, String[] funders, String FunderUserIdentifier, ProfileType profile) {
                
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
        List<QuickDecisionResponse> quickDecisionResponseList = new List<QuickDecisionResponse>();
        
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                         	+ '<QuickDecisionMultipleRequest>';
                         	for (String funder : funders){
	                        	body = body + '<QuickDecisionRequest>'
	                            + '<LeaseDeskID>' + leaseDeskID + '</LeaseDeskID>'
	                            + '<SalesforceOpptyID>' + salesforceOpptyID + '</SalesforceOpptyID>'
	                            + '<Amount>' + amount + '</Amount>'
	                            + '<Term>' + term + '</Term>'
	                            + '<Funder>' + funder + '</Funder>'
	                            + '<FunderUserIdentifier>' + funderUserIdentifier + '</FunderUserIdentifier>'
	                            + '<Profile>' + profile + '</Profile>'
	                            + '</QuickDecisionRequest>';
                         	}
	                    	body = body + '</QuickDecisionMultipleRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'QuickDecisionMultipleRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);                                           
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            QuickDecisionResponse quickDecisionResponse = new QuickDecisionResponse();
            quickDecisionResponse.ErrorText = 'ERROR: ' + e;
            quickDecisionResponseList.Add(quickDecisionResponse);
            return quickDecisionResponseList;
        }  
 
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
        
        // process the results   
		XmlDom.Element[] elements = doc.getElementsByTagName('QuickDecisionResponse');
        if (elements != null) {
        for (XmlDom.Element element : elements)
            quickDecisionResponseList.add(ToQuickDecisionResponse(element));
        }
        return quickDecisionResponseList;
    }
    
    public HoldingProposalResponse CreateHoldingProposal(String agentEIN, String agentOUC, String leaseDeskID, String salesforceOpptyID, Double amount, Integer term) {
                
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
        
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                            + '<CreateHoldingProposalRequest>'
                                + '<LeaseDeskID>' + leaseDeskID + '</LeaseDeskID>'
                                + '<SalesforceOpptyID>' + salesforceOpptyID + '</SalesforceOpptyID>'
                                + '<Amount>' + amount + '</Amount>'
                                + '<Term>' + term + '</Term>'
                                + '</CreateHoldingProposalRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'CreateHoldingProposalRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);                                            
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            HoldingProposalResponse holdingProposalResponse = new HoldingProposalResponse();
            holdingProposalResponse.Error = 'ERROR: ' + e;
            return holdingProposalResponse;
        }  
 
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
        
        // process the results
        XmlDom.Element[] elements = doc.getElementsByTagName('Header');
        String ErrorType = elements[0].getValue('ErrorType');
        String ErrorText = elements[0].getValue('ErrorText');         
        If(ErrorType != 'None' || Test_Factory.GetProperty('IsTest') == 'yes') {
            system.debug('##################### CALLOUT RESPONSE ERROR: ' + ErrorText);
            HoldingProposalResponse holdingProposalResponse = new HoldingProposalResponse();
            holdingProposalResponse.Error = 'ERROR: ' + ErrorText;
            return holdingProposalResponse;
        }
        else {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                HoldingProposalResponse holdingProposalResponse = new HoldingProposalResponse();
                return holdingProposalResponse;
            }
            else{
                elements = doc.getElementsByTagName('CreateHoldingProposalResponse');      
                HoldingProposalResponse holdingProposalResponse = new HoldingProposalResponse();
                holdingProposalResponse.holdingURL = elements[0].getValue('HoldingURL');
                holdingProposalResponse.proposalID = elements[0].getValue('LeaseDeskProposalID');     
                return holdingProposalResponse;
            }       
        }             
    }
    
    public String CreateProposal(String agentEIN, String agentOUC, String leaseDeskProposalID, String sfOpptyID, Double value) {
                
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
        
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                            + '<CreateProposalRequest>'
                                + '<LeaseDeskProposalID>' + leaseDeskProposalID + '</LeaseDeskProposalID>'
                                + '<SalesforceOpptyID>' + sfOpptyID + '</SalesforceOpptyID>'
                                + '<Value>' + value + '</Value>'
                            + '</CreateProposalRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'CreateProposalRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);
            insert logList;                                                       
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            return 'ERROR: ' + e;
        }        
        
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
        
        // process the results
        XmlDom.Element[] elements = doc.getElementsByTagName('Header');
        String ErrorType = elements[0].getValue('ErrorType');
        String ErrorText = elements[0].getValue('ErrorText');         
        If(ErrorType != 'None' || Test_Factory.GetProperty('IsTest') == 'yes') {
            system.debug('##################### CALLOUT RESPONSE ERROR: ' + ErrorText);
            return 'ERROR: ' + ErrorText;
        }
        else {
            elements = doc.getElementsByTagName('CreateProposalResponse');          
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                return 'TEST';
            }
            else{
                return elements[0].getValue('FunderURL'); 
            }       
        } 
    }
    
    public String CheckProposal(String agentEIN, String agentOUC, String leaseDeskProposalID, String FunderUserIdentifier, ProfileType profile) {
                
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
        
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                            + '<CheckProposalRequest>'
                                + '<LeaseDeskProposalID>' + leaseDeskProposalID + '</LeaseDeskProposalID>'
                                + '<FunderUserIdentifier>' + funderUserIdentifier + '</FunderUserIdentifier>'
                            	+ '<Profile>' + profile + '</Profile>'
                            + '</CheckProposalRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'CheckProposalRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);
            insert logList;                                                                  
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            return 'ERROR: ' + e;
        }        
        
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
        
        // process the results
        XmlDom.Element[] elements = doc.getElementsByTagName('Header');
        String ErrorType = elements[0].getValue('ErrorType');
        String ErrorText = elements[0].getValue('ErrorText');         
        If(ErrorType != 'None' || Test_Factory.GetProperty('IsTest') == 'yes') {
            system.debug('##################### CALLOUT RESPONSE ERROR: ' + ErrorText);
            return 'ERROR: ' + ErrorText;
        }
        else {                          
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                return 'TEST';
            }
            else{
                elements = doc.getElementsByTagName('CheckProposalResponse');
                String ragValue = elements[0].getValue('RAGStatus');
                return ragValue;
            }       
        }      
    }
    
    public List<LeasingDetails> GetLeasingHistory(String agentEIN, String agentOUC, String leaseDeskID) {
                
        HttpRequest req = GetHttpRequest();        
        HTTPResponse resp = new HttpResponse();
        Http http = new Http();        
        
        List<LeasingDetails> leasingDetailsList = new List<LeasingDetails>();
        
        // add the body to the request       
        String body = '<LD-API-Request xmlns="http://www.lease-desk.com/LD-API-Request/">'
                        + '<Header>'
                            + '<AgentEIN>' + agentEIN + '</AgentEIN>'
                            + '<AgentOUC>' + agentOUC + '</AgentOUC>'
                        + '</Header>'
                        + '<Body>'
                            + '<GetLeasingHistoryRequest>'
                                + '<LeaseDeskID>' + leaseDeskID + '</LeaseDeskID>'
                            + '</GetLeasingHistoryRequest>'
                        + '</Body>'
                    + '</LD-API-Request>';
        
        req.setBody(body);
        
        try {
            if(Test_Factory.GetProperty('IsTest') == 'yes'){
                String testBody = '<Header>'
                                    + '<ErrorType>TEST</ErrorType>'
                                    + '<ErrorText>TEST</ErrorText'
                                + '</Header>';
                
                resp.SetBody(testBody); 
            }
            else{
                resp = http.send(req);
            }     
            // logging
            Leasing_API_Log__c log = new Leasing_API_Log__c(Operation__c = 'GetLeasingHistoryRequest', Request__c = String.valueof(req.getBody()), Response__c = String.valueof(resp.getBody()));
            logList.Add(log);
            insert logList;      
            // debugging 
            System.debug('##################### CALLOUT REQUEST: ' + req.getBody());
            System.debug('##################### CALLOUT RESPONSE: ' + resp.getBody());
        }    
        catch(Exception e) {
            system.debug('##################### API EXCEPTION: ' + e);
            LeasingDetails leasingDetails = new LeasingDetails();
            leasingDetails.Error = 'ERROR: ' + e;
            leasingDetailsList.add(leasingDetails);
            return leasingDetailsList;
        }        
        
        // create the xml doc that will contain the results of the REST operation
        XmlDom doc = new XmlDom(resp.getBody());
        
        // process the results
        XmlDom.Element[] elements = doc.getElementsByTagName('Header');
        String ErrorType = elements[0].getValue('ErrorType');
        String ErrorText = elements[0].getValue('ErrorText');         
        If(ErrorType != 'None' || Test_Factory.GetProperty('IsTest') == 'yes') {
            system.debug('##################### CALLOUT RESPONSE ERROR: ' + ErrorText);
            LeasingDetails leasingDetails = new LeasingDetails();
            leasingDetails.Error = 'ERROR: ' + ErrorText;
            leasingDetailsList.add(leasingDetails);
            return leasingDetailsList;
        }
        else {
            elements = doc.getElementsByTagName('LeasingDetails');
            if (elements != null) {
            for (XmlDom.Element element : elements)
                leasingDetailsList.add(ToLeasingDetails(element));
            }
            return leasingDetailsList;
        } 
    }   
        
    // utility method to convert the xml element to the inner class
    public QuickDecisionResponse ToQuickDecisionResponse(XmlDom.Element element) {
 
        QuickDecisionResponse quickDecisionResponse = new QuickDecisionResponse();  
        try{quickDecisionResponse.ragStatus = element.getValue('RAGStatus');}
        catch(Exception e){}
        try{quickDecisionResponse.proposalID = element.getValue('LeaseDeskProposalID');}
        catch(Exception e){}
        try{quickDecisionResponse.creditLimit = Double.ValueOf(element.getValue('CreditLimit'));}
        catch(Exception e){}
        try{quickDecisionResponse.Funder = element.getValue('Funder');}
        catch(Exception e){}
        try{quickDecisionResponse.ErrorType = element.getValue('ErrorType');}     
        catch(Exception e){}
        try{quickDecisionResponse.ErrorCode = element.getValue('ErrorCode');}     
        catch(Exception e){}
        try{quickDecisionResponse.ErrorText = element.getValue('ErrorText');}     
        catch(Exception e){}
        return quickDecisionResponse;
    }
    
    // utility method to convert the xml element to the inner class
    public Company ToCompany(XmlDom.Element element) {
 
        Company company = new Company();
        company.credSafeID = element.getValue('CreditSafeID');
        company.companyName = element.getValue('CompanyName');
        company.addr1 = element.getValue('Addr1');
        company.addr2 = element.getValue('Addr2');
        company.addr3 = element.getValue('Addr3');
        company.postCode = element.getValue('PostCode');
        company.companiesHouseReg = element.getValue('CompaniesHouseReg');
        company.companyType = element.getValue('CompanyType');
        return company;
    }
    
    // utility method to convert the xml element to the inner class
    public LeasingDetails ToLeasingDetails(XmlDom.Element element) {
 
        LeasingDetails leasingDetails = new LeasingDetails();
        leasingDetails.leCode = element.getValue('LECode');
        leasingDetails.salesforceAccountID = element.getValue('SalesforceAccountID');
        leasingDetails.salesforceAccountName = element.getValue('SalesforceAccountName');
        leasingDetails.leaseDeskAccountID = element.getValue('LeaseDeskAccountID');
        leasingDetails.leaseDeskCompanyName = element.getValue('LeaseDeskCompanyName');
        leasingDetails.leaseDeskProposalID = element.getValue('LeaseDeskProposalID');
        leasingDetails.cugID = element.getValue('BTCUGID');
        leasingDetails.salesforceOpptyID = element.getValue('SalesforceOpptyID');
        try{leasingDetails.rbo = Double.ValueOf(element.getValue('RBO'));} 
        catch(Exception e){}
        try{leasingDetails.endDate = Date.ValueOf(element.getValue('EndDate'));}
        catch(Exception e){}
        leasingDetails.status = element.getValue('Status');
        leasingDetails.leaseDeskURL = element.getValue('LeaseDeskURL');
        leasingDetails.leaseDeskAgreementID = element.getValue('LeaseDeskAgreementID');
        leasingDetails.funder = element.getValue('Funder');
        leasingDetails.funderURL = element.getValue('FunderURL');
        try{leasingDetails.funderCreditLimit = Double.ValueOf(element.getValue('FunderCreditLimit'));}
        catch(Exception e){}
        leasingDetails.poNumber = element.getValue('PONumber');
        try{leasingDetails.poValue = Double.ValueOf(element.getValue('POValue'));}
        catch(Exception e){}
        leasingDetails.axReference = element.getValue('AXReference');
        try{leasingDetails.regularPayment = Double.ValueOf(element.getValue('RegularPayment'));}
        catch(Exception e){}
        leasingDetails.frequency = element.getValue('Frequency');
        try{leasingDetails.term = Integer.ValueOf(element.getValue('Term'));}
        catch(Exception e){}
        try{leasingDetails.periodsRemaining = Integer.ValueOf(element.getValue('PeriodsRemaining'));}
        catch(Exception e){}
        try{leasingDetails.settlement = Double.ValueOf(element.getValue('Settlement'));}
        catch(Exception e){}
        try{leasingDetails.residualValue = Double.ValueOf(element.getValue('ResidualValue'));}
        catch(Exception e){}
        leasingDetails.product = element.getValue('Product');
        try{leasingDetails.ragStatus = element.getValue('RAGStatus');}
        catch(Exception e){}
        try{leasingDetails.ragDate = Date.ValueOf(element.getValue('RAGDate'));}
        catch(Exception e){}
        return leasingDetails;
    }
    
    public class QuickDecisionResponse {
    
        public String ragStatus {get; set;}
        public String proposalID {get; set;}
        public Double creditLimit {get; set;} 
        public String Funder {get; set;} 
        public String ErrorType {get; set;}
        public String ErrorCode {get; set;}  
        public String ErrorText {get; set;}            
    } 
    
    public class HoldingProposalResponse {
    
        public String holdingURL {get; set;}
        public String proposalID {get; set;} 
        public String Error {get; set;}   
        
    }        
        
    public class Company {
 
        public String credSafeID {get; set;}
        public String companyName {get; set;}
        public String addr1 {get; set;}
        public String addr2 {get; set;}
        public String addr3 {get; set;}
        public String postCode {get; set;}
        public String companiesHouseReg {get; set;}
        public String companyType {get; set;}
        public String Error {get; set;}
    }
    
    public class LeasingDetails {
 
        public String leCode {get; set;}
        public String salesforceAccountID {get; set;}
        public String salesforceAccountName {get; set;}
        public String leaseDeskAccountID {get; set;}
        public String leaseDeskCompanyName {get; set;}
        public String leaseDeskProposalID {get; set;}
        public String cugID {get; set;}
        public String salesforceOpptyID {get; set;}
        public String salesforceOID {get; set;}
        public Double rbo {get; set;}
        public Date endDate {get; set;}
        public String status {get; set;}
        public String leaseDeskURL {get; set;}
        public String leaseDeskAgreementID {get; set;}
        public String funder {get; set;}
        public String funderURL {get; set;}
        public Double funderCreditLimit {get; set;}
        public String poNumber {get; set;}
        public Double poValue {get; set;}
        public String axReference {get; set;}
        public Double regularPayment {get; set;}
        public String frequency {get; set;}
        public Integer term {get; set;}
        public Integer periodsRemaining {get; set;}
        public Double settlement {get; set;}
        public Double residualValue {get; set;}
        public String product {get; set;}
        public String ragStatus {get; set;}
        public Date ragDate {get; set;}
        public String Error {get; set;}
    }
    
    public class ContactType {
        
        public String name {get; set;}
        public String phoneNumber {get; set;}
        public String emailAddress {get; set;}                
    }
    
    public enum Funder {GE, Shire, CIT, DLL} 
    
    public enum ProfileType {Corp, LB}
    
    public String FormatXmlChars(String strVal)
    {   
        if(strVal != null){
        	if(strVal.Contains('&')) {
            strVal = strVal.replace('&','&amp;');
        	}
        }
        return strVal;
    }
 }