@isTest
    global class MockHttpResponseGenerator4 implements HttpCalloutMock {
        // Implement this interface method   
     global HTTPResponse respond(HTTPRequest req) {
         
        // Optionally, only send a mock response for a specific endpoint and method.
        //System.assertEquals('https://www.wsd-rtref.robt.bt.co.uk:53080/get/dev/Salesforce/customers/0123456.xml?v=10.0', req.getEndpoint());
        //System.assertEquals('GET', req.getMethod());
     
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><AddressResponse xmlns="http://capabilities.nat.bt.com/xsd/crm/v4/Addresses"><addressLst><addressDetails><addressIdentifier><value>X05000276372</value></addressIdentifier><contactIntegrationId>Test</contactIntegrationId><addressDetail><location><address><addressName>X05000276372</addressName><postTown>Aberdeen</postTown><Id>Test Id</Id><title>Test Id</title><firstName>Test</firstName><lastName>Test</lastName><jobTitle>Test</jobTitle><primaryRole>Test</primaryRole><telephoneNumber>Test</telephoneNumber><postCode>AB10 1XE</postCode><buildingName>Rubislaw House</buildingName><BuildingNumber>HIG123</BuildingNumber><ThoroughfareName>Test</ThoroughfareName><Locality>Wall Street</Locality><county>Aberdeenshire</county><createdBy>CMPS</createdBy><createdOn>2014-06-24T12:11:23.000+01:00</createdOn><lastModifiedBy>ROBTESB</lastModifiedBy><lastModifiedOn>2014-12-08T06:51:36.000Z</lastModifiedOn></address><country><name>United Kingdom</name></country>M/location></address></addressDetail></addressLst></AddressResponse>');
        res.setStatusCode(200);
        return res;
       
    }
} 

//<?xml version="1.0" encoding="UTF-8" standalone="yes"?><AddressResponse xmlns="http://capabilities.nat.bt.com/xsd/crm/v4/Addresses"><addressLst><addressDetails><addressIdentifier><value>X05000276372</value></addressIdentifier><addressDetail><address><addressName>X05000276372</addressName><postTown>Aberdeen</postTown><postCode>AB10 1XE</postCode><buildingName>Rubislaw House</buildingName><county>Aberdeenshire</county><createdBy>CMPS</createdBy><createdOn>2014-06-24T12:11:23.000+01:00</createdOn><lastModifiedBy>ROBTESB</lastModifiedBy><lastModifiedOn>2014-12-08T06:51:36.000Z</lastModifiedOn></address><country><name>United Kingdom</name></country></addressDetail></addressDetails></addressLst></AddressResponse>