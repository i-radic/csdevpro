global class CS_CustomSpendCapsLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){
        return '["Spend Cap Lookup Id"]';
    }

    public override Object[] doLookupSearch(Map<String, String>
			searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset,
			Integer pageLimit){

    	final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
		final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
		Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
			
		String product = '';
		
		if(searchFields.get('Spend Cap Lookup Id') != null)
		    product = searchFields.get('Spend Cap Lookup Id');
		
		system.debug(LoggingLevel.INFO, 'CS_CustomSpendCapsLookup.product = ' + product);
		system.debug(LoggingLevel.INFO, 'CS_CustomSpendCapsLookup.searchFields = ' + searchFields);
		
	    List<Spend_Cap__c> spendCapList = new List<Spend_Cap__c>();
        if(product != '')
    		spendCapList = [
    		    SELECT 	Id,
    					Name,
    					Value__c,
    					Commercial_Product__c,
    					Billing_Code__c,
    					Add_On__c
    			FROM	Spend_Cap__c
    			WHERE	Add_On__c = :product OR Commercial_Product__c = :product
    			ORDER BY Value__c ASC
    			LIMIT 	:SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT 
    			OFFSET 	:recordOffset
    		];

		return spendCapList;
    }
}