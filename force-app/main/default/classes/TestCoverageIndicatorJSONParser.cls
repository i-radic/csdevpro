@isTest
public class TestCoverageIndicatorJSONParser{
      static testMethod void testparse() {
         
         String json = '{\"links\" : [{\"rel\" : \"self\",\"href\" : \"/rest/v1/coverage/locations/ls101lj\",\"method\"'+
          ' : \"get\"}],\"locations\": [{\"title\" : \"LS10 1LJ\",\"lat\" : \"53.789864\",\"lng\" : \"-1.5338647\",\"links\" '+
          ': [{\"rel\" : \"self\",\"href\" : \"/rest/v1/coverage/locations/ls101lj\",\"method\" : \"get\"}], \"coverage\" : '+
          '[ {\"type\" : \"2G\", \"strength\" : \"5\"}, {\"type\" : \"3G\", \"strength\" : \"5\"}, {\"type\" : \"4G\",'+
          ' \"strength\" : \"4\", \"comingsoon\" : \"false\"}, {\"type\" : \"ee_single_flat\", \"strength\" : \"0\"}]}],'+
          '\"lastUpdated\": \"17 March 2014 12:00 AM\"}';
            String jsonV2='{\"links\" : [{\"rel\" : \"self\",\"href\" : \"/rest/v2/coverage/locations/w24df\",'+
           '\"method\" : \"get\"}],\"locations\": [{\"title\" : \"W24DF\",\"lat\" : \"51.514603\",\"lng\" :'+
           ' \"-0.18885851\",\"links\" : [{\"rel\" : \"self\",\"href\" : \"/rest/v2/coverage/locations/w24df\",'+
           '\"method\" : \"get\"}], \"coverage\" : [ {\"type\" : \"2G\", \"strength\" : \"5\"}, {\"type\" : \"3G\",'+
           ' \"strength\" : \"5\"}, {\"type\" : \"4G800\", \"strength\" : \"0\"}, {\"type\" : \"4G1800\", \"strength\"'+
           ' : \"5\"}, {\"type\" : \"4G2600\", \"strength\" : \"3\"}, {\"type\" : \"4GPlus\", \"strength\" : \"3\"}, '+
           '{\"is4GHighSpeed\" : \"true\"}, {\"type\" : \"ee_single_flat\", \"strength\" : \"0\"}]}],\"lastUpdated\": '+
           '\"16 February 2016 12:00 AM\"}'; 
          CoverageIndicatorJSONParser  obj = CoverageIndicatorJSONParser.parse(jsonV2);
          List<CoverageIndicatorJSONParser.Coverage> covList = obj.locations[0].coverage;
          CoverageIndicatorJSONParser.Coverage cov = new CoverageIndicatorJSONParser.Coverage();
          CoverageIndicatorJSONParser.Links links = new CoverageIndicatorJSONParser.Links();
          CoverageIndicatorJSONParser.Locations loc= new CoverageIndicatorJSONParser.Locations ();
          CoverageIndicatorJSONParser  ccJSONParser   = new CoverageIndicatorJSONParser ();
          ccJSONParser.lastUpdated='';
          
          //CoverageIndicatorJSONParser .Locations loc = obj.locations[0];
          System.assertEquals(1,  obj.locations.size());                  
          System.assertEquals(8,  covList.size());
          System.assertEquals('2G',  covList[0].Type);
          //obj.Coverage = obj.Coverage();
          System.assert(obj != null); 
      
      
      
      }




}