@isTest(SeeAllData=true)
// See all data = true is enables as you cannot create, update or delete content libraries via API
private class ProjectTriggerUtilities_Test {

    /**
     * Without this custom setting existing and being populated the whole process of project provisioning will fail. Hence this unit test
     * relies on pre-exisiting data. This is an exception made for Custom Settings
    */
    static testMethod void customSettingExistsAndIsPopulatedTest() {
        // As you cannot create libraries this unit test assumed the libraries and the custom setting are in existance
        // Whilst this is generally not considered good practice, in this instance it would be more code to work around
        // the platform issues than used in the solution.
        CPM_Documentation_Settings__c cpmDS = [SELECT Document_Workspace_Id__c, Document_Template_Workspace_Id__c
                                               FROM CPM_Documentation_Settings__c][0];
        
        System.Assert(cpmDS.Document_Template_Workspace_Id__c != null);
        System.Assert(cpmDS.Document_Workspace_Id__c != null);
    }
    /*
    static testMethod void createProjectTemplatesTest() {
        //TODO: TEST FOR NO RESULTS
        CPM_Documentation_Settings__c cpmDS = [SELECT Document_Workspace_Id__c, Document_Template_Workspace_Id__c
                                               FROM CPM_Documentation_Settings__c][0];
        
        Integer numberOfDocumentsInTemplatesLibraryStart = [SELECT COUNT() 
                                                            FROM ContentWorkspaceDoc 
                                                            WHERE ContentWorkspaceId = :cpmDS.Document_Template_Workspace_Id__c];
                                                            
        Integer numberOfDocumentsInProjectsLibraryStart =  [SELECT COUNT() 
                                                            FROM ContentWorkspaceDoc 
                                                            WHERE ContentWorkspaceId = :cpmDS.Document_Workspace_Id__c];
        
        
        if(numberOfDocumentsInTemplatesLibraryStart == 0) {
            // Create a new document
            ContentVersion cv = new ContentVersion();
            
            cv.title = 'Foo.txt';
            cv.PathOnClient = 'foo.txt';
            cv.versionData = Blob.valueOf('sklefhwfj;asjf;awjf;ajf;askljf;slakjdf;slajkdf;alsglaghasdrioj');
            cv.FirstPublishLocationId = cpmDS.Document_Template_Workspace_Id__c;
            
            insert cv;
            
            numberOfDocumentsInTemplatesLibraryStart = 1;
        }
        
        //Get stage name
           String stageName = [SELECT MasterLabel
           FROM OpportunityStage
           WHERE IsClosed = FALSE
           LIMIT 1].MasterLabel;
       
        Opportunity opp = new Opportunity(name='unit test opp', 
                                          StageName = stageName, 
                                          CloseDate = Date.today(),
                                          //NextStep = 'test',
                                          //Next_Action_Date__c = Date.today(),
                                          Incumbent__c = 'Other',
                                          Customer_Status__c = 'New to Orange',
                                          Competitors__c = 'O2');
        insert opp;
        
        Project__c p = new Project__c();
        p.Project_Name__c = 'Unit Test Project';
        p.Opportunity__c = opp.Id;
        insert p;
        
        Integer numberOfDocumentsInTemplatesLibraryEnd =   [SELECT COUNT() 
                                                            FROM ContentWorkspaceDoc 
                                                            WHERE ContentWorkspaceId = :cpmDS.Document_Template_Workspace_Id__c];
                
        Integer numberOfDocumentsInProjectsLibraryEnd =    [SELECT COUNT() 
                                                            FROM ContentWorkspaceDoc 
                                                            WHERE ContentWorkspaceId = :cpmDS.Document_Workspace_Id__c];
        System.Assert(numberOfDocumentsInTemplatesLibraryStart == numberOfDocumentsInTemplatesLibraryEnd);
        System.Assert(numberOfDocumentsInProjectsLibraryEnd > numberOfDocumentsInProjectsLibraryStart);
        List<ContentVersion> cvs = [SELECT Title, Project__c FROM ContentVersion WHERE Project__c = :p.Id];
        System.assert(cvs.size() > 0);
        System.assert(cvs[0].Title.startsWith(p.Project_Name__c));
        
    }    
    */
}