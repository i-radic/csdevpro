@IsTest
public class CS_ConfiguratorSubscriberTest  {
	private static cscfga__Product_Basket__c basket;
	private static cscfga__Product_Configuration__c pc;
	private static Map<Id, cscfga__Product_Configuration__c> configsMap;
	private static Map<Id, cscfga__Attribute_Definition__c> attDefsMap;
	private static List<cscfga__Product_Configuration__c> pcsList;
	private static Map<Id, cscfga__Product_Definition__c> prodDefMap;

	private static Map<String, Object> payload;

	private static void createTestData() {
		CS_TestDataFactory.insertTriggerDeactivatingSetting();
		CS_TestDataFactory.setupNoTriggersFlag();

        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
                
		Account acc = CS_TestDataFactory.generateAccount(true, 'Test Account');
		Opportunity opp = CS_TestDataFactory.generateOpportunity(true, 'Test Opp', acc);
		basket =  CS_TestDataFactory.generateProductBasket(true, 'Test Basket', opp);

		cscfga__Product_Category__c prodCategory = CS_TestDataFactory.generateProductCategory(true, 'EE SME');

        cscfga__Product_Definition__c pd = CS_TestDataFactory.generateProductDefinition(false, 'Test Prod Def');
		pd.cscfga__Product_Category__c = prodCategory.Id;
		INSERT pd;

		List<String> attDefNames = new List<String>{
            'Data_Costs',
            'Data_Costs_Future',
            'Data_Revenue',
            'Data_Revenue_Future',
            'Rate_Card_Line',
            'Usage_Profile',
            'Voice_Costs',
            'Voice_Costs_Future',
            'Voice_Revenue',
            'Voice_Revenue_Future'};

		List<cscfga__Attribute_Definition__c> attDefList = CS_TestDataFactory.generateAttributeDefinitions(true, attDefNames, pd);

        pc = CS_TestDataFactory.generateProductConfiguration(false, 'Test PC', basket);
        pc.cscfga__Product_Definition__c = pd.ID;
		pc.Tenure__c = 5;
		pc.cscfga__Configuration_Status__c = 'Valid';
        pc.Calculations_Product_Group__c = 'Future Mobile';
        INSERT pc;

		List<cscfga__Attribute__c> attsList = CS_TestDataFactory.generateAttributesForConfiguration(true, attDefList, pc);

        pcsList = new List<cscfga__Product_Configuration__c>{pc, pc, pc};
        configsMap = new Map<Id, cscfga__Product_Configuration__c>();
		attDefsMap = new Map<Id, cscfga__Attribute_Definition__c>();
		prodDefMap = new Map<Id, cscfga__Product_Definition__c>{pd.Id => pd};

        for(cscfga__Product_Configuration__c p : pcsList){
        	configsMap.put(p.Id, p);
        }

		for(cscfga__Attribute_Definition__c attDef : attDefList) {
			attDefsMap.put(attDef.Id, attDef);
		}

		payload = new Map<String, Object>();
		payload.put('AllAttrs', attsList);
		payload.put('AllConfigs', new List<cscfga__Product_Configuration__c>{pc});
		payload.put('AttributeDefinitionsMap', attDefsMap);
		payload.put('RootConfig', pc);
		payload.put('Container', basket);
		payload.put('RootConfig', pc);
		payload.put('Configs', new List<cscfga__Product_Configuration__c>{pc});


        CS_TestDataFactory.generatePLReportConfigRecords();
    	List<cscfga__Attribute_Definition__c> atrDefs = CS_TestDataFactory.generateAttributeDefinitions(true, new List<String>{'Test PD'}, pd);
    	Map<Id, cscfga__Attribute_Definition__c> atrMap = new Map<Id, cscfga__Attribute_Definition__c>();
    	for(cscfga__Attribute_Definition__c ad : atrDefs){
			atrMap.put(ad.Id, ad);
    	}

    	List<cscfga__Attribute__c> atrsList = CS_TestDataFactory.generateAttributesForConfiguration(true, atrDefs, pcsList[0]);
	}

	@IsTest
	public static void testSubscribersController(){
		createTestData();

        Test.startTest();

        CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
		
		Map<Id, cscfga__Product_Definition__c> prodDefs = controller.getProductDefinitions(pcsList);
		System.assertNotEquals(prodDefs, null);

	    Map<Id, cscfga__Product_Configuration__c> cfgMap = controller.getOldProductConfigurations(configsMap);
		System.assertNotEquals(cfgMap, null);

		try{
			controller.createPCTotals(configsMap);
	    }
	    catch(Exception exc){}

	    try{
	    	controller.updateProductConfigsInBasket(basket, configsMap);
	    }
	    catch(Exception exc){}

	    try{
			controller.updateProductBasket(configsMap, basket);	
	    }
	    catch(Exception exc){}

	    try{
			controller.updateCalculationProductGroup(payload, new Map<Id, cscfga__Product_Definition__c>());
	    }
	    catch(Exception exc){}
	    
	    try{
	        controller.updateProductConfigsInBasket(basket, configsMap);
	    }
	    catch(Exception exc){}
	    
	    try{
	        controller.updateProductBasket(configsMap, basket);
	    }
	    catch(Exception exc){}
	    
	    try{
	        controller.createPCTotals(configsMap);
	    }
	    catch(Exception exc){}
	    
	     try{
	         Map<Id, cscfga__Product_Configuration__c>  mapa = controller.getCurrentConfigs(configsMap, configsMap.keySet());
	    }
	    catch(Exception exc){}
	    
	    cfgMap = controller.getCurrentConfigs(configsMap, configsMap.keySet());
		System.assertNotEquals(cfgMap, null);

		controller.updateUsageProfileCalcs(payload, prodDefMap);

		controller.updateConfigs(payload);
		
		controller.updateProductDefinitionName(payload, prodDefMap);

        Test.stopTest();
	}

	@IsTest
	public static void testOnMessageAfterSaveV2() {
		createTestData();

		Test.startTest();
		pc.Calculations_Product_Group__c = 'BT Net';
		UPDATE pc;
		CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
		try{
			controller.onMessage('Test Topic', 'AfterSaveV2', payload);
		}
		catch(Exception exc){}
		
		try{
	        controller.updateProductConfigsInBasket(basket, configsMap);
	    }
	    catch(Exception exc){}
	    
	    try{
	        controller.updateProductBasket(configsMap, basket);
	    }
	    catch(Exception exc){}
	    
	    try{
	        controller.createPCTotals(configsMap);
	    }
	    catch(Exception exc){}
	    
	     try{
	         Map<Id, cscfga__Product_Configuration__c>  mapa = controller.getCurrentConfigs(configsMap, configsMap.keySet());
	    }
	    catch(Exception exc){}
		
		Test.stopTest();

		Product_Configuration_Totals__c pcTotal = [SELECT Id, Name, Product_Configuration__c From Product_Configuration_Totals__c WHERE Product_Configuration__c = :pc.Id];
		System.assertNotEquals(null, pcTotal);
	}

    @IsTest
	private static void testOnMessage2AfterSaveV2() {
		createTestData();

		Test.startTest();
		pc.Calculations_Product_Group__c = 'Cloud Voice';
		UPDATE pc;
		CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
		try{
			controller.onMessage('Test Topic', 'AfterSaveV2', payload);
		}
		catch(Exception exc){}
		
		try{
	        controller.updateProductConfigsInBasket(basket, configsMap);
	    }
	    catch(Exception exc){}
	    
	    try{
	        controller.updateProductBasket(configsMap, basket);
	    }
	    catch(Exception exc){}
	    
	    try{
	        controller.createPCTotals(configsMap);
	    }
	    catch(Exception exc){}
	    
	     try{
	         Map<Id, cscfga__Product_Configuration__c>  mapa = controller.getCurrentConfigs(configsMap, configsMap.keySet());
	    }
	    catch(Exception exc){}
		
		Test.stopTest();

		Product_Configuration_Totals__c pcTotal = [SELECT Id, Name, Product_Configuration__c From Product_Configuration_Totals__c WHERE Product_Configuration__c = :pc.Id];
		System.assertNotEquals(null, pcTotal);
	}

	@IsTest
	public static void testOnMessageBeforeSaveV2() {
		createTestData();

		Test.startTest();
		CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
		controller.onMessage('Test Topic', 'BeforeSaveV2', payload);
		Test.stopTest();

		List<cscfga__Product_Configuration__c> pcListWithCalcProdGroup = [SELECT Id, Name, Calculations_Product_Group__c FROM cscfga__Product_Configuration__c WHERE Id = :pc.Id];
		System.assertNotEquals(null, pcListWithCalcProdGroup);
	}
	
	@IsTest
	public static void testOnMessage2BeforeSaveV2() {
		createTestData();
		cscfga__Product_Category__c prodCategory = [SELECT Id, Name FROM cscfga__Product_Category__c LIMIT 1];
		prodCategory.Name = 'Cloud Voice';
		UPDATE prodCategory;
		
		Test.startTest();
		CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
		controller.onMessage('Test Topic', 'BeforeSaveV2', payload);
		Test.stopTest();

		List<cscfga__Product_Configuration__c> pcListWithCalcProdGroup = [SELECT Id, Name, Calculations_Product_Group__c FROM cscfga__Product_Configuration__c WHERE Id = :pc.Id];
		System.assertNotEquals(null, pcListWithCalcProdGroup);
	}

	@IsTest
	public static void testOnMessageDeleted() {
		createTestData();
		Test.startTest();
		System.assertEquals(false, basket.Implementation_Form_Valid__c);
		CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
		try{
			controller.onMessage('Test Topic', 'Deleted', payload);
		}
		catch(Exception exc){}
		Test.stopTest();
	}

	@IsTest
	public static void testGetOldProductConfigurations() {
		createTestData();

		Test.startTest();
		CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
		Map<Id, cscfga__Product_Configuration__c> returnedMap = controller.getOldProductConfigurations(configsMap);
		Test.stopTest();

		System.assertNotEquals(null, returnedMap);
	}

	@IsTest
	public static void testExecuteActions(){
		createTestData();

		Test.startTest();
		
		CS_ConfiguratorSubscriber controller = new CS_ConfiguratorSubscriber();
    	controller.executeActions(new Map<Id, cscfga__Product_Configuration__c>{pc.Id => pc});

		Test.stopTest();
	}
}