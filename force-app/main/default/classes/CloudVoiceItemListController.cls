public class CloudVoiceItemListController {
    public List<cscfga__Product_Configuration__c> cfgs {get;set;}
    public List<ObjectWrapper> wrappedCfgs {get;set;}
    Public Decimal availableToFinance {get;set;}
    Public Decimal selectedForFinance {get;set;}
    Public Boolean financeDeal {get;set;}
    Public Boolean selectAllProducts {get;set;}
    Public cscfga__Product_Basket__c basketToUpdate {get;set;}
    Public Id bsktId { get; set; }
	public Id opptyId { get; set; }
	public String financeAttachment { get; set; }
	public String financeAttachmentDateTime { get; set; }

    public class ObjectWrapper {
        public ObjectWrapper(boolean fo,string ooc,string pn,string q,string m,string g,string sn,string cid){
            this.financeOption=fo;
            this.oneOffCharge=ooc;
            this.productName=pn;
			this.quantity=q;
			this.module=m;
			this.grouping=g;
            this.siteName=sn;
            this.configId=cid;
        }
            public boolean financeOption {get;set;}
            public string oneOffCharge {get;set;}
            public string productName {get;set;}
			public string quantity {get;set;}
			public string module {get;set;}
			public string grouping {get;set;}
            public string siteName {get;set;}
            public Id configId {get;set;}
    }
    
    public CloudVoiceItemListController() {
        bsktId=ApexPages.currentPage().getParameters().get('basketId');
        basketToUpdate=[select id,DepositNRC__c,Finance_Provider__c,repaymentRC__c,RepaymentDuration__c,Total_Financed__c from cscfga__Product_Basket__c where id=:bsktId limit 1];
        system.debug(basketToUpdate);
        wrappedCfgs=new List<ObjectWrapper>();
        availableToFinance=0;
        financeDeal=True;
        selectAllProducts=True;
        Set<String> attributeNames = new Set<String>{'Finance Option','One Off Charge'};

        Set<Id> configIds = new Set<Id>();
        Set<Id> deffIds = new Set<Id>();
        
        Map<Id,Boolean> financedMap = new Map<Id,Boolean>();
        Map<Id,String> financePossibilityMap = new Map<Id,String>();
        
        Map<Id,String> oneOffChargeMap = new Map<Id,String>();
        Map<Id,String> productNameMap = new Map<Id,String>();
		Map<Id,String> quantityMap = new Map<Id,String>();
		Map<Id,String> moduleMap = new Map<Id,String>();
		Map<Id,String> groupingMap = new Map<Id,String>();
        Map<Id,String> siteNameMap = new Map<Id,String>();

		getFinanceAttachment();
                
        cfgs=[select id,name,Financed__c,cscfga__Description__c,cscfga__Quantity__c,Module__c,Grouping__c,Site_Name__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c=:bsktId];
        for (cscfga__Product_Configuration__c cfg : cfgs) {
             configIds.add(cfg.id);
             financedMap.put(cfg.id,cfg.Financed__c);
             
             productNameMap.put(cfg.Id,cfg.cscfga__Description__c);
             quantityMap.put(cfg.Id,String.valueOf(cfg.cscfga__Quantity__c));
             moduleMap.put(cfg.Id,cfg.Module__c);
             groupingMap.put(cfg.Id,cfg.Grouping__c);
             siteNameMap.put(cfg.Id,cfg.Site_Name__c);
        }
        
        List<cscfga__Attribute__c> attributes = [select id,name,cscfga__Value__c,cscfga__Product_Configuration__c from cscfga__Attribute__c where name in :attributeNames and cscfga__Product_Configuration__c in :configIds]; 
        for (cscfga__Attribute__c att : attributes) {
            if (att.name=='Finance Option') financePossibilityMap.put(att.cscfga__Product_Configuration__c,att.cscfga__Value__c);
            if (att.name=='One Off Charge') oneOffChargeMap.put(att.cscfga__Product_Configuration__c,att.cscfga__Value__c);
        }
        
        
        for (cscfga__Product_Configuration__c cfg : cfgs) {
            system.debug(cfg.name);
            if (financePossibilityMap.get(cfg.id)=='Yes') { 
                wrappedCfgs.add(new ObjectWrapper(financedMap.get(cfg.id),oneOffChargeMap.get(cfg.id),productNameMap.get(cfg.id),quantityMap.get(cfg.id),moduleMap.get(cfg.id),groupingMap.get(cfg.id),siteNameMap.get(cfg.id),cfg.id));
                availableToFinance+=decimal.valueOf(oneOffChargeMap.get(cfg.id));
            }
        }
        recalculateFinances();
    }
    
    public void recalculateFinances() {
        system.debug(wrappedCfgs);
        selectedForFinance=0;
        for (ObjectWrapper onew : wrappedCfgs) {
            if (onew.financeOption==true) {
                selectedForFinance+=decimal.valueOf(onew.oneOffCharge);
            }
        }
    }

	public PageReference getFinanceAttachment() {
		cscfga__Product_Basket__c basket = [select id, cscfga__Opportunity__c from cscfga__Product_Basket__c where id = :bsktId];
		Opportunity opportunity = [select Id from Opportunity where Id = :basket.cscfga__Opportunity__c];
		opptyId = opportunity.Id;

		List<Attachment> attachmentList = [SELECT ParentId,Name,Description,Id,ContentType,BodyLength,LastModifiedDate FROM Attachment WHERE ParentId =: opportunity.Id AND Description = 'Cloud Voice Finance Agreement' order by LastModifiedDate desc]; 
		if(attachmentList.Size() < 1) {
			financeAttachment = 'No File Attached';
		}
		else{
			financeAttachment = attachmentList[0].Name;
			financeAttachmentDateTime = attachmentList[0].LastModifiedDate.format('DD/MM/YY HH:MM:ss');
		}
		return null;
	}
   
    public void SaveResults() {
        system.debug(wrappedCfgs);

        List<cscfga__Product_Configuration__c> cfgsToUpdate = new List<cscfga__Product_Configuration__c>();
        for(ObjectWrapper oneWrap : wrappedCfgs) {
            cfgsToUpdate.add(New cscfga__Product_Configuration__c(id=oneWrap.configId,Financed__c=Boolean.valueOf(oneWrap.financeOption)) );
        }
        system.debug(cfgsToUpdate);
        if (cfgsToUpdate.size()>0) {
            update cfgsToUpdate;
        }
        basketToUpdate.Total_Financed__c = selectedForFinance;
        system.debug(basketToUpdate);
        update basketToUpdate;
    }
    
}