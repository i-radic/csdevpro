@isTest
private class Test_BSClassicSLProductsCtrl {
static testMethod void MyUnitTest() {
    User thisUser = [select id from User where id=:userinfo.getUserid()];
     System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
     }
    Account a = Test_Factory.CreateAccount();
    a.Name = 'Test';
    a.ownerId = thisuser.id ;
    a.Base_Team_Assign_Date__c=System.today();
    insert a;
    
    BS_Classic_Sales_log__c bl=new BS_Classic_Sales_log__c();
    bl.Account__c=a.Id;
    bl.Order_Number__c='test12345678';
    bl.Work_Area__c='Blend';
    bl.Call_Type__c='test1234';
    insert bl;
    
    BS_Classic_SL_Product__c bp=new BS_Classic_SL_Product__c();
    bp.Product_Type2__c='1 year PSTN';
    //bp.Product__c='Acquisition Lines';
    //bp.Quantity__c=1;
    bp.Sales_Log__c=bl.Id;
    insert bp;
    
    BS_Classic_SL_Product__c bp1=new BS_Classic_SL_Product__c();
    bp1.Product_Type2__c='1 year PSTN';
    bp1.Product__c='Acquisition Lines';
    bp1.Quantity__c=1;
    bp1.Sales_Log__c=bl.Id;

    
     ApexPages.currentPage().getParameters().put('ID',bp1.Id);
     ApexPages.StandardController stdcon = new ApexPages.StandardController(bp1);
     BSClassicSLProductsCtrl ctr=new BSClassicSLProductsCtrl(stdcon);
     //BS_Classic_SL_Product__c aprod=ctr.newProd;
     List<BS_Classic_SL_Product__c> aprodlist=ctr.getProdList();
     ctr.newProd=bp1;
     PageReference pr=ctr.addProd();
     
     BS_Classic_SL_Product__c bp2=new BS_Classic_SL_Product__c();
     //bp2.Product_Type2__c='1 year PSTN';
     //bp2.Product__c='Acquisition Lines';
     //bp2.Quantity__c=1;
     //bp2.Sales_Log__c=bl.Id;

    
     ApexPages.currentPage().getParameters().put('ID',bp2.Id);
     ApexPages.StandardController stdcon1 = new ApexPages.StandardController(bp2);
     BSClassicSLProductsCtrl ctr1=new BSClassicSLProductsCtrl(stdcon1);
     //BS_Classic_SL_Product__c aprod=ctr.newProd;
     List<BS_Classic_SL_Product__c> aprodlist1=ctr1.getProdList();
     //ctr.newProd=bp1;
     PageReference pr1=ctr1.addProd();
     
     
     BS_Classic_SL_Product__c bp3=new BS_Classic_SL_Product__c();
     bp3.Product_Type2__c='1 year PSTN';
     //bp3.Product__c='Acquisition Lines';
     //bp3.Quantity__c=1;
     //bp3.Sales_Log__c=bl.Id;

    
     ApexPages.currentPage().getParameters().put('ID',bp3.Id);
     ApexPages.StandardController stdcon2 = new ApexPages.StandardController(bp3);
     BSClassicSLProductsCtrl ctr2=new BSClassicSLProductsCtrl(stdcon2);
     //BS_Classic_SL_Product__c aprod=ctr.newProd;
     List<BS_Classic_SL_Product__c> aprodlist2=ctr2.getProdList();
     ctr2.newProd=bp3;
     PageReference pr2=ctr2.addProd();
     
     
     BS_Classic_SL_Product__c bp4=new BS_Classic_SL_Product__c();
     bp4.Product_Type2__c='1 year PSTN';
     bp4.Product__c='Acquisition Lines';
     //bp4.Quantity__c=1;
     //bp4.Sales_Log__c=bl.Id;

    
     ApexPages.currentPage().getParameters().put('ID',bp4.Id);
     ApexPages.StandardController stdcon3 = new ApexPages.StandardController(bp4);
     BSClassicSLProductsCtrl ctr3=new BSClassicSLProductsCtrl(stdcon3);
     //BS_Classic_SL_Product__c aprod=ctr.newProd;
     List<BS_Classic_SL_Product__c> aprodlist3=ctr3.getProdList();
     ctr3.newProd=bp4;
     PageReference pr3=ctr3.addProd();
    
    
    
}
}