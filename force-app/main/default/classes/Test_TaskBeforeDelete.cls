@isTest
private class Test_TaskBeforeDelete {
    
    static Task t1, t2;
    static User uUser, uUser1;
    
    static{
        Campaign c;
        c = new Campaign(Name='TestCampaign1', Status='In Progress X', Description='Some description X', X_Day_Rule__c = 3, isActive=true, Data_Source__c='Self Generated');
        insert c;
        Contact contact = Test_Factory.CreateContact();
        insert contact;
        RecordType recordtypeid_corp_task  = [select id from RecordType where SobjectType='Task' and name ='Corporate Campaign Task' limit 1];
        
        Profile pProfile = [select id from profile where name='System Administrator'];
        uUser = new User(alias = 'tst2', email='tst2cr@bt.com', 
        emailencodingkey='UTF-8', lastname='Testing ADMIN', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = pProfile.Id, timezonesidkey='Europe/London', username='tst2cr@bt.com',
        EIN__c='tst2cr');
        insert uUser;
        
        Profile pProfile1 = [select id from profile where name='Field: Standard User'];
        uUser1 = new User(alias = 'tst3', email='tst3cr@bt.com', 
        emailencodingkey='UTF-8', lastname='Testing NORM', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = pProfile1.Id, timezonesidkey='Europe/London', username='tst3cr@bt.com',
        EIN__c='tst3cr');
        insert uUser1;
        
        t1 = new Task(RecordTypeId = recordtypeid_corp_task.id, Status = 'Not Started', WhatId=c.Id, WhoId=contact.Id, Type='Call', ownerId = uUser.Id);
        t2 = new Task(RecordTypeId = recordtypeid_corp_task.id, Status = 'Not Started', WhatId=c.Id, WhoId=contact.Id, Type='Call',ownerId = uUser1.Id);
        insert t1;
        insert t2;
    }

    static testMethod void okToDelete() {
        System.runAs(uUser) {
            Task tt = [select id, OkToDelete__c, RecordType.Name from Task where Id =:t1.Id ];
            delete t1;
            List<Task> tasks = [select id from Task where Id = :t1.Id];
            System.assertEquals(0, tasks.size());
        }
    }
    
    static testMethod void notOkToDelete(){
        System.runAs(uUser1) {
            //this user should not be able to delete the task with this particular record type
            Task tt = [select id, OkToDelete__c, RecordType.Name from Task where Id =:t2.Id ];
            try{
                delete t2;
            }catch (Exception e){
                System.assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION, You do not have an appropriate profile to delete this record, please contact a System Administrator!' ));
            }
            List<Task> tasks = [select id from Task where Id = :t2.Id];
            System.assertEquals(1 , tasks.size());
        }
    }
    
  //Book2Bill task deletion validation  
  
   Static User B2BUser;
      static Task B2BT;
    static testMethod void notB2BTasks(){

    Profile B2BProfile1 = [select id from profile where name='Field: Standard User'];
        B2BUser= new User(alias = 'B2B', email='B2B3cr@bt.com', 
        emailencodingkey='UTF-8', lastname='Testing B2B', languagelocalekey='en_US', 
        localesidkey='en_US', profileid = B2BProfile1.Id, timezonesidkey='Europe/London', username='B2BProfile13cr@bt.com',
        EIN__c='B2B3cr');
        insert B2BUser;


    Opportunity o=new Opportunity();
    o.Name='Book2Bill';
    o.StageName='Created';
    o.ownerid=B2BUser.id;
    o.CloseDate = system.today();
    insert o;

    BookToBill__c b=new BookToBill__c();
    b.Progress_Comments__c='test1';
    b.Opportunity__c=o.Id;
    b.RecordTypeId = [Select Id from RecordType where DeveloperName='Revenue_Assurance_ICT'].Id;
    insert b;
    
    RecordType recordtypeid_RA_task  = [select id from RecordType where SobjectType='Task' and name ='BookToBill RA SS' limit 1];
   
    B2BT= new Task(RecordTypeId = recordtypeid_RA_task.id, Status = 'Not Started', WhatId=b.Id, Type='Call', ownerId = B2BUser.id);
   
     

    }
}