/*
###########################################################################
# File..................: TrialHelperTest.cls
# Version...............: 1
# Created by............: Martin Eley
# Created Date..........: 25th September 2012
# Last Modified by......: Varun Pokhriyal
# Last Modified Date....: 4th April 2013
# Description...........: Test Class                         
# VF page...............:             
# Change Log:               
# Apex Class............: TrialHelper.cls
# Apex Trigger....... ..: TrialTrigger.cls
# Copyright (c) 2000-2012. Astadia, Inc. All Rights Reserved.
#
# Created by the Astadia, Inc. Modification must retain the above copyright notice.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any commercial purpose, without fee, and without a written
# agreement from Astadia, Inc., is hereby forbidden. Any modification to source
# code, must include this paragraph and copyright.
#   
# Permission is not granted to anyone to use this software for commercial uses.
#
# Contact address: 2839 Paces Ferry Road, Suite 350, Atlanta, GA 30339
# Company URL : http://www.astadia.com
#
###########################################################################
*/
 @IsTest
(SeeAllData=True)
public with sharing class TrialHelperTest {
    
     static Account acc1 = null;
     static Contact contact1 = null;
     static Opportunity opp1 = null;
     static Trial__c aTrial = null;
     
     static SKU__c aTabletSKU = null;
     static Stock_Item__c aTabletSKUStockItem1 = null;
     static Stock_Item__c aTabletSKUStockItem2 = null;
     static Trial_Line_Item__c aTabletTrialLineItem = null;
     static Trial_Stock_Item__c aTableTrialStockItem1 = null;
     
     static SKU__c aNonTabletSKU = null;
     static Stock_Item__c aNonTabletSKUStockItem1 = null;
     static Stock_Item__c aNonTabletSKUStockItem2 = null;
     static Trial_Line_Item__c aNonTabletTrialLineItem = null;
     static Trial_Stock_Item__c aNonTableTrialStockItem1 = null;
     
     static echosign_dev1__SIGN_Agreement__c aAgreement = null;
     static List<SKU__c> skuLst = null;
     
     private static void prepareTestData(){
         
         List<Stock_Item__c> StockItems= new List<Stock_Item__c>();
         skuLst = new List<SKU__c>();
         
         acc1 = Test_Utils.createAccount('Test Account');
         contact1 = Test_Utils.createContact(null, null, acc1.Id, null);
         opp1 = Test_Utils.createOpp('Test Opportunity', 'Test Stage', Date.today() + 28, acc1.Id);
         aTrial =  Test_Utils.createTrial(opp1, contact1);
         
         aTabletSKU = Test_Utils.populateSKU('Test Tablet SKU1', 'Test Tablet SKU1', 99.99 , true, 'Micro', 'Device');
         aNonTabletSKU = Test_Utils.populateSKU('Test Tablet SKU2', 'Test Tablet SKU2', 11.99 , false, 'Nano', 'Device');
         skuLst.add(aTabletSKU);
         skuLst.add(aNonTabletSKU);
         insert(skuLst);
          
         aTabletSKUStockItem1 = Test_Utils.populateStockItem(skuLst[0], 'Active', 'Tablet1','07050505050');
         aTabletSKUStockItem2 = Test_Utils.populateStockItem(skuLst[0], 'Active', 'Tablet2','07060606060');
         
         aNonTabletSKUStockItem1 = Test_Utils.populateStockItem(skuLst[1], 'Active', 'NonTablet1','07070707070');
         aNonTabletSKUStockItem2 = Test_Utils.populateStockItem(skuLst[1], 'Active', 'NonTablet2','07080808080');
         
         StockItems.add(aTabletSKUStockItem1);
         StockItems.add(aTabletSKUStockItem2);
         StockItems.add(aNonTabletSKUStockItem1);
         StockItems.add(aNonTabletSKUStockItem2);
         insert StockItems;
        
         aTabletTrialLineItem = Test_Utils.createTrialLineItem(aTrial.Id, aTabletSKU.Id, 1);
         aNonTabletTrialLineItem = Test_Utils.createTrialLineItem(aTrial.Id, aNonTabletSKU.Id, 1);
     }
     
     private static void prepareTrialStockItemTestData(){
        List<Trial_Stock_Item__c> trialStockItems= new List<Trial_Stock_Item__c>();
        
        aTableTrialStockItem1 = Test_Utils.populateTrialStockItem(aTrial, aTabletSKUStockItem1, 'Dispatched');
        aNonTableTrialStockItem1 = Test_Utils.populateTrialStockItem(aTrial, aNonTabletSKUStockItem1, 'Dispatched');
        
        trialStockItems.add(aTableTrialStockItem1);
        trialStockItems.add(aNonTableTrialStockItem1);
         
        insert trialStockItems;
     }
     
     
     static testMethod void testSentStatusNoAgreements(){
         Test.startTest();
         prepareTestData();
         
         //Set status of trial to "Signed" without having a signed agreement
        aTrial.Status__c = 'Out for Signature';
        try{
            update aTrial;
        } catch (Exception aException){
            //Assert
            System.assertEquals('Update failed. First exception on row 0 with id '
                    + aTrial.Id
                    + '; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, You cannot set the status to "Out for Signature".  There are no agreements out for Signature for this trial: []'
                ,aException.getMessage());
        }
        
        Test.stopTest();
     }
     
     
     static testMethod void testSentStatusWithAgreements(){
        
        Test.startTest();
        prepareTestData();
        
        //Echo Sign Agreement
        aAgreement = Test_Utils.createEchoSignAgreement(aTrial, 'Draft');

        aAgreement.echosign_dev1__Opportunity__c = opp1.Id;
        aAgreement.Name = 'EE Business Agreement Test';
        aAgreement.echosign_dev1__Status__c = 'Out for Signature';
        update aAgreement;

        aAgreement.echosign_dev1__Status__c = 'Signed';
        update aAgreement;

        //Set status of trial to "Signed" without having a signed agreement
        aTrial.Status__c = 'Out for Signature';
        try{
            update aTrial;
        } catch (Exception aException){
            //Assert
            System.assertEquals('Update failed. First exception on row 0 with id '
                    + aTrial.Id
                    + '; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, You cannot set the status to "Out for Signature".  There are no agreements out for Signature for this trial: []'
                ,aException.getMessage());
        }
        Test.stopTest();
    }
    
    static testMethod void testSentStatusWithSentAgreements(){
        
         Test.startTest();
         prepareTestData();
        
        //Echo Sign Agreement
         aAgreement = Test_Utils.createEchoSignAgreement(aTrial, 'Out for Signature');
        
        //Set status of trial to "Signed" with a signed agreement
        aTrial.Status__c = 'Out for Signature';
        update aTrial;
        
        aAgreement.echosign_dev1__Opportunity__c = opp1.Id;
        aAgreement.Name = 'Credit Check Test';
        aAgreement.echosign_dev1__Status__c = 'Pre-Send';
        update aAgreement;

        aAgreement.echosign_dev1__Status__c = 'Signed';
        update aAgreement;
        
        //Assert
        System.assertEquals('Out for Signature', aTrial.Status__c);
        Test.stoptest();        
    }       

    static testMethod void testSignedStatusNoAgreements(){
        
        Test.startTest();
        prepareTestData();
        try{
            update aTrial;
        } catch (Exception aException){
            //Assert
            System.assertEquals('Update failed. First exception on row 0 with id '
                    + aTrial.Id
                    + '; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, You cannot set the status to "Signed".  There are no signed agreements for this trial: []'
                ,aException.getMessage());
        }
        Test.stopTest();
    } 
    
    
    static testMethod void testSignedStatusWithAgreements(){
        
        Test.startTest();
        prepareTestData();
        
        //Echo Sign Agreement
         aAgreement = Test_Utils.createEchoSignAgreement(aTrial, 'Out for Signature');
        
        //Set status of trial to "Signed" without having a signed agreement
        aTrial.Status__c = 'Signed';
        aTrial.Brand__c = 'T-Mobile';
        try{
            update aTrial;
        } catch (Exception aException){
            //Assert
            /*
            System.assertEquals('Update failed. First exception on row 0 with id '
                    + aTrial.Id
                    + '; first error: FIELD_CUSTOM_VALIDATION_EXCEPTION, You cannot set the status to "Signed".  There are no signed agreements for this trial: []'
                ,aException.getMessage());
                */
            System.debug('Update failed. First exception on row 0 with id' + aException);   
        }
        Test.stopTest();
    }   
    
    
    static testMethod void testSignedStatusWithSignedAgreements(){
        
        Test.startTest();
        prepareTestData();
        prepareTrialStockItemTestData();
        
        try{
          //Echo Sign Agreement
          aAgreement = Test_Utils.createEchoSignAgreement(aTrial, 'Signed');    
        }
        catch (Exception aException){
            System.debug('Update failed. First exception on row 0 with id' + aException);
        }
        
        //Set status of trial to "Signed" with a signed agreement
        aTrial.Status__c = 'Signed';
        try{
           update aTrial;   
        }
        catch (Exception aException){
            System.debug('Update failed. First exception on row 0 with id' + aException);
        }
        
        //Assert
        System.assertEquals('Signed', aTrial.Status__c);
        Test.stopTest();
    }   
    
    static testMethod void testCalculateTrialLength(){
        Test.startTest();
        TrialHelper aTrialHelper = new TrialHelper();
        System.assertEquals(7 ,aTrialHelper.calculateTrialLength('1 Week'));
        System.assertEquals(14 ,aTrialHelper.calculateTrialLength('2 Weeks'));
        System.assertEquals(21 ,aTrialHelper.calculateTrialLength('3 Weeks'));
        System.assertEquals(28 ,aTrialHelper.calculateTrialLength('4 Weeks'));
        Test.stopTest();
    }
    
}