public with sharing class cntOpportunityNew {

	
	public cntOpportunityNew(ApexPages.StandardController stdController) {
        System.debug('cntOpportunityNew - In constructor:' + ApexPages.CurrentPage().getParameters());
        
    }
    
    public PageReference onStart() {
    	
    	System.debug('cntOpportunityNew - In onStart:');
    	String returl = ApexPages.CurrentPage().getParameters().get('returl');
    	String accid = ApexPages.CurrentPage().getParameters().get('accid');
    	String conid = ApexPages.CurrentPage().getParameters().get('conid');
    	    	
        String pgURL = '/006/e?opp11=Created&nooverride=1&returl=' + returl;
        if(accid != null)pgURL= pgURL + '&accid=' + accid;
        if(conid != null)pgURL= pgURL + '&conid=' + conid;
        
    	PageReference retPageRef = new Pagereference(pgURL);
    	
    	return retPageRef;
    }





}