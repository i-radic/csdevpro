global class DemoConvertController {
   
    webService static String generateDocument(Id docConfigId, Id objectId, String docType ) {
        Map<String, Object> inputMap = new Map<String, Object>();
        inputMap.put('recordId', objectId);
        csdg.DocGenerationWrapper result;
        if(docType == 'DOCX'){
            csdg.MetadataWrapper.targetTypeStatic = 'DOCX';
        }
        if(docConfigId != null) {
          result = csdg.DocumentDataHandler.generate(docConfigId, objectId, inputMap);
        } else {
            result = new csdg.DocGenerationWrapper();
        }
         
        return result.attachmentId;
        //return null;
    }
      
     
    @AuraEnabled
    global static String convertToPdfLightning(Id docConfigId, Id objectId) {
      return generateDocument(docConfigId, objectId, 'PDF');
    }
    @AuraEnabled
    global static String convertToDocxLightning(Id docConfigId, Id objectId) {
      return generateDocument(docConfigId, objectId, 'DOCX');
    }
}