@isTest(SeeAllData=false)
private class CSContractSol_newAgreementControllerTest {
    

        static Account testAccount;
        static Opportunity testOpportunity;
        static Contact testContact;
        static Associated_LEs__c testAssociatedLE;
        static cscfga__Product_Basket__c testBasket;
            
        private static void prepareTestData(){
            // testAccount = new Account(  name = 'RT test account',
            //                             OwnerId =UserInfo.getUserId() );
            // insert testAccount;
            //testAccount=[select id from account limit 1];
            //testContact=[select id from contact limit 1];
            TriggerDeactivating__c triggerSetting = new TriggerDeactivating__c(SetupOwnerId = UserInfo.getUserId(), Account__c = TRUE, Opportunity__c = TRUE);
            INSERT triggerSetting;
            OLI_Sync__c oliSync = new OLI_Sync__c(SetupOwnerId = UserInfo.getUserId(), Synchronisation_field__c = 'csbb__Synchronised_with_Opportunity__c');
            INSERT oliSync;
            
            testAccount=CS_TestDataFactory.generateAccount(true,'testingAccount');
            testContact=CS_TestDataFactory.generateContact(true,'testingContact');
            testAssociatedLE = CS_TestDataFactory.generateAssociatedLES(true,'testAssocLE',testAccount);
            testOpportunity= new Opportunity(   Name='RT test',
                                                AccountId=testAccount.id,
                                                CloseDate=date.today(),
                                                StageName='Created'
                                                );
            insert testOpportunity;
            
            testBasket = new cscfga__Product_Basket__c(cscfga__Opportunity__c=testOpportunity.Id);
            testBasket.Base_Proposition_Name__c = 'test Base Proposition Name';
            insert testBasket;
            
            testOpportunity.Tenure__c=24.0;
            testOpportunity.Basket_ID__c=testBasket.Id;
            testOpportunity.AssociatedLE__c = testAssociatedLE.Id;
            testOpportunity.Primary_Contact__c=testContact.Id;
            testOpportunity.Company_Signatory1__c=testContact.Id;
            testOpportunity.Company_Signatory2__c=testContact.Id;
            update testOpportunity; 

            csclm__Document_Definition__c docDefinition = new csclm__Document_Definition__c(Name = 'Opportunity DD', csclm__Linked_Object__c = 'Opportunity');
            INSERT docDefinition;
            
            csclm__Document_Template__c docTemplate = new csclm__Document_Template__c(Name = 'Full EEBA Template', csclm__Document_Definition__c = docDefinition.Id, csclm__Active__c = TRUE, csclm__Valid__c = TRUE, csclm__Effective_From__c = System.today().addDays(-10), csclm__Effective_To__c = System.today().addDays(10));
            INSERT docTemplate;

            system.debug('inserted test Basket and test Opportunity');
        }

        private static testMethod void testfetchBasket(){
         
            Test.StartTest(); 
            prepareTestData();
            PageReference pageRef = Page.CS_ContractSolution_newAgreement; // Add your VF page Name here
            pageRef.getParameters().put('id', testOpportunity.Id);
            //pageRef.getParameters().put('orderId', String.valueOf(testAccount.Id));
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity);
            CSContractSol_newAgreementController testController = new CSContractSol_newAgreementController(sc);
            //testController.redirPage();
            testController.createAgreement();
            
            Test.StopTest();
        }
        
        private static testMethod void testfetchBasket2(){
         
            Test.StartTest(); 
            prepareTestData();
            testOpportunity.AssociatedLE__c = NULL;
            update testOpportunity;
            PageReference pageRef = Page.CS_ContractSolution_newAgreement; // Add your VF page Name here
            pageRef.getParameters().put('id', testOpportunity.Id);
            //pageRef.getParameters().put('orderId', String.valueOf(testAccount.Id));
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.StandardController(testOpportunity);
            CSContractSol_newAgreementController testController = new CSContractSol_newAgreementController(sc);
            //testController.redirPage();
            testController.createAgreement();
            
            Test.StopTest();
        }

        /*
        private static testMethod void testfetchBasket(){
            test.Starttest();
            prepareTestData();
            CSContractSol_newAgreementController myControllerTest = new CSContractSol_newAgreementController();
            cscfga__Product_Basket__c returnedBasket = myControllerTest.fetchBasket(testBasket.Id);
            system.assertEquals(returnedBasket,testBasket);
            test.Stoptest();
        }

        private static testMethod void testcreateAgreement(){
            test.Starttest();
            prepareTestData();
            CSContractSol_newAgreementController myControllerTest = new CSContractSol_newAgreementController();
            PageReference fromClass = myControllerTest.createAgreement();


            csclm__Agreement__c newAgreement = new csclm__Agreement__c();
            newAgreement.Name = testOpportunity.Name + '_EEBA';
            newAgreement.csclm__Opportunity__c = testOpportunity.Id;
            string templateId = [select Id from csclm__Document_Template__c where Name ='Full EEBA Template'].Id;
            newAgreement.csclm__Document_Template__c = templateId;
          


            PageReference newPageref = new PageReference ('/'+newAgreement.Id);
            system.assertEquals(newPageRef,fromClass);
            test.Stoptest();
        }
        */

  
}