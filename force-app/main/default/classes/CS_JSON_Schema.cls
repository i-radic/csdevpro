/**
 * @name CS_JSON_Schema
 * @description Defines JSON Schema
 *
 */
public class CS_JSON_Schema {
    public List<CS_JSON_Section> sections {get; set;}
    
    public List<CS_JSON_Setting> settings {get; set;}
    
    public List<CS_JSON_Email> emails {get; set;}
    
    private Map<String, CS_JSON_Section> sectionsByName {
        get {
            if(sectionsByName == null) {
                sectionsByName = new Map<String, CS_JSON_Section>();
                for(CS_JSON_Section section : sections) {
                    sectionsByName.put(section.name, section);
                }
            }
            
            return sectionsByName;
        }
        private set;
    }
    
    private Map<String, CS_JSON_Setting> settingsByName {
        get {
            if(settingsByName == null) {
                settingsByName = new Map<String, CS_JSON_Setting>();
                for(CS_JSON_Setting setting : settings) {
                    settingsByName.put(setting.name, setting);
                }
            }
            
            return settingsByName;
        }
        private set;
    }
    
    /**
     * Class Constructor
     * @return  CS_JSON_Schema
     */
    public CS_JSON_Schema() {
    }
    
    /**
     * Gets section by name
     * @param name String
     * @return     CS_JSON_Section
     */  
    public CS_JSON_Section getSection(String name) {
        return sectionsByName.get(name);
    }

    /**
     * Gets setting by name
     * @param name String
     * @return     CS_JSON_Setting
     */ 
    public CS_JSON_Setting getSetting(String name) {
        return settingsByName.get(name);
    }
    
    /**
     * @name CS_JSON_Section
     * @description Defines sections for JSON schema
     *
     */
    public class CS_JSON_Section {
        public String name {get; set;}
        
        public String setting {get; set;}
        
        public CS_JSON_Setting settingInstance {get; set;}
        
        /**
         * Class Constructor
         * @return  CS_JSON_Section
         */
        public CS_JSON_Section() {
        }
    }
    
    /**
     * @name CS_JSON_Setting
     * @description Defines Settings in JSON Schema
     * @revision
     *
     */
    public class CS_JSON_Setting {
        public String name {get; set;}
        
        public String service {get; set;}
        
        public String filter {get; set;}
        
        public List<CS_JSON_Object> definitions {get; set;}
        
        private Map<String, CS_JSON_Object> definitionsByName {
            get {
                if(definitionsByName == null) {
                    definitionsByName = new Map<String, CS_JSON_Object>();
                    for(CS_JSON_Object def : definitions) {
                        System.debug('def >>>'+def.name);
                        definitionsByName.put(def.name, def);
                    }
                }
                
                return definitionsByName;
            }
            private set;
        }
        
        /**
         * Class Constructor
         * @return  CS_JSON_Setting
         */
        public CS_JSON_Setting() {
        }
        
        /**
         * Gets definition by name
         * @param name String
         * @return     CS_JSON_Object
         */ 
        public CS_JSON_Object getDefinition(String name) {
            System.debug('getDefinition >>>'+name);
            return definitionsByName.get(name);
        }   
    }
    
    /**
     * @name CS_JSON_Object
     * @description Defines objects for JSON Schema
     *
     */
    public class CS_JSON_Object {
        public String name {get; set;}
        
        public String type {get; set;}
        
        public String grouping {get; set;}
        
        public String relatedList {get; set;}
            
        public String filter {get; set;}
        
        public String parentField {get; set;}
        
        public List<CS_JSON_Field> fields {get; set;}
        
        public List<CS_JSON_Field> attributes {get; set;}
        
        public List<CS_JSON_Object> relatedObjects {get; set;}
        //adedd on 14-June-2019
        public List<CS_JSON_Object> siteProducts {get; set;}
        public List<CS_JSON_Object> relatedChild {get; set;}
        
    
        /**
         * Class  Constructor
         * @return  CS_JSON_Object
         */
        public CS_JSON_Object() {
        }
    }
    
    /**
     * @name CS_JSON_Field
     * @description Defines fields and attributes for JSON Schema
     *
     */
    public class CS_JSON_Field {
        public String name {get; set;}
        
        public String label {get; set;}
        
        public String type {get; set;}
        
        /**
         * Class Constructor
         * @return  CS_JSON_Field
         */
        public CS_JSON_Field() {
        }
        
        public String getType() {
            if(String.isBlank(this.type)) {
                return CS_JSON_Constants.FieldType_String;
            }
            else return this.type;
        }
        
        public String getAttributeName() {
            if(name.contains('.')) {
                List<String> separatedField = name.split('\\.', 2);
                return separatedField.get(0);
            }
            else {
                return name;
            }
        }
        
        public String getAttributeField() {
            if(name.contains('.')) {
                List<String> separatedField = name.split('\\.', 2);
                return separatedField.get(1);
            }
            else {
                return CS_JSON_Constants.AttributeField_Value;
            }           
        }
    }
    
    public class CS_JSON_Email {
        public String name {get; set;}
        public String recipient {get; set;}
        public String template {get; set;}
        public String condition {get; set;}
        public List<CS_JSON_MergeField> mergeFields {get; set;}
    }
    
    public class CS_JSON_MergeField {
        public String name {get; set;}
        public String filter {get; set;}
        public CS_JSON_MergeFieldPath path {get; set;}
    }
    
    public class CS_JSON_MergeFieldPath {
        public String value {get; set;}
        public CS_JSON_MergeFieldPath subpath {get; set;}
        public List<CS_JSON_MergeFieldFilter> filters {get; set;}
        
        public Boolean evaluate(Map<String, Object> obj) {
            Boolean evaluation = false;
            for(CS_JSON_MergeFieldFilter filter : this.filters) {
                evaluation = obj.get(filter.property) == filter.value && filter.operator == 'equals';
            }
            
            return evaluation;
        }
    }
    
    public class CS_JSON_MergeFieldFilter {
        public String property {get; set;}
        public String operator {get; set;}
        public String value {get; set;}
    }
    
    public class CS_JSON_Exception extends Exception {}
}