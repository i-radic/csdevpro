global class CS_CustomLookupBTOPCompanyExtras extends cscfga.ALookupSearch{

    public override String getRequiredAttributes(){
        return '["Product", "Exclude Product", "Excluded product addon names"]';
    }

    public override Object[] doLookupSearch(Map<String, String>
			searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset,
			Integer pageLimit){

		final Integer SELECT_LIST_LOOKUP_PAGE_SIZE = 25;
		final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = 26;
		Integer recordOffset = pageOffset * SELECT_LIST_LOOKUP_PAGE_SIZE;
			
		String product = searchFields.get('Product');
		String excludeProduct = searchFields.get('Exclude Product');
		List<String> excludeProductList = excludeProduct.split(',');
		List<String> excludeNames = searchFields.get('Excluded product addon names').split(',');
		String searchTerm = searchFields.get('searchValue');
        if (string.isEmpty(searchTerm)) {
            searchTerm = '%';
        } else {
            searchTerm = '%' + searchTerm + '%';
        }
			
		List<cspmb__Price_Item__c> priceItemList = [
			SELECT 		Id,
						Name,
						cspmb__One_Off_Charge__c,
						cspmb__One_Off_Cost__c,
						cspmb__Recurring_Charge__c,
						cspmb__Is_Active__c,
						BT_Hardware_Fund__c,
						BT_Technology_Fund__c,
						Discountable__c,
						cspmb__Recurring_Cost__c,
						Minimum_Recurring_Price__c,
						Minimum_One_Off_Price__c,
						Product_Code__c,
						Maximum_Users__c,
						Minimum_Number_of_users__c,
						Product_Type_Name__c,
						Record_Type_Name__c,
						Is_Free_Price_Entry__c,
						LoggedIn_User_Name__c
				FROM	cspmb__Price_Item__c
				WHERE	cspmb__Is_Active__c = true
					AND	Product_Type__c = :product
					AND Record_Type_Name__c = 'BTOP Company Extras'
					AND Name LIKE :searchTerm
					AND Id NOT IN :excludeProductList
					AND Name NOT IN :excludeNames
				ORDER BY Sequence__c
				LIMIT 	:SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT 
				OFFSET 	:recordOffset
		];

		return priceItemList;
    }
}