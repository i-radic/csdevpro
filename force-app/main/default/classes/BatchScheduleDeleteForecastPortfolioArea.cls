/*  ======================================================
    History                                                            
    -------                                                            
VERSION  AUTHOR             DATE              DETAIL                                 FEATURES
1.00     Dan Measures       13/06/2011        class used to identify which Forecast Portfolio Area records to delete - all whose opps close date is > ~2 weeks ago

*/
global class BatchScheduleDeleteForecastPortfolioArea implements Schedulable{
   	global void execute(SchedulableContext sc) {
		Date myDate = (Date.today()- 16);
   		String sDate = String.valueOf(myDate);
		String qString = 'select id from Forecasting_Portfolio_Area__c where Opportunity__r.isClosed = true and Opportunity__r.CloseDate  < '+ sDate;
        BatchDeleteForecastPortfolioArea b = new BatchDeleteForecastPortfolioArea(qString);
    	database.executebatch(b, 200);
    }
}