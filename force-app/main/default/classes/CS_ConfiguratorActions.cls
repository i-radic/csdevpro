public class CS_ConfiguratorActions {
    public static String CONDITIONS_ATTRIBUTE = 'ProductActionConditions';
    public static final Map<String, Integer> ACTION_PRIORITY = new Map<String, Integer>{
        'invalidate' => 0,
        'revalidate' => 1,
        'none' => 999
    };
        
    public static Map<String, ProductAction> actionSettings {
        get {
            if(actionSettings == null)
                actionSettings = (Map<String, ProductAction>) JSON.deserialize(CS_Utils.loadStaticResource('ConfiguratorActions').toString(), Map<String, ProductAction>.class);
            
            return actionSettings;
        }
        private set;
    }
    
    public static Map<Id, String> configActions {
        get {
            if(configActions == null)
                configActions = new Map<Id, String>();
            
            return configActions;
        }
        private set;
    }
    
    public static Map<Id, String> getActions(Map<String, Map<String, cscfga__Attribute__c>> attributes, List<cscfga__Product_Configuration__c> configs,
                                             Map<Id, cscfga__Product_Definition__c> definitions, String event) {
        for(cscfga__Product_Configuration__c cfg : configs) {
            String action = 'none';
            Map<String, cscfga__Attribute__c> atts = attributes.get(cfg.cscfga__Key__c);
            ProductAction pAction = actionSettings.get(definitions.get(cfg.cscfga__Product_Definition__c).Name);
            if(pAction != null) {
                Map<String, String> oldValues = getOldValues(atts);
                Map<String, String> newValues = new Map<String, String>();
                for(ActionConditions condition : pAction.conditions) {
                    newValues.put(condition.name, atts.get(condition.name).cscfga__Value__c);
                    if(condition.isValid(oldValues.get(condition.name), newValues.get(condition.name), event)) {
                        action = getAction(action, condition.action);
                    }
                }
                
                updateConditionAttribute(atts, newValues);
                system.debug('mpk2 id- '+cfg.Id+' action-'+action);
                configActions.put(cfg.Id, action);
            }
        }        
        
        return configActions;
    }
    
    public static Map<String, Set<Id>> getConfigsByActions() {
        Map<String, Set<Id>> configsByActions = new Map<String, Set<Id>>();
        for(String action : ACTION_PRIORITY.keySet()) {
            configsByActions.put(action, new Set<Id>());
        }
        
        return configsByActions;
    }
    
    public static void executeActions(Map<Id, cscfga__Product_Configuration__c> allConfigs) {
        Set<Id> allConfigsToAction = new Set<Id>();
        Map<String, Set<Id>> configsByActions = getConfigsByActions();
        Map<String, Set<Id>> configsByDef = getConfigsByDef(allConfigs.values());
        system.debug('mpk1- '+configActions.keySet());
        for(Id cfgId : configActions.keySet()) {
            if(cfgId != null && configActions.get(cfgId) != 'none') {
                cscfga__Product_Configuration__c cfg = allConfigs.get(cfgId);
                ProductAction pAction = actionSettings.get(cfg.Product_Definition_Name__c);
            	if(pAction != null) {
                    for(String depProducts : pAction.dependentProducts) {
                        if(configsByDef.containsKey(depProducts)) {
                        	Set<Id> configsAffected = configsByDef.get(depProducts);
                        	for(Id cfgAffectedId : configsAffected) {
                        		cscfga__Product_Configuration__c cfgAffected = allConfigs.get(cfgAffectedId);
                        		if(cfgAffected.cscfga__Configuration_Status__c == 'Valid') {
                        			configsByActions.get(configActions.get(cfgId)).add(cfgAffectedId);
                        			allConfigsToAction.add(cfgAffectedId);
                        		}
                        	}
                        }
                    }
                }
            }
        }

		if(!allConfigsToAction.isEmpty()) {
            setProductConfigurationRequests(allConfigsToAction, 'revalidating');
			
			System.enqueueJob(new CS_ConfiguratorActionsProcessor(configsByActions));
		}
    }
    
    public static void setProductConfigurationRequests(Set<Id> configIds, String status) {
        List<csbb__Product_Configuration_Request__c> requests = [
            Select id, csbb__Status__c
            From csbb__Product_Configuration_Request__c
            Where csbb__Product_Configuration__c = :configIds
        ];
        for(csbb__Product_Configuration_Request__c request : requests) {
            request.csbb__Status__c = status;
        }
        
        update requests;        
    }
    
    public static void setPCRequests(Set<Id> configIds, String status) {
        List<csbb__Product_Configuration_Request__c> requests = [
            Select id, csbb__Status__c
            From csbb__Product_Configuration_Request__c
            Where csbb__Product_Configuration__c = :configIds
        ];
        for(csbb__Product_Configuration_Request__c request : requests) {
            request.csbb__Status__c = status;
        }
        
        update requests;        
    }
    
    public static Map<String, ProductAction> actionSettingsFromJSON {
        get {
            if(actionSettings == null)
                actionSettings = (Map<String, ProductAction>) JSON.deserialize(CS_Utils.loadStaticResource('ConfiguratorActions').toString(), Map<String, ProductAction>.class);
            
            return actionSettings;
        }
        private set;
    }
    
    public static Map<Id, String> configActionsMap {
        get {
            if(configActions == null)
                configActions = new Map<Id, String>();
            
            return configActions;
        }
        private set;
    }
    
    public static Map<String, Set<Id>> getConfigsByDefFromConfigList(List<cscfga__Product_Configuration__c> configs) {
        Map<String, Set<Id>> configsByDef = new Map<String, Set<Id>>();
        for(cscfga__Product_Configuration__c cfg : configs) {
            if(!configsByDef.containsKey(cfg.Product_Definition_Name__c)) {
                configsByDef.put(cfg.Product_Definition_Name__c, new Set<Id>());
            }
            
            configsByDef.get(cfg.Product_Definition_Name__c).add(cfg.Id);
        }
        
        return configsByDef;
    }
    
    public static Map<String, Set<Id>> getConfigsByDef(List<cscfga__Product_Configuration__c> configs) {
        Map<String, Set<Id>> configsByDef = new Map<String, Set<Id>>();
        for(cscfga__Product_Configuration__c cfg : configs) {
            if(!configsByDef.containsKey(cfg.Product_Definition_Name__c)) {
                configsByDef.put(cfg.Product_Definition_Name__c, new Set<Id>());
            }
            
            configsByDef.get(cfg.Product_Definition_Name__c).add(cfg.Id);
        }
        
        return configsByDef;
    }
    
    public static String getAction(String currentAction, String newAction) {
        return ACTION_PRIORITY.get(currentAction) > ACTION_PRIORITY.get(newAction) ? newAction : currentAction;
    }
    
    public static Map<String, String> getOldValues(Map<String, cscfga__Attribute__c> atts) {
        cscfga__Attribute__c conditionsAtt = atts.get(CONDITIONS_ATTRIBUTE);
        return String.isBlank(conditionsAtt.cscfga__Value__c) ? new Map<String, String>() : (Map<String, String>) JSON.deserialize(conditionsAtt.cscfga__Value__c, Map<String, String>.class);
    }
    
    public static void updateConditionAttribute(Map<String, cscfga__Attribute__c> atts, Map<String, String> newValues) {
        cscfga__Attribute__c conditionsAtt = atts.get(CONDITIONS_ATTRIBUTE);
        conditionsAtt.cscfga__Value__c = JSON.serialize(newValues);
    }
    
    public class ProductAction {
        public String product {get; set;}
        public List<String> dependentProducts {get; set;}
        public List<ActionConditions> conditions {get; set;}
    }
    
    public class ActionConditions {
        public String name {get; set;}
        public String action {get; set;}
        public String value {get; set;}
        public String event {get; set;}
        
        public Boolean isValid(String priorValue, String currentValue, String event) {
        	if(this.event != event) {
        		return false;
        	}
        	else if(event == 'change') {
        		return String.isBlank(priorValue) || (String.isNotBlank(priorValue) && priorValue != currentValue); 
        	}
        	else if(event == 'delete') {
        		return String.isNotBlank(priorValue) && priorValue == this.value || String.isBlank(this.value);
        	}
            else {
            	return false;
            }
        }
    }
    
    public static List<cscfga__Attribute__c> getAttributeListFromConfiguration(Id configId){
        List<cscfga__Attribute__c> attrList = new List<cscfga__Attribute__c>();
        if(configId != null)
            attrList = [
                SELECT Id, Name 
                FROM cscfga__Attribute__c 
                WHERE cscfga__Product_Configuration__c = :configId
            ];
            
        if(attrList != null)
            return attrList;
            
        return new List<cscfga__Attribute__c>();
    }
}