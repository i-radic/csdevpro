@isTest(SeeAllData=true)
private class tgrUpdateForecastPortfolioArea_Test {

    static testMethod void myUnitTest() {
        
         Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.com',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@bt.com',
                           EIN__c = 'OUFPA_def'
                           );
        insert u1;
    
        Date uDate = date.today().addDays(7);
        Account a = Test_Factory.CreateAccount();
        a.Sector_Code__c = 'BTLB';
        a.OwnerId = u1.Id;
        insert a;
        Opportunity o = Test_Factory.CreateOpportunity(a.Id);
        insert o;
        Forecasting_Portfolio_Area__c fpa = new Forecasting_Portfolio_Area__c(Opportunity__c = o.Id);
        insert fpa;
        o.StageName = 'In Progress';
        update o;
        o.CloseDate = uDate;
        o.StageName = 'Won';        
        update o;
        o.CloseDate = uDate;
        o.StageName = 'Created';        
        update o;
        
        Pricebook2 priceBook = [select Id from Pricebook2 where isStandard=true limit 1];
        
        Product2 product = Test_Factory.CreateProduct();
        product.Portfolio_Area__c = 'TEST';
        insert product;
       
        PricebookEntry pricebookEntry = Test_Factory.CreatePricebookEntry(priceBook.Id, product.Id);
        insert pricebookEntry;

        OpportunityLineItem opportunityLineItem = Test_Factory.CreateOpportunityLineItem(pricebookEntry.Id, o.Id);
        opportunityLineItem.Qty__c = 1;
        
        insert opportunityLineItem;
        
        List<Forecasting_Portfolio_Area__c> listfpa = [select id from Forecasting_Portfolio_Area__c where Opportunity__c = :o.Id];
        system.debug(listfpa.size());
        //System.assertEquals(listfpa.size(), 2); 
        
    }
}