@isTest(seeAllData=true)
public class TestUsageProfilesUploadResignPacks 
{
     static testMethod void testParseCSV()
    {
      StaticResource  sr = [Select Body From StaticResource Where Name = 'ResignPackTestClassContent' LIMIT 1];
      Blob csvContent = sr.Body;
      PageReference pageRef = Page.UploadResignPack;
      Test.setCurrentPage(pageRef);
      Account a = new Account();
      a.Name ='Test';
      insert a;
      Opportunity o =new Opportunity();
      o.accountId=a.id;
      o.StageName='Created';
      o.name='Test';
      insert o;
      Customer_Billing_Accounts__c cba = new Customer_Billing_Accounts__c();
      cba.Company__c = a.id;
      cba.name ='12345';
      insert cba;
      Resign_Request__c rrp = new Resign_Request__c();
      //rrp.Name ='TestResign';
      rrp.Account__c =a.id;
      rrp.Reason_for_no_Open_Opportunity__c='Test';
      rrp.Early_Termination_Charges_Complete__c=true;
      rrp.Customer_Billing_Accounts__c = cba.id;
      rrp.Early_Termination_Charges__c = 'Yes';  
      rrp.Rework_of_ETC__c='No';
      rrp.Opportunity__c=o.Id;  
      insert rrp;
      System.currentPageReference().getParameters().put('id', rrp.id);
      ApexPages.StandardController sc = new ApexPages.StandardController(rrp );
      usageProfilesUploadResignPacks  controllerExt = new usageProfilesUploadResignPacks  (sc);
      controllerExt.fileName ='Test';
      controllerExt.fileContent =csvContent  ;
      System.debug('csvContent  '+csvContent  );
      controllerExt.readFile();
      System.assertEquals('File read successfully.Please click the below link to go to the usage profile created.',controllerExt.uploadResponse);
    }
 }