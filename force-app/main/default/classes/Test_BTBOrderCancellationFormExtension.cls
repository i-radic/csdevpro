@isTest
private class Test_BTBOrderCancellationFormExtension{

static testMethod void myUnitTest() {

  BTB_Order_Line__c BOB=[Select Currently_Actioned_By__c,BTB_CSS_ORDER_NO__c,CreatedDate,BTB_ORDER__r.OV_ORDER_NUM__c
                      From BTB_Order_Line__c where ID <> 'a1j200000015Lv9AAE'limit 1];
  BOB.Currently_Actioned_By__c=UserInfo.getName();
  BOB.Currently_Actioned_Time__c=datetime.now();
  // changes by Robin......
  try
  {
  Update BOB;
  }
  catch(Exception d)
  {
   d.getmessage();
  }
                      
  User U;
                  
  PageReference PageRef = Page.BTBOrderCancellationForm;
  test.setCurrentPage(PageRef); 
  
  BTLB_Order_Cancellation_Form__c BOCF=new BTLB_Order_Cancellation_Form__c();
  BOCF.Form_Completion_Date__c=system.today();
  BOCF.Form_Created_By_Name__c=UserInfo.getName();
  BOCF.BTB_Order_Line__c=BOB.Id;
  BOCF.Customer_Contacted__c='Yes';
  BOB.Cancellation_Current_Status__c='Completed';
  
  ApexPages.currentPage().getParameters().put('BTBOrderLineId',BOB.Id);
 
  Apexpages.Standardcontroller sc = new Apexpages.Standardcontroller(BOCF);
  BTBOrderCancellationFormExtension BC= new BTBOrderCancellationFormExtension(sc);
  
  Boolean DisplayAHD,CFClosed=True,CFError,RecordLockError,SubmitLockError,DisplayErr;
  double dcalcsave;
  String customerContacted='Yes',Q1='None',Q2,Q3,Q6,Q7,Q8,Q4,Q4a,Q5,Q5a;
  BC.RecordLocking();
  
  BOB.Currently_Actioned_By__c=NULL;
  BOB.Currently_Actioned_Time__c=datetime.now();
   try
  {
  Update BOB;
  BC.RecordLocking();
  }
  catch(Exception d)
  {
   d.getmessage();
  }
  
  
  BOB.Currently_Actioned_By__c='Hello';
  BOB.Currently_Actioned_Time__c=datetime.now();
  try
  {
  Update BOB;
  BC.RecordLocking();
  }
  catch(Exception d)
  {
   d.getmessage();
  }
  
  
  BC.incrementCounter();
  BC.getDispPageSectionError();
  BC.getRecLockError();
  BC.getSaveLockError();
  BC.getDispPageSection();
  BC.getDispAhdSection();
  
  BOCF.Company_Name_OV__c='Test';
  BOCF.Telephone_Number_OV__c=NULL;
 
  BC.getQ1Items();
  BC.getQ2Items();
  BC.getQ3Items();
  BC.getQ3aItems();
  BC.getQ4Items();
  BC.getQ5Items();
  BC.getQ5aItems();
  BC.getQ6Items();
  BC.getQ7Items();
  BC.getQ8Items();
  BC.incrementQ1();
  BC.incrementQ2();
  BC.incrementQ3();
  BC.incrementQ8();
  BC.getDispQ2Section();
  BC.getDispQ3Section();
  BC.getDispQ3aSection();
  BC.getDispQ4Section();
  BC.getDispQ4aSection();
  BC.getDispQ5Section();
  BC.getDispQ678Section();
  BC.getDispQ9Section();
  BC.getDispErrSection();
  BC.getcustomerContactedItems();
  BC.getcustomerContacted();
  BC.getBTOCF();
  BC.setBTOCF(BOCF);
  BC.setcustomerContacted('No');
  
  BOCF.Company_Name_OV__c='ABC Corp';
  BOCF.Contact_Name_OV__c='Test';
  BOCF.Telephone_Number_OV__c='123434';
  BOCF.Number_of_lines_involved__c=112;
  BOCF.Customer_Telephone_Number__c='123434';
  
  BOCF.Name_of_Contact__c='Hello';
  BOCF.Position_in_Company__c='PM';
  BOCF.Address__c='UK';
  BOCF.Number_of_Employees__c='<10';
  
  Competitors_Suppliers_Manufacturers__c C=new Competitors_Suppliers_Manufacturers__c(E2E_Competitor__c=True,Name='6666');   
  Insert C;
  BOCF.Losing_Supplier_Name__c=C.Id;
  
  Q1='Yes';
  Q2='No';
  DisplayErr=False;

  BC.SubmitForm();

  
}
}