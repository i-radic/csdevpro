public class UsageProfileFMController {
    
    public Id AccountId{get;set;}   
    public Id basketId{get;set;}  
    public list<Usage_Profile__c> result {get; set;}
    public UsageProfileFMController(){
        result = new list<Usage_Profile__c>();  
        basketId = System.currentPageReference().getParameters().get('basketId');
        AccountId= System.currentPageReference().getParameters().get('AccountId'); 
        System.debug('AccountId>>>>>>>>>>>>>>>'+AccountId);
        SearchUsageProfile();  
    }
    public void SearchUsageProfile(){
        result = new list<Usage_Profile__c>();
        result = [select Id,Name,Account__r.Id,FM_Calls_to_EE__c,FM_Calls_to_other_UK_mobile_networks__c,
                  FM_IDD_SMS_Zone_A__c,FM_IDD_SMS_Zone_B__c,FM_IDD_SMS_Zone_C__c,FM_IDD_Zone_A__c,FM_IDD_Zone_B__c,
                  FM_IDD_Zone_C__c,FM_MMS__c,FM_MMS_Messaging__c,FM_Non_Geographic_Calls__c,FM_Receiving_call_in_Zone_A__c,
                  FM_Receiving_call_in_Zone_B__c,FM_Receiving_call_in_Zone_C__c,FM_Roaming_SMS_Zone_A__c,FM_Roaming_SMS_Zone_B__c,
                  FM_Roaming_SMS_Zone_C__c,FM_SMS_to_EE__c,FM_SMS_to_other_network__c,FM_UK_Landlines__c,FM_Voice_calls_back_to_UK_from_Zone_A__c,
                  FM_Voice_calls_back_to_UK_from_Zone_B__c,FM_Voice_calls_back_to_UK_from_Zone_C__c,FM_Voice_calls_to_Zone_A__c,
                  FM_Voice_calls_to_Zone_B__c,FM_Voice_calls_to_Zone_C__c,
                  Expected_Voice__c,Expected_Voice_per_User__c,Expected_SMS__c,Expected_SMS_per_User__c,
                  Expected_Data__c,Expected_Data_per_User__c
                  from Usage_Profile__c where Account__r.Id = :AccountId and Active__c = true];
                  
         System.debug('SearchUsageProfile result>>>>>>>>>>>>>>>'+result);
        if(result.size() == 0)
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid Account Id'));
    }
    public Usage_Profile__c getProfile() {
        System.debug('result>>>>>>>>>>>>>>>'+result);
        if (!result.isEmpty()){ 
            return result[0];
        }            
        else {
             return null;
        }
           
    }
    
  
}