/******************************************************************************************************
Name : QuestionnaireControllerview
Description : Controller for page questionairepageview
Test Class:NA

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    1/12/2015              Jacob Thomas                         Class created         
*********************************************************************************************************/
public class QuestionnaireControllerview
{
    public List<QuestionAnswerWrapperClass> wrapper {get;set;}
    public String gateName{get;set;}
    public id qtid;
    public Questionnaire__c qtrecord;
    //public boolean showscore {get;set;}
    public Double score {get;set;}
    public map<id,QuestionnaireReply__c> questreplyMap = new map<id,QuestionnaireReply__c>();
    public QuestionnaireControllerview()
    {
        wrapper  =  new List<QuestionAnswerWrapperClass>();
        qtid =System.currentPagereference().getParameters().get('qt');
        if(qtid!=null)
        {
            qtrecord = [select id,Overall_Score__c,Opportunity__c,Gate__c  from Questionnaire__c where id =:qtid];
        }
        List<QuestionnaireReply__c> qustreplyList = new List<QuestionnaireReply__c>();
        Set<id> qustid = new Set<id>();
        Map<id,id> qustansMap = new Map<id,id>();
        if(qtrecord !=null)
        {
            qustreplyList=[select id,Answer__c,Question__c,Questionnaire__c from QuestionnaireReply__c where Questionnaire__c =:qtrecord.id];
            gatename=qtrecord.Gate__c ;
        }
        if(! qustreplyList.isEmpty())
        {
            for(QuestionnaireReply__c reply : qustreplyList)
            {
                qustid.add(reply.Question__c);
                qustansMap.put(reply.Question__c,reply.Answer__c);
            }
        }
        List<QuestionnaireQuestion__c> questionnaireList =  new  List<QuestionnaireQuestion__c>();
        String catgory=' ';
        String toparea= ' ';
        integer srn =0;
        questionnaireList = [Select id,Opportunity_Gates__c,Order__c,Question__c,Category__c,Topic_Area__c,Weighting__c, (Select id,Answer__c,Questionnaire_Question__c,Order__c,Score__c from Questionnaire_Answers__r order by Order__c) from QuestionnaireQuestion__c where RecordType.Name= 'Qualification' and id IN:qustid order by Order__c];
        if(! questionnaireList.isEmpty())
        {
            for(QuestionnaireQuestion__c questions : questionnaireList)
            {
                List<QuestionnaireAnswer__c > ansList = questions.Questionnaire_Answers__r;
                List<QuestionnaireAnswer__c> options = new List<QuestionnaireAnswer__c>();
                String selectedDefault;
                if(qustansMap.containsKey(questions.id))
                {
                    selectedDefault=qustansMap.get(questions.id);
                }
                for(QuestionnaireAnswer__c answers : ansList )
                {
                    if(answers.Questionnaire_Question__c == questions.id )
                    {
                        options.add(answers);
                    }
                }
                if(catgory != questions.Category__c)
                {
                    srn=1;
                    catgory=questions.Category__c;
                }
                else
                {
                    srn+=1;
                    catgory=' ';
                }
                if(toparea != questions.Topic_Area__c)
                {
                    toparea=questions.Topic_Area__c;
                }
                else
                {
                    toparea=' ';
                }
                wrapper.add(new QuestionAnswerWrapperClass(options,questions,selectedDefault,catgory,toparea,srn));
            }
        }
        if(qtrecord !=null)
        {
            score=qtrecord.Overall_Score__c;
        }
    }

    public class QuestionAnswerWrapperClass 
    {
        public List<QuestionnaireAnswer__c> answers{get;set;}
        public QuestionnaireQuestion__c questions{get;set;}
        public String selectedAnswer {get;set;}
        public String category{get;set;}
        public String topicarea{get;set;}
        public integer srn{get;set;}
        public QuestionAnswerWrapperClass (List<QuestionnaireAnswer__c> answers , QuestionnaireQuestion__c questions,String selectedAnswer,String category,String topicarea,integer srn) 
        {
            this.answers=answers;
            this.questions=questions;    
            this.selectedAnswer =selectedAnswer;   
            this.category=category;
            this.topicarea=topicarea;
            this.srn=srn;
        }
    }
    public PageReference Edit()
    {
        PageReference pageRef = Page.Questionnairepageedit;
        pageRef.getParameters().put('qt',qtrecord.id);
        return PageRef;
    } 
}