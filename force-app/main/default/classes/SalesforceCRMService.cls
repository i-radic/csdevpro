/*
* Change History *
Feb 2019    : Praveen Malyala   : Added changes for new MPA callouts - NodeLocator, ProvLineQlty, GetECC
July 2019   : Praveen Malyala   : //NEW GEA RESPONSE tags for FTTP
*/

public class SalesforceCRMService {
    @TestVisible
    public class CRMContact {
        public String CUG{ get; set; }
        public String AccountManagerContact{ get; set; }
        public String ActiveFlag{ get; set; }
        public String Comment{ get; set; }
        public String Type_x{ get; set; }
        public String ExternalId{ get; set; }
        public String Title{ get; set; }
        public String FirstName{ get; set; }
        public String MiddleName{ get; set; }
        public String LastName{ get; set; }
        public String AliasName{ get; set; }
        public String ContactSource{ get; set; }
        public String JobSeniority{ get; set; }
        public String Profession{ get; set; }
        public DateTime DateOfBirth{ get; set; }
        public String MotherMaidenName{ get; set; }
        public String Status{ get; set; }
        public String JobTitle{ get; set; }
        public String KeyDecisionMaker{ get; set; }
        public String FaxNumber{ get; set; }
        public String crmOrganization{ get; set; }
        public String Department{ get; set; }
        public String RightToContact{ get; set; }
        public String PreferredContactChannel{ get; set; }
        public String SecondaryPreferredContactChannel{ get; set; }
        public String EmailAddress{ get; set; }
        public String EmailAddressId{ get; set; }
        public String EmailConsentPermission{ get; set; }
        public DateTime EmailConsentDate{ get; set; }
        public String EmailConsentType{ get; set; }
        public String HomePhoneId{ get; set; }
        public String HomePhone{ get; set; }
        public String HomePhoneConsentPermission{ get; set; }
        public DateTime HomePhoneConsentDate{ get; set; }
        public String HomePhoneConsentType{ get; set; }
        public String MobilePhoneId{ get; set; }
        public String MobilePhone{ get; set; }
        public String MobilePhoneConsentPermission{ get; set; }
        public String MobilePhoneSMSConsentPermission{ get; set; }
        public DateTime MobilePhoneConsentDate{ get; set; }
        public String MobilePhoneConsentType{ get; set; }
        public String FaxId{ get; set; }
        public String Fax{ get; set; }
        public String FaxConsentPermission{ get; set; }
        public DateTime FaxConsentDate{ get; set; }
        public String FaxConsentType{ get; set; }
        public String WorkPhoneId{ get; set; }
        public String WorkPhone{ get; set; }
        public String WorkPhoneConsentPermission{ get; set; }
        public DateTime WorkPhoneConsentDate{ get; set; }
        public String WorkPhoneConsentType{ get; set; }
        public String AddressId{ get; set; }
        public String AddressNumber{ get; set; }
        public String AddressStreet{ get; set; }
        public String AddressLocality{ get; set; }
        public String AddressDoubleDependentLocality{ get; set; }
        public String AddressPostCode{ get; set; }
        public String AddressPostTown{ get; set; }
        public String AddressCounty{ get; set; }
        public String AddressCountry{ get; set; }
        public String AddressSector{ get; set; }
        public String AddressSubBuilding{ get; set; }
        public String AddressZipCode{ get; set; }
        public String AddressConsentPermission{ get; set; }
        public DateTime AddressConsentDate{ get; set; }
        public String AddressTimeAt{ get; set; }
        public String AddressConsentType{ get; set; }
        public Boolean isExisting{ get; set; }
        public String ContactCategory{ get; set; }
        public String isLegacyResponse{ get; set; } 
        
        
        public String EmailAddress1{ get; set; }
        public String EmailAddress2{ get; set; }
        public String EmailAddress3{ get; set; }
        public String Email1IntegrationId{ get; set; }
        public String Email2IntegrationId{ get; set; }
        public String Email3IntegrationId{ get; set; }        
        
        public String EmailPreferred{get;set;}        
        public String Email1Delete{get;set;}
        public String Email2Delete{get;set;}
        public String Email3Delete{get;set;}
        public String Email1DeleteIntegrationId{ get; set; }
        public String Email2DeleteIntegrationId{ get; set; }
        public String Email3DeleteIntegrationId{ get; set; } 
        
        //Voice related
        public String Voice1{get;set;}
        public String Voice2{get;set;}
        public String Voice3{get;set;}
        public String VoiceType1{get;set;}
        public String VoiceType2{get;set;}
        public String VoiceType3{get;set;}        
        public String Voice1Id{get;set;}
        public String Voice2Id{get;set;}
        public String Voice3Id{get;set;}        
        public String VoiceType1Delete{get;set;}
        public String VoiceType2Delete{get;set;}
        public String VoiceType3Delete{get;set;}                
        public String VoicePreferred{get;set;}                       
        public String Voice1Delete{get;set;}
        public String Voice2Delete{get;set;}
        public String Voice3Delete{get;set;}
        public String Voice1DeleteIntegrationID{get;set;}
        public String Voice2DeleteIntegrationID{get;set;}
        public String Voice3DeleteIntegrationID{get;set;}
        
        //Generic Consent
        public String EmailConsent{ get; set; }
        public String PhoneConsent{ get; set; }
        public String SMSConsent{ get; set; }
        public String AutomatedPhonecallsConsent{ get; set; }
        public String PostConsent{ get; set; } 
        public String ProfileConsent{ get; set; }
        public String DigitalMarketingConsent{ get; set; }
        
        public DateTime AutomatedPhonecallsConsentDate{ get; set; }        
        public DateTime ProfilingConsentDate{ get; set; }
        public DateTime DigitalMarketingConsentDate{ get; set; } 
    }
    
    public class CRMAccount {
        public String CUG{ get; set; }
        public String SAC{ get; set; }
        public String PrimaryContactid{ get; set; }
        public String CustomerName{ get; set; }
        public Boolean IsResidential{ get; set; }
        public String Type_x{ get; set; }
        public String CustomerClass{ get; set; }
        public Boolean IsBTBLegacy{ get; set; }
        public String OrganizationName{ get; set; }
        public String Postcode{ get; set; }
        public String strategicJourney{ get; set; }
        public String Status{ get; set; }
        public String Sector{ get; set; }
        public String SubSector{ get; set; }
        public String LineOfBusiness{ get; set; }
        public String wlr3Flag{ get; set; }
    }
    
    public class CRMAddress {
        public String IdentifierId{ get; set; }
        public String IdentifierName{ get; set; }
        public String IdentifierValue{ get; set; }
        public String Country{ get; set; }
        public String County{ get; set; }
        public String Name{ get; set; }
        public String POBox{ get; set; }
        public String BuildingNumber{ get; set; }
        public String BuildingName{ get; set; }
        public String Street{ get; set; }
        public String Locality{ get; set; }
        public String DoubleDependentLocality{ get; set; }
        public String PostCode{ get; set; }
        public String Town{ get; set; }
        public String SubBuilding{ get; set; }
        public String ExchangeGroupCode{ get; set; }
        //NodeMPACall
        public String northing{ get; set;}
        public String easting{ get; set;}
        public String ExchangeName{ get; set;}
        public String ExchangeCode{ get; set;}
        public String ExchangeType{ get; set;}
        public String RadialDistance{ get; set;}
        public String MainLinkDistance{ get; set;}
        public String PrimaryMetroNode{ get; set;}
        public String AccessCategory{ get; set;}
        public String exchangeDistID{ get; set;}
        public String qualifier{ get; set;}
        public SalesforceCRMService.PAInstance[] PAInstance{ get; set;}
    }
    
    public class CRMAddressList {
        public String responseStatus;
        public SalesforceCRMService.CRMArrayOfAddress Addresses;
    }
    
    public class CRMArrayOfAddress {
        public SalesforceCRMService.CRMAddress[] Address;
    }
    
    public class BTLBEventInteraction {
        public string EventId { get; set; }
        public string CUG { get; set; }
        public string RecordId { get; set; }
        public string agentEIN { get; set; }
        public string interactionIntegrationId { get; set; }
    }
    
    //MPA start
    public class PAInstance {
        public String ProductName{ get; set;}
        public String OHPExchangeName{ get; set;}
        public String OHPExchangeCode{ get; set;}
        public String AvailabilityFlag{ get; set;}
        public String EthernetUpstream{ get; set;}
        public String EthernetDownstream{ get; set;}
        public String EtherwayUpstream{ get; set;}
        public String EtherwayDownstream{ get; set;}
        public String BandwidthPerMPFPair{ get; set;}
        public String FTTPAvailable{ get; set;}        
        public String UpstreamSpeed{ get; set;}
        public String DownstreamSpeed{ get; set;}
        //ecc
        public String IndicativeECCOutSideThreshold{get; set;}
        public String IndicativeECCTariff{get; set;}
        public String IndicativeOrderCategory{get; set;}
        public String UpperIndicativeECC{get; set;}
        public String LowerIndicativeECC{get; set;}
        //NEW GEA RESPONSE tags for FTTP
        public String FTTPCPTransferOrWLTOAvailable{ get; set;}
        public String FTTPExistingONTAvailable{ get; set;}
        public String FTTPNewONTAvailable{ get; set;}        
        public String FTTPExistingONTAvailableCOMB{ get; set;}
        public String FTTPNewONTAvailableCOMB{ get; set;}
        public String L2SId{ get; set;}
    }
    //MPA end
    
    
    // ------------------------------------------------------------------------------------------
    
    public static SalesforceCRMService.CRMAccount GetAccount(String CUG) {
        //
        try { 
            SalesforceCRMService.CRMAccount crmAcc = new SalesforceCRMService.CRMAccount();
            crmAcc = new VordelAdapterService.GetCustomer().doWebRequest(CUG);
            
            system.debug('CRMAcccccc ::::::::::::'+crmAcc );
            return crmAcc; 
        } catch(Exception ex) {
            system.debug('GetAccount Exception ' + ex);
            return null;
        } finally {
            VordelAdapterService.logApiInfo();
        }
    }
    
    public static SalesforceCRMService.CRMContact GetContact(String CUG,String contactId) {
        
        try {
            SalesforceCRMService.CRMAccount crmAcc = GetCustomerIsLegacy(CUG);
            
            SalesforceCRMService.CRMContact crmCon = new SalesforceCRMService.CRMContact();
            crmCon = new VordelAdapterService.GetContact().doWebRequest(CUG, contactId,crmAcc.strategicJourney);
            system.debug('CRMgetContact ::::::::::::'+crmCon);
            return crmCon;
            
        } catch(Exception ex) {
            system.debug('GetContact Exception ' + ex);
            return null;
            
        } finally {
            VordelAdapterService.logApiInfo();
        }
    }
    @testVisible
    public static String GetCUGId(String telephoneNumber) {
        // 
        try{
            String cugId = new VordelAdapterService.GetCustomer().doWebRequestForCUG(telephoneNumber);
            
            return cugId;
        } catch(Exception ex) {
            system.debug('GetCUGId Exception ' + ex);
            return null;
        } finally {
            VordelAdapterService.logApiInfo();
        }
    }
    
    @testVisible
    public static SalesforceCRMService.CRMAddress GetAddress(String nadKey) {
        // 
        try { 
            system.debug('IIIIIIIIIII'+nadkey);
            SalesforceCRMService.CRMAddress crmAddr = new SalesforceCRMService.CRMAddress();
            crmAddr = new VordelAdapterService.GetAddress().doWebRequest(nadKey);
            
            return crmAddr;
        } catch(Exception ex) {
            system.debug('GetAccount Exception ' + ex);
            return null;
        } finally {
            VordelAdapterService.logApiInfo();
        }
    }
    @testVisible
    public static SalesforceCRMService.CRMContact InsertContact(SalesforceCRMService.CRMContact crmCon, boolean isNew) {
        try{
            SalesforceCRMService.CRMContact crmConExisting = new SalesforceCRMService.CRMContact();
            system.debug('errtyyuuuiyyyyyyyyyyyyy ' + isNew+'                    iiiiiiii '+crmCon);
            String crmContactResponse; 
            
            if(!isNew){
                
                SalesforceCRMService.CRMAccount crmAcc = GetCustomerIsLegacy(crmCon.CUG);
                crmCon.isLegacyResponse = crmAcc.strategicJourney;
                
                crmConExisting = new VordelAdapterService.ValidateContact().doWebRequest(crmCon);
                system.debug('crmConExisting ' + crmConExisting);
                if(crmConExisting == null){
                    return null;
                } else {
                    if(!crmConExisting.isExisting) {
                        crmContactResponse = createContact(crmCon);
                    }
                    crmConExisting.isLegacyResponse = crmCon.isLegacyResponse;
                    crmConExisting.ExternalId = crmCon.ExternalId;
                    return crmConExisting;
                }
            } else {
                crmContactResponse = createContact(crmCon);
                crmCon.isExisting = false;
                return crmCon;
            }
        } catch(Exception ex) {
            system.debug('insert contact exception ' + ex);
            return null;
        } finally {
            VordelAdapterService.logApiInfo();
        }
        
    }
    
    public static String UpdateContact(SalesforceCRMService.CRMContact crmCon) {
        try{
            
            String crmContactResponse;
            
            SalesforceCRMService.CRMAccount crmAcc = GetCustomerIsLegacy(crmCon.CUG);
            system.debug('crmAcc ' + crmAcc);
            
            SalesforceCRMService.CRMAddress crmAddr = new SalesforceCRMService.CRMAddress();
            crmAddr = new VordelAdapterService.GetAddress().doWebRequest(crmCon.AddressId);
            //crmAddr = new VordelAdapterService.GetAddress().doWebRequest('X05000096433');
            system.debug('crm Address ' + crmAddr);
            if(crmAddr == null){
                SalesforceCRMService.CRMAddress crmAddr2 = new SalesforceCRMService.CRMAddress();
                crmAddr2.IdentifierValue = crmCon.AddressId;
                crmAddr2.Town = crmCon.AddressPostTown;
                crmAddr2.PostCode = crmCon.AddressPostCode;
                crmAddr2.SubBuilding = crmCon.AddressSubBuilding;
                crmAddr2.BuildingNumber = crmCon.AddressNumber;
                crmAddr2.Street = crmCon.AddressStreet;
                crmAddr2.Locality = crmCon.AddressLocality;
                crmAddr2.DoubleDependentLocality = crmCon.AddressDoubleDependentLocality;
                crmAddr2.County = crmCon.AddressCounty;
                crmAddr2.Country = crmCon.AddressCountry;
                //crmAddr2.Name = crmCon.AddressSubBuilding;
                string createAddressResponse = new VordelAdapterService.CreateAddress().doWebRequest(crmCon, crmAddr2,crmAcc.strategicJourney);
            }
            crmCon.isLegacyResponse = crmAcc.strategicJourney;
            string createContactResponse = new VordelAdapterService.ManageContact().doWebRequest(crmCon, 'UpdateContact');
            return createContactResponse;
        } catch(Exception ex) {
            system.debug('exception manage contact ' + ex);
            return null;
        }  finally {
            VordelAdapterService.logApiInfo();
        }
    }
    
    @testVisible
    public static CRMAddressList GetNADAddress(String postCode) {   //mpk
        return GetNADAddress(postCode,'ROBT',true);
    }
    
     @testVisible
    public static CRMAddressList GetNADAddress(String postCode,Boolean logIt) {   //mpk
        return GetNADAddress(postCode,'ROBT',logIt);
    }  
    
    @testVisible
    public static CRMAddressList GetNADAddress(String postCode, String keyType, Boolean logIt) {
        CRMAddressList retAddresslist = new CRMAddressList();
        try
        {
            system.debug(' Nad PostCode'+postCode);
            
            SalesforceCRMService.CRMAddressList test = new VordelAdapterService.GetNADAddress().doWebRequest(postCode, keyType, logIt);
            
            system.debug('nad address value ' + test+'retAddresslist  : '+retAddresslist);
            
            retAddresslist = test;            
            return retAddresslist;
            
        }catch(Exception ex) {
            system.debug('exception manage contact ' + ex);
            return null;
        }finally {
            if(logIt) VordelAdapterService.logApiInfo(); //to avoid issue in Honeycomb create Addresse
        }
    } 
    
    public static String CreateNADAddress(SalesforceCRMService.CRMAddress address) {
        try {
            //NADServiceLibrary service = new NADServiceLibrary();
            String response = new VordelAdapterService.CreateNADAddress().doWebRequest(address);
            
            system.debug(' Try Block : '+response );
            //String resp = service.createNADAddress(address);
            return response;
        }catch(Exception ex) {
            system.debug('create NAD ' + ex);
            return null;
        } finally {
            VordelAdapterService.logApiInfo();
        }
    }
    
    public static void CreateEventInteraction(BTLBEventInteraction nEvent) {
        try{
            SalesforceCRMService.CRMAccount crmAcc = GetCustomerIsLegacy(nEvent.CUG);
            String btlbEventResponse = new VordelAdapterService.EventInteraction().doWebRequest(nEvent, crmAcc.strategicJourney);
        }catch(Exception ex) {
            
            //return null;
        } finally {
            VordelAdapterService.logApiInfo();
        }
        
    }
    
    
    //MPA atart
    //MPACallCRMService    
    @testVisible
    public static CRMAddressList GetORNADAddress(String postCode, Boolean logIt) {   //mpk
        return GetNADAddress(postCode,'OR',logIt);
    }
    
    //GetRadialDistance
    @testVisible
    public static CRMAddressList GetRadialDistance(String postCode, String northing, String easting, String bandwidth, Boolean logIt){       
        CRMAddressList crmNAD = null;
        CRMAddressList crmMPA = null;
        try
        {
            if(northing == '' || easting == ''){
                crmNAD = GetNADAddress(postCode,'ROBT',false);     //TODO: OR call is not returning Northing and easting values, so this 
                if (crmNAD.Addresses.Address[1] != null){
                    easting = crmNAD.Addresses.Address[1].easting;
                    northing = crmNAD.Addresses.Address[1].northing;                
                }
            }
            system.debug('Easting   -'  +easting    +'- Northing    -'+northing);
            if(Easting!=null && Northing!=null){
                crmMPA = new VordelAdapterService.GetRadialDistance().doWebRequest(postCode, northing, easting, bandwidth);
                system.debug('MPA GetRadialDistance Res - '+crmMPA);
            }
            return crmMPA;
        }
        catch(Exception ex) {
            system.debug('exception GetRadialDistance ' + ex);
            return null;
        }finally {
            if(logIt) VordelAdapterService.logApiInfo(); 
        }
    } 
    
    //GetLineQltyByAddr
    @testVisible
    public static CRMAddressList GetLineQltyByAddr(String postCode, String ORNADKey, String exchangeDistID, String lineType, Boolean logIt){ // where lineType = GEA or EFM
        //CRMAddressList crmORNAD = null;
        CRMAddressList crmMPA = null;
        try
        {
            //CRMAddressList crmORNAD = GetORNADAddress(postCode, false);     //to get OR NAD key            
            if(ORNADKey!=null && exchangeDistID!=null && lineType!=null){
                crmMPA = new VordelAdapterService.GetLineQltyByAddr().doWebRequest(postCode, ORNADKey, exchangeDistID, lineType);
                system.debug('MPA GetLineQltyByAddr Res - '+crmMPA);
            }
            return crmMPA;
        }
        catch(Exception ex) {
            system.debug('exception GetLineQltyByAddr ' + ex);
            return null;
        }finally {
            if(logIt) VordelAdapterService.logApiInfo(); 
        }
    } 
    
    //GETECC
    @testVisible
    public static CRMAddressList GetECC(String exchangeIDorNADKeyA, String nADKeyB, Boolean logIt){
        try
        {            
            CRMAddressList crmMPA = null;
            system.debug('exchangeID -'+exchangeIDorNADKeyA+'- nADKeyB  -'+nADKeyB);
            
            if(exchangeIDorNADKeyA!=null && nADKeyB!=null){
                crmMPA = new VordelAdapterService.GetECC().doWebRequest(exchangeIDorNADKeyA, nADKeyB);
                system.debug('MPA GetECC Res - '+crmMPA);
            }
            return crmMPA;
        }
        catch(Exception ex) {
            system.debug('exception GetECC ' + ex);
            return null;
        }finally {
            if(logIt) VordelAdapterService.logApiInfo(); 
        }
    } 
    //MPA end
    
    //----------------------------------- PRIVATE --------------------------------------------------------------------
    @testVisible
    private static String createContact(SalesforceCRMService.CRMContact crmCon) {        
        
        SalesforceCRMService.CRMAddress crmAddr = new SalesforceCRMService.CRMAddress();
        crmAddr = new VordelAdapterService.GetAddress().doWebRequest(crmCon.AddressId);
        // Log response of GetAddress
        system.debug('createContact CRMaddr ' + crmAddr);
        if(crmAddr == null){
            SalesforceCRMService.CRMAddress crmAddr2 = new SalesforceCRMService.CRMAddress();
            crmAddr2.IdentifierValue = crmCon.AddressId;
            crmAddr2.Town = crmCon.AddressPostTown;
            crmAddr2.PostCode = crmCon.AddressPostCode;
            crmAddr2.SubBuilding = crmCon.AddressSubBuilding;
            crmAddr2.BuildingNumber = crmCon.AddressNumber;
            crmAddr2.Street = crmCon.AddressStreet;
            crmAddr2.Locality = crmCon.AddressLocality;
            crmAddr2.DoubleDependentLocality = crmCon.AddressDoubleDependentLocality;
            crmAddr2.County = crmCon.AddressCounty;
            crmAddr2.Country = crmCon.AddressCountry;
            
            string createAddressResponse = new VordelAdapterService.CreateAddress().doWebRequest(crmCon, crmAddr2,null);
        }
        string createContactResponse = new VordelAdapterService.ManageContact().doWebRequest(crmCon, 'InsertContact');        
        return createContactResponse;
    }
    @testVisible
    private static CRMAccount GetCustomerIsLegacy(String CUG){
        SalesforceCRMService.CRMAccount crmAccount = new SalesforceCRMService.CRMAccount();
        crmAccount = new VordelAdapterService.GetCustomer().DoWebRequest(CUG);
        
        if(crmAccount != null){
            system.debug('ssssssss'+crmAccount.strategicJourney.toUpperCase());
            if (crmAccount.strategicJourney.toUpperCase() == 'Y'){
                crmAccount.strategicJourney = 'N';
            }
            else {
                crmAccount.strategicJourney = 'Y';
            }
            return crmAccount;
        }
        return null;
    }
    
    /* 
public static String GetNADExchangeGroupCode(String postCode) {
CRMAddressList retAddresslist = new CRMAddressList();
try
{
NADServiceLibrary service = new NADServiceLibrary();
//retval.Addresses = service.GetExchangeGroupCode(nadId);
return null;
}
catch (Exception e)
{

}
return null;

} 
*/
    
}