public  class GetClosingtheLoopController
{

    Public List<Closing_the_Loop_Improvements__c> clilist {get; set;}
    public GetClosingtheLoopController (ApexPages.StandardController sc)
    {
        clilist = new List<Closing_the_Loop_Improvements__c>();
        clilist = [select  name,Overview_Of_Project__c,Project_Name__c,Launch_Timescales__c,Key_Contact__c,Link__c from Closing_the_Loop_Improvements__c order by Name ASC];
    }
}