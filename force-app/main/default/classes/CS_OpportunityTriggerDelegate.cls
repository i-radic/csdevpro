/**
 * @name CS_OpportunityTriggerDelegate
 * @description Trigger delegate for Opportunity object
 * @revision
 *
 */
public class CS_OpportunityTriggerDelegate extends CS_TriggerHandler.DelegateBase {
	public static Boolean JSON_Generated = false;
	
	public Map<Id, Opportunity> closedWonOpportunities;
	public Map<Id, cscfga__Product_Basket__c> basketMap;
	
	public override void prepareBefore() {
		closedWonOpportunities = new Map<Id, Opportunity>();
	}
	
    public override void beforeUpdate(sObject old, sObject o) {
    }

	public override void prepareAfter() {
		closedWonOpportunities = new Map<Id, Opportunity>();
	}
	
    public override void afterUpdate(sObject old, sObject o) {
        Opportunity oldOpp = (Opportunity) old;
    	Opportunity newOpp = (Opportunity) o;

		CS_JSON_Generator JSON_Generator = new CS_JSON_Generator(newOpp.Id);
        
    	if(newOpp.IsClosed != oldOpp.IsClosed && newOpp.IsWon != oldOpp.IsWon && newOpp.StageName != oldOpp.StageName
    		&& newOpp.IsClosed && newOpp.IsWon) {
				JSON_Generator.generateJSON();
    		closedWonOpportunities.put(newOpp.Id, newOpp);
    	}
    }
    
    public override void finish() {
		if(!closedWonOpportunities.isEmpty() && !JSON_Generated) {
			CS_JSON_ExportGenerator generator = new CS_JSON_ExportGenerator(new List<Id>(closedWonOpportunities.keySet()).get(0));
			generator.generate();
			JSON_Generated = true;
		}

        if(!closedWonOpportunities.isEmpty()) {
            List<cscfga__Product_Basket__c> baskets = [
            	SELECT Id, csbb__Synchronised_With_Opportunity__c,Product_Definitions__c, Implementation_Form_Valid__c, 
            		cscfga__Opportunity__c, Total_Subscribers__c
                FROM cscfga__Product_Basket__c
                WHERE csbb__Synchronised_With_Opportunity__c = true AND 
               		cscfga__Opportunity__c = :closedWonOpportunities.keySet()
            ];
            Map<Id, cscfga__Product_Basket__c> basketsByOpp = new Map<Id, cscfga__Product_Basket__c>();
            for(cscfga__Product_Basket__c basket : baskets) {
                basketsByOpp.put(basket.cscfga__Opportunity__c, basket);
            }

	    	for(Opportunity opp : closedWonOpportunities.values()) {
                cscfga__Product_Basket__c productBasket = basketsByOpp.get(opp.Id);
	    		   if(productBasket != null && productBasket.Product_Definitions__c!= null && !String.valueof(productBasket.Product_Definitions__c).containsIgnoreCase('BTOP') && productBasket.Total_Subscribers__c < 100 && productBasket.Implementation_Form_Valid__c == false) {
	    			opp.addError(Label.CS_OpportunityImplementationFormDataNotValid);
               }
	    	}
		}
    }
}