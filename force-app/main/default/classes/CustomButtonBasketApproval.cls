global with sharing class CustomButtonBasketApproval extends csbb.CustomButtonExt {
    public String performAction (String basketId) {
        Map<Id, cscfga__Product_Configuration__c> configs = CS_ProductBasketService.getConfigs(new List<cscfga__Product_Basket__c>{new cscfga__Product_Basket__c(Id = basketId)});
        cscfga__Product_Basket__c basket = CS_ProductBasketService.getBasket(basketId);
        
        try {
            //Bypass the code if Cloud Voice or Future Mobile
            Boolean solutionConfigs = false;

            CS_Solution_Calculations__c cs = CS_Solution_Calculations__c.getOrgDefaults();
            String calculateProductGroup = cs.Skip_Old_Calculations_Product_Group__c;

            for(cscfga__Product_Configuration__c config : configs.values()) {
                if (config.Calculations_Product_Group__c != null && calculateProductGroup.contains(config.Calculations_Product_Group__c)) {
                    solutionConfigs = true;
                    break;
                }
            }
            if (!solutionConfigs) {
                CS_ProductBasketService.prepareTechFund(basket, configs, 'Tech_Fund__c');
                
                CS_Util.upsertReportConfigurationAttachment(configs.keySet());
                CS_ProductBasketService.calculateBasketTotals(configs, basket);
                CS_ProductBasketService.setSpecialConditions(configs, basket);
                update basket;
            }
        }
        catch (Exception e) {}
        String newUrl = '/apex/c__CS_BasketApproval?id='+basketId;
        
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    }
}