@isTest
private class tgrCustomerSalesSupportAfter_Test {
  
  static testMethod void myUnitTest() {
    	User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        Profile prof = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
         User u1 = new User(
                           alias = 'B2B1', email = 'B2B13cr@bt.it',
                           emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                           languagelocalekey = 'en_US',
                           localesidkey = 'en_US', ProfileId=prof.Id,
                           timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@testemail.com',
                           EIN__c = 'OTHER_def'
                           );
        insert u1;
        
        Account a = Test_Factory.CreateAccount();
        a.Name = 'Testing';
        a.LE_CODE__c = 'T-99999';
        a.OwnerId = u1.Id;
        insert a;
        
        Contact con = Test_Factory.CreateContact();
        con.AccountId = a.Id;
        con.Cug__c = 'cugAlan1';
        con.Contact_Post_Code__c = 'WR5 3RL';
        insert con;
        
        Opportunity o = Test_Factory.CreateOpportunity(a.id);
        insert o;
        
        RecordType rt_PM = [select id from RecordType where SobjectType='Customer_Sales_Support__c' and name ='Project Management' limit 1];
        // Commented as part of CR9277
       //RecordType rt_CS = [select id from RecordType where SobjectType='Customer_Sales_Support__c' and name ='Customer Satisfaction' limit 1];
        
        Customer_Sales_Support__c css1 = new Customer_Sales_Support__c();
        css1.Opportunity_Name__c = o.Id;
        css1.RecordTypeId = rt_PM.Id;
        css1.Comments__c = 'Test';
        insert css1;
        
        Customer_Sales_Support__c css2 = new Customer_Sales_Support__c();
        css2.Opportunity_Name__c = o.Id;
        css2.RecordTypeId = rt_PM.Id;
        css2.Comments__c = 'Test';
        insert css2;
        /* commented as part of CR9277
        Customer_Sales_Support__c css = new Customer_Sales_Support__c();
        css.Opportunity_Name__c = o.Id;
        css.RecordTypeId = rt_CS.Id;
        css.Comments__c = 'Test';
        css.Description_of_Project__c = 'test';
        insert css; */
        
        delete css1;
       // delete css; --> Commented as part of CR9277
        delete css2;           
        
  }
  }
  
}