/**
 * Interface class to allow dynamic class instantiation
 *
 */
global interface CS_ICustomiserExtension {
	
	/**
	 * Returns additional html to be used on configurator page
	 *
	 * @param ctrl
	 * @return String 
	 */
	String getTopComponentHtml(cscfga.ProductConfiguratorController ctrl);
	
	/**
	 * After save logic
	 *
	 * @param ctrl 
	 */
	void afterSave(cscfga.ProductConfiguratorController ctrl);

}