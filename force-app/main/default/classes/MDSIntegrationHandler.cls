/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 8.2020
   @author Mahaboob Basha

   @description This class is used as a custom Orchestrator step for uploading xml file to MDS 

   @modifications
   06.08.2020 [Mahaboob]
   
 */
global class MDSIntegrationHandler implements CSPOFA.ExecutionHandler {

	public List<SObject> process(List<SObject> data) {

		List<SObject> result = new List<SObject>();

		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;

		List<CSPOFA__Orchestration_Step__c> extendedList = [SELECT Id, /*CSPOFA__Orchestration_Process__r.Order__c,
		                                                    CSPOFA__Orchestration_Process__r.Order__r.Id,
		                                                    CSPOFA__Orchestration_Process__r.Order__r.csord__Account__c,*/
		                                                    CSPOFA__Orchestration_Process__r.csordtelcoa__Service__c
		                                                    FROM CSPOFA__Orchestration_Step__c
		                                                    WHERE Id IN :stepList];

		List<csord__Order__c> ordersToCheck = new List<csord__Order__c>();
		for (CSPOFA__Orchestration_Step__c step : extendedList) {
			//ordersToCheck.add(step.CSPOFA__Orchestration_Process__r.Order__r);
		}

		//recalcUpdateMySportsPrices(ordersToCheck);

		try {
			for (CSPOFA__Orchestration_Step__c step : extendedList) {
				step.CSPOFA__Status__c         = 'Complete';
				step.CSPOFA__Completed_Date__c = Date.today();
				result.add(step);
			}
		} catch (Exception ex) {
			for (CSPOFA__Orchestration_Step__c step : extendedList) {
				step.CSPOFA__Status__c  = 'Error';
				step.CSPOFA__Message__c = 'Error occured while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
				result.add(step);
			}
		}
		return result;
	}
	
	private void sendOrderDetailsToMDS () {
	    
	}
	
}