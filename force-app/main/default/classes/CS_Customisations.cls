global class CS_Customisations extends cscfga.AConfigurationCustomisation
{
    List<cscfga.AConfigurationCustomisation> customisations;

    global CS_Customisations()
    {
        customisations = new List<cscfga.AConfigurationCustomisation>();

        CS_Customisations__c settings =
            CS_Customisations__c.getInstance(UserInfo.getUserId());

        if (settings != null
            && settings.Classes__c != null)
        {
            List<String> classes = settings.Classes__c.split(',', 0);
            for (String cls : classes)
            {
                customisations.add
                    ( (cscfga.AConfigurationCustomisation)
                      Type.forName(cls.trim()).newInstance() );
            }
        }
    }

    global override ApexPages.Component getTopComponent()
    {
        ApexPages.Component returnValue = null;

        for (cscfga.AConfigurationCustomisation customisation : customisations)
        {
            returnValue = customisation.getTopComponent();
            if (returnValue != null)
            {
                break;
            }
        }

        return returnValue;
    }

    global override String getTopComponentHtml()
    {
        String returnValue = null;

        for (cscfga.AConfigurationCustomisation customisation : customisations)
        {
            returnValue = customisation.getTopComponentHtml();
            if (returnValue != null)
            {
                break;
            }
        }

        return returnValue;
    }

    global override String getTopComponentHtml
        ( cscfga.ProductConfiguratorController ctrl )
    {
        String returnValue = null;

        for (cscfga.AConfigurationCustomisation customisation : customisations)
        {
            returnValue = customisation.getTopComponentHtml(ctrl);
            if (returnValue != null)
            {
                break;
            }
        }

        return returnValue;
    }

    global override void beforeSave(cscfga.ProductConfiguratorController ctrl)
    {
        for (cscfga.AConfigurationCustomisation customisation : customisations)
        {
            customisation.beforeSave(ctrl);
        }
    }

    global override void afterSave(cscfga.ProductConfiguratorController ctrl)
    {
        for (cscfga.AConfigurationCustomisation customisation : customisations)
        {
            customisation.afterSave(ctrl);
        }
    }

    global override System.PageReference redirectAfterFinish
        ( cscfga.ProductConfiguratorController ctrl )
    {
        System.PageReference returnValue = null;

        for (cscfga.AConfigurationCustomisation customisation : customisations)
        {
            returnValue = customisation.redirectAfterFinish(ctrl);
            if (returnValue != null)
            {
                break;
            }
        }

        return returnValue;
    }

    global override System.PageReference redirectAfterCancel
        ( cscfga.ProductConfiguratorController ctrl )
    {
        System.PageReference returnValue = null;

        for (cscfga.AConfigurationCustomisation customisation : customisations)
        {
            returnValue = customisation.redirectAfterCancel(ctrl);
            if (returnValue != null)
            {
                break;
            }
        }

        return returnValue;
    }
}