@isTest
private class CS_Test_Basket_Compare {
    private static Account testAcc;
    private static cscfga__Product_Basket__c testBasket;
    private static cscfga__Product_Basket__c testBasket1;
    private static cscfga__Product_Configuration__c config;
    private static cscfga__Product_Configuration__c config1;

    private static void createTestData() {
        CS_TestDataFactory.insertTriggerDeactivatingSetting();
        // create account
        testAcc = new Account();
        testAcc.Name = 'TestBasketCompareAccount';
        testAcc.NumberOfEmployees = 10;
        insert testAcc;
        
        // Create OLI_Sync__c
        OLI_Sync__c oliSetting = CS_TestDataFactory.generateOLISync(true);
        CS_Solution_Calculations__c csSolCalc = CS_TestDataFactory.generateSolutionCalculation(true);
        
        // create opportunity
        Opportunity testOpp = new Opportunity();    
        testOpp.Name = 'Test basket compare opp: ' + testAcc.Name + ':'+datetime.now();
        testOpp.AccountId = testAcc.Id;
        testOpp.CloseDate = System.today();
        testOpp.StageName = 'Closed Won';   //@TODO - This should be a configuration
        testOpp.TotalOpportunityQuantity = 0; // added this as validation rule was complaining it was missing
        insert testOpp;
        
        // create basket
        testBasket = new cscfga__Product_Basket__c();
        Datetime d = system.now();
        String strDatetime = d.format('yyyy-MM-dd HH:mm:ss');
        testBasket.Name = 'Test basket1' + strDatetime;
        testBasket.cscfga__Opportunity__c = testOpp.Id;
        insert testBasket;
        
        
        // create basket
        testBasket1 = new cscfga__Product_Basket__c();
        Datetime d1 = system.now();
        String strDatetime1 = d1.format('yyyy-MM-dd HH:mm:ss');
        testBasket1.Name = 'Test basket2 ' + strDatetime1;
        testBasket1.cscfga__Opportunity__c = testOpp.Id;
        insert testBasket1;
        
        createProductConfigurations();
    }

    private static void createProductConfigurations() {
        String offerId = null;
        String offerId1 = null;
        cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c(Name='Test Definition 1', cscfga__Description__c = 'Test helper');
        insert prodDef;
        cscfga__Product_Definition__c prodDefNew = new cscfga__Product_Definition__c(Name='Test Definition 2', cscfga__Description__c = 'Test helper');
        insert prodDefNew;
        
         cscfga__Product_Definition__c prodDef1 = new cscfga__Product_Definition__c(Name='Test Definition 3', cscfga__Description__c = 'Test helper');
        insert prodDef1;
        cscfga__Product_Definition__c prodDefNew1 = new cscfga__Product_Definition__c(Name='Test Definition 4', cscfga__Description__c = 'Test helper');
        insert prodDefNew1;
        
        config = new cscfga__Product_Configuration__c();
        config.cscfga__Product_Basket__c = testBasket.Id;
        config.cscfga__Product_Definition__c = prodDef.Id;
        if (offerId != null)
            config.cscfga__Configuration_Offer__c = offerId;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.cscfga__Unit_Price__c = 10;
        config.cscfga__Quantity__c = 1;
        config.cscfga__Recurrence_Frequency__c = 12;
        insert config;
        
        cscfga__Product_Configuration__c subConfig = new cscfga__Product_Configuration__c();
        subConfig.cscfga__Product_Basket__c = testBasket.Id;
        subConfig.cscfga__Product_Definition__c = prodDefNew.Id;
        if (offerId1 != null)
            subConfig.cscfga__Configuration_Offer__c = offerId;
        subConfig.cscfga__Configuration_Status__c = 'Valid';
        subConfig.cscfga__Unit_Price__c = 10;
        subConfig.cscfga__Quantity__c = 1;
        subConfig.cscfga__Recurrence_Frequency__c = 12;
        subConfig.cscfga__Root_Configuration__c = config.Id;
        insert subConfig;
        
        
        config1 = new cscfga__Product_Configuration__c();
        config1.cscfga__Product_Basket__c = testBasket1.Id;
        config1.cscfga__Product_Definition__c = prodDef1.Id;
        if (offerId1 != null)
            config1.cscfga__Configuration_Offer__c = offerId;
        config1.cscfga__Configuration_Status__c = 'Valid';
        config1.cscfga__Unit_Price__c = 10;
        config1.cscfga__Quantity__c = 1;
        config1.cscfga__Recurrence_Frequency__c = 12;
        insert config1;
        
        cscfga__Product_Configuration__c subConfig1 = new cscfga__Product_Configuration__c();
        subConfig1.cscfga__Product_Basket__c = testBasket1.Id;
        subConfig1.cscfga__Product_Definition__c = prodDefNew1.Id;
        if (offerId != null)
            subConfig1.cscfga__Configuration_Offer__c = offerId;
        subConfig1.cscfga__Configuration_Status__c = 'Valid';
        subConfig1.cscfga__Unit_Price__c = 10;
        subConfig1.cscfga__Quantity__c = 1;
        subConfig1.cscfga__Recurrence_Frequency__c = 12;
        subConfig1.cscfga__Root_Configuration__c = config1.Id;
        insert subConfig1;
    }

    private static testMethod void testBasketCompareCtrl() {
        CreateTestData();
        
        Test.StartTest();
        
        PageReference pageRef = Page.CS_Basket_Compare;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', testBasket.cscfga__Opportunity__c);
         ApexPages.currentPage().getParameters().put('basketId', testBasket1.Id);
        CS_Basket_Compare ctrl = new CS_Basket_Compare(new ApexPages.StandardController(testBasket));
        ctrl.compareBaskets();
        System.assertEquals(true, ctrl.getnoRecords());
        ctrl.removeBasket();
        ctrl.removeAllBaskets();
        System.assertEquals(0, ctrl.selectedBaskets.size());
       
        Test.StopTest();
    }

}