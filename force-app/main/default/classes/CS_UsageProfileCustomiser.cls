/**
 * Apex class used to insert HTML elements for Usage profile
 
 * NO LONGER USED
 * 
 * @author Kristijan Kosutic
 */
global class CS_UsageProfileCustomiser extends cscfga.AConfigurationCustomisation {
    public void coverage() {}
	/*
	
	 * Method returns html representation of usage charges and costs. This will be mapped
	 * to atributes using JS actions. 
	 *
	 * Usage Profile is taken either from Account or from the 
	 * default one (lookup on Service Plan). Rate Card with Rate Card Lines are linked through 
	 * Price Item Rate Card association. If there is one linked to the Account, that one will be used
	 * instead of default one. 
	 *
	 * @param ctrl
	 * @return String html to insert on configurator page
	 
	global override String getTopComponentHtml(cscfga.ProductConfiguratorController ctrl) {
		cscfga.ProductConfiguration pc = ctrl.getRootConfig();
		String servicePlanId;
		String accountId;
		List<Schema.FieldSetMember> voiceFields = SObjectType.Usage_Profile__c.FieldSets.Voice.getFields();
		List<Schema.FieldSetMember> smsFields = SObjectType.Usage_Profile__c.FieldSets.SMS.getFields();
		List<Schema.FieldSetMember> dataFields = SObjectType.Usage_Profile__c.FieldSets.Data.getFields();
		cscfga__Product_Configuration__c prodConfig = pc.getSObject();
		cscfga__Product_Basket__c basket = ctrl.basket;
		Opportunity opp = [select Id, AccountId from Opportunity where id = :basket.cscfga__Opportunity__c];
		// edit and new behave differnetly -> need to re-fetch object if it's in edit mode
		if (pc.getSObject().Id == null) {
			servicePlanId = prodConfig.Service_Plan__c;
			accountId = opp.AccountId;
		} else {
			List<cscfga__Product_Configuration__c> pcList = [Select Id, Service_Plan__c, Account__c from cscfga__Product_Configuration__c where Id = :pc.getSObject().Id];
			if (!pcList.isEmpty()) {
				prodConfig =  pcList[0];
				servicePlanId = prodConfig.Service_Plan__c;
				accountId = opp.AccountId;
			}
		}
		system.debug('accountId + ' + ctrl.basket);
		String html = 'null';
		Map<String, Decimal> usageTotals = new Map<String, Decimal>();
		
		if (servicePlanId != null) {
			List<cspmb__Price_Item__c> priceItems = [Select Id, Default_Usage_Profile__c from cspmb__Price_Item__c where Id = :servicePlanId];
			cspmb__Price_Item__c servicePlan;
			if (!priceItems.isEmpty()) {
				servicePlan = priceItems[0];
				List<Usage_Profile__c> usageProfiles = Database.query('select ' 
					+ CS_Utils.getSobjectFields('Usage_Profile__c') + ' from Usage_Profile__c where Id = \'' 
					+ servicePlan.Default_Usage_Profile__c + '\'' + 
					'or Account__c = \'' + accountId + '\'');
				List<cspmb__Price_Item_Rate_Card_Association__c> rateCardAssociations = [Select 
					Id, cspmb__Rate_Card__c, cspmb__Price_Item__c, cspmb__Rate_Card__r.cspmb__Account__c 
					from cspmb__Price_Item_Rate_Card_Association__c 
					where cspmb__Price_Item__c = :servicePlanId];
				Id defaultRateCard;
				Id accountRateCard;
				for (cspmb__Price_Item_Rate_Card_Association__c rca : rateCardAssociations) {
					if (rca.cspmb__Rate_Card__r.cspmb__Account__c == accountId) {
						accountRateCard = rca.cspmb__Rate_Card__c;
					} else if (rca.cspmb__Rate_Card__r.cspmb__Account__c == null) {
						defaultRateCard = rca.cspmb__Rate_Card__c;
					}
				}
				Id rateCardId; 
				if (accountRateCard != null) {
					rateCardId = accountRateCard;
				} else {
					rateCardId = defaultRateCard;
				}
				List<cspmb__Rate_Card_Line__c> rateCardLines = new List<cspmb__Rate_Card_Line__c>();
				Map<String, cspmb__Rate_Card_Line__c> rateCardLinesMap = new Map<String, cspmb__Rate_Card_Line__c>();
				if (!rateCardAssociations.isEmpty()) {
					rateCardLines = Database.query('select ' + CS_Utils.getSobjectFields('cspmb__Rate_Card_Line__c') + 
						' from cspmb__Rate_Card_Line__c where cspmb__Rate_Card__c = \'' + 
						rateCardId + '\'');
					for (cspmb__Rate_Card_Line__c rl : rateCardLines) {
						rateCardLinesMap.put(rl.Name, rl);
					}
				}
				if (!usageProfiles.isEmpty()) {
					Usage_Profile__c usageProfile = usageProfiles[0];
					for (Schema.FieldSetMember flm : voiceFields) {
						String costPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost__c';
						String chargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge__c';
						String totalChargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge_0';
						String totalCostPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost_0';
						Decimal totalCost = 0;
						Decimal totalCharge = 0 ;
						if (usageProfile.get('Expected_Voice__c') != null && usageProfile.get(flm.getFieldPath()) != null) {
							totalCharge = (Decimal) usageProfile.get('Expected_Voice__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
							totalCost = (Decimal) usageProfile.get('Expected_Voice__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
						}
						if (rateCardLinesMap.get('Voice') != null && rateCardLinesMap.get('Voice Interconnect') != null) {
							Decimal chargeMultiplier = 0;
							Decimal costMultiplier = 0;
							if (rateCardLinesMap.get('Voice').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('Voice').get(chargePath);
							}
							if (rateCardLinesMap.get('Voice Interconnect').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('Voice Interconnect').get(chargePath);
							}
							if (rateCardLinesMap.get('Voice').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('Voice').get(costPath);
							}
							if (rateCardLinesMap.get('Voice Interconnect').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('Voice Interconnect').get(costPath);
							}
							totalCharge *= chargeMultiplier / 100;
							totalCost *= costMultiplier / 100;
						}
						usageTotals.put(totalChargePath, totalCharge.setScale(2));
						usageTotals.put(totalCostPath, totalCost.setScale(2));
					}
					for (Schema.FieldSetMember flm : smsFields) {
						String costPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost__c';
						String chargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge__c';
						String totalChargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge_0';
						String totalCostPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost_0';
						Decimal totalCharge = 0;
						Decimal totalCost = 0;
						if (usageProfile.get('Expected_SMS__c') != null && usageProfile.get(flm.getFieldPath()) != null) {
							totalCharge = (Decimal) usageProfile.get('Expected_SMS__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
							totalCost = (Decimal) usageProfile.get('Expected_SMS__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
						}
						if (rateCardLinesMap.get('SMS') != null && rateCardLinesMap.get('SMS Interconnect') != null) {
							Decimal chargeMultiplier = 0;
							Decimal costMultiplier = 0;
							if (rateCardLinesMap.get('SMS').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('SMS').get(chargePath);
							}
							if (rateCardLinesMap.get('SMS Interconnect').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('SMS Interconnect').get(chargePath);
							}
							if (rateCardLinesMap.get('SMS').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('SMS').get(costPath);
							}
							if (rateCardLinesMap.get('SMS Interconnect').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('SMS Interconnect').get(costPath);
							}
							totalCharge *= chargeMultiplier / 100;
							totalCost *= costMultiplier / 100;
						}
						usageTotals.put(totalChargePath, totalCharge.setScale(2));
						usageTotals.put(totalCostPath, totalCost.setScale(2));
					}
					for (Schema.FieldSetMember flm : dataFields) {
						String costPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost__c';
						String chargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge__c';
						String totalChargePath = flm.getFieldPath().substringBeforeLast('__c') + '_Charge_0';
						String totalCostPath = flm.getFieldPath().substringBeforeLast('__c') + '_Cost_0';
						Decimal totalCharge = 0;
						Decimal totalCost = 0;
						if (usageProfile.get('Expected_Data__c') != null && usageProfile.get(flm.getFieldPath()) != null) {
							totalCharge = (Decimal) usageProfile.get('Expected_Data__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
							totalCost = (Decimal) usageProfile.get('Expected_Data__c') * (Decimal) usageProfile.get(flm.getFieldPath()) / 100;
						}
						if (rateCardLinesMap.get('Data') != null && rateCardLinesMap.get('Data Interconnect') != null) {
							Decimal chargeMultiplier = 0;
							Decimal costMultiplier = 0;
							if (rateCardLinesMap.get('Data').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('Data').get(chargePath);
							}
							if (rateCardLinesMap.get('Data Interconnect').get(chargePath) != null) {
								chargeMultiplier += (Decimal) rateCardLinesMap.get('Data Interconnect').get(chargePath);
							}
							if (rateCardLinesMap.get('Data').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('Data').get(costPath);
							}
							if (rateCardLinesMap.get('Data Interconnect').get(costPath) != null) {
								costMultiplier += (Decimal) rateCardLinesMap.get('Data Interconnect').get(costPath);
							}
							totalCharge *= chargeMultiplier / 100;
							totalCost *= costMultiplier / 100;
						}
						usageTotals.put(totalChargePath, totalCharge.setScale(2));
						usageTotals.put(totalCostPath, totalCost.setScale(2));
					}
					html = JSON.serialize(usageTotals);
				}
			}
		}
		html = '<script>usageTotals = ' + html + '; ';
		html += 'console.log(usageTotals);';
		html += 'function setAttributeValues(usageTotals)'; 
		html += '{var attributeName = jQuery(\'[id$=\"Name_0\"]\').attr(\'name\');';
		html += 'var childPrefix = attributeName.substr(0, attributeName.indexOf(\':\'));';
		html += 'usageTotalKeys = Object.keys(usageTotals);';
		html += 'for (key in usageTotalKeys) ';
		html += '{var fieldName = childPrefix + \":\" + usageTotalKeys[key];';
		html += 'if (fieldName.indexOf(\"function\") == -1)';
		html += '{CS.setAttribute(fieldName, usageTotals[usageTotalKeys[key]]);}}}';
		html += '</script>';
		return html;
	}
	
	
	 * Method creates attachment on root configuration. 
	 *
	 * Child configurations values are stored in JSON format and will be used on parent to 
	 * populate some values or for validations when second child will be created. We will
	 * use Customiser on parent to access this and extract relevant data. Attachments are
	 * used to avoid limitation of salesforce long text area fields.
	 *
	 * @param ctrl
	
	global override void afterSave(cscfga.ProductConfiguratorController ctrl) {
		
		cscfga.ProductConfiguration root = ctrl.getRootConfig();
		cscfga.ProductConfiguration pc = ctrl.getConfig();
		String definitionName = pc.getSObject().cscfga__Product_Definition__r.Name;
		cscfga__Product_Basket__c basket = ctrl.basket;
		system.debug('root ' + root);
		system.debug('root ' + root.getSObject());
		
		// Collect only child configuration data
		if (!pc.isRoot()) {
			Map<String, CS_Related_Product_Attributes__c> relatedProductAttributes = CS_Related_Product_Attributes__c.getall();
			Map<String, String> attributes = new Map<String, String>();
			
			if (relatedProductAttributes.get(definitionName) != null && relatedProductAttributes.get(definitionName).Attribute_Names__c != null) {
				List<String> attributeNames = relatedProductAttributes.get(definitionName).Attribute_Names__c.split(',');
				for (String attributeName : attributeNames) {
					if (pc.containsAttribute(attributeName)) {
						cscfga.Attribute att = pc.getAttribute(attributeName);
						attributes.put(att.getSObject().Name, att.getValue());
					}
				}
			}
			List<Attachment> attList = [Select Id, Name, body from Attachment where ParentId = :basket.Id and Name = :definitionName];
			Attachment attachment;
			if (!attList.isEmpty()) {
				attachment = attList[0];
			}
			if (attachment == null) {
				attachment = new Attachment(
					Name = definitionName,
					ParentId = root.getSObject().Id,
					body = Blob.valueOf('')
				);
			}
			if (attachment.body.toString() != null && attachment.body.toString() != '') {
				Map<String, Map<String, String>> attBody = (Map<String, Map<String, String>>) JSON.deserialize(attachment.body.toString(), Map<String, Map<String, String>>.class);
				attBody.put(pc.getSObject().Id, attributes);
				attachment.body = Blob.valueOf(JSON.serialize(attBody));
			} else {
				Map<String, Map<String, String>> attBody = new Map<String, Map<String, String>>();
				attBody.put(pc.getSObject().Id, attributes);
				attachment.body = Blob.valueOf(JSON.serialize(attBody));
			}
			upsert attachment;
		}
		if (pc.containsAttribute('Customiser Processed')) {
			cscfga.Attribute att = pc.getAttribute('Customiser Processed');
			att.setValue('No');
			upsert att.getSObject();
			if (!pc.isRoot()) {
				cscfga.ProductConfiguration parent = pc.getParent();
				if (parent.containsAttribute('Customiser Processed')) {
					cscfga.Attribute att2 = parent.getAttribute('Customiser Processed');
					att2.setValue('No');
					upsert att2.getSObject();
				}
			}
		}
	}
	
	*/

}