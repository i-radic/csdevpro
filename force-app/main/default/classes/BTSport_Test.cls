@isTest(SeeAllData=true)
private class BTSport_Test {

    static BT_Sport_Pricing__c CreateOffer() {
        BT_Sport_Pricing__c offer = new BT_Sport_Pricing__c();
        offer.Entry_Type__c = 'Discount';
        offer.Start_Date__c = (Date.today()-10);
        offer.End_Date__c = (Date.today()+10);
        offer.Discount_Name__c = '15 percent discount test offer';
        offer.Discount_Amount__c = 10.00;
        offer.Discount_Period__c = 1;
        offer.UnavailableContractTypes__c = 'Pay as you go monthly::3 month contract::6 month contract::12 month contract with June and July 2017 FREE::18 month contract::24 month contract::24 Month Bundle::';
        offer.Discount_Period_Months__c = 12;
        return offer;        
    }
    
    
       testmethod static void Test_FlagIsSetExist(){
        	Test_Factory.setProperty('IsTest', 'yes');
			BT_Sport_Pricing__c ofr = CreateOffer();
            Database.SaveResult[] offerResult = Database.insert(new BT_Sport_Pricing__c [] {ofr});        
        //StaticVariables.setContactIsRunBefore(False);     
        
        RecordType BTSOppyType = [select id from RecordType where SobjectType='Opportunity' and name ='BT Sport' limit 1];
        RecordType BTSHeaderType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='Header' limit 1];
        RecordType BTSNonMSAType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='non MSA' limit 1];
//      RecordType BTSMSAType = [select id from RecordType where SobjectType='BT_Sport__c' and name ='MSA' limit 1];           
        
        Date uDate = date.today().addDays(7);                
        Opportunity newOppty = new Opportunity();
        newOppty.Name = 'BTS';
        newOppty.StageName = 'Created';
        newOppty.CloseDate = uDate;
        newOppty.RecordTypeId = BTSOppyType.Id;
        Database.SaveResult[] oppResult = Database.insert(new Opportunity [] {newOppty});  
        
              
        BT_Sport__c Header1 = new BT_Sport__c();
        Header1.Opportunity__c = oppResult[0].id; 
        Header1.RecordTypeId = BTSHeaderType.id;
        Header1.BT_Sport_MSA_Oppy__c = 'No';
        Database.SaveResult[] Header1Result = Database.insert(new BT_Sport__c [] {Header1});        
              
        BT_Sport__c Site1 = new BT_Sport__c();
        Site1.BT_Sport_Header__c = Header1Result[0].id; 
        Site1.RecordTypeId = BTSNonMSAType.id;
        Site1.Site_Type__c = 'BT Sport Pack Pub (GB)';
        Site1.Contract_Type__c = '12 month contract';
        Site1.Band__c = 'Band A - £0 - £5,000';
        Site1.Discounts__c = '15 percent discount test offer';
        Site1.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site1.Site_Display_Name__c = 'TESTER';
        Site1.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site1Result = Database.insert(new BT_Sport__c [] {Site1});
        
        BT_Sport__c Site1a = new BT_Sport__c();
        Site1a.BT_Sport_Header__c = Header1Result[0].id; 
        Site1a.RecordTypeId = BTSNonMSAType.id;
        Site1a.Site_Type__c = 'BT Sport Pack Hotel Rooms';
        Site1a.Contract_Type__c = '12 month contract';
        Site1a.Band__c = '';
        Site1a.Discounts__c = '';
        Site1a.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site1a.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site1a.Site_Display_Name__c = 'TESTER';
        Site1a.Site_Telephone__c = '02085878834';
        //Database.SaveResult[] Site1aResult = Database.insert(new BT_Sport__c [] {Site1a});  
        
        BT_Sport__c Site1b = new BT_Sport__c();
        Site1b.BT_Sport_Header__c = Header1Result[0].id; 
        Site1b.RecordTypeId = BTSNonMSAType.id;
        Site1b.Site_Type__c = 'BT Sport Pack General';
        Site1b.Contract_Type__c = '12 month contract';
        Site1b.Band__c = '';
        Site1b.Discounts__c = '15 percent discount test offer';
        Site1b.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site1b.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site1b.Site_Display_Name__c = 'TESTER';
        Site1b.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site1bResult = Database.insert(new BT_Sport__c [] {Site1b});  
    
        BT_Sport_Pricing__c BTSoprtPricing = new BT_Sport_Pricing__c();
        BTSoprtPricing.Discount_Amount__c = 0;
        BTSoprtPricing.Discount_Period__c = 0;
        BTSoprtPricing.Discount_Name__c = 'Hero Offer';
        BTSoprtPricing.UnavailableSiteTypes__c = '';
        BTSoprtPricing.UnavailableContractTypes__c = '';
        BTSoprtPricing.Entry_Type__c = 'Discount';
        BTSoprtPricing.Start_Date__C = (Date.Today())-1;
        BTSoprtPricing.End_Date__C = (Date.Today())+1 ;
        
        Database.SaveResult[] PricingResult = Database.insert(new BT_Sport_Pricing__c [] {BTSoprtPricing});
        
        
        
        BT_Sport__c Site1c = new BT_Sport__c();
        Site1c.BT_Sport_Header__c = Header1Result[0].id; 
        Site1c.RecordTypeId = BTSNonMSAType.id;
        Site1c.Site_Type__c = 'BT Sport Pack Golf Club (NI)';
        Site1c.Contract_Type__c = '12 month contract';
        Site1c.Band__c = 'Band A - £1 - £40,000';
        Site1c.Discounts__c = 'Hero Offer';
        Site1c.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site1c.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site1c.Site_Display_Name__c = 'TESTER';
        Site1c.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site1cResult = Database.insert(new BT_Sport__c [] {Site1c});  
          
        // MSA no longer required
/*
        BT_Sport__c Header2 = new BT_Sport__c();
        Header2.Opportunity__c = oppResult[0].id; 
        Header2.RecordTypeId = BTSHeaderType.id;
        Header2.BT_Sport_MSA_Oppy__c = 'Yes';
        Header2.BT_Sport_General_MSA_Discount__c = '10';
        Header2.BT_Sport_Cat_2_Pub_MSA_Discount__c = '20';
        Header2.BT_Sport_General_MSA_Term__c = '1 Year';
        Header2.BT_Sport_Cat_2_MSA_Term__c = '1 Year';
        Database.SaveResult[] Header2Result = Database.insert(new BT_Sport__c [] {Header2});        
              
        BT_Sport__c Site2a = new BT_Sport__c();
        Site2a.BT_Sport_Header__c = Header2Result[0].id; 
        Site2a.RecordTypeId = BTSMSAType.id;
        Site2a.Site_Type__c = 'BT Sport Pack Pub - Category 2 (GB)';
        Site2a.Contract_Type__c = '12 month contract';
        Site2a.Band__c = 'Band A - £0 - £5000';
        Site2a.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site2a.Site_Display_Name__c = 'TESTER';
        Site2a.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site2aResult = Database.insert(new BT_Sport__c [] {Site2a});  
                 
        BT_Sport__c Site2b = new BT_Sport__c();
        Site2b.BT_Sport_Header__c = Header2Result[0].id; 
        Site2b.RecordTypeId = BTSMSAType.id;
        Site2b.Site_Type__c = 'BT Sport Pack Pub (GB)';
        Site2b.Contract_Type__c = '12 month contract';
        Site2b.Band__c = 'Band A - £0 - £5000';
        Site2b.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site2b.Site_Display_Name__c = 'TESTER';
        Site2b.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site2bResult = Database.insert(new BT_Sport__c [] {Site2b});  
        
        BT_Sport__c Site2c = new BT_Sport__c();
        Site2c.BT_Sport_Header__c = Header2Result[0].id; 
        Site2c.RecordTypeId = BTSMSAType.id;
        Site2c.Site_Type__c = 'BT Sport Pack Hotel Bar';
        Site2c.Contract_Type__c = '12 month contract';
        Site2c.Band__c = 'Band A - 1 rooms - 10 rooms';
        Site2c.Number_of_Rooms_receiving_BT_Sport__c = 4;
        Site2c.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site2c.Site_Display_Name__c = 'TESTER';
        Site2c.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site2cResult = Database.insert(new BT_Sport__c [] {Site2c});  
        
        
     delete Site2c;
        
        BT_Sport__c Header3 = new BT_Sport__c();
        Header3.Opportunity__c = oppResult[0].id; 
        Header3.RecordTypeId = BTSHeaderType.id;
        Header3.BT_Sport_MSA_Oppy__c = 'Yes';
        Header3.BT_Sport_General_MSA_Discount__c = '2';
        Header3.BT_Sport_Cat_2_Pub_MSA_Discount__c = '2';
        Header3.BT_Sport_General_MSA_Term__c = '1 Year';
        Header3.BT_Sport_Cat_2_MSA_Term__c = '1 Year';
        Database.SaveResult[] Header3Result = Database.insert(new BT_Sport__c [] {Header3});        
              
        BT_Sport__c Site3a = new BT_Sport__c();
        Site3a.BT_Sport_Header__c = Header3Result[0].id; 
        Site3a.RecordTypeId = BTSMSAType.id;
        Site3a.Site_Type__c = 'BT Sport Pack Pub - Category 2 (GB)';
        Site3a.Contract_Type__c = '12 month contract';
        Site3a.Band__c = 'Band A - £0 - £5000';
        Site3a.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site3a.Site_Display_Name__c = 'TESTER';
        Site3a.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site3aResult = Database.insert(new BT_Sport__c [] {Site3a});  
        
        
        BT_Sport__c Site3c = new BT_Sport__c();
        Site3c.BT_Sport_Header__c = Header3Result[0].id; 
        Site3c.RecordTypeId = BTSMSAType.id;
        Site3c.Site_Type__c = 'BT Sport Pack Pub (GB)';
        Site3c.Contract_Type__c = '12 month contract';
        Site3c.Band__c = 'Band A - £0 - £5000';
        Site3c.Licensed_Marketing_Toolkit_Consent__c = 'Yes';
        Site3c.Site_Display_Name__c = 'TESTER';
        Site3c.Site_Telephone__c = '02085878834';
        Database.SaveResult[] Site3cResult = Database.insert(new BT_Sport__c [] {Site3c});
 */     


StaticVariables.setTaskIsRun(false);

/*
       List<BT_Sport__c> SportHeader = [Select BT_Sport_General_MSA_Discount__c, BT_Sport_Cat_2_Pub_MSA_Discount__c from BT_Sport__c where  id = :Header3Result[0].id limit 1];
    
for (BT_Sport__c sp : SportHeader)  { 
         system.debug('ADJ TEST Before MSA update = '+ SportHeader);
    sp.BT_Sport_General_MSA_Discount__c = '5';
    sp.BT_Sport_Cat_2_Pub_MSA_Discount__c = '10';
   
      }
        system.debug('ADJ TEST After MSA update = '+ SportHeader);
       update SportHeader;
}

       */    
           
       }         
   
        }