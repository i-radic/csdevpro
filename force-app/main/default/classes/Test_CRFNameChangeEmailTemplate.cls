@isTest

private class Test_CRFNameChangeEmailTemplate{    
    
    static testMethod void myUnitTest(){
    Contact c = Test_Factory.CreateContact();
        c.Contact_Building__c = '1';
        c.Contact_Sub_Building__c = '2';
        c.Contact_Address_Number__c = '3';
        c.Address_POBox__c = '4';
        c.Contact_Address_Street__c = '5';
        c.Contact_Locality__c = '6';
        c.Contact_Post_Town__c = '7';
        c.Contact_Post_Code__c = '8';
        c.Address_County__c = '9';
        c.Address_Country__c = '10';
        c.Email = 'test12345@test.com';
        c.Phone = '9874562230';
        c.MobilePhone = '4567891230';
        insert c;
        opportunity o = new opportunity();
        o.name='test';
        o.StageName='FOS_Stage';
        o.CloseDate = system.today();
        insert o;
          
        CRF__c crf = new CRF__c();
        crf.RecordTypeId = [Select Id from RecordType where DeveloperName='Name_Change_CRF'].Id;
        crf.Opportunity__c = o.Id;       
        crf.Contact__c = c.Id;
        crf.Why_was_customer_not_signed_up_on_DD__c = 'Customer not interested in DD';
        insert crf;
        
        /*
        CRF__c crf1 = new CRF__c();
        crf1.RecordTypeId = [Select Id from RecordType where DeveloperName='Number_Port'].Id;
        crf1.Opportunity__c = o.Id;
        crf1.Vol_ref_Mover__c='BT';
        insert crf1;
        */
               
        CRFNameChangeEmailTemplate.SendEmail(crf.Id);
        //CRFNameChangeEmailTemplate.SendEmailNumberPort(crf1.Id);
    }
}