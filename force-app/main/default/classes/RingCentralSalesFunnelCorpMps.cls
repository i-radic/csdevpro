public class RingCentralSalesFunnelCorpMps {
    public CloudPhone_RC_Deal_Management__c rcDealMan;
    public CloudPhone_RC_Deal_Management__c rcDealMan1;
    public String sign {get; set;}
    public String customerFirstName {get; set;}
    public String customerLastName {get; set;}
    public String customerEmail {get; set;}
    public String customerContactNumber {get; set;}
    public String customerCompanyName {get; set;}
    public String oppID {get; set;}
    public String cug {get; set;}
    public String salesAgentID {get; set;}
    public String lbID {get; set;}
    public String numberUsers {get; set;}
    public String editionType {get; set;}
    public String testID {get; set;}
    public String mosScore {get; set;}
    public String simultaneousUsers {get; set;}
    public String bandwidthResult {get; set;}
    public String accountActivationDateDD {get; set;}
    public String accountActivationDateMM {get; set;}
    public String accountActivationDateYYYY {get; set;}
    public String accountActivationDate {get; set;}
    public String contractEndDateDD {get; set;}
    public String contractEndDateMM {get; set;}
    public String contractEndDateYYYY {get; set;}
    public String contractEndDate {get; set;}
    public String initialPurchaseCreditLimit {get; set;}
    public String recurringMonthlyCreditLimit {get; set;}
    public String referenceNoteCreditChange {get; set;} 
    public String rcDealID {get; set;}  
    public String rcdmid= System.currentPageReference().getParameters().get('id');
    public String url {get; set;}
    public Project__c pro{get; set;}
    public String proID {get; set;}
    public Id recordTypeId{get; set;}
    
    
    public RingCentralSalesFunnelCorpMps(ApexPages.StandardController controller) {
        this.rcDealMan= [SELECT ID,RingCentralDealRegContact__r.id, RCDeal_ID__c,   Company_Name__c,Licence_Type__c, Opportunity__r.Account.Name, Number_of_Users__c, Opportunity__r.Account_CUG__c, Opportunity__r.Account.AccountClonedID__c, Opportunity__r.Opportunity_Id__c, Opportunity__r.Owner__r.EIN__c, Opportunity__r.Owner__r.Department FROM  CloudPhone_RC_Deal_Management__c WHERE ID =: rcdmid];
        this.rcDealMan1= (CloudPhone_RC_Deal_Management__c)controller.getRecord();
    }

    public void SetTestHarnessValues(){
        //--------RC Security Validation
        Blob var1 = Blob.valueOf('12345qwerty');
        Blob var2 = Blob.valueOf('12345qwerty|');
        Blob signature = System.Crypto.signWithCertificate('RSA-SHA256', var1, 'SalesFunnel');
        sign = EncodingUtil.base64Encode(var2) + EncodingUtil.base64Encode(signature);
    }    

    public void SetValues(){
        ////Cloud_Voice_Site__c site = getCVSites()[0];
        //--------RC Security Validation
        Blob var1 = Blob.valueOf('12345qwerty');
        Blob var2 = Blob.valueOf('12345qwerty|');
        Blob signature = System.Crypto.signWithCertificate('RSA-SHA256', var1, 'SalesFunnel');
        sign = EncodingUtil.base64Encode(var2) + EncodingUtil.base64Encode(signature);
        //--------Endpoint URL
        RingCentralURL__c RSUrl = null;
        if (Test.isRunningTest()){   
            RSUrl = [select Name, Username__c, Password__c, Url__c, Partner_Login_Url__c from RingCentralURL__c where Name = 'testRCurl'];
          
            }else{
            RSUrl = [select Name, Username__c, Password__c, Url__c from RingCentralURL__c where Name = 'SalesFunnel'];
        }
        url = String.valueOf(RSUrl.Url__c);
        //--------User
        salesAgentID = rcDealMan.Opportunity__r.Owner__r.EIN__c;
        //--------Contact
        Contact contact = [SELECT FirstName, LastName, Email ,Phone FROM Contact WHERE ID =:   rcDealMan.RingCentralDealRegContact__r.id];
        customerFirstName = contact.FirstName;
        customerLastName = contact.LastName;
        customerEmail = contact.Email;
        customerContactNumber = contact.Phone;
        //--------Other details
        if (rcDealMan.Company_Name__c.length() > 64){
            customerCompanyName = rcDealMan.Company_Name__c.SubString(0, 64);
        }
        else
        {
            customerCompanyName = rcDealMan.Company_Name__c;
        } 
        numberUsers =  String.valueOf(rcDealMan.Number_of_Users__c);
        oppID = rcDealMan.Opportunity__r.Opportunity_Id__c;
        cug = rcDealMan.Opportunity__r.Account_CUG__c;
       lbID = rcDealMan.Opportunity__r.Owner__r.Department;
        //--------CUG fix for Business Partner Sales (BPS)
      
            ////cug = cvRec.Opportunity__r.Account.AccountClonedID__c;
        
        //--------Product
        
        //--------Account Activation Date
        
        editionType = rcDealMan.Licence_Type__c;
        //--------Bandwidth
        ////testID = cvRec.zBandwidthID__c;
        ////mosScore = cvRec.zBandwidthMosScore__c;
        ////simultaneousUsers = cvRec.zBandwidthSimultaneousUsers__c;
        //-------- Credit - change to new default values and catch inflight missing
    
        initialPurchaseCreditLimit = '0';  
        recurringMonthlyCreditLimit ='0';
        referenceNoteCreditChange = 'none';  
        System.debug('Opportunity__c = ' +rcDealMan.Opportunity__r.id );
        // Default date for MPS orders
        
        Date dateMPS =  System.TODAY() +60;
        System.debug('dateMPS date= ' + dateMPS );
        accountActivationDateDD = String.valueOf(dateMPS.DAY());
        accountActivationDateMM = String.valueOf(dateMPS.MONTH());
        accountActivationDateYYYY = String.valueOf(dateMPS.YEAR());
             

        try{
             pro = [Select id,name,Customer_Required_By_Date__c  from Project__c where Opportunity__c =: rcDealMan.Opportunity__r.id  and RecordType.developerName ='Implementation'   LIMIT 1];
           
             if ( (pro.Customer_Required_By_Date__c != null))
             {
                System.debug('project number= ' + pro.id);
                System.debug('project date= ' + pro.Customer_Required_By_Date__c);
                accountActivationDateDD = String.valueOf(pro.Customer_Required_By_Date__c.day());
                accountActivationDateMM = String.valueOf(pro.Customer_Required_By_Date__c.month());
                accountActivationDateYYYY = String.valueOf(pro.Customer_Required_By_Date__c.year());
             }
             
                
        }catch(exception e){}
        //--------Shipping Address
         /* shippingAddressLine1 = contact.Contact_Address_Number__c;
        shippingAddressLine2 = contact.Contact_Address_Street__c;
        shippingAddressCity = contact.Contact_Post_Town__c;
        shippingAddressCounty = contact.Address_County__c;
        shippingAddressPostCode = contact.Contact_Post_Code__c;*/ 
        rcDealID = rcDealMan.RCDeal_ID__c;
   
        
        

    }

    
    public PageReference FinishButton(){
        ////update cvRec1;
        ////
        
        ////PageReference retPg = new PageReference('/' + rcDealMan.ID);
        /////        return retpg.setRedirect(true);
        return null;
       
    }
    
     public void setPro(Project__c pro)
    {
               this.pro=pro;
    }
      public void setRecordTypeId(Id recordTypeId)
    {
               this.recordTypeId=recordTypeId;
    }
}