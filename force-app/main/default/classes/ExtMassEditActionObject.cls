/*
###########################################################################
# File..................: ExtMassEditActionObject
# Version...............: 1
# Created by............: Rahul Khilwani
# Created Date..........: 28-Oct-2014
# Description...........: Extension to Handle mass Insert/Update of Action__c object
############################################################################
*/

public class ExtMassEditActionObject {
  
    public Action__c objActn {get;set;}
    public List<Action__c> cpmActions { get; set; }
    public List<WrapperClassEx> WrapperList{get;set;}
    public Map<String, List<WrapperClassTask>> TaskMap{get;set;}
    public List<Schema.FieldSetMember> entryFields{get;set;}
    public List<Schema.FieldSetMember> actionFields{get;set;}
    public String tabId{get; set{
            tabId = value;
            System.debug('value 1: '+value);
    }}
    public String plus{get; set{
            plus = value;
            System.debug('value 2: '+value);
    }}
    public String minus{get; set{
            minus = value;
            System.debug('value 3: '+value);
    }}
    
    public String meetingId{get; set{
            meetingId = value;
            System.debug('value 4: '+value);
    }}
    
    public String aId{get; set{
            aId = value;
            System.debug('value 5: '+value);
    }}
    
    public List<PRD_Entry__c> Tasks;
    public Project__c prjctId ;
    public List<String> cpmAIds;
    public Integer recordSize {get;set;}
    
    public ExtMassEditActionObject(ApexPages.StandardController stdController){
        this.prjctId = (Project__c)stdController.getRecord();
        system.debug('constructor --->');
        objActn = new Action__c();
        loadData(); 
    }
    
    private void loadData()
    {
        this.cpmActions = getCpmActions();
        this.entryFields = getTaskFields();
        this.actionFields = getFields();
        this.recordSize = this.cpmActions.size();
        
        WrapperList = New List<WrapperClassEx>();
        cpmAIds = new List<String>();
        for(Action__c cpmA : this.cpmActions ){
            cpmAIds.add(cpmA.Id);
        }
        
        this.Tasks = getTasks(cpmAIds);

        system.debug('Tasks--->' + this.Tasks);
        TaskMap = new Map<String, List<WrapperClassTask>>();
        Integer m = 1;
        for(PRD_Entry__c n: this.Tasks){
            if(TaskMap.get(n.Action__c) != null){
               List<WrapperClassTask> tempTask = TaskMap.get(n.Action__c);
               tempTask.add(new WrapperClassTask(n, m));
               TaskMap.put(n.Action__c,tempTask);
            }else{
               TaskMap.put(n.Action__c, new List<WrapperClassTask>());
               TaskMap.get(n.Action__c).add(new WrapperClassTask(n, m));
            }
            m++;
        }
        
        System.debug('TaskMap--->' + TaskMap);
        Integer a = 1;
        for(Action__c cpmA : this.cpmActions ){
            String tabId = String.valueof(a);
            System.debug('tabId ---> ' + tabId);
            if(TaskMap.get(cpmA.Id) != null)
                WrapperList.add(New WrapperClassEx(cpmA, tabId, TaskMap.get(cpmA.Id)));
            else
                WrapperList.add(New WrapperClassEx(cpmA, tabId, new List<WrapperClassTask>()));
            a++;
        }
    }
    
    
    public List<Schema.FieldSetMember> getTaskFields() {
        return SObjectType.PRD_Entry__c.FieldSets.MassCreateSubCategory.getFields();
    }
    public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Action__c.FieldSets.MassCreate.getFields();
    }
    
    private List<PRD_Entry__c> getTasks(List<String> actionIds) {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getTaskFields()) {
            query += f.getFieldPath() + ', ' ;
        }
        query += 'Action__c FROM PRD_Entry__c Where Action__c in :actionIds ORDER BY Ref__c ASC NULLS LAST';
        
        system.debug('Query' + query);
        return Database.query(query);
    }
    
    private List<Action__c> getCpmActions() {
        String query = 'SELECT ';
        for(Schema.FieldSetMember f : this.getFields()) {
            query += f.getFieldPath() + ', ' ;
        }
        query += 'Id FROM Action__c Where Project__c = \'' + this.prjctId.Id + '\'';
        
        
        if (objActn.Entry_Type__c != null)
            query += ' and Entry_Type__c = \'' + objActn.Entry_Type__c + '\'';
                    
        if (objActn.Status_Criticality__c != null)
            query += ' and Status_Criticality__c = \'' + objActn.Status_Criticality__c + '\'';

        if (objActn.Entry_View__c != null)
            query += ' and Entry_View__c = \'' + objActn.Entry_View__c + '\'';
        
        query += 'ORDER BY Ref__c ASC NULLS LAST';
                    
        system.debug('Query' + query);
        return Database.query(query);
    }
    
    public class WrapperClassEx{
        public Action__c CpmAfieldObj{get;set;}
        public List<WrapperClassTask> Tasks{get;set;}
        public String tabId{get;set;}
        public Boolean flag{get;set;}
    
        public WrapperClassEx(Action__c CpmAfieldRec, String tab, List<WrapperClassTask> allTasks){
            CpmAfieldObj = CpmAfieldRec;
            Tasks = allTasks;
            tabId = tab;
            flag = true;
        }
    }
    
    public class WrapperClassTask{
        public PRD_Entry__c meeting{get;set;}
        public Boolean flag{get;set;}
        public Integer mNum{get;set;}
        
        public WrapperClassTask(PRD_Entry__c meet, Integer mNum){
            this.meeting = meet;
            this.mNum = mNum;
            this.flag = true;
            System.debug('mNum Class --> ' + mNum);
        }
        
    }
    
    public void createNewAction(){
        Integer size = WrapperList.size() + 1;
        String tabId = string.valueof(size);
        System.debug('tabId Create--->' + tabId);
        WrapperList.add(New WrapperClassEx(New Action__c(Project__c = this.prjctId.Id), tabId, new List<WrapperClassTask>()));
    }
    
     public PageReference SaveCloseActionChanges(){
        saveAction();
        PageReference projectPage = new ApexPages.StandardController(this.prjctId).view();
        projectPage.setRedirect(true);
        return projectPage;
    }

     public PageReference SaveActionChanges(){
        saveAction();
        PageReference projectPage = new PageReference('/apex/MassEditActionObject?id=' + this.prjctId.Id);
        projectPage.setRedirect(true);
        return projectPage;
    }
    
    private void saveAction()
    {
        List<Action__c> cpmUpdatedList = new List<Action__c>();
        List<PRD_Entry__c> Tasks = new List<PRD_Entry__c>();
        List<Action__c> actionToDelete = new List<Action__c>();
        Set<ID> actionsDeleted = new Set<ID>();
        
        for(WrapperClassEx wc: WrapperList){
            
            system.debug('Flag ---> ' + wc.flag);
            system.debug('CpmAfieldObj ---> ' + wc.CpmAfieldObj);
            
            if(wc.flag == false){
                if(wc.CpmAfieldObj.Id!= null){
                    actionToDelete.add(wc.CpmAfieldObj);
                    actionsDeleted.add(wc.CpmAfieldObj.Id);
                    system.debug('actionToDelete ---> ' + wc.CpmAfieldObj);
                }
                
                if(wc.Tasks.size() > 0){
                    for(WrapperClassTask t: wc.Tasks){
                        t.flag = false;
                    }
                }
            }else{
                cpmUpdatedList.add(wc.CpmAfieldObj);
            }
        }
        system.debug('cpmUpdatedList   ----> ' + cpmUpdatedList);
        database.upsert(cpmUpdatedList,false);
        system.debug('actionToDelete   ----> ' + actionToDelete);
        database.delete(actionToDelete);
        
        /*Delete PRD Entry object first as its the child*/
        List<PRD_Entry__c> taskToDelete = new List<PRD_Entry__c>();
        for(WrapperClassEx wc: WrapperList){
            if(wc.Tasks.size()>0){
                for(WrapperClassTask n:wc.Tasks){
                    if(n.meeting.Action__c == null){
                        n.meeting.Action__c = wc.CpmAfieldObj.Id;
                    }
                    
                    if(n.flag == false ){
                        if(n.meeting.Id != null && !actionsDeleted.contains(n.meeting.Action__c)){
                            taskToDelete.add(n.meeting);
                        }
                    }else{
                        Tasks.add(n.meeting);    
                    }
                }
            }   
        }
        
        upsert Tasks;
        delete taskToDelete;
        
    }
    
    public PageReference cancelChanges(){
        PageReference projectPage = new ApexPages.StandardController(this.prjctId).view();
        projectPage.setRedirect(true);
        return projectPage;
    }
    
    public PageReference filterActions()
    {
        system.debug('Entry Type ---> ' +  objActn.Entry_Type__c );
        system.debug('Status ---> ' + objActn.Status_Criticality__c);
        loadData();
        return null; 
    }
    
    public void deleteAction(){
        String actionId = aId;
        List<WrapperClassEx> WrapperListDel = new List<WrapperClassEx>();
        Integer rowNum;
        
        WrapperListDel = WrapperList.Clone();
        system.debug('**WrapperListDelBefore' + WrapperListDel );
        
        for(rowNum =0; rowNum < WrapperList.size();rowNum ++){
            if(actionId == WrapperList[rowNum ].tabId){
               WrapperList[rowNum ].flag = false;
                if(WrapperList[rowNum ].CpmAfieldObj.Id== null)
                    //remove blank action from wrapper list
                    WrapperListDel.remove(rowNum );
 
            }
        }
         
         //adding Blank Actions for delete
         system.debug('**WrapperListBefore' + WrapperList);
         WrapperList.clear();
         WrapperList = WrapperListDel.Clone();
         system.debug('**WrapperListAfter' + WrapperList);
    }
    
    public void deleteMeeting(){
        List<String> actNmeeting = meetingId.split('_');
        system.debug('Meeting Action-- >' + actNmeeting);
        String tabId = actNmeeting[0];
        Integer mNum = Integer.valueOf(actNmeeting[1]);
        for(WrapperClassEx w: WrapperList){
            if(w.tabId == tabId){
                System.debug('TabId -->' + w.tabId);
                for(WrapperClassTask n: w.Tasks){
                    system.debug('n -->' + n);
                    if(n.mNum == mNum){
                        System.debug('nNum -->' + n.mNum);
                        n.flag = false;
                    }
                    system.debug('n -->' + n);
                }
            }
        }
    }
    
    public void createNewMeeting(){
        System.debug('get Tab -->' + tabId);
        for(WrapperClassEx wc : WrapperList){
            if(wc.tabId == tabId){
               wc.Tasks.add(new WrapperClassTask(new PRD_Entry__c(Action__c = wc.CpmAfieldObj.Id), wc.Tasks.size()+1));
            }
        }   

    }
}