global class CustomUsageProfileLookup extends cscfga.ALookupSearch {
    private String CHILD_OBJECT_NAME_CONST = 'Usage_Profile__c';
    public override String getRequiredAttributes(){
        return '["AccountId","Proposition Name","AccountsUsageProfileId"]'; 
    }
      
   public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit){
        List<Usage_Profile__c> usageProfileRecordList;
        List<Usage_Profile__c> usageProfileReturnList = new List<Usage_Profile__c>();
        String pdFields = JSON.serializePretty(searchFields);
        String propositionName = searchFields.get('Proposition Name');
        String accountId = searchFields.get('AccountId');
        String basketUsageProfile = searchFields.get('AccountsUsageProfileId');
        System.debug('*******propositionName' + propositionName);
        System.debug('*******AccountId' + AccountId);
        String objectFields = getSobjectFields(CHILD_OBJECT_NAME_CONST);
        if(objectFields == null){
            return null;
        }

        String filter = '';
        String searchTerm = searchFields.get('searchValue');
        if(searchTerm != ''){
            filter = ' AND Name LIKE \'%' + searchTerm + '%\' ';
        }
        
        String recordsQuery = 'SELECT '+ objectFields + ' FROM Usage_Profile__c WHERE Active__c = true AND (Is_Default_Profile__c = true OR Account__C = \''+accountId+'\')' + filter;
        usageProfileRecordList = Database.query(recordsQuery);
        System.debug('*******usageProfileRecordList' + usageProfileRecordList);
        for(Usage_Profile__c usProf:usageProfileRecordList){
            if(propositionName == 'Mobile Sharer' && usProf.Is_Default_Profile__c == true){
                usageProfileReturnList.add(usProf);
            }else if (propositionName == 'Mobile Flex' && usProf.Is_Default_Profile__c == false){
                usageProfileReturnList.add(usProf);
            }
        }
        System.debug('*******usageProfileReturnList' + usageProfileReturnList);
        return usageProfileReturnList;
    }  
 public override Object[] doDynamicLookupSearch(Map<String, String> searchFields,String productDefinitionID){
        List<Usage_Profile__c> usageProfileRecordList;
        List<Usage_Profile__c> usageProfileReturnList = new List<Usage_Profile__c>();
        String pdFields = JSON.serializePretty(searchFields);
        String propositionName = searchFields.get('Proposition Name');
        String accountId = searchFields.get('AccountId');
        System.debug('*******propositionName' + propositionName);
        System.debug('*******AccountId' + AccountId);
        String basketUsageProfile = searchFields.get('AccountsUsageProfileId');
        String objectFields = getSobjectFields(CHILD_OBJECT_NAME_CONST);
        if(objectFields == null){
            return null;
        }
        String recordsQuery1;
        String recordsQuery2;
        
        String filter = '';
        String searchTerm = searchFields.get('searchValue');
        if(searchTerm != ''){
            filter = ' AND Name LIKE \'%' + searchTerm + '%\' ';
        }

        if(basketUsageProfile != null){
             recordsQuery1 = 'SELECT '+ objectFields + ' FROM Usage_Profile__c WHERE Active__c = true AND (Is_Default_Profile__c = true OR Account__C = \''+accountId+'\')' + filter;
        }else{
             recordsQuery2 = 'SELECT '+ objectFields + ' FROM Usage_Profile__c WHERE id = \''+basketUsageProfile+'\')' + filter;
        }
        if(propositionName == 'Mobile Sharer'){
            usageProfileRecordList = Database.query(recordsQuery1);
            System.debug('*******usageProfileRecordList' + usageProfileRecordList);
            for(Usage_Profile__c usProf:usageProfileRecordList){
                if(usProf.Is_Default_Profile__c == true){
                    usageProfileReturnList.add(usProf);
                }
            }
        }    
        else if (propositionName == 'Mobile Flex'){
             usageProfileRecordList = Database.query(recordsQuery2);
             usageProfileReturnList.add(usageProfileRecordList[0]);
        }

        System.debug('*******usageProfileReturnList' + usageProfileReturnList);
        return usageProfileReturnList;
    }
    
    public String getSobjectFields(String so) {
            String fieldString;
       
            SObjectType sot = Schema.getGlobalDescribe().get(so);
            if (sot == null) return null;
       
            List<Schema.SObjectField> fields = sot.getDescribe().fields.getMap().values();
           
            fieldString = fields[0].getDescribe().LocalName;
            for (Integer i = 1; i < fields.size(); i++) {
                fieldString += ',' + fields[i].getDescribe().LocalName;
            }
            return fieldString;
    }
}