global class BatchScheduleTaskExpire implements Schedulable{
    global void execute(SchedulableContext sc) {
        //edit to run as me not SF Greg
        Date myDate = Date.Today();
        List<string> mylist = new list<string>();
        mylist.add(staticVariables.corp_campaign_task_record_type);
        mylist.add(staticVariables.deluxe_record_type);
        system.debug('ok'+ mylist);
        String sDate = String.valueOf(myDate);
            BatchTaskExpire b = new BatchTaskExpire('SELECT Id FROM Task WHERE (isClosed=false) AND Call_Status__c != \'' + 'Call Back Scheduled' + '\' AND ((recordTypeid = \'' + staticVariables.corp_campaign_task_record_type + '\') OR (recordTypeid = \'' + staticVariables.deluxe_record_type + '\')) AND WhatId IN (SELECT Id FROM Campaign WHERE EndDate <'+ sDate +')');
            system.debug('query is:' +'SELECT Id FROM Task WHERE (isClosed=false) AND Call_Status__c != \'' + 'Call Back Scheduled' + '\' AND ((recordTypeid = \'' + staticVariables.corp_campaign_task_record_type + '\') OR (recordTypeid = \'' + staticVariables.deluxe_record_type + '\')) AND WhatId IN (SELECT Id FROM Campaign WHERE EndDate <'+ sDate +')');
        database.executebatch(b, 200);
    }
}