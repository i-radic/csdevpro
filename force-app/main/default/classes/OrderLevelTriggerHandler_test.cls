/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=true)
private class OrderLevelTriggerHandler_test {

	private static Account mAccount;

	
	static{
		
		// Just create a single test account. false = no incremental count for the Company Name
		mAccount = EE_TestClass.createStaticAccount('Test Company', false);
		insert mAccount;
		
		// Create Custom Settings
        createCustomSettings();
		
	}




   /*
    * @name         testRollupOnCompanyOnCreateOrderLevelWelcomeDayChargeable
    * @description  Updates the Rollup Summary for every Inserted Order Level
    * @author       P Goodey
    * @date         Dec 2014
    * @see			WR
    */
    static testmethod void testRollupOnCompanyOnCreateOrderLevelWelcomeDayChargeable() {    

        Order_Level__c lOrderLevel = new Order_Level__c();
		lOrderLevel.Company__c 				= mAccount.Id;
		lOrderLevel.Product_Order_Type__c	= 'Welcome Day | Chargeable Service';
		lOrderLevel.Quantity__c				= 10;

        Test.StartTest();
        
        // insert the order item
        insert lOrderLevel;
        
        id lIdCompanyId = mAccount.Id;

        List<Account> lListCompany = new List<Account>();
         // Query for the passed company name
        String lsSOQL = 'SELECT Id, Name, Remaining_Chg_Welcome_Days__c FROM Account WHERE Id = :lIdCompanyId LIMIT 1';
        lListCompany = Database.query(lsSOQL);



        
        System.AssertEquals( 10, (double)lListCompany[0].get('Remaining_Chg_Welcome_Days__c'));
        
        Test.StopTest();
    
    }



   /*
    * @name         createCustomSettings
    * @description  create Custom Settings
    * @author       P Goodey
    * @date         Mar 2014
    * @see			WR
    */
    private static void createCustomSettings() {
    	
    	// Get the Trigger Enabler Custom Setting for Order Leve
        List<TriggerEnabler__c> lListTrgEnablerOrderLevel = new List<TriggerEnabler__c>();
        lListTrgEnablerOrderLevel = [SELECT Id, Name, TriggerEnabled__c FROM TriggerEnabler__c WHERE Name='Order_Level__c' LIMIT 1 ]; 
    	if(lListTrgEnablerOrderLevel==null){
    		TriggerEnabler__c trgEnabler = EE_TestClass.createTriggerEnabler('Order_Level__c', true);
			insert trgEnabler;    		
    	}

    	// Get the Order Level Custom Setting for Welcome Day Chargeable Order Requests
        List<OrderLevel_RollupSettings__c> lListOrWelcomeDayChargeable = new List<OrderLevel_RollupSettings__c>();
        lListOrWelcomeDayChargeable = [SELECT Id FROM OrderLevel_RollupSettings__c WHERE Name='Welcome Day Chargeable' LIMIT 1 ]; 
    	if(lListOrWelcomeDayChargeable==null){
	    	// Create a Custom Setting for setting Order Request Items
	        OrderLevel_RollupSettings__c lOrRollup = new OrderLevel_RollupSettings__c();
	        lOrRollup.put('Name', 'Welcome Day Chargeable');
	        lOrRollup.put('Company_Rollup_Field_Name__c', 'Remaining_Chg_Welcome_Days__c');
	        lOrRollup.put('Product_Order_Type__c', 'Welcome Day | Chargeable Service');
			insert lOrRollup;  		
    	}
    }




}