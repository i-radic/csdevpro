/**
      Name:  Test_Tgr_ContactAfter.class()
     Copyright © 2010  BT.
     ======================================================
    ======================================================
    Purpose:                                                            
    ----------       
    This class contains unit tests for validating the behavior of Apex classes
    for the Bulk Campaign Load functionality.
 
  Unit tests are class methods that verify whether a particular piece
  of code is working properly. Unit test methods take no arguments,
  commit no data to the database, and are flagged with the testMethod
  keyword in the method definition.
  
  All test methods in an organization are executed whenever Apex code is deployed
  to a production organization to confirm correctness, ensure code
  coverage, and prevent regressions. All Apex classes are
  required to have at least 75% code coverage in order to be deployed
  to a production organization. In addition, all triggers must have some code coverage.
    
  The @isTest class annotation indicates this class only contains test
  methods. Classes defined with the @isTest annotation do not count against
  the organization size limit for all Apex scripts.
   
  See the Apex Language Reference for more information about Testing and Code Coverage.
 
   This test class contains two methods, for testing BTLB and Corporate.
   
   Testing Batch Apex requires that the executeBatch method is called directly.
   In order words it is not possible to call a class which in turn calls the batch apex.
   Because batch Apex runs asynchronously it most also be surrounded with Test.startTest() and
   Test.stopTest() statements.
   
   Another condition on testing batch apex is that the execute method can only be called once
   from a single test.
   
   Becuase the batch apex process functions differently for BTLB and Corporate, i have split the testing
   of the two into separate methods and call the execute method once from each.
     
 
                                                      
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
    1.0 -    Greg Scarrott    31/08/2010        INITIAL DEVELOPMENT              Initial Build: 
    
    ***********************************************************************/
  
@isTest
private class tgrContactAfter_Test {

    static testMethod void myUnitTest() {
      
       Test_Factory.SetProperty('istest', 'yes');
  		User thisUser = [select id from User where id=:userinfo.getUserid()];
     	System.runAs( thisUser ){
         
         TriggerDeactivating__c settings = TriggerDeactivating__c.getOrgDefaults();
         settings.Account__c = FALSE;
         settings.Contact__c = FALSE;
         settings.Opportunity__c = FALSE;
         settings.OpportunitylineItem__c = FALSE;
         settings.Task__c = FALSE;
         settings.Event__c = FALSE;
         
         upsert settings TriggerDeactivating__c.Id;
        
    Profile prof = [SELECT Id FROM Profile WHERE Name = 'BT: Dataloader2'];
     User u1 = new User(
                       alias = 'B2B1', email = 'B2B13cr@bt.com',
                       emailencodingkey = 'UTF-8', lastname = 'Testing B2B1',
                       languagelocalekey = 'en_US',
                       localesidkey = 'en_US', ProfileId=prof.Id, //'00e20000001MX85AAG',
                       timezonesidkey = 'Europe/London', username = 'B2BProfile13167cr@bt.com',
                       EIN__c = 'OTHER_def'
                       );
    insert u1;  
        system.runAs(u1){
    Account accA = Test_Factory.CreateAccount(); // a LOB Level Account to link to        
    accA.Name = 'DELETE ME - Unit Test Account';        
    accA.LOB_Code__c = 'TEST';        
    accA.SAC_Code__c = 'TSTABC';        
    accA.LE_Code__c = null;     
    accA.CUG__c = 'ADJ0012345';
    accA.OwnerId = u1.Id; 
    insert accA;
        
         Account testAcc = Test_Factory.CreateAccount();
        testAcc.name += 'C';
        testAcc.sac_code__c += 'C';
        testAcc.Sector_code__c='CORP';
        testAcc.LOB_Code__c='FIELDM';
        Database.SaveResult[] accResult = Database.insert(new Account[] {testAcc});
                      
        //  JMM Commented out for deploment purposes      
        staticvariables.setcontactIsRunBefore(false);
        
        Contact con1 = Test_Factory.CreateContact();
        con1.SAC_code__c = accA.SAC_Code__c;
        con1.LE_code__c = accA.LE_Code__c;  
        con1.Contact_Post_Code__c = 'WR5 3RL';
        con1.AccountId = accResult[0].getid();
        con1.Contact_Key_Decision_Maker__c = 'KDM Operational';
        insert con1;            
        staticvariables.setcontactIsRunBefore(false);
            
        contact c=[Select Id,Contact_Address_Street__c from Contact where Id=: con1.Id];
        
         c.Contact_Address_Street__c = 'test';
         update c;
        
        Contact con2 = Test_Factory.CreateContact();
        con2.SAC_code__c = accA.SAC_Code__c;
        con2.LE_code__c = accA.LE_Code__c;
        con2.AccountId = accResult[0].getid();
        con2.Contact_Key_Decision_Maker__c = 'KDM Strategic';
        insert con2;
        
        
      //  contact testcontact1 = [select target_contact__c from contact where id = :con1.id];
        contact testcontact2 = [select target_contact__c from contact where id = :con2.id];
        
    //    system.assertequals(testcontact1.target_contact__c,true);
    //    system.assertequals(testcontact2.target_contact__c,false);
        }
        }        
  }
}