/**
   (c) 2020 CloudSense LTD
   Developed by CloudSense LTD, London (UK)

   @date 8.2020
   @author Mahaboob Basha

   @description A Web Service that sends the XML file to MDS

   @modifications
   06.08.2020 [Mahaboob]
   
 */
public class MDSWebService {

    //Upload Files on FTP Server
	public static HttpResponse uploadFileOnMDSServer(String http_method, String http_body) {
		HttpRequest req = createHttpRequest(http_method, http_body, MDSWebServiceUtility.mdsConfiguration);
		Http httpreq = new Http();
		return httpreq.send(req);
	}

	private static HttpRequest createHttpRequest(String method, String body, CS_Integration_Settings__c mdsConfiguration) {
		HttpRequest req = new HttpRequest();
		req.setHeader('ftp-host', mdsConfiguration.MDS_Host__c);
		req.setHeader('ftp-type', mdsConfiguration.MDS_Type__c);
		req.setHeader('username', mdsConfiguration.MDS_Username__c);
		req.setHeader('password', mdsConfiguration.MDS_Password__c);
		req.setHeader('port', mdsConfiguration.MDS_Port__c);
		//req.setHeader('Content-Type', 'application/json');
		req.setHeader('Content-Type', mdsConfiguration.MDS_Content_Type__c);
		req.setEndpoint(mdsConfiguration.MDS_Endpoint__c + MDSWebServiceUtility.FILE_UPLOAD);
		req.setMethod(method);
		if(String.isNotBlank(body) && String.isNotEmpty(body)){
			req.setBody(body);
		}
		req.setTimeout(120000);
		return req;
	}
}