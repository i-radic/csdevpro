var aomPlugins = {};

function setAOMPlugin(plugin, event) {
	aomPlugins[event] = plugin;
}

function evaluatePlugins(event, params) {
	if (aomPlugins[event]) {
		return aomPlugins[event](params);
	} else {
		return null;
	}
}

function initCSAddonApi() {

	var addOnData = {};
	var defaultAddOns = {};
	var incompatibleAddOns = {};
	var relatedProductData = {};
	var labels = void 0;


	function initAOMData(data) {
		if (data) {
			relatedProductData = data.relatedProductData;
			var pluginResponse = evaluatePlugins('afterInit', data.addOnData);
			if (pluginResponse) {
				data.addOnData = pluginResponse;
			}
			setAOM(data.addOnData);
		} else {
			return CS.AOM.WebService.getRelatedProductData().then(function(rps) {
				relatedProductData = rps;
				return CS.AOM.WebService.getAddonPriceItemAssociations().then(function(data) {
					if (data.data) {
						setLookupRecords(data.data);
					}
					var pluginResponse = evaluatePlugins('afterInit', data);
					if (pluginResponse) {
						data = pluginResponse;
					}
					if (data) {
						return setAOM(data);
					} else {
						return Promise.resolve({});
					}
				});
			});
		}
	}

	function setLookupRecords(data) {
		Object.keys(data).forEach(key => {
			data[key].forEach(association => {
				var newLookupRecord = {};
				Object.keys(association).forEach(assocKey => {
					if (assocKey.endsWith('__r')) {
						Object.keys(association[assocKey]).forEach(assocRelKey => {
							newLookupRecord[assocKey.toLowerCase() + '.' + assocRelKey.toLowerCase()] = association[assocKey][assocRelKey];
						})
					} else {
						newLookupRecord[assocKey.toLowerCase()] = association[assocKey];
					}
				})
				if (CS.lookupRecords) {
					CS.lookupRecords[association.Id] = newLookupRecord;
				}
			})
		})
	}

	function setAOM(data) {
		defaultAddOns = {};
		incompatibleAddOns = {};
		labels = data.labels;
		addOnData = createAddonMap(data.data);
		var pluginResponse = evaluatePlugins('afterAddOnsPrepared', addOnData);
		if (pluginResponse) {
			addOnData = pluginResponse;
		}
		CS.AOM.UI.renderAOM();
		return Promise.resolve(addOnData);
	}

	function createAddonMap(data) {
		var addonMap = Object.keys(data).map(function(key) {
			var defaultAddons = data[key].filter(function(item) {
				return item.cspmb__Default_Quantity__c && item.cspmb__Default_Quantity__c > 0;
			});
			var allAddons = data[key].sort(compareBySequence);
			var groupNames = allAddons.map(function(item) {
				return item.cspmb__Group__c;
			}).filter(function(value, index, a) {
				return a.indexOf(value) === index;
			});
			var addOnGroups = groupNames.map(function(key) {
				var addonsFromGroup = allAddons.filter(function(item) {
					return item.cspmb__Group__c === key;
				});
				var addonIds = addonsFromGroup.map(function(item) {
					return item.cspmb__Add_On_Price_Item__c;
				});
				var min = void 0,
					max = void 0;
				if (addonsFromGroup.length > 0) {
					min = addonsFromGroup[0].cspmb__Min__c;
					max = addonsFromGroup[0].cspmb__Max__c;
				}
				return {min: min, max: max, name: key, addOns: addonsFromGroup, addonIds: addonIds};
			});
			incompatibleAddOns[key] = [];
			for (var j = 0; j < relatedProductData.length; j++) {
				if (relatedProductData[j].definitionId === key) {
					for (var l = 0; l < relatedProductData[j].added.length; l++) {
						var found = false;
						var _addonValue = relatedProductData[j].added[l];
						for (var k = 0; k < addOnGroups.length; k++) {
							var grp = addOnGroups[k];
							if (grp.addonIds.includes(_addonValue)) {
								found = true;
							}
						}
						if (!found) {
							incompatibleAddOns[key].push(_addonValue);
						}
					}
				}
			}
			var addonValue = {defaultAddons: defaultAddons, allAddons: allAddons, addOnGroups: addOnGroups};
			return {key: key, data: addonValue};
		}).reduce(function(map, obj) {
			map[obj.key] = obj.data;
			return map;
		}, {});
		return addonMap;
	}

	function getAOMData() {
		return addOnData;
	}

	function getDefaultAddOns() {
		var defaultAddOns = Object.keys(addOnData).map(function(key) {
			var defaultAddons = addOnData[key].defaultAddons;
			return {key: key, data: defaultAddons};
		}).reduce(function(map, obj) {
			map[obj.key] = obj.data;
			return map;
		}, {});
		return defaultAddOns;
	}

	function compareBySequence(a, b) {
		if (a.cspmb__Sequence__c && b.cspmb__Sequence__c && a.cspmb__Sequence__c < b.cspmb__Sequence__c || a.cspmb__Sequence__c && !b.cspmb__Sequence__c) {
			return 1;
		}
		if (a.cspmb__Sequence__c && b.cspmb__Sequence__c && a.cspmb__Sequence__c > b.cspmb__Sequence__c || !a.cspmb__Sequence__c && b.cspmb__Sequence__c) {
			return -1;
		}
		return 0;
	}

	function minMaxLogic() {
		var validationObj = {};
		if (relatedProductData) {
			var rpIds = Object.keys(relatedProductData);
			for (var i = 0; i < rpIds.length; i++) {
				var currentRp = relatedProductData[i];
				if (currentRp) {
					var rps = currentRp.added;
					var adds = addOnData[currentRp.definitionId];
					var rpData = [];
					if (adds && adds.addOnGroups) {
						for (var k = 0; k < adds.addOnGroups.length; k++) {
							var grp = adds.addOnGroups[k];
							var count = 0;
							if (rps && rps.length > 0) {
								for (var l = 0; l < rps.length; l++) {
									var addonValue = rps[l];
									if (grp.addonIds.includes(addonValue)) {
										count++;
									}
								}
							}
							grp.count = count;
							var msg = 'Ok';
							if (count > grp.max) {
								msg = 'Please remove ' + (count - grp.max) + ' add on(s).';
							} else if (count < grp.min) {
								msg = 'Please add more add ons.';
							}
							rpData.push({
								name: grp.name,
								min: grp.min,
								max: grp.max,
								count: count,
								msg: msg,
								addonIds: grp.addonIds
							});
						}
					}
					validationObj[currentRp.definitionId] = rpData;
				}
			}
		}
		return Promise.resolve(validationObj);
	}

	function checkMinMax(data) {
		if (data) {
			return minMaxLogic();
		} else {
			return CS.AOM.WebService.getRelatedProductData().then(function(relatedProductData) {
				return minMaxLogic();
			});
		}
	}

	function getIncompatibleAddOns() {
		return incompatibleAddOns;
	}

	return {
		getDefaultAddOns: getDefaultAddOns,
		getIncompatibleAddOns: getIncompatibleAddOns,
		initAOMData: initAOMData,
		checkMinMax: checkMinMax,
		getAOMData: getAOMData
	};
}
