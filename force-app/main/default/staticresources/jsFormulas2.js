var PLCalculations = function() {
	function getBasketJson() {
		return window.basketJSON;
	}
	
	function getConfigIds(filter) {
		var allConfigIds = Object.keys(getBasketJson().configDetails);
		var validConfigIds = [];
		for (var i = 0; i < allConfigIds.length; i++) {
			var config = getBasketJson().configDetails[allConfigIds[i]];
			var evaluation = filter.expression;
			if(evaluation) {
				for(var j = 0; j < filter.fields.length; j++) {
					var fld = filter.fields[j];
					var fieldValue = '"' + config.config[fld] + '"';
					var evaluation = evaluation.replace(new RegExp(fld, 'g'), fieldValue);
				}
				
				if(eval(evaluation)) {
					validConfigIds.push(config.config.Id);
				}
			}
			else {
				validConfigIds.push(config.config.Id);
			}
		}
		
		return validConfigIds;
	}
	
	function getMaxTerm() {
		var maxTerm = 0;
		var allConfigIds = Object.keys(getBasketJson().configDetails);
		for (var i = 0; i < allConfigIds.length; i++) {
			var config = getBasketJson().configDetails[allConfigIds[i]];
			if(config.config.cscfga__Contract_Term__c > maxTerm) {
				maxTerm = config.config.cscfga__Contract_Term__c;
			}
		}
		
		return maxTerm;
	}
	
	function getVolume() {
		return getBasketJson().basketDetails.Total_Subscribers__c;
	}
	
	function getNpv(term) {
		var result = [1];
		var years = Math.ceil(term / 12);
		for(var i = 0; i < years - 1; i++) {
			result.push(0.9);
		}
		
		return result;
	}
	
	function getYears(contractTerm) {
		return Math.floor(contractTerm / 12);
	}
	
	function getRemainingMonths(contractTerm) {
		return contractTerm % 12;
	}
	
	function sumYears(config, fields) {
		var result = [];
		var years = getYears(config.config.cscfga__Contract_Term__c);
		
		for(var i = 0; i < years; i++) {
			var totalYear = 0;
			for(var j = 0; j < fields.length; j++) {
				var field = fields[j];
				var fieldValue = config.config[field.name];
				if(!isNaN(fieldValue)) {
					if(field.isRecurring) {
						totalYear += (fieldValue * 12);
					}
					else if(i === 0) {
						totalYear += fieldValue;
					}
				}
			}
			
			result.push(totalYear);
		}
		
		return result;
	}

	function sum(configIds, fields) {
		var total = 0;
		for (var i = 0; i < configIds.length; i++) {
			var config = basketJSON.configDetails[configIds[i]];
			for(var j = 0; j < fields.length; j++) {
				var field = fields[j];
				var fieldValue = config.config[field.name];
				if(!isNaN(fieldValue)) {
					total += (fieldValue);
				}
			}
		}

		return total;
	}
	
	function sumArrays(arr1, arr2) {
		for(var j = 0; j < arr2.length; j++) {
			if(arr1[j] === undefined) arr1[j] = 0;
			arr1[j] += arr2[j];
		}
		
		return arr1;
	}

	function divArrays(arr1, arr2) {
		for(var j = 0; j < arr2.length; j++) {
			if(arr1[j] === undefined) arr1[j] = 0;
			if(arr2[j] !== 0) {
				arr1[j] /= arr2[j];
			}
		}
		
		return arr1;		
	}
	
	function mulArrays(arr1, arr2) {
		for(var j = 0; j < arr2.length; j++) {
			if(arr1[j] === undefined) arr1[j] = 0;
			arr1[j] *= arr2[j];
		}
		
		return arr1;		
	}
	
	function negArray(arr) {
		for(var i = 0; i < arr.length; i++) {
			arr[i] = arr[i] * -1;
		}
		
		return arr;
	}
	
	function sumElements(arr) {
		var total = 0;
		
		for(var i = 0; i < arr.length; i++) {
			total += arr[i];
		}
		
		return total;		
	}
	
	function sumRemainingMonths(config, fields) {
		var remainingMonths = getRemainingMonths(config.config.cscfga__Contract_Term__c);
		var numberOfYears = getYears(config.config.cscfga__Contract_Term__c);
		var totalRemainingMonths = 0;
		for(var i = 0; i < fields.length; i++) {
			var field = fields[i];
			var fieldValue = config.config[field.name];
			if(!isNaN(fieldValue)) {
				if(field.isRecurring) {
					totalRemainingMonths += (fieldValue * remainingMonths);
				}
				else if(numberOfYears === 0) {
					totalRemainingMonths += fieldValue;
				}
			}
		}
		
		return totalRemainingMonths;
	}
	
	function getBasketCalc(fields) {
		var result = [0];
		var basketJSON = getBasketJson();
		
		var basket = basketJSON.basketDetails;
		for(var i = 0; i < fields.length; i++) {
			if(basket[fields[i].name]) {
				result[0] += basket[fields[i].name];
			}
		}

		return result;		
	}
	
	function getYearsCalc(configIds, fields) {
		var result = [];
		var basketJSON = getBasketJson();
		
		for (var i = 0; i < configIds.length; i++) {
			var config = basketJSON.configDetails[configIds[i]];
			var totalYears = sumYears(config, fields);
			var totalRemainingMonths = sumRemainingMonths(config, fields);
			var numberOfYears = getYears(config.config.cscfga__Contract_Term__c);
			
			sumArrays(result, totalYears);
			if(totalRemainingMonths !== 0) {
				if(result[numberOfYears] === undefined) result[numberOfYears] = 0;
				result[numberOfYears] += totalRemainingMonths;
			}
		}

		return result;
	}
	// Bt net changes start
	function BTNetRevenueCalcuF() {
		 return getYearsBTNETCalc(getConfigIds({fields: ['Calculations_Product_Group__c'],
										 expression: 'Calculations_Product_Group__c == "BT Net"'}),
			           [{name:'NPV_Monthly_Cost__c', isRecurring: true},
							 {name:'NPV_Monthly_Revenue__c', isRecurring: true},
						     {name:'NPV_Upfront_Cost__c', isRecurring: false},
							 {name:'NPV_Upfront_Revenue__c', isRecurring: false}]);
	}
	
	function BTNetRevenueTotalF() {
		return getTotalsCalc(BTNetRevenueCalcuF());
	}					
	function getYearsBTNETCalc(configIds, fields) {
		var result = [];
		var basketJSON = getBasketJson();
		for (var i = 0; i < configIds.length; i++) {
			var config = basketJSON.configDetails[configIds[i]];
			var totalYears = calculateBTNetYearCharges(config, fields);		
            console.log('totalYears =====' +totalYears);			
			sumArrays(result, totalYears);		
		}
		return result;
	}
	
		function calculateBTNetYearCharges(config, fields) {
		var years = getYears(config.config.cscfga__Contract_Term__c);
		var totalYear = [];
		for(var i = 0; i < years; i++) {
			
			var term1Multiplier = parseFloat("11.47262987");
			var term2Multiplier = parseFloat("21.86450476");
			var term3Multiplier = parseFloat("31.27743491");
			var term4Multiplier = parseFloat("39.80363975");
			var term5Multiplier = parseFloat("47.52665139");
			
			if(typeof config.config['NPV_Monthly_Cost__c'] !== "undefined"){ 
			    var NPVMonthlyCost = config.config['NPV_Monthly_Cost__c'];
			}
			else {
				var NPVMonthlyCost = 0;
			}
			if(typeof config.config['NPV_Upfront_Cost__c'] !== "undefined"){
				var NPVUpfrontCost = config.config['NPV_Upfront_Cost__c'];
			}
			else {
				var NPVUpfrontCost = 0;
			}
			if(typeof config.config['NPV_Upfront_Revenue__c'] !== "undefined"){
				var NPVUpfrontRevenue = config.config['NPV_Upfront_Revenue__c'];
			}
			else {
				var NPVUpfrontRevenue = 0;
			}
			if(typeof config.config['NPV_Monthly_Revenue__c'] !== "undefined"){
				var NPVMonthlyRevenue = config.config['NPV_Monthly_Revenue__c'];
			}
			else {
				var NPVMonthlyRevenue = 0;
			}

			if(years <= 5){
				totalYear[0] = parseFloat(term1Multiplier * (NPVMonthlyRevenue - NPVMonthlyCost) - NPVUpfrontCost + NPVUpfrontRevenue + NPVMonthlyRevenue - NPVMonthlyCost);
			}else totalYear[0] = 0;
			if(years >= 2 && years <= 5){
				totalYear[1] = parseFloat((term2Multiplier * (NPVMonthlyRevenue - NPVMonthlyCost) - NPVUpfrontCost +  NPVUpfrontRevenue + NPVMonthlyRevenue - NPVMonthlyCost) - totalYear[0]);
			}else totalYear[1] = 0;
			if(years >= 3 && years <= 5){
				totalYear[2] = parseFloat((term3Multiplier * (NPVMonthlyRevenue - NPVMonthlyCost) - NPVUpfrontCost +  NPVUpfrontRevenue + NPVMonthlyRevenue - NPVMonthlyCost) - (totalYear[0] + totalYear[1]));
			}else totalYear[2] = 0;
			if(years >= 4 && years <= 5){
				totalYear[3] = parseFloat((term4Multiplier * (NPVMonthlyRevenue - NPVMonthlyCost) - NPVUpfrontCost +  NPVUpfrontRevenue + NPVMonthlyRevenue - NPVMonthlyCost) - (totalYear[0] + totalYear[1] + totalYear[2]));
			}else totalYear[3] = 0;
			if(years == 5){
				totalYear[4] = parseFloat((term5Multiplier * (NPVMonthlyRevenue - NPVMonthlyCost) - NPVUpfrontCost +  NPVUpfrontRevenue + NPVMonthlyRevenue - NPVMonthlyCost) - (totalYear[0] + totalYear[1] + totalYear[2] + totalYear[3]));
			}else totalYear[4] = 0;
			
			
		}
		
		return totalYear;
	}
	
	function NPVTotalCalcuF() {
		return BTNetRevenueCalcuF();
	}
	
	function NPVTotalTotalF() {
		return getTotalsCalc(NPVTotalCalcuF());
	}
	///////BT net changes end //////////							

	function formatNumber(str) {
        return str.replace(new RegExp(',', 'g'), '');
	}
	
	function getRowObject(rowName) {
		var tableObj = JSON.parse(window.getTable);
		var rowObject;
		for (var i = 0; i < tableObj.length; i++) {
			if (tableObj[i].name == rowName) {
				rowObject = tableObj[i];
			}
		}
		return rowObject;
	}
	
	function getTotalsCalc(arr) {
		return sumElements(arr);
	}
	
	function EmptyYearsCalcuF() {
		return [];
	}

	function EmptyYearsTotalF() {
		return 0;
	}

	function VoiceRevenueCalcuF() {
		return getYearsCalc(getConfigIds({fields: ['Calculations_Product_Group__c'],
								expression: 'Calculations_Product_Group__c == "BT Mobile" || Calculations_Product_Group__c == "EE SME"'}),
								[{name:'Voice_Revenue_inc_SMS__c', isRecurring: true}]);		
	}

	function VoiceRevenueTotalF() {
		return getTotalsCalc(VoiceRevenueCalcuF());
	}
	
	function DataRevenueCalcuF() {
		return getYearsCalc(getConfigIds({fields: ['cspl__Type__c', 'Calculations_Product_Group__c'],
										 expression: 'Calculations_Product_Group__c == "BT Mobile" || Calculations_Product_Group__c == "BT Mobile Broadband" || (Calculations_Product_Group__c == "EE SME" && (cspl__Type__c == "Data" || cspl__Type__c == "Tablet" || cspl__Type__c == "Data SIM"))'}),
										[{name:'Data_Revenue__c', isRecurring: true}]);
	}

	function DataRevenueTotalF() {
		return getTotalsCalc(DataRevenueCalcuF());
	}
	
	function CareRevenueCalcuF() {
		return getYearsCalc(getConfigIds({}),
							[{name:'Care_and_Chargable_Service_Revenue__c', isRecurring: true}]);
	}
	
	function CareRevenueTotalF() {
		return getTotalsCalc(CareRevenueCalcuF());
    }
    
    function SiteAndInstallCalcuF() {
        return [];
    }

    function SiteAndInstallTotalF() {
		return 0;
	}
	
	function OtherRevenueCalcuF() {
		return getYearsCalc(getConfigIds({}),
							[{name:'Year_1_Other_Revenue__c', isRecurring: false},
							 {name:'Other_Revenue__c', isRecurring: true}]);
	}
	
	function OtherRevenueTotalF() {
		return getTotalsCalc(OtherRevenueCalcuF());
	}
	
	function BillableRevenueCalcuF() {
		return sumArrays(sumArrays(sumArrays(VoiceRevenueCalcuF(), DataRevenueCalcuF()), CareRevenueCalcuF()), OtherRevenueCalcuF());
	}
	
	function BillableRevenueTotalF() {
		return getTotalsCalc(BillableRevenueCalcuF());
	}
	
	function IncomingRevenueCalcuF() {
		return getYearsCalc(getConfigIds({}),
							[{name:'Incoming_Revenue__c', isRecurring: true}]);
	}
	
	function IncomingRevenueTotalF() {
		return getTotalsCalc(IncomingRevenueCalcuF());
	}
	
	function RevenueCalcuF() {
		return sumArrays(BillableRevenueCalcuF(), IncomingRevenueCalcuF());
	}
	
	function RevenueTotalF() {
		return getTotalsCalc(RevenueCalcuF());
	}
	
	function AirtimeFundCalcuF() {
		return getBasketCalc([{name:'Airtime_Fund__c', isRecurring: false}]);
	}
	
	function AirtimeFundTotalF() {
		return getTotalsCalc(AirtimeFundCalcuF());
	}
	
	function TechFundCalcuF() {
		return sumArrays(getYearsCalc(getConfigIds({fields: ['Calculations_Product_Group__c'],
										 expression: 'Calculations_Product_Group__c == "BT Mobile Broadband" || Calculations_Product_Group__c == "EE SME"'}),[{name:'BT_Technology_Fund__c', isRecurring: false}, {name:'Investment__c', isRecurring: false}]), getBasketCalc([{name:'BT_Technology_Fund_Calculated__c', isRecurring: false}]));
	}
	
	function TechFundTotalF() {
		return getTotalsCalc(TechFundCalcuF());
	}
	
	function HardwareExtrasCalcuF() {
		return getYearsCalc(getConfigIds({}),[{name:'Hardware_Extra_For_PandL__c', isRecurring: true}])
	}
	
	function HardwareExtrasTotalF() {
		return getTotalsCalc(HardwareExtrasCalcuF());
	}

	function ChequeCalcuF() {
        return [];
    }

    function ChequeTotalF() {
		return 0;
    }
	
	function TotalInvestmentCalcuF() {
		return sumArrays(sumArrays(AirtimeFundCalcuF(), TechFundCalcuF()), HardwareExtrasCalcuF());
	}
	
	function TotalInvestmentTotalF() {
		return getTotalsCalc(TotalInvestmentCalcuF());
	}

	function RevInvestCalcuF() {
		return sumArrays(RevenueCalcuF(), negArray(TotalInvestmentCalcuF()));
	}
	
	function RevInvestTotalF() {
		return getTotalsCalc(RevInvestCalcuF());
	}
	
	function VoiceCostCalcuF() {
		return getYearsCalc(getConfigIds({fields: ['cspl__Type__c'], expression: 'cspl__Type__c != "Data"'}),
										[{name:'Mobile_Voice_One_Off_Cost__c', isRecurring: false},
										{name:'Mobile_Voice_Recurring_Cost__c', isRecurring: true},
										{name:'Voice_Costs_inc_SMS__c', isRecurring: true}]);
	}
	
	function VoiceCostTotalF() {
		return getTotalsCalc(VoiceCostCalcuF());
	}
	
	function DataCostCalcuF() {
		return getYearsCalc(getConfigIds({fields: ['cspl__Type__c', 'Calculations_Product_Group__c'],
										 expression: 'cspl__Type__c == "Data" || cspl__Type__c == "Tablet" || cspl__Type__c == "Data SIM" || Calculations_Product_Group__c == "BT Mobile" || Calculations_Product_Group__c == "BT Mobile Broadband"'}),
										[{name:'Data_One_Off_Cost__c', isRecurring: false},
										{name:'Data_Recurring_Cost__c', isRecurring: true},
										{name:'Mobile_Voice_One_Off_Cost__c', isRecurring: false},
										{name:'Mobile_Voice_Recurring_Cost__c', isRecurring: true},
										{name:'BT_Mobile_Data_Costs__c', isRecurring: true}]);
	}
	
	function DataCostTotalF() {
		return getTotalsCalc(DataCostCalcuF());
	}
	
	function CareCostCalcuF() {
		return getYearsCalc(getConfigIds({}),
							[{name:'Year_1_Care_and_Chargable_Service_Costs__c', isRecurring: false},
							{name:'Care_and_Chargable_Service_Costs__c', isRecurring: true}]);
	}

	function CareCostTotalF() {
		return getTotalsCalc(CareCostCalcuF());
	}

	function SiteAndSubInstallCalcuF() {
		return [];
	}

	function SiteAndSubInstallTotalF() {
		return 0;
	}
	
	function NetDeviceCalcuF() {
		return getYearsCalc(getConfigIds({fields: ['Calculations_Product_Group__c'],
										 expression: 'Calculations_Product_Group__c == "BT Mobile"'}),
							[{name:'Net_device_profit_cost__c', isRecurring: false}]);
	}
	
	function NetDeviceTotalF() {
		return getTotalsCalc(NetDeviceCalcuF());
	}

	function CommissionCalcuF() {
		return getYearsCalc(getConfigIds({}), [{name:'Sales_Commision_One_Off_Cost__c', isRecurring: false}, {name:'BT_Total_Commission__c', isRecurring: false}]);
	}
	
	function CommissionTotalF() {
		return getTotalsCalc(CommissionCalcuF());
	}
	
	function OtherCostCalcuF() {
		return getYearsCalc(getConfigIds({}), [{name:'Year_1_Other_Costs__c', isRecurring: false},
												{name:'Other_Costs__c', isRecurring: true}]);
	}
	
	function OtherCostTotalF() {
		return getTotalsCalc(OtherCostCalcuF());
	}

	function CostCalcuF() {
		return sumArrays(sumArrays(sumArrays(sumArrays(sumArrays(VoiceCostCalcuF(), DataCostCalcuF()), CommissionCalcuF()), CareCostCalcuF()), negArray(NetDeviceCalcuF())), OtherCostCalcuF());
	}
	
	function CostTotalF() {
		return getTotalsCalc(CostCalcuF());
	}

	function MarginCalcuF() {
		return sumArrays(RevInvestCalcuF(), negArray(CostCalcuF()));
	}
	
	function MarginTotalF() {
		return getTotalsCalc(MarginCalcuF());
	}
	
	function MarginPercentCalcuF() {
		return [];
	}
	
	function MarginPercentTotalF() {
		var ebtd = parseFloat(formatNumber(getRowObject("Gross Margin").total));
		var rev = parseFloat(formatNumber(getRowObject("TOTAL REVENUE").total));
		if(rev > 0) {
			return ebtd / rev * 100;
		}
		else {
			return 0;
		}
	}
	
	function IncrementalOverheadCalcuF() {
		return getYearsCalc(getConfigIds({}),
					[{name:'Year_1_Incremental_Overheads__c', isRecurring: false},
					{name:'Incremental_Overheads__c', isRecurring: true}]);
	}
	
	function IncrementalOverheadTotalF() {
		return getTotalsCalc(IncrementalOverheadCalcuF());
	}
	
	function IncrementalEBITDACalcuF() {
		return sumArrays(MarginCalcuF(), negArray(IncrementalOverheadCalcuF()));
	}
	
	function IncrementalEBITDATotalF() {
		return getTotalsCalc(IncrementalEBITDACalcuF());
	}
	
	function IncrementalEBITDAPrecentCalcuF() {
		return []; 
	}
	
	function IncrementalEBITDAPrecentTotalF() {
		//var ebtd = IncrementalEBITDATotalF();
		var ebtd = parseFloat(formatNumber(getRowObject("Incremental EBITDA").total));
		var rev = parseFloat(formatNumber(getRowObject("TOTAL REVENUE").total));
		if(rev > 0) {
			return ebtd / rev * 100;
		}
		else {
			return 0;
		}
	}
	
	function IncrementalNpvCalcuF() {
		return [];
	}
	
	function IncrementalNpvTotalF() {
		return getTotalsCalc(mulArrays(IncrementalEBITDACalcuF(), getNpv(getMaxTerm()))) / getVolume(); 
	}

	var IncrNpvPerSubCalcuF = function() {
		return [];
	}
	
	var IncrNpvPerSubTotalF = function() {
		var incrEbitdaOneOff = parseFloat(formatNumber(getRowObject("Incremental EBITDA One Off").total));
		var incrEbitdaYears = parseFloat(formatNumber(getRowObject("Incremental EBITDA Recurring").total));
	
		var result = (incrEbitdaYears * 0.9 + incrEbitdaOneOff) / getVolume();

		return result;
	}
	
	function OtherOverheadCalcuF() {
		return getYearsCalc(getConfigIds({}),
							[{name:'Year_1_Other_Overheads__c', isRecurring: false},
							{name:'Other_Overheads__c', isRecurring: true}]);
	}
	
	function OtherOverheadTotalF() {
		return getTotalsCalc(OtherOverheadCalcuF());
	}
	
	function EBITDACalcuF() {
		return sumArrays(IncrementalEBITDACalcuF(), negArray(OtherOverheadCalcuF()));
	}
	
	function EBITDATotalF() {
		return getTotalsCalc(EBITDACalcuF());
	}
	
	function EBITDAPercentCalcuF() {
		return [];
	}

	function EBITDAPercentTotalF() {
		var rev = parseFloat(formatNumber(getRowObject("TOTAL REVENUE").total));
		var ebtd = parseFloat(formatNumber(getRowObject("EBITDA").total));
		if(rev > 0) {
			return ebtd / rev * 100;
		}
		else {
			return 0;
		}
	}		
	
	function NPVPerSubCalcuF() {
		return [];
	}
	
	function NpvPerSubTotalF() {
		return getTotalsCalc(mulArrays(EBITDACalcuF(), getNpv(getMaxTerm()))) / getVolume(); 
	}

	var NPVPerSubTotalF = function() {	
		var ebitdaOneOff = parseFloat(formatNumber(getRowObject("EBITDA One Off").total));
		var ebitdaYears = parseFloat(formatNumber(getRowObject("EBITDA Recurring").total));

		var result = (ebitdaYears * 0.9 + ebitdaOneOff) / getVolume();

		return result;
	}
	
	function init() {
		for (var key in this) {
			if (this.hasOwnProperty(key)) {
				if(key !== 'init') {
					window[key] = this[key];
					window[key + "Lig"] = this[key];
				}
			}
		}
	}
	
	function getAllFunctions() {
		var allFunctions = [];
		for (var key in PL.Calculations) {
			if (PL.Calculations.hasOwnProperty(key)) {
				if(key !== 'init') {
					allFunctions.push(key);
					allFunctions.push(key + "Lig");
				}
			}
		}
		
		return allFunctions;		
	}

	function DisconnectionAllowanceCalcuF() {
		yearDisconnectAllowance = [];
		specialConditionConfigId = getConfigIds({fields: ['Product_Definition_Name__c'],
			expression: 'Product_Definition_Name__c == "Special Conditions"'})[0];
		disconnectionAllowance = -1 * parseFloat(basketJSON.configDetails[specialConditionConfigId].config.Disconnection_Allowance_Percentage__c);

		for(var i=1; i <= Math.ceil(basketJSON.basketDetails.Tenure__c/12); i++) {
			if (i == 1) {
				yearDisconnectAllowance.push(1);
			} else {
				yearDisconnectAllowance.push(disconnectionAllowance*0.01);
			}
		}

		return yearDisconnectAllowance;
	}

	function DisconnectionAllowanceTotalF() {
		specialConditionConfigIdTotals = getConfigIds({fields: ['Product_Definition_Name__c'],
			expression: 'Product_Definition_Name__c == "Special Conditions"'})[0];
		disconnectionAllowanceTotals = -1 * parseFloat(basketJSON.configDetails[specialConditionConfigId].config.Disconnection_Allowance_Percentage__c);

		return disconnectionAllowanceTotals*0.01;
	}
	
    return {
        init: init,
		getAllFunctions: getAllFunctions,
		VoiceRevenueCalcuF: VoiceRevenueCalcuF,
		VoiceRevenueTotalF: VoiceRevenueTotalF,
		DataRevenueCalcuF: DataRevenueCalcuF,
		DataRevenueTotalF: DataRevenueTotalF,
		CareRevenueCalcuF: CareRevenueCalcuF,
		CareRevenueTotalF: CareRevenueTotalF,
		SiteAndInstallCalcuF: SiteAndInstallCalcuF,
		SiteAndInstallTotalF: SiteAndInstallTotalF,
		OtherRevenueCalcuF: OtherRevenueCalcuF,
		OtherRevenueTotalF: OtherRevenueTotalF,
		BillableRevenueCalcuF: BillableRevenueCalcuF,
		BillableRevenueTotalF: BillableRevenueTotalF,
		IncomingRevenueCalcuF: IncomingRevenueCalcuF,
		IncomingRevenueTotalF: IncomingRevenueTotalF,
		RevenueCalcuF: RevenueCalcuF,
		RevenueTotalF: RevenueTotalF,		
		AirtimeFundCalcuF: AirtimeFundCalcuF,
		AirtimeFundTotalF: AirtimeFundTotalF,
		TechFundCalcuF: TechFundCalcuF,
		TechFundTotalF: TechFundTotalF,
		HardwareExtrasCalcuF: HardwareExtrasCalcuF,
		HardwareExtrasTotalF: HardwareExtrasTotalF,
		ChequeCalcuF: ChequeCalcuF,
		ChequeTotalF: ChequeTotalF,
		TotalInvestmentCalcuF: TotalInvestmentCalcuF,
		TotalInvestmentTotalF: TotalInvestmentTotalF,
		RevInvestCalcuF: RevInvestCalcuF,
		RevInvestTotalF: RevInvestTotalF,
		VoiceCostCalcuF: VoiceCostCalcuF,
		VoiceCostTotalF: VoiceCostTotalF,
		DataCostCalcuF: DataCostCalcuF,
		DataCostTotalF: DataCostTotalF,
		SiteAndSubInstallCalcuF: SiteAndSubInstallCalcuF,
		SiteAndSubInstallTotalF: SiteAndSubInstallTotalF,
		CareCostCalcuF: CareCostCalcuF,
		CareCostTotalF: CareCostTotalF,
		NetDeviceCalcuF: NetDeviceCalcuF,
		NetDeviceTotalF: NetDeviceTotalF,
		CommissionCalcuF: CommissionCalcuF,
		CommissionTotalF: CommissionTotalF,
		OtherCostCalcuF: OtherCostCalcuF,
		OtherCostTotalF: OtherCostTotalF,
		CostCalcuF: CostCalcuF,
		CostTotalF: CostTotalF,
		MarginCalcuF: MarginCalcuF,
		MarginTotalF: MarginTotalF,
		MarginPercentCalcuF: MarginPercentCalcuF,
		MarginPercentTotalF: MarginPercentTotalF,
		IncrementalOverheadCalcuF: IncrementalOverheadCalcuF,
		IncrementalOverheadTotalF: IncrementalOverheadTotalF,
		IncrementalEBITDACalcuF: IncrementalEBITDACalcuF,
		IncrementalEBITDATotalF: IncrementalEBITDATotalF,
		IncrementalEBITDAPrecentCalcuF: IncrementalEBITDAPrecentCalcuF,
		IncrementalEBITDAPrecentTotalF: IncrementalEBITDAPrecentTotalF,
		IncrementalNpvCalcuF: IncrementalNpvCalcuF,
		IncrementalNpvTotalF: IncrementalNpvTotalF,
		IncrNpvPerSubCalcuF: IncrNpvPerSubCalcuF,
		IncrNpvPerSubTotalF: IncrNpvPerSubTotalF,
		OtherOverheadCalcuF: OtherOverheadCalcuF,
		OtherOverheadTotalF: OtherOverheadTotalF,
		EBITDACalcuF: EBITDACalcuF,
		EBITDATotalF: EBITDATotalF,
		EBITDAPercentCalcuF: EBITDAPercentCalcuF,
		EBITDAPercentTotalF: EBITDAPercentTotalF,
		NPVPerSubCalcuF: NPVPerSubCalcuF,
		NPVPerSubTotalF: NPVPerSubTotalF,
		EmptyYearsCalcuF: EmptyYearsCalcuF,
		EmptyYearsTotalF: EmptyYearsTotalF,
		BTNetRevenueCalcuF: BTNetRevenueCalcuF,
		BTNetRevenueTotalF: BTNetRevenueTotalF,
		NPVTotalCalcuF: NPVTotalCalcuF,
		NPVTotalTotalF: NPVTotalTotalF,
		DisconnectionAllowanceCalcuF:DisconnectionAllowanceCalcuF,
		DisconnectionAllowanceTotalF:DisconnectionAllowanceTotalF,
		DisconnectionAllowanceCalcuF:DisconnectionAllowanceCalcuF
    }
}

var PL = PL || {};
PL.Calculations = PLCalculations();
PL.Calculations.init();