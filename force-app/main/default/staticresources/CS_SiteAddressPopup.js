/************************************
 * 
 * 
 * BTNet and Cloud Work events logic
 * 
 * 
 * *********************************/

var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

// Listen to message from child window
eventer(messageEvent, function(e) {
    if(e !== null) {
        var key = e.message ? "message" : "data";
        var data = e[key];
        console.log("[CS_SiteAddressPopup] Event received: " + data.event_id);
        if(typeof data.event_id !== 'undefined') {
            if(data.event_id === 'site_address_popup_window_cancel') {
                console.log("[CS_SiteAddressPopup] Received event from popup window: CANCEL. Nothing to do.");
                jQuery('#AddressIframe').remove();
                jQuery('.cdk-overlay-pane').remove();
                jQuery('.cdk-overlay-backdrop').remove();
            } else if(data.event_id === 'site_address_popup_window_save') {
                try {
                    console.log("[CS_SiteAddressPopup] Received event from popup window: SAVE");
                    console.log("[CS_SiteAddressPopup] Received data from popup window: ");
                    console.log(data.data);
                    let returnedMap = data.data;
                     // BTnet
                    if(returnedMap.hasOwnProperty("cs_product") && returnedMap.cs_product === "btnet") {
                        if(returnedMap.hasOwnProperty("primary") && returnedMap.hasOwnProperty("serviceVariant") && returnedMap.hasOwnProperty("accessType") && returnedMap.hasOwnProperty("postCode") && returnedMap.hasOwnProperty("geo") && returnedMap.hasOwnProperty("pricingNode") && returnedMap.hasOwnProperty("threshold") && returnedMap.hasOwnProperty("nadKey") && returnedMap.hasOwnProperty("address") && returnedMap.hasOwnProperty("cs_product")) {
                            let threshold = 0;
                            if(returnedMap.threshold !== '') {
                                threshold = returnedMap.threshold;
                            }
                            updateFields(returnedMap.serviceVariant, returnedMap.accessType, returnedMap.geo, returnedMap.pricingNode, threshold, returnedMap.address);
                        } else {
                            console.log("[CS_SiteAddressPopup] Error! Data from popup missing!");
                        }
                    }
                    
                    // cloud work site address
                    if(returnedMap.hasOwnProperty("cs_product") && returnedMap.cs_product === "cloudwork_siteAddress") {
                       console.log("[CS_SiteAddressPopup] Cloud Work Site Address popup called");
                       updateFieldsCloudWorkSiteAddress(returnedMap.address, returnedMap.town, returnedMap.postCode, returnedMap.country, returnedMap.nadKey);
                    }
                    
                    // cloud work site address
                    if(returnedMap.hasOwnProperty("cs_product") && returnedMap.cs_product === "cloudwork_shippingAddress") {
                       console.log("[CS_SiteAddressPopup] Cloud Work Shipping Address popup called");
                       updateFieldsCloudWorkShippingAddress(returnedMap.address, returnedMap.town, returnedMap.postCode, returnedMap.country, returnedMap.nadKey);
                    }
                } catch(e) {
                    console.log("[CS_SiteAddressPopup] Exception while parsing json data from popup window!");
                    console.log(e);
                }
            }
        }
    }
}, false);

async function updateFields(serviceVariant, accessType, geo, pricingNode, threshold, address) {
    let solution = await CS.SM.getActiveSolution();
    for(let component of Object.values(solution.components)) {
        for(let configuration of Object.values(component.schema.configurations)) {
            if(configurationGuidUpdated == configuration.guid) {
                updateConfigurationAttributeValue('Service Variant', serviceVariant, serviceVariant, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('StandardLA', serviceVariant, serviceVariant, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Access Type', accessType, accessType, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Geography', geo, geo, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Primary Pricing Node', pricingNode, pricingNode, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Excess Construction Charge', threshold, threshold, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Excess Construction Charge Default Popup Value', threshold, threshold, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('ECC From OpenReach', 'No', 'No', false, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Address', address, address, true, solution, configuration.guid, true);
                let inputMap = {};
                inputMap["accessType"] = accessType;
                inputMap["contractTerm"] = configuration.getAttribute('Contract Term').value;
                inputMap["geography"] = geo;
                inputMap["standardLA"] = serviceVariant;
                inputMap["action"] = 'CS_BTNetLookupQueryForSolutionConsole';
                let currentBasket = await CS.SM.getActiveBasket();
                let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
                console.log(result)
                if(result["DataFound"] == true) {
                    updateConfigurationAttributeValue('Access Bearer', result["PriceItemId"], result["PriceItemName"], true, component, configuration.guid, false);
                }
                updateConfigNameBTNetOnMainComponent(configuration, solution, result["PriceItemName"]);
            }
        } 
    }
}

async function updateFieldsCloudWorkSiteAddress(siteAddress, siteTown, sitePostcode, siteCountry, siteNADKey) {
    let solution = await CS.SM.getActiveSolution();
    for(let component of Object.values(solution.components)) {
        for(let configuration of Object.values(component.schema.configurations)) {
            if(configurationGuidUpdated == configuration.guid) {
                console.log("[CS_SiteAddressPopup] Updating cloud Work site fields");
                updateConfigurationAttributeValue('Site-Address', siteAddress, siteAddress, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Site-Town', siteTown, siteTown, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Site-Postcode', sitePostcode, sitePostcode, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Site-Country', siteCountry, siteCountry, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Site NAD Key', siteNADKey, siteNADKey, true, solution, configuration.guid, true);

                updateConfigurationAttributeValue('Shipping-Address', siteAddress, siteAddress, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping-Town', siteTown, siteTown, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping-Postcode', sitePostcode, sitePostcode, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping-Country', siteCountry, siteCountry, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping NAD Key', siteNADKey, siteNADKey, true, solution, configuration.guid, true);

                updateConfigNameCloudWorkSiteComponent(configuration, solution, siteAddress);
                /*updateConfigurationAttributeValue('Service Variant', serviceVariant, serviceVariant, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('StandardLA', serviceVariant, serviceVariant, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Access Type', accessType, accessType, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Geography', geo, geo, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Primary Pricing Node', pricingNode, pricingNode, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Excess Construction Charge', threshold, threshold, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Excess Construction Charge Default Popup Value', threshold, threshold, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('ECC From OpenReach', 'No', 'No', false, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Address', address, address, true, solution, configuration.guid, true);
                let inputMap = {};
                inputMap["accessType"] = accessType;
                inputMap["contractTerm"] = configuration.getAttribute('Contract Term').value;
                inputMap["geography"] = geo;
                inputMap["standardLA"] = serviceVariant;
                inputMap["action"] = 'CS_BTNetLookupQueryForSolutionConsole';
                let currentBasket = await CS.SM.getActiveBasket();
                let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
                console.log(result)
                if(result["DataFound"] == true) {
                    updateConfigurationAttributeValue('Access Bearer', result["PriceItemId"], result["PriceItemName"], true, component, configuration.guid, false);
                }
                updateConfigNameBTNetOnMainComponent(configuration, solution, result["PriceItemName"]);*/
            }
        } 
    }
}

async function updateFieldsCloudWorkShippingAddress(siteAddress, siteTown, sitePostcode, siteCountry, shippingNADKey) {
    let solution = await CS.SM.getActiveSolution();
    for(let component of Object.values(solution.components)) {
        for(let configuration of Object.values(component.schema.configurations)) {
            if(configurationGuidUpdated == configuration.guid) {
                console.log("[CS_SiteAddressPopup] Updating cloud Work shipping fields");

                updateConfigurationAttributeValue('Shipping-Address', siteAddress, siteAddress, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping-Town', siteTown, siteTown, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping-Postcode', sitePostcode, sitePostcode, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping-Country', siteCountry, siteCountry, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Shipping NAD Key', shippingNADKey, shippingNADKey, true, solution, configuration.guid, true);

                updateConfigNameCloudWorkSiteComponent(configuration, solution, "Cloud Work TODO");
                /*updateConfigurationAttributeValue('Service Variant', serviceVariant, serviceVariant, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('StandardLA', serviceVariant, serviceVariant, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Access Type', accessType, accessType, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Geography', geo, geo, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Primary Pricing Node', pricingNode, pricingNode, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Excess Construction Charge', threshold, threshold, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Excess Construction Charge Default Popup Value', threshold, threshold, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('ECC From OpenReach', 'No', 'No', false, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Address', address, address, true, solution, configuration.guid, true);
                let inputMap = {};
                inputMap["accessType"] = accessType;
                inputMap["contractTerm"] = configuration.getAttribute('Contract Term').value;
                inputMap["geography"] = geo;
                inputMap["standardLA"] = serviceVariant;
                inputMap["action"] = 'CS_BTNetLookupQueryForSolutionConsole';
                let currentBasket = await CS.SM.getActiveBasket();
                let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
                console.log(result)
                if(result["DataFound"] == true) {
                    updateConfigurationAttributeValue('Access Bearer', result["PriceItemId"], result["PriceItemName"], true, component, configuration.guid, false);
                }
                updateConfigNameBTNetOnMainComponent(configuration, solution, result["PriceItemName"]);*/
            }
        } 
    }
}