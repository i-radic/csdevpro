var PDattributes = [
	{
		"Cloud Phone" : {
			"recurringCharge" : [
				"Recurring_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Support_Cost_0",
				"Recurring_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0",
				"Customer_Level_Support_Cost_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Total_Number_of_Users_0"
		},
		
		"Cloud Phone Devices" : {
			"recurringCharge" : [
			],
			
			"recurringCost" : [
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Number_of_Users_0"
		},
		
		"Cloud Phone Add On Licenses" : {
			"recurringCharge" : [
				"Total_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Total_Number_of_Users_0"
		},
		
		"BT Product Fixed Broadband" : {
			"recurringCharge" : [
				"Total_Broadband_Plan_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Broadband_Plan_Monthly_Cost_0",
				"Total_Monthly_IP_Block_Costs_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],

			"tenure" : "Tenure_0"
			
		}, 
	},
	{			
		"BT Product Lines and Calls" : {
			"recurringCharge" : [
				"Total_Recurring_Charges_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Costs_0"
			],
			
			"oneoffCharge" : [
				"Total_setup_charges_0"
			],
			
			"oneoffCost" : [
				"Line_Setup_Costs_Total_0"
			],

			"numberOfUsers" : "Number_of_Users_0",
			
			"tenure" : "Tenure_0"
			
		}, 
	},
	{			
		"MVR Enterprise" : {
			"recurringCharge" : [
				"Total_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
	
			],
			
			"oneoffCost" : [
				
			],

			"numberOfUsers" : "Quantity_0_0",
			
			"tenure" : "Tenure_0"
			
		}, 
	},
	{			
		"PBX to EE" : {
			"recurringCharge" : [
				"Total_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
	
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0"
			
		}, 
	},
	{			
		"Fixed Line Add On" : {
			"recurringCharge" : [
				"Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],

			"numberOfUsers" : "Quantity_0",
			
			"tenure" : "Tenure_0"
			
		}, 
	},
	{			
		"BT Product Line Features" : {
			"recurringCharge" : [
				"Feature_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
			
		}, 
	},
	{			
		"Special Conditions" : {
			"recurringCharge" : [
			
			],
			
			"recurringCost" : [
			
			],
			
			"oneoffCharge" : [
				"Total_Basket_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_Cost_0"
			],

			"numberOfUsers" : "Total_Number_of_Devices_0",
			
			"tenure" : "Tenure_0"
			
		}, 
	},
	{
		"Mobile Voice" : {
			"recurringCharge" : [
				"Payment_Type_Charge_0",
				"MVOC_0",
				"SMS_Roaming_Usage_0",
				"Roaming_Data_Usage_0"
			],
			
			"recurringCost" : [
				"Payment_Type_Cost_0",
				"MVOCo_0",
				"SMS_Roaming_Cost_0",
				"Roaming_Data_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_Setup_Charge_0"
			],
			
			"oneoffCost" : [
				"Sales_Overhead_0",
				"Total_Setup_Cost_0"
			],
			
			"numberOfUsers" : "Total_number_of_users_0",
			
			"tenure" : "Tenure_0"
		}
	},
	{	
		"User Group" : {
			"recurringCharge" : [
				"Care_Monthly_Charges_0",
				"SMS_Usage_0",
				"tmpRoamingUsage_0",
				"Data_Usage_0",
				"Incoming_Revenue_0"
			],
			
			"recurringCost" : [
				"Care_Monthly_Costs_0",
				"SMS_Cost_0",
				"tmpRoamingCost_0",
				"Data_Cost_0"
			],
			
			"oneoffCharge" : [
				"Care_One_Off_Total_0",
				"Total_Cost_0",
				"Roaming_Setup_Total_0"
			],
			
			"oneoffCost" : [
				"Total_Care_One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"Single Add On" : {
			"recurringCharge" : [
				"SMS_MRCh_0",
				"Fixed_MRCh_0",
				"MVoice_MRCh_0",
				"Other_MRCh_0",
				"Data_MRCh_0"
				
			],
			
			"recurringCost" : [
				"SMS_MRCo_0",
				"Data_MRCo_0",
				"Fixed_MRCo_0",
				"MVoice_MRCo_0",
				"Other_MRCo_0"
				
			],
			
			"oneoffCharge" : [
				"SMS_OOCh_0",
				"Data_OOCh_0",
				"Fixed_OOCh_0",
				"MVoice_OOCh_0",
				"Other_OOCh_0"
			],
			
			"oneoffCost" : [
				"SMS_OOCo_0",
				"Data_OOCo_0",
				"Fixed_OOCo_0",
				"MVoice_OOCo_0",
				"Other_OOCo_0"
			],
			
			"tenure" : "Tenure_0"
			
		},
	},
	{			
		"Accessory" : {
			"recurringCharge" : [
			
			],
			
			"recurringCost" : [
			
			],
			
			"oneoffCharge" : [
				"Total_Charges_0"
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0"
			
		}, 
	},
	{			
		"Shared Add On" : {
			"recurringCharge" : [
				"SMS_MRCh_0",
				"MVoice_MRCh_0",
				"Other_MRCh_0",
				"Data_MRCh_0"
				
			],
			
			"recurringCost" : [
				"SMS_MRCo_0",
				"Data_MRCo_0",
				"MVoice_MRCo_0",
				"Other_MRCo_0"
				
			],
			
			"oneoffCharge" : [
				"SMS_OOCh_0",
				"Data_OOCh_0",
				"MVoice_OOCh_0",
				"Other_OOCh_0"
			],
			
			"oneoffCost" : [
				"SMS_OOCo_0",
				"Data_OOCo_0",
				"MVoice_OOCo_0",
				"Other_OOCo_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{		
		"Service Add On" : {
			"recurringCharge" : [
				"Monthly_Recurring_Charge_0"
				
			],
			
			"recurringCost" : [
				"Monthly_Recurring_Cost_0"
				
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"Mobile Broadband User Group" : {
			"recurringCharge" : [
				
			],
			
			"recurringCost" : [
				
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Mobile Broadband" : {
			"recurringCharge" : [
				"Total_Monthly_Rental_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Monthly_Rental_Cost_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				"Sales_Overhead_0"
			],
			
			"numberOfUsers" : "Total_Number_of_Users_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Lone Worker" : {
			"recurringCharge" : [
				"Total_Monthly_Rental_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Monthly_Rental_Cost_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"numberOfUsers" : "Total_Number_of_Users_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Lone Worker User Group" : {
			"recurringCharge" : [
				
			],
			
			"recurringCost" : [
				
			],
			
			"oneoffCharge" : [
				"Device_One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"Device_One_Of_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Fixed Line" : {
			"recurringCharge" : [
				
			],
			
			"recurringCost" : [
				
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"numberOfUsers" : "Number_of_Lines_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Lines and Channels" : {
			"recurringCharge" : [
				"Tot_Rec_Chg_0"
			],
			
			"recurringCost" : [
				"Total_Rec_Cost_0"
			],
			
			"oneoffCharge" : [
				"Tot_OneOff_Chg_0"
			],
			
			"oneoffCost" : [
				"Total_OneOff_Cost_0"
			],
			
			"numberOfUsers" : "Number_of_Lines_0",
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"Fixed Line Calling Bundle" : {
			"recurringCharge" : [
				"Monthly_Net_Charge_0"
			],
			
			"recurringCost" : [
				"Monthly_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Fixed Broadband" : {
			"recurringCharge" : [
				"Recurring_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Recurring_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"numberOfUsers" : "Total_Number_of_Users_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Fixed User Group" : {
			"recurringCharge" : [
				
			],
			
			"recurringCost" : [
				
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Fixed Add on" : {
			"recurringCharge" : [
				"Total_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Security" : {
			"recurringCharge" : [
				"Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],
			
			"numberOfUsers" : "Total_Number_of_Users_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Security User Group" : {
			"recurringCharge" : [
				"Total_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"numberOfUsers" : "Total_Number_of_Users_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"EMM Advanced" : {
			"recurringCharge" : [
				"EMM_Licenses_Fee_0"
			],
			
			"recurringCost" : [
				"EMM_Licenses_Cost_0"
			],
			
			"oneoffCharge" : [
				"Setup_Entity_Fee_0"
			],
			
			"oneoffCost" : [
				"Setup_Entity_Cost_0"
			],
			
			"numberOfUsers" : "Number_of_Users_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"4GEE Public WiFi in a Box" : {
			"recurringCharge" : [
				"Total_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"numberOfUsers" : "Quantity_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Data VPN" : {
			"recurringCharge" : [
				
			],
			
			"recurringCost" : [
				
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"numberOfUsers" : "Quantity_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Leased Line" : {
			"recurringCharge" : [
				"TRCh_0"
			],
			
			"recurringCost" : [
				"TRCo_0"
			],
			
			"oneoffCharge" : [
				"TOOCh_0"
			],
			
			"oneoffCost" : [
				"TOOCo_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Additional Services" : {
			"recurringCharge" : [
				"TRCh_0"
			],
			
			"recurringCost" : [
				"TRCo_0"
			],
			
			"oneoffCharge" : [
				"TOOCh_0"
			],
			
			"oneoffCost" : [
				"TOOCo_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Router Requirements" : {
			"recurringCharge" : [
				"TRCh_0"
			],
			
			"recurringCost" : [
				"TRCo_0"
			],
			
			"oneoffCharge" : [
				"TOOCh_0"
			],
			
			"oneoffCost" : [
				"TOOCo_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Fieldlink" : {
			"recurringCharge" : [
				"Recurring_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Recurring_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"numberOfUsers" : "Total_Number_of_Users_0",
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Fieldlink User Group" : {
			"recurringCharge" : [
				"Roaming_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Roaming_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"Roaming_Setup_Charge_0"
			],
			
			"oneoffCost" : [
				"Roaming_Setup_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Fieldlink Shared Add On" : {
			"recurringCharge" : [
				"Monthly_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Monthly_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"Monthly_One_Off_Recurring_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Coverage Solutions" : {
			"recurringCharge" : [
				"Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Coverage Solutions Device" : {
			"recurringCharge" : [
				"Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Credit Funds" : {
			"recurringCharge" : [
				
			],
			
			"recurringCost" : [
				"Rolling_Air_Time_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				"Cloned_Staged_Air_Time_0",
				"Cloned_Tech_Fund_0",
				"Cloned_Unconditional_Cheque_0",
				"Cloned_Buy_out_Cheque_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Total Resource" : {
			"recurringCharge" : [
				"Monthly_Price_0"
			],
			
			"recurringCost" : [
				"Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Length_of_Contract_0"
		},
	},
	{			
		"Other Costs" : {
			"recurringCharge" : [
				"Monthly_Revenue_0"
			],
			
			"recurringCost" : [
				"Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
				"Upfront_Revenue_0"
			],
			
			"oneoffCost" : [
				"Upfront_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"Rapid Site" : {
			"recurringCharge" : [
				"Total_Recurring_Charges_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Costs_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Number_of_Sites_0"
		},
	},
	{			
		"Connected Vehicle" : {
			"recurringCharge" : [
				"Total_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_off_Charge_0"
			],
			
			"oneoffCost" : [
				"Total_One_off_Cost_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Number_Required_0"
		},
	},
	{			
		"Additional Features and Services" : {
			"recurringCharge" : [
				"Total_Recurring_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_One_Off_0"
			],
			
			"oneoffCost" : [
				"Total_One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		},
	},
	{			
		"Mobile Data for Tablets" : {
			"recurringCharge" : [
				"Total_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
			
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Number_of_Tablet_Users_0"
		}, 
	},
	{			
		"Tablet Bundles" : {
			"recurringCharge" : [
				"Monthly_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Cost_0"
			],
			
			"oneoffCharge" : [
			
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"MVB Aggregated Data" : {
			"recurringCharge" : [
				"Total_Monthly_Charges_0"
			],
			
			"recurringCost" : [
				"Total_Monthly_Cost_0"
			],
			
			"oneoffCharge" : [
			
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"M2M" : {
			"recurringCharge" : [
				"Total_Recurring_Revenue_0"
			],
			
			"recurringCost" : [
				
			],
			
			"oneoffCharge" : [
				"Total_Device_Costs_0"
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Quantity_0"
		}, 
	},
	{			
		"M2M Bundle" : {
			"recurringCharge" : [
				"Total_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"One_Off_Charge_0"
			],
			
			"oneoffCost" : [
				"One_Off_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"Super Bundles" : {
			"recurringCharge" : [
				"Total_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Payback_Period_Months_0"
		}, 
	},
	{			
		"Fixed Landline Bespoke" : {
			"recurringCharge" : [
				"Total_Recurring_Charges_0"
			],
			
			"recurringCost" : [
				"Total_Recurring_Costs_0"
			],
			
			"oneoffCharge" : [
				"Total_setup_charges_0"
			],
			
			"oneoffCost" : [
				"Line_Setup_Costs_Total_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Number_of_Users_0"
		}, 
	},
	{			
		"International Service Plan Option" : {
			"recurringCharge" : [
				"Total_Monthly_Charge_0"
			],
			
			"recurringCost" : [
				"Total_Monthly_Cost_BTW_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"Mobile Voice VPN" : {
			"recurringCharge" : [
				"Monthly_Recurring_Charge_0"
			],
			
			"recurringCost" : [
				"Monthly_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				
			],
			
			"oneoffCost" : [
				
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Number_of_users_0"
		}, 
	},
	{			
		"Mobile Voice Bespoke" : {
			"recurringCharge" : [
				"PnL_Data_Monthly_Charges_0",
				"PnL_Mobile_Voice_Recurring_Charges_0",
				"PnL_SMS_Charge_0",
				"Incoming_Revenue_Recurring_0"
			],
			
			"recurringCost" : [
				"PnL_Data_Monthly_Costs_0",
				"PnL_Mobile_Voice_Recurring_Cost_0",
				"PnL_SMS_Cost_0"
			],
			
			"oneoffCharge" : [
				"Smart_Numbers_Connection_Total_0",
				"Professional_Services_Charge_0"
			],
			
			"oneoffCost" : [
				"Smart_Numbers_Connection_Cost_Total_0",
				"Professional_Services_Cost_0",
				"PnL_Sales_Commission_Cost_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Number_of_Users_0"
		}, 
	},
	{			
		"MVB Devices and Accessories" : {
			"recurringCharge" : [
				
			],
			
			"recurringCost" : [
				
			],
			
			"oneoffCharge" : [
				"Total_Device_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_Device_Cost_0"
			],
			
			"tenure" : "Tenure_0"
		}, 
	},
	{			
		"Mobile Data" : {
			"recurringCharge" : [
				"Data_Recurring_Charges_0"
			],
			
			"recurringCost" : [
				"Data_Recurring_Cost_0"
			],
			
			"oneoffCharge" : [
				"Total_Device_Charges_0"
			],
			
			"oneoffCost" : [
				"Total_Device_Cost_0"
			],
			
			"tenure" : "Tenure_0",
			
			"Number_of_Users_0" : "Number_of_Users_0"
		}
	},
	{
		"Mobile Voice SME" : {
			"totalCharge" : [
				"Total_Charges_After_Discount_0"
			],
			
			"totalInvestmentPot" : [
				"Investment_Pot_0"
			],
			
			"chargesPerCTN" : [
				"Charges_per_CTN_0"
			],
			
			"totalDeviceCharges" : [
				"Total_Device_Charges_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Total_number_of_users_0"
		}
	},
	{
		"User Group SME" : {
			"totalCharge" : [
				"Total_Recurring_Charges_0"
			],
			
			"totalInvestmentPot" : [
				
			],
			
			"chargesPerCTN" : [
				
			],
			
			"totalDeviceCharges" : [
				"Total_Device_Charges_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : ""
		}
	},
	{
		"Single Add On SME" : {
			"totalCharge" : [
				"Total_Recurring_Charges_0"
			],
			
			"totalInvestmentPot" : [
				
			],
			
			"chargesPerCTN" : [
				
			],
			
			"totalDeviceCharges" : [
				
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : ""
		
		}
	},
	{
		"Shared Add On SME" : {
			"totalCharge" : [
				"Total_Recurring_Charges_0"
			],
			
			"totalInvestmentPot" : [
			    "Add_Ons_Investment_Pot_0"
			],
			
			"chargesPerCTN" : [
				
			],
			
			"totalDeviceCharges" : [
				
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : ""
		}
	},
	{
		"Mobile Broadband and Tablet" : {
			"totalCharge" : [
				"Total_Charges_After_Discount_0"
			],
			
			"totalInvestmentPot" : [
				"Investment_Pot_0"
			],
			
			"chargesPerCTN" : [
				"Charges_per_CTN_0"
			],
			
			"totalDeviceCharges" : [
				"Total_Device_Charges_0"
			],
			
			"tenure" : "Tenure_0",
			
			"numberOfUsers" : "Total_number_of_users_0"
		}
	}
];