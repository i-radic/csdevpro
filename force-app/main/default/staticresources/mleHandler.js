function mleMsgToTPE(message, methodName) {
    var urlToParse = window.location.search;
    var resultParams = parseQuery(urlToParse);
    var defId = resultParams['productDefinitionId'];
    var msg = btoa(JSON.stringify({ value: message, sender: defId, recipient: 'TPE', method: methodName }));
    window.parent.postMessage(msg, '*');
}

function parseQuery(queryString) {
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}

function onSave(){
    console.log('on Save');
    mleMsgToTPE('save', 'reRenderMainDetail');
}

function onRemove(){
    console.log('on Remove');
    mleMsgToTPE('remove', 'reRenderMainDetail');
}

function checkIfMLELoaded(){
    console.log('Waiting for MLE');
    
    CSMLEAPI.onSaveSuccess(onSave);
    jQuery('button:contains("Close")').hide();

    var removeButton = jQuery('button[ng-click="delete()"]');
    removeButton.on('click', onRemove);
}

setTimeout(checkIfMLELoaded, 1000);

