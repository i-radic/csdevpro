/*
	BTnet
*/
	function displayPrimary()
	{
		let BTnetPrimary = CS.getAttributeValue('PrimaryBTnetRecordID_0');
		let BTNetSecondary = CS.getAttributeValue('SecondaryBTnetRecordID_0');
		let isSecondary = 'false';
		displayDate(BTnetPrimary, BTNetSecondary, isSecondary);
	}
	
	
	function displaySecondary()
	{
		let BTNetSecondary = CS.getAttributeValue('SecondaryBTnetRecordID_0');
		let BTnetPrimary = CS.getAttributeValue('PrimaryBTnetRecordID_0');
		let isSecondary = 'true';
		displayDate(BTnetPrimary, BTNetSecondary, isSecondary);
	}

	
	function displayDate(BTNetPrimary, BTNetSecondary, isSecondary)
	{
		//alert('java Script - Option 196');
		let el = jQuery('.slds-section__content');
		
		if(jQuery('#BtnetIframe').length>0)
		{
			jQuery('#BtnetIframe').show();
		}
		else
		{	
			let OpportunityId = CS.getAttributeValue('OpportunityID_0');
			let BasketId = CS.getAttributeValue('BasketID_0');
			let ProductId = getQueryVariable("configId");
			let warningsms = 'false';
			let urlString = '<div id="BtnetIframe" class="demo-only" height="100%">';
					urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
						urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
							urlString += '<div class="slds-modal__header">';
								urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
									urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
										urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
									urlString +='</svg>';
								urlString += '</button>';
								urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
								if(isSecondary == 'true'){
									urlString +='SECONDARY </h2>';
									urlString += '</div>';
								}else{
									urlString +='PRIMARY</h2>';
									urlString += '</div>';
									}
							
								if(isSecondary == 'true')
								{
									let SerVarSelected = CS.getAttributeValue('Service_Variant_0');
									if((BTNetSecondary == '' || BTNetSecondary == null) && SerVarSelected != 'Back Up')
									{
										urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 25%;">';
										urlString += '<div class="slds-notify_alert slds-theme_alert-texture slds-theme_error" role="alert" style="text-align: center; margin-top: 3rem; color: red;">';
										urlString += '<span class="slds-assistive-text">error</span>';
										urlString += '<h1 style="font-size: 2em;">  Primary lookup is required </h1>';
										urlString += '</div>';
										warningsms = 'true';
										
									} else
									{
										if(SerVarSelected == 'Standard' || SerVarSelected == 'BGP' || SerVarSelected == '' || SerVarSelected == null)
										{
											urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 25%;">';
											urlString += '<div class="slds-notify_alert slds-theme_alert-texture slds-theme_error" role="alert" style="text-align: center; margin-top: 3rem; color: red;">';
											urlString += '<span class="slds-assistive-text">error</span>';
											urlString += '<h1 style="font-size: 2em;"> Service Variant do not allow create a secondary lookup. </h1>';
											urlString += '</div>';
											warningsms = 'true';
										}
									}											
								}
							
								if(warningsms == 'false')
								{
									
										urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
											//alert('BTNetPrimary>>'+BTNetPrimary);
											//alert('BTNetSecondary>>'+BTNetSecondary);
											if(BTNetPrimary != null && BTNetPrimary != '' && isSecondary == 'false'){
												//alert('in BTNetPrimary>>');
												urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_BTnet_QuickCreate?Id='+BTNetPrimary+'&';
											}else if(BTNetSecondary != null && BTNetSecondary != '' && isSecondary == 'true'){
												//alert('in BTNetSecondary>>');
												urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_BTnet_QuickCreate?Id='+BTNetSecondary+'&';
											}else{
												//alert('in else>>');
												urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_BTnet_QuickCreate?';
											}
											if(BTNetPrimary != null && BTNetPrimary != ''){
												urlString +='BTNetPrimaryId='+BTNetPrimary+'&';
											}
											if(BTNetSecondary != null && BTNetSecondary != ''){
												urlString +='BTNetSecondaryId='+BTNetSecondary+'&';
											}
											if(isSecondary != null && isSecondary != ''){
												urlString +='isSecondary='+isSecondary+'&';
											}
										
											urlString += 'OpportunityId='+OpportunityId+'&BasketId='+BasketId+'&ProductId='+ProductId+'"/>';

								}
							
							urlString += '</div>';
							if(warningsms == 'true'){
								urlString += '<div class="slds-modal__footer">';
									urlString += '<button class="slds-button slds-button--neutral" id="BTnetIframeCloseButton" onclick="closeIframeModal();">Close</button>';
								urlString += '</div>';
							}
							
						urlString += '</div>';
					urlString += '</section>';
					urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
				urlString += '</div>';
				//alert('urlString>>>'+urlString);
			jQuery(el[0]).append(urlString);
		
			//if(BTnetId != null && BTnetId != ''){
			//	if(isSecondary == 'true'){
			//		CS.setAttribute('SecondaryBTnetRecordID_0','000000000000000');
			//		delete CS.lookupQueryCache["Lookup(Secondary BTnet Object Lookup)-[\"{\\\"SecondaryBTnetRecordID\\\":\\\""+BTnetId+"\\\"}\"]"];	
			//	}else{
			//		CS.setAttribute('PrimaryBTnetRecordID_0','000000000000000');
			//		delete CS.lookupQueryCache["Lookup(Primary BTnet Object Lookup)-[\"{\\\"PrimaryBTnetRecordID\\\":\\\""+BTnetId+"\\\"}\"]"];					
			//	}
			//	CS.setAttribute('isChanged_0','No');
			//}
			CS.setAttribute('isChanged_0','No');
		}
	}
	
	
	function getQueryVariable(variable)
	{
		   var query = window.location.search.substring(1);
		   var vars = query.split("&");
		   for (var i=0;i<vars.length;i++) {
				   var pair = vars[i].split("=");
				   if(pair[0] == variable){return pair[1];}
		   }
		   return(false);
	}

	
    function handleMessage(event) 
    {  
        var message = event.data;
		var parseMessage = message.split('.'); 
		var modalAction = parseMessage[0];
		var userAction = parseMessage[1];
		
		//alert('message111>>>'+message);
        if(modalAction =='closeModal') 
        { 
			//jQuery('#BtnetIframe').hide();
			jQuery('#BtnetIframe').remove();
			if(userAction =='primary' )
			{
				//alert('primary option');
				var btNetId = parseMessage[2]; 
				//CS.setAttribute('PrimaryBTnetRecordID_0','');
				CS.setAttribute('isChanged_0','Yes');
				if(btNetId != null ){
				
					let BTnetExist = CS.getAttributeValue('PrimaryBTnetRecordID_0');
					if(BTnetExist != null && BTnetExist != ''){
						CS.setAttribute('PrimaryBTnetRecordID_0','000000000000000');
						delete CS.lookupQueryCache["Lookup(Primary BTnet Object Lookup)-[\"{\\\"PrimaryBTnetRecordID\\\":\\\""+BTnetExist+"\\\"}\"]"];	
					}					
					CS.setAttribute('PrimaryBTnetRecordID_0',btNetId);
				}
				
				//alert('primary else option'+ CS.getAttributeValue('SecondaryBTnetRecordID_0'));
				let secondaryBTnetRecord = CS.getAttributeValue('SecondaryBTnetRecordID_0');
				if(secondaryBTnetRecord != null && secondaryBTnetRecord != ''){
					CS.setAttribute('SecondaryBTnetRecordID_0','000000000000000');
					delete CS.lookupQueryCache["Lookup(Secondary BTnet Object Lookup)-[\"{\\\"SecondaryBTnetRecordID\\\":\\\""+secondaryBTnetRecord+"\\\"}\"]"];		
					CS.setAttribute('SecondaryBTnetRecordID_0',null);	
				}
				
			}
			
			if(userAction =='secondary' )
			{
				var btNetId = parseMessage[2]; 
				//CS.setAttribute('SecondaryBTnetRecordID_0','');
				CS.setAttribute('isChanged_0','Yes');
				if(btNetId != null )
				{
					let BTnetExist = CS.getAttributeValue('SecondaryBTnetRecordID_0');
					if(BTnetExist != null && BTnetExist != ''){
						CS.setAttribute('SecondaryBTnetRecordID_0','000000000000000');
						delete CS.lookupQueryCache["Lookup(Secondary BTnet Object Lookup)-[\"{\\\"SecondaryBTnetRecordID\\\":\\\""+BTnetExist+"\\\"}\"]"];		
					}
					CS.setAttribute('SecondaryBTnetRecordID_0',btNetId);
				}
			}
			
			if(userAction =='dual' )
			{
				var btNetId = parseMessage[2]; 
				var secondaryBTNetId = parseMessage[3]; 
				CS.setAttribute('isChanged_0','Yes');
				if(btNetId != null ){
				
					let BTnetExist = CS.getAttributeValue('PrimaryBTnetRecordID_0');
					if(BTnetExist != null && BTnetExist != ''){
						CS.setAttribute('PrimaryBTnetRecordID_0','000000000000000');
						delete CS.lookupQueryCache["Lookup(Primary BTnet Object Lookup)-[\"{\\\"PrimaryBTnetRecordID\\\":\\\""+BTnetExist+"\\\"}\"]"];	
					}					
					CS.setAttribute('PrimaryBTnetRecordID_0',btNetId);
				}
				if(secondaryBTNetId != null )
				{
					let BTnetExist = CS.getAttributeValue('SecondaryBTnetRecordID_0');
					if(BTnetExist != null && BTnetExist != ''){
						CS.setAttribute('SecondaryBTnetRecordID_0','000000000000000');
						delete CS.lookupQueryCache["Lookup(Secondary BTnet Object Lookup)-[\"{\\\"SecondaryBTnetRecordID\\\":\\\""+BTnetExist+"\\\"}\"]"];		
					}
					CS.setAttribute('SecondaryBTnetRecordID_0',secondaryBTNetId);
				}else{
					let BTnetExist = CS.getAttributeValue('SecondaryBTnetRecordID_0');
					if(BTnetExist != null && BTnetExist != ''){
						CS.setAttribute('SecondaryBTnetRecordID_0','000000000000000');
						delete CS.lookupQueryCache["Lookup(Secondary BTnet Object Lookup)-[\"{\\\"SecondaryBTnetRecordID\\\":\\\""+BTnetExist+"\\\"}\"]"];		
					}
					CS.setAttribute('SecondaryBTnetRecordID_0',null);
				}
			}
		}
    }
    window.addEventListener('message', handleMessage, false); 

	
	function closeIframeModal()
	{
		jQuery('#BtnetIframe').remove();
	}
	
	
	
	