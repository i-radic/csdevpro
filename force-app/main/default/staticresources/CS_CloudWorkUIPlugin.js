async function displayPopupSiteAddressCloudWork(configurationGuid){
    console.log("[CS_CloudWorkUIPlugin] Rendering, calling from Cloud Voice Site Address");
    let urlString = '';
    urlString += '<div id="BtnetIframe" class="demo-only" height="100%">';
        urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
            urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
                urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
                    urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudWork_QuickCreate?cs_product=cloudwork_siteAddress">';
                urlString += '</div>';
            urlString += '</div>';
       	urlString += '</section>';
    urlString += '</div>';
        
    configurationGuidUpdated = configurationGuid;
    return urlString;
}

async function displayPopupShippingAddressCloudWork(configurationGuid){
    console.log("[CS_CloudWorkUIPlugin] Rendering, calling from Cloud Voice Shipping Address");
    let urlString = '';
    urlString += '<div id="BtnetIframe" class="demo-only" height="100%">';
        urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
            urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
                urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
                    urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudWork_QuickCreate?cs_product=cloudwork_shippingAddress">';
                urlString += '</div>';
            urlString += '</div>';
       	urlString += '</section>';
    urlString += '</div>';
        
    configurationGuidUpdated = configurationGuid;
    return urlString;
}

var cloudWorkSections = {
    'Site Address' : 'Site Address, reference to main section',
    'Access Type' : 'Access Type, reference to main section'
}

// pay attention between "reference to main section" labels!!!!!!

var cloudWorkMainSections = {
    'Site Address, reference to main section' : 'Address Details',
    'Access Type, reference to main section' : 'Additional Details'
}

formatPcUIForCloudWork = async() => {

    console.log("[CS_CloudWorkUIPlugin] Rendering UI sections for Cloud Work");

    let labels = document.querySelectorAll('label');
    
    let updatedParentElements = [];
    if (labels) {
        let configuredSections = Object.keys(cloudWorkSections);
        for (let i = 0 ; i< labels.length; i++) {
            if (configuredSections.includes(labels[i].innerText)) {
                let cs = labels[i].parentElement.parentElement.getElementsByClassName('cs-custom-section');
                if (cs && cs.length > 0 && !updatedParentElements.includes(labels[i].parentElement.parentElement)) {
                    updatedParentElements.push(labels[i].parentElement.parentElement);
                }
            }
        }

        for (let i = 0 ; i< labels.length; i++) {
            if (configuredSections.includes(labels[i].innerText)) {
                if (updatedParentElements.includes(labels[i].parentElement.parentElement)) {
                    continue;
                }
                
                let configuredMainSections = Object.keys(cloudWorkMainSections);
                if (configuredMainSections.includes(cloudWorkSections[labels[i].innerText])) {
                    let mainHeader = document.createElement('h3');
                    mainHeader.classList.add('cs-custom-section');
                    mainHeader.classList.add('slds-size_5-of-5');
                    mainHeader.classList.add('modal-header');
                    mainHeader.classList.add('slds-modal__header');
                    mainHeader.classList.add('slds-align_absolute-center');
                    mainHeader.innerText = cloudWorkMainSections[cloudWorkSections[labels[i].innerText]];
                    mainHeader.style.height='15px';
                    mainHeader.style.justifyContent = 'left';
                    mainHeader.style.fontWeight = 'bold';
                    mainHeader.style.marginTop = '15px';
                    mainHeader.style.marginBottom = '15px';
                    labels[i].parentElement.parentElement.insertBefore(mainHeader,labels[i].parentElement);
                }
            }
        }
    }
    return Promise.resolve(true);
}

function updateConfigNameCloudWorkSiteComponent(_config, _solution, _accessType) {    
	let siteName = 'Cloud Work - Primary Site: ' + _accessType;

    console.log("[CS_CloudWorkUIPlugin] Setting up new config name on: Cloud Work -> site: " + siteName);
	
	updateConfigurationAttributeValue('Config Name',
                                         siteName,
                                         siteName,
                                         false,
                                         _solution,
                                         _config.guid,
                                         true);

    _config.configurationName = siteName;
    console.log(_config);

    /*let componentNumber = _config.getAttribute('ComponentID').value;

    let components = _solution.getComponents();
    Object.values(components['components']).forEach(compo => {
        if (compo.name == BT_NET_CONST.portSpeed || compo.name == BT_NET_CONST.managedCPE) {
            Object.values(compo.schema.configurations).forEach(conf => {
                if (conf.getAttribute('ComponentID').value == componentNumber) {
                    updateConfigurationAttributeValue('Config Name',
                                         siteName,
                                         siteName,
                                         false,
                                         _solution,
                                         conf.guid,
                                         true);

                    conf.configurationName = siteName;
                }
            });
        }
    });*/
}