var onLoadRows;
//var npvValue = 9.6;
window.PNL.subscribe('onLoadRows', (data) => {
    return new Promise((resolve) => {
		onLoadRows = data;
        resolve(data);
    });
});

var dataHolder;
window.PNL.subscribe('onFinishCalculations', (data) => {
    return new Promise((resolve) => {
		//start
		//check what is NPV and how to calc it
		//this here is only example of how to manipulate with fields
		/*for (let i = 0; i < data.length; i++) {
			if (onLoadRows[i].rowClass.includes('NPVCalc')) {
				for (let j = 0; j < data[i].data.length; j++) {Header
					data[i].data[j] = data[i].data[j] * npvValue;
				}
			}
		}*/
		//end
		dataHolder = data;
        resolve(data);
    });
});

window.PNL.subscribe('onLoad', (data) => {
	var yearRows = document.getElementsByClassName('cs-table-header');
	for (let i = 0; i < yearRows.length; i++){
		var row = yearRows[i];
		var divsInRow = row.getElementsByTagName('div');
		if (divsInRow && divsInRow.length > 0){
			for (let j=0; j < divsInRow.length; j++){
				var spanElem = divsInRow[j];
				var innerText = spanElem.innerText;
				if (innerText.substring(0,4) === 'Year') {
					var yearNumber = innerText.substring(5,innerText.length);
					var numberInt = parseInt(yearNumber) - 1;
					if (numberInt == 0) {
					    spanElem.innerText = 'Initial';
					} else {
					    spanElem.innerText = 'Year ' + numberInt;
					}
				}
				spanElem.style.fontWeight = "bold";
			}
		}
	}
	
	var btClassRows = document.getElementsByClassName('BTcost');
	for (let i = 0; i < btClassRows.length; i++){
		var tds = btClassRows[i].getElementsByTagName('td');
		for (let j = 1; j < tds.length; j++) {
			var spanElem = tds[j];
			spanElem.style.color = "red";
		}
	}
	
	var boldingRows = document.getElementsByClassName('BTbold');
	for (let i = 0; i < boldingRows.length; i++){
		var tds = boldingRows[i].getElementsByTagName('td');
		for (let j = 0; j < tds.length; j++) {
			var spanElem = tds[j];
			spanElem.style.fontWeight = "bold";
		}
	}
	
	var highlightedRows = document.getElementsByClassName('BTcolor');
	for (let i = 0; i < highlightedRows.length; i++){
		var tds = highlightedRows[i].getElementsByTagName('td');
		for (let j = 0; j < tds.length; j++) {
			var spanElem = tds[j];
			spanElem.style.backgroundColor = "#BCB2E4";
		}
	}
	
	var headerRows = document.getElementsByClassName('Header');
	for (let i = 0; i < headerRows.length; i++){
		var tds = headerRows[i].getElementsByTagName('td');
		for (let j = 0; j < tds.length; j++) {
			var spanElem = tds[j];
			if (j == 0) {
				spanElem.style.fontWeight = "bold";
			} else {
				spanElem.innerHTML = '';
			}
			spanElem.style.backgroundColor = "#5514b4";
			spanElem.style.color = "#ffffff";
		}
	}
	
	var headerRows1 = document.getElementsByClassName('Header1');
	for (let i = 0; i < headerRows1.length; i++){
		var tds = headerRows1[i].getElementsByTagName('td');
		for (let j = 0; j < tds.length; j++) {
			var spanElem = tds[j];
			spanElem.style.fontWeight = "bold";
			spanElem.style.backgroundColor = "#9d68eb";
			spanElem.style.color = "#ffffff";
		}
	}
	
	var headerRows2 = document.getElementsByClassName('Header2');
	for (let i = 0; i < headerRows2.length; i++){
		var tds = headerRows2[i].getElementsByTagName('td');
		for (let j = 0; j < tds.length; j++) {
			var spanElem = tds[j];
			spanElem.style.fontWeight = "bold";
			spanElem.style.backgroundColor = "#5514b4";
			spanElem.style.color = "#ffffff";
		}
	}
	
	var headerRows3 = document.getElementsByClassName('Header3');
	for (let i = 0; i < headerRows3.length; i++){
		var tds = headerRows3[i].getElementsByTagName('td');
		for (let j = 0; j < tds.length; j++) {
			var spanElem = tds[j];
			if (j == 0) {
				spanElem.style.fontWeight = "bold";
			} else {
				spanElem.innerHTML = '';
			}
			spanElem.style.backgroundColor = "#9d68eb";
			spanElem.style.color = "#ffffff";
		}
	}
	
	var blankRow = document.getElementsByClassName('Blank');
	for (let i = 0; i < blankRow.length; i++){
		var tds = blankRow[i].getElementsByTagName('td');
		for (let j = 0; j < tds.length; j++) {
			var spanElem = tds[j];
			spanElem.innerHTML = '';
		}
	}
		
	var charts = document.getElementsByClassName('victory-container');
	for (let i = 0; i < charts.length; i++) {
		var chart = charts[i];
		chart.style.display = "none";
	}
	
	var downloadBtn = document.getElementsByClassName('icon-download');
	downloadBtn[0].style.display = "none";
	
	var downloadBtnNew = document.getElementsByTagName('h3');
	var btn = document.createElement("BUTTON");
	btn.innerHTML = "Download report";
	btn.style.overflow = 'hidden';
	btn.style.outline = 'none';
	btn.style.border = 'none';
	btn.style.cursor = 'pointer';
	btn.style.background = 'none';
	btn.style.margin = '0 0 0 20px';
	btn.onclick = function() { saveFile(); }
	downloadBtnNew[1].appendChild(btn);
	
	var btn2 = document.createElement("BUTTON");
	btn2.innerHTML = "Change Grouping";
	btn2.style.overflow = 'hidden';
	btn2.style.outline = 'none';
	btn2.style.border = 'none';
	btn2.style.cursor = 'pointer';
	btn2.style.background = 'none';
	btn2.style.margin = '0 0 0 20px';
	btn2.onclick = function() { redirectToPage(); }
	downloadBtnNew[1].appendChild(btn2);
	
	var btn3 = document.createElement("BUTTON");
	btn3.innerHTML = "Back to Basket";
	btn3.style.overflow = 'hidden';
	btn3.style.outline = 'none';
	btn3.style.border = 'none';
	btn3.style.cursor = 'pointer';
	btn3.style.background = 'none';
	btn3.style.margin = '0 0 0 20px';
	btn3.onclick = function() { backToBasket(); }
	downloadBtnNew[1].appendChild(btn3);
	
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	
	function saveFile () {
		var sheet_data = [];
		var rowNames = document.getElementsByClassName('BTclass');
		var increment = 0;
		for (let i = 0; i < dataHolder.length; i++) {
			if (onLoadRows[i].rowClass.includes('BTclass')) {
				var dataArray = dataHolder[i].data;
				var dataMap = {};
				dataMap.Name = rowNames[increment].getElementsByTagName('span')[0].innerText;
				var prevNum = 0;
				for (let j = 0; j < dataArray.length; j++) {
					if (j < 1 || j > 11) {
						dataMap['Month ' + prevNum] = dataArray[j].toFixed(2);;
						prevNum++;
					}
				}
				var getTotalRow = rowNames[increment].getElementsByTagName('span')[rowNames[increment].getElementsByTagName('span').length - 1].innerText;
				dataMap.Total = getTotalRow;
				sheet_data.push(dataMap);
				increment++;
			}
		}
		
		JSONToCSVConvertor(sheet_data, "PL", true);
	}
	
	function redirectToPage () {
		const pbId = urlParams.get('basketId');
		window.open('/apex/c__CS_PLFamilySelect?basketId=' + pbId ,'_self');
	}
	
	function backToBasket () {
		const pbId = urlParams.get('basketId');
		window.open('/' + pbId ,'_self');
	}
	
	function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
		var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
		var CSV = 'sep=,' + '\r\n\n';
		if (ShowLabel) {
			var row = "";
			for (var index in arrData[0]) {
				row += index + ',';
			}

			row = row.slice(0, -1);
			CSV += row + '\r\n';
		}
		
		for (var i = 0; i < arrData.length; i++) {
			var row = "";
			for (var index in arrData[i]) {
				row += '" ' + arrData[i][index] + '",';
				//added space in top row to solve auto formating numbers to date in sheet
			}

			row.slice(0, row.length - 1);
			CSV += row + '\r\n';
		}

		if (CSV == '') {        
			alert("Invalid data");
			return;
		}   
		
		const familyName = urlParams.get('familyName');
		var fileName = ReportTitle + ' ' + familyName;
		fileName = fileName.replace(/ /g,"_");   
		var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);
		var link = document.createElement("a");    
		link.href = uri;
		
		link.style = "visibility:hidden";
		link.download = fileName + ".csv";
		
		document.body.appendChild(link);
		link.click();
		document.body.removeChild(link);
	}
	
	return new Promise((resolve) => {
        resolve(data);
    });
});