/*
	Configuration Basket Totals
*/

var CBT = {
    
    // Loads Product Configuration Edit top bar with basket totals
    loadTopBar: function(){

		var pcId = CS.Service.config[CS.Service.getCurrentConfigRef()].config.Id;
		var pbId = CS.Service.config[CS.Service.getCurrentConfigRef()].config.cscfga__Product_Basket__c;
		var rootId = CS.Service.config[CS.Service.getCurrentConfigRef()].config.cscfga__Root_Configuration__c;
		var parentId = CS.Service.config[CS.Service.getCurrentConfigRef()].config.cscfga__Parent_Configuration__c;

		// If the BasketTotals iframe is not found, then creates it - prevents duplicates
    	if(jQuery('#BasketTotals').length == 0){

    		var ifrm = document.createElement("iframe");
			var pageUrl = window.location.href;
			var url = new URL(pageUrl);
		
			var urlString = "/apex/c__CS_ConfigurationBasketTotals?";
			if(pbId != '' && pbId !== undefined) urlString += "configId="+pcId;
			if(pcId != '' && pcId !== undefined) urlString += "&basketId="+pbId;
			if(rootId != '' && rootId !== undefined) urlString += "&rootId="+rootId;
			if(parentId != '' && parentId !== undefined) urlString += "&parentId="+parentId;

			console.log("[CBT] page URL: "+urlString);

			ifrm.src = urlString;
			ifrm.id = "BasketTotals";
			ifrm.style.width = "-webkit-fill-available";
		    ifrm.style.height = "111px";
		    ifrm.style.border = "0px";
		    ifrm.style.display = "block";
		    ifrm.style.margin = "0 auto";
		    		     
		    jQuery('#screensList').after(ifrm);
		}
			
		var msg = {};
		msg.name = 'configData';
		msg.newConfig = {};
		msg.newConfig.totalCost = 0;
		msg.newConfig.totalCharges = 0;
		msg.newConfig.totalChargesNPV = 0;
		msg.newConfig.totalCostsNPV = 0;
		msg.newConfig.totalRecurringCost = 0;
		msg.newConfig.totalRecurringCharge = 0;
		msg.newConfig.totalOneOffCharge = 0;
		msg.newConfig.totalOneOffCost = 0;
		msg.newConfig.numberOfUsers = 0;
		msg.newConfig.tenure = 0;

		//++++++++++ Is this still needed?
		msg.parentConfig = {};
		msg.parentConfig.exists = false;
		msg.grandParentConfig = {};
		msg.grandParentConfig.exists = false;

		if(parentId != '' && parentId !== undefined) {
			msg.parentConfig = parentId;
			msg.parentConfig.exists = true;
		}

		if(rootId != '' && rootId !== undefined) {
			msg.grandParentConfig = rootId;
			msg.grandParentConfig.exists = true;
		}
		//-------------------------------
		
		// Get Related PDs
		var rps = _.filter(CS.Service.config, function(it) {
		    return it.attr && it.attr.cscfga__Attribute_Definition__r && it.attr.cscfga__Attribute_Definition__r.cscfga__Type__c=='Related Product'
		});

		var definitionName = CS.Service.config[""].config.cscfga__Product_Family__c;

		console.log('[CBT] Root PD: '+definitionName);

		var pdFound = false;
		
		for (var iPD = 0; iPD < PDattributes.length; iPD++) {

			if(PDattributes[iPD][definitionName] !== undefined){

				pdFound = true;

		 		var recurringCharge = PDattributes[iPD][definitionName].recurringCharge;
		 		var recurringCost = PDattributes[iPD][definitionName].recurringCost;
		 		var oneoffCharge = PDattributes[iPD][definitionName].oneoffCharge;
		 		var oneoffCost = PDattributes[iPD][definitionName].oneoffCost;
		 		var numberOfUsers = PDattributes[iPD][definitionName].numberOfUsers;
		 		var tenure = PDattributes[iPD][definitionName].tenure;

		 		for (var iAt = 0; iAt < recurringCharge.length; iAt++) {
					var value = CS.getAttributeValue(recurringCharge[iAt]);
					if (value != undefined && !isNaN(value) && value != "") {
						msg.newConfig.totalRecurringCharge += parseFloat(value);
					}
				}

				for (var iAt = 0; iAt < recurringCost.length; iAt++) {
					var value = CS.getAttributeValue(recurringCost[iAt]);
					if (value != undefined && !isNaN(value) && value != "") {
						msg.newConfig.totalRecurringCost += parseFloat(value);
					}
				}

				for (var iAt = 0; iAt < oneoffCharge.length; iAt++) {
					var value = CS.getAttributeValue(oneoffCharge[iAt]);
					if (value != undefined && !isNaN(value) && value != "") {
						msg.newConfig.totalOneOffCharge += parseFloat(value);
					}
				}

				for (var iAt = 0; iAt < oneoffCost.length; iAt++) {
					var value = CS.getAttributeValue(oneoffCost[iAt]);
					if (value != undefined && !isNaN(value) && value != "") {
						msg.newConfig.totalOneOffCost += parseFloat(value);
					}
				}

				if (numberOfUsers != undefined && CS.getAttributeValue(numberOfUsers) != undefined) {
					msg.newConfig.numberOfUsers = parseFloat(CS.getAttributeValue(numberOfUsers));
				}

				msg.newConfig.tenure = parseFloat(CS.getAttributeValue(tenure));
				break;
			}
		}

		if(!pdFound){
			console.log('[CBT] PD "'+definitionName+'" not found in PDattributes static resource.');
		}
		
		for (var iRPt = 0; iRPt < rps.length; iRPt++) {

			for (var iRP = 0; iRP < rps[iRPt].relatedProducts.length; iRP++) {

				var rpName = CS.Service.getProductIndex().productsById[rps[iRPt].relatedProducts[iRP].config.cscfga__Product_Definition__c].Name;
				console.log('[CBT] Related Product: '+rpName);
				var rpRef = rps[iRPt].relatedProducts[iRP].reference;
				var rpPDfound = false;

				for (var iPD = 0; iPD < PDattributes.length; iPD++) {

					if(PDattributes[iPD][rpName] !== undefined){

						rpPDfound = true;

				 		var recurringChargeRP = PDattributes[iPD][rpName].recurringCharge;
				 		var recurringCostRP = PDattributes[iPD][rpName].recurringCost;
				 		var oneoffChargeRP = PDattributes[iPD][rpName].oneoffCharge;
				 		var oneoffCostRP = PDattributes[iPD][rpName].oneoffCost;

				 		for (var iAt = 0; iAt < recurringChargeRP.length; iAt++) {
							var value = CS.getAttributeValue(rpRef+':'+recurringChargeRP[iAt]);
							if (value != undefined && !isNaN(value) && value != "") {
								msg.newConfig.totalRecurringCharge += parseFloat(value);
							}
						}

						for (var iAt = 0; iAt < recurringCostRP.length; iAt++) {
							var value = CS.getAttributeValue(rpRef+':'+recurringCostRP[iAt]);
							if (value != undefined && !isNaN(value) && value != "") {
								msg.newConfig.totalRecurringCost += parseFloat(value);
							}
						}

						for (var iAt = 0; iAt < oneoffChargeRP.length; iAt++) {
							var value = CS.getAttributeValue(rpRef+':'+oneoffChargeRP[iAt]);
							if (value != undefined && !isNaN(value) && value != "") {
								msg.newConfig.totalOneOffCharge += parseFloat(value);
							}
						}

						for (var iAt = 0; iAt < oneoffCostRP.length; iAt++) {
							var value = CS.getAttributeValue(rpRef+':'+oneoffCostRP[iAt]);
							if (value != undefined && !isNaN(value) && value != "") {
								msg.newConfig.totalOneOffCost += parseFloat(value);
							}
						}
					}
					
				}
				if(!rpPDfound){
					console.log('[CBT] PD "'+rpName+'" not found in PDattributes static resource.');
				}
			}
		}
		
		msg.newConfig.totalCharges = msg.newConfig.tenure * msg.newConfig.totalRecurringCharge + msg.newConfig.totalOneOffCharge;
		msg.newConfig.totalCost = msg.newConfig.tenure * msg.newConfig.totalRecurringCost + msg.newConfig.totalOneOffCost;

		console.log("[CBT] JSON Msg: "+JSON.stringify(msg));

		jQuery('#BasketTotals').get(0).contentWindow.postMessage(JSON.stringify(msg), '*');
	},

	formatName: function(name){
        return name.split(' ').join('_') + '_';
    }
};