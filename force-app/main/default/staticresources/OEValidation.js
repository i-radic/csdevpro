// window.definitionName = Implementation Form | Account Admins | Apple DEP | ...
// window.schema.definitionName = BTMobile | BTOP | ...
// window.embedded = false; - obsolete
window.commDefName = window.schema.definitionName,
window.nonCommDefName = window.definitionName;

var OEValidation = {
	spendCapMap: {},
	Columns: window.schema.attributes.map(function(item){  return item.name; }),
	Label: {
		GenericInvalid: 'Please check marked columns or cells, as they contain invalid and/or required data.'
	},
	UI: {
		DOM: {},
		Init: {},
		Handlers: {},
		ToggleFields: function(row, attributeNames, disableFlag){
			for(var i = 0; i < attributeNames.length; i++){
				var cell = row.find('td:nth-child('+ (OEValidation.Columns.indexOf(attributeNames[i]) + 2) +') div');
				if(cell && jQuery(cell).children()){
					jQuery(jQuery(cell)).removeClass('oe-invalid');
					if(jQuery(cell).children()[0] != null)
						jQuery(cell).children()[0].disabled = disableFlag;
				}
			}
		},
		ToggleField: function(attributeName, disableFlag){
			var field = jQuery(".slds-input[data-id='"+ attributeName +"']");
			field.parent().removeClass('oe-required oe-invalid');
			field.attr('disabled', disableFlag);
		},
		RequireFields: function(row, attributeNames, requireFlag){
			for(var i = 0; i < attributeNames.length; i++){
				var cell = row.find('td:nth-child('+ (OEValidation.Columns.indexOf(attributeNames[i]) + 2) +') div');
				if(cell && jQuery(cell).children()){
					if(requireFlag && jQuery(jQuery(cell)[0]))//.hasClass('oe-invalid'))
						jQuery(jQuery(cell)[0]).addClass('oe-required');
					else
						jQuery(jQuery(cell)[0]).removeClass('oe-required');
				}
			}
		},
		ValidateFields: function(row, attributeNames, validateFlag){
			for(var i = 0; i < attributeNames.length; i++){
				var cell = row.find('td:nth-child('+ (OEValidation.Columns.indexOf(attributeNames[i]) + 2) +') div');
				if(cell && jQuery(cell).children()){
					if(validateFlag && jQuery(jQuery(cell)[0]))
						jQuery(jQuery(cell)[0]).addClass('oe-invalid');
					else
						jQuery(jQuery(cell)[0]).removeClass('oe-invalid');
				}
			}
		},
		ShowNotification: function(text){
			var nc = document.getElementById('notificationContainer');
			text = ' ' + text;
			if(nc){
				var re = new RegExp(text, "g");
				nc.innerText = nc.innerText.replace(re, '');
				nc.innerText += text;
				nc.style.display = 'block';
			}
		},
		HideNotification: function(){
			var nc = document.getElementById('notificationContainer');
			nc.innerText = '';
			if(nc)
				nc.style.display = 'none';
		},
		ResetNotification: function(){
			var nc = document.getElementById('notificationContainer');
			if(nc)
				nc.innerText = '';
		},
		ShowTooltip: function(event, validityMap, availableOptions){
			var attributeName = jQuery(event.target).find('div').text(),
				tc = document.getElementById('tooltipContainer'),
				tooltipHtml = '<ul>';
			for(var prop in validityMap){
				if(!/^Extra/g.test(attributeName)){
					if(prop === 'Authority to make changes' && attributeName === 'Authority to make changes' && !validityMap[prop].valid)
						tooltipHtml += '<li>Authority to make changes checkbox needs to be checked for at least one user.</li>';
					else if(prop === 'Number Type' && attributeName === 'Number Type' && !validityMap[prop].valid)
						tooltipHtml += '<li>You need to have <b>' + availableOptions[prop] + ' Number Types</b> assigned.</li>';
					else if(!validityMap[prop].valid && validityMap[prop].parent === attributeName)
						tooltipHtml += '<li>The expected allocations for <b>' + prop + '</b> is <b>' + availableOptions[prop] + '</b></li>';
				}
				else{
					if(!validityMap[prop].valid && validityMap[prop].parent === 'Extra 1')
						tooltipHtml += '<li>The expected allocations for <b>' + prop + '</b> is <b>' + availableOptions[prop] + '</b></li>';
				}
			}

			tooltipHtml += '</ul>';
			if(tc && tooltipHtml !== '<ul></ul>'){
				tc.innerHTML = tooltipHtml;
				tc.style.display = 'block';
			}
		},
		HideTooltip: function(){
			var tc = document.getElementById('tooltipContainer');
			if(tc)
				tc.style.display = 'none';
		},
		InitializeTooltips: function(data){
			var $table = jQuery('#schemaTable');
			$table.find('thead th').off('mouseenter');
			$table.find('thead th').off('mouseleave');
			$table.find('thead th.oe-invalid').on('mouseenter', function(event){
				OEValidation.UI.ShowTooltip(event, data.vMap, data.aOptions);
			});
			$table.find('thead th.oe-invalid').on('mouseleave', function(event){
				OEValidation.UI.HideTooltip();
			});
		},
		SetValidityOnParent: function(isValid){
			if(isValid > 0){
				SPM.setParentValue('OE_Validation_0', 'Order Enrichment data is invalid. ' + OEValidation.Label.GenericInvalid);
				OEValidation.UI.ShowNotification(OEValidation.Label.GenericInvalid);
			}
			else
				SPM.setParentValue('OE_Validation_0', null);
		},
		CheckInvalid: function(){
			if(jQuery('#schemaTable').find('.oe-invalid').length > 0)
				OEValidation.UI.ShowNotification(OEValidation.Label.GenericInvalid);
			else
				OEValidation.UI.HideNotification();
		},
		CountRows: function(){
			setTimeout(function(){
				document.getElementById('rowCounter').style.right = (jQuery('#pagination').parent().outerWidth(true) + 10) + 'px';
				document.getElementById('rowCounter').innerHTML = 'Row count: <b>' + OEValidation.Util.GetRowCount() + '</b>';
			}, 250);
		}
	},
	Rules: {
		Defaults: {
			MobileNumber: function(element){
				jQuery(element).parent().removeClass('oe-invalid oe-required');
				if(!element.disabled){
					var isValid = /^0\d{10}$/g.test(element.value.toString());
					if(!isValid)
						jQuery(element).parent().removeClass('oe-invalid oe-required').addClass('oe-invalid oe-required');
				}
			},
			PAC: function(element){
				jQuery(element).parent().removeClass('oe-invalid oe-required');
				if(!element.disabled){
					var isValid = /^\d{6}[a-zA-Z]{3}|[a-zA-Z]{3}\d{6}$/g.test(element.value.toString());
					if(!isValid)
						jQuery(element).parent().removeClass('oe-invalid oe-required').addClass('oe-invalid oe-required');
				}
			},
			Date: function(element){
				var row = jQuery(element).closest('tr');
				var rowGuid = jQuery(row).find('button').data('guid');
				var	timestamp = Date.parse(element.value);
				if(element.value === '' || (Date.now() > timestamp))
					jQuery(element).parent().removeClass('oe-invalid oe-required').addClass('oe-invalid oe-required');
				else
					jQuery(element).parent().removeClass('oe-invalid oe-required');
			},
			Email: function(element){
				var isValid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g.test(element.value.toString());
				jQuery(element).parent().removeClass('oe-invalid oe-required');
				if(!isValid)
					jQuery(element).parent().addClass('oe-invalid oe-required');
			},
			NonEmpty: function(element){
				jQuery(element).parent().removeClass('oe-invalid oe-required');
				if(element.value === '')
					jQuery(element).parent().addClass('oe-invalid oe-required');
			},
			BasketContents: function(BasketContents, SubscriptionsMap, toValidate){
				var	validityMap = {},
					selectedOptions = {}, // Table wide Map of values selected from picklists
					availableOptions = {}, // Basket wide Map which holds quantity of available products
					invalidColumns = [], // Array which holds the values of invalid columns
					$schemaTableHeader = jQuery('#schemaTable').find('thead tr'); // Table header 

				$schemaTableHeader.find('.oe-invalid').removeClass('oe-invalid'); // Reset invalid markers

				// Available options for subs
				for(var sub in SubscriptionsMap){
					availableOptions[sub] = SubscriptionsMap[sub];
				}
			
				// populate selectedOptions
				for(var i = 0; i < toValidate.length; i++){
					var picklistOptions = OEValidation.Util.GetPicklistOptionsFromSchema(toValidate[i]);
					for(var j = 0; j < picklistOptions.length; j++){
						var currentOption = picklistOptions[j]; // eg. Existing Hardware
						if(typeof selectedOptions[currentOption] === 'undefined') // if there is no key in obj create
							selectedOptions[currentOption] = 0;
						var count = 0;
						for(var rowGuid in window.configurationMap){
							var configuration = window.configurationMap[rowGuid];
							if(configuration[toValidate[i]] === currentOption)
								count++;
						}
						selectedOptions[currentOption] = selectedOptions[currentOption] + count;
					}
				}

				// Populate invalidColumns adn push to validity map
				for(var p in availableOptions){
					var attName = OEValidation.Util.GetAttributeNameFromOption(p);
					var isValid = (selectedOptions[p] === availableOptions[p]) ? true : false;
					validityMap[p] = { valid: isValid, parent: attName };
					if(!isValid)
						invalidColumns.push(attName);	
				}

				// Mark invalid entries
				for(var l = 0; l < invalidColumns.length; l++){
					var column = invalidColumns[l],
						columnIndex = OEValidation.Columns.indexOf(column) + 2;
					switch(column){
						case undefined:
						case 'undefined':
							// do nothing
							break;
						default:
							$schemaTableHeader.find('th:nth-child(' + columnIndex + ')').addClass('oe-invalid');
							break;
					}
				}
				return { vMap: validityMap, aOptions: availableOptions };
			}
		}		
	},
	Validate: {
		SingleField: function(element, SCT){
			var attributeName = element.getAttribute('data-id');
			switch(attributeName){
				case 'Number Type':
				case 'Deal Type':
				case 'Connection Type':
					OEValidation.Rules[commDefName][nonCommDefName].DealType(element);
					break;
				case 'Mobile Number':
					OEValidation.Rules[commDefName][nonCommDefName].MobileNumber(element);
					break;
				case 'PAC/STAC':
					OEValidation.Rules[commDefName][nonCommDefName].PAC(element);
					break;
				case 'Customer Connection Date':
				case 'Subscription Start Date':
					OEValidation.Rules.Defaults.Date(element);
					break;
				case 'E-mail':
				case 'E-mail Adresss':
					OEValidation.Rules[commDefName][nonCommDefName].Email(element);
					break;
				case 'Last Name':
				case 'First Name':
				case 'Name':
				case 'User name':
					OEValidation.Rules[commDefName][nonCommDefName].NonEmpty(element);
					break;
				case 'Extension Number':
					OEValidation.Rules[commDefName][nonCommDefName].ExtensionNumber();
					break;
				case 'PAC Expiry Date':
					OEValidation.Rules.Defaults.Date(element);
					break;
				case 'Mobile Number CSV':
					OEValidation.Rules[commDefName][nonCommDefName].MobileNumberCSV(element);
				case 'SIP Extra Softphone Only':
					OEValidation.Rules[commDefName][nonCommDefName].SIPExtraSoftphoneOnly();
					break;
				case 'Opt out of automatic enrolment':
					OEValidation.Rules[commDefName][nonCommDefName].OptOut(element);
					break;
				case 'Customer Apple ID':
					OEValidation.Rules[commDefName][nonCommDefName].CAppleID(element);
					break;
				case 'Sharer Subscriptions':
					OEValidation.Rules[commDefName][nonCommDefName].SpendCapBillingCode(element); //spendCap changes
					break;
				case 'Handset':
				case 'Device':
				default:
					break;
			}
		},
		Table: {},
		BasketContents: {}
	},
	Util: {
		GetAttributeNameFromOption: function(option){
			var attributeName = window.schema.attributes.filter(function(item){ return item.type === 'Picklist' && item.options.indexOf(option) > -1; });
			if(attributeName && attributeName.length)
				return attributeName[0].name;
		},
		GetSingleSchemaAttribute: function(attributeName){
			var attribute = window.schema.attributes.filter(function(item){ return item.name === attributeName; });
			if(attribute && attribute.length)
				return attribute[0];
		},
		GetRowAttributeValue: function(rowGuid, attributeName){
			var configuration = window.configurationMap[rowGuid];
			if(configuration)
				return configuration[attributeName];
		},
		SetRowAttributeValue: function(rowGuid, attributeName, value){
			var configuration = window.configurationMap[rowGuid],
				row = jQuery('[data-guid="' + rowGuid + '"]').closest('tr');

			// set value in configuration
			if(configuration){
				configuration[attributeName] = value;
				// set value in table
				var cell = row.find('td:nth-child('+ (OEValidation.Columns.indexOf(attributeName) + 2) +') div'); 
				if(cell && jQuery(cell).children()){
					jQuery(jQuery(cell).children()[0]).val(value);
				}

			}else{
				OEValidation.UI.ShowNotification('Please save configurations in order to set field values');
			}
		},
		GetAttributeValue: function(attributeName){
			return window.configuration.configurations[0][attributeName];
		},
		SetAttributeValue: function(attributeName, value){
			window.configuration.configurations[0][attributeName] = value;
			jQuery(".slds-input[data-id='"+ attributeName +"']").val(value);
		},
		GetPicklistOptionsFromSchema: function(attributeName){
			var attribute = window.schema.attributes.filter(function(item){  return item.name === attributeName }),
				allOptions = attribute[0].options.filter(function(value, index, self){ return  self.indexOf(value) === index; });

			switch(attributeName){
				case 'Handset':
				case 'Device':
					allOptions = allOptions.slice(3);
					break;
				case 'Deal Type':
				case 'Connection Type':
				case 'Mobile Number CSV':
					break;
				default:
					allOptions = allOptions.slice(1);
					break;

			}

			return allOptions;
		},
		IterateAllRows: function(callback, attributeName){
			var isInSchema = window.schema.attributes.filter(function(item){  return item.name === attributeName });
			if(isInSchema.length > 0){
				var $table = jQuery('#schemaTable').find('tbody'),
					columnIndex = OEValidation.Columns.indexOf(attributeName) + 2;
				$table.find('tr').each(function(i, elem){
					var field = jQuery(elem).find('td:nth-child('+ columnIndex +') div').children();
					callback(field[0]);
				});
			}
		},
		AccountAdmins: function(){
			var $row = jQuery('#schemaTable').find('tbody tr:first-child');
			if($row.length !== 0){
				OEValidation.UI.RequireFields($row, ['Name', 'Mobile Number', 'E-mail'], true);
				OEValidation.UI.ValidateFields($row, ['Name', 'Mobile Number', 'E-mail'], true);
			}
		},
		SetInputReadOnly: function(attributeName){
			var columnIndex = OEValidation.Columns.indexOf(attributeName) + 2,
				$table = jQuery('#schemaTable tbody');

			$table.find('td:nth-child('+ columnIndex +') input').each(function(index, element){
				if(element)
					element.readOnly = true;
			});
		},
		SetInputReadOnlySingle: function(attributeName){
			jQuery('#single').find('[data-id="'+ attributeName +'"]').attr('readOnly', true);
		},
		GetRowCount: function(){
			var count = 0;
			for(var prop in window.configurationMap){
				++count;
			}
			return count;
		}
	}
};
/* BTMobile */
OEValidation.UI.DOM.BTMobile = {
	'Implementation Form': function(){
		var notificationContainer = document.createElement('DIV'),
			tooltipContainer = document.createElement('DIV'),
			currentlyContainer = document.createElement('DIV'),
			rowCounterContainer = document.createElement('DIV');

		notificationContainer.id = 'notificationContainer';
		notificationContainer.className = 'oe-notification';
		tooltipContainer.id = 'tooltipContainer';
		tooltipContainer.className = 'oe-tooltip';
		currentlyContainer.id = 'currentlyContainer';
		rowCounterContainer.id = 'rowCounter';
		
		document.body.appendChild(notificationContainer);
		document.body.appendChild(tooltipContainer);
		document.body.appendChild(currentlyContainer);
		document.body.appendChild(rowCounterContainer);
	},
	'Account Admins': function(){

		var notificationContainer = document.createElement('DIV'),
			tooltipContainer = document.createElement('DIV');

		notificationContainer.id = 'notificationContainer';
		notificationContainer.className = 'oe-notification';
		tooltipContainer.id = 'tooltipContainer';
		tooltipContainer.className = 'oe-tooltip';
		
		document.body.appendChild(notificationContainer);
		document.body.appendChild(tooltipContainer);
	},
	'Apple DEP': function(){
		var notificationContainer = document.createElement('DIV'),
			tooltipContainer = document.createElement('DIV');

		notificationContainer.id = 'notificationContainer';
		notificationContainer.className = 'oe-notification';
		tooltipContainer.id = 'tooltipContainer';
		tooltipContainer.className = 'oe-tooltip';
		
		document.body.appendChild(notificationContainer);
		document.body.appendChild(tooltipContainer);
	}
};
OEValidation.UI.Init.BTMobile = {
	'Implementation Form': function(){
		OEValidation.UI.CountRows();
		// set currently editing to User name of current row
		jQuery('#schemaTable tbody').on('mousedown', 'tr', function(){
			var currentlyContainer = document.getElementById('currentlyContainer');
				currentlyContainer.innerHTML = 'Currently editing:<br />' + jQuery(this).find('td:nth-child(2) input').val();
		});
	},
	'Account Admins': function(){
		OEValidation.Util.AccountAdmins();
	},
	'Apple DEP': function(){
		var $caId = jQuery('.slds-input[data-id="Customer Apple ID"]');
		setTimeout(function(){
			OEValidation.Util.SetInputReadOnlySingle('Apple Reseller ID');
			OEValidation.Rules.BTMobile['Apple DEP'].CAppleID($caId[0]);
		}, 100);
	}
};
OEValidation.UI.Handlers.BTMobile = {
	'Implementation Form': {
		onDelete: function(rowGuid){
			console.log('Implementation Form onDelete');
			OEValidation.UI.CountRows();
		},
		onNewRow: function(rowGuid){
			console.log('Implementation Form onNewRow');
			OEValidation.UI.CountRows();
		},
		onInputChange: function(element, SCT){
			console.log('Implementation Form onInputChange');
			OEValidation.Validate.SingleField(element, SCT);
		},
		onSave: function(params){
			console.log('Implementation Form onSave');

			var toValidate = ["Authority to make changes", "Connection Type", "Sharer Subscriptions", "Device", "Extra 1", "Extra 2", 
							"Extra 3", "Extra 4", "Shared Data 1", "Shared Data 2", "MDM", "Office 365"], // Columns == Attribute Names to validate
				data = OEValidation.Validate.BasketContents.BTMobile['Implementation Form'](params.basketContents, params.subscriptionsMap, toValidate);

			OEValidation.Validate.Table.BTMobile['Implementation Form']();
			OEValidation.UI.CheckInvalid();
			OEValidation.UI.InitializeTooltips(data);

			// Save validity flag to basket
			SPM.remoteAction({ 
				configId: window.configurationId,
				basketId: params.basketContents.Id,
				definitionName: commDefName,
				action: 'ImplementationForm',
				invalidCount: jQuery('#schemaTable').find('.oe-invalid').length },
				'CS_OESaveHandler', 
				'onSaveHandler');
		},
		onFetchParentData: function(response, BC){
			console.log('Implementation From onFetchParentData');
			var relatedMap = {},
				NoneOption = '-- None --',
				sharerOptions = [ NoneOption ],
				data1Options = [ NoneOption ],
				data2Options = [ NoneOption ],
				mdmOptions = [ NoneOption ],
				officeOptions = [ NoneOption ],
				SharerSubscriptionList = [],
				subsMap = {};

			if(response && response.data){
				for(var i = 0; i < response.data.length; i++){
					var item = response.data[i],
						relatedObj = { Name: item.Name, Quantity: item.Volume__c};
					if(item.Spend_Cap_Name__c !== undefined) {
						relatedObj.Name = item.Name + " - " + item.Spend_Cap_Name__c;
						//spendCap changes
						if(item.Spend_Cap_Billing_Code__c !== undefined)
							OEValidation.spendCapMap[relatedObj.Name] = item.Spend_Cap_Billing_Code__c;
						else
							OEValidation.spendCapMap[relatedObj.Name] = '';
					}
					if(typeof relatedMap[item.Product_Definition_Name__c] === 'undefined'){				
						if(item.Product_Definition_Name__c === 'Sharer Subscription'){
							relatedObj.DealType = item.cscfga__Attributes__r[0].cscfga__Value__c;
						}
						relatedMap[item.Product_Definition_Name__c] = [relatedObj];
					}else{
						if(item.Product_Definition_Name__c === 'Sharer Subscription'){
							relatedObj.DealType = item.cscfga__Attributes__r[0].cscfga__Value__c;
						}
						relatedMap[item.Product_Definition_Name__c].push(relatedObj);
					}
				}
			}
			for(var prop in relatedMap){
				var relatedArr = relatedMap[prop];
				for(var j = 0; j < relatedArr.length; j++){
					// create options
					switch(prop){
						case 'Sharer Subscription':
							if(SharerSubscriptionList.indexOf(relatedArr[j].Name) === -1){
								SharerSubscriptionList.push(relatedArr[j].Name);
							}
							if(typeof subsMap[relatedArr[j].DealType] !== 'undefined'){
								subsMap[relatedArr[j].DealType] += parseInt(relatedArr[j].Quantity);
							}else{
								subsMap[relatedArr[j].DealType] = parseInt(relatedArr[j].Quantity);
							}
						break;
						case 'Shared Data':
			                if(j === 0){
			                    data1Options = data1Options.concat([relatedArr[0].Name]);
			                }
			                if(j == 1 && relatedArr.length === 2){
			                    data2Options = data2Options.concat([relatedArr[1].Name]);
			                }
						break;
						case 'MDM':
							if(mdmOptions.indexOf(relatedArr[j].Name === -1)){
								mdmOptions.push(relatedArr[j].Name);
							}
						break;
						case 'Office 365':
							if(officeOptions.indexOf(relatedArr[j].Name === -1)){
								officeOptions.push(relatedArr[j].Name);
							}
						break;
						default:
						break;
					}
					// Put in Map
					if(typeof subsMap[relatedArr[j].Name] !== 'undefined'){
						subsMap[relatedArr[j].Name] += parseInt(relatedArr[j].Quantity);
					}else{
						subsMap[relatedArr[j].Name] = parseInt(relatedArr[j].Quantity);
					}
				}
			}
			if(BC.Flex !== null && BC.Flex.length){
				var newConn = parseInt(BC.Flex[0].Number_of_New_Connections__c),
					port = parseInt(BC.Flex[0].Number_of_Ports__c),
					resign = parseInt(BC.Flex[0].Number_of_Resigns__c),
					sub = BC.Flex[0].Name.split('-')[1].slice(1) + " - " + BC.Flex[0].Spend_Cap_Name__c;
					//spendCap changes
					if(BC.Flex[0].Spend_Cap_Billing_Code__c !== undefined)
						OEValidation.spendCapMap[sub] = BC.Flex[0].Spend_Cap_Billing_Code__c;
					else
						OEValidation.spendCapMap[sub] = '';

				subsMap['New Connection'] = newConn;
				subsMap['Port'] = port;
				subsMap['Resign'] = resign;
				subsMap[sub] = newConn + port + resign;

				sharerOptions = sharerOptions.concat([sub]);
			}
			if(SharerSubscriptionList && SharerSubscriptionList.length){
				sharerOptions = sharerOptions.concat(SharerSubscriptionList);	
			}
			if(BC.Broadband !== null && BC.Broadband.length){
				sharerOptions = sharerOptions.concat(BC.Broadband);
			}

			updateOptions('Sharer Subscriptions', sharerOptions);
			updateOptions('Shared Data 1', data1Options);
			updateOptions('Shared Data 2', data2Options);
			updateOptions('Office 365', officeOptions);
			updateOptions('MDM', mdmOptions);

			return subsMap;
		} 
	},
	'Account Admins': {
		onDelete: function(rowGuid){
			console.log('Account Admins onDelete');
			setTimeout(OEValidation.Validate.Table.BTMobile['Account Admins'], 10);
		},
		onNewRow: function(rowGuid){
			console.log('Account Admins onNewRow');
			setTimeout(OEValidation.Validate.Table.BTMobile['Account Admins'], 10);
		},
		onInputChange: function(element){
			console.log('Account Admins onInputChange');
			OEValidation.Validate.SingleField(element);
		},
		onSave: function(params){
			console.log('Account Admins onSave');
			var $table = jQuery('#schemaTable');
			setTimeout(OEValidation.Validate.Table.BTMobile['Account Admins'], 10);
			OEValidation.UI.CheckInvalid();
			setTimeout(function(){
				SPM.remoteAction({ 
					configId: window.configurationId, 
					basketId: params.basketContents.Id,
					definitionName: commDefName,
					action: 'AccountAdmins',
					invalidCount: $table.find('tbody tr').length > 0 ? $table.find('.oe-invalid').length : 1 },
					'CS_OESaveHandler', 
					'onSaveHandler');
			}, 50);
		},
		onFetchParentData: function(response, BC){

		}
	},
	'Apple DEP': {
		onDelete: function(rowGuid){
			console.log('Apple DEP onDelete');
		},
		onNewRow: function(rowGuid){
			console.log('Apple DEP onNewRow');
		},
		onInputChange: function(element){
			console.log('Apple DEP onInputChange');
			OEValidation.Validate.SingleField(element);
		},
		onSave: function(params){
			console.log('Apple DEP onSave');
			var caId = OEValidation.Util.GetAttributeValue('Customer Apple ID'),
				optOut = OEValidation.Util.GetAttributeValue('Opt out of automatic enrolment'),
				$caId = jQuery('.slds-input[data-id="Customer Apple ID"]'),
				$optOut = jQuery('input:not(".grid-input")[data-id="Opt out of automatic enrolment"]');

			OEValidation.Util.SetInputReadOnlySingle('Apple Reseller ID');
			OEValidation.Rules.BTMobile['Apple DEP'].OptOut($optOut[0]);
			OEValidation.Rules.BTMobile['Apple DEP'].CAppleID($caId[0]);
			
			SPM.remoteAction({ 
				configId: window.configurationId, 
				basketId: params.basketContents.Id,
				definitionName: commDefName,
				action: 'AppleDEP',
				customerAppleId: caId,
				optOut: optOut }, 
				'CS_OESaveHandler', 
				'onSaveHandler');
		},
		onFetchParentData: function(response, BC){
			console.log('Apple DEP onFetchParentData');
			if(response && response.basket){
				SPM.setAttributeValue({ value: { attributeName: 'Apple Reseller ID', attributeValue: response.basket[0].Apple_Reseller_Id__c}});
				if(response.basket[0].Customer_Apple_Id__c !== '' && window.configuration.configurations[0]['Customer Apple ID'] === ''){
					SPM.setAttributeValue({ value: { attributeName: 'Customer Apple ID', attributeValue: response.basket[0].Customer_Apple_Id__c}});
				}
			}
		}
	}
};
OEValidation.Rules.BTMobile = {
	'Implementation Form': {
		DealType: function(element){
				var row = jQuery(element).closest('tr'),
					rowGuid = jQuery(row).find('button').data('guid');
				OEValidation.UI.ToggleFields(row, ['Spend Cap Billing Code'], true); //spendCap changes
				switch(element.value){			
					case 'Port':
						if(OEValidation.Util.GetRowAttributeValue(rowGuid, 'Device') === '-- None --'){
							OEValidation.Util.SetRowAttributeValue(rowGuid, 'New SIM Required', 'Yes');
						}
						OEValidation.UI.RequireFields(row, ['Mobile Number', 'PAC/STAC'], true);
						OEValidation.UI.ValidateFields(row, ['Mobile Number', 'PAC/STAC'], true);
						OEValidation.UI.ToggleFields(row, ['Mobile Number', 'PAC/STAC'], false);
						break;
					case 'New Connection':
						OEValidation.Util.SetRowAttributeValue(rowGuid, 'Mobile Number', '');
						OEValidation.Util.SetRowAttributeValue(rowGuid, 'PAC/STAC', '');
						if(OEValidation.Util.GetRowAttributeValue(rowGuid, 'Device') === '-- None --'){
							OEValidation.Util.SetRowAttributeValue(rowGuid, 'New SIM Required', 'Yes');
						}
						OEValidation.UI.RequireFields(row, ['Mobile Number', 'PAC/STAC'], false);
						OEValidation.UI.ToggleFields(row, ['Mobile Number', 'PAC/STAC'], true);
						break;
					case 'Resign':
						if(OEValidation.Util.GetRowAttributeValue(rowGuid, 'Device') === '-- None --'){
							OEValidation.Util.SetRowAttributeValue(rowGuid, 'New SIM Required', 'Yes');
						}
						
						OEValidation.Util.SetRowAttributeValue(rowGuid, 'PAC/STAC', '');
						
						OEValidation.UI.RequireFields(row, ['Mobile Number'], true);
						OEValidation.UI.ValidateFields(row, ['Mobile Number'], true);
						OEValidation.UI.ToggleFields(row, ['Mobile Number'], false);
						
						OEValidation.UI.RequireFields(row, ['PAC/STAC'], false);
						OEValidation.UI.ToggleFields(row, ['PAC/STAC'], true);
						break;
					case undefined:
					case 'undefined':	
					default:
						OEValidation.UI.RequireFields(row, ['Mobile Number', 'PAC/STAC'], false);
						OEValidation.UI.ToggleFields(row, ['Mobile Number', 'PAC/STAC'], true);
						break;
				}
		},
		SpendCapBillingCode: function(element){
			//spendCap changes
			var row = jQuery(element).closest('tr'),
				rowGuid = jQuery(row).find('button').data('guid');
			//OEValidation.UI.ToggleFields(row, ['Spend Cap Billing Code'], true);
			if(OEValidation.spendCapMap[element.value] !== undefined)
				OEValidation.Util.SetRowAttributeValue(rowGuid, 'Spend Cap Billing Code', OEValidation.spendCapMap[element.value]);
			else
				OEValidation.Util.SetRowAttributeValue(rowGuid, 'Spend Cap Billing Code', '');
		},
		MobileNumber: OEValidation.Rules.Defaults.MobileNumber,
		PAC: OEValidation.Rules.Defaults.PAC,
		Date: OEValidation.Rules.Defaults.Date,
		Email: OEValidation.Rules.Defaults.Email,
		NonEmpty: OEValidation.Rules.Defaults.NonEmpty
	},
	'Account Admins': {
		MobileNumber: function(element){
			OEValidation.UI.ResetNotification();
			jQuery(element).parent().removeClass('oe-invalid').removeClass('oe-required');
			if(!element.disabled){
				var isValid = /^0\d{10}$/g.test(element.value.toString());
				if(!isValid){
					jQuery(element).parent().addClass('oe-required').addClass('oe-invalid');
					OEValidation.UI.ShowNotification('Mobile Number is in wrong format...');
				}
				else
					OEValidation.UI.HideNotification();
			}
		},
		Email: OEValidation.Rules.Defaults.Email,
		NonEmpty: OEValidation.Rules.Defaults.NonEmpty
	},
	'Apple DEP': {
		OptOut: function(element){
			if(!element.disabled){
				if(element.checked){
					OEValidation.Util.SetAttributeValue('Customer Apple ID', '');
					jQuery(element).parent().removeClass('oe-invalid').removeClass('oe-required');
				}
				OEValidation.UI.ToggleField(['Customer Apple ID'], element.checked);
			}
		},
		CAppleID: function(element){
			if(!element.disabled){
				var isValid = element.value !== '' || (OEValidation.Util.GetAttributeValue('Opt out of automatic enrolment') && element.value === '');
				if(!isValid){
					jQuery(element).parent().addClass('oe-invalid');
				}else{
					jQuery(element).parent().removeClass('oe-invalid').removeClass('oe-required');
				}
			}
		}
	}
};
OEValidation.Validate.Table.BTMobile = {
	'Implementation Form': function(){
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].DealType, 'Connection Type');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].Date, 'Customer Connection Date');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].MobileNumber, 'Mobile Number');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].PAC, 'PAC/STAC');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].NonEmpty, 'User name');
	},
	'Account Admins': function(){
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].MobileNumber, 'Mobile Number');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].Email, 'E-mail');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].NonEmpty, 'Name');
		/*
		OEValidation.Util.AccountAdmins();
		*/
		OEValidation.UI.SetValidityOnParent(jQuery('#schemaTable').find('.oe-invalid').length);
	},
	'Apple DEP': function(){

	}
};
OEValidation.Validate.BasketContents.BTMobile = {
	'Implementation Form': function(BasketContents, SubscriptionsMap, toValidate){

			var	validityMap = {},
				selectedOptions = {}, // Table wide Map of values selected from picklists
				availableOptions = {}, // Basket wide Map which holds quantity of available products
				invalidColumns = [], // Array which holds the values of invalid columns
				$schemaTableHeader = jQuery('#schemaTable').find('thead tr'); // Table header 

			$schemaTableHeader.find('.oe-invalid').removeClass('oe-invalid'); // Reset invalid markers

			// Available options for subs
			for(var sub in SubscriptionsMap){
				availableOptions[sub] = SubscriptionsMap[sub];
			}
		
			if(BasketContents.Map.Handsets !== null){
				// Available options value for Handsets
				for(var h = 0; h < BasketContents.Map.Handsets.length; h++){
					availableOptions[BasketContents.Map.Handsets[h].name] = BasketContents.Map.Handsets[h].quantity;
				}
			}

			if(BasketContents.Map.Extras !== null){
				// Available options value for Extras
				for(var e = 0; e < BasketContents.Map.Extras.length; e++){
					availableOptions[BasketContents.Map.Extras[e].name] = BasketContents.Map.Extras[e].quantity;
				}
			}

			if(BasketContents.Map.Broadband !== null){
				// Available options value for Broadband
				for(var f = 0; f < BasketContents.Map.Broadband.length; f++){
					availableOptions[BasketContents.Map.Broadband[f].name] = BasketContents.Map.Broadband[f].quantity;
					if(typeof availableOptions[BasketContents.Map.Broadband[f].dealType] === 'undefined'){
						availableOptions[BasketContents.Map.Broadband[f].dealType] = BasketContents.Map.Broadband[f].quantity;
					}else{
						availableOptions[BasketContents.Map.Broadband[f].dealType] += BasketContents.Map.Broadband[f].quantity
					}
				}
			}
			
			availableOptions['Authority to make changes'] = 1;

			// populate selectedOptions
			for(var i = 0; i < toValidate.length; i++){
				if(toValidate[i] !== 'Authority to make changes'){ 
					var picklistOptions = OEValidation.Util.GetPicklistOptionsFromSchema(toValidate[i]);
					for(var j = 0; j < picklistOptions.length; j++){
						var currentOption = picklistOptions[j]; // eg. Existing Hardware
						if(typeof selectedOptions[currentOption] === 'undefined') // if there is no key in obj create
							selectedOptions[currentOption] = 0;
						
						var count = 0;
						for(var rowGuid in window.configurationMap){
							var configuration = window.configurationMap[rowGuid];
							if(configuration[toValidate[i]] === currentOption)
								count++;
						}
						selectedOptions[currentOption] = selectedOptions[currentOption] + count;
					}
				}
				else{ // Special case for Authority to make changes
					var count = 0;
					for(var rowGuid in window.configurationMap){
						var configuration = window.configurationMap[rowGuid];
						if(configuration[toValidate[i]] === true){
							count++;
						}
					}
					selectedOptions[toValidate[i]] = count;
				}
			}

			// Populate invalidColumns adn push to validity map
			for(var p in availableOptions){
				var attName = OEValidation.Util.GetAttributeNameFromOption(p);
				if(p !== 'SharerSubscriptionList'){ // exclude list of subs
					if(p === 'Authority to make changes'){ // special case
						var isValid = selectedOptions[p] >= availableOptions[p];
						validityMap[p] = { valid: isValid, parent: null };
						if(!isValid)
							invalidColumns.push(p);
					}
					else if(typeof attName !== 'undefined' && attName.indexOf('Shared Data') === 0){ // If shared data
						var isValid = (availableOptions[p] > 0 && selectedOptions[p] >= 1) ? true : false;
						validityMap[p] = { valid: isValid, parent: attName };
						if(!isValid)
							invalidColumns.push(attName);
					}
					else {
						var isValid = (selectedOptions[p] === availableOptions[p]) ? true : false;
						validityMap[p] = { valid: isValid, parent: attName };
						if(!isValid){
							if(invalidColumns.indexOf(attName) < 0){
								if(attName === 'Extra 1'){
									invalidColumns.push('Extra 1');
									invalidColumns.push('Extra 2');
									invalidColumns.push('Extra 3');
									invalidColumns.push('Extra 4');
								}
								else
									invalidColumns.push(attName);	
							}
						}
					}
				}
			}

			// Mark invalid entries
			for(var l = 0; l < invalidColumns.length; l++){
				var column = invalidColumns[l],
					columnIndex = OEValidation.Columns.indexOf(column) + 2;

				switch(column){
					case undefined:
					case 'undefined':
						// do nothing
						break;
					default:
						$schemaTableHeader.find('th:nth-child(' + columnIndex + ')').addClass('oe-invalid');
						break;
				}
			}

			return { vMap: validityMap, aOptions: availableOptions };
	}
};
/* BTMobile */

/* BTOP */
OEValidation.UI.DOM.BTOP = {
	'Implementation Form': function(){
		var notificationContainer = document.createElement('DIV'),
			tooltipContainer = document.createElement('DIV'),
			currentlyContainer = document.createElement('DIV'),
			rowCounterContainer = document.createElement('DIV');

		notificationContainer.id = 'notificationContainer';
		notificationContainer.className = 'oe-notification';
		tooltipContainer.id = 'tooltipContainer';
		tooltipContainer.className = 'oe-tooltip';
		currentlyContainer.id = 'currentlyContainer';
		rowCounterContainer.id = 'rowCounter';

		document.body.appendChild(notificationContainer);
		document.body.appendChild(tooltipContainer);
		document.body.appendChild(currentlyContainer);
		document.body.appendChild(rowCounterContainer);
	}
};
OEValidation.UI.Init.BTOP = {
	'Implementation Form': function(){
		OEValidation.UI.CountRows();
		// set currently editing to User name of current row
		jQuery('#schemaTable tbody').on('mousedown', 'tr', function(){
			var currentlyContainer = document.getElementById('currentlyContainer');
				currentlyContainer.innerHTML = 'Currently editing:<br />' + jQuery(this).find('td:nth-child(3) input').val() + ' ' + jQuery(this).find('td:nth-child(4) input').val();
		});
	}
};
OEValidation.UI.Handlers.BTOP = {
	'Implementation Form': {
		onDelete: function(rowGuid){
			OEValidation.UI.CountRows();

			OEValidation.Rules['BTOP']['Implementation Form'].AdditionalMobileNumber(rowGuid, 'delete', '');
			OEValidation.Rules['BTOP']['Implementation Form'].ExtensionNumber();
			OEValidation.Rules['BTOP']['Implementation Form'].SIPExtraSoftphoneOnly();
		},
		onNewRow: function(rowGuid){
			OEValidation.UI.CountRows();

			var rowValue = OEValidation.Util.GetRowAttributeValue(rowGuid, 'Additional mobile number').toLowerCase();
			OEValidation.Rules['BTOP']['Implementation Form'].AdditionalMobileNumber(rowGuid, 'insert', rowValue);
			OEValidation.Rules['BTOP']['Implementation Form'].ExtensionNumber();
		},
		onInputChange: function(element){
			OEValidation.Validate.SingleField(element);

			var row = jQuery(element).closest('tr'),
				rowGuid = jQuery(row).find('button').data('guid');
			var rowValue = OEValidation.Util.GetRowAttributeValue(rowGuid, 'Additional mobile number');
			OEValidation.Rules['BTOP']['Implementation Form'].AdditionalMobileNumber(rowGuid, 'update', rowValue.toLowerCase());
		},
		onSave: function(params){
			var toValidate = ['Subscription Type', 'Individual Data Extra', 'Roaming Extra', 'Call Recording', 'BT Signal Assist', 'SIM Extra','Device Management','Devices Required'], // Columns == Attribute Names to validate
				data = OEValidation.Validate.BasketContents.BTOP['Implementation Form'](params.basketContents, params.subscriptionsMap, toValidate);

			setTimeout(OEValidation.Validate.Table.BTOP['Implementation Form'], 10);
			setTimeout(OEValidation.UI.CheckInvalid, 20);
			
			OEValidation.UI.InitializeTooltips(data);

			// Save validity flag to basket
			SPM.remoteAction({ 
				configId: window.configurationId,
				basketId: params.basketContents.Id,
				definitionName: commDefName,
				action: 'ImplementationForm',
				invalidCount: jQuery('#schemaTable').find('.oe-invalid').length },
				'CS_OESaveHandler', 
				'onSaveHandler');
		},
		onFetchParentData: function(response_NOT_USED, BC){
			console.log('onFetchParentData BTOP');
			var subsMap = {},
				nType = '';

			var cliInBasket = false;
			var configList = BasketItems.Map.BasketConfigs;
			var totalNumbersAllowed = 0;

			for(cfgId in configList){
				var config = configList[cfgId];
				if(config.name != null && config.name != ''){
					if(config.name.toLowerCase().indexOf('cli') > -1)
						cliInBasket = true;
					if(config.name.toLowerCase().indexOf('additional mobile number') > -1)
						totalNumbersAllowed += config.quantity;
				}
			} 

			if(!cliInBasket)
				updateOptions('Flexible Caller Identity', ['No']);
			else
				updateOptions('Flexible Caller Identity', ['No', 'Yes']);

			BasketItems.AdditionalMobileNumber = {
				allowedAdditionalNumbers : totalNumbersAllowed,
				additionalNumbersUsed : 0,
				addedNumberRowIds: []
			}

			var callRecordingMap = {
				baseName : 'Call Recording Standard - 1 Month',
				baseTotalQty : 0,
				relatedQty : 0
			};

			for(var prop in BC.Map){
				if(prop != 'BasketConfigs'){
					var product = BC.Map[prop];
					for(var i = 0; i < product.length; i++){
						var productName = product[i].name;
						if(productName === callRecordingMap.baseName)
							callRecordingMap.baseTotalQty += parseInt(product[i].quantity);
						else if(productName.indexOf('Call Recording') > -1)
							callRecordingMap.relatedQty += parseInt(product[i].quantity);

						if(typeof subsMap[product[i].name] === 'undefined')
							subsMap[product[i].name] = parseInt(product[i].quantity);
						else
							subsMap[product[i].name] += parseInt(product[i].quantity);
					}
				}
			}
			subsMap[callRecordingMap.baseName] = callRecordingMap.baseTotalQty - callRecordingMap.relatedQty;
			return subsMap;
		}
	}
};
OEValidation.Rules.BTOP = {
	'Implementation Form': {
		NonEmpty: function(element){
			if(element.value === '')
				jQuery(element).parent().addClass('oe-invalid').addClass('oe-required');
			else
				jQuery(element).parent().removeClass('oe-invalid').removeClass('oe-required');
		},
		Email: function(element){
			var isValid = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g.test(element.value.toString()) && !/@gmail|@yahoomail|@hotmail/g.test(element.value.toString());
			if(!isValid)
				jQuery(element).parent().addClass('oe-invalid').addClass('oe-required');
			else
				jQuery(element).parent().removeClass('oe-invalid').removeClass('oe-required');
		},
		DealType: function(element){
			var row = jQuery(element).closest('tr'),
				rowGuid = jQuery(row).find('button').data('guid');
			OEValidation.UI.ToggleFields(row, ['Mobile Number', 'PAC/STAC', 'PAC Expiry Date'], false);
			OEValidation.UI.RequireFields(row, ['Mobile Number', 'PAC/STAC', 'PAC Expiry Date'], false);
			OEValidation.UI.ValidateFields(row, ['Mobile Number', 'PAC/STAC', 'PAC Expiry Date'], false);
			switch(element.value){			
				case 'Port':
					OEValidation.UI.RequireFields(row, ['Mobile Number', 'PAC/STAC', 'PAC Expiry Date'], true);
					OEValidation.UI.ValidateFields(row, ['Mobile Number', 'PAC/STAC', 'PAC Expiry Date'], true);
					var myrow = configurationMap[rowGuid];
					var myelementPAC = jQuery('[data-guid="' + myrow['guid'] + '"]').parent().parent().find('input[data-id="PAC/STAC"]');
					OEValidation.Rules[commDefName][nonCommDefName].PAC(myelementPAC[0]);	
					var myelementPACDate = jQuery('[data-guid="' + myrow['guid'] + '"]').parent().parent().find('input[data-id="PAC Expiry Date"]');
					OEValidation.Rules[commDefName][nonCommDefName].Date(myelementPACDate[0]);	
					break;
				case 'Migration From Other BT':
					OEValidation.UI.RequireFields(row, ['Mobile Number', 'PAC/STAC', 'PAC Expiry Date'], true);
					OEValidation.UI.ValidateFields(row, ['Mobile Number', 'PAC/STAC', 'PAC Expiry Date'], true);
					break;
				case 'New Connection':
				case 'New':
					OEValidation.Rules['BTOP']['Implementation Form'].ExtensionNumber();
					OEValidation.UI.ToggleFields(row, ['PAC Expiry Date'], true);
					break;
				case 'Resign':
				case 'BTOP Re-Sign':
				case undefined:
				case 'undefined':	
				default:
					//OEValidation.UI.RequireFields(row, ['Mobile Number', 'PAC/STAC'], false);
					//OEValidation.UI.ValidateFields(row, ['Mobile Number', 'PAC/STAC'], false);
					break;
			}
		},
		MobileNumber: function(element){
			var row = jQuery(element).closest('tr'),
				rowGuid = jQuery(row).find('button').data('guid'),
				numberType = OEValidation.Util.GetRowAttributeValue(rowGuid, 'Number Type');

			jQuery(element).parent().removeClass('oe-invalid oe-required');
			if(!element.disabled && element.value.toString() != ''){
				var isValid = /^44\d{10}$/g.test(element.value.toString());
				if(!isValid)
					jQuery(element).parent().removeClass('oe-invalid oe-required').addClass('oe-invalid oe-required');
			}
		},
		MobileNumberCSV: function(element){},
		SIPExtraSoftphoneOnly: function(){
			OEValidation.UI.ResetNotification();
			var sipExtraAllowed = 0;
			BasketItems.Map.BasketConfigs.forEach(
				function(value){
					if(value.name.toLowerCase().indexOf('sip extra') > -1)
						sipExtraAllowed += value.quantity;
				}
			);
			if(sipExtraAllowed == 0)
				updateOptions('SIP Extra Softphone Only', ['No']);
			else{
				var sipExtraTaken = 0;
				var showWarning = false;
				Object.keys(configurationMap).forEach(function(el, index){
					var row = configurationMap[el];
					var rowValue = row['SIP Extra Softphone Only'];
					var element = jQuery('[data-guid="' + row['guid'] + '"]').parent().parent().find('input[data-id="SIP Extra Softphone Only"]');
					if(rowValue != ''){
						if(rowValue.indexOf('Yes') && sipExtraTaken < sipExtraAllowed)
							sipExtraTaken += 1;
						if(rowValue.indexOf('Yes') && sipExtraTaken == sipExtraAllowed){
							showWarning = true;
							element.parent().removeClass('oe-invalid oe-required').addClass('oe-invalid oe-required');
						}
					}
				});
				
				if(showWarning)
					OEValidation.UI.ShowNotification('Number of allowed SIP Extra in basket is ' + sipExtraAllowed);
			}
		},
		ExtensionNumber: function(){
			OEValidation.UI.ResetNotification();
			var numbersList = [];
			var duplicateElements = [];
			var duplicateNumberIdexRows = '';
			Object.keys(configurationMap).forEach(function(el, index){
				var row = configurationMap[el];
				var rowValue = row['Extension Number'];
				var numberType = row['Number Type'];
				var element = jQuery('[data-guid="' + row['guid'] + '"]').parent().parent().find('input[data-id="Extension Number"]').parent();
				element.removeClass('oe-invalid oe-required');

				if(numberType.toLowerCase().indexOf('new') > -1 && rowValue == '')
					element.addClass('oe-invalid oe-required');
				
				if(rowValue != ''){
					if(numbersList.indexOf(rowValue) == -1)
						numbersList.push(rowValue);
					else{
						duplicateNumberIdexRows += Number(index + 1) + ' and ';
						duplicateElements.push(element);
					}
					var isValid = /^[1-9]\d{2,4}$/g.test(rowValue) && !/^1571$|^999$|^8803$|^8903$|^88..$|^890.$/.test(rowValue);
					if(!isValid)
						element.addClass('oe-invalid oe-required');
				}
			});

			if(duplicateNumberIdexRows != ''){
				duplicateElements.forEach(function(element, index){
					element.addClass('oe-invalid oe-required');
				});
				duplicateNumberIdexRows = duplicateNumberIdexRows.substring(0, duplicateNumberIdexRows.length - 3);
				OEValidation.UI.ShowNotification('Extension mobile number duplicates in row/s: ' + duplicateNumberIdexRows);
			}
		},
		PAC: function(element){
				jQuery(element).parent().removeClass('oe-invalid oe-required');
				if(!element.disabled){
					var isValid = /^\d{6}[a-zA-Z]{3}|[a-zA-Z]{3}\d{6}$/g.test(element.value.toString());
					if(!isValid)
						jQuery(element).parent().removeClass('oe-invalid oe-required').addClass('oe-invalid oe-required');
				}
		},
		Date: function(element){
				var row = jQuery(element).closest('tr');
				var rowGuid = jQuery(row).find('button').data('guid');
				var	timestamp = Date.parse(element.value);
				if(element.value === '' || (Date.now() > timestamp))
					jQuery(element).parent().removeClass('oe-invalid oe-required').addClass('oe-invalid oe-required');
				else
					jQuery(element).parent().removeClass('oe-invalid oe-required');
		},
		AdditionalMobileNumber: function(rowGuid, operation, rowValue){
			//check if additional mobile number PC is in basket and validate how much of AMN can be selected in form 
			var rowHeaderName = 'Additional mobile number';
			var mobileNumberMap = BasketItems.AdditionalMobileNumber;
			if(mobileNumberMap != undefined){
				if(operation == 'insert' || operation == 'update'){
					if(rowValue != '' && rowValue.indexOf('yes') > -1 && mobileNumberMap.addedNumberRowIds != undefined &&  mobileNumberMap.addedNumberRowIds != null && mobileNumberMap.addedNumberRowIds.indexOf(rowGuid) == -1){
						if(mobileNumberMap.additionalNumbersUsed < mobileNumberMap.allowedAdditionalNumbers){
							mobileNumberMap.addedNumberRowIds.push(rowGuid);
							mobileNumberMap.additionalNumbersUsed += 1;
						}
						else if(mobileNumberMap.additionalNumbersUsed == mobileNumberMap.allowedAdditionalNumbers)
							OEValidation.Util.SetRowAttributeValue(rowGuid, rowHeaderName, 'No');
					}
					if(rowValue != '' && rowValue.indexOf('no') > -1){
						if(mobileNumberMap.addedNumberRowIds != undefined && mobileNumberMap.addedNumberRowIds.length > 0 && mobileNumberMap.addedNumberRowIds.indexOf(rowGuid) > -1){
							mobileNumberMap.addedNumberRowIds.pop(rowGuid);
							mobileNumberMap.additionalNumbersUsed -= 1;
						}
					}
				}
				if(operation == 'delete'){
					if(mobileNumberMap.addedNumberRowIds != undefined && mobileNumberMap.addedNumberRowIds.length > 0 && mobileNumberMap.addedNumberRowIds.indexOf(rowGuid) > -1){
						mobileNumberMap.addedNumberRowIds.pop(rowGuid);
						mobileNumberMap.additionalNumbersUsed -= 1;
					}
				}
			}
		}
	}
};
OEValidation.Validate.Table.BTOP = {
	'Implementation Form': function(params){
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].DealType, 'Number Type');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].MobileNumber, 'Mobile Number');
		//OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].PAC, 'PAC/STAC');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].NonEmpty, 'Last Name');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].NonEmpty, 'First Name');
		OEValidation.Util.IterateAllRows(OEValidation.Rules[commDefName][nonCommDefName].Email, 'Email Address');
	}
};
OEValidation.Validate.BasketContents.BTOP = {
	'Implementation Form': function(BasketContents, SubscriptionsMap, toValidate){
		var	validityMap = {},
			selectedOptions = {}, // Table wide Map of values selected from picklists
			availableOptions = {}, // Basket wide Map which holds quantity of available products
			invalidColumns = [], // Array which holds the values of invalid columns
			$schemaTableHeader = jQuery('#schemaTable').find('thead tr'); // Table header 
		$schemaTableHeader.find('.oe-invalid').removeClass('oe-invalid'); // Reset invalid markers
		availableOptions['BT Signal Assist'] = 0;
		availableOptions['SIM Extra'] = 0;

		// Available options for subs
		for(var sub in SubscriptionsMap){
			availableOptions[sub] = SubscriptionsMap[sub];
		}

		// populate selectedOptions
		for(var i = 0; i < toValidate.length; i++){
			if(toValidate[i] !== 'BT Signal Assist' && toValidate[i] !== 'Number Type' && toValidate[i] !== 'SIM Extra'){
				var picklistOptions = OEValidation.Util.GetPicklistOptionsFromSchema(toValidate[i]);
				for(var j = 0; j < picklistOptions.length; j++){
					var currentOption = picklistOptions[j]; // eg. Existing Hardware
					if(typeof selectedOptions[currentOption] === 'undefined')// if there is no key in obj create
						selectedOptions[currentOption] = 0;
					var count = 0;
					for(var rowGuid in window.configurationMap){
						var configuration = window.configurationMap[rowGuid];
						if(configuration[toValidate[i]] === currentOption)
							count++;
					}
					selectedOptions[currentOption] = selectedOptions[currentOption] + count;
				}
			}
			else{
				if(typeof selectedOptions[toValidate[i]] === 'undefined')// if there is no key in obj create
					selectedOptions[toValidate[i]] = 0;
				var count = 0;
				for(var rowGuid in window.configurationMap){
					var configuration = window.configurationMap[rowGuid];
					if(configuration[toValidate[i]] === 'Yes')
						count++;
				}
				selectedOptions[toValidate[i]] = selectedOptions[toValidate[i]] + count;
			}
		}

		// Populate invalidColumns and push to validity map
		for(var p in availableOptions){
			var specialCase = (p === 'BT Signal Assist' || p === 'SIM Extra');
			var attName =  specialCase ? p : OEValidation.Util.GetAttributeNameFromOption(p);
			var isValid = (selectedOptions[p] === availableOptions[p]) ? true : false;
			validityMap[p] = { valid: isValid, parent: attName };

			if(!isValid)
				invalidColumns.push(attName);	
		}

		// Special case for Number Type
		if(OEValidation.Util.GetRowCount() !== BasketContents.HQ[0].Volume__c){
			invalidColumns.push('Number Type');
			validityMap['Number Type'] = { valid: false, parent: null };
			availableOptions['Number Type'] = BasketContents.HQ[0].Volume__c;
		}

		// Mark invalid entries
		for(var l = 0; l < invalidColumns.length; l++){
			var column = invalidColumns[l],
				columnIndex = OEValidation.Columns.indexOf(column) + 2;
			switch(column){
				case undefined:
				case 'undefined':
					// do nothing
					break;
				default:
					$schemaTableHeader.find('th:nth-child(' + columnIndex + ')').addClass('oe-invalid');
					break;
			}
		}

		return { vMap: validityMap, aOptions: availableOptions };
	}
};
/* BTOP*/

(function(document, window, OEV, BC, undefined){

	jQuery('body').addClass(commDefName);

	// Definitions[commDefName].DOM[nonCommDefName]();
	OEV.UI.DOM[commDefName][nonCommDefName](); // Add Containers to DOM
	OEV.UI.Init[commDefName][nonCommDefName](); // Additional Initialization functions
		
	var validityMap = null,
		availableOptions = null,
		subscriptionsMap = null;

	window.onDelete =  function(rowGuid){
		OEV.UI.Handlers[commDefName][nonCommDefName].onDelete(rowGuid);
		setTimeout(onSave, 10);
	};
	window.onNewRow = function(rowGuid){
		OEV.UI.Handlers[commDefName][nonCommDefName].onNewRow(rowGuid);
		setTimeout(onSave, 10);
	};
	window.onInputChange = function(element){
		setMultipleConfiguration();
		OEV.UI.Handlers[commDefName][nonCommDefName].onInputChange(element, BC.SIMCardType);
		setTimeout(onSave, 10);
	};
	window.onSave = function(label){
		console.log('onSave window');
		OEV.UI.Handlers[commDefName][nonCommDefName].onSave({ 
			label: label, 
			basketContents: BC,
			subscriptionsMap: subscriptionsMap
		});
	};

	function onSaveHandler(response){
		console.log('onSaveHandler', response);
	}

	function onFetchParentData(responseMap){
		subscriptionsMap = OEV.UI.Handlers[commDefName][nonCommDefName].onFetchParentData(responseMap, BC);
		setTimeout(onSave, 10);
	}

	registerHandler('remoteAction', onFetchParentData); // standalone OE
	registerHandler('remoteAction', onSaveHandler);
	registerHandler('onDelete', onDelete);
	registerHandler('onNewRow', onNewRow);
	registerHandler('onInputChange', onInputChange);
	registerHandler('onSave', onSave);
	registerHandler('onCancel', function(){ onSave('local'); });

	SPM.remoteAction({ configId: window.configurationId, name: window.commDefName + 'Related' }, 'CS_OEDataHandler', 'onFetchParentData');
	
})(document, window, OEValidation, BasketItems);

function changeMLEdisplay(pageNumber) {
	jQuery("#tableBody").empty();
	var index = pageNumber - 1;
	var configurationGuids = Object.keys(configurationMap);
	for (var i = index * pageSize; i < (index + 1) * (pageSize); i++) {
		if (configurationGuids[i]) {
			initRow(configurationMap[configurationGuids[i]]);
		}
	}
	if (detectIE()) {
		var dateInputs = jQuery('[type="date"]');
		if (dateInputs.length > 0) {
			dateInputs.datepicker({ dateFormat: 'mm-dd-yy' });
			dateInputs.attr('readonly', true);
		}
	}
}

function detectIE() {
	var ua = window.navigator.userAgent;

	var msie = ua.indexOf('MSIE ');
	if (msie > 0) {
		// IE 10 or older => return version number
		return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
	}

	var trident = ua.indexOf('Trident/');
	if (trident > 0) {
		// IE 11 => return version number
		var rv = ua.indexOf('rv:');
		return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
	}

	var edge = ua.indexOf('Edge/');
	if (edge > 0) {
		// Edge (IE 12+) => return version number
		//return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
	}

	// other browser
	return false;
}