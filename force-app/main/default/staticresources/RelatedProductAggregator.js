var log = function(){
  console.log.apply(console, ['>>> Aggregator Log >>>'].concat(Array.prototype.slice.call(arguments)) );  
};

/*
    Aggregator > RelatedProductAggregator.js
    Usage:  Create an Action [Execute JS] on a Product Definition, enter "RP.Aggregator.init();" and associate with new rule. 
            Check the RelatedProductMap.js to see if the related Definition has an assigned map.
            If the mapping is there, compile the PD and thats it!
*/

var Aggregator = function() {
    
    function init(){
        log('RelatedProductAggregator init');
        var definitionName = CS.Service.getProductIndex().productsById[CS.Service.config[CS.Service.getCurrentConfigRef()].config.cscfga__Product_Definition__c].Name,
            definitionMap = RP.Map.getDefinition(definitionName);

        if(definitionMap !== null && typeof definitionMap !== 'undefined' && definitionMap !== undefined){
            
            log(definitionMap);
        
            for(var i = 0; i < definitionMap.relatedProducts.length; i++){
                
                var childMap = RP.Map.getDefinition(definitionMap.relatedProducts[i]);
                
                log(childMap);
                
                if(childMap && childMap.nested && childMap.parent.indexOf(definitionName) > -1){
                    
                    var childAggMap = getChildAggregationMap(childMap.mappings, definitionMap.relatedProducts[i]);
                    setAttributes(aggregateFromMap(childAggMap));
                    
                }
                
            }
            
            var parentAggMap = getAggregationMap(definitionMap.mappings);
            setAttributes(aggregateFromMap(parentAggMap));
        }
    }
    
    
    function getChildAggregationMap(mappings, parent){
        
        var map = [],
            childCount = CS.countRelatedProducts(formatName(parent) + '0');

            for(var childIndex = 0; childIndex < childCount; childIndex++){
            
                for(var i = 0; i < mappings.length; i++){
                
                    var attributeName = Object.keys(mappings[i])[0],
                        relatedAttributes = mappings[i][attributeName];
                
                        for(var j = 0; j < relatedAttributes.length; j++){
                          
                            var relatedItem = relatedAttributes[j],
                                grandChildCount = CS.countRelatedProducts(formatName(parent) + childIndex + ':' + formatName(relatedItem.relatedProductName) + '0'); 
                
                          
                            for(var k = 0; k < grandChildCount; k++){
                            
                                var grandChildAttribute = formatName(parent) + childIndex + ':' + formatName(relatedItem.relatedProductName) + k + ':' + formatName(relatedItem.relatedProductAttribute) + '0',
                                    childAttribute = formatName(parent) + childIndex + ':' + formatName(attributeName) + '0';
                                    
                                
                                map.push({
                                    from: grandChildAttribute,
                                    to: childAttribute
                                });
                            }
                
                      }
                
                }
            }
            
        return map;
    }
    
    function getAggregationMap(mappings){
        
        var map = [];

            for(var i = 0; i < mappings.length; i++){
            
              var attributeName = Object.keys(mappings[i])[0],
                  relatedAttributes = mappings[i][attributeName];
            
            
                  for(var j = 0; j < relatedAttributes.length; j++){
            
                      var relatedItem = relatedAttributes[j],
                          childCount = CS.countRelatedProducts(formatName(relatedItem.relatedProductName) + '0');
						  
							if(childCount === 0) {
								map.push({
									from: null,
									to: formatName(attributeName) + '0'
								});
							}
            
							for(var k = 0; k < childCount; k++){
                            
                            var childAttribute = formatName(relatedItem.relatedProductName) + k + ':' + formatName(relatedItem.relatedProductAttribute) + '0';
                                    
                                map.push({
                                    from: childAttribute,
                                    to: formatName(attributeName) + '0'
                                });
                          }
                 }
            
            }
            
        return map;
    }
    
    function aggregateFromMap(map){
        var aggregated = {};
        for(var i = 0; i < map.length; i++){
            
            var attrMap = map[i];
            
            if(typeof aggregated[attrMap.to] === 'undefined'){
                aggregated[attrMap.to] = [];
            }
			
			var attributeInConfig = CS.Service.config[attrMap.from];

			if(attributeInConfig !== null && typeof attributeInConfig !== 'undefined' && attributeInConfig !== undefined && attributeInConfig.attr.cscfga__Value__c !== ''){
				aggregated[attrMap.to] = aggregated[attrMap.to].concat(attributeInConfig.attr.cscfga__Value__c);
			}
        }
        return aggregated;
    }
    
    function setAttributes(map){
        log(map);
        for(var prop in map){
      if (attributeIsPresent(prop)) {
          var array = map[prop],
              value;
          
          if(array.length > 0 && isNumber(array[0])){
                    value = array.reduce(function(a, c){ return parseFloat(a) + parseFloat(c); });
          }else{
              value = array.join(',');
          }
        
        CS.setAttribute(prop, value + '');
      }
        }
        
    }

    /* Utility */

    function formatName(name){
        return name.split(' ').join('_') + '_';
    }

    function attributeIsPresent(ref) {
        return (CS.Service.config[ref] && CS.Service.config[ref].attr);
    }
      
    function isNumber(ref){
        return ref !== undefined && ref !== null ? !isNaN(ref) : false;
    }

    return {
        init: init
    }
};


var RP = RP || {};
RP.Aggregator = Aggregator();