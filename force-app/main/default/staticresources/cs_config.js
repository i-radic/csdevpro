/**
* Custom resource name: 'cs_config'
*
* Idea here is to create main container object and namespce for custom code, view, model and configuration properties
* that will be needed for customisation of the page *
*/
/**
* Custom namespace container - CloudSenseCustom */

require(['cs_js/cs-full', 'cs_js/cs-event-handler'], function (CS, EventHandler) { 

var RootDefinitionName = "";

CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_SAVE_ACTION, function(payload) {

	console.log('BEFORE_SAVE_ACTION');
	RootDefinitionName = getRootDefinitionName();
	
	switch(RootDefinitionName) 
	{
  	case "Mobile":
  	case "SIP Trunking":
    	//RP.Aggregator.init();
    }

});

CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_ADD_RELATED_PRODUCT_ACTION, function(payload) {

	console.log('AFTER_ADD_RELATED_PRODUCT_ACTION');
	window.scrollTo(0,0);

});

CS.EventHandler.subscribe(CS.EventHandler.Event.SCREEN_LIST_POPULATED, function(payload) {
    


    

switch(RootDefinitionName) 
	{
  	case "PRODUCT_DEFINITION_NAME":
    	
    break;

  	default:
    console.log("cs_config.js: There is no SCREEN_LIST_POPULATED functionality for PD " + RootDefinitionName);
	}


});



CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATOR_READY, function(payload) {
	
	console.log('CONFIGURATOR_READY');
	RootDefinitionName = getRootDefinitionName();
	console.log("cs_config.js: " + RootDefinitionName + '.CONFIGURATOR_READY executing');

	switch(RootDefinitionName) 
	{
  	case "CloudVoice":
    case "Future Mobile Service":
        CS.UI.NotificationMessages.clearUI();
        CS.UI.displayErrorMessage("This configuration can only be edited in Solution Console!");

        CS.UI.ScreenSections.collapseAll();
        //document.getElementsByClassName("cs-btn cs-btn-no-icon cs-btn-default cs-btn-initial")[0].disabled=true;
        //document.getElementsByClassName("cs-btn cs-btn-no-icon cs-btn-default cs-btn-brand")[0].disabled=true;
        //document.getElementsByClassName("cs-btn cs-btn-no-icon cs-btn-default cs-btn-brand")[1].disabled=true;
            
        var buttons = document.getElementsByClassName("cs-btn cs-btn-initial cs-btn-default cs-btn-no-icon");

        Array.prototype.forEach.call(buttons, function(el) {
            el.disabled=true;
        });
            
        buttons = document.getElementsByClassName("cs-btn cs-btn-no-icon cs-btn-default cs-btn-brand");

        Array.prototype.forEach.call(buttons, function(el) {
            el.disabled=true;
        });
    
    break;

  	default:
    console.log("cs_config.js: There is no CONFIGURATOR_READY functionality for PD " + RootDefinitionName);
	}

    
   

    

});

    
function getRootDefinitionName() {
    var index = CS.Service.getProductIndex();
    var definitionId = CS.Service.config[""].config['cscfga__Product_Definition__c'];
    return index.all[definitionId].Name;
}


//*****************

//CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATOR_LAUNCH, function(payload) { console.log("CONFIGURATOR_LAUNCH")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_DELEGATE_FUNCTION, function(payload) { console.log("BEFORE_DELEGATE_FUNCTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_DELEGATE_FUNCTION, function(payload) { console.log("AFTER_DELEGATE_FUNCTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.RULES_STARTED, function(payload) { console.log("RULES_STARTED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATION_RESET, function(payload) { console.log("CONFIGURATION_RESET")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_ATTRIBUTE_UPDATE, function(payload) { console.log("AFTER_ATTRIBUTE_UPDATE")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.RULES_ITERATION_START, function(payload) { console.log("RULES_ITERATION_START")});
//
//CS.EventHandler.subscribe(CS.EventHandler.Event.RULES_ITERATION_END, function(payload) { console.log("RULES_ITERATION_END")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.RULES_FINISHED, function(payload) { console.log("RULES_FINISHED")});
//
//CS.EventHandler.subscribe(CS.EventHandler.Event.SCREEN_LIST_POPULATED, function(payload) { console.log("EVENT_SCREEN_LIST_POPULATED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BUTTONS_DISPLAYED, function(payload) { console.log("EVENT_BUTTONS_DISPLAYED")});
//
//CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATOR_READY, function(payload) { console.log("CONFIGURATOR_READY")});








//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_ADD_RELATED_PRODUCT_ACTION, function(payload) { console.log('AFTER_ADD_RELATED_PRODUCT_ACTION')});
//
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_CANCEL_ACTION, function(payload) { console.log("AFTER_CANCEL_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_CLEAR_DATE_ACTION, function(payload) { console.log("AFTER_CLEAR_DATE_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_CLEAR_LOOKUP_ACTION, function(payload) { console.log("AFTER_CLEAR_LOOKUP_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_CONTINUE_ACTION, function(payload) { console.log("AFTER_CONTINUE_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_COPY_RELATED_PRODUCT, function(payload) { console.log("AFTER_COPY_RELATED_PRODUCT")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_COPY_RELATED_PRODUCT_ACTION, function(payload) { console.log("AFTER_COPY_RELATED_PRODUCT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_DELEGATE_CALLBACK, function(payload) { console.log("AFTER_DELEGATE_CALLBACK")});
//
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_EDIT_RELATED_PRODUCT_ACTION, function(payload) { console.log("AFTER_EDIT_RELATED_PRODUCT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_EDIT_RP_QUANTITY_ACTION, function(payload) { console.log("AFTER_EDIT_RP_QUANTITY_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_FINISH_ACTION, function(payload) { console.log("AFTER_FINISH_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_LOOKUP_SELECT_ACTION, function(payload) { console.log("AFTER_LOOKUP_SELECT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_NEXT_SCREEN_ACTION, function(payload) { console.log("AFTER_NEXT_SCREEN_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_OPEN_LOOKUP_ACTION, function(payload) { console.log("AFTER_OPEN_LOOKUP_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_PREVIOUS_SCREEN_ACTION, function(payload) { console.log("AFTER_PREVIOUS_SCREEN_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_REMOVE_LOOKUP_ACTION, function(payload) { console.log("AFTER_REMOVE_LOOKUP_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_REMOVE_RELATED_PRODUCT, function(payload) { console.log("AFTER_REMOVE_RELATED_PRODUCT")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_REMOVE_RELATED_PRODUCT_ACTION, function(payload) { console.log("AFTER_REMOVE_RELATED_PRODUCT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_SAVE_ACTION, function(payload) { console.log("AFTER_SAVE_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_SHOW_SCREEN_ACTION, function(payload) { console.log("AFTER_SHOW_SCREEN_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_ADD_RELATED_PRODUCT_ACTION, function(payload) { console.log("BEFORE_ADD_RELATED_PRODUCT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_ATTRIBUTE_UPDATE, function(payload) { console.log("BEFORE_ATTRIBUTE_UPDATE")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_CANCEL_ACTION, function(payload) { console.log("BEFORE_CANCEL_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_CLEAR_DATE_ACTION, function(payload) { console.log("BEFORE_CLEAR_DATE_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_CLEAR_LOOKUP_ACTION, function(payload) { console.log("BEFORE_CLEAR_LOOKUP_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_CONTINUE_ACTION, function(payload) { console.log("BEFORE_CONTINUE_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_COPY_RELATED_PRODUCT, function(payload) { console.log("BEFORE_COPY_RELATED_PRODUCT")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_COPY_RELATED_PRODUCT_ACTION, function(payload) { console.log("BEFORE_COPY_RELATED_PRODUCT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_EDIT_RELATED_PRODUCT_ACTION, function(payload) { console.log("BEFORE_EDIT_RELATED_PRODUCT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_EDIT_RP_QUANTITY_ACTION, function(payload) { console.log("BEFORE_EDIT_RP_QUANTITY_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_FINISH_ACTION, function(payload) { console.log("BEFORE_FINISH_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_LOOKUP_SELECT_ACTION, function(payload) { console.log("BEFORE_LOOKUP_SELECT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_NEXT_SCREEN_ACTION, function(payload) { console.log("BEFORE_NEXT_SCREEN_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_OPEN_LOOKUP_ACTION, function(payload) { console.log("BEFORE_OPEN_LOOKUP_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_PREVIOUS_SCREEN_ACTION, function(payload) { console.log("BEFORE_PREVIOUS_SCREEN_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_REMOVE_LOOKUP_ACTION, function(payload) { console.log("BEFORE_REMOVE_LOOKUP_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_REMOVE_RELATED_PRODUCT, function(payload) { console.log("BEFORE_REMOVE_RELATED_PRODUCT")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_REMOVE_RELATED_PRODUCT_ACTION, function(payload) { console.log("BEFORE_REMOVE_RELATED_PRODUCT_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_SAVE_ACTION, function(payload) { console.log("BEFORE_SAVE_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_SELECT_RP_DEFINITION_ACTION, function(payload) { console.log("BEFORE_SELECT_RP_DEFINITION_ACTION")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_SHOW_SCREEN_ACTION, function(payload) { console.log("BEFORE_SHOW_SCREEN_ACTION")});
//
//CS.EventHandler.subscribe(CS.EventHandler.Event.CHUNKED_SAVE_FAILED, function(payload) { console.log("CHUNKED_SAVE_FAILED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.CHUNKED_SAVE_FINISHED, function(payload) { console.log("CHUNKED_SAVE_FINISHED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.CHUNKED_SAVE_STARTED, function(payload) { console.log("CHUNKED_SAVE_STARTED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.CHUNKED_SAVE_UPDATED, function(payload) { console.log("CHUNKED_SAVE_UPDATED")});
//
//
//CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATOR_READY, function(payload) { console.log("CONFIGURATOR_READY")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.CURRENT_PRODUCT_CHANGED, function(payload) { console.log("CURRENT_PRODUCT_CHANGED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.DEFERRED_UI_UPDATES_COMPLETE, function(payload) { 
//  console.log("DEFERRED_UI_UPDATES_COMPLETE");
//  //CS.Rules.evaluateAllRules();
//});
//CS.EventHandler.subscribe(CS.EventHandler.Event.DYNAMIC_LOOKUPS_DONE, function(payload) { console.log("DYNAMIC_LOOKUPS_DONE")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.DYNAMIC_LOOKUPS_START, function(payload) { console.log("DYNAMIC_LOOKUPS_START")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.DYNAMIC_LOOKUP_FAILED, function(payload) { console.log("DYNAMIC_LOOKUP_FAILED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.DYNAMIC_LOOKUP_RETRIEVED, function(payload) { console.log("DYNAMIC_LOOKUP_RETRIEVED")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.MARK_CONFIGURATION_INVALID, function(payload) { console.log("MARK_CONFIGURATION_INVALID")});
//CS.EventHandler.subscribe(CS.EventHandler.Event.PRODUCT_DEFINITION_ITERATION_CHANGED, function(payload) { console.log("PRODUCT_DEFINITION_ITERATION_CHANGED")});
//*****************








});