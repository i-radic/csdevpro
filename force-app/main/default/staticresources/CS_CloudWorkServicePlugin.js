var CLOUD_WORK_CONST = {
	solution: 'Cloud Work', //Main SCHEMA name for plugin creation
	mainComponent: 'Cloud Work',
	site: 'Site',
	//portSpeed: 'Port Speed',
	//managedCPE: 'Managed CPE'
};

const ATTRIBUTE = {
    SITE_NAME: 'Site Name',
    SITE_TYPE: 'Site Type',
    NUMBER_OF_USERS: 'Number of Users',
    ACTIVATION_DATE: 'Activation Date',
    SITE_ADDRESS: 'Site Address',
    SITE_NAD_KEY: 'Site NAD Key',
    SHIPPING_NAD_KEY: 'Shipping NAD Key',
	USE_SAME_ADDRESS_FOR_SHIPPING: 'Use Same Address for Shipping',
    SHIPPING_ADDRESS: 'Shipping Address',
    IS_INTERNATIONAL_ADDRESS: 'Is International Address',
    SITE_ADDRESS_AUTO_POPULATED: 'Site-Address',
    SITE_TOWN_AUTO_POPULATED: 'Site-Town',
    SITE_POSTCODE_AUTO_POPULATED: 'Site-Postcode',
    SITE_COUNTRY_AUTO_POPULATED: 'Site-Country',
    SHIPPING_ADDRESS_AUTO_POPULATED: 'Shipping-Address',
    SHIPPING_TOWN_AUTO_POPULATED: 'Shipping-Town',
    SHIPPING_POSTCODE_AUTO_POPULATED: 'Shipping-Postcode',
    SHIPPING_COUNTRY_AUTO_POPULATED: 'Shipping-Country'    
};

//Register the Cloud Work Plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', async function() {
		await CS.SM.registerPlugin(CLOUD_WORK_CONST.solution)
			.then(plugin => {
				console.log("[CS_CloudWorkServicePlugin] Plugin registered for Cloud Work");
				CloudWorkHooks(plugin);
			});
	});
}

function CloudWorkHooks(CloudWorkPlugin) {

    CloudWorkPlugin.beforeConfigurationAdd = async function(component, configuration) {
        return Promise.resolve(true);
    };

    CloudWorkPlugin.afterConfigurationAdd = async function(component, configuration) {        
        return Promise.resolve(true);
	};

    CloudWorkPlugin.afterConfigurationDelete = async function(component, configuration) {
	    return Promise.resolve(true);
    };
    
    CloudWorkPlugin.afterAttributeUpdated = async function(component, configuration, attribute, oldValueMap) {
        let solution = await CS.SM.getActiveSolution();

        if (component.name == CLOUD_WORK_CONST.site) {
            if (attribute.name == ATTRIBUTE.SITE_NAME) {
                console.log("[CS_CloudWorkServicePlugin] Site Name change event fired!");

                if (!attribute.value.match(/^[a-z]/i)) {
                    CS.SM.displayMessage('Site Name cannot start with a number or special character', 'error');
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_NAME, '', '', false, solution, configuration.guid, true);
                    return false;
                }
            }

            if (attribute.name == ATTRIBUTE.SITE_TYPE) {
                console.log("[CS_CloudWorkServicePlugin] ");
            }

            if (attribute.name == ATTRIBUTE.NUMBER_OF_USERS) {
                console.log("[CS_CloudWorkServicePlugin] Number of Users changed");

                if (!attribute.value.match(/^[0-9]/i)) {
                    CS.SM.displayMessage('Number of users can only be valid numeric value', 'error');
                    updateConfigurationAttributeValue(ATTRIBUTE.NUMBER_OF_USERS, '', '', false, solution, configuration.guid, true);
                    calculateTotalNumberOfUsersOnAllComponents(component);
                    return false;
                }
                else {
                    calculateTotalNumberOfUsersOnAllComponents(component);
                }
            }

            if (attribute.name == ATTRIBUTE.ACTIVATION_DATE) {
                console.log("[CS_CloudWorkServicePlugin] ");
            }

            if (attribute.name == ATTRIBUTE.USE_SAME_ADDRESS_FOR_SHIPPING) {
                console.log("[CS_CloudWorkServicePlugin] Check/Uncheck Use Same Address for Shipping fired!");

                if (attribute.value == true) {
                    /*updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_ADDRESS, 'Shipping Address is the same as Billing Address',
                    'Shipping Address is the same as Billing Address', true, solution, configuration.guid, true);*/
                    configuration.updateAttribute(ATTRIBUTE.SHIPPING_ADDRESS, { value: null, showInUi: false } );
                    configuration.updateAttribute('Spacer0', { value: null, showInUi: true } );

                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_ADDRESS_AUTO_POPULATED, configuration.getAttribute(ATTRIBUTE.SITE_ADDRESS_AUTO_POPULATED).value, configuration.getAttribute(ATTRIBUTE.SITE_ADDRESS_AUTO_POPULATED).value, true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_TOWN_AUTO_POPULATED, configuration.getAttribute(ATTRIBUTE.SITE_TOWN_AUTO_POPULATED).value, configuration.getAttribute(ATTRIBUTE.SITE_TOWN_AUTO_POPULATED).value, true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_POSTCODE_AUTO_POPULATED, configuration.getAttribute(ATTRIBUTE.SITE_POSTCODE_AUTO_POPULATED).value, configuration.getAttribute(ATTRIBUTE.SITE_POSTCODE_AUTO_POPULATED).value, true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_COUNTRY_AUTO_POPULATED, configuration.getAttribute(ATTRIBUTE.SITE_COUNTRY_AUTO_POPULATED).value, configuration.getAttribute(ATTRIBUTE.SITE_COUNTRY_AUTO_POPULATED).value, true, solution, configuration.guid, true);
                }
                else {
                    //updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_ADDRESS, '', '', false, solution, configuration.guid, true);
                    configuration.updateAttribute(ATTRIBUTE.SHIPPING_ADDRESS, { value: null, showInUi: true } );
                    configuration.updateAttribute('Spacer0', { value: null, showInUi: false } );
                    
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_ADDRESS_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_TOWN_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_POSTCODE_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_COUNTRY_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                }
            }

            if (attribute.name == ATTRIBUTE.IS_INTERNATIONAL_ADDRESS) {
                if (attribute.value == true) {
                    console.log("[CS_CloudWorkServicePlugin] International Address enabled");

                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_ADDRESS_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_TOWN_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_POSTCODE_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_COUNTRY_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_ADDRESS_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_TOWN_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_POSTCODE_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_COUNTRY_AUTO_POPULATED, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_NAD_KEY, '', '', false, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_NAD_KEY, '', '', false, solution, configuration.guid, true);
                }
                else {
                    console.log("[CS_CloudWorkServicePlugin] International Address disabled");

                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_ADDRESS_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_TOWN_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_POSTCODE_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_COUNTRY_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_ADDRESS_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_TOWN_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_POSTCODE_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_COUNTRY_AUTO_POPULATED, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SITE_NAD_KEY, '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue(ATTRIBUTE.SHIPPING_NAD_KEY, '', '', true, solution, configuration.guid, true);
                }
            }
        }
        return Promise.resolve(true);
	};
}

// AFTER NAVIGATE
async function afterNavigateCloudWork(solution, currentComponent, previousComponent) {
    console.log('afterNavigateCloudWork', solution, currentComponent, previousComponent);

    correctProfile = 'true';
	if (correctProfile == null) {
		var currentBasket = await CS.SM.getActiveBasket();
		var result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
			'action': 'isProfileQueuedRemote'
		});
		correctProfile = result["goodProfile"];
		console.log(result);
	}
	if (currentComponent.name == CLOUD_WORK_CONST.mainComponent) {
	}

	if (currentComponent.name == CLOUD_WORK_CONST.site) {		
	}

    return true;
}

function calculateTotalNumberOfUsersOnAllComponents(component) {
    let usersTotal = 0;
    for (let allConfigs of Object.values(component.schema.configurations)) {
        usersTotal += parseInt(allConfigs.getAttribute(ATTRIBUTE.NUMBER_OF_USERS).value);
    }

    console.log("[CS_CloudWorkServicePlugin] Total number of users on all components:");
    console.log(usersTotal);
}