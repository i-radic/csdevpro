var Map = function(){
	
	var UsageProfileMap = {
			"Data": [
				"Data",
				"MMS",
				"MMS Roaming",
				"Roaming data in EU",
				"Roaming data in ROW",
				"Wifi",
				"Wifi in EU",
				"Wifi in ROW"
			],
			"SMS": [
				"Closed User Group SMS",
				"International numbers text messaging",
				"Other SMS",
				"Outgoing Roaming SMS",
				"SMS to EE",
				"SMS to Orange",
				"SMS to Other UK Networks",
				"SMS to T-Mobile"
			],
			"Voice": [
				"Calls to EE",
				"Calls to T-Mobile",
				"Calls to other UK mobile networks",
				"Closed user group",
				"Calls to Orange",
				"Answer phone",
				"Incoming roaming in EU",
				"Incoming roaming in ROW",
				"International calls",
				"Landline",
				"Freephone",
				"Other",
				"Outgoing roaming from EU",
				"Outgoing roaming from ROW"
			]
		};	

	function getMap(name){
		return UsageProfileMap[name];
	}

	function getItems(){
		var items = [];

		if(typeof Object.getOwnPropertyNames !== 'function' && typeof Object.getOwnPropertyNames !== 'undefined'){
			items = Object.getOwnPropertyNames(UsageProfileMap);
		}else{
			for(var prop in UsageProfileMap){
				items.push(prop);
			}
		}
		return items;
	}

	return {
		getMap: getMap,
		getItems: getItems
	}
};

var UP = UP || {};
UP.Map = Map();