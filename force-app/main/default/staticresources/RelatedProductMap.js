var Map = function(){

  var RelatedProductMap = [
    {
        "definitionName": "Cloud Phone",
        "relatedProducts": ["Cloud Phone Devices", "Cloud Phone Add On Licenses"],
        "mappings": [
            {
                "Number of Users": [{
                    "relatedProductName": "Cloud Phone Devices",
                    "relatedProductAttribute": "Number of Users"
                }]
			},
			{
				"Total Professional Services": [{
                    "relatedProductName": "Professional Services",
                    "relatedProductAttribute": "Total Professional Services"
                }]
            }
        ]
    },
    {
        "definitionName": "Mobile Broadband and Tablet",
        "relatedProducts": ["User Group SME"],
        "mappings": [{
                "Total Recurring Charges": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Total Recurring Charges"
                }]
            },
            {
                "Total Device Charges": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Total Device Charges"
                }]
            },
            {
                "Number of users": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Number of users"
                }]
            },
            {
                "Total Investment": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Total Investment"
                }]
            },
            {
                "Total Contribution": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Total Contribution"
                }]
            }
        ]
    },
    {
        "definitionName": "Mobile Voice SME",
        "relatedProducts": ["User Group SME", "Shared Add On SME"],
        "mappings": [{
                "Total Recurring Charges": [{
                        "relatedProductName": "User Group SME",
                        "relatedProductAttribute": "Total Recurring Charges"
                    },
                    {
                        "relatedProductName": "Shared Add On SME",
                        "relatedProductAttribute": "Total Recurring Charges"
                    }
                ]
            },
            {
                "Total Device Charges": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Total Device Charges"
                }]
            },
            {
                "Number of users": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Number of users"
                }]
            },
            {
                "Total Investment": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Total Investment"
                }]
            },
            {
                "Total Contribution": [{
                    "relatedProductName": "User Group SME",
                    "relatedProductAttribute": "Total Contribution"
                }]
            },
            {
                "Add Ons Investment Pot": [{
                    "relatedProductName": "Shared Add On SME",
                    "relatedProductAttribute": "Add Ons Investment Pot"
                }]
            }

        ]
    },
    {
        "definitionName": "User Group SME",
        "relatedProducts": ["Single Add On SME"],
        "nested": true,
        "parent": ["Mobile Voice SME", "Mobile Broadband and Tablet"],
        "mappings": [{
                "Total Recurring Charges": [{
                    "relatedProductName": "Single Add On SME",
                    "relatedProductAttribute": "Total Recurring Charges"
                }]
            },
            {
                "Recurring Charge": [{
                    "relatedProductName": "Single Add On SME",
                    "relatedProductAttribute": "Recurring Charge"
                }]
            }
        ]
    },
                           {
                            "definitionName" : "Fieldlink",
                            "relatedProducts": [ "Fieldlink User Group" ],
                            "mappings" : 
                            [
                              {
                                "Total Number of Users": 
                                [
                                  {
                                    "relatedProductName" : "Fieldlink User Group",
                                    "relatedProductAttribute" : "Number of users"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Data VPN",
                            "relatedProducts": [ "Leased Line",  "Additional Services", "Router Requirements"],
                            "mappings" : 
                            [
                              {
                                "TRCh": 
                                [
                                  {
                                    "relatedProductName" : "Leased Line",
                                    "relatedProductAttribute" : "TRCh"  
                                  },
                                  {
                                    "relatedProductName" : "Additional Services",
                                    "relatedProductAttribute" : "TRCh"  
                                  },
                                  {
                                    "relatedProductName" : "Router Requirements",
                                    "relatedProductAttribute" : "TRCh"  
                                  }
                                ]
                              },
                              {
                                "TRCo": 
                                [
                                  {
                                    "relatedProductName" : "Leased Line",
                                    "relatedProductAttribute" : "TRCo"  
                                  },
                                  {
                                    "relatedProductName" : "Additional Services",
                                    "relatedProductAttribute" : "TRCo"  
                                  },
                                  {
                                    "relatedProductName" : "Router Requirements",
                                    "relatedProductAttribute" : "TRCo"  
                                  }
                                ]
                              },
                              {
                                "TOOCh": 
                                [
                                  {
                                    "relatedProductName" : "Leased Line",
                                    "relatedProductAttribute" : "TOOCh"  
                                  },
                                  {
                                    "relatedProductName" : "Additional Services",
                                    "relatedProductAttribute" : "TOOCh"  
                                  },
                                  {
                                    "relatedProductName" : "Router Requirements",
                                    "relatedProductAttribute" : "TOOCh"  
                                  }
                                ]
                              },
                              {
                                "TOOCo": 
                                [
                                  {
                                    "relatedProductName" : "Leased Line",
                                    "relatedProductAttribute" : "TOOCo"  
                                  },
                                  {
                                    "relatedProductName" : "Additional Services",
                                    "relatedProductAttribute" : "TOOCo"  
                                  },
                                  {
                                    "relatedProductName" : "Router Requirements",
                                    "relatedProductAttribute" : "TOOCo"  
                                  }
                                ]
                              },
                              {
                                "Charges": 
                                [
                                  {
                                    "relatedProductName" : "Leased Line",
                                    "relatedProductAttribute" : "Charges"  
                                  },
                                  {
                                    "relatedProductName" : "Additional Services",
                                    "relatedProductAttribute" : "Charges"  
                                  },
                                  {
                                    "relatedProductName" : "Router Requirements",
                                    "relatedProductAttribute" : "Charges"  
                                  }
                                ]
                              },
                              {
                                "Costs": 
                                [
                                  {
                                    "relatedProductName" : "Leased Line",
                                    "relatedProductAttribute" : "Costs"  
                                  },
                                  {
                                    "relatedProductName" : "Additional Services",
                                    "relatedProductAttribute" : "Costs"  
                                  },
                                  {
                                    "relatedProductName" : "Router Requirements",
                                    "relatedProductAttribute" : "Costs"  
                                  }
                                ]
                              },
                              {
                                "Quantity": 
                                [
                                  {
                                    "relatedProductName" : "Leased Line",
                                    "relatedProductAttribute" : "Quantity"  
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Coverage Solutions",
                            "relatedProducts": [ "Coverage Solutions Device" ],
                            "mappings" : 
                            [
                              {
                                "One Off Charge":
                                [
                                  {
                                    "relatedProductName" : "Coverage Solutions Device",
                                    "relatedProductAttribute" : "One Off Charge"  
                                  }
                                ]
                              },
                              {
                                "One Off Cost":
                                [
                                  {
                                    "relatedProductName" : "Coverage Solutions Device",
                                    "relatedProductAttribute" : "One Off Cost"  
                                  }
                                ]
                              },
                              {
                                "Monthly Charge":
                                [
                                  {
                                    "relatedProductName" : "Coverage Solutions Device",
                                    "relatedProductAttribute" : "Monthly Charge"  
                                  }
                                ]
                              },
                              {
                                "Monthly Cost":
                                [
                                  {
                                    "relatedProductName" : "Coverage Solutions Device",
                                    "relatedProductAttribute" : "Monthly Cost"  
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "BT Product Lines and Calls",
                            "relatedProducts": [ "BT Product Line Features" ],
                            "mappings" :
                            [
                              {
                                "Feature Monthly Charge":
                                [
                                  {
                                    "relatedProductName" : "BT Product Line Features",
                                    "relatedProductAttribute" : "Feature Monthly Charge" 
                                  }
                                ]
                              },
                              {
                                "Total Feature Charges":
                                [
                                  {
                                    "relatedProductName" : "BT Product Line Features",
                                    "relatedProductAttribute" : "Total Feature Charges" 
                                  }
                                ]
                              },
                              {
                                "Total Feature Costs":
                                [
                                  {
                                    "relatedProductName" : "BT Product Line Features",
                                    "relatedProductAttribute" : "Total Feature Costs" 
                                  }
                                ]
                              },
                              {
                                "Calling Feature Product Name":
                                [
                                  {
                                    "relatedProductName" : "BT Product Line Features",
                                    "relatedProductAttribute" : "Calling Feature Product Name" 
                                  }
                                ]
                              },
                              {
                                "Quantity":
                                [
                                  {
                                    "relatedProductName" : "BT Product Line Features",
                                    "relatedProductAttribute" : "Quantity" 
                                  }
                                ]
                              },
                              {
                                "Feature Line Type":
                                [
                                  {
                                    "relatedProductName" : "BT Product Line Features",
                                    "relatedProductAttribute" : "Feature Line Type" 
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Fixed Broadband",
                            "relatedProducts": [ "Fixed User Group" ],
                            "mappings" :
                            [
                              {
                                "Total Monthly Charge":
                                [
                                  {
                                    "relatedProductName" : "Fixed User Group",
                                    "relatedProductAttribute" : "Total Monthly Charge"  
                                  } 
                                ]
                              },
                              {
                                "Total Setup Charge":
                                [
                                  {
                                    "relatedProductName" : "Fixed User Group",
                                    "relatedProductAttribute" : "Total Setup Charge"  
                                  } 
                                ]
                              },
                              {
                                "Number of Users":
                                [
                                  {
                                    "relatedProductName" : "Fixed User Group",
                                    "relatedProductAttribute" : "Number of Users"  
                                  } 
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Fixed Line",
                            "relatedProducts": [ "Lines and Channels" ],
                            "mappings" :
                            [
                              {
                                "Number of Lines":
                                [
                                  {
                                    "relatedProductName" : "Lines and Channels",
                                    "relatedProductAttribute" : "Number of Lines" 
                                  }
                                ]
                              },
                              {
                                "Number of Channels":
                                [
                                  {
                                    "relatedProductName" : "Lines and Channels",
                                    "relatedProductAttribute" : "Number of Channels" 
                                  }
                                ]
                              },
                              {
                                "Tot Rec Chg":
                                [
                                  {
                                    "relatedProductName" : "Lines and Channels",
                                    "relatedProductAttribute" : "Tot Rec Chg" 
                                  }
                                ]
                              },
                              {
                                "Tot Rec Cost":
                                [
                                  {
                                    "relatedProductName" : "Lines and Channels",
                                    "relatedProductAttribute" : "Tot Rec Cost" 
                                  }
                                ]
                              },
                              {
                                "Tot OneOff Chg":
                                [
                                  {
                                    "relatedProductName" : "Lines and Channels",
                                    "relatedProductAttribute" : "Tot OneOff Chg" 
                                  }
                                ]
                              },
                              {
                                "Tot OneOff Cost":
                                [
                                  {
                                    "relatedProductName" : "Lines and Channels",
                                    "relatedProductAttribute" : "Tot OneOff Cost" 
                                  }
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Lone Worker",
                            "relatedProducts": [ "Lone Worker User Group" ],
                            "mappings" :
                            [
                              {
                                "Number of users": 
                                [
                                  {
                                    "relatedProductName" : "Lone Worker User Group",
                                    "relatedProductAttribute" : "Number of users"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "M2M",
                            "relatedProducts": [ "M2M Bundle" ],
                            "mappings" :
                            [
                              {
                                "Additional SIM Orders": 
                                [
                                  {
                                    "relatedProductName" : "M2M Bundle",
                                    "relatedProductAttribute" : "Additional SIM Orders"  
                                  }      
                                ]
                              },
                              {
                                "Initial SIM Orders": 
                                [
                                  {
                                    "relatedProductName" : "M2M Bundle",
                                    "relatedProductAttribute" : "Initial SIM Orders"  
                                  }      
                                ]
                              },
                              {
                                "Number Required": 
                                [
                                  {
                                    "relatedProductName" : "M2M Bundle",
                                    "relatedProductAttribute" : "Number Required"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Mobile Broadband",
                            "relatedProducts": [ "Mobile Broadband User Group" ],
                            "mappings" :
                            [
                              {
                                "Number of users": 
                                [
                                  {
                                    "relatedProductName" : "Mobile Broadband User Group",
                                    "relatedProductAttribute" : "Number of users"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Mobile Broadband and Tablet",
                            "relatedProducts": [ "User Group SME" ],
                            "mappings" :
                            [
                              {
                                "Total Recurring Charges": 
                                [
                                  {
                                    "relatedProductName" : "User Group SME",
                                    "relatedProductAttribute" : "Total Recurring Charges"  
                                  }      
                                ]
                              },
                              {
                                "Total Device Charges": 
                                [
                                  {
                                    "relatedProductName" : "User Group SME",
                                    "relatedProductAttribute" : "Total Device Charges"  
                                  }      
                                ]
                              },
                              {
                                "Number of users": 
                                [
                                  {
                                    "relatedProductName" : "User Group SME",
                                    "relatedProductAttribute" : "Number of users"  
                                  }      
                                ]
                              },
                              {
                                "Total Investment": 
                                [
                                  {
                                    "relatedProductName" : "User Group SME",
                                    "relatedProductAttribute" : "Total Investment"  
                                  }      
                                ]
                              },
                              {
                                "Total Contribution": 
                                [
                                  {
                                    "relatedProductName" : "User Group SME",
                                    "relatedProductAttribute" : "Total Contribution"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Mobile Data for Tablets",
                            "relatedProducts": [ "Tablet Bundles" ],
                            "mappings" :
                            [
                              {
                                "Number Required": 
                                [
                                  {
                                    "relatedProductName" : "Tablet Bundles",
                                    "relatedProductAttribute" : "Number Required"  
                                  }      
                                ]
                              },
                              {
                                "Monthly Recurring Charge": 
                                [
                                  {
                                    "relatedProductName" : "Tablet Bundles",
                                    "relatedProductAttribute" : "Monthly Recurring Charge"  
                                  }      
                                ]
                              },
                              {
                                "Total Cost": 
                                [
                                  {
                                    "relatedProductName" : "Tablet Bundles",
                                    "relatedProductAttribute" : "Total Cost"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Security",
                            "relatedProducts": [ "Security User Group" ],
                            "mappings" :
                            [
                              {
                                "Total Recurring Charge": 
                                [
                                  {
                                    "relatedProductName" : "Security User Group",
                                    "relatedProductAttribute" : "Total Recurring Charge"  
                                  }      
                                ]
                              },
                              {
                                "Total One Off Charge": 
                                [
                                  {
                                    "relatedProductName" : "Security User Group",
                                    "relatedProductAttribute" : "Total One Off Charge"  
                                  }      
                                ]
                              },
                              {
                                "Total Number of Users": 
                                [
                                  {
                                    "relatedProductName" : "Security User Group",
                                    "relatedProductAttribute" : "Total Number of Users"  
                                  }      
                                ]
                              },
                              {
                                "Total Recurring Cost": 
                                [
                                  {
                                    "relatedProductName" : "Security User Group",
                                    "relatedProductAttribute" : "Total Recurring Cost"  
                                  }      
                                ]
                              },
                              {
                                "Total One Off Cost": 
                                [
                                  {
                                    "relatedProductName" : "Security User Group",
                                    "relatedProductAttribute" : "Total One Off Cost"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "User Group SME",
                            "relatedProducts": [ "Single Add On SME" ],
                            "mappings" :
                            [
                              {
                                "Total Recurring Charges": 
                                [
                                  {
                                    "relatedProductName" : "Single Add On SME",
                                    "relatedProductAttribute" : "Total Recurring Charges"  
                                  }      
                                ]
                              },
                              {
                                "Recurring Charge": 
                                [
                                  {
                                    "relatedProductName" : "Single Add On SME",
                                    "relatedProductAttribute" : "Recurring Charge"  
                                  }      
                                ]
                              }
                            ]
                          },
                          {
                            "definitionName" : "Mobile Voice",
                            "relatedProducts": [ "User Group", "Shared Add On" ],
                            "mappings" :
                            [
                              {
                                "IDD Voice Usage": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "IDD Voice Usage"  
                                  }      
                                ]
                              },
                              {
                                "IDD Voice Cost": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "IDD Voice Cost"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Data Usage": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Roaming Data Usage"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Data Cost": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Roaming Data Cost"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Voice Usage": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Roaming Voice Usage"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Voice Cost": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Roaming Voice Cost"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Total Usage": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Roaming Total Usage"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Total Cost": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Roaming Total Cost"  
                                  }      
                                ]
                              },
                              {
                                "Total Non roaming Usage": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Total Non roaming Usage"  
                                  }      
                                ]
                              },
                              {
                                "Total Non Roaming Cost": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Total Non Roaming Cost"  
                                  }      
                                ]
                              },
                              {
                                "Type": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Type"  
                                  }      
                                ]
                              },
                              {
                                "Number of users": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Number of users"  
                                  }      
                                ]
                              },
                              {
                                "OOB": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "OOB"  
                                  }      
                                ]
                              },
                              {
                                "OOB Cost": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "OOB Cost"  
                                  }      
                                ]
                              },
                              {
                                "Tot Rec Chg": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Tot Rec Chg"  
                                  },
                                  {
                                    "relatedProductName" : "Shared Add On",
                                    "relatedProductAttribute" : "Tot Rec Chg"  
                                  }      
                                ]
                              },
                              {
                                "Single Roaming": 
                                [
                                  {
                                    "relatedProductName" : "User Group",
                                    "relatedProductAttribute" : "Single Roaming"  
                                  }      
                                ]
                              },
                              {
                                "Value to customer": 
                                [
                                  {
                                    "relatedProductName" : "Shared Add On",
                                    "relatedProductAttribute" : "Value to customer"  
                                  }      
                                ]
                              },
                              {
                                "Profit Boost": 
                                [
                                  {
                                    "relatedProductName" : "Shared Add On",
                                    "relatedProductAttribute" : "Profit Boost"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Type": 
                                [
                                  {
                                    "relatedProductName" : "Shared Add On",
                                    "relatedProductAttribute" : "Roaming Type"  
                                  }      
                                ]
                              },
                              {
                                "Shared Type": 
                                [
                                  {
                                    "relatedProductName" : "Shared Add On",
                                    "relatedProductAttribute" : "Shared Type"  
                                  }      
                                ]
                              },
                              {
                                "Shared Roaming": 
                                [
                                  {
                                    "relatedProductName" : "Shared Add On",
                                    "relatedProductAttribute" : "Shared Roaming"  
                                  }      
                                ]
                              } 
                            ]
                          },
                          {
                            "definitionName" : "User Group",
                            "relatedProducts": [ "Single Add On" ],
                            "nested": true,
                            "parent": "Mobile Voice",
                            "mappings" :
                            [
                              {
                                "Type": 
                                [
                                  {
                                    "relatedProductName" : "Single Add On",
                                    "relatedProductAttribute" : "Type"  
                                  }      
                                ]
                              },
                              {
                                "FCC": 
                                [
                                  {
                                    "relatedProductName" : "Single Add On",
                                    "relatedProductAttribute" : "FCC"  
                                  }      
                                ]
                              },
                              {
                                "Value to customer": 
                                [
                                  {
                                    "relatedProductName" : "Single Add On",
                                    "relatedProductAttribute" : "Value to customer"  
                                  }      
                                ]
                              },
                              {
                                "Roaming Type": 
                                [
                                  {
                                    "relatedProductName" : "Single Add On",
                                    "relatedProductAttribute" : "Roaming Type"  
                                  }      
                                ]
                              },
                              {
                                "Single Roaming": 
                                [
                                  {
                                    "relatedProductName" : "Single Add On",
                                    "relatedProductAttribute" : "Single Roaming"  
                                  }      
                                ]
                              }
                            ]
                          }
];

  function getDefinition(definitionName){
    return RelatedProductMap.filter(function(item){ return item.definitionName === definitionName; })[0];
  }

  return {
    getDefinition: getDefinition
  }
};

var RP = RP || {};
RP.Map = Map();