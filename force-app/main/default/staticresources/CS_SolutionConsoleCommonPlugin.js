// CS_SolutionConsoleCommonPlugin
globalAddonDeliveryMap={};
//Useful function to update configuration attribute value
function updateConfigurationAttributeValue(attrName, attrValue, attrDisplayValue, readOnly, solution, configurationGUID, skipHooks) {
    let updateData;
    if(readOnly==null) updateData = [{name : attrName, value : attrValue, displayValue : attrDisplayValue}];
    else updateData = [{name : attrName, value : attrValue, displayValue : attrDisplayValue, readOnly : readOnly}];
    console.log('updateConfigurationAttributeValue',updateData);
    solution.updateConfigurationAttribute(configurationGUID, updateData, skipHooks);
}


//Useful function to update dependent picklist values
function updateDependentPicklistValues(attributeName, attributeValue, item, solution, configuration, skipHooks,unchangedValue) {
    console.log('updateDependentPicklistValues',item);
    console.log('attributeName',attributeName);
    if (item.attributeName == attributeName){
        let dependentAttName = item.dependentAttName;
	    let pickListOption =[];
	    console.log(attributeValue=='');
	    console.log(attributeValue=='Please select');
	    if((attributeValue=='' || attributeValue=='Please select') && attributeName=='Access New/Existing/OTT')  {
			pickListOption.push('');
	    }
	    else {
	        console.log('in else');
	        for(dependentAttItem of Object.values(item.dependentAttArray)) {
		        let addItem = true;
				dependentAttItem.whereClause.forEach((clause) => {
					if (clause.attributeValue != configuration.getAttribute(clause.attributeName).value) {
						addItem = false;
					}
				});
				if (addItem) {
				    pickListOption.push(dependentAttItem.dependentAttValues);
				}
			}
	    }
        if(pickListOption.length ==0){
            pickListOption.push('');
        }
	    console.log('pickListOption',pickListOption);
	    
		let updateData;
        var currentValue = configuration.getAttribute(item.dependentAttName).value;
        
        if (currentValue=='Please select') {
		    updateData = [{name : item.dependentAttName, value : '', displayValue : '', options : pickListOption}];
        }
        else {
		    updateData = [{name : item.dependentAttName, value : 'Please select', displayValue : 'Please select', options : pickListOption}];
        }        
        if(unchangedValue!='') {
		    updateData = [{name : item.dependentAttName, value : unchangedValue, displayValue : unchangedValue, options : pickListOption}];
        }
	    console.log('updateData',updateData);
        solution.updateConfigurationAttribute(configuration.guid, updateData, false);
        return pickListOption;
	}
}


//Useful function to check minimum length in the expression
function minMaxLengthRegex(attrValue, minimumLength, maximumLength) {
    let regexStr = new RegExp('^[a-zA-Z]([0-9a-zA-Z]{'+(minimumLength-1)+','+(maximumLength-1)+'})');
    if (!regexStr.test(attrValue)) {
        errorMessage = 'Value cannot be less than '+minimumLength+' characters and more than ' +maximumLength+ ' characters and must start with a letter'  ;
        return {error : true, messageDisplay : errorMessage};
    } else {
        return {error : false, message : ''};
    }
}
//Useful function to set commercial product from Salesforce objects
async function updateCommercialProductLookup(grouping, module, contractTerm, attrName, configuration, solution) {
    console.log('updateCommercialProductLookup');
    let inputMap = {};
    inputMap["module"] = module;
    inputMap["grouping"] = grouping;
    inputMap["contractTerm"] = contractTerm;
    inputMap["action"] = 'CS_LookupQueryForSolutionConsole';

    console.log('Update commercial product',grouping, module, contractTerm, attrName, configuration);
    
    currentBasket = await CS.SM.getActiveBasket();
    var result= await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
    console.log('result is', result["PriceItemId"], result["PriceItemName"]);
    updateConfigurationAttributeValue(attrName, result["PriceItemId"], result["PriceItemName"], true, solution, configuration.guid, true);
}

async function getAddOnDetails(component,configuration){
    console.log('getAddOnDetails,configuration:',configuration);
    console.log('configuration.relatedProductList:',configuration.relatedProductList);
     let addOnList = [];
    //let solution= CS.SM.getActiveSolution();
    //let component = solution.getComponentByName('LAN');
    //var configs = component.getConfigurations();
    var relatedProductList = configuration.relatedProductList;
    console.log('relatedProductList velicine ',relatedProductList.length,relatedProductList);
    let allrelatedProductList =  await component.getAddonsForConfiguration(configuration.guid);
    console.log('allrelatedProductList', allrelatedProductList, '');
    if (Object.values(allrelatedProductList)[0].allAddons.length>0) {
    	for(let addOn of Object.values(allrelatedProductList)[0].allAddons){
            let outreKey = addOn.cspmb__Group__c +':'+addOn.cspmb__Add_On_Price_Item__r.Name;
            console.log('outreKey',outreKey);
    		for(let relatedProductInner of relatedProductList){
    		    let innerKey = relatedProductInner.groupName +':'+relatedProductInner.name;
                console.log('innerKey',innerKey);
        		if(outreKey == innerKey){
        		    addOnList.push(addOn.cspmb__Add_On_Price_Item__r);
        		}
    	    }
        }
    }
   return addOnList;
}

async function processAddOnComponetFields(component, configuration, relatedProduct, action, fieldName) {
    console.log('processAddOnComponetFields');

    let addOnList = await getAddOnDetails(component,configuration);
    console.log('addOnList::::','addOnList');
    let result = 0;
    if(action == 'SUM'){
        for(let addOn of addOnList){
            if(addOn[fieldName]){
		        result += addOn[fieldName];
            }
		}
    }else if(action == 'COUNT_TRUE'){
        
    }
    
    return result;
}

async function calculateLANaddons(solution,configuration){
    console.log('entering calculateLANaddons');
    var totalPortsReqired;
    var additionalPortsReqired;
    var portsForLAN;
    var lanType;
    var numberOf8PortsAddon = 0;
    var numberOfNon8PortsAddon = 0;
    var portsAvailableThroughRoutre;
    var numberofAllAvailablePortsOnLanAddOns=0;

    for(let component of Object.values(solution.components)) {
        if(component.name=='LAN') {
            for(let attribute of Object.values(configuration.attributes)) {
                if(attribute.name=='Total Ports Required') {
                    totalPortsReqired=Number(attribute.value);
                    if(isNaN(totalPortsReqired)) totalPortsReqired=0;
                }
                if(attribute.name=='Additional Ports') {
                    additionalPortsReqired=Number(attribute.value);
                    if(isNaN(additionalPortsReqired)) additionalPortsReqired=0;
                }
                if(attribute.name=='LAN Type') {
                    lanType=attribute.displayValue;
                }
                if(attribute.name=='Gigabit Switch') {
                    gigabitSwitch=Boolean(attribute.value);
                    if(gigabitSwitch==null) gigabitSwitch = Boolean(false);
                }
            }
            console.log('totalPortsReqired',totalPortsReqired);
            console.log('additionalPortsReqired',additionalPortsReqired);
            console.log('lanType',lanType);
            console.log('gigabitSwitch',gigabitSwitch);
            portsForLAN=totalPortsReqired+additionalPortsReqired;

            if(lanType === 'BT LAN' && portsForLAN >0){
                if(configuration.relatedProductList.length >0){
                    for(let existingAddOn of configuration.relatedProductList){
                        await deleteAddOns(component,configuration,existingAddOn.guid,false);
            		}
                 }
                let allAddOntList =  await component.getAddonsForConfiguration(configuration.guid);
                let allAddOn = Object.values(allAddOntList)[0].allAddons.sort((a,b) => (a.cspmb__Add_On_Price_Item__r.Available_Ports__c > b.cspmb__Add_On_Price_Item__r.Available_Ports__c) ? 1 : ((b.cspmb__Add_On_Price_Item__r.Available_Ports__c > a.cspmb__Add_On_Price_Item__r.Available_Ports__c) ? -1 : 0));
                let filterredAddOns ={};
                for(let addOn of allAddOn){
                    if(addOn.cspmb__Add_On_Price_Item__r.Available_Ports__c>0 && addOn.cspmb__Group__c == 'BT LAN' && addOn.cspmb__Add_On_Price_Item__r.Gigabit_Switch__c == gigabitSwitch){
                        filterredAddOns[addOn.cspmb__Add_On_Price_Item__r.Available_Ports__c]=addOn;
                    }
                }
                 
                console.log(filterredAddOns);
                console.log(portsForLAN);
                if(portsForLAN>0 && portsForLAN<9) {//8
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[8], configuration.guid,1, component);
                 numberOf8PortsAddon=1;
                 numberofAllAvailablePortsOnLanAddOns=8;
                }
                if(portsForLAN>8 && portsForLAN<17) {//8+8
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[8], configuration.guid,2, component);
                 numberOf8PortsAddon=2;
                 numberofAllAvailablePortsOnLanAddOns=16;
                }
                if(portsForLAN>16 && portsForLAN<25) {//24
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[24], configuration.guid,1, component);
                 numberOfNon8PortsAddon=1;
                 numberofAllAvailablePortsOnLanAddOns=24;
                }
                if(portsForLAN>24 && portsForLAN<33) {//8+24
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[8], configuration.guid,1, component);
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[24], configuration.guid,1, component);
                 numberOf8PortsAddon=1;
                 numberOfNon8PortsAddon=1;
                 numberofAllAvailablePortsOnLanAddOns=32;
                }
                if(portsForLAN>32 && portsForLAN<49) {//48
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[48], configuration.guid,1, component);
                 numberOfNon8PortsAddon=1;
                 numberofAllAvailablePortsOnLanAddOns=48;
                }
                if(portsForLAN>48 && portsForLAN<57) {//48+8
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[8], configuration.guid,1, component);
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[48], configuration.guid,1, component);
                 numberOf8PortsAddon=1;
                 numberOfNon8PortsAddon=1;
                 numberofAllAvailablePortsOnLanAddOns=56;
                }
                if(portsForLAN>56 && portsForLAN<73) {//48+24
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[24], configuration.guid,1, component);
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[48], configuration.guid,1, component);
                 numberOfNon8PortsAddon=2;
                 numberofAllAvailablePortsOnLanAddOns=72;
                }
                if(portsForLAN>72 && portsForLAN<97) {//48+48
                 await addAddOnProductConfiguration('LAN Switches', filterredAddOns[48], configuration.guid,2, component);
                 numberOfNon8PortsAddon=2;
                 numberofAllAvailablePortsOnLanAddOns=96;
                }
                await addDependentLANAddOns(component,configuration);
            }    
        }
        
        let availablePorts;
        let portsAvailableThroughRoutre = 0;
        for(let hubAttribute of Object.values(configuration.attributes)) {
            if(hubAttribute.name=='Router Used' && hubAttribute.value=='BT Hub 5') {
                portsAvailableThroughRoutre = 4;
            }
        }
        availablePorts = (numberofAllAvailablePortsOnLanAddOns + portsAvailableThroughRoutre) - totalPortsReqired;
        if(numberOfNon8PortsAddon >0){
            availablePorts -= (numberOfNon8PortsAddon*2) - 1;
        }
        availablePorts -= additionalPortsReqired;
        if(isNaN(availablePorts)) availablePorts=0;
        updateConfigurationAttributeValue('Available Ports',availablePorts,availablePorts,true,solution,configuration.guid,true);
    }
}

async function getAddONByGroupName(component, configuration,groupName){
    console.log('getAddONByGroupName');
    let resultAddON =[];
    let allRelatedProductList =  await component.getAddonsForConfiguration(configuration.guid);
    if (Object.keys(allRelatedProductList).length > 0) {
        if(Object.values(allRelatedProductList)[0].allAddons.length>0) {
    	    for(let addOn of Object.values(allRelatedProductList)[0].allAddons){
    	        if(addOn.cspmb__Group__c === groupName){
    	            resultAddON.push(addOn);
    	        }
            }
        }
    }
    return resultAddON;
}
async function getExistingRelatedProductByGroupName(configuration,groupName){
    console.log('getExistingRelatedProductByGroupName');
    let resultRelatedProductON =[];
    let relatedProductList = configuration.relatedProductList;
    if(Object.values(relatedProductList).length >0){
        for(let relatedProduct of Object.values(relatedProductList)){
    	    if(relatedProduct.groupName  === groupName){
    		    resultRelatedProductON.push(relatedProduct);
    		}
        }
    }
    return resultRelatedProductON;
}

async function getAllAddOns(component, configuration) {
    console.log('getAllAddOns');
    let resultAddON =[];
    let allRelatedProductList =  await component.getAddonsForConfiguration(configuration.guid);
    if(Object.values(allRelatedProductList).length>0 && Object.values(allRelatedProductList)[0].allAddons.length>0) {
    	for(let addOn of Object.values(allRelatedProductList)[0].allAddons){
    	    console.log('addOn::', addOn, addOn.Name);
    	    resultAddON.push(addOn);
    	}
    }
    return resultAddON;
}

function validateLANAddOns(component, configuration, relatedProduct){
   console.log('validateLANAddOns');
   let numberOfSwitchAddOn =0 ; 
   let numberofInstallingAddOn =0;
   let numberOfPeripheralAddOn =0 ;
   let numberOfPSUAddOn =0 ;
   let numberOfAdditionalAddOn = 0;
   let validationFailed = false;
   let relatedProductList = configuration.relatedProductList;
    
   for(let relatedProduct of Object.values(relatedProductList)){
       console.log('relatedProduct.groupName', relatedProduct.groupName, '');
       Object.values(relatedProduct.configuration.attributes).forEach(attribute => {
            if (attribute.name=='Quantity') {
                if(relatedProduct.groupName === 'BT LAN'){
        		     numberOfSwitchAddOn += attribute.value;
        		}else if(relatedProduct.groupName === 'Installation'){
        		     numberofInstallingAddOn += attribute.value;
        		}else if(relatedProduct.groupName === 'Peripheral'){
        		    numberOfPeripheralAddOn+= attribute.value;//Additional LAN
        		}else if(relatedProduct.groupName === 'Additional LAN'){
        		    numberOfAdditionalAddOn+= attribute.value;//
        		}else if(relatedProduct.groupName === 'PSU'){
        		    numberOfPSUAddOn+= attribute.value;
        		}
            }
        });
    }
    
    if ((numberOfSwitchAddOn > 2) || (numberOfAdditionalAddOn >1)) {
        configuration.status = false;
        configuration.statusMessage = 'More than 2x LANs or more than 1x Vulcan can not be added per Site.';
    }
    else { 
        if (configuration.statusMessage == 'More than 2x LANs or more than 1x Vulcan can not be added per Site.'){
            configuration.status = true;
            configuration.errorMessage = null;
        }
    }
}

async function addAttendenceChargesForCabling(component, configuration, relatedProduct){
        console.log('addAttendenceChargesForCabling');

   let numberOfCallPlans =0 ; 
   let numberofSupplyOnly =0;
   let numberofAttendanceCharge =0;
   
   let relatedProductList = configuration.relatedProductList;
    
   for(let relatedProduct of Object.values(relatedProductList)){
       console.log('relatedProduct.groupName', relatedProduct.groupName, '');
       Object.values(relatedProduct.configuration.attributes).forEach(attribute => {
            if (attribute.name=='Quantity') {
                if(relatedProduct.groupName === 'Supply Only'){
        		   numberofSupplyOnly+= attribute.value;
        		}else if(relatedProduct.groupName === 'Cableplan'){
        		    if(relatedProduct.name === 'Attendance Charge'){
        		        numberofAttendanceCharge += attribute.value;
        		    }else{
        		        numberOfCallPlans+= attribute.value;
        		    }
        		}
            }
        });
    }
    if((numberofSupplyOnly >0 || numberOfCallPlans > 0 ) && numberofAttendanceCharge == 0){
        let cableplanAddOn= await getAddONByGroupName(component, configuration,'Cableplan');
        if(cableplanAddOn.length > 0){
            for(let addOn of cableplanAddOn){
                if(addOn.cspmb__Add_On_Price_Item__r.Name == 'Attendance Charge' ){
                    addAddOnProductConfiguration('Cabling Add On', addOn, configuration.guid,1, component);
                    break;
                }
            }
        }
    }
   
}
async function addDependentLANAddOns(component, configuration){
    console.log('addDependentLANAddOns');
    let numberOfSwitchAddOn =0 ; 
    let numberofInstallingAddOn =0;
    let numberOfPeripheralAddOn =0 ;
    let numberOfPSUAddOn =0 ;
    let numberOfAdditionalAddOn = 0;
    let validationFailed = false;
    let relatedProductList = configuration.relatedProductList;
    
    for(let relatedProduct of Object.values(relatedProductList)){
        console.log('relatedProduct.groupName', relatedProduct.groupName, '');
        Object.values(relatedProduct.configuration.attributes).forEach(attribute => {
            if (attribute.name=='Quantity') {
                if(relatedProduct.groupName === 'BT LAN'){
        		    numberOfSwitchAddOn += attribute.value;
        		}else if(relatedProduct.groupName === 'Installation'){
        		    numberofInstallingAddOn += attribute.value;
        		}else if(relatedProduct.groupName === 'Peripheral'){
        		    numberOfPeripheralAddOn+= attribute.value;//Additional LAN
        		}else if(relatedProduct.groupName === 'Additional LAN'){
        		    numberOfAdditionalAddOn+= attribute.value;
        		}else if(relatedProduct.groupName === 'PSU'){
        		    numberOfPSUAddOn+= attribute.value;
        		}
            }
        });
    }
    if(numberOfAdditionalAddOn >0 && numberOfPSUAddOn == 0){
        let psuAddOn= await getAddONByGroupName(component, configuration,'PSU');
        if(psuAddOn.length > 0){
            if(configuration.relatedProductList.length >0){
                for(let existingAddOn of configuration.relatedProductList){
                    if(existingAddOn.groupName === 'PSU'){
                        await deleteAddOns(component,configuration,existingAddOn.guid,false);
                    }
                }
            }
            addAddOnProductConfiguration('LAN Switches', psuAddOn[0], configuration.guid,numberOfAdditionalAddOn, component);
        }
    }
    if(numberOfSwitchAddOn >0 && numberofInstallingAddOn < numberOfSwitchAddOn ){
        let btLanAddOn= await getAddONByGroupName(component, configuration,'Installation');
        if(configuration.relatedProductList.length >0){
            for(let existingAddOn of configuration.relatedProductList){
                console.log('existingAddOn to delete',existingAddOn);
                if(existingAddOn.Name === 'LAN Installation'){
                    await deleteAddOns(component,configuration,existingAddOn.guid,false);
                }
    		}
         }
        for(let addOn of btLanAddOn){
            if(addOn.cspmb__Add_On_Price_Item__r.Name == 'LAN Installation' ){
                console.log('addOn to add',addOn);
                await addAddOnProductConfiguration('LAN Switches', addOn, configuration.guid,(numberOfSwitchAddOn-numberofInstallingAddOn), component);
                break;
            }
        }
    }
    
}
//LPDE-460 new
async function calculateAndAddDeliveryAddOn(solution){
    var start = new Date();
    console.log('calculateAndAddDeliveryAddOn started '+start.getMinutes()+' : '+start.getSeconds());
    var siteQuantityMap= {};
    var currentSite;
    var currentQuantity;
    var currentDelivery;

    for(let solComponent of Object.values(solution.components)) {
        if(solComponent.name=='CPEs' || solComponent.name=='LAN') {
            for(let configuration of Object.values(solComponent.schema.configurations)) {
                currentSite='';
                currentQuantity='';
                currentDelivery='';

                for(let attribute of Object.values(configuration.attributes)) {
                    if(attribute.name=='Site Access') {
                        currentSite=attribute.value;
                    } 
                    if(attribute.name=='Quantity') {
                        currentQuantity=Number(attribute.value);
                        if (isNaN(currentQuantity)) currentQuantity=0;
                    }
                    if(attribute.name=='Delivery Charge Required') {
                        currentDelivery=Boolean(attribute.value);
                    }
                }
                if (currentDelivery==false) currentQuantity=0;
                if(siteQuantityMap[currentSite]!=null) siteQuantityMap[currentSite]+=currentQuantity;
                else siteQuantityMap[currentSite]=currentQuantity;
               
                //before querying addons, check if maybe we already have all the info
                var needToQuery=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    var addonID;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            addonID=attribute.value;
                            break;
                        }
                    }
                    if(globalAddonDeliveryMap[addonID]==null) {
                        needToQuery=true;
                        break;
                    }
                }
                if(needToQuery) {
                    //retrieve config addons and fill id->delivery map
                    try {
                        currentAddons = await solComponent.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(currentAddons) ) {
                            segment.allAddons.forEach(oneAddon => {
                                globalAddonDeliveryMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Delivery_Charge_Required__c;
                            });
                        }
                    } catch (error) {}
                }

                //retrieve config addons and fill id->delivery map
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    currentQuantity=0;
                    currentDelivery=''
                    for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Quantity') {
                            currentQuantity=Number(attribute.value);
                        }
                        if (attribute.name=='Add On ID') {
                            currentDelivery=Boolean(globalAddonDeliveryMap[attribute.value]);
                        }
                    }
                    if (currentDelivery==false || isNaN(currentQuantity)) currentQuantity=0;
                    if(siteQuantityMap[currentSite]!=null) siteQuantityMap[currentSite]+=currentQuantity;
                    else siteQuantityMap[currentSite]=currentQuantity;
                }
            }
        }
    }    
    for(let solComponent of Object.values(solution.components)) {
        if(solComponent.name=='Site') {
            for(let configuration of Object.values(solComponent.schema.configurations)) {
            	updateConfigurationAttributeValue('Total Delivery Required',siteQuantityMap[configuration.guid],siteQuantityMap[configuration.guid],true,solution,configuration.guid,false);	
            	var deliveryBracket=0;
            	var bracketOne=0;
            	var bracketTwo=0;
            	var bracketThree=0;
                if(siteQuantityMap[configuration.guid]>0 && siteQuantityMap[configuration.guid]<=10) deliveryBracket=1;
                if(siteQuantityMap[configuration.guid]>10 && siteQuantityMap[configuration.guid]<=100) deliveryBracket=2;
                if(siteQuantityMap[configuration.guid]>100) deliveryBracket=3;
                
            	//before delete then add, check if you need to do that
            	for(let relatedProduct of Object.values(configuration.relatedProductList)) {
            	    if (relatedProduct.groupName=='Delivery') {
            	        if(relatedProduct.name=='CPE Delivery 1-10') bracketOne+=1;
            	        if(relatedProduct.name=='CPE Delivery 11-100') bracketTwo+=1;
            	        if(relatedProduct.name=='CPE Delivery 100+') bracketThree+=1;
            	    }
            	}
            	for(let relatedProduct of Object.values(configuration.relatedProductList)) {
            	    if (relatedProduct.groupName=='Delivery') {
            	        if((relatedProduct.name=='CPE Delivery 1-10' && bracketOne!=1) || (relatedProduct.name=='CPE Delivery 11-100' && bracketTwo!=1)
            	        || (relatedProduct.name=='CPE Delivery 100+' && bracketThree!=1) || (deliveryBracket!=1 && relatedProduct.name=='CPE Delivery 1-10')
            	        || (deliveryBracket!=2 && relatedProduct.name=='CPE Delivery 11-100') || (deliveryBracket!=3 && relatedProduct.name=='CPE Delivery 100+')) {
            	            await deleteAddOns(solComponent,configuration,relatedProduct.guid,true);
            	        }
            	    }
            	}
            	
            	
            	if( (deliveryBracket==1 && bracketOne!=1) || (deliveryBracket==2 && bracketTwo!=1) || (deliveryBracket==3 && bracketThree!=1) ) {
                	let DeliveryAddOns=await solComponent.getAddonsForConfiguration(configuration.guid);
                	for(let segment of Object.values(DeliveryAddOns)) {
                        for(let oneAddon of segment.allAddons) {
                            if(deliveryBracket==1) {
                                if(oneAddon.cspmb__Add_On_Price_Item__r.Name == 'CPE Delivery 1-10' ){
                                    await addAddOnProductConfiguration('Site Access Add-Ons', oneAddon, configuration.guid, 1, solComponent);
                                }
                            }
                            if(deliveryBracket==2) {
                                if(oneAddon.cspmb__Add_On_Price_Item__r.Name == 'CPE Delivery 11-100' ){
                                    await addAddOnProductConfiguration('Site Access Add-Ons', oneAddon, configuration.guid, 1, solComponent);
                                }
                            }
                            if(deliveryBracket==3) {
                                if(oneAddon.cspmb__Add_On_Price_Item__r.Name == 'CPE Delivery 100+' ){
                                    await addAddOnProductConfiguration('Site Access Add-Ons', oneAddon, configuration.guid, 1, solComponent);
                                }
                            }
                        }
                        break;
                	} 
            	}
            }
        }
    }
    var finish = new Date();
    console.log('calculateAndAddDeliveryAddOn finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

function enableQOSOrPOE(solution,component,configuration,attributeDV){
    console.log('enableQOSOrPOE');
    var qosReadonly=Boolean(attributeDV!='Existing / 3rd Party' && attributeDV!='Router Only');
    var poeReadonly=Boolean(attributeDV!='Existing / 3rd Party');

    let qosData;
    if(qosReadonly) {
        qosData = [{name : 'Supports QoS', value : ' ', displayValue : ' ', readOnly : qosReadonly}];
    }
    else {
        qosData = [{name : 'Supports QoS', value : 'Please Select', displayValue : 'Please Select', readOnly : qosReadonly}];
    }
    solution.updateConfigurationAttribute(configuration.guid, qosData, true);
 
    let poeData;
    if(poeReadonly) {
        poeData= [{name : 'Supports PoE', value : ' ', displayValue : ' ', readOnly : poeReadonly}];
    }
    else {
        poeData= [{name : 'Supports PoE', value : 'Please Select', displayValue : 'Please Select', readOnly : poeReadonly}];
    }
    solution.updateConfigurationAttribute(configuration.guid, poeData, false);
}

function validateQOSorPOE(configuration){
    SupportPoE = configuration.getAttribute('Supports PoE').value;
    SupportsQoS = configuration.getAttribute('Supports QoS').value;
    LANtype = configuration.getAttribute('LAN Type').displayValue;
    
    if ( (SupportPoE=='' || SupportsQoS == '' || SupportPoE=='Please Select' || SupportsQoS=='Please Select') && LANtype=='Existing / 3rd Party') {
        configuration.status = false;
        configuration.statusMessage = 'Supports PoE and Supports QoS are required if Lan type is Existing / 3rd Party';
    }
    else {
        if (configuration.statusMessage == 'Supports PoE and Supports QoS are required if Lan type is Existing / 3rd Party'){
            configuration.status = true;
            configuration.errorMessage = null;
        }
    }
}


async function deleteAddOns(component,configuration, addonConfigurationGuid, skipHooks){
   console.log('deleteAddOns',configuration,addonConfigurationGuid);
   await component.deleteAddon(configuration.guid, addonConfigurationGuid, skipHooks);
}

async function addAddOnProductConfiguration(relatedProductName, addOn, configurationGUID, quantity,component) {
    let addOnData = {
        'relatedProductName' : relatedProductName,
        'associationId' : addOn.Id,
        'addonPriceItemId' : addOn.cspmb__Add_On_Price_Item__c,
        'name' : addOn.cspmb__Add_On_Price_Item__r.Name,
        'quantity' : quantity,
        'group' : addOn.cspmb__Group__c,
        'raw' : addOn
    };
    console.log('addAddOnProductConfiguration',addOnData);
    await component.addAddOn(configurationGUID, addOnData);
}