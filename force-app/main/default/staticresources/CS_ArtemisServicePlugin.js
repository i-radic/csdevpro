var imported = document.createElement('script');
imported.src = '/resource/1491499237000/cscfga__jQuery_min';
document.head.appendChild(imported);

var globalSPAddonMapArtemis={};
var globalUGAddonMapArtemis={};
var globalOneOffCostMapArtemis={};
var globalRecurringCostMapArtemis={};

var usageProfiledata = {};
var correctProfile = null;
var globalAccountID='';

var customCallerAddOnArray = {'UK Landlines':'FMUKLandlines','Calls to EE':'FMCallstoEE','Calls to other UK mobile networks':'FMCallstootherUKmobilenetworks','IDD Zone A':'FMIDDSMSZoneA','IDD Zone B':'FMIDDSMSZoneB','IDD Zone C':'FMIDDSMSZoneC','Non-Geographic Calls':'FMNonGeographicCalls','SMS to EE':'FMSMStoEE','SMS to other network':'FMSMStoothernetwork','Voice calls to Zone A':'FMVoicecallstoZoneA','Voice calls to Zone B':'FMVoicecallstoZoneB','Voice calls to Zone C':'FMVoicecallstoZoneC','Voice calls back to UK from Zone A':'FMVoicecallsbacktoUKfromZoneA','Voice calls back to UK from Zone B':'FMVoicecallsbacktoUKfromZoneB','Voice calls back to UK from Zone C':'FMVoicecallsbacktoUKfromZoneC','Receiving call in Zone A':'FMReceivingcallinZoneA','Receiving call in Zone B':'FMReceivingcallinZoneB','Receiving call in Zone C':'FMReceivingcallinZoneC','MMS Messaging':'FMMMSMessaging'};

console.log('------------>Loaded default plugin for Artemis Service');

if(!CS || !CS.SM){
    throw Error('Artemis Solution Console Api not loaded?');
}

if (!CS || !CS.SM) {
    console.log('not loaded');
    throw Error('Artemis Solution Console Api not loaded?');
}

// Create a mapping to match your solution's components
// This means we can avoid too much hard-coding below and can more easily reuse code
var ARTEMIS_CONST = {
    solution: 'Artemis Service', //Main SCHEMA name for plugin creation
    mainComponent: 'Artemis',
    userGroup: 'User Group',//Component name for use in functions//ARTEMIS_CONST.userGroup
    special: 'Special Conditions'
};

//Register the Artemis Plugin
if (CS.SM.registerPlugin) {
    window.document.addEventListener('SolutionConsoleReady', async function() {
        console.log('SolutionConsoleReady Event Listener');
        await CS.SM.registerPlugin(ARTEMIS_CONST.solution)
            .then(plugin => {
                console.log("--------->Plugin registered for Artemis Service");
                artemisHooks(plugin);
            });
    });
}


var artemisSections = {
    'UK Voice Plan' : 'UK Voice',
    'UK Data Plan' : 'UK Data',
    'Roaming Solution Type (Voice)' : 'ROW Voice',
    'Roaming Solution Type (Data)' : 'ROW Data'
}

var artemisMainSections = {
    'UK Voice' : 'Voice Service',
    'UK Data' : 'Data Service'
}

loadUsageData = async() => {
    var solution = await CS.SM.getActiveSolution();
    var artemisServiceConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    let urlString = displayUsageProfileArtemis('', artemisServiceConfiguration.guid);
    console.log('urlString in loadUsageData : ' + urlString);
    try {
        let divContainer = document.createElement('div');
        divContainer.innerHTML = urlString;
        document.body.append(divContainer);
    } catch (err) {
        CS.SM.displayMessage('Error loading usage data page!', 'error');
    }
}

renameCustomAttributeEditLinks = async(currentComponentName) => {
    if (currentComponentName !== ARTEMIS_CONST.mainComponent) {
        return;
    }
    let csButtons = document.querySelectorAll('button:only-child');
    main_loop:
    for (let i = 0 ; i< csButtons.length; i++) {
    	if (csButtons[i].innerText === 'Edit') {
    		let rootDiv = csButtons[i].parentElement.parentElement.parentElement.parentElement.parentElement
    		let divChildNodes = rootDiv.childNodes;
    		let usageRenamed = false;
    		let bespokeRenamed = false;
    		let futureTiersRenamed = false;
    		for (let j = 0 ; j < divChildNodes.length; j++) {
    			if (divChildNodes[j].innerText === 'Load/Refresh Usage Data') {
    			    csButtons[i].innerHTML = 'Load';
    				usageRenamed = true;
    			}
    			if (divChildNodes[j].innerText === 'Bespoke Voice & Data (CADM)') {
    				csButtons[i].innerHTML = 'Load';
    				bespokeRenamed = true;
    			}
    			if (divChildNodes[j].innerText === 'View Future Tiers') {
    				csButtons[i].innerHTML = 'View';
    				futureTiersRenamed = true;
    			}
    			if (usageRenamed === true && bespokeRenamed === true && futureTiersRenamed === true) {
    			    break main_loop;
    			}
    		}
    	}
    }
}

subscribeToExpandButtonEvents = async(currentComponentName) => {
    if (currentComponentName !== ARTEMIS_CONST.mainComponent) {
        return;
    }
    setTimeout(() => {
        let buttons = document.getElementsByClassName('expand-btn');
        if (buttons) {
            for (let i = 0; i < buttons.length; i++) {
               buttons[i].addEventListener("mouseup", (e) => {
               setTimeout(() => {
                        formatPcUI();
                    }, 10);
                });
            }
        }
        let tabs = document.getElementsByClassName('tab-name');
        if (tabs) {
            for (let i = 0; i < tabs.length; i++) {
                if (tabs[i].innerText !== 'Details') {
                    continue;
                }
                tabs[i].addEventListener("mouseup", (e) => {
                setTimeout(() => {
                        formatPcUI();
                    }, 20);
                });
            }
        }

        let configs = document.getElementsByClassName('config-name');
        if (configs) {
            for (let i = 0; i < configs.length; i++) {
                configs[i].addEventListener("mouseup", (e) => {
                setTimeout(() => {
                        formatPcUI();
                    }, 20);
                });
            }
        }

    }, 100);
   

    formatPcUI();
}

formatPcUI = async() => {

    let labels = document.querySelectorAll('label');
    let updatedParentElements = [];
    if (labels) {
        let configuredSections = Object.keys(artemisSections);
        for (let i = 0 ; i< labels.length; i++) {
            if (configuredSections.includes(labels[i].innerText)) {
                let cs = labels[i].parentElement.parentElement.getElementsByClassName('cs-custom-section');
                if (cs && cs.length > 0 && !updatedParentElements.includes(labels[i].parentElement.parentElement)) {
                    updatedParentElements.push(labels[i].parentElement.parentElement);
                }
            }
        }

        for (let i = 0 ; i< labels.length; i++) {
            if (configuredSections.includes(labels[i].innerText)) {
                if (updatedParentElements.includes(labels[i].parentElement.parentElement)) {
                    continue;
                }

                
                let configuredMainSections = Object.keys(artemisMainSections);
                if (configuredMainSections.includes(artemisSections[labels[i].innerText])) {
                    let mainHeader = document.createElement('h3');
                    mainHeader.classList.add('cs-custom-section');
                    mainHeader.classList.add('slds-size_5-of-5');
                    mainHeader.classList.add('modal-header');
                    mainHeader.classList.add('slds-modal__header');
                    mainHeader.classList.add('slds-align_absolute-center');
                    mainHeader.innerText = artemisMainSections[artemisSections[labels[i].innerText]];
                    mainHeader.style.height='15px';
                    mainHeader.style.justifyContent = 'left';
                    mainHeader.style.fontWeight = 'bold';
                    labels[i].parentElement.parentElement.insertBefore(mainHeader,labels[i].parentElement);
                }
                

                let header = document.createElement('h5');
                header.classList.add('cs-custom-section');
                header.classList.add('slds-size_5-of-5');
                header.classList.add('modal-header');
                header.classList.add('slds-align_absolute-center');
                header.innerText = artemisSections[labels[i].innerText];
                header.style.height='15px';
                header.style.marginTop = '10px';
                header.style.marginBottom = '10px';
                header.style.justifyContent = 'left';
                header.style.fontWeight = 'bold';
                header.style.fontSize = '6';
                header.style.paddingLeft = '15px';
                
                labels[i].parentElement.parentElement.insertBefore(header,labels[i].parentElement);
                
                
                let underline = document.createElement('h3');
                underline.classList.add('cs-custom-section');
                underline.classList.add('slds-size_5-of-5');
                
                underline.classList.add('slds-align_absolute-center');
                
                underline.style.height='2px';
                underline.style.backgroundColor = 'lightblue';
                labels[i].parentElement.parentElement.insertBefore(underline,labels[i].parentElement);
            }
        }
    }
    return Promise.resolve(true);
}

function artemisHooks(ArtemisPlugin) {
    var newValue;

    ArtemisPlugin.beforeSave = async function(solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
        console.log('beforeSave');
        return true;
    };
    ArtemisPlugin.afterSave = async function( solutionResult, solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
        console.log('afterSave Artemis', solution, configurationsProcessed, saveOnlyAttachment, configurationGuids);
        setTimeout( ()=>{  
            renameCustomAttributeEditLinks(ARTEMIS_CONST.mainComponent);
            subscribeToExpandButtonEvents(ARTEMIS_CONST.mainComponent);
        },1000);
        return Promise.resolve(true);
    };

    ArtemisPlugin.afterSolutionLoaded = async function(previousSolution, loadedSolution) {
        console.log('AFTER SOL LOADED',loadedSolution);
    };

    ArtemisPlugin.beforeAttributeValidation = function(component, configuration, attribute, value) {
        return Promise.resolve(true);
    };

    ArtemisPlugin.beforeAttributeUpdated = function(component, configuration, attribute, oldValueMap) {
        return Promise.resolve(true);
    };

    ArtemisPlugin.afterAttributeUpdated =async function(component, configuration, attribute, oldValueMap) {
        console.log('ArtemisPlugin.afterAttributeUpdated',component,configuration,attribute,oldValueMap);
        if (component.name == ARTEMIS_CONST.mainComponent && attribute.name == 'Solution Type') {
            calculateAndUpdateTotalSubscriptions();
        }
        if (component.name == ARTEMIS_CONST.mainComponent && attribute.name == '% Usage Increase') {
            updateROWVoiceData();
        }
        if (component.name == ARTEMIS_CONST.mainComponent && attribute.name == '% Increments in Contract') {
            updateROWVoiceData();
        }
        
        if (component.name == ARTEMIS_CONST.mainComponent && attribute.name == 'Roaming Solution Type Voice') {
            setROWVoiceCharges();
        }

        if (component.name == ARTEMIS_CONST.mainComponent && (attribute.name == 'Override Voice Charge' || attribute.name == 'Override Voice Cost')) {
            setROWVoiceCharges();
        }
        if (component.name == ARTEMIS_CONST.mainComponent && attribute.name == 'Roaming Solution Type Data') {
            checkAndValidateROWDataUsage();
            setROWDataCharges();
        }
        if (component.name == ARTEMIS_CONST.mainComponent && (attribute.name == 'Override Data Charge' || attribute.name == 'Override Data Cost')) {
            setROWDataCharges();
        }
        if(component.name == ARTEMIS_CONST.userGroup && attribute.name == 'Subscription Type') {
            calculateAndUpdateTotalSubscriptions();
            updateROWVoiceData();

            solution = await CS.SM.getActiveSolution();
            addVoiceAndDataArtemis(solution, component,configuration,attribute.displayValue);
        }

        if (component.name == ARTEMIS_CONST.mainComponent && (attribute.name == 'Unit Recurring Charge Voice Lookup' || attribute.name == 'Override Monthly Recurring Price Voice')) {
            setUKVoicePrice();
            calculateOverrideVoiceAndDataCharges();
        }
        if (component.name == ARTEMIS_CONST.mainComponent && (attribute.name == 'Unit Recurring Charge Data Lookup' || attribute.name == 'Override Monthly Recurring Price Data')) {
            setUKDataPrice();
            calculateOverrideVoiceAndDataCharges();
        }
        
        return Promise.resolve(true);
    };

    ArtemisPlugin.afterAttributeValidation = function(component, configuration, attribute, validationResult, value) {
        return Promise.resolve(true);
    };

    ArtemisPlugin.beforeConfigurationAdd =async function(component, configuration) {
        return Promise.resolve(true);
    };

    ArtemisPlugin.beforeRelatedProductAdd =async function(component, configuration, relatedProduct) {
        console.log('ArtemisPlugin.beforeRelatedProductAdd', component, configuration, relatedProduct);

        return Promise.resolve(true);
    };
    ArtemisPlugin.afterRelatedProductAdd = async function(component, configuration, relatedProduct) {
        let solution= await CS.SM.getActiveSolution();
        
        if(component.name == ARTEMIS_CONST.userGroup) {
           calculateVoiceAndDataAddOnChargesArtemis(relatedProduct);
        }
        return Promise.resolve(true);
    };

    ArtemisPlugin.beforeRelatedProductDelete = function(component, configuration, relatedProduct) {
        console.log('ArtemisPlugin.beforeRelatedProductDelete', component, configuration, relatedProduct);
        return Promise.resolve(true);
    };

    ArtemisPlugin.afterRelatedProductDelete =async function(component, configuration, relatedProduct) {
        console.log('ArtemisPlugin.afterRelatedProductDelete', component, configuration, relatedProduct);
        let solution = await CS.SM.getActiveSolution();
      
        return Promise.resolve(true);
    };

    ArtemisPlugin.afterConfigurationAdd = async function(component, configuration) {


    }
    
    ArtemisPlugin.afterConfigurationDelete = async function(component, configuration) {
        if(component.name == ARTEMIS_CONST.userGroup) {
            calculateAndUpdateTotalSubscriptions();
            updateROWVoiceData();
        }
        return Promise.resolve(true);
    }

    ArtemisPlugin.afterQuantityUpdated = async function(component, configurationGuid, oldQuantity, newQuantity) {
        console.log('ArtemisPlugin.afterQuantityUpdated ',component, configurationGuid,  oldQuantity, newQuantity);
        let solution = await CS.SM.getActiveSolution();
        let config = solution.getConfiguration(configurationGuid);
        if(component.name == ARTEMIS_CONST.userGroup && oldQuantity != newQuantity) {
            calculateAndUpdateTotalSubscriptions();
        }
        return Promise.resolve(true);
    }
    
}

async function beforeNavigateArtemis(solution,currentComponent, previousComponent) {
    console.log('beforeNavigateArtemis',solution,currentComponent,previousComponent);
    return true;
}

async function afterNavigateArtemis(solution, currentComponent, previousComponent) {
    renameCustomAttributeEditLinks(currentComponent.name);
    subscribeToExpandButtonEvents(currentComponent.name);
    if(currentComponent.name == ARTEMIS_CONST.mainComponent){
        var mainConfiguration = Object.values(solution.schema.configurations)[0];
        if (mainConfiguration.getAttribute('Requires Usage Data Refresh').value == true) {
            mainConfiguration.status = false;
            mainConfiguration.statusMessage = 'Requires Usage Data Refresh';
        } else {
            if(mainConfiguration.statusMessage =='Requires Usage Data Refresh') {
                mainConfiguration.status = true;
                mainConfiguration.errorMessage = null;
            }
        }
    }
    return true;
}

async function calculateAndUpdateTotalSubscriptions() {
    let totalQuantity = 0;
    let voiceQuantity = 0;
    let dataQuantity = 0; 
    let solution = await CS.SM.getActiveSolution();
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    let solutionType = mainConfiguration.getAttribute('Solution Type').displayValue;
    for(let component of Object.values(solution.components) ) {
        if (component.name == ARTEMIS_CONST.userGroup) {
            for(let configuration of Object.values(component.schema.configurations)) {
                var subType = configuration.getAttribute('Subscription Type').displayValue;
                totalQuantity += parseInt(configuration.configurationProperties.quantity);
                if (subType === 'Voice and Data' || subType === 'Voice Only') {
                    voiceQuantity += parseInt(configuration.configurationProperties.quantity);
                }
                if (subType === 'Voice and Data' || subType === 'Data Only') {
                    dataQuantity += parseInt(configuration.configurationProperties.quantity);
                }
            }
        }
    }
    if (totalQuantity < 250 && solutionType === 'Corporate Capped 2') {
        mainConfiguration.status = false;
        mainConfiguration.statusMessage = 'Corporate Capped 2 requires 250 subscriptions or more';
    } else {
        if(mainConfiguration.statusMessage =='Corporate Capped 2 requires 250 subscriptions or more') {
            mainConfiguration.status = true;
            mainConfiguration.errorMessage = null;
        }
    }
    updateConfigurationAttributeValue('Total Voice Subscriptions', voiceQuantity, voiceQuantity, true, solution, mainConfiguration.guid, true);
    updateConfigurationAttributeValue('Total Data Subscriptions', dataQuantity, dataQuantity, true, solution, mainConfiguration.guid, true);
}

async function updateROWVoiceData() {
    let startingFairUsageVoice = 0;
    let minsPerUser = 0;
    let increments = 0;
    let costPerIncrement = 0;
    let solution = await CS.SM.getActiveSolution();
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    let usageIncreasePercentage = mainConfiguration.getAttribute('% Usage Increase').value;
    let rowVoiceUsage = mainConfiguration.getAttribute('ROW Voice Usage').value;
    let incrementsPercentage = mainConfiguration.getAttribute('% Increments in Contract').value;
    let totalVoiceSubscriptions = mainConfiguration.getAttribute('Total Voice Subscriptions').value;
    let rowVoiceCharge = mainConfiguration.getAttribute('ROW Voice Charge (ppm)').value;
    startingFairUsageVoice = ((usageIncreasePercentage / 100 ) + 1 ) * rowVoiceUsage;
    increments = (incrementsPercentage / 100 ) * startingFairUsageVoice;
    increments = increments.toFixed(2);
    startingFairUsageVoice = startingFairUsageVoice.toFixed(2);
    if (totalVoiceSubscriptions != 0) {
        minsPerUser = startingFairUsageVoice / totalVoiceSubscriptions;
        minsPerUser = minsPerUser.toFixed(2);
        updateConfigurationAttributeValue('Mins Per User', minsPerUser, minsPerUser, true, solution, mainConfiguration.guid, true);
    } else if (totalVoiceSubscriptions == 0) {
        updateConfigurationAttributeValue('Mins Per User', minsPerUser, minsPerUser, true, solution, mainConfiguration.guid, true);
    }
    updateConfigurationAttributeValue('Starting Fair Usage Voice', startingFairUsageVoice, startingFairUsageVoice, true, solution, mainConfiguration.guid, true);
    updateConfigurationAttributeValue('Increments (mins)', increments, increments, true, solution, mainConfiguration.guid, true);
    costPerIncrement = (rowVoiceCharge * increments) / 100;
    costPerIncrement = Math.ceil(costPerIncrement).toFixed(2);
    if (isNaN(costPerIncrement) == false) {
        costPerIncrement = '£ ' + costPerIncrement.toString();
        updateConfigurationAttributeValue('Cost Per Increment', costPerIncrement, costPerIncrement, true, solution, mainConfiguration.guid, true);
    }
}

async function setROWVoiceCharges() {
    let defaultROWVoiceCharge = 0;
    let defaultROWVoiceCost = 0;
    let rowVoiceCharge = 0;
    let rowVoiceCost = 0;

    let solution = await CS.SM.getActiveSolution();
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);

    let roamingSolutionTypeVoice = mainConfiguration.getAttribute('Roaming Solution Type Voice').displayValue;

    if (roamingSolutionTypeVoice == 'IDD Only') {
        defaultROWVoiceCharge = 12;
        defaultROWVoiceCost = -1.89;
    }

    if (roamingSolutionTypeVoice == 'Roaming & IDD') {
        let roamingPercentage = usageProfiledata["ROW Roaming Percentage"];
        let iddPercentage = usageProfiledata["IDD Percentage"];

        defaultROWVoiceCharge = (roamingPercentage * 30) + (30 * iddPercentage);
        defaultROWVoiceCost = (roamingPercentage * 19) + (19 * iddPercentage);
    }

    updateConfigurationAttributeValue('Default ROW Voice Charge', defaultROWVoiceCharge, defaultROWVoiceCharge, true, solution, mainConfiguration.guid, true);
    updateConfigurationAttributeValue('Default ROW Voice Cost', defaultROWVoiceCost, defaultROWVoiceCost, true, solution, mainConfiguration.guid, true);

    let overrideVoiceCharge = mainConfiguration.getAttribute('Override Voice Charge').value;
    let overrideVoiceCost = mainConfiguration.getAttribute('Override Voice Cost').value;

    if (overrideVoiceCharge > 0) {
        rowVoiceCharge = overrideVoiceCharge;
    } else {
        rowVoiceCharge = defaultROWVoiceCharge;
    }
    updateConfigurationAttributeValue('ROW Voice Charge (ppm)', rowVoiceCharge, rowVoiceCharge, true, solution, mainConfiguration.guid, true);
    if (overrideVoiceCost > 0) {
        rowVoiceCost = overrideVoiceCost;
    } else {
        rowVoiceCost = defaultROWVoiceCost;
    }
    updateConfigurationAttributeValue('ROW Voice Cost (ppm)', rowVoiceCost, rowVoiceCost, true, solution, mainConfiguration.guid, true);
    return rowVoiceCharge;
}

async function checkAndValidateROWDataUsage() {
    let solution = await CS.SM.getActiveSolution();
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    let roamingSolutionTypeData = mainConfiguration.getAttribute('Roaming Solution Type Data').displayValue;
    let rowDataUsage = mainConfiguration.getAttribute('ROW Data Usage').value;
    if (rowDataUsage < 5 && roamingSolutionTypeData === 'Bespoke') {
        mainConfiguration.status = false;
        mainConfiguration.statusMessage = '5GB or more of ROW data usage is require for Bespoke';
    } else {
        if(mainConfiguration.statusMessage =='5GB or more of ROW data usage is require for Bespoke') {
            mainConfiguration.status = true;
            mainConfiguration.errorMessage = null;
        }
    }
}


async function setROWDataCharges() {
    let solution = await CS.SM.getActiveSolution();
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    let roamingSolutionTypeData = mainConfiguration.getAttribute('Roaming Solution Type Data').displayValue;
    let defaultROWDataCharge = mainConfiguration.getAttribute('Default ROW Data Charge').value;
    let defaultROWDataCost = mainConfiguration.getAttribute('Default ROW Data Cost').value;
    let overrideDataCharge = mainConfiguration.getAttribute('Override Data Charge').value;
    let overrideDataCost = mainConfiguration.getAttribute('Override Data Cost').value;
    if (overrideDataCharge > 0) {
        updateConfigurationAttributeValue('Data Charge Per MB', overrideDataCharge, overrideDataCharge, true, solution, mainConfiguration.guid, true);
    } else {
        updateConfigurationAttributeValue('Data Charge Per MB', defaultROWDataCharge, defaultROWDataCharge, true, solution, mainConfiguration.guid, true);
    }
    if (overrideDataCost > 0) {
        updateConfigurationAttributeValue('Data Cost Per MB', overrideDataCost, overrideDataCost, true, solution, mainConfiguration.guid, true);
    } else {
        updateConfigurationAttributeValue('Data Cost Per MB', defaultROWDataCost, defaultROWDataCost, true, solution, mainConfiguration.guid, true);
    }
}


async function updateUserGroupConfig(solution) {
    var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    var servicePlanName = mainConfig.getAttribute('Service Plan Name').value;
    updateData = [{name : 'Service Plan Name', value : servicePlanName , displayValue : servicePlanName, readOnly : true}];
    solution.updateConfigurationAttribute(mainConfig.guid, updateData, true);

    for(let component of Object.values(solution.components) ) {
        if (component.name== FUTURE_MOBILE_CONST.userGroups) {
            for(let configuration of Object.values(component.schema.configurations)) {
                updateData = [{name : 'Service Plan Name', value : servicePlanName, displayValue : servicePlanName, readOnly : true}];
                component.updateConfigurationAttribute(configuration.guid, updateData, true);
            }
        }
    }
}

async function calculateAndAddUserGroupAddOnForTeam(solution, component,userGroupConfiguration,attributeValue) {
    var start = new Date();
    var serviceplanName = userGroupConfiguration.getAttribute('Service Plan Name').value;
    if(attributeValue == ''){
        attributeValue = userGroupConfiguration.getAttribute('Subscription Type Name').value;
    }
    if(serviceplanName == 'Team' || serviceplanName == 'Custom'){
        console.log('calculateAndAddUserGroupAddOnNew started '+start.getMinutes()+' : '+start.getSeconds());
        //attributeValue=userGroupConfiguration.getAttribute('Subscription Type Name').displayValue;
        if(attributeValue=='Voice Only') attributeValue='Voice';
        if(attributeValue=='Data Only') attributeValue='Data';


        // delete exiting User Plans
        if(userGroupConfiguration.relatedProductList.length > 0){
            for(let relatedProduct of Object.values(userGroupConfiguration.relatedProductList)) {
                if(relatedProduct.relatedProductName == 'User Plans'){
                    if( (!attributeValue.includes('Data') && relatedProduct.groupName.includes('Data')) || (!attributeValue.includes('Voice') && relatedProduct.groupName.includes('Voice')) ) {
                        await deleteAddOns(component, userGroupConfiguration, relatedProduct.guid, true);
                    }
                }
            }
        }
        let servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
        var voicePlan = servicePlanConfiguration.getAttribute('Team Voice Plan').displayValue;
        var dataPlan = servicePlanConfiguration.getAttribute('Team Data Plan').displayValue;

        if(voicePlan == 'N/A' && userGroupConfiguration.getAttribute('Service Plan Name').value == 'Custom'){
            voicePlan = 'Line Rental pupm';
        }
        // add voice plan
        if(voicePlan != '' && (attributeValue == 'Voice' || attributeValue == 'Voice and Data')){
            await addDataAndVoiceAddOn(voicePlan, 'Voice', component,userGroupConfiguration,attributeValue);
        }
        // add voice plan
        if(dataPlan != '' && (attributeValue == 'Data'  || attributeValue == 'Voice and Data')){
            await addDataAndVoiceAddOn(dataPlan, 'Data', component,userGroupConfiguration,attributeValue);
        }
        //var updateData = [{name : 'Show AddOn', value : false, displayValue : false, readOnly : true}];
        //solution.updateConfigurationAttribute(userGroupConfiguration.guid, updateData, true);
    }else if(serviceplanName == 'Individual'){
        await addDataAndVoiceAddOn('Unlimited Minutes & Text', 'Voice', component,userGroupConfiguration,attributeValue);
    }
    var finish = new Date();
    console.log('calculateAndAddUserGroupAddOnNew finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

async function addDataAndVoiceAddOn(planName, groupName, component,configure,attributeValue) {
    if(globalUGAddonMap[planName] == null){
        try{
            let currentAddons = await component.getAddonsForConfiguration(configure.guid);
            for(let segment of Object.values(currentAddons) ) {
                segment.allAddons.forEach(oneAddon => {
                    globalUGAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                    globalOneOffCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
                    globalRecurringCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
                });
            }
        }catch (error) {console.log('ERROR IS ',error);}
    }
    if(globalUGAddonMap[planName]!=null) {
        let ugAddon = globalUGAddonMap[planName];
        console.log('calculateAndAddUserGroupAddOn ugAddon',ugAddon);
        if(ugAddon.cspmb__Group__c.includes(groupName)){
            await addAddOnProductConfiguration('User Plans',ugAddon,configure.guid,1,component);
        }
    }
}

async function calculateVoiceAndDataAddOnCharges(component,configuration,relatedProduct) {
    var dataSchemeDiscount = configuration.getAttribute('Data Scheme Discount').value;
    var voiceSchemeDiscount = configuration.getAttribute('Voice Scheme Discount').value;
    if(relatedProduct.groupName.includes('Data')){
        relatedProduct.recurringCharge = Number(relatedProduct.recurringCharge) + Number(dataSchemeDiscount);
        relatedProduct.configuration.recurringPrice = Number(relatedProduct.configuration.recurringPrice) + Number(dataSchemeDiscount);
    }else if(relatedProduct.groupName.includes('Voice')){
        relatedProduct.recurringCharge = Number(relatedProduct.recurringCharge) + Number(voiceSchemeDiscount);
        relatedProduct.configuration.recurringPrice = Number(relatedProduct.configuration.recurringPrice) + Number(voiceSchemeDiscount);
    }
}

async function addCustomCallerPlans(solution,component,configuration,relatedProductName) {
    if(globalUGAddonMap[relatedProductName] == null){
        try{
            let currentAddons = await component.getAddonsForConfiguration(configuration.guid);
            for(let segment of Object.values(currentAddons) ) {
                segment.allAddons.forEach(oneAddon => {
                    globalUGAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                    globalOneOffCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
                    globalRecurringCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
                });
            }
        }catch (error) {console.log('ERROR IS ',error);}
    }
    if(globalUGAddonMap[relatedProductName]!=null) {
        for(let oneAddonKey of Object.keys(globalUGAddonMap)) {
            var oneAddon = globalUGAddonMap[oneAddonKey];
            if(oneAddon.cspmb__Group__c == 'Voice Bundle' || oneAddon.cspmb__Group__c == 'SMS Bundle' ){
                await addAddOnProductConfiguration('Service Plans',oneAddon,configuration.guid,1,component);
            }
        }
    }
}

/*async function calculateCustomCallerCharges(solution,component, configuration, relatedProduct) {
    let inputMap={};
    inputMap["basketId"] = solution.basketId;
    inputMap["Id"] = '0013E000012WrIS';
    currentBasket = await CS.SM.getActiveBasket();
    if(Object.keys(usageProfiledata).length === 0){
        let currentBasket = await CS.SM.getActiveBasket();
        usageProfiledata= await  currentBasket.performRemoteAction('CS_UsageProfileFM', inputMap);
    }
    var addonName = relatedProduct.name +' (Unit Price : £'+relatedProduct.recurringCharge+'/Minute)';

    if(usageProfiledata["DataFound"] == true){
        var fieldKey = customCallerAddOnArray[relatedProduct.name];
        if(fieldKey != ''){
            var persenageValue = usageProfiledata[fieldKey];
            var expectedUsage = 0;
            if(relatedProduct.groupName.includes('Voice')){
                expectedUsage= usageProfiledata['ExpectedVoice'];
            }else if(relatedProduct.groupName.includes('Data')){
                expectedUsage= usageProfiledata['ExpectedData'];
            }else if(relatedProduct.groupName.includes('SMS')){
                expectedUsage= usageProfiledata['ExpectedSMS'];
            }
            var calculatedUsage = Number((Number(expectedUsage) /100)* persenageValue)
            var recCharge = calculatedUsage*relatedProduct.recurringCharge;

            for (let attribute of Object.values(relatedProduct.configuration.attributes)) {

                if(attribute.name=='Expected Usage') attribute.value=expectedUsage;
                if(attribute.name=='Percentage Contribution') attribute.value=persenageValue;
                if(attribute.name=='Calculated Recurring Charge') attribute.value=recCharge;
                if(attribute.name=='Calculated Usage') attribute.value=calculatedUsage;
                if(attribute.name=='Unit Recurring Charge') attribute.value=relatedProduct.recurringCharge;
            }
            relatedProduct.recurringCharge = recCharge;
            relatedProduct.configuration.recurringPrice =recCharge;
            relatedProduct.name = addonName;
        }
    }
}*/

async function loadUsageProfileArtemis(componentName, configurationGuid){
    console.log('in loadUsageProfileArtemis');
    let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    let currentBasket = await CS.SM.getActiveBasket();
    if(globalAccountID == ''){
        let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole',{"basketId" : basketId, 'action' : 'CS_LookupQueryForAccountDetails'});
        if(result["DataFound"] == true) {
            globalAccountID=result["AccountId"];
        } 
    }
    let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
    urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
    urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
    urlString += '<div class="slds-modal__header">';
    urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
    urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
    urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
    urlString +='</svg>';
    urlString += '</button>';
    urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
    urlString +='Usage Profile </h2>';
    urlString += '</div>';
    urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
    urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__DisplayUsageProfileArtemisPage?accountId='+globalAccountID+'"';
    urlString += '</div>';
    urlString += '</div>';
    urlString += '</section>';
    urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
    urlString += '</div>';

    let inputMap={};
    inputMap["basketId"] = solution.basketId;
    inputMap["Id"] = globalAccountID;
   
    usageProfiledata = await  currentBasket.performRemoteAction('CS_UsageProfileFM', inputMap);
    
    if(usageProfiledata["DataFound"] == true){
        updateConfigurationAttributeValue('Requires Usage Data Refresh' , false, false, true, solution, mainConfiguration.guid, true);
        if(mainConfiguration.statusMessage =='Requires Usage Data Refresh') {
            mainConfiguration.status = true;
            mainConfiguration.errorMessage = null;
        }

        updateConfigurationAttributeValue('ETC' , usageProfiledata["ETC"], usageProfiledata["ETC"], true, solution, mainConfiguration.guid, true);
        updateConfigurationAttributeValue('ROW Voice Usage' , usageProfiledata["ROW Voice Usage"], usageProfiledata["ROW Voice Usage"], true, solution, mainConfiguration.guid, true);
        updateConfigurationAttributeValue('ROW Data Usage' , usageProfiledata["ROW Data Usage"], usageProfiledata["ROW Data Usage"], true, solution, mainConfiguration.guid, true);
    }
    return urlString;
}

async function displayUsageProfileArtemis(componentName,configGuid){
    console.log('in displayUsageProfileArtemis');
    let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;
    let configuration = solution.getConfiguration(configGuid);
    if(globalAccountID == '') {
        let currentBasket = await CS.SM.getActiveBasket();
        console.log('basketId',basketId);
        let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole',{"basketId" : basketId, 'action' : 'CS_LookupQueryForAccountDetails'});
        if(result["DataFound"] == true) globalAccountID=result["AccountId"];
        console.log('result',result);
        console.log('globalAccountID',globalAccountID);
    }
    
    var rateCardId = '';

    let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
    urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
    urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
    urlString += '<div class="slds-modal__header">';
    urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
    urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
    urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
    urlString +='</svg>';
    urlString += '</button>';
    urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
    urlString +='Usage Profile </h2>';
    urlString += '</div>';
    urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
    urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__ManageArtemis?accountId='+globalAccountID+'&basketId='+basketId+'&rateCardId='+rateCardId+'"';
    urlString += '</div>';
    urlString += '</div>';
    urlString += '</section>';
    urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
    urlString += '</div>';
    return urlString;
}

async function buildUKDataFutureTiersArtemis(componentName,configGuid){
    console.log('in buildUKDataFutureTiersArtemis');
    let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;
    let configuration = solution.getConfiguration(configGuid);
    let startingTierValueUK = configuration.getAttribute('Starting Tier Value (UK)').value;
    let startingTierChargeUK = configuration.getAttribute('Starting Tier Charge (UK)').value;
    let tierIncrementsValueUK = configuration.getAttribute('Tier Increments Value (UK)').value;
    let tierIncrementsPriceUK = configuration.getAttribute('Tier Increments Price (UK)').value;
    
    console.log('startingTierValueUK=', startingTierValueUK);
    console.log('startingTierChargeUK=', startingTierChargeUK);
    console.log('tierIncrementsValueUK=', tierIncrementsValueUK);
    console.log('tierIncrementsPriceUK=', tierIncrementsPriceUK);

    let urlString = '<div id="futureTiersIfFrame" class="demo-only" height="100%">';
    urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
    urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
    urlString += '<div class="slds-modal__header">';
    urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
    urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
    urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
    urlString +='</svg>';
    urlString += '</button>';
    urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
    urlString +='Data Future Tiers </h2>';
    urlString += '</div>';
    urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
    //urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__UsageProfileFM?basketid='+basketId+'&AccountId='+globalAccountID+'"';
    urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__ArtemisDataFutureTiersPage?startingTierValue='+startingTierValueUK+'&startingTierCharge='+startingTierChargeUK+'&tierIncrements='+tierIncrementsValueUK+'&tierIncrementsPrice='+tierIncrementsPriceUK+'"';
    //urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__ArtemisDataFutureTiersPage';
    urlString += '</div>';
    urlString += '</div>';
    urlString += '</section>';
    urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
    urlString += '</div>';
    return urlString;
}

async function buildROWDataFutureTiersArtemis(componentName,configGuid){
    console.log('in buildROWDataFutureTiersArtemis');
    let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;
    let configuration = solution.getConfiguration(configGuid);
    let startingTierValueROW = configuration.getAttribute('Starting Tier ROW').value;
    let startingTierChargeROW = configuration.getAttribute('Starting Tier Charge (ROW)').value;
    let tierIncrementsValueROW = configuration.getAttribute('Tier Increments Value (ROW)').value;
    let tierIncrementsPriceROW = configuration.getAttribute('Tier Increments Price (ROW)').value;
    
    console.log('startingTierValueROW=', startingTierValueROW);
    console.log('startingTierChargeROW=', startingTierChargeROW);
    console.log('tierIncrementsValueROW=', tierIncrementsValueROW);
    console.log('tierIncrementsPriceROW=', tierIncrementsPriceROW);
    
    startingTierChargeROW = parseInt(startingTierChargeROW) / 100;
    startingTierChargeROW = startingTierChargeROW.toFixed(2);

    let urlString = '<div id="futureTiersIfFrame" class="demo-only" height="100%">';
    urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
    urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
    urlString += '<div class="slds-modal__header">';
    urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
    urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
    urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
    urlString +='</svg>';
    urlString += '</button>';
    urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
    urlString +='Data Future Tiers </h2>';
    urlString += '</div>';
    urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
    //urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__UsageProfileFM?basketid='+basketId+'&AccountId='+globalAccountID+'"';
    urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__ArtemisDataFutureTiersPage?startingTierValue='+startingTierValueROW+'&startingTierCharge='+startingTierChargeROW+'&tierIncrements='+tierIncrementsValueROW+'&tierIncrementsPrice='+tierIncrementsPriceROW+'"';
    //urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__ArtemisDataFutureTiersPage';
    urlString += '</div>';
    urlString += '</div>';
    urlString += '</section>';
    urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
    urlString += '</div>';
    return urlString;
}

function calculateTotalUserSetupCharges(solution,configuration) {
    let totalRecOther =0.0;
    let totalOneOffOther =0.0;
    let totalVoiceCharges =0.0;
    let totalDataCharges =0.0;

    var quantity = configuration.configurationProperties.quantity;
    //var numberOfUser = configuration.getAttribute('Number Of User').value;
    //var contractTerm = configuration.getAttribute('Contract Term').value;
    if(configuration.relatedProductList.length >0){
        for(let relatedProduct of Object.values(configuration.relatedProductList)) {
            let addOnQuantity=-1;
            for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                if (attribute.name=='Quantity') {
                    addOnQuantity=Number(attribute.value);
                    if(isNaN(addOnQuantity) || addOnQuantity=='') addOnQuantity=0;
                }
            }
            if(addOnQuantity==-1) addOnQuantity=1;
            if(relatedProduct.groupName.includes('Voice') && relatedProduct.relatedProductName=='User Plans'){
                totalVoiceCharges += Number(relatedProduct.configuration.recurringPrice)*parseInt(quantity)*parseInt(addOnQuantity);
            }
            if(relatedProduct.groupName.includes('Data')  && relatedProduct.relatedProductName=='User Plans'){
                totalDataCharges += Number(relatedProduct.configuration.recurringPrice)*parseInt(quantity)*parseInt(addOnQuantity);
            }
            if(relatedProduct.relatedProductName!='User Plans'){
                totalRecOther += Number(relatedProduct.configuration.recurringPrice)*parseInt(quantity)*parseInt(addOnQuantity);
                totalOneOffOther+=  Number(relatedProduct.configuration.oneOffPrice)*parseInt(quantity)*parseInt(addOnQuantity);
            }
        }
    }

    updateConfigurationAttributeValue('Total Data Charge', totalDataCharges, totalDataCharges, true, solution, configuration.guid, true);
    updateConfigurationAttributeValue('Total Voice Charge', totalVoiceCharges, totalVoiceCharges, true, solution, configuration.guid, true);
    updateConfigurationAttributeValue('Total Other Upfront', totalOneOffOther, totalOneOffOther, true, solution, configuration.guid, true);
    updateConfigurationAttributeValue('Total Other Recurring', totalRecOther, totalRecOther, true, solution, configuration.guid, true);

}

function calculateTotalCharges(solution,mainConfiguration) {
    let totalRecOther =0.0;
    let totalOneOffOther =0.0;
    let totalVoiceCharges =0.0;
    let totalDataCharges =0.0;

    var contractTerm = mainConfiguration.getAttribute('Contract Term').value;

    for(let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
        var addOnQuantity=-1;
        if(relatedProduct.groupName.includes('Voice') && relatedProduct.relatedProductName=='Service Plans'){
            if (relatedProduct.name == 'Unlimited Minutes & Texts') {
                totalVoiceCharges += Number(relatedProduct.configuration.recurringPrice) * mainConfiguration.getAttribute('Number of User for AddOn').value;
            } else {
                totalVoiceCharges += Number(relatedProduct.configuration.recurringPrice);
            } 
        }
        if(relatedProduct.groupName.includes('Data')  && relatedProduct.relatedProductName=='Service Plans'){
            totalDataCharges += Number(relatedProduct.configuration.recurringPrice);
        }
        if(!(relatedProduct.relatedProductName=='Service Plans' && (relatedProduct.groupName.includes('Data') || relatedProduct.groupName.includes('Voice')))){
            for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                if (attribute.name=='Quantity') {
                    addOnQuantity=Number(attribute.value);
                    if(isNaN(addOnQuantity) || addOnQuantity=='') addOnQuantity=0;
                }
            }
            if(addOnQuantity==-1) addOnQuantity=1;
            totalRecOther += Number(relatedProduct.configuration.recurringPrice)*parseInt(addOnQuantity);
            totalOneOffOther+=  Number(relatedProduct.configuration.oneOffPrice)*parseInt(addOnQuantity);
        }
    }

    for(let component of Object.values(solution.components) ) {
        if (component.name== FUTURE_MOBILE_CONST.userGroups) {
            for(let configuration of Object.values(component.schema.configurations)) {
                totalDataCharges+=configuration.getAttribute('Total Data Charge').value;
                totalVoiceCharges+=configuration.getAttribute('Total Voice Charge').value;
                totalOneOffOther+=configuration.getAttribute('Total Other Upfront').value;
                totalRecOther+=configuration.getAttribute('Total Other Recurring').value;
            }
        }
    }
    updateConfigurationAttributeValue('Total Data Charge', totalDataCharges, totalDataCharges, true, solution, mainConfiguration.guid, true);
    updateConfigurationAttributeValue('Total Voice Charge', totalVoiceCharges, totalVoiceCharges, true, solution, mainConfiguration.guid, true);
    updateConfigurationAttributeValue('Total Other Upfront', totalOneOffOther, totalOneOffOther, true, solution, mainConfiguration.guid, true);
    updateConfigurationAttributeValue('Total Other Recurring', totalRecOther, totalRecOther, true, solution, mainConfiguration.guid, true);
}

function updateConfigNameArtemis(con, componentName,solution) {
    var newName;
    if(componentName==FUTURE_MOBILE_CONST.mainComponent) {
        newName = con.getAttribute('Service Plan').displayValue+' : '+con.getAttribute('Contract Term').displayValue+' Months';
    }
    if(componentName==FUTURE_MOBILE_CONST.userGroups) {
        newName = con.getAttribute('Group Name').displayValue+' : '+con.getAttribute('Service Plan Name').displayValue+' : '+con.getAttribute('Subscription Type').displayValue;
    }
    con.configurationName=newName;
}

async function updateRateCardAttribute(RateCardID,totalCharge,totalCost) {
    console.log('RateCardID'+RateCardID);
    var solution = await CS.SM.getActiveSolution();
    var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    updateConfigurationAttributeValue('Rate Card Lookup' , RateCardID, RateCardID, true, solution, servicePlanConfiguration.guid, true);
    updateConfigurationAttributeValue('Custom Caller Total Estimated Charge' , totalCharge, totalCharge, true, solution, servicePlanConfiguration.guid, true);
    updateConfigurationAttributeValue('Custom Caller Total Estimated Cost' , totalCost, totalCost, true, solution, servicePlanConfiguration.guid, true);
}

async function autoAddSIM(solution,configParam) {
    var mainConfiguration=Object.values(solution.schema.configurations)[0];
    var connectionType=mainConfiguration.getAttribute('Connection Type').displayValue;

    for(let component of Object.values(solution.components) ) {
        if (component.name== FUTURE_MOBILE_CONST.userGroups) {
            for(let configuration of Object.values(component.schema.configurations)) {
                if(configParam==null || configuration.guid==configParam.guid) {
                    var simGrp=configuration.getAttribute('eSIM Required').displayValue==true ? 'eSIM' : 'SIM';
                    if(connectionType!='' && configuration.getAttribute('Subscription Type').value!='') {
                        var correctAddonExists=false;
                        for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                            if(relatedProduct.Add_On_Type__c==connectionType && relatedProduct.cspmb__Group__c==simGrp ) {
                                correctAddonExists=true;
                            }
                            else {
                                if(relatedProduct.groupName=='SIM' || relatedProduct.groupName=='eSIM')
                                    await deleteAddOns(component, configuration, relatedProduct.guid, true);
                            }
                        }
                        if(!correctAddonExists) {
                            var availableAddons = await solution.getAddonsForConfiguration(configuration.guid);
                            for(let segment of Object.values(availableAddons) ) {
                                for(let oneAddon of Object.values(segment.allAddons)) {
                                    globalOneOffCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
                                    globalRecurringCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
                                    if(oneAddon.Add_On_Type__c==connectionType && oneAddon.cspmb__Group__c==simGrp) {
                                        try{await addAddOnProductConfiguration('Devices',oneAddon,configuration.guid,1,component);
                                        } catch(exception) {console.log('this',exception);}
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

async function autoAddSharedAccessFee(solution,configParam) {
    var mainConfiguration=Object.values(solution.schema.configurations)[0];
    var servicePlan=mainConfiguration.getAttribute('Service Plan').displayValue;

    for(let component of Object.values(solution.components) ) {
        if (component.name== FUTURE_MOBILE_CONST.userGroups) {
            for(let configuration of Object.values(component.schema.configurations)) {
                if(configParam==null || configuration.guid==configParam.guid) {
                    if(servicePlan=='The Shared Plan' && configuration.getAttribute('Subscription Type Name').value!='') {//if shared plans should exist
                        var correctAddonExists=false;
                        for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                            if(relatedProduct.name == 'Shared Access Fee') {
                                correctAddonExists=true;
                            }
                        }
                        if(!correctAddonExists) {
                            var availableAddons = await solution.getAddonsForConfiguration(configuration.guid);
                            for(let segment of Object.values(availableAddons) ) {
                                for(let oneAddon of Object.values(segment.allAddons)) {
                                    if(oneAddon.Add_On_Name__c == 'Shared Access Fee') {
                                        try{await addAddOnProductConfiguration('User Plans',oneAddon,configuration.guid,1,component);
                                        } catch(exception) {console.log('this',exception);}
                                    }
                                }
                            }
                        }
                    }
                    else {//if shared plans should not exist
                        for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                            if(relatedProduct.name == 'Shared Access Fee') {
                                await deleteAddOns(component, configuration, relatedProduct.guid, true);
                            }
                        }
                    }
                }
            }
        }
    }
}

function checkDevices(solution,configParam) {
    var deviceExists;
    for(let component of Object.values(solution.components) ) {
        if (component.name== FUTURE_MOBILE_CONST.userGroups) {
            for(let configuration of Object.values(component.schema.configurations)) {
                if(configParam==null || configuration.guid==configParam.guid) {
                    deviceExists=false;
                    for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                        if(relatedProduct.relatedProductName=='Devices') {
                            deviceExists=true;
                        }
                    }
                    updateConfigurationAttributeValue('Devices Added' , deviceExists, deviceExists, true, solution, configuration.guid, true);
                }
            }
        }
    }
}


function calculateUsersCount(solution){
    var mainConfiguration=Object.values(solution.schema.configurations)[0];
    var contractTerm;
    var subCount=0;
    var totalCharge=0;
    var totalCost=0;
    var qbs;
    var qbsNonIndividualUserGroups;
    var nonIndividualUserGroups = 0;
    for(let component of Object.values(solution.components) ) {
        for(let configuration of Object.values(component.schema.configurations)) {
            if (component.name== FUTURE_MOBILE_CONST.special) {
                contractTerm=Number(mainConfiguration.getAttribute('Contract Term').value);
            }
            else {
                contractTerm=Number(configuration.getAttribute('Contract Term').value);
            }
            if(isNaN(contractTerm)) contractTerm=0;
            if (component.name== FUTURE_MOBILE_CONST.userGroups) {

                updateConfigurationAttributeValue('Quantity' ,configuration.configurationProperties.quantity, configuration.configurationProperties.quantity, true, solution, configuration.guid, false);

                qbs=Number(configuration.configurationProperties.quantity);
                subCount+=qbs;

                if (!configuration.getAttribute('Individual Override').value) {
                    qbsNonIndividualUserGroups = Number(configuration.configurationProperties.quantity);
                    nonIndividualUserGroups += qbsNonIndividualUserGroups;
                }
            }
            else {
                qbs=1;
            }
            for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                if(!isNaN(Number(relatedProduct.recurringCharge))) {
                    totalCharge+=Number(relatedProduct.configuration.recurringPrice)*qbs*contractTerm;
                    //totalCost+=Number(relatedProduct.configuration.recurringCost)*qbs*contractTerm;
                }
                if(!isNaN(Number(relatedProduct.configuration.oneOffPrice))) {
                    totalCharge+=Number(relatedProduct.configuration.oneOffPrice)*qbs;
                    //totalCost+=Number(relatedProduct.configuration.oneOffCost)*qbs;
                }
            }
        }
    }
    contractTerm=Number(mainConfiguration.getAttribute('Contract Term').value);
    if(isNaN(contractTerm)) contractTerm=0;

    for(let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
        if(!isNaN(Number(relatedProduct.recurringCharge))) {
            totalCharge+=Number(relatedProduct.configuration.recurringPrice)*contractTerm;
            //totalCost+=Number(relatedProduct.configuration.recurringCost)*contractTerm;
        }
        if(!isNaN(Number(relatedProduct.configuration.oneOffPrice))) {
            totalCharge+=Number(relatedProduct.configuration.oneOffPrice);
            //totalCost+=Number(relatedProduct.configuration.oneOffCost);
        }
    }
    updateConfigurationAttributeValue('Total Number of Users',subCount,subCount,true,solution,mainConfiguration.guid,true);
    updateConfigurationAttributeValue('Total Charge',totalCharge.toFixed(2),totalCharge.toFixed(2),true,solution,mainConfiguration.guid,true);
    updateConfigurationAttributeValue('Number of User for AddOn', nonIndividualUserGroups, nonIndividualUserGroups, true, solution, mainConfiguration.guid, true);
    //updateConfigurationAttributeValue('Total Cost',totalCost.toFixed(2),totalCost.toFixed(2),true,solution,mainConfiguration.guid,true);
}
async function preloadAddonCostData(solution){
    var needsToQuery;
    for(let component of Object.values(solution.components) ) {
        for(let configuration of Object.values(component.schema.configurations)) {
            needsToQuery=false;
            for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                if(globalOneOffCostMap[relatedProduct.name]==null || globalRecurringCostMap[relatedProduct.name]==null) {
                    needsToQuery=true;
                    break;
                }
            }
            if(needsToQuery) {
                let availableAddons = await component.getAddonsForConfiguration(configuration.guid);
                for(let segment of Object.values(availableAddons) ) {
                    for(let oneAddon of Object.values(segment.allAddons)) {
                        globalOneOffCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
                        globalRecurringCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
                    }
                }
            }
        }
    }
    var mainConfiguration=Object.values(solution.schema.configurations)[0];
    for(let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
        if(globalOneOffCostMap[relatedProduct.name]==null || globalRecurringCostMap[relatedProduct.name]==null) {
            needsToQuery=true;
            break;
        }
    }
    if(needsToQuery) {
        let availableAddons = await solution.getAddonsForConfiguration(mainConfiguration.guid);
        for(let segment of Object.values(availableAddons) ) {
            for(let oneAddon of Object.values(segment.allAddons)) {
                globalOneOffCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
                globalRecurringCostMap[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
            }
        }
    }
}


function reloadUIafterSolutionLoadedFM(solution) {
    var mainConfiguration = Object.values(solution.schema.configurations)[0];
    servicePlan = mainConfiguration.getAttribute('Service Plan').displayValue;
    customDataServicePlan = mainConfiguration.getAttribute('Custom Data Service Plan').displayValue;

    for(let component of Object.values(solution.components) ) {
        if(component.name==FUTURE_MOBILE_CONST.mainComponent) {
            if(servicePlan!='Custom Caller') {
                updateData = [{name : 'Custom Data Service Plan', value : 'N/A' , displayValue : 'N/A', readOnly : true, required : false}];
                if(mainConfiguration.getAttribute('Custom Data Service Plan')=='')
                    component.updateConfigurationAttribute(configuration.guid, updateData, false);
                updateData = [{name : 'Custom Voice Rate Card', value : 'N/A' , displayValue : 'N/A', readOnly : true, required : false}];
                if(mainConfiguration.getAttribute('Custom Voice Rate Card')=='')
                    component.updateConfigurationAttribute(configuration.guid, updateData, false);
            }
            if(servicePlan!='The Team Plan') {
                updateData = [{name : 'Variant', value : 'N/A' , displayValue : 'N/A', readOnly : true, required : false}];
                if(mainConfiguration.getAttribute('Variant')=='')
                    component.updateConfigurationAttribute(configuration.guid, updateData, true);
                updateData = [{name : 'Team Voice Plan', value : 'N/A' , displayValue : 'N/A', readOnly : true, required : false}];
                if(mainConfiguration.getAttribute('Team Voice Plan')=='')
                    component.updateConfigurationAttribute(configuration.guid, updateData, false);
                updateData = [{name : 'Team Voice Scheme', value : 'N/A' , displayValue : 'N/A', readOnly : true, required : false}];
                if(mainConfiguration.getAttribute('Team Voice Scheme')=='')
                    component.updateConfigurationAttribute(configuration.guid, updateData, false);
            }
            if(servicePlan!='The Team Plan' && customDataServicePlan!='Team') {
                updateData = [{name : 'Team Data Plan', value : 'N/A' , displayValue : 'N/A', readOnly : true, required : false}];
                if(mainConfiguration.getAttribute('Team Data Plan')=='')
                    component.updateConfigurationAttribute(configuration.guid, updateData, false);
                updateData = [{name : 'Team Data Scheme', value : 'N/A' , displayValue : 'N/A', readOnly : true, required : false}];
                if(mainConfiguration.getAttribute('Team Data Scheme')=='')
                    component.updateConfigurationAttribute(configuration.guid, updateData, false);
            }
        }
        if(component.name==FUTURE_MOBILE_CONST.creditFund) {
            for(let configuration of Object.values(component.schema.configurations)) {
                updateConfigurationAttributeValue('Total Airtime Fund Available Display' , 'N/A', 'N/A', true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Remaining Airtime Funds Display' , 'N/A', 'N/A', true, solution, configuration.guid, true);
            }
        }
    }
}

async function setUKVoicePrice() {
    let voicePrice;
    let solution = await CS.SM.getActiveSolution();
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    
    voiceOverridePrice = parseFloat(mainConfiguration.getAttribute('Override Monthly Recurring Price Voice').value);
    voiceLookupPrice = parseFloat(mainConfiguration.getAttribute('Unit Recurring Charge Voice Lookup').value);

    if (voiceOverridePrice > 0.0) {
        voicePrice = voiceOverridePrice;
    } else {
        voicePrice = voiceLookupPrice;
    }
    
    updateConfigurationAttributeValue('Unit Recurring Charge Voice' , voicePrice, voicePrice, true, solution, mainConfiguration.guid, true);
} 

async function setUKDataPrice() {
    let dataPrice;
    let solution = await CS.SM.getActiveSolution();
    let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    
    dataOverridePrice = parseFloat(mainConfiguration.getAttribute('Override Monthly Recurring Price Data').value);
    dataLookupPrice = parseFloat(mainConfiguration.getAttribute('Unit Recurring Charge Data Lookup').value);

    if (dataOverridePrice > 0.0) {
        dataPrice = dataOverridePrice;
    } else {
        dataPrice = dataLookupPrice;
    }

    updateConfigurationAttributeValue('Unit Recurring Charge Data' , dataPrice, dataPrice, true, solution, mainConfiguration.guid, true);
}

async function addVoiceAndDataArtemis(solution, component,userGroupConfiguration,subscriptionTypeName) {
    var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    subscriptionTypeName = userGroupConfiguration.getAttribute('Subscription Type').displayValue;
    
    // delete exiting User Plans
    if(userGroupConfiguration.relatedProductList.length > 0){
        for(let relatedProduct of Object.values(userGroupConfiguration.relatedProductList)) {
            if(relatedProduct.relatedProductName == 'User Plans'){
                await deleteAddOns(component, userGroupConfiguration, relatedProduct.guid, true);
            }
        }
    }

    var dataPlanType = userGroupConfiguration.getAttribute('Data Plan Type').value;
    var voicePlanToAdd = 'Corporate Capped 2 Unlimited Minutes & Text';
    var voicePlan = mainConfiguration.getAttribute('UK Data Plan').displayValue;

    if (subscriptionTypeName == 'Voice Only') {
        if (voicePlan != '' || voicePlan != null) {
            addAddOnUserGroupArtemis(voicePlanToAdd, 'Voice', component, userGroupConfiguration, subscriptionTypeName);
        }
        return;
    }

    if (subscriptionTypeName == 'Voice and Data') {
        if (voicePlan != '' || voicePlan != null) {
            addAddOnUserGroupArtemis(voicePlanToAdd, 'Voice', component, userGroupConfiguration, subscriptionTypeName)
        }

        if (dataPlanType.includes('Aggregated')) {
            var startingTierPlan = mainConfiguration.getAttribute('Starting Tier (UK)').displayValue;
            addAddOnUserGroupArtemis(startingTierPlan, 'Data', component, userGroupConfiguration, subscriptionTypeName);
        }
    }

    if (subscriptionTypeName == 'Data Only') {
        if (dataPlanType.includes('Aggregated')) {
            var startingTierPlan = mainConfiguration.getAttribute('Starting Tier (UK)').displayValue;
            addAddOnUserGroupArtemis(startingTierPlan, 'Data', component, userGroupConfiguration, subscriptionTypeName);
        }
    }
}

async function addAddOnUserGroupArtemis(planName, groupName, component,configure,subscriptionTypeName) {
    if(globalUGAddonMapArtemis[planName] == null){
        try{
            let currentAddons = await component.getAddonsForConfiguration(configure.guid);
            for(let segment of Object.values(currentAddons) ) {
                segment.allAddons.forEach(oneAddon => {
                    globalUGAddonMapArtemis[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                    globalOneOffCostMapArtemis[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
                    globalRecurringCostMapArtemis[oneAddon.Add_On_Name__c]=oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
                });
            }
        }catch (error) {console.log('ERROR IS ',error);}
    } 

    if(globalUGAddonMapArtemis[planName]!=null) {
        let ugAddon = globalUGAddonMapArtemis[planName];
        console.log('calculateAndAddUserGroupAddOn ugAddon',ugAddon);
        await addAddOnProductConfiguration('User Plans',ugAddon,configure.guid,1,component);
    }
}

async function calculateOverrideVoiceAndDataCharges() {
    solution = await CS.SM.getActiveSolution();
    for(let component of Object.values(solution.components) ) {
        if (component.name== ARTEMIS_CONST.userGroup) {
            for(let userGroupConfiguration of Object.values(component.schema.configurations)) {
                if(userGroupConfiguration.relatedProductList.length > 0){
                    for(let relatedProduct of Object.values(userGroupConfiguration.relatedProductList)) {
                        if(relatedProduct.relatedProductName == 'User Plans'){
                            calculateVoiceAndDataAddOnChargesArtemis(relatedProduct);
                        }       
                    }    
                }
            }
        }
    }
}

async function calculateVoiceAndDataAddOnChargesArtemis(relatedProduct) {
    var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    var voicePrice = mainConfiguration.getAttribute('Unit Recurring Charge Voice').value;
    var dataPrice = mainConfiguration.getAttribute('Unit Recurring Charge Data').value;
    if (relatedProduct.groupName.includes('Data')) {
        relatedProduct.recurringCharge = Number(dataPrice);
        relatedProduct.configuration.recurringPrice = Number(dataPrice);
    } else if(relatedProduct.groupName.includes('Voice')){
        relatedProduct.recurringCharge = Number(voicePrice);
        relatedProduct.configuration.recurringPrice = Number(voicePrice);
    }
}
