var css = {
	success: {
		label: [
			'background-color:rgba(0,128,0,0.5)',
			'color:#fff',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		timestamp: [
			'background-color:rgba(0,128,0,0.5)',
			'color:#fff',
			'padding:0 10px 0 0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		at: [
			'background-color:rgba(0,128,0,0.5)',
			'color:#ff0',
			'padding:0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			'font-variant: small-caps',
			//'border-radius:5px',
			'font-weight:bold',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		separator: [
			'background-color:rgba(0,128,0,0.5)',
			'color:rgba(255,255,255,.7)',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size: 12px',
			//'border-radius:5px',
			'font-weight: normal',
			'margin-top: -5px',
			'margin-left: -14px',
			'margin-bottom:-3px',
			'font-style:italic'
		].join(';')
	},
	warning: {
		label: [
			'background-color:rgba(255,128,0,0.5)',
			'color:#fff',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		timestamp: [
			'background-color:rgba(255,128,0,0.5)',
			'color:#fff',
			'padding:0 10px 0 0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		at: [
			'background-color:rgba(255,128,0,0.5)',
			'color:#ff0',
			'padding:0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			'font-variant: small-caps',
			//'border-radius:5px',
			'font-weight:bold',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		separator: [
			'background-color:rgba(255,128,0,0.5)',
			'color:rgba(255,255,255,.7)',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size: 12px',
			//'border-radius:5px',
			'font-weight: normal',
			'margin-top: -5px',
			'margin-left: -14px',
			'margin-bottom:-3px',
			'font-style:italic'
		].join(';')
	},
	error: {
		label: [
			'background-color:rgba(192,0,0,0.5)',
			'color:#fff',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		timestamp: [
			'background-color:rgba(192,0,0,0.5)',
			'color:#fff',
			'padding:0 10px 0 0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		at: [
			'background-color:rgba(192,0,0,0.5)',
			'color:#ff0',
			'padding:0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			'font-variant: small-caps',
			//'border-radius:5px',
			'font-weight:bold',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		separator: [
			'background-color:rgba(192,0,0,0.5)',
			'color:rgba(255,255,255,.7)',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size: 12px',
			//'border-radius:5px',
			'font-weight: normal',
			'margin-top: -5px',
			'margin-left: -14px',
			'margin-bottom:-3px',
			'font-style:italic'
		].join(';')
	},
	info: {
		label: [
			'background-color:rgba(0,0,192,0.5)',
			'color:#fff',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		timestamp: [
			'background-color:rgba(0,0,192,0.5)',
			'color:#fff',
			'padding:0 10px 0 0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			//'border-radius:5px',
			'font-weight:normal',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		at: [
			'background-color:rgba(0,0,192,0.5)',
			'color:#ff0',
			'padding:0',
			'line-height:1.8',
			'font:monospace',
			'font-size:12px',
			'font-variant: small-caps',
			//'border-radius:5px',
			'font-weight:bold',
			'margin-top:-5px',
			'margin-bottom:-3px'
		].join(';'),
		separator: [
			'background-color:rgba(0,0,192,0.5)',
			'color:rgba(255,255,255,.7)',
			'padding:0 10px 0 10px',
			'line-height:1.8',
			'font:monospace',
			'font-size: 12px',
			//'border-radius:5px',
			'font-weight: normal',
			'margin-top: -5px',
			'margin-left: -14px',
			'margin-bottom:-3px',
			'font-style:italic'
		].join(';')
	}
};

var logger = {
	success: function(){
		let timestamp = new Date().getTime();
		console.groupCollapsed('%c'+ arguments[0], css.success.label);
		for(let i = 1; i < arguments.length; i++){
			console.log(arguments[i]);
		}
		console.log('%c'+ timestamp, css.success.separator);
		console.groupEnd();
	},
	warning: function(){
		let timestamp = new Date().getTime();
		console.groupCollapsed('%c'+ arguments[0], css.warning.label);
		for(let i = 1; i < arguments.length; i++){
			console.log(arguments[i]);
		}
		console.log('%c'+ timestamp, css.warning.separator);
		console.groupEnd();
	},
	error: function(){
		let timestamp = new Date().getTime();
		console.groupCollapsed('%c'+ arguments[0], css.error.label);
		for(let i = 1; i < arguments.length; i++){
			console.log(arguments[i]);
		}
		console.log('%c'+ timestamp, css.error.separator);
		console.groupEnd();
	},
	info: function(){
		let timestamp = new Date().getTime();
		console.groupCollapsed('%c'+ arguments[0], css.info.label);
		for(let i = 1; i < arguments.length; i++){
			console.log(arguments[i]);
		}
		console.log('%c'+ timestamp, css.info.separator);
		console.groupEnd();
	}
};