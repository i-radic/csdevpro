﻿var Flag;

/*********************CR2611 Requirement For*******************/
/*'Upgrade to Fibre' to be available for Lincoln and Dundee - OMD only initially as a trial*/
/*function ddlSubCategoryServiceSitePopulation(objDDL) {
if (objDDL == '') {
//$("#ddlBB option[value='Upgrade to Fibre']").hide();
$("#ddlBB").find("option[value='Upgrade to Fibre']").attr('disabled', 'disabled');
}
else if (objDDL != 'Alness T2R' && objDDL != 'Lincoln' && objDDL != 'Dundee - OMD' && objDDL != 'Dundee FL (Frontline)' && objDDL != 'Nottingham' && objDDL != 'Newcastle' && objDDL != 'Blackburn' && objDDL != 'Dundee TOU (Technical Ownership Unit)') {
//$("#ddlBB option[value='Upgrade to Fibre']").hide();
$("#ddlBB").find("option[value='Upgrade to Fibre']").attr('disabled', 'disabled');
}
else {
$("#ddlBB option[value='Upgrade to Fibre']").attr('disabled', '');
}
}*/
/*************************************************************/
function ddlCustType_onchange(objDDL) {
    //$('#tdCustomerID').hide(); $('#txtCUGLabel').val('');
    $('#ddlProduct')[0].value = '';
    $('#TrBTBCorporateMobile').hide();
    $('#tdAdditionalLines').hide(); $('#ddlAdditionalLines').val('');
    $('#tdMobile').hide(); $('#ddlMobile').val('');
    $('#tdSwitch').hide(); $('#ddlSwitch').val('');
    $('#tdBB').hide(); $('#ddlBB').val('');
    $('#tdBBMovers').hide(); $('#ddlBBMovers').val('');
    $('#tdVOIP').hide(); $('#ddlVOIP').val('');
    $('#tdISND').hide(); $('#ddlISND').val('');
    $('#tdLeasedConf').hide(); $('#ddlLeasedConf').val('');
    $('#tdCallslines').hide(); $('#ddlCallslines').val('');
    $('#tdBBTelNo').hide(); $('#txtBBTelNo').val('');
    $('#tdEtherwayEtherflow').hide(); $('#ddlEtherwayEtherflow').val('');
    $('#tdMPLS').hide(); $('#ddlMPLS').val('');
    $('#tdWanLan').hide(); $('#ddlWanLan').val('');
    $('#tdMobileSelfFulfil').hide(); $('#ddlMobileSelfFulfil').val('');
    $('#tdMobileSelfFulfilCorporate').hide();
    $('#tdBTSport').hide(); $('#ddlBTSport').val('');
    $('#tdchkSendEmail2OnePlan').hide(); $('#chkSendEmail2OnePlan').val('false');
    $('#tdBTCloudVoice').hide(); $('#ddBTCloudVoiceLabel').val('');

    switch (objDDL.value) {
        case 'BT Business':
            $('#BTCloudVoice').fadeIn('slow');
            break;
        case 'Corporate':
            $('#tdCorporateHText').fadeIn('slow');
            $('#BTCloudVoice').hide();
            break;
        case 'Northern Ireland':
            $('#BTCloudVoice').hide();
            break;
        default:
            $('#tdCorporateHText').hide();
            break;
    }

}

var count = 0;
function onlyDigitsNDecimalPoint(e, field) {

    var key;
    if (window.event)
        key = window.event.keyCode;
    else if (e)

        key = e.which;
    else
        return true;
    var isControlKey = (key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27); //|| (key == 32)-emptyspace;

    if (key == 46)
        count++;

    if (key == 8)
        count = 0;

    return (key > 47 && key < 58) || isControlKey || key == 43 || (count <= 1);

}


function hideCommonFields() {
    $('#tdAdditionalLines').hide(); $('#ddlAdditionalLines').val('');
    $('#tdMobile').hide(); $('#ddlMobile').val('');
    $('#tdSwitch').hide(); $('#ddlSwitch').val('');
    $('#tdBB').hide(); $('#ddlBB').val('');
    $('#tdBBMovers').hide(); $('#ddlBBMovers').val('');
    $('#tdVOIP').hide(); $('#ddlVOIP').val('');
    $('#tdISND').hide(); $('#ddlISND').val('');
    $('#tdLeasedConf').hide(); $('#ddlLeasedConf').val('');
    $('#tdCallslines').hide(); $('#ddlCallslines').val('');
    $('#tdBBTelNo').hide(); $('#txtBBTelNo').val('');
    $('#tdEtherwayEtherflow').hide(); $('#ddlEtherwayEtherflow').val('');
    $('#tdMPLS').hide(); $('#ddlMPLS').val('');
    $('#tdWanLan').hide(); $('#ddlWanLan').val('');
    $('#tdMobileSelfFulfil').hide(); $('#ddlMobileSelfFulfil').val('');
    $('#tdMobileSelfFulfilCorporate').hide(); $('#ddlMobileSelfFulfilCorporate').val('');
    $('#tdBTSport').hide(); $('#ddlBTSport').val('');
    $('#tdchkSendEmail2OnePlan').hide(); $('#chkSendEmail2OnePlan').val('false');
    $('#tdBTCloudVoice').hide(); $('#ddBTCloudVoice').val('');
    $('#tdServiceSelfFulfil').hide(); $('#ddlServiceSelfFulfil').val(''); 
}

// show fields for Corporate - Mobile Selffulfil products
function showFieldsForCoporate() {
    $('#tdMobileSelfFulfilCorporate').fadeIn('slow');
    $('#tdTariff').fadeIn('slow');
    $('#tdGroup').fadeIn('slow');
    $('#tdCorporate').fadeIn('slow');
    $('#tdDiceAccountNumber').fadeIn('slow');
    $('#tdSubscription').fadeIn('slow');
    $('#tdContractTerm').fadeIn('slow');
    $('#tdServiceCost').fadeIn('slow');
    $('#tdContractStartDate').fadeIn('slow');
    $('#tdCCNumber').fadeIn('slow');
}

// hide fields for Corporate - Mobile Selffulfil products.
function hideFieldsForCorporate() {
    $('#tdMobileSelfFulfilCorporate').hide(); $('#ddlMobileSelfFulfilCorporate').val('');
    $('#tdTariff').hide(); $('#ddlTariff').val('');
    $('#tdGroup').hide(); $('#ddlGroup').val('');
    $('#tdCorporate').hide(); $('#ddlCorporate').val('');
    $('#tdSubscription').hide(); $('#txtSubscription').val('');
    $('#tdContractTerm').hide(); $('#ddlContractTerm').val('');
    $('#tdServiceCost').hide(); $('#txtServiceCost').val('');
    $('#tdContractStartDate').hide(); $('#txtContractStartDate').val('');
    $('#tdCCNumber').hide(); $('#txtCCMNo').val('');
}
// Dice Account number is common for both BTB and Corporate

function showFieldsForBTB() {
    $('#tdMobileSelfFulfil').fadeIn('slow');
    $('#tdTariffBTB').fadeIn('slow');
    $('#tdDiceAccountNumber').fadeIn('slow');
    $('#tdOrderReference').fadeIn('slow');
    $('#tdResignConnections').fadeIn('slow');
    $('#tdAddToFleetConnections').fadeIn('slow');
    $('#tdRevenue').fadeIn('slow');
    $('#tdHandset').fadeIn('slow');

}

function hideFieldsForBTB() {
    $('#tdMobileSelfFulfil').hide(); $('#ddlMobileSelfFulfil').val('');
    $('#tdTariffBTB').hide(); $('#txtTariffBTB').val('');
    $('#tdOrderReference').hide(); $('#txtOrderReference').val('');
    $('#tdResignConnections').hide(); $('#txtResignConnections').val('');
    $('#tdAddToFleetConnections').hide(); $('#txtAddToFleetConnections').val('');
    $('#tdRevenue').hide(); $('#txtRevenue').val('');
    $('#tdHandset').hide(); $('#txtHandset').val('');
}

function hideFieldsForMobileSelfFulfil() {
    $('#tdCustomerID').hide();
    $('#tdCustomerTitle').hide();
    $('#tdCCFirstName').hide();
    $('#tdCCLastName').hide();
    $('#tdCCTelNumber').hide();
    $('#tdCEmailAddress').hide();
    $('#tdPostcode').hide();

    $('#tdSalesAgentEIN').hide();
    $('#tdOpportunityGoLiveDate').hide();
    $('#tdCorporateHText').hide();
}

  function hideFieldsForServiceSelfFulfil() {
 /* $('#tdCustomerID').hide();
    $('#tdCustomerTitle').hide();
    $('#tdCCFirstName').hide();
    $('#tdCCLastName').hide();
    $('#tdCCTelNumber').hide();
    $('#tdCEmailAddress').hide();
    $('#tdPostcode').hide();  */
    
    $('#tdSalesAgentEIN').hide();
    $('#tdOpportunityGoLiveDate').hide();
    $('#tdCorporateHText').hide();
}  

function showFieldsForServiceSelfFulfil() {
 /* $('#tdMobileSelfFulfil').fadeIn('slow');
    $('#tdTariffBTB').fadeIn('slow');
    $('#tdDiceAccountNumber').fadeIn('slow'); */
    $('#tdOrderReference').fadeIn('slow');
    $('#tdContractTerm').fadeIn('slow');
    $('#tdRevenue').fadeIn('slow');
    $('#tdContractStartDate').fadeIn('slow');
    $('#tdQuantity').fadeIn('slow');
    $('#tdStatus').fadeIn('slow');
 /* $('#tdResignConnections').fadeIn('slow');
    $('#tdAddToFleetConnections').fadeIn('slow');
    $('#tdRevenue').fadeIn('slow');
    $('#tdHandset').fadeIn('slow'); */

}  

function ddlProduct_onchange(objDDL) {
    hideCommonFields();

    switch (objDDL.value) {
        case 'Mobile':
            $('#tdMobile').fadeIn('slow');
            if ($("#ddlCustType")[0].value == 'Northern Ireland') {
                $('#tdMobile').hide();
                $('#TrBTBCorporateMobile').fadeIn('slow');
            }
            break;
        case 'BT Cloud Voice':
            $('#tdBTCloudVoice').fadeIn('slow');
            break;            
        case 'Switch':
            $('#tdSwitch').fadeIn('slow');
            break;
        case 'Additional lines and maintenance':
            $('#tdAdditionalLines').fadeIn('slow');
            break;
        case 'Leased Lined /Private Circuits ( BTnet)':
            $('#tdLeasedConf').fadeIn('slow');
            break;
        case 'Conferencing and Inbound Services':
            $('#tdLeasedConf').fadeIn('slow');
            break;
        case 'Broadband':
            $('#tdBB').fadeIn('slow');
            $('#tdBBTelNo').fadeIn('slow');
            /*********************CR2611 Requirement For*******************/
            /*'Upgrade to Fibre' to be available for Lincoln and Dundee - OMD only initially as a trial*/
            //ddlSubCategoryServiceSitePopulation($('#ddlServiceSite').val());
            /*************************************************************/
            break;
        case 'Broadband-Movers':
            $('#tdBBMovers').fadeIn('slow');
            $('#tdBBTelNo').fadeIn('slow');
            break;
        case 'ISDN':
            $('#tdISND').fadeIn('slow');
            break;
        case 'Calls/Lines Non BT Customers (winback)':
            $('#tdCallslines').fadeIn('slow');
            break;
        /*********************CR2478 Requirement For*******************/ 
        case 'VOIP':
            $('#tdVOIP').fadeIn('slow');
            break;
        /*************************************************************/ 

        /*********************CR3398*******************/ 
        case 'Etherway/Etherflow':
            $('#tdEtherwayEtherflow').fadeIn('slow');
            break;
        case 'MPLS':
            $('#tdMPLS').fadeIn('slow');
            break;
        case 'Wan/Lan':
            $('#tdWanLan').fadeIn('slow');
            break;
        case 'Mobile Self Fulfil':

            $('#tdStatus').fadeIn('slow');

            hideFieldsForMobileSelfFulfil();

            if ($("#ddlCustType")[0].value == 'BT Business') {
                $('#tdchkSendEmail2OnePlan').fadeIn('slow');
                showFieldsForBTB();
                hideFieldsForCorporate();

            }
            if ($("#ddlCustType")[0].value == 'Corporate') {
                showFieldsForCoporate();
                hideFieldsForBTB();

            }
            if ($("#ddlCustType")[0].value == 'Northern Ireland') {
                showFieldsForBTB();
                hideFieldsForCorporate();
            }
            break;
        case 'Service Self-fulfil':
            {
                $('#tdServiceSelfFulfil').fadeIn('slow');
                hideFieldsForServiceSelfFulfil();
                showFieldsForServiceSelfFulfil();

            }
            break;   
        case 'BT Sport':
            $('#tdBTSport').fadeIn('slow');
            break;

        /*************************************************************/ 
    }
}

function ddlCategory_onchange(objDDL) {
    switch (objDDL.value) {
        case 'Upgrade to Fibre':
            alert('Please ensure you have checked ‘Tags’ or \'Broadband availability Checker\' BEFORE placing this lead. Remember, Fibre must be available NOW.');
            break;

        default:
            if (objDDL.value != 'New Connections' && $('#ddlProduct')[0].value == 'Mobile Self Fulfil' && $('#ddlCustType')[0].value == 'Corporate') {
                $('#tdchkSendEmail2OnePlan').fadeIn('slow');

                $('#tdTariffBTB').fadeIn('slow');
                $('#tdDiceAccountNumber').fadeIn('slow');
                $('#tdOrderReference').fadeIn('slow');
                $('#tdResignConnections').fadeIn('slow');
                $('#tdAddToFleetConnections').fadeIn('slow');
                $('#tdRevenue').fadeIn('slow');
                $('#tdHandset').fadeIn('slow');

                $('#tdCCNumber').hide();
                $('#tdMobileSelfFulfil').hide();
                $('#tdTariff').hide();
                $('#tdCorporateNewConnections').hide();
                $('#tdGroupNewConnction').hide();

                $('#tdGroup').hide();
                $('#tdCorporate').hide();
                $('#tdSubscription').hide();
                $('#tdContractTerm').hide();
                $('#tdServiceCost').hide();
                $('#tdContractStartDate').hide();
            }
            if (objDDL.value == 'Tariff Change' && $('#ddlProduct')[0].value == 'Mobile Self Fulfil') {
                $('#tdchkSendEmail2OnePlan').hide(); $('#chkSendEmail2OnePlan').val('false');

                $('#tdTariff').fadeIn('slow');
                //                $('#tdGroup').fadeIn('slow');
                //                $('#tdCorporate').fadeIn('slow');
                $('#tdGroup').hide();
                $('#tdGroupNewConnction').fadeIn('slow');
                $('#tdCorporate').hide();
                $('#tdCorporateNewConnections').fadeIn('slow');



                $('#tdDiceAccountNumber').fadeIn('slow');
                $('#tdSubscription').fadeIn('slow');
                $('#tdContractTerm').fadeIn('slow');
                $('#tdServiceCost').fadeIn('slow');
                $('#tdContractStartDate').fadeIn('slow');
                $('#tdCCNumber').fadeIn('slow');

                $('#tdTariffBTB').hide();

                $('#tdOrderReference').hide();
                $('#tdResignConnections').hide();
                $('#tdAddToFleetConnections').hide();
                $('#tdRevenue').hide();
                $('#tdHandset').hide();
               
            }

            if (objDDL.value == 'New Connections' && $('#ddlProduct')[0].value == 'Mobile Self Fulfil' && $('#ddlCustType')[0].value == 'Corporate') {
                $('#tdchkSendEmail2OnePlan').hide(); $('#chkSendEmail2OnePlan').val('false');

                $('#tdTariff').fadeIn('slow');
                //                $('#tdGroup').fadeIn('slow');
                //                $('#tdCorporate').fadeIn('slow');
                $('#tdGroup').hide();
                $('#tdGroupNewConnction').fadeIn('slow');
                $('#tdCorporate').hide();
                $('#tdCorporateNewConnections').fadeIn('slow');



                $('#tdDiceAccountNumber').fadeIn('slow');
                $('#tdSubscription').fadeIn('slow');
                $('#tdContractTerm').fadeIn('slow');
                $('#tdServiceCost').fadeIn('slow');
                $('#tdContractStartDate').fadeIn('slow');
                $('#tdCCNumber').fadeIn('slow');

                $('#tdTariffBTB').hide();

                $('#tdOrderReference').hide();
                $('#tdResignConnections').hide();
                $('#tdAddToFleetConnections').hide();
                $('#tdRevenue').hide();
                $('#tdHandset').hide();
                if (objDDL.value == 'New Connections') {
                    $('#tdMobileSelfFulfil').hide();
                }

            }

            break;
    }
}

function checkEMailformat(email) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
        return true;
    }
    else {
        return false;
    }
}
function checkValidPhoneNo(value) {
    //var rexp = /^((\(?0\d{5}\)?\s?\d{5})|(\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4})}})+$/;    
    var rexp = /^((\(?0\d{5}\)?\s?\d{5})|(\(?0\d{4}\)?\s?\d{3}\s?\d{3})|(\(?0\d{3}\)?\s?\d{3}\s?\d{4})|(\(?0\d{2}\)?\s?\d{4}\s?\d{4})}})|(\(?0\d{4}\)?\s?\d{5})+$/;
    if (value.search(rexp) == -1)
        return false;
    else
        return true;

}
function checkValidMobileNo(value) {
    return (/^(([0]{1})|([\+][4]{2}))([1]|[2]|[3]|[5]|[7]|[8]){1}\d{8,9}$/).test(value);
}
function checkEIN(value) {
    return (/^(\d{9})$/).test(value);
}
function checkPostCode(e) {
    var key;
    var keychar;

    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;
    keychar = String.fromCharCode(key);
    keychar = keychar.toLowerCase();

    // control keys
    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27))
        return true;
    // alphas and numbers
    else if ((("abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ ").indexOf(keychar) > -1))
        return true;
    else
        return false;
}
function onlyDigitsNPlus(e, field) {
    var key;
    if (window.event)
        key = window.event.keyCode;
    else if (e)
        key = e.which;
    else
        return true;

    var isControlKey = (key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27); //|| (key == 32)-emptyspace;

    return (key > 47 && key < 58) || isControlKey || key == 43;


}
function checkPostcode(postcodeControl) {
    var test;
    var valid = true;
    test = $('#' + postcodeControl)[0].value;
    PostCodeForAdded = $('#' + postcodeControl)[0].value;
    size = test.length;
    test = test.toUpperCase(); //Change to uppercase
    while (test.slice(0, 1) == " ") //Strip leading spaces
    {
        test = test.substr(1, size - 1); size = test.length
    }
    while (test.slice(size - 1, size) == " ") //Strip trailing spaces
    {
        test = test.substr(0, size - 1); size = test.length
    }
    if (test != '') {
        $('#' + postcodeControl)[0].value = test; //write back to form field
        if (size < 6 || size > 8) { //Code length rule
            //alert(test + " is not a valid postcode - wrong length");
            valid = false;
        }
        if (!(isNaN(test.charAt(0)))) { //leftmost character must be alpha character rule
            //alert(test + " is not a valid postcode - cannot start with a number");
            valid = false;
        }
        if (isNaN(test.charAt(size - 3))) { //first character of inward code must be numeric rule
            //alert(test + " is not a valid postcode - alpha character in wrong position");
            valid = false;
        }
        if (!(isNaN(test.charAt(size - 2)))) { //second character of inward code must be alpha rule
            //alert(test + " is not a valid postcode - number in wrong position");
            valid = false;
        }
        if (!(isNaN(test.charAt(size - 1)))) { //third character of inward code must be alpha rule
            //alert(test + " is not a valid postcode - number in wrong position");
            valid = false;
        }
        if (!(test.charAt(size - 4) == " ")) {//space in position length-3 rule
            //alert(test + " is not a valid postcode - no space or space in wrong position");
            test = test.substr(0, size - 3) + " " + test.substr(size - 3, size - 1);
            //valid = false;
        }
        count1 = test.indexOf(" "); count2 = test.lastIndexOf(" ");
        if (count1 != count2) {//only one space rule
            //alert(test + " is not a valid postcode - only one space allowed");
            valid = false;
        }
        if (test.length != -1) {
            var iChars = "!@#$%^&*()+=-[]\\\';,./{}|\":<>?";
            for (var i = 0; i < test.length; i++) {
                if (iChars.indexOf(test.charAt(i)) != -1) {
                    valid = false;
                    break;
                }
            }
        }
        $('#' + postcodeControl)[0].value = test; //write back to form field
        return valid;
    }
}
function onCreateNew() {
    $('#lblMsg')[0].InnerHTML = "";
    $('#divMain').fadeIn('slow');
    $('#divResult').hide();
}

function popitup(url) {
    newwindow = window.open(url, 'CustomerLookup', 'height=350,width=450,left=410,top=210');
    if (window.focus) { newwindow.focus() }
    return false;
}

function BAUOrderPopUp() {
    alert('List of Business as Usual orders which cannot be put through the L2S Programme: \n\n Stops/Starts \n Moving customers \n Shifts of lines/extensions \n Reconnection of ceased lines (i.e. for non-payment) or ceased in error \n TRC \n RCF/CNI \n Amendments to orders \n Failed orders \n Renumbers \n Select Services \n Fast track orders \n Replacement routers \n Engineer Visits for Broadband Install \n Broadband Resigns/Regrades/Upgrades(Unless upgrade to fibre)');
    return false;
}

function validate() {
    //to handle double clicks
    if ($('#hdnOnCreateClick')[0].value == '1') {
        alert('Please wait your request is being processed...');
        return false;
    }
    var found = false;
    var fieldValue = '';
    var errors = '';

//    $("#ddlCustType")[0].value == 'BT Business' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil'
    if ($("#ddlMobileSelfFulfil")[0].value == 'Tariff Change' || $("#ddlMobileSelfFulfilCorporate")[0].value == 'Tariff Change') {
        var ReqFields = ["ddlServiceSite", "txtYourEIN", "ddlCCtitle",
                    "txtCCFN", "txtCCLN", "txtCCMNo",
                    "txtEmail", "txtPostcode", "txtCmpyName", "txtBBTelNo",
                    "ddlProduct", "ddlMobile", "ddlSwitch",
                    "ddlAdditionalLines", "ddlBB", "ddlISND", "ddlLeasedConf",
                    "txtLeadInfo", "ddlCallslines", "ddlBBMovers",
                    "ddlCustType", "ddlVOIP", "ddlEtherwayEtherflow", "ddlMPLS",
                    "ddlWanLan", "ddlBTSport", "ddlMobileSelfFulfil", "ddlMobileSelfFulfilCorporate", "ddlTariff",
                    "txtDiceAccountNumber", "txtSubscription",
                    "ddlContractTerm", "txtServiceCost", "txtContractStartDate", "txtCorporateNewConns"];
        // "ddlGroup", "ddlCorporate",
    }
    else if ($("#ddlProduct")[0].value == 'Service Self-fulfil') {
        var ReqFields = ["ddlServiceSite", "txtYourEIN", "ddlCCtitle",
                    "txtCCFN", "txtCCLN", "txtCCMNo",
                    "txtEmail", "txtPostcode", "txtCmpyName", "txtBBTelNo",
                    "ddlProduct", "txtLeadInfo", "txtOrderReference", "txtRevenue", "txtQuantity",
                    "ddlContractTerm", "txtContractStartDate", "txtCorporateNewConns", "ddlServiceSelfFulfil"];
        // "ddlGroup", "ddlCorporate",
    }
    else {
        var ReqFields = ["ddlServiceSite", "txtYourEIN", "ddlCCtitle",
                    "txtCCFN", "txtCCLN", "txtCCMNo",
                    "txtEmail", "txtPostcode", "txtCmpyName", "txtBBTelNo",
                    "ddlProduct", "ddlMobile", "ddlSwitch",
                    "ddlAdditionalLines", "ddlBB", "ddlISND", "ddlLeasedConf",
                    "txtLeadInfo", "ddlCallslines", "ddlBBMovers",
                    "ddlCustType", "ddlVOIP", "ddlEtherwayEtherflow", "ddlMPLS",
                    "ddlWanLan", "ddlBTSport", "ddlMobileSelfFulfil", "ddlMobileSelfFulfilCorporate",
                    "ddlTariff", "txtTariffBTB", "txtDiceAccountNumber", "txtOrderReference", "txtResignConnections", 
                    "txtAddToFleetConnections", "txtRevenue", "txtHandset", "txtSubscription",
                    "ddlContractTerm", "txtServiceCost", "txtContractStartDate", "txtCorporateNewConns"];
        // "ddlGroup", "ddlCorporate",
    }

    jQuery.each(ReqFields, function () {
        fieldValue = $("#" + this)[0].value;
        if (this == 'ddlMobile') {
            if ($("#ddlProduct")[0].value == 'Mobile' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlSwitch') {
            if ($("#ddlProduct")[0].value == 'Switch' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlAdditionalLines') {
            if ($("#ddlProduct")[0].value == 'Additional lines and maintenance' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }

        else if (this == 'ddlBB') {
            if (($("#ddlProduct")[0].value == 'Broadband' && fieldValue == '') || ($("#ddlBB")[0].value == 'Upgrade to Fibre' && $("#ddlBB").find("option[value='Upgrade to Fibre']").attr('disabled') == true)) {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlBBMovers') {
            if (($("#ddlProduct")[0].value == 'Broadband-Movers' && fieldValue == '') || ($("#ddlBBMovers")[0].value == 'Upgrade to Fibre' && $("#ddlBBMovers").find("option[value='Upgrade to Fibre']").attr('disabled') == true)) {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlISND') {
            if ($("#ddlProduct")[0].value == 'ISDN' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlLeasedConf') {
            if (($("#ddlProduct")[0].value == 'Leased Lined /Private Circuits ( BTnet)' ||
                    $("#ddlProduct")[0].value == 'Conferencing and Inbound Services') && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        /*********************CR2478 Requirement For*******************/
        else if (this == 'ddlVOIP') {
            if ($("#ddlProduct")[0].value == 'VOIP' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        /*********************CR2478 Requirement For*******************/

        /*********************CR3398 Start*******************/
        else if (this == 'ddlEtherwayEtherflow') {
            if ($("#ddlProduct")[0].value == 'Etherway/Etherflow' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlMPLS') {
            if ($("#ddlProduct")[0].value == 'MPLS' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlWanLan') {
            if ($("#ddlProduct")[0].value == 'Wan/Lan' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlBTSport') {
            if ($("#ddlProduct")[0].value == 'BT Sport' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        /*********************CR3398 END*******************/
        else if (this == 'ddlCallslines') {
            if ($("#ddlProduct")[0].value == 'Calls/Lines Non BT Customers (winback)' && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtBBTelNo') {
            if (($("#ddlProduct")[0].value == 'Broadband-Movers' || $("#ddlProduct")[0].value == 'Broadband') && fieldValue == '') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        /* Mobile Self Fulfil changes start */
        else if (this == 'ddlMobileSelfFulfil') {
            if ($("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '' && $("#ddlCustType")[0].value == 'BT Business') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;

            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlMobileSelfFulfilCorporate') {
            if ($("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '' && $("#ddlCustType")[0].value == 'Corporate') {
                $('#' + this + 'Label').attr("class", "divTcellRequired");
                found = true;

            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }

        else if (this == 'ddlTariff') {
            if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }

        else if (this == 'txtTariffBTB') {
            if ($("#ddlCustType")[0].value == 'BT Business' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            } else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        /*
        else if (this == 'ddlGroup') {
        if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
        debugger;found = true;
        $('#' + this + 'Label').attr("class", "divTcellRequired");
        }
        else {
        $('#' + this + 'Label').attr("class", "divTcell");
        }
        }
        */
//        else if (this == 'ddlCorporate') {
//            if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && fieldValue == '') {
//                debugger;found = true;
//                $('#' + this + 'Label').attr("class", "divTcellRequired");
//            }
//            else {
//                $('#' + this + 'Label').attr("class", "divTcell");
//            }
//        }
        else if (this == 'txtCorporateNewConns') {
            if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtDiceAccountNumber') {
            if (($("#ddlCustType")[0].value == 'Corporate' || $("#ddlCustType")[0].value == 'BT Business') && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtOrderReference') {
            if ($("#ddlCustType")[0].value == 'BT Business' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtResignConnections') {
            if ($("#ddlCustType")[0].value == 'BT Business' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtAddToFleetConnections') {
            if ($("#ddlCustType")[0].value == 'BT Business' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtRevenue') {
            if ($("#ddlCustType")[0].value == 'BT Business' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtHandset') {
            if ($("#ddlCustType")[0].value == 'BT Business' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtSubscription') {
            if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'ddlContractTerm') {
            if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtServiceCost') {
            if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtContractStartDate') {
            if ($("#ddlCustType")[0].value == 'Corporate' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtCmpyName' || this == 'txtLeadInfo' || this == 'ddlCustType') {
            if (fieldValue == '') {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtCCMNo') {
            if ((fieldValue == '' && $("#ddlProduct")[0].value != 'Mobile Self Fulfil') || (fieldValue == '' && $("#ddlCustType")[0].value == 'Corporate' && $("#ddlMobileSelfFulfilCorporate")[0].value == 'New Connections' && $("#ddlProduct")[0].value == 'Mobile Self Fulfil')) {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
        else if (this == 'txtEmail' || this == 'txtPostcode' || this == 'txtCCFN' || this == 'txtCCLN') {
            if ((fieldValue == '' && $("#ddlProduct")[0].value != 'Mobile Self Fulfil')) {
                found = true;
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }

        else if (fieldValue == '' && $("#ddlProduct")[0].value != 'Mobile Self Fulfil') {
            $('#' + this + 'Label').attr("class", "divTcellRequired");
            found = true;
        }
        else {
            if ((this == 'txtEmail') && !checkEMailformat(fieldValue)) {
                found = true;
                errors += '<br>Invalid email address';
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else if (this == 'txtCCTNo' && !checkValidMobileNo(fieldValue)) {
                found = true;
                errors += '<br>Invalid telephone number';
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else if (this == 'txtPostcode' && !checkPostcode('txtPostcode')) {
                found = true;
                errors += '<br>Invalid Postcode';
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            else if (this == 'txtYourEIN' && !checkEIN(fieldValue)) {
                found = true;
                errors += '<br>Invalid EIN';
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            /*
            else if(this == 'txtMgrsEIN' && !checkEIN(fieldValue)){
            debugger;found = true;
            errors += '<br>Invalid manager EIN';
            $('#'+this+'Label').attr("class","divTcellRequired"); 
            }
            */
            else if (this == 'txtCCMNo' && !checkValidMobileNo(fieldValue)) {
                found = true;
                errors += '<br>Invalid Contact Number';
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }

            else {
                $('#' + this + 'Label').attr("class", "divTcell");
            }
        }
    });

    //validation for non-required fields
    var NonReqFields = ["txtSalesAgentEIN", "txtCCMNo"];
    jQuery.each(NonReqFields, function () {
        fieldValue = $("#" + this)[0].value;
        if (fieldValue != "") {
            $('#' + this + 'Label').attr("class", "divTcell");
            //            if(this == 'txtYourEmail' && !checkEMailformat(fieldValue)){
            //                debugger;found = true;
            //                errors += '<br>Invalid email address';
            //                $('#'+this+'Label').attr("class","divTcellRequired"); 
            //            }
            if (this == 'txtSalesAgentEIN' && !checkEIN(fieldValue)) {
                found = true;
                errors += '<br>Invalid sales agent EIN';
                $('#' + this + 'Label').attr("class", "divTcellRequired");
            }
            /*
            else if(this == 'txtCCMNo' && !checkValidMobileNo(fieldValue)){
            debugger;found = true;
            errors += '<br>Invalid Mobile number';
            $('#'+this+'Label').attr("class","divTcellRequired"); 
            } 
            */
        }

    });

    if (found) {
        $('#hdnOnCreateClick')[0].value = '0';
        $('#lblMsg')[0].innerHTML = 'Please enter required fields' + errors
        //$('#lblMsg')[0].innerHTML = 'Please enter required fields' + errors + fieldValue + "   " + $("#" + this)[0];
    }
    else {
        $('#lblMsg')[0].innerHTML = '';
        if ($('#txtCUG')[0].value == '' && $('#ddlProduct')[0].value != 'Mobile Self Fulfil') {
            if (!confirm('Are you sure you don’t want to add the Customer ID, this will speed up the allocation to a Sales Agent to deal with your lead, otherwise it may be delayed getting to Sales.\n\nOk = Yes, proceed without Customer ID\nCancel = No, Go back and enter Customer ID')) {
                $('#txtCUG')[0].focus();
                return false;
            }
        }
        $('#hdnOnCreateClick')[0].value = '1';
        $('#lblMsg')[0].innerHTML = '<img src="Images/iconLoading.gif" id="imgSI" /><span class="pleasewait">&nbsp;Please wait...</span>';
    }
    return !found;

    //javascript:__doPostBack('btnOK','')   
}
