CSMLEAPI.gridOptions = {enableClickEdit: true}

var tpe= function() {
    const TPE_FRAME_ID = '#technicalConfigFrame';

    
    var responseMethods = { "test": testMethod,
                            "mleMsgToTPE":mleMsgToTPE,
                            "setMLEAttributeValue" :  setMLEAttributeValue,
                            "getConfigTechnical" :getConfigTechnical,
                            "setValueTechnical" : setMLEAttributeValue,
                            "setParentValue" : setParentValue,
                            "refreshMle": refreshMLE


                            };
    
    var configUpdateMap = {}; 


    
    
    function setAllAttributesForConfig() {
        if (configUpdateMap[CS.Service.config[""].config.Id] !== undefined) {
            CS.setAttribute(configUpdateMap[CS.Service.config[""].config.Id].attName, configUpdateMap[CS.Service.config[""].config.Id].value);
            delete configUpdateMap[CS.Service.config[""].config.Id];

        }
    }

    
    function getConfigTechnical(params) {
        var scope = angular.element(jQuery('[class="container ng-scope"]')[0]).scope();
        mleMsgToTPE({data:scope.CsService.grid.data, method:params.value.method}, 'getConfigCallback');
    }

    
    function setParentValue(attributeName, attributeValue) {
        mleMsgToTPE({attributeName: attributeName, attributeValue:attributeValue}, 'setValueCommercial');



    }    
     
    
    function setMLEAttributeValue(params) {
        var attName = params.value.attributeName;
        var value = params.value.attributeValue;
        var updateAtt = {};
        updateAtt['attName'] = attName;
        updateAtt['value'] = value;
        var scope = angular.element(jQuery('[class="container ng-scope"]')[0]).scope();
        for (var i = 0; i < scope.CsService.grid.data.length; i++) {
            configUpdateMap[scope.CsService.grid.data[i].id] = updateAtt;
            scope.CsService.grid.data[i]._MLE_Saved_Status = 'Changed';
            scope.CsService.grid.data[i].original = {};

        }
        scope.$apply();
        CSMLEAPI.saveAsync();


    }                        
                            
    function receiveMessageMLE(event) {
        try {
            var response = JSON.parse(atob(event.data));
            var configId = CS.params.configId;
            if (response['recipient'] === configId || response['recipient'] === 'all') {
                var method = response['method'];
                if (method && responseMethods[method]) {
                    responseMethods[method](response);


                }
            }
        } catch (err) {
            console.log('***** Ignoring msg:' + JSON.stringify(err));

        }
    }

    
    function mleMsgToTPE(message, methodName) {
        var urlToParse = window.location.search;
        var resultParams = parseQueryString(urlToParse);
        var defId = resultParams['productDefinitionId'];
        var msg = btoa(JSON.stringify({ value: message, sender: defId, recipient: 'TPE', method: methodName }));
        window.parent.postMessage(msg, '*');
    }

    
    function testMethod(params) {
        console.log('MLE Test method, params:' +JSON.stringify(params));
    }

    
    //if one mle element is changed function returns true
    function checkForChange() {
        var scope = angular.element(jQuery('[class="container ng-scope"]')[0]).scope();
        var mleData = scope.CsService.grid.data;
        var flag = false;

        
        for (var i = 0; i < mleData.length; i++) {
            if (mleData[i]._MLE_Saved_Status == 'Changed')
                flag = true;

        }
        if(flag) {
            var urlToParse = location.search;
            var resultParams = parseQueryString(urlToParse);
            mleMsgToTPE(resultParams['definitionName'], 'mleIsChanged');

        }
    }

    
    function aggregateMleAttributeValues() {
        var scope = angular.element(jQuery('[class="container ng-scope"]')[0]).scope();
        var mleData = scope.CsService.grid.data;

        if(mleData && mleData.length)

        {        
            var objectKeys = Object.keys(mleData[0]);
            var aggegateAttributesMap = {}
            var onlyAttributeKeys = [];
            objectKeys.forEach(function (objectKey) {
                if(objectKey !== ('id') && objectKey !== 'original' && objectKey !== '_MLE_Saved_Status' && objectKey !== '_MLE_Validate')
                    onlyAttributeKeys.push({'name': objectKey, 'type': mleData[0].original[objectKey]!==undefined? mleData[0].original[objectKey].definition.cscfga__Data_Type__c:'String'});

            });            
            var aggregateValue;
            onlyAttributeKeys.forEach(function(objectKey) {
                if(objectKey.type === 'String')
                     aggregateValue = [];
                else if(objectKey.type === 'Integer' || objectKey.type === 'Decimal')
                     aggregateValue = 0;

                mleData.forEach(function(mleRow) {
                    if(objectKey.type === 'String')
                        aggregateValue.push(mleRow[objectKey.name].cscfga__Value__c);
                    else if(objectKey.type === 'Integer' || objectKey.type === 'Decimal')
                        aggregateValue += parseFloat(mleRow[objectKey.name].cscfga__Value__c);

                });

                aggegateAttributesMap[objectKey.name] = aggregateValue;
            });

        }
        mleMsgToTPE(aggegateAttributesMap, 'mleAggregatedValues');
    }

        
    var parseQueryString = function(urlToParse) {
          var urlParams = {};
          urlToParse.replace(
            new RegExp("([^?=&]+)(=([^&]*))?", "g"),
            function($0, $1, $2, $3) {
              urlParams[$1] = $3;


            }
          ); 
          return urlParams;
    };
     var setMLEButtons = function () {
        var urlToParse = window.location.search;
        var resultParams = parseQueryString(urlToParse);
        if (resultParams['CommercialConfigurationId'] !== undefined) { 
            var cancelButtons = jQuery( "button[ng-click='cancel()']")
            for(var i = 0; i<cancelButtons.length; i++) {
               jQuery(cancelButtons[i]).unbind('click').attr('onclick', '').attr('ng-click','').on('click',cancelConfiguration).text('Cancel');

            }
            jQuery("button[ng-click='showColumnSelector = true']").hide();          

        }
    };

    
    function sendOnLoadRequest() {
        var urlToParse = location.search;
        var resultParams = parseQueryString(urlToParse);
        if (resultParams['definitionName']) {
            mleMsgToTPE(resultParams['definitionName'], 'onLoadTechnical');

        }
    }

    
    function onSave() {
        var urlToParse = location.search;
        var resultParams = parseQueryString(urlToParse);
        if (resultParams['definitionName']) {
            mleMsgToTPE(resultParams['definitionName'], 'mleSaved');

        }
    }
    function onAdd() {
        updateQuantity();        
        setTimeout(function() {
            var urlToParse = location.search;
            var resultParams = parseQueryString(urlToParse);
            if (resultParams['definitionName']) {
                mleMsgToTPE(resultParams['definitionName'], 'mleAddedRow');

            }
        }, 350);
    }
    function cancelConfiguration() {
        var urlToParse = window.location.search;
        var resultParams = parseQueryString(urlToParse);
        var defId = resultParams['productDefinitionId'];        
        mleMsgToTPE({defId:defId}, 'onCancelConfiguration');
    }
    function updateQuantity() {
        var scope = angular.element(jQuery('[class="container ng-scope"]')[0]).scope();
        var mleData = scope.CsService.grid.data;
        var urlToParse = window.location.search;
        var resultParams = parseQueryString(urlToParse);
        var defId = resultParams['productDefinitionId'];
        mleMsgToTPE({defId:defId, quantity:mleData.length}, 'onUpdateMLEQuantity');
    }
    function onRemove() {
        updateQuantity();
        setTimeout(function() {
            var scope = angular.element(jQuery('[class="container ng-scope"]')[0]).scope();
            var mleData = scope.CsService.grid.data;
            var flag = false;
            for (var i = 0; i < mleData.length; i++) {
                if (mleData[i]._MLE_Saved_Status == 'Changed')
                    flag = true;

            }
            if(!flag) {
                var urlToParse = location.search;
                var resultParams = parseQueryString(urlToParse);
                mleMsgToTPE(resultParams['definitionName'], 'mleSaved');

            }
        }, 350);
    }
    var loadCounter= 0;
    function checkIfMLELoaded(){
         console.log('Waiting for MLE');
         if (jQuery('[class="ui-widget-content slick-row even"]').length > 0) {
            setTimeout(sendOnLoadRequest, 300);
         } else if(loadCounter<2){
             loadCounter++;
             setTimeout(checkIfMLELoaded, 500);



         }
         else {
            setTimeout(sendOnLoadRequest, 300);
         }
    }    
     
    var setCommercialConfigurationId = function() {
        var urlToParse = location.search;
        var resultParams = parseQueryString(urlToParse);
        if (resultParams['CommercialConfigurationId'] && CS.getAttributeValue('Configuration_Id_0') == '') {
            CS.setAttribute("Configuration_Id_0", resultParams['CommercialConfigurationId']);

        }
    }
    function refreshMLE(params){
        CSMLEAPI.refreshGridUI();
    }

    
    var initNonCommercialConfiguration = function() {
        setCommercialConfigurationId();
        setAllAttributesForConfig();
        setTimeout(checkForChange, 350);
    }

    
    var initialLoad = false;

    
    var initListeners = function() {
        setMLEButtons();
        checkIfMLELoaded();
        console.log('******** Registering non commercial listeners [MLE]');        
        window.addEventListener('message', receiveMessageMLE);
    }
    var svButton = jQuery('button[ng-click="save()"]');
    svButton[0].addEventListener("click", onSave);
    var addButton = jQuery('button[ng-click="addNew()"]');
    addButton[0].addEventListener("click", onAdd);
    var delButton = jQuery('button[ng-click="delete()"]');
    delButton[0].addEventListener("click", onRemove);    
    return {
        "initListeners": initListeners,
        "setMLEAttributeValue" : setMLEAttributeValue,
        "responseMethods" : responseMethods,
        "setParentValue" : setParentValue,
        "mleMsgToTPE" : mleMsgToTPE,
        "getConfigTechnical":getConfigTechnical,
        "initNonCommercialConfiguration" : initNonCommercialConfiguration,
        "checkForChange" : checkForChange,
        "aggregateMleAttributeValues":aggregateMleAttributeValues,
        "initialLoad" : initialLoad,
        "refreshMLE":refreshMLE
    }
}
jQuery(document).ready(function(){
	jQuery('body').css('background-color', '#fff');
    window.SPM = tpe();
    SPM.initListeners();
});
