// Create a mapping to match your solution's components
// This means we can avoid too much hard-coding below and can more easily reuse code
let configurationGuidUpdated = '';

var BT_NET_CONST = {
	solution: 'BTnet Solution', //Main SCHEMA name for plugin creation
	//solution: 'Service Plan and Solutions', //Main SCHEMA name for plugin creation
	mainComponent: 'BTnet',
	siteAccess: 'Site Access',
	portSpeed: 'Port Speed',
	managedCPE: 'Managed CPE'
};

//Register the BTNet Plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', async function() {
		await CS.SM.registerPlugin(BT_NET_CONST.solution)
			.then(plugin => {
				console.log("[CS_BTNetServicePlugin]  Plugin registered for BTNet");
				BTNetHooks(plugin);
			});
	});
}

async function displayPopup(configurationGuid){
    console.log(" [CS_BTNetServicePlugin]  Rendering, calling from BTNet Site Address");
    let urlString = '';
    urlString += '<div id="BtnetIframe" class="demo-only" height="100%">';
        urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
            urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
                urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
                    urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_BTnet_QuickCreate?cs_product=btnet">';
                urlString += '</div>';
            urlString += '</div>';
       	urlString += '</section>';
    urlString += '</div>';
        
    configurationGuidUpdated = configurationGuid;
    return urlString;
}

/*************** 
 * 
 * 
 * Start of UI Hooks
 * 
 * 
 * ****************/
function BTNetHooks(BTNetPlugin) {
    BTNetPlugin.beforeConfigurationAdd = async function(component, configuration) {
    	let solution = await CS.SM.getActiveSolution();
    	var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
    	if (component.name == BT_NET_CONST.siteAccess) {
    		if (mainConfiguration.getAttribute('Contract Term').value.length == 0) {
    		    CS.SM.displayMessage('Can not add Site Access because Contract Term on BTnet component is empty', 'error');
				return false;
    		}
    	}
    	
    	return Promise.resolve(true);
    };

	BTNetPlugin.afterConfigurationAdd = async function(component, configuration) {
        let solution = await CS.SM.getActiveSolution();
        let componentNumber = 0;
        if (component.name == BT_NET_CONST.siteAccess) {
            for (let allConfigs of Object.values(component.schema.configurations)) {
                let num = allConfigs.getAttribute('ComponentID').value;
                if (num > componentNumber) {
                    componentNumber = num;
                }
            }
            ++componentNumber;
            
            var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
            let contractTerm = mainConfiguration.getAttribute('Contract Term').value;
            updateConfigurationAttributeValue('Contract Term', contractTerm, contractTerm, true, solution, configuration.guid, true);
            updateConfigurationAttributeValue('ComponentID', componentNumber, componentNumber, true, solution, configuration.guid, true);
            
            for (let componentLoop of Object.values(solution.components)) {
    			if (componentLoop.name == BT_NET_CONST.portSpeed) {
					let conf = componentLoop.createConfiguration();
					componentLoop.addConfiguration(conf).then(addedConfiguration => 
					    updateConfigurationAttributeValue('ComponentID', componentNumber, componentNumber, true, solution, conf.guid, true)
					);					
    			}
    			
    			if (componentLoop.name == BT_NET_CONST.managedCPE) {
					let conf = componentLoop.createConfiguration();
					componentLoop.addConfiguration(conf).then(addedConfiguration => 
					    updateConfigurationAttributeValue('ComponentID', componentNumber, componentNumber, true, solution, conf.guid, true)
				    );
    			}
            }
        }
        
        return Promise.resolve(true);
	};
	
	BTNetPlugin.afterConfigurationDelete = async function(component, configuration) {
	    let solution = await CS.SM.getActiveSolution();
	    if (component.name == BT_NET_CONST.siteAccess) {
            let componentNumber = configuration.getAttribute('ComponentID').value;
            
            let components = solution.getComponents();
            Object.values(components['components']).forEach(compo => {
                if (compo.name == BT_NET_CONST.portSpeed || compo.name == BT_NET_CONST.managedCPE) {
                    Object.values(compo.schema.configurations).forEach(config => {
                        if (config.getAttribute('ComponentID').value == componentNumber) {
                            compo.deleteConfiguration(config.guid);
                        }
                    });
                }
            });
            
            if (Object.values(component.schema.configurations).length == 0) {
                var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
                updateConfigurationAttributeValue('Contract Term', '', '', false, solution, mainConfiguration.guid, true);
            }
        }
	    return Promise.resolve(true);
    };
    
    BTNetPlugin.afterAttributeUpdated = async function(component, configuration, attribute, oldValueMap) {
        let solution = await CS.SM.getActiveSolution();
        if (component.name == BT_NET_CONST.mainComponent) {
            if (attribute.name == 'Contract Term') {
                for (let componentLoop of Object.values(solution.components)) {
        			if (componentLoop.name == BT_NET_CONST.siteAccess) {
        			    for (let conf of Object.values(componentLoop.schema.configurations)) {
        			        componentLoop.deleteConfiguration(conf.guid);
        			    }
        			}
        			if (componentLoop.name == BT_NET_CONST.portSpeed) {
        			    for (let conf of Object.values(componentLoop.schema.configurations)) {
        			        componentLoop.deleteConfiguration(conf.guid);
        			    }
        			}
        			if (componentLoop.name == BT_NET_CONST.managedCPE) {
        			    for (let conf of Object.values(componentLoop.schema.configurations)) {
        			        componentLoop.deleteConfiguration(conf.guid);
        			    }
        			}
                }
                
                if (attribute.value) {
                    console.log("[CS_BTNetServicePlugin] Adding configs");
                    for (let componentLoop of Object.values(solution.components)) {
                        if (componentLoop.name == BT_NET_CONST.siteAccess) {
                            let conf = componentLoop.createConfiguration();
                            componentLoop.addConfiguration(conf).then(addedConfiguration => console.log('[CS_BTNetServicePlugin] New config added'));
                        }
                    }
                }                
            }
        }
        
        if (component.name == BT_NET_CONST.siteAccess) {
            if (attribute.name == 'Access Bearer') {
                let componentNumber = configuration.getAttribute('ComponentID').value;
                for (let componentLoop of Object.values(solution.components)) {
        			if (componentLoop.name == BT_NET_CONST.portSpeed) {
    					for (let conf of Object.values(componentLoop.schema.configurations)) {
							if (conf.getAttribute('ComponentID').value == componentNumber) {
							    updateConfigurationAttributeValue('Bearer Speed', configuration.getAttribute('Access Type').value, configuration.getAttribute('Access Type').value, true, solution, conf.guid, true);
							    updateConfigurationAttributeValue('Contract Term', configuration.getAttribute('Contract Term').value, configuration.getAttribute('Contract Term').value, true, solution, conf.guid, true);
							    updateConfigurationAttributeValue('Geography', configuration.getAttribute('Geography').value, configuration.getAttribute('Geography').value, true, solution, conf.guid, true);
							}
						}
        			}
        			
        			if (componentLoop.name == BT_NET_CONST.managedCPE) {
    					for (let conf of Object.values(componentLoop.schema.configurations)) {
							if (conf.getAttribute('ComponentID').value == componentNumber) {
 							    updateConfigurationAttributeValue('Contract Term', configuration.getAttribute('Contract Term').value, configuration.getAttribute('Contract Term').value, true, solution, conf.guid, true);
							}
						}
        			}
                }
            }

            if (attribute.name == 'ECC From OpenReach') {
                if (attribute.value == 'Yes') {
                    updateConfigurationAttributeValue('Excess Construction Charge',
                    '',
                    '',
                    false,
                    solution,
                    configuration.guid,
                    true);                    
                }
                else {
                    updateConfigurationAttributeValue('Excess Construction Charge', 
                        configuration.getAttribute('Excess Construction Charge Default Popup Value').value,
                        configuration.getAttribute('Excess Construction Charge Default Popup Value').value,
                        true,
                        solution,
                        configuration.guid,
                        false);
                }
            }
        }
        
        if (component.name == BT_NET_CONST.portSpeed) {
            if (attribute.name == 'Port Speed') {
                let bandwidthVal;
                let inputMap={};
                inputMap["piId"] = attribute.value;
                inputMap["action"] = 'CS_BTNetBandwidthForSolutionConsole';
				let currentBasket = await CS.SM.getActiveBasket();
                let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
                if(result["DataFound"] == true){
                    bandwidthVal = result["Bandwidth"];
                }
                
                updateConfigNameBTNetPortSpeed(configuration, solution);
                
                let componentNumber = configuration.getAttribute('ComponentID').value;
                for (let componentLoop of Object.values(solution.components)) {
        			if (componentLoop.name == BT_NET_CONST.managedCPE) {
    					for (let conf of Object.values(componentLoop.schema.configurations)) {
							if (conf.getAttribute('ComponentID').value == componentNumber) {
							    updateConfigurationAttributeValue('Bandwidth', bandwidthVal, bandwidthVal, true, solution, conf.guid, true);
							    updateConfigurationAttributeValue('Managed CPE', '', '', false, solution, conf.guid, true);
							    updateConfigurationAttributeValue('Managed CPE Connection', '', '', false, solution, conf.guid, true);
                                updateConfigNameBTNetManagedCPEOnPortSpeedUpdate(conf, solution);
							}
    					}
        			}
                }
            }
        }

        if (component.name == BT_NET_CONST.managedCPE) {
            if (attribute.name == 'Managed CPE') {
                updateConfigNameBTNetManagedCPE(configuration, solution);
            }
        }
        
        /*CS.SM.getActiveBasket().then(basket => {
            return basket.saveSolution(solution.id).then(function(resolve, reject) {
                console.log('Solution saved!');
            }).catch(function(err) {
                if (err.message === 'No active solution') {
                    console.log('No active solution!');
                }
            });
        }).catch(value => {
            CS.SM.displayMessage(value, 'error');
        });*/

        return Promise.resolve(true);
	};
}

// AFTER NAVIGATE
async function afterNavigateBTNet(solution, currentComponent, previousComponent) {
    console.log('[CS_BTNetServicePlugin] AfterNavigateBTNet', solution, currentComponent, previousComponent);

    correctProfile = 'true';
	if (correctProfile == null) {
		var currentBasket = await CS.SM.getActiveBasket();
		var result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
			'action': 'isProfileQueuedRemote'
		});
		correctProfile = result["goodProfile"];
		console.log(result);
	}
	if (currentComponent.name == BT_NET_CONST.mainComponent) {
	}

	if (currentComponent.name == BT_NET_CONST.siteAccess) {		
	}

	if (currentComponent.name == BT_NET_CONST.portSpeed) {
	}

    if (currentComponent.name == BT_NET_CONST.managedCPE) {
	}   

    return true;
}

/*************** 
 * 
 * 
 * Start of UI labels rendering for Site Access
 * 
 * 
 * ****************/

var btnetSections = {
    'Excess Construction Charge' : 'Excess Construction Charge, reference to main section',
    'SIP Enabled' : 'SIP Enabled, reference to main section'
}

// pay attention between "reference to main section" labels!!!!!!

var btnetMainSections = {
    'Excess Construction Charge, reference to main section' : 'ECC & Lead Times',
    'SIP Enabled, reference to main section' : 'Optional Extras'
}

formatPcUIForBTNet = async() => {

    console.log("[CS_BTNetServicePlugin] Rendering UI sections");

    let labels = document.querySelectorAll('label');
    
    let updatedParentElements = [];
    if (labels) {
        let configuredSections = Object.keys(btnetSections);
        for (let i = 0 ; i< labels.length; i++) {
            if (configuredSections.includes(labels[i].innerText)) {
                let cs = labels[i].parentElement.parentElement.getElementsByClassName('cs-custom-section');
                if (cs && cs.length > 0 && !updatedParentElements.includes(labels[i].parentElement.parentElement)) {
                    updatedParentElements.push(labels[i].parentElement.parentElement);
                }
            }
        }

        for (let i = 0 ; i< labels.length; i++) {
            if (configuredSections.includes(labels[i].innerText)) {
                if (updatedParentElements.includes(labels[i].parentElement.parentElement)) {
                    continue;
                }
                
                let configuredMainSections = Object.keys(btnetMainSections);
                if (configuredMainSections.includes(btnetSections[labels[i].innerText])) {
                    let mainHeader = document.createElement('h3');
                    mainHeader.classList.add('cs-custom-section');
                    mainHeader.classList.add('slds-size_5-of-5');
                    mainHeader.classList.add('modal-header');
                    mainHeader.classList.add('slds-modal__header');
                    mainHeader.classList.add('slds-align_absolute-center');
                    mainHeader.innerText = btnetMainSections[btnetSections[labels[i].innerText]];
                    mainHeader.style.height='15px';
                    mainHeader.style.justifyContent = 'left';
                    mainHeader.style.fontWeight = 'bold';
                    mainHeader.style.marginTop = '15px';
                    mainHeader.style.marginBottom = '15px';
                    labels[i].parentElement.parentElement.insertBefore(mainHeader,labels[i].parentElement);
                }
            }
        }
    }
    return Promise.resolve(true);
}

function updateConfigNameBTNetOnMainComponent(_config, _solution, _accessType) {    
	let siteName = 'BTnet - Primary Site: ' + _config.getAttribute('Address').value;

    console.log("[CS_BTNetServicePlugin] Setting up new config name on site access: " + siteName);
	
	updateConfigurationAttributeValue('Config Name',
                                         siteName + ' [' +  _accessType + ']',
                                         siteName + ' [' +  _accessType + ']',
                                         false,
                                         _solution,
                                         _config.guid,
                                         true);

    _config.configurationName = siteName + ' [' +  _accessType + ']';
    console.log(_config);

    let componentNumber = _config.getAttribute('ComponentID').value;

    let components = _solution.getComponents();
    Object.values(components['components']).forEach(compo => {
        if (compo.name == BT_NET_CONST.portSpeed || compo.name == BT_NET_CONST.managedCPE) {
            Object.values(compo.schema.configurations).forEach(conf => {
                if (conf.getAttribute('ComponentID').value == componentNumber) {
                    updateConfigurationAttributeValue('Config Name',
                                         siteName,
                                         siteName,
                                         false,
                                         _solution,
                                         conf.guid,
                                         true);

                    conf.configurationName = siteName;
                }
            });
        }
    });
}

function updateConfigNameBTNetPortSpeed(_config, _solution) {    
	let siteName = _config.getAttribute('Config Name').value;

    if (_config.getAttribute('Port Speed').displayValue) {
        siteName +=  ' [' + _config.getAttribute('Port Speed').displayValue + ']';
    } else {
        siteName = siteName.substring(0, siteName.indexOf("["));
    }

    console.log("[CS_BTNetServicePlugin] Setting up new config name for Port Speed: " + siteName);
	
	updateConfigurationAttributeValue('Config Name',
                                         siteName,
                                         siteName,
                                         false,
                                         _solution,
                                         _config.guid,
                                         true);

    _config.configurationName = siteName;
    console.log(_config);
}

function updateConfigNameBTNetManagedCPE(_config, _solution) {
	let siteName = _config.getAttribute('Config Name').value

    if (_config.getAttribute('Managed CPE').displayValue) {
        siteName +=  ' [' + _config.getAttribute('Managed CPE').displayValue + ']';
    } else {
        siteName = siteName.substring(0, siteName.indexOf("["));
    }

    console.log("[CS_BTNetServicePlugin] Setting up new config name for Managed CPE: " + siteName);

    updateConfigurationAttributeValue('Config Name',
                                            siteName,
                                            siteName,
                                            false,
                                            _solution,
                                            _config.guid,
                                            true);

    _config.configurationName = siteName;
    console.log(_config);
}

function updateConfigNameBTNetManagedCPEOnPortSpeedUpdate(_config, _solution) {
	let siteName = _config.getAttribute('Config Name').value

    if (siteName) {
        if (siteName.includes("[")){
            siteName = siteName.substring(0, siteName.indexOf("["));
        }
    }

    console.log("[CS_BTNetServicePlugin] Setting up new config name for Managed CPE: " + siteName);

    updateConfigurationAttributeValue('Config Name',
                                            siteName,
                                            siteName,
                                            false,
                                            _solution,
                                            _config.guid,
                                            true);

    _config.configurationName = siteName;
    console.log(_config);
}