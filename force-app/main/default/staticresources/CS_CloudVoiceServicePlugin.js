var imported = document.createElement('script');
imported.src = '/resource/1491499237000/cscfga__jQuery_min';
document.head.appendChild(imported);

var ErrorValidationMap = new Map();
var lanTypeChanged = false;
var gigaByteChanged = false;
var siteAccessConfig = false;
var additionalPortChanged = false;
var userProfile=null;

var globalAddonPowerMap={};
var globalAddonPortMap={};
var globalAddonPortProvidedMap={};
var globalAddonOutletMap={};
var globalAddonUFPRequiredMap={};

var globalAddonMap={};
var globalUnlimitedAddonMap={};
var globalAccountID='';
var currentConfigGuidPDF;
var addonGigaMap= {};   //input:addon price item ID, output: true/false


//Cloud Voice Service plugin code
//console.log('Loading Cloud Service plugin code')
if (!CS || !CS.SM) {
    console.log('not loaded');
    throw Error('Solution Console Api not loaded?');
}

// Create a mapping to match your solution's components
// This means we can avoid too much hard-coding below and can more easily reuse code
var CLOUD_VOICE = {
    solution: 'Cloud Voice', //Main SCHEMA name for plugin creation
    siteAccess: 'Site', //Component name for use in functions
    UFP: 'UFPs',
    cabling: 'Cabling',
    CPE: 'CPEs',
    LAN: 'LAN',
    SIPTswitch: 'Switch'
};

var Sections = {
    'Solution' : 'PUHERATKAISU VAKIO EXTENSION',
    'Customer Account' : 'ACCOUNT DETAILS',
    'Company Name' : 'SHIPPING DETAILS',
    'List price monthly charge' : 'FINANCIAL INFORMATION',
    'Campaign Name' : 'DISCOUNT DETAILS'
}

//Register the Cloud Voice Plugin
if (CS.SM.registerPlugin) {
    window.document.addEventListener('SolutionConsoleReady', async function() {
    await CS.SM.registerPlugin(CLOUD_VOICE.solution)
            .then(plugin => {
                console.log("[CS_CloudeVoiceServicePlugin] Plugin registered for Cloud Voice");
                cloudVoiceHooks(plugin);
            });
    });
}

//Add event every time a solution is clicked
window.document.addEventListener('SolutionSetActive', function(e) {
    console.log('SolutionActive', e);

	setTimeout ( () => {
		CloudVoiceCustomComponent.drawComponent();
		}, 100 
	);
}); 

 
/**
 * Add solution buttons
 */
console.log('Loaded buttons plugin');

let buttons = {};

buttons.setButtons = function() {
    let elems = document.getElementsByClassName('slds-page-header__row');
    if (elems && elems.length > 0) {
       try {
			
			let row3 = elems[0];
            let btn3 = document.createElement('button');
            btn3.innerHTML = 'Back to Opportunity';
            btn3.classList = 'slds-button slds-button_neutral';
            btn3.onclick = buttons.backToOpportunity;
			
			row3.append(btn3); 
			
			let row4 = elems[0];
			let btn4 = document.createElement('button');
            btn4.innerHTML = 'Financing';
            btn4.classList = 'slds-button slds-button_neutral';
            btn4.onclick = buttons.goToFinance;

            row4.append(btn4);
			
			let row5 = elems[0];
			let btn5 = document.createElement('button'); 
            btn5.innerHTML = 'Deal management';
            btn5.classList = 'slds-button slds-button_neutral';
            btn5.onclick = buttons.goToDealManagement;

            row5.append(btn5);
            
            let row2 = elems[0];
            let btn2 = document.createElement('button');
            btn2.innerHTML = 'Sync Basket';
            btn2.classList = 'slds-button slds-button_neutral';
			btn2.onclick = buttons.syncBasket;
			
            row2.append(btn2);
            
            let row1 = elems[0];
            let btn1 = document.createElement('button');

            btn1.innerHTML = 'Take Ownership';
            btn1.classList = 'slds-button slds-button_neutral';
			btn1.onclick = buttons.takeOwnership;
			
            row1.append(btn1);
            
            let row = elems[0];
            let btn = document.createElement('button');
            btn.innerHTML = 'Send to Service';
            btn.classList = 'slds-button slds-button_neutral';
			btn.onclick = buttons.sendToService;
			
            row.append(btn);
            
            let row6 = elems[0];
			let btn6 = document.createElement('button');
            btn6.innerHTML = 'Chatter';
            btn6.classList = 'slds-button slds-button_neutral';
            btn6.onclick = buttons.openChatterComponent;

            row6.append(btn6);

          } catch(err) {
            console.error(err);
            clearInterval(buttons.interval);
        }
        clearInterval(buttons.interval);
    }
}

buttons.sendToService = async function() {

    let basket = await CS.SM.getActiveBasket();
    let inputMap={'basketId' : basket.basketId, 'action' : 'CS_SendToService'};
    let result= await basket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
    

    if(result != null) {
		if (result['status'] == 'ok') {
			CS.SM.displayMessage(result['message'], 'info');
		}
        
		if (result['status'] == 'error') {
			CS.SM.displayMessage(result['message'], 'error');
		}
    }
}

buttons.takeOwnership = async function() {
    
    let basket = await CS.SM.getActiveBasket();
    let inputMap={'basketId' : basket.basketId, 'action' : 'CS_TakeOwnership'};
    let result= await basket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
    
    if(result != null) {
		if (result['status'] == 'ok') {
			CS.SM.displayMessage(result['message'], 'info');
		}
        
		if (result['status'] == 'error') {
			CS.SM.displayMessage(result['message'], 'error');
		}
    }
}

buttons.syncBasket = async function() {

    let basket = await CS.SM.getActiveBasket();
    let inputMap={'basketId' : basket.basketId, 'action' : 'CS_SynchronizeWithOpportunity'};
    let result= await basket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
    let syncResult;

    if(result != null) {
        syncResult = JSON.parse(result['status']);
		
		return await CS.SM.getActiveSolution().then(solution => {

        return CS.SM.getActiveBasket().then(basket => {

            return basket.saveSolution(solution.id).then(function(resolve, reject) {
				if (syncResult['status'] == 'ok') {
					CS.SM.displayMessage('Sync Successful', 'info');
					window.location.href = syncResult['redirectURL'];
				}
				if (syncResult['status'] == 'error') {
					CS.SM.displayMessage(syncResult['text'], 'error');
				}
            }).catch(function(err) {
            if (err.message === 'No active solution') {
                    CS.SM.displayMessage(err.message ? err.message : err, 'error');
                }
            });
        }).catch(value => {
            CS.SM.displayMessage(value, 'error');
        });
		}).catch(err => {
			if (err.message === 'No active solution found!'){
				return CS.SM.getActiveBasket().then(basket => {
				    CS.SM.displayMessage('Select a Solution', 'error');   
				});
			}else{
				CS.SM.displayMessage(err.message ? err.message : err, 'error');
			}
			return;
		});
    }
}

buttons.openChatterComponent = async function() {

     let elem = document.getElementById('chatterFeedChatterFeed');
     let basket = await CS.SM.getActiveBasket();
     let inputMap={'basketId' : basket.basketId, 'action' : 'CS_LookupOpportunitySolutionConsole'};
     let result= await  basket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
     let opportunityId;

     if(result != null) {
         opportunityId = result['opportunityId'];
     }

     if (elem) {
         elem.style.display = 'block';
     } else {

         let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
         	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
         	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
         	urlString += '<div class="slds-modal__header">';
         	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
         	urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
         	urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
         	urlString +='</svg>';
         	urlString += '</button>';
         	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
         	urlString +='Chatter </h2>';
         	urlString += '</div>';
         	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
         	urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_SolutionConsoleChatterFeed?opportunityId='+opportunityId+'"';
         	urlString += '</div>';
         	urlString += '</div>';
         	urlString += '</section>';
         	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
         	urlString += '</div>';

         const elems = document.getElementsByClassName('slds-page-header__row');

         if (elems && elems.length > 0) {
            try {
                 let divContainer = document.createElement('div');
                 divContainer.innerHTML = urlString;

                 document.body.append(divContainer);

             } catch (err) {
                 CS.SM.displayMessage('Error loading Chatter page!', 'error');
             }
         }
     }
 }

buttons.backToOpportunity = async function() {

    let basket = await CS.SM.getActiveBasket();
    let inputMap={'basketId' : basket.basketId, 'action' : 'CS_LookupOpportunitySolutionConsole'};
    let result= await basket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
    let opportunityId;

    if(result != null) {
        opportunityId = result['opportunityId'];
    }

    return await CS.SM.getActiveSolution().then(solution => {

        return CS.SM.getActiveBasket().then(basket => {

            return basket.saveSolution(solution.id).then(function(resolve, reject) {
                window.location.href = '/' + opportunityId;
            }).catch(function(err) {

                if (err.message === 'No active solution') {
                    window.top.location = '/' + opportunityId;
                }
            });
        }).catch(value => {
            CS.SM.displayMessage(value, 'error');
        });
    }).catch(err => {
        if (err.message === 'No active solution found!'){
            return CS.SM.getActiveBasket().then(basket => {
                window.top.location = '/' + opportunityId;
            });
        }else{
            CS.SM.displayMessage(err.message ? err.message : err, 'error');
        }
        return;
    });
}

buttons.goToFinance = async function() {

    return await CS.SM.getActiveSolution().then(solution => {

        return CS.SM.getActiveBasket().then(basket => {

            return basket.saveSolution(solution.id).then(function(resolve, reject) {
                window.location.href = '/apex/c__CloudVoiceItemList?basketId=' + basket.basketId;
            }).catch(function(err) {

                if (err.message === 'No active solution') {
                    window.top.location = 'apex/c__CloudVoiceItemList?basketId=' + basket.basketId;
                }
            });
        }).catch(value => {
            CS.SM.displayMessage(value, 'error');
        });
    }).catch(err => {
        if (err.message === 'No active solution found!'){
            return CS.SM.getActiveBasket().then(basket => {
                window.top.location = 'apex/c__CloudVoiceItemList?basketId=' + basket.basketId;
            });
        }else{
            CS.SM.displayMessage(err.message ? err.message : err, 'error');
        }
        return;
    });
}

buttons.goToDealManagement = async function() {

    return await CS.SM.getActiveSolution().then(solution => {

        return CS.SM.getActiveBasket().then(basket => {

            return basket.saveSolution(solution.id).then(function(resolve, reject) {
                window.location.href = '/apex/c__CS_DiscountRedirect?basketId=' + basket.basketId;
            }).catch(function(err) {

                if (err.message === 'No active solution') {
                    window.top.location = 'apex/c__CS_DiscountRedirect?basketId=' + basket.basketId;
                }
            });
        }).catch(value => {
            CS.SM.displayMessage(value, 'error');
        });
    }).catch(err => {
        if (err.message === 'No active solution found!'){
            return CS.SM.getActiveBasket().then(basket => {
                window.top.location = 'apex/csdiscounts__DiscountPage?basketId=' + basket.basketId;
            });
        }else{
            CS.SM.displayMessage(err.message ? err.message : err, 'error');
        }
        return;
    });
}


buttons.interval = setInterval(buttons.setButtons, 300);

// Freeze object to prevent accidental overrides
Object.freeze(buttons);

/**Dependent Picklist Attribute Maps**/
var switchProvisionDependantMap = {
    'Existing BT Maintained Switch' : ['IP Office','Quantumn','Mitel MiVoice - BT Managed','Mitel MiVoice - MiVoice Managed','Mitel Workbench'],
    'Non-BT Maintained Switch' : []
}

var siteAccessDependentAttributeMapHW = [{
    attributeName: "Access New/Existing/OTT",
    dependentAttName: "Access Type",
    dependentAttArray: [
    {
        dependentAttValues: "BT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    },
    {
        dependentAttValues: "BT Fiber",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    },
    {
        dependentAttValues: "Existing / 3rd Party",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    },
    {
        dependentAttValues: "BT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    },
    {
        dependentAttValues: "BT Fiber",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    },
    {
        dependentAttValues: "Existing / 3rd Party",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    },
    {
        dependentAttValues: "OTT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    },
    {
        dependentAttValues: "OTT Fiber",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    }]
}];  
   
var siteAccessDependentAttributeMapSIPT = [{
    attributeName: "Access New/Existing/OTT",
    dependentAttName: "Access Type",
    dependentAttArray: [
    {
        dependentAttValues: "BT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    },
    {
        dependentAttValues: "BT Net",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    }, 
    {
        dependentAttValues: "BT Fibre",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    }, 
    {
        dependentAttValues: "IPCUK",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    }, 
    {
        dependentAttValues: "BT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    }, 
    {
        dependentAttValues: "BT Net",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    }, 
    {
        dependentAttValues: "BT Fibre",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    }, 
    {
        dependentAttValues: "IPCUK",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    },
    {
        dependentAttValues: "OTT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    }, 
    {
        dependentAttValues: "OTT Fibre",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    }, 
    {
        dependentAttValues: "OTT Leased Line",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    }]    
}];  

var siteAccessDependentAttributeMap = [{
    attributeName: "Access New/Existing/OTT",
    dependentAttName: "Access Type",
    dependentAttArray: [{
        dependentAttValues: "BT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }, {
            attributeName: "Broadband Status",
            attributeValue: "Available"
        }]
    }, {
        dependentAttValues: "BT Fibre",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }, {
            attributeName: "Fiber Status",
            attributeValue: "Available"
        }]
    }, {
        dependentAttValues: "BT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    }, {
        dependentAttValues: "BT Fibre",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    }, {
        dependentAttValues: "BT Net",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    }, {
        dependentAttValues: "BT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    }, {
        dependentAttValues: "BT Fibre",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    }, {
        dependentAttValues: "IPCUK",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        }]
    }, {
        dependentAttValues: "BT Net",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        }]
    }, {
        dependentAttValues: "OTT Fibre",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    }, {
        dependentAttValues: "OTT Broadband",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    }, {
        dependentAttValues: "OTT Leased Line",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        }]
    }]
},{
    attributeName: "Access Type",
    dependentAttName: "Firewall Option",
    dependentAttArray: [{
        dependentAttValues: "Use BT hub internal firewall",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Broadband"
        }]
    },{
        dependentAttValues: "Use own firewall on BT hub",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Broadband"
        }]
    },{
        dependentAttValues: "Use BT hub internal firewall",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Fibre"
        }]
    },{
        dependentAttValues: "Use own firewall on BT hub",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Fibre"
        }]
    },{
        dependentAttValues: "Use BT hub internal firewall",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Broadband"
        }]
    },{
        dependentAttValues: "Use own firewall on BT hub",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Broadband"
        }]
    },{
        dependentAttValues: "Use BT hub internal firewall",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Fibre"
        }]
    },{
        dependentAttValues: "Use own firewall on BT hub",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Fibre"
        }]
    },
    //LPDE-102
    {
        dependentAttValues: "Use own firewall on Btnet Cisco router",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Net"
        }]
    },{
        dependentAttValues: "Use own firewall on Btnet Cisco Meraki router",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Net"
        }]
    },{
        dependentAttValues: "Use own firewall on BTnet",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "New"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Net"
        }]
    },{
        dependentAttValues: "Use own firewall on Btnet Cisco router",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Net"
        }]
    },{
        dependentAttValues: "Use own firewall on Btnet Cisco Meraki router",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Net"
        }]
    },{
        dependentAttValues: "Use own firewall on BTnet",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "Existing"
        },
        {
            attributeName: "Access Type",
            attributeValue: "BT Net"
        }]
    },{    //LPDE-103
        dependentAttValues: "Use own firewall on non-BT access",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        },
        {
            attributeName: "Access Type",
            attributeValue: "OTT Broadband"
        }]
    },{
        dependentAttValues: "Use own firewall on non-BT access",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        },
        {
            attributeName: "Access Type",
            attributeValue: "OTT Fibre"
        }]
    },{
        dependentAttValues: "Use own firewall on non-BT access",
        whereClause: [{
            attributeName: "Access New/Existing/OTT",
            attributeValue: "OTT"
        },
        {
            attributeName: "Access Type",
            attributeValue: "OTT Leased Line"
        }]
    }]
}];


/***Constants Declaration ***/

var CONSTANT_PROPERTIES = {
    LEAD_TIMES: {
        'BT Net': 90,
        'IPCUK': 8,
        'BT Fibre': 20,
        'BT Broadband': 15
    }
}

/*****Start of UI Hooks*****/

window.document.addEventListener('SolutionConsoleReady', function () {

    if (CS.SM.registerUIPlugin) {

        CS.SM.UIPlugin.alterAlertMessage = async function (message, type) {return { message, type }; };

        CS.SM.UIPlugin.buttonClickHandler =async function (buttonSettings) {

        console.log('buttonSettings', buttonSettings);
             if (buttonSettings.id === 'backToBasketInternal') {
                console.log('Back button', buttonSettings.basketId);
                return Promise.resolve('/' + buttonSettings.basketId);
                //sforce.one.navigateToURL('/apex/csbb__CSbasketredirect?Id='+buttonSettings.basketId +'#/', true);
                //sforce.one.navigateToURL('/' + basketId , true);
                //let activeSol = await CS.SM.getActiveSolution();
                //let pbId = activeSol.basketId;
                //window.open('/apex/csbb__CSbasketredirect?Id='+ pbId +'#/');
                //sforce.one.navigateToURL('/' + pbId , true);
                //return Promise.resolve(false);
             }

            if (buttonSettings.id === 'numberportredirect') {
                let solution=await CS.SM.getActiveSolution();
                let basketId = solution.basketId;
                let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
                let numbersToPort = mainConfiguration.getAttribute('Numbers to Port').value;
                console.log(basketId);
                console.log('Numbers to Port: ' + numbersToPort);
                if(numbersToPort === '' || numbersToPort === '0'){
                    alert('Numbers to Port value must be 1 or greater!')
                    return false;
                }
                //return Promise.resolve('/apex/c__CS_CloudVoice_NumberPort?basketid='+basketId+'&numstoport='+numbersToPort+'&returl='+window.location.href);
                window.open('/apex/c__CS_CloudVoice_NumberPort?basketid='+basketId+'&numstoport='+numbersToPort+'&returl='+window.location.href);
                return Promise.resolve(false);
            }        
            if (buttonSettings.id === 'salestoolkit') {
                window.open('https://office1.bt.com/sites/thegenie/Shared%20Documents/Toolkit_Interactive_LB.pdf?Web=0');
            }
            if(buttonSettings.id === "createImplementationForm"){
                // createImplementationFormXLSL(ALL_DATA)
            }
            return Promise.resolve(true);
        }
        
        CS.SM.UIPlugin.buttonIframeClosed = function (dialogResult, buttonSettings) {
            console.log('Dialog closed ', dialogResult, buttonSettings);
            if (buttonSettings.id === 'test1') {
                console.log('Dialog result for ', buttonSettings.id, dialogResult);
                return Promise.resolve(true);
            }
            return Promise.resolve(true);
        };

        CS.SM.UIPlugin.beforeNavigate =async function (currentComponent, previousComponent) {
            let solution= await CS.SM.getActiveSolution();
            
            if(solution.name=='Cloud Voice') {
                let noNavigateErrors=await beforeNavigateCloudVoice(solution,currentComponent, previousComponent);
                if (!noNavigateErrors) return Promise.resolve(false);
            }      
            if(solution.name.includes('Service Plan and Solutions')) {
                let noNavigateErrors=await beforeNavigateFutureMobile(solution,currentComponent, previousComponent);
                if (!noNavigateErrors) return Promise.resolve(false);
            } 
            
            return Promise.resolve({ allow: true/*, message: 'Success beforeNavigate'*/ });
        };

        CS.SM.UIPlugin.afterNavigate =async function (currentComponent, previousComponent) {
            console.log('afterNavigate ', currentComponent, previousComponent);
            let solution= await CS.SM.getActiveSolution();
            console.log('afterNavigate ', solution.name);
            
            if(solution.name=='Cloud Voice') {
                let noNavigateErrors=await afterNavigateCloudVoice(solution,currentComponent, previousComponent);
                if (!noNavigateErrors) return Promise.resolve(false);
            }      
            if(solution.name.includes('Service Plan and Solutions')) {
                let noNavigateErrors=await afterNavigateFutureMobile(solution,currentComponent, previousComponent);
                if (!noNavigateErrors) return Promise.resolve(false);
            } 
            if(solution.name.includes('Artemis')) {
                let noNavigateErrors=await afterNavigateArtemis(solution,currentComponent, previousComponent);
                if (!noNavigateErrors) return Promise.resolve(false);
            }
            if(solution.name.includes('BTnet')) {
                let noNavigateErrors=await afterNavigateBTNet(solution, currentComponent, previousComponent);
                if (!noNavigateErrors) return Promise.resolve(false);
            }
            /*if(solution.name.includes('Cloud Work')) {
                let noNavigateErrors=await afterNavigateCloudWork(solution, currentComponent, previousComponent);
                if (!noNavigateErrors) return Promise.resolve(false);
            }*/

           return Promise.resolve(true);

        };

        CS.SM.UIPlugin.onCustomAttributeFormat = function(solutionName, componentName, configurationGuid, attributeName) {
            if (attributeName === 'Address Lookup') {
                console.log("------------> BTNet hook executed: onCustomAttributeFormat, attribute: " + attributeName);
                setTimeout(() => {
                    formatPcUIForBTNet();
                }, 10);
                
                return Promise.resolve(false);
            }

            /*if (solutionName === 'Cloud Work' && attributeName === 'Site Address') {
                console.log("------------> Cloud Work hook executed: onCustomAttributeFormat, attribute: " + attributeName);
                setTimeout(() => {
                    formatPcUIForCloudWork();
                }, 5);
                
                return Promise.resolve(false);
            }*/
        }
 
        CS.SM.UIPlugin.onCustomAttributeEdit = async function (solutionName, componentName, configurationGuid, attributeName) {
            console.log('onCustomAttributeEdit=>',attributeName);
			if(attributeName === 'Custom Voice Rate Card'){
            		return displayUsageProfile(componentName, configurationGuid);
            }
            if(attributeName === 'Bespoke Voice & Data (CADM)'){
                    return displayUsageProfileArtemis(componentName, configurationGuid);
            }
            if(attributeName === 'SC Site Address'){
            		return displayAddressSearch(componentName, configurationGuid, 'site');
            }
            if(attributeName === 'SC Delivery Address'){
            	return displayAddressSearch(componentName, configurationGuid, 'delivery');
            }
            if(attributeName === 'Line Checker'){
            	return new Promise(function (res, rej) {
            	res(true);
            	}).then(
            			() =>
            			getLineCheckParamsForSiteAccess(componentName, configurationGuid)
            	);
            }
            if(attributeName === 'Minimum Upload Requirements'){
            	return new Promise(function (res, rej) {
            	res(true);
            	}).then(
            			() =>
            			getMinUploadReqParamsForSiteAccess(componentName, configurationGuid)
            	);
           	}
            if(attributeName === 'Email Speed Test Link'){
            	return new Promise(function (res, rej) {
            	res(true);
            	}).then(
            			() =>
            			getEmailSpeedTestLinkParamsForSiteAccess(componentName, configurationGuid)
            	);
            }
            if(attributeName === 'Qualification Criteria'){
                console.log('Inside CablePlan Qualification Criteria');
            	return new Promise(function (res, rej) {
            	res(true);
            	}).then(
            			() =>
            			getCablePlanQualificationCriteria(componentName, configurationGuid)
            	);
            }
            if(attributeName === 'Verify OTT Reference'){
            	return new Promise(function (res, rej) {
            	res(true);
            	}).then(
            			() =>
            			getVerifyOTTReferenceParamsForSiteAccess(componentName, configurationGuid)
            	);
            }
            
            if(attributeName === 'Import North Supply'){
                console.log('Inside Import North Supply');
                currentConfigGuidPDF=configurationGuid;
            	return new Promise(function (res, rej) {res(true);}).then( () => handleGatewayImport(configurationGuid) );
            }
            
            if(componentName === 'Artemis' && attributeName === 'Load/Refresh Usage Data'){
                return loadUsageProfileArtemis(componentName, configurationGuid);
            }
            
            if(attributeName === 'View Future Tiers (UK)'){
                return buildUKDataFutureTiersArtemis(componentName, configurationGuid);
            }
            
            if(attributeName === 'View Future Tiers (ROW)'){
                return buildROWDataFutureTiersArtemis(componentName, configurationGuid);
            }
            
            if(attributeName === 'Scheme Visual Ladder'){
                return buildFMVoiceAndDataSchemeLadder(componentName, configurationGuid);
            }

            // BTNet stuff...
            if (attributeName === 'Address Lookup') {
                console.log(solutionName);
                return displayPopup(configurationGuid);
            }

            // Cloud Work Stuff
            /*if (solutionName == CLOUD_WORK_CONST.solution && attributeName === 'Site Address') {
                return displayPopupSiteAddressCloudWork(configurationGuid);
            }

            if (solutionName == CLOUD_WORK_CONST.solution && attributeName === 'Shipping Address') {
                return displayPopupShippingAddressCloudWork(configurationGuid);
            }*/

            // Global Roaming
            /*if(componentName === 'Roaming' && attributeName === 'Load/Refresh Usage Data'){
                return loadFMUsageData(componentName, configurationGuid);
            }

            if(componentName === 'Roaming' && attributeName === 'Global Roaming Optional Tiers'){
                return globalRoamingFuturePricing(componentName, configurationGuid);
            }

            if(componentName === 'Roaming' && attributeName === 'Travel Allowance Optional Tiers'){
                return allowanceFuturePricing(componentName, configurationGuid);
            }*/
        };

        CS.SM.UIPlugin.onCustomAttributeClose = function () {
            return Promise.resolve(true);
        };

        CS.SM.registerUIPlugin();
    }
});

/*****End of UI Hooks*****/

/*****Start of UI functions *****/

const CloudVoiceCustomComponent = {}

CloudVoiceCustomComponent.drawComponent = async () => {

	let parent = document.getElementById('custom-component-div-1');
    
	if (!parent) {
        return Promise.resolve(false);
    }

    let activeSolution = await CS.SM.getActiveSolution();
    let devicesComponent = await activeSolution.getComponentByName('Customer Detail');

    let customHtml = '';

    //basket details
    customHtml+= '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
    customHtml+= '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudVoice_OverviewChkList?id='+activeSolution.solutionId+'"';
    customHtml+= '</div>';
	
	parent.innerHTML = customHtml;
	
	return Promise.resolve(true);
}

async function displayAddressSearch(componentName,configurations,addressType){
    console.log('in displayAddressSearch');
	let solution =  await CS.SM.getActiveSolution();
	let basketId = solution.basketId;

	let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString +='</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
	urlString +='Site Address </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudVoice_AddressSearch?basketid='+basketId+'&configGuid='+configurations+'&addressType='+addressType+'"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}

async function getLineCheckParamsForSiteAccess(componentName, configurationGuid){
        console.log('in getLineCheckParamsForSiteAccess');
        let solution = await CS.SM.getActiveSolution();
		let accessConfiguration = solution.getConfiguration(configurationGuid);
		let lineCheckerURLStringVar = accessConfiguration.getAttribute('lineCheckkerURLString').value;
        let addressId = accessConfiguration.getAttribute('Site Address').value;

		if(lineCheckerURLStringVar == '' && addressId != ''){
			lineCheckerURLStringVar = '?addressId='+addressId;
		}
		else if(lineCheckerURLStringVar == ''){
			lineCheckerURLStringVar = '?';
		}
		return displayLineChecker(lineCheckerURLStringVar,componentName, configurationGuid);
}

async function displayLineChecker(queryStringVar,componentName, configurationGuid){
    console.log('in displayLineChecker');
	let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;

    let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 60%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString +='</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
	urlString +='Broadband/Infinity Availability Checker </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	urlString += '<iframe height="100%" width="	100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudVoice_LineCheck'+queryStringVar+'&configGuid='+configurationGuid+'"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}

async function getMinUploadReqParamsForSiteAccess(componentName, configurationGuid){
    console.log('getMinUploadReqParamsForSiteAccess');
		let solution = await CS.SM.getActiveSolution();
        let accessConfiguration = solution.getConfiguration(configurationGuid);
		let minUploadReqURLStringVar = accessConfiguration.getAttribute('minUploadReqURLString').value;

		if(minUploadReqURLStringVar == ''){
			minUploadReqURLStringVar = '?';
		}
		return displayMinUploadReq(minUploadReqURLStringVar,componentName, configurationGuid);
}

async function displayMinUploadReq(queryStringVar,componentName, configurations){
    console.log('displayMinUploadReq');
	let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;

	let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 60%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString +='</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
	urlString +='Minimum Upload Requirements </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	urlString += '<iframe height="100%" width="	100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudVoice_MinimumUploadReq'+queryStringVar+'&configGuid='+configurations+'"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}

async function getEmailSpeedTestLinkParamsForSiteAccess(componentName, configurationGuid){
		let solution = await CS.SM.getActiveSolution();
        let accessConfiguration = solution.getConfiguration(configurationGuid);
		let provideTypeVar  = accessConfiguration.getAttribute('Access New/Existing/OTT').value;

		return displaySpeedTestEmail(provideTypeVar,componentName, configurationGuid);
}


async function displaySpeedTestEmail(provideTypeVar,componentName, configurations){
	let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;

	let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 50%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString +='</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
	urlString +='Email Speed Test Link </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudVoice_SpeedTestEmail?basketid='+basketId+'&accesstype='+provideTypeVar+'&configGuid='+configurations+'"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}
async function getCablePlanQualificationCriteria(componentName, configurationGuid){
	let solution =  await CS.SM.getActiveSolution();
    let accessConfiguration = solution.getConfiguration(configurationGuid); 
    let cablePlanQualificationURLStringVar = accessConfiguration.getAttribute('cablePlanQualificationURLString').value;
    let qualificationCriteriaAgreed = accessConfiguration.getAttribute('Qualification Criteria Agreed').value;
    let cablingType  = accessConfiguration.getAttribute('Cabling Type').displayValue;
    let totalOutletRequired = accessConfiguration.getAttribute('Total Outlets Required').value;
    let salesToAttendSurvey = accessConfiguration.getAttribute('Sales to Attend Survey').value;
    let siteAccessConfig = solution.getConfiguration(accessConfiguration.getAttribute('Site Access').value);
    console.log('siteAccessConfig=>',siteAccessConfig);
    
    //Site Address
    let siteInstallationAddress = siteAccessConfig.getAttribute('Site Address').displayValue;
    
    if(cablingType === ''){
		alert('Please select option for Cabling Type!')
		return false;
    }
    if(totalOutletRequired == ''){
        totalOutletRequired = 0;
    }
    

    
    
    console.log('cablePlanQualificationURLStringVar=>',cablePlanQualificationURLStringVar);
    console.log('qualificationCriteriaAgreed=>',qualificationCriteriaAgreed);
    console.log('cablingType=>',cablingType);
    console.log('totalOutletRequired=>',totalOutletRequired);
    console.log('salesToAttendSurvey=>',salesToAttendSurvey);
    console.log('Site Address=>',siteInstallationAddress);

    if(cablePlanQualificationURLStringVar == '' && cablingType != ''){
		cablePlanQualificationURLStringVar = '?totalOutletRequired='+totalOutletRequired+'&cablingType='+cablingType+'&salesToAttendSurvey='+salesToAttendSurvey+'&siteInstallationAddress='+siteInstallationAddress;
	}
	else if(cablePlanQualificationURLStringVar != '' && cablingType != ''){
	    cablePlanQualificationURLStringVar = cablePlanQualificationURLStringVar+'&cablingType='+cablingType+'&salesToAttendSurvey='+salesToAttendSurvey+'&siteInstallationAddress='+siteInstallationAddress;
	}

    for(oneOEmap of Object.values(siteAccessConfig.orderEnrichments)) {
        oneOE=Object.values(oneOEmap)[0];
        if(oneOE.configurationName.includes('Site Details')) {
            let contactName='';
            let contactEmail='';
            let contactMobile='';
            for(let OEatt of Object.values(oneOE.attributes)) {
                if(OEatt.name=='Site Contact Name') {
                    contactName=OEatt.displayValue.replace(' ','+');
                }
                if(OEatt.name=='Site Contact Email') {
                    contactEmail=OEatt.displayValue;
                }
                if(OEatt.name=='Site Contact Mobile') {
                    contactMobile=OEatt.displayValue;
                }
            }
            
            if(cablePlanQualificationURLStringVar=='') cablePlanQualificationURLStringVar='?';
            else cablePlanQualificationURLStringVar+='&';
            cablePlanQualificationURLStringVar+=('contactName='+contactName);
            cablePlanQualificationURLStringVar+=('&contactEmail='+contactEmail);
            cablePlanQualificationURLStringVar+=('&contactMobile='+contactMobile);
        }
    }
	if(cablePlanQualificationURLStringVar == ''){
		cablePlanQualificationURLStringVar = '?';
	}

	return displayCablePlanQualificationCriteria(cablePlanQualificationURLStringVar,componentName, configurationGuid,cablingType,qualificationCriteriaAgreed);
}
async function displayCablePlanQualificationCriteria(queryStringVar,componentName, configurationGuid,cablingType,qualificationCriteriaAgreed){
    let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;
    
    console.log('cablingType In=>',cablingType);
    let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 50%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString +='</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
	urlString += cablingType + ' Qualification Criteria </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudVoice_CableQualificationCriteria'+queryStringVar+'&configGuid='+configurationGuid+'&qualificationAgreedValue='+qualificationCriteriaAgreed+'&basketId='+basketId+'"';
	//urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_CloudVoice_CableQualificationCriteria?configGuid='+configurationGuid+'"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}

async function getVerifyOTTReferenceParamsForSiteAccess(componentName, configurationGuid){
		let solution = await CS.SM.getActiveSolution();
        let accessConfiguration = solution.getConfiguration(configurationGuid);
		let referenceVar  = accessConfiguration.getAttribute('Reference Id').value;

		return displayVerifyOTTReference(referenceVar,componentName, configurationGuid);
}

async function displayVerifyOTTReference(referenceVar,componentName, configurations){
    if(referenceVar === ''){
        alert('Reference Id must be populated!')
    	return false;
    }
    window.open('https://bt.visualware.com/myspeed/db/runtemplate?templateid=-18&rtype=norm&last=43200&allvar=crt&sid='+referenceVar);
}

function closeIframeModal()
{
	jQuery('#AddressIframe').remove();
	jQuery('.cdk-overlay-pane').remove();
	jQuery('.cdk-overlay-backdrop').remove();
	return Promise.resolve(false);
}

async function handleMessage(event) {
    console.log("[CS_CloudVoiceServicePlugin] Cloud Voice message from event: ");
    console.log(event);

    if (event.data && event.data.length !== 0) {
        // do NOTHING with events containing data like: "lightning-window-handshake" or "lightning-window-connection"
        if (!JSON.stringify(event.data).includes('lightning')) {
            var testMessage = event.data;
            let isDataValidJson = "";

            try {        
                isDataValidJson = JSON.stringify(testMessage);
            } catch (e) {
                console.log("[CS_CloudVoiceServicePlugin] JSON parsing failed");
                isDataValidJson = "";
            }

            // if message is in string, not json format, execute old code for Cloud Voice
            // BTnet and CloudWork messages are in json format
            if (!isDataValidJson.includes('event_id')) {
                console.log("[CS_CloudVoiceServicePlugin] Executing legacy Cloud Voice events code");
                let solution = await CS.SM.getActiveSolution();
                var message = event.data;
                var parseMessage = message.split(',');
                var modalAction = parseMessage[0];
                var userAction = parseMessage[1]; 
                var res = '';
                
                if(modalAction =='closeModal' && userAction =='updateRateCardAttribute')
                {
                    console.log(parseMessage);
                    var rateCardId= parseMessage[2];
                    var totalCharge= parseMessage[3];
                    var totalCost= parseMessage[4];
                    updateRateCardAttribute(rateCardId,totalCharge,totalCost);
                }
            
                if(modalAction =='closeModal')
                {
                    jQuery('#AddressIframe').remove();
                    jQuery('.cdk-overlay-pane').remove();
                    jQuery('.cdk-overlay-backdrop').remove();
            
                    if(userAction =='saveSiteAddress' )
                    {
                        var configurationGuid = parseMessage[2];
                        var addressId = parseMessage[3];
                        var addressText = parseMessage[4];
                        var nadKey = parseMessage[5];
            
                        updateConfigurationAttributeValue(
                                                        'Site Address',
                                                        addressId,
                                                        addressText,
                                                        false,
                                                        solution,
                                                        configurationGuid,
                                                        true
                        );
            
                        updateConfigurationAttributeValue(
                                                        'Delivery Address',
                                                        addressId,
                                                        addressText,
                                                        false,
                                                        solution,
                                                        configurationGuid,
                                                        true
                        );
            
                        updateConfigurationAttributeValue(
                                                        'Site NAD Key',
                                                        nadKey,
                                                        nadKey,
                                                        false,
                                                        solution,
                                                        configurationGuid,
                                                        true
                        );
            
                    }
                    if(userAction =='saveDeliveryAddress' )
                    {
                        var configurationGuid = parseMessage[2];
                        var addressId = parseMessage[3];
                        var addressText = parseMessage[4];
            
                        updateConfigurationAttributeValue(
                                                        'Delivery Address',
                                                        addressId,
                                                        addressText,
                                                        false,
                                                        solution,
                                                        configurationGuid,
                                                        false
                        );
                    }
                    if(userAction =='saveLineCheckerData' )
                    {
                        var configurationGuid = parseMessage[2];
                        var broadbandStatus = parseMessage[3];
                        var fiberStatus = parseMessage[4];
                        var queryString = parseMessage[5];
            
                        updateConfigurationAttributeValue(
                                                        'Broadband Status',
                                                            broadbandStatus,
                                                            broadbandStatus,
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                        );
            
                        updateConfigurationAttributeValue(
                                                            'Fiber Status',
                                                            fiberStatus,
                                                            fiberStatus,
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                        );
            
                        updateConfigurationAttributeValue(
                                                            'lineCheckkerURLString',
                                                            queryString,
                                                            queryString,
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                        );
            
            
                    }
                    if(userAction =='saveMinUploadReqData' )
                    {
                        var configurationGuid = parseMessage[2];
                        var voice = parseMessage[3];
                        var data = parseMessage[4];
                        var total = parseMessage[5];
                        var queryString = parseMessage[6];
                        console.log('saveMinUploadReqData Quesry String=>',queryString);
                        var codec = parseMessage[6].split('codec=');
                        console.log('codec=>',codec[1]);
                        updateConfigurationAttributeValue(
                                                        'Total Upload Speed Required',
                                                            total,
                                                            total,
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                        );
            
                        updateConfigurationAttributeValue(
                                                            'minUploadReqURLString',
                                                            queryString,
                                                            queryString,
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                        );
                        
                        updateConfigurationAttributeValue(
                                                            'Call Quality (Codec)',
                                                            codec[1],
                                                            codec[1],
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                        );
            
                    }
                    if(userAction =='saveQualificationCreteria')
                    {
                        var queryString = parseMessage[2];
                        var configurationGuid = parseMessage[3]; 
                        console.log('saveQualificationCreteria queryString=>',queryString);//?qualificationAgreedValue=on&totalOutletRequired=10
                        var qualificationCriteriaAgreedParam = parseMessage[4]; 
                        console.log('qualificationCriteriaAgreedParam=>',qualificationCriteriaAgreedParam);
                        var cablingType = parseMessage[5]; 
                        var cablingTypeId = parseMessage[6];
                        console.log('parseMessage=>',parseMessage);
                        
                        updateConfigurationAttributeValue(
                                                            'Cabling Type',
                                                            cablingTypeId,
                                                            cablingType,
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                        );
                        updateConfigurationAttributeValue(
                                                            'Qualification Criteria Agreed',
                                                            true,
                                                            true,
                                                            true,
                                                            solution,
                                                            configurationGuid,
                                                            false
                            );
            
                        updateConfigurationAttributeValue(
                                                            'cablePlanQualificationURLString',
                                                            queryString,
                                                            queryString,
                                                            false,
                                                            solution,
                                                            configurationGuid,
                                                            false
                                );
                    }
                    
                    if(userAction =='saveRelatedProductData') {
                        var configPDF = solution.getConfiguration(currentConfigGuidPDF);
                        var siteNamePDF = configPDF.getAttribute('Site Name').value;
                        console.log(parseMessage);
                        for(i = 0; i < parseMessage.length; i++ ) {
                            console.log('something ',i, parseMessage[i]);
                            if(i>2) {
                                if(parseMessage.length>= i+5) {
                                    var switchComponent = solution.getComponentByName(CLOUD_VOICE.SIPTswitch);
                                    var minPeriodMonths=parseMessage[i+7];
                                    if(minPeriodMonths=='') minPeriodMonths=1;
                                    var relatedProduct = await switchComponent.createRelatedProduct( 'BT Maintained Switch',[
                                        {name: "Quantity", value: { value: parseMessage[i], displayValue: parseMessage[i] }},
                                        {name: "Product Description", value: { value: parseMessage[i+1], displayValue: parseMessage[i+1] }},
                                        {name: "SC", value: { value: parseMessage[i+2], displayValue: parseMessage[i+2] }},
                                        {name: "Initial charges", value: { value: parseMessage[i+3], displayValue: parseMessage[i+3] }},
                                        {name: "Recurring charges", value: { value: parseMessage[i+4], displayValue: parseMessage[i+4] }},
                                        {name: "Discount", value: { value: parseMessage[i+5], displayValue: parseMessage[i+5] }},
                                        {name: "CC", value: { value: parseMessage[i+6], displayValue: parseMessage[i+6] }},
                                        {name: "Min Period months", value: { value: minPeriodMonths, displayValue: minPeriodMonths }},
                                        {name: "Product Code (BT)", value: { value: parseMessage[i+8], displayValue: parseMessage[i+8] }},
                                        {name: "Category Type", value: { value: parseMessage[i+9], displayValue: parseMessage[i+9] }},
                                        {name: "Site Name", value: { value: siteNamePDF, displayValue: siteNamePDF }}
                                        ]);
                                    await switchComponent.addRelatedProduct( currentConfigGuidPDF,relatedProduct ).then( configuration=>console.log( configuration ));
                                    i=i+9;
                                    
                                }
                                else break;
                            }
                        }
                        currentBasket = await CS.SM.getActiveBasket();
            
                        await currentBasket.saveSolution(solution.id);
                    }
                }
            }
            else {
                console.log("[CS_CloudVoiceServicePlugin] New BTnet or Cloud Voice event detected, executing code in CS_SiteAddressPopup");
            }
        }
        else {
            console.log("[CS_CloudVoiceServicePlugin] Spam event message detected, ignoring it");
        }
    }
	
	return Promise.resolve(true);
}
window.addEventListener('message', handleMessage, false);

/*****End of UI Functions*****/

/*****Start of Hooks*****/

function cloudVoiceHooks(Plugin) {
    var newValue;
    
    /**
     * Hook to check value before its updated. If we return false then the attribute value will not
     * be updated. Inorder for the update to continue we need to return true.
     *
     * @param {string} componentName - Tab name where the attribute belongs to.
     * @param {object} attribute - The attribute which is being updated
     * @param {string} beforeValue - Before change value
     * @param {string} afterValue - New value to be updated
     */
    Plugin.beforeAttributeUpdated =
        async function(component, configuration, attribute, newValueMap) {
            newValue = newValueMap;
            let solution = await CS.SM.getActiveSolution();
            let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);

            if (attribute.name == 'Site Type') {
                attribute.readOnly = true;
            }
            if (attribute.name == 'Site Access') {

                for(let component of Object.values(solution.components)) {
                    if(component.name==CLOUD_VOICE.siteAccess) {
                        for(let configLoop of Object.values(component.schema.configurations)) {
                            if(newValueMap.value==configLoop.guid && configLoop.getAttribute('Site Type').displayValue=='Cloud Voice - Homeworker') {
                                CS.SM.displayMessage('There can only be one UFP on a homeworker site','error');
                                return false;
                            }
                        }
                    }
                }
                attribute.readOnly = true;
            }
            return Promise.resolve(true);
        };

    /**
     * Provides the user with an opportunity to do something once the attribute is updated.
     *
     * @param {string} componentName - Tab name where the attribute belongs to.
     * @param {object} attribute - The attribute which is being updated.
     * @param {string} beforeValue - Before change value.
     * @param {string} afterValue - New value to be updated.
     */


    Plugin.afterOESplit = async function(component, configurationGuid, OEConfigurationGuids) {
        console.log('afterOESplit',component,configurationGuid,OEConfigurationGuids );
        let solution = await CS.SM.getActiveSolution();
        for(let configuration of Object.values(component.schema.configurations)) {
            let attribute;
            if(component.name==CLOUD_VOICE.UFP) attribute=configuration.getAttribute('Type');
            if(component.name==CLOUD_VOICE.CPE) attribute=configuration.getAttribute('Phone');
            updateConfigName(attribute.name, attribute.displayValue,configuration, component.name, solution);
        }
        
    }

    Plugin.afterConfigurationAddedToMacBasket = async function afterConfigurationAddedToMacBasket(componentName, configurationGUID) {
        console.log('After added to mac basket', componentName, configurationGUID);
        if (componentName != CLOUD_VOICE.solution) {
            let solution = await CS.SM.getActiveSolution();
            let config = solution.getConfiguration(configurationGUID);   
            
            updateConfigurationAttributeValue('Replaced Config Id' , config.replacedConfigId, config.replacedConfigId, true, solution, configurationGUID, false);
            
        }
    }
     
     
    Plugin.afterAttributeUpdated = async function(component, configuration, attribute, oldValueMap) {
        let solution = await CS.SM.getActiveSolution();
        let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
        console.log('afterAttributeUpdated', attribute,component, oldValueMap);
        
        //CPE attributes
        if(component.name == CLOUD_VOICE.CPE) {
            if (attribute.name == 'Site Access') {
                var siteType = solution.getConfiguration(attribute.value).getAttribute('Site Type').displayValue;
                var siteNADkey = solution.getConfiguration(attribute.value).getAttribute('Site NAD Key').value;
                updateSiteInfo(solution, configuration, attribute.displayValue, attribute.value, siteType, siteNADkey);
                if(attribute.displayValue!='') attribute.readOnly=true;
            }
            if (attribute.name == 'Quantity') {
                await calculateAvailabilityUFP(solution);
                calculateSoftphoneLicence(solution);
            }
            if (attribute.name == 'Phone') {
                updateConfigName(attribute.name, attribute.displayValue,configuration, component.name,solution);
                calculateSoftphoneLicence(solution);
                var siteAccessConfig = solution.getConfiguration(configuration.getAttribute('Site GUID').value);
                var siteType= siteAccessConfig.getAttribute('Site Type').value;
                console.log(siteType);
                if(siteType!='Cloud Voice - Homeworker' && attribute.displayValue!=''){
                    var skipInstallationAdd=false;
                    for(let relatedProduct of Object.values(configuration.relatedProductList)){
                        if (relatedProduct.name=='Phone Installation') {
                            skipInstallationAdd=true;
                            break;
                        }
                    }
                    if(!skipInstallationAdd) {
                        let installationAddOns = await getAddONByGroupName(component, configuration, 'Installation');
                        for (let addOn of installationAddOns) {
                            if(addOn.cspmb__Add_On_Price_Item__r.Name == 'Phone Installation' ){
                                addAddOnProductConfiguration('CPE Add Ons', addOn, configuration.guid, 1, component);
                                break;
                            }
                        }
                    }
                }
                if(siteType=='Cloud Voice - Homeworker' && attribute.displayValue!=''){
                    let PSUAddOns = await getAddONByGroupName(component, configuration, 'PSU');
                    for (let addOn of PSUAddOns) {
                        addAddOnProductConfiguration('CPE Add Ons', addOn, configuration.guid, 1, component);
                        break;
                    }
                }
            }
        }
        
        if(component.name == CLOUD_VOICE.UFP) {
            if (attribute.name == 'Site Access' && attribute.displayValue!='') {
                let siteAccessConfig = solution.getConfiguration(attribute.value);

                var siteType = siteAccessConfig.getAttribute('Site Type').displayValue;
                var siteNADkey = siteAccessConfig.getAttribute('Site NAD Key').value;
                updateSiteInfo(solution, configuration, attribute.displayValue, attribute.value, siteType, siteNADkey);
                if(attribute.displayValue!='') attribute.readOnly=true;
            }

            if (attribute.name == 'Type') {
                updateConfigName(attribute.name, attribute.displayValue,configuration, component.name,solution);
                toggleAttributeEdit(solution,component,attribute.name,"true");
                calculateSoftphoneLicence(solution);
                await calculateAvailabilityUFP(solution);
                licenceSumUFP(solution);
                await addTotalCareAddOns(solution, configuration, mainConfiguration);

                
                toggleAttributeEdit(solution,component,attribute.name,"");
                
            }
            if(attribute.name=='Contract Term') {
                updateConfigurationAttributeValue('Type','','',false, solution, configuration.guid, true);
                await addTotalCareAddOns(solution, configuration, mainConfiguration);				
            }
            
            if(attribute.name=='Quantity'){
                toggleAttributeEdit(solution,component,attribute.name,"true");
                await addTotalCareAddOns(solution, configuration, mainConfiguration);
                calculateSoftphoneLicence(solution);
                await calculateAvailabilityUFP(solution);
                licenceSumUFP(solution);
                await calculateUnlimitedCallPlan(configuration,solution)
                
                toggleAttributeEdit(solution,component,attribute.name,"");
                
                let siteAccessConfig = solution.getConfiguration(configuration.getAttribute('Site GUID').value);
                if(siteAccessConfig.getAttribute('Site Type').displayValue=='Cloud Voice - Homeworker' && attribute.value>1) {
                    configuration.status = false;
                    configuration.statusMessage = 'UFP of Homeworker type should only be one of type and quantity';
                }
                else {
                    if (configuration.statusMessage == 'UFP of Homeworker type should only be one of type and quantity'){
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }
            }
        }
        
        if(component.name==CLOUD_VOICE.solution) {
            if (attribute.name=='Additional Numbers Required') {
                console.log(component);
                toggleAttributeEdit(solution,solution,'Additional Numbers Required',"true");
                await calculateNumbersRequired(solution);
                toggleAttributeEdit(solution,solution,'Additional Numbers Required',"");
            }
            if (attribute.name == 'Service Connection') {
                let passportAddOns = await getAddONByGroupName(component, configuration, 'Passport');
                if(passportAddOns.length > 0) {
                    for (let addOn of passportAddOns) {
                        console.log(addOn);
                        if(addOn.cspmb__Add_On_Price_Item__r.Name.includes(mainConfiguration.getAttribute('Contract Term').value / 12)){
                            console.log('Passport',addOn);
                            addAddOnProductConfiguration('Training Add-On', addOn, configuration.guid, 1 , component);
                            break;
                        }
                    }
                }
            }
            if (attribute.name == 'Call Analytics Package'){
                callAnaliticsAddons(solution,attribute.value);
            }
            if (attribute.name == 'Care Level') {
                var UFPComponent = solution.getComponentByName(CLOUD_VOICE.UFP);
                var sconfigs = UFPComponent.getConfigurations();
                for (let config of Object.values(sconfigs)) {
                    updateConfigurationAttributeValue(attribute.name,attribute.displayValue,attribute.displayValue, true,solution,config.guid,true);
                }
                await addTotalCareAddOns(solution, null, mainConfiguration);
            }
            if (attribute.name == 'Contract Term') {
                await calculateSIPChannelsRequired(solution);
                await updateCommercialProductLookup('Connection', 'Service',attribute.value, 'Service Connection', mainConfiguration, solution);
                await calculateNumbersRequired(solution);
                await autoAddWebcast(solution, component, configuration);
				// CR12677 - Add BT Cloud Voice Training - 5 year addon if contract term is 5 years
				await addPassportAddOnForContractTerm(solution,configuration,attribute.value);
				

            }
            if (attribute.name == 'SIP Channels Required') {
                toggleAttributeEdit(solution,component,attribute.name,"None");
                
                checkUserFeatures(configuration);
                await calculateSIPChannelsRequired(solution);
                await addTotalCareAddOns(solution, configuration, mainConfiguration);
                await calculateUnlimitedCallPlan(configuration,solution);
                
                toggleAttributeEdit(solution,component,attribute.name,"");
            }
        }
        
        if(component.name == CLOUD_VOICE.cabling) {
            if (attribute.name == 'Cabling Type') {
                if (configuration.getAttribute('Qualification Criteria Agreed').displayValue != true) {
                    configuration.status = false;
                    configuration.statusMessage = 'Qualification Criteria needs to be agreed by the customer';
                }
                if(attribute.displayValue=='Cabling Survey (NG Bailey)') {
                    updateConfigurationAttributeValue('One Off Charge', '', '', false, solution, configuration.guid, true);
                }
                if (attribute.displayValue == 'Data Techniques') {
                    updateConfigurationAttributeValue('Data Technique Reference', '', '', false, solution, configuration.guid, true);
                }
                else {
                    updateConfigurationAttributeValue('Data Technique Reference', '', '', true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue('One Off Charge', '0', '0', true, solution, configuration.guid, true);
                }
                updateConfigName(attribute.name, attribute.displayValue,configuration, component.name,solution);
                await autoAddAttendance(component,configuration);
                updateCablingDependentFields(configuration,attribute);
                let siteAccessComponent = solution.getComponentByName(CLOUD_VOICE.siteAccess);
                let siteAccessConfig = solution.getConfiguration(configuration.getAttribute('Site Access').value);
                for(let relatedProduct of Object.values(siteAccessConfig.relatedProductList) ) {
                    if(relatedProduct.name.includes('Auto Attendant')) {
                        await deleteAddOns(siteAccessComponent, siteAccessConfig, relatedProduct.guid,true);
                    }
                }
                if (attribute.displayValue == 'BT Cableplan') {
                    let siteAddOns = await getAddONByGroupName(siteAccessComponent, siteAccessConfig, 'Site Add On');
    
                    if (siteAddOns.length > 0) {
                        for(let addOn of siteAddOns) {
                            if(addOn.cspmb__Add_On_Price_Item__r.Name.includes('Auto Attendant')) {
                                addAddOnProductConfiguration('Site Access Add-Ons', addOn , siteAccessConfig.guid, 1, siteAccessComponent);
                                break;
                            }
                        }
                    }
                }
                updateConfigurationAttributeValue('Supply Type', '', '', false, solution, configuration.guid, false);
            }
            if (attribute.name=='Supply Type') {
                console.log('changed S T');
                await autoAddAttendance(component,configuration);
                if (attribute.value!='') await addPatchCordsAddon(solution,configuration);
            }
        }
        
        if(component.name == CLOUD_VOICE.SIPTswitch) {
            if(attribute.name == 'Switch Provision') {
                changeUploadAttributeText('Import North Supply','Upload here');
                let careLevel=Object.values(solution.schema.configurations)[0].getAttribute('Care Level').displayValue;
                updateConfigurationAttributeValue( 'Care Level',careLevel,careLevel, true,solution,configuration.guid,true);
                updateConfigName(attribute.name, attribute.displayValue,configuration, component.name,solution);
                
                if(attribute.displayValue!='Non-BT Maintained Switch' && attribute.displayValue!='Existing BT Maintained Switch' ) {
                    updateConfigurationAttributeValue('Gateway/SBC Type','N/A','N/A', true,solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Interface Type','N/A','N/A', true,solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Version','N/A','N/A', true, solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Gateway Maintenance Term','N/A','N/A', true, solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Mode','N/A','N/A', true, solution, configuration.guid,true);
                }
                else {
                    updateConfigurationAttributeValue('Gateway/SBC Type','','', false,solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Interface Type','','', false,solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Version','','', false, solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Gateway Maintenance Term','','', false, solution, configuration.guid,true);
                    updateConfigurationAttributeValue('Mode','','', false, solution, configuration.guid,true);
                }
                
                if(attribute.displayValue=='New BT Maintained Switch') {
                    updateConfigurationAttributeValue('New Switch Type','','', false,solution, configuration.guid,true);
                }
                else {
                    updateConfigurationAttributeValue('New Switch Type','N/A','N/A', true,solution, configuration.guid,true);
                }
                
                checkGatewayExistance(configuration);
            }
            if(attribute.name == 'Mode') {
                if(attribute.displayValue=='SBC') {
        		    updateData = [{name : 'Interface Type', value : 'Both', displayValue : 'Both', options : ['Both'], readOnly: true}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                    updateConfigurationAttributeValue('Gateway CAC Max (PRI)','250','250',true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue('Gateway CAC Max (BRI)','60','60',true, solution, configuration.guid, true);
                    

                }
                else {
                    updateData = [{name : 'Interface Type', value : '', displayValue : '', options : ['PRI', 'BRI'], readOnly: false}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                    if(attribute.displayValue=='Gateway') {
                        updateConfigurationAttributeValue('Gateway CAC Max (PRI)','30','30',true, solution, configuration.guid, true);
                        updateConfigurationAttributeValue('Gateway CAC Max (BRI)','8','8',true, solution, configuration.guid, true);
                    }
                    else {
                        updateConfigurationAttributeValue('Gateway CAC Max (PRI)','','',true, solution, configuration.guid, true);
                        updateConfigurationAttributeValue('Gateway CAC Max (BRI)','','',true, solution, configuration.guid, true);
                    }
                }
            }
            
        }
        
        if(attribute.name == 'Qualification Criteria Agreed'){
            console.log('Qualification Criteria Agreed Validation');
            var cablingType = configuration.getAttribute('Cabling Type').displayValue;
            var criteriaAgreed = configuration.getAttribute('Qualification Criteria Agreed').value;
            var salesToAttendSurvey = configuration.getAttribute('Sales to Attend Survey').value;
            console.log('cablingType=>',cablingType);
            console.log('criteriaAgreed=>',criteriaAgreed);
            console.log('salesToAttendSurvey=>',salesToAttendSurvey);
            if (attribute.value=='true') await addPatchCordsAddon(solution,configuration);
            if ((cablingType == 'BT Cableplan' || cablingType == 'Cabling Survey (NG Bailey)' || cablingType == 'Existing / Not Required') && criteriaAgreed != true) {
               configuration.status = false;
               configuration.statusMessage = 'Qualification Criteria needs to be agreed by the customer';
            }
            else {
                if (configuration.statusMessage == 'Qualification Criteria needs to be agreed by the customer'){
                    configuration.status = true;
                    configuration.errorMessage = null;
                }
            }
        }
     
        
        /* SITE ACCESS ATTRIBUTES */
        
        if (component.name == CLOUD_VOICE.siteAccess) {
            if(attribute.name=='Site Name') {
                updateConfigurationAttributeValue( 'Config Name', attribute.value, attribute.value, false, solution, configuration.guid, true);
                for(let componentLoop of Object.values(solution.components) ) {
                    if (componentLoop.name== CLOUD_VOICE.UFP || componentLoop.name== CLOUD_VOICE.CPE) {
                        for(let config of Object.values(componentLoop.schema.configurations)) {
                            if(config.getAttribute('Site Access').value==configuration.guid) {
                                updateConfigurationAttributeValue( 'Site Access', oldValueMap.value, attribute.value, true, solution, config.guid, true);
                            }
                        }
                     }
                }
                configuration.configurationName=attribute.value;
            }
            if(attribute.name=='Site Required by Date') {
                validateLeadTimeForAccess(attribute.value, configuration);
            }

            if(attribute.name=='Lead Time') {
                validateLeadTimeForAccess(configuration.getAttribute('Site Required by Date').value, configuration);
            }

            if(attribute.name == 'Site Address') {
                saveSiteNADKeys(solution);
            }
            
            if (attribute.name == 'Access New/Existing/OTT') {
                if(configuration.statusMessage =='Firewall Option must be selected if any are available.') {
                    configuration.status = true;
                    configuration.errorMessage = null;
                }
                if(attribute.displayValue=='OTT' || attribute.displayValue=='') {
                    updateData = [{name : 'Reference Id', value : 'N/A', displayValue : 'N/A', readOnly: true, required : false}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                }
                else {
                    updateData = [{name : 'Reference Id', value : '', displayValue : '',readOnly : false, required : true}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                }
                
                let siteType = configuration.getAttribute('Site Type').displayValue;
                if(siteType=='Cloud Voice - Business Site'){
                    updateDependentPicklistValues(attribute.name, attribute.value, siteAccessDependentAttributeMap[0], solution, configuration, true, '');
                }
                if(siteType=='Cloud Voice - Homeworker'){
                    updateDependentPicklistValues(attribute.name, attribute.value, siteAccessDependentAttributeMapHW[0], solution, configuration, true,'');
                }
                if(siteType=='SIP-T - Business Site'){
                    updateDependentPicklistValues(attribute.name, attribute.value, siteAccessDependentAttributeMapSIPT[0], solution, configuration, true,'');
                }
                
                var currentAValue = configuration.getAttribute('Firewall Option').value;
                if (currentAValue=='Please select') {
        		    updateData = [{name : 'Firewall Option', value : '', displayValue : '', options : ['']}];
                }
                else {
        		    updateData = [{name : 'Firewall Option', value : 'Please Select', displayValue : 'Please Select', options : ['']}];
                }
                solution.updateConfigurationAttribute(configuration.guid, updateData, true);
            }

            if (attribute.name == 'Access Type') {
                for(let attr of Object.values(configuration.attributes)) {
                    if(attr.name == 'Access New/Existing/OTT') {
                        accessAttr=attr.value;
                    }
                }
                if ((accessAttr=='New' || accessAttr=='Existing') && attribute.value=='BT Net') {
                    updateConfigurationAttributeValue( 'Lead Time','See BTnet Product','See BTnet Product',
                                                        true,solution,configuration.guid,false);
                }
                else {
                    if(attribute.value=='' || attribute.value.includes('OTT')) {
                        updateConfigurationAttributeValue( 'Lead Time', null, null,
                                                            true, solution, configuration.guid, false);
                    }
                    else{
                        updateConfigurationAttributeValue(  'Lead Time', CONSTANT_PROPERTIES.LEAD_TIMES[attribute.value],
                                                            CONSTANT_PROPERTIES.LEAD_TIMES[attribute.value] + ' days',
                                                            true, solution, configuration.guid, false);
                    }
                }
                
                var pickListValues = updateDependentPicklistValues(attribute.name, attribute.value, siteAccessDependentAttributeMap[1], solution, configuration, false,'');
                updateConfigurationAttributeValue('Reference Id','','',false,solution,configuration.guid,false);
                if(attribute.value=='BT Net' && accessAttr=='New') {
                    //toggleAttributePicklistEdit(solution,component,'BT Net Access',"block")
                }
                else {
                    //updateConfigurationAttributeValue('BT Net Access','','',true,solution,configuration.guid,true);
                    //toggleAttributePicklistEdit(solution,component,'BT Net Access',"none")
                }

                let FWattribute = configuration.getAttribute('Firewall Option');
                console.log(FWattribute);
                if(pickListValues[0]!=[""] && (FWattribute.displayValue=='Please select' || FWattribute.displayValue=='Please Select' || FWattribute.displayValue=='')) {
                    configuration.status = false;
                    configuration.statusMessage='Firewall Option must be selected if any are available.';
                }
                else {
                    if(configuration.statusMessage =='Firewall Option must be selected if any are available.') {
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }
            }
            if (attribute.name == 'Firewall Option') {
                console.log(attribute);
                if(attribute.options!=[''] && (attribute.displayValue=='Please select' || attribute.displayValue=='Please Select' || attribute.displayValue=='')) {
                    configuration.status = false;
                    configuration.statusMessage='Firewall Option must be selected if any are available.';
                }
                else {
                    if(configuration.statusMessage =='Firewall Option must be selected if any are available.') {
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }
            }
            
           /* if (attribute.name == 'BT Net Access') {
                let inputMap={'configID' : attribute.value, 'action' : 'getConfigInfoRemote'};
                currentBasket = await CS.SM.getActiveBasket();
                console.log(currentBasket.basketId);
                let result= await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
                if(isNaN(Number(result['speed']))) result['speed']=0;
                updateConfigurationAttributeValue('Upload Speed (Mbps)',Number(result['speed']),Number(result['speed']),false,solution,configuration.guid,true);
                updateConfigurationAttributeValue('Reference Id',attribute.value,attribute.value,false,solution,configuration.guid,true);
            }*/
            
            if (attribute.name == 'Site Type') {
                attribute.readOnly = true;
                if(attribute.displayValue=='SIP-T - Business Site') {
                    updateData = [{name : 'SIP Users Required', value : '', displayValue : '', readOnly: false, required : true}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                }
                else {
                    updateData = [{name : 'SIP Users Required', value : 'N/A', displayValue : 'N/A', readOnly: true, required : false}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                }

                await createChildConfigurations('UFPs', configuration);
                await createChildConfigurations('CPEs', configuration);
                await createChildConfigurations('LAN', configuration);
                await createChildConfigurations('Cabling', configuration);
                await createChildConfigurations('Switch', configuration);
                let siteAccessComponent = solution.getComponentByName(CLOUD_VOICE.siteAccess);
                try{
                    let siteAccessAddOns = await getAllAddOns(siteAccessComponent, configuration);
                    if(siteAccessAddOns.length > 0) {
                        for (let addOn of siteAccessAddOns) {
                            if(addOn.cspmb__Add_On_Price_Item__r.Name == 'Free Auto-Attendant' ){
                                addAddOnProductConfiguration('Site Access Add-Ons', addOn, configuration.guid, 1, siteAccessComponent);
                                break;
                            }
                        }
                    }
                } catch (error) {console.log('ERROR IS ',error);}
                checkIfBusinessTypeExists(component);
            }
        }
        //the attribute on LAN has the same name as main component and is called here because of RND bug
        if(attribute.name=='Total Ports Required') {
            checkTotalPortLimit(solution);
        }
        if(component.name==CLOUD_VOICE.LAN){
            if(attribute.name=='LAN Type') {
                updateConfigName(attribute.name, attribute.displayValue,configuration, component.name,solution);
                if(attribute.displayValue=='Router Only') {
                     updateData = [{name : 'Router Used', value : 'BT Hub 5', displayValue : 'BT Hub 5', readOnly: false, required : true}];
                     solution.updateConfigurationAttribute(configuration.guid, updateData, false);
                }
                else {
                    updateData = [{name : 'Router Used', value : 'N/A', displayValue : 'N/A', readOnly: true, required : false}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, false);
                }

                updateConfigurationAttributeValue('Model','N/A','N/A',true,solution,configuration.guid,true);
                updateConfigurationAttributeValue('Make','N/A','N/A',true,solution,configuration.guid,true);
                updateConfigurationAttributeValue('Supports QoS','N/A','N/A',true,solution,configuration.guid,true);


                enableQOSOrPOE(solution,component, configuration, attribute.displayValue);
                checkTotalPortLimit(solution);
                
                toggleAttributeEdit(solution,component,'Additional Ports Required',"true");
                toggleAttributeCheckboxEdit(solution,component,'Gigabit Switch Required',"None");

                await calculateLANaddons(solution,configuration);
                
                toggleAttributeEdit(solution,component,'Additional Ports Required',"");
                toggleAttributeCheckboxEdit(solution,component,'Gigabit Switch Required',"");
                validateQOSorPOE(configuration);

                if(attribute.displayValue!='ITS Bespoke LAN') {
                    updateData = [{name : 'ITS Opportunity Lookup', value : '', displayValue : '', readOnly: true, required : false}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                }
                else {
                    updateData = [{name : 'ITS Opportunity Lookup', value : '', displayValue : '',readOnly : false, required : true}];
                    solution.updateConfigurationAttribute(configuration.guid, updateData, true);
                }
            }

            if(attribute.name == 'Gigabit Switch'){
                toggleAttributeEdit(solution,component,'Additional Ports Required',"true");
                toggleAttributeCheckboxEdit(solution,component,'Gigabit Switch Required',"None");

                await calculateLANaddons(solution,configuration);
                
                toggleAttributeEdit(solution,component,'Additional Ports Required',"");
                toggleAttributeCheckboxEdit(solution,component,'Gigabit Switch Required',"");
            }
            if(attribute.name == 'Additional Ports'){
                toggleAttributeEdit(solution,component,'Additional Ports Required',"true");
                toggleAttributeCheckboxEdit(solution,component,'Gigabit Switch Required',"None");

                checkTotalPortLimit(solution);
                await calculateLANaddons(solution,configuration);
                
                toggleAttributeEdit(solution,component,'Additional Ports Required',"");
                toggleAttributeCheckboxEdit(solution,component,'Gigabit Switch Required',"");
            }

            if(attribute.name=='Router Used' || attribute.name=='Model' || attribute.name=='Make') {
                if(attribute.name=='Router Used') {
                    if(attribute.displayValue=='BT Hub 5') {
                        updateConfigurationAttributeValue('Supports QoS','Yes','Yes',true, solution, configuration.guid, true);
                    }
                    else {
                        if(attribute.displayValue=='Other') {
                            updateConfigurationAttributeValue('Model','','',false, solution, configuration.guid, true);
                            updateConfigurationAttributeValue('Make','','',false, solution, configuration.guid, true);
                            updateConfigurationAttributeValue('Supports QoS','','',false, solution, configuration.guid, true);
                            updateConfigurationAttributeValue('Supports PoE','','',false, solution, configuration.guid, true);

                        }
                        else {
                            updateConfigurationAttributeValue('Model','N/A','N/A',true, solution, configuration.guid, true);
                            updateConfigurationAttributeValue('Make','N/A','N/A',true, solution, configuration.guid, true);
                            updateConfigurationAttributeValue('Supports QoS','N/A','N/A',true, solution, configuration.guid, true);
                            updateConfigurationAttributeValue('Supports PoE','N/A','N/A',true, solution, configuration.guid, true);
                        }
                    }
                }

                let siteAccessConfig = await solution.getConfiguration(configuration.getAttribute('Site Access').value);
                let result = validateRouterUsedForSiteAccess(   siteAccessConfig.getAttribute('Access Type').value,
                                                                configuration.getAttribute('Make').value,
                                                                configuration.getAttribute('Model').value,
                                                                configuration.getAttribute('Supports QoS').value,
                                                                configuration.getAttribute('Router Used').value);
                if (result!='') {
                    configuration.status = false;
                    configuration.statusMessage = result;
                }
                else {
                    if (configuration.statusMessage == 'Please select Router Used which is required for BT Fibre and BT Broadband Access Type'
                    || configuration.statusMessage == 'Make, Model and QoS Enabled are required When Router Used is Other'){
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }                                                
            }
            
            if(attribute.name == 'Supports QoS' || attribute.name == 'Supports PoE') {
                 validateQOSorPOE(configuration);
            }
        }


        //OE Attribute update
        
        if(attribute.name == 'Existing Contact' && component.name == 'Site Details' && newValue.lookupValue!=undefined){
        // if(attribute.name == 'Existing Site Contact' && component.name == CLOUD_VOICE.siteAccess && newValue.lookupValue!=undefined){
            console.log('newValue.lookupValue=>',newValue.lookupValue);
            configuration.getAttribute('Site Contact Name').displayValue = attribute.displayValue;
            configuration.getAttribute('Site Contact Name').value = attribute.displayValue;
            configuration.getAttribute('Site Contact Name').readOnly = true;
            configuration.getAttribute('Site Contact Name').required = false;
            //Contact Email
            configuration.getAttribute('Site Contact Email').displayValue = newValue.lookupValue.email;
            configuration.getAttribute('Site Contact Email').value = newValue.lookupValue.email;
            configuration.getAttribute('Site Contact Email').readOnly = true;
            configuration.getAttribute('Site Contact Email').required = false;
            //Contact Phone
            if(newValue.lookupValue.phone=='') {
                configuration.getAttribute('Site Contact Phone').displayValue = '...';
                configuration.getAttribute('Site Contact Phone').value = '...';
            }
            else {
                configuration.getAttribute('Site Contact Phone').displayValue = newValue.lookupValue.phone;
                configuration.getAttribute('Site Contact Phone').value = newValue.lookupValue.phone;
            }
            configuration.getAttribute('Site Contact Phone').readOnly = true;
            configuration.getAttribute('Site Contact Phone').required = false;
            //Conact Mobile
            configuration.getAttribute('Site Contact Mobile').displayValue = newValue.lookupValue.mobilephone;
            configuration.getAttribute('Site Contact Mobile').value = newValue.lookupValue.mobilephone;
            configuration.getAttribute('Site Contact Mobile').readOnly = true;
            configuration.getAttribute('Site Contact Mobile').required = false;
        }


        if(component.name == 'Hunt Group') {  //LPDE-850
            if( (attribute.name == 'Call Forwarding When Busy' || attribute.name == 'Scheduled Call Forwarding') && attribute.value==true){
                var huntPlusExists=false;
                for( let relatedProduct of Object.values(solution.getConfiguration(configuration.parentConfiguration).relatedProductList)){
                    if (relatedProduct.name=='Hunt Group Plus' && huntPlusExists==false) {
                        Object.values(relatedProduct.configuration.attributes).forEach(attributeLoop => {
                            if(attributeLoop.name=='Quantity') {
                                if(attributeLoop.value!=0 && attributeLoop.value!='') {
                                    huntPlusExists=true;
                                }
                            }
                        });
                    }
                }
                if(!huntPlusExists) {
                    CS.SM.displayMessage('Please add the required Hunt Group Plus Licenses for this feature','error');
                    attribute.value=false;
                    attribute.displayValue=false;
                }
            }
        }

        if(component.name == 'Call Centre') {  //LPDE-850
            if(attribute.name == 'Advanced Reporting' && attribute.value==true){
                if(mainConfiguration.getAttribute('Call Analytics Package').displayValue=='') {
                    CS.SM.displayMessage('Please add the required Call Analytics Licenses for this feature','error');
                    attribute.value=false;
                    attribute.displayValue=false;
                }
            }
            if( (attribute.name == 'Call Forwarding When Busy' || attribute.name == 'Scheduled Call Forwarding') && attribute.value==true){
                var ACDplusExists=false;

                for( let relatedProduct of Object.values(solution.getConfiguration(configuration.parentConfiguration).relatedProductList)){
                    if (relatedProduct.name=='Call Centre ACD Plus' && ACDplusExists==false) {
                        Object.values(relatedProduct.configuration.attributes).forEach(attributeLoop => {
                            if(attributeLoop.name=='Quantity') {
                                if(attributeLoop.value!=0 && attributeLoop.value!='') {
                                    ACDplusExists=true;
                                }
                            }
                        });
                    }
                }
                if(!ACDplusExists) {
                    CS.SM.displayMessage('Please add the required Call Centre ACD Plus Licenses for this feature','error');
                    attribute.value=false;
                    attribute.displayValue=false;
                }
            }
        }

        if(attribute.name == 'Reason For Order') {
            console.log(component, configuration, attribute);
            for(let attributeLoop of Object.values(configuration.attributes)) {
                if(attributeLoop.name=='Moving Date' && attributeLoop.displayValue=='') {
                    if(attribute.displayValue=='Site Mover') {
                        attributeLoop.error=true;
                        attributeLoop.errorMessage='Required';
                        attributeLoop.required=true;
                    }
                    else {
                        attributeLoop.error=false;
                        attributeLoop.errorMessage='';
                        attributeLoop.required=false;
                    }
                }
            }
        }

        if(attribute.name == 'Existing Contact' && component.name == 'Additional Contacts' && newValue.lookupValue!=undefined){
            configuration.getAttribute('Contact Name').displayValue = attribute.displayValue;
            configuration.getAttribute('Contact Name').value = attribute.displayValue;
            configuration.getAttribute('Contact Name').readOnly = true;
            configuration.getAttribute('Contact Name').required = false;
            //Contact Email
            configuration.getAttribute('Contact Email').displayValue = newValue.lookupValue.email;
            configuration.getAttribute('Contact Email').value = newValue.lookupValue.email;
            configuration.getAttribute('Contact Email').readOnly = true;
            configuration.getAttribute('Contact Email').required = false;
            //Contact Phone

            if(newValue.lookupValue.phone=='') {
                configuration.getAttribute('Contact Phone').displayValue = '...';
                configuration.getAttribute('Contact Phone').value = '...';
            }
            else {
                configuration.getAttribute('Contact Phone').displayValue = newValue.lookupValue.phone;
                configuration.getAttribute('Contact Phone').value = newValue.lookupValue.phone;
            }
            configuration.getAttribute('Contact Phone').readOnly = true;
            configuration.getAttribute('Contact Phone').required = false;
            //Conact Mobile
            configuration.getAttribute('Contact Mobile').displayValue = newValue.lookupValue.MobilePhone;
            configuration.getAttribute('Contact Mobile').displayValue = newValue.lookupValue.MobilePhone;
            configuration.getAttribute('Contact Mobile').readOnly = true;
            configuration.getAttribute('Contact Mobile').required = false;
        }
        if(attribute.name == 'Existing Contact' && component.name == 'Site Contacts' && newValue.lookupValue!=undefined){
            configuration.getAttribute('Contact Name').displayValue = attribute.displayValue;
            configuration.getAttribute('Contact Name').value = attribute.displayValue;
            configuration.getAttribute('Contact Name').readOnly = true;
            configuration.getAttribute('Contact Name').required = false;
            //Contact Email
            configuration.getAttribute('Contact Email').displayValue = newValue.lookupValue.email;
            configuration.getAttribute('Contact Email').value = newValue.lookupValue.email;
            configuration.getAttribute('Contact Email').readOnly = true;
            configuration.getAttribute('Contact Email').required = false;
            //Contact Phone
            configuration.getAttribute('Contact Phone').displayValue = newValue.lookupValue.phone;
            configuration.getAttribute('Contact Phone').value = newValue.lookupValue.phone;
            configuration.getAttribute('Contact Phone').readOnly = true;
            configuration.getAttribute('Contact Phone').required = false;
            //Conact Mobile
            configuration.getAttribute('Contact Mobile').displayValue = newValue.lookupValue.MobilePhone;
            configuration.getAttribute('Contact Mobile').value = newValue.lookupValue.MobilePhone;
            configuration.getAttribute('Contact Mobile').readOnly = true;
            configuration.getAttribute('Contact Mobile').required = false;
        }

        //LPDE-626
        if(attribute.name == 'Existing Contact' && component.name == 'Service Details' && newValue.lookupValue!=undefined){
            configuration.getAttribute('Administrator Name').displayValue = attribute.displayValue;
            configuration.getAttribute('Administrator Name').value = attribute.displayValue;
            configuration.getAttribute('Administrator Name').readOnly = true;
            configuration.getAttribute('Administrator Name').required = false;
            //Contact Email
            configuration.getAttribute('Administrator Email').displayValue = newValue.lookupValue.email;
            configuration.getAttribute('Administrator Email').value = newValue.lookupValue.email;
            configuration.getAttribute('Administrator Email').readOnly = true;
            configuration.getAttribute('Administrator Email').required = false;
            //Contact Phone
            configuration.getAttribute('Administrator Telephone').displayValue = newValue.lookupValue.phone;
            configuration.getAttribute('Administrator Telephone').value = newValue.lookupValue.phone;
            configuration.getAttribute('Administrator Telephone').readOnly = true;
            configuration.getAttribute('Administrator Telephone').required = false;
        } 
        
        //LPDE-45
        if((attribute.name == 'Billing Account' || attribute.name == 'BAC Reference' || attribute.name == 'Opt Out Direct Debit' || attribute.name == 'Opt Out One Bill' || attribute.name == 'Opt Out Paperless') && component.name == 'Billing Information'){
            var billingAccount = configuration.getAttribute('Billing Account').value;
            var bacReference = configuration.getAttribute('BAC Reference').value;
            var optOutDirectDebit = configuration.getAttribute('Opt Out Direct Debit').value;
            var optOutOneBill = configuration.getAttribute('Opt Out One Bill').value;
            var optOutPaperless = configuration.getAttribute('Opt Out Paperless').value;
            
            console.log('billingAccount=>',billingAccount);
            console.log('optOutDirectDebit=>',optOutDirectDebit);
            
            if(billingAccount == 'Existing' && bacReference == ''){
                 configuration.getAttribute('BAC Reference').required = true;
                 configuration.getAttribute('Opt Out Direct Debit').required = false;
                 configuration.getAttribute('Opt Out One Bill').required = false;
                 configuration.getAttribute('Opt Out Paperless').required = false;
            }else if(billingAccount == 'New' && (optOutDirectDebit == '' || optOutOneBill == '' || optOutPaperless == '')){
                console.log('Billing Account New validate');
                configuration.getAttribute('BAC Reference').required = false;
                configuration.getAttribute('Opt Out Direct Debit').required = true;
                configuration.getAttribute('Opt Out One Bill').required = true;
                configuration.getAttribute('Opt Out Paperless').required = true;
            }else
            {
                configuration.getAttribute('BAC Reference').required = false;
                configuration.getAttribute('Opt Out Direct Debit').required = false;
                 configuration.getAttribute('Opt Out One Bill').required = false;
                 configuration.getAttribute('Opt Out Paperless').required = false;
            }
        }
    }
        /*if((attribute.name == 'Billing Account' || attribute.name == 'BAC Reference' ) && component.name == 'Billing Information'){
            var billingAccount = configuration.getAttribute('Billing Account').value;
            var bacReference = configuration.getAttribute('BAC Reference').value;
            let result = validateBillingInfoExisting(billingAccount,bacReference);
            console.log('Billing Info Existing Result=>',result);
            if (result.error) {
               configuration.getAttribute('BAC Reference').required = true;
            } else {
                 configuration.getAttribute('BAC Reference').required = false;
            }
        }
        
        if((attribute.name == 'Billing Account' || attribute.name == 'Opt Out Direct Debit' || attribute.name == 'Opt Out One Bill' || attribute.name == 'Opt Out Paperless') && component.name == 'Billing Information'){
            var billingAccount = configuration.getAttribute('Billing Account').value;
            var optOutDirectDebit = configuration.getAttribute('Opt Out Direct Debit').value;
            var optOutOneBill = configuration.getAttribute('Opt Out One Bill').value;
            var optOutPaperless = configuration.getAttribute('Opt Out Paperless').value;
            let result = validateBillingInfoNew(billingAccount,optOutDirectDebit,optOutOneBill,optOutPaperless);
            console.log('Billing Info New Result=>',result);
            if (result.error) {
               configuration.getAttribute('Opt Out Direct Debit').required = true;
               configuration.getAttribute('Opt Out One Bill').required = true;
               configuration.getAttribute('Opt Out Paperless').required = true;
            } else {
                 configuration.getAttribute('Opt Out Direct Debit').required = false;
                 configuration.getAttribute('Opt Out One Bill').required = false;
                 configuration.getAttribute('Opt Out Paperless').required = false;
            }
        }
        return Promise.resolve(true);
    }; */

    /**
     * Hook executed before performing the default validations. If we return validation result as
     * false the attribute value will not be updated.
     *
     * @param {string} componentName - Name of the tab/component where the attribute belongs to.
     * @param {object} attribute - The attribute being updated
     * @param {boolean} validationResult - The result of the final validations performed.
     * @param {string} value - The new value being set.
     */

    Plugin.beforeAttributeValidation =
        async function(component, configuration, attribute, value) {
            let solution = await CS.SM.getActiveSolution();
            let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
            
            /***Site Access Attributes****/
         
            if (component.name == CLOUD_VOICE.siteAccess) {
                if (attribute.name == 'Site Name') {
                    let result = minMaxLengthRegex(value.value, 3, 20);
                    if (result.error) {
                        return Promise.resolve({
                            error: true,
                            message: result.messageDisplay
                        });
                    }
                    else {
                        attribute.errorMessage = null;
                        let configurationSiteGuid = configuration.getAttribute('guid').value;
                        for(let componentLoop of Object.values(solution.components) ) {
                            if (componentLoop.name!= CLOUD_VOICE.siteAccess && componentLoop.name!= CLOUD_VOICE.solution) {
                                for(let config of Object.values(componentLoop.schema.configurations)) {
                                    let configSiteGuid = config.getAttribute('Site GUID').value;
                                    if(configSiteGuid==configurationSiteGuid) {
                                        if(componentLoop.name==CLOUD_VOICE.UFP){
                                            let UFPattribute = config.getAttribute('Type');
                                            updateConfigName('Type',UFPattribute.displayValue, config, componentLoop.name,solution);
                                        }
                                        if(componentLoop.name==CLOUD_VOICE.CPE){
                                            let CPEattribute = config.getAttribute('Phone');
                                            updateConfigName('Phone',CPEattribute.displayValue, config, componentLoop.name,solution);
                                        }
                                        if(componentLoop.name==CLOUD_VOICE.LAN){
                                            let LANattribute = config.getAttribute('LAN Type');
                                            updateConfigName('LAN Type',LANattribute.displayValue, config, componentLoop.name,solution);
                                        }
                                        if(componentLoop.name==CLOUD_VOICE.cabling){
                                            let CabAttribute = config.getAttribute('Cabling Type');
                                            updateConfigName('Cabling Type',CabAttribute.displayValue, config, componentLoop.name,solution);
                                        }
                                        if(componentLoop.name==CLOUD_VOICE.SIPTswitch){
                                            let SwiAttribute = config.getAttribute('Switch Provision');
                                            updateConfigName('Switch Provision',SwiAttribute.displayValue, config, componentLoop.name,solution);
                                        }
                                    }
                                }
                            }
                        }
                        return Promise.resolve(true);
                    }
                }
                
                if (attribute.name=='SIP Users Required') {
                    if (!isNaN(attribute.value) && attribute.value>3000 ) return Promise.resolve({error: true,message: 'Can not have more than 3000 SIP users'});
                    else return Promise.resolve(true);
                }
                
                if (attribute.name == 'Reference Id'){
                    let accessAtt = configuration.getAttribute('Access New/Existing/OTT').value;
                    let accessTypeAtt = configuration.getAttribute('Access Type').value;
                    let result = validateReferenceIDForAccess(accessAtt,accessTypeAtt,value.value);

                    if (result!=null && result.error) {
                        configuration.status = false;
                        configuration.statusMessage = result.messageDisplay;
                        attribute.error=true;
                        return Promise.resolve({
                            error: true,
                            message: result.messageDisplay
                        });
                    } else {
                        configuration.status = true;
                        attribute.errorMessage = null;
                        attribute.error=false;
                        return Promise.resolve(true);
                    }
                }
                if (attribute.name=='Upload Speed (Mbps)') {
                    let result = validateFieldsByType(attribute,'PositiveNumber');
                    if (result!=null) return Promise.resolve({error: true,message: result});
                    else return Promise.resolve(true);
                }
                if (attribute.name=='Site Contact Email') {
                    let result = validateFieldsByType(attribute,'Email');
                    if (result!=null) return Promise.resolve({error: true,message: result});
                    else return Promise.resolve(true);
                }
                if (attribute.name=='Site Contact Phone') {
                    let result = validateFieldsByType(attribute,'Phone');
                    if (result!=null) return Promise.resolve({error: true,message: result});
                    else return Promise.resolve(true);
                }
            }   
            if(component.name==CLOUD_VOICE.UFP) {
                if (attribute.name=='Quantity' || attribute.name=='User Setups Required') {
                    let result = validateFieldsByType(attribute,'PositiveWholeNumber');
                    if (result!=null) return Promise.resolve({error: true,message: result});
                    else return Promise.resolve(true);
                }
            }
            
            if(component.name==CLOUD_VOICE.CPE) {
                if (attribute.name=='Quantity') {
                    let result = validateFieldsByType(attribute,'PositiveWholeNumber');
                    if (result!=null) return Promise.resolve({error: true,message: result});
                    else return Promise.resolve(true);
                }
            }
            if(component.name==CLOUD_VOICE.LAN) {
                if (attribute.name=='Additional Ports') {
                    let result = validateFieldsByType(attribute,'PositiveWholeNumber');
                    if (result!=null) return Promise.resolve({error: true,message: result});
                    else return Promise.resolve(true);
                }
            }
            
            if(component.name==CLOUD_VOICE.solution) {
                if (attribute.name=='Additional Numbers Required') {
                    let result = validateFieldsByType(attribute,'PositiveWholeNumber');
                    if (result!=null) return Promise.resolve({error: true,message: result});
                    else return Promise.resolve(true);
                }
            }
            
            /*****OE Attributes*****/

            if (component.name == 'Site Details') {
                if (attribute.name == 'CV Number') {
                    if (!validateNumberLength(attribute.value, 3, 15))
                        return Promise.resolve({
                            error: true,
                            message: 'Must be a number between 3 and 15 digits'
                        });
                }
                if (attribute.name == 'Extension Length') {
                    if (!validateNumberLength(attribute.value, 2, 6))
                        return Promise.resolve({
                            error: true,
                            message: 'Must be a number between 2 and 6 digits'
                        });
                }

                if (attribute.name == 'Intra-site Dialling Code') {
                    if (!validateNumberLength(attribute.value, 3, 15))
                        return Promise.resolve({
                            error: true,
                            message: 'Must be a number between 3 and 15 digits'
                        });
                }
                
                
            }
             if (component.name == 'Enterprise Trunk' || component.name == 'Switch Trunk') {
                if(attribute.name == 'Concurrent Call Limit'){
                    var maxConcurrentCallsAvailable = Number(mainConfiguration.getAttribute('SIP Channels Required').value);
                    if(isNaN(maxConcurrentCallsAvailable)) maxConcurrentCallsAvailable=0;
                    var attributeValue =Number(attribute.value);
                    if(isNaN(attributeValue)) attributeValue=0;
                    if(attributeValue > maxConcurrentCallsAvailable){
                        return Promise.resolve({
                            error: true,
                            message: 'CAC Limit is higher than the total number of SIP Channels added to the solution. Please reduce CAC Limit or add more SIP Channels'
                        });
                    }
                    
                    // if(component.name == 'Switch Trunk') {
                    //     let parentConfiguration = solution.getConfiguration(configuration.parentConfiguration);
                    //     modeAtt=parentConfiguration.getAttribute('Mode').displayValue;
                    //     if((modeAtt=='Gateway' && attributeValue!=8 && attributeValue!=30) || (modeAtt=='SBC' && attributeValue!=60 && attributeValue!=250) ) {
                    //         return Promise.resolve({
                    //             error: true,
                    //             message: ' CAC Limit must be 8 or 30 for Gateway Mode, or 60 or 250 for SBC Mode'
                    //         });
                    //     }
                    // }
                    return Promise.resolve({
                        error: false,
                        message: ''
                    });
                }
            }
            
            //LPDE-499
             if(attribute.name == 'Quantity' && component.name == 'Number Configuration'){
                totalQuantity=0;
                newNumbersRequired=0;
               	for(let config of Object.values(component.schema.configurations)){
               	    for(let attri of Object.values(config.attributes)){
               	        if(attri.name=='Quantity'){
        			        totalQuantity=totalQuantity+Number(attri.value);
        		        }
               	    }
               	}
            	newNumbersRequired = mainConfiguration.getAttribute('New Numbers Required').value;

        	    if(totalQuantity>newNumbersRequired){
        	    	return Promise.resolve({
                            error: true,
                            message: 'Quantity canÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢t be more than New Numbers Required'
                        });
        	    }else{
                         return Promise.resolve({
                            error: false,
                            message: ''
                        });
                  }
            }
            return Promise.resolve(true);
        };

    /**
     * Hook executed once attribute validation is completed.If we return validation result as
     * false the attribute value will not be updated.
     *
     * @param {string} componentName - Name of the tab/component where the attribute belongs to.
     * @param {object} attribute - The attribute being updated
     * @param {boolean} validationResult - The result of the final validations performed.
     * @param {string} value - The new value being set.
     */
    Plugin.afterAttributeValidation =
        async function(component, configuration, attribute, validationResult, value) {
            let solution = await CS.SM.getActiveSolution();
            if(component.name==CLOUD_VOICE.CPE) {
                console.log('afterValidation', component, configuration, value);
                if(attribute.name==='Phone'){
                    if(value.displayValue.startsWith('BT Cloud Voice Access Control: Visitor')){
                        configuration.updateAttribute('Maintenance Contract Term', { value: null, showInUi: true } );

                        //Auto Add Addons
                        solution.getAddonsForConfiguration(configuration.guid).then( availableAddons => {
                            console.log(availableAddons);
                            for (let segment of Object.values(availableAddons)) {
                                for (let oneAddon of Object.values(segment.allAddons)) {
                                    var i = Object.values(configuration.getAddons()).findIndex((element) => { 
                                        return element.name === oneAddon.Add_On_Name__c;
                                    });
                                    if(i === -1) addAddOnProductConfiguration('CPE Add Ons', oneAddon, configuration.guid, 1, component);
                                }
                            }
                        });
                        
                    }
                    else {
                        configuration.updateAttribute('Maintenance Contract Term', { value: null, showInUi: false } );
                    }
                }
            }
            return Promise.resolve(true);
        };

    Plugin.beforeQuantityUpdated = async function(component, configurationGuid, oldQuantity, newQuantity) {
        console.log('beforeQuantityUpdated ',component, configurationGuid,  oldQuantity, newQuantity);
        let solution=await CS.SM.getActiveSolution();
            if(component.name==CLOUD_VOICE.UFP) {
                let currentConfig = solution.getConfiguration(configurationGuid);
                siteGUID=currentConfig.getAttribute('Site GUID').value;
                let siteConfig = solution.getConfiguration(siteGUID);
                siteType= siteConfig.getAttribute('Site Type').displayValue;
                if(siteType=='Cloud Voice - Homeworker' && newQuantity>1) {
                    CS.SM.displayMessage('Homeworker UFP can not be more than 1 in Quantity','error');
                    return Promise.resolve(false);
                }
            }

        return Promise.resolve(true);
    }

    Plugin.afterQuantityUpdated = async function(component, configurationGuid, oldQuantity, newQuantity) {
        console.log('afterQuantityUpdated ',component, configurationGuid,  oldQuantity, newQuantity);
        let solution=await CS.SM.getActiveSolution();
            if(component.name==CLOUD_VOICE.UFP || component.name==CLOUD_VOICE.CPE) {
                updateConfigurationAttributeValue('Quantity' ,newQuantity, newQuantity, true, solution, configurationGuid, false);
            }
            
        return Promise.resolve(true);
    }

    /**
     * Hook executed before the configuration is added via the UI add configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration
     */
    Plugin.beforeConfigurationAdd = function(component, configuration) {
        console.log('beforeConfigurationAdd');
        return Promise.resolve(true);
    };

    /**
     * Hook executed after the configuration is added via the UI add configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration
     */
    Plugin.afterConfigurationAdd = async function(component, configuration) {
        console.log('afterConfigurationAdd', component, configuration);
        var solution = await CS.SM.getActiveSolution();
        var mainConfiguration = Object.values(solution.schema.configurations)[0];

        if (component.name == CLOUD_VOICE.siteAccess) {
            var basketId = await CS.SM.getActiveBasketId();    

            updateConfigurationAttributeValue('Account ID', globalAccountID, globalAccountID, true, solution, configuration.guid, true);
            await updateCommercialProductLookup('Site Access', 'Site Access', null, 'Site Access Commercial Product', configuration, solution);
            updateConfigurationAttributeValue(
                            'BasketID',
                            configuration.basketId,
                            configuration.basketId,
                            false,
                            solution,
                            configuration.guid,
                            true
                        );

            checkIfBusinessTypeExists(component);
        }
        if(component.name ==CLOUD_VOICE.UFP){
            licenceSumUFP(solution);
        }
        
        if(component.name ==CLOUD_VOICE.CPE){
            await calculateAvailabilityUFP(solution);
            calculateSoftphoneLicence(solution);
        }
        if(component.name ==CLOUD_VOICE.SIPTswitch){
            checkIfSIPTexists(solution);
            changeUploadAttributeText('Import North Supply','Upload here');
        }
        return Promise.resolve(true);
    };

    /**
     * Hook executed before the Related product is added to the configuration
     *
     * @param {Object} component
     * @param {Object} configuration
     * @param {Object} relatedProduct
     */
    Plugin.beforeRelatedProductAdd = async function(component, configuration, relatedProduct) {
        //if addon quantity is empty or 0, put 1
        calculateAddonQuantity(component,configuration,relatedProduct);

        if(component.name ==CLOUD_VOICE.LAN){
            if (relatedProduct.groupName=='BT LAN' && relatedProduct.relatedProductName=='LAN Switches') {
                var errorExists = await validateLANAddonsGigabit(component,configuration,relatedProduct);
                if (errorExists) { 
                    CS.SM.displayMessage('You can not have both a Giga and a non-Giga LAN on the same site','error');
                    return Promise.resolve(false);
                }
            }
        }
        if(component.name ==CLOUD_VOICE.CPE && relatedProduct.name=='Phone Installation'){
            let solution = await CS.SM.getActiveSolution();
            let siteGuid = configuration.getAttribute('Site GUID').value;
            let siteAccessConfig = solution.getConfiguration(siteGuid);
            let SiteType = siteAccessConfig.getAttribute('Site Type').displayValue;
            if (SiteType=='Cloud Voice - Homeworker') {
                CS.SM.displayMessage('You can not add a Phone Installation on a Homeworker Site','error');
                return Promise.resolve(false);
            }
        }
        if(component.name ==CLOUD_VOICE.SIPTswitch){
            var siteName = configuration.getAttribute('Site Name');
            for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                if(attribute.name=='Site Name') attribute.value=siteName;
            }
        }
        
        return Promise.resolve(true);
    };

    /**
     * Hook executed after the related product is added to the configuration
     *
     * @param {string} componentName
     * @param {Object} configuration
     */
     Plugin.afterRelatedProductAdd = async function(component, configuration, relatedProduct) {
        console.log('afterRelatedProductAdd', configuration, relatedProduct);
        let solution = await CS.SM.getActiveSolution();
        
        if (component.name==CLOUD_VOICE.solution) {
            checkUserFeatures(configuration);
            checkSharerPlanAddons(configuration);
            checkConditionForSkillAddon(configuration);
            if(relatedProduct.name=='Unlimited' && relatedProduct.groupName=='UK') {
                //function for unlimited
                await calculateUnlimitedCallPlan(configuration,solution);
            }
        }
        
        if(component.name ==CLOUD_VOICE.siteAccess){
            validateCallCentreAddon(solution);                   
        }
        if((component.name == CLOUD_VOICE.CPE && relatedProduct.name=='Phone Installation') 
        || (component.name == CLOUD_VOICE.LAN && relatedProduct.name=='LAN Installation')){
            toggleSiteInstallations(solution,relatedProduct);
        }
        if(component.name == 'LAN'){
            await addDependentLANAddOns(component, configuration);
            validateLANAddOns(component,configuration,relatedProduct);
        }
        
        if(component.name ==CLOUD_VOICE.UFP){
            licenceSumUFP(solution);
        }

        if(component.name == 'Cabling'){
            // add attendence charges this is done with another function, not needed
            //addAttendenceChargesForCabling(component,configuration,relatedProduct);
        }
        
        if(component.name == CLOUD_VOICE.SIPTswitch){
            checkGatewayExistance(configuration);
            if(relatedProduct.name.includes('SIP Gateway (')) {
                checkGatewaySum(component);
            }
        }
        
        return Promise.resolve(true);
    }; 
    
 
    /**
     * Hook executed before the site product is added to the configuration
     *
     * @param {string} componentName
     * @param {Object} configuration
     */
    Plugin.beforeSiteProductAdd = function(componentName, configuration) {
        console.log('beforeSiteProductAdd');
        return Promise.resolve(true);
    };

    /**
     * Hook executed after the configuration is added via the UI add configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration
     */

    Plugin.afterSiteProductAdd = function(componentName, configuration, siteProduct) {
        console.log('afterSiteProductAdd', configuration, siteProduct);
        return Promise.resolve(true);
    };

    /**
     * Hook executed before the configuration is deleted via the UI delete configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration - Configuration object which is going to be deleted
     */
    Plugin.beforeConfigurationDelete = async function(component, configuration) {
        console.log('beforeConfigurationDelete');
        let solution=await CS.SM.getActiveSolution();

        if (component.name == CLOUD_VOICE.UFP || component.name == CLOUD_VOICE.CPE || component.name == CLOUD_VOICE.LAN
        || component.name == CLOUD_VOICE.cabling || component.name == CLOUD_VOICE.SIPTswitch) {
            let passed = checkAndPreventConfigurationDelete(solution,component,configuration);
            if (passed!=true) {
                CS.SM.displayMessage('You can not remove the last configuration related to this site: '+passed,'error');
                return Promise.resolve(false);
            }
        }
        return Promise.resolve(true);
    };

    /**
     * Hook executed after the configuration is deleted via the UI delete configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration - Configuration object which was deleted
     */
    Plugin.afterConfigurationDelete =async function(component, configuration) {
        console.log('afterConfigurationDelete');
        let solution=await CS.SM.getActiveSolution();

        if (component.name == CLOUD_VOICE.siteAccess) {
            await deleteRelatedSiteAccessComponents(configuration);
            checkIfBusinessTypeExists(component);
            sipTExists=false;
            for(let configLoop of Object.values(component.schema.configurations)) {
                if(configLoop.getAttribute('Site Type').displayValue=='SIP-T - Business Site') sipTExists=true;
            }
            if(!sipTExists) {
                var removed=false;
                var mainConfiguration=Object.values(solution.schema.configurations)[0];
                for(let OEconfig of Object.values(mainConfiguration.orderEnrichmentList)) {
                    if(OEconfig.configurationName.includes('Enterprise Trunk')) {
                        CS.SM.displayMessage(OEconfig.configurationName+' has been removed','info');
                        await solution.deleteOrderEnrichmentConfiguration( mainConfiguration.guid, OEconfig.guid, true )
                    }
                }
            }
        }
        
        if(component.name ==CLOUD_VOICE.CPE || component.name ==CLOUD_VOICE.UFP){
            calculateSoftphoneLicence(solution);
            await calculateAvailabilityUFP(solution);
        }
        
        if(component.name ==CLOUD_VOICE.SIPTswitch){
            checkIfSIPTexists(solution);
        }
        
        return Promise.resolve(true);
    };


    /**
     * Hook executed before we save the complex product to SF. We need to resolve the promise as a
     * boolean. The save will continue only if the promise resolve with true.
     *
     * @param {Object} complexProduct
     */
    Plugin.beforeSave = async function(solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
        console.log('beforeSave');
        console.log(solution);

        if (solution.isMACD == true) {
            let mainConfiguration = Object.values(solution.schema.configurations)[0];
            updateConfigurationAttributeValue('Replaced Config Id' , mainConfiguration.replacedConfigId, mainConfiguration.replacedConfigId, true, solution, mainConfiguration.guid, false);
        }

        saveSiteNADKeys(solution);
        await calculateSIPChannelsRequired(solution);
        await calculatePowerPortOutlet(solution);
        await licenceSumUFP(solution);
        checkIfBusinessTypeExists(solution);
        validateCallCentreAddon(solution);
        await calculateNumbersRequired(solution);
        await autoAddAttendance(solution,null);
        await addPatchCordsAddon(solution,null);
        calculateAndAddDeliveryAddOn(solution);

        for(let component of Object.values(solution.components)) {
            for(let configuration of Object.values(component.schema.configurations)) {
                if (configuration.error == true || configuration.status == false) {
                    CS.SM.displayMessage('Invalid configurations in the basket', 'error');
                    return Promise.resolve(false);
                }
            }
        }
        return true;
    };

    /**
     * Hook executed before we save the complex product to SF. We need to resolve the promise as a
     * boolean. The save will continue only if the promise resolve with true.
     *
     * @param {Object} complexProduct
     */
    Plugin.afterSave = async function( solutionResult, solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
        console.log('afterSave', solution, configurationsProcessed, saveOnlyAttachment, configurationGuids);

        let ASolution=await CS.SM.getActiveSolution();
        let currentBasket = await CS.SM.getActiveBasket();
        let basketId = await CS.SM.getActiveBasketId();
        let servicePlanConfiguration = ASolution.getConfiguration(Object.keys(ASolution.schema.configurations)[0]);

        reloadUIafterSolutionLoaded(ASolution);

        await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole',{"configId" : servicePlanConfiguration.id, 'action' : 'CS_SaveTSV', 'Total Solution Value' : ASolution.totalContractValue, 'Total One Off Value' : ASolution.totalOneOffCharge, 'Total Recurring Value' : ASolution.totalRecurringCharge, 'solutionId' : ASolution.solutionId});
        await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole',{"basketId" : basketId, 'action' : 'CS_SolutionConsoleBasketCalculation'});
        return true;
    };

    /**
     * Hook executed before we save the complex product to SF. We need to resolve the promise as a
     * boolean. The save will continue only if the promise resolve with true.
     *
     * @param {Object} complexProduct
     */
    Plugin.afterSolutionLoaded = async function(previousSolution, loadedSolution) {
        console.log('AFTER SOL LOADED',loadedSolution);
        let mainConfiguration = loadedSolution.getConfiguration(Object.keys(loadedSolution.schema.configurations)[0]);
        var basketId = await CS.SM.getActiveBasketId();
        reloadUIafterSolutionLoaded(loadedSolution);
        try{
            let currentBasket = await CS.SM.getActiveBasket();
            console.log('currentBasket',currentBasket);
            let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole',{"basketId" : basketId, 'action' : 'CS_LookupQueryForAccountDetails'});
            if(result["DataFound"] == true) globalAccountID=result["AccountId"];
            console.log('result',result);
            console.log('globalAccountID',globalAccountID);

            if (mainConfiguration.getAttribute('Contract Term').value == '') {
                // Set the Main component configuration Name only once
                updateConfigurationAttributeValue(
                    'Config Name',
                    'Cloud Voice Service',
                    'Cloud Voice Service',
                    false,
                    loadedSolution,
                    Object.keys(loadedSolution.schema.configurations)[0],
                    true
                );
            }
        } catch (error) {}

        
        //Create default site access configuration only once
        for( let component of Object.values(loadedSolution.components)) {
            if(component.name==CLOUD_VOICE.siteAccess) {
                console.log(component.schema.configurations);
                if(Object.values(component.schema.configurations).length<1) {
                    createDefaultSiteAccessConfiguration(loadedSolution);
                }
            }
        }
        
        checkUserFeatures(mainConfiguration);
        checkSharerPlanAddons(mainConfiguration);
        checkConditionForSkillAddon(mainConfiguration);
        checkIfSIPTexists(loadedSolution);

        return Promise.resolve(true);
    };

    Plugin.beforeConfigurationClone =async function(component, clonedConfigurations)  {
        console.log('yea',component);
        //if(component.schema.name=='LAN Schema' || component.schema.name=='Cabling Schema') {
        CS.SM.displayMessage('Can not clone configurations','error');
        return Promise.resolve(false);

    }

    Plugin.afterConfigurationClone = async function(component, clonedConfigurations)  {
        console.log('delet',component, clonedConfigurations);
        if (component.name == CLOUD_VOICE.siteAccess) {
            clonedConfigurations.forEach(config => {
               let siteType = config.getAttribute('Site Type');
               if(siteType != '') {
                   createChildConfigurations('UFPs', config);
                   createChildConfigurations('CPEs', config);
                   createChildConfigurations('LAN', config);
                   createChildConfigurations('Cabling', config);
                   createChildConfigurations('Switch', config);
               }
            });
        }
        var solution = await CS.SM.getActiveSolution();
        calculateSoftphoneLicence(solution);
    }

    Plugin.beforeRelatedProductDelete =async function(component, configuration, relatedProduct) {

        console.log('beforeRelatedProductDelete', configuration, relatedProduct);
        if(component.name ==CLOUD_VOICE.LAN){
            if(relatedProduct.groupName === 'PSU'){
                let PeripheralAddOn = await getExistingRelatedProductByGroupName(configuration,'Peripheral');
                if(PeripheralAddOn.length > 0){
                    CS.SM.displayMessage('Can not delete PSU as Vulcan is added','error');
                    return Promise.resolve(false);
                } 
            }
        }
        return Promise.resolve(true);
    };

    Plugin.afterRelatedProductDelete =async function(component, configuration, relatedProduct) {
        console.log('afterRelatedProductDelete', configuration, relatedProduct);
        var solution=await CS.SM.getActiveSolution();
        
        if (component.name==CLOUD_VOICE.solution) {
            checkUserFeatures(configuration);
            checkSharerPlanAddons(configuration);
            checkConditionForSkillAddon(configuration);
			
            if(relatedProduct.name=='Unlimited' && relatedProduct.groupName=='UK') {
                //function for unlimited
                await calculateUnlimitedCallPlan(configuration,solution);
            }
        }
        
        if(component.name ==CLOUD_VOICE.LAN){
            await addDependentLANAddOns(component, configuration);
            validateLANAddOns(component,configuration,relatedProduct);
        }
        if((component.name == CLOUD_VOICE.CPE && relatedProduct.name=='Phone Installation') 
        || (component.name == CLOUD_VOICE.LAN && relatedProduct.name=='LAN Installation')){
            toggleSiteInstallations(solution,relatedProduct);
        }
        if(component.name ==CLOUD_VOICE.siteAccess){
            validateCallCentreAddon(solution);                   
        }
        
        if(component.name ==CLOUD_VOICE.UFP){
            licenceSumUFP(solution);      
        }
        
        if(component.name == CLOUD_VOICE.SIPTswitch){
            checkGatewayExistance(configuration);
            if(relatedProduct.name.includes('SIP Gateway (')) {
                checkGatewaySum(component);
            }
        }
        reloadUIafterSolutionLoaded(solution);
        return Promise.resolve(true);
    };
    Plugin.beforeSiteProductDelete = function(componentName, configuration, siteProduct) {
        console.log('beforeSiteProductDelete', configuration, siteProduct);
        return Promise.resolve(true);
    };

    Plugin.afterSiteProductDelete = function(componentName, configuration, siteProduct) {
        console.log('afterSiteProductDelete', configuration, siteProduct);
        return Promise.resolve(true);
    };

    /**
     * Hook before import file
     *
     * @param {String} component
     * @param {Object} fileData
     */
    Plugin.beforeImport = function(component, fileData) {
        console.log('Plugin beforeImport', component, fileData);
        return Promise.resolve(fileData);
    };

    /**
     * Hook after import file
     *
     * @param {String} component
     */
    Plugin.afterImport = function(component) {
        console.log('Plugin afterImport', component);
        return Promise.resolve(component);
    };

    /**
     * Hook executed before the configuration is added via the UI add configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration - Empty configuration object with attributes to be set
     */
    Plugin.beforeOrderEnrichmentConfigurationAdd = function(solution,
        configuration,
        orderEnrichmentConfiguration) {
            
        console.log('beforeOrderEnrichmentConfigurationAdd', solution,
            configuration,
            orderEnrichmentConfiguration
        );
        if(orderEnrichmentConfiguration.configurationName.includes('Enterprise Trunk')){
            var canAdd = false;
            for(let component of Object.values(solution.components) ) {
                if (component.name== CLOUD_VOICE.siteAccess) {
                    for(let configuration of Object.values(component.schema.configurations)) {
                        if( configuration.getAttribute('Site Type').displayValue=='SIP-T - Business Site') canAdd = true;
                    }
                }
            }
            if (!canAdd) {
                CS.SM.displayMessage('You can not add an Enterprise Trunk without having a SIP-T site','error');
                return Promise.resolve(false);
            }
        }
        /*var passed = checkAndPreventOE(solution,configuration,orderEnrichmentConfiguration);
        console.log('passed',passed);//if passed is false, prevent adding
        */

         updateOrderEnrichmentSiteDetailsFields(solution,configuration,orderEnrichmentConfiguration);

        return Promise.resolve(true);
    };

    /**
     * Hook executed after the configuration is added via the UI add configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration - Configuration object with attributes to be set
     */
    Plugin.afterOrderEnrichmentConfigurationAdd = function(component,
        configuration,
        orderEnrichmentConfiguration) {
        console.log('afterOrderEnrichmentConfigurationAdd', component,
            configuration,
            orderEnrichmentConfiguration
        );
        
        if(orderEnrichmentConfiguration.name.includes('Switch Trunk')) {
            checkGatewaySum(component);
        }

        if(orderEnrichmentConfiguration.name.includes('Service Details') || orderEnrichmentConfiguration.name.includes('Billing Information')
         || orderEnrichmentConfiguration.name.includes('Site Details') || orderEnrichmentConfiguration.name.includes('999 Emergency Details')
         || orderEnrichmentConfiguration.name.includes('Additional Contacts')) {
             for(let OEatt of Object.values(orderEnrichmentConfiguration.attributes)) {
                 if (OEatt.name=='Account ID') {
                     OEatt.value=globalAccountID;
                     OEatt.displayValue=globalAccountID;
                 }
             }
        }

        return Promise.resolve(true);
    };

    /**
     * Hook executed before the configuration is deleted via the UI delete configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration - Configuration object which is going to be deleted
     */
    Plugin.beforeOrderEnrichmentConfigurationDelete = function(componentName,
        configuration,
        orderEnrichmentConfiguration) {
        console.log('beforeOrderEnrichmentConfigurationDelete', componentName,
            configuration,
            orderEnrichmentConfiguration
        );
        return Promise.resolve(true);
    };

    /**
     * Hook executed after the configuration is deleted via the UI delete configuration button
     *
     * @param {string} componentName
     * @param {Object} configuration - Configuration object which was deleted
     */
    Plugin.afterOrderEnrichmentConfigurationDelete = function(component,
        configuration,
        orderEnrichmentConfiguration) {
        console.log('afterOrderEnrichmentConfigurationDelete', component,
            configuration,
            orderEnrichmentConfiguration
        );
        if(orderEnrichmentConfiguration.name.includes('Switch Trunk')) {
            checkGatewaySum(component);
        }
        return Promise.resolve(true);
    };


    console.log('plugin: ', Plugin);
}

/*****End of Hooks*****/

/*****Start of Cloud Voice Service functions*****/

//Create the default Site Access configuration

async function updateOrderEnrichmentSiteDetailsFields(component,configuration,orderEnrichmentConfiguration){
    console.log('updateOrderEnrichmentSiteDetailsFields');
    console.log('component.name=>',component.name);
    console.log('configuration=>',configuration);
    console.log('orderEnrichmentConfiguration=>',orderEnrichmentConfiguration);
    Access = 'N';
    if (component.name== CLOUD_VOICE.siteAccess) { //Component
            Object.values(component.schema).forEach((solutionAttri) => {
                console.log('solutionAttri=>',solutionAttri);    
            });

    		Object.values(configuration.attributes).forEach((configAttribute) => {//Configuration Attributes
              	console.log('attribute.name=>',configAttribute.name);
              	if(configAttribute.name=='Upload Speed (Mbps)'){
              	    console.log('uploadSpeed=>',configAttribute.value);
              	    uploadSpeed = Number(configAttribute.value);
              	}
              	if(configAttribute.name=='Call Quality (Codec)'){
              	    console.log('codecSelected=>',configAttribute.value);
              	    codecSelected = Number(configAttribute.value);
              	}
    		    if(configAttribute.name=='Access New/Existing/OTT' && configAttribute.value=='New' || configAttribute.value=='Existing'){
    		        console.log('Access',configAttribute.value); 
    		        Access = 'Y';
    		    }
    		});
    		
    		console.log('orderEnrichmentConfiguration.attributes=>',orderEnrichmentConfiguration.attributes);
    		if(Access == 'Y'){  
    		    Object.values(orderEnrichmentConfiguration.attributes).forEach((oeAttribute) => {
    			    console.log('oeAttribute.name=>',oeAttribute.name);
    				if(oeAttribute.name == 'Max Concurrent Calls Available'){
    				    //(Upload Speed (Mbps) * 1000)/Codec Quality value
    				    cacValue = ( uploadSpeed * 1000 ) / codecSelected;
    				    cacValue = Math.round(cacValue *100)/100 ;
    				    // (124 * 1000) / 120
    					oeAttribute.value = cacValue;
    					 //cacValue = oeAttribute.value;
    					 console.log('CAC value',cacValue);
    				}
    			});
    	    }	
            // });
    }
}

function validateFields(recordCount){
    console.log('validateFields');
    //console.log('component.name=>',component.name);
    //console.log('orderEnrichmentConfiguration=>',orderEnrichmentConfiguration);
    //if (component.name== CLOUD_VOICE.siteAccess) {
	    //recordCount=0;
        //Object.values(configuration.orderEnrichments).forEach((orderEnrichment) => {
            //console.log('configuration.orderEnrichments=>',orderEnrichment);
	        //recordCount=recordCount+1;	
	        console.log('Record Count=>'+recordCount);
	        errorMessage = 'Record can not be more than 1';
	        if(recordCount>1){
		        console.log('Record cant be more than 1');
		        return {
                    error: true,
                    messageDisplay: errorMessage
                };
		    }else {
                return {
                    error: false,
                    messageDisplay: ''
                };
            }
        //});
    //}
}

async function createDefaultSiteAccessConfiguration(solution) {
    console.log('createDefaultSiteAccessConfiguration');
    let component = solution.getComponentByName(CLOUD_VOICE.siteAccess);
    let configuration = component.createConfiguration();
    let addedSiteConfiguration;

    component.addConfiguration(configuration).then(addedConfiguration =>console.log(addedConfiguration));


    await updateCommercialProductLookup('Site Access', 'Site Access', null, 'Site Access Commercial Product', configuration, solution);

    updateConfigurationAttributeValue(
                                'BasketID',
                                configuration.basketId,
                                configuration.basketId,
                                false,
                                solution,
                                configuration.guid,
                                true
                            );

}


function updateConfigName(attrName, attValue, configuration, componentName,solution) {
    console.log('attrName: ',attrName,'attValue: ',attValue);
    let siteAccessConfig = solution.getConfiguration(configuration.getAttribute('Site GUID').value);
    let access = siteAccessConfig.getAttribute('Site Name').value;

    
	let siteName = access + ': ' + attValue;
	if(attrName=='Site Name') siteName=access;
	
	updateConfigurationAttributeValue(
                                        'Config Name',
                                         siteName,
                                         siteName,
                                         false,
                                         solution,
                                         configuration.guid,
                                         true
                );
    configuration.configurationName=siteName;
    console.log(configuration);
}

async function updateCablingDependentFields(configuration,attribute) {
    console.log('updateCablingDependentFields')
    let solution = await CS.SM.getActiveSolution();
    
    console.log('Cabling Type Survey/Existing=>',attribute.displayValue);
    if(attribute.name=='Cabling Type' && attribute.displayValue == 'Cabling Survey (NG Bailey)' || attribute.displayValue == 'Existing / Not Required'){
        console.log('Update Qualification Creteria');
        updateConfigurationAttributeValue(
            'Qualification Criteria Agreed',
            '',
            '',
            true,
            solution,
            configuration.guid,
            false
        );
        updateConfigurationAttributeValue(
            'cablePlanQualificationURLString',
            '',
            '',
            true,
            solution,
            configuration.guid,
            false 
        );
    }
    if(attribute.name=='Cabling Type' && attribute.displayValue == 'Cabling Survey (NG Bailey)'){
            console.log('Update One Off Charge False=>',configuration.getAttribute('Survey One Off Charge').value);
            //configuration.getAttribute('Survey One Off Charge').readOnly = false;
            updateConfigurationAttributeValue(
                        'Survey One Off Charge',
                        '',
                        '',
                        false,
                        solution,
                        configuration.guid,
                        false
                    );
            updateConfigurationAttributeValue(
                        'Survey One Off Charge',
                        0.00,
                        0.00,
                        false,
                        solution,
                        configuration.guid,
                        false
                    );
        }else
        {
            console.log('Update One Off Charge True=>',configuration.getAttribute('Survey One Off Charge').value);
            //configuration.getAttribute('Survey One Off Charge').readOnly = true;
           updateConfigurationAttributeValue(
                        'Survey One Off Charge',
                        '',
                        '',
                        true,
                        solution,
                        configuration.guid,
                        false
                    );
            updateConfigurationAttributeValue(
                        'Survey One Off Charge',
                        0.00,
                        0.00,
                        true,
                        solution,
                        configuration.guid,
                        false
                    );
        }
    
    
}

function validateBillingInfoExisting(billingAccount,bacReference){
    if(billingAccount == 'Existing' && bacReference == ''){
        console.log('`In the Billing Account Validation Existing');
        errorMessage = 'BAC Reference is required for Existing Billing Account';
        
        return {
            error: true,
            messageDisplay: errorMessage
        };
    }else
    {
        return {
            error: false,
            message: ''
        };
    }
}

function validateBillingInfoNew(billingAccount,optOutDirectDebit,optOutOneBill,optOutPaperless){
    if(billingAccount == 'New' && optOutDirectDebit != true || optOutOneBill != true || optOutPaperless != true){
        console.log('`In the Billing Account Validation New');
        errorMessage = 'Fields are required';
        
        return {
            error: true,
            messageDisplay: errorMessage
        };
    }else
    {
        return {
            error: false,
            message: ''
        };
    }
}
/*****Start of Site Access functions*****/

//after site access is created, create child access and link them to site access
async function createChildConfigurations(componentName, configuration){

    let siteType = configuration.getAttribute('Site Type').value;
    let siteName = configuration.getAttribute('Site Name').value;
	let contractTerm = configuration.getAttribute('Contract Term').value;
	let quantity = '';

    if(componentName == CLOUD_VOICE.UFP && (siteType == 'Cloud Voice - Business Site' || siteType == 'Cloud Voice - Homeworker') ){
	    createChildComponentForSiteAccess(componentName,'Type','','UFP',contractTerm, quantity, siteType, siteName,configuration.guid);
	}
	if(componentName == 'CPEs' && (siteType == 'Cloud Voice - Business Site' || siteType == 'Cloud Voice - Homeworker')){
		createChildComponentForSiteAccess(componentName,'','','',contractTerm, quantity, siteType, siteName,configuration.guid);
	}
	if(componentName == 'LAN' && siteType == 'Cloud Voice - Business Site'){
        createChildComponentForSiteAccess(componentName,'','','',contractTerm, quantity, siteType, siteName,configuration.guid);
	}
	if(componentName == 'Cabling' && siteType == 'Cloud Voice - Business Site'){
	    createChildComponentForSiteAccess(componentName,'','','',contractTerm, quantity, siteType, siteName,configuration.guid);
	}
	if(componentName == 'Switch' && siteType == 'SIP-T - Business Site'){
	    createChildComponentForSiteAccess(componentName,'','','',contractTerm, quantity, siteType, siteName,configuration.guid);
	}
}

//after site access is created, create child access and link them to site access
async function createChildComponentForSiteAccess(componentName, commercialProductAttribute, grouping, module, contractTerm, quantity, siteType, siteName, siteAccessGuid){
	var attributeArray = [];
	attributeArray= [{
						name: 'Site Type',
						value : {
						    value : siteType,
						    displayValue : siteType
						}
					 },{
					    name: 'Site GUID',
						value : {
						    value : siteAccessGuid ,
						    displayValue : siteAccessGuid
						}
					 },{
						name: 'Site Access',
						value : {
						    value : siteAccessGuid ,
						    displayValue : siteName,
						    readOnly : true
						}
					},{
						name: 'Site Name',
						value : {
						    value : siteName ,
						    displayValue : siteName
						}
					 }];

	if (commercialProductAttribute) {
	    if (quantity == '')	quantity = 1;
        let inputMap={};
        inputMap["module"] = module;
        inputMap["grouping"] = grouping;
        inputMap["contractTerm"] = contractTerm;
        inputMap["action"] = 'CS_LookupQueryForSolutionConsole';

        currentBasket = await CS.SM.getActiveBasket();
        let result= await  currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);

        var attributeMap = [];

        if(result["DataFound"] == true){
        	attributeMap= [{
    						name: commercialProductAttribute,
    						value : {
    						    value : result["PriceItemId"],
    							displayValue : result["PriceItemName"]
    							}
    						},{
    							name : 'Config Name',
    							value : {
    							    value : siteName+' : '+result["PriceItemName"] +' x '+quantity,
    							    displayValue : siteName+' : '+result["PriceItemName"] +' x '+quantity
    						}
    		}];
            
        }

		attributeArray.push.apply(attributeArray, attributeMap);
	}
	else {
		var conFifAttItem={
						name: 'Config Name',
						value : {
						    value : siteName + ' :',
						    displayValue : siteName + ' :'
						}
		};
		attributeArray.push(conFifAttItem);
    }

	var solution = await CS.SM.getActiveSolution();
    if(componentName=='Switch') {
        careLevel=Object.values(solution.schema.configurations)[0].getAttribute('Care Level').displayValue;
        attributeArray.push({
							name: 'Care Level',
							value : {
							    value : careLevel,
							    displayValue : careLevel
							}
        });
    }
    
    if(componentName==CLOUD_VOICE.UFP) {
        let solution=await CS.SM.getActiveSolution();
        let mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
        let callRecordingStorageOption = mainConfiguration.getAttribute('Call Recording Storage Option').value;
        if (callRecordingStorageOption=='') callRecordingStorageOption='.';

        attributeArray.push({
							name: 'Contract Term',
							value : {
							    value : contractTerm,
							    displayValue : contractTerm
							}},
							{
							name: 'Call Recording Storage Option',
							value : {
							    value : callRecordingStorageOption,
							    displayValue : callRecordingStorageOption
							}}
                            );
    }
      
	let component = solution.getComponentByName(componentName);
	console.log('Attribute Data', attributeArray);
    let configuration = component.createConfiguration(attributeArray);
    console.log('Adding Configuration',configuration);
    component.addConfiguration(configuration).then(addedConfiguration => console.log(addedConfiguration));
}

//remove any configurations related to site access passed
async function deleteRelatedSiteAccessComponents(configuration) {
    let solution = await CS.SM.getActiveSolution();
    let components = solution.getComponents();

    Object.values(components['components']).forEach(compo => {
        if (compo.name == CLOUD_VOICE.UFP || compo.name == 'CPEs' || compo.name == 'LAN' || compo.name == 'Cabling' || compo.name=='Switch') {
            Object.values(compo.schema.configurations).forEach(config => {
                if (config.getAttribute('Site GUID').value == configuration.guid) {
                    compo.deleteConfiguration(config.guid);
                }
            });
        }
    });
}

async function renameRelatedSiteAccessComponents(siteName, configuration) {
        let solution = await CS.SM.getActiveSolution();
        let components = solution.getComponents();

        Object.values(components['components']).forEach(compo => {
            if (compo.name == CLOUD_VOICE.UFP || compo.name == 'CPEs' || compo.name == 'LAN' || compo.name == 'Cabling') {
                Object.values(compo.schema.configurations).forEach(config => {
                    if (config.getAttribute('Site GUID').value == configuration.guid) {
                        let currentConfigName = config.getAttribute('Config Name').value;
                        updateConfigurationAttributeValue(
                                        'Config Name',
                                         siteName + ' ' + currentConfigName.substr(currentConfigName.indexOf(':'), currentConfigName.length ),
                                         siteName + ' ' + currentConfigName.substr(currentConfigName.indexOf(':'), currentConfigName.length ),
                                         false,solution,config.guid,true
                        );
                    }
                });
            }
        });
}

function validateLeadTimeForAccess(siteRequiredByDate, configuration) {
    let siteReqDate = new Date(siteRequiredByDate);
    let leadTimeDate = new Date();
    let today = new Date();
    leadTimeDate.setDate(today.getDate() + parseInt(CONSTANT_PROPERTIES.LEAD_TIMES[configuration.getAttribute('Access Type').value]));
    let leadTimeDateValue= configuration.getAttribute('Lead Time').displayValue;
    if ( leadTimeDateValue!='See BTnet Product' && leadTimeDateValue!='' &&  (siteReqDate < leadTimeDate)) {
         configuration.status = false;
         configuration.statusMessage = 'Site Required by Date must be within Lead Time ' + CONSTANT_PROPERTIES.LEAD_TIMES[configuration.getAttribute('Access Type').value] + ' days';
     }
     else {
         if (configuration.statusMessage!= null && configuration.statusMessage.includes('Site Required by Date must be within Lead Time')){
             configuration.status = true;
             configuration.errorMessage = null;
         }
    }
}

//based on access and access type attribute, validating reference ID
function validateReferenceIDForAccess(access, accessType, refIdValue) {
    console.log('validateReferenceIDForAccess');
    if(refIdValue=='') {
        return {
            error: false,
            message: ''
        };
    }
    if (access === 'Existing') {
        if (accessType == 'BT Fibre' || accessType == 'BT Broadband') {
            let regexStr = /^(?=\d{10,11}$)(01|02|066)\d+/;
            if (regexStr.test(refIdValue) == false) {
                errorMessage = 'Reference Id is not Valid, it should be of 10 or 11 digits and start with 01,02,066.';
                return {
                    error: true,
                    messageDisplay: errorMessage
                };

            } else {
                return {
                    error: false,
                    message: ''
                };
            }
        } else if (accessType == 'BT Net' && access!='New') { 
            let regexStr = /^(FTIP)\d{9}$/;
            if (regexStr.test(refIdValue) == false) {
                errorMessage = 'Reference Id is not Valid, it should start with FTIP followed by 9 digits';
                return {
                    error: true,
                    messageDisplay: errorMessage
                };
            } else {
                return {
                    error: false,
                    message: ''
                };
            }
        } else if (accessType == 'IPCUK') {
            let regexStr = /^(VPNN)\d{9}$/;
            if (regexStr.test(refIdValue) == false) {
                errorMessage = 'Reference Id is not Valid, it should start with VPNN followed by 9 digits';
                return {
                    error: true,
                    messageDisplay: errorMessage
                };
            } else {
                return {
                    error: false,
                    message: ''
                };
            }
        } else {
            return {
                error: false,
                message: ''
            };
        }
    } else if (access === 'New') {
        if (accessType == 'BT Fibre' || accessType == 'BT Broadband') {
            let regexStr = /^(BT)\d{6}$/;
            if (regexStr.test(refIdValue) == false) {
                errorMessage = 'Reference Id is not Valid, it should start with BT followed by 6 digits';
                return {
                    error: true,
                    messageDisplay: errorMessage
                };
            } else {
                return {
                    error: false,
                    message: ''
                };
            }
        } else if (accessType == 'IPCUK') {
            let regexStr = /^(VPNN)\d{9}$/;
            if (regexStr.test(refIdValue) == false) {
                errorMessage = 'Reference Id is not Valid, it should start with VPNN followed by 9 digits';
                return {
                    error: true,
                    messageDisplay: errorMessage
                };
            } else {
                return {
                    error: false,
                    message: ''
                };
            }
        } else {
            return {
                error: false,
                message: ''
            };
        }
    }
}


function validateRouterUsedForSiteAccess(accessType,make,model,QosEnabled,refIdValue){
    console.log('validateRouterUsedForSiteAccess');
    errorMessage=''; 
    if ((accessType == 'BT Fibre' || accessType == 'BT Broadband') && (refIdValue == '' || refIdValue == 'Please Select')) {
        errorMessage = 'Please select Router Used which is required for BT Fibre and BT Broadband Access Type';
    }
    if ((make == '' || model == '' || QosEnabled == '' || QosEnabled == 'Please Select') && refIdValue == 'Other') {
        errorMessage = 'Make, Model and QoS Enabled are required When Router Used is Other';
    }
    return errorMessage;
}

//LPDE-457 must be at least one business type per solution
function checkIfBusinessTypeExists(component){
    if (component.name== CLOUD_VOICE.siteAccess) {
        var businessExists=false;
        var homeworkerExists=false;
        Object.values(component.schema.configurations).forEach((configuration) => {
            Object.values(configuration.attributes).forEach((attribute) => {
                if (attribute.name=='Site Type' && attribute.value=='Cloud Voice - Business Site') {
                    businessExists=true;
                }
                if (attribute.name=='Site Type' && attribute.value=='Cloud Voice - Homeworker') {
                    homeworkerExists=true;
                }
            });
        });
        Object.values(component.schema.configurations).forEach(configuration => {
            configuration.errorMessage=configuration.guid;
            if (!businessExists && homeworkerExists) {
                configuration.status = false;
                configuration.statusMessage = 'Site can not have a Homeworker Type without a Business Site Type';
            }
            else {
                if (configuration.statusMessage == 'Site can not have a Homeworker Type without a Business Site Type'){
                    configuration.status = true;
                    configuration.errorMessage = null;
                }
            }
        });
    }
}

//462 calculate total power required
async function calculatePower(solution){
    var start = new Date();
    console.log('power started '+start.getMinutes()+' : '+start.getSeconds());

    var totalPower=0.00;
    var currentPower;
    var currentAddonPower;
    var productQuantity;
    var productAddonQuantity;
    var sitePowerMap={};        //input-site GUID, output - total power (provided-required) attribute
    
    
    var sitePowerRelevant={};   //input-site GUID, output - total power relevant?
    
    var currentSiteGUID;
    var currentPoERequired;

    //first fill in the site map values with 0
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                sitePowerMap[configuration.guid]=0;
            }
        }
    }
    
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.LAN || component.name==CLOUD_VOICE.CPE) {
            for(let configuration of Object.values(component.schema.configurations)) {

                if(component.name== CLOUD_VOICE.LAN) {
                    currentSiteGUID=configuration.getAttribute('Site GUID').value;
                    if (configuration.getAttribute('Supports PoE').displayValue=='Yes' && configuration.getAttribute('LAN Type').displayValue=='Existing / 3rd Party') {
                        sitePowerRelevant[currentSiteGUID]=false;
                    }
                    else {
                        sitePowerRelevant[currentSiteGUID]=true;
                    }
                }
                totalPower=0.00;
                productQuantity=0;
                currentPower=0;
                currentSite='';
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Power') {
                        currentPower=Number(attribute.value);
                        if(isNaN(currentPower)) currentPower=0;
                    }
                    if (attribute.name=='Quantity') {
                        productQuantity=Number(attribute.value);
                        if(isNaN(Number(productQuantity))) productQuantity=0;
                    }
                    if (attribute.name=='Site GUID') {
                        currentSite=attribute.value;
                    }
                }); 
                if(component.name== CLOUD_VOICE.LAN) productQuantity=1;
                totalPower+=productQuantity*currentPower;
                console.log(configuration.name,' currentPower ',currentPower);
                console.log('totalPower',totalPower);
                //before querying addons, check if maybe we already have all the info
                var needToQuery=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    var addonID;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            addonID=attribute.value;
                            break;
                        }
                    }
                    if(globalAddonPowerMap[addonID]==null) {
                        needToQuery=true;
                        break;
                    }
                }
                if(needToQuery) {
                    //retrieve config addons and fill id->power/port/outlet map
                    try {
                        availableAddons = await component.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(availableAddons) ) {
                            for(let oneAddon of Object.values(segment.allAddons)) {
                                globalAddonPowerMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Power__c;
                                globalAddonPortMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Port_Required__c;
                                globalAddonPortProvidedMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Available_Ports__c;
                                globalAddonOutletMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Outlet_Required__c;
                                globalAddonUFPRequiredMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.UFP_Required__c;
                            }
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }
                //calculate quantity * power * productqtty if cpe)
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    currentAddonPower=0.00;
                    productAddonQuantity=0;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            currentAddonPower=Number(globalAddonPowerMap[attribute.value]);
                            if(isNaN(currentAddonPower)) currentAddonPower=0;
                        }
                        if (attribute.name=='Quantity') {
                            productAddonQuantity=Number(attribute.value);
                            if (isNaN(productAddonQuantity)) productAddonQuantity=0;
                        }
                    }
                    if(component.name==CLOUD_VOICE.CPE) productAddonQuantity=1;
                    totalPower+=(productAddonQuantity*currentAddonPower*productQuantity);
                    
                    console.log(relatedProduct.name,' AddonPower of ',currentAddonPower);
                    console.log('totalPower in addons',totalPower);
                }
                sitePowerMap[currentSite]+=totalPower;
            }
        }
    }
    
    console.log('sitePowerMap',sitePowerMap);
    
    //update site access to show new numbers
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                //check if site is relevant for power
                if(sitePowerRelevant[configuration.guid]==false) {
                    updateConfigurationAttributeValue('Total Power Required',parseFloat(0.00).toFixed(2),parseFloat(0.00).toFixed(2),false,solution,configuration.guid,false);
                    if (configuration.statusMessage == 'There are CPE items without sufficient power'){
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }
                else {
                    updateConfigurationAttributeValue('Total Power Required',parseFloat(sitePowerMap[configuration.guid]).toFixed(2),parseFloat(sitePowerMap[configuration.guid]).toFixed(2),false,solution,configuration.guid,false);
                    if (sitePowerMap[configuration.guid]<0) {
                        configuration.status = false;
                        configuration.statusMessage = 'There are CPE items without sufficient power';
                    }
                    else { 
                        if (configuration.statusMessage == 'There are CPE items without sufficient power'){
                            configuration.status = true;
                            configuration.errorMessage = null;
                        }
                    }
                }
            }
        }
    }
    var finish = new Date();
    console.log('power finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//462 calculate total ports required
async function calculatePort(solution) {
    var start = new Date();
    console.log('port started '+start.getMinutes()+' : '+start.getSeconds());
    var totalPortsRequired=0;
    var currentPortRequired;
    var productQuantity;
    var productAddonQuantity;
    var sitePortMap={};         //input-site GUID, output - total port required attribute
    var LANPortMap={};          //input-LAN GUID, output -site GUID
    var currentSiteGUID;

    //first fill in the site map values with 0
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                sitePortMap[configuration.guid]=0;
            }
        }
        if (component.name== CLOUD_VOICE.cabling || component.name== CLOUD_VOICE.LAN) {
            for(let configuration of Object.values(component.schema.configurations)) {
                currentSiteGUID='';
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Site GUID') {
                        currentSiteGUID=attribute.value;
                        if(component.name== CLOUD_VOICE.LAN) LANPortMap[configuration.guid]=attribute.value;
                    }
                }); 
            }
        }
    }
    
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.LAN || component.name==CLOUD_VOICE.CPE) {
            for(let configuration of Object.values(component.schema.configurations)) {
                var addonPortMap={};
                totalPortsRequired=0;
                productQuantity=0;
                currentSite='';
                currentPortRequired=null;
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Quantity') {
                        productQuantity=Number(attribute.value);
                        if(isNaN(Number(productQuantity))) productQuantity=0;
                    }
                    if (attribute.name=='Port Required') {
                        currentPortRequired=attribute.value=='true' ? true : false;
                    }
                    if (attribute.name=='Site GUID') {
                        currentSite=attribute.value;
                    }
                }); 
                if(component.name== CLOUD_VOICE.LAN) productQuantity=1;
                if(currentPortRequired) totalPortsRequired+=productQuantity;

                //before querying addons, check if maybe we already have all the info
                var needToQuery=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    var addonID;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            addonID=attribute.value;
                            break;
                        }
                    }
                    if(globalAddonPortMap[addonID]==null) {
                        needToQuery=true;
                        break;
                    }
                }
                if(needToQuery) {
                    //retrieve config addons and fill id->port map
                    try {
                        currentAddons = await component.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(currentAddons) ) {
                            segment.allAddons.forEach(oneAddon => {
                                globalAddonPowerMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Power__c;
                                globalAddonPortMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Port_Required__c;
                                globalAddonPortProvidedMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Available_Ports__c
                                globalAddonOutletMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Outlet_Required__c;                           
                                globalAddonUFPRequiredMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.UFP_Required__c;   
                            });
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }
                
                //calculate quantity * port
                 for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    productAddonQuantity=0;
                    currentPortRequired=null;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            currentPortRequired=Boolean(globalAddonPortMap[attribute.value]);
                        }
                        if (attribute.name=='Quantity') {
                            productAddonQuantity=Number(attribute.value);
                            if (isNaN(productAddonQuantity)) productAddonQuantity=0;
                        }
                    }
                    if(component.name==CLOUD_VOICE.CPE) productAddonQuantity=1;

                    if(currentPortRequired) totalPortsRequired+=(productAddonQuantity*productQuantity);
                }
                sitePortMap[currentSite]+=totalPortsRequired;
            }
        }
    }

    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.LAN) {
            for(let configuration of Object.values(component.schema.configurations)) {
                updateConfigurationAttributeValue('AccountID', globalAccountID, globalAccountID, true, solution, configuration.guid, true);
                updateConfigurationAttributeValue('Total Ports Required',sitePortMap[LANPortMap[configuration.guid]],sitePortMap[LANPortMap[configuration.guid]],true,solution,configuration.guid,false);
            }
        }
    }
    var finish = new Date();
    console.log('port finished '+finish.getMinutes()+' : '+finish.getSeconds());
}


//462 calculate total outlets required
async function calculateOutlet(solution) {
    var start = new Date();
    console.log('outlet started '+start.getMinutes()+' : '+start.getSeconds());
    var totalOutletsRequired=0;
    var currentOutletRequired;
    var productQuantity;
    var productAddonQuantity;
    var siteOutletMap={};       //input-site GUID, output - total outlet required attribute
    var cablingOutletMap={};    //input-cabling GUID, output -site GUID
    var currentSiteGUID;

    //first fill in the site map values with 0
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                siteOutletMap[configuration.guid]=0;
            }
        }
        if (component.name== CLOUD_VOICE.cabling || component.name== CLOUD_VOICE.LAN) {
            for(let configuration of Object.values(component.schema.configurations)) {
                currentSiteGUID='';
                currentPoERequired='';
                currentLANType='';
                
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Site GUID') {
                        currentSiteGUID=attribute.value;
                        if(component.name== CLOUD_VOICE.cabling) cablingOutletMap[configuration.guid]=attribute.value;
                    }
                }); 
            }
        }
    }
    
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.LAN || component.name==CLOUD_VOICE.CPE) {
            for(let configuration of Object.values(component.schema.configurations)) {
                var addonOutletMap={};
                totalOutletsRequired=0;
                productQuantity=0;
                currentSite='';
                currentOutletRequired=null;
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Quantity') {
                        productQuantity=Number(attribute.value);
                        if(isNaN(Number(productQuantity))) productQuantity=0;
                    }
                    if (attribute.name=='Outlet Required') {
                        currentOutletRequired=attribute.value;
                    }
                    if (attribute.name=='Site GUID') {
                        currentSite=attribute.value;
                    }
                }); 
                
                if(currentOutletRequired==true) totalOutletsRequired+=productQuantity;
                if(component.name== CLOUD_VOICE.LAN) productQuantity=1;
                
                //before querying addons, check if maybe we already have all the info
                var needToQuery=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    var addonID;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            addonID=attribute.value;
                            break;
                        }
                    }
                    if(globalAddonOutletMap[addonID]==null) {
                        needToQuery=true;
                        break;
                    }
                }
                if(needToQuery) {
                    //retrieve config addons and fill id->outlet map
                    try {
                        currentAddons = await component.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(currentAddons) ) {
                            segment.allAddons.forEach(oneAddon => {
                                globalAddonPowerMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Power__c;
                                globalAddonPortMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Port_Required__c;
                                globalAddonPortProvidedMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Available_Ports__c
                                globalAddonOutletMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Outlet_Required__c;                           
                                globalAddonUFPRequiredMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.UFP_Required__c;   
                            });
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }
                
                //calculate quantity * power, quantity * port, quantity * outlet
                 for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    productAddonQuantity=0;
                    currentOutletRequired=null;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            currentOutletRequired=Boolean(addonOutletMap[attribute.value]);
                        }
                        if (attribute.name=='Quantity') {
                            productAddonQuantity=Number(attribute.value);
                            if (isNaN(productAddonQuantity)) productAddonQuantity=0;
                        }
                    }
                    if(component.name==CLOUD_VOICE.CPE) productAddonQuantity=1;

                    if(currentOutletRequired==true) totalOutletsRequired+=(productAddonQuantity*productQuantity);
                }
                siteOutletMap[currentSite]+=totalOutletsRequired;
            }
        }
    }
    
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.cabling) {
            for(let configuration of Object.values(component.schema.configurations)) {
                updateConfigurationAttributeValue('Total Outlets Required',siteOutletMap[cablingOutletMap[configuration.guid]],siteOutletMap[cablingOutletMap[configuration.guid]],true,solution,configuration.guid,false);
            }
        }
    }
    var finish = new Date();
    console.log('outlet finished '+finish.getMinutes()+' : '+finish.getSeconds());
}


//462 sum of port,outlet,quantity required (CPE,CPE addon,LAN,LAN addon)
async function calculatePowerPortOutlet(solution) {
    var start = new Date();
    console.log('calculatePowerPortOutlet started '+start.getMinutes()+' : '+start.getSeconds());
    var availablePorts=0;
    var totalPower=0.00;
    var totalPortsRequired=0;
    var totalOutletsRequired=0;
    
    var currentPortRequired;
    var currentOutletRequired;
    var currentPower;
    var currentAddonPower;
    var productQuantity;
    var productAddonQuantity;
    
    
    var sitePowerMap={};        //input-site GUID, output - total power (provided-required) attribute
    var sitePortMap={};         //input-site GUID, output - total port required attribute
    var sitePortAvailableMap={};//input-site GUID, output - total port (provided-required) attribute
    var siteOutletMap={};       //input-site GUID, output - total outlet required attribute
    
    var cablingOutletMap={};    //input-cabling GUID, output -site GUID
    var LANPortMap={};          //input-LAN GUID, output -site GUID
    var sitePowerRelevant={};   //input-site GUID, output - total power relevant?
    
    var currentSiteGUID;
    var currentPoERequired;
    var currentLANType;
    
    //first fill in the site map values with 0
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                sitePowerMap[configuration.guid]=0;
                sitePortMap[configuration.guid]=0;
                siteOutletMap[configuration.guid]=0;
                sitePortAvailableMap[configuration.guid]=0;
            }
        }
        if (component.name== CLOUD_VOICE.cabling || component.name== CLOUD_VOICE.LAN) {
            for(let configuration of Object.values(component.schema.configurations)) {
                currentSiteGUID='';
                currentPoERequired='';
                currentLANType='';
                
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Site GUID') {
                        currentSiteGUID=attribute.value;
                        if(component.name== CLOUD_VOICE.cabling) cablingOutletMap[configuration.guid]=attribute.value;
                        if(component.name== CLOUD_VOICE.LAN) LANPortMap[configuration.guid]=attribute.value;
                    }
                    if (attribute.name=='Supports PoE') {
                        currentPoERequired=attribute.value;
                    }
                    if (attribute.name=='LAN Type') {
                        currentLANType=attribute.displayValue;
                    }
                }); 
                
                if(component.name== CLOUD_VOICE.LAN) {
                    if (currentPoERequired=='Yes' && currentLANType=='Existing / 3rd Party') {
                        sitePowerRelevant[currentSiteGUID]=false;
                    }
                    else {
                        sitePowerRelevant[currentSiteGUID]=true;
                    }
                }
            }
        }
    }
    
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.LAN || component.name==CLOUD_VOICE.CPE) {
            console.log('component.name=>',component.name);
            for(let configuration of Object.values(component.schema.configurations)) {
                var addonPowerMap={};
                var addonPortMap={};
                var addonPortProvidedMap={};
                var addonOutletMap={};
                totalPortsRequired=0;
                totalOutletsRequired=0;
                totalPower=0.00;
                availablePorts=0;
                productQuantity=0;
                currentPower=0;
                currentSite='';
                currentPortRequired=null;
                currentOutletRequired=null;
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Power') {
                        currentPower=Number(attribute.value);
                        if(isNaN(currentPower)) currentPower=0;
                    }
                    if (attribute.name=='Quantity') {
                        productQuantity=Number(attribute.value);
                        if(isNaN(Number(productQuantity))) productQuantity=0;
                    }
                    if (attribute.name=='Port Required') {
                        currentPortRequired=attribute.value;
                    }
                    if (attribute.name=='Outlet Required') {
                        currentOutletRequired=attribute.value;
                    }
                    if (attribute.name=='Site GUID') {
                        currentSite=attribute.value;
                    }
                }); 
                if(component.name== CLOUD_VOICE.LAN) productQuantity=1;
                if(currentPortRequired==true) {
                    totalPortsRequired+=productQuantity;
                    availablePorts-=productQuantity;
                }
                if(currentOutletRequired==true) totalOutletsRequired+=productQuantity;
                totalPower+=productQuantity*currentPower;

                //retrieve config addons and fill id->power/port/outlet map
                try {
                    currentAddons = await component.getAddonsForConfiguration(configuration.guid);
                    for(let segment of Object.values(currentAddons) ) {
                        segment.allAddons.forEach(oneAddon => {
                            addonPowerMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Power__c;
                            addonPortMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Port_Required__c;
                            addonPortProvidedMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Available_Ports__c;
                            addonOutletMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Outlet_Required__c;
                        });
                    }
                } catch (error) {console.log('ERROR IS ',error);}
                //calculate quantity * power, quantity * port, quantity * outlet
                 for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    currentAddonPower=0.00;
                    productAddonQuantity=0;
                    currentPortRequired=null;
                    currentPortProvided=null;
                    currentOutletRequired=null;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            currentAddonPower=Number(addonPowerMap[attribute.value]);
                            if(isNaN(currentAddonPower)) currentAddonPower=0;
                            currentPortRequired=Boolean(addonPortMap[attribute.value]);
                            currentPortProvided=Number(addonPortProvidedMap[attribute.value]);
                            if(isNaN(currentPortProvided)) currentPortProvided=0;
                            currentOutletRequired=Boolean(addonOutletMap[attribute.value]);
                        } 
                        if (attribute.name=='Quantity') {
                            productAddonQuantity=Number(attribute.value);
                            if (isNaN(productAddonQuantity)) productAddonQuantity=0;
                        }
                    }
                    if(component.name==CLOUD_VOICE.CPE) productAddonQuantity=1;

                    if(currentPortRequired==true) totalPortsRequired+=(productAddonQuantity*productQuantity);
                    if(currentOutletRequired==true) totalOutletsRequired+=(productAddonQuantity*productQuantity);
                    totalPower+=(productAddonQuantity*currentAddonPower*productQuantity);
                    availablePorts+=(productAddonQuantity*currentPortProvided*productQuantity);
                }
                sitePowerMap[currentSite]+=totalPower;
                sitePortMap[currentSite]+=totalPortsRequired;
                siteOutletMap[currentSite]+=totalOutletsRequired;
                sitePortAvailableMap[currentSite]+=availablePorts;
            }
        }
    }

    //update site access to show new numbers, no errors needed
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                //check if site is relevant for power 
                if(sitePowerRelevant[configuration.guid]==false) {
                    updateConfigurationAttributeValue('Total Power Required',parseFloat(0.00).toFixed(2),parseFloat(0.00).toFixed(2),false,solution,configuration.guid,false);
                    if (configuration.statusMessage == 'There are CPE items without sufficient power'){
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }
                else {
                    updateConfigurationAttributeValue('Total Power Required',parseFloat(sitePowerMap[configuration.guid]).toFixed(2),parseFloat(sitePowerMap[configuration.guid]).toFixed(2),false,solution,configuration.guid,false);
                    if (sitePowerMap[configuration.guid]<0) {
                        configuration.status = false;
                        configuration.statusMessage = 'There are CPE items without sufficient power';
                    }
                    else { 
                        if (configuration.statusMessage == 'There are CPE items without sufficient power'){
                            configuration.status = true;
                            configuration.errorMessage = null;
                        }
                    }
                }
            }
        }
    }

    
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.cabling || component.name== CLOUD_VOICE.LAN) {
            for(let configuration of Object.values(component.schema.configurations)) {
                if(component.name== CLOUD_VOICE.cabling) {
                    updateConfigurationAttributeValue('Total Outlets Required',siteOutletMap[cablingOutletMap[configuration.guid]],siteOutletMap[cablingOutletMap[configuration.guid]],true,solution,configuration.guid,false);
                }
                else {
                    updateConfigurationAttributeValue('AccountID', globalAccountID, globalAccountID, true, solution, configuration.guid, true);
                    updateConfigurationAttributeValue('Total Ports Required',sitePortMap[LANPortMap[configuration.guid]],sitePortMap[LANPortMap[configuration.guid]],true,solution,configuration.guid,false);
                }
            }
        }
    }
    var finish = new Date();
    console.log('calculatePowerPortOutlet finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//LPDE-299 either all gigabit or none gigabit
async function validateLANAddonsGigabit(component,configuration,addonConfigParam) {
    var gigaRequired=false;
    var errorExists=false;
    var needToQuery=false;

    for(let attribute of Object.values(addonConfigParam.configuration.attributes)) {
        if (attribute.name=='Add On ID') {
            if(addonGigaMap[attribute.value]==null) needToQuery=true;
        }
    }
    if(needToQuery) {
        allConfigAddons= await component.getAddonsForConfiguration(configuration.guid);
        for(let segment of Object.values(allConfigAddons) ) {
            for(let oneAddon of Object.values(segment.allAddons)) {
                if(oneAddon.cspmb__Group__c=='BT LAN'){
                addonGigaMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Gigabit_Switch__c;
                }
            }
        }
    }
    //check addon parameter for giga/nongiga
    for(let attribute of Object.values(addonConfigParam.configuration.attributes)) {
        if (attribute.name=='Add On ID') {
            if(addonGigaMap[attribute.value]==true) gigaRequired=true;
        }
    }
    //check existing addons for giga/nongiga
    for (let relatedProduct of Object.values(configuration.relatedProducts)){
        for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
            if (attribute.name=='Add On ID') {
                if(addonGigaMap[attribute.value]!=gigaRequired && addonGigaMap[attribute.value]!= null) {
                    errorExists=true;
                }
            }
        }
    }
    return errorExists;
}

//updates sie NAd Key attribute to match site access
function saveSiteNADKeys(solution) {
    for(let component of Object.values(solution.components)) {
        if (component.name != CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                
                let siteAccessConfig = solution.getConfiguration(configuration.getAttribute('Site GUID').value);
                
                updateConfigurationAttributeValue(  'Site NAD Key',
                                                    siteAccessConfig.getAttribute('Site NAD Key').value,
                                                    siteAccessConfig.getAttribute('Site NAD Key').value,
                                                    true,solution,configuration.guid,true );
             
             updateConfigurationAttributeValue( 'Site Name',
                                                siteAccessConfig.getAttribute('Site Name').value,
                                                siteAccessConfig.getAttribute('Site Name').value,
                                                true,solution,configuration.guid,true );
            }
        }
    }
}

//500 Auto-Add/Remove Numbers based on numbers required
async function calculateNumbersRequired(solution) {
    var start = new Date();
    console.log('calculateNumbersRequired started '+start.getMinutes()+' : '+start.getSeconds());
    var totalNumbersRequired=0;
    var currentNumberRequired;
    var currentQuantity;

    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.UFP || component.name==CLOUD_VOICE.CPE) {
            for(let configuration of Object.values(component.schema.configurations)) {
                addonNumberMap={};

                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Quantity') {
                        currentQuantity=Number(attribute.value);
                        if(isNaN(currentQuantity)) currentQuantity=0;
                    }
                    if (attribute.name=='Number Required') {
                        currentNumberRequired=attribute.value=='true'? true : false;
                    }
                });
                if(currentNumberRequired) totalNumbersRequired+=currentQuantity;

                try {
                    currentAddons = await component.getAddonsForConfiguration(configuration.guid);
                    for(let segment of Object.values(currentAddons) ) {
                        segment.allAddons.forEach(oneAddon => {
                            addonNumberMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Number_Required__c;
                        });
                    }
                }
                catch (error) {console.log('ERROR IS ',error);}
                Object.values(configuration.relatedProductList).forEach(relatedProduct => {
                    currentNumberRequired=null;
                    Object.values(relatedProduct.configuration.attributes).forEach(attribute => {
                        if (attribute.name=='Add On ID') {
                            currentNumberRequired=Boolean(addonNumberMap[attribute.value]);
                        }
                    });
                    if(currentNumberRequired==true) totalNumbersRequired+=currentQuantity;
                });
            }
       }
       if (component.name== CLOUD_VOICE.SIPTswitch) {
            for(let configuration of Object.values(component.schema.configurations)) {
                currentNumberRequired=0;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if(relatedProduct.name.includes('SIP Gateway (')) {
                        for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
                            if(!isNaN(Number(attribute.value))) currentNumberRequired+=Number(attribute.value);
                        }
                    }
                }
                if(currentNumberRequired==0) currentNumberRequired=1;
                totalNumbersRequired+=currentNumberRequired;
            }
       }
       
       if (component.name== CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                currentQuantity=Number(configuration.getAttribute('SIP Users Required').value);
                if(!isNaN(currentQuantity)) totalNumbersRequired+=currentQuantity
            }
       }
    }
    for(var configuration of Object.values(solution.schema.configurations)) {
        var additionalNumbersRequired=0;
        updateConfigurationAttributeValue('Minimum Numbers Required',totalNumbersRequired,totalNumbersRequired,true,solution,configuration.guid,true);
        for(let attribute of Object.values(configuration.attributes) ) {
            if (attribute.name=='Additional Numbers Required') {
                if(!isNaN(Number(attribute.value))) additionalNumbersRequired=Number(attribute.value);
            }
        }
        
        var NCounter=0;
        let latestGuid='';
        let latestConnection='';
        for(let relatedProduct of Object.values(configuration.relatedProductList) ) {
            if(relatedProduct.name=='Site Number') {
                latestGuid=relatedProduct.guid;
                NCounter+=1;
            }
            if(relatedProduct.name=='Number Connection') {
                latestConnection=relatedProduct.guid;
            }
            
        }
        if(NCounter==1) {
            for(let relatedProduct of Object.values(configuration.relatedProductList) ) {
                if (relatedProduct.guid==latestGuid || relatedProduct.guid==latestConnection) {
                    for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Quantity') {
                            attribute.value=totalNumbersRequired+additionalNumbersRequired;
                        }
                    }
                }
            }
        }
        else {
            console.log(Object.values(configuration.relatedProductList));
            for(let relatedProduct of Object.values(configuration.relatedProductList) ) {
                if(relatedProduct.name=='Site Number' || relatedProduct.name=='Number Connection') {
                    await deleteAddOns(solution,configuration,relatedProduct.guid,true);
                }
            }
            if(totalNumbersRequired+additionalNumbersRequired>0) {
                try {
                    var possibleAddons = await solution.getAddonsForConfiguration(configuration.guid);
                    for(let segment of Object.values(possibleAddons) ) {
                        for(var oneAddon of Object.values(segment.allAddons) ) {
                            if(oneAddon.cspmb__Add_On_Price_Item__r.Name=='Site Number' || oneAddon.cspmb__Add_On_Price_Item__r.Name=='Number Connection') {
                                await addAddOnProductConfiguration('Numbers Add-On', oneAddon, configuration.guid,totalNumbersRequired+additionalNumbersRequired, solution);
                            }
                        }
                    }
                } catch (error) {console.log('ERROR IS ',error);}
            }
        }
    }
    var finish = new Date();
    console.log('calculateNumbersRequired finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//auto adds and removes SIP Channels Addon
async function calculateSIPChannelsRequired(solution) {
    var start = new Date();
    console.log('calculateSIPChannelsRequired started '+start.getMinutes()+' : '+start.getSeconds());
    var currentQuantity, channelAddon, addonName;

    for(var configuration of Object.values(solution.schema.configurations)) {
        var contractTerm = configuration.getAttribute('Contract Term').value;
        addonName='BTCV SIP Trunk Channel '+contractTerm+' Months';
        var channelsRequired = Number(configuration.getAttribute('SIP Channels Required').value);
        if(isNaN(channelsRequired)) channelsRequired=0;
        
        var CCounter=0;
        let latestGuid='';
        for(let relatedProduct of Object.values(configuration.relatedProductList) ) {
            if(relatedProduct.name==addonName) {
                latestGuid=relatedProduct.guid;
                CCounter+=1;
            }
        }
        if(CCounter==1 && channelsRequired!=0) {
            for(let relatedProduct of Object.values(configuration.relatedProductList) ) {
                if(relatedProduct.guid==latestGuid) {
                    for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Quantity') {
                            attribute.value=channelsRequired;
                        }
                    }
                }
            }
        }
        else {
            for(let relatedProduct of Object.values(configuration.relatedProductList) ) {
                if(relatedProduct.name.includes('BTCV SIP Trunk Channel ')) {
                    await deleteAddOns(solution,configuration,relatedProduct.guid,true);
                }
            }
            
            if(channelsRequired!=0) {
                try {
                    if(globalAddonMap[addonName]==null) {
                        var possibleAddons=await solution.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(possibleAddons) ) {
                            for(let oneAddon of Object.values(segment.allAddons)) {
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name==addonName) {
                                    globalAddonMap[addonName]=oneAddon;
                                    break;
                                }
                            }
                        }
                    }
                    channelAddon=globalAddonMap[addonName];
                    await addAddOnProductConfiguration('SIP Channels', channelAddon, configuration.guid, channelsRequired, solution);
                } catch (error) {console.log('ERROR IS ',error);}
            }
        }
    }
    var finish = new Date();
    console.log('calculateSIPChannelsRequired finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//LPDE-231/230 softphone UC business colaborate ufp licences #QBSRULE
function calculateSoftphoneLicence(solution) {
    var start = new Date();
    console.log('calculateSoftphoneLicence started '+start.getMinutes()+' : '+start.getSeconds());
    var totalQuantityRequired;
    var totalQuantityProvided;
    var currentQuantityProvided;
    var currentSite;
    var currentType;
    var currentUFPRequired;

    var siteQuantityMap={};
    var siteQuantityUsageMap={};
    
    //get all UFP component and put them in site-SoftPhoneQuantity map
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.UFP) {

            for(let configuration of Object.values(component.schema.configurations)) {
                currentQuantityProvided=0
                totalQuantityProvided=0;             
                currentSite=configuration.getAttribute('Site Access').value;
                currentType=configuration.getAttribute('Type').displayValue;
                if(currentType.includes('Collaborate')) {
                    currentQuantityProvided=Number(configuration.getAttribute('Quantity').value);
                    if(!isNaN(currentQuantityProvided)) totalQuantityProvided+=currentQuantityProvided;
                }

                Object.values(configuration.relatedProductList).forEach(relatedProduct => {
                    currentQuantityProvided=0;
                    if (relatedProduct.name.includes('UC Business')) {
                        totalQuantityProvided+=1;
                    }
                });
                if(siteQuantityMap[currentSite]==null) {
                    siteQuantityMap[currentSite]=totalQuantityProvided;
                }
                else {
                    siteQuantityMap[currentSite]+=totalQuantityProvided;
                }
            }
        }
    }
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.CPE) {
            for(let configuration of Object.values(component.schema.configurations)) {
                totalQuantityRequired=0;
                currentUFPRequired=false;
                currentSite='';
                currentPhone='';
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Phone') {
                        currentPhone=attribute.displayValue;
                    }
                    if (attribute.name=='Quantity') {
                        totalQuantityRequired=Number(attribute.value);
                    }
                    if (attribute.name=='Site GUID') {
                        currentSite=attribute.value;
                    }
                });
                if (currentPhone.includes('Softphone')) {
                    if(siteQuantityUsageMap[currentSite]==null) {
                        siteQuantityUsageMap[currentSite]=totalQuantityRequired;
                    }
                    else {
                        siteQuantityUsageMap[currentSite]+=totalQuantityRequired;
                    }
                }
            }
        }
    }
   
    //at this moment we have total UFP required info and total ufp sum maps
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                if(siteQuantityUsageMap[configuration.guid]!=null && siteQuantityMap[configuration.guid]<siteQuantityUsageMap[configuration.guid] ){
                    configuration.status = false;
                    configuration.statusMessage = 'There are insufficient Collaborate or UC Business items for the amount of Soft Phones';
                }
                else {
                    if (configuration.statusMessage == 'There are insufficient Collaborate or UC Business items for the amount of Soft Phones'){
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }
            }
        }
    }
    var finish = new Date();
    console.log('calculateSoftphoneLicence finished '+finish.getMinutes()+' : '+finish.getSeconds());
}


//527 sum of allowed quantity for UFP licences and addon quantity for CPE #QBSRULE
function licenceSumUFP(solution) {
    var totalQuantity;
    var currentQuantity;
    var maxAddonQuantity;
    var productQuantity;

    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.UFP || component.name==CLOUD_VOICE.CPE) {
            Object.values(component.schema.configurations).forEach(configuration => { 
                currentQuantity=0;
                totalQuantity=0;
                productQuantity=0;
                maxAddonQuantity=0;
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Quantity') {
                        currentQuantity=Number(attribute.value);
                        productQuantity=Number(attribute.value);
                        if(!isNaN(currentQuantity)) totalQuantity+=currentQuantity;
                    }
                }); 
                Object.values(configuration.relatedProductList).forEach(relatedProduct => {
                    currentQuantity=0;
                    for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if ((relatedProduct.name.includes('CRM Integrator') || relatedProduct.name.includes('CRM Lite'))
                         && component.name==CLOUD_VOICE.UFP) {
                            if(attribute.name=='Quantity') {
                                if(!isNaN(Number(attribute.value))) totalQuantity-=Number(attribute.value);
                            }
                        }
                        if(attribute.name=='Quantity' && component.name==CLOUD_VOICE.CPE) {
                            if(!isNaN(Number(attribute.value))) {
                                if(maxAddonQuantity==null || maxAddonQuantity<Number(attribute.value))
                                    maxAddonQuantity=Number(attribute.value);
                            }
                        }
                    }
                });
                if (component.name==CLOUD_VOICE.UFP) {
                    if (totalQuantity<0) {
                        configuration.status = false;
                        configuration.statusMessage = 'Quantity must hold total "CRM Lite" and "CRM Integrator" licences';
                    }
                    else {
                        if (configuration.statusMessage == 'Quantity must hold total "CRM Lite" and "CRM Integrator" licences'){
                            configuration.status = true;
                            configuration.errorMessage = null;
                        }
                    }
                }

                if (component.name==CLOUD_VOICE.CPE) {
                    if(maxAddonQuantity>productQuantity) {
                        configuration.status = false;
                        configuration.statusMessage = 'Addon quantity can\'t be higher than product quantity';
                    }
                    else {
                        if (configuration.statusMessage == 'Addon quantity can\'t be higher than product quantity'){
                            configuration.status = true;
                            configuration.errorMessage = null;
                        }
                    }
                }
            });
        }
    }
}

//LPDE-297
//LAn type can only be "bespoke LAN"  if there are  96 or more ports required
function checkTotalPortLimit(solution) {
    var portOverflowGuids = new Set();
    var currentOverflow;
    var currentValue;
    var totalPortsRequired;

    //for these configurations, restrict picklist values on LAN components
    for(let component of Object.values(solution.components)) {
        if(component.name==CLOUD_VOICE.LAN) {
            for(let configuration of Object.values(component.schema.configurations)) {
                totalPortsRequired=Number(configuration.getAttribute('Total Ports Required').value)+Number(configuration.getAttribute('Additional Ports').value);
                type=configuration.getAttribute('LAN Type').displayValue;

                if (totalPortsRequired>96 && type!='ITS Bespoke LAN') {
                    configuration.status = false;
                    configuration.statusMessage = 'Ports required is greater than 96 and therefore will require a Bespoke Solution';
                }
                else {
                    if (configuration.statusMessage == 'Ports required is greater than 96 and therefore will require a Bespoke Solution'){
                        configuration.status = true;
                        configuration.errorMessage = null; 
                    }
                }
            }
        }
    }
}

//adds or removes total care addons depending on attribute "total care" on the main component
async function addTotalCareAddOns(solution, configurationParam, mainConfiguration) {
    var start = new Date();
    console.log('addTotalCareAddOns started '+start.getMinutes()+' : '+start.getSeconds());
    var allConfigAddons;
    
    //UFP total care part
    for(let component of Object.values(solution.components)) {
        if(component.name==CLOUD_VOICE.UFP) {
            for(let configuration of Object.values(component.schema.configurations)) {
                if(configurationParam==null || configurationParam.guid==configuration.guid) {
                    if (mainConfiguration.getAttribute('Care Level').value == 'Total') {
                        var careExists=false;
                        for (let relatedProduct of Object.values(configuration.relatedProductList)) {
                            if (relatedProduct.name.includes('BTCV Total Care')) {
                                careExists=true;
                            }
                        }
                        if(!careExists) {
                            try{
                                var dAddon;
                                allConfigAddons= await component.getAddonsForConfiguration(configuration.guid);
                                for(let segment of Object.values(allConfigAddons) ) {
                                    for(oneAddon of Object.values(segment.allAddons)) {
                                        if(oneAddon.cspmb__Add_On_Price_Item__r.Name=='Unlimited' && oneAddon.cspmb__Group__c=='UFP Add Ons') globalUnlimitedAddonMap['UFP']=oneAddon;
                                        if(oneAddon.cspmb__Add_On_Price_Item__r.Name.includes('BTCV Total Care')) {
                                            dAddon=oneAddon;
                                        }
                                    }
                                }
                                addAddOnProductConfiguration('UFP Add Ons', dAddon, configuration.guid, 1, component);
                            } catch (error) {console.log('ERROR IS ',error);}
                        } else {
                            for (let relatedProduct of Object.values(configuration.relatedProductList)) {
                                if (relatedProduct.name.includes('BTCV Total Care')) {
                                    console.log(relatedProduct);
                                    var UFPQuantity = Number(configuration.getAttribute('Quantity').value);
                                    relatedProduct.configuration.attributes.quantity.value = UFPQuantity;
                                    relatedProduct.configuration.attributes.quantity.displayValue = UFPQuantity;
                                }
                            }
                        }
                    }
                    else {
                        for (let relatedProduct of Object.values(configuration.relatedProductList)) {
                            if (relatedProduct.name.includes('BTCV Total Care')) {
                                await deleteAddOns(component, configuration, relatedProduct.guid, true);
                            }
                        }
                    }
                }
            }
        }
    }
    //SIP total care part
    for(var configuration of Object.values(solution.schema.configurations)) {
        var SIPchannelQuantity = Number(configuration.getAttribute('SIP Channels Required').value);
        if(isNaN(SIPchannelQuantity)) SIPchannelQuantity=0;
        if(configurationParam==null || configurationParam.guid==configuration.guid) {
            if (mainConfiguration.getAttribute('Care Level').value == 'Total') {
                var TCcount=false;
                for (let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if (relatedProduct.name.includes('BTCV Total Care')) {
                        TCcount=true;
                    }
                }
                if(TCcount==1 && SIPchannelQuantity!=0) {
                }
                else {
                    for (let relatedProduct of Object.values(configuration.relatedProductList)) {
                        if (relatedProduct.name.includes('BTCV Total Care')) {
                            await deleteAddOns(solution, configuration, relatedProduct.guid, true);
                        }
                    }
                    try{
                        var dAddon;
                        allConfigAddons= await solution.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(allConfigAddons) ) {
                            for(oneAddon of Object.values(segment.allAddons)) {
                                if(oneAddon.cspmb__Add_On_Price_Item__r.Name.includes('BTCV Total Care')) {
                                    dAddon=oneAddon;
                                }
                            }
                        }
                        if (SIPchannelQuantity >= 1) {
                            await addAddOnProductConfiguration('SIP Channels', dAddon, configuration.guid, SIPchannelQuantity, solution);
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }
            }
            else {
                for (let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if (relatedProduct.name.includes('BTCV Total Care')) {
                        await deleteAddOns(solution, configuration, relatedProduct.guid, true);
                    }
                }
            }
        }
    }
    var finish = new Date();
    console.log('addTotalCareAddOns finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//LPDE-784
//check if there are enough switch trunk order enrichments for each gateway addon
function checkGatewaySum(component) {
    var currentGatewayQuantity,totalGatewayQuantity,trunkQuantity;
    for(let configuration of Object.values(component.schema.configurations)) {
        currentGatewayQuantity=0;
        totalGatewayQuantity=0;
        trunkQuantity=0;
        for(let orderEnrichment of Object.values(configuration.orderEnrichmentList)) {
            if(orderEnrichment.name.includes('Switch Trunk')) {
                trunkQuantity+=1;          
            }            
        }
        for (let relatedProduct of Object.values(configuration.relatedProductList)) {
            if(relatedProduct.name.includes('SIP Gateway (')) {
                for(attribute of Object.values(relatedProduct.configuration.attributes)) {
                    if(attribute.name=='Quantity') {
                        if(!isNaN(Number(attribute.value))) currentGatewayQuantity+=Number(attribute.value);
                    }
                }
            }
        }
        if(currentGatewayQuantity==0) {
            currentGatewayQuantity=1;
        }
        
        
        if (currentGatewayQuantity>trunkQuantity) {
            configuration.status = false;
            configuration.statusMessage = 'There must be a Trunk for each Gateway on the Site';
        }
        else {
            if (configuration.statusMessage == 'There must be a Trunk for each Gateway on the Site'){
                configuration.status = true;
                configuration.errorMessage = null;
            }
        }
    }
}


//LPDE-486 sum of all UFP required
//LPDE-244 SUM OF UFP required must be higher than installation CPE addon #QBSRULE
async function calculateAvailabilityUFP(solution) {
    var start = new Date();
    console.log('calculateAvailabilityUFP started '+start.getMinutes()+' : '+start.getSeconds());
    var totalQuantityRequired;
    var currentQuantityProvided;
    var currentSite;
    var currentUFPRequired;
    var currentQuantityProvided;

    var siteQuantityMap={};
    var siteQuantityUsageMap={};
    var siteQuantityInstallationMap={};
    var siteQuantityPeripheralMap={};
    
    //get all UFP component and put them in site-quantity map
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.UFP) {
            for(let configuration of Object.values(component.schema.configurations)) {
                currentSite='';
                currentQuantityProvided=0;

                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='Site GUID') {
                        currentSite=attribute.value;
                    }
                    if (attribute.name=='Quantity') {
                        if(!isNaN(Number(attribute.value))) currentQuantityProvided+=Number(attribute.value);
                    }
                });
                if(siteQuantityMap[currentSite]==null) {
                    siteQuantityMap[currentSite]=currentQuantityProvided;
                }
                else {
                    siteQuantityMap[currentSite]+=currentQuantityProvided;
                }
            }
        }
    }
    //here we have siteQuantityMap with total UFP provided for each site
    //now check CPE sum of UFP require
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.CPE) {
            for(let configuration of Object.values(component.schema.configurations)) {
                var totalInstallation=0;
                var totalPeripherals=0;
                totalQuantityRequired=0;
                currentUFPRequired=false;
                currentSite='';
                Object.values(configuration.attributes).forEach(attribute => {
                    if (attribute.name=='UFP Required') {
                        currentUFPRequired=Boolean(attribute.value);
                    }
                    if (attribute.name=='Quantity') {
                        totalQuantityRequired=Number(attribute.value);
                    }
                    if (attribute.name=='Site GUID') {
                        currentSite=attribute.value;
                    }
                });

                //figure out if query is needed or all addons are in the map already
                var needToQuery=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if(needToQuery==true) break;
                    var addonID;
                    for(attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            addonID=attribute.value;
                            break;
                        }
                    }
                    if(globalAddonUFPRequiredMap[addonID]==null) {
                        console.log('addonID missing',addonID);
                        needToQuery=true;
                    }
                }

                if(needToQuery){
                    try{
                        let currentAddons = await component.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(currentAddons) ) {
                            segment.allAddons.forEach(oneAddon => {
                                globalAddonPowerMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Power__c;
                                globalAddonPortMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Port_Required__c;
                                globalAddonPortProvidedMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Available_Ports__c
                                globalAddonOutletMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Outlet_Required__c;                           
                                globalAddonUFPRequiredMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.UFP_Required__c;
                            });
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }
                Object.values(configuration.relatedProductList).forEach(relatedProduct => {
                    currentAddonRequired=false;
                    productAddonQuantity=0;
                    Object.values(relatedProduct.configuration.attributes).forEach(attribute => {
                        if (attribute.name=='Add On ID') {
                            currentAddonRequired=Boolean(globalAddonUFPRequiredMap[attribute.value]);
                        }
                        if (attribute.name=='Quantity') {
                            if (relatedProduct.name=='Phone Installation') {
                                totalInstallation+=1;
                            }
                            if (relatedProduct.groupName=='Peripheral') {
                                totalPeripherals+=1;
                            }
                        }
                    });
                    if(currentAddonRequired==true) totalQuantityRequired+=1;
                });
                    
                
                if(siteQuantityUsageMap[currentSite]==null) {
                    siteQuantityUsageMap[currentSite]=totalQuantityRequired;
                }
                else {
                    siteQuantityUsageMap[currentSite]+=totalQuantityRequired;
                }
                
                if(siteQuantityInstallationMap[currentSite]==null) {
                    siteQuantityInstallationMap[currentSite]=totalInstallation;
                }
                else {
                    siteQuantityInstallationMap[currentSite]+=totalInstallation;
                }
                if(siteQuantityPeripheralMap[currentSite]==null) {
                    siteQuantityPeripheralMap[currentSite]=totalPeripherals;
                }
                else {
                    siteQuantityPeripheralMap[currentSite]+=totalPeripherals;
                }
            }
        }
    }
    //at this moment we have total UFP and CPE info
    for(let component of Object.values(solution.components) ) {
        if (component.name==CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                
                if(siteQuantityUsageMap[configuration.guid]!=null && siteQuantityMap[configuration.guid]<siteQuantityUsageMap[configuration.guid] ){
                    configuration.status = false;
                    configuration.statusMessage = 'There are not enough UFPs to cover the number of phones added at this site';
                }
                else {
                    if (configuration.statusMessage == 'There are not enough UFPs to cover the number of phones added at this site'){
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                    if(siteQuantityInstallationMap[configuration.guid]!=null && siteQuantityUsageMap[configuration.guid]<siteQuantityInstallationMap[configuration.guid] ){
                        configuration.status = false;
                        configuration.statusMessage = 'There can not be more CPE installations than total physical phones at the site';
                    }
                    else {
                        if (configuration.statusMessage == 'There can not be more CPE installations than total physical phones at the site'){
                            configuration.status = true;
                            configuration.errorMessage = null;
                        }
                        if(siteQuantityPeripheralMap[configuration.guid]!=null && siteQuantityUsageMap[configuration.guid]<siteQuantityPeripheralMap[configuration.guid]){
                            configuration.status = false;
                            configuration.statusMessage = 'There can not be more CPE peripherals than total physical phones at the site';
                        }
                        else {
                            if (configuration.statusMessage == 'There can not be more CPE peripherals than total physical phones at the site'){
                                configuration.status = true;
                                configuration.errorMessage = null;
                            }
                        }
                    }
                }
            }
        }
    }
    var finish = new Date();
    console.log('calculateAvailabilityUFP finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//LPDE-446 Site addons
//LPDE-850   #QBSRULE
function validateCallCentreAddon (solution) {
    var totalQuantity=0;
    var currentQuantity;
    var currentSite;
    var acdPlusExists;
    var huntPlusExists;
    for(let component of Object.values(solution.components) ) {
        if(component.name==CLOUD_VOICE.siteAccess) {
            Object.values(component.schema.configurations).forEach(configuration => {
                totalQuantity=0;
                acdPlusExists=false;
                huntPlusExists=false;
                Object.values(configuration.relatedProductList).forEach(relatedProduct => {
                    if (relatedProduct.name=='Call Centre ACD' || relatedProduct.name=='Call Centre ACD Plus' || relatedProduct.name=='Hunt Group Plus') {
                        currentQuantity=0;
                        Object.values(relatedProduct.configuration.attributes).forEach(attribute => {
                            if (attribute.name=='Quantity') {
                                currentQuantity=Number(attribute.value);
                                if (relatedProduct.name=='Call Centre ACD' && !isNaN(currentQuantity)) {
                                    totalQuantity+=currentQuantity;
                                }
                                if (relatedProduct.name=='Call Centre ACD Plus' && !isNaN(currentQuantity)) {
                                    if (currentQuantity>0) acdPlusExists=true;
                                    totalQuantity-=currentQuantity;
                                }
                                if (relatedProduct.name=='Hunt Group Plus' && !isNaN(currentQuantity)) {
                                    if (currentQuantity>0) huntPlusExists=true;
                                }
                            }
                        });
                    }
                });
                
                if (totalQuantity<0) {
                    configuration.status = false;
                    configuration.statusMessage = 'There can not be more "ACD Call Centre Plus" add-ons than there are "ACD Call Centre" add-ons on a Site';
                }
                else { 
                    if (configuration.statusMessage == 'There can not be more "ACD Call Centre Plus" add-ons than there are "ACD Call Centre" add-ons on a Site'){
                        configuration.status = true;
                        configuration.errorMessage = null;
                    }
                }

                if(!acdPlusExists || !huntPlusExists) {
                    for(let oneOE of Object.values(configuration.orderEnrichmentList)) {
                        if(oneOE.configurationName.includes('Call Centre') && !acdPlusExists) {
                            for(let OEatt of Object.values(oneOE.attributes)) {
                                if(OEatt.name=='Call Forwarding When Busy' || OEatt.name=='Scheduled Call Forwarding') {
                                    OEatt.value=false;
                                    OEatt.displayValue=false;
                                }
                            }
                        }
                        if(oneOE.configurationName.includes('Hunt Group') && !huntPlusExists) {
                            for(let OEatt of Object.values(oneOE.attributes)) {
                                if(OEatt.name=='Call Forwarding When Busy' || OEatt.name=='Scheduled Call Forwarding') {
                                    OEatt.value=false;
                                    OEatt.displayValue=false;
                                }
                            }
                        }
                    }
                }
            });
        }
    }
}

//LPDE-605 prevent adding of order enrichment
function checkAndPreventOE(solution,configurationParam,orderEnrichmentConfiguration){
    var productIDNameMap = {};

    for(let OEnrichment of Object.values(solution.orderEnrichments)) {
        productIDNameMap[OEnrichment.productOptionId]=OEnrichment.name;
    }

    newOEProduct=productIDNameMap[orderEnrichmentConfiguration.productOptionId];
    if(newOEProduct==null || newOEProduct=='Site Contacts' || newOEProduct=='Number Configuration' || newOEProduct=='Enterprise Trunk') return true;
    for(let oneOE of Object.values(configurationParam.orderEnrichmentList)) {
        if(productIDNameMap[oneOE.productOptionId]==newOEProduct && oneOE.guid!=orderEnrichmentConfiguration.guid) {
            return false;
        }
    }
    return true;
}

//prevents deleting of important configurations if it is the last configuration related to a site
//returns TRUE if config can be deleted, FALSE if it cannot
function checkAndPreventConfigurationDelete(solution,componentParam,configurationParam) {
    var siteGuids={};
    for(let component of Object.values(solution.components)) {
        if(component.name==CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                console.log(configuration);
                siteGuids[configuration.guid]=configuration.configurationName;
            }
        }
    }
    if(siteGuids[configurationParam.getAttribute('Site GUID').value]==null) {
        return true;
    }
    for(let component of Object.values(solution.components)) {
        if(component.name==componentParam.name) {
            siteCount=0;
            for(let configuration of Object.values(component.schema.configurations)) {
                if(configuration.getAttribute('Site GUID').value==configurationParam.getAttribute('Site GUID').value) {
                    siteCount+=1;
                }
            }
            if (siteCount>1) {
                return true;
            }
            else {
                return siteGuids[configurationParam.getAttribute('Site GUID').value];
            }
        }
    }
}


//LPDE-310 add "onsite installation" if installation exists on site
async function toggleSiteInstallations(solution,addonParam) {
    var start = new Date();
    console.log('toggleSiteInstallations finished '+start.getMinutes()+' : '+start.getSeconds());
    var installationNeeded;
    var instalExists;
    var sitesRequiringInstal = {};
    
    for(let component of Object.values(solution.components) ) {
        if(component.name==CLOUD_VOICE.CPE || component.name==CLOUD_VOICE.LAN) {
            for(let configuration of Object.values(component.schema.configurations)){
                installationNeeded=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if (relatedProduct.name=='LAN Installation' || relatedProduct.name=='Phone Installation') {
                        installationNeeded=true;
                    }
                }
                if(installationNeeded) {
                    //find site guid through attribute on configuration
                    for(let attribute of Object.values(configuration.attributes)) {
                        if (attribute.name=='Site GUID') {
                            sitesRequiringInstal[attribute.value]=true;
                        }
                    }
                }
            }
        }
    } 
    for(let component of Object.values(solution.components) ) {
        if(component.name==CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)){
                instalExists=false;
                //check if addon already exists
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if (relatedProduct.name=='BTCV Onsite Installation') {
                        instalExists=true;
                        break;
                    }
                }
                if (sitesRequiringInstal[configuration.guid]==true && instalExists==false) {    //if addon requires instal
                    //on site level, find addon to add, then add
                    try{
                        var possibleAddons = await component.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(possibleAddons)) {
                            for(let oneAddon of segment.allAddons) {
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='BTCV Onsite Installation') {
    
                                    let addOnData = {
                                        'relatedProductName' : 'Site Access Add-Ons', 'associationId' : oneAddon.Id,
                                        'addonPriceItemId' : oneAddon.cspmb__Add_On_Price_Item__c, 'name' : oneAddon.cspmb__Add_On_Price_Item__r.Name,
                                        'quantity' : 1, 'group' : oneAddon.cspmb__Group__c, 'raw' : oneAddon
                                    };
                                    await component.addAddOn(configuration.guid, addOnData);
                                    break;
                                }
                            }
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }
                if (sitesRequiringInstal[configuration.guid]!=true && instalExists==true) { //addon doesnt require instal
                    for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                        if (relatedProduct.name=='BTCV Onsite Installation') {
                            await deleteAddOns(component,configuration,relatedProduct.guid,true);
                        }
                    }
                }
            }
        }
    }
    var finish = new Date();
    console.log('toggleSiteInstallations finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//if addon quantity is empty,put 1
function calculateAddonQuantity(component,configuration,relatedProduct) {
    for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
        if(attribute.name=='Quantity') {
            if(attribute.value==null || attribute.value=='0') {
                attribute.value=1;
            }
        }
    }
}

//LPDE-289 add calbing addon patch coord
//if configParam==null, do for all configs
async function addPatchCordsAddon(solution,configParam){
    var start = new Date();
    console.log('addPatchCordsAddon started '+start.getMinutes()+' : '+start.getSeconds());
    
    var addonName='Patch Cord - Cat 6 0.5 metre RJ45-RJ45 - Grey (Per pack of 5)';
    var addonNameFit='Patch Cord - Cat 6 0.5 metre RJ45-RJ45 - Grey';
    var LANaddonMap={}; //input Site-GUID --output string revealing LAN addon combination
    var currentSite;
    var configSite;
    
    if(configParam!=null) configSite=configParam.getAttribute('Site GUID').value; 

    for(let component of Object.values(solution.components)) {
        if (component.name=='LAN') {
            for(let configuration of Object.values(component.schema.configurations)) {
                //if it is one configuration, and not related to this one
                currentSite=configuration.getAttribute('Site GUID').value;
                if(configParam!=null && configSite!=currentSite) continue;
                //before querying addons, check if maybe we already have all the info
                var needToQuery=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    var addonID;
                    for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            addonID=attribute.value;
                            break;
                        }
                    }
                    if(globalAddonPortProvidedMap[addonID]==null) {
                        needToQuery=true;
                        break;
                    }
                }
                if(needToQuery) {
                    try {
                        //determine how many ports each LAN addon has
                        var LANAddons=await component.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(LANAddons) ) {
                            for(let oneAddon of Object.values(segment.allAddons)) {
                                globalAddonPowerMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Power__c;
                                globalAddonPortMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Port_Required__c;
                                globalAddonPortProvidedMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Available_Ports__c
                                globalAddonOutletMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Outlet_Required__c;                           
                                globalAddonUFPRequiredMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.UFP_Required__c; 
                            }
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }

                //determine real amount of ports for existing LAN addons
                var LANcombinationMap={}; //input port provided -- output addon quantity
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if(relatedProduct.groupName=='BT LAN') {
                        for(let attribute of Object.values(relatedProduct.configuration.attributes)) {
                            if(attribute.name=='Add On ID') {
                                currentPortProvided=globalAddonPortProvidedMap[attribute.value];
                            }
                            if(attribute.name=='Quantity') {
                                currentQuantityProvided=Number(attribute.value);
                            }
                        }
                        if (!isNaN(currentQuantityProvided) && !isNaN(currentPortProvided)) LANcombinationMap[currentPortProvided]=currentQuantityProvided;
                    }
                }
                console.log(Object.values(LANcombinationMap).length);
                console.log(LANcombinationMap);
                if(LANcombinationMap[8]==1 && Object.values(LANcombinationMap).length==1) LANaddonMap[currentSite]='1-8';
                if(LANcombinationMap[8]==2 && Object.values(LANcombinationMap).length==1) LANaddonMap[currentSite]='2-8';
                if(LANcombinationMap[24]==1 && Object.values(LANcombinationMap).length==1) LANaddonMap[currentSite]='1-24';
                if(LANcombinationMap[8]==1 && LANcombinationMap[24]==1 && Object.values(LANcombinationMap).length==2) LANaddonMap[currentSite]='1-8 1-24';
                if(LANcombinationMap[48]==1 && Object.values(LANcombinationMap).length==1) LANaddonMap[currentSite]='1-48';
                if(LANcombinationMap[8]==1 && LANcombinationMap[48]==1 && Object.values(LANcombinationMap).length==2) LANaddonMap[currentSite]='1-8 1-48';
                if(LANcombinationMap[24]==1 && LANcombinationMap[48]==1 && Object.values(LANcombinationMap).length==2) LANaddonMap[currentSite]='1-24 1-48';
                if(LANcombinationMap[48]==2 && Object.values(LANcombinationMap).length==1) LANaddonMap[currentSite]='2-48';
            }
        }
    }
    
    for(let component of Object.values(solution.components)) {
        if (component.name=='Cabling') {
            for(let configuration of Object.values(component.schema.configurations)) {
                var qtt;
                var theAddon;
                var cablingAddon;
                var supplyType=configuration.getAttribute('Supply Type').displayValue;
                currentSite=configuration.getAttribute('Site GUID').value;
                if(configSite==null || currentSite==configSite) {
                    console.log('configSite',configSite,'currentSite',currentSite);
                    if(LANaddonMap[currentSite]=='1-8') qtt=2;
                    if(LANaddonMap[currentSite]=='2-8') qtt=4;
                    if(LANaddonMap[currentSite]=='1-24') qtt=5;
                    if(LANaddonMap[currentSite]=='1-8 1-24') qtt=7;
                    if(LANaddonMap[currentSite]=='1-48') qtt=10;
                    if(LANaddonMap[currentSite]=='1-8 1-48') qtt=12;
                    if(LANaddonMap[currentSite]=='1-24 1-48') qtt=15;
                    if(LANaddonMap[currentSite]=='2-48') qtt=20;

                    if(qtt>=2) {
                        //add or update addon
                        theAddon=null;
                        for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                            if( (relatedProduct.name==addonName && supplyType=='Supply Only')
                             || (relatedProduct.name==addonNameFit && supplyType=='Supply and Fit') ){
                                theAddon=relatedProduct;
                            }
                        }
                        if(theAddon!= null) {
                            for(let addonAttribute of Object.values(theAddon.configuration.attributes)) {
                                if(addonAttribute.name=='Quantity') addonAttribute.value=qtt;
                            }
                        }
                        else {
                            if(globalAddonMap[addonName]==null || globalAddonMap[addonNameFit]==null) {
                                try {
                                    var cablingAddons=await component.getAddonsForConfiguration(configuration.guid);
                                    for(let segment of Object.values(cablingAddons) ) {
                                        for(let oneAddon of Object.values(segment.allAddons)) {
                                            if( (oneAddon.cspmb__Add_On_Price_Item__r.Name==addonName && supplyType=='Supply Only')
                                            || (oneAddon.cspmb__Add_On_Price_Item__r.Name==addonNameFit && supplyType=='Supply and Fit')) {
                                                globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                                                break;
                                            }
                                        }
                                    }
                                } catch (error) {console.log('ERROR IS ',error);}
                            }
                            if(supplyType=='Supply Only') cablingAddon=globalAddonMap[addonName];
                            if(supplyType=='Supply and Fit') cablingAddon=globalAddonMap[addonNameFit];

                            if(cablingAddon!=null){
                                await addAddOnProductConfiguration('Cabling Add On',cablingAddon,configuration.guid,qtt,component);
                            }
                        }
                    }
                    else {
                        //delete addon
                        for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                            if (relatedProduct.name==addonName || relatedProduct.name==addonNameFit) {
                                await deleteAddOns(component,configuration,relatedProduct.guid,true);
                            }
                        }
                    }
                }                
            }
        }
    }
    var finish = new Date();
    console.log('addPatchCordsAddon finished '+finish.getMinutes()+' : '+finish.getSeconds());
}


//LPDE-771
//adds or removes "Unlimited Call Plan" addons or "SIP Channels" addons
async function calculateUnlimitedCallPlan(configurationParam,solution) {
    //SIP part
    var mainConfiguration= Object.values(solution.schema.configurations)[0];
    var unlimitedDummyExists=false;
    var unlimitedCount=0;
    var unlimitedAddonFound;
    for(let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
        if(relatedProduct.name=='Unlimited') {
            if(relatedProduct.groupName=='UK') {
                unlimitedDummyExists=true;
            }
            if(relatedProduct.groupName=='Call Plan') {
                unlimitedCount+=1;
                unlimitedAddonFound=relatedProduct;
             }
        }
    }
    var SIPChannelsRequired = Number(mainConfiguration.getAttribute('SIP Channels Required').value);
    if(isNaN(SIPChannelsRequired)) SIPChannelsRequired=0;
    if(unlimitedCount==1 && unlimitedDummyExists && SIPChannelsRequired>0) {
        //change the one existing unlimited quantity to SIPChannelsRequired quantity
        for(let addonAttribute of Object.values(unlimitedAddonFound.configuration.attributes)) {
            if(addonAttribute.name=='Quantity') addonAttribute.value=SIPChannelsRequired;
        }
    }
    else {
        //delete all unlimited from config
        for(let relatedProduct of Object.values(configurationParam.relatedProductList)) {
            if(relatedProduct.name=='Unlimited' && relatedProduct.groupName=='Call Plan') {
                await deleteAddOns(solution, configurationParam, relatedProduct.guid, true);
            }
        }
        //find and add unlimited addon with quantity of SIPChannelsRequired
        if(unlimitedDummyExists && SIPChannelsRequired>0) {
            
            try {
                if(globalUnlimitedAddonMap['Site']==null) {
                    var unlimitedAddons=await solution.getAddonsForConfiguration(configurationParam.guid);
                    for(let segment of Object.values(unlimitedAddons) ) {
                        for(let oneAddon of Object.values(segment.allAddons)) {
                            if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Unlimited' && oneAddon.cspmb__Group__c=='Call Plan') {
                                globalUnlimitedAddonMap['Site']=oneAddon;
                            }
                        }
                    }
                } 
                await addAddOnProductConfiguration('SIP Channels',globalUnlimitedAddonMap['Site'],configurationParam.guid,SIPChannelsRequired,solution);
            } catch (error) {console.log('ERROR IS ',error);}
        }
    }
   
    //UFP part
    for(let component of Object.values(solution.components)) {
        if (component.name==CLOUD_VOICE.UFP) {
            for(let configuration of Object.values(component.schema.configurations)) {
                unlimitedCount=0;
                unlimitedAddonFound=null;
                var quantityUFP = Number(configuration.getAttribute('Quantity').value);
                if(isNaN(quantityUFP)) quantityUFP=0;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    if(relatedProduct.name=='Unlimited') {
                        unlimitedCount+=1;
                        unlimitedAddonFound=relatedProduct;
                    }
                }
            
                if(unlimitedCount==1 && unlimitedDummyExists && quantityUFP>0) {
                    //change the one existing unlimited quantity to UFP quantity
                    for(let addonAttribute of Object.values(unlimitedAddonFound.configuration.attributes)) {
                        if(addonAttribute.name=='Quantity') addonAttribute.value=quantityUFP;
                    }
                }
                else {
                    //delete all unlimited from config
                    for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                        if(relatedProduct.name=='Unlimited') {
                            await deleteAddOns(component, configuration, relatedProduct.guid, true);
                        }
                    }
                    //find and add unlimited addon with quantity of UFP
                    if(unlimitedDummyExists && quantityUFP>0){
                        try {
                            if(globalUnlimitedAddonMap['UFP']==null) {
                                var unlimitedAddons=await solution.getAddonsForConfiguration(configuration.guid);
                                for(let segment of Object.values(unlimitedAddons) ) {
                                    for(let oneAddon of Object.values(segment.allAddons)) {
                                        if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Unlimited') {
                                            globalUnlimitedAddonMap['UFP']=oneAddon;
                                        }
                                    }
                                }
                            }
                            await addAddOnProductConfiguration('UFP Add Ons',globalUnlimitedAddonMap['UFP'],configuration.guid,1,component);
                        } catch (error) {console.log('ERROR IS ',error);}
                    }
                }
            }
        }
    }
}

//LPDE-43
//checks if there is at least 1 of each Sharer Plan: UK and International addons
function checkSharerPlanAddons(configuration){
    var internationalCount=0;
    var UKCount=0; 
    for(let relatedProduct of Object.values(configuration.relatedProducts)) {
        if(relatedProduct.relatedProductName=='Sharer Call Plan Add-On' || relatedProduct.relatedProductName=='Call Sharer Plan Add-On') {
            if(relatedProduct.groupName=='International') internationalCount+=1;
            if(relatedProduct.groupName=='UK') UKCount+=1;
        }
    }
    if (internationalCount!=1 || UKCount!=1) {
        configuration.status = false;
        configuration.statusMessage = 'There must be 1 of each Sharer Plan: UK and International';
    }
    else { 
        if (configuration.statusMessage == 'There must be 1 of each Sharer Plan: UK and International'){
            configuration.status = true;
            configuration.errorMessage = null;
        }
    }
}

//checks if there is at least 1 of each Sharer Plan: UK and International addons
function checkConditionForSkillAddon(configuration){
    var requiredTrainingAddFound=false;
    var skillAddongAddFound=false;
    for(let relatedProduct of Object.values(configuration.relatedProducts)) {
        if(relatedProduct.relatedProductName=='Training Add-On') {
            if(relatedProduct.groupName=='Skills'){
				skillAddongAddFound = true;
			}
        }
    }
	if(skillAddongAddFound){
		for(let relatedProduct of Object.values(configuration.relatedProducts)) {
			if(relatedProduct.relatedProductName=='Training Add-On') {
				if(relatedProduct.groupName=='Passport'){
					requiredTrainingAddFound = true;
				}
			}
		}
		if (!requiredTrainingAddFound) {
			configuration.status = false;
			configuration.statusMessage = 'There must be either the one year outright sale; 3 or 5 year rental products required for Skill Product';
		}
		else { 
			if (configuration.statusMessage == 'There must be either the one year outright sale; 3 or 5 year rental products required for Skill Product'){
				configuration.status = true;
				configuration.errorMessage = null;
			}
		}
	}
}

//makes "SIP Channels Required" readonly if there are no switch configuration, and reverse
function checkIfSIPTexists(solution) {
    var SIPexists;
    for(let component of Object.values(solution.components)) {
        if (component.name==CLOUD_VOICE.SIPTswitch) {
            if(Object.values(component.schema.configurations).length>0) SIPexists=true;
        }
    }
    let mainConfiguration = Object.values(solution.schema.configurations)[0];
    SIPChannelsRequired=mainConfiguration.getAttribute('SIP Channels Required').value;
    if(SIPChannelsRequired=='N/A') SIPChannelsRequired='';

   if(SIPexists) {
        updateData = [{name : 'SIP Channels Required', value : SIPChannelsRequired, displayValue : SIPChannelsRequired, readOnly: false, required : true}];
        solution.updateConfigurationAttribute(mainConfiguration.guid, updateData, true);
    }
    else {
        updateData = [{name : 'SIP Channels Required', value : 'N/A', displayValue : 'N/A',readOnly : true, required : false}];
        solution.updateConfigurationAttribute(mainConfiguration.guid, updateData, true);
    }
}




//LPDE-750
//checks if "User Failover" and "Smart Forwarding" addons are smaller in quantity than "SIP Channels Required" attribute
function checkUserFeatures(configuration){
    var userFailover=0;
    var smartForwarding=0;
    var SIPUsersRequired;

    for(let relatedProduct of Object.values(configuration.relatedProducts)) {
        if(relatedProduct.groupName=='User Failover') {
            for(let addonAttribute of Object.values(relatedProduct.configuration.attributes)) {
                if(addonAttribute.name=='Quantity') if(!isNaN(Number(addonAttribute.value))) userFailover+=Number(addonAttribute.value);
            }
        }
        if(relatedProduct.groupName=='Smart Forwarding') {
            for(let addonAttribute of Object.values(relatedProduct.configuration.attributes)) {
                if(addonAttribute.name=='Quantity') if(!isNaN(Number(addonAttribute.value))) smartForwarding+=Number(addonAttribute.value);
            }
        }
    }
    SIPUsersRequired=Number(configuration.getAttribute('SIP Channels Required').value);
    if(isNaN(SIPUsersRequired)) SIPUsersRequired=0;
    
    if (SIPUsersRequired<userFailover || SIPUsersRequired<smartForwarding) {
        configuration.status = false;
        configuration.statusMessage = 'There can not be more User Failover or Smart Forwarding addons than there are SIP Channels';
    }
    else { 
        if (configuration.statusMessage == 'There can not be more User Failover or Smart Forwarding addons than there are SIP Channels'){
            configuration.status = true;
            configuration.errorMessage = null;
        }
    }
}
// CR12677 - Add BT Cloud Voice Training - 5 year addon if contract term is 5 years
async function addPassportAddOnForContractTerm(solution,configuration,attributeValue){
	for(let relatedProduct of Object.values(configuration.relatedProducts)) {		
		if(relatedProduct.groupName=='Passport') {			
				await deleteAddOns(solution, configuration, relatedProduct.guid, true);		
		}
	}
	if(attributeValue=='60'){	
		try {
			var tariningAddOn;
			if(globalAddonMap['BT Cloud Voice Training - 5 year']==null) {
				var passportAddons=await solution.getAddonsForConfiguration(configuration.guid);
				for(let segment of Object.values(passportAddons) ) {
					for(let oneAddon of Object.values(segment.allAddons)) {
						globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
						if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='BT Cloud Voice Training - 5 year') {
							tariningAddOn=oneAddon;
						}
					}
				}
			}
			else {
				console.log('using from addon map');
				tariningAddOn=globalAddonMap['BT Cloud Voice Training - 5 year'];
			}
			await addAddOnProductConfiguration('Training Add-On',tariningAddOn,configuration.guid,1,solution);
		} catch (error) {console.log('ERROR IS ',error);}			
	}
	else if(attributeValue=='36'){	
		try {
			var tariningAddOn;
			if(globalAddonMap['BT Cloud Voice Training - 3 year']==null) {
				var passportAddons=await solution.getAddonsForConfiguration(configuration.guid);
				for(let segment of Object.values(passportAddons) ) {
					for(let oneAddon of Object.values(segment.allAddons)) {
						globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
						if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='BT Cloud Voice Training - 3 year') {
							tariningAddOn=oneAddon;
						}
					}
				}
			}
			else {
				console.log('using from addon map');
				tariningAddOn=globalAddonMap['BT Cloud Voice Training - 3 year'];
			}
			await addAddOnProductConfiguration('Training Add-On',tariningAddOn,configuration.guid,1,solution);
		} catch (error) {console.log('ERROR IS ',error);}			
	}
	
	
}

//LPDE-564
//Adds required type of Call analytics addons and removes the incorrect types of Call analytics addons
async function callAnaliticsAddons(solution,attributeValue){
    var start = new Date();
    console.log('callAnaliticsAddons started '+start.getMinutes()+' : '+start.getSeconds());
    
    for(var configuration of Object.values(solution.schema.configurations)) {
        var RCount=0;
        var RPCount=0;
        var ICount=0;
        for(let relatedProduct of Object.values(configuration.relatedProducts)) {
            if(relatedProduct.name=='Call Analytics Supervisor (Report)') {
                RCount+=1;
            }
            if(relatedProduct.name=='Call Analytics Supervisor (Report Premier)') {
                RPCount+=1;
            }
            if(relatedProduct.name=='Call Analytics Supervisor (Insight)') {
                ICount+=1;
            }
        }
        if(attributeValue=='Report') { //ONLY FOR REPORT
            for(let relatedProduct of Object.values(configuration.relatedProducts)) {
                if(relatedProduct.name.includes('Call Analytics Supervisor')) {
                    if(!(RCount==1 && relatedProduct.name=='Call Analytics Supervisor (Report)')){
                        await deleteAddOns(solution, configuration, relatedProduct.guid, true);
                    }
                }
            }
            if(RCount!=1) {
                try {
                    var callAddon;
                    if(globalAddonMap['Call Analytics Supervisor (Report)']==null) {
                        var callAddons=await solution.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(callAddons) ) {
                            for(let oneAddon of Object.values(segment.allAddons)) {
                                globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Unlimited' && oneAddon.cspmb__Group__c=='Call Plan') globalUnlimitedAddonMap['Site']=oneAddon;
                                
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Call Analytics Supervisor (Report)') {
                                    callAddon=oneAddon;
                                }
                            }
                        }
                    }
                    else {
                        console.log('using from addon map');
                        callAddon=globalAddonMap['Call Analytics Supervisor (Report)'];
                    }
                    await addAddOnProductConfiguration('Call Analytics',callAddon,configuration.guid,1,solution);
                } catch (error) {console.log('ERROR IS ',error);}
            }
        }
        
        if(attributeValue=='Report Premier') { //ONLY FOR REPORT PREMIER
            for(let relatedProduct of Object.values(configuration.relatedProducts)) {
                if(relatedProduct.name.includes('Call Analytics Supervisor')) {
                    if(!(RPCount!=1 && relatedProduct.name=='Call Analytics Supervisor (Report Premier)')){
                        await deleteAddOns(solution, configuration, relatedProduct.guid, true);
                    }
                }
            }
            if(RPCount!=1) {
                try {
                    var callAddon;
                    if(globalAddonMap['Call Analytics Supervisor (Report Premier)']==null) {
                        var callAddons=await solution.getAddonsForConfiguration(configuration.guid);
                        console.log(callAddons);
                        for(let segment of Object.values(callAddons) ) {
                            for(let oneAddon of Object.values(segment.allAddons)) {
                                globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Unlimited' && oneAddon.cspmb__Group__c=='Call Plan') globalUnlimitedAddonMap['Site']=oneAddon;
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Call Analytics Supervisor (Report Premier)') {
                                    callAddon=oneAddon;
                                }
                            }
                        }
                    }
                    else {
                        callAddon=globalAddonMap['Call Analytics Supervisor (Report Premier)'];
                    }
                    await addAddOnProductConfiguration('Call Analytics',callAddon,configuration.guid,1,solution);
                } catch (error) {console.log('ERROR IS ',error);}
            }
        }
        
        if(attributeValue=='Insight') { //ONLY FOR INSIGHT
            for(let relatedProduct of Object.values(configuration.relatedProducts)) {
                if(relatedProduct.name.includes('Call Analytics Supervisor')) {
                    if(!(ICount!=1 && relatedProduct.name=='Call Analytics Supervisor (Insight)')){
                        await deleteAddOns(solution, configuration, relatedProduct.guid, true);
                    }
                }
            }
            if(ICount!=1) {
                try {
                    var callAddon;
                    if(globalAddonMap['Call Analytics Supervisor (Insight)']==null) {
                        var callAddons=await solution.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(callAddons) ) {
                            for(let oneAddon of Object.values(segment.allAddons)) {
                                globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Unlimited' && oneAddon.cspmb__Group__c=='Call Plan') globalUnlimitedAddonMap['Site']=oneAddon;
                                if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Call Analytics Supervisor (Insight)') {
                                    callAddon=oneAddon;
                                }
                            }
                        }
                    }
                    else {
                        console.log('using from addon map');
                        callAddon=globalAddonMap['Call Analytics Supervisor (Insight)'];
                    }
                    await addAddOnProductConfiguration('Call Analytics',callAddon,configuration.guid,1,solution);
                } catch (error) {console.log('ERROR IS ',error);}
            }
        }
    }
    var finish = new Date();
    console.log('callAnaliticsAddons finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//adds or removes attendance charge addon(s)
async function autoAddAttendance(solution,configurationParam){
    console.log(globalAddonMap);
    var start = new Date();
    console.log('autoAddAttendance started '+start.getMinutes()+' : '+start.getSeconds());
    
    for(var configuration of Object.values(solution.schema.configurations)) {
        if(configurationParam!=null && configuration.guid!=configurationParam.guid) continue;
        var ACount=0;
        var currentCType;
        var currentSType;
        for(let attribute of Object.values(configuration.attributes)) {
            if (attribute.name=='Cabling Type') {
                currentCType=attribute.displayValue;
            }
            if (attribute.name=='Supply Type') {
                currentSType=attribute.value;
            }
        }
        console.log('currentCType',currentCType,'currentSType',currentSType,'ACount',ACount);
        for(let relatedProduct of Object.values(configuration.relatedProducts)) {
            if(relatedProduct.name=='Attendance Charge') {
                if(currentCType!='BT Cableplan' || currentSType!='Supply and Fit') {
                   await deleteAddOns(solution, configuration, relatedProduct.guid, true); 
                }
                ACount+=1;
            }
        }
        if(ACount!=1 && currentCType=='BT Cableplan' && currentSType=='Supply and Fit') {
            for(let relatedProduct of Object.values(configuration.relatedProducts)) {
                if(relatedProduct.name=='Attendance Charge') {
                    await deleteAddOns(solution, configuration, relatedProduct.guid, true);
                }
            }
            var cablingAddon;
            try {
                if(globalAddonMap['Attendance Charge']==null) {
                    var cablingAddons=await solution.getAddonsForConfiguration(configuration.guid);
                    for(let segment of Object.values(cablingAddons) ) {
                        for(let oneAddon of Object.values(segment.allAddons)) {
                            globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                            if (oneAddon.cspmb__Add_On_Price_Item__r.Name=='Attendance Charge') {
                                console.log('found addon');
                                cablingAddon=oneAddon;
                            }
                        }
                    }
                }
                else {
                    cablingAddon=globalAddonMap['Attendance Charge'];
                }
                await addAddOnProductConfiguration('Cabling Add On',cablingAddon,configuration.guid,1,solution);
            } catch (error) {console.log('ERROR IS ',error);}
        }
    }  
    var finish = new Date();
    console.log('autoAddAttendance finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//LPDE-777
//checks if there is at least one Gateway addon for a Non - BT maintained switch
function checkGatewayExistance(configuration) {
    var switchProvision=configuration.getAttribute('Switch Provision').displayValue;
    var existance=false;
    for(let relatedProduct of Object.values(configuration.relatedProductList)) {
        if(relatedProduct.groupName=='Gateway' && relatedProduct.name.includes('SIP Gateway')) {
            existance=true;
        }
    }
    if (!existance && switchProvision=='Non-BT Maintained Switch') {
        configuration.status = false;
        configuration.statusMessage = 'Minimum of one Gateway is required for a Non-BT Maintained Switch';
    }
    else { 
        if (configuration.statusMessage == 'Minimum of one Gateway is required for a Non-BT Maintained Switch'){
            configuration.status = true;
            configuration.errorMessage = null;
        }
    }
}

//fixes UI after solution reloaded
function reloadUIafterSolutionLoaded(solution){
    let mainConfiguration = Object.values(solution.schema.configurations)[0];

    for(attribute of Object.values(mainConfiguration.attributes)) {
        if (attribute.value=='N/A') {
            attribute.readOnly=true;
        }
    }

    for(let component of Object.values(solution.components) ) {
        for(let configuration of Object.values(component.schema.configurations)) {
            for(attribute of Object.values(configuration.attributes)) {
                if (attribute.value=='N/A' || (attribute.name=='Site Type' && attribute.value!='')
                 || (attribute.name=='Interface Type' && attribute.value=='Both')
                  || (attribute.name=='Site Access' && attribute.value!='') ) {
                    attribute.readOnly=true; 
                }
            }
        }
        
        if(component.name==CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {

                let siteType = configuration.getAttribute('Site Type').displayValue;
                
                if(siteType=='Cloud Voice - Business Site'){
                    updateDependentPicklistValues('Access New/Existing/OTT', configuration.getAttribute('Access New/Existing/OTT'),
                    siteAccessDependentAttributeMap[0], solution, configuration, true, configuration.getAttribute('Access Type').displayValue);
                }
                if(siteType=='Cloud Voice - Homeworker'){
                    updateDependentPicklistValues('Access New/Existing/OTT', configuration.getAttribute('Access New/Existing/OTT'),
                    siteAccessDependentAttributeMapHW[0], solution, configuration, true, configuration.getAttribute('Access Type').displayValue);
                }
                if(siteType=='SIP-T - Business Site'){
                    updateDependentPicklistValues('Access New/Existing/OTT', configuration.getAttribute('Access New/Existing/OTT'),
                    siteAccessDependentAttributeMapSIPT[0], solution, configuration, true, configuration.getAttribute('Access Type').displayValue);
                }

                updateDependentPicklistValues('Access Type', configuration.getAttribute('Access Type').displayValue,
                 siteAccessDependentAttributeMap[1], solution, configuration, false,configuration.getAttribute('Firewall Option').displayValue);
            }
        }
    }
}


//caches addon  data on solution load to speed up async calls
async function preloadAddonData(solution){
    var start = new Date();
    console.log('preloadAddonData started '+start.getMinutes()+' : '+start.getSeconds());
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.LAN || component.name==CLOUD_VOICE.CPE) {
            for(let configuration of Object.values(component.schema.configurations)) {
    
                //figure out if query is needed or all addons are in the map already
                var needToQuery=false;
                for(let relatedProduct of Object.values(configuration.relatedProductList)) {
                    var addonID;
                    for(attribute of Object.values(relatedProduct.configuration.attributes)) {
                        if (attribute.name=='Add On ID') {
                            addonID=attribute.value;
                            break;
                        }
                    }
                    if(globalAddonPowerMap[addonID]==null || globalAddonPortMap[addonID]==null || globalAddonPortProvidedMap[addonID]==null || globalAddonUFPRequiredMap[addonID]==null || globalAddonOutletMap[addonID]==null ) {
                        console.log('addonID missing',addonID);
                        needToQuery=true;
                        break;
                    }
                }

                if(needToQuery){
                    try{
                        let currentAddons = await component.getAddonsForConfiguration(configuration.guid);
                        for(let segment of Object.values(currentAddons) ) {
                            for(let oneAddon of Object.values(segment.allAddons)) {
                                globalAddonPowerMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Power__c;
                                globalAddonPortMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Port_Required__c;
                                globalAddonPortProvidedMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Available_Ports__c
                                globalAddonOutletMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.Outlet_Required__c;                           
                                globalAddonUFPRequiredMap[oneAddon.cspmb__Add_On_Price_Item__r.Id]=oneAddon.cspmb__Add_On_Price_Item__r.UFP_Required__c;   
                            }
                        }
                    } catch (error) {console.log('ERROR IS ',error);}
                }
            }
        }
    }
    var finish = new Date();
    console.log('preloadAddonData finished '+finish.getMinutes()+' : '+finish.getSeconds());
}

//auto adds Webcast addon to the main component
async function autoAddWebcast(solution, component, configuration) {
    return;
    var needAddon=true;
    //var monthYearMap = {('12','1'),('36','3'),('60','5')};
    var term = configuration.getAttribute('Contract Term').displayValue;
    for(let relatedProduct of Object.values(configuration.relatedProductList)) {
        if(relatedProduct.name.includes('Webcast Max Passport - ') &&
        (relatedProduct.name.includes('1') && term=='12') ||
        (relatedProduct.name.includes('3') && term=='36') ||
        (relatedProduct.name.includes('5') && term=='60')) {
            needAddon=false;
        }
        else{
            if(relatedProduct.name.includes('Webcast Max Passport - '))
                await deleteAddOns(component, configuration, relatedProduct.guid,true);
        }
    }
    /*if(needAddon) {
        try{
            if(globalAddonMap['Webcast Max Passport - '+monthYearMap[term]+' Yr']==null) {
                var possibleAddons=await solution.getAddonsForConfiguration(configuration.guid);
                for(let segment of Object.values(possibleAddons) ) {
                    for(let oneAddon of Object.values(segment.allAddons)) {
                        globalAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name]=oneAddon;
                    }
                }
            }
            await addAddOnProductConfiguration('Training Add-On',globalAddonMap['Webcast Max Passport - 1 Yr'],configuration.guid,1,solution);
        } catch(error) {}
    }*/
} 

//function that updates site info on components that are linked to the site (CPE, UFP, ..)
function updateSiteInfo(solution, configuration, siteAttributeName, siteAttributeGuid, siteType, siteNADkey) {
    updateConfigurationAttributeValue('Site GUID',siteAttributeGuid,siteAttributeGuid,true,solution,configuration.guid,true);
    updateConfigurationAttributeValue('Site Name',siteAttributeName,siteAttributeName,true,solution,configuration.guid,true);
    updateConfigurationAttributeValue('Site Type',siteType,siteType,true,solution,configuration.guid,true);
    updateConfigurationAttributeValue('Site NAD Key',siteNADkey,siteNADkey,true,solution,configuration.guid,true);
}




/*****CS hack visibilities*****/

//LPDE-590 hide add new and import buttons where necessary
function updateComponentLevelButtonVisibility(solution, componentName) {
    var homeExists=false;
    var businessExists=false;
    var siptExists=false;
    for(let component of Object.values(solution.components) ) {
        if (component.name== CLOUD_VOICE.siteAccess) {
            for(let configuration of Object.values(component.schema.configurations)) {
                let siteType=configuration.getAttribute('Site Type');
                if(siteType.displayValue=='Cloud Voice - Business Site') businessExists=true;
                if(siteType.displayValue=='Cloud Voice - Homeworker') homeExists=true;
                if(siteType.displayValue=='SIP-T - Business Site') siptExists=true;
            }
        }
    }

    //clone button manipulation
    let cloneButtons = document.getElementsByClassName('cs-btn icon icon-copy copy-btn');
    if (cloneButtons) {
        for (let i = 0; i < cloneButtons.length; i++) {
            cloneButtons[i].style.display = "none";
        }
    }

    //import button manipulation
	let importButtons = document.getElementsByClassName('slds-file-selector');
	if (importButtons) {
		for (let i = 0; i < importButtons.length; i++) {
			if(componentName!=CLOUD_VOICE.SIPTswitch) importButtons[i].style.display = "none";
		}
	}

	//add button manipulation
    let addButtons = document.getElementsByClassName('cs-btn btn-transparent');
    if (addButtons) {
        for (let i = 0; i < addButtons.length; i++) {
            if(( (!homeExists && !businessExists && (componentName==CLOUD_VOICE.UFP || componentName==CLOUD_VOICE.CPE))
            || componentName==CLOUD_VOICE.SIPTswitch
            || componentName==CLOUD_VOICE.cabling
            || componentName==CLOUD_VOICE.LAN) && addButtons[i].innerText.includes('Add ')) {
                addButtons[i].style.display = "none";
            }
            else {
                addButtons[i].style.display = "block";
            }
        }
    }

}

//function that queries the users profile using a remote action for later use
async function hideBasedOnProfile(solution) {
    globalCommission=''
    if(userProfile==null) {
        var currentBasket = await CS.SM.getActiveBasket();
        let inputMap={'basketID' : currentBasket.basketId,'action' : 'getProfileInfoRemote'};
        try{
            let result= await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', inputMap);
            userProfile=result['profile'];
            console.log('userProfile',userProfile);
        } catch (error) {console.log('ERROR IS ',error);}
    }
    if(  (String(userProfile)!='System Administrator' || userProfile==null)) {}
}

//irrelevant after introducing the spinner
//used to make a picklist attribute read only or editable on the UI with CSS
function toggleAttributePicklistEdit(solution,component,attributeDisplayName,readonly) {
    console.log('attributeDisplayName',attributeDisplayName,'is readonly?',readonly);
	var fields = document.getElementsByClassName('additional-product-container');
	var atName;
	for(var field of Object.values(fields)) {
	    if (field.children[0].children[0].innerText==attributeDisplayName) {
	        field.children[1].children[0].style.display=readonly;
	    }
	}
	return;
}

//irrelevant after introducing the spinner
//used to make an attribute read only or editable on the UI with CSS
function toggleAttributeEdit(solution,component,attributeDisplayName,readonly) {
    console.log('attributeDisplayName',attributeDisplayName,'is readonly?',readonly);
	var fields = document.getElementsByClassName('additional-product-container');
	var atName;
	for(var field of Object.values(fields)) {
	    if (field.children[0].children[0].innerText==attributeDisplayName) {
	        field.children[1].children[0].children[0].children[0].readOnly=readonly;
	    }
	}
	return;
}

//irrelevant after introducing the spinner
//used to disable or enable a checkbox attribute on the UI with CSS
function toggleAttributeCheckboxEdit(solution,component,attributeDisplayName,display) {
    console.log('about to display',display,attributeDisplayName);
	var fields = document.getElementsByClassName('additional-product-container');
	var atName;
	for(var field of Object.values(fields)) {
	    if (field.children[0].children[0].innerText==attributeDisplayName) {
	        field.children[1].children[0].style.display=display;
	    }
	}
	return;
}

//used for validating fields by type,depending on fieldType String
function validateFieldsByType(attribute,fieldType) {
    console.log('validateFieldsByType');
    errorMessage='Must be a '; 
    
    if (fieldType.includes('Positive')) {
        if (Number(attribute.value)<0) {
            errorMessage += ' positive';
        }
    }
    if (fieldType.includes('Negative')) {
        if (Number(attribute.value)>0) {
            errorMessage += ' negative';
        }
    }
    if (fieldType.includes('Whole')) {
        if (Number(attribute.value)%1!=0) {
            errorMessage += ' whole';
        }
    }
    if (fieldType.includes('Number')) {
        if (isNaN(Number(attribute.value)) || errorMessage!='Must be a ') {
            errorMessage += ' number.';
        }
    }
    if (fieldType.includes('Email')) {
        if (!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(attribute.value))) {
            errorMessage += ' valid Email.';
        }
    }
    if (fieldType.includes('Phone')) {
        if (isNaN(Number(attribute.value)) || Number(attribute.value)%1!=0 || Number(attribute.value)<0) {
            errorMessage += ' valid Phone number.';
        }
    }

    if (errorMessage=='Must be a ') {
        attribute.error = false;
        attribute.errorMessage = null;
        return null;
    }
    else{
        attribute.error = true;
        attribute.errorMessage = errorMessage;
        return errorMessage;
    } 
}

//function that is called before navigating to a component, and if Solution is Cloud Voice.
async function beforeNavigateCloudVoice(solution,currentComponent, previousComponent) {
    console.log('beforeNavigateCloudVoice',solution,currentComponent,previousComponent);

    let mainConfiguration = Object.values(solution.schema.configurations)[0];

    if (currentComponent.name==CLOUD_VOICE.siteAccess) {
        validateCallCentreAddon(solution);
    }
    
    return true;
}

//function that is called after navigating to a component, and if Solution is Cloud Voice.
async function afterNavigateCloudVoice(solution,currentComponent, previousComponent) {
    console.log('afterNavigateCloudVoice',solution,currentComponent,previousComponent);
    
    updateComponentLevelButtonVisibility(solution,currentComponent.name);

    if(currentComponent.name==CLOUD_VOICE.siteAccess) {
        await calculatePower(solution);
        await calculateAndAddDeliveryAddOn(solution);
        await calculateAvailabilityUFP(solution);
    }

    if(currentComponent.name==CLOUD_VOICE.UFP) {
        let mainConfiguration = Object.values(solution.schema.configurations)[0];
        let callRecordingStorageOption=mainConfiguration.getAttribute('Call Recording Storage Option').value;
        if (callRecordingStorageOption=='') callRecordingStorageOption='.';
        for(let configuration of Object.values(currentComponent.schema.configurations)) {
            if(configuration.getAttribute('Call Recording Storage Option').value=='') {
                updateConfigurationAttributeValue('Call Recording Storage Option', callRecordingStorageOption,
                                                   callRecordingStorageOption, false, solution, configuration.guid, true);
            }
        }
    }
    
    if(currentComponent.name==CLOUD_VOICE.LAN) {
        calculatePort(solution);
    }
    if(currentComponent.name==CLOUD_VOICE.cabling){
        calculateOutlet(solution);
    }
    if (currentComponent.name==CLOUD_VOICE.solution) {
        console.log('currentComponent is',currentComponent);

        if (globalAccountID=='') {
            let currentBasket = await CS.SM.getActiveBasket();
            console.log('currentBasket',currentBasket);
            let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole',{"basketId" : basketId, 'action' : 'CS_LookupQueryForAccountDetails'});
            if(result["DataFound"] == true) globalAccountID=result["AccountId"];
            console.log('result',result);
            console.log('globalAccountID',globalAccountID);
        }


        await calculateNumbersRequired(solution);
    }
    if (currentComponent.name==CLOUD_VOICE.SIPTswitch) {
        changeUploadAttributeText('Import North Supply','Upload here');
    }
    return true;
}


async function handleGatewayImport(configurationGuid) {
	var solution =  await CS.SM.getActiveSolution();
    var returntext= displayHandleGatewayImport(solution, configurationGuid);
    console.log('returntext',returntext);
    return returntext;
}

async function displayHandleGatewayImport(solution, configurationGuid){
    console.log('displayHandleGatewayImport');
    let basketId = solution.basketId;
    
    let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 50%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString +='<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString +='<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString +='</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
	urlString += ' Import North Supply </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__importDataFromCSV?configGuid='+configurationGuid+'&basketId='+basketId+'"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}

function changeUploadAttributeText(attributeDisplayName,newText) {
    console.log('attributeDisplayName    ',attributeDisplayName,'newText    ',newText);
	var fields = document.getElementsByClassName('additional-product-container');
	for(var field of Object.values(fields)) {
	    if (field.children[0].children[0].innerText==attributeDisplayName) {
	        field.children[1].children[0].children[0].children[0].children[0].innerText=newText;
	    }
	} 
	return;
}
/*****OE functions*****/

//checks if the string is a number between minimum and maximum length, allowing borders
function validateNumberLength(input, minimum, maximum) {
    return !(isNaN(input) || input.length > maximum || input.length < minimum)
}