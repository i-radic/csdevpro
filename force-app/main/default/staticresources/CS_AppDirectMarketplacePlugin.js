var imported = document.createElement('script');
imported.src = '/resource/1491499237000/cscfga__jQuery_min';
document.head.appendChild(imported);

console.log('------------>Loaded default plugin for App Direct Marketplace');

if(!CS || !CS.SM){
    throw Error('App Direct Marketplace Solution Console Api not loaded?');
}

if (!CS || !CS.SM) {
    console.log('not loaded');
    throw Error('App Direct Marketplace Solution Console Api not loaded?');
}

// Create a mapping to match your solution's components
// This means we can avoid too much hard-coding below and can more easily reuse code
var APPDIRECT_CONST = {
    solution: 'App Direct Marketplace', //Main SCHEMA name for plugin creation
    mainComponent: 'App Direct Marketplace',
    appDirectProduct: 'Product'//Component name for use in functions//ARTEMIS_CONST.appDirectProduct
};

//Register the Artemis Plugin
if (CS.SM.registerPlugin) {
    window.document.addEventListener('SolutionConsoleReady', async function() {
        console.log('SolutionConsoleReady Event Listener');
        await CS.SM.registerPlugin(APPDIRECT_CONST.solution)
            .then(plugin => {
                console.log("--------->Plugin registered for App Direct Marketplace");
                appDirectHooks(plugin);
            });
    });
}


function appDirectHooks(AppDirectPlugin) {
    
    AppDirectPlugin.beforeConfigurationAdd = async function(component, configuration) {
        console.log('AppDirectPlugin.beforeConfigurationAdd', component, configuration);
		if (component.name == APPDIRECT_CONST.appDirectProduct) {
		    var solution = await CS.SM.getActiveSolution();
		    var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
			var brandId = mainConfiguration.getAttribute('Brand').value;
			var brandValue = mainConfiguration.getAttribute('Brand').displayValue;
		    for (let attribute of Object.values(configuration.attributes)) {
				if (attribute.name == 'Brand') {
					attribute.value = brandValue;
					attribute.displayValue = brandValue;
					attribute.readOnly = true;
				}
				if (attribute.name == 'BrandId') {
					attribute.value = brandId;
					attribute.displayValue = brandId;
					attribute.readOnly = true;
				}
			}
		}
		return Promise.resolve(true);
    };
    
    AppDirectPlugin.afterAttributeUpdated = async function(component, configuration, attribute, oldValueMap) {
		console.log('AppDirectPlugin.afterAttributeUpdated', component, configuration, attribute, oldValueMap);
		var solution = await CS.SM.getActiveSolution();
		if (component.name == APPDIRECT_CONST.mainComponent) {
    		if (attribute.name == 'Brand') {
    		    updateConfigNameAppDirect(configuration, component.name);
    		}
		}
		if (component.name == APPDIRECT_CONST.appDirectProduct) {
		    if (attribute.name == 'Group') {
		        updateConfigurationAttributeValue('Product', '', '', false, component, configuration.guid, false);
		        updateConfigurationAttributeValue('Contract Term Lookup', '', '', false, component, configuration.guid, false);
		    }
		    if (attribute.name == 'Product') {
		        updateConfigNameAppDirect(configuration, component.name);
		        updateConfigurationAttributeValue('Contract Term Lookup', '', '', false, component, configuration.guid, false);
		    }
		}
		return Promise.resolve(true);
    };
}

function updateConfigNameAppDirect(con, componentName) {
	var newName;
	console.log('updateConfigNameAppDirect', con, componentName, con.parentConfiguration, con.parentConfiguration == '');
	if (componentName == APPDIRECT_CONST.mainComponent && con.parentConfiguration == '') {
		newName = 'App Direct : ' + con.getAttribute('Brand').displayValue;
	}
	if (componentName == APPDIRECT_CONST.appDirectProduct) {
		newName = con.getAttribute('Brand').value + ' : ' + con.getAttribute('Product').displayValue;
	}
	con.configurationName = newName;
}