// Global var that contains basket validation data
window.BasketItems = { 
	Map: {} 
};

(function(document, window, undefined){

	var NoneOption = '-- None --';
	var BTOPNoneOption = 'none';
	registerHandler('remoteAction', updatePicklists);
	SPM.remoteAction({ configId: window.configurationId, name: window.schema.definitionName }, 
		'CS_OEDataHandler', 'updatePicklists');

	function updatePicklists(response){
		console.log('*** Basket Level Items ***', response);
		Definitions[window.schema.definitionName](response);
	}

	var Definitions = {
		BTMobile: function(response){
			if(response){
				if(response.mobileHardware){
					var handsetOptions = [ NoneOption, 'Existing hardware/SIM', 'Replacement SIM' ],
						newHandsetOptions = [],
						handsetMap = [];

					response.mobileHardware.forEach(function(el){
						handsetMap.push({ name: el.Name.replace(/&amp;/g, '&').replace(/&quot;/g, ' inch'), quantity: el.Volume__c});
						newHandsetOptions.push(el.Name.replace(/&amp;/g, '&').replace(/&quot;/g, ' inch'));
					});

					BasketItems.Handsets = newHandsetOptions;
					BasketItems.Map.Handsets = handsetMap;

					updateOptions('Device', handsetOptions.concat(newHandsetOptions));
				}
				if(response.extras){
					var extraOptions = [ NoneOption ],
						newExtrasOptions = [],
						extrasMap = [];

					response.extras.forEach(function(el){
						extrasMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
						newExtrasOptions.push(el.Name.replace(/&amp;/g, '&'));
					});

					BasketItems.Extras = newExtrasOptions;
					BasketItems.Map.Extras = extrasMap;

					for(var i = 1; i <= 4; i++){
						updateOptions('Extra ' + i, extraOptions.concat(newExtrasOptions));
					}
				}
				if(response.mobileBroadband){
					var broadbandOptions = [],
						broadbandMap = [];

					response.mobileBroadband.forEach(function(el){
						broadbandMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c, dealType: el.Deal_Type__c});
						broadbandOptions.push(el.Name.replace(/&amp;/g, '&'));
					});

					BasketItems.Broadband = broadbandOptions;
					BasketItems.Map.Broadband = broadbandMap;
				}
				if(response.basketId){
					BasketItems.Id = response.basketId;
				}
				if(response.flex){
					BasketItems.Flex = response.flex;
				}
			}
		},
		BTOP: function(response){		
			if(response){
				if(response.dataExtras){
					var dataExtrasOptions = [ BTOPNoneOption ],
						dataExtrasMap = [];

					response.dataExtras.forEach(function(el){
						dataExtrasMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
						dataExtrasOptions.push(el.Name.replace(/&amp;/g, '&'));
					});

					BasketItems.DataExtras = dataExtrasOptions;
					BasketItems.Map.DataExtras = dataExtrasMap;

					updateOptions('Individual Data Extra', dataExtrasOptions);
				}
				if(response.roamingExtras){
					var roamingExtrasOptions = [ BTOPNoneOption ],
						roamingExtrasMap = [];

					response.roamingExtras.forEach(function(el){
						roamingExtrasMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
						roamingExtrasOptions.push(el.Name.replace(/&amp;/g, '&'));
					});

					BasketItems.RoamingExtras = roamingExtrasOptions;
					BasketItems.Map.RoamingExtras = roamingExtrasMap;

					updateOptions('Roaming Extra', roamingExtrasOptions);
				}
				if(response.callRecordings){
					var callRecordingsOptions = [ BTOPNoneOption ],
						callRecordingsMap = [];
					
					response.callRecordings.forEach(function(el){
						callRecordingsMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
						if(callRecordingsOptions.indexOf(el.Name.replace(/&amp;/g, '&')) == -1)
							callRecordingsOptions.push(el.Name.replace(/&amp;/g, '&'));
					});

					BasketItems.CallRecordings = callRecordingsOptions;
					BasketItems.Map.CallRecordings = callRecordingsMap;

					updateOptions('Call Recording', callRecordingsOptions);
				}
				/* CR 20180731 */
				if(response.deviceExtras && response.deviceExtras.length){
					var deviceExtrasOptions = [ BTOPNoneOption ],
						deviceExtrasMap = {},
						deviceExtrasList = [],
					    deviceCount = 0, bbCount = 0;
					
					response.deviceExtras.forEach(function(el){
						deviceCount += el.Volume__c;
						//deviceExtrasMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: deviceCount});
						debugger;
						if(deviceExtrasMap[el.Name.replace(/&amp;/g, '&')] != undefined)
							deviceExtrasMap[el.Name.replace(/&amp;/g, '&')] += el.Volume__c;
						else
							deviceExtrasMap[el.Name.replace(/&amp;/g, '&')] = el.Volume__c;
						deviceExtrasOptions.push(el.Name.replace(/&amp;/g, '&'));
					});	
					//mpk live fix
					if(response.blackberryExtras && response.blackberryExtras.length){
						response.blackberryExtras.forEach(function(el){
							bbCount += el.Volume__c;
							debugger;
							if(deviceExtrasMap[el.Name.replace(/&amp;/g, '&')] != undefined)
								deviceExtrasMap[el.Name.replace(/&amp;/g, '&')] += el.Volume__c;
							else
								deviceExtrasMap[el.Name.replace(/&amp;/g, '&')] = el.Volume__c;
							deviceExtrasOptions.push(el.Name.replace(/&amp;/g, '&'));
						});
					}
					//mpk live fix
					Object.keySet(deviceExtrasMap).forEach(function(el){
						deviceExtrasList.push({name: el, quantity: deviceExtrasMap[el]});
					});
					BasketItems.DeviceExtras = deviceExtrasOptions;
					BasketItems.Map.DeviceExtras = deviceExtrasList;

					updateOptions('Device Management', deviceExtrasOptions);
				}				
				else if(response.blackberryExtras && response.blackberryExtras.length){
					var blackberryExtrasOptions = [ BTOPNoneOption ],
						blackberryExtrasMap = [],
						deviceCount = 0, bbCount = 0;
					
					response.blackberryExtras.forEach(function(el){
						bbCount += el.Volume__c;
						blackberryExtrasMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
						blackberryExtrasOptions.push(el.Name.replace(/&amp;/g, '&'));
					});
					//mpk live fix
					if(response.deviceExtras && response.deviceExtras.length){
						response.deviceExtras.forEach(function(el){
							deviceCount += el.Volume__c;
							blackberryExtrasMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
							blackberryExtrasOptions.push(el.Name.replace(/&amp;/g, '&'));
						});
					}
					//mpk live fix
					BasketItems.BlackberryExtras = blackberryExtrasOptions;
					BasketItems.Map.BlackberryExtras = blackberryExtrasMap;

					updateOptions('Device Management', blackberryExtrasOptions);
				}
				/* CR 20180731 */
				if(response.devices){
					var devicesOptions = [ BTOPNoneOption ],
						devicesMap = [];
					
					response.devices.forEach(function(el){
						devicesMap.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
						devicesOptions.push(el.Name.replace(/&amp;/g, '&'));
					});

					BasketItems.Devices = devicesOptions;
					BasketItems.Map.Devices = devicesMap;

					updateOptions('Devices Required', devicesOptions);
				}
				if(response.subs){
					var subsOptions = [ BTOPNoneOption ],
						subsMap = [];
					
					response.subs.forEach(function(el){
						if(typeof el.Product_Type__c !== 'undefined'){
							subsMap.push({ name: el.Product_Type__c.replace(/&amp;/g, '&'), quantity: el.Volume__c});
							subsOptions.push(el.Product_Type__c.replace(/&amp;/g, '&'));
						}
					});

					BasketItems.Subscriptions = subsOptions;
					BasketItems.Map.Subscriptions = subsMap;

					updateOptions('Subscription Type', subsOptions);
				}
				if(response.signalAssist){
					var signalAssistMap = [];
					
					response.signalAssist.forEach(function(el){
						signalAssistMap.push({ name: 'BT Signal Assist', quantity: el.Volume__c});
					});

					BasketItems.Map.SignalAssist = signalAssistMap;
				}
				if(response.simExtras){
					var simExtrasMap = [];
					//updateOptions('SIM Extra', [BTOPNoneOption, 'NO']); mpk
					updateOptions('SIM Extra', ['No', 'Yes']);
					if(response.subsLevelAddon){
						var containsTriggerElements = false;
						response.subsLevelAddon.forEach(
							function(el){
								if(el.toLowerCase().indexOf('flexible') > -1 ||
									el.toLowerCase().indexOf('additional mobile number') > -1 ||
									el.toLowerCase().indexOf('sim extra') > -1){
									containsTriggerElements = true;
								}
							}
						);
						if(containsTriggerElements)
							//updateOptions('SIM Extra', [BTOPNoneOption, 'NO', 'Yes']); mpk
							updateOptions('SIM Extra', ['No', 'Yes']);
					}
					response.simExtras.forEach(function(el){
						simExtrasMap.push({ name: 'SIM Extra', quantity: el.Volume__c});
					});

					BasketItems.Map.SIMExtras = simExtrasMap;
				}
				if(response.basketId){
					BasketItems.Id = response.basketId;
				}
				if(response.hq){
					BasketItems.HQ = response.hq;
					//updateOptions('Number Type', [NoneOption, BasketItems.HQ[0].Deal_Type__c]);
				}
				if(response.csv && response.csv.length > 0){
					BasketItems.CSV = [];
					response.csv.forEach(function(el){
						var arr = JSON.parse(el.replace(/&quot;/g, '\"'));
						BasketItems.CSV = BasketItems.CSV.concat(arr);
					});
				}
				if(response.configsFromBtopBasket){
					var BasketConfigs = [];

					var sipExtraAllowed = 0;
					response.configsFromBtopBasket.forEach(function(el){
						BasketConfigs.push({ name: el.Name.replace(/&amp;/g, '&'), quantity: el.Volume__c});
						if(el.Name.replace(/&amp;/g, '&').toLowerCase().indexOf('sip extra') > -1)
							sipExtraAllowed += el.Volume__c;
					});

					if(sipExtraAllowed == 0)
						updateOptions('SIP Extra Softphone Only', ['No']);

					BasketItems.Map.BasketConfigs = BasketConfigs;
				}
			}
		}
	};

})(document, window);