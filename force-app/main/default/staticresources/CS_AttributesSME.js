{
	"Mobile Voice SME" : {
		"totalCharge" : [
			"Total_Charges_After_Discount_0"
		],
		
		"totalInvestmentPot" : [
			"Investment_Pot_0"
		],
		
		"chargesPerCTN" : [
			"Charges_per_CTN_0"
		],
		
		"totalDeviceCharges" : [
			"Total_Device_Charges_0"
		],
		
		"tenure" : "Tenure_0",
		
		"numberOfUsers" : "Total_number_of_users_0"
	},
	
	"User Group SME" : {
	"totalCharge" : [
			"Total_Recurring_Charges_0"
		],
		
		"totalInvestmentPot" : [
			
		],
		
		"chargesPerCTN" : [
			
		],
		
		"totalDeviceCharges" : [
			"Total_Device_Charges_0"
		],
		
		"tenure" : "Tenure_0",
		
		"numberOfUsers" : ""
	}, 
	
	"Single Add On SME" : {
		"totalCharge" : [
			"Total_Recurring_Charges_0"
		],
		
		"totalInvestmentPot" : [
			
		],
		
		"chargesPerCTN" : [
			
		],
		
		"totalDeviceCharges" : [
			
		],
		
		"tenure" : "Tenure_0",
		
		"numberOfUsers" : ""
		
	},
	
	"Shared Add On SME" : {
		"totalCharge" : [
			"Total_Recurring_Charges_0"
		],
		
		"totalInvestmentPot" : [
		    "Add_Ons_Investment_Pot_0"
		],
		
		"chargesPerCTN" : [
			
		],
		
		"totalDeviceCharges" : [
			
		],
		
		"tenure" : "Tenure_0",
		
		"numberOfUsers" : ""
	},
	
	"Mobile Broadband and Tablet" : {
		"totalCharge" : [
			"Total_Charges_After_Discount_0"
		],
		
		"totalInvestmentPot" : [
			"Investment_Pot_0"
		],
		
		"chargesPerCTN" : [
			"Charges_per_CTN_0"
		],
		
		"totalDeviceCharges" : [
			"Total_Device_Charges_0"
		],
		
		"tenure" : "Tenure_0",
		
		"numberOfUsers" : "Total_number_of_users_0"
	}
}