let cloudVoicePlugin;
if (CS && CS.SM && CS.SM.createPlugin) {
    cloudVoicePlugin = CS.SM.createPlugin("Cloud Voice");
}



cloudVoicePlugin.afterAttributeUpdated= async function(componentName, guid, attribute, oldValue, newValue){
    console.log('CloudVoice attribute updated ',attribute.name,componentName);
}