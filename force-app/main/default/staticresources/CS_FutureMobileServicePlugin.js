var imported = document.createElement('script');
imported.src = '/resource/1491499237000/cscfga__jQuery_min';
document.head.appendChild(imported);

var exceljspolyfill = document.createElement('script');
exceljspolyfill.src = 'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.26.0/polyfill.js';
document.head.appendChild(exceljspolyfill);

var exceljs = document.createElement('script');
exceljs.src = 'https://cdn.jsdelivr.net/npm/exceljs@1.13.0/dist/exceljs.min.js';
document.head.appendChild(exceljs);

var filesaver = document.createElement('script');
filesaver.src = 'https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.js';
document.head.appendChild(filesaver);

var globalSPAddonMap = {};
var globalUGAddonMap = {};
var globalOneOffCostMap = {};
var globalRecurringCostMap = {};

var usageProfiledata = {};
var correctProfile = null;
var globalAccountID = '';

var customCallerAddOnArray = {
	'UK Landlines': 'FMUKLandlines',
	'Calls to EE': 'FMCallstoEE',
	'Calls to other UK mobile networks': 'FMCallstootherUKmobilenetworks',
	'IDD Zone A': 'FMIDDSMSZoneA',
	'IDD Zone B': 'FMIDDSMSZoneB',
	'IDD Zone C': 'FMIDDSMSZoneC',
	'Non-Geographic Calls': 'FMNonGeographicCalls',
	'SMS to EE': 'FMSMStoEE',
	'SMS to other network': 'FMSMStoothernetwork',
	'Voice calls to Zone A': 'FMVoicecallstoZoneA',
	'Voice calls to Zone B': 'FMVoicecallstoZoneB',
	'Voice calls to Zone C': 'FMVoicecallstoZoneC',
	'Voice calls back to UK from Zone A': 'FMVoicecallsbacktoUKfromZoneA',
	'Voice calls back to UK from Zone B': 'FMVoicecallsbacktoUKfromZoneB',
	'Voice calls back to UK from Zone C': 'FMVoicecallsbacktoUKfromZoneC',
	'Receiving call in Zone A': 'FMReceivingcallinZoneA',
	'Receiving call in Zone B': 'FMReceivingcallinZoneB',
	'Receiving call in Zone C': 'FMReceivingcallinZoneC',
	'MMS Messaging': 'FMMMSMessaging'
};

console.log('------------>Loaded default plugin for Future Mobile New');

if (!CS || !CS.SM) {
	throw Error('FM Solution Console Api not loaded?');
}

if (!CS || !CS.SM) {
	console.log('not loaded');
	throw Error('FM Solution Console Api not loaded?');
}

// Create a mapping to match your solution's components
// This means we can avoid too much hard-coding below and can more easily reuse code
var FUTURE_MOBILE_CONST = {
	solution: 'Future Mobile Service', //Main SCHEMA name for plugin creation
	//solution: 'Service Plan and Solutions', //Main SCHEMA name for plugin creation
	mainComponent: 'Service Plan and Solutions',
	userGroups: 'User Groups', //Component name for use in functions//FUTURE_MOBILE_CONST.userGroups
	creditFund: 'Funds',
	special: 'Special Conditions',
	sharedVAS: 'Shared VAS',
	bespokeSolution: 'Bespoke Coverage Solution'
};

//Register the Cloud Voice Plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', async function() {



		await CS.SM.registerPlugin(FUTURE_MOBILE_CONST.solution)
			.then(plugin => {
				console.log("--------->Plugin registered for Future Mobile Service New");
				futureMobileHooks(plugin);
			});
	});
}


//OE Event Listener
window.document.addEventListener('OrderEnrichmentTabLoaded', async function(e) {
	console.log('OrderEnrichmentTabLoaded', e);

	console.log('OE Name : ', e.detail.orderEnrichment.name);
	if (e.detail.orderEnrichment.name == 'User Subscription Details') {
		setOEData();
	}
});


async function setOEData() {
	var solution = await CS.SM.getActiveSolution();
	console.log('OrderEnrichmentTabLoaded Solution : ', solution);
	var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	console.log('OrderEnrichmentTabLoaded Service Plan Configuration', servicePlanConfiguration);
}

function futureMobileHooks(FMPlugin) {
	var newValue;

	FMPlugin.beforeSave = async function(solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
		console.log('beforeSave');
		var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
		servicePlan = servicePlanConfiguration.getAttribute('Service Plan').displayValue;
		customDataServicePlan = servicePlanConfiguration.getAttribute('Custom Data Service Plan').displayValue;

		if (servicePlan != 'The Team Plan') {
			if (servicePlanConfiguration.getAttribute('Team Voice Plan').displayValue == 'N/A') {
				updateConfigurationAttributeValue('Team Voice Plan', '', '', true, solution, servicePlanConfiguration.guid, true);
			}
			if (servicePlanConfiguration.getAttribute('Team Voice Scheme').displayValue == 'N/A') {
				updateConfigurationAttributeValue('Team Voice Scheme', '', '', true, solution, servicePlanConfiguration.guid, true);
			}
		}
		if (servicePlan != 'The Team Plan' && customDataServicePlan != 'Team') {
			if (servicePlanConfiguration.getAttribute('Team Data Plan').displayValue == 'N/A') {
				updateConfigurationAttributeValue('Team Data Plan', '', '', true, solution, servicePlanConfiguration.guid, true);
			}
			if (servicePlanConfiguration.getAttribute('Team Data Scheme').displayValue == 'N/A') {
				updateConfigurationAttributeValue('Team Data Scheme', '', '', true, solution, servicePlanConfiguration.guid, true);
			}
		}

		checkDevices(solution, null);
		calculateUsersCount(solution);
		calculateTotalCharges(solution, servicePlanConfiguration);
		//await autoAddDataCollectorVAS(solution, servicePlanConfiguration.getAttribute('Add Data Collector').displayValue);
		//await autoAddTotalResourceVAS(solution, servicePlanConfiguration.getAttribute('Add Total Resource').displayValue);
		
		for (let component of Object.values(solution.components)) {
			for (let configuration of Object.values(component.schema.configurations)) {
				if (configuration.error == true || configuration.status == false) {
					CS.SM.displayMessage('Invalid configurations in the basket', 'error');
					return Promise.resolve(false);
				}
			}
		}
		var isInValid = validateUGConfigurations(solution, servicePlan, customDataServicePlan);
		if (isInValid) {
		    CS.SM.displayMessage('Invalid User Group configurations in the basket', 'error');
			return Promise.resolve(false);
		}
        
		return true;
	};
	FMPlugin.afterSave = async function(solutionResult, solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
		console.log('afterSave', solution, configurationsProcessed, saveOnlyAttachment, configurationGuids);

		let ASolution = await CS.SM.getActiveSolution();
		let currentBasket = await CS.SM.getActiveBasket();
		let basketId = await CS.SM.getActiveBasketId();

		var servicePlanConfiguration = ASolution.getConfiguration(Object.keys(ASolution.schema.configurations)[0]);
		servicePlan = servicePlanConfiguration.getAttribute('Service Plan').displayValue;
		customDataServicePlan = servicePlanConfiguration.getAttribute('Custom Data Service Plan').displayValue;

		if (servicePlan != 'The Team Plan') {
			if (servicePlanConfiguration.getAttribute('Team Voice Plan').displayValue == '') {
				updateConfigurationAttributeValue('Team Voice Plan', 'N/A', 'N/A', true, ASolution, servicePlanConfiguration.guid, true);
			}
			if (servicePlanConfiguration.getAttribute('Team Voice Scheme').displayValue == '') {
				updateConfigurationAttributeValue('Team Voice Scheme', 'N/A', 'N/A', true, ASolution, servicePlanConfiguration.guid, true);
			}
		}
		if (servicePlan != 'The Team Plan' && customDataServicePlan != 'Team') {
			if (servicePlanConfiguration.getAttribute('Team Data Plan').displayValue == '') {
				updateConfigurationAttributeValue('Team Data Plan', 'N/A', 'N/A', true, ASolution, servicePlanConfiguration.guid, true);
			}
			if (servicePlanConfiguration.getAttribute('Team Data Scheme').displayValue == '') {
				updateConfigurationAttributeValue('Team Data Scheme', 'N/A', 'N/A', true, ASolution, servicePlanConfiguration.guid, true);
			}
		}
		
		calculateTotalCharges(ASolution, servicePlanConfiguration);

		reloadUIafterSolutionLoaded(ASolution);
		disableFCC(ASolution)

		await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
			"configId": servicePlanConfiguration.id,
			'action': 'CS_SaveTSV',
			'Total Solution Value': ASolution.totalContractValue,
			'Total One Off Value': ASolution.totalOneOffCharge,
			'Total Recurring Value': ASolution.totalRecurringCharge
		});
		await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
			"basketId": basketId,
			'action': 'CS_SolutionConsoleBasketCalculation'
		});
		
		/*var noOfUsers = Number(servicePlanConfiguration.getAttribute('Total Number of Users').value);
		var completeMobilityConfig = false;
		var minConnectionsOk = false;
		var maxConnectionsOk = false;
	    for (let relProduct of Object.values(servicePlanConfiguration.relatedProducts)) {
			if (relProduct.name == FUTURE_MOBILE_CONST.sharedVAS) {
				for (let attribute of Object.values(relProduct.configuration.attributes)) {
    			    if (attribute.name == 'Group Name' && attribute.value == 'Complete Mobility') {
				        completeMobilityConfig = true;
				    }
				    if (attribute.name == 'Minimum Connections' && noOfUsers >= attribute.value) {
				        minConnectionsOk = true;
				    }
				    if (attribute.name == 'Maximum Connections' && noOfUsers <= attribute.value) {
				        maxConnectionsOk = true;
				    }
    			}
    			if (completeMobilityConfig && (!minConnectionsOk || !maxConnectionsOk)) {
			        CS.SM.displayMessage('VAS band doesnot match the Total Number of Users', 'error');
				    return Promise.resolve(false);
			    }
			}
		}*/
		return true;
	};

	FMPlugin.afterSolutionLoaded = async function(previousSolution, loadedSolution) {
		console.log('AFTER SOL LOADED', loadedSolution);
		for (let component of Object.values(loadedSolution.components)) {
			if (component.name == FUTURE_MOBILE_CONST.special) {
				if (Object.values(component.schema.configurations).length < 1) {
					let configuration = component.createConfiguration();
					component.addConfiguration(configuration).then(addedConfiguration => console.log('Special Conditions Added', addedConfiguration));
				}
			}

			if (component.name == FUTURE_MOBILE_CONST.userGroups) {
				let fccValidationPassed = true;
				for (let configuration of Object.values(component.schema.configurations)) {
					calculateTotalUserSetupCharges(loadedSolution, configuration, null, null, null);

					if (fccValidationPassed) {
						fccValidationPassed = validateFCC(loadedSolution, configuration);
					}

				}
					
				await calculateMaxFCCAllUG(loadedSolution, component);
			}
		}
		
		reloadUIafterSolutionLoaded(loadedSolution);
		reloadUIafterSolutionLoadedFM(loadedSolution);
		disableFCC(loadedSolution);
	}

	FMPlugin.beforeAttributeValidation = function(component, configuration, attribute, value) {
	    console.log('beforeAttributeValidation', component, configuration, attribute, value);
		if (component.name == FUTURE_MOBILE_CONST.mainComponent) {
			if (attribute.name == 'Contract Term' && configuration.parentConfiguration == '') {
				let badResult = (isNaN(Number(value.value)) || (value.value) < 12 || Number(value.value) > 60);
				if (badResult) {
					return Promise.resolve({
						error: true,
						message: "Enter a valid contract term"
					});
				} else {
					return Promise.resolve({
						error: false,
						message: null
					});
				}
			}
		}

		/*if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			if (attribute.name == 'FCC' && value.value != 'N/A' && value.value != null) {
				if (parseFloat(value.value) > parseFloat(configuration.getAttribute('Maximum FCC').value)) {
					return Promise.resolve({
						error: true,
						message: "FCC entered is more than maximum value allowed"
					});
				} else {
					attribute.errorMessage = null;
					configuration.status = true;
					configuration.statusMessage = null;
					return Promise.resolve({
						error: false,
						message: null
					});
				}
			}
		}*/
	}

	FMPlugin.beforeAttributeUpdated = function(component, configuration, attribute, oldValueMap) {
		return Promise.resolve(true);
	};

	FMPlugin.afterAttributeUpdated = async function(component, configuration, attribute, oldValueMap) {
		console.log('FMPlugin.afterAttributeUpdated', component, configuration, attribute, oldValueMap);
		var solution = await CS.SM.getActiveSolution();
		var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);

		if (component.name == FUTURE_MOBILE_CONST.userGroups) {

			if (attribute.name == 'eSIM Required Picklist') {
				if (attribute.value == null || attribute.value == '') {
					return;
				}
				var fliterValue;
				
				if (attribute.value == 'Yes') {
					fliterValue = 'eSIM';
				} else {
					fliterValue = 'eNoSIM';
				}
				updateDataeSIMFilter = [{
					name: 'eSIM Filter',
					value: fliterValue,
					displayValue: fliterValue,
					readOnly: true,
					required: false
				}];
                await component.updateConfigurationAttribute(configuration.guid, updateDataeSIMFilter, false);
                
				var subscriptionTypeName = configuration.getAttribute('Subscription Type Name').value;
                updateData = [{
					name: 'Subscription Type Name',
					value: '',
					displayValue: '',
					readOnly: true,
					required: false
                }]; 
                await component.updateConfigurationAttribute(configuration.guid, updateData, true);

                updateData = [{
					name: 'Subscription Type Name',
					value: subscriptionTypeName,
					displayValue: subscriptionTypeName,
					readOnly: true,
					required: false
                }]; 
                await component.updateConfigurationAttribute(configuration.guid, updateData, false);
			}

			if (attribute.name == 'Contract Term') {
				calculateMaxFCC(configuration, solution, component);
			}

			if (attribute.name == 'FCC') {
				validateTRGAndTCG(solution);
			}

			if (attribute.name == 'Subscription Type') {
				updateData = [{
					name: 'Device Type',
					value: null,
					displayValue: null,
					readOnly: false,
					required: true
				}];
				await component.updateConfigurationAttribute(configuration.guid, updateData, false);

				updateData = [{
					name: 'eSIM Required Picklist',
					value: null,
					displayValue: null,
					readOnly: false,
					required: true
				}];
				await component.updateConfigurationAttribute(configuration.guid, updateData, false);

				updateData = [{
					name: 'Subscription Type Name',
					value: attribute.displayValue,
					displayValue: attribute.displayValue,
					readOnly: true,
					required: false
				}];
				await component.updateConfigurationAttribute(configuration.guid, updateData, false);
				updateConfigNameFM(configuration, component.name, solution);

				if (attribute.displayValue == 'Data Only' || attribute.displayValue == null) {
					updateData = [{
						name: 'FCC',
						value: 0.0,
						displayValue: 0.0,
						readOnly: true,
						required: false
					}];
					await component.updateConfigurationAttribute(configuration.guid, updateData, false);
				} else {
					updateData = [{
						name: 'FCC',
						value: null,
						displayValue: null,
						readOnly: false,
						required: false
					}];
					await component.updateConfigurationAttribute(configuration.guid, updateData, false);
				}
			}

			if (attribute.name == 'Individual Override') {
				if (attribute.value == true) {
					updateData = [{
						name: 'Service Plan Name',
						value: 'Individual',
						displayValue: 'Individual',
						readOnly: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					await updateUserGroupConfig(solution);
					await autoAddSIM(solution, configuration);
					await calculateAndAddUserGroupAddOnForTeam(solution, component, configuration, attribute.value);
					await autoAddSharedAccessFee(solution, configuration);
					updateConfigurationAttributeValue('Voice Scheme Discount', 0, 0, true, solution, configuration.guid, true);
					updateConfigurationAttributeValue('Data Scheme Discount', 0, 0, true, solution, configuration.guid, true);
				} else {
					currentContractTerm = servicePlanConfiguration.getAttribute('Contract Term').displayValue;
					updateData = [{
						name: 'Contract Term',
						value: '+',
						displayValue: '+',
						readOnly: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
					updateData = [{
						name: 'Contract Term',
						value: currentContractTerm,
						displayValue: currentContractTerm,
						readOnly: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
					await updateUserGroupConfig(solution);
					await autoAddSIM(solution, configuration);
					await autoAddSharedAccessFee(solution, configuration);
					await calculateAndAddUserGroupAddOnForTeam(solution, component, configuration, attribute.value);
					var voiceSchemeDiscount = servicePlanConfiguration.getAttribute('Voice Scheme Discount').value;
			        var dataSchemeDiscount = servicePlanConfiguration.getAttribute('Data Scheme Discount').value;
			        updateConfigurationAttributeValue('Voice Scheme Discount', voiceSchemeDiscount, voiceSchemeDiscount, true, solution, configuration.guid, true);
			        updateConfigurationAttributeValue('Data Scheme Discount', dataSchemeDiscount, dataSchemeDiscount, true, solution, configuration.guid, true);
				}
			}

			if (attribute.name == 'Subscription Type Name') {
				await autoAddSIM(solution, configuration);
				await autoAddSharedAccessFee(solution, configuration);
				await calculateAndAddUserGroupAddOnForTeam(solution, component, configuration, attribute.value);
			}
			if (attribute.name == 'Group Name' || attribute.name == 'Service Plan Name') {
				updateConfigNameFM(configuration, component.name, solution);
			}

			if (attribute.name == 'Device Type') {
				var fliterValue;

				updateData = [{
					name: 'eSIM Required Picklist',
					value: null,
					displayValue: null,
					readOnly: false,
					required: true
				}];
				await component.updateConfigurationAttribute(configuration.guid, updateData, false);

				if (attribute.value == 'SIM Only') {
					fliterValue = 'SIM Only';
				}
				if (attribute.value == 'Device Only') {
					fliterValue = 'DEVICE ONLY';
				}
				if (attribute.value == 'Device with SIM') {
					fliterValue = 'DEVICE SIM';
				}
				updateDataDeviceFilter = [{
					name: 'Device Filter',
					value: fliterValue,
					displayValue: fliterValue,
					readOnly: true,
					required: false
				}];
				await component.updateConfigurationAttribute(configuration.guid, updateDataDeviceFilter, false);
                
                var subscriptionTypeName = configuration.getAttribute('Subscription Type Name').value;
                updateData = [{
					name: 'Subscription Type Name',
					value: '',
					displayValue: '',
					readOnly: true,
					required: false
                }]; 
                await component.updateConfigurationAttribute(configuration.guid, updateData, true);

                updateData = [{
					name: 'Subscription Type Name',
					value: subscriptionTypeName,
					displayValue: subscriptionTypeName,
					readOnly: true,
					required: false
                }]; 
                await component.updateConfigurationAttribute(configuration.guid, updateData, false);
			}

			if (attribute.name == 'Voice Scheme Discount' || attribute.name == 'Data Scheme Discount') {
				calculateTotalUserSetupCharges(solution, configuration, attribute.value, oldValueMap.value, attribute.name);
			}
		}

		if (component.name == FUTURE_MOBILE_CONST.mainComponent) {
			if (attribute.name == 'Service Plan Name') {
				await updateUserGroupConfig(solution);
			}

			/*if (attribute.name == 'Team Voice Scheme' || attribute.name == 'Team Data Scheme') {
				for (let componentLoop of Object.values(solution.components)) {
					if (componentLoop.name == FUTURE_MOBILE_CONST.userGroups) {
						await calculateAndAddUserGroupAddOnForTeam(solution, componentLoop, null, '');
					}
				}
			}*/
			
			

			if (attribute.name == 'Service Plan') {
				if (attribute.displayValue == 'Custom Caller') {
					updateData = [{
						name: 'Custom Data Service Plan',
						value: '',
						displayValue: '',
						readOnly: false,
						required: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Custom Voice Rate Card',
						value: 'Click',
						displayValue: 'Click',
						readOnly: false,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				} else {
					updateData = [{
						name: 'Custom Data Service Plan',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Custom Voice Rate Card',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				}
				if (attribute.displayValue == 'The Team Plan') {
					updateData = [{
						name: 'Team Voice Plan',
						value: '',
						displayValue: '',
						readOnly: false,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Data Plan',
						value: '',
						displayValue: '',
						readOnly: false,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Voice Scheme',
						value: '',
						displayValue: '',
						readOnly: false,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Data Scheme',
						value: '',
						displayValue: '',
						readOnly: false,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Variant',
						value: '',
						displayValue: '',
						readOnly: false,
						required: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
				} else {
					updateData = [{
						name: 'Team Voice Plan',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Data Plan',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Voice Scheme',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Data Scheme',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Variant',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
				}

				updateConfigNameFM(configuration, component.name, solution);
				if (attribute.displayValue == 'The Shared Plan' || attribute.displayValue == 'Custom Caller') {
					var planExists = false;
					for (let relatedProduct of Object.values(configuration.relatedProductList)) {
						if (relatedProduct.relatedProductName == 'Service Plans') {
							planExists = true;
						}
					}
					if (!planExists) {
						for (let componentLoop of Object.values(solution.components)) {
							if (componentLoop.name == FUTURE_MOBILE_CONST.userGroups) {
								if (Object.values(componentLoop.schema.configurations).length > 0) {
									CS.SM.displayMessage('User Group configurations have been removed', 'info');
								}
								for (let configuration of Object.values(componentLoop.schema.configurations)) {
									componentLoop.deleteConfiguration(configuration.guid);
								}
							}
						}
					}
				}
				await autoAddSharedAccessFee(solution, null);
				/*for (let relProduct of Object.values(configuration.relatedProducts)) {
					if (relProduct.name == FUTURE_MOBILE_CONST.sharedVAS) {
						updateConfigurationAttributeValue(attribute.name, attribute.value, attribute.value, true, component, relProduct.configuration.guid, false);
						updateConfigurationAttributeValue('Shared VAS', '', '', false, component, relProduct.configuration.guid, false);
					}
				}*/
			}
			if (attribute.name == 'Voice Scheme Discount') {
		        for (let componentLoop of Object.values(solution.components)) {
					if (componentLoop.name == FUTURE_MOBILE_CONST.userGroups) {
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
						    var individualOverride = configLoop.getAttribute('Individual Override').value;
						    if (individualOverride == true) {
						        updateData = [{
    								name: 'Voice Scheme Discount',
    								value: 0,
    								displayValue: 0,
    								readOnly: true
    							}];
    							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						    } else {
    							updateData = [{
    								name: 'Voice Scheme Discount',
    								value: attribute.value,
    								displayValue: attribute.value,
    								readOnly: true
    							}];
    							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						    }
						}
					}
		        }
			}
			if (attribute.name == 'Data Scheme Discount') {
			    for (let componentLoop of Object.values(solution.components)) {
					if (componentLoop.name == FUTURE_MOBILE_CONST.userGroups) {
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
						    var individualOverride = configLoop.getAttribute('Individual Override').value;
						    if (individualOverride == true) {
						        updateData = [{
    								name: 'Data Scheme Discount',
    								value: 0,
    								displayValue: 0,
    								readOnly: true
    							}];
    							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						    } else {
    							updateData = [{
    								name: 'Data Scheme Discount',
    								value: attribute.value,
    								displayValue: attribute.value,
    								readOnly: true
    							}];
    							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						    }
						}
					}
			    }
			}
			if (attribute.name == 'Contract Term' && configuration.name.includes('Future Mobile Service')) {
				updateConfigNameFM(configuration, component.name, solution);
                var currentContractTerm = configuration.getAttribute('Contract Term').value;
				for (let componentLoop of Object.values(solution.components)) {
					if (componentLoop.name == FUTURE_MOBILE_CONST.userGroups) {
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
							updateData = [{
								name: 'Contract Term',
								value: currentContractTerm,
								displayValue: currentContractTerm,
								readOnly: true,
								required: false
							}];
							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						}
					}
					if (componentLoop.name == FUTURE_MOBILE_CONST.creditFund) {
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
							updateData = [{
								name: 'Contract Term',
								value: currentContractTerm,
								displayValue: currentContractTerm,
								readOnly: true,
								required: false
							}];
							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						}
					}
					if (componentLoop.name == FUTURE_MOBILE_CONST.special) {
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
							updateData = [{
								name: 'Tenure',
								value: currentContractTerm,
								displayValue: currentContractTerm,
								readOnly: true,
								required: false
							}];
							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						}
					}
				}
			}
			if (attribute.name == 'Custom Data Service Plan') {
				var servicePlan = configuration.getAttribute('Service Plan').displayValue;

				for (let componentLoop of Object.values(solution.components)) {
					if (componentLoop.name == FUTURE_MOBILE_CONST.userGroups) {
						if (Object.values(componentLoop.schema.configurations).length > 0) {
							CS.SM.displayMessage('User Group configurations have been removed', 'info');
						}
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
							componentLoop.deleteConfiguration(configLoop.guid);
						}
					}
				}
				if (attribute.value != 'N/A') {
					updateData = [{
						name: 'Custom Data Plan Name',
						value: attribute.value,
						displayValue: attribute.value,
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				}
				if (attribute.displayValue == 'Team' || servicePlan == 'The Team Plan') {
					updateData = [{
						name: 'Team Data Plan',
						value: '',
						displayValue: '',
						readOnly: false,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Data Scheme',
						value: '',
						displayValue: '',
						readOnly: false,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Variant',
						value: '',
						displayValue: '',
						readOnly: false,
						required: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				} else {
					updateData = [{
						name: 'Team Data Plan',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Team Data Scheme',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
					updateData = [{
						name: 'Variant',
						value: 'N/A',
						displayValue: 'N/A',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				}


				if (attribute.name == 'Custom Data Plan Name') {
					//await addCustomCallerPlans(solution,component,configuration,'UK Landlines');
				}
            }
			var servicePlan = servicePlanConfiguration.getAttribute('Service Plan').displayValue;
			if (attribute.name == 'Team Voice Plan') {
				if (servicePlan == 'The Custom Caller' || servicePlan == 'The Team Plan') {
					var otherAttr = servicePlanConfiguration.getAttribute('Team Data Plan');
					if ((attribute.displayValue == '' || attribute.displayValue == 'N/A') || (otherAttr.displayValue == '' || otherAttr.displayValue == 'N/A')) {
						for (let component of Object.values(solution.components)) {
							if (component.name == FUTURE_MOBILE_CONST.userGroups) {
								if (Object.values(component.schema.configurations).length > 0) {
									CS.SM.displayMessage('User Group configurations have been removed', 'info');
								}
								for (let configuration of Object.values(component.schema.configurations)) {
									component.deleteConfiguration(configuration.guid);
								}
							}
						}
					}
				}
				if (attribute.displayValue == 'Team Unlimited Minutes' || attribute.displayValue == 'N/A') {
					updateConfigurationAttributeValue('Team Voice Scheme', 'N/A', 'N/A', true, solution, configuration.guid, false);
				} else {
					updateConfigurationAttributeValue('Team Voice Scheme', '', '', false, solution, configuration.guid, false);
				}
			}
			if (attribute.name == 'Team Data Plan') {
				if (servicePlan == 'The Custom Caller' || servicePlan == 'The Team Plan') {
					var otherAttr = servicePlanConfiguration.getAttribute('Team Voice Plan');
					if ((attribute.displayValue == '' || attribute.displayValue == 'N/A') || (otherAttr.displayValue == '' || otherAttr.displayValue == 'N/A')) {
						for (let component of Object.values(solution.components)) {
							if (component.name == FUTURE_MOBILE_CONST.userGroups) {
								if (Object.values(component.schema.configurations).length > 0) {
									CS.SM.displayMessage('User Group configurations have been removed', 'info');
								}
								for (let configuration of Object.values(component.schema.configurations)) {
									component.deleteConfiguration(configuration.guid);
								}
							}
						}
					}
				}
			}
			/*if (attribute.name == 'Add Data Collector') {
				await autoAddDataCollectorVAS(solution, attribute.value);
			}
			if (attribute.name == 'Add Total Resource') {
				await autoAddTotalResourceVAS(solution, attribute.value);
				for (let relProduct of Object.values(configuration.relatedProducts)) {
					if (relProduct.name == FUTURE_MOBILE_CONST.sharedVAS) {
						updateConfigurationAttributeValue('Add Total Resource', attribute.value, attribute.value, true, component, relProduct.configuration.guid, false);
					}
				}
			}*/

			if (attribute.name == 'Paper Billing Required') {
				//this function auto adds an addon, for now it is not needed
				//await autoAddAccountBill(solution,attribute.value);
				await autoAddAccountBillVAS(solution, attribute.value);
			}

			if (attribute.name == 'Connection Type') {
				//await autoAddSIM(solution, null);
			}

			if (attribute.name == 'Managed MDM' || attribute.name == 'Mobile Iron Cloud') {
				//validateManagedMDM(configuration);
				/*for (let relProduct of Object.values(configuration.relatedProducts)) {
					if (relProduct.name == FUTURE_MOBILE_CONST.sharedVAS) {
						updateConfigurationAttributeValue(attribute.name, attribute.value, attribute.value, true, component, relProduct.configuration.guid, false);
					}
				}*/
			}

			if (attribute.name == 'Total Number of Users' && configuration.parentConfiguration == '') {
			    for (let componentLoop of Object.values(solution.components)) {
					if (componentLoop.name == FUTURE_MOBILE_CONST.special) {
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
							updateData = [{
								name: 'Number of Users',
								value: attribute.value,
								displayValue: attribute.value,
								readOnly: true,
								required: false
							}];
							componentLoop.updateConfigurationAttribute(configLoop.guid, updateData, false);
						}
					}
					
					//Added for sharedVAS
					if (componentLoop.name == FUTURE_MOBILE_CONST.sharedVAS) {
						for (let configLoop of Object.values(componentLoop.schema.configurations)) {
							var minUserNumber = configLoop.getAttribute('Min Number of Users').value;
							var maxUserNumber = configLoop.getAttribute('Max Number of Users').value;
							var currentUserNumber = attribute.value;
							var groupSelected = configLoop.getAttribute('Group').displayValue;
							var productListIncludedForUsers = ['Complete Mobility', 'Tailored Set Up', 'Tailored Service', 'Total Resource', 'Data Collector'];
							if (minUserNumber != '' && minUserNumber != null && maxUserNumber != '' && maxUserNumber != null && productListIncludedForUsers.includes(groupSelected)) {
								if (currentUserNumber < minUserNumber || currentUserNumber > maxUserNumber) {
									configLoop.status = false;
									if (configLoop.statusMessage != null && configLoop.statusMessage != '') {
										if (!configLoop.statusMessage.includes('Product selected should be between')) {
											configLoop.statusMessage += ' || Product selected should be between ' + minUserNumber + ' and ' + maxUserNumber + ' Users.';
										}
									} else {
										configLoop.statusMessage = 'Product selected should be between ' + minUserNumber + ' and ' + maxUserNumber + ' Users.';
									}
								} else {
									if (configLoop.statusMessage != null && configLoop.statusMessage != '' && configLoop.statusMessage.includes(' || ')) {
										var messageSplited = configLoop.statusMessage.split(' || ');
										for (let i = 0; i < messageSplited.length; i++) {
											if (messageSplited[i].includes('Product selected should be between')) {
												continue;
											} else {
												configLoop.statusMessage = messageSplited[i];
											}
										}
									} else if (configLoop.statusMessage != null && configLoop.statusMessage != '') {
										if (configLoop.statusMessage.includes('Product selected should be between')) {
											configLoop.statusMessage = '';
										} 
									}
								}
								if (configLoop.statusMessage == null || configLoop.statusMessage == '') {
									configLoop.status = true;
									configLoop.errorMessage = null;
								}
							} else {
								if (configLoop.statusMessage != null && configLoop.statusMessage != '' && configLoop.statusMessage.includes(' || ')) {
									var messageSplited = configLoop.statusMessage.split(' || ');
									for (let i = 0; i < messageSplited.length; i++) {
										if (messageSplited[i].includes('Product selected should be between')) {
											continue;
										} else {
											configLoop.statusMessage = messageSplited[i];
										}
									}
								} else if (configLoop.statusMessage != null && configLoop.statusMessage != '') {
									if (configLoop.statusMessage.includes('Product selected should be between')) {
										configLoop.statusMessage = '';
									} 
								}
								if (configLoop.statusMessage == null || configLoop.statusMessage == '') {
									configLoop.status = true;
									configLoop.errorMessage = null;
								}
							}
						}
					}
				}
				numbersCanBeDisconnected(solution);
			}
			if (attribute.name == 'Group' && attribute.type == 'Lookup') {
				updateConfigurationAttributeValue('Shared VAS', '', '', false, component, configuration.guid, false);
			}
			if (attribute.name == 'Product Name') {
			    await validateWanderaProduct(solution, configuration);
		        await validatePerfTrackerDivProduct(solution, configuration);
			}
			if (attribute.name == 'Contract Type') {
			    if (attribute.value == 'No Term') {
				    updateConfigurationAttributeValue('Contract Term', 1, 1, true, component, configuration.guid, false);
			    }
				/*else if (attribute.value == 'MCP') {
				    updateConfigurationAttributeValue('Contract Term', 24, 24, true, component, configuration.guid, false);
				}*/
				else if (attribute.value == 'MAT') {
				    var servicePlanContractTerm = servicePlanConfiguration.getAttribute('Contract Term').displayValue;
				    updateConfigurationAttributeValue('Contract Term', servicePlanContractTerm, servicePlanContractTerm, true, component, configuration.guid, false);
				}
			}
			if (attribute.name == 'Contract Term' && configuration.parentConfiguration != '') {
                await validateSharedVASContractTerm(solution, configuration);
			}
			if (attribute.name == 'Product Billing Frequency') {
			    if (attribute.value == 'Annually') {
			        updateConfigurationAttributeValue('Bill Frequency', 'Annual', 'Annual', true, component, configuration.guid, false);
			    } else {
			        updateConfigurationAttributeValue('Bill Frequency', 'Monthly', 'Monthly', true, component, configuration.guid, false);
			    }
			}
			if (attribute.name == 'Quantity') {
			    await validateWanderaProduct(solution, configuration);
			}
			if (attribute.name == 'Recurring Charge') {
			    if (attribute.value == 0 || attribute.value == undefined) {
			        updateConfigurationAttributeValue('Bill Frequency', 'One Off', 'One Off', true, component, configuration.guid, false);
			    } else {
			        updateConfigurationAttributeValue('Bill Frequency', 'Monthly', 'Monthly', true, component, configuration.guid, false);
			    }
			}
			
			if (attribute.name == 'Tariff Type') {
			    if (attribute.value == 'Recurring') {
			        updateConfigurationAttributeValue('One Off Charge', 0, 0, true, component, configuration.guid, false);
			        updateConfigurationAttributeValue('One Off Cost', 0, 0, true, component, configuration.guid, false);
			        updateConfigurationAttributeValue('Annual Recurring Charge', 0, 0, false, component, configuration.guid, false);
			        updateConfigurationAttributeValue('Annual Recurring Cost', 0, 0, false, component, configuration.guid, false);
			    } else {
			        updateConfigurationAttributeValue('One Off Charge', 0, 0, false, component, configuration.guid, false);
			        updateConfigurationAttributeValue('One Off Cost', 0, 0, false, component, configuration.guid, false);
			        updateConfigurationAttributeValue('Annual Recurring Charge', 0, 0, true, component, configuration.guid, false);
			        updateConfigurationAttributeValue('Annual Recurring Cost', 0, 0, true, component, configuration.guid, false);
			    }
			}
			
			if (attribute.name == 'One Off Charge' || attribute.name == 'One Off Cost') {
			    await validateOneOffChargeORCost(component, configuration, attribute.value);
			}
			
			if (attribute.name == 'Annual Recurring Charge' || attribute.name == 'Annual Recurring Cost') {
			    await validateAnnualRecurringChargeORCost(component, configuration, attribute.value);
			}
			
			if (attribute.name == 'Team Voice Scheme' || attribute.name == 'Team Data Scheme' || attribute.name == 'Team Voice Plan' || attribute.name == 'Team Data Plan') {
			    for (let componentLoop of Object.values(solution.components)) {
					if (componentLoop.name == FUTURE_MOBILE_CONST.userGroups) {
					    await updateUserGroupConfig(solution);
    					for (let configLoop of Object.values(componentLoop.schema.configurations)) {
    					
    					    await autoAddSIM(solution, configLoop);
        					await autoAddSharedAccessFee(solution, configLoop);
        					await calculateAndAddUserGroupAddOnForTeam(solution, componentLoop, configLoop, '');
        					for (let relatedProduct of Object.values(configLoop.relatedProductList)) {
        					    await calculateVoiceAndDataAddOnCharges(componentLoop, configLoop, relatedProduct, Object.values(solution.schema.configurations)[0]);
        					}
    					}
					}
			    }
			    calculateMaxFCCAllUG(solution, solution.getComponentByName('User Groups'));
			}
			   
			    
		}

		if (component.name == FUTURE_MOBILE_CONST.creditFund) {
			if (attribute.name == 'Credit Type') {
				updateConfigNameFM(configuration, component.name, solution);
				if (attribute.value == 'Hardware') {
					updateData = [{
						name: 'Staged or Rolling',
						value: '.',
						displayValue: '.',
						options: ['Staged']
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
					updateData = [{
						name: 'Staged or Rolling',
						value: 'Please select',
						displayValue: 'Please select',
						options: ['Staged']
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
				} else {
					updateData = [{
						name: 'Staged or Rolling',
						value: '.',
						displayValue: '.',
						options: ['Staged', 'Rolling']
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
					updateData = [{
						name: 'Staged or Rolling',
						value: 'Please select',
						displayValue: 'Please select',
						options: ['Staged', 'Rolling']
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
				}
				calculateCreditFundsTwo(solution);
			}
			if (attribute.name == 'Staged or Rolling') {
				checkTotalCreditFunds(configuration);
				updateConfigNameFM(configuration, component.name, solution);
				checkStagedCreditMonth(configuration);
				if (attribute.value == 'Staged') {
					updateData = [{
						name: 'Staged Credit Month',
						value: '',
						displayValue: '',
						readOnly: false,
						required: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				} else {
					updateData = [{
						name: 'Staged Credit Month',
						value: '',
						displayValue: '',
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				}
			}
			if (attribute.name == 'Credit Amount') {
				checkTotalCreditFunds(configuration);
				updateConfigNameFM(configuration, component.name, solution);
				calculateCreditFundsTwo(solution);

			}
			if (attribute.name == 'Staged Credit Month') {
				checkStagedCreditMonth(configuration);
			}
			if (attribute.name == 'Contract Term') {
				checkStagedCreditMonth(configuration);
			}
		}

		if (component.name == FUTURE_MOBILE_CONST.special) {
			if (attribute.name == 'Disconnection Allowance Percentage') {
				numbersCanBeDisconnected(solution);
			}
			if (attribute.name == 'Total Revenue Contract Picklist') {
				if (attribute.value == 'Yes') updateConfigurationAttributeValue('Total Revenue Contract', true, true, false, solution, configuration.guid, true);
				if (attribute.value == 'No') updateConfigurationAttributeValue('Total Revenue Contract', false, false, false, solution, configuration.guid, true);
				validateTRGAndTCG(solution);
				allowDisconnectAllowance(solution, configuration);
			}
			if (attribute.name == 'Total Revenue Guarantee Picklist') {
				if (attribute.value == 'Yes') updateConfigurationAttributeValue('Total Revenue Guarantee', true, true, false, solution, configuration.guid, true);
				if (attribute.value == 'No') updateConfigurationAttributeValue('Total Revenue Guarantee', false, false, false, solution, configuration.guid, true);
				validateTRGAndTCG(solution);
				allowDisconnectAllowance(solution, configuration);
			}
			if (attribute.name == 'Minimum Connection Period') {
				validateTRGAndTCG(solution);
			}
			if (attribute.name == 'Full Co Terminus Picklist') {
				if (attribute.value == 'Yes') updateConfigurationAttributeValue('Full Co Terminus', true, true, false, solution, configuration.guid, true);
				if (attribute.value == 'No') updateConfigurationAttributeValue('Full Co Terminus', false, false, false, solution, configuration.guid, true);
			}
			if (attribute.name == 'Averaging Co Terminus Picklist') {
				if (attribute.value == 'Yes') updateConfigurationAttributeValue('Averaging Co Terminus', true, true, false, solution, configuration.guid, true);
				if (attribute.value == 'No') updateConfigurationAttributeValue('Averaging Co Terminus', false, false, false, solution, configuration.guid, true);
			}
			if (attribute.name == 'Disconnect Allowance Picklist') {
				if (attribute.value == 'Yes') updateConfigurationAttributeValue('Disconnect Allowance', true, true, false, solution, configuration.guid, true);
				if (attribute.value == 'No') updateConfigurationAttributeValue('Disconnect Allowance', false, false, false, solution, configuration.guid, true);
			}
			if (attribute.name == 'Unlocking Fees Picklist') {
				if (attribute.value == 'Yes') updateConfigurationAttributeValue('Unlocking Fees', true, true, false, solution, configuration.guid, true);
				if (attribute.value == 'No') updateConfigurationAttributeValue('Unlocking Fees', false, false, false, solution, configuration.guid, true);
			}
			if (attribute.name == 'Competitive Clause Picklist') {
				if (attribute.value == 'Yes') updateConfigurationAttributeValue('Competitive Clause', true, true, false, solution, configuration.guid, true);
				if (attribute.value == 'No') updateConfigurationAttributeValue('Competitive Clause', false, false, false, solution, configuration.guid, true);
			}

			if (attribute.name == 'Total Revenue Contract Picklist' || attribute.name == 'Total Revenue Guarantee Picklist' || attribute.name == 'Averaging Co Terminus Picklist' || attribute.name == 'Full Co Terminus Picklist') {

				if (configuration.getAttribute('Total Revenue Contract Picklist').value == 'Yes' && configuration.getAttribute('Total Revenue Guarantee Picklist').value == 'Yes') {
					configuration.status = false;
					configuration.statusMessage = 'Total Contract and Revenue Guarantee can not be picked at the same time';
				} else {
					if (configuration.getAttribute('Averaging Co Terminus Picklist').value == 'Yes' && configuration.getAttribute('Full Co Terminus Picklist').value == 'Yes') {
						configuration.status = false;
						configuration.statusMessage = 'Averaging and Full Co-Terminus can not be picked at the same time';
					} else {
						if (configuration.statusMessage == 'Averaging and Full Co-Terminus can not be picked at the same time') {
							configuration.status = true;
							configuration.errorMessage = null;
						}
						if (configuration.statusMessage == 'Total Contract and Revenue Guarantee can not be picked at the same time') {
							configuration.status = true;
							configuration.errorMessage = null;
						}
					}
				}
			}
			
			if(attribute.name === 'Opt Out of Price Increases Picklist'){
			    console.log('Opt Out of Price Increases Picklist', attribute.value);
			    optOutPropagation(solution, configuration);
			}
		}

		//Added for sharedVAS
		if (component.name == FUTURE_MOBILE_CONST.sharedVAS) {
			if (attribute.name == 'Group') {
				updateData = [{
					name: 'Shared VAS',
					value: '',
					displayValue: '',
					readOnly: false,
					required: true
				}];
				component.updateConfigurationAttribute(configuration.guid, updateData, true);
				updateData = [{
					name: 'Quantity',
					value: '',
					displayValue: '',
					readOnly: false,
					required: true
				}];
				component.updateConfigurationAttribute(configuration.guid, updateData, true);
				updateData = [{
					name: 'Contract Term',
					value: '',
					displayValue: '',
					readOnly: false,
					required: true
				}];
				component.updateConfigurationAttribute(configuration.guid, updateData, true);

				configuration.statusMessage = '';
				configuration.status = true;
			}

			if (attribute.name == 'Shared VAS') {
				var minContractTerm = configuration.getAttribute('Min Contract Term').value;
				var maxContractTerm = configuration.getAttribute('Max Contract Term').value;
				var currentContractTerm = configuration.getAttribute('Contract Term').value;
				if (minContractTerm != '' && minContractTerm != null && maxContractTerm != '' && maxContractTerm != null) {
					if (currentContractTerm < minContractTerm || currentContractTerm > maxContractTerm) {
						configuration.status = false;
						if (configuration.statusMessage != null && configuration.statusMessage != '') {
							if (!configuration.statusMessage.includes('Contract Term should be between')) {
								configuration.statusMessage += ' || Contract Term should be between ' + minContractTerm + ' and ' + maxContractTerm + '.';
							}
						} else {
							configuration.statusMessage = 'Contract Term should be between ' + minContractTerm + ' and ' + maxContractTerm + '.';
						}
					} else {
						if (configuration.statusMessage != null && configuration.statusMessage != '' && configuration.statusMessage.includes(' || ')) {
							var messageSplited = configuration.statusMessage.split(' || ');
							for (let i = 0; i < messageSplited.length; i++) {
								if (messageSplited[i].includes('Contract Term should be between ')) {
									continue;
								} else {
									configuration.statusMessage = messageSplited[i];
								}
							}
						} else if (configuration.statusMessage != null && configuration.statusMessage != '') {
							if (configuration.statusMessage.includes('Contract Term should be between ')) {
								configuration.statusMessage = '';
							} 
						}
						if (configuration.statusMessage == null || configuration.statusMessage == '') {
							configuration.status = true;
							configuration.errorMessage = null;
						}
					}
				} else {
					if (configuration.statusMessage != null && configuration.statusMessage != '' && configuration.statusMessage.includes(' || ')) {
						var messageSplited = configuration.statusMessage.split(' || ');
						for (let i = 0; i < messageSplited.length; i++) {
							if (messageSplited[i].includes('Contract Term should be between ')) {
								continue;
							} else {
								configuration.statusMessage = messageSplited[i];
							}
						}
					} else if (configuration.statusMessage != null && configuration.statusMessage != '') {
						if (configuration.statusMessage.includes('Contract Term should be between ')) {
							configuration.statusMessage = '';
						} 
					}
					if (configuration.statusMessage == null || configuration.statusMessage == '') {
						configuration.status = true;
						configuration.errorMessage = null;
					}
				}

				var minUserNumber = configuration.getAttribute('Min Number of Users').value;
				var maxUserNumber = configuration.getAttribute('Max Number of Users').value;
				var solution = await CS.SM.getActiveSolution();
				var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
				var currentUserNumber = servicePlanConfiguration.getAttribute('Total Number of Users').value;
				var groupSelected = configuration.getAttribute('Group').displayValue;
				var productListIncludedForUsers = ['Complete Mobility', 'Tailored Set Up', 'Tailored Service', 'Total Resource', 'Data Collection'];
				if (minUserNumber != '' && minUserNumber != null && maxUserNumber != '' && maxUserNumber != null && productListIncludedForUsers.includes(groupSelected)) {
					if (currentUserNumber < minUserNumber || currentUserNumber > maxUserNumber) {
						configuration.status = false;
						if (configuration.statusMessage != null && configuration.statusMessage != '') {
							if (!configuration.statusMessage.includes('Product selected should be between')) {
								configuration.statusMessage += ' || Product selected should be between ' + minUserNumber + ' and ' + maxUserNumber + ' Users.';
							}
						} else {
							configuration.statusMessage = 'Product selected should be between ' + minUserNumber + ' and ' + maxUserNumber + ' Users.';
						}
					} else {
						if (configuration.statusMessage != null && configuration.statusMessage != '' && configuration.statusMessage.includes(' || ')) {
							var messageSplited = configuration.statusMessage.split(' || ');
							for (let i = 0; i < messageSplited.length; i++) {
								if (messageSplited[i].includes('Product selected should be between')) {
									continue;
								} else {
									configuration.statusMessage = messageSplited[i];
								}
							}
						} else if (configuration.statusMessage != null && configuration.statusMessage != '') {
							if (configuration.statusMessage.includes('Product selected should be between')) {
								configuration.statusMessage = '';
							} 
						}
					}
					if (configuration.statusMessage == null || configuration.statusMessage == '') {
						configuration.status = true;
						configuration.errorMessage = null;
					}
				} else {
					if (configuration.statusMessage != null && configuration.statusMessage != '' && configuration.statusMessage.includes(' || ')) {
						var messageSplited = configuration.statusMessage.split(' || ');
						for (let i = 0; i < messageSplited.length; i++) {
							if (messageSplited[i].includes('Product selected should be between')) {
								continue;
							} else {
								configuration.statusMessage = messageSplited[i];
							}
						}
					} else if (configuration.statusMessage != null && configuration.statusMessage != '') {
						if (configuration.statusMessage.includes('Product selected should be between')) {
							configuration.statusMessage = '';
						} 
					}
					if (configuration.statusMessage == null || configuration.statusMessage == '') {
						configuration.status = true;
						configuration.errorMessage = null;
					}
				}

				updateConfigNameFM(configuration, component.name, solution);
			}

			if (attribute.name == 'Contract Term') {
				var minContractTerm = configuration.getAttribute('Min Contract Term').value;
				var maxContractTerm = configuration.getAttribute('Max Contract Term').value;
				var currentContractTerm = attribute.value;
				if (minContractTerm != '' && minContractTerm != null && maxContractTerm != '' && maxContractTerm != null) {
					if (currentContractTerm < minContractTerm || currentContractTerm > maxContractTerm) {
						configuration.status = false;
						if (configuration.statusMessage != null && configuration.statusMessage != '') {
							if (!configuration.statusMessage.includes('Contract Term should be between')) {
								configuration.statusMessage += ' || Contract Term should be between ' + minContractTerm + ' and ' + maxContractTerm + '.';
							}
						} else {
							configuration.statusMessage = 'Contract Term should be between ' + minContractTerm + ' and ' + maxContractTerm + '.';
						}
					} else {
						if (configuration.statusMessage != null && configuration.statusMessage != '' && configuration.statusMessage.includes(' || ')) {
							var messageSplited = configuration.statusMessage.split(' || ');
							for (let i = 0; i < messageSplited.length; i++) {
								if (messageSplited[i].includes('Contract Term should be between ')) {
									continue;
								} else {
									configuration.statusMessage = messageSplited[i];
								}
							}
						} else if (configuration.statusMessage != null && configuration.statusMessage != '') {
							if (configuration.statusMessage.includes('Contract Term should be between ')) {
								configuration.statusMessage = '';
							} 
						}
						if (configuration.statusMessage == null || configuration.statusMessage == '') {
							configuration.status = true;
							configuration.errorMessage = null;
						}
					}
				} else {
					if (configuration.statusMessage != null && configuration.statusMessage != '' && configuration.statusMessage.includes(' || ')) {
						var messageSplited = configuration.statusMessage.split(' || ');
						for (let i = 0; i < messageSplited.length; i++) {
							if (messageSplited[i].includes('Contract Term should be between ')) {
								continue;
							} else {
								configuration.statusMessage = messageSplited[i];
							}
						}
					} else if (configuration.statusMessage != null && configuration.statusMessage != '') {
						if (configuration.statusMessage.includes('Contract Term should be between ')) {
							configuration.statusMessage = '';
						} 
					}
					if (configuration.statusMessage == null || configuration.statusMessage == '') {
						configuration.status = true;
						configuration.errorMessage = null;
					}
				}
			}

			if (attribute.name == 'Unit One Off Charge') {
				if (attribute.value > 0) {
					updateData = [{
						name: 'Contract Term',
						value: '1',
						displayValue: '1',
						readOnly: false,
						required: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
				}
			}
		}

		return Promise.resolve(true);
	};


	FMPlugin.afterAttributeValidation = function(component, configuration, attribute, validationResult, value) {
		if (attribute.name == 'Staged Credit Month' && attribute.value != 'N/A') {
			//using validateFieldsByType function from CloudVoiceServicePlugin
			let result = validateFieldsByType(attribute, 'WholePositiveNumber');
			if (result != null) return Promise.resolve({
				error: true,
				message: result
			});
			else return Promise.resolve(true);
		}

		return Promise.resolve(true);
	};

	FMPlugin.beforeRelatedProductAdd = async function(component, configuration, relatedProduct) {
		console.log('FMPlugin.beforeRelatedProductAdd', component, configuration, relatedProduct);
		//using function from CS_CloudVoiceServicePlugin
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			var solution = await CS.SM.getActiveSolution();
			var mainConfiguration = Object.values(solution.schema.configurations)[0];
            
            /*if (relatedProduct.relatedProductName == 'User Plans') {
				if (configuration.getAttribute('Individual Override').value == false) {
					calculateVoiceAndDataAddOnCharges(component, configuration, relatedProduct, mainConfiguration);
				}
			}*/
			if (relatedProduct.name == 'Mobile Iron Threat Defence') {
				CS.SM.displayMessage('Customer must have Silver on their Estate', 'info');
			}
		}

		if (component.name == FUTURE_MOBILE_CONST.mainComponent) {
			var servicePlanId = configuration.getAttribute('Service Plan').value;
			//var addTotalResource = configuration.getAttribute('Add Total Resource').value;
			var mobileIronCLoud = configuration.getAttribute('Mobile Iron Cloud').value;
			var managedMDM = configuration.getAttribute('Managed MDM').value;
			var noOfUsers = Number(configuration.getAttribute('Total Number of Users').value);
			if (relatedProduct.relatedProductName == 'Account Level Add-Ons' && relatedProduct.name == 'Mobile Iron Threat Defence') {
				CS.SM.displayMessage('User Group needs Silver, Gold or Platinum', 'info');
			}
			if (relatedProduct.groupName == 'Voice Bundle') {
				var addonsDeleted = false;
				if (relatedProduct.name == 'Unlimited Minutes & Texts') {
					for (let relatedProductLoop of Object.values(configuration.relatedProductList)) {
						if (relatedProductLoop.groupName == 'Voice Bundle' && relatedProductLoop.name.includes('Shared') && relatedProductLoop.name.includes('Minutes & Texts')) {
							await deleteAddOns(component, configuration, relatedProductLoop.guid, true);
							addonsDeleted = true;
						}
					}
				}
				if (relatedProduct.name.includes('Shared') && relatedProduct.name.includes('Minutes & Texts')) {
					for (let relatedProductLoop of Object.values(configuration.relatedProductList)) {
						if (relatedProductLoop.groupName == 'Voice Bundle' && (relatedProductLoop.name == 'Unlimited Minutes & Texts')) {
							await deleteAddOns(component, configuration, relatedProductLoop.guid, true);
							addonsDeleted = true;
						}
					}
				}
				if (addonsDeleted) CS.SM.displayMessage('Some Voice plans have been removed', 'info');
			}
			/*if (relatedProduct.groupName.includes(FUTURE_MOBILE_CONST.sharedVAS)) {
			    var contractTermReadOnly = false;
				for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
					if (attribute.name == 'Service Plan') {
						attribute.value = servicePlanId;
						attribute.displayValue = servicePlanId;
						attribute.readOnly = true;
					}
					if (attribute.name == 'Add Total Resource') {
						attribute.value = addTotalResource;
						attribute.displayValue = addTotalResource;
						attribute.readOnly = true;
					}
					if (attribute.name == 'Mobile Iron Cloud') {
						attribute.value = mobileIronCLoud;
						attribute.displayValue = mobileIronCLoud;
						attribute.readOnly = true;
					}
					if (attribute.name == 'Managed MDM') {
						attribute.value = managedMDM;
						attribute.displayValue = managedMDM;
						attribute.readOnly = true;
					}
					if (attribute.name == 'Total Number of Users') {
						attribute.value = noOfUsers;
						attribute.displayValue = noOfUsers;
						attribute.readOnly = true;
					}
					if (attribute.name == 'Group' && attribute.value != null && attribute.value != '') {
					    attribute.readOnly = true;
					}
					if (attribute.name == 'Shared VAS' && attribute.value != null && attribute.value != '') {
					    attribute.readOnly = true;
					}
					if (attribute.name == 'Contract Term' && attribute.value != null && attribute.value != '') {
					    attribute.readOnly = true;
					}
				}
			}*/
		}
		calculateAddonQuantity(component, configuration, relatedProduct);
		return Promise.resolve(true);
	};

	FMPlugin.afterRelatedProductAdd = async function(component, configuration, relatedProduct) {
		let solution = await CS.SM.getActiveSolution();
		if (component.name == FUTURE_MOBILE_CONST.mainComponent) {
			var servicePlan = configuration.getAttribute('Service Plan').displayValue;
			if (servicePlan == 'Custom Caller') {
				if (relatedProduct.groupName.includes('Voice Bundle') || relatedProduct.groupName.includes('SMS Bundle')) {
					await calculateCustomCallerCharges(solution, component, configuration, relatedProduct);
				}
			}
			calculateTotalCharges(solution, configuration);
			calculateMaxFCCAllUG(solution, solution.getComponentByName('User Groups'));
		}

		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
		    await calculateVoiceAndDataAddOnCharges(component, configuration, relatedProduct, Object.values(solution.schema.configurations)[0]);
		    calculateTotalUserSetupCharges(solution, configuration, null, null, null);
			calculateMaxFCC(configuration, solution, component);
			if (relatedProduct.relatedProductName == 'Devices') {
				checkDevices(solution, configuration);
			}
			
		}
		return Promise.resolve(true);
	};

	FMPlugin.beforeRelatedProductDelete = function(component, configuration, relatedProduct) {
		console.log('FMPlugin.beforeRelatedProductDelete', component, configuration, relatedProduct);
		return Promise.resolve(true);
	};

	FMPlugin.afterRelatedProductDelete = async function(component, configuration, relatedProduct) {
		console.log('FMPlugin.afterRelatedProductDelete', component, configuration, relatedProduct);
		let solution = await CS.SM.getActiveSolution();
		if (component.name == FUTURE_MOBILE_CONST.mainComponent) {
			calculateTotalCharges(solution, configuration);
		}
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			calculateTotalUserSetupCharges(solution, configuration, null, null, null);
			calculateMaxFCC(configuration, solution, component);
			if (relatedProduct.relatedProductName == 'Devices') {
				checkDevices(solution, configuration);
			}
		}
		return Promise.resolve(true);
	};
	
	FMPlugin.beforeConfigurationAdd = async function(component, configuration) {
	    console.log('FMPlugin.beforeConfigurationAdd', component, configuration);
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
		    var solution = await CS.SM.getActiveSolution();
		    var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
			var servicePlan = servicePlanConfiguration.getAttribute('Service Plan').displayValue;
			var voicePlan = servicePlanConfiguration.getAttribute('Team Voice Plan').displayValue;
			var dataPlan = servicePlanConfiguration.getAttribute('Team Data Plan').displayValue;
			var mainContractTerm = servicePlanConfiguration.getAttribute('Contract Term').value;
			var voiceSchemeDiscount = servicePlanConfiguration.getAttribute('Voice Scheme Discount').value;
			var dataSchemeDiscount = servicePlanConfiguration.getAttribute('Data Scheme Discount').value;
			if ((servicePlan == 'The Custom Caller' || servicePlan == 'The Team Plan') && (voicePlan == '' || voicePlan == 'N/A') && (dataPlan == '' || dataPlan == 'N/A')) {
				CS.SM.displayMessage('You must have a Voice or a Data plan in order to add a User Group', 'error');
				return false;
			}

			if (servicePlan == 'The Shared Plan' || servicePlan == 'The Custom Caller') {
				var planExists = false;
				for (let relatedProduct of Object.values(servicePlanConfiguration.relatedProductList)) {
					if (relatedProduct.relatedProductName == 'Service Plans') {
						planExists = true;
					}
				}
				if (!planExists) {
					CS.SM.displayMessage('You must have a Voice or a Data service plan add-on in order to add a User Group', 'error');
					return false;
				}
			}
			for (let attribute of Object.values(configuration.attributes)) {
				if (attribute.name == 'Contract Term') {
					attribute.value = mainContractTerm;
					attribute.displayValue = mainContractTerm;
					attribute.readOnly = true;
				}
				if (attribute.name == 'Voice Scheme Discount') {
					attribute.value = voiceSchemeDiscount;
					attribute.displayValue = voiceSchemeDiscount;
					attribute.readOnly = true;
				}
				if (attribute.name == 'Data Scheme Discount') {
					attribute.value = dataSchemeDiscount;
					attribute.displayValue = dataSchemeDiscount;
					attribute.readOnly = true;
				}
			}
		}
		if (component.name == FUTURE_MOBILE_CONST.creditFund) {
		    var solution = await CS.SM.getActiveSolution();
			var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
			var mainContractTerm = servicePlanConfiguration.getAttribute('Contract Term').value;
			for (let attribute of Object.values(configuration.attributes)) {
				if (attribute.name == 'Contract Term') {
					attribute.value = mainContractTerm;
					attribute.displayValue = mainContractTerm;
					attribute.readOnly = true;
				}
			}
			if (correctProfile != 'true') {
				for (let attribute of Object.values(configuration.attributes)) {
					if (attribute.name == 'Credit Type') {
						attribute.value = 'Hardware';
						attribute.displayValue = 'Technology';
						attribute.readOnly = true;
					}
				}
			}
		}
		return Promise.resolve(true);
	};
	
	FMPlugin.afterConfigurationAdd = async function(component, configuration) {
	    console.log('FM afterConfigurationAdd--->', component, configuration);
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			let solution = await CS.SM.getActiveSolution();
			await updateUserGroupConfig(solution);
		}
		if (component.name == FUTURE_MOBILE_CONST.creditFund) {
			let solution = await CS.SM.getActiveSolution();
			calculateCreditFunds(solution);
		}
		if (component.name == FUTURE_MOBILE_CONST.special) {
		    let solution = await CS.SM.getActiveSolution();
		    var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
			var mainContractTerm = servicePlanConfiguration.getAttribute('Contract Term').value;
			var noOfUsers = Number(servicePlanConfiguration.getAttribute('Total Number of Users').value);
			for (let attribute of Object.values(configuration.attributes)) {
				updateConfigurationAttributeValue('Tenure', mainContractTerm, mainContractTerm, true, solution, configuration.guid, true);
				updateConfigurationAttributeValue('Number of Users', noOfUsers, noOfUsers, true, solution, configuration.guid, true);
			}
		}
	};

	FMPlugin.afterQuantityUpdated = async function(component, configurationGuid, oldQuantity, newQuantity) {
		console.log('afterQuantityUpdated ', component, configurationGuid, oldQuantity, newQuantity);
		let solution = await CS.SM.getActiveSolution();
		let config = solution.getConfiguration(configurationGuid);
		var mainConfiguration = Object.values(solution.schema.configurations)[0];
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			updateConfigurationAttributeValue('Quantity', newQuantity, newQuantity, true, solution, configurationGuid, false);
			//Added to update total number of Users automatically -- because it is used for sharedVAS and needs to be populated after quantity update
			updateConfigurationAttributeValue('Total Number of Users', newQuantity, newQuantity, true, solution, mainConfiguration.guid, false);
		}
		calculateTotalUserSetupCharges(solution, config, null, null, null);
		calculateMaxFCC(config, solution, component);
		return Promise.resolve(true);
	};
}

async function beforeNavigateFutureMobile(solution, currentComponent, previousComponent) {
	console.log('beforeNavigateFutureMobile', solution, currentComponent, previousComponent);
	var totalAirtime;
	var remainingAirtime;
	if (currentComponent.name == FUTURE_MOBILE_CONST.creditFund) {
		for (let configuration of Object.values(currentComponent.schema.configurations)) {
			console.log("about to change funds", correctProfile, configuration.getAttribute('Credit Type').readOnly);
			let currentType = configuration.getAttribute('Credit Type').displayValue;

			if (correctProfile == 'true') {
				updateConfigurationAttributeValue('Credit Type', 'N/A', 'N/A', true, solution, configuration.guid, true);
				var totalAirtime = configuration.getAttribute('Total Airtime Fund Available').value;
				var remainingAirtime = configuration.getAttribute('Remaining Airtime Funds').value;
				updateConfigurationAttributeValue('Credit Type', currentType, currentType, false, solution, configuration.guid, true);
			} else {
				totalAirtime = 'N/A';
				remainingAirtime = 'N/A';
				updateConfigurationAttributeValue('Credit Type', 'N/A', 'N/A', false, solution, configuration.guid, true);
				updateConfigurationAttributeValue('Credit Type', currentType, currentType, true, solution, configuration.guid, true);
			}
			updateConfigurationAttributeValue('Total Airtime Fund Available Display', totalAirtime, totalAirtime, true, solution, configuration.guid, true);
			updateConfigurationAttributeValue('Remaining Airtime Funds Display', remainingAirtime, remainingAirtime, true, solution, configuration.guid, true);
		}
	}

	return true;
}

async function afterNavigateFutureMobile(solution, currentComponent, previousComponent) {
	console.log('afterNavigateFutureMobile', solution, currentComponent, previousComponent);
	correctProfile = 'true';
	if (correctProfile == null) {
		var currentBasket = await CS.SM.getActiveBasket();
		var result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
			'action': 'isProfileQueuedRemote'
		});
		correctProfile = result["goodProfile"];
		console.log(result);
	}
	if (currentComponent.name == FUTURE_MOBILE_CONST.mainComponent) {
		let servicePlanConfiguration = Object.values(solution.schema.configurations)[0];
		displayUsageProfile(currentComponent, servicePlanConfiguration.guid);
		calculateUsersCount(solution);
		calculateTotalCharges(solution, servicePlanConfiguration);
		//await autoAddDataCollectorVAS(solution, servicePlanConfiguration.getAttribute('Add Data Collector').displayValue);
		//await autoAddTotalResourceVAS(solution, servicePlanConfiguration.getAttribute('Add Total Resource').displayValue);
	}
	if (currentComponent.name == FUTURE_MOBILE_CONST.special) {
		calculateUsersCount(solution);
		for (let configuration of Object.values(currentComponent.schema.configurations)) {
			allowDisconnectAllowance(solution, configuration);
		}
	}

	if (currentComponent.name == FUTURE_MOBILE_CONST.creditFund) {
		calculateCreditFunds(solution);
		calculateCreditFundsTwo(solution);

	}

	return true;
}

    function validateUGConfigurations(solution, servicePlan, customDataServicePlan) {
    var result = false;
    var ugConfigs = solution.getComponentByName('User Groups').getAllConfigurations();
    if (servicePlan != 'The Shared Plan' && servicePlan == 'Custom Caller' && customDataServicePlan != 'Shared') {
        for (let ugConfig of Object.values(ugConfigs)) {
            var subscriptionType = ugConfig.getAttribute('Subscription Type').displayValue;
            var dataAddonExists = false;
            var voiceAddonExists = false;
            for (let relatedProduct of Object.values(ugConfig.relatedProductList)) {
    			if (relatedProduct.type == 'Add On Component' && relatedProduct.groupName == 'Voice Bundle') {
    				voiceAddonExists = true;
    			}
    			if (relatedProduct.type == 'Add On Component' && relatedProduct.groupName == 'Data Bundle') {
    				dataAddonExists = true;
    			}
    		}
    		if ((subscriptionType == 'Voice and Data' && (!voiceAddonExists || !dataAddonExists)) || (subscriptionType == 'Voice Only' && !voiceAddonExists) || (subscriptionType == 'Data Only' && !dataAddonExists)) {
    		    result = true;
    		}
        }
    }
    return result;
}


async function updateUserGroupConfig(solution) {
	// get main component
	let mainConfig = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	// update lookup attribute
	var servicePlanName = mainConfig.getAttribute('Service Plan Name').value;
	var customDataServicePlan = mainConfig.getAttribute('Custom Data Service Plan').value;
	var ugServicePlanName = '';

	if (servicePlanName == 'Custom') {
		if (customDataServicePlan == 'Individual') {
			ugServicePlanName = 'Custom Individual';
		} else if (customDataServicePlan == 'Team') {
			ugServicePlanName = 'Custom Team'
		} else if (customDataServicePlan == 'Shared') {
			ugServicePlanName = 'Custom Shared'
		}
	}

	if (servicePlanName != 'Custom') {
		ugServicePlanName = servicePlanName;
	}

	/* updateData = [{name : 'Service Plan Name', value : servicePlanName , displayValue : servicePlanName, readOnly : true}];
	 solution.updateConfigurationAttribute(mainConfig.guid, updateData, true);
	*/
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			for (let configuration of Object.values(component.schema.configurations)) {
				if (configuration.getAttribute('Individual Override').value != true) {
					updateData = [{
						name: 'Service Plan Name',
						value: ugServicePlanName,
						displayValue: ugServicePlanName,
						readOnly: true
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				}
			}
		}
	}

}

async function calculateAndAddUserGroupAddOnForTeam(solution, component, userGroupConfiguration, attributeValue) {
	if (!userGroupConfiguration) {
		for (let configLoop of Object.values(component.schema.configurations)) {
			if (!isValidAutoAddOn(configLoop)) {
				continue;
			}

			var serviceplanName = configLoop.getAttribute('Service Plan Name').value;
			if (attributeValue == '') {
				attributeValue = configLoop.getAttribute('Subscription Type Name').value;
			}
			if (serviceplanName == 'Team' || serviceplanName.includes('Custom')) {
				if (attributeValue == 'Voice Only') attributeValue = 'Voice';
				if (attributeValue == 'Data Only') attributeValue = 'Data';


				// delete exiting User Plans
				if (configLoop.relatedProductList.length > 0) {
					for (let relatedProduct of Object.values(configLoop.relatedProductList)) {
						if (relatedProduct.relatedProductName == 'User Plans') {
							if ((relatedProduct.groupName.includes('Data')) || (relatedProduct.groupName.includes('Voice'))) {
								await deleteAddOns(component, configLoop, relatedProduct.guid, true);
							}
						}
					}
				}
				let servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
				var voicePlan = servicePlanConfiguration.getAttribute('Team Voice Plan').displayValue;
				var dataPlan = servicePlanConfiguration.getAttribute('Team Data Plan').displayValue;

				if (voicePlan == 'N/A' && configLoop.getAttribute('Service Plan Name').value.includes('Custom')) {
					voicePlan = 'Line Rental';
				}
				// add voice plan
				if (voicePlan != '' && (attributeValue == 'Voice' || attributeValue == 'Voice and Data')) {
					await addDataAndVoiceAddOn(voicePlan, 'Voice', component, configLoop, attributeValue);
				}
				// add voice plan
				if (dataPlan != '' && (attributeValue == 'Data' || attributeValue == 'Voice and Data')) {
					await addDataAndVoiceAddOn(dataPlan, 'Data', component, configLoop, attributeValue);
				}

				//var updateData = [{name : 'Show AddOn', value : false, displayValue : false, readOnly : true}];
				//solution.updateConfigurationAttribute(configLoop.guid, updateData, true);
			} else if (serviceplanName == 'Individual' && configLoop.getAttribute('Individual Override').value != true) {
				if (attributeValue == 'Voice Only') attributeValue = 'Voice';
				if (attributeValue == 'Data Only') attributeValue = 'Data';

				// delete exiting User Plans
				if (configLoop.relatedProductList.length > 0) {
					for (let relatedProduct of Object.values(configLoop.relatedProductList)) {
						if (relatedProduct.relatedProductName == 'User Plans') {
							if (relatedProduct.groupName.includes('Voice')) {
								await deleteAddOns(component, configLoop, relatedProduct.guid, true);
							}
						}
					}
				}
				console.log('voice plan debug2 - ' + voicePlan);
				if (voicePlan != '' && (attributeValue == 'Voice' || attributeValue == 'Voice and Data')) {
					await addDataAndVoiceAddOn('Unlimited Minutes & Text', 'Voice', component, configLoop, attributeValue);
				}
			}
		}
	} else {
		if (!isValidAutoAddOn(userGroupConfiguration)) {
			return;
		}

		var configLoop = userGroupConfiguration;

		var serviceplanName = configLoop.getAttribute('Service Plan Name').value;
		if (attributeValue == '') {
			attributeValue = configLoop.getAttribute('Subscription Type Name').value;
		}
		if (serviceplanName == 'Team' || serviceplanName.includes('Custom')) {
			if (attributeValue == 'Voice Only') attributeValue = 'Voice';
			if (attributeValue == 'Data Only') attributeValue = 'Data';


			// delete exiting User Plans
			if (configLoop.relatedProductList.length > 0) {
				for (let relatedProduct of Object.values(configLoop.relatedProductList)) {
					if (relatedProduct.relatedProductName == 'User Plans') {
						if ((relatedProduct.groupName.includes('Data')) || (relatedProduct.groupName.includes('Voice'))) {
							await deleteAddOns(component, configLoop, relatedProduct.guid, true);
						}
					}
				}
			}
			let servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
			var voicePlan = servicePlanConfiguration.getAttribute('Team Voice Plan').displayValue;
			var dataPlan = servicePlanConfiguration.getAttribute('Team Data Plan').displayValue;

			if (voicePlan == 'N/A' && configLoop.getAttribute('Service Plan Name').value.includes('Custom')) {
				voicePlan = 'Line Rental';
			}
			// add voice plan
			if (voicePlan != '' && (attributeValue == 'Voice' || attributeValue == 'Voice and Data')) {
				await addDataAndVoiceAddOn(voicePlan, 'Voice', component, configLoop, attributeValue);
			}
			// add voice plan
			if (dataPlan != '' && (attributeValue == 'Data' || attributeValue == 'Voice and Data')) {
				await addDataAndVoiceAddOn(dataPlan, 'Data', component, configLoop, attributeValue);
			}

			//var updateData = [{name : 'Show AddOn', value : false, displayValue : false, readOnly : true}];
			//solution.updateConfigurationAttribute(configLoop.guid, updateData, true);
		} else if (serviceplanName == 'Individual' && configLoop.getAttribute('Individual Override').value != true) {
			if (attributeValue == 'Voice Only') attributeValue = 'Voice';
			if (attributeValue == 'Data Only') attributeValue = 'Data';

			// delete exiting User Plans
			if (configLoop.relatedProductList.length > 0) {
				for (let relatedProduct of Object.values(configLoop.relatedProductList)) {
					if (relatedProduct.relatedProductName == 'User Plans') {
						if (relatedProduct.groupName.includes('Voice')) {
							await deleteAddOns(component, configLoop, relatedProduct.guid, true);
						}
					}
				}
			}
			if (voicePlan != '' && (attributeValue == 'Voice' || attributeValue == 'Voice and Data')) {
				await addDataAndVoiceAddOn('Unlimited Minutes & Text', 'Voice', component, configLoop, attributeValue);
			}
		}
	}
}

async function addDataAndVoiceAddOn(planName, groupName, component, configure, attributeValue) {
	if (globalUGAddonMap[planName] == null) {
		try {
			let currentAddons = await component.getAddonsForConfiguration(configure.guid);
			for (let segment of Object.values(currentAddons)) {
				segment.allAddons.forEach(oneAddon => {
					globalUGAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name] = oneAddon;
					globalOneOffCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
					globalRecurringCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
				});
			}
		} catch (error) {
			console.log('ERROR IS ', error);
		}
	}
	if (globalUGAddonMap[planName] != null) {
		let ugAddon = globalUGAddonMap[planName];
		console.log('calculateAndAddUserGroupAddOn ugAddon', ugAddon);
		if (ugAddon.cspmb__Group__c.includes(groupName)) {
			await addAddOnProductConfiguration('User Plans', ugAddon, configure.guid, 1, component);
		}
	}
}

async function calculateVoiceAndDataAddOnCharges(component, configuration, relatedProduct, mainConfiguration) {
	var dataSchemeDiscount = mainConfiguration.getAttribute('Data Scheme Discount').value;
	var voiceSchemeDiscount = mainConfiguration.getAttribute('Voice Scheme Discount').value;
	if (relatedProduct.groupName.includes('Data')) {
		/*relatedProduct.recurringCharge = Number(relatedProduct.recurringCharge) + Number(dataSchemeDiscount);
		relatedProduct.configuration.recurringPrice = Number(relatedProduct.configuration.recurringPrice) + Number(dataSchemeDiscount);*/
		relatedProduct.recurringCharge = Number(mainConfiguration.getAttribute('Team Data Monthly Charge').value);
		relatedProduct.configuration.recurringPrice = Number(mainConfiguration.getAttribute('Team Data Monthly Charge').value) ;
	} else if (relatedProduct.groupName.includes('Voice')) {
		/*relatedProduct.recurringCharge = Number(relatedProduct.recurringCharge) + Number(voiceSchemeDiscount);
		relatedProduct.configuration.recurringPrice = Number(relatedProduct.configuration.recurringPrice) + Number(voiceSchemeDiscount);*/
		relatedProduct.recurringCharge = Number(mainConfiguration.getAttribute('Team Voice Monthly Charge').value);
		relatedProduct.configuration.recurringPrice = Number(mainConfiguration.getAttribute('Team Voice Monthly Charge').value);
	}
}
async function addCustomCallerPlans(solution, component, configuration, relatedProductName) {
	if (globalUGAddonMap[relatedProductName] == null) {
		try {
			let currentAddons = await component.getAddonsForConfiguration(configuration.guid);
			for (let segment of Object.values(currentAddons)) {
				segment.allAddons.forEach(oneAddon => {
					globalUGAddonMap[oneAddon.cspmb__Add_On_Price_Item__r.Name] = oneAddon;
					globalOneOffCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
					globalRecurringCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
				});
			}
		} catch (error) {
			console.log('ERROR IS ', error);
		}
	}
	if (globalUGAddonMap[relatedProductName] != null) {
		for (let oneAddonKey of Object.keys(globalUGAddonMap)) {
			var oneAddon = globalUGAddonMap[oneAddonKey];
			if (oneAddon.cspmb__Group__c == 'Voice Bundle' || oneAddon.cspmb__Group__c == 'SMS Bundle') {
				await addAddOnProductConfiguration('Service Plans', oneAddon, configuration.guid, 1, component);
			}
		}
	}

	//var updateData = [{name : 'Show AddOn', value : false, displayValue : false, readOnly : true}];
	//solution.updateConfigurationAttribute(configuration.guid, updateData, true);
}


async function calculateCustomCallerCharges(solution, component, configuration, relatedProduct) {
	let inputMap = {};
	inputMap["basketId"] = solution.basketId;
	inputMap["Id"] = '0013E000012WrIS';

	currentBasket = await CS.SM.getActiveBasket();

	if (Object.keys(usageProfiledata).length === 0) {
		let currentBasket = await CS.SM.getActiveBasket();
		usageProfiledata = await currentBasket.performRemoteAction('CS_UsageProfileFM', inputMap);
	}
	var addonName = relatedProduct.name + ' (Unit Price : Â£' + relatedProduct.recurringCharge + '/Minute)';

	if (usageProfiledata["DataFound"] == true) {
		var fieldKey = customCallerAddOnArray[relatedProduct.name];
		if (fieldKey != '') {
			var persenageValue = usageProfiledata[fieldKey];
			var expectedUsage = 0;
			if (relatedProduct.groupName.includes('Voice')) {
				expectedUsage = usageProfiledata['ExpectedVoice'];
			} else if (relatedProduct.groupName.includes('Data')) {
				expectedUsage = usageProfiledata['ExpectedData'];
			} else if (relatedProduct.groupName.includes('SMS')) {
				expectedUsage = usageProfiledata['ExpectedSMS'];
			}


			var calculatedUsage = Number((Number(expectedUsage) / 100) * persenageValue)
			var recCharge = calculatedUsage * relatedProduct.recurringCharge;


			console.log('persenageValue>>>>' + calculatedUsage);
			console.log('expectedUsage>>>>' + expectedUsage);

			for (let attribute of Object.values(relatedProduct.configuration.attributes)) {

				if (attribute.name == 'Expected Usage') attribute.value = expectedUsage;

				if (attribute.name == 'Percentage Contribution') attribute.value = persenageValue;

				if (attribute.name == 'Calculated Recurring Charge') attribute.value = recCharge;

				if (attribute.name == 'Calculated Usage') attribute.value = calculatedUsage;

				if (attribute.name == 'Unit Recurring Charge') attribute.value = relatedProduct.recurringCharge;
			}

			relatedProduct.recurringCharge = recCharge;
			relatedProduct.configuration.recurringPrice = recCharge;
			relatedProduct.name = addonName;
		}
	}

}

async function displayUsageProfile(componentName, configGuid) {
	console.log('in displayAddressSearch');
	let solution = await CS.SM.getActiveSolution();
	let basketId = solution.basketId;
	let configuration = solution.getConfiguration(configGuid);
	if (globalAccountID == '') {
		let currentBasket = await CS.SM.getActiveBasket();
		console.log('basketId', basketId);
		let result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
			"basketId": basketId,
			'action': 'CS_LookupQueryForAccountDetails'
		});
		if (result["DataFound"] == true) globalAccountID = result["AccountId"];
		console.log('result', result);
		console.log('globalAccountID', globalAccountID);
	}
	var rateCardId = configuration.getAttribute('Rate Card Lookup').value;

	let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString += '<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString += '<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString += '</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium">';
	urlString += 'Usage Profile </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	//urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__UsageProfileFM?basketid='+basketId+'&AccountId='+globalAccountID+'"';
	urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__ManageRateCardLines?accountId=' + globalAccountID + '&basketId=' + basketId + '&rateCardId=' + rateCardId + '"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}

async function buildFMVoiceAndDataSchemeLadder(componentName, configGuid) {
	console.log('in buildFMVoiceAndDataSchemeLadder');
	
	let solution =  await CS.SM.getActiveSolution();
    let basketId = solution.basketId;
    let configuration = solution.getConfiguration(configGuid);
    let teamVoicePlan = configuration.getAttribute('Team Voice Plan').displayValue;
    let teamVoiceScheme = configuration.getAttribute('Team Voice Scheme').displayValue;
    let teamDataPlan = configuration.getAttribute('Team Data Plan').displayValue;
    let teamDataScheme = configuration.getAttribute('Team Data Scheme').displayValue;
    
    console.log('teamVoicePlan=', teamVoicePlan);
    console.log('teamVoiceScheme=', teamVoiceScheme);
    console.log('teamDataPlan=', teamDataPlan);
    console.log('teamDataScheme=', teamDataScheme);

	let urlString = '<div id="AddressIframe" class="demo-only" height="100%">';
	urlString += '<section role="dialog" tabindex="-1" class="slds-modal slds-fade-in-open slds-modal_large" aria-labelledby="header43" aria-modal="true" aria-describedby="modal-content-id-1" style="background-color: #76767bb0;">';
	urlString += '<div class="slds-modal__container" style="max-width: 120rem; width: 90%;">';
	urlString += '<div class="slds-modal__header">';
	urlString += '<button class="slds-button slds-button_icon slds-modal__close slds-button_icon-inverse" style="right: 0.5rem; padding-right: 0rem; padding-left: 0rem; top: .5rem;" title="Close" onclick="closeIframeModal();">';
	urlString += '<svg class="slds-button__icon slds-button__icon_large" style=" color: #999; width: 2rem; height: 2rem;" aria-hidden="true">';
	urlString += '<use id="nodeInfoMPADown" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="/apexpages/slds/latest/assets/icons/utility-sprite/svg/symbols.svg#close"></use>';
	urlString += '</svg>';
	urlString += '</button>';
	urlString += '<h2 id="modal-BtnetPage" class="slds-text-heading--medium" style="color:#0070d2;font-size: 2em">';
	urlString += 'Scheme Ladder </h2>';
	urlString += '</div>';
	urlString += '<div class="slds-modal__content slds-p-around_medium" id="modaliframmmine" style="height: 100%;">';
	//urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_FMSchemeVisualLadder"';
	urlString += '<iframe height="100%" width="100%" style="border: none;" id="coisas" src="/apex/c__CS_FMSchemeVisualLadder?teamVoicePlan='+teamVoicePlan+'&teamVoiceScheme='+teamVoiceScheme+'&teamDataPlan='+teamDataPlan+'&teamDataScheme='+teamDataScheme+'"';
	urlString += '</div>';
	urlString += '</div>';
	urlString += '</section>';
	urlString += '<div class="slds-backdrop slds-backdrop_open"></div>';
	urlString += '</div>';
	return urlString;
}

function calculateTotalUserSetupCharges(solution, configuration, newSchemeValue, oldSchemeValue, attributeName) {
	let totalRecOther = 0.0;
	let totalOneOffOther = 0.0;
	let totalVoiceCharges = 0.0;
	let totalDataCharges = 0.0;
	let totalHardwareCharges = 0.0;

	let voiceCharge = 0.0;
	let dataCharge = 0.0;
	let voiceBaseCharge = 0.0;
	let dataBaseCharge = 0.0;

	var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);

	voiceBaseCharge = Number(mainConfiguration.getAttribute('Team Voice MRC List').displayValue);
	dataBaseCharge = Number(mainConfiguration.getAttribute('Team Data MRC List').displayValue);

	var quantity = configuration.configurationProperties.quantity;
	if (configuration.relatedProductList.length > 0) {
		for (let relatedProduct of Object.values(configuration.relatedProductList)) {
			console.log('calculateTotalUserSetupCharges relatedProduct: ');
			console.log(relatedProduct);
			let addOnQuantity = -1;
			for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
				if (attribute.name == 'Quantity') {
					addOnQuantity = Number(attribute.value);
					if (isNaN(addOnQuantity) || addOnQuantity == '') addOnQuantity = 0;
				}
			}
			if (addOnQuantity == -1) addOnQuantity = 1;

			if (relatedProduct.relatedProductName == 'Devices') {
				totalHardwareCharges += Number(relatedProduct.configuration.oneOffPrice) * parseInt(quantity) * parseInt(addOnQuantity);
			}

			if (relatedProduct.groupName.includes('Voice') && relatedProduct.relatedProductName == 'User Plans') {
				totalVoiceCharges += Number(relatedProduct.configuration.recurringPrice) * parseInt(quantity) * parseInt(addOnQuantity);
				voiceCharge = Number(relatedProduct.configuration.recurringPrice);
			}
			else if (relatedProduct.groupName.includes('Data') && relatedProduct.relatedProductName == 'User Plans') {
				totalDataCharges += Number(relatedProduct.configuration.recurringPrice) * parseInt(quantity) * parseInt(addOnQuantity);
				dataCharge = Number(relatedProduct.configuration.recurringPrice);
			}
			else {
				totalRecOther += Number(relatedProduct.configuration.recurringPrice) * parseInt(quantity) * parseInt(addOnQuantity);
				totalOneOffOther += Number(relatedProduct.configuration.oneOffPrice) * parseInt(quantity) * parseInt(addOnQuantity);
			}
		}
	}

	if (configuration.getAttribute('Service Plan Name').value == 'Team' && configuration.getAttribute('Individual Override').value == false) {
		if (attributeName == null) {

			if (voiceBaseCharge + (Number((configuration.getAttribute('Voice Scheme Discount').value))) != voiceCharge && dataBaseCharge + (Number((configuration.getAttribute('Data Scheme Discount').value))) != dataCharge) {
				totalVoiceCharges = totalVoiceCharges + (configuration.getAttribute('Voice Scheme Discount').value * quantity);
				totalDataCharges = totalDataCharges + (configuration.getAttribute('Data Scheme Discount').value * quantity);
			}
			else {
				totalVoiceCharges = voiceCharge * quantity;
				totalDataCharges = dataCharge * quantity;
			}
		}

		if (attributeName == 'Voice Scheme Discount') {
			totalVoiceCharges = (totalVoiceCharges - (oldSchemeValue * quantity)) + (newSchemeValue * quantity);

			let dataScheme = configuration.getAttribute('Data Scheme Discount').value;

			if (dataScheme) {
				totalDataCharges = totalDataCharges + (dataScheme * quantity);
			}
		}

		if (attributeName == 'Data Scheme Discount') {
			totalDataCharges = (totalDataCharges - (oldSchemeValue * quantity)) + (newSchemeValue * quantity);

			let voiceScheme = configuration.getAttribute('Voice Scheme Discount').value;

			if (voiceScheme) {
				totalVoiceCharges = totalVoiceCharges + (voiceScheme * quantity);
			}
		}
	}
	console.log('totalDataCharges after ' + totalDataCharges);

	totalVoiceCharges = totalVoiceCharges < 0 ? 0 : totalVoiceCharges;
	totalDataCharges = totalDataCharges < 0 ? 0 : totalDataCharges;

	if (configuration.getAttribute('Service Plan Name').value == 'Shared') {
		for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
			if (relatedProduct.name == 'Unlimited Minutes & Texts') {
				totalVoiceCharges = mainConfiguration.getAttribute('Total Voice Charge').value;
			}
		}
	}

	updateConfigurationAttributeValue('Total Data Charge', totalDataCharges, totalDataCharges, true, solution, configuration.guid, true);
	updateConfigurationAttributeValue('Total Voice Charge', totalVoiceCharges, totalVoiceCharges, true, solution, configuration.guid, true);
	updateConfigurationAttributeValue('Total Other Upfront', totalOneOffOther, totalOneOffOther, true, solution, configuration.guid, true);
	updateConfigurationAttributeValue('Total Other Recurring', totalRecOther, totalRecOther, true, solution, configuration.guid, true);
	updateConfigurationAttributeValue('Total Hardware Charge', totalHardwareCharges, totalHardwareCharges, true, solution, configuration.guid, true);
}

function calculateTotalCharges(solution, mainConfiguration) {
	let totalRecOther = 0.0;
	let totalOneOffOther = 0.0;
	let totalVoiceCharges = 0.0;
	let totalDataCharges = 0.0;
	let totalHardwareCharges = 0.0;

	var contractTerm = mainConfiguration.getAttribute('Contract Term').value;

	for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
		var addOnQuantity = -1;
		if (relatedProduct.groupName.includes('Voice') && relatedProduct.relatedProductName == 'Service Plans') {
			if (relatedProduct.name != 'Unlimited Minutes & Texts') {
				totalVoiceCharges += Number(relatedProduct.configuration.recurringPrice);
			} else {
				totalVoiceCharges += Number(relatedProduct.configuration.recurringPrice) * mainConfiguration.getAttribute('Number of User for AddOn').value;
			}
		}
		if (relatedProduct.groupName.includes('Data') && relatedProduct.relatedProductName == 'Service Plans') {
			totalDataCharges += Number(relatedProduct.configuration.recurringPrice);
		}
		if (!(relatedProduct.relatedProductName == 'Service Plans' && (relatedProduct.groupName.includes('Data') || relatedProduct.groupName.includes('Voice')))) {
			for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
				if (attribute.name == 'Quantity') {
					addOnQuantity = Number(attribute.value);
					if (isNaN(addOnQuantity) || addOnQuantity == '') addOnQuantity = 0;
				}
			}
			if (addOnQuantity == -1) addOnQuantity = 1;
			totalRecOther += Number(relatedProduct.configuration.recurringPrice) * parseInt(addOnQuantity);
			totalOneOffOther += Number(relatedProduct.configuration.oneOffPrice) * parseInt(addOnQuantity);
		}
	}

	console.log('calculateTotalCharges - totalVoiceCharges1: ' + totalVoiceCharges);

	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			for (let configuration of Object.values(component.schema.configurations)) {
				totalDataCharges += configuration.getAttribute('Total Data Charge').value;
				
				totalOneOffOther += configuration.getAttribute('Total Other Upfront').value;
				totalRecOther += configuration.getAttribute('Total Other Recurring').value;
				totalHardwareCharges += configuration.getAttribute('Total Hardware Charge').value;
				console.log('calculateTotalCharges - totalVoiceCharges2: ' + totalVoiceCharges);
				if (mainConfiguration.getAttribute('Service Plan Name').value != 'Shared') {
					totalVoiceCharges += configuration.getAttribute('Total Voice Charge').value;
				}
			}
		}
	}

	console.log('calculateTotalCharges - totalVoiceCharges final: ' + totalVoiceCharges);
	
	updateConfigurationAttributeValue('Total Data Charge', totalDataCharges, totalDataCharges, true, solution, mainConfiguration.guid, true);
	updateConfigurationAttributeValue('Total Voice Charge', totalVoiceCharges, totalVoiceCharges, true, solution, mainConfiguration.guid, true);
	updateConfigurationAttributeValue('Total Other Upfront', totalOneOffOther, totalOneOffOther, true, solution, mainConfiguration.guid, true);
	updateConfigurationAttributeValue('Total Other Recurring', totalRecOther, totalRecOther, true, solution, mainConfiguration.guid, true);
	updateConfigurationAttributeValue('Total Hardware Charge', totalHardwareCharges, totalHardwareCharges, true, solution, mainConfiguration.guid, true);
}

function calculateCreditFunds(solution) {
	//this is needed
	var totalContractValue = 0;
	var totalRecOther = 0.0;
	var totalOneOffOther = 0.0;
	var totalTechnology = 0.0;
	var servicePlanConfiguration = Object.values(solution.schema.configurations)[0];
	var contractTerm = servicePlanConfiguration.getAttribute('Contract Term').value;
	for (let relatedProduct of Object.values(servicePlanConfiguration.relatedProductList)) {
		var addOnQuantity = -1;
		for (let attribute of Object.values(relatedProduct.configuration.attributes)) {
			if (attribute.name == 'Quantity') {
				addOnQuantity = Number(attribute.value);
				if (isNaN(addOnQuantity) || addOnQuantity == '') addOnQuantity = 0;
			}
		}
		if (addOnQuantity == -1) addOnQuantity = 1;
		totalRecOther += Number(relatedProduct.configuration.recurringPrice) * parseInt(addOnQuantity);
		totalOneOffOther += Number(relatedProduct.configuration.oneOffPrice) * parseInt(addOnQuantity);
		totalContractValue += Number(relatedProduct.configuration.oneOffPrice) * parseInt(addOnQuantity);
		totalContractValue += Number(relatedProduct.configuration.recurringPrice) * parseInt(addOnQuantity) * contractTerm;
	}

	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			for (let configuration of Object.values(component.schema.configurations)) {
				var qbs = Number(configuration.configurationProperties.quantity);
				totalRecOther += configuration.getAttribute('Total Data Charge').value;
				totalRecOther += configuration.getAttribute('Total Voice Charge').value;
				totalOneOffOther += configuration.getAttribute('Total Other Upfront').value;
				totalRecOther += configuration.getAttribute('Total Other Recurring').value;
				totalTechnology +=  configuration.getAttribute('Total Hardware Charge').value;
				for (let relatedProduct of Object.values(configuration.relatedProductList)) {
					totalContractValue += Number(relatedProduct.configuration.oneOffPrice) * qbs;
					totalContractValue += Number(relatedProduct.configuration.recurringPrice) * qbs * contractTerm;
				}
			}
		}
	}
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.creditFund) {
			for (let configuration of Object.values(component.schema.configurations)) {
				updateConfigurationAttributeValue('Total Airtime Fund Available', (parseInt(totalContractValue) * 0.45).toFixed(2), (parseInt(totalContractValue) * 0.45).toFixed(2), true, solution, configuration.guid, true);
				updateConfigurationAttributeValue('Total Hardware Fund Available', (parseInt(totalContractValue) * 0.55).toFixed(2), (parseInt(totalContractValue) * 0.55).toFixed(2), true, solution, configuration.guid, true);
				updateConfigurationAttributeValue('Total Technology', parseFloat(totalTechnology), parseFloat(totalTechnology),  true, solution, configuration.guid, true);
				if (correctProfile == 'true') {
					updateConfigurationAttributeValue('Total Airtime Fund Available Display', (parseInt(totalContractValue) * 0.45).toFixed(2), (parseInt(totalContractValue) * 0.45).toFixed(2), true, solution, configuration.guid, true);
				}
			}
		}
	}
}

function updateConfigNameFM(con, componentName, solution) {
	var newName;
	console.log('updateConfigNameFM', con, componentName, solution, con.parentConfiguration, con.parentConfiguration == '');
	if (componentName == FUTURE_MOBILE_CONST.mainComponent && con.parentConfiguration == '') {
		newName = con.getAttribute('Service Plan').displayValue + ' : ' + con.getAttribute('Contract Term').displayValue + ' Months';
	}
	if (componentName == FUTURE_MOBILE_CONST.userGroups) {
		newName = con.getAttribute('Group Name').displayValue + ' : ' + con.getAttribute('Service Plan Name').displayValue + ' : ' + con.getAttribute('Subscription Type').displayValue;
	}
	if (componentName == FUTURE_MOBILE_CONST.creditFund) {
		newName = 'Fund : ' + con.getAttribute('Credit Type').displayValue + ' : ' + con.getAttribute('Staged or Rolling').displayValue + ' : ' + con.getAttribute('Credit Amount').displayValue;
	}
	if (componentName == FUTURE_MOBILE_CONST.sharedVAS) {
		newName = con.getAttribute('Shared VAS').displayValue;
	}
	con.configurationName = newName;
}

function checkTotalCreditFunds(configuration) {
	//this is needed
	return;
	var stagedOrRolling = configuration.getAttribute('Staged or Rolling').value;
	var creditAmount = configuration.getAttribute('Credit Amount').value;
	if (isNaN(creditAmount) || creditAmount == '') creditAmount = 0;
	var passed = true;
	if (stagedOrRolling == 'Staged') {
		var stagedCredit = configuration.getAttribute('Total Staged Credit on Deal').value;
		if (isNaN(stagedCredit) || stagedCredit == '') stagedCredit = 0;
		if (creditAmount > stagedCredit) passed = false;
	}

	if (stagedOrRolling == 'Rolling') {
		var rollingCredit = configuration.getAttribute('Total Rolling Credit on Deal').value;
		if (isNaN(rollingCredit) || rollingCredit == '') rollingCredit = 0;
		if (creditAmount > rollingCredit) passed = false;
	}

	if (!passed) {
		configuration.status = false;
		configuration.statusMessage = 'Credit Amount can not be higher than the corresponding Total Funds on Deal';
	} else {
		if (configuration.statusMessage == 'Credit Amount can not be higher than the corresponding Total Funds on Deal') {
			configuration.status = true;
			configuration.errorMessage = null;
		}
	}
}

function checkStagedCreditMonth(configuration) {
	var stagedOrRolling = configuration.getAttribute('Staged or Rolling').value;
	var stagedCreditMonth = configuration.getAttribute('Staged Credit Month').value;
	if (isNaN(stagedCreditMonth) || stagedCreditMonth == '') stagedCreditMonth = 0;
	var stagedCredit = configuration.getAttribute('Contract Term').value;
	if (isNaN(stagedCredit) || stagedCredit == '') stagedCredit = 0;
	if (stagedOrRolling == 'Staged' && Number(stagedCreditMonth) >= Number(stagedCredit)) {
		configuration.status = false;
		configuration.statusMessage = 'Staged Credit Month can not exceed Contract Term if Staged is selected';
	} else {
		if (configuration.statusMessage == 'Staged Credit Month can not exceed Contract Term if Staged is selected') {
			configuration.status = true;
			configuration.errorMessage = null;
		}
	}
}


async function updateRateCardAttribute(RateCardID, totalCharge, totalCost) {
	console.log('RateCardID' + RateCardID);
	var solution = await CS.SM.getActiveSolution();
	var servicePlanConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	updateConfigurationAttributeValue('Rate Card Lookup', RateCardID, RateCardID, true, solution, servicePlanConfiguration.guid, true);
	updateConfigurationAttributeValue('Custom Caller Total Estimated Charge', totalCharge, totalCharge, true, solution, servicePlanConfiguration.guid, true);
	updateConfigurationAttributeValue('Custom Caller Total Estimated Cost', totalCost, totalCost, true, solution, servicePlanConfiguration.guid, true);

}

async function validatePerfTrackerDivProduct(solution, configuration) {
    //var correctAddonExists = false;
    var perfGroupExists = false;
    var perfDivExists = false;
    var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	/*for (let relProduct of Object.values(mainConfiguration.relatedProducts)) {
	    if (relProduct.name == FUTURE_MOBILE_CONST.sharedVAS) {
			for (let attribute of Object.values(relProduct.configuration.attributes)) {
			    if (attribute.name == 'Product Name' && attribute.value == 'Performance Tracker Group') {
			        perfGroupExists = true;
			    }
			    if (attribute.name == 'Product Name' && attribute.value == 'Performance Tracker Divisional') {
			        perfDivExists = true;
			    }
			}
	    }
    }*/
    console.log('validatePerfTrackerDivProduct', perfGroupExists, perfDivExists);
    if (!perfGroupExists && perfDivExists) {
        configuration.status = false;
		configuration.statusMessage = (configuration.statusMessage != null && configuration.statusMessage != '') ? ((!configuration.statusMessage.includes(' Performance Tracker Divisional Can only be taken if Performance Tracker Group is taken.')) ? configuration.statusMessage + ' Performance Tracker Divisional Can only be taken if Performance Tracker Group is taken.' : configuration.statusMessage) : ' Performance Tracker Divisional Can only be taken if Performance Tracker Group is taken.';
    } else {
		if (configuration.statusMessage.includes(' Performance Tracker Divisional Can only be taken if Performance Tracker Group is taken.')) {
            configuration.statusMessage = configuration.statusMessage.replace(' Performance Tracker Divisional Can only be taken if Performance Tracker Group is taken.', '');
            if (configuration.statusMessage == null || configuration.statusMessage == '') {
                configuration.status = true;
                configuration.errorMessage = null;
            }
        } else {
            if (configuration.statusMessage == null || configuration.statusMessage == '') {
                configuration.status = true;
                configuration.errorMessage = null;
            }
        }
		/*for (let relProduct of Object.values(mainConfiguration.relatedProducts)) {
    	    if (relProduct.name == FUTURE_MOBILE_CONST.sharedVAS) {
    	        if (relProduct.configuration.statusMessage.includes(' Performance Tracker Divisional Can only be taken if Performance Tracker Group is taken.')) {
                    relProduct.configuration.statusMessage = relProduct.configuration.statusMessage.replace(' Performance Tracker Divisional Can only be taken if Performance Tracker Group is taken.', '');
                    if (relProduct.configuration.statusMessage == null || relProduct.configuration.statusMessage == '') {
                        relProduct.configuration.status = true;
                        relProduct.configuration.errorMessage = null;
                    }
                }
    	    }
        }*/
    }
}

async function validateWanderaProduct(solution, configuration) {
    var wanderaProductExists = false;
    var quantityLessthanTen = false;
    //var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	/*for (let relProduct of Object.values(mainConfiguration.relatedProducts)) {
	    if (relProduct.name == FUTURE_MOBILE_CONST.sharedVAS && relProduct.guid == configuration.guid) {
			for (let attribute of Object.values(relProduct.configuration.attributes)) {
			    if (attribute.name == 'Product Name' && attribute.value.includes('Wandera')) {
			        wanderaProductExists = true;
			    }
			    if (attribute.name == 'Quantity' && attribute.value < 10) {
			        quantityLessthanTen = true;
			    }
			}
	    }
    }*/
    console.log('validateWanderaProduct', wanderaProductExists, quantityLessthanTen);
    if (wanderaProductExists && quantityLessthanTen) {
        configuration.status = false;
        configuration.statusMessage = (configuration.statusMessage != null && configuration.statusMessage != '') ? ((!configuration.statusMessage.includes(' Minimum Quanity of 10 is required for Wandera.')) ? configuration.statusMessage + ' Minimum Quanity of 10 is required for Wandera.' : configuration.statusMessage) : ' Minimum Quanity of 10 is required for Wandera.';
    } else {
        if (configuration.statusMessage.includes(' Minimum Quanity of 10 is required for Wandera.')) {
            configuration.statusMessage = configuration.statusMessage.replace(' Minimum Quanity of 10 is required for Wandera.', '');
            if (configuration.statusMessage == null || configuration.statusMessage == '') {
                configuration.status = true;
                configuration.errorMessage = null;
            }
        } else {
            if (configuration.statusMessage == null || configuration.statusMessage == '') {
                configuration.status = true;
                configuration.errorMessage = null;
            }
        }
    }
}

async function validateSharedVASContractTerm(solution, configuration) {
    var contractTypeMCP = false;
    var contractTermValue;
    var mincontractTermValue;
	for (let attribute of Object.values(configuration.attributes)) {
	    if (attribute.name == 'Contract Type' && attribute.value.includes('MCP')) {
	        contractTypeMCP = true;
	    }
	    if (attribute.name == 'Contract Term') {
	        contractTermValue = Number(attribute.value);
	    }
	    if (attribute.name == 'Minimum Contract Term') {
	        if (attribute.value == '12 Months') {
	            mincontractTermValue = 12;
	        } else if (attribute.value == '24 Months') {
	            mincontractTermValue = 24;
	        } else if (attribute.value == '36 Months') {
	            mincontractTermValue = 36;
	        } else if (attribute.value == '48 Months') {
	            mincontractTermValue = 48;
	        } else if (attribute.value == '60 Months') {
	            mincontractTermValue = 60;
	        }
	    }
	}
    console.log('validateSharedVASContractTerm', contractTypeMCP, contractTermValue, mincontractTermValue);
    if (contractTypeMCP && !(contractTermValue >= mincontractTermValue && contractTermValue <= 60)) {
        configuration.status = false;
        configuration.statusMessage = (configuration.statusMessage != null && configuration.statusMessage != '') ? ((!configuration.statusMessage.includes(' Contract Term should be between ' + mincontractTermValue + ' and 60.')) ? configuration.statusMessage + ' Contract Term should be between ' + mincontractTermValue + ' and 60.' : configuration.statusMessage) : ' Contract Term should be between ' + mincontractTermValue + ' and 60.';
    } else {
        if (configuration.statusMessage.includes(' Contract Term should be between ' + mincontractTermValue + ' and 60.')) {
            configuration.statusMessage = configuration.statusMessage.replace(' Contract Term should be between ' + mincontractTermValue + ' and 60.', '');
            console.log('configuration.statusMessage', configuration.statusMessage, configuration.statusMessage == null, configuration.statusMessage == '');
            if (configuration.statusMessage == null || configuration.statusMessage == '') {
                configuration.status = true;
                configuration.errorMessage = null;
            }
        } else {
            if (configuration.statusMessage == null || configuration.statusMessage == '') {
                configuration.status = true;
                configuration.errorMessage = null;
            }
        }
    }
}

async function validateOneOffChargeORCost(solution, configuration, attrValue) {
    var minOneOffCharge = 0;
    var maxOneOffCharge = 0;
    var isBespokeProduct = false;
    var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	for (let relProduct of Object.values(mainConfiguration.relatedProducts)) {
	    if (relProduct.name == FUTURE_MOBILE_CONST.bespokeSolution && relProduct.guid == configuration.guid) {
	            isBespokeProduct = true;
        	for (let attribute of Object.values(relProduct.configuration.attributes)) {
        	    if (attribute.name == 'Minimum OneOff Border') {
        	        minOneOffCharge = Number(attribute.value);
        	    }
        	    if (attribute.name == 'Maximum OneOff Border') {
        	        maxOneOffCharge = Number(attribute.value);
        	    }
        	}
	    }
	}
	if(isBespokeProduct) {
    	if (Number(attrValue) >= minOneOffCharge && Number(attrValue) <= maxOneOffCharge) {
    	    if (configuration.statusMessage.includes(' You cannot sell for this price.')) {
                configuration.statusMessage = configuration.statusMessage.replace(' You cannot sell for this price.', '');
                if (configuration.statusMessage == null || configuration.statusMessage == '') {
                    configuration.status = true;
                    configuration.errorMessage = null;
                }
            } else {
                if (configuration.statusMessage == null || configuration.statusMessage == '') {
                    configuration.status = true;
                    configuration.errorMessage = null;
                }
            }
    	} else {
    	    configuration.status = false;
            configuration.statusMessage = (configuration.statusMessage != null && configuration.statusMessage != '') ? ((!configuration.statusMessage.includes(' You cannot sell for this price.')) ? configuration.statusMessage + ' You cannot sell for this price.' : configuration.statusMessage) : ' You cannot sell for this price.';
    	}
	}
}

async function validateAnnualRecurringChargeORCost(solution, configuration, attrValue) {
    var minRecurringCharge = 0;
    var maxRecurringCharge = 0;
    var isBespokeProduct = false;
    var mainConfiguration = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	for (let relProduct of Object.values(mainConfiguration.relatedProducts)) {
	    if (relProduct.name == FUTURE_MOBILE_CONST.bespokeSolution && relProduct.guid == configuration.guid) {
	        isBespokeProduct = true;
        	for (let attribute of Object.values(relProduct.configuration.attributes)) {
        	    if (attribute.name == 'Minimum Recurring Border') {
        	        minRecurringCharge = Number(attribute.value);
        	    }
        	    if (attribute.name == 'Maximum Recurring Border') {
        	        maxRecurringCharge = Number(attribute.value);
        	    }
        	}
	    }
	}
	if(isBespokeProduct) {
    	if (Number(attrValue) >= minRecurringCharge && Number(attrValue) <= maxRecurringCharge) {
    	    if (configuration.statusMessage.includes(' You cannot sell for this price.')) {
                configuration.statusMessage = configuration.statusMessage.replace(' You cannot sell for this price.', '');
                if (configuration.statusMessage == null || configuration.statusMessage == '') {
                    configuration.status = true;
                    configuration.errorMessage = null;
                }
            } else {
                if (configuration.statusMessage == null || configuration.statusMessage == '') {
                    configuration.status = true;
                    configuration.errorMessage = null;
                }
            }
    	} else {
    	    configuration.status = false;
            configuration.statusMessage = (configuration.statusMessage != null && configuration.statusMessage != '') ? ((!configuration.statusMessage.includes(' You cannot sell for this price.')) ? configuration.statusMessage + ' You cannot sell for this price.' : configuration.statusMessage) : ' You cannot sell for this price.';
    	}
	}
}

//paper bill
async function autoAddAccountBill(solution, attributeValue) {
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	if (attributeValue == 'Yes') {
		var correctAddonExists = false;
		for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
			if (relatedProduct.name.includes('Fully Itemised Paper Bill')) {
				correctAddonExists = true;
			}
		}
		if (!correctAddonExists) {
			availableAddons = await solution.getAddonsForConfiguration(mainConfiguration.guid);
			for (let segment of Object.values(availableAddons)) {
				for (let oneAddon of Object.values(segment.allAddons)) {
					if (oneAddon.Add_On_Name__c == 'Fully Itemised Paper Bill') {
						await addAddOnProductConfiguration('Account Level Add-Ons', oneAddon, mainConfiguration.guid, 1, solution);
					}
				}
			}
		}
	}
	if (attributeValue == 'No') {
		for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
			if (relatedProduct.name == 'Fully Itemised Paper Bill') {
				await deleteAddOns(solution, mainConfiguration, relatedProduct.guid, true);
			}
		}
	}
}

/*async function autoAddAccountBillVAS(solution, attributeValue) {
    let currentBasket = await CS.SM.getActiveBasket();
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	if (attributeValue == 'Yes') {
		var correctAddonExists = false;
		var accountBillConfig = false;
		var servicePlanId = mainConfiguration.getAttribute('Service Plan').value;
		var noOfUsers = Number(mainConfiguration.getAttribute('Total Number of Users').value);
		var servicePlanContractTerm = Number(mainConfiguration.getAttribute('Contract Term').value);
		if (noOfUsers > 0) {
		    for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
    			if (relatedProduct.groupName == 'Shared VAS' && relatedProduct.name == 'Shared VAS') {
    				for (let sharedVASAttribute of Object.values(relatedProduct.configuration.attributes)) {
    				    if (sharedVASAttribute.name == 'Group' && sharedVASAttribute.displayValue == 'Account Bill') {
    				        accountBillConfig = true;
    				    }
    				    if (accountBillConfig) {
    				        correctAddonExists = true;
    				    }
    				}
    				if (accountBillConfig && !correctAddonExists) {
    				    await solution.deleteRelatedProduct(mainConfiguration.guid, relatedProduct.guid);
    				}
    			}
    		}
    		if (!correctAddonExists) {
        		var result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
        			'action': 'getSharedVAS',
        			'servicePlanId': servicePlanId,
        			'addOnType': 'All',
        			'addOngroup': 'Account Bill',
        			'noOfUsers': noOfUsers
        		});
        		console.log('Account Bill VAS', result, result != null, result != '', Object.keys(result).length > 0);
        		if (Object.keys(result).length > 0) {
            		var group = result["Group"];
            		var groupName = result["GroupName"];
            		var product = result["Product"];
            		var productName = result["ProductName"];
            		var recurringCharge = result["RecurringCharge"];
            		var oneOffCharge = result["OneOffCharge"];
            		var productBillingFrequency = result["ProductBillingFrequency"];
            		var minConnections = result['MinimumConnections'];
            		var maxConnections = result['MaximumConnections'];
            		var contractType = result['ContractType'];
            		var minContractTerm = result["MinContractTerm"];
            		var billFrequency;
            		var contractTerm;
        		    if (recurringCharge == 0 || recurringCharge == undefined) {
        		        billFrequency = 'One Off';
        		    } else {
        		        if (productBillingFrequency == 'Annually') {
            		        billFrequency = 'Annual';
            		    } else {
            		        billFrequency = 'Monthly';
            		    }
        		    }
        		    if (contractType == 'No Term') {
    				    contractTerm = 1;
    			    }
    				else if (contractType == 'MCP') {
    				    //contractTerm = 24;
    				}
    				else if (contractType == 'MAT') {
    				    contractTerm = servicePlanContractTerm;
    				}
    				if (contractType != null && contractType != '' && contractTerm != undefined) {
            		    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Contract Term", value: {value: contractTerm, displayValue: contractTerm}, readOnly: true}, {name: "Minimum Contract Term", value: {value: minContractTerm, displayValue: minContractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
            		    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (accountBillPrd => console.log('accountBillPrd', accountBillPrd));
    				} else {
    				    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Minimum Contract Term", value: {value: minContractTerm, displayValue: minContractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
            		    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (accountBillPrd => console.log('accountBillPrd', accountBillPrd));
    				}
				}
    		}
		}
	}
	if (attributeValue == 'No' || attributeValue == '') {
		for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
			if (relatedProduct.groupName == 'Shared VAS' && relatedProduct.name == 'Shared VAS') {
			    for (let sharedVASAttribute of Object.values(relatedProduct.configuration.attributes)) {
				    if (sharedVASAttribute.name == 'Group' && sharedVASAttribute.displayValue == 'Account Bill') {
				        await solution.deleteRelatedProduct(mainConfiguration.guid, relatedProduct.guid);
				    }
			    }
			}
		}
	}
}*/

/*async function autoAddDataCollectorVAS(solution, attributeValue) {
    let currentBasket = await CS.SM.getActiveBasket();
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	if (attributeValue == 'Yes') {
		var correctAddonExists = false;
		var dataCollectionConfig = false;
		var minConnectionsOk = false;
		var maxConnectionsOk = false;
		var servicePlanId = mainConfiguration.getAttribute('Service Plan').value;
		var noOfUsers = Number(mainConfiguration.getAttribute('Total Number of Users').value);
		var servicePlanContractTerm = Number(mainConfiguration.getAttribute('Contract Term').value);
		if (noOfUsers > 0) {
		    for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
    			if (relatedProduct.groupName == 'Shared VAS' && relatedProduct.name == 'Shared VAS') {
    				for (let sharedVASAttribute of Object.values(relatedProduct.configuration.attributes)) {
    				    if (sharedVASAttribute.name == 'Group' && sharedVASAttribute.displayValue == 'Data Collection') {
    				        dataCollectionConfig = true;
    				    }
    				    if (sharedVASAttribute.name == 'Minimum Connections' && noOfUsers >= sharedVASAttribute.value) {
    				        minConnectionsOk = true;
    				    }
    				    if (sharedVASAttribute.name == 'Maximum Connections' && noOfUsers <= sharedVASAttribute.value) {
    				        maxConnectionsOk = true;
    				    }
    				    if (dataCollectionConfig && minConnectionsOk && maxConnectionsOk) {
    				        correctAddonExists = true;
    				    }
    				}
    				if (dataCollectionConfig && !correctAddonExists) {
    				    await solution.deleteRelatedProduct(mainConfiguration.guid, relatedProduct.guid);
    				}
    			}
    		}
    		if (!correctAddonExists) {
        		var result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
        			'action': 'getSharedVAS',
        			'servicePlanId': servicePlanId,
        			'addOnType': 'All',
        			'addOngroup': 'Data Collection',
        			'noOfUsers': noOfUsers
        		});
        		console.log('DataCollection VAS', result, result != null, result != '', Object.keys(result).length > 0);
        		if (Object.keys(result).length > 0) {
            		var group = result["Group"];
            		var groupName = result["GroupName"];
            		var product = result["Product"];
            		var productName = result["ProductName"];
            		var recurringCharge = result["RecurringCharge"];
            		var oneOffCharge = result["OneOffCharge"];
            		var productBillingFrequency = result["ProductBillingFrequency"];
            		var minConnections = result['MinimumConnections'];
            		var maxConnections = result['MaximumConnections'];
            		var contractType = result['ContractType'];
            		var minContractTerm = result["MinContractTerm"];
            		var billFrequency;
            		var contractTerm;
            		//const sharedVASRelPrdConfig;
        		    if (recurringCharge == 0 || recurringCharge == undefined) {
        		        billFrequency = 'One Off';
        		    } else {
        		        if (productBillingFrequency == 'Annually') {
            		        billFrequency = 'Annual';
            		    } else {
            		        billFrequency = 'Monthly';
            		    }
        		    }
        		    if (contractType == 'No Term') {
    				    contractTerm = 1;
    			    } else if (contractType == 'MCP') {
    				    //contractTerm = 24;
    				} else if (contractType == 'MAT') {
    				    contractTerm = servicePlanContractTerm;
    				}
    				if (contractType != null && contractType != '' && contractTerm != undefined) {
            		    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Contract Term", value: {value: contractTerm, displayValue: contractTerm}, readOnly: true}, {name: "Minimum Contract Term", value: {value: minContractTerm, displayValue: minContractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
            		    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (dataCollectionPrd => console.log('dataCollectionPrd', dataCollectionPrd));
    				} else {
    				    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Minimum Contract Term", value: {value: minContractTerm, displayValue: minContractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
    				    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (dataCollectionPrd => console.log('dataCollectionPrd', dataCollectionPrd));           		
    				}
        		}
    		}
		}
	}
	if (attributeValue == 'No' || attributeValue == '') {
		for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
			if (relatedProduct.groupName == 'Shared VAS' && relatedProduct.name == 'Shared VAS') {
			    for (let sharedVASAttribute of Object.values(relatedProduct.configuration.attributes)) {
				    if (sharedVASAttribute.name == 'Group' && sharedVASAttribute.displayValue == 'Data Collection') {
				        await solution.deleteRelatedProduct(mainConfiguration.guid, relatedProduct.guid);
				    }
			    }
			}
		}
	}
}*/

async function autoAddTotalResourceVAS(solution, attributeValue) {
    let currentBasket = await CS.SM.getActiveBasket();
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	if (attributeValue == 'Annual' || attributeValue == 'Monthly') {
		var correctAddonExists = false;
		var totalResourceGroupMatch = false;
		var totalResourceValueMatch = false;
		var minConnectionsOk = false;
		var maxConnectionsOk = false;
		var servicePlanId = mainConfiguration.getAttribute('Service Plan').value;
		var noOfUsers = Number(mainConfiguration.getAttribute('Total Number of Users').value);
		var servicePlanContractTerm = Number(mainConfiguration.getAttribute('Contract Term').value);
		if (noOfUsers >= 250) {
    	    if (mainConfiguration.statusMessage.includes(' You need 250 or more Subscriptions for Total Resource - please add more')) {
                mainConfiguration.statusMessage = mainConfiguration.statusMessage.replace(' You need 250 or more Subscriptions for Total Resource - please add more', '');
                if (mainConfiguration.statusMessage == null || mainConfiguration.statusMessage == '') {
                    mainConfiguration.status = true;
                    mainConfiguration.errorMessage = null;
                }
            } else {
                if (mainConfiguration.statusMessage == null || mainConfiguration.statusMessage == '') {
                    mainConfiguration.status = true;
                    mainConfiguration.errorMessage = null;
                }
            }
    	} else {
    	    mainConfiguration.status = false;
            mainConfiguration.statusMessage = (mainConfiguration.statusMessage != null && mainConfiguration.statusMessage != '') ? ((!mainConfiguration.statusMessage.includes(' You need 250 or more Subscriptions for Total Resource - please add more')) ? mainConfiguration.statusMessage + ' You need 250 or more Subscriptions for Total Resource - please add more' : mainConfiguration.statusMessage) : ' You need 250 or more Subscriptions for Total Resource - please add more';
    	}
		if (noOfUsers > 0) {
		    /*for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
    			if (relatedProduct.groupName == 'Shared VAS' && relatedProduct.name == 'Shared VAS') {
    				for (let sharedVASAttribute of Object.values(relatedProduct.configuration.attributes)) {
    				    if (sharedVASAttribute.name == 'Group' && sharedVASAttribute.displayValue == 'Total Resource') {
    				        totalResourceGroupMatch = true;
    				    }
    				    if (sharedVASAttribute.name == 'Add Total Resource' && sharedVASAttribute.value == attributeValue) {
    				        totalResourceValueMatch = true;
    				    }
    				    if (sharedVASAttribute.name == 'Minimum Connections' && noOfUsers >= sharedVASAttribute.value) {
    				        minConnectionsOk = true;
    				    }
    				    if (sharedVASAttribute.name == 'Maximum Connections' && noOfUsers <= sharedVASAttribute.value) {
    				        maxConnectionsOk = true;
    				    }
    				    if (totalResourceGroupMatch && totalResourceValueMatch && minConnectionsOk && maxConnectionsOk) {
    				        correctAddonExists = true;
    				    }
    				}
    				if (totalResourceGroupMatch && !correctAddonExists) {
    				    await solution.deleteRelatedProduct(mainConfiguration.guid, relatedProduct.guid);
    				}
    			}
    		}
    		if (!correctAddonExists) {
        		var result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
        			'action': 'getSharedVAS',
        			'servicePlanId': servicePlanId,
        			'addOnType': attributeValue,
        			'addOngroup': 'Total Resource',
        			'noOfUsers': noOfUsers
        		});
        		console.log('TotalResource VAS', result, result != null, result != '', Object.keys(result).length > 0);
        		if (Object.keys(result).length > 0) {
            		var group = result["Group"];
            		var groupName = result["GroupName"];
            		var product = result["Product"];
            		var productName = result["ProductName"];
            		var recurringCharge = result["RecurringCharge"];
            		var oneOffCharge = result["OneOffCharge"];
            		var productBillingFrequency = result["ProductBillingFrequency"];
            		var minConnections = result['MinimumConnections'];
            		var maxConnections = result['MaximumConnections'];
            		var contractType = result['ContractType'];
            		var minContractTerm = result["MinContractTerm"];
            		var billFrequency;
            		var contractTerm;
        		    if (recurringCharge == 0 || recurringCharge == undefined) {
        		        billFrequency = 'One Off';
        		    } else {
        		        if (productBillingFrequency == 'Annually') {
            		        billFrequency = 'Annual';
            		    } else {
            		        billFrequency = 'Monthly';
            		    }
        		    }
        		    if (contractType == 'No Term') {
    				    contractTerm = 1;
    			    }
    				else if (contractType == 'MCP') {
    				    //contractTerm = 24;
    				}
    				else if (contractType == 'MAT') {
    				    contractTerm = servicePlanContractTerm;
    				}
    				if (contractType != null && contractType != '' && contractTerm != undefined) {
            		    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Contract Term", value: {value: contractTerm, displayValue: contractTerm}, readOnly: true}, {name: "Minimum Contract Term", value: {value: minContractTerm, displayValue: minContractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
            		    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (totalResourcePrd => console.log('totalResourcePrd', totalResourcePrd));
    				} else {
    				    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Minimum Contract Term", value: {value: minContractTerm, displayValue: minContractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
            		    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (totalResourcePrd => console.log('totalResourcePrd', totalResourcePrd));
    				}
        		}
    		}*/
		}
	}
	if (attributeValue == 'NA' || attributeValue == '') {
	    /*for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
			if (relatedProduct.groupName == 'Shared VAS' && relatedProduct.name == 'Shared VAS') {
			    for (let sharedVASAttribute of Object.values(relatedProduct.configuration.attributes)) {
				    if (sharedVASAttribute.name == 'Group' && sharedVASAttribute.displayValue == 'Total Resource') {
				        await solution.deleteRelatedProduct(mainConfiguration.guid, relatedProduct.guid);
				    }
			    }
			}
		}*/
		if (mainConfiguration.statusMessage.includes(' You need 250 or more Subscriptions for Total Resource - please add more')) {
            mainConfiguration.statusMessage = mainConfiguration.statusMessage.replace(' You need 250 or more Subscriptions for Total Resource - please add more', '');
            if (mainConfiguration.statusMessage == null || mainConfiguration.statusMessage == '') {
                mainConfiguration.status = true;
                mainConfiguration.errorMessage = null;
            }
        } else {
            if (mainConfiguration.statusMessage == null || mainConfiguration.statusMessage == '') {
                mainConfiguration.status = true;
                mainConfiguration.errorMessage = null;
            }
        }
	}
}

async function autoAddTailoredServiceVAS(solution) {
    let currentBasket = await CS.SM.getActiveBasket();
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	var correctAddonExists = false;
	var tailoredServiceGroupMatch = false;
	var tailoredServiceValueMatch = false;
	var minConnectionsOk = false;
	var maxConnectionsOk = false;
	var servicePlanId = mainConfiguration.getAttribute('Service Plan').value;
	var noOfUsers = Number(mainConfiguration.getAttribute('Total Number of Users').value);
	var servicePlanContractTerm = Number(mainConfiguration.getAttribute('Contract Term').value);
	if (noOfUsers > 0) {
	    /*for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
			if (relatedProduct.groupName == 'Shared VAS' && relatedProduct.name == 'Shared VAS') {
				for (let sharedVASAttribute of Object.values(relatedProduct.configuration.attributes)) {
				    if (sharedVASAttribute.name == 'Group' && sharedVASAttribute.displayValue == 'Tailored Service') {
				        tailoredServiceGroupMatch = true;
				    }
				    if (sharedVASAttribute.name == 'Shared VAS' && sharedVASAttribute.displayValue.includes('Tailored Service')) {
				        tailoredServiceValueMatch = true;
				    }
				    if (sharedVASAttribute.name == 'Minimum Connections' && noOfUsers >= sharedVASAttribute.value) {
				        minConnectionsOk = true;
				    }
				    if (sharedVASAttribute.name == 'Maximum Connections' && noOfUsers <= sharedVASAttribute.value) {
				        maxConnectionsOk = true;
				    }
				    if (tailoredServiceGroupMatch && tailoredServiceValueMatch && minConnectionsOk && maxConnectionsOk) {
				        correctAddonExists = true;
				    }
				}
				if (tailoredServiceGroupMatch && !correctAddonExists) {
				    await solution.deleteRelatedProduct(mainConfiguration.guid, relatedProduct.guid);
				}
			}
		}
		if (!correctAddonExists) {
    		var result = await currentBasket.performRemoteAction('CVRemoteActionsSolutionConsole', {
    			'action': 'getSharedVAS',
    			'servicePlanId': servicePlanId,
    			'addOnType': 'All',
    			'addOngroup': 'Tailored Service',
    			'noOfUsers': noOfUsers
    		});
    		console.log('Tailored Service VAS', result, result != null, result != '', Object.keys(result).length);
    		if (Object.keys(result).length > 0) {
        		var group = result["Group"];
        		var groupName = result["GroupName"];
        		var product = result["Product"];
        		var productName = result["ProductName"];
        		var recurringCharge = result["RecurringCharge"];
        		var oneOffCharge = result["OneOffCharge"];
        		var productBillingFrequency = result["ProductBillingFrequency"];
        		var minConnections = result['MinimumConnections'];
        		var maxConnections = result['MaximumConnections'];
        		var contractType = result['ContractType'];
        		var minContractTerm = result["MinContractTerm"];
        		var billFrequency;
        		var contractTerm;
    		    if (recurringCharge == 0 || recurringCharge == undefined) {
    		        billFrequency = 'One Off';
    		    } else {
    		        if (productBillingFrequency == 'Annually') {
        		        billFrequency = 'Annual';
        		    } else {
        		        billFrequency = 'Monthly';
        		    }
    		    }
    		    if (contractType == 'No Term') {
				    contractTerm = 1;
			    }
				else if (contractType == 'MCP') {
				    //contractTerm = 24;
				}
				else if (contractType == 'MAT') {
				    contractTerm = servicePlanContractTerm;
				}
				if (contractType != null && contractType != '' && contractTerm != undefined) {
        		    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Contract Term", value: {value: contractTerm, displayValue: contractTerm}, readOnly: true}, {name: "Minimum Contract Term", value: {value: mincontractTerm, displayValue: mincontractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
        		    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (totalResourcePrd => console.log('tailoredServiceVAS', totalResourcePrd));
				} else {
				    const sharedVASRelPrdConfig = await solution.createRelatedProduct('Shared VAS', [{name: "Group", value: {value: group, displayValue: groupName}, readOnly: true}, {name: "Shared VAS", value: {value: product, displayValue: productName}, readOnly: true}, {name: "Recurring Charge", value: {value: recurringCharge, displayValue: recurringCharge}}, {name: "One Off Charge", value: {value: oneOffCharge, displayValue: oneOffCharge}}, {name: "Product Billing Frequency", value: {value: productBillingFrequency, displayValue: productBillingFrequency}}, {name: "Bill Frequency", value: {value: billFrequency, displayValue: billFrequency}}, {name: "Minimum Connections", value: {value: minConnections, displayValue: minConnections}}, {name: "Maximum Connections", value: {value: maxConnections, displayValue: maxConnections}}, {name: "Contract Type", value: {value: contractType, displayValue: contractType}}, {name: "Minimum Contract Term", value: {value: mincontractTerm, displayValue: mincontractTerm}, readOnly: true}, {name: "Total Number of Users", value: {value: noOfUsers, displayValue: noOfUsers}}]);
        		    await solution.addRelatedProduct(mainConfiguration.guid, sharedVASRelPrdConfig).then (totalResourcePrd => console.log('tailoredServiceVAS', totalResourcePrd));
				}
    		}
		}*/
	}
}

async function autoAddSIM(solution, configParam) {
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	var connectionType = mainConfiguration.getAttribute('Connection Type').displayValue;

	if (!configParam) {
		for (let component of Object.values(solution.components)) {
			if (component.name == FUTURE_MOBILE_CONST.userGroups) {
				for (let configuration of Object.values(component.schema.configurations)) {
					if (!isValidAutoAddOn(configuration)) {
						continue;
					}

					var deviceType = configuration.getAttribute('Device Type').displayValue;
					if (deviceType.includes('SIM Only')) {
						var simGrp = configuration.getAttribute('eSIM Required Picklist').value == 'Yes' ? 'eSIM' : 'SIM';
						if (connectionType != '' && configuration.getAttribute('Subscription Type').value != '') {
							var correctAddonExists = false;
							for (let relatedProduct of Object.values(configuration.relatedProductList)) {
								if (relatedProduct.Add_On_Type__c == connectionType && relatedProduct.cspmb__Group__c == simGrp) {
									correctAddonExists = true;
								} else {
									if (relatedProduct.groupName == 'SIM' || relatedProduct.groupName == 'eSIM')
										await deleteAddOns(component, configuration, relatedProduct.guid, true);
								}
							}
							if (!correctAddonExists) {
								var availableAddons = await solution.getAddonsForConfiguration(configuration.guid);
								for (let segment of Object.values(availableAddons)) {
									for (let oneAddon of Object.values(segment.allAddons)) {
										globalOneOffCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
										globalRecurringCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
										if (oneAddon.Add_On_Type__c == connectionType && oneAddon.cspmb__Group__c == simGrp) {
											try {
												for (let relatedProduct of Object.values(configuration.relatedProductList)) {
													if (relatedProduct.groupName == 'SIM' || relatedProduct.groupName == 'eSIM')
													await deleteAddOns(component, configuration, relatedProduct.guid, true);
												}
												await addAddOnProductConfiguration('Devices', oneAddon, configuration.guid, 1, component);
											} catch (exception) {
												console.log('this', exception);
											}
										}
									}
								}
							}
						}
					}
					if (deviceType.includes('Device Only')) {
						for (let relatedProduct of Object.values(configuration.relatedProductList)) {
							if (relatedProduct.groupName == 'SIM' || relatedProduct.groupName == 'eSIM')
								await deleteAddOns(component, configuration, relatedProduct.guid, true);
						}
					}
				}
			}
		}
	} else {
		if (!isValidAutoAddOn(configParam)) {
			return;
		}

		var configuration = configParam;
		var component = solution.getComponentByName('User Groups');

		var deviceType = configuration.getAttribute('Device Type').displayValue;
		if (deviceType.includes('SIM Only')) {
			var simGrp = configuration.getAttribute('eSIM Required Picklist').value == 'Yes' ? 'eSIM' : 'SIM';
			if (connectionType != '' && configuration.getAttribute('Subscription Type').value != '') {
				var correctAddonExists = false;
				for (let relatedProduct of Object.values(configuration.relatedProductList)) {
					if (relatedProduct.Add_On_Type__c == connectionType && relatedProduct.cspmb__Group__c == simGrp) {
						correctAddonExists = true;
					} else {
						if (relatedProduct.groupName == 'SIM' || relatedProduct.groupName == 'eSIM')
							await deleteAddOns(component, configuration, relatedProduct.guid, true);
					}
				}
				if (!correctAddonExists) {
					var availableAddons = await solution.getAddonsForConfiguration(configuration.guid);
					for (let segment of Object.values(availableAddons)) {
						for (let oneAddon of Object.values(segment.allAddons)) {
							globalOneOffCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
							globalRecurringCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
							if (oneAddon.Add_On_Type__c == connectionType && oneAddon.cspmb__Group__c == simGrp) {
								try {
									for (let relatedProduct of Object.values(configuration.relatedProductList)) {
										if (relatedProduct.groupName == 'SIM' || relatedProduct.groupName == 'eSIM')
										await deleteAddOns(component, configuration, relatedProduct.guid, true);
									}
									await addAddOnProductConfiguration('Devices', oneAddon, configuration.guid, 1, component);
								} catch (exception) {
									console.log('this', exception);
								}
							}
						}
					}
				}
			}
		}
		if (deviceType.includes('Device Only')) {
			for (let relatedProduct of Object.values(configuration.relatedProductList)) {
				if (relatedProduct.groupName == 'SIM' || relatedProduct.groupName == 'eSIM')
					await deleteAddOns(component, configuration, relatedProduct.guid, true);
			}
		}
	}
}

async function autoAddSharedAccessFee(solution, configParam) {
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	var servicePlan = mainConfiguration.getAttribute('Service Plan').displayValue;
	var servicePlanName = mainConfiguration.getAttribute('Service Plan Name').value;
	var customDataServicePlan = mainConfiguration.getAttribute('Custom Data Service Plan').value;

	if(!configParam) {
		for (let component of Object.values(solution.components)) {
			if (component.name == FUTURE_MOBILE_CONST.userGroups) {
				for (let configuration of Object.values(component.schema.configurations)) {
					if (!isValidAutoAddOn(configuration)) {
						continue;
					}

					if ((servicePlan == 'The Shared Plan' || (servicePlanName == 'Custom' && customDataServicePlan == 'Shared'))  && configuration.getAttribute('Subscription Type Name').value != '') { //if shared plans should exist
						var correctAddonExists = false;
						for (let relatedProduct of Object.values(configuration.relatedProductList)) {
							if (relatedProduct.name == 'Shared Access Fee') {
								correctAddonExists = true;
							} else {
								if (relatedProduct.name == 'Shared Access Fee')
									await deleteAddOns(component, configuration, relatedProduct.guid, true);
							}
						}
						if (!correctAddonExists) {
							var availableAddons = await solution.getAddonsForConfiguration(configuration.guid);
							for (let segment of Object.values(availableAddons)) {
								for (let oneAddon of Object.values(segment.allAddons)) {
									if (oneAddon.Add_On_Name__c == 'Shared Access Fee') {
										try {
											for (let relatedProduct of Object.values(configuration.relatedProductList)) {
												if (relatedProduct.name == 'Shared Access Fee')
												await deleteAddOns(component, configuration, relatedProduct.guid, true);
											}
											await addAddOnProductConfiguration('User Plans', oneAddon, configuration.guid, 1, component);
										} catch (exception) {
											console.log('this', exception);
										}
									}
								}
							}
						}
					} else { //if shared plans should not exist
						for (let relatedProduct of Object.values(configuration.relatedProductList)) {
							if (relatedProduct.name == 'Shared Access Fee') {
								await deleteAddOns(component, configuration, relatedProduct.guid, true);
							}
						}
					}
				}
			}
		}
	} else {
		if (!isValidAutoAddOn(configParam)) {
			return;
		}

		var configuration = configParam;
		var component = solution.getComponentByName('User Groups');

		if ((servicePlan == 'The Shared Plan' || (servicePlanName == 'Custom' && customDataServicePlan == 'Shared'))  && configuration.getAttribute('Subscription Type Name').value != '') { //if shared plans should exist
			var correctAddonExists = false;
			for (let relatedProduct of Object.values(configuration.relatedProductList)) {
				if (relatedProduct.name == 'Shared Access Fee') {
					correctAddonExists = true;
				} else {
					if (relatedProduct.name == 'Shared Access Fee')
						await deleteAddOns(component, configuration, relatedProduct.guid, true);
				}
			}
			if (!correctAddonExists) {
				var availableAddons = await solution.getAddonsForConfiguration(configuration.guid);
				for (let segment of Object.values(availableAddons)) {
					for (let oneAddon of Object.values(segment.allAddons)) {
						if (oneAddon.Add_On_Name__c == 'Shared Access Fee') {
							try {
								for (let relatedProduct of Object.values(configuration.relatedProductList)) {
									if (relatedProduct.name == 'Shared Access Fee')
									await deleteAddOns(component, configuration, relatedProduct.guid, true);
								}
								await addAddOnProductConfiguration('User Plans', oneAddon, configuration.guid, 1, component);
							} catch (exception) {
								console.log('this', exception);
							}
						}
					}
				}
			}
		} else { //if shared plans should not exist
			for (let relatedProduct of Object.values(configuration.relatedProductList)) {
				if (relatedProduct.name == 'Shared Access Fee') {
					await deleteAddOns(component, configuration, relatedProduct.guid, true);
				}
			}
		}
	}
}

function checkDevices(solution, configParam) {
	var deviceExists;
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			for (let configuration of Object.values(component.schema.configurations)) {
				if (configParam == null || configuration.guid == configParam.guid) {
					deviceExists = false;
					for (let relatedProduct of Object.values(configuration.relatedProductList)) {
						if (relatedProduct.relatedProductName == 'Devices') {
							deviceExists = true;
						}
					}
					updateConfigurationAttributeValue('Devices Added', deviceExists, deviceExists, true, solution, configuration.guid, true);
				}
			}
		}
	}
}

function calculateCreditFundsTwo(solution) {
	var totalAirtime = 0;
	var totalHardware = 0;
	var usedAirtime = 0;
	var usedHardware = 0;
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.creditFund) {
			for (let configuration of Object.values(component.schema.configurations)) {
				totalAirtime = Number(configuration.getAttribute('Total Airtime Fund Available').value);
				if (isNaN(totalAirtime)) totalAirtime = 0;
				totalHardware = Number(configuration.getAttribute('Total Hardware Fund Available').value);
				if (isNaN(totalHardware)) totalHardware = 0;

				var currentUsed = isNaN(configuration.getAttribute('Credit Amount').value) ? 0 : Number(configuration.getAttribute('Credit Amount').value);
				if (configuration.getAttribute('Credit Type').displayValue == 'Airtime') usedAirtime += currentUsed;
				if (configuration.getAttribute('Credit Type').value == 'Hardware') usedHardware += currentUsed;
			}
		}
	}
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.creditFund) {
			for (let configuration of Object.values(component.schema.configurations)) {
				updateConfigurationAttributeValue('Remaining Hardware Funds', Number(totalHardware - usedHardware).toFixed(2), Number(totalHardware - usedHardware).toFixed(2), true, solution, configuration.guid, true);
				updateConfigurationAttributeValue('Remaining Airtime Funds', Number(totalAirtime - usedAirtime).toFixed(2), Number(totalAirtime - usedAirtime).toFixed(2), true, solution, configuration.guid, true);
				if (configuration.getAttribute('Credit Type').value == 'Hardware') {
					netValue = parseFloat(configuration.getAttribute('Total Technology').value) - parseFloat(configuration.getAttribute('Credit Amount').value);
					//updateConfigurationAttributeValue('Total Technology Fund Shortage Overage', Number(netValue).toFixed(2), Number(netValue).toFixed(2), true, solution, configuration.guid, true);
				}
				
				if (correctProfile == 'true') {
					updateConfigurationAttributeValue('Remaining Airtime Funds Display', Number(totalAirtime - usedAirtime).toFixed(2), Number(totalAirtime - usedAirtime).toFixed(2), true, solution, configuration.guid, true);
				}
			}
		}
	}
}

function calculateUsersCount(solution) {
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	var contractTerm;
	var subCount = 0;
	var totalCharge = 0;
	var totalCost = 0;
	var qbs;
	var qbsNonIndividualUserGroups;
	var nonIndividualUserGroups = 0;
	var individualUserGroups = 0;
	var voiceIndividualUsers = 0;
	for (let component of Object.values(solution.components)) {
		for (let configuration of Object.values(component.schema.configurations)) {
			if (component.name == FUTURE_MOBILE_CONST.special) {
				contractTerm = Number(mainConfiguration.getAttribute('Contract Term').value);
			} else {
				contractTerm = Number(configuration.getAttribute('Contract Term').value);
			}
			if (isNaN(contractTerm)) contractTerm = 0;
			if (component.name == FUTURE_MOBILE_CONST.userGroups) {

				updateConfigurationAttributeValue('Quantity', configuration.configurationProperties.quantity, configuration.configurationProperties.quantity, true, solution, configuration.guid, false);

				qbs = Number(configuration.configurationProperties.quantity);
				subCount += qbs;

				if (!configuration.getAttribute('Individual Override').value) {
					qbsNonIndividualUserGroups = Number(configuration.configurationProperties.quantity);
					nonIndividualUserGroups += qbsNonIndividualUserGroups;

					if (configuration.getAttribute('Subscription Type Name').value.includes('Voice')) {
						voiceIndividualUsers += qbsNonIndividualUserGroups;
					}
				} else {
					qbsIndividualUserGroups = Number(configuration.configurationProperties.quantity);
					individualUserGroups += qbsIndividualUserGroups;
				}
			} else {
				qbs = 1;
			}
			for (let relatedProduct of Object.values(configuration.relatedProductList)) {
				if (!isNaN(Number(relatedProduct.recurringCharge))) {
					totalCharge += Number(relatedProduct.configuration.recurringPrice) * qbs * contractTerm;
					//totalCost+=Number(relatedProduct.configuration.recurringCost)*qbs*contractTerm;
				}
				if (!isNaN(Number(relatedProduct.configuration.oneOffPrice))) {
					totalCharge += Number(relatedProduct.configuration.oneOffPrice) * qbs;
					//totalCost+=Number(relatedProduct.configuration.oneOffCost)*qbs;
				}
			}
		}
	}
	contractTerm = Number(mainConfiguration.getAttribute('Contract Term').value);
	if (isNaN(contractTerm)) contractTerm = 0;

	for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
		if (!isNaN(Number(relatedProduct.recurringCharge))) {
			totalCharge += Number(relatedProduct.configuration.recurringPrice) * contractTerm;
			//totalCost+=Number(relatedProduct.configuration.recurringCost)*contractTerm;
		}
		if (!isNaN(Number(relatedProduct.configuration.oneOffPrice))) {
			totalCharge += Number(relatedProduct.configuration.oneOffPrice);
			//totalCost+=Number(relatedProduct.configuration.oneOffCost);
		}
	}
	updateConfigurationAttributeValue('Total Number of Users', subCount, subCount, true, solution, mainConfiguration.guid, false);
	updateConfigurationAttributeValue('Total Charge', totalCharge.toFixed(2), totalCharge.toFixed(2), true, solution, mainConfiguration.guid, true);
	updateConfigurationAttributeValue('Number of User for AddOn', voiceIndividualUsers, voiceIndividualUsers, true, solution, mainConfiguration.guid, true);
	updateConfigurationAttributeValue('Total Service Level Users', nonIndividualUserGroups, nonIndividualUserGroups, true, solution, mainConfiguration.guid, true);
	updateConfigurationAttributeValue('Total Individual Override', individualUserGroups, individualUserGroups, true, solution, mainConfiguration.guid, true);

	//updateConfigurationAttributeValue('Total Cost',totalCost.toFixed(2),totalCost.toFixed(2),true,solution,mainConfiguration.guid,true);
}
async function preloadAddonCostData(solution) {
	var needsToQuery;
	for (let component of Object.values(solution.components)) {
		for (let configuration of Object.values(component.schema.configurations)) {
			needsToQuery = false;
			for (let relatedProduct of Object.values(configuration.relatedProductList)) {
				if (globalOneOffCostMap[relatedProduct.name] == null || globalRecurringCostMap[relatedProduct.name] == null) {
					needsToQuery = true;
					break;
				}
			}
			if (needsToQuery) {
				let availableAddons = await component.getAddonsForConfiguration(configuration.guid);
				for (let segment of Object.values(availableAddons)) {
					for (let oneAddon of Object.values(segment.allAddons)) {
						globalOneOffCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
						globalRecurringCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
					}
				}
			}
		}
	}
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	for (let relatedProduct of Object.values(mainConfiguration.relatedProductList)) {
		if (globalOneOffCostMap[relatedProduct.name] == null || globalRecurringCostMap[relatedProduct.name] == null) {
			needsToQuery = true;
			break;
		}
	}
	if (needsToQuery) {
		let availableAddons = await solution.getAddonsForConfiguration(mainConfiguration.guid);
		for (let segment of Object.values(availableAddons)) {
			for (let oneAddon of Object.values(segment.allAddons)) {
				globalOneOffCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__One_Off_Cost__c;
				globalRecurringCostMap[oneAddon.Add_On_Name__c] = oneAddon.cspmb__Add_On_Price_Item__r.cspmb__Recurring_Cost__c;
			}
		}
	}
}

function reloadUIafterSolutionLoadedFM(solution) {
	var mainConfiguration = Object.values(solution.schema.configurations)[0];
	servicePlan = mainConfiguration.getAttribute('Service Plan').displayValue;
	customDataServicePlan = mainConfiguration.getAttribute('Custom Data Service Plan').displayValue;

	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			for (let configuration of Object.values(component.schema.configurations)) {
				updateConfigNameFM(configuration, component.name, );
			}
		}

		if (component.name == FUTURE_MOBILE_CONST.mainComponent) {
			if (servicePlan != 'Custom Caller') {
				updateData = [{
					name: 'Custom Data Service Plan',
					value: 'N/A',
					displayValue: 'N/A',
					readOnly: true,
					required: false
				}];
				if (mainConfiguration.getAttribute('Custom Data Service Plan') == '')
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				updateData = [{
					name: 'Custom Voice Rate Card',
					value: 'N/A',
					displayValue: 'N/A',
					readOnly: true,
					required: false
				}];
				if (mainConfiguration.getAttribute('Custom Voice Rate Card') == '')
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
			}
			if (servicePlan != 'The Team Plan') {
				updateData = [{
					name: 'Variant',
					value: 'N/A',
					displayValue: 'N/A',
					readOnly: true,
					required: false
				}];
				if (mainConfiguration.getAttribute('Variant') == '')
					component.updateConfigurationAttribute(configuration.guid, updateData, true);
				updateData = [{
					name: 'Team Voice Plan',
					value: 'N/A',
					displayValue: 'N/A',
					readOnly: true,
					required: false
				}];
				if (mainConfiguration.getAttribute('Team Voice Plan') == '')
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				updateData = [{
					name: 'Team Voice Scheme',
					value: 'N/A',
					displayValue: 'N/A',
					readOnly: true,
					required: false
				}];
				if (mainConfiguration.getAttribute('Team Voice Scheme') == '')
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
			}
			if (servicePlan != 'The Team Plan' && customDataServicePlan != 'Team') {
				updateData = [{
					name: 'Team Data Plan',
					value: 'N/A',
					displayValue: 'N/A',
					readOnly: true,
					required: false
				}];
				if (mainConfiguration.getAttribute('Team Data Plan') == '')
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
				updateData = [{
					name: 'Team Data Scheme',
					value: 'N/A',
					displayValue: 'N/A',
					readOnly: true,
					required: false
				}];
				if (mainConfiguration.getAttribute('Team Data Scheme') == '')
					component.updateConfigurationAttribute(configuration.guid, updateData, false);
			}
		}
		if (component.name == FUTURE_MOBILE_CONST.creditFund) {
			for (let configuration of Object.values(component.schema.configurations)) {
				updateConfigurationAttributeValue('Total Airtime Fund Available Display', 'N/A', 'N/A', true, solution, configuration.guid, true);
				updateConfigurationAttributeValue('Remaining Airtime Funds Display', 'N/A', 'N/A', true, solution, configuration.guid, true);
				if (correctProfile != 'true') {
					let currentType = configuration.getAttribute('Credit Type').displayValue;
					updateConfigurationAttributeValue('Credit Type', 'N/A', 'N/A', false, solution, configuration.guid, true);
					updateConfigurationAttributeValue('Credit Type', currentType, currentType, true, solution, configuration.guid, true);
				}
			}
		}
	}
}

async function calculateMaxFCCAllUG(solution, component) {
	for (let configuration of Object.values(component.schema.configurations)) {
		calculateMaxFCC(configuration, solution, component);
	}
}

function calculateMaxFCC(ugConfig, solution, component) {
	var mainConfig = solution.getConfiguration(Object.keys(solution.schema.configurations)[0]);
	calculateTotalUserSetupCharges(solution, ugConfig, null, null, null);

	console.log('calculateMaxFCC - mainConfig');
	console.log(mainConfig);

	var contractTerm = mainConfig.getAttribute('Contract Term').value;
	var servicePlan = mainConfig.getAttribute('Service Plan').displayValue;

	console.log('calculateMaxFCC - servicePlan');
	console.log(servicePlan);

	var ugTotalVoiceCharges = ugConfig.getAttribute('Total Voice Charge').value /*+ (Number(mainConfig.getAttribute('Voice Scheme Discount').value))*/;
	var ugTotalDataCharges = ugConfig.getAttribute('Total Data Charge').value /*+ (Number(mainConfig.getAttribute('Data Scheme Discount').value))*/;
	var ugSubscriptionType = ugConfig.getAttribute('Subscription Type').displayValue;

	console.log('calculateMaxFCC - ugTotalVoiceCharges');
	console.log(ugTotalVoiceCharges);
	console.log('calculateMaxFCC - ugTotalDataCharges');
	console.log(ugTotalDataCharges);
	
	var maxFCC;

	if (ugSubscriptionType == 'Data Only') {
		maxFCC = 0;

		updateDataOnly = [{//Zvone
			name: 'Maximum FCC',
			value: maxFCC,
			displayValue: maxFCC,
			readOnly: true,
			required: false
		}];

		component.updateConfigurationAttribute(ugConfig.guid, updateDataOnly, false);
		return;
	}

	var lineRental;	

	if (servicePlan == 'The Individual Plan' || servicePlan == 'The Team Plan') {
		lineRental = (ugTotalVoiceCharges + ugTotalDataCharges) / ugConfig.originalMacQuantity;
	}

	if (servicePlan == 'The Shared Plan') {
		var accessFee = 0.0;
		var unlimitedFee = 0.0;

		for (let relatedProduct of Object.values(ugConfig.relatedProductList)) {
			if (relatedProduct.name == 'Shared Access Fee') {
				accessFee = relatedProduct.configuration.recurringPrice;
			}
		}

		for (let relatedProduct of Object.values(mainConfig.relatedProductList)) {
			if (relatedProduct.name == 'Unlimited Minutes & Texts') {
				unlimitedFee = relatedProduct.configuration.recurringPrice;
				console.log('shared plan - unlimitedFee1: ' + unlimitedFee);
				//unlimitedFee = mainConfig.getAttribute('Total Voice Charge').value;
				
				if (unlimitedFee > 10) {
				    unlimitedFee = unlimitedFee / ugConfig.originalMacQuantity;
				}
			}
		}

		console.log('shared plan - accessFee: ' + accessFee);
		console.log('shared plan - unlimitedFee: ' + unlimitedFee);

		lineRental = accessFee + unlimitedFee;
	}

	if (servicePlan == 'Custom Caller') {

		let ugTotalVoiceCharge;
		let mcDataMonthlyCharge = mainConfig.getAttribute('Team Data Monthly Charge').value;
		ugTotalDataCharges = ugConfig.getAttribute('Total Data Charge').value /*+ dataDiscount*/;

		if (ugTotalVoiceCharges < 0 || ugTotalDataCharges < 0) {
			return;
		}

		for (let relatedProduct of Object.values(ugConfig.relatedProductList)) {
			if (relatedProduct.name == 'Line Rental') {
				ugTotalVoiceCharge = relatedProduct.configuration.recurringPrice;
			}
		}

		if (ugConfig.originalMacQuantity > 1 && mainConfig.getAttribute('Custom Data Service Plan').value != 'Individual') {
			ugTotalDataCharges = (mainConfig.getAttribute('Team Data Monthly Charge').value * ugConfig.originalMacQuantity);
		}

		lineRental = ugTotalVoiceCharge + mcDataMonthlyCharge;

		if (mainConfig.getAttribute('Custom Data Service Plan').value == 'Shared') {
			var accessFee = 0.0;
			var unlimitedFee = 0.0;

			for (let relatedProduct of Object.values(ugConfig.relatedProductList)) {
				if (relatedProduct.name == 'Shared Access Fee') {
					accessFee = relatedProduct.configuration.recurringPrice;
				}
				if (relatedProduct.name == 'Line Rental') {
					unlimitedFee = relatedProduct.configuration.recurringPrice;
				}
			}
			
			lineRental = accessFee + unlimitedFee;
		}

		if (mainConfig.getAttribute('Custom Data Service Plan').value == 'Individual') {

			lineRental = (ugTotalVoiceCharges + ugTotalDataCharges) / ugConfig.originalMacQuantity;
		}
	}

	maxFCC = Math.round(lineRental * contractTerm * 0.55);

	if (maxFCC > ugConfig.getAttribute('FCC').value) {
		ugConfig.status = true;
	}

	updateData = [{
		name: 'Maximum FCC',
		value: maxFCC,
		displayValue: maxFCC,
		readOnly: true,
		required: false
	}];
	component.updateConfigurationAttribute(ugConfig.guid, updateData, false);

	validateFCC(solution, ugConfig);
}

function validateFCC(solution, configuration) {
	fcc = parseFloat(configuration.getAttribute('FCC').value);
	maxFCC = parseFloat(configuration.getAttribute('Maximum FCC').value);

	if (configuration.status == false) {
		return false;
	}

	if (fcc > maxFCC) {
		configuration.status = false;
		configuration.statusMessage = 'FCC entered is more than maximum value allowed';
		CS.SM.displayMessage('FCC entered is more than maximum value allowed', 'error');

		return false;

	} else {
		configuration.status = true;
		configuration.statusMessage = null;
	}
}

function validateManagedMDM(configuration) {
	if (configuration.getAttribute('Managed MDM').value != null && configuration.getAttribute('Managed MDM').value != '' && configuration.getAttribute('Mobile Iron Cloud').value == 'Bronze') {
		configuration.status = false;
		configuration.statusMessage = 'Managed MDM must be sold with MobileIron Silver, Gold or Platinum. ';
	} else {
		configuration.status = true;
		configuration.statusMessage = null;
	}
}

function disableFCC(solution) {
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			for (let configuration of Object.values(component.schema.configurations)) {
				subscriptionType = configuration.getAttribute('Subscription Type');
				if (subscriptionType.displayValue == 'Data Only' || subscriptionType.displayValue == null) {
					updateData = [{
						name: 'FCC',
						value: 0.0,
						displayValue: 0.0,
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateData, false);

					updateDataMaxFCC = [{
						name: 'Maximum FCC',
						value: 0.0,
						displayValue: 0.0,
						readOnly: true,
						required: false
					}];
					component.updateConfigurationAttribute(configuration.guid, updateDataMaxFCC, false);
				}
			}
		}
	}
}

function validateTRGAndTCG(solution) {
	var isFCC = false;
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.userGroups) {
			for (let configuration of Object.values(component.schema.configurations)) {
				fcc = configuration.getAttribute('FCC');
				if (fcc.value > 0) {
					isFCC = true;
					break;
				}
			}
		}
		if (isFCC) break;
	}

	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.special) {
			for (let configuration of Object.values(component.schema.configurations)) {
				trg = configuration.getAttribute('Total Revenue Guarantee Picklist');
				tcg = configuration.getAttribute('Total Revenue Contract Picklist');
				mcp = configuration.getAttribute('Minimum Connection Period');

				if(mcp.value !== '30' && tcg.value == 'Yes'){
					configuration.status = false;
					configuration.statusMessage = 'If \"Total Revenue Contract\" has been selected \"Minimum Connection Period\" needs to be set to 30 Days';
				} else if (mcp.value == '30' && trg.value == 'No' && tcg.value == 'No') {
					configuration.status = false;
					configuration.statusMessage = 'Please select either \"Total Revenue Guarantee\" or \"Total Revenue Contract\" for \"Minimum Connection Period\" of 30 Days';
				} else {
					if (configuration.statusMessage == 'Please select either \"Total Revenue Guarantee\" or \"Total Revenue Contract\" for \"Minimum Connection Period\" of 30 Days' || configuration.statusMessage == 'TCG or TRG are not allowed when FCC' || configuration.statusMessage == 'If \"Total Revenue Contract\" has been selected \"Minimum Connection Period\" needs to be set to 30 Days') {
						configuration.status = true;
						configuration.statusMessage = null;
					}
				}

				if (isFCC) {
					if (trg.value == 'Yes' || tcg.value == 'Yes') {
						configuration.status = false;
						configuration.statusMessage = 'TCG or TRG are not allowed when FCC';
					} else {
						configuration.status = true;
						configuration.statusMessage = null;
					}
				} else {
					if (configuration.statusMessage == 'TCG or TRG are not allowed when FCC') {
						configuration.status = true;
						configuration.statusMessage = null;
					}
				}
			}
		}
	}
}

function numbersCanBeDisconnected(solution) {
	for (let component of Object.values(solution.components)) {
		if (component.name == FUTURE_MOBILE_CONST.special) {
			for (let configuration of Object.values(component.schema.configurations)) {
				disconnectionAllowancePercentage = configuration.getAttribute('Disconnection Allowance Percentage').value;
				numberOfUsers = configuration.getAttribute('Number of Users').value;
                if (disconnectionAllowancePercentage) {
    				numbersDisconnection = Math.ceil(0.01 * parseFloat(disconnectionAllowancePercentage) * parseFloat(numberOfUsers));
    				updateData = [{
    					name: 'Numbers can be Disconnected',
    					value: numbersDisconnection,
    					displayValue: numbersDisconnection,
    					readOnly: true,
    					required: false
    				}];
    				component.updateConfigurationAttribute(configuration.guid, updateData, true);
                } else {
                    updateData = [{
    					name: 'Numbers can be Disconnected',
    					value: 0,
    					displayValue: 0,
    					readOnly: true,
    					required: false
    				}];
    				component.updateConfigurationAttribute(configuration.guid, updateData, true);
                }
			}
		}
	}
}

/* Implementation Form start */
function createImplementationFormXLSL() {
	window.serviceConfig = null;
	window.ugConfigs = null;
	window.planType = null;
	CS.SM.getActiveSolution().then(AfterSolutionLoad);
}

const LABELS = {
	FMS: 'Future Mobile Service',
	UG: 'User Groups'
};
const HEADERS = {
	// Å½UTI
	USERNAME: {
		header: 'Username',
		key: 'username'
	},
	EEMOBNUM: {
		header: 'EE Mobile Number',
		key: 'eemobilenumber'
	},
	SIMNUM: {
		header: 'SIM Number',
		key: 'simnumber'
	},
	IMEINUM: {
		header: 'IMEI Number',
		key: 'imeinumber'
	},
	PORTNUM: {
		header: 'Porting Number',
		key: 'portingnumber'
	},
	PACCODE: {
		header: 'PAC Code',
		key: 'paccode'
	},
	PORTDATE: {
		header: 'Port Date',
		key: 'portdate'
	},
	ROAM: {
		header: 'Roaming',
		key: 'roaming'
	},
	ROAMLIMIT: {
		header: 'â‚¬50 Roaming Data Limit',
		key: 'datalimit'
	},
	DEAAMOUNT: {
		header: 'DEA Spend Cap Amount',
		key: 'deaamount'
	},
	BILLALL: {
		header: 'Bill Protector - All Data',
		key: 'billprotectorall'
	},
	BILLROAM: {
		header: 'Bill Protector - Roaming Data',
		key: 'billprotectorroaming'
	},
	BTWIFI: {
		header: 'BT WiFi',
		key: 'btwifi'
	},
	IDDIRL: {
		header: 'IDD Ireland Talk & Text 3000',
		key: 'iddirltt3000'
	},
	IDDEUR: {
		header: 'IDD Europe Talk & Text 120',
		key: 'iddeurtt120'
	},
	IDDBEUR: {
		header: 'IDD Beyond Europe Talk & Text 120',
		key: 'iddbeurtt120'
	},
	// ZELENI
	DEVICE: {
		header: 'Device',
		key: 'device'
	},
	TARIFF: {
		header: 'Tariff',
		key: 'tariff'
	},
	TEAMPVT: {
		header: 'Team Plan Voice & Texts',
		key: 'teamplanvt'
	},
	TEAMPD: {
		header: 'Team Plan Data',
		key: 'teampland'
	},
	INTCALL: {
		header: 'International Calling',
		key: 'internationalcalling'
	},
	DAILYROAM: {
		header: 'Daily Roamer Applied',
		key: 'dailyroamerapplied'
	},
	DATABOOST: {
		header: 'Data Booster Bundles',
		key: 'databoosterbundles'
	}
};
var MAP = {
	'The Team Plan': {
		device: {
			parent: 'User Groups',
			type: 'addon',
			rpName: 'Devices',
			groupName: null
		},
		tariff: {
			parent: 'Future Mobile Service',
			type: 'attribute',
			name: 'Service Plan'
		},
		teamplanvt: {
			parent: 'Future Mobile Service',
			type: 'attribute',
			name: 'Team Voice Plan'
		},
		teampland: {
			parent: 'Future Mobile Service',
			type: 'attribute',
			name: 'Team Data Plan'
		},
		/*internationalcalling: {
		    parent: 'Future Mobile Service',
		    type: 'attribute',
		    name: null 
		},*/
		dailyroamerapplied: {
			parent: 'Future Mobile Service',
			type: 'attribute',
			name: 'Daily Roamer'
		},
		databoosterbundles: {
			parent: 'User Groups',
			type: 'addon',
			rpName: 'User Add-Ons',
			groupName: 'Data Booster'
		}
	}
};

var AfterSolutionLoad = function(solution) {

	ugConfigs = solution.getComponentByName('User Groups').getAllConfigurations();
	serviceConfig = getFMconfig(solution);
	planType = serviceConfig.attributes['service plan'].displayValue;

	for (const configId in ugConfigs) {
		let config = ugConfigs[configId],
			quantity = parseInt(config.configurationProperties.quantity);

		// CREATE WORKBOOK
		window.workbook = new ExcelJS.Workbook();
		window.worksheet = workbook.addWorksheet('TEST WORKSHEET');
		// CREATE HEADER AND INSERT    
		CreateHeader(worksheet, planType);
		let headerKeys = GetHeaderKeys(worksheet.columns);
		let rowData = [],
			picklistValidation = [];

		for (let i = 0; i < quantity; i++) {
			// CREATE ROW AND INSERT
			let rowObj = {};
			for (let j = 0; j < headerKeys.length; j++) {
				let rowVal = GetRowValue(headerKeys[j], config);

				if (rowVal.type === 'attribute') {
					rowObj[headerKeys[j]] = rowVal.value;
				} else {
					// picklist for addons
					picklistValidation.push({
						row: i + 1,
						name: headerKeys[j],
						value: rowVal.value
					});
					rowObj[headerKeys[j]] = '';
				}
			}
			rowData.push(rowObj);
		}

		for (let j = 0; j < rowData.length; j++) {
			worksheet.addRow(rowData[j]);
		}

		for (let k = 0; k < picklistValidation.length; k++) {
			let item = picklistValidation[k];

			worksheet.getRow(item.row).getCell(item.name).dataValidation = {
				type: 'list',
				allowBlank: true,
				formulae: [item.value]
			};
		}
	}
};

function download() {
	workbook.xlsx.writeBuffer().then(function(data) {
		const blob = new Blob([data], {
			type: this.blobType
		});
		saveAs(blob, 'test.xlsx');
	});
}

function getFMconfig(solution) {
	let allConfigs = solution.getAllConfigurations();
	for (let cId in allConfigs) {
		if (allConfigs[cId].name.startsWith('Future Mobile Service')) return allConfigs[cId];
	}
	return null;
}

var DataValidation = {
	YesNo: {
		type: 'list',
		allowBlank: true,
		formulae: ['"Yes,No"']
	}
};

var GetRowValue = function(key, ugConfig) {
	let mapped = MAP[planType][key],
		retValue = null,
		retType = null;

	if (typeof mapped !== 'undefined') {
		retType = mapped.type;
		if (mapped.type === 'attribute') {
			if (mapped.parent === LABELS.FMS) {
				retValue = serviceConfig.attributes[mapped.name.toLowerCase()].displayValue;
			} else {
				// Team - User Groups - trenutno nema
			}
		}

		if (mapped.type === 'addon') {
			if (mapped.parent === LABELS.FMS) {
				// Team - trenutno nema
			} else {
				// User Groups
				let filtered = ugConfig.relatedProductList.filter(function(rp) {
					return rp.relatedProductName === mapped.rpName;
				});

				if (mapped.groupName !== null) {
					filtered = filtered.filter(function(rp) {
						return rp.groupName === mapped.groupName;
					});
				}

				let filteredNameList = filtered.map(function(rp) {
					return rp.name;
				});

				retValue = '"' + filteredNameList.join(',') + '"';
			}
		}

	} else {
		retValue = '';
	}
	return {
		type: retType,
		value: retValue
	}
};


var CreateHeader = function(worksheet, planType) {
	switch (planType) {
		case 'The Team Plan':
			worksheet.columns = [
				HEADERS.USERNAME,
				HEADERS.EEMOBNUM,
				HEADERS.SIMNUM,
				HEADERS.DEVICE,
				HEADERS.IMEINUM,
				HEADERS.PORTNUM,
				HEADERS.PACCODE,
				HEADERS.PORTDATE,
				HEADERS.TARIFF,
				HEADERS.TEAMPVT,
				HEADERS.TEAMPD,
				HEADERS.ROAM,
				HEADERS.INTCALL,
				HEADERS.DAILYROAM,
				HEADERS.ROAMLIMIT,
				HEADERS.DEAAMOUNT,
				HEADERS.BILLALL,
				HEADERS.BILLROAM,
				HEADERS.DATABOOST,
				HEADERS.IDDIRL,
				HEADERS.IDDEUR,
				HEADERS.IDDBEUR
			];
			break;
		default:
			break;
	}
}

var GetHeaderKeys = function(columns) {
	return columns.map(function(o) {
		return o.key;
	});
}

function allowDisconnectAllowance(solution, configuration) {
	var trc = configuration.getAttribute('Total Revenue Contract Picklist').value;
	var trg = configuration.getAttribute('Total Revenue Guarantee Picklist').value;

	if (trc == 'Yes') {
		updateConfigurationAttributeValue('Disconnection Allowance Percentage', 0, '0%', true, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance Picklist', 'Please Select', 'Please Select', true, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance', false, false, true, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Numbers can be Disconnected', 0, 0, true, solution, configuration.guid, true);
	} else if (trg == 'Yes') {
		var disconnectAllowanceValue = configuration.getAttribute('Disconnection Allowance Percentage').value;
		var disconnectAllowanceDisplayValue = configuration.getAttribute('Disconnection Allowance Percentage').displayValue;
		updateConfigurationAttributeValue('Disconnection Allowance Percentage', disconnectAllowanceValue, disconnectAllowanceDisplayValue, false, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance Picklist', 'No', 'No', true, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance', false, false, true, solution, configuration.guid, false);
	} else if (trc == 'No' && trg == 'No') {
		var disconnectAllowanceValue = configuration.getAttribute('Disconnection Allowance Percentage').value;
		var disconnectAllowanceDisplayValue = configuration.getAttribute('Disconnection Allowance Percentage').displayValue;
		updateConfigurationAttributeValue('Disconnection Allowance Percentage', disconnectAllowanceValue, disconnectAllowanceDisplayValue, false, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance Picklist', 'Yes', 'Yes', true, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance', true, true, true, solution, configuration.guid, false);
	} else {
		var disconnectAllowanceValue = configuration.getAttribute('Disconnection Allowance Percentage').value;
		var disconnectAllowanceDisplayValue = configuration.getAttribute('Disconnection Allowance Percentage').displayValue;
		updateConfigurationAttributeValue('Disconnection Allowance Percentage', disconnectAllowanceValue, disconnectAllowanceDisplayValue, false, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance Picklist', 'Please Select', 'Please Select', true, solution, configuration.guid, false);
		updateConfigurationAttributeValue('Disconnect Allowance', false, false, true, solution, configuration.guid, false);
	}
}

function isValidAutoAddOn(configuration) {
	if (configuration.getAttribute('Subscription Type').value == null ||  configuration.getAttribute('Subscription Type').value == ''
		|| configuration.getAttribute('Device Type').value == null ||  configuration.getAttribute('Device Type').value == '' || configuration.getAttribute('Device Type').value == 'Please Select'
		|| configuration.getAttribute('eSIM Required Picklist').value == null || configuration.getAttribute('eSIM Required Picklist').value == ''
	) {
		console.log('I was calculated false');
		return false;
	}
	else {
		return true;
	}
}

function optOutPropagation(s, c){
    
    const PICKLIST = 'Opt Out of Price Increases Picklist',
        CHECKBOX = 'Opt Out of Price Increases';
        
    var pValue = c.getAttribute(PICKLIST).value;
    
    if(pValue === 'Yes'){
        updateConfigurationAttributeValue(CHECKBOX, true, true, false, s, c.guid, true);    
    }else{
        updateConfigurationAttributeValue(CHECKBOX, false, false, false, s, c.guid, true);
    }
    
    
}
