var formValidator;

/* Validation */

$.validator.addMethod("AddressVisible", function(value, element) {
    return $('#BT_business_address').is(':visible');
}, "Please enter your address details");

$.validator.addMethod("UKTelephoneNumber", function(value, element) {
    return this.optional(element) || window.telephoneNumber.isValid(value);
}, "Please enter a valid UK telephone number");

$.validator.addMethod("UKPostcode", function(value, element) {
    return this.optional(element) || 
        /^([a-zA-Z]{1,2}[0-9]{1,2}[0-9a-zA-Z]{0,1}\s?[0-9]{1}[a-zA-Z]{2})|(GIR 0AA)$/.test(value);
}, "Please enter a valid UK postcode");

$(document).ready(function() {
    formValidator = $('.BT_main_container form').validate({
        rules: {
            'BTSportForm:BT_callback_form_main:BT_text_first_name'        : { required: true },
            'BTSportForm:BT_callback_form_main:BT_text_last_name'         : { required: true },
            'BTSportForm:BT_callback_form_main:BT_text_business_name'     : { required: true },
            'BT_text_business_address_search'                             : { AddressVisible: true },
            'BTSportForm:BT_callback_form_main:BT_text_business_address_1': { required: true },
            'BTSportForm:BT_callback_form_main:BT_text_business_address_2': { },
            'BTSportForm:BT_callback_form_main:BT_text_business_city'     : { required: true },
            'BTSportForm:BT_callback_form_main:BT_text_business_country'  : { required: true },
            'BTSportForm:BT_callback_form_main:BT_text_business_postcode' : { required: true, UKPostcode: true },
            'BT_select_business_type'                                     : { required: true },
            'BTSportForm:BT_callback_form_main:BT_text_phone'             : { required: true, UKTelephoneNumber: true },
            'BTSportForm:BT_callback_form_main:BT_text_email'             : { required: true, email: true },
            'BTSportForm:BT_callback_form_main:BT_text_email_confirm'     : { required: true, email: true, equalTo: '.BT_text_email' },
            'BTSportForm:BT_callback_form_main:BT_checkbox_confirm'       : { required: true }
        },
        showErrors: function(errorMap, errorList) {
            if (this.lastElement) {
                var $lastElement = $(this.lastElement);

                $lastElement
                    .closest('.BT_field')
                    .removeClass('BT_valid BT_error')
                    .addClass(
                        typeof errorMap[this.lastElement.name] == 'undefined'
                            ? 'BT_valid'
                            : 'BT_error'
                    );
                }

            $.each(errorList, function(i, errorItem) {
                $(errorItem.element)
                    .closest('.BT_field')
                    .addClass('BT_error');
            });
        },
        onfocusin: function (element, e) {
            if (element.getAttribute('name') == 'BT_text_business_address_search') {
                $(element).closest('.BT_field').removeClass('BT_error BT_valid');
                return;
            }

            $.validator.defaults.onfocusin.apply(formValidator, arguments);
        },
        onfocusout: function (element, e) {
            if (element.getAttribute('name') == 'BT_text_business_address_search') {
                $(element).closest('.BT_field').removeClass('BT_error BT_valid');
                return;
            }

            $.validator.defaults.onfocusout.apply(formValidator, arguments);
        },
        onkeyup: function (element, e) {
            if (element.getAttribute('name') == 'BT_text_business_address_search') {
                $(element).closest('.BT_field').removeClass('BT_error BT_valid');
                return;
            }

            $.validator.defaults.onkeyup.apply(formValidator, arguments);
        }
    });
});

// Focus events for IE
$(document).ready(function () {
    $('.BT_field_select_cta').click(function() {
        $(this).closest('.BT_input').find('select').focus();
    });
});

/* Validation */
$(document).ready(function () {
    $('.BT_popup_signifier').on('click', function (e) {
        $('.BT_info_popup').show();
        e.preventDefault();
    });

    $('.BT_popup_close').on('click', function (e) {
        $('.BT_info_popup').hide();
        e.preventDefault();
    });
});

/* Postcode address finder */
$(document).ready(function() {
    if ($('.BT_text_business_name').val()) {
        $('.BT_enter_address_manually').hide();
        $('#BT_business_address').show();
    } else {
        $('.BT_enter_address_manually').show();
        $('#BT_business_address').hide();
    }

    // Remove placeholder text
    $('#BT_business_address .placeholder').filter(function() {
        return this.value.length !== 0;
    }).removeClass('placeholder');

    $('.BT_enter_address_manually').on('click', function () {
        $('.BT_enter_address_manually').slideUp(function() {
            $('#BT_text_business_address_search').val('');
            $('#BT_business_address').slideDown();

            // Remove placeholder text
            $('#BT_business_address .placeholder').filter(function() {
                return this.value.length !== 0;
            }).removeClass('placeholder');
        });
    });
});

capturePlus.listen("load", function(control) {
    control.listen("populate", function(address) {
        $('.BT_enter_address_manually').slideUp(function() {
            $('#BT_text_business_address_search').val('');
            $('#BT_business_address').slideDown();

            // Remove placeholder text
            $('#BT_business_address .placeholder').filter(function() {
                return this.value.length !== 0;
            }).removeClass('placeholder');
        });
    });
});


/* Google Analytics */
$(document).on('focus', 'input[data-ga-category!=""]', function() {
    if (typeof ga !== 'undefined') {
        var $this = $(this);
        ga('send', 'event',
            $this.attr('data-ga-category'),
            $this.attr('data-ga-action'),
            $this.attr('data-ga-label')
        );
    }
});

/* telephoneNumber.js */
(function(){window.telephoneNumber={};window.telephoneNumber.ALLOW_SPACES=1;window.telephoneNumber.ALLOW_HYPENS=2;window.telephoneNumber.ALLOW_AREA_CODE=4;window.telephoneNumber.ALLOW_DRAMA=8;window.telephoneNumber.clean=function(e,t){if(typeof t==="undefined"){t=window.telephoneNumber.ALLOW_SPACES|window.telephoneNumber.ALLOW_HYPENS|window.telephoneNumber.ALLOW_AREA_CODE}e=String(e);if(t|window.telephoneNumber.ALLOW_SPACES==t){e=e.replace(/\s/g,"")}if(t|window.telephoneNumber.ALLOW_HYPENS==t){e=e.replace(/\-/g,"")}if(t|window.telephoneNumber.ALLOW_AREA_CODE==t){e=e.replace(/^\+44/,"0")}return e};window.telephoneNumber.isValid=function(e,t){if(typeof t==="undefined"){t=window.telephoneNumber.ALLOW_SPACES|window.telephoneNumber.ALLOW_HYPENS|window.telephoneNumber.ALLOW_AREA_CODE}e=window.telephoneNumber.clean(e,t);if(/^\d{10,11}$/.test(e)!==true){return false}if(t|window.telephoneNumber.ALLOW_DRAMA!=t){var n=[/^01134960\d{3}$/,/^01144960\d{3}$/,/^01154960\d{3}$/,/^01164960\d{3}$/,/^01174960\d{3}$/,/^01184960\d{3}$/,/^01214960\d{3}$/,/^01314960\d{3}$/,/^01414960\d{3}$/,/^01514960\d{3}$/,/^01614960\d{3}$/,/^02079460\d{3}$/,/^01914980\d{3}$/,/^02890180\d{3}$/,/^02920180\d{3}$/,/^01632960\d{3}$/,/^07700900\d{3}$/,/^08081570\d{3}$/,/^09098790\d{3}$/,/^03069990\d{3}$/];for(var r in n){if(n[r].test(e)){return false}}}if(/^(01|02|03|05|070|071|072|073|074|075|07624|077|078|079)\d+$/.test(e)!=true){return false}return true}})();

/*! http://mths.be/placeholder v2.0.8 by @mathias */
(function(e,t,n){function c(e){var t={};var r=/^jQuery\d+$/;n.each(e.attributes,function(e,n){if(n.specified&&!r.test(n.name)){t[n.name]=n.value}});return t}function h(e,t){var r=this;var i=n(r);if(r.value==i.attr("placeholder")&&i.hasClass("placeholder")){if(i.data("placeholder-password")){i=i.hide().next().show().attr("id",i.removeAttr("id").data("placeholder-id"));if(e===true){return i[0].value=t}i.focus()}else{r.value="";i.removeClass("placeholder");r==d()&&r.select()}}}function p(){var e;var t=this;var r=n(t);var i=this.id;if(t.value==""){if(t.type=="password"){if(!r.data("placeholder-textinput")){try{e=r.clone().attr({type:"text"})}catch(s){e=n("<input>").attr(n.extend(c(this),{type:"text"}))}e.removeAttr("name").data({"placeholder-password":r,"placeholder-id":i}).bind("focus.placeholder",h);r.data({"placeholder-textinput":e,"placeholder-id":i}).before(e)}r=r.removeAttr("id").hide().prev().attr("id",i).show()}r.addClass("placeholder");r[0].value=r.attr("placeholder")}else{r.removeClass("placeholder")}}function d(){try{return t.activeElement}catch(e){}}var r=Object.prototype.toString.call(e.operamini)=="[object OperaMini]";var i="placeholder"in t.createElement("input")&&!r;var s="placeholder"in t.createElement("textarea")&&!r;var o=n.fn;var u=n.valHooks;var a=n.propHooks;var f;var l;if(i&&s){l=o.placeholder=function(){return this};l.input=l.textarea=true}else{l=o.placeholder=function(){var e=this;e.filter((i?"textarea":":input")+"[placeholder]").not(".placeholder").bind({"focus.placeholder":h,"blur.placeholder":p}).data("placeholder-enabled",true).trigger("blur.placeholder");return e};l.input=i;l.textarea=s;f={get:function(e){var t=n(e);var r=t.data("placeholder-password");if(r){return r[0].value}return t.data("placeholder-enabled")&&t.hasClass("placeholder")?"":e.value},set:function(e,t){var r=n(e);var i=r.data("placeholder-password");if(i){return i[0].value=t}if(!r.data("placeholder-enabled")){return e.value=t}if(t==""){e.value=t;if(e!=d()){p.call(e)}}else if(r.hasClass("placeholder")){h.call(e,true,t)||(e.value=t)}else{e.value=t}return r}};if(!i){u.input=f;a.value=f}if(!s){u.textarea=f;a.value=f}n(function(){n(t).delegate("form","submit.placeholder",function(){var e=n(".placeholder",this).each(h);setTimeout(function(){e.each(p)},10)})});n(e).bind("beforeunload.placeholder",function(){n(".placeholder").each(function(){this.value=""})})}})(this,document,jQuery);
$(document).ready(function() { $('input').placeholder(); });
