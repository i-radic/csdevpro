/**
 * BT Business - The Genie - Salesforce
 * @version: 1.1 (30/04/2014)
 * @author: Peel Solutions Ltd
 * @requires: jQuery-1.6.2
 */

var j$ = jQuery.noConflict();

var BTB = window.BTB || {};

BTB.theGenie.salesforce = (function() {
    var _settings = {
		tag :					''
    };
    
    var search = {
        submit : function() {
            //alert(window.remoting);
            //if (window.remoting !== true || typeof window.remoting === null) {
                var keyword = j$('input[id$=searchKeyword]').val();
                if (keyword != '' && keyword != null) {
                    url = '/apex/GenieSearch?search=' + keyword;
                    window.location.href = url;
                }
            //} else {
            //    window.setTimeout(function() {search.submit();}, 500);
            //}
        }
    };
	
	var getParameterByName = function(name) {
	    name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
		var regexS = "[\\?&]"+name+"=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(window.location.href);
		if(results == null)
			return "";
		else
			return decodeURIComponent(results[1].replace(/\+/g, " "));
	};
	
	var getData = function(obj, qry, addFields) {	
		GenieController.findSObjects(obj, qry, addFields, function(result, event){
			if(event.type == 'exception') {
				  alert(event.message);
			} else {
				console.log(event.message);
				console.log(result);
				return result;
				 //tags = result;
				 //response(sObjects);
			}
		});
	};
	
	var pagination = function() {
		this.elementsPerPage = 10;
    	this.currentPage = 1;
    	this.totalItems = 0;
    	this.type = '';
    	this.pagingControlsContainer = '#pagingControls';
    	
	    this.numPages = function() {
	        var numPages = 0;
	        if (this.elements != null && this.elementsPerPage != null) {
	            numPages = Math.ceil(this.elements.length / this.elementsPerPage);
	        }
	        return numPages;
	    };
	
	    this.showPage = function(page) {	    
	        this.currentPage = page;
	        this.totalItems = this.elements.length;
	        j$(this.elementsContainer).children().not('.pagination').css('display', 'none').slice((page-1) * this.elementsPerPage, ((page-1)*this.elementsPerPage) + this.elementsPerPage).css('display', 'block');
	        
	        //var html = '';
	        //this.elements.slice((page-1) * this.elementsPerPage,
	        //    ((page-1)*this.elementsPerPage) + this.elementsPerPage).each(function() {
	        //    html += '<div>' + j$(this).html() + '</div>';
	        //});
	        //j$(this.elementsContainer).html(html);
	        if (this.totalItems >= 10) renderControls(this.elementsContainer, this.totalItems, this.currentPage, this.numPages(), this.type);
	    }
	
	    var renderControls = function(container, totalItems, currentPage, numPages, type) {
	    	var content = '',
	    		summary = '',
	    		first = (currentPage*10)-9,
	    		last = currentPage*10,
	    		pages = '';
	    	if (last < totalItems) {
	    		summary = '<span>Showing <strong>' + first + '</strong> to <strong>' + last + '</strong> of <strong>' + totalItems + '</strong> ' + type + '</span>';
	    	} else {
	    		summary = '<span>Showing <strong>' + first + '</strong> to <strong>' + totalItems + '</strong> of <strong>' + totalItems + '</strong> ' + type + '</span>';
	    	}
	    	for (var i = 1; i <= numPages; i++) {
	            if (i != currentPage) {
	                pages += '<a href="javascript:void(0);" class="page" id="' + i + '">' + i + '</a> ';
	            } else {
	                pages += '<strong>' + i + '</strong> ';
	            }
			}
	    	if (j$('.pagination').length) {
	    		j$('.pagination').children('span').not('.pages').html(summary);
				j$('.pagination').children('span.pages').html(pages);
	        } else {
	        	content = '<div class="moduleInner pagination">' + '<span>' + summary + '</span><span class="pages">' + pages + '</span>' + '</div>';
		        j$(container).prepend(content).append(content);
			}
	    }
	};
	
    var fullScreen = {
        toggle : function() {
            if (j$('#AppBodyHeader').css('display') == 'block') {
                j$('#AppBodyHeader').css('display', 'none');
            } else {
                j$('#AppBodyHeader').css('display', 'block');
            }
            if (j$('.sidebarCollapsible').hasClass('sidebarCollapsed')) {
                j$('.sidebarCollapsible').removeClass('sidebarCollapsed');
            } else {
                j$('.sidebarCollapsible').addClass('sidebarCollapsed');
            }
        }
    };
    
    var replaceContent = {
        tick : function(element) {
            j$(element).each(function() {
                var item = j$(this);
                text = item.html();
                text = text.replace(/\[tick]/g, '<img src="https://s3.amazonaws.com/clients.peel-solutions.com/bt/business/thegenie/images/tick.png" width="11" height="9" alt="tick">');
                text = text.replace(/\[pinktick]/g, '<img src="https://s3.amazonaws.com/clients.peel-solutions.com/bt/business/thegenie/images/tickPink.png" width="11" height="9" alt="tick">');
                item.html(text);
            });  
        }
    };
    
    var hubPage = {
        settings : {
            documents :                         {}  
        },
        
        showDocuments : function() {
            if (hubPage.settings.documents.length > 0) {
                var content = '<div class="col standard colLast"><ul>';
                for(var i = 0; i < hubPage.settings.documents.length; i++) {
                    var audience = hubPage.settings.documents[i].Audience;
                    if (audience == 'External') {
                        audience = '<span class="external">(External)</span>';
                    } else {
                        audience = '<span class="internal">(Internal)</span>';
                    }
                    content += '<li class="icon ' + hubPage.settings.documents[i].docExt + '"> \
                                    <a href="/' + hubPage.settings.documents[i].id + '" target="_blank">' + hubPage.settings.documents[i].title + '</a>  ' + audience + '\
                                </li>';
                }
                content += '</ul></div>';
                j$('.overview').find('.moduleInner').append(content);
            }
        }
    };

    var productsPage = {        
        showDocuments : function(result) {
            if (result.length > 0) {
                var content = '<ul>';
                for(var i = 0; i < result.length; i++) {
                    var audience = result[i].Audience;
                    if (audience == 'External') {
                        audience = '<span class="external">(External)</span>';
                    } else {
                        audience = '<span class="internal">(Internal)</span>';
                    }
                    content += '<li class="icon ' + result[i].docExt + '"> \
                                    <a href="/' + result[i].id + '" target="_blank">' + result[i].title + '</a>  ' + audience + '\
                                </li>';
                }
                content += '</ul>';
                j$('.update').append(content).removeClass('update');
            }
        }
    };
	
    return {
        getDocuments : function(result, event) {
            if(event.type == 'exception') {
                //alert(event.message);
            } else {
                hubPage.settings.documents = result;
                hubPage.showDocuments();
            }
        },
        
	getProductDocuments : function(result, event) {
	    if(event.type == 'exception') {
                //alert(event.message);
            } else {
                productsPage.showDocuments(result);
            }
	},

	init : function() {
            j$('input[id$=searchKeyword]').bind('keypress', function(e) {
                var code = e.keyCode || e.which;
                if(code == 13) {
                    search.submit();
                    e.cancelBubble = true;
                    e.returnValue = false;
                    e.preventDefault();
                }
            });
            j$('#searchSubmit').on('click', search.submit);
            j$('.toggleFullScreen').on('click', fullScreen.toggle);
            replaceContent.tick('.ms-rteTable-default tr td');
		}
	};
})();