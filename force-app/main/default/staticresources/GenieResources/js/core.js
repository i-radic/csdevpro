/**
 * @fileOverview BT Business
 * @version 1.0 (JUN-2011)
 * @author LBi - http:// www.lbi.co.uk/
 */
/*jslint undef: true */
/*global  j$, jQuery, window, location, document, navigator, console, swfobject, swfsound */

var BTB = window.BTB || {};

/**
 * Wrapper function for the project
 * @requires jquery-1.6.1
 */
BTB.PE = (function (j$) {

	return {

		hoverFix : function (target) {
			var j$target = j$(target);

			j$target.each(function () {

				j$(this).bind('mouseover', function (e) {
					j$(this).addClass('hover');
				});

				j$(this).bind('mouseout', function (e) {
					j$(this).removeClass('hover');
				});
			});
		},

		addTabbedPromoMod: function addTabbedPromoMod(trigger) {
			var i, carouselType;

			for (i = 0; i < j$(trigger).length; i += 1) {
				j$(trigger + ':eq(' + i + ')').attr('id', 'tabSwitch-' + i);
				if (j$(trigger + ':eq(' + i + ')').hasClass('imageCarousel')) {
					carouselType = 'image';
				}

				j$('#tabSwitch-' + i).tabswitcher({
					animateSpeed: 1000,
					carousel: carouselType
				});
			}
		},

		expandableItem: function (elem) {

			j$(elem).each(function () {

				var j$this = j$(this),
					j$target = j$('#' + j$this.attr('id')),
					j$trigger = j$('<a href="#' + j$target.attr('id') + '" class="editableState">&#43;&nbsp;Edit</a>'),
					j$cancelTrigger = j$('<a href="#">Cancel</a>'),
					dtHeight = j$this.parent().prev().outerHeight(),
					ddHeight = j$this.parent().outerHeight(),
					input = j$target.find('input:text');

				j$trigger.click(function (e) {

					j$this.parent().animate({
						'height': ddHeight + 'px'
					}, 400, function () {
						j$target.fadeIn(400);
						j$trigger.hide();
					});

					j$this.parent().prev().animate({
						'height': ddHeight + 'px'
					}, 400);

					e.preventDefault();

				});

				j$cancelTrigger.click(function (e) {

					j$target.fadeOut(400, function () {
						j$this.parent().animate({
							'height': dtHeight + 'px'
						}, 400);
						j$this.parent().prev().animate({
							'height': dtHeight + 'px'
						}, 400, function () {

							//remove error label
							j$target.find('label.error').remove();
							//remove error class on input
							j$(input).removeClass('error');
							//clear input field
							BTB.PE.clearInput(input);

							j$trigger.show();
						});

					});

					e.preventDefault();

				});

				//append triggers
				j$this.parent().append(j$trigger);
				j$this.find('button').after(j$cancelTrigger);

				j$target.hide();
				j$this.parent().css('height', dtHeight + 'px');

			});

		},

		priceCheck : function (j$element, cookie) {

			var j$body = j$('body'),
				cookieName;

			//Set a cookie name if one is provided
			if (cookie) {
				cookieName = cookie;
			}

			//Set the body class name to show/hide (in CSS) relevant price
			function setBodyClass(price) {
				j$body.removeClass("priceCheck-default priceCheck-hidden")
					.addClass(cookieName + '-' + price);
			}

			//Read from cookie on page load
			if (j$.cookie(cookieName)) {
				setBodyClass(j$.cookie(cookieName));
			}

			//Set body class and cookie on element click
			j$element.click(function () {

				var price = j$(this).text();

				if (cookie) {

					setBodyClass(price);

					//Set cookie
					j$.cookie(cookieName, price);

				}

			});

		},
		priceToggle : function (j$element) {

			j$element.each(function (i) {

				//Cache selectors
				var j$this = j$(this),
					j$input = j$this.siblings('input');

				function toggle(j$element) {

					var j$parent = j$element.closest(j$('.priceHasRadios')),
						j$selected = j$parent.find('.price[data-month=' + j$element.text() + ']');

					j$parent.find('.price').hide();
					j$selected.show();
				}

				//Select content on click
				j$this.click(function () {
					toggle(j$this);
				});

				if(j$input.is(':checked')) {
					toggle(j$this);
				}

			});

		},


// MDW - priceToggleAll extends priceToggle to enable swapping of additional col elements
priceToggleAll : function (j$element) {

	j$element.each(function (i) {

		//Cache selectors
		var j$this = j$(this);
        j$input = j$this.siblings('input');		

		function toggle(j$element) {

			var j$parent = j$element.closest(j$('.radioSwitch')),
				j$selected = j$parent.find('.contractSwitch[data-month=' + j$element.text() + ']');

			j$parent.find('.contractSwitch').hide();
			j$selected.show();
		}

				//Select content on click
				j$this.click(function () {
					toggle(j$this);
				});

				if(j$input.is(':checked')) {
					toggle(j$this);
				}

			});

		},

		
		gallery : function (j$element) {

			if (j$element.length > 0) {

				// Initialize Advanced Galleriffic Gallery
				var gallery = j$element.galleriffic({
					delay:                     3000, // in milliseconds
					numThumbs:                 12, // The number of thumbnails to show page
					maxPagesToShow:            7,  // The maximum number of pages to display in either the top or bottom pager
					imageContainerSel:         '#slideshow', // The CSS selector for the element within which the main slideshow image should be rendered
					controlsContainerSel:      '#controls', // The CSS selector for the element within which the slideshow controls should be rendered
					captionContainerSel:       '#caption', // The CSS selector for the element within which the captions should be rendered
					loadingContainerSel:       '#loading', // The CSS selector for the element within which should be shown when an image is loading
					renderSSControls:          false, // Specifies whether the slideshow's Play and Pause links should be rendered
					renderNavControls:         false,
					enableHistory:             false, // Specifies whether the url's hash and the browser's history cache should update when the current slideshow image changes
					enableKeyboardNavigation:  true, // Specifies whether keyboard navigation is enabled
					autoStart:                 false, // Specifies whether the slideshow should be playing or paused when the page first loads
					syncTransitions:           true, // Specifies whether the out and in transitions occur simultaneously or distinctly
					defaultTransitionDuration: 1000 // If using the default transitions, specifies the duration of the transitions
				});

			}
		},

		returnAnchorOnParent : function (j$parent) {

			//Loop through each element
			j$parent.each(function () {

				var j$this = j$(this),
					j$anchor = j$this.find('a'),
					href = j$anchor.attr('href');

				//Attach click listener which locates to href when clicked
				j$this.click(function () {
					if(j$anchor.attr('target') === '_blank') {
						window.open(href);
					} else {
						window.location = href;
					}
				});

			});

		},

		quickLinks : function (j$element) {

			var self = this.quickLinks,
				j$homePromoFade = j$('.promoSlots'), //MKC: Include variable for Home Page to hide promos
				j$container = j$('#homepage'),
				j$qlTitle = j$element.find('.qlTitle'),
				j$qlContent = j$element.find('.qlContent'),
				j$qlLinks = j$element.find('.qlLinks'),
				j$panelContent = j$element.find('.panelContent'),
				j$qlAll = null;

			function equalHeight(j$el) {

				var maxHeight = 0;

				//Find largest height
				j$el.each(function () {

					var height = j$(this).height();

					if (height > maxHeight) {
						maxHeight = height;
					}

				});

				//Apply largest height
				j$el.each(function () {
					j$(this).css({'height' : maxHeight});
				});

			}

			function toggleLinks() {
				if (!self.open) {
					j$qlLinks.slideToggle("fast");
					j$qlAll.slideToggle("fast");
					j$homePromoFade.fadeToggle('slow'); //MKC: Toggle homepage promo					
				}
			}

			function posContainer() {

				var height = j$container.outerHeight(),
					margin = 26;

				j$container
					.css({'height': height + margin}) //Give the container a fixed height so you can position it's children absolutely without the height shrinking
					.addClass('active'); //Apply an active class to let our CSS know when you can achieve the above

			}

			function posContent() {
				var height = j$element.outerHeight();
				j$qlContent.css({'bottom' : height});
			}

			function slideItem(j$target) {

				//keep content positioned as element animates
				var posContentTimeout = setTimeout(isAnimating, 10);
				function isAnimating() {
					posContent();
					posContentTimeout = setTimeout(isAnimating, 10); // repeat myself
				}

				j$target.animate({height: 'toggle'}, 300, 'swing', function () {
					clearTimeout(posContentTimeout);
				}); //Toggle height between 100% and 0%

			}

			function closeAll() {
				slideItem(self.open); //close old item
				self.open = null; //Clear the open item
				j$qlTitle.find('a').removeClass('active'); //Remove active styling
				toggleLinks(); //Show content
			}

			function toggleItem(id) {

				var j$target = j$(id);

				//is there already a tab open?
				if (self.open) {

					//is the tab trying to reopen itself?
					if (("#" + self.open.attr('id')) === id) {
						closeAll();
						return;
					} else {
						slideItem(self.open); //close old item
					}

				} else {
					toggleLinks();
				}

				slideItem(j$target); //Open new item
				self.open = j$target; //Store new item

			}

			function init() {

				j$element.addClass('active');

				posContainer(); //Fix content at bottom of container
				posContent(); //Postion content above the quick links
				equalHeight(j$qlLinks); //Equalise j$qlLinks height

				//When header is clicked
				j$qlTitle.find('a').click(function (e) {

					var j$this = j$(this),
						id = j$this.attr('href');

					e.preventDefault();

					j$qlTitle.find('a').removeClass('active');
					j$this.addClass('active');

					//If nothing is open or nothing is animating
					if (!self.open || self.open.is(':not(:animated)')) {
						toggleItem(id);
					}

				});

				//Append 'view all' link
				j$element.append('<p class="quickLinksAll" style="display:none"><a href="#" class="icon chevron">Open quick links</a></p>');
				j$qlAll = j$element.find('.quickLinksAll'); //cache new element

				//When 'view all' is clicked
				j$qlAll.click(function (e) {
					e.preventDefault();
					closeAll();
				});

				//Append close button
				j$panelContent.each(function() {
					j$(this).append('<a href="#" class="cboxClose">Close</a>');
				});

				j$('.cboxClose').click(function (e) {
					e.preventDefault();
					closeAll();
				});

			}

			init();

		},

		footerQuickLinks : function (j$element) {

			var j$menuModule = j$element.find('.menuModule'),
				outerHeight = j$element.children('a').outerHeight();

			j$menuModule.css({ bottom : outerHeight })
				.prepend('<a class="close" href="javascript:void(0)">Close menu</a>');

			function slideItem(j$el) {
				j$el.animate({height: 'toggle'}, 300, 'swing'); //Toggle height between 100% and 0%
				j$element.toggleClass('selected');
			}

			j$element.children('a').add(j$element.find('.close')).click(function (e) {
				e.preventDefault();
				if (j$menuModule.is(':not(:animated)')) {
					slideItem(j$menuModule);
				}
			});

		},

		fullWidthRadioButton : function (j$element) {

			var elSelected = 'fullWidthRadioSelected',
				elFocus = 'fullWidthRadioFocus',
				elExpandContent = 'fullWidthRadioContent',
				j$checked = j$element.find(':checked').parent();

			function toggleSelection(j$element) {

				var j$current = j$element.parent().find('.' + elSelected); //Traverse up a level to search for the current selection

				//Deselect j$current
				j$current
					.removeClass(elSelected)
					.find('input') //find the radio
					.attr('checked', false); //uncheck the radio behind the scenes

				//Select j$this
				j$element
					.addClass(elSelected)
					.find('input') //find the radio
					.attr('checked', true); //check the radio behind the scenes

			}

			function expandContent(j$element) {

				var j$next = j$element.next();

				if (j$next.hasClass(elExpandContent)) { //If j$next can be expanded
					if (j$next.is(':hidden')) { //only if elExpandContent is hidden do we want to show it
						j$('.' + elExpandContent).slideUp(); //hide all other elExpandContent's
						j$next.slideDown(); //show correct content
					}
				} else { //If j$next cannot be expanded
					j$('.' + elExpandContent).slideUp(); //Slide all elExpandContent's up
				}

				//If there is a form with fieldset[data-max-val] we need to deselect any that are unselected
				j$('.fullWidthRadioContent fieldset[data-val-max]').not('fieldset[data-val-max="0"]').find('input').attr({'checked': false, 'disabled': false});

			}

			//'Check' and toggle content on load
			toggleSelection(j$checked);//Select the relevant checkbox
			expandContent(j$checked); //Expand elExpandContent

			//'Check' and toggle content on click
			j$element.click(function (e) {
				var j$this = j$(this); //Cache selector
				e.preventDefault(); //Prevent the click event from bubbling
				toggleSelection(j$this);//'check' the relevant checkbox
				expandContent(j$this); //Expand elExpandContent
			});

			//Add styling to focused elements
			j$element.find('input').focus(function () {
				j$(this).parent().addClass(elFocus);
			}).blur(function () {
				j$(this).parent().removeClass(elFocus);
			});

		},

		slideButton : function (j$element) {
			
			var j$slideButton = j$element.find('.slideButton'),
				id = 'modalBackground',
				j$modal;

			//Append modal background
			j$('body').append('<div id="' + id + '" />');
			j$modal = j$('#' + id);
			
			j$element.find('popoutContent').css({width: '12px'});
			
			j$element
				.animate({left: '0'}, 300)
				.animate({left: '-5'}, 200)
				.animate({left: '0'}, 200);

			function toggle(j$el) {
				j$el.prev().animate({width: 'toggle'});
				j$el.toggleClass("open hover");

				//Check to see if all the elements are hidden
				if(j$slideButton.hasClass('open') === false) {
					j$modal.hide();
				} else {
					j$modal.show();
				}
			}

			function close() {
				j$slideButton.each(function() {

					var j$this = j$(this);

					//Only close items if they are open
					if(j$this.hasClass('open')) {
						toggle(j$this);
					}

				});

			}

			j$slideButton.click(function (e) {
				e.preventDefault();
				toggle(j$(this));
			});

			j$modal.click(function() {
				close();
			});

		},

		modal : function (j$element) {

			j$element.click(function (e) {

				var j$this = j$(this),
					width = j$this.attr('data-width'),
					height = j$this.attr('data-height'),
					href = j$this.attr('href');

				e.preventDefault();

				//Set default width if none is set
				if (!width) {
					width = 750;
				}

				//Set default height if none is set
				if (!height) {
					height = 350;
				}

				j$.colorbox({
					href: href,
					height: height,
					width: width,
					iframe: true
				});

			});

		},

		callBackForm : function (j$element) {

			//Called from within LBI_validate.js

			var callBackUrl = 'example_callback_response.html',
				name = j$('#txtName').attr('value'),
				postcode = j$('#txtPostcode').attr('value'),
				tel = j$('#txtTel').attr('value'),
				company = j$('#txtcomp').attr('value'),
				query = "?name=" + name + "&pocode=" + postcode + "&phone=" + tel + "&Companyname=" + company;

			j$.ajax({
				url: callBackUrl + query,
				success: function (data) {
					j$element.closest('.callBackContainer').html(data);
				}
			});

		},

		alignBottom : function (j$element) {

			j$element.each(function () {

				var j$align = j$(this).find('.align'),
					highestPosition = 0;

				function findPosition() {

					j$align.each(function () {

						var j$this = j$(this),
							position;

						//Set parent to relative so the position is worked out correctly
						j$this.parent().css('position', 'relative');

						//Calculate position from top of relative parent
						position =  j$this.position();

						if (position.top > highestPosition) {
							highestPosition = position.top;
						}

					});

				}

				function setPosition() {

					j$align.each(function () {

						var j$this = j$(this),
							position = j$this.position(),
							top = Number(highestPosition - position.top);

						j$this.css('top', top + 'px');

					});

				}

				findPosition();
				setPosition();

			});

		},

		expandContent : function (j$element) {

			j$element.each(function () {

				var j$this = j$(this),
					j$content = j$(j$this.attr('href'));

				j$this.click(function (e) {

					e.preventDefault();

					j$this.toggleClass('pe_expand_open');
					j$content.slideToggle();

				});

			});

		},

		hiddenTextInputs : function (j$element) {

			j$element.click(function (e) {

				var j$parent = j$(this).closest('.dynamicTextFields'),
					radioValue = (j$(this).closest('div').prevAll().length) - 1;

				j$parent.find('.hiddenInputRow').hide();
				j$parent.find('.hiddenInputRow:eq(' + radioValue + ')').show().prevAll().show();
			});
		},



		VAL_max : function (j$element) {

			j$element.each(function () {

				var j$this = j$(this),
					limit = Number(j$this.attr('data-val-max')); //Get our max-value

				if (limit === 0) { //Max value 0 means there is no limit

					j$this.find('input').attr({'checked': true, 'disabled': true}); //check and disable all, as there is no limit

				} else {

					j$this.find('input').change(function () { //track when the input state changes from checked/non-checked

						var checked = j$this.find("input:checked").length; //count how many checkboxes are checked

						if (checked >= limit) { //If we have reached the checked limit
							j$this.find('input').not(':checked').attr({'disabled': true}); //Disable the rest of the checkboxes
						} else {
							j$this.find('input').attr({'disabled': false}); //Else if we are under the limit, ensure the rest of the checkboxes are enabled
						}

					});

				}

			});

		},

		toggleInputGroup : function (j$element) {

			var target = j$element.attr('data-toggle-group'),
				name = j$element.attr('name');

			j$('input[name=' + target + ']').click(function () {
				j$element.trigger('click');
			});

			j$('input[name=' + name + ']').not(j$element).click(function () {
				j$('input[name=' + target + ']').attr('checked', false);
			});

		},

		maxHeight : function (j$container, selector) {

			var maxHeight = 0;

			j$container.find(selector).each(function () {

				var height = j$(this).height();

				if (height > maxHeight) {
					maxHeight = height;
				}

			});

			return maxHeight;

		},

		equalHeightPrimaryModule : function (j$container, selector) {

			j$container.each(function (i) {

				var maxHeight = BTB.PE.maxHeight(j$container, selector);
				j$(this).find(selector).css('height', maxHeight + 'px');

			});

		},

		equalHeights : function () {

			// Exclude operating if quickLinks exists
			if(j$('#ql').length === 0) {
				j$(".twoColPromo, .twoColCompare, .twoColSeperator, .asideHasRows, .threeColInner, .threeColGrid, .lineCheck, .fourColSeperator, .equalHeights").equalHeights();
			}

		},

		sideTabs: function (j$element) {
			
			j$element.each(function () {
			
				var j$this = j$(this),
					j$tabLinks = j$this.find('.nav a'),
					j$initalTab = j$this.find('.nav li:first-child a'),
					j$content = j$this.find('.content');

				function populate(j$el) {

					var link = j$el.attr("id"),
						j$listLi = j$el.closest('ul').children(),
						contentDivID = j$(link).html();

					j$listLi.removeClass("active");
					j$el.closest('.sideTabs').find('.activeRegion').html(contentDivID);
					j$el.parent().addClass('active');
					

					//Attach handlers again
			//		j$('.clearOnFocus').LBI_clearOnFocus();
			// MDW - 23.04.12 
			// MDW - the above clearOnFocus call is causing error in sidetabs

				}

				j$tabLinks.click(function (e) {
					e.preventDefault();
					populate(j$(this));
				});

				//Append live region
				j$content.prepend('<div class="activeRegion" aria-live="polite" aria-atomic="false"></div>');

				populate(j$initalTab);

				//Hide the tab content now it's ready
				j$('.tabContent').hide();

			});

		}

	};

}(jQuery));

var j$ = jQuery.noConflict();
j$(document).ready(function () {

	//BTB.PE.hoverFix('.imageLinkCol');
	//BTB.PE.gallery(j$('.gallery'));
	//BTB.PE.returnAnchorOnParent(j$('.imageLinkCol'));
	BTB.PE.footerQuickLinks(j$('#footerMenu .footerQuickLink'));
	//BTB.PE.slideButton(j$('.chatPanel,.helpPanel'));
	//BTB.PE.fullWidthRadioButton(j$('.fullWidthRadio'));
	//BTB.PE.modal(j$('.pe_lightbox'));
	BTB.PE.alignBottom(j$('.alignBottom'));
	BTB.PE.expandContent(j$('.pe_expand'));
	//BTB.PE.VAL_max(j$('[data-val-max]'));
	//BTB.PE.addTabbedPromoMod('.PE_tabSwitcher');
	BTB.PE.toggleInputGroup(j$('.toggleInputGroup'));
	BTB.PE.equalHeightPrimaryModule(j$('.equalHeightContainer'), ".equalHeight");
	BTB.PE.equalHeights();
	//BTB.PE.sideTabs(j$('.panelContent'));

	j$('.tabbedContentGroup').LBI_tabbedContent();
	j$('.expandableList').LBI_expandableList({ speed: 400 });
	//j$(".defaultList").LBI_expandableList_365();
	j$('.pe_tooltip').LBI_tooltip();
//	j$('.clearOnFocus').LBI_clearOnFocus();  // MDW
//	j$(".pe_validate").LBI_validate();   // MDW

	j$('.searchSelect, .customerTypeSelect, .monthSelect, .hiddenInputRadio').LBI_radiobutton();
	j$('.expandableTable').LBI_expandableTable({ speed: 300 });

	//Need to come after plugins
	BTB.PE.priceToggle(j$('.priceRadios .radioAsButton'));
	BTB.PE.priceToggleAll(j$('.priceRadiosAll .radioAsButton')); // MDW	
	BTB.PE.hiddenTextInputs(j$('.radioAsButton'));
	BTB.PE.equalHeightPrimaryModule(j$('.tabsEvenThree .tabHeadingList'), "a");
	BTB.PE.quickLinks(j$('#ql'));
	
	j$('#footerQuickLinks').live('mouseup', function () {
		if (!j$(this).parent().hasClass('selected')) {
			j$('.footerMenuQuickLink').removeClass('selected');
			j$('.menuModule').css('display','none');
			j$(this).parent().addClass('selected');
			j$(this).next().css('display', 'block');
			j$('html').bind('click', function(e) {
				if (!j$(e.target).is('.footerMenuQuickLink *, .footerMenuQuickLink') && !j$(e.target).is('.menuModule *, .menuModule')) {
					//resetContactUsFields();      
					//location.reload(); 
					j$('.footerMenuQuickLink').removeClass('selected');
					j$('.menuModule').css('display','none');
					j$('html').unbind('click');
				}
			});
		} else {
			//resetContactUsFields();      
			//location.reload(); 
			j$(this).parent().removeClass('selected');
			j$(this).next().css('display', 'none');
		}
	});

	j$('.close').live('mouseup', function () {
		//resetContactUsFields();  
		//location.reload(); 
		j$(this).parent().parent().removeClass('selected');
		j$(this).parent().css('display', 'none');
	});
});