/*--------------------------------------------------------------------
 * JQuery Plugin: "EqualHeights"
 * by:	Scott Jehl, Todd Parker, Maggie Costello Wachs (http://www.filamentgroup.com)
 *
 * Copyright (c) 2008 Filament Group
 * Licensed under GPL (http://www.opensource.org/licenses/gpl-license.php)
 *
 * Description: Compares the heights or widths of the top-level children of a provided element
 		and sets their min-height to the tallest height (or width to widest width). Sets in em units
 		by default if pxToEm() method is available.
 * Dependencies: jQuery library, pxToEm method	(article:
		http://www.filamentgroup.com/lab/retaining_scalable_interfaces_with_pixel_to_em_conversion/)
 * Usage Example: $(element).equalHeights();
  		Optional: to set min-height in px, pass a true argument: $(element).equalHeights(true);
 * Version: 2.0, 08.01.2008
--------------------------------------------------------------------*/
(function($){$.fn.equalHeights=function(a){$(this).each(function(){var a=0;$(this).children().each(function(b){if($(this).height()>a){a=$(this).height()}});if($.browser.msie&&$.browser.version==6){$(this).children().css({height:a,zoom:"1"})}else{$(this).children().css({"min-height":a})}});return this};})(jQuery);

/**
 * LBi Tabbed Content plugin
 * @version: 1.0 (JUNE-2011)
 * @author: LBi - http:// www.lbi.co.uk/
 * @requires: jQuery-1.6.1
 */
(function($){$.fn.LBI_tabbedContent=function(options){var settings={tabsHeadingSelector:'.tabbedContentTab',tabsContainerSelector:'.tabbedContentContainer',tabsContainerIdPrefix:'tabContent-'};if(options){$.extend(settings,options);}
return this.each(function(g){var $container=$(this),tabsHeadingList=$('<ul class="tabHeadingList" role="tablist" />'),height=0;$(this).find(settings.tabsHeadingSelector).each(function(i){var tabsHeadingLabel=$(this).html(),tabsHeadingListItem=$('<li role="tab" />'),$thisNeighbourContainer=$(this).next(settings.tabsContainerSelector),tabId=settings.tabsContainerIdPrefix+g+'-'+String(i);tabsHeadingListItem.attr({'aria-controls':tabId});$thisNeighbourContainer.attr('id',tabId);if(i===0){tabsHeadingListItem.addClass('selected');}else{$thisNeighbourContainer.css('display','none');}
tabsHeadingListItem.append(tabsHeadingLabel);tabsHeadingListItem.wrapInner('<a href="#" />');tabsHeadingListItem.click(function(e){e.preventDefault();$(this).siblings('.selected').removeClass('selected');$(this).addClass('selected');$container.find(settings.tabsContainerSelector).animate({height:$('#'+tabId).height()},'slow',function(){$container.find(settings.tabsContainerSelector).css('display','none');$container.find(settings.tabsContainerSelector).css('height','auto');$('#'+tabId).css('display','block');j$(".twoColPromo, .twoColCompare, .twoColSeperator, .asideHasRows, .threeColInner, .threeColGrid, .lineCheck, .fourColSeperator, .equalHeights").equalHeights();});});tabsHeadingList.append(tabsHeadingListItem);$(this).remove();});$container.prepend(tabsHeadingList);});};})(jQuery);

/**
 * LBi expandable list plugin
 * @version: 1.0 (AUGUST-2011)
 * @author: LBi - http:// www.lbi.co.uk/
 * @requires: jQuery-1.6.2
 */
(function($){$.fn.LBI_expandableList=function(options){var settings={triggerSelector:'dt',triggerTarget:'dd',speed:'slow'};if(options){$.extend(settings,options)}function toggleRow($this){$this.each(function(){var $thisElem=$(this);$thisElem.next(settings.triggerTarget).slideToggle(settings.speed,function(){if(!$(this).is(':visible')){$(this).removeClass('open');$(this).addClass('closed');$thisElem.removeClass('open');$thisElem.addClass('closed')}else{$(this).removeClass('closed');$(this).addClass('open');$thisElem.removeClass('closed');$thisElem.addClass('open')}})})}return this.each(function(){var $thisTrigger=$(this).find(settings.triggerSelector);$thisTrigger.each(function(){$(this).wrapInner('<a href="#" />');$(this).next(settings.triggerTarget).wrapInner('<div class="togglePadding" />')});toggleRow($(this).find($(settings.triggerSelector)));$thisTrigger.bind('click',function(e){var $this=$(this);e.preventDefault();toggleRow($this)})})}})(jQuery);

/**
 * Wait for DOM Element to be created
 */
(function($){$.fn.onAvailable = function(fn){var sel=this.selector;var timer;var self=this;if(this.length > 0){fn.call(this);}else{timer=setInterval(function(){if($(sel).length > 0){fn.call($(sel));clearInterval(timer);}},50);}}})(jQuery);

/**
 * BT Business - The Genie - Core
 * @version: 1.1 (30/04/2014)
 * @author: Peel Solutions Ltd
 * @requires: jQuery-1.6.2
 */

var j$ = jQuery.noConflict();

var BTB = window.BTB || {};

BTB.theGenie = (function() {
    var settings = {
        location :                          '',
        channel :                           '',
        tag :                               '',
        productArea :                       '',
        productFamily :                     '',
        letter :                            ''
    };
        
    var equalHeights = function() {
        j$(".twoColPromo, .twoColCompare, .twoColSeperator, .asideHasRows, .threeColInner, .threeColGrid, .lineCheck, .fourColSeperator, .equalHeights").equalHeights();
    };

    
    var maxHeight = function(container, selector) {
        var maxHeight = 0;
        
        container.find(selector).each(function () {
            var height = j$(this).height();
            
            if (height > maxHeight) {
                maxHeight = height;
            }
        });
        return maxHeight;
    };
    
    var equalHeightPrimaryModule = function(container, selector) {
        container.each(function (i) {
            var maxHeight = maxHeight(container, selector);
            j$(this).find(selector).css('height', maxHeight + 'px');
        });
    };
    
    var verticalTabs = function(element) {
		element.each(function() {
			var tab = j$(this),
				tabs = tab.find('.nav a'),
				initalTab = tab.find('.nav li:first-child a'),
				content = tab.find('.content');

			function populate(elem) {
				var link = elem.attr('id'),
					list = elem.closest('ul').children(),
					liveContent = j$(link).html();

				list.removeClass('active');
				elem.closest('.sideTabs').find('.activeRegion').html(liveContent);
				elem.parent().addClass('active');
			}

			tabs.click(function(e) {
				e.preventDefault();
				populate(j$(this));
			});

			content.prepend('<div class="activeRegion" aria-live="polite" aria-atomic="false"></div>');
			populate(initalTab);
			j$('.tabContent').hide();
		});
	};
            
    var featuredModules = {
		settings : {
			modules :                        [],
			content :                        ''
		},
		
		getModules : function() {
			featuredModules.settings.modules = [];
			j$('.primaryModule.featured').each(function() {
				featuredModules.settings.modules.push(j$('.mainHeader > h2', this).text());
				j$(this).before('<a name="' + j$('.mainHeader > h2', this).text() + '"></a>');
			});
			if (j$('.featuredDownloads').length) {
				featuredModules.settings.modules.push('Downloads');
				j$('.featuredDownloads').before('<a name="Downloads"></a>');
			} else if (j$('.otherDownloads').length) {
				featuredModules.settings.modules.push('Downloads');
				j$('.otherDownloads').before('<a name="Downloads"></a>');
			}
			if (j$('.usefulLinks').length) {
				featuredModules.settings.modules.push('Useful links');
				j$('.usefulLinks').before('<a name="Useful links"></a>');
			}
			if (featuredModules.settings.modules.length > 0) {
				var content = '<nav class="tertiary-menu"><ul class="pageNav"><li class="first"><span>Jump to:</span></li>';
				for (i = 0; i < featuredModules.settings.modules.length; i++) {
					if (i == featuredModules.settings.modules.length && featuredModules.settings.modules.length > 1) {
						content += '<li class="last"><a href="#' + featuredModules.settings.modules[i] + '">' + featuredModules.settings.modules[i] + '</a></li>';
					} else {
						content += '<li><a href="#' + featuredModules.settings.modules[i] + '">' + featuredModules.settings.modules[i] + '</a></li>';
					}
				}
				content += '</ul></nav>';
                j$('.contentContainer').prepend(content);
			}
		},
		
		init : function() {
			featuredModules.getModules();
		}
	};
    
    var products = {
        init : function() {
        }
    };
    
    var footerMenu = {
		settings : {
			width :				          ''
		},
	
		resize : function() {
			if(footerMenu.settings.width >= 980) {
				j$('#footerMenu').css('width', footerMenu.settings.width + 'px');
			} else {
				j$('#footerMenu').css('width', '980px');
			}
		},
		
        quickLinks : function(element) {
            var menuModule = element.find('.menuModule'),
				outerHeight = element.children('a').outerHeight();

			menuModule.css({ bottom : outerHeight })
				.prepend('<a class="close" href="javascript:void(0)">Close menu</a>');
            
            function slideItem(el) {
				el.animate({height: 'toggle'}, 300, 'swing');
				element.toggleClass('selected');
			}
            element.children('a').add(element.find('.close')).click(function (e) {
                e.preventDefault();
                if (menuModule.is(':not(:animated)')) {
                    slideItem(menuModule);
                }
            });
            j$('#footerQuickLinks').live('mouseup', function () {
                if (!j$(this).parent().hasClass('selected')) {
                    j$('.footerMenuQuickLink').removeClass('selected');
                    j$('.menuModule').css('display','none');
                    j$(this).parent().addClass('selected');
                    j$(this).next().css('display', 'block');
                    j$('html').bind('click', function(e) {
                        if (!j$(e.target).is('.footerMenuQuickLink *, .footerMenuQuickLink') && !j$(e.target).is('.menuModule *, .menuModule')) {
                            j$('.footerMenuQuickLink').removeClass('selected');
                            j$('.menuModule').css('display','none');
                            j$('html').unbind('click');
                        }
                    });
                } else {
                    j$(this).parent().removeClass('selected');
                    j$(this).next().css('display', 'none');
                }
            });
            j$('.close').live('mouseup', function () {
                j$(this).parent().parent().removeClass('selected');
                j$(this).parent().css('display', 'none');
            });
        },
        
		init : function() {
			footerMenu.settings.width = j$('#genie').width();
			footerMenu.resize();
			j$(window).resize(function() {
				footerMenu.settings.width = j$('#genie').width();
    			footerMenu.resize();
    		});
            footerMenu.quickLinks(j$('#footerMenu .footerQuickLink'));
        }
    };

	var expandableTable = {
		settings : {
			table :                      j$('.col table'),
			tableSplit :                 '.showMoreInfo'
		},
		
		getRowIndex : function() {
			return this.settings.table.find('tr' + this.settings.tableSplit).index()+1;
		},
		
		getTruncatedHeight : function() {
			var height = i = 0;
			
			while (i < this.getRowIndex()) {
				height += this.settings.table.find('tr').eq(i).height();
				i++;
			}
		},
		
		setWrapperHeight : function() {
			var truncatedHeight = (this.getTruncatedHeight() + this.settings.table.find('caption').outerHeight() + parseInt(this.options.table.css('margin-top'))) + 'px',
				expandedHeight = wrapperElement.innerHeight() + 'px';
			
			wrapperElement.addClass('tableWrapper').data({
				'truncated' :           truncatedHeight,
				'expanded' :            expandedHeight
			}).css({
				'height' :              truncatedHeight,
				'overflow' :            'hidden',
				'position' :            'relative'
			});
		},
		
		addFauxPadding : function() {
			var wrapperElement = this.settings.table.wrap('<div>').parent();
			
			this.settings.table.find('.faux-padding').each(function() {
				var parentClasses = j$(this).attr('class'),
					validClasses = j$.grep(parentClasses.split(' '), function(n, i) {
						return n.match('^cover-');
					}).join(' ');
				j$(this).prepend('<span class="faux-padding-top"></span>').find('> span').first().addClass(validClasses);
				j$(this).append('<span class="faux-padding-bottom"></span>').find('> span').last().addClass(validClassess);
			});
			
			if (j$('toggleTableHeightLink').length) {
				this.setWrapperHeight(wrapperElement);
			} else {
				wrapperElement.addClass('tableWrapperFullHeight');
			}
		},
		
		init : function() {
			this.addFauxPadding();
		}
	};
	
	var tableStyling = {
		getCells : function() {
			return this.table.find('th, td');
		},
		
		getRows : function() {
			return this.table.find('tr');
		},
		
		wrapContent : function(tag) {
			j$.each(this.cells, function() {
				j$(this).wrapInner(tag);
			});
		},
		
		setHeights : function() {
			j$.each(this.rows, j$.proxy(function(index, element) {
			var row                      = element,
                cells                    = j$(element).find('th, td'),
                spans                    = j$(element).find('th > span, td > span'),
                rowMaxCellHeight         = this.findMax(cells, 'height'),
                rowMaxTopPadding         = this.findMax(cells, 'padding-top'),
                rowMaxBottomPadding      = this.findMax(cells, 'padding-bottom'),
                rowMaxSpanTopPadding     = this.findMax(spans, 'padding-top'),
                rowMaxSpanBottomPadding  = this.findMax(spans, 'padding-bottom'),
                spanHeight               = parseInt(rowMaxCellHeight - rowMaxTopPadding - rowMaxBottomPadding - rowMaxSpanTopPadding - rowMaxSpanBottomPadding);
			}, this));
		},
		
		findMax : function(element, property) {
			return Math.max.apply(null, element.map(function () {
	            if (property === 'height') {
	                return parseInt(j$(this).height());
	           	} else {
	                return parseInt(j$(this).css(property));
	            }
        	}).get());
		},
		
		init : function(table) {
			this.table = table;
			this.cells = this.getCells();
			this.rows = this.getRows();
			
			this.wrapContent('<span></span>');
		}
	};

	

	return {
        getParameterByName : function(name) {
            name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
            var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
                results = regex.exec(location.search);
            return results == null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
        },
        
        copyToClipboard : function(el) {
			j$(el).bind({
				'mouseenter' : function() {
					j$(this).find('span').fadeIn('fast');
				},
				'mouseleave' : function() {
					j$(this).find('span').fadeOut('fast').text('Copy');
				},
				'click' : function() {
					if(window.clipboardData && clipboardData.setData) {
						clipboardData.setData('Text', j$(this).find('input').attr('value'));
						j$(this)
						.find('span')
						.text('Copied')
						.end()
						.find('input').blur();
					} else {
						j$(this).find('input').select();
					}
                }
            });
        },
        
        
        init : function() {		
			document.getElementsByTagName('body')[0].className += ' js';
			settings.location = window.location.pathname.replace(/^.*\/|\.[^.]*$/g, '');
			switch (settings.location) {
   				case 'GenieHome' :
					break;
   				case 'GenieProducts' :
                    products.init();
   					break;
   				case 'GenieAtoZ' :
   					break;
   				case 'GenieTagDetail' :
                    featuredModules.init();
   					tableStyling.init(j$('.col table'));
   					setTimeout(function(){ expandableTable.init(); },200);
   					break;
   				default:
   					break;
			}
            j$('.expandableList').LBI_expandableList({speed: 400});
            j$('.tabbedContentGroup').LBI_tabbedContent();
            equalHeights();
            verticalTabs(j$('.panelContent'));
            footerMenu.init();
		}
	};
})();