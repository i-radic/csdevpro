/**
 * BT Business - The Genie - Omniture Link Tracking
 * @version: 1.1 (30/04/2014)
 * @author: Peel Solutions Ltd
 * @requires: jQuery-1.6.2
 */

var j$ = jQuery.noConflict();

var BTB = window.BTB || {};

BTB.theGenie.omniture = (function() {
    var settings = {
        productFamily :                     '',
        pageName :                          '',
        userOUC :                           ''
    };
    
    var tracking = {
        toolButtons : function(e) {
            var sName = j$(this).text();
            var pName = 'BTB:Salesforce:';
            if (settings.productFamily != '') pName += settings.productFamily + ':';
            pName += settings.pageName + ':';
            
            if (sName == 'Follow' || j$(this).parent().children('span').text() == 'Follow') {
                pName += 'Follow';
            } else if (j$(this).parent().text() == 'Following') {
                pName += 'Stop following';
            } else if (sName == ' Print') {
                pName += 'Print';
            } else if (j$(this).hasClass('familyLink')) {
                pName += 'Back to family';
            } else {
                pName = '';
            }
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        takeMeTo : function(e){
            var pName = 'BTB:Salesforce:Homepage:Take me to:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        portfolioAreas : function(e){
            var pName = 'BTB:Salesforce:Homepage:Portfolio Areas:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },

        haveYouSeen : function(e){
            var pName = 'BTB:Salesforce:Homepage:Have You Seen:' + j$(this).parent().parent().children('h3').text();
                if (pName !== '') {
                    tracking.send(pName);
                }
        },
        
        recentUpdates : function(e){
            var pName = 'BTB:Salesforce:Homepage:Recent updates:';
            if (j$(this).attr('target') == '_blank') {
                pName += 'Download:';
            }
            pName += j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        essentialInformationTab : function(e){
            var pName = 'BTB:Salesforce:Homepage:Essential information:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        essentialInformation : function(e){
            var pName = 'BTB:Salesforce:Homepage:Essential information:';
            if (j$(this).children('h3').text() !== '' || null) {
                pName += 'Download:' + j$(this).children('h3').text();
            } else {
                pName += j$('.essentialInformation .mainCol .nav li[class="active"] a').text();
            }
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        content : function(e){
            var sName = j$(this).parent().parent().parent().children('.mainHeader').children('h2').html();
            var pName = 'BTB:Salesforce:' + settings.pageName + ':';
            
            if (sName == 'Everything you need to know') {
                pName += 'Everything you need to know:' + j$(this).text();
            } else if (sName == 'Useful links') {
                pName += 'Useful links:' + j$(this).text();
            } else {
                sName = j$(this).parent().parent().parent().parent().children('.mainHeader').children('h2').text();
                if (sName == 'Other downloads') {
                    pName += 'Download:' + j$(this).text();
                } else {
                    sName = j$(this).parent().parent().children('.mainHeader').children('h2').text();
                    if (sName == 'Featured downloads') {
                        pName += 'Download:' + j$(this).prevAll('h3:first').text();
                    } else {
                        pName = '';
                    }
                }
            }
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        forCustomers : function(e){
            var pName = 'BTB:Salesforce:' + settings.pageName + ':';
            pName += 'Download:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
                //console.log(pName);
            }
        },
        
        forYou : function(e){
            var pName = 'BTB:Salesforce:' + settings.pageName + ':';
            pName += 'Download:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
                //console.log(pName);
            }
        },
        
        downloads : function(e){
            var pName = 'BTB:Salesforce:' + settings.pageName + ':';
            pName += 'Download:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
                //console.log(pName);
            }
        },
        
        links : function(e){
            var pName = 'BTB:Salesforce:' + settings.pageName + ':';
            pName += 'Useful links:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
                //console.log(pName);
            }
        },
        
        viewProductSummary : function(e) {
            var pName = 'BTB:Salesforce:' + settings.pageName + ':View summary:';
            pName += j$(this).children('span').text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        productSummaryDownload : function(e) {
            var pName = 'BTB:Salesforce:' + settings.pageName + ':Download:';
            pName += j$('.productList dt.open:last').attr('id') + ':' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        viewProduct : function(e) {
            var pName = 'BTB:Salesforce:' + settings.pageName + 'View full product details:';
            if (j$(this).parent().children('span').text() !== '' || null) {
                pName += j$(this).parent().children('span').text();
            } else if (j$('.productList dt.open:last').attr('id') !== '' || null) {
                pName += j$('.productList dt.open:last').attr('id');
            } else {
                pName = '';
            }
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        campaignPromo : function(e) {
            var pName = 'BTB:Salesforce:Campaigns & events:Promo:' + j$(this).children('h2').text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        documentLibrary : function(e) {
            var pName = 'BTB:Salesforce:Document library:Download:' + j$(this).text();
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        footer : function(e) {
            var sName = j$('#footerMenu .footerMenuInner ul li.selected .mainHeader h2').text() + ':';
            var pName = 'BTB:Salesforce:';
            if (settings.productFamily != '') pName += settings.productFamily + ':';
            pName += settings.pageName + ':Footer:';
            
            if (sName != '' || sName != null){
                if (j$(this).attr('target') == '_blank' && sName == 'I\'m following:') {
                    pName = pName + sName + 'Download:' + j$(this).text();
                } else {
                    pName = pName + sName + j$(this).text();
                }
            }
            if (pName !== '') {
                tracking.send(pName);
            }
        },
        
        send : function(name) {
            var s                       = s_gi('btiesforcegenie');
            s.linkTrackVars             = 'eVar19,prop50,prop1';
            s.eVar19                    = name;
            s.prop50                    = name;
            s.prop1                     = settings.userOUC;
            s.tl(this,'o',name);
        }
    }

    return {
	    init : function(family, page, ouc) {
                settings.productFamily = family;
                settings.pageName = page;
                settings.userOUC = ouc;
            
                // Homepage tracking
                j$('.take-me-to div ul li a').on('click', tracking.takeMeTo);
                j$('.portfolio-areas div ul li a').on('click', tracking.portfolioAreas);
	            j$(document.body).on('click', '.homepageBanners a', tracking.haveYouSeen);
                j$('.essentialInformation .asideCol a').on('click', tracking.recentUpdates);
                j$('.essentialInformation .nav a').on('click', tracking.essentialInformationTab);
                j$('#essentialInformation').on('click', 'a', tracking.essentialInformation);
                // Tag detail tracking
                j$('.toolButtons a').on('click', tracking.toolButtons);
                j$('.primaryModule a').on('click', tracking.content);
                j$('.for-customers a').on('click', tracking.forCustomers);
                j$('.for-you a').on('click', tracking.forYou);
                j$('body').on('click', '.download-links li a', tracking.downloads);
                j$('.useful-links li a').on('click', tracking.links);
                // Products & services/Search results tracking
                j$('.productList dt a').on('click', tracking.viewProductSummary);
                j$('.productList dd li.icon a').on('click', tracking.productSummaryDownload);
                j$('.viewProduct').on('click', tracking.viewProduct);
                j$('.productList dd a.linkAsButton').on('click', tracking.viewProduct);
                // Campaign & events tracking
                j$('a.article-link').on('click', tracking.campaignPromo);
                // Document library tracking
                j$('.documentList table a').on('click', tracking.documentLibrary);
                // Generic tracking
                j$('#footerMenu .moduleInner a').on('click', tracking.footer);
		}
	};
})();