var j$ = jQuery.noConflict();

var BTB = window.BTB || {};

BTB.theGenie = (function() {
    var _settings = {
        location :				'',
		channel : 				'',
		tag :					'',
		productArea :			'',
		productFamily :			'',
		letter :				''
    };
    
    var atozPage = {
        letter :                    'A',
        type :                      'Pages',
        error :                     '<div class="columns twelve"> \
                                        <h3>Sorry!</h3> \
                                        <p>Something must have gone terribly wrong, please try reloading the page.</p> \
                                    </div>',
        content : {
            tags : {
                list :                  'Tags',
                container :             'atozContent',
                query :                 '',
                fields :                '<FieldRef Name="Title" /><FieldRef Name="Description" /><FieldRef Name="Tag_x0020_Type" />',
                template :              '<div class="columns four"> \
                                            <h3><a href="/sites/genie/SitePages/TagDetail.aspx?name=%title%&channel=%channel%">%title%</a></h3> \
                                            <p>%description%</p> \
                                        </div>',
                error :                 '<div class="columns twelve"> \
                                            <h3>Sorry!</h3> \
                                            <p>We were unable to find any links starting with <strong>%letter%</strong>, please try a different letter.</p> \
                                        </div>',
                items :                 []
            },
            links : {
                list :                  'Links',
                container :             'atozContent',
                query :                 '',
                fields :                '<FieldRef Name="Title0" /><FieldRef Name="URL" /><FieldRef Name="Comments" /><FieldRef Name="Tags" />',
                template :              '<div class="columns four"> \
                                            <h3><a href="%url%" target="_blank">%title%</a></h3> \
                                            <p>Related to: %tags%</p> \
                                            <p>%description%</p> \
                                        </div>',
                error :                 '<div class="columns twelve"> \
                                            <h3>Sorry!</h3> \
                                            <p>We were unable to find any links starting with <strong>%letter%</strong>, please try a different letter.</p> \
                                        </div>',
                items :                 []
            },
            jargon : {
                list :                  'Snippets',
                container :             'atozContent',
                query :                 '',
                fields :                '<FieldRef Name="Title" /><FieldRef Name="Content" />',
                template :              '<div class="columns four"> \
                                            <h3>%title%</h3> \
                                            <p>%content%</p> \
                                        </div>',
                error :                 '<div class="columns twelve"> \
                                            <h3>Sorry!</h3> \
                                            <p>We were unable to find any acronyms/jargon starting with <strong>%letter%</strong>, please try a different letter.</p> \
                                        </div>',
                items :                 []
            },
            salestips : {
                list :                  'Snippets',
                container :             'atozContent',
                query :                 '',
                fields :                '<FieldRef Name="Title" /><FieldRef Name="Content" /><FieldRef Name="Person" /><FieldRef Name="Company" /><FieldRef Name="Tags" />',
                template :              '<div class="columns four"> \
                                            <h3>%title%</h3> \
                                            <p>%content%</p> \
                                            <p>%person%<br /> \
                                            %company%</p> \
                                        </div>',
                error :                 '<div class="columns twelve"> \
                                            <h3>Sorry!</h3> \
                                            <p>We were unable to find any sales tips starting with <strong>%letter%</strong>, please try a different letter.</p> \
                                        </div>',
                items :                 []
            }
        },

        reset : function() {
            document.getElementById('atozContent').innerHTML = '';
        },
            
        setLetter : function(e) {
            j$('#' + atozPage.letter).html('<a href="javascript:void(0);">' + atozPage.letter + '</a>');
            atozPage.letter = j$(this).attr('id');
			j$('#' + atozPage.letter).html('<span>' + atozPage.letter + '</span>');
			atozPage.getData(atozPage.type);
		},
        
        setType : function(e) {
            atozPage.type = j$(this).attr('id');
            j$('.tabHeadingList li').removeClass('selected');
            j$(this).addClass('selected');
            atozPage.getData(atozPage.type);
        },
            
        handleTags : function() {
            var content = '',
                listItemsEnumerator = atozPage.content.tags.items.getEnumerator();

            while (listItemsEnumerator.moveNext()) {
                var item = listItemsEnumerator.get_current();
                content += render(atozPage.content.tags.template, {title: item.get_item('Title'), description: item.get_item('Description'), tagtype: item.get_item('Tag_x0020_Type').get_lookupValue()});
            }
            if (content === '') {
                content = render(atozPage.content.tags.error, {letter: atozPage.letter});
            }
            document.getElementById(atozPage.content.tags.container).innerHTML = content;
        },
        
        handleLinks : function() {
            var content = '',
                listItemsEnumerator = atozPage.content.links.items.getEnumerator();

            while (listItemsEnumerator.moveNext()) {
                var item = listItemsEnumerator.get_current(),
                    tagsArray = item.get_item('Tags'),
                    tags = '';
                for (var i=0; i < tagsArray.length; i++) {
                    if (i === 0) {
                        tags += tagsArray[i].get_lookupValue();
                    } else {
                        tags += ',' + tagsArray[i].get_lookupValue();
                    }
                }
                content += render(atozPage.content.links.template, {title: item.get_item('Title0'), description: item.get_item('Comments'), tags: tags, url: item.get_item('URL').get_url()});
            }
            if (content === '') {
                content = render(atozPage.content.links.error, {letter: atozPage.letter});
            }
            document.getElementById(atozPage.content.links.container).innerHTML = content;
        },
            
        handleJargon : function() {
            var content = '',
                listItemsEnumerator = atozPage.content.jargon.items.getEnumerator();

            while (listItemsEnumerator.moveNext()) {
                var item = listItemsEnumerator.get_current();
                content += render(atozPage.content.jargon.template, {title: item.get_item('Title'), content: item.get_item('Content')});
            }
            if (content === '') {
                content = render(atozPage.content.jargon.error, {letter: atozPage.letter});
            }
            document.getElementById(atozPage.content.jargon.container).innerHTML = content;
        },
            
        handleSalesTips : function() {
            var content = '',
                listItemsEnumerator = atozPage.content.salestips.items.getEnumerator();

            while (listItemsEnumerator.moveNext()) {
                var item = listItemsEnumerator.get_current();
                content += render(atozPage.content.salestips.template, {title: item.get_item('Title'), content: item.get_item('Content'), person: item.get_item('Person'), company: item.get_item('Company')});
            }
            if (content === '') {
                content = render(atozPage.content.salestips.error, {letter: atozPage.letter});
            }
            document.getElementById(atozPage.content.salestips.container).innerHTML = content;
        },
        
        init : function() {
            atozPage.getContent(atozPage.type);
			j$('.sectionNav.atoz li').on('click', atozPage.setLetter);
            j$('.tabHeadingList li').on('click', atozPage.setType);
        }
    };
    
    return {
		init : function() {
			// Set location
			_settings.location = window.location.pathname.replace(/^.*\/|\.[^.]*$/g, '');
			// Set channel name
			_settings.channel = decodeURI(getParameterByName('channel')) == '' ? 'Genie Business Sales Classic' : decodeURI(getParameterByName('channel'));
            _settings.tag = decodeURI(getParameterByName('name')) == '' ? '' :  decodeURI(getParameterByName('name'));
			// Setup pages
			switch (_settings.location) {
   				case 'AtoZ':
   					atozPage.init();
   					break;
   				default:
   					break;
			}
		}
	};
});