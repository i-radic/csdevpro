/*
* add support for Product Family dropdown, Year 1 + rolling years, get values by attributes
* multiple fields and/or attributes
*/
//row numbers constants
const DATA_COST_ROW_INDEX = 27;

function emptyYears () {
	var arrToReturn = [];
	var tableObj = JSON.parse(table);
	var years = tableObj[0].dateNames.length;
	for (var i = 0; i < years; i++) {
		arrToReturn.push(0);
	}

	return arrToReturn;
}

function twoRowsPercentage(row1, row2) {
	var result;
	var tableObj = JSON.parse(table);
	var row1Obj = {};
	var row2Obj = {};

	for (var i = 0; i < tableObj.length; i++) {
		if (tableObj[i].name == row1) {
			row1Obj = tableObj[i];
		}
		if (tableObj[i].name == row2) {
			row2Obj = tableObj[i];
		}
	}
	var row1Total = row1Obj.total.replace(",", "");
	var row2Total = row2Obj.total.replace(",", "");

	result = parseFloat(row1Total) / parseFloat(row2Total) * 100;
	return result;
}

function getVolumeForPandL() {
	var volumeForPandL;
	var productConfigurationIds = Object.keys(basketJSON.configDetails);
	for (var i = 0; i < productConfigurationIds.length; i++) {
		var pcId = productConfigurationIds[i];
		if (basketJSON.configDetails[pcId].config.Product_Definition_Name__c == "BT Mobile Sharer" || basketJSON.configDetails[pcId].config.Product_Definition_Name__c == "BT Mobile Flex") {
			volumeForPandL = basketJSON.configDetails[pcId].config.Volume_For_PandL__c;
		}
	}
	return volumeForPandL;
}

function formatNumber(str) {
	return str.replace(",", "");
}

function getRowObject(rowName) {
	var tableObj = JSON.parse(table);
	var rowObject;
	for (var i = 0; i < tableObj.length; i++) {
		if (tableObj[i].name == rowName) {
			rowObject = tableObj[i];
		}
	}
	return rowObject;
}

var GetGmPercentageCalcuF = function () {
	var arrToReturn = emptyYears();
	return arrToReturn;
}
window.GetGmPercentageCalcuF = GetGmPercentageCalcuF;

var GetGmPercentageTotalF = function() {
	return twoRowsPercentage("Gross Margin", "TOTAL REVENUE");
}
window.GetGmPercentageTotalF = GetGmPercentageTotalF;

var GetIncrEbitdaPercentageCalcuF = function() {
	var arrToReturn = emptyYears();
	return arrToReturn;
}
window.GetIncrEbitdaPercentageCalcuF = GetIncrEbitdaPercentageCalcuF;

var GetIncrEbitdaPercentageTotalF = function() {
	return twoRowsPercentage("Incremental EBITDA", "TOTAL REVENUE");
}
window.GetIncrEbitdaPercentageTotalF = GetIncrEbitdaPercentageTotalF;

var IncrNpvPerSubCalcuF = function() {
	var arrToReturn = emptyYears();
	return arrToReturn;
}
window.IncrNpvPerSubCalcuF = IncrNpvPerSubCalcuF;

var IncrNpvPerSubTotalF = function() {
	var incrEbitdaOneOff = getRowObject("Incremental EBITDA One Off (hide later)").total;
	incrEbitdaOneOff = parseFloat(formatNumber(incrEbitdaOneOff));
	var incrEbitdaYears = getRowObject("Incremental EBITDA Recurring (hide later)");

	var yearsSum = 0;
	var year;
	var years = Object.values(incrEbitdaYears.dates);
	for (var i = 0; i < years.length; i++) {
		year = parseFloat(formatNumber(years[i]));
		yearsSum += year;
	}

	var volumeForPandL = getVolumeForPandL();
	var result = (yearsSum * 0.9 + incrEbitdaOneOff) / volumeForPandL;
	return result
}
window.IncrNpvPerSubTotalF = IncrNpvPerSubTotalF;


var EbitdaPercentageCalcuF = function() {
	var arrToReturn = emptyYears();
	return arrToReturn;
}
window.EbitdaPercentageCalcuF = EbitdaPercentageCalcuF;

var EbitdaPercentageTotalF = function() {
	return twoRowsPercentage("EBITDA", "TOTAL REVENUE");
}
window.EbitdaPercentageTotalF = EbitdaPercentageTotalF;

var NpvPerSubCalcuF = function() {
	var arrToReturn = emptyYears();
	return arrToReturn;
}
window.NpvPerSubCalcuF = NpvPerSubCalcuF;

var NpvPerSubTotalF = function() {
	var ebitdaOneOff = getRowObject("EBITDA One Off (hide later)").total;
	ebitdaOneOff = parseFloat(formatNumber(ebitdaOneOff));
	var ebitdaYears = getRowObject("EBITDA Recurring (hide later)");

	var yearsSum = 0;
	var year;
	var years = Object.values(ebitdaYears.dates);
	for (var i = 0; i < years.length; i++) {
		year = parseFloat(formatNumber(years[i]));
		yearsSum += year;
	}

	var volumeForPandL = getVolumeForPandL();
	var result = (yearsSum * 0.9 + ebitdaOneOff) / volumeForPandL;
	return result
}
window.NpvPerSubTotalF = NpvPerSubTotalF;

var getAllFunctions = function(){ 
	var allfunctions=[];
	for ( var i in window) {
		if((typeof window[i]).toString()=="function"){
			allfunctions.push(window[i].name);
		}
	}
	return allfunctions;
}
window.getAllFunctions = getAllFunctions;