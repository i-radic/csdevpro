/*######################################################################################
PURPOSE


HISTORY
17/09/14    John McGovern   Launch version
######################################################################################*/
trigger CloudVoiceSiteAfter on Cloud_Voice_Site__c (after update) {
Integer count = 0;

if(!HelperRollUpSummary.getCVSiteStop()){   
    Set<Id> scIds = new Set<Id>(); 
    Set<Id> cvIds = new Set<Id>();
    Set<Id> cvsIds = new Set<Id>();

    for (Cloud_Voice_Site__c c:trigger.New){
        scIds.add(c.SC_Quote__c);
        cvIds.add(c.CV__c );
        cvsIds.add(c.Id);
    }

    List<Cloud_Voice_Site_Product__c> cvspList= [SELECT ID, Unit_Cost__c,Connection_Charge__c,Total_Cost_SC_Quote__c, Cloud_Voice_Site__r.Structured_Cabling_Helpdesk_Quote__c FROM Cloud_Voice_Site_Product__c WHERE Id = :scIds LIMIT 1];

    for (Cloud_Voice_Site__c c:trigger.New){ 
        for (Cloud_Voice_Site_Product__c cvsp:cvspList ){
            cvsp.Connection_Charge__c = c.Structured_Cabling_Helpdesk_Quote__c;
        }
    }
    update cvspList;

    List<Cloud_Voice_Site__c> cvsList= [SELECT ID, CV__c, zAccessProvision__c, Access_Order_Reference__c FROM Cloud_Voice_Site__c WHERE CV__c In :cvIds];
    List<Cloud_Voice__c> cvList= [SELECT ID, zCVAccessRequired__c FROM Cloud_Voice__c WHERE Id = :cvsList[0].CV__c LIMIT 1];

    for (Cloud_Voice_Site__c c:cvsList){ 
        if(c.zAccessProvision__c != 'Existing' && (c.Access_Order_Reference__c=='' || c.Access_Order_Reference__c==Null)){ 
            count = count + 1;
        }
    }   
    cvList[0].zCVAccessRequired__c = count; 
    update cvList;
}
}