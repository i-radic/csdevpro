/**
 * All product bundle triggers
 *
 */
trigger CS_AllBundleTriggers on cscfga__Product_Bundle__c (after delete, after insert, after undelete, 
	after update, before delete, before insert, before update) {
	
	CS_TriggerHandler.execute(new CS_ProductBundleTriggerDelegate());
	
}