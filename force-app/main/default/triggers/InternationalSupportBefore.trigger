trigger InternationalSupportBefore on International_Support__c (before insert, before update) {
    International_Support__c objold;

    if(trigger.isUpdate)
    {
     for(International_Support__c objnew: Trigger.new){
        objold = new International_Support__c ();
        objold = Trigger.oldmap.get(objnew.id);
         
         if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Assigned'){
                objnew.Date_new__c=datetime.now();
                //objnew.Test__c=objold.Days_in_Assigned_status__c;
                
            }
        
        if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='In Progress')
        {
                
                objnew.In_progress_date__c=datetime.now();
                //objnew.Test__c=objold.Days_in_In_Progress_status__c;
        }
    
         if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Rejected')
         {
             objnew.Rejected_date__c=datetime.now();
             //objnew.Test__c=objold.Days_in_Rejected_status__c;
         }
         
         if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Deferred')
         {
             objnew.Deferred_date__c=datetime.now();
         }
         
          if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Waiting on Someone Else')
         {
             objnew.Waiting_on_date__c=datetime.now();
         }
         
          if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Cancelled')
         {
             objnew.Cancelled_date__c=datetime.now();
         }
         
          if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Completed')
         {
             objnew.Completed_date__c=datetime.now();
         }
         
          if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Sent to Workflow')
         {
             objnew.Sent_to_Workflow_date__c=datetime.now();
         }
         
          if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Created')
         {
             objnew.Created_status_date__c=datetime.now();
         }
         
          if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Vet and Validation – PPR')
         {
             objnew.Vet_and_ValidationPPR_date__c=datetime.now();
         }
         
          if(objnew.Status__c!=objold.Status__c && objnew.Status__c=='Vet and Validation')
         {
             objnew.Vet_and_Validation_date__c=datetime.now();
         }
        
        
      if(objold.Status__c=='Assigned' && objnew.Status__c!='Assigned')
       {
            objnew.Store_Assigned__c=objold.Days_in_Assigned_status__c;
        }
         else if(objold.Status__c=='In Progress' && objnew.Status__c!='In Progress')
         {
             objnew.Store_In_Progress__c=objold.Days_in_In_Progress_status__c;
         }
         else if(objold.Status__c=='Rejected' && objnew.Status__c!='Rejected')
         {
             objnew.Store_Rejected__c=objold.Days_in_Rejected_status__c;
         }
         else if(objold.Status__c=='Deferred' && objnew.Status__c!='Deferred')
         {
             objnew.Store_Deferred__c=objold.Days_in_Deferred_status__c;
         }
         else if(objold.Status__c=='Waiting on Someone Else' && objnew.Status__c!='Waiting on Someone Else')
         {
             objnew.Store_Waiting_on__c=objold.Days_in_Waiting_on_status__c;
         }
         else if(objold.Status__c=='Cancelled' && objnew.Status__c!='Cancelled')
         {
             objnew.Store_Cancelled__c=objold.Days_in_Cancelled_status__c;
         }
         else if(objold.Status__c=='Completed' && objnew.Status__c!='Completed')
         {
             objnew.Store_Completed__c=objold.Days_in_Completed_status__c;
         }
         else if(objold.Status__c=='Sent to Workflow' && objnew.Status__c!='Sent to Workflow')
         {
             objnew.Store_Sent_to_Workflow__c=objold.Days_in_Sent_to_Workflow_status__c;
         }
         else if(objold.Status__c=='Created' && objnew.Status__c!='Created')
         {                
             objnew.Store_Created__c=objold.Days_in_Created_status__c;                       
         }
         else if(objold.Status__c=='Vet and Validation – PPR' && objnew.Status__c!='Vet and Validation – PPR')
         {
             objnew.Store_Vet_and_ValidationPPR__c=objold.Days_in_Vet_and_ValidationPPR_status__c;
         }
         else if(objold.Status__c=='Vet and Validation' && objnew.Status__c!='Vet and Validation')
         {
             objnew.Store_Vet_and_Validation__c=objold.Days_in_Vet_and_Validation_status__c;
         }
         else
         {
         }
    }
        
    }
    if(trigger.isInsert)
    {
        for(International_Support__c objnew_insertion:Trigger.new)
        {
        
        
            if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Assigned'){
                    objnew_insertion.Date_new__c=datetime.now();
        }
            
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='In Progress'){
                    objnew_insertion.In_progress_date__c=datetime.now();
        }
            
           if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Rejected'){
                    objnew_insertion.Rejected_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Deferred'){
                    objnew_insertion.Deferred_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Waiting on Someone Else'){
                    objnew_insertion.Waiting_on_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Cancelled'){
                    objnew_insertion.Cancelled_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Completed'){
                    objnew_insertion.Completed_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Sent to Workflow'){
                    objnew_insertion.Sent_to_Workflow_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Created'){
                    objnew_insertion.Created_status_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Vet and Validation – PPR'){
                    objnew_insertion.Vet_and_ValidationPPR_date__c=datetime.now();
        } 
             if(objnew_insertion.Status__c!=null && objnew_insertion.Status__c=='Vet and Validation'){
                    objnew_insertion.Vet_and_Validation_date__c=datetime.now();
        } 
            
    }
    }
}