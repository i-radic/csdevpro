trigger BTSportSVOCAfter on BT_Sport_SVOC__c(after Insert, after Update, after delete, after undelete){    
    
    if(Trigger.isInsert){ 
        
        Map<Id,String> legacyCugIds = new Map<Id,String>();    //Contains legacy customer group key OR CUG
        Map<String,Id> relatedAcc = new Map<String,Id>();
        Map<Id,Id> relatedCon = new Map<Id,Id>();
        List<BT_Sport_SVOC__c> SVOCList = new List<BT_Sport_SVOC__c>();
        Map<Id,BT_Sport_SVOC__c> updateSVOCMap = new Map<Id,BT_Sport_SVOC__c>();
        Map<Id,Lead> newLeadsMap = new Map<Id,Lead>();
        Lead newLead;
        Lead tempLead;
        
        for(BT_Sport_SVOC__c svoc : Trigger.new){            
            if(svoc.Legacy_Customer_Group_Key__c != Null){
                legacyCugIds.put(svoc.Id,svoc.Legacy_Customer_Group_Key__c);
            }else if(svoc.CUG_ID__c != Null){
                legacyCugIds.put(svoc.Id,svoc.CUG_ID__c);
            }        
            
            if(svoc.Lead__c == null){
                
                newLead = new Lead(
                    //LastName = svoc.Contact_Name__c == Null ? 'TestLN' : svoc.Contact_Name__c,         - for testing purpose
                    //Company = svoc.Business_Name__c == Null ? 'TestCompanyN' : svoc.Business_Name__c,  - for testing purpose
                    LastName =(svoc.Contact_Name__c == Null ? svoc.Prospect_Name__c : svoc.Contact_Name__c),                               
                    Company = svoc.Business_Name__c,
                    Status = 'Not Started',
                    LeadSource = 'BT Sport',                                                               
                    Ownerid = '00G200000013FiG',                                                     
                    BTS_Single_View_UID__c = svoc.BTS_Single_View_UID__c,
                    Type_of_venue__c = svoc.Type_of_Business__c,
                    Other_venue__c = svoc.Venue_Type_Other__c,
                    Venue_detail__c = svoc.Venue_Classification_Other__c,
                    Existing_Customer__c = svoc.BT_Sport_Customer__c,
                    Number_of_business_premises__c = svoc.Number_of_Premises__c,
                    BTSport_Genesis_how_many_tvs__c = svoc.How_Many_TVs_Currently_at_Your_Pub__c,
                    Are_you_a_Sky_customer__c = svoc.Did_venue_have_SKY__c,
                    BTSport_Genesis_Rateable_Value__c  = svoc.Customers_RV__c,  
                    Banding__c = svoc.Full_Rate_Card_Band__c,
                    Price__c = String.ValueOf(svoc.Price__c),                  
                    Email = (svoc.DM_Email__c == '') ? svoc.Email__c:svoc.DM_Email__c,
                    How_likely_is_DM_to_subscribe_c__c = svoc.How_likely_is_DM_to_subscribe__c,
                    
                    Phone = svoc.Telephone_Number__c,
                    Customer_Interested__c = svoc.Customer_Interested__c,
                    Likelihood_to_Buy__c = svoc.Likelihood_to_Buy__c,
                    Why_Did_The_Customer_Not_Buy_BT_Sport__c = svoc.Rejection_Reasons__c,
                    Comment__c = svoc.Other_Rejection_Reason__c,                    
                    Outcomes__c = svoc.Outcomes__c,
                    Other_Outcome__c = svoc.Other_Outcome__c,
                    //CR11512
                    Are_you_food_or_drink_based__c=svoc.Are_you_food_based__c,
                    Broadband_Provider__c=svoc.Broadband_Provider__c,
                    Broadband_Status__c=svoc.Broadband_Status__c,
                    Music_in_Venue_and_or_via_ext_Provider__c=svoc.Music_in_Venue_and_or_via_ext_Provider__c,
                    Ownership_Type__c=svoc.Type_of_Ownership__c,
                    Sky_Flag__c=svoc.Not_a_SKY_Pub__c,
                    Telephone_Provider__c=svoc.Telephone_Provider__c,
                    Which_Sports_Are_Your_Cust_Interested_In__c	=svoc.Which_Sports_Are_Your_Cust_Interested_In__c
                    
                );           
                newLead.LastName = newLead.LastName != Null ? newLead.LastName : 'UNKNOWN';
                newLead.Company = newLead.Company != Null ? newLead.Company: 'UNKNOWN';
                
                newLeadsMap.put(svoc.Id,newLead );      
                
            }
            
        } 
        
        if(legacyCugIds.values().size() > 0){
            for(Account acc: [Select Account.Id,Account.CUG__c,(Select Id from Account.Contacts where Contact.Target_Contact__c = TRUE LIMIT 1) from Account where Account.CUG__c IN: legacyCugIds.values()]){
                relatedAcc.put(acc.CUG__c,acc.Id);
                for(Contact con : acc.Contacts){
                    relatedCon.put(acc.Id,con.Id);
                }                      
            }
        }
        
        for(Id svocId : legacyCugIds.KeySet()){
            tempLead = newLeadsMap.get(svocId);
            tempLead.Account__c = relatedAcc.get(legacyCugIds.get(svocId));
            tempLead.Contact__c = relatedCon.get(tempLead.Account__c);
            newLeadsMap.put(svocId,tempLead);
        }
        
        If(newLeadsMap.Values().size() > 0){
            system.debug('@@@@ '+newLeadsMap);
            Insert newLeadsMap.Values();}
        
        SVOCList = [Select Account__c,Lead__c,CUG_ID__c,Legacy_Customer_Group_Key__c from BT_Sport_SVOC__c where Id IN:Trigger.new];       
        
        for(BT_Sport_SVOC__c svoc :SVOCList){
            
            if(svoc.Legacy_Customer_Group_Key__c != null && relatedAcc.containskey(svoc.Legacy_Customer_Group_Key__c)){
                svoc.Account__c = relatedAcc.get(svoc.Legacy_Customer_Group_Key__c);
                if(svoc.Lead__c == null){
                    svoc.Lead__c = newLeadsMap.get(svoc.Id).Id;                 
                } 
                updateSVOCMap.put(svoc.Id,svoc);
            }
            else if(svoc.CUG_ID__c != null && relatedAcc.containskey(svoc.CUG_ID__c)){
                svoc.Account__c = relatedAcc.get(svoc.CUG_ID__c);
                if(svoc.Lead__c == null){
                    svoc.Lead__c = newLeadsMap.get(svoc.Id).Id;                 
                }
                updateSVOCMap.put(svoc.Id,svoc);
            }else if(svoc.Lead__c == null){
                svoc.Lead__c = newLeadsMap.get(svoc.Id).Id;
                updateSVOCMap.put(svoc.Id,svoc);
            }  
        }
        
        If(updateSVOCMap.Values().size() > 0){
            system.debug('@@@@ '+updateSVOCMap.Values());
            update updateSVOCMap.Values();
        }
    }
    
    if(Trigger.isUpdate){   
        
        Map<Id,String> legacyCugIds = new Map<Id,String>();  
        Map<String,Id> relatedAcc = new Map<String,Id>();
        Map<Id,Id> relatedCon = new Map<Id,Id>();
        Map<Id,BT_Sport_SVOC__c> SVOCMap = new Map<Id,BT_Sport_SVOC__c>();
        List<Lead> leadsToUpdateOrCreate = new List<Lead>();
        List<Lead> leadsToUpdate = new List<Lead>();                
        Map<Id,Lead> leadsToCreate = new Map<Id,Lead>();
        List<BT_Sport_SVOC__c> SVOCtoUpdate = new List<BT_Sport_SVOC__c>();
        
        for(BT_Sport_SVOC__c svoc : Trigger.new){
            if(svoc.Legacy_Customer_Group_Key__c != Null){
                legacyCugIds.put(svoc.Id,svoc.Legacy_Customer_Group_Key__c);
            }else if(svoc.CUG_ID__c != Null){
                legacyCugIds.put(svoc.Id,svoc.CUG_ID__c);
            }       
            SVOCMap.put(svoc.Lead__c,svoc);
        }
        
        if(legacyCugIds.values().size() > 0){
            for(Account acc: [Select Account.Id,Account.CUG__c,(Select Id from Account.Contacts where Contact.Target_Contact__c = TRUE LIMIT 1) from Account where Account.CUG__c IN: legacyCugIds.values()]){
                relatedAcc.put(acc.CUG__c,acc.Id);
                for(Contact con : acc.Contacts){
                    relatedCon.put(acc.Id,con.Id);
                }                      
            }
        }
        
        leadsToUpdateOrCreate = [Select Id,LastName,Type_of_venue__c,Company,Are_you_a_Sky_customer__c,IsConverted from Lead WHERE Id IN: SVOCMap.KeySet()];
        
        For(Lead l :leadsToUpdateOrCreate){
            
            if(!l.IsConverted) {
                BT_Sport_SVOC__c tempSVOC = SVOCMap.get(l.Id);
                //l.LastName = tempSVOC.Contact_Name__c == Null ? 'TestLN' : tempSVOC.Contact_Name__c;         //for testing purpose
                //l.Company = tempSVOC.Business_Name__c == null ? 'TestCompanyN' : tempSVOC.Business_Name__c;  //for testing purpose
                l.LeadSource = 'BT Sport';                                                                     
                //l.OwnerId = '00G200000013FiG';    //Dont change the ower while updatng lead - ask from Steve Howell
                l.BTS_Single_View_UID__c = tempSVOC.BTS_Single_View_UID__c;
                if(l.LastName == null){
                    l.LastName =(tempSVOC.Contact_Name__c == Null ? tempSVOC.Prospect_Name__c : tempSVOC.Contact_Name__c);                
                    l.LastName = l.LastName != Null ? l.LastName : 'UNKNOWN';
                }
                l.Type_of_venue__c = tempSVOC.Type_of_Business__c;
                if(l.Company == null){
                    l.Company = tempSVOC.Business_Name__c;
                    l.Company = l.Company != Null ? l.Company: 'UNKNOWN';
                }
                l.Other_venue__c = tempSVOC.Venue_Type_Other__c;
                l.Venue_detail__c = tempSVOC.Venue_Classification_Other__c;
                l.Existing_Customer__c = tempSVOC.BT_Sport_Customer__c;
                l.Number_of_business_premises__c = tempSVOC.Number_of_Premises__c;
                l.BTSport_Genesis_how_many_tvs__c = tempSVOC.How_Many_TVs_Currently_at_Your_Pub__c;
                l.Are_you_a_Sky_customer__c = tempSVOC.Did_venue_have_SKY__c;
                l.BTSport_Genesis_Rateable_Value__c  = tempSVOC.Customers_RV__c;                
                l.Banding__c = tempSVOC.Full_Rate_Card_Band__c;
                l.Price__c = String.ValueOf(tempSVOC.Price__c);
                //l.Email= tempSVOC.Email__c;
                l.Email = (tempSVOC.DM_Email__c == '') ? tempSVOC.Email__c:tempSVOC.DM_Email__c;
                l.How_likely_is_DM_to_subscribe_c__c = tempSVOC.How_likely_is_DM_to_subscribe__c;
                /* 
                l.Customer_Interested__c = tempSVOC.Customer_Interested__c;
                l.Why_Did_The_Customer_Not_Buy_BT_Sport__c = tempSVOC.Rejection_Reasons__c;
                l.Comment__c = tempSVOC.Other_Rejection_Reason__c;
                l.Likelihood_to_Buy__c = tempSVOC.Likelihood_to_Buy__c;
                l.Outcomes__c = tempSVOC.Outcomes__c;
                l.Other_Outcome__c = tempSVOC.Other_Outcome__c; 
                */                
                //CR11512
                l.Are_you_food_or_drink_based__c = tempSVOC.Are_you_food_based__c;
                l.Broadband_Provider__c = tempSVOC.Broadband_Provider__c;
                l.Broadband_Status__c = tempSVOC.Broadband_Status__c;
                l.Music_in_Venue_and_or_via_ext_Provider__c = tempSVOC.Music_in_Venue_and_or_via_ext_Provider__c;
                l.Ownership_Type__c = tempSVOC.Type_of_Ownership__c;
                l.Sky_Flag__c = tempSVOC.Not_a_SKY_Pub__c;
                l.Telephone_Provider__c = tempSVOC.Telephone_Provider__c;
                l.Which_Sports_Are_Your_Cust_Interested_In__c	=tempSVOC.Which_Sports_Are_Your_Cust_Interested_In__c;
                
                l.Account__c = relatedAcc.get(legacyCugIds.get(tempSVOC.Id));
                l.Contact__c = relatedCon.get(l.Account__c);             
                
                leadsToUpdate.add(l);
                
                if(l.Account__c != tempSVOC.Account__c){
                    BT_Sport_SVOC__c svoc = new BT_Sport_SVOC__c();
                    svoc.Id = tempSVOC.Id;
                    svoc.Account__c = l.Account__c;
                    SVOCtoUpdate.add(svoc);
                }
            }
            else {
                /* 19/03/19 - discusse with Steve and decided that creating new lead is not required so commenting this code
                * 
                BT_Sport_SVOC__c tempSVOCcreate = SVOCMap.get(l.Id);
                Lead newLead = new Lead();
                //newLead.LastName = tempSVOCcreate.Contact_Name__c == Null ? 'TestLN' : tempSVOCcreate.Contact_Name__c;                    //For testing purpose
                //newLead.Company = tempSVOCcreate.Business_Name__c == null ? 'TestCompanyN' : tempSVOCcreate.Business_Name__c;             //For testing purpose   
                if(newLead.LastName == null){
                newLead.LastName =(tempSVOCcreate.Contact_Name__c == Null ? tempSVOCcreate.Prospect_Name__c : tempSVOCcreate.Contact_Name__c);
                newLead.LastName = newLead.LastName != Null ? newLead.LastName : 'UNKNOWN';
                }
                if(newLead.Company == null){
                newLead.Company = tempSVOCcreate.Business_Name__c;
                newLead.Company = newLead.Company != Null ? newLead.Company: 'UNKNOWN';
                }
                newLead.Status = 'Not Started';
                newLead.LeadSource = 'BT Sport';                                                                                  
                newlead.Ownerid = '00G200000013FiG';                               
                newlead.BTS_Single_View_UID__c = tempSVOCcreate.BTS_Single_View_UID__c;
                newLead.Type_of_venue__c = tempSVOCcreate.Type_of_Business__c;
                newLead.Other_venue__c = tempSVOCcreate.Venue_Type_Other__c;
                newLead.Venue_detail__c = tempSVOCcreate.Venue_Classification_Other__c;
                newLead.Existing_Customer__c = tempSVOCcreate.BT_Sport_Customer__c;
                newLead.Number_of_business_premises__c = tempSVOCcreate.Number_of_Premises__c;
                newLead.BTSport_Genesis_how_many_tvs__c = tempSVOCcreate.How_Many_TVs_Currently_at_Your_Pub__c;
                newLead.Are_you_a_Sky_customer__c = tempSVOCcreate.Did_venue_have_SKY__c;
                newLead.BTSport_Genesis_Rateable_Value__c  = tempSVOCcreate.Customers_RV__c;                
                newLead.Banding__c = tempSVOCcreate.Full_Rate_Card_Band__c;
                newLead.Price__c = String.ValueOf(tempSVOCcreate.Price__c);
                newLead.Email= tempSVOCcreate.Email__c;
                newLead.How_likely_is_DM_to_subscribe_c__c = tempSVOCcreate.How_likely_is_DM_to_subscribe__c;
                
                newLead.Customer_Interested__c = tempSVOCcreate.Customer_Interested__c;
                newLead.Why_Did_The_Customer_Not_Buy_BT_Sport__c = tempSVOCcreate.Rejection_Reasons__c;
                newLead.Comment__c = tempSVOCcreate.Other_Rejection_Reason__c;
                newLead.Likelihood_to_Buy__c = tempSVOCcreate.Likelihood_to_Buy__c;
                newLead.Outcomes__c = tempSVOCcreate.Outcomes__c;
                newLead.Other_Outcome__c = tempSVOCcreate.Other_Outcome__c;  
                
                newLead.Account__c = relatedAcc.get(legacyCugIds.get(tempSVOCcreate.Id));
                newLead.Contact__c = relatedCon.get(newLead.Account__c);
                
                leadsToCreate.put(tempSVOCcreate.Id, newLead); //mpk for testing

			*/
            }
        }
        If(leadsToUpdate.size() > 0){
            update leadsToUpdate;}
        
        If(leadsToCreate.size() > 0){
            insert leadsToCreate.values();}
        
        for(Id svocId : leadsToCreate.keyset()) {
            BT_Sport_SVOC__c svoc = new BT_Sport_SVOC__c();
            svoc.Id = svocId;
            svoc.Lead__c = leadsToCreate.get(svocId).Id;
            SVOCtoUpdate.add(svoc);
        }
        If(SVOCtoUpdate.size() > 0){
            update SVOCtoUpdate;}  
        
    }
    /*CR10511 Trigger logic begins - to calculate RollUp Summary*/
    If(Trigger.IsAfter){
        
        If(Trigger.IsInsert)
            BTSportMVOCHandler.AfterInsertHandler(Trigger.New);        
        If(Trigger.IsUpdate && BTSportMVOCHandler.AfterUpdateRecurssionflag){
            BTSportMVOCHandler.AfterUpdateRecurssionflag = False;
            BTSportMVOCHandler.AfterUpdateHandler(Trigger.New); 
        }
    }
    
    If(Trigger.IsDelete){
        If(Trigger.IsDelete)
            BTSportMVOCHandler.AfterDeleteHandler(Trigger.Old);                      
    }
    
    if(Trigger.isUnDelete){
        BTSportMVOCHandler.AfterUnDeleteHandler(Trigger.new);     
    }
    /*CR10511 Trigger logic ends*/
    
}