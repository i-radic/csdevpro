trigger tgrOpportunityBefore on Opportunity(before insert, before update, before delete) {

    //  Code #101OICDT opportunityInsertCloseData  All Lines used in this trigger would be marked with this code for future reference.
    //  Code #102SFCNT trgSetFosCreatorName  All Lines used in this trigger would be marked with this code for future reference.
    //  Code #103FTSFT trgFOSTimeStampFields  All Lines used in this trigger would be marked with this code for future reference.
    //  Code #104FUAFT trgFOSUpdateAccountFlag All Lines used in this trigger would be marked with this code for future reference.
    //  Code #105LFSAT LFS_autoAllocate All Lines used in this trigger would be marked with this code for future reference.
    //  Code #106FOENA FOSOwnerEmailUpdateNotification All Lines used in this trigger would be marked with this code for future reference.
    //  Date            Author                     Comment
    //  19/08/11        Krupakar Jogannagari 
    //  29/09/11        Rita Opoku-Serebuoh         Added #106FOENA for sending FOS email notifications
    //  28/03/2011      Anil Devunoori              Added code related to CR2180 - Last Activity on a campaign
    //  15/05/2012      Adam Soobroy                Added code related to Lease Desk - Set RAG Status to None if Leasing History has 0 records
    //  15/11/2012      Phanidra Mangipudi          Added code related to LFS - for routing Wan/Lan to BTLB's
    //  21/01/2013      Rita Opoku-Serebuoh         Commented out FOS related code - Search string 'FOSRemoval'
    //  12/03/2013      John McGovern               Additional code to support Busiess Direct SF to SF CR3809
    //  25/06/2013      Rita Opoku-Serebuoh         Routing for new 'New Mobile (Non BT More than 5)' sub-category for LFS CR4460
    //  28/11/2014      Anusha Manyam               CR3845 - to Prevent BTLB agents creating opportunities against default owner accounts
    //  28/05/2015      Sridevi Bayyapuneedi        Additional code as per CR7583
    //  20/11/2015      Anusha Manyam               CR8318 - To send opportunity to the business direct org
    //  Copyright © 2012


    
    if(TriggerDeactivating__c.getInstance().Opportunity__c ) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }  
    // --- This is used since most of the triggers are for Insert and Update on Opportunity.
    if (trigger.isInsert || trigger.isUpdate) {
        //#101OICD - Variable Declaration -- Start    
        StaticVariables.setAccountDontRun(true); // stop account trigger from running
        List < Id > accIds = new List < Id > ();
        Set < id > accidset = new set < id > ();
        //#101OICD - Variable Declaration -- End
        
        //To stop Updating Last Account Call Date Field in Accounts by below Profiles
        ID SystemAdminProfileId = '00e20000001MX7zAAG';
        ID DataLoaderprofileId = '00e200000015EhzAAE';
        ID ReportingProfileId = '00e20000001QB4tAAG';
        ID BTLBAdminProfileId = '00e200000015K0NAAU';
        ID CorpAdminProfileID = '00e200000015IRvAAM';
        //ID fosRecType = '01220000000A1Sg'; - Already Duplicated in trgSetFosCreatorName
        ID stdRecType = '01220000000PjDu';
        ID btSalesforceId = '00520000001UxeU';
        //#103FTSFT - Variable Declaration -- End

        //#105LFSAT - Variable Declaration -- Start
        ID lfsRecType = '01220000000ABFB';
        ID btniRecType = '01220000000AIB4';
        ID lfsCorpProfileId = '00e200000015P8S';
        Set < String > LFS_CUGs = new Set < String > ();
        Map < String, Account > mCugAcc = new Map < String, Account > ();
        //#105LFSAT - Variable Declaration -- End

        //CR SF5935 - Variable Declaration --start           
        Set < id > acctidsset = new Set < id > ();
        List < id > acctids = new List < id > ();
        Account tempacct;
        Account acct;
        List < Account > newaccts = new List < Account > ();
        Map < Id, List < opportunity >> mapOppty = new Map < Id, List < opportunity >> ();
        Date target_date = Date.today() - 30;
        String CreatedWithin10Days;
        //CR SF5935 - Variable Declaration --end

        Set < Id > allCampaignIds = new Set < Id > (); //CR2180

        //#101OICD -- Loop Through Opp records  and assign values -- Start
        for (Opportunity o: Trigger.new) {

            system.debug('Originator EIN ' + o.Originator_EIN__c);
            allCampaignIds.add(o.CampaignId); //CR2180        
            //if(o.Opportunity_Owner_EIN__c == '802558891') o.ABR_Count__c =1;  

            /* -- Included in another LFS trigger START
            //As Part of CR CR2578 **************** Start**********
            if (trigger.isInsert) {
                if (o.RecordTypeId == '01220000000ABFB' && lfsCorpProfileId == UserInfo.getProfileId()) {
                    o.Name = 'LFS_' + o.Name;
                }
            }
            //As Part of CR CR2578 **************** End************
            -- Included in another LFS trigger END */
            
            if (o.CloseDate == null) o.CloseDate = (Date.today() + 365);

            if (o.AccountId != null && o.Auto_Assign_Owner__c) accIds.add(o.accountId);

            if (o.OwnerId != null) o.owner__c = o.OwnerId;

            //add flag for org to org
            if ((o.Routing_Product__c == 'Engage IT' || o.Product_Family__c == 'ENGAGE IT') && o.CreatedDate >= (date.newinstance(2011, 11, 19)) && o.Type != 'Records Only') {
                o.s2s_Link__c = 'BTB to Engage IT';
            }
            //added in CR3809 & CR8318
            if ((o.Routing_Product__c == 'Business Direct' || o.Routing_Product__c == 'IT Services Education' || o.Routing_Product__c == 'IT Services Enterprise'|| o.Routing_Product__c == 'IT Services Kit'|| o.Product_Family__c == 'BUSINESS DIRECT') && o.CreatedDate >= (date.newinstance(2013, 04, 18)) && o.Type != 'Records Only') {
                o.s2s_Link__c = 'BTB to Business Direct';
            }
            if ((o.Routing_Product__c == 'iNet' || o.Product_Family__c == 'INET') && o.CreatedDate >= (date.newinstance(2013, 04, 18)) && o.Type != 'Records Only') {
                o.s2s_Link__c = 'BTB to iNet';
            }
            //added as per CR7583
            if ((o.Product_Family__c == 'IT SERVICES') && o.CreatedDate >= (date.newinstance(2015, 05, 28)) && o.Type != 'Records Only'){
                o.s2s_Link__c = 'BTB to IT Services';
            }

            //CR SF5935 -- start        
            if (trigger.isInsert) {
                if (o.AccountId != NULL) acctidsset.add(o.AccountId);
            } //CR SF5935 -- end

            if (trigger.isupdate) {
                if (trigger.newmap.get(o.id).stagename != trigger.oldmap.get(o.id).stagename) {
                    accidset.add(o.accountid);
                }
                 if (trigger.newmap.get(o.id).Astute_Status__c != trigger.oldmap.get(o.id).Astute_Status__c) {
                    if(trigger.newmap.get(o.id).Astute_Status__c != 'Assigned' )
                    o.Astute_Status_Date__c  = date.today();
                }     
            } else if (trigger.isinsert) {
                accidset.add(o.accountid);
            }
            
            //#105LFSAT -- Code Part Of the Opportunity Loop in LFS_autoAllocate -- Start
            if (trigger.isInsert) {
                
                 //CR3845 --START--                                          
                /*
                if (userinfo.getname() != 'Connection User') {//added to stop running when connection user as null pointer returned
                    if( Userinfo.getProfileId().substring(0, 15) == '00e20000001QC4D' || Userinfo.getProfileId().substring(0, 15) == '00e20000001Q80F' ){
                        BTUtilities.userDetails btUser = BTUtilities.getUser(Userinfo.getUserId());
                        user u=[select Id,Department from user where id=: Userinfo.getUserId() limit 1];
                        if(Userinfo.getUserId().substring(0, 15) != o.Account_OwnerID__c && btUser.getfirstLevelManagerId.substring(0, 15) != o.Account_OwnerID__c && u.Department != 'BTLB Bath and Bristol' && u.Department != 'BTLB Greater London North East'){
                            system.debug('User Id :'+Userinfo.getUserId().substring(0, 15)+'\n Account Owner Id:'+o.Account_OwnerID__c +'\n User manager Id:'+ btUser.getfirstLevelManagerId.substring(0, 15) );
                            //Set<Id> subRoleIds = BTUtilities.getRoleSubordinateUsers(o.Account_OwnerID__c);
                            //if(!subRoleIds.contains(Userinfo.getUserId()))
                                o.addError('Opportunity creation not available due to ownership: for further clarification please contact the base team');
                        }                                                              
                    }
                }   */
                // CR3845 --END—- 
                
                /* -- // Included in another LFS trigger START
                //CR4160 Start
                //LFS Process For Sales EIN                              
                if (lfsRecType == o.RecordTypeId) {
                    system.debug('Entered LFS MAIN IF');
                    if (!String.isEmpty(o.Sales_Agent_EIN__c)) {
                        system.debug('Entered LFS SALES AGENT EIN IF');
                        List < User > user = [select IsActive, Email, OUC__c, ManagerId, EIN__c, Id from User where EIN__c = : o.Sales_Agent_EIN__c and EIN__c <> null];
                        if (user.size() > 0) {
                            o.OwnerId = user[0].Id;
                        }
                    } else {
                        system.debug('Entered LFS SALES AGENT EIN ELSE');
                        o.OwnerId = '005200000025XVC';
                        if (lfsRecType == o.RecordTypeId && lfsCorpProfileId != UserInfo.getProfileId()) 
                            o.OwnerId = '00520000001Bu2U'; //LIVE LFS default user
                        if (lfsRecType == o.RecordTypeId && lfsCorpProfileId == UserInfo.getProfileId()) {
                            o.OwnerId = UserInfo.getUserId();
                            o.Lead_Source__c = 'Leads from Service';
                            o.Service_Site__c = 'Skelmersdale Corporate Bus Centre';
                        }
                    }
                }
                //CR4160 End
                -- Included in LFS trigger END
                */
                //BTNI LFS Process - JMM
                if (btniRecType == o.RecordTypeId && o.Lead_Source__c == 'BTNI BRT') {
                    if (o.Sales_Agent_EIN__c != '') {
                        List < User > user = [select IsActive, Email, OUC__c, ManagerId, EIN__c, Id from User where EIN__c = : o.Sales_Agent_EIN__c and EIN__c <> null];
                        if (user.size() > 0) {
                            o.OwnerId = user[0].Id;
                        }
                    } else {
                        o.OwnerId = '005200000025XVC';
                    }
                }
                /*-- Included in LFS trigger START
                if (o.LFS_CUG__c != null && lfsRecType == o.RecordTypeId) o.LFS_CUG__c = 'CUG' + o.LFS_CUG__c;
                -- Included in LFS trigger END 
                */
                // populating the Manager's EIN/OUC and Email Address for LFS - CR# SF7824
                //Modified as part of LFS Requirement CR2578
                //if (((o.Originator_EIN__c != null) || (o.Originator_EIN__c != '')) && ((lfsRecType == o.RecordTypeId) && (o.Lead_Source__c == 'Leads from Service'))) {
                /* -- Included in LFS trigger START
                if (lfsRecType == o.RecordTypeId && o.Lead_Source__c == 'Leads from Service') {
                    //List<User> user = [Select ManagerId, Manager_EIN__c, Manager.IsActive, Email, OUC__c from User where EIN__c =: o.Originator_EIN__c];
                    List < User > user = null;
                    if (lfsCorpProfileId == UserInfo.getProfileId()) user = [select IsActive, Email, OUC__c, ManagerId, EIN__c, Id, Manager.IsActive, Manager.ManagerId, Manager.EIN__c, Manager.Manager.IsActive, Manager.Manager.ManagerId, Manager.Manager.EIN__c, Manager.Manager.Manager.IsActive, Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.Manager.EIN__c from User where id = : UserInfo.getUserId()]; //Modified as part of LFS Requirement CR2578 //EIN__c =: o.Originator_EIN__c and EIN__c <> null];
                    else user = [select IsActive, Email, OUC__c, ManagerId, EIN__c, Id, Manager.IsActive, Manager.ManagerId, Manager.EIN__c, Manager.Manager.IsActive, Manager.Manager.ManagerId, Manager.Manager.EIN__c, Manager.Manager.Manager.IsActive, Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.Manager.EIN__c from User where EIN__c = : o.Originator_EIN__c and EIN__c <> null];
                    if (user.size() > 0) {
                        if (o.Account_Sector__c != null && o.Account_Sector__c != '') {
                            if (o.Account_Sector__c.toLowerCase().contains('corporate')) {
                                o.Customer_Type__c = 'Corporate';
                            } else {
                                o.Customer_Type__c = 'BT Business';
                            }
                        }
                        o.Originator_EIN__c = user[0].EIN__c;
                        o.Originator_Email__c = user[0].Email;
                        o.LFS_OUC__c = user[0].OUC__c;
                        o.Manager_EIN__c = user[0].Manager.EIN__c;
                    }
                    // LFS Owner Routing in Insert Only    
                    if(o.Product__c == 'BTBD Enquiry'){
                        o.OwnerId = '00520000001U801'; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                    } 
                    if (o.Product__c == 'BT Sport'){
                        o.OwnerId = '00520000001U90L'; // BT Sport Leads => John Rowlands
                    }
                    if(o.UKBS_Customer__c){                                             
                        if(!String.isEmpty(o.Sales_Agent_EIN__c)){
                            User salesUser = [SELECT Id FROM USER Where EIN__C = :o.Sales_Agent_EIN__c];                        
                            o.OwnerId = salesUser.Id;
                        }                       
                    }
                    // Below if condition was already commented
                    //if(o.UKBS_Customer__c && (o.Sales_Agent_EIN__c != null || o.Sales_Agent_EIN__c != '')){
                    //  if(o.Customer_type__c == 'BT Business'){
                    //      if(o.Product__c == 'Broadband' ||o.Product__c == 'Broadband-Movers' ||o.Product__c == 'Additional lines and maintenance' ||o.Product__c == 'Calls/Lines Non BT Customers (winback)'){
                    //          o.OwnerId = '00520000001U9Ta'; //Route to Brentwood BS Team
                    //      }
                    //  }
                    //}
                    // LFS Owner Routing END
                }
                -- Included in LFS trigger END */
            }
            //#105LFSAT -- Code Part Of the Opportunity Loop in LFS_autoAllocate -- End

            //#103FTSFT -- Code Part Of the Opportunity Loop in trgFOSTimeStampFields -- Start
            /* -- Included in LFS trigger START
            if (trigger.isUpdate) {

                if (o.LFS_CUG__c != null && lfsRecType == o.RecordTypeId && o.OwnerId != null && o.AccountId == null) {
                    
                    List < Account > AcountOwner = [Select id, ownerId from Account WHERE CUG__c = : o.LFS_CUG__c limit 1];
                    system.debug('LFS CUG Value @ tgrBefore :'+o.LFS_CUG__c);                    
                    AcountOwner.add(mCugAcc.get(o.LFS_CUG__c));
                    system.debug('accountOwner size ' + AcountOwner.size());
                    if (!AcountOwner.isEmpty()) {
                        o.AccountId = AcountOwner[0].id;
                    }
                    if (!AcountOwner.isEmpty() && (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == '')) {
                        //CR3398 - added Wan/Lan for Routing
                        if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN30') || (o.Product__c == 'VOIP')) && (o.Customer_type__c == 'BT Business')) {
                            o.OwnerId = AcountOwner[0].OwnerId;
                        }
                        if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)'))) && (o.Customer_type__c == 'BT Business'))) {
                            //if(o.Sub_Category__c == 'Additional handsets (existing BT mobile)') {  o.OwnerId = '005200000027Gy2'; } // Default Warrignton routing
                            o.OwnerId = AcountOwner[0].OwnerId; //BTB Default Account Owner
                            
                        } else if (o.Customer_type__c == 'Corporate') {                            
                                o.OwnerId = AcountOwner[0].OwnerId;
                                if(o.Product__c == 'BTBD Enquiry'){
                                    o.OwnerId = '00520000001U801'; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                                }
                        }
                    } else if (AcountOwner.isEmpty() && (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == '')) {
                        //CR3398 - added Wan/Lan for Routing
                        if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN') || (o.Product__c == 'VOIP')) && (o.Customer_type__c == 'BT Business')) {
                            o.OwnerId = '00520000001Befx'; //BTB Default Account Owner
                        }
                        if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)'))) && (o.Customer_type__c == 'BT Business'))) {
                            //if(o.Sub_Category__c == 'Additional handsets (existing BT mobile)') {  o.OwnerId = '005200000027Gy2'; } // Default Warrignton routing
                            o.OwnerId = '00520000001Befx';  //BTB Default Account Owner
                        } else if (o.Customer_type__c == 'Corporate') {
                            o.OwnerId = '00520000001Befs'; //Corporate Default Account Owner
                            if(o.Product__c == 'BTBD Enquiry'){
                                    o.OwnerId = '00520000001U801'; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                                }
                        } else if(o.Product__c == 'BTBD Enquiry'){
                            o.OwnerId = '00520000001U801'; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                        }
                    }
                }
                if (o.LFS_CUG__c == null && lfsRecType == o.RecordTypeId && o.OwnerId != null && o.AccountId == null) {
                    //CR3398 - added Wan/Lan for Routing
                    if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN30') || (o.Product__c == 'VOIP')) && (o.Customer_type__c == 'BT Business')) {
                        o.OwnerId = '00520000001Befx'; //BTB Default Account Owner
                    }
                    if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)') )) && (o.Customer_type__c == 'BT Business'))) {
                        
                        o.OwnerId = '00520000001Befx';  //BTB Default Account Owner
                    } else if (o.Customer_type__c == 'Corporate') {
                        o.OwnerId = '00520000001Befs'; //Corporate Default Account Owner
                    }
                }         
            }
            -- Included in LFS trigger END */
            //#103FTSFT -- Code Part Of the Opportunity Loop in trgFOSTimeStampFields -- End   
        }        

        //CR2180 - start        
        if (allCampaignIds.size() > 0) {
            list < Campaign > allcampaignlist = new list < Campaign > ();
            updateCampaignLastContacted updateLastActivityDate = new updateCampaignLastContacted();
            allcampaignlist = updateLastActivityDate.updateCampaign(allCampaignIds);
            update allcampaignlist;
        }
        //CR2180 - end
        
        if (trigger.isInsert) {
            for (opportunity op: [Select Id, StageName, CloseDate, Account.Id From Opportunity where StageName = 'Cancelled'
            and CloseDate >= : target_date
            and Account.Id In: acctidsset]) {
                List < opportunity > LstOppty = new List < opportunity > ();
                if (mapOppty.containsKey(op.Account.Id)) {
                    LstOppty = mapOppty.get(op.Account.Id);
                    LstOppty.add(op);
                    mapOppty.put(op.Account.Id, LstOppty);
                } else {
                    LstOppty.add(op);
                    mapOppty.put(op.Account.Id, LstOppty);
                }
            }

            for (Id I: mapOppty.keySet()) {
                List < opportunity > LstOpptyQry = new List < opportunity > ();
                LstOpptyQry = mapOppty.get(I);
                if (LstOpptyQry.size() > 0) CreatedWithin10Days = 'Y';
                else CreatedWithin10Days = 'N';
                acct = new Account(id = I);
                newaccts.add(acct);
            }
            try {
                update newaccts;
            } Catch(exception e) {
                system.debug('error inserting list');
            } finally {
                tempacct = null;
                acct = null;
                newaccts = null;
            }
        }
        
        if (accIds.size() > 0) {
            for (List < Account > accs: [Select ownerId from account where Id in : accIds]) {
                for (Account acc: accs) {
                    for (Opportunity o: Trigger.new) {
                        if ((o.Auto_Assign_Owner__c && o.AccountId == acc.id)) {
                            o.OwnerId = acc.OwnerId;
                            o.sendAssignOwnerNotificationEmail__c = true;
                            if (o.StageName == 'Created') o.StageName = 'Assigned';
                        }

                        if (o.OwnerId != null) o.owner__c = o.OwnerId;
                        o.Auto_Assign_Owner__c = false;
                    }
                }
            }
        }
        
        //Added by GS to update account last call date.
        //Update the account last call date field for these accounts Exclude below Profiles
        if(UserInfo.getProfileId() != SystemAdminProfileId && UserInfo.getProfileId() != DataLoaderprofileId && 
           UserInfo.getProfileId() != ReportingProfileId && UserInfo.getProfileId() != BTLBAdminProfileId 
           && UserInfo.getProfileId() != CorpAdminProfileID ){      
        list < account > updateCallDate = new list < account > ();
        updateAccountLastContacted updateAccountCallDate = new updateAccountLastContacted();
        //updateCallDate = updateAccountCallDate.updateAccount(accidset);
        String objName = 'Opportunities';
        updateCallDate = updateAccountCallDate.updateAccount(accidset, objName);
        update updateCallDate;
        //#101OICD -- Part of 101OICD Trigger to Modify Account Information -- End
           }
    }
    /* Telephone Number Look up - Start
    if (Trigger.isUpdate) {
    
        for (Opportunity o: Trigger.New) {
    
            if (o.RecordTypeId == '01220000000ABFB') {
    
                system.debug('LFS CUG Value UPDATE# @ tgrBefore :' + o.LFS_CUG__c);
                if (o.LFS_CUG__c != null) {
                    Account AcountOwner = [Select id, ownerId from Account WHERE CUG__c = : o.LFS_CUG__c limit 1];
                    if (AcountOwner != null) {
                        o.AccountId = AcountOwner.id;
                        system.debug('New Account Owner:' + o.Product__c);
                        //CR3398 - added Wan/Lan for Routing
                        if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN') || (o.Product__c == 'VOIP')) && (o.Customer_type__c == 'BT Business')) {
                            system.debug('Am Entered:');
                            o.OwnerId = AcountOwner.OwnerId;
                        }
                        if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)'))) && (o.Customer_type__c == 'BT Business'))) {
                            //if(o.Sub_Category__c == 'Additional handsets (existing BT mobile)') {  o.OwnerId = '005200000027Gy2'; } // Default Warrignton routing
                            o.OwnerId = AcountOwner.OwnerId; //BTB Default Account Owner
    
                        } else if (o.Customer_type__c == 'Corporate') {
                            o.OwnerId = AcountOwner.OwnerId;
                        }
                    }
                } else if (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == '') {
                    //CR3398 - added Wan/Lan for Routing
                    if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN') || (o.Product__c == 'VOIP')) && (o.Customer_type__c == 'BT Business')) {
                        o.OwnerId = '00520000001Befx'; //BTB Default Account Owner
                    }
                    if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)'))) && (o.Customer_type__c == 'BT Business'))) {
                        //if(o.Sub_Category__c == 'Additional handsets (existing BT mobile)') {  o.OwnerId = '005200000027Gy2'; } // Default Warrignton routing
                        o.OwnerId = '00520000001Befx'; //BTB Default Account Owner
                    } else if (o.Customer_type__c == 'Corporate') {
                        o.OwnerId = '00520000001Befs'; //Corporate Default Account Owner
                        if (o.Product__c == 'BTBD Enquiry') {
                            o.OwnerId = '00520000001U801'; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                        }
                    } else if (o.Product__c == 'BTBD Enquiry') {
                        o.OwnerId = '00520000001U801'; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                    }
                }
            }
        }
    }   
    
    // Telephone number lookup - end */ 
    
    if (trigger.isUpdate || trigger.isInsert) {

        List < Id > OpptyIds = new List < Id > ();
    
        for (Opportunity BC: Trigger.New)
            OpptyIds.add(BC.Id);
        List < OpportunityLineItem > OLI1 = [Select Qty__c, ProdCode__c, ProdName__c, FFA_Supplier__c, Category__c, First_Bill_Date__c, TotalPrice, OpportunityId, Description From OpportunityLineItem where OpportunityId IN: OpptyIds];
    
        Map < Id, string > LineItemProdQtyHolders = new Map < Id, String > ();
        Map < Id, Date > LineItemDateHolders = new Map < Id, Date > ();
    
        system.debug('Entered into oPp Line Item Coding');
        Integer volMobileNew = 0, volBlackberryNew = 0, volDataNew = 0, voliPhoneNew = 0, volVoiceNew = 0;
        Date firstDate;
    
        for (OpportunityLineItem lineItem: OLI1) {
    
            if (lineItem.ProdCode__c == 'MVNRV' || lineItem.ProdCode__c == 'MVDBO' || lineItem.ProdCode__c == 'TTMON' || lineItem.ProdCode__c == 'TTOFN' || lineItem.ProdCode__c == 'TTSOL'|| lineItem.ProdCode__c == 'MVNEE') {
                volVoiceNew += (Integer) lineItem.Qty__c;
            } else if (lineItem.ProdCode__c == 'BBNEW' || lineItem.ProdCode__c == 'TTBBR') {
                volBlackberryNew += (Integer) lineItem.Qty__c;
            } else if (lineItem.ProdCode__c == 'BTBMB' || lineItem.ProdCode__c == 'MDEXN' || lineItem.ProdCode__c == 'TAB04'|| lineItem.ProdCode__c == 'MBNEE' || lineItem.ProdCode__c == 'MBINE') {
                volDataNew += (Integer) lineItem.Qty__c;
            } else if (lineItem.ProdCode__c == 'IPHON' || lineItem.ProdCode__c == 'TTIPN') {
                voliPhoneNew += (Integer) lineItem.Qty__c;
            }
    
            if (lineItem.Category__c == 'Main') {
                firstDate = lineItem.First_Bill_Date__c;
            }
    
            volMobileNew = volBlackberryNew + volDataNew + voliPhoneNew + volVoiceNew;
            LineItemProdQtyHolders.put(lineItem.OpportunityId, String.valueOf(volMobileNew));
            LineItemDateHolders.put(lineItem.OpportunityId, firstDate);
        }
    
        for (Opportunity op: Trigger.new) {
            for (Id mapId: LineItemProdQtyHolders.KeySet()) {
                if (op.Id == mapId) {
                    op.volMobileCurrent_B2B__c = LineItemProdQtyHolders.get(mapId);
                    op.firstBillDateCurrentt_B2B__c = LineItemDateHolders.get(mapId);
                }
            }
        }
    }
    
        // END
}