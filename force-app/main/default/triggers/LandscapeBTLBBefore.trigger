trigger LandscapeBTLBBefore on Landscape_BTLB__c (before update) {
/*####################################################################
Trigger to add history of into a custom object for tracking purposes.
####################################################################*/
Set<id> LSIDs = new Set<id>();
string jmRun = null; 
integer jmCallLines = 0;
integer jmCustomerDetails = 0;
integer jmIT = 0;
integer jmInternetBroadband = 0;
integer jmMobile = 0;
integer jmVOIP = 0;
integer jmTelephoneSystem = 0;

//Get the id's      
for(Landscape_BTLB__c LS:Trigger.new){  
     LSIDs.add(LS.id);
}

for(Landscape_BTLB__c LSCount:Trigger.new){

If (LSCount.Calls_contract_expiry_date__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.Lines_contract_expiry_date__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.Confirm_no_of_lines_your_company_has__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.Do_you_use_BT_for_all_your_calls__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.Do_you_use_BT_for_all_your_lines__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.Has_a_call_contract_expiry_date__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.Has_line_contract_expiry_date__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.How_much_spent_annually_on_calls__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.If_not_BT_who_do_you_use_for_calls__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.If_not_BT_who_do_you_use_for_your_lines__c != null) {
            jmCallLines = jmCallLines+1;                                 
        }
If (LSCount.Approx_when_did_your_biz_start_trading__c != null) {
            jmCustomerDetails = jmCustomerDetails+1;                                 
        }
If (LSCount.Are_you_planning_to_move_premises__c != null) {
            jmCustomerDetails = jmCustomerDetails+1;                                 
        }
If (LSCount.How_many_employees_are_not_office_based__c != null) {
            jmCustomerDetails = jmCustomerDetails+1;                                 
        }
If (LSCount.How_many_employees_in_your_company__c != null) {
            jmCustomerDetails = jmCustomerDetails+1;                                 
        }
If (LSCount.How_many_sites_abroad__c != null) {
            jmCustomerDetails = jmCustomerDetails+1;                                 
        }
If (LSCount.How_many_sites_in_the_UK__c != null) {
            jmCustomerDetails = jmCustomerDetails+1;                                 
        }
If (LSCount.ISP_contract_expiry_date__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.Do_you_have_a_website__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.what_is_your_Domain_Name__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }        
If (LSCount.Do_you_sell_online__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.Does_your_company_have_Broadband__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.Has_an_expiry_date_for_ISP_contract__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.Who_is_your_internet_service_provider__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.Data_internet_security_provider__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }  
If (LSCount.Who_is_your_leased_line_provider__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.Leased_line_contract_end_date__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }
If (LSCount.Number_of_leased_line_connections__c != null) {
            jmInternetBroadband = jmInternetBroadband+1;                                 
        }            
If (LSCount.Has_an_IT_Support_contract_renewal_date__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Network_contract_renewal_date__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Are_the_PC_s_networked_or_stand_alone__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Are_the_sites_networked_together__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Do_you_have_a_server__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Has_a_network_contract_renewal_date__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Has_an_expiry_date_for_ISP_contract__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.How_many_are_Laptops_Notebooks__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.No_of_company_PC_s_including_Apple_Mac_s__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Who_manages_your_network__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Who_provides_your_IT_support__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Who_supplies_your_network_LAN__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.LAN_contract_renewal_date__c != null) {
            jmIT = jmIT+1;                                 
        }
If (LSCount.Mobile_contract_expiry_date__c != null) {
            jmMobile = jmMobile+1;                                 
        }
If (LSCount.Has_a_mobile_contract_renewal_date__c != null) {
            jmMobile = jmMobile+1;                                 
        }
If (LSCount.No_mobile_handsets_used_for_biz_purposes__c != null) {
            jmMobile = jmMobile+1;                                 
        }
If (LSCount.Use_company_mob_to_send_receive_emails__c != null) {
            jmMobile = jmMobile+1;                                 
        }
If (LSCount.Who_is_your_business_mobile_supplier__c != null) {
            jmMobile = jmMobile+1;                                 
        }
If (LSCount.Finance_contract_expiry_date__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.Phone_system_maint_contract_expiry_date__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.Has_a_contract_expiry_date_for_maint__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.Has_expiry_date_for_finance_contract__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.Has_finance_on_existing_system__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.How_many_years_old_is_your_phone_system__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.Who_maintains_your_phone_switch_system__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.Who_manufactured_your_phone_switch_sys__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.When_purchased_your_phone_switch_system__c != null) {
            jmTelephoneSystem = jmTelephoneSystem+1;                                 
        }
If (LSCount.Contract_expiry_for_calls_over_internet__c != null) {
            jmVOIP = jmVOIP+1;                                 
        }
If (LSCount.Has_internet_calls_supp_contract_expiry__c != null) {
            jmVOIP = jmVOIP+1;                                 
        }
If (LSCount.Which_supplier_for_calls_over_internet__c != null) {
            jmVOIP = jmVOIP+1;                                 
        }


LSCount.zCallLines__c = jmCallLines ;
LSCount.zCustomerDetails__c = jmCustomerDetails ;
LSCount.zIT__c = jmIT;
LSCount.zInternetBroadband__c = jmInternetBroadband ;
LSCount.zMobile__c = jmMobile ;
LSCount.zTelephoneSystem__c = jmTelephoneSystem;
LSCount.zVOIP__c = jmVOIP;


}

// insert custom history object
List<Landscape_BTLB_History__c> HistNew= new List <Landscape_BTLB_History__c>() ; 
//    system.debug('AllLS' + findLSToADD ) ;

for(Landscape_BTLB__c LS:Trigger.new){
    Landscape_BTLB__c beforeUpdate = System.Trigger.oldMap.get(LS.Id);
     //string contractEndDateString = getOpportunity().Contract_End_Date__c.format();
       
If (LS.Calls_contract_expiry_date__c != beforeUpdate.Calls_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Calls contract expiry date';
             If (LS.Calls_contract_expiry_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Calls_contract_expiry_date__c.format();
            }
            If (beforeUpdate.Calls_contract_expiry_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Calls_contract_expiry_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q20';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Lines_contract_expiry_date__c != beforeUpdate.Lines_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Lines contract expiry date';
             If (LS.Lines_contract_expiry_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Lines_contract_expiry_date__c.format();
            }
            If (beforeUpdate.Lines_contract_expiry_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Lines_contract_expiry_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q25';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Confirm_no_of_lines_your_company_has__c != beforeUpdate.Confirm_no_of_lines_your_company_has__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Confirm no of lines your company has';
            HistRec.Old_Value__c = beforeUpdate.Confirm_no_of_lines_your_company_has__c;
            HistRec.New_Value__c = LS.Confirm_no_of_lines_your_company_has__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q22';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Do_you_use_BT_for_all_your_calls__c != beforeUpdate.Do_you_use_BT_for_all_your_calls__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you use BT for all your calls';
            HistRec.Old_Value__c = beforeUpdate.Do_you_use_BT_for_all_your_calls__c;
            HistRec.New_Value__c = LS.Do_you_use_BT_for_all_your_calls__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q18';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Do_you_use_BT_for_all_your_lines__c != beforeUpdate.Do_you_use_BT_for_all_your_lines__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you use BT for all your lines';
            HistRec.Old_Value__c = beforeUpdate.Do_you_use_BT_for_all_your_lines__c;
            HistRec.New_Value__c = LS.Do_you_use_BT_for_all_your_lines__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q23';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Has_a_call_contract_expiry_date__c != beforeUpdate.Has_a_call_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a call contract expiry date';
            HistRec.Old_Value__c = beforeUpdate.Has_a_call_contract_expiry_date__c;
            HistRec.New_Value__c = LS.Has_a_call_contract_expiry_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q19.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Has_line_contract_expiry_date__c != beforeUpdate.Has_line_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has line contract expiry date';
            HistRec.Old_Value__c = beforeUpdate.Has_line_contract_expiry_date__c;
            HistRec.New_Value__c = LS.Has_line_contract_expiry_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q24.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.How_much_spent_annually_on_calls__c != beforeUpdate.How_much_spent_annually_on_calls__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How much spent annually on calls';
            HistRec.Old_Value__c = beforeUpdate.How_much_spent_annually_on_calls__c;
            HistRec.New_Value__c = LS.How_much_spent_annually_on_calls__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q21';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.If_not_BT_who_do_you_use_for_calls__c != beforeUpdate.If_not_BT_who_do_you_use_for_calls__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who is your calls provider';
            HistRec.Old_Value__c = beforeUpdate.If_not_BT_who_do_you_use_for_calls__c;
            HistRec.New_Value__c = LS.If_not_BT_who_do_you_use_for_calls__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q19';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.If_not_BT_who_do_you_use_for_your_lines__c != beforeUpdate.If_not_BT_who_do_you_use_for_your_lines__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'If not BT, who do you use for your lines';
            HistRec.Old_Value__c = beforeUpdate.If_not_BT_who_do_you_use_for_your_lines__c;
            HistRec.New_Value__c = LS.If_not_BT_who_do_you_use_for_your_lines__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q24';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Approx_when_did_your_biz_start_trading__c != beforeUpdate.Approx_when_did_your_biz_start_trading__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Approx when did your biz start trading';
             If (LS.Approx_when_did_your_biz_start_trading__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Approx_when_did_your_biz_start_trading__c.format();
            }
            If (beforeUpdate.Approx_when_did_your_biz_start_trading__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Approx_when_did_your_biz_start_trading__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q30';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Are_you_planning_to_move_premises__c != beforeUpdate.Are_you_planning_to_move_premises__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Are you planning to move premises';
            HistRec.Old_Value__c = beforeUpdate.Are_you_planning_to_move_premises__c;
            HistRec.New_Value__c = LS.Are_you_planning_to_move_premises__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q5';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.How_many_employees_are_not_office_based__c != beforeUpdate.How_many_employees_are_not_office_based__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many employees are not office based';
            HistRec.Old_Value__c = beforeUpdate.How_many_employees_are_not_office_based__c;
            HistRec.New_Value__c = LS.How_many_employees_are_not_office_based__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.How_many_employees_in_your_company__c != beforeUpdate.How_many_employees_in_your_company__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many employees in your company';
            HistRec.Old_Value__c = beforeUpdate.How_many_employees_in_your_company__c;
            HistRec.New_Value__c = LS.How_many_employees_in_your_company__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.How_many_sites_abroad__c != beforeUpdate.How_many_sites_abroad__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many sites abroad';
            HistRec.Old_Value__c = beforeUpdate.How_many_sites_abroad__c;
            HistRec.New_Value__c = LS.How_many_sites_abroad__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.How_many_sites_in_the_UK__c != beforeUpdate.How_many_sites_in_the_UK__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many sites in the UK';
            HistRec.Old_Value__c = beforeUpdate.How_many_sites_in_the_UK__c;
            HistRec.New_Value__c = LS.How_many_sites_in_the_UK__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q3';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.ISP_contract_expiry_date__c != beforeUpdate.ISP_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'ISP contract expiry date';
             If (LS.ISP_contract_expiry_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.ISP_contract_expiry_date__c.format();
            }
            If (beforeUpdate.ISP_contract_expiry_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.ISP_contract_expiry_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Do_you_have_a_website__c != beforeUpdate.Do_you_have_a_website__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you have a website';
            HistRec.Old_Value__c = beforeUpdate.Do_you_have_a_website__c;
            HistRec.New_Value__c = LS.Do_you_have_a_website__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.what_is_your_Domain_Name__c  != beforeUpdate.what_is_your_Domain_Name__c ) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you have a website';
            HistRec.Old_Value__c = beforeUpdate.what_is_your_Domain_Name__c ;
            HistRec.New_Value__c = LS.what_is_your_Domain_Name__c ;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q32';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }        
If (LS.Do_you_sell_online__c != beforeUpdate.Do_you_sell_online__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you sell online';
            HistRec.Old_Value__c = beforeUpdate.Do_you_sell_online__c;
            HistRec.New_Value__c = LS.Do_you_sell_online__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Does_your_company_have_Broadband__c != beforeUpdate.Does_your_company_have_Broadband__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Does your company have Broadband';
            HistRec.Old_Value__c = beforeUpdate.Does_your_company_have_Broadband__c;
            HistRec.New_Value__c = LS.Does_your_company_have_Broadband__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q6';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Has_an_expiry_date_for_ISP_contract__c != beforeUpdate.Has_an_expiry_date_for_ISP_contract__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has an expiry date for ISP contract';
            HistRec.Old_Value__c = beforeUpdate.Has_an_expiry_date_for_ISP_contract__c;
            HistRec.New_Value__c = LS.Has_an_expiry_date_for_ISP_contract__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q7.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }         
If (LS.Who_is_your_internet_service_provider__c != beforeUpdate.Who_is_your_internet_service_provider__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who is your internet service provider';
            HistRec.Old_Value__c = beforeUpdate.Who_is_your_internet_service_provider__c;
            HistRec.New_Value__c = LS.Who_is_your_internet_service_provider__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q7';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Data_internet_security_provider__c != beforeUpdate.Data_internet_security_provider__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Data & internet security provider';
            HistRec.Old_Value__c = beforeUpdate.Data_internet_security_provider__c;
            HistRec.New_Value__c = LS.Data_internet_security_provider__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8.3';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        } 
If (LS.Who_is_your_leased_line_provider__c != beforeUpdate.Who_is_your_leased_line_provider__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who is your leased line provider';
            HistRec.Old_Value__c = beforeUpdate.Who_is_your_leased_line_provider__c;
            HistRec.New_Value__c = LS.Who_is_your_leased_line_provider__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q31';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Leased_line_contract_end_date__c != beforeUpdate.Leased_line_contract_end_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Leased line contract end date';
             If (LS.Leased_line_contract_end_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Leased_line_contract_end_date__c.format();
            }
            If (beforeUpdate.Leased_line_contract_end_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Leased_line_contract_end_date__c.format();
            }    
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q31.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        } 
If (LS.Number_of_leased_line_connections__c != beforeUpdate.Number_of_leased_line_connections__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Number of leased line connections';
            HistRec.Old_Value__c = beforeUpdate.Number_of_leased_line_connections__c;
            HistRec.New_Value__c = LS.Number_of_leased_line_connections__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q31.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }                         
If (LS.IT_support_contract_renewal_date__c != beforeUpdate.IT_support_contract_renewal_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'IT support contract renewal date';
             If (LS.IT_support_contract_renewal_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.IT_support_contract_renewal_date__c.format();
            }
            If (beforeUpdate.IT_support_contract_renewal_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.IT_support_contract_renewal_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11.3';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Network_contract_renewal_date__c != beforeUpdate.Network_contract_renewal_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Network contract renewal date';
             If (LS.Network_contract_renewal_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Network_contract_renewal_date__c.format();
            }
            If (beforeUpdate.Network_contract_renewal_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Network_contract_renewal_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4.3';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Are_the_PC_s_networked_or_stand_alone__c != beforeUpdate.Are_the_PC_s_networked_or_stand_alone__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Are the PC\'s networked or stand alone';
            HistRec.Old_Value__c = beforeUpdate.Are_the_PC_s_networked_or_stand_alone__c;
            HistRec.New_Value__c = LS.Are_the_PC_s_networked_or_stand_alone__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Are_the_sites_networked_together__c != beforeUpdate.Are_the_sites_networked_together__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Are the sites networked together';
            HistRec.Old_Value__c = beforeUpdate.Are_the_sites_networked_together__c;
            HistRec.New_Value__c = LS.Are_the_sites_networked_together__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Do_you_have_a_server__c != beforeUpdate.Do_you_have_a_server__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you have a server';
            HistRec.Old_Value__c = beforeUpdate.Do_you_have_a_server__c;
            HistRec.New_Value__c = LS.Do_you_have_a_server__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Has_a_network_contract_renewal_date__c != beforeUpdate.Has_a_network_contract_renewal_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a network contract renewal date';
            HistRec.Old_Value__c = beforeUpdate.Has_a_network_contract_renewal_date__c;
            HistRec.New_Value__c = LS.Has_a_network_contract_renewal_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4.4';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Has_an_IT_Support_contract_renewal_date__c != beforeUpdate.Has_an_IT_Support_contract_renewal_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has an IT Support contract renewal date';
            HistRec.Old_Value__c = beforeUpdate.Has_an_IT_Support_contract_renewal_date__c;
            HistRec.New_Value__c = LS.Has_an_IT_Support_contract_renewal_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.How_many_are_Laptops_Notebooks__c != beforeUpdate.How_many_are_Laptops_Notebooks__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many are Laptops/Notebooks';
            HistRec.Old_Value__c = beforeUpdate.How_many_are_Laptops_Notebooks__c;
            HistRec.New_Value__c = LS.How_many_are_Laptops_Notebooks__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.No_of_company_PC_s_including_Apple_Mac_s__c != beforeUpdate.No_of_company_PC_s_including_Apple_Mac_s__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'No of company PC\'s including Apple Mac\'s';
            HistRec.Old_Value__c = beforeUpdate.No_of_company_PC_s_including_Apple_Mac_s__c;
            HistRec.New_Value__c = LS.No_of_company_PC_s_including_Apple_Mac_s__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q9';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Who_manages_your_network__c != beforeUpdate.Who_manages_your_network__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who manages your network';
            HistRec.Old_Value__c = beforeUpdate.Who_manages_your_network__c;
            HistRec.New_Value__c = LS.Who_manages_your_network__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Who_provides_your_IT_support__c != beforeUpdate.Who_provides_your_IT_support__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who provides your IT support';
            HistRec.Old_Value__c = beforeUpdate.Who_provides_your_IT_support__c;
            HistRec.New_Value__c = LS.Who_provides_your_IT_support__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Who_supplies_your_network_LAN__c != beforeUpdate.Who_supplies_your_network_LAN__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who supplies your network (LAN)';
            HistRec.Old_Value__c = beforeUpdate.Who_supplies_your_network_LAN__c;
            HistRec.New_Value__c = LS.Who_supplies_your_network_LAN__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.LAN_contract_renewal_date__c != beforeUpdate.LAN_contract_renewal_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'LAN contract renewal date';
            If (LS.LAN_contract_renewal_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.LAN_contract_renewal_date__c.format();
            }
            If (beforeUpdate.LAN_contract_renewal_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.LAN_contract_renewal_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10.3';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Mobile_contract_expiry_date__c != beforeUpdate.Mobile_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Mobile contract expiry date';
            If (LS.Mobile_contract_expiry_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Mobile_contract_expiry_date__c.format();
            }
            If (beforeUpdate.Mobile_contract_expiry_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Mobile_contract_expiry_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q17';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
                  
If (LS.Has_a_mobile_contract_renewal_date__c != beforeUpdate.Has_a_mobile_contract_renewal_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a mobile contract renewal date';
            HistRec.Old_Value__c = beforeUpdate.Has_a_mobile_contract_renewal_date__c;
            HistRec.New_Value__c = LS.Has_a_mobile_contract_renewal_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q16.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.No_mobile_handsets_used_for_biz_purposes__c != beforeUpdate.No_mobile_handsets_used_for_biz_purposes__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'No mobile handsets used for biz purposes';
            HistRec.Old_Value__c = beforeUpdate.No_mobile_handsets_used_for_biz_purposes__c;
            HistRec.New_Value__c = LS.No_mobile_handsets_used_for_biz_purposes__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q15';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Use_company_mob_to_send_receive_emails__c != beforeUpdate.Use_company_mob_to_send_receive_emails__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Use company mob to send & receive emails';
            HistRec.Old_Value__c = beforeUpdate.Use_company_mob_to_send_receive_emails__c;
            HistRec.New_Value__c = LS.Use_company_mob_to_send_receive_emails__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q17.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Who_is_your_business_mobile_supplier__c != beforeUpdate.Who_is_your_business_mobile_supplier__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who is your business mobile supplier';
            HistRec.Old_Value__c = beforeUpdate.Who_is_your_business_mobile_supplier__c;
            HistRec.New_Value__c = LS.Who_is_your_business_mobile_supplier__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q16';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Finance_contract_expiry_date__c != beforeUpdate.Finance_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Finance contract expiry date';
             If (LS.Finance_contract_expiry_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Finance_contract_expiry_date__c.format();
            }
            If (beforeUpdate.Finance_contract_expiry_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Finance_contract_expiry_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.5';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Phone_system_maint_contract_expiry_date__c != beforeUpdate.Phone_system_maint_contract_expiry_date__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Phone system maint contract expiry date';
             If (LS.Phone_system_maint_contract_expiry_date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Phone_system_maint_contract_expiry_date__c.format();
            }
            If (beforeUpdate.Phone_system_maint_contract_expiry_date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Phone_system_maint_contract_expiry_date__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.2';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Has_a_contract_expiry_date_for_maint__c != beforeUpdate.Has_a_contract_expiry_date_for_maint__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a contract expiry date for maint';
            HistRec.Old_Value__c = beforeUpdate.Has_a_contract_expiry_date_for_maint__c;
            HistRec.New_Value__c = LS.Has_a_contract_expiry_date_for_maint__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Has_expiry_date_for_finance_contract__c != beforeUpdate.Has_expiry_date_for_finance_contract__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has expiry date for finance contract';
            HistRec.Old_Value__c = beforeUpdate.Has_expiry_date_for_finance_contract__c;
            HistRec.New_Value__c = LS.Has_expiry_date_for_finance_contract__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.4';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Has_finance_on_existing_system__c != beforeUpdate.Has_finance_on_existing_system__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has finance on existing system';
            HistRec.Old_Value__c = beforeUpdate.Has_finance_on_existing_system__c;
            HistRec.New_Value__c = LS.Has_finance_on_existing_system__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.3';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.How_many_years_old_is_your_phone_system__c != beforeUpdate.How_many_years_old_is_your_phone_system__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many years old is your phone system';
            HistRec.Old_Value__c = beforeUpdate.How_many_years_old_is_your_phone_system__c;
            HistRec.New_Value__c = LS.How_many_years_old_is_your_phone_system__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q13';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Who_maintains_your_phone_switch_system__c != beforeUpdate.Who_maintains_your_phone_switch_system__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who maintains your phone switch system';
            HistRec.Old_Value__c = beforeUpdate.Who_maintains_your_phone_switch_system__c;
            HistRec.New_Value__c = LS.Who_maintains_your_phone_switch_system__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Who_manufactured_your_phone_switch_sys__c != beforeUpdate.Who_manufactured_your_phone_switch_sys__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who manufactured your phone switch sys';
            HistRec.Old_Value__c = beforeUpdate.Who_manufactured_your_phone_switch_sys__c;
            HistRec.New_Value__c = LS.Who_manufactured_your_phone_switch_sys__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q12';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.When_purchased_your_phone_switch_system__c != beforeUpdate.When_purchased_your_phone_switch_system__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'When purchased your phone switch system';
            If (LS.When_purchased_your_phone_switch_system__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.When_purchased_your_phone_switch_system__c.format();
            }
            If (beforeUpdate.When_purchased_your_phone_switch_system__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.When_purchased_your_phone_switch_system__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q13.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Contract_expiry_for_calls_over_internet__c != beforeUpdate.Contract_expiry_for_calls_over_internet__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Contract expiry for calls over internet';
            If (LS.Contract_expiry_for_calls_over_internet__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Contract_expiry_for_calls_over_internet__c.format();
            }
            If (beforeUpdate.Contract_expiry_for_calls_over_internet__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Contract_expiry_for_calls_over_internet__c.format();
            }  
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'VOIP';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q29';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Has_internet_calls_supp_contract_expiry__c != beforeUpdate.Has_internet_calls_supp_contract_expiry__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has internet calls supp contract expiry';
            HistRec.Old_Value__c = beforeUpdate.Has_internet_calls_supp_contract_expiry__c;
            HistRec.New_Value__c = LS.Has_internet_calls_supp_contract_expiry__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'VOIP';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q28';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
If (LS.Which_supplier_for_calls_over_internet__c != beforeUpdate.Which_supplier_for_calls_over_internet__c) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Which supplier for calls over internet';
            HistRec.Old_Value__c = beforeUpdate.Which_supplier_for_calls_over_internet__c;
            HistRec.New_Value__c = LS.Which_supplier_for_calls_over_internet__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'VOIP';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q27';
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }

//stamp the user updating this question to the main record
If (LS.Lines_contract_expiry_date__c != beforeUpdate.Lines_contract_expiry_date__c) {  
            LS.Lines_Last_Updated__c = system.today();
            LS.Lines_Last_Updated_By__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();         
        }
If (LS.Calls_contract_expiry_date__c != beforeUpdate.Calls_contract_expiry_date__c) {
            LS.Calls_Last_Updated__c = system.today();
            LS.Calls_Last_Updated_By__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        }
If (LS.Mobile_contract_expiry_date__c != beforeUpdate.Mobile_contract_expiry_date__c) {
            LS.Mobile_Last_Updated__c = system.today();
            LS.Mobile_Last_Updated_By__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        }
If (LS.ISP_contract_expiry_date__c != beforeUpdate.ISP_contract_expiry_date__c) {
            LS.ISP_Last_Updated__c = system.today();
            LS.ISP_Last_Updated_By__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
        }
        
// processing the last updated to replace workflow conflict
Profile checkProfile;
checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];
system.debug ('ProfileID: ' + checkProfile.Name) ;

If (checkProfile.Name.contains('BTLB')) {
            LS.Last_Updated__c = system.today();
            LS.Last_Updated_By__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();                   
        }        
If (checkProfile.Name.contains('BS Classic')) {
            LS.Last_Updated_BS_Date__c = system.today();
            LS.Last_Updated_BS_Person__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();                   
        }      
    }
     
    If (jmRun != null) {
        insert HistNew;
    }
    

}