trigger case_after_update on Case (after update) {
	
	if (Trigger.isAfter && Trigger.isUpdate)
	{
		CaseTriggerHandler triggerHandler = new CaseTriggerHandler();
		
		triggerHandler.handleCaseAfterUpdates(Trigger.newMap, Trigger.oldMap);
	}

}