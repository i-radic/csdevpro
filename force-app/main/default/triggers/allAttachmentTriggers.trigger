trigger allAttachmentTriggers on Attachment (after delete, after insert, after update, 
before delete, before insert, before update) 
{
    No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
    
    if(notriggers == null || !notriggers.Flag__c)
    {
        // Copies agreement attachment accross to the opportunity to make it easier to use with ClickApprove
        // Also copies attachments on basket accross to the opportunity for the same purpose
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate))
        {
            Map<Attachment,Id> attAgrMap = new Map<Attachment,Id>(); 
            Map<Attachment,Id> attBskMap = new Map<Attachment,Id>();
            for(Attachment att : trigger.new)
            {
                if(att.ParentId.getSObjectType() == csclm__Agreement__c.SobjectType)
                {
                    attAgrMap.put(att,att.ParentId);
                }
                else if( (att.ParentId.getSObjectType() == cscfga__Product_Basket__c.SobjectType) && (att.Name != 'profitandloss.json'))
                {
                
                    attBskMap.put(att,att.ParentId);
                } 
            }
            if(!attAgrMap.isEmpty())
            {
                Map<Id,csclm__Agreement__c> aggMap = new Map<Id,csclm__Agreement__c>([select id, csclm__Opportunity__c
                                                                                   from csclm__Agreement__c 
                                                                                   where id in :attAgrMap.values()]);
                if(!aggMap.isEmpty())
                {
                    // Map Opp Id with attachment name 
                    Map<Id,String> oppAttMap = new Map<Id,String>();
                    for(Attachment att : trigger.new)
                    {
                        if(attAgrMap.containsKey(att))
                        {
                            if(aggMap.containsKey(att.ParentId))
                            {
                                oppAttMap.put(aggMap.get(att.ParentId).csclm__Opportunity__c,att.Name);
                            }
                        }
                    }
                    // Delete old attachments from opportunity
                    List<Attachment> deleteAttList = new List<Attachment>();
                    List<Attachment> suspectAttList = [select id, name, ParentId 
                                                       from Attachment 
                                                       where ParentId in :oppAttMap.keySet()];
                    if(!suspectAttList.isEmpty())
                    {
                        for(Attachment suspectAtt : suspectAttList)
                        {
                            if(oppAttMap.get(suspectAtt.ParentId) == suspectAtt.name)
                            {
                                deleteAttList.add(suspectAtt);
                            }
                        }
                        if(!deleteAttList.isEmpty())
                        {
                            delete deleteAttList;
                        }
                    }
                    
                    List<Attachment> insertAttList = new List<Attachment>();
                    for(Attachment att : trigger.new)
                    {
                        if(attAgrMap.containsKey(att))
                        {
                            if(aggMap.containsKey(att.ParentId))
                            {
                                // Moves attachment to Opportunity
                                //att.ParentId = aggMap.get(att.ParentId).csclm__Opportunity__c;
                                
                                // Copies attachment to Opportunity
                                Attachment insertAtt = new Attachment
                                    (
                                    name = att.name,
                                    body = att.body,
                                    parentid = aggMap.get(att.ParentId).csclm__Opportunity__c
                                    );
                                insertAttList.add(insertAtt); 
                            }
                        }
                    }
                    if(!insertAttList.isEmpty())
                    {
                        insert insertAttList;
                    }
                }      
            }
            
            if(!attBskMap.isEmpty())
            {
                Map<Id,cscfga__Product_Basket__c> bskMap = new Map<Id,cscfga__Product_Basket__c>
                    ([select id, cscfga__Opportunity__c
                      from cscfga__Product_Basket__c 
                      where id in :attBskMap.values()]);
                if(!bskMap.isEmpty())
                {
                    // Map Opp Id with attachment name 
                    Map<Id,String> oppAttMap = new Map<Id,String>();
                    for(Attachment att : trigger.new)
                    {
                        if(attBskMap.containsKey(att))
                        {
                            if(bskMap.containsKey(att.ParentId))
                            {
                                oppAttMap.put(bskMap.get(att.ParentId).cscfga__Opportunity__c,att.Name);
                            }
                        }
                    }
                    // Delete old attachments from opportunity
                    List<Attachment> deleteAttList = new List<Attachment>();
                    List<Attachment> suspectAttList = [select id, name, ParentId 
                                                       from Attachment 
                                                       where ParentId in :oppAttMap.keySet()];
                    if(!suspectAttList.isEmpty())
                    {
                        for(Attachment suspectAtt : suspectAttList)
                        {
                            if(oppAttMap.get(suspectAtt.ParentId) == suspectAtt.name)
                            {
                                deleteAttList.add(suspectAtt);
                            }
                        }
                        if(!deleteAttList.isEmpty())
                        {
                            delete deleteAttList;
                        }
                    }
                    
                    List<Attachment> insertAttList = new List<Attachment>();
                    for(Attachment att : trigger.new)
                    {
                        if(attBskMap.containsKey(att))
                        {
                            if(bskMap.containsKey(att.ParentId))
                            {
                                // Moves attachment to Opportunity
                                //att.ParentId = bskMap.get(att.ParentId).csclm__Opportunity__c;
                                
                                // Copies attachment to Opportunity
                                Attachment insertAtt = new Attachment
                                    (
                                    name = att.name,
                                    body = att.body,
                                    parentid = bskMap.get(att.ParentId).cscfga__Opportunity__c
                                    );
                                insertAttList.add(insertAtt); 
                            }
                        }
                    }
                    if(!insertAttList.isEmpty())
                    {
                        insert insertAttList;
                    }
                }      
            }
            
        }
    }
}