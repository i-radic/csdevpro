trigger ContractChangeTrigger on Contract_Change__c (after insert, after update, before insert, before update) {


	ContractChangeTriggerHandler handler = new ContractChangeTriggerHandler(
								Trigger.newMap, Trigger.oldMap, Trigger.new, Trigger.old,
								Trigger.Size,
								Trigger.IsInsert, Trigger.IsUpdate, Trigger.IsBefore, Trigger.IsAfter,
								Trigger.IsExecuting	);

	// Execute if Trigger Enabler custom setting is enabled
    if (EE_TriggerClass.isTriggerEnabled('Contract_Change__c')) {
    	
		handler.processTrigger();
		
    }


}