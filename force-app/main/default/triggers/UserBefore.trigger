trigger UserBefore on User (before insert, before update) {
   //     Author           Date            Comment
    //    Jon Brayshaw     26/05/2010      Added Functionality to state if user is at or below certain person in hierarchy
    //    John McGovern    11/01/12        Chanes to support chatter auto groups/individuals following, commented out for testing
    
    
    List<User> ApexTriggerEnabled = [Select Run_Apex_Triggers__c  from user  Where id = :Userinfo.getUserId()];
    boolean triggerRunner = ApexTriggerEnabled[0].Run_Apex_Triggers__c;
    if (trigger.isinsert && triggerRunner == true) { // only trigger if dataload user is updating records
        
        // getting id of default profile to assign to users when first created            
        List<Profile> profiles2 = [Select p.Id, p.Name from Profile p Where p.name = 'BT: Null'];
        string profileId2 = profiles2[0].Id;
        
        for(User u:Trigger.new) { 
                 u.TIMEZONESIDKEY = 'Europe/London';
                 u.LOCALESIDKEY  = 'en_GB';
                 u.EMAILENCODINGKEY = 'ISO-8859-1';                               
                 u.PROFILEID = profileId2;
                 u.LANGUAGELOCALEKEY = 'en_US';
                 u.ISACTIVE = FALSE;
                 if (u.Email == null){
                    u.email = 'no.reply@bt.com';
                 }
        }
    }
    
    for(User u:Trigger.new) {
        u.SharingOppsByRole__c=false;
        if(u.UserRoleId != null){
            for(OpportunityPartnerSharingRoles__C thisoppPartShareRole : OpportunityPartnerSharingRoles__C.getall().values()){
                if(u.UserRoleId == thisoppPartShareRole.RoleId__c)u.SharingOppsByRole__c=true;
            }
        }
        if (Trigger.isupdate) {
             User beforeUpdate = System.Trigger.oldMap.get(u.Id);
             
            If (u.ISACTIVE != beforeUpdate.ISACTIVE) {
                u.Status_Last_Changed__c = date.today();
            }
        }         
    }
/* replacing with Informatica
if(!Test.isRunningTest() && !StaticVariables.getUserDontRun() ){  
system.debug('JMM getUserDontRun(): '+StaticVariables.getUserDontRun());
Set<Id> addBTLB= new Set<Id>();
Set<Id> addLEMServ= new Set<Id>();
Set<Id> addBS= new Set<Id>();
Set<Id> addDesk= new Set<Id>();
Set<Id> addField= new Set<Id>();
Set<Id> addCS= new Set<Id>();
Set<Id> BTLBProf= new Set<Id>();
Set<Id> BSProf= new Set<Id>();
Set<Id> DeskProf= new Set<Id>();
Set<Id> FieldProf= new Set<Id>();
Set<Id> CSProf= new Set<Id>();

List<Profile> profs = [Select Id, Name from Profile];
    for (Profile p:profs){
        if(p.Name.Contains('BTLB')){
            BTLBprof.add(p.Id);
        }
        if(p.Name.Contains('BS Classic')){
            BSProf.add(p.Id);
        }        
        if(p.Name.Contains('DBAM')){
            DeskProf.add(p.Id);
        }  
        if(p.Name.Contains('Field')){
            FieldProf.add(p.Id);
        }   
        if(p.Name.Contains('Custom')){
            CSProf.add(p.Id);
        }                       
    }       
        
  for(User u:Trigger.new){
    //Copying isActive to isActive2 text field
    If(u.isActive){
        u.isActive2__c='True';
    }
    if (BTLBprof.contains(u.ProfileId) == True){
            addBTLB.add(u.Id);
            addLEMServ.add(u.Id);
    }
    if (BSprof.contains(u.ProfileId) == True){
            addBS.add(u.Id);
            addLEMServ.add(u.Id);
    }    
    if (DeskProf.contains(u.ProfileId) == True){
            addDesk.add(u.Id);
    }
    if (Fieldprof.contains(u.ProfileId) == True){
            addField.add(u.Id);
    }     
    if (CSProf.contains(u.ProfileId) == True){
            addCS.add(u.Id);
    }            
              
    If (Trigger.isupdate) {
         User beforeUpdate = System.Trigger.oldMap.get(u.Id);
         
        If (u.ISACTIVE != beforeUpdate.ISACTIVE) {
            u.Status_Last_Changed__c = date.today();
        }
    }
  }    
  //insert group members
  if(!addBTLB.isEmpty()){
      ChatterHelper.SendBTLB(addBTLB);
  } 
  
  if(!addBS.isEmpty()){
      ChatterHelper.SendBS(addBS);
  } 
  
  if(!addLEMServ.isEmpty()){
      ChatterHelper.SendLEMServ(addLEMServ);
  }   
  if(!addDesk.isEmpty()){
      ChatterHelper.SendDesk(addDesk);
  } 
     
  if(!addField.isEmpty()){
      ChatterHelper.SendField(addField);
  }   
  
  if(!addCS.isEmpty()){
      ChatterHelper.SendCS(addCS);
  }  

}  
*/        
}