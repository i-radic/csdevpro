trigger LeadAfter on Lead (after insert,after update) {
    
    Set<Id> leadstoUpdate = new Set<Id>();
    List<Lead> leadstoUpdateList = new List<Lead>();
    List<Lead> UpdateList = new List<Lead>();
    BT_Sport_SVOC__c newSVOC ;
    List<BT_Sport_SVOC__c> newSVOCList = new List<BT_Sport_SVOC__c>();
    List<BT_Sport_SVOC__c> svocs = new List<BT_Sport_SVOC__c>();
    
    Set<Id> oppIds = new Set<Id>();
    Set<Id> oppIdBTSs = new Set<Id>();
    Set<Id> leadIds = new Set<Id>();
    Set<Id> allLeads = new Set<Id>();
    Set<String> SVOCref = new Set<String>();
    public String HeaderID = null;
    
    Map<Id,String> LeadStatus =  new Map<Id,String>();
    Set<Id> campaignIds = new Set<Id>();
    Map<Id,Boolean> IsSVOCMap = new Map<Id,Boolean>();
    List<CampaignMember> CampMemUpdate = new List<CampaignMember>();
    
    for(Lead myLead: Trigger.new){        
        // excluding BT Sport records created from the BT Sport call back form
        if(Trigger.isInsert && myLead.RecordTypeId == '01220000000ANXqAAO' && myLead.LeadSource == 'BT Sport' && String.isBlank(mylead.BTS_Single_View_UID__c) && mylead.CreatedById != '00520000003r94CAAQ'){
            newSVOC = new BT_Sport_SVOC__c();   
            newSVOC.Lead__c = myLead.Id; 
            newSVOC.BTS_Single_View_UID__c = myLead.BTS_Single_View_UID_PG__c;
            newSVOC.RecordTypeId = '01220000000B3vfAAC';                        // Prospect of SVOC
            newSVOCList.add(newSVOC);
        }
        LeadStatus.put(myLead.Id,myLead.Status);
    }
    
    if(newSVOCList.size() >0){
        Insert newSVOCList;
    }
    
    for(Lead myLead: Trigger.new){
        //system.debug('harish lead'+mylead);
        allLeads.add(myLead.Id);
        //system.debug('harish lead'+mylead+allLeads+myLead.isconverted+myLead.Status);
        system.debug('mpk Lead isconverted = '+myLead.isconverted +' Status =' + myLead.Status);
        if((myLead.isconverted==false) && (myLead.Status=='Opportunity Identified')){
            system.debug('inside'+mylead);
            Database.LeadConvert lc = new database.LeadConvert();
            lc.setLeadId(myLead.Id);
            lc.convertedStatus = 'Opportunity Identified';
            //Database.ConvertLead(lc,true);
            lc.setDoNotCreateOpportunity(false);
            lc.AccountId= myLead.Account__c;
            lc.ContactId= myLead.Contact__c;
            
            
            //Added as part of CR10439
            if(myLead.RecordTypeId=='0120J000000jnCCQAY') {                
                lc.opportunityname = 'Adobe opportunity: '+ myLead.AccountName__c;
                //system.debug('opp name'+lc.opportunityname);               
            }//end of change CR10439
            else //if(myLead.RecordTypeId!='0120J000000jnCCQAY')
            {
                lc.OpportunityName='Landscape Opp: '+ myLead.AccountName__c;
            }
            if(myLead.LeadSource == 'BT Sport'){
                lc.OpportunityName='BT Sport: '+ myLead.AccountName__c;
            }
            if(myLead.LeadSource == 'Desk Landscape Alert'){
                lc.OwnerId = myLead.Account__r.OwnerId;
            }
            else{
                lc.OwnerId = Userinfo.GetUserID();
            }
            
            //UKBCE-186769_Genesys_IWD_Leads_Through_Service // CAT ID if(myLead.RecordTypeId=='0129E000000VWuk')
            if(myLead.RecordTypeId=='0120J000000kdaR'){// Production ID
                lc.OpportunityName='LFS_'+ myLead.AccountName__c;
            }
            
            StaticVariables.setTaskIsRun(True);  
            StaticVariables.settaskIsBeforerun(true);    
            
            StaticVariables.setcontactIsRunBefore(true);   
            StaticVariables.setAccountDontRun(true);   
            StaticVariables.setcontactBeforeTriggerRun(true);   
            StaticVariables.setAccountBeforeDontRun(true);   
            
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            System.assert(lcr.isSuccess());
            oppIds.add(lcr.getOpportunityId());
            leadIds.add(myLead.Id);
            if(!String.isBlank(mylead.BTS_Single_View_UID_PG__c))
                SVOCref.add(mylead.BTS_Single_View_UID_PG__c);
        }
    }
    system.debug('mpk converted oppIds = '+oppIds);
    if(oppIds.size()>0){
        List<Opportunity> opps = [SELECT Id,Name,Account.Name,CloseDate, LeadSource, RecordTypeId,
                                  Sales_Agent_EIN__c,Product__c,Sub_Category__c,Service_Site__c,Customer_Type__c,
                                  Send_Email_to_Mobile_Team__c,Customer_Contact_Salutation__c,Customer_Contact_First_Name__c,
                                  Customer_Contact_Last_Name__c,Account_Telephone_Number__c,Customer_Contact_Number__c,Email_Address__c,
                                  Postcode__c,Business_Name__c,Lead_Information__c,Opportunity_Go_live_date__c
                                  FROM Opportunity WHERE Id In :oppIds];
        List<Lead> leads = [SELECT Id, ConvertedOpportunityId,Description,RecordTypeId,Opportunity_Go_live_date__c,
                            preSales_User_EIN__c,Product_Group__c,Product_Group_Type__c,Service_Site__c,Customer_Type__c,
                            Send_Email_to_Mobile_Team__c,Salutation,FirstName,LastName,Phone,MobilePhone,Email,Lead_Post_Code__c,Company
                            FROM Lead WHERE ConvertedOpportunityId In :oppIds]; 
        
        //UKBCE-186769_Genesys_IWD_Leads_Through_Service 
        Map<id,Lead> ConvertedOppInfo = new Map<id,Lead>();
        
        
        for(Lead L : Leads){
            ConvertedOppInfo.put(L.ConvertedOpportunityId,L);               
        }
        for(Opportunity o:opps){
            o.CloseDate = system.Today()+30;
            if(o.Name == 'BT Sport: null'){
                o.Name = 'BT Sport: '+o.Account.Name;
            }
            if(o.LeadSource == 'BT Sport'){
                o.CloseDate = system.Today()+30;
                o.RecordTypeId = '01220000000AL0BAAW';
                if(svocs.size() >0){  
                    svocs[0].Opportunity__c = o.Id;
                }   
            }if(o.LeadSource == 'Leads from Service'){
                
                o.CloseDate = system.Today();
                o.RecordTypeId = '01220000000ABFB';
                o.Sales_Agent_EIN__c = ConvertedOppInfo.get(o.id).preSales_User_EIN__c;
                o.Product__c = ConvertedOppInfo.get(o.id).Product_Group__c;
                o.Sub_Category__c = ConvertedOppInfo.get(o.id).Product_Group_Type__c;
                o.Service_Site__c = ConvertedOppInfo.get(o.id).Service_Site__c; 
                o.Customer_Type__c = ConvertedOppInfo.get(o.id).Customer_Type__c;
                o.Send_Email_to_Mobile_Team__c = ConvertedOppInfo.get(o.id).Send_Email_to_Mobile_Team__c;
                o.Customer_Contact_Salutation__c =ConvertedOppInfo.get(o.id).Salutation;
                o.Customer_Contact_First_Name__c=ConvertedOppInfo.get(o.id).FirstName;
                o.Customer_Contact_Last_Name__c =ConvertedOppInfo.get(o.id).LastName;
                o.Account_Telephone_Number__c =ConvertedOppInfo.get(o.id).Phone; 
                o.Customer_Contact_Number__c=ConvertedOppInfo.get(o.id).MobilePhone;
                o.Email_Address__c=ConvertedOppInfo.get(o.id).Email;
                o.Postcode__c=ConvertedOppInfo.get(o.id).Lead_Post_Code__c;
                o.Business_Name__c=ConvertedOppInfo.get(o.id).Company;
                o.Lead_Information__c=ConvertedOppInfo.get(o.id).Description;                
                o.StageName = 'Created';
                o.Opportunity_Go_live_date__c=ConvertedOppInfo.get(o.id).Opportunity_Go_live_date__c;
                
            }
        }    
        if(opps.size() >0){ 
            update opps;
        }
    }
    if(SVOCref.size() >0){
        svocs = [SELECT Id, Opportunity__c FROM BT_Sport_SVOC__c WHERE BTS_Single_View_UID__c In :SVOCref];
    }
    if(svocs.size() > 0){
        update svocs;
    }   
    
    // Modifying Campaign Member Status
    List<CampaignMember> cm = [SELECT Id,CampaignId,LeadId, Status FROM CampaignMember WHERE LeadId In :allLeads];
    for(CampaignMember campMem: cm){
        campaignIds.add(campMem.CampaignId);
    }
    
    List<Campaign> campaignList = [SELECT Id,IsSVOC__c FROM Campaign WHERE Id In :campaignIds];
    for(Campaign cl: campaignList){
        IsSVOCMap.put(cl.Id,cl.IsSVOC__c);
    }
    
    for(CampaignMember c: cm ){
        //SVOC campaign member status are not modified as below during data loads
        //SVOC campaign member status are modified as below during manual changes
        if(IsSVOCMap.get(c.CampaignId) == False || (UserInfo.getProfileId() != '00e200000015EhzAAE' && IsSVOCMap.get(c.CampaignId) == True)){  //00e200000015EhzAAE = BT:Dataloader2
            if(LeadStatus.get(c.LeadId) == 'In Progress'){
                c.Status = 'Callback';}
            else if(LeadStatus.get(c.LeadId) == 'Opportunity Identified'){
                c.Status = 'Opportunity Created';}
            else if(LeadStatus.get(c.LeadId) == 'No Opportunity'){
                c.Status = 'Customer Not Interested';
            }
            CampMemUpdate.add(c);
        }
    }
    
    if(CampMemUpdate.size() > 0){
        update CampMemUpdate;
    }  
}