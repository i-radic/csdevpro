trigger CS_OpportunityTrigger on Opportunity (before update, after update) {
    CS_TriggerHandler.execute(new CS_OpportunityTriggerDelegate());
}