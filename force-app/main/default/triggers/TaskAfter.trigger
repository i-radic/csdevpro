trigger TaskAfter on Task (after update, after insert) {
    
    //
    //  Date            Author          Comment
    //  28 Sep 2010     Alan Jackson    added static variable to prevent Account trigger running 
    //  15/10/10        John McGovern   line 40 to remove call backs from the update
    //  06/05/2010      Dan Measures    Added CR3931 - add campaign member to campaign if the contact selected on a Corporate Campaign Task is not a campaign member.  
    //  28/03/2012      Anil Devunoori  Added code related to CR2180 - Last Activity on a campaign
    //  28/03/2012      Alan Jackson    Added if statment to prevent eloqua user updating last call date etc ( around line 94 & 101)
    
    
    if(TriggerDeactivating__c.getInstance().Task__c) {
        System.debug('Bypassing trigger due to custom setting' );
        return;
    }      
    
    System.debug('----> Run TaskAfter trigger: ' + StaticVariables.getTaskIsRun());
    if(StaticVariables.getTaskIsRun())
        return;
    StaticVariables.setAccountDontRun(true); // stop account trigger from running
    StaticVariables.setTaskIsRun(true);
    
    Id ElequaProfile = '00e200000015aJvAAI'; 
    ID SystemAdminProfileId = '00e20000001MX7zAAG';
    ID DataLoaderprofileId = '00e200000015EhzAAE';
    ID ReportingProfileId = '00e20000001QB4tAAG';
    ID BTLBAdminProfileId = '00e200000015K0NAAU';
    ID CorpAdminProfileID = '00e200000015IRvAAM';
    
    List<Task> lCampaignTasks = new List<Task>();
    Set<String> sCampaignMemberKey = new Set<String>();
    Map<Id, Id> mTaskIdsToAccountIds = new Map<Id, Id>();
    
    System.debug('----> Trigger Size:' + Trigger.new.size());       
    
    //Added by GS 08-09-10
    //Update the account last call date field for these accounts      
    Set<id> accids = new set<id>();
    Set<id> taskids = Trigger.newmap.keyset(); 
    Set<id> whatIds = new set<id>();
    
    Set<id> contactids = new set<id>();
    Set<id> oppids = new set<id>();
    Set<id> accountids = new set<id>();
    List<opportunity> newOpps = new List<opportunity>();
    Set<Id> allCampaignIds = new Set<Id>(); //CR2180
    
    //Get all of the what id's
    for(Task t : trigger.new){
        system.debug('adding' + t.whatid);
        whatids.add(t.whatid);     
    }
    system.debug('mpk taskids- ' + taskids);
    // added Call Back exception to stop campaign x-day rule kicking in for call backs
    List<task> tasklist = [select what.type,whatid,who.type from task where id in :taskids and call_status__c <> 'Call-back'];
    system.debug('mpk tasklist- ' + tasklist);
    for(Task t1 : tasklist ){
        system.debug('mpk t1- ' + t1);
        if(t1.what.type=='Opportunity'){
            if(!oppids.contains(t1.whatid)){  
                oppids.add(t1.whatid);
                // ROS - 24/01/2013
                //Opportunity newOpp = new Opportunity(id=t1.whatid,last_updated__c =date.today());
                /*Opportunity newOpp = new Opportunity(id=t1.whatid);	//mpk  
                newOpps.add(newOpp);*/
           }
        }else if(t1.who.type=='Contact'){
            if(!contactids.contains(t1.whoid))
                contactids.add(t1.whoid);
        }else if(t1.what.type=='Account'){
            if(!accids.contains(t1.whatid))
                accids.add(t1.whatid);
        }
    }
    /*if(newOpps.size()>0){ //mpk  
        system.debug('mpk newOpps- ' + newOpps);
        update newOpps;
    }*/
    //Get a list of related contacts
    if(contactids.size()>0){
        List<contact> contactAccounts = [select accountid from contact where id in :contactids];
        for (contact thisContact:contactAccounts){
            system.debug('found contact' + thiscontact);
            accids.add(thisContact.accountid);
        }
    }
    
    //Get a list of related opps
    if(oppids.size()>0){        
        List<opportunity> oppAccounts = [select accountid from opportunity where id in :oppids];
        for (opportunity thisOpp:oppAccounts ){
            accids.add(thisOpp.accountid);
        }
    }  
    //Get the related accounts
    /*if(accountids.size()>0){		//mpk - already got this in line 70-72
        List<account> accountAccounts = [select id from account where id in :whatids];
        for (Account thisAccount:accountAccounts ){
            accids.add(thisaccount.id);
        }
    }*/
    
    if(accids.size()>0){          
        system.debug('gothere2');
        // added if statement to prevent tasks created by Eloqua setting lastcallDate
        System.debug('ADJ TaksAfter UserInfo.getProfileId' +  UserInfo.getProfileId() );  
        if ( UserInfo.getProfileId() != ElequaProfile && UserInfo.getProfileId() != SystemAdminProfileId && 
            UserInfo.getProfileId() != DataLoaderprofileId && UserInfo.getProfileId() != ReportingProfileId && 
            UserInfo.getProfileId() != BTLBAdminProfileId && UserInfo.getProfileId() != CorpAdminProfileID ){
                list<account> updateCallDate = new list<account>();
                updateAccountLastContacted updateAccountCallDate = new updateAccountLastContacted(); 
                updateCallDate = updateAccountCallDate.updateAccount(accids); 
                String objName='Tasks';
                updateCallDate = updateAccountCallDate.updateAccount(accids, objName);           
                update updateCallDate;   
            }
    }    
    
    
    for(Task t : Trigger.new){
        
        System.debug('----> Task Id: ' + t.Id);
        System.debug('----> what Id: ' + t.WhatId);
        System.debug('----> closed: ' + t.isClosed);
        System.debug('----> status: ' + t.Status);
        System.debug('----> type: ' + t.Type);
        System.debug('----> User: ' + UserInfo.getName());
        
        //We only work on 'Closed' Campaign tasks 
        String strWhatId = t.WhatId;                
        if(strWhatId != null && strWhatId.startsWith('701') && t.isClosed && t.Status != 'Expired' && t.Type == 'Call'){
            
            System.debug('----> Campaign Task Id where status != Expired: ' + t.Id);    
            
            if(t.WhoId != null){
                System.debug('-----> Task isClosed: ' + t.IsClosed + ' Status: ' + t.Status + ' ...adding to list of Tasks to be processed.');  
                lCampaignTasks.add(t);
                //Build unique key to identify Campaign Members later. Used in updating Campaign Members 'Status'.
                sCampaignMemberKey.add(t.WhoId + '_' + t.WhatId);
                mTaskIdsToAccountIds.put(t.Id, t.AccountId);
            }else{
                t.WhoId.addError('Task (related to campaign) is missing WhoId (Contact).');
                continue;
            }
        }
        
        //CR2180 - start        
        //Get all what Ids of campaign tasks             
        if(strWhatId != null && strWhatId.startsWith('701')) {                
            allCampaignIds.add(t.WhatId);                
            System.debug('AAAAAAAAAAAAAAAAAAAAAA'+t.WhatId);            
        }                
        //CR2180 - end   
    }
    
    //CR2180 - start        
    if(allCampaignIds.size()>0){                          
        list<Campaign> allcampaignlist = new list<Campaign>();                
        updateCampaignLastContacted updateLastActivityDate = new updateCampaignLastContacted();                                     
        allcampaignlist = updateLastActivityDate.updateCampaign(allCampaignIds);                           
        update allcampaignlist;                              
    }                 
    //CR2180 - end
    
    if(!lCampaignTasks.isEmpty()){  
        
        //Prevent recursive calls as this trigger updates/inserts other Tasks.
        StaticVariables.setTaskIsRun(true);
        
        List<CampaignMember> lCampaignMembersToUpdate = new List<CampaignMember>();
        List<Account> lAccountsToUpdate = new List<Account>();
        List<Opportunity> lOppsToInsert = new List<Opportunity>();
        List<OpportunityContactRole> lOppsContactRoleToInsert = new List<OpportunityContactRole>();
        List<Id> lContactIds = new List<Id>();
        List<Task> lTasksToInsert = new List<Task>();
        List<Contact> lContactsToUpdate = new List<Contact>();
        List <Task> lSiblingTasksToUpdate = new List<Task>();
        List<Task> lSiblingTasks = [select Id, Assigned_User_Id__c, AccountId from Task where AccountId IN :mTaskIdsToAccountIds.values() 
                                    AND Campaign_Task__c = 'TRUE' AND Type = 'Call'];
        System.debug('GOV:TaskAfter1');
        List <CampaignMember> lCampaignMembers = [select Unique_Key__c, id, status, ContactId, Unsuccessful_Call_Attempts__c 
                                                  from CampaignMember where Unique_Key__c IN :sCampaignMemberKey];
        
        // CR3931 - Start - change for corp who want to be able to switch the contact on the Campaign Task
        RecordType recordtypeid_corp_task  = [select id from RecordType where SobjectType='Task' and name ='Corporate Campaign Task' limit 1];
        RecordType recordtypeid_deluxe_corp_task  = [select id from RecordType where SobjectType='Task' and name ='Deluxe Survey Review' limit 1];
        Set<String> sExistingCMUniqueKeys = new Set<String>();
        for(CampaignMember cm: lCampaignMembers){
            sExistingCMUniqueKeys.add(cm.Unique_Key__c);
        }
        List<CampaignMember> cmToCreate = new List<CampaignMember>();
        for(Task t: lCampaignTasks){
            if(t.RecordTypeId == recordtypeid_corp_task.Id || t.RecordTypeId == recordtypeid_deluxe_corp_task.Id){  // only auto create missing Campaign Members if corp task
                if(!sExistingCMUniqueKeys.contains(t.WhoId + '_' + t.WhatId)){
                    //if contact is not a campaign member of the campaign then create them as a CM
                    CampaignMember cm = new CampaignMember(CampaignId = t.WhatId, ContactId = t.WhoId);
                    cmToCreate.add(cm);
                }
            }
        }
        if(!cmToCreate.isEmpty()){
            insert cmToCreate;
            //retrieve FULL list of campaign members now additional members have been added
            lCampaignMembers = [select Unique_Key__c, id, status, ContactId, Unsuccessful_Call_Attempts__c 
                                from CampaignMember where Unique_Key__c IN :sCampaignMemberKey];
        }
        // CR3931 - End
        
        System.debug('GOV:TaskAfter2');
        Map<Id, List<Task>> mAccountIdsToTasks = new Map<Id,List<Task>>();
        Map <Id, Account> mAccounts = new Map<Id, Account>([select id, Sector_Code__c from Account where Id IN :mTaskIdsToAccountIds.values()]);
        System.debug('GOV:TaskAfter3'); 
        Map <String, CampaignMember> mUniqueIdsToCampaignMembers = new Map<String, CampaignMember>();
        Map<String, CMStatusMap__c> mAllCallCutcomeStates = CMStatusMap__c.getAll();
        
        for(String calloutcomestate : mAllCallCutcomeStates.keyset()) {
            System.debug('-----> All CMStatusMap Datasets: ' + calloutcomestate);
        }
        
        for(CampaignMember cm :lCampaignMembers){
            mUniqueIdsToCampaignMembers.put(cm.Unique_Key__c, cm);
        }
        
        //These are all the Tasks relevant to the accounts associated to the Tasks (in the batch) retrieved from the DB that need updating 
        for(Task st : lSiblingTasks){
            if(mAccountIdsToTasks.containsKey(st.AccountId)){
                mAccountIdsToTasks.get(st.AccountId).add(st);
            }else{
                List<Task> lAccountTasks = new List<Task>();
                lAccountTasks.add(st);
                mAccountIdsToTasks.put(st.AccountId, lAccountTasks);
            }
        }
        //Added by GS - 16th-August
        Recordtype oppRecType = [select id from recordtype where sobjecttype='Opportunity' and name ='Standard'];
        
        for(Task t: lCampaignTasks){
            
            Account a = mAccounts.get(t.AccountId);
            
            System.debug('-----> Tasks associated Account Id: ' + a.Id);    
            System.debug('-----> Account Sector code: ' + a.Sector_Code__c);
            
            CampaignMember cm = mUniqueIdsToCampaignMembers.get(t.WhoId + '_' + t.WhatId);
            Boolean bCmChanged = false;
            
            if(cm == null){
                t.addError('Campaign Member could not be found with composite Unique Key: [' + t.WhoId + '_' + t.WhatId + ']');
                continue;
            }
            
            if(a == null){
                t.addError('Account could not be found.');
                continue;
            }
            
            String newCmStatus = null;
            CMStatusMap__c statusmap = mAllCallCutcomeStates.get(t.Call_Status__c);
            if(statusmap!= null){
                newCmStatus = statusmap.CM_Status__c;
            }
            if(newCmStatus != null){
                //Update the CampaignMember Status with the value entered on the Task
                cm.Status = newCmStatus;
                bCmChanged = true;
            }else{
                t.addError('No Campaign Member Status map exists for the Call Outcome specified: ['+ t.Call_Status__c +'].  Please contact your administrator.');
                continue;
            }
            
            System.debug('-----> Call Succeeded (i.e. True/False/Callback): ' + t.Call_Succeeded__c);
            
            if(t.Call_Succeeded__c == 'TRUE'){
                a.Last_Account_Call_Date__c = date.today();
                //a.Last_Update_to_Last_Account_Call_Date__c = 'Tasks/Events - '+ UserInfo.getName() + ' - ' + System.now();
                //System.debug('AAAAAAA'+a.Last_Update_to_Last_Account_Call_Date__c);
                a.Call_Back_Time__c = null;
                //Reset unsuccessful call count
                cm.Unsuccessful_Call_Attempts__c = 0;
                bCmChanged = true;
                //Now check if opportunity needs to be created
                if(t.Call_Status__c == 'Opportunity Created'){
                    Opportunity newOpp = new Opportunity(OwnerId=t.OwnerId, AccountId =t.AccountId, 
                                                         CampaignId =t.WhatId, Name= 'From Call: ' + t.Subject, 
                                                         StageName='Created',created_from_task_id__c = t.Id,recordtypeid=oppRecType.id);
                    lOppsToInsert.add(newOpp);
                    //Need to store the corresponding Contact Id for the opportunity (will be at same index in list) - used later to insert OpportunityContactRole
                    lContactIds.add(cm.ContactId);
                }       
            }else if (t.Call_Succeeded__c == 'CALLBACK'){
                bCmChanged = true;
                
                System.debug('-----> Number of Callbacks (Before): ' + cm.Unsuccessful_Call_Attempts__c);   
                
                //Add to unsuccessful count
                if(cm.Unsuccessful_Call_Attempts__c != null){ 
                    cm.Unsuccessful_Call_Attempts__c = cm.Unsuccessful_Call_Attempts__c + 1;
                }else{
                    cm.Unsuccessful_Call_Attempts__c = 1;
                }
                
                System.debug('-----> Number of Callbacks (After): ' + cm.Unsuccessful_Call_Attempts__c);
                
                String businessLine = a.Sector_Code__c;
                
                System.debug('-----> Contacts Account Type (i.e. corp/btlb): ' + businessLine);
                
                if(businessLine == null){
                    //Should never enter here as Account 'Sector' field should have a value.
                    t.adderror('The Account does not have a Sector. This is the Account of the Contact on the Task, check the Accounts [Sector] field.  Please contact your administrator.');
                    continue;   
                }else if(businessLine.equalsIgnoreCase('CORP')){
                    Datetime callbacktime = datetime.now().addHours(3);
                    if(t.Call_Status__c == 'Call-back'){
                        callbacktime = t.Call_Back_Time_Copy__c;
                    }       
                    a.Call_Back_Time__c = callbacktime;
                    //Create a follow-up task
                    Task newTask = new Task(OwnerId= t.OwnerId, WhoId = t.WhoId, 
                                            WhatId = t.WhatId, ActivityDate= t.ActivityDate, Auto_Genereted__c = true, 
                                            Subject = t.Subject, Call_Back_Time_Copy__c = callbacktime, 
                                            RecordTypeId=StaticVariables.campaign_task_record_type, X_Day_Rule_Copy__c= t.X_Day_Rule_Copy__c);
                    lTasksToInsert.add(newTask);
                }else if (businessLine.equalsIgnoreCase('BTLB')){ 
                    //Must be a BTLB manually created Task - if unsuccessful call attempts = 3 mark CM as not contactable.
                    if(cm.Unsuccessful_Call_Attempts__c >= 3){
                        System.debug('-----> Btlb - mark contact as do not call as no of callbacks >=3'); 
                        
                        cm.Unsuccessful_Call_Attempts__c = 0;
                        cm.Status = 'Unable to Contact';
                    }else{
                        //Update call back date on account (used in Contact lists formula field)
                        Datetime callbacktime = datetime.now().addHours(3);
                        if(t.Call_Status__c == 'Call-back'){
                            callbacktime = t.Call_Back_Time_Copy__c;
                        }
                        a.Call_Back_Time__c = callbacktime;
                    }
                }else{
                    //t.addError('The Account field Sector could not be resolved to (btlb or corp).  This is the Account of the Contact on the Task, check the Accounts [Sector] field. Please contact your administrator.');
                    continue;
                }
            }
            
            //Clear down Account - so User Assignment so Campaign member is free to be called by another agent.
            a.Assigned_User__c = null;
            a.Assigned_User_Datetime__c=null;
            lAccountsToUpdate.add(a);
            
            //Update this Tasks sibling Tasks (in DB)- so Campaign member is free to be called by another agent.
            List<Task> lSibTasks = mAccountIdsToTasks.get(t.AccountId);
            if(lSibTasks != null && lSibTasks.size() > 0){
                for(Task st : lSibTasks){
                    if(st != null){
                        if(t.Call_Succeeded__c == 'TRUE'){
                            st.Last_Account_Call_Date__c = date.today();
                            st.Call_Back_Time_Copy__c = null;
                        }
                        st.Assigned_User_Id__c = null;
                        st.Assigned_User_Datetime__c=null;
                        lSiblingTasksToUpdate.add(st);  
                        if(lSiblingTasksToUpdate.size() == 1000){
                            StaticVariables.setTaskUpdatingCampaignTasks(true);
                            update lSiblingTasksToUpdate;
                            StaticVariables.setTaskUpdatingCampaignTasks(false);
                            lSiblingTasksToUpdate.clear();
                        }
                    }
                }
            }   
            if(bCmChanged){
                lCampaignMembersToUpdate.add(cm);
            }
        }
        
        if(!lSiblingTasksToUpdate.isEmpty()){
            StaticVariables.setTaskUpdatingCampaignTasks(true);
            update lSiblingTasksToUpdate;
            StaticVariables.setTaskUpdatingCampaignTasks(false);
        }
        
        if(!lCampaignMembersToUpdate.isEmpty()){
            update lCampaignMembersToUpdate;
        }
        
        if(!lAccountsToUpdate.isEmpty()){
            update lAccountsToUpdate;
        }
        
        if(!lContactsToUpdate.isEmpty()){
            update lContactsToUpdate;
        }
        
        if(!lOppsToInsert.isEmpty()){
            //insert lOppsToInsert;
            System.assert(lOppsToInsert.size() == lContactIds.size(), 'Opportunity list to be inserted should always have the same number of elements as ContactIds list.  This ensures correct mappings for insert of OpportunityContactRoles.');
            //List lOppsToInsert and lContactIds should always have same number of elements.  As list is an ordered collection I know index 0 in both provides the contactId TO OppId mapping i need to create the OpportunityContactRoles
            Database.SaveResult[] MySaveResult = Database.Insert(lOppsToInsert);
            //Now create OpportunityContactRoles for insert Opportunities
            Integer i = 0;
            for(Database.SaveResult sr: MySaveResult){
                if(sr.isSuccess()){
                    OpportunityContactRole newOppContactRole = new OpportunityContactRole(ContactId=lContactIds[i], OpportunityId=sr.getId());
                    lOppsContactRoleToInsert.add(newOppContactRole);
                }
                i++;
            }
            
            //Added by GS 13th July '10 - Create link from Task to Opportunity
            //Go around the Opps and populate the id's into the corresponding tasks.
            List<Task> updateTasks = new List<Task>();
            Task updateTask ;
            
            for(Opportunity thisOpp:lOppsToInsert){
                for(task thisTask:lCampaignTasks){
                    if(thisOpp.created_from_task_id__c==thisTask.id){
                        updateTask = thisTask.clone(true,true);
                        updateTask.Related_Opportunity_ID__c = thisOpp.id;
                        updateTasks.add(updateTask);
                    }
                }
            }
            //Update the task list with the id's
            StaticVariables.setTaskUpdatingCampaignTasks(true);
            update updateTasks;
            StaticVariables.setTaskUpdatingCampaignTasks(false);
            
            if(!lOppsContactRoleToInsert.isEmpty()){
                insert lOppsContactRoleToInsert;
            }   
        }
        
        if(!lTasksToInsert.isEmpty()){
            StaticVariables.setTaskUpdatingCampaignTasks(true);
            insert  lTasksToInsert;
            StaticVariables.setTaskUpdatingCampaignTasks(false);
        }   
    }
}