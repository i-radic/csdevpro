trigger BTSportSiteBeforeS4 on BT_Sport_Site__c (before insert, before update) {
    
    Date TodaysDate = Date.today();
    Date PreviousSeason_StartDate = Date.valueOf('2016-01-01');//('2015-01-01');
    Date PreviousSeason_EndDate = Date.valueOf('2017-08-31');//('2016-08-31'); /// needs updating before go live
    //Date CurrentSeason_StartDate = Date.valueOf('2016-09-01'); //i think its not required- mpk
    //Date CurrentSeason_EndDate = Date.valueOf('2019-09-01');
    
    public String roomsonly = null;
    public String roomsPmfLookupKey = null;
    public Decimal roomsPrice =null;
    public String PriceType = null;
    public String Band = null;
    public String var_site_Type = null;
    public String var_contract_Type = null;
    set<string> PMFUnique = new Set<string>();
    set<string> HeaderId = new Set<string>();
    //Variable To Get OppIds vvv
    map<string, string> mSiteHIds = new map<string,string>();
    map<string, string> mSiteHIdOppIdss = new map<string,string>();
    map<string, string> mOppIdContRoles = new map<string,string>();
    map<string, string> mConIdEmails = new map<string,string>();
    map<string, string> mConIdContactBuilding = new map<string,string>();
    map<string, string> mConIdAddressNumber = new map<string,string>();
    map<string, string> mConIdAddressStreet = new map<string,string>();
    map<string, string> mConIdPostCode = new map<string,string>();
    Id BTDataloader2Id = '00e200000015EhzAAE'; 
    
    map<string, BT_Sport_SVOC__c> mOpptyToSvoc = new map<string,BT_Sport_SVOC__c>();    
    
    RecordType RecType = [Select Id From RecordType  Where SobjectType = 'BT_Sport_Site__c' and DeveloperName = 'Season4' limit 1];
    
    /* Code To Get OppIds Start*/
    for (BT_Sport_Site__c t:Trigger.new) {
        if(t.RecordTypeID == RecType.ID && userinfo.getProfileId() != BTDataloader2Id) { 
            system.debug('Entered Trigger!!!!!!  ' + t.BT_Sport_CL__c);
            if(!String.isBlank(t.BT_Sport_CL__c)) {
                mSiteHIds.put(t.Id, t.BT_Sport_CL__c);
                system.debug('Site-CL Map : ' + mSiteHIds);
            }        
        }
        for(BT_Sport_CL__c btscl : [SELECT Id,Opportunity__c FROM BT_Sport_CL__c WHERE Id IN : mSiteHIds.values()]) {
            mSiteHIdOppIdss.put(btscl.Id, btscl.Opportunity__c);
        }
        for(OpportunityContactRole ocr : [SELECT ContactId,Id,OpportunityId FROM OpportunityContactRole WHERE OpportunityId IN : mSiteHIdOppIdss.values()]) {
            mOppIdContRoles.put(ocr.OpportunityId, ocr.ContactId);
            system.debug('Opportunity Contact Role Map : ' + mOppIdContRoles);
        }
        
        //MPK - get SVOCs hanging on Opp - Season5
        system.debug('mpk mSiteHIdOppIdss : ' + mSiteHIdOppIdss);
        for(opportunity oppty : [select Id, (select id, name, Master_Product__c, Master_Product_Detail__c, X2010_Rateable_Value__c, X2017_Rateable_Value__c, Customer_band__c, Band_for_2017_Rateable_Value__c,Rate_Card_Based_on_2017_Rateable_Value__c, 
                                             Does_customer_have_Hero_offer__c, Site_Address_Line_1__c, Site_Address_Line_2__c, Site_Address_Line_3__c, Site_City__c,Site_County__c, Site_ZIP_Code__c, BT_Sport_Service_ID__c, BT_Sport_Customer_Status__c,
                                             DM_Email__c,DM_Alternative_Contact_Number__c,DM_Telephone_Number__c,BAC_List__c, Selected_Personalised_offer__c, Personalised_Price__c from BT_Sport_SVOCs__r) 
                                 from opportunity WHERE Id IN : mSiteHIdOppIdss.values()]) {
                                     if(oppty.BT_Sport_SVOCs__r.size()==1){
                                         mOpptyToSvoc.put(oppty.Id, oppty.BT_Sport_SVOCs__r[0]); 
                                         
                                         string RV2010='',RV2017='',Hero_flag='',BTSCustStatus='';
                                         RV2010 = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).X2010_Rateable_Value__c;
                                         RV2017 = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).X2017_Rateable_Value__c;
                                         Hero_flag = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Does_customer_have_Hero_offer__c;
                                         BTSCustStatus = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).BT_Sport_Customer_Status__c;
                                         
                                         if(string.isBlank(t.Building_Name__c)) t.Building_Name__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Site_Address_Line_1__c;
                                         if(string.isBlank(t.Building_Number__c)) t.Building_Number__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Site_Address_Line_2__c;
                                         if(string.isBlank(t.Street__c)) t.Street__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Site_Address_Line_3__c;
                                         if(string.isBlank(t.Town__c)) t.Town__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Site_City__c;
                                         if(string.isBlank(t.County__c)) t.County__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Site_County__c;
                                         if(string.isBlank(t.Post_Code__c)) t.Post_Code__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Site_ZIP_Code__c;                 
                                         if(t.Rateable_Value__c == NULL) t.Rateable_Value__c = decimal.valueOf((RV2010 == null || RV2010 == '') ? '0': RV2010);
                                         if(t.X2017_Rateable_Value__c == NULL) t.X2017_Rateable_Value__c = decimal.valueOf((RV2017==null) ? '0': RV2017);
                                         if(string.isNotBlank(Hero_flag) && t.Hero__c == NULL) t.Hero__c = (Hero_flag=='Y') ? 'Yes': 'No';
                                         if(string.isBlank(t.Customer_band__c)) t.Customer_band__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Customer_band__c;
                                         if(BTSCustStatus == 'Active' && string.isBlank(t.BT_Sport_Service_ID__c)) t.BT_Sport_Service_ID__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).BT_Sport_Service_ID__c;
                                         if(string.isBlank(t.BAC_List__c)) t.BAC_List__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).BAC_List__c;
                                         if(t.Selected_Personalised_offer__c == NULL) t.Selected_Personalised_offer__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Selected_Personalised_offer__c;
                                         if(t.Personalised_Price__c ==NULL) t.Personalised_Price__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Personalised_Price__c;
                                         
                                         if(t.Onsite_contact_email__c == NULL){
                                             if(mOpptyToSvoc.get(oppty.Id).DM_Email__c != NULL)
                                                 t.Onsite_contact_email__c = mOpptyToSvoc.get(oppty.Id).DM_Email__c; 
                                             //else
                                             //t.Onsite_contact_email__c.adderror('You must enter a value');
                                         } 
                                         if(t.Site_Contact_Name__c == NULL){
                                             if(mOpptyToSvoc.get(oppty.Id).DM_Alternative_Contact_Number__c != NULL)
                                                 t.Site_Contact_Name__c = mOpptyToSvoc.get(oppty.Id).DM_Alternative_Contact_Number__c; 
                                             //else
                                             //t.Site_Contact_Name__c.adderror('You must enter a value');
                                         } 
                                         if(t.Onsite_contact_telephone_number__c == NULL){
                                             if(mOpptyToSvoc.get(oppty.Id).DM_Telephone_Number__c != NULL)
                                                 t.Onsite_contact_telephone_number__c = mOpptyToSvoc.get(oppty.Id).DM_Telephone_Number__c;
                                             //else
                                             //t.Onsite_contact_telephone_number__c.adderror('You must enter a value');
                                         }
                                         
                                         if(t.Master_product_for_price_based_on_2017RV__c == NULL) t.Master_product_for_price_based_on_2017RV__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Master_Product__c;
                                         if(t.Prod_Details_for_Price_Based_on_2017RV__c == NULL) t.Prod_Details_for_Price_Based_on_2017RV__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Master_Product_Detail__c;
                                         if(t.Band__c == NULL) t.Band__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Customer_band__c; //might not be required
                                         if(t.Band_for_2017_Rateable_Value__c == NULL) t.Band_for_2017_Rateable_Value__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Band_for_2017_Rateable_Value__c;
                                         if(t.Rate_card_based_on_2017_Rateable_Value__c == NULL) t.Rate_card_based_on_2017_Rateable_Value__c = mOpptyToSvoc.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)).Rate_Card_Based_on_2017_Rateable_Value__c;
                                     }   
                                     else {//if(oppty.BT_Sport_SVOCs__r.size() == 0) {
                                         /*if(t.Onsite_contact_email__c == NULL){
                                            t.Onsite_contact_email__c.adderror('You must enter a value');
                                            }
                                            if(t.Site_Contact_Name__c == NULL){
                                            t.Site_Contact_Name__c.adderror('You must enter a value');
                                            }                                         
                                            if(t.Onsite_contact_telephone_number__c == NULL){
                                            t.Onsite_contact_telephone_number__c.adderror('You must enter a value');
                                            }*/
                                     }
                                     system.debug('mpk Opportunity svoc Map : ' + oppty);
                                 }
        //MPK- end
        
        for(Contact c : [SELECT Id,Email,Contact_Building__c, Contact_Address_Number__c, Contact_Address_Street__c, Contact_Post_Code__c FROM Contact WHERE Id IN : mOppIdContRoles.values()]) {
            mConIdEmails.put(c.Id, c.Email);
            mConIdContactBuilding.put(c.Id, c.Contact_Building__c);
            mConIdAddressNumber.put(c.Id, c.Contact_Address_Number__c);
            mConIdAddressStreet.put(c.Id, c.Contact_Address_Street__c);
            mConIdPostCode.put(c.Id, c.Contact_Post_Code__c);
            system.debug('Contact Map : ' + mConIdEmails);
            
            /*Code To Get OppIds End*/            
            
            /* Code To Get Contact Details Start*/
            t.zPrimaryContactEmail__c = mConIdEmails.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Building__c = mConIdContactBuilding.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Address_Number__c = mConIdAddressNumber.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Address_Street__c = mConIdAddressStreet.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Post_Code__c = mConIdPostCode.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            
            /* Code To Get Contact Details  End*/
            
            if(t.PMF_Effective_Date__c != null){
                TodaysDate = t.PMF_Effective_Date__c;
            }
            
            // select either PAYG price points or default to 12 months
            if(t.Contract_Type__c == 'Pay as you go monthly'){
                var_contract_Type = 'Pay as you go monthly';
            }            
            else {
                var_contract_Type = '12 month contract';
            }
            
            //Check for Hero Discounts, if present update site type
            var_site_Type = t.Site_Type__c; // set default 
            If (t.Hero__c == 'Yes'){
                var_site_Type = t.Site_Type__c +' - Hero'; //MPK: Season 5 change
            }
            t.Price_after_Discounts__c = t.Discount_Amount__c = t.gross_price__c = 0;
            
            //Get price type           
            //List<BT_Sport_Pricing__c> PriceTypeSelect = [Select Site_Type__c, Price_Type__c  from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type Limit 1 ];
            for (BT_Sport_Pricing__c BTPrice : [Select Site_Type__c, Price_Type__c  from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type Limit 1]){
                // set pmflookup key          
                If( BTPrice.Price_Type__c == '1'){ // ie price based purly on site type
                    PMFUnique.add(var_site_Type);
                    t.PmfLookupKey__c = var_site_Type;
                }  
                else if( BTPrice.Price_Type__c == '2'){  // price based on site & contract (not bands)
                    PMFUnique.add(var_site_Type + var_contract_Type);
                    t.PmfLookupKey__c = var_site_Type + var_contract_Type;
                    system.debug('Price_Type__c = '+ var_site_Type + var_contract_Type);
                    system.debug('ADJ BTS Other t.PmfLookupKey__c = '+ t.PmfLookupKey__c);   
                }            
                else {                
                    if(BTPrice.Price_Type__c == '3RM'){ // && t.Number_of_Rooms_receiving_BT_Sport__c != Null){  //JMM added not null condition to prevent mis banding 
                        if(t.Number_of_Rooms_receiving_BT_Sport__c == Null)  t.Number_of_Rooms_receiving_BT_Sport__c.addError('This is required for Hotel Bar or Bar only');
                        
                        List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and Price_Band_Start__c <= :t.Number_of_Rooms_receiving_BT_Sport__c and Price_Band_end__c >= :t.Number_of_Rooms_receiving_BT_Sport__c Limit 1 ];
                        t.Band__c = PriceBandSelect[0].Band__c;                    
                    }                
                    else if(BTPrice.Price_Type__c == '3RV'){ //JMM added not null condition to prevent mis banding
                        if(t.Rateable_Value__c == Null) t.Rateable_Value__c.addError('This is required');	//MPK
                        
                        List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and  Price_Band_Start__c <= :t.Rateable_Value__c and Price_Band_end__c >= :t.Rateable_Value__c Limit 1 ];
                        t.Band__c = PriceBandSelect[0].Band__c;
                    }
                    
                    
                    if(t.Override_Band__c != Null){		//MPK: Season 5 change
                        PMFUnique.add(t.Site_Type__c + var_contract_Type + t.Override_Band__c);
                        t.PmfLookupKey__c = t.Site_Type__c + var_contract_Type + t.Override_Band__c;
                    }else{
                        PMFUnique.add(t.Site_Type__c + var_contract_Type + t.Band__c);
                        t.PmfLookupKey__c = t.Site_Type__c + var_contract_Type + t.Band__c;
                    }
                    
                    If (t.Product__C == 'BT Sport Total'){
                        roomsPmfLookupKey = 'BT Sport Total Hotel Rooms' + var_contract_Type;
                    }
                    If (t.Product__C == 'BT Sport 1'){
                        roomsPmfLookupKey = 'BT Sport 1 Hotel Rooms' + var_contract_Type;
                    }                
                    system.debug('ADJ PMFUnique.add = '+ PMFUnique);
                    system.debug('ADJ t.PmfLookupKey__c = '+ t.PmfLookupKey__c);   
                    system.debug('ADJ roomsPmfLookupKey = '+ roomsPmfLookupKey);  
                }
                
                // lookup using  PmfLookupKey__c the correct price entry    
                set<string> PMFcodes = new Set<string>();
                Map<String, String> PMF_Value = new Map<String, String>();
                
                List<BT_Sport_Pricing__c> pmfCode = [Select Site_Type__c, PMF_Code__c, PmfLookupKey__c from BT_Sport_Pricing__c where 
                                                     Entry_Type__C = 'PricePoint' and (PmfLookupKey__c in :PMFUnique or 
                                                                                       PmfLookupKey__c = :roomsPmfLookupKey)];
                for (BT_Sport_Pricing__c pc :pmfCode) {
                    PMF_Value.put(pc.PmfLookupKey__c, pc.PMF_Code__c);
                    PMFcodes.add(pc.PMF_Code__c);
                    if (pc.PmfLookupKey__c == roomsPmfLookupKey){
                        RoomsOnly = pc.PMF_Code__c;
                    }
                    system.debug('ADJ pc.PmfLookupKey__c '+ pc.PmfLookupKey__c );
                    system.debug('ADJ pc.PMF_Code__c = '+ pc.PMF_Code__c);
                }
                Map<String, Decimal> PMF_Price = new Map<String, Decimal>();
                Map<String, Decimal> PMF_Version = new Map<String, Decimal>();
                
                List<BT_Sport_Pricing__c> pmfPrice = [Select PMF_Code__c, Price__c, PMF_Version__c from BT_Sport_Pricing__c where Entry_Type__C = 'PMF' and PMF_Code__c in :PMFcodes and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate] ;
                //Start_Date__C <= :CurrentSeason_StartDate and End_Date__C >= :CurrentSeason_EndDate  ] ; mpk
                for (BT_Sport_Pricing__c pp :pmfPrice) {
                    PMF_Price.put(pp.PMF_Code__c, pp.Price__c);
                    PMF_Version.put(pp.PMF_Code__c, pp.PMF_Version__c);
                    system.debug('ADJ pp.PMF_Code__c '+ pp.PMF_Code__c );
                    system.debug('ADJ pp.Price__c = '+ pp.Price__c);
                }
                system.debug('mpk pmfPrice = '+pmfPrice+'PMFcodes = '+PMFcodes);
                
                t.gross_price__c = PMF_Price.get(PMF_Value.get(t.PmfLookupKey__c));
                t.pmf_version__c = PMF_Version.get(PMF_Value.get(t.PmfLookupKey__c));                

                //system.debug('ADJ t.gross_price__c = '+ t.gross_price__c);
                //system.debug('ADJ t.pmf_version__c = '+ t.pmf_version__c);               
                
                //Calculate Hotel Rooms only Price
                If(t.Site_Type__c.contains('Hotel Rooms') && t.Number_of_Rooms_receiving_BT_Sport__c != null){
                    t.gross_price__c = PMF_Price.get(PMF_Value.get(t.PmfLookupKey__c)) * t.Number_of_Rooms_receiving_BT_Sport__c;
                    // Minimum spend on rooms
                    if(t.gross_price__c <60){
                        t.gross_price__c =60;
                    }
                }
                
                // Hotel Bar = add the value of rooms ( if any) to gross price 
                If (t.Site_Type__c.contains('Hotel Bar')  && t.Number_of_Rooms_receiving_BT_Sport__c != null  && !(t.Site_Type__c.contains('Hotel Bar Only')))  {
                    roomsPrice = PMF_Price.get(roomsonly) * integer.valueof(t.Number_of_Rooms_receiving_BT_Sport__c);
                    if(roomsPrice <60){
                        roomsPrice =60;
                    }
                    t.gross_price__c =  t.gross_price__c + roomsPrice; 
                }
                
                // Discounts - Perishable discount  ( now called offer on screen)
                Map<String, Decimal> Discount_Amount = new Map<String, Decimal>();
                Map<String, Decimal> Discount_Period = new Map<String, Decimal>();
                Map<String, String> UnAvailableSiteType = new Map<String, String>();
                Map<String, String> UnAvailableContractType = new Map<String, string>();
                // Map<String, integer> DiscountPeriodMonths = new Map<String, string>();
                // Map<String, Date> DiscountPeriodEndDate = new Map<String, Date>();
                
                List<BT_Sport_Pricing__c> Discounts = [Select Discount_Period_End_Date__c, Discount_Period_Months__c, Discount_Amount__c, Discount_Period__c, Discount_Name__C, UnavailableSiteTypes__c, UnavailableContractTypes__c from BT_Sport_Pricing__c where Entry_Type__C = 'Discount' and Discount_Name__C = :t.Discounts__c and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate  ] ;
                system.debug('ADJ Discounts.size() = '+ Discounts.size());
                
                for (BT_Sport_Pricing__c dc :Discounts) {
                    Discount_Amount.put(dc.Discount_Name__C, dc.Discount_Amount__c);
                    Discount_Period.put(dc.Discount_Name__C, dc.Discount_Period__c);
                    UnAvailableSiteType.put(dc.Discount_Name__C, dc.UnavailableSiteTypes__c);
                    UnAvailableContractType.put(dc.Discount_Name__C, dc.UnavailableContractTypes__c);
                    t.Discount_Period_End_Date__c = null;
                    t.Discount_Period_Months__c = null;
                    
                    t.Discount_Period_End_Date__c = dc.Discount_Period_End_Date__c;
                    t.Discount_Period_Months__c = dc.Discount_Period_Months__c;
                    
                    system.debug('ADJ dc.Discount_Amount__c = '+ dc.Discount_Amount__c);
                    system.debug('ADJ dc.Discount_Period__c = '+ dc.Discount_Period__c);
                    system.debug('ADJ dc.UnavailableSiteTypes__c = '+ dc.UnavailableSiteTypes__c);
                    system.debug('ADJ dc.UnavailableContractTypes__c = '+ dc.UnavailableContractTypes__c);
                    system.debug('ADJ dc.Discount_Name__C = '+ dc.Discount_Name__C);
                    system.debug('ADJ dc.Discount Amount = '+ Discount_Amount.get(t.Discounts__c));
                }
                
                if (t.Discounts__c != null){                    
                    if (Discounts.size() < 1){
                        t.Discounts__c.addError('This discount is not currently available.');
                    }                    
                    else if (UnAvailableContractType.get(t.Discounts__c) == null){
                        t.Discount_Amount__c = t.gross_price__c * Discount_Amount.get(t.Discounts__c)/ 100;
                        system.debug('ADJ t.Discount_Amount__c = '+ t.Discount_Amount__c);
                    } 
                    else if (UnAvailableContractType.get(t.Discounts__c) != null){
                        
                        if (UnAvailableContractType.get(t.Discounts__c).contains(t.Contract_Type__c +'::')){
                            t.Discounts__c.addError('This discount is not available to the selected contract type.');
                        }   
                        else {
                            system.debug('mpk dis amt = '+t.Discount_Amount__c+' gross = '+t.gross_price__c+' dis_c = '+t.Discounts__c+' dis get = '+Discount_Amount.get(t.Discounts__c));                            
                            t.Discount_Amount__c = t.gross_price__c * Discount_Amount.get(t.Discounts__c)/ 100;                            
                            system.debug('ADJ t.Discount_Amount__c = '+ t.Discount_Amount__c);
                        }
                    }
                }
                //Net Price - Calculate net price
                
                t.Price_after_Discounts__c = t.gross_price__c - t.Discount_Amount__c ;
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////
                ///                                                                                                        ///
                ///                                 START Calc existing season 3 prices                                    ///
                ///                                                                                                        ///
                //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            /* 
             *   commented as Season 3 is not used anymore - mpk 28Feb19
             * 
                // can not change site type or discount in season 3/4
                t.Site_Type_S2__c = t.Site_Type__c;
                t.Discounts_S2__c = t.Discounts__c;           
                
                // set pmflookup key          
                If( BTPrice.Price_Type__c == '1'){ // ie price based purly on site type
                    PMFUnique.add(t.Site_Type_S2__c);
                    t.PmfLookupKey__c = var_site_Type;
                }  
                else if( BTPrice.Price_Type__c == '2'){  // price based on site & contract (not bands)
                    PMFUnique.add(t.Site_Type_S2__c + var_contract_Type);
                    t.PmfLookupKey__c = t.Site_Type_S2__c + var_contract_Type;
                    system.debug('Price_Type__c = '+ var_site_Type + var_contract_Type);
                    system.debug('ADJ BTS Other t.PmfLookupKey__c = '+ var_contract_Type);   
                }
                else { 
                    
                    if  (BTPrice.Price_Type__c == '3RM'){// && t.Number_of_Rooms_receiving_BT_Sport__c != Null){  //JMM added not null condition to prevent mis banding
                        if(t.Number_of_Rooms_receiving_BT_Sport__c == Null)  t.Number_of_Rooms_receiving_BT_Sport__c.addError('This is required for Hotel Bar or Bar only');
                        
                        List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where 
                                                                     Site_Type__c = :var_site_Type and 
                                                                     Price_Band_Start__c <= :t.Number_of_Rooms_receiving_BT_Sport__c and Price_Band_end__c >= :t.Number_of_Rooms_receiving_BT_Sport__c Limit 1 ];
                        t.Band__c = PriceBandSelect[0].Band__c;
                    }
                    
                    else if  (BTPrice.Price_Type__c == '3RV'){// && t.Rateable_Value__c != Null){ //JMM added not null condition to prevent mis banding
                        if(t.Rateable_Value__c == Null)  t.Rateable_Value__c.addError('This is required');
                        List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where 
                                                                     Site_Type__c = :var_site_Type and  
                                                                     Price_Band_Start__c <= :t.Rateable_Value__c and Price_Band_end__c >= :t.Rateable_Value__c Limit 1 ];
                        t.Band__c = PriceBandSelect[0].Band__c;
                    }
                    
                    
                    PMFUnique.add(t.Site_Type_S2__c + var_contract_Type + t.Band__c);
                    t.PmfLookupKey__c = t.Site_Type_S2__c + var_contract_Type + t.Band__c;
                    roomsPmfLookupKey = 'BT Sport Total Hotel Rooms' + var_contract_Type;
                    
                    system.debug('ADJ PMFUnique.add2 = '+ t.Site_Type__c + var_contract_Type + t.Band__c);
                    system.debug('ADJ t.PmfLookupKey__c2 = '+ t.PmfLookupKey__c);   
                    system.debug('ADJ roomsPmfLookupKey2 = '+ roomsPmfLookupKey);  
                }
                
                // lookup using  PmfLookupKey__c the correct price entry    
                set<string> PMFcodes2 = new Set<string>();
                Map<String, String> PMF_Value_S2 = new Map<String, String>();
                system.debug('mpk PMFUnique2 = '+PMFUnique+' roomsPmfLookupKey2 = '+roomsPmfLookupKey);
                List<BT_Sport_Pricing__c> pmfCode_S2 = [Select Site_Type__c, PMF_Code__c, PmfLookupKey__c from BT_Sport_Pricing__c where 
                                                        Entry_Type__C = 'PricePoint' and 
                                                        (PmfLookupKey__c in :PMFUnique or PmfLookupKey__c = :roomsPmfLookupKey)];
                for (BT_Sport_Pricing__c pc2 :pmfCode_S2) {
                    PMF_Value_S2.put(pc2.PmfLookupKey__c, pc2.PMF_Code__c);
                    PMFcodes2.add(pc2.PMF_Code__c);
                    if (pc2.PmfLookupKey__c == roomsPmfLookupKey){
                        RoomsOnly = pc2.PMF_Code__c;
                    }                
                }
                Map<String, Decimal> PMF_Price_S2 = new Map<String, Decimal>();
                Map<String, Decimal> PMF_Version_S2 = new Map<String, Decimal>();
                system.debug('mpk PMFcodes2 = '+PMFcodes2);
                
                List<BT_Sport_Pricing__c> pmfPrice_S2 = [Select PMF_Code__c, Price__c, PMF_Version__c from BT_Sport_Pricing__c where 
                                                         Entry_Type__C = 'PMF' and PMF_Code__c in :PMFcodes2 and 
                                                         Start_Date__C <= :PreviousSeason_StartDate and End_Date__C <= :PreviousSeason_EndDate ] ;
                //mpk Start_Date__C <= :PreviousSeason_StartDate and End_Date__C >= :PreviousSeason_EndDate ] ;
                for (BT_Sport_Pricing__c pp2 :pmfPrice_S2) {
                    PMF_Price_S2.put(pp2.PMF_Code__c, pp2.Price__c);
                    PMF_Version_S2.put(pp2.PMF_Code__c, pp2.PMF_Version__c);                
                }
                
                t.gross_price_s2__c = PMF_Price_S2.get(PMF_Value_S2.get(t.PmfLookupKey__c));
                t.pmf_version_S2__c = PMF_Version.get(PMF_Value_S2.get(t.PmfLookupKey__c));
                t.Discount_Amount_s2__c = 0;
                t.Discount_Amount_RO__c = 0;
                system.debug('ADJ t.gross_price_S2__c = '+ t.gross_price_s2__c);
                system.debug('ADJ t.pmf_version_S2__c = '+ t.pmf_version_S2__c);
                system.debug('ADJ S2 rooms price = '+ PMF_Price_S2.get(roomsonly)); 
                
                //Calculate Hotel Rooms only Price
                If(t.Site_Type_S2__c.contains('Hotel Rooms') && t.Number_of_Rooms_receiving_BT_Sport__c != null){
                    t.gross_price_s2__c = PMF_Price_S2.get(PMF_Value_S2.get(t.PmfLookupKey__c)) * t.Number_of_Rooms_receiving_BT_Sport__c;
                    if(t.gross_price_s2__c <60){
                        t.gross_price_s2__c =60;
                    }
                }
                
                //BT Sport Pack Hotel Bar = add the value of rooms ( if any) to gross price 
                If (t.Site_Type_S2__c.contains('Hotel Bar') && t.Number_of_Rooms_receiving_BT_Sport__c != null&& !(t.Site_Type_S2__c.contains('Hotel Bar Only')))  {
                    // t.gross_price_s2__c =  t.gross_price_s2__c + (PMF_Price_S2.get(roomsonly) * integer.valueof(t.Number_of_Rooms_receiving_BT_Sport__c)); 
                    roomsPrice = PMF_Price_S2.get(roomsonly) * integer.valueof(t.Number_of_Rooms_receiving_BT_Sport__c);
                    if(roomsPrice <60){
                        roomsPrice =60;
                    }
                    t.gross_price_S2__c =  t.gross_price_s2__c + roomsPrice;
                }
                
                //Loyalty Discounts - Perishable discount  
                Map<String, Decimal> Discount_Amount2 = new Map<String, Decimal>();
                Map<String, Decimal> Discount_Period2 = new Map<String, Decimal>();
                Map<String, String> UnAvailableSiteType2 = new Map<String, String>();
                Map<String, String> UnAvailableContractType2 = new Map<String, string>();
                system.debug('mpk Discounts_S2__c = '+t.Discounts_S2__c+' TodaysDate = '+TodaysDate);
                List<BT_Sport_Pricing__c> Discounts2 = [Select Discount_Period_End_Date__c, Discount_Period_Months__c,Discount_Amount__c, 
                                                        Discount_Period__c, Discount_Name__C, UnavailableSiteTypes__c, UnavailableContractTypes__c 
                                                        from BT_Sport_Pricing__c where Entry_Type__C = 'Discount' and Discount_Name__C = :t.Discounts_S2__c 
                                                        and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate  ] ;
                system.debug('ADJ Discounts2.size() = '+ Discounts2.size());
                
                for (BT_Sport_Pricing__c dc2 :Discounts2) {
                    Discount_Amount2.put(dc2.Discount_Name__C, dc2.Discount_Amount__c);
                    Discount_Period2.put(dc2.Discount_Name__C, dc2.Discount_Period__c);
                    UnAvailableSiteType2.put(dc2.Discount_Name__C, dc2.UnavailableSiteTypes__c);
                    UnAvailableContractType2.put(dc2.Discount_Name__C, dc2.UnavailableContractTypes__c);
                    t.Discount_Period_End_Date_S2__c = null;
                    t.Discount_Period_Months_S2__c = null;
                    
                    t.Discount_Period_End_Date_S2__c = dc2.Discount_Period_End_Date__c;
                    t.Discount_Period_Months_S2__c = dc2.Discount_Period_Months__c;
                    system.debug('ADJ dc.Discount_Amount__c2 = '+ dc2.Discount_Amount__c);
                    system.debug('ADJ dc.Discount_Period__c2 = '+ dc2.Discount_Period__c);
                    system.debug('ADJ dc.UnavailableSiteTypes__c2 = '+ dc2.UnavailableSiteTypes__c);
                    system.debug('ADJ dc.UnavailableContractTypes__c2 = '+ dc2.UnavailableContractTypes__c);
                    system.debug('ADJ dc.Discount_Name__C2 = '+ dc2.Discount_Name__C);
                    system.debug('ADJ dc.Discount Amount2 = '+ Discount_Amount2.get(t.Discounts_S2__c));
                }
                
                if (t.Discounts_S2__c != null){
                    if (Discounts2.size() < 1){
                        t.Discounts_S2__c.addError('This Discount is not Currently available');
                    }
                    
                    else if (UnAvailableContractType2.get(t.Discounts__c) == null){
                        t.Discount_Amount_S2__c = t.gross_price_S2__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;
                        t.Discount_Amount_RO__c = t.gross_price__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;
                        system.debug('ADJ t.Discount_Amount__c2 = '+ t.Discount_Amount_S2__c);
                    }                
                    
                    else if (UnAvailableContractType2.get(t.Discounts_S2__c) != null){
                        
                        if (UnAvailableContractType2.get(t.Discounts_S2__c).contains(t.Contract_Type__c +'::')){
                            t.Discounts_S2__c.addError('This Discount is not available to this contract type');
                        }   
                        else {
                            system.debug('mpk Discount_Amount_S2__c= '+t.Discount_Amount_S2__c+' gross = '+t.gross_price_s2__c+' Discounts_S2__c = '+t.Discounts_S2__c+' dis get = '+Discount_Amount.get(t.Discounts_S2__c));
                            if(t.gross_price_S2__c !=null)
                                t.Discount_Amount_S2__c = t.gross_price_s2__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;
                            t.Discount_Amount_RO__c = t.gross_price__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;                        
                            system.debug('ADJ t.Discount_Amount_s2__c = '+ t.Discount_Amount_s2__c);
                        }
                    }
                }
                //Net Price - Calculate net price
                if(t.gross_price_S2__c !=null)
                    t.Price_after_Discounts_S2__c = t.gross_price_S2__c - t.Discount_Amount_S2__c ;
                t.Price_after_Discounts_RO__c = t.gross_price__c - t.Discount_Amount_RO__c ; 
                */
                //END Interim Season 3 additions 
            }           
        }
    }
}