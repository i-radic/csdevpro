trigger case_after_insert on Case (after insert) {
	
	if (Trigger.isAfter && Trigger.isInsert)
	{
		CaseTriggerHandler triggerHandler = new CaseTriggerHandler();
		
		triggerHandler.handleCaseAfterInserts(Trigger.new);
	}

}