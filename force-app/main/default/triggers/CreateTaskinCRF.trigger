trigger CreateTaskinCRF on CRF__c (before update) {

    CRF__c[] c = trigger.new;
    CRF__c[] cold = trigger.old; 
     
    Id loggedinUser = UserInfo.getUserId();
    
       
    if((c[0].Back_Office_Status__c != cold[0].Back_Office_Status__c) && c[0].Back_Office_Status__c=='Pending BB'){          
     
        //if(c[0].Pending_BB_Date__c == cold[0].Pending_BB_Date__c){
            //c[0].Pending_BB_Date__c = System.Today()+7;
        //}
        if(c[0].Pending_BB_Date__c != null){ 
        Date dToday = c[0].Pending_BB_Date__c;
        Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
        dt=dt.addHours(8);
     
        Task t = new Task();
        t.ownerId=loggedinUser;  
        t.WhatId=c[0].Id;
        t.subject='Sequential Pending BB';
        t.ActivityDate=c[0].Pending_BB_Date__c;
        t.IsReminderSet=true;
        t.ReminderDateTime=dt;
        t.RecordTypeId=[Select Id from RecordType where DeveloperName=:'CRF_Task'].Id;
        insert t; 
        
        }
    }     
    
           
    if((c[0].Back_Office_Status__c == cold[0].Back_Office_Status__c) && c[0].Back_Office_Status__c=='Pending BB'){          
     
        if(c[0].Pending_BB_Date__c != cold[0].Pending_BB_Date__c && c[0].Pending_BB_Date__c!=null){

            Date dToday = c[0].Pending_BB_Date__c;
            Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
            dt=dt.addHours(8);
             
            //Updating existing task with the new pending date
            List<Task> TList = [Select Id,ActivityDate,IsReminderSet,ReminderDateTime from Task where WhatId=:c[0].Id AND Status!='Completed' AND Subject=:'Sequential Pending BB'];
            if(TList.size()>0){
            
            
                TList[0].ActivityDate=c[0].Pending_BB_Date__c;
                TList[0].IsReminderSet=true;
                TList[0].ReminderDateTime=dt;
                update TList[0];
                
                /*
                delete TList[0];
                
                
                Task t = new Task();
                t.ownerId=loggedinUser;  
                t.WhatId=c[0].Id;
                t.subject='Sequential Pending BB';
                t.ActivityDate=c[0].Pending_BB_Date__c;
                t.IsReminderSet=true;
                t.ReminderDateTime=dt;
                insert t; 
                System.debug('-------------->'+c[0].Id);
                Id crfId = c[0].Id;
                
                //CRFRedirectfromTrigger obj = new CRFRedirectfromTrigger ();

                //obj.ReDirect(crfId); 
                
                CRFRedirectfromTrigger.Redirect(crfId);
                System.debug('Redirected to class');
                */
            
        }
    }    

    }
    
    //Closing all the open activities when CRF Order Issued.
    if((c[0].Back_Office_Status__c != cold[0].Back_Office_Status__c) && c[0].Back_Office_Status__c=='Order Issued'){          
    List<Task> TList = [Select Id,Status,Description from Task where WhatId=:c[0].Id AND Status!='Completed' AND Subject=:'Sequential Pending BB'];
    If(TList.size()>0){ 
        for(Task TL:TList){
            TL.Status='Completed';
            TL.Description='CRF Order Issued';        
        }
        update TList;
    
    }
    }
    
    
    
    //Creating task for Olympics CRF
    
    if((c[0].CRF_Status__c != cold[0].CRF_Status__c) && c[0].CRF_Status__c=='PSTN APPOINTED'){          
     
        //if(c[0].Pending_BB_Date__c == cold[0].Pending_BB_Date__c){
            //c[0].Pending_BB_Date__c = System.Today()+7;
        //}
        if(c[0].PSTN_Appt_Date__c != null){ 
        Date dToday = c[0].PSTN_Appt_Date__c;
        Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
        dt=dt.addHours(8);
     
        Task t = new Task();
        t.ownerId=loggedinUser;  
        t.WhatId=c[0].Id;
        t.subject='PSTN Appt Date';
        t.ActivityDate=c[0].PSTN_Appt_Date__c;
        t.IsReminderSet=true;
        t.ReminderDateTime=dt;
        t.RecordTypeId=[Select Id from RecordType where DeveloperName=:'CRF_Task'].Id;
        insert t; 
        
        }
    }
    if((c[0].CRF_Status__c == cold[0].CRF_Status__c) && c[0].CRF_Status__c=='PSTN APPOINTED'){          
     
        if(c[0].PSTN_Appt_Date__c != cold[0].PSTN_Appt_Date__c && c[0].PSTN_Appt_Date__c!=null){

            Date dToday = c[0].PSTN_Appt_Date__c;
            Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
            dt=dt.addHours(8);
             
            //Updating existing task with the new pending date
            List<Task> TList = [Select Id,ActivityDate,IsReminderSet,ReminderDateTime from Task where WhatId=:c[0].Id AND Status!='Completed' AND Subject=:'PSTN Appt Date'];
            if(TList.size()>0){
            
            
                TList[0].ActivityDate=c[0].PSTN_Appt_Date__c;
                TList[0].IsReminderSet=true;
                TList[0].ReminderDateTime=dt;
                update TList[0];
   
            }
        }    

    }
    if((c[0].CRF_Status__c != cold[0].CRF_Status__c) && c[0].CRF_Status__c=='BROADBAND ISSUED'){          
     
        
        if(c[0].BB_Comp_Date__c != null){ 
        Date dToday = c[0].BB_Comp_Date__c;
        Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
        dt=dt.addHours(8);
     
        Task t = new Task();
        t.ownerId=loggedinUser;  
        t.WhatId=c[0].Id;
        t.subject='BB Comp Date';
        t.ActivityDate=c[0].BB_Comp_Date__c;
        t.IsReminderSet=true;
        t.ReminderDateTime=dt;
        t.RecordTypeId=[Select Id from RecordType where DeveloperName=:'CRF_Task'].Id;
        insert t; 
        
        }
    }      
    if((c[0].CRF_Status__c == cold[0].CRF_Status__c) && c[0].CRF_Status__c=='BROADBAND ISSUED'){          
     
        if(c[0].BB_Comp_Date__c != cold[0].BB_Comp_Date__c && c[0].BB_Comp_Date__c!=null){

            Date dToday = c[0].BB_Comp_Date__c;
            Datetime dt = datetime.newInstance(dToday.year(), dToday.month(),dToday.day());
            dt=dt.addHours(8);
             
            //Updating existing task with the new broad date
            List<Task> TList = [Select Id,ActivityDate,IsReminderSet,ReminderDateTime from Task where WhatId=:c[0].Id AND Status!='Completed' AND Subject=:'BB Comp Date'];
            if(TList.size()>0){
            
            
                TList[0].ActivityDate=c[0].BB_Comp_Date__c;
                TList[0].IsReminderSet=true;
                TList[0].ReminderDateTime=dt;
                update TList[0];
   
            }
        }    

    }
           
    
    
    //Closing all the open activities when CRFStatus is "Order Delivered"(Olympics CRF).
    if((c[0].CRF_Status__c != cold[0].CRF_Status__c) && c[0].CRF_Status__c=='ORDER DELIVERED'){          
    List<Task> TList = [Select Id,Status,Description from Task where WhatId=:c[0].Id AND Status!='Completed' AND (Subject=:'PSTN Appt Date' OR Subject=:'BB Comp Date')];
    If(TList.size()>0){ 
        for(Task TL:TList){
            TL.Status='Completed';
            TL.Description='CRF Order Delivered';        
        }
        update TList;
    
    }
    }
    
}