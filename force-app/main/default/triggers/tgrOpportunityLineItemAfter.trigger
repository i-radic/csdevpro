trigger tgrOpportunityLineItemAfter on OpportunityLineItem (after delete, after insert,after update) {
	
    if(TriggerDeactivating__c.getInstance().OpportunityLineItem__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }

     if (trigger.isDelete) {
    
        List<Opportunity_Line_Item_Delete__c> oliItemDeleteInsert = new List<Opportunity_Line_Item_Delete__c>();
    
        List<Id> BTLBOppTaskUpdateOwners = new List<id>();
        
        
        for (OpportunityLineItem oliItems: Trigger.old){
            Opportunity_Line_Item_Delete__c oliDelete = new Opportunity_Line_Item_Delete__c();
            oliDelete.Line_Item_Id__c = oliItems.Id;
            oliDelete.Product_Code__c = oliItems.ProdCode__c;
            oliDelete.Category__c = oliItems.Category__c;
            oliDelete.LastUpdatedDate__c = oliItems.LastModifiedDate;
            oliDelete.LastUpdatedByEIN__c = oliItems.LastModifiedBy_EIN__c;
            oliDelete.Parent_Opportunity_Id__c = oliItems.Parent_Opportunity_Id__c;
            oliItemDeleteInsert.add(oliDelete);
            }
        
        if (!oliItemDeleteInsert.isEmpty()){
        insert oliItemDeleteInsert;
        }
              
        Set<Id> OpportunityId = new Set<Id>();      
        Set<Id> OpportunityLineItemId = new Set<Id>();      
        MAP<Id,Id> MapOpportunityIDs = new MAP<Id,Id>();        
        MAP<Id,decimal> LFSValues = new MAP<Id,decimal>();      
        List<OpportunityLineItem>  OppoLineItems = new List<OpportunityLineItem>();
        
        for(OpportunityLineItem oliItem: Trigger.old){
            OpportunityId.add(oliItem.OpportunityId);
            OpportunityLineItemId.add(oliItem.id);
            MapOpportunityIDs.put(oliItem.OpportunityId,oliItem.OpportunityId);
        }
        

        OppoLineItems = [select Id,LFS_Value__c,OpportunityId from OpportunityLineItem Where Id IN :OpportunityLineItemId AND OpportunityId IN : OpportunityId];  
        
        
        for(OpportunityLineItem OppList : Trigger.old){
            
             if(MapOpportunityIDs.containsKey(OppList.OpportunityId))
                 {
                     if(LFSValues.containsKey(OppList.OpportunityId)) {                         
                        decimal val = LFSValues.get(OppList.OpportunityId);
                        val += OppList.Lfs_Value__c;
                        LFSValues.put(OppList.OpportunityId,val);
                     }
                     else {                         
                        LFSValues.put(OppList.OpportunityId,OppList.Lfs_Value__c);
                     }
                    } 
                 } 
              
        
       
    
    List<Opportunity> UpdateOpportunitylist = new List<Opportunity>();
    List<Opportunity> UpdateList = new  List<Opportunity>();
    UpdateOpportunitylist = [select Id,LFS_Value__c from Opportunity where ID IN : OpportunityId];
    
        for(Opportunity O : UpdateOpportunitylist){
            if(LFSValues.ContainsKey(O.id)){
                system.debug('LLLLL KKKKKKK'+LFSValues.get(O.id)+'IIIIIIII'+O.LFS_Value__c);
                if(O.LFS_Value__c !=null){
                O.LFS_Value__c -= LFSValues.get(O.id);
                UpdateList.add(O);
                }              
            }
        }
        
        Update UpdateList;   
        
    }    
 
  
    
   if(trigger.isInsert || trigger.isUpdate){
        
        Set<Id> OpportunityId = new Set<Id>();
        
        Set<Id> OpportunityLineItemId = new Set<Id>();
        
        MAP<Id,Id> MapOpportunityIDs = new MAP<Id,Id>();
        
         MAP<Id,decimal> LFSValues = new MAP<Id,decimal>();
        
        List<OpportunityLineItem>  OppoLineItems = new List<OpportunityLineItem>();
        
        for(OpportunityLineItem olineItems : trigger.new){
            
                OpportunityId.add(olineItems.OpportunityId);
                OpportunityLineItemId.add(olineItems.id);
                MapOpportunityIDs.put(olineItems.OpportunityId,olineItems.OpportunityId);
        }
        
        OppoLineItems = [select Id,LFS_Value__c,OpportunityId from OpportunityLineItem Where Id NOT IN :OpportunityLineItemId AND OpportunityId IN : OpportunityId];    
        
        if(OppoLineItems.size()>0)
        {
        for(OpportunityLineItem OppList : OppoLineItems){
            
             if(MapOpportunityIDs.containsKey(OppList.OpportunityId))
                 {
                     if(LFSValues.containsKey(OppList.OpportunityId)) {                         
                        decimal val = LFSValues.get(OppList.OpportunityId);
                        val += OppList.Lfs_Value__c;
                        LFSValues.put(OppList.OpportunityId,val);
                     }
                     else {                         
                        LFSValues.put(OppList.OpportunityId,OppList.Lfs_Value__c);
                     }
                    } 
                 } 
        
        }
        
        for(OpportunityLineItem OppLineItemList : trigger.new){
            
             if(MapOpportunityIDs.containsKey(OppLineItemList.OpportunityId))
                 {
                     if(LFSValues.containsKey(OppLineItemList.OpportunityId)) {                         
                        decimal val = LFSValues.get(OppLineItemList.OpportunityId);
                        val += OppLineItemList.Lfs_Value__c;
                        LFSValues.put(OppLineItemList.OpportunityId,val);
                     }
                     else {                         
                        LFSValues.put(OppLineItemList.OpportunityId,OppLineItemList.Lfs_Value__c);
                     }
                    } 
                 } 
         
    
    List<Opportunity> UpdateOpportunitylist = new List<Opportunity>();
    List<Opportunity> UpdateList = new  List<Opportunity>();
    UpdateOpportunitylist = [select Id,LFS_Value__c from Opportunity where ID IN : OpportunityId];
    
        for(Opportunity O : UpdateOpportunitylist){
            if(LFSValues.ContainsKey(O.id)){
                O.LFS_Value__c = LFSValues.get(O.id);
                UpdateList.add(O);              
            }
        }
        
        Update UpdateList;
    } 
    
   
}