// History
//
// Version       Date            Author          Comments
// 1.01.0        10-07-2009      Alan Jackson    Initial version
// 1.02.0        10-07-2009      Alan Jackson    coded cleaned up & changed field names to cater for Tier RAG


trigger calculateCommitmentTarget on Contract (before insert, before update){

//get global variables
/*Set<string> selectGV = new Set<string>();//Create a set of Global Variable to get from custom object
selectGV.add('contract_commitment_tgt_red');
Map<String, String> globVar = new Map<String, String>();//create map to hold values
List<Global_Variables__c> GVs = [select Name, Gvalue__c from Global_Variables__c where Name in :selectGV];// read in values
for (Global_Variables__c GV :GVs)
globVar.put(gv.Name,gv.gValue__c);//populate map
integer TargetRed = integer.valueOf(globVar.get('contract_commitment_tgt_red'));//set variables
*/
integer TargetRed = 100;

public Date sDate = null;
public Date mDate = null;
public integer DaysIntoContract = 0;    
public Double TargetSpendToDate = 0;    
public Double spendPercent = 0; 
public Double cSpend = 0;
public Double mSpend = 0;


    for(Contract c:Trigger.new)
  { 
    

        if     (c.Committed_Spend__c < 0.01 || c.Committed_Spend__c == null ){//IF No commitment spend show Green RAG status
                c.Monitor_Spend_Commitment_Image__c = '/img/samples/light_green.gif';
                c.Monitor_Spend_Commitment_Percent__c = 99999999;
                }
       else if (c.Current_Monitor_Spend__c < 0.01 || c.Current_Monitor_Spend__c == null){//IF No monitor spend show red RAG status
                c.Monitor_Spend_Commitment_Image__c = '/img/samples/light_red.gif';
                c.Monitor_Spend_Commitment_Percent__c = 0;
                }       
                
        else{// calculate if on target to meet commitment 
              if (c.Last_Rollover_Date__c > c.StartDate)//set start date to be greater of start or rollover date
                sDate = c.Last_Rollover_Date__c;
              else
                sDate = c.StartDate;
            
           mDate = c.Current_Monitor_Spend_Date__c;
           
           DaysIntoContract = sDate.daysbetween(mDate);
           if (DaysIntoContract > 365)
            DaysIntoContract = (DaysIntoContract - 365);
        
            if (DaysIntoContract > 0)
                TargetSpendToDate = (c.Committed_Spend__c * DaysIntoContract / 365);
            else 
                TargetSpendToDate = 0;  
            
            if (TargetSpendToDate != 0)//calculate % spend to date - catch zero spend divide by error
                spendPercent = (c.Current_Monitor_Spend__c *100 / TargetSpendToDate); 
            else 
                spendPercent = 0;
            spendPercent = spendPercent.intValue();   
            
            if  (spendPercent < TargetRed ){// Red TFL
            c.Monitor_Spend_Commitment_Image__c = '/img/samples/light_red.gif';
            c.Monitor_Spend_Commitment_Percent__c = spendPercent;
            }
            else { // Green TFC
            c.Monitor_Spend_Commitment_Image__c = '/img/samples/light_green.gif';// Green TFL
            c.Monitor_Spend_Commitment_Percent__c = spendPercent;
            }     
    } 
  } 
}