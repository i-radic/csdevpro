trigger BookToBillTaskClose on BookToBill__c (before update, after update) {

String oldval;
String newval;

if (!FollowUpTaskHelper.hasAlreadyCreatedFollowUpTasks()){


List<Task> taskList=new List<Task>();
Set<String> btbIds = new Set<String>();

    if(Trigger.isBefore){
        

    for(BookToBill__c dold:trigger.old)

    oldval = dold.Revenue_Assurance_Status__c;
    
    
    String ProfileId=UserInfo.getProfileId();
    List<Profile> p=[Select name from Profile where id=:ProfileId]; 
    //System.debug('newval---->'+newval);
    //System.debug('ProfileName---->'+p[0].name);

    if((oldval== 'Audit Closed'||oldval== 'Audit Cancelled')&& (Trigger.isUpdate) && (Trigger.isBefore)&&(p[0].name!='System Administrator'&& p[0].name!='BookToBill Team'&& p[0].name!='Corporate: Admin user'&& p[0].name!='BookToBill WinBack')){
       Trigger.new[0].addError('Unable to modify the record due to the status of Revenue Assurance is Audit Closed.');
    }
    
    }
    
    if(Trigger.isAfter){
    //BookToBill__c[] dnew = trigger.new;
    
    for(BookToBill__c dnew:trigger.new)
    btbIds.add(dnew.Id);
    
    List<BookToBill__c> dlist = [SELECT Id,Revenue_Assurance_Status__c FROM BookToBill__c WHERE id in : btbIds];
    
    for(BookToBill__c dnew:trigger.new){
    
    newval = dlist[0].Revenue_Assurance_Status__c;
    //System.debug('oldval---->'+oldval);
    //System.debug('newval---->'+newval);
    List<Task> tlist = [SELECT Id,status FROM Task where WhatId =: dnew.Id];
    if(tList.size()>0){
    if(oldval!=newval && newval== 'Audit Closed' && tList[0].Status!= 'Completed'){
    
        //System.debug('tlist -----------'+tlist.size());
        if(tlist.size()>0){
        
        for(Task tsk:tlist){
        tsk.Status='Completed';
        tsk.Description='Auto Closed';
        taskList.add(tsk);
        
        }
    }
    if(newval == 'Audit Closed')
    FollowUpTaskHelper.setAlreadyCreatedFollowUpTasks();  
    //FollowUpTaskHelper.setAlreadyCreatedFollowUpTasks(dnew[0].Id);  
    //System.debug('xxxxxxx -----------'+taskList.size());     
    
    
    }
   }
   }
  
   Update taskList;
   
   }
         
   }
}