trigger LandscapeDBAMinsertupdatedby on Landscape_DBAM__c (before Insert) {

Set<id> LSIDs = new Set<id>();


    for(Landscape_DBAM__c LSS :trigger.new)
    {
        Profile checkProfile;
        checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];
        system.debug ('ProfileID: ' + checkProfile.Name) ;
        
          
         
             if( LSS.Total_Calls_Spend__c!= null || 
                LSS.Audio_Conferencing_End_Date__c != null ||
                LSS.Broadband_End_Date__c != null ||
                LSS.Calls_contract_end_date__c != null ||
                LSS.Inbound_service_contract_end_date__c != null ||
                LSS.LAN_End_Date__c != null ||
                LSS.Leased_line_End_Date__c != null ||
                LSS.Lines_contract_end_date__c != null ||
                LSS.Mobile_End_Date__c != null ||
                LSS.PBX_maintenance_contract_end_date__c != null ||                
                LSS.SM_Validation_Date__c != null ||
                LSS.Video_Conferencing_End_Date__c != null ||
                LSS.WAN_End_Date__c != null ||
                LSS.Calls_and_Lines_Optional_Notes__c != null ||
                LSS.Communications_Systems_Optional_Notes__c != null ||
                LSS.Conferencing_Optional_Notes__c != null ||
                LSS.Getting_Online_and_Apps_Optional_Notes__c != null ||               
                LSS.IT_Services_Optional_Notes__c != null ||
                LSS.Mobility_Optional_Notes__c != null ||
                LSS.Network_Optional_Notes__c != null ||
                LSS.SM_Validation_Notes__c != null ||
                LSS.The_total_IT_Comms_spend__c != null ||
                LSS.Additional_calls_suppliers__c != null ||
                LSS.Additional_lines_suppliers__c != null ||
                LSS.How_many_connections_do_you_have_BB__c != null ||
                LSS.How_many_employees_use_IT__c != null ||
                LSS.How_many_lines_do_you_have__c != null ||
                LSS.How_many_mobiles_do_you_have__c != null ||
                LSS.How_many_printers_do_you_have__c != null ||
                LSS.How_many_servers_do_you_have__c != null ||
                LSS.How_many_switches_do_you_have_overall__c != null ||
                LSS.Total_number_of_employees__c != null ||
                LSS.Total_number_of_sites__c != null ||
                LSS.Total_number_of_sites_outside_UK__c != null ||
                LSS.Number_of_UK_employees__c != null ||
                LSS.Number_of_non_UK_employees__c != null ||
                LSS.Number_of_UK_sites__c != null ||
                LSS.How_many_connections_do_you_have_LL__c != null ||
                LSS.How_old_is_your_main_system__c != null ||
                LSS.Volume_of_Homeworkers__c != null ||
                LSS.Audio_conferencing_supplier__c != null ||
                LSS.Brand_of_equipment__c != null ||
                LSS.Broadband_provider__c != null ||
                LSS.Is_the_customer_prepared_to_have_a_conve__c != null ||
                LSS.Has_the_customer_ceased_trading__c != null ||
                LSS.How_are_your_PCs_maintained__c != null ||
                LSS.How_are_your_printers_maintained__c != null ||
                LSS.Inbound_services_supplier__c != null ||
                LSS.LAN_Equipment__c != null ||
                LSS.LAN_maintenance_supplier__c != null ||
                LSS.Leased_line__c != null ||
                LSS.Line_of_Business__c != null ||
                LSS.Main_PBX_brand__c != null ||
                LSS.Main_PC_manufacturer__c != null ||
                LSS.Number_of_PCs__c != null ||
                LSS.Primary_calls_supplier__c != null ||
                LSS.Primary_lines_supplier__c != null ||
                LSS.Primary_mobile_supplier__c != null ||
                LSS.Type__c != null ||
                LSS.Video_conferencing_supplier__c != null ||
                LSS.WAN_supplier__c != null ||
                LSS.What_is_the_dominant_PBX__c != null ||
                LSS.What_type_of_hosting_do_you_use__c != null ||
                LSS.Which_email_system_do_you_use__c != null ||
                LSS.Who_is_your_hosting_supplier__c != null ||
                LSS.Who_maintains_your_main_PBX__c != null ||
                LSS.Registration_Number__c != null ||
                LSS.Validated_By__c != null ||
                LSS.Website__c != null ||
                LSS.Phones_connected_to_main_phone_system__c != null ||
                LSS.What_type_of_WAN_do_you_use__c != null ||
                LSS.How_many_sites_make_up_your_WAN__c != null ||
                LSS.WAN_Optional_Notes__c != null ||
                LSS.LAN_Optional_Notes__c != null ||
                LSS.Where_do_you_buy_your_IT_Hardware__c != null ||
                LSS.Preferred_IT_Hardware_brand__c != null ||
                LSS.Who_is_your_server_provider__c != null ||
                LSS.IT_support_contract_renewal_date__c != null ||               
                LSS.Actual_Industry_Classification__c != null ||
                LSS.Does_the_customer_have_a_HQ_in_the_UK__c != null ||
                LSS.Audio_Conferencing_no_contractual_term__c != null ||
                LSS.Broadband_no_contractual_term__c != null ||
                LSS.Calls_no_contractual_term__c != null ||
                LSS.Inbound_service_no_contractual_term__c != null ||
                LSS.IT_support_no_contractual_term__c != null ||
                LSS.LAN_maintenance_no_contractual_term__c != null ||
                LSS.Leased_line_no_contractual_term__c != null ||
                LSS.Lines_no_contractual_term__c != null ||
                LSS.Mobile_no_contractual_term__c != null ||
                LSS.Phone_syst_no_contractual_term__c != null ||
                LSS.Video_Conferencing_no_contractual_term__c != null ||
                LSS.WAN_no_contractual_term__c != null ||
                LSS.How_many_office_based_connected_users__c != null ||
                LSS.How_many_mobile_connected_users__c != null && checkProfile.Name.contains('DBAM') )
                {
   
             
             
             
             
                LSS.Updated__c = system.now();
                //System.debug('@@@@@@'+LSS.Updated__c);
                
                LSS.UpdatedBy__c = UserInfo.getFirstName() + ' ' + UserInfo.getLastName(); 
                }
                             
           
      
    }




      
    
    
    }