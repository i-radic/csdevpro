trigger CampaignAfter on Campaign (after update, after insert) {
    
    /*if(trigger.isUpdate){
        Set<Id> campaignIds = new Set<Id>();
        for(Campaign c : trigger.new){
            if(c.X_Day_Rule__c != trigger.oldMap.get(c.Id).X_Day_Rule__c 
                || c.EndDate != trigger.oldMap.get(c.Id).EndDate){
                campaignIds.add(c.Id);
            }
        }
        //do async call to update Users and tasks due to large volumes
        if(campaignIds.size()>0){
            FutureCampaignInvokedUpdates.updateTasksAndUsers(campaignIds);
        }
    }*/
    
    if(trigger.isInsert){
        Set<Id> campaignIds = new Set<Id>();
        for(Campaign c: trigger.new){
            campaignIds.add(c.Id);  
        }
        
        List<CampaignMemberStatus> lCmsToInsert = new List<CampaignMemberStatus>();
        List<Campaign_Call_Status__c> lCcs = [select Id, Campaign_Type__c, Responded__c, Status__c, Default__c from Campaign_Call_Status__c];
        
        if(!lCcs.isEmpty()){
            Map<String, List<Campaign_Call_Status__c>> mCampaignMemberStatus = new Map<String, List<Campaign_Call_Status__c>>();
            List<CampaignMemberStatus> lCmsToDelete = [select Id from CampaignMemberStatus where CampaignId IN :campaignIds];
            
            for(Campaign_Call_Status__c ccs : lCcs){
                if(mCampaignMemberStatus.containsKey(ccs.Campaign_Type__c)){
                    mCampaignMemberStatus.get(ccs.Campaign_Type__c).add(ccs);
                }else{
                    List<Campaign_Call_Status__c> lCampaign_Call_Status = new List<Campaign_Call_Status__c>();
                    lCampaign_Call_Status.add(ccs);
                    mCampaignMemberStatus.put(ccs.Campaign_Type__c, lCampaign_Call_Status);
                }
            }
            
            for(Campaign c: trigger.new){
                List<Campaign_Call_Status__c> listCcs = mCampaignMemberStatus.get(c.Campaign_Type_For_Status__c);
                if(listCcs != null && !listCcs.isEmpty()){
                    integer i = 3;//start at 3 as there are 2 x default status (sent and responded) that are automatically added to the campaign member
                    for(Campaign_Call_Status__c ccs : listCcs){
                        CampaignMemberStatus cms = new CampaignMemberStatus(CampaignId = c.Id, HasResponded=ccs.Responded__c, Label=ccs.Status__c, isDefault = ccs.Default__c, SortOrder=i);
                        lCmsToInsert.add(cms);
                        i++;
                        if(lCmsToInsert.size() == 1000){
                            insert lCmsToInsert;
                            lCmsToInsert.clear();
                        }
                    }
                }
            }
        
            if(!lCmsToInsert.isEmpty()){
                insert lCmsToInsert;
            }
            //delete the original statuses (should be able to remove this if in setup the 'sent/responded' status are adjusted)
            if(!lCmsToDelete.isEmpty()){
                delete lCmsToDelete;
            }
        }
    }
}