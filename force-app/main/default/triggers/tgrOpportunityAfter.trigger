trigger tgrOpportunityAfter on Opportunity (after delete, after insert, after undelete, after update) {
/*
      Code #101CCDCT tgrCascadeCloseDateChangesToLineItems All Lines used in this trigger would be marked with this code for future reference.
      Code #102OPPCT tgrOpCompetitor All Lines used in this trigger would be marked with this code for future reference.
      Code #103SUSCT trgSendUpdatedStageToCosine All Lines used in this trigger would be marked with this code for future reference.
      Code #104UFPAT tgrUpdateForecastPortfolioArea All Lines used in this trigger would be marked with this code for future reference.
      Date            Author                       Comment
      29/08/11        Krupakar Jogannagari
      03/11/11        John McGovern                #105 Salesforce to Salesforce for BT EngageIT
      18/12/2012      Phanidra Mangipudi           #All tasks must be Cancelled if Opp Cancelled (Line # 122 - 138)  
      21/01/2013      Rita Opoku-Serebuoh          Commented out FOS related code - Search string 'FOSRemoval'  
      12/03/2013      John McGovern                Additional code to support Busiess Direct SF to SF CR3809
      26/11/2015      Praveen Malyala              CR8377 - update BTOP status when Opp status is Lost or Cancelled. line - 215-225
      Copyright © 2010  BT.
*/    

    //Do not run on CloudVoice quick start
    if(TriggerDeactivating__c.getInstance().Opportunity__c ) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
    if(StaticVariables.getCloudVoiceDontRun()){
        return; 
    }

    if(trigger.isInsert || trigger.isUpdate) {
  
        //Cloud Voice -- update CV Site zOpptyStage field if Oppty won        
        for (Opportunity o: Trigger.new) {
            if(o.stagename == 'Won') {
                List<cloud_Voice_Site__c> cvsList = [SELECT Id, zOpptyWon__c, CV__r.Opportunity__c FROM cloud_Voice_Site__c WHERE CV__r.Opportunity__c =: o.Id];
                List<cloud_Voice__c> cvList = [SELECT Id, zOpptyWon__c FROM cloud_Voice__c WHERE Opportunity__c =: o.Id];
                if(cvsList.Size() > 0){
                    for (cloud_Voice_Site__c site : cvsList) {
                            site.zOpptyWon__c = True;
                    }
                    update cvsList;
                }
                if(cvList.Size() > 0){
                    for (cloud_Voice__c head : cvList) {
                            head.zOpptyWon__c = True;
                    }
                    update cvList;     
                }           
            }
        }       
        //Line Added To Counter 101 SOQL Exception On Cloud Voice
        StaticVariables.setCloudVoiceDontRun(true);

        //#105 S2S Start      
        if (trigger.isInsert){
            StaticVariables.setContactValidation(true);
        }
        
        if(StaticVariables.gets2sOpportunityDontRun())return;
 
        Set<Id> allOpps = new Set<Id>();
        Set<Id> allOppsCon = new Set<Id>();
               
        List<s2s_Opportunities__c> newS2S = new List<s2s_Opportunities__c>(); // create list to populate LE details if owning SAC
        
        //ignore for test case
        if (Test.isRunningTest() == False){
            for (Opportunity o:Trigger.new) {
                
                 if (((o.Routing_Product__c == 'Engage IT' || o.Product_Family__c == 'ENGAGE IT') && o.CreatedDate >= (date.newinstance(2011, 11, 19)))&& o.Type <> 'Records Only'){ 
                     allOpps.add(o.Id);
                     if (trigger.isUpdate &&((trigger.newmap.get(o.id).Routing_Product__c != trigger.oldmap.get(o.id).Routing_Product__c) || (trigger.newmap.get(o.id).Product_Family__c != trigger.oldmap.get(o.id).Product_Family__c )))    {
                          allOpps.add(o.Id);                   
                    }
                    StaticVariables.s2sType = 'Engage';
                }
                 if (((o.Product_Family__c == 'INET') && o.CreatedDate >= (date.newinstance(2014, 09, 15)))&& o.Type <> 'Records Only'){ 
                     allOpps.add(o.Id);
                     if (trigger.isUpdate &&((trigger.newmap.get(o.id).Routing_Product__c != trigger.oldmap.get(o.id).Routing_Product__c) || (trigger.newmap.get(o.id).Product_Family__c != trigger.oldmap.get(o.id).Product_Family__c )))    {
                          allOpps.add(o.Id);                   
                    }
                    StaticVariables.s2sType = 'Engage';
                }
                //added in CR3809 & CR8318
                if (((o.Routing_Product__c == 'Business Direct' || o.Routing_Product__c == 'IT Services Education' || o.Routing_Product__c == 'IT Services Enterprise'|| o.Routing_Product__c == 'IT Services Kit'|| o.Product_Family__c == 'BUSINESS DIRECT') && o.CreatedDate > (date.newinstance(2013, 04, 18)))&& o.Type <> 'Records Only'){ 
                     allOpps.add(o.Id);
                     if (trigger.isUpdate &&((trigger.newmap.get(o.id).Product_Family__c != trigger.oldmap.get(o.id).Product_Family__c )))    {
                          allOpps.add(o.Id);          
                    }
                    StaticVariables.s2sType = 'BusDir';
                }   
            }        
        }      
        List<OpportunityContactRole> oppCons = [SELECT Id,ContactId, OpportunityId, Opportunity.Opportunity_Id__c FROM OpportunityContactRole WHERE OpportunityId = :allOpps AND IsPrimary = True ];//list of Primary contacts
        
        system.debug('OOOOOO OOOOOOOOOO'+oppCons);
        
        for (OpportunityContactRole oC: oppCons ) {
            allOppsCon.add(oC.OpportunityId);
        } 
   
            //check for those with no contact and error out
            
            for (Opportunity o:Trigger.new) {
                    if ((allOppsCon.contains(o.Id) == False) && (allOpps.contains(o.Id) == True) && (StaticVariables.getContactValidation() == False) ) {
                    o.addError('A primary contact must be selected before sending this to Business Direct, Engage IT or iNet.\nPlease add a Primary Contact to Contact Roles or change the Product Family/Routing Product.') ; 
                    return ;               
                    }
                }     
            for (OpportunityContactRole oC: oppCons ) {
                s2s_Opportunities__c s2sO = new s2s_Opportunities__c();
                s2sO.Opp_ID__c = oC.OpportunityId;
                s2sO.Opportunity_ID__c = oC.Opportunity.Opportunity_Id__c;
                newS2S.add(s2sO); 
                
                system.debug('KKKLLLLL KLLLLLLLL'+newS2S+'LOOOOOOOO ::::::'+ s2sO.Opportunity_ID__c);
            }
                                                        
        //upsert new or changed records in in batch ( rather than one by one which breaks Govenor Limits) 
        if(newS2S.size() > 0) 
        upsert newS2S Opportunity_ID__c;
        //#105 S2S END
        
        //#101CCDCT - Variable Declaration -- Start
        List<opportunitylineitem> opplinelist = new list<opportunitylineitem>();
        List<Opportunitylineitem> updatelist = new list<opportunitylineitem>();
        List<Product2> prodList = new list<Product2>();    
        Integer closedatediff = 0;
        Integer fbdDateDiff = 0;
        Integer srdDateDiff = 0;
        //#101CCDCT - Variable Declaration -- End    
        
        //#103SUSCT - Variable Declaration -- Start
        ID fosRecType = '01220000000A1Sg';
        ID stdRecType = '01220000000PjDu';
        //#103SUSCT - Variable Declaration -- End
        
        //#104UFPAT - Variable Declaration -- Start
        Set<Id> oppIds = new Set<Id>();
        Set<Id> oppIds_btlb = new Set<Id>();
        Set<Id> prodIds = new Set<Id>();
        Map<String, Forecasting_Portfolio_Area__c> mPortfolioForecasts = new Map <String, Forecasting_Portfolio_Area__c>();
        Map<String, String> mcheckOpplineItems = new Map <String, String>();
        Map<ID, Product2> mProducts = new Map <ID, Product2>();
        Date myDate = System.today() - 16;
        //#104UFPAT - Variable Declaration -- End
        
        // CR3682 - code changes in progess - phani
        
        ID lfsRecType = '01220000000ABFB';
        if( trigger.isInsert ) {
            for (Opportunity o: Trigger.new) {
                if ((o.LFS_CUG__c == null || o.LFS_CUG__c != null) && lfsRecType == o.RecordTypeId) {
                    system.debug('Am called in LFS CUG NUll');
                    
                    //List<Opportunity> opp = [SELECT id,Account.Phone__c FROM Opportunity WHERE id =: o.Id LIMIT 1];
                    
                    //SalesforceServicesCRM.CRMServiceSoap service = new SalesforceServicesCRM.CRMServiceSoap();
                    //service.timeout_x = 60000;
                    //string s = service.getCUGId('02074352331');
                    //o.LFS_CUG__c = new SalesforceServicesCRM.GetCUGId('02074352331');                                                            
                    //o.LFS_CUG__c = service.GetCUGId(opp[0].Account.Phone__c);            
                    //system.debug('Am Phani Called :'+s);
                    /*
                    SalesforceServicesCRM.CRMServiceSoap service = Endpoints.SFCRMGateway();
                    service.timeout_x = 60000;
                    String CugId = '02074352331';
                    String s  = service.GetCUGId(CugId);
                    */
                    /*
                    system.debug('*-* Customer Mobile : ' + o.Customer_Contact_Mobile__c);
                    LfsCallToCMPS.getCUGId(o.Customer_Contact_Mobile__c, o.Id);                 
                    system.debug('*-* in Trigger ' + LfsCallToCMPS.lfsCugId);*/
                    //o.LFS_CUG__c = LfsCallToCMPS.lfsCugId;
                    //LFS_GetCugUsingTelephone.getCug(); 
                    //system.debug('CUG ID : '+LFS_GetCugUsingTelephone.getCug());
                    
                }
            }
        } //CR3682 - End */
              
        if(trigger.isUpdate) {
                        
            //#101CCDCT - Code Part Of the Opportunity Line Items -- Start    
            if(NIBRandNIERCalculator.isrun){
                return;
            }
            NIBRandNIERCalculator.isrun=true;
            opplinelist = [select id,First_Bill_Date__c,service_ready_date__c,opportunityid,Manual_FBD_Diff__c,Manual_SRD_Diff__c,Close_Date_Diff__c, Category__c, ProdId__c, Competitor__c from opportunitylineitem where opportunityid in:trigger.new];
            for(opportunitylineitem oli : opplinelist){
                prodIds.add(oli.ProdId__c);
            }
            prodList = [Select p.SRD_Lead_Time__c, p.Name, p.IsActive, p.Id, p.FBD_Lead_Time__c From Product2 p where p.Id in:prodIds];
            for(Product2 p : prodList){
                mProducts.put(p.Id, p);
            }
            //#101CCDCT - Code Part Of the Opportunity Line Items -- End
            
            //#101CCDCT - Code Part Of the Opportunity Line Items -- Start 
            for(Opportunity o : trigger.new){
                system.debug('Trigger update and new  : ');                            
                //#104UFPAT - Code Opportunity ID Population -- Start
                if(Test.isRunningTest() || (o.isClosed == true && o.CloseDate > myDate)){
                    oppIds.add(o.Id);
                }
                
                //CR2519 - if Opp stage is cancelled all tasks under opportunities must be closed.
                
                if( o.StageName == 'Cancelled' ){                     
                    List<Task> taskList = [SELECT Id, Status, Description, IsDeleted FROM Task WHERE WhatId = :o.id AND Status != 'Completed' AND IsDeleted=false];                   
                    if(!taskList.isEmpty()){
                        for(Task t : taskList){                            
                            t.Status = 'Completed'; 
                            if(t.Description == null || t.Description == '')
                                t.Description = 'Auto Closed as Opportuinity Cancelled';    
                            t.Details__c = 'Auto Closed as Opportuinity Cancelled';                    
                            system.debug('Task Status : '+t.Status);                            
                            update t;
                        }
                    }                       
                }
                //CR8377 - Update BTOP status when Opp status is Lost or Cancelled
                if( o.StageName == 'Cancelled' || o.StageName == 'Lost'){
                    List<BT_One_Phone__c> bTOPList = [SELECT Id, Opportunity_IDx__c, Status__c FROM BT_One_Phone__c WHERE Opportunity__c= :o.id];                   
                    if(!bTOPList.isEmpty()){
                        for(BT_One_Phone__c b : bTOPList){                            
                            b.Status__c= 'Opportunity Cancelled';                                               
                            system.debug('BT_One_Phone__c Status : '+b.Status__c + ' :: Opportunity_IDx__c : ' + b.Opportunity_IDx__c);                            
                            update b;
                        }
                    }                       
                }
                
                //CR2519 - END                
                    
                //#104UFPAT - Code Opportunity ID Population -- End
                
                //#101CCDCT - Code for cascade close date changes to lineitems -- Start
                if(o.closedate !=trigger.oldMap.get(o.id).closedate){
                    system.debug('Found a difference');
                    //How much do we need to change the dates by ?
                    if(trigger.oldMap.get(o.id).closedate !=null && o.closedate!=null){
                        system.debug('Found a close date difference');
                        closedatediff = trigger.oldMap.get(o.id).closedate.daysbetween(o.closedate);
                    }
                    
                    //Now loop through the lineitems and update as necessary
                    integer ncount=0;
                    for (OpportunityLineItem oppline:opplinelist){
                        system.debug('Found a line item to update');
                        if((oppline.opportunityid ==o.id && oppline.service_ready_date__c != null)){
                            if(closedatediff !=0 ) {
                                    // ROS - 01/03/2013
                                    oppline.First_Bill_Date__c = oppline.First_Bill_Date__c.adddays(closedatediff);
                                    oppline.Service_Ready_Date__c = oppline.Service_Ready_Date__c.adddays(closedatediff);
                                    oppline.Close_Date_Diff__c = closedatediff;

                                //Map Code to avoid Duplicate entries
                                mcheckOpplineItems.put(oppline.Id, oppline.Id);
                                updatelist.add(oppline);
                            }
                            //#102OPPCT - Code Condition -- Start
                            if (o.Main_Competitor__c != trigger.oldMap.get(o.Id).Main_Competitor__c && oppline.Category__c == 'Main' && oppline.OpportunityId == o.Id){
                                oppline.Competitor__c=o.Main_Competitor__c;
                                //Map Code to avoid Duplicate entries
                                if(!mcheckOpplineItems.containsKey(oppline.Id)) {
                                    updatelist.add(oppline);
                                }
                                                            }
                            //#102OPPCT - Code Condition -- End
                        } 
                    }

                }
                else {
                    for (OpportunityLineItem oppline:opplinelist){
                        //#102OPPCT - Code Condition -- Start
                        if (o.Main_Competitor__c != trigger.oldMap.get(o.Id).Main_Competitor__c && oppline.Category__c == 'Main' && oppline.OpportunityId == o.Id){
                            oppline.Competitor__c=o.Main_Competitor__c;
                            updatelist.add(oppline);
                        }
                        //#102OPPCT - Code Condition -- End
                    }
                }
                //#101CCDCT - Code for cascade close date changes to lineitems -- End
                
            }
            //#101CCDCT - Code Part Of the Opportunity Line Items -- End           
            //#104UFPAT - update forecast protfolio area trigger -- Start
            for(Opportunity o : [select Id, Account.Sector_Code__c from Opportunity where Id in: oppIds and Account.Sector_Code__c = 'BTLB']){
                oppIds_btlb.add(o.Id);
            }
      
            for(Forecasting_Portfolio_Area__c fpa : [Select f.id, f.Opportunity__c, f.Sales_Type__c, f.Portfolio_Area__c, f.Actual_Revenue_SOV__c, f.Actual_Qty__c, f.Actual_ACV__c From Forecasting_Portfolio_Area__c f where f.Opportunity__c in :oppIds_btlb]){
                fpa.Actual_Revenue_SOV__c = 0;
                fpa.Actual_Qty__c = 0;
                fpa.Actual_ACV__c = 0;
                mPortfolioForecasts.put(fpa.Opportunity__c + fpa.Portfolio_Area__c + fpa.Sales_Type__c, fpa);
            }
            
            for(OpportunityLineItem oli : [select Portfolio_Area__c, Sales_Type__c, UnitPrice, ACV_Calc__c, Qty__c, OpportunityId, Opportunity.IsWon, Opportunity.Close_Date_Fiscal_Week__c,Opportunity.zCurrent_Fiscal_Week__c from OpportunityLineItem where OpportunityId in :oppIds_btlb]){
                if(!mPortfolioForecasts.containsKey(oli.OpportunityId + oli.Portfolio_Area__c + oli.Sales_Type__c)){
                    Forecasting_Portfolio_Area__c fpa;
                    if(oli.Opportunity.IsWon == true){
                        fpa = new Forecasting_Portfolio_Area__c(
                            Portfolio_Area__c = oli.Portfolio_Area__c,
                            Sales_Type__c = oli.Sales_Type__c,
                            Actual_Revenue_SOV__c = oli.UnitPrice, 
                            Actual_Qty__c = oli.Qty__c,
                            Actual_ACV__c = oli.ACV_Calc__c,
                            
                            Opportunity__c = oli.OpportunityId);
                    }else{
                        fpa = new Forecasting_Portfolio_Area__c(
                            Portfolio_Area__c = oli.Portfolio_Area__c,
                            Sales_Type__c = oli.Sales_Type__c,
                            Actual_Revenue_SOV__c = 0, 
                            Actual_Qty__c = 0,
                            Actual_ACV__c = 0,
                            Opportunity__c = oli.OpportunityId);
                    }
                    mPortfolioForecasts.put(oli.OpportunityId + oli.Portfolio_Area__c + oli.Sales_Type__c, fpa);
                }else{
                    Forecasting_Portfolio_Area__c fpa = mPortfolioForecasts.get(oli.OpportunityId + oli.Portfolio_Area__c + oli.Sales_Type__c);
                    if(oli.Opportunity.IsWon == true){                      
                        fpa.Actual_Revenue_SOV__c += oli.UnitPrice;
                        fpa.Actual_Qty__c += oli.Qty__c;
                        fpa.Actual_ACV__c += oli.ACV_Calc__c;
                        
                    }else{
                       
                        fpa.Actual_Revenue_SOV__c = 0;
                        fpa.Actual_Qty__c = 0;
                        fpa.Actual_ACV__c = 0;
                    }
                }
            }
     
      
            //#104UFPAT - update forecast protfolio area trigger -- End
        }
       
        //#101CCDCT - Code Part Of the Opportunity Line Items -- Start
        if(updatelist.size() > 0)
            database.update(updatelist);
        //#101CCDCT - Code Part Of the Opportunity Line Items -- End
        
        //#104UFPAT - update forecast protfolio area trigger -- Start
        if(!mPortfolioForecasts.isEmpty())
            upsert mPortfolioForecasts.values() id;
        //#104UFPAT - update forecast protfolio area trigger -- End
    }
}