trigger tgrCreateSalesJourneyandNIBR_NIER on OpportunityLineItem (after insert,after update,after delete) {
 /**********************************************************************
     Name:  tgrCreateSalesJourney.trigger()
     Copyright Â© 2010  BT.
     ======================================================
    ======================================================
    Purpose 1:                                                            
    ----------       
    
    This Trigger is used to create new Sales journey records and relate them to
    Oppportunities. The Sales Journey records are only created when an ICT product
    is added to an opportunity.
    
    Once the record is created, the record type of the opportunity is also
    changed so that the ICT sales journey page layout is displayed and not the 
    standard Opportunity record type 
    
    Purpose 2: This trigger also enables NIBR NIER functionality on the Opportunity.
    It examines various attributes of the opportunity line item being added/removed/ameneded
    and create both a set of product revenue records, which detail the revenue(NIBR + NIER) for each fiscal year
    of the contract. It also creates a set of revenue summary fields which aggregate up all of the products and give
    combined revenue amounts for given fiscal years.
                                                                
    
                                                          
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
    1.0 -    Greg Scarrtott    22/02/2010        INITIAL DEVELOPMENT OF ICT             Initial Build: 
    2.0 -    Greg Scarrtott    15/03/2010        CAT TESTING Fixes/Enhancements         Remove Role Functionality: 
    3.0 -    Greg Scarrtott    1/04/2010         INITIAL Development of NIBR/NIER 
                                                 functionality                          Creationg of product revenue and revenue summary records 
    4.0 -   Jon Brayshaw        10/04/2010       Sharing functionality                  Add ability to share opportunity based on main product 
    
    ***********************************************************************/

    //Constants
    final string ICT_REC_TYPE_NAME_CONST='ICT - Sales Process';
    final string ICT_PRODUCT_FLAG='ICT';
    final string ICT_TIMELINE='ICT Product';
    final string CATEGORY_MAIN='Main';
    
    
   //The timeline variables
    private double leadTimescale = 0;
    private integer intleadTimescale = 0;
    private double qualifyTimescale = 0;
    private integer intqualifyTimescale = 0;
    private double sellTimescale = 0;
    private integer intsellTimescale = 0;
    private double proposeTimescale = 0;
    private integer intproposeTimescale = 0;
    private double closureTimescale = 0;
    private integer intclosureTimescale = 0;

    //The new Sales Journey Record
    private ict_sales_journey__c ictsj = new ict_sales_journey__c ();
    
    //List of Sales Journey records for bulkified trigger
    private list<ict_sales_journey__c> ictsjlist = new list<ict_sales_journey__c>();
    
    //List of opportunities related to these Opp Line Items
    //(For bulkified Trigger)
    private list<opportunity> opplist = new list<opportunity>();

    //Used for opps,products and line items
    private Set<ID> oppidset = new Set<ID>();
    private Set<ID> prodset = new Set<ID>();
    private boolean bresult = false;

    //Variable used to store current opportunity
    private Opportunity thisopprec = new Opportunity ();

    private Product_Revenue__c prodrevrec = new product_revenue__c();
    private List<Product_Revenue__c> prodrevlist = new list<Product_Revenue__c>(); 
         
    private List<product_revenue__c> prl = new list<product_revenue__c>();
    private List<opportunitylineitem> oli = new list<opportunitylineitem>();
    system.debug('opplinetriggercalled');
    
    //Added by GS - 7-11-2010 - Should not be run for BTLB
    system.debug('UserType----'+UserInfo.getUserType()+'getName--'+UserInfo.getName());
    
     system.debug('Profile---'+userinfo.getprofileid());
    //Added To Fix EE Migration Issue for S2S User 
    profile userProfile = null;
    List<Profile> p = new List<Profile>();
    if(UserInfo.getUserType() != 'PartnerNetwork' && UserInfo.getName() != 'Connection User'){
        p = [select name from profile where id =:userinfo.getprofileid()];
    }    
     
    if(p.size()>0){
        system.debug('Profile'+p);
            if(!String.isBlank(p[0].name)){ 
                system.debug('ProfileName'+p[0].name);               
             if ((UserInfo.getUserType() != 'PartnerNetwork' && UserInfo.getName() != 'Connection User') || p[0].name.contains('BTLB')){
             return;        
            }
        }
    }          
   
    if(NIBRandNIERCalculator.runopplineonce){
            return;
    }
    NIBRandNIERCalculator.runopplineonce=true;
    
    if(trigger.isDelete){
        oli = trigger.old;
    }else{
        oli = trigger.new;
    }
    
    //Get a list of opportunities for this trigger
    for(OpportunityLineItem opplineitem : oli){
        oppidset.add(opplineitem.opportunityid);
        prodset.add(opplineitem.prodid__c);
    }

    //Create map of opps and products. 
    private Map<id,Opportunity> mapopplist = new Map<id,Opportunity>([select id,name,createddate from opportunity where id in :oppidset]);
    private Map<id,Product2> mapprodlist = new Map<id,Product2>([select id,name,isictproduct__c from product2 where id in :prodset]);

    private ID prodid ;

    private List<OpportunityLineItem> addShare = new List<OpportunityLineItem>{};
    private List<OpportunityLineItem> deleteShare = new List<OpportunityLineItem>{};
    private List<String> groupNames = new List<String>{};

    private Boolean isICTProduct = false;
    private Product2 prodrec = new product2();

    //Get the id for the ICT record type. We may need this.
    private RecordType ictrectype = [select id from recordtype where name = :ICT_REC_TYPE_NAME_CONST];
    system.debug('**rectype = ' + ictrectype);


    //Get every Opportunity Product for this opportunity and build up the revenue records
        //Start
     private List<Opportunitylineitem> opplineitemlist = new List<Opportunitylineitem>();
     private Set<id> oliid = new Set<id>();
        for (opportunitylineitem olirec:oli){
            oliid.add(olirec.opportunityid);
        }
        // ROS - 28/01/2013
        //opplineitemlist = [select opportunityid,Billing_Cycle__c,category__c,unitprice,Initial_Cost__c,Contract_Term_Months__c,First_Bill_Date__c,service_ready_date__c,ProdId__c,opp_service_ready__c,opp_first_bill__c from opportunitylineitem where opportunityid in :oliid];
        opplineitemlist = [select opportunityid,Billing_Cycle__c,category__c,unitprice,Initial_Cost__c,Contract_Term_Months__c,First_Bill_Date__c,service_ready_date__c,ProdId__c from opportunitylineitem where opportunityid in :oliid];
    
         //Flush the old product revenue records for all Opps
        
        List<product_revenue__c> deleterecs = new List<product_revenue__c>();
                
        deleterecs = [select id,fiscal_year_commencing__c,product__c,earned_revenue_auto__c,billed_revenue_auto__c from product_revenue__c pr where pr.opportunity__c in :oliid];
        system.debug('gsdel' + deleterecs.size());
        system.debug('gsset' + oliid);
        
       //GPS2806 delete deleterecs;
     
        system.debug('doesnt get here');
        
    //Loop through the line items
    try{
    for(OpportunityLineItem opplineitem : oli){
        //Get the opportunity record for this line item
        system.debug('masteroppline'+opplineitem);
        
        thisopprec = mapopplist.get(opplineitem.opportunityid);
        
        //Get the product record for this line item
        prodid =opplineitem.prodid__c; 
        prodrec = mapprodlist.get(prodid);
        
        //Is the product for this line item an ICT product ?
        if(prodrec.isictproduct__c=='True'){
            isICTProduct=true;
        }else{
            isICTProduct=false;
        }
        //If the product on the line item is an ICT product, and there are not
        //already any Sales Journey records, go ahead.
        
       //--------------------------------------
       /** If((thisopprec.ICT_Sales_Journey_Count__c==0) && isICTproduct){
            //Set the name of the Sales Journey the same as the Opp Name
            ictsj.name = thisopprec.name;
            
            //Now get the custom setting timeline for this process
            List<SalesJourney__c> sj = SalesJourney__c.getall().values();
            if (sj[0].name == ICT_TIMELINE) {
                leadTimescale = sj[0].Lead_Time_weeks__c;
                qualifyTimescale=sj[0].Qualify_Time_weeks__c;
                sellTimescale=sj[0].Sell_Time_weeks__c;
                proposeTimescale=sj[0].Propose_Time_weeks__c;
                closureTimescale=sj[0].Closure_Time_weeks__c;
            }
            
            //Create the ict record
            datetime tempdate;
            date newtempdate;
            
            //Set opp relation
            ictsj.opportunity_c__c = opplineitem.opportunityid;
            
            //Get a starting date from the opp create date
            tempdate = thisopprec.CreatedDate;
            newtempdate = tempdate.date();
            
            //convert all of the doubles to ints
            intleadtimescale = leadtimescale.intvalue();
            intqualifytimescale = qualifytimescale.intvalue();
            intselltimescale = selltimescale.intvalue();
            intproposetimescale = proposetimescale.intvalue();
            intclosuretimescale = closuretimescale.intvalue(); 
    
            //add the custom setting to the create date to get the lead date
            ictsj.Lead_Due_Date__c = dateHolidayHandler.adddayswithholidays(newtempdate,intleadtimescale,false);
        
            //add the custom setting to the lead date to get the qualify date
            ictsj.qualify_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.Lead_Due_Date__c,intqualifytimescale,false);
            
            //add the custom setting to the qualify date to get the sell date
            ictsj.sell_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.qualify_Due_Date__c,intselltimescale,false);
            
            //add the custom setting to the sell date to get the propose date
            ictsj.propose_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.sell_Due_Date__c,intproposetimescale,false);
            
            //add the custom setting to the propose date to get the closure date
            ictsj.closure_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.propose_Due_Date__c,intclosuretimescale,false);
            
            //Add this ICT sales journey to the list
            
            ictsjlist.add(ictsj);
        
            //update the opportunity calculated close date
            opportunity upopp = new opportunity(id=opplineitem.opportunityid);
            //upopp.Calculated_Close_Date__c = ictsj.closure_Due_Date__c;
            
            //Flip the record type for this opportunity to make it an "ICT" opportunity.
            Id rectypeId = [SELECT RecordtypeId FROM Opportunity WHERE Id=:upopp.Id].RecordtypeId;
            String rectypeName = [SELECT DeveloperName FROM RecordType WHERE Id=:rectypeId].DeveloperName;
            if(rectypeName != 'Leads_from_Service')
            upopp.RecordTypeId = ictrectype.Id;
            
            //Add this opportunity to the list
            opplist.add(upopp);
            
        }*/
        
        //MOVED JB CODE
        // jb add logic to generate sharing rules
        system.debug('JB oppline info category:' +  opplineitem.category__c);
        system.debug('JB oppline info trigger type:' +  trigger.isDelete);
        system.debug('JB oppline info supplier:' +  opplineitem.ProductSupplier__c);
        If (opplineitem.category__c == CATEGORY_MAIN && trigger.isDelete && opplineitem.ProductSupplier__c != null) {
            system.debug('JB: delete share product supplier is:' + opplineitem.ProductSupplier__c);
            deleteShare.add(opplineitem);
            groupNames.add(opplineitem.ProductSupplier__c);
        }
        else If (opplineitem.category__c == CATEGORY_MAIN && opplineitem.ProductSupplier__c != null && opplineitem.OwnerCanShare__c == 'YES'){
            system.debug('JB: adding share product supplier is:' + opplineitem.ProductSupplier__c);
            addShare.add(opplineitem);
            groupNames.add(opplineitem.ProductSupplier__c);
        }
        
        //END
        //GPS28-06 
     }  // moved end of main for loop
        
        //NIBR + NIER SECTION START.
        
        prl.clear();
        
        List<Product_Revenue__c> NIBRProdrevlist = new list<Product_Revenue__c>(); 
        List<Product_Revenue__c> NIERProdrevlist = new list<Product_Revenue__c>();  
        
       
        //If all of the products have been removed - tidy up the revenue
        //summary records
        
        NIBRandNIERCalculator NandNClear = new NIBRandNIERCalculator();
       
      
        
       // if(opplineitemlist.size()==0){
       //   //Tidy up the revenue summary records.
       //   NandNClear.refreshrevenuesummary(thisopprec.id);
       // }
        
        //Generate the list of product revenue records for NIBR + NIER
        
        For(opportunitylineitem thisopplinerec:opplineitemlist){
                NIBRProdrevlist=NIBRandNIERCalculator.getprodrevlist(thisopplinerec,True);
                NIERProdrevlist=NIBRandNIERCalculator.getprodrevlist(thisopplinerec,False);
                
                system.debug('NIBRProdrevlist:' + NIBRProdrevlist);
                system.debug('NIERProdrevlist:' + NIERProdrevlist);
                
                //Combine the two lists for a complete NIBR / NIER product revenue list
                prl.addAll(NIBRProdrevlist);
                Boolean bmatch = false;
                    
                        for(product_revenue__c pr : NIERProdrevlist){
                            for (product_revenue__c pr2:NIBRProdrevlist ){
                                    if(pr2.fiscal_year_commencing__c==pr.fiscal_year_commencing__c){
                                        pr2.earned_revenue_auto__c = pr.earned_revenue_auto__c;         
                                        bMatch=true;
                                    }
                            }
                            If(bMatch==false){
                                system.debug('Adding a NIER Only'+pr);
                                prl.add(pr);
                            }
                            bMatch = false;
                        }
        }
        
        
    
    //GPS28-06}  // end of main for loop
    
    //
    // setup the records to add/remove the sharing records
    //
    OpportunityShare [] addOppShare = new OpportunityShare []{};
    String [] deleteGroupId = new String []{};
    String [] deleteOppId = new string [] {};
    if(groupNames.size() > 0){
        for(Group [] allGroups : [select id, Name from group where name in :GroupNames] )
          for(Group thisGroup : allGroups) {
            for(OpportunityLineItem thisLineItem : addShare){
                if(thisLineItem.ProductSupplier__c == thisGroup.Name){
                    OpportunityShare thisOppShare = new OpportunityShare(UserOrGroupId = thisGroup.Id, OpportunityId = thisLineItem.OpportunityId, OpportunityAccessLevel='Edit');
                    addOppShare.add(thisOppShare);
                }
            }
            for(OpportunityLineItem thisLineItem : deleteShare){
                if(thisLineItem.ProductSupplier__c == thisGroup.Name){  
                    deleteGroupId.add(thisGroup.Id);
                    deleteOppId.add(thisLineItem.OpportunityId);
                }
            }
        }
            if(addOppShare.size() > 0)insert addOppShare;
            if(deleteOppId.size() > 0) {
                system.debug('JB: query share to delete groups and opps:' + deleteGroupId + deleteOppId);
                OpportunityShare [] toDelete = [select id from OpportunityShare where userorgroupid in :deleteGroupId and opportunityid in :deleteoppid];
                if(toDelete.size() > 0)delete toDelete;
            }
    }
    
   
        //Update the DB
        //Added by GS 28th June 2010 - Need to reduce DML so just delete/insert the records we need to
        //Which of these delete recs have records in the upsert list.
        List<product_revenue__c> newDeleteList = new List<product_revenue__c>();
        List<product_revenue__c> newInsertList = new List<product_revenue__c>();
        
        Boolean bFound = false;
        Set<id> MatchIDs = new Set<id>();
        for(product_revenue__c insertPR:prl){
            for(product_revenue__c deletePR:deleterecs){
                // Modified by DS 2/12/10
  //              if((insertpr.product__c == deletePR.product__c) && (insertpr.earned_revenue_auto__c ==deletepr.earned_revenue_auto__c) && (insertpr.billed_revenue_auto__c ==deletepr.billed_revenue_auto__c)){
               if((insertpr.product__c == deletePR.product__c) && (insertpr.earned_revenue_auto__c ==deletepr.earned_revenue_auto__c) && (insertpr.billed_revenue_auto__c ==deletepr.billed_revenue_auto__c) && (insertpr.fiscal_year_commencing__c ==deletepr.fiscal_year_commencing__c)){
                    bFound = true;
                    MatchIDs.add(deletePR.id);
                    // Added break statement
                    break;
                }
            }
            if(bFound==false){
                newInsertList.add(insertPR);
            }
            bFound = false;
        }
        
        //create new delete list
        for(product_revenue__c deletePR:deleterecs){
            if(!matchIds.contains(deletePr.id)){
                newDeleteList.add(deletePR);
            }
        }
        
        delete newDeleteList;
        insert newInsertList;
        
        //Tidy up revenue summary if upsert list is empty
        if(prl.size()==0){
            //Tidy up the revenue summary records.
            NandNClear.refreshrevenuesummary(thisopprec.id);
        }
        
        //Now refresh the summary revenue for this records
        
        NIBRandNIERCalculator NandN = new NIBRandNIERCalculator();
        Set<id> setid = new set<id>();
        system.debug('entering revsumfor:');
        for(product_revenue__c pr : prl){
            if(!setid.contains(pr.opportunity__c)){
                system.debug('runningrevenuerecalc');
                
                NandN.refreshrevenuesummary(pr.opportunity__c);
                setid.add(pr.opportunity__c);
            }
        }
    
       // insert ictsjlist;
       // update opplist;
 
     }catch(Exception e){
        System.debug( 'Exception tgrCreateSalesJourneyandNIBR_NIER' + e );
     }
     
}