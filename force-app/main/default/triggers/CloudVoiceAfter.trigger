trigger CloudVoiceAfter on Cloud_Voice__c (after insert, after update) {
Set<Id> Ids = New Set<Id>();
Set<Id> careIds = New Set<Id>();

if(Trigger.isUpdate && !HelperRollUpSummary.getCValt()){  

    for(Cloud_Voice__c c:Trigger.New){
        Cloud_Voice__c oldc = Trigger.oldMap.get(c.Id);  
        if(c.Contract_Term__c != oldc.Contract_Term__c){
            Ids.add(c.ID);
        }   
        if(c.Care__c != oldc.Care__c){
            careIds.add(c.ID);
        }            
    }  
    List<Cloud_Voice_Site__c> cvs= [SELECT ID,CPE_Multiple_Delivery_Addresses__c FROM Cloud_Voice_Site__c WHERE CV__c = :Ids];  
    List<Cloud_Voice_Site__c> carecvs= [SELECT ID FROM Cloud_Voice_Site__c WHERE CV__c = :careIds];      
    List<Cloud_Voice_Site_Product__c> cvsp= [SELECT ID, Cloud_Voice_Site__r.CV__r.Contract_Term__c, Term__c, Term_Alternative__c, Term_Alternative_12__c, Term_Alternative_24__c, Term_Alternative_36__c, Term_Alternative_60__c, SFCSSProdId__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :cvs AND Type__c = 'License'];     
    List<Cloud_Voice_Site_Product__c> carecvsp= [SELECT ID, Care__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :carecvs AND Type__c = 'License'];      
    
    for (Cloud_Voice_Site_Product__c c:cvsp){
		String cvTerm = c.Cloud_Voice_Site__r.CV__r.Contract_Term__c;
		if(cvTerm == '12 months'){
	        c.SFCSSProdId__c = c.Term_Alternative_12__c;
		}else if(cvTerm == '24 months'){
	        c.SFCSSProdId__c = c.Term_Alternative_24__c;
		}else if(cvTerm == '36 months'){
	        c.SFCSSProdId__c = c.Term_Alternative_36__c;
		}else if(cvTerm == '60 months'){
	        c.SFCSSProdId__c = c.Term_Alternative_60__c;
		}
    }
    for (Cloud_Voice_Site__c c:cvs){
        if(c.CPE_Multiple_Delivery_Addresses__c == True){
            HelperRollUpSummary.setCValt(true);
        }
    }
    for(Cloud_Voice__c cv:Trigger.New){ 
        for (Cloud_Voice_Site_Product__c c:carecvsp){
            c.Care__c = cv.Care__c;
        }
    }

    if(cvsp != Null){
        update cvsp;
    }
    if(carecvsp != Null){    
        update carecvsp;
    }   
}
}