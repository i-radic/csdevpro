//
// History
//
// Version      Date            Author          Comments
// 1.0.0        25-01-2011      Dan Measures    Initial version
// 2.0.0        25-04-2011      Krupakar Reddy  Changes According to New Data Model
//

//
// Comments
//
// 1. Populate relationships: Opportunity and Owner (User)
//
trigger BTBOrderBefore on BTB_Order__c (before insert, before update) {
	Boolean bUpdateDataload = true; 
    Set<String> sEin = new Set<String>();
    Set<String> sOppId = new Set<String>();
    Set<String> sAcc = new Set<String>();               //LE code
    Map<String, Id> mEinUser = new Map<String, Id>();
    Map<String, Id> mOpp = new Map<String, Id>();   
    Map<String, Id> mAcc = new Map<String, Id>();       //LE code
    
    //  20110509 - Start - Check Cases (ODF/CVD)
    Map<String,Id> mOrderToCaseODF = new Map<String, Id>();
    Map<String,Id> mOrderToCaseODF_closed = new Map<String, Id>();
    Map<String,Id> mOrderToCaseCVD = new Map<String, Id>();
    Map<String,Id> mOrderToCaseCVD_closed = new Map<String, Id>();
    Set<String> sOrderId = new Set<String>();
      
    RecordType rt = [select id from RecordType where SobjectType='Case' and name ='Order Discrepancy' limit 1];
    RecordType rt_creditvet = [select id from RecordType where SobjectType='Case' and name ='Credit Vet' limit 1];
        
    //----Update Status to Loading -- Not Available
    Profile checkProfile;
	checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];
	if(Test.isRunningTest() || checkProfile.Name == 'System Administrator') {
	    if(bUpdateDataload) {
		    Dataloads__c dl_InProgress = [Select Status__c, Next_Update__c, Name, Id, Data_Load__c, DataLoadOrg__c, DataLoadNotes__c, DataLoadDate__c From Dataloads__c d WHERE d.Data_Load__c =:'E2E Reporting'];
		    dl_InProgress.Status__c = 'Not Available';
		    dl_InProgress.DataLoadDate__c = datetime.now();
		    dl_InProgress.Next_Update__c = date.today().addDays(1);
		    update dl_InProgress;
		    bUpdateDataload = false;
	    }
	}

    for (BTB_Order__c o : Trigger.new){
        sOrderId.add(o.OV_ORDER_NUM__c);
    }

    for(Case c : [select id, Vol_Reference__c, RecordTypeId, Status from Case where Vol_Reference__c != null and Vol_Reference__c in :sOrderId and CVD_Feedback__c = false and (RecordTypeId = :rt_creditvet.Id or RecordTypeId = :rt.Id)]){
        if(c.RecordTypeId == rt.Id && c.Status !='Closed') 
            mOrderToCaseODF.put(c.Vol_Reference__c.toLowerCase(), c.Id);
        if(c.RecordTypeId == rt.Id && c.Status =='Closed') 
            mOrderToCaseODF_closed.put(c.Vol_Reference__c.toLowerCase(), c.Id);
        if(c.RecordTypeId == rt_creditvet.Id && c.Status !='Closed')
            mOrderToCaseCVD.put(c.Vol_Reference__c.toLowerCase(), c.Id);
        if(c.RecordTypeId == rt_creditvet.Id && c.Status =='Closed')
            mOrderToCaseCVD_closed.put(c.Vol_Reference__c.toLowerCase(), c.Id);
    }
    // End - check Cases (ODF/CVD)
    
    for (BTB_Order__c o : Trigger.new){
        //  calculate substatus for this order, we to run on update as at time of insert there are no order lines which is used in some of the business logic
        E2EReporting.generateOrderSubStatus(o, mOrderToCaseODF, mOrderToCaseODF_closed, mOrderToCaseCVD, mOrderToCaseCVD_closed);
        o.Name = o.OV_ORDER_NUM__c;
        if(o.EIN__c != null && o.EIN__c != '')
            sEin.add(o.EIN__c);
        if(o.OPPORTUNITY_ID__c != null && o.OPPORTUNITY_ID__c != '')
            sOppId.add(o.OPPORTUNITY_ID__c);
    }
    
    if(!sEin.isEmpty()){
        //for(User u : [select Id, EIN__c from user where EIN__c in :sEin and isActive = true and EIN__c <> null]){
        for(User u : [Select IsActive, ManagerId, EIN__c, Id, Manager.IsActive, Manager.ManagerId, Manager.EIN__c, Manager.Manager.IsActive, Manager.Manager.ManagerId, Manager.Manager.EIN__c, Manager.Manager.Manager.IsActive, Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.Manager.EIN__c from User where EIN__c in :sEin and EIN__c <> null]){
            if(Test.isRunningTest() || u.IsActive){
                mEinUser.put(u.EIN__c, u.Id);
            }else if(u.Manager.IsActive){
                mEinUser.put(u.EIN__c, u.ManagerId);    //  user is inactive so assign to 1st level manager if they are active
            } else if(u.Manager.Manager.IsActive){
                mEinUser.put(u.EIN__c, u.Manager.ManagerId);    //  user is inactive so assign to 2nd level manager if they are active
            } else if(u.Manager.Manager.Manager.IsActive){
                mEinUser.put(u.EIN__c, u.Manager.Manager.ManagerId);    //  user is inactive so assign to 3rd level manager if they are active
            } else if(u.Manager.Manager.Manager.Manager.IsActive){
                mEinUser.put(u.EIN__c, u.Manager.Manager.Manager.ManagerId);    //  user is inactive so assign to 4th level manager if they are active
            } else {
                mEinUser.put(u.EIN__c, StaticVariables.CAT_OWNER_NIGEL_STAGG);  //  user is inactive so assign to Nigel Stagg
            }
        }
    }
    
    if(!sOppId.isEmpty()){
        for(Opportunity opp : [select Id, Opportunity_Id__c from Opportunity where Opportunity_Id__c in :sOppId]){
            mOpp.put(opp.Opportunity_Id__c, opp.Id);
        }
    }

    for(BTB_Order__c o : Trigger.new){
        //  on insert make the owner the user specified in EIN field if provided
        if(mEinUser.containsKey(o.EIN__c)){
            o.OwnerId = mEinUser.get(o.EIN__c);
            o.Owner_Shadow__c = mEinUser.get(o.EIN__c);
        }else{
            o.Owner_Shadow__c = o.OwnerId;
        }
        if(mOpp.containsKey(o.Opportunity_Id__c)){
            //  link to opportunity
            o.Opportunity__c = mOpp.get(o.Opportunity_Id__c);
        }
    }
}