trigger BiilingInfoAutoNumber on Billing_Information__c (before insert, after delete) {

    if(Trigger.isInsert){
    Billing_Information__c[] PL = Trigger.New;
    Id CRFId = PL[0].Related_to_CRF__c;

    
    List<Billing_Information__c> PLList = [Select Id,Name,Related_to_CRF__c from Billing_Information__c where Related_to_CRF__c=:CRFId];
    if(PLList.size()>=0){
    string a = string.valueOf(PLList.size()+1);
    PL[0].Name=a;
    }
    }
    
    if(Trigger.isDelete){
    Billing_Information__c[] PL = Trigger.Old;
    Id CRFId = PL[0].Related_to_CRF__c;
    List<Billing_Information__c> aList=[SELECT Id,Name FROM Billing_Information__c WHERE Related_to_CRF__c=:CRFId ORDER BY Createddate ASC];
    if(aList.size()>0){
    Integer k = 0;
    for(Integer i=aList.size();i!=0;i--){
    aList[k].Name = string.valueOf(aList.size()-i+1); 
    k++;       
    }
    update aList;
    }
    }
}