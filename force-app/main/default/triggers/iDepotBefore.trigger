trigger iDepotBefore on iDepot__c(before insert, before update) {
/*####################################################################
Trigger to add history of into a custom object for tracking purposes.
####################################################################*/
    //Set<id> LSIDs = new Set<id>();
    string jmRun = null;
    Static Integer statusCounter;

    if (Trigger.isInsert) {

        Set < String > accnts = new Set < String > ();
        //# Code as Part Of CR 3310 - Start   
        Set < String > ownerIds = new Set < String > ();
        Map < String, String > mOwnrsDep = new Map < String, String > ();
        Map < String, String > mOwnrsDiv = new Map < String, String > ();
        //# Code as Part Of CR 3310 - End

        Map < Id, Id > mAcc = new Map < Id, Id > ();

        for (iDepot__c LS: Trigger.new) {
            accnts.add(LS.Account__c);
            //# Code as Part Of CR 3310 - Start
            string strOwnerId = LS.OwnerId;
            if (strOwnerId.substring(0, 3) == '005') {
                ownerIds.add(LS.OwnerId);
            }
            //# Code as Part Of CR 3310 - End
        }

        List < Account > AcountOwners = [Select id, ownerId from Account WHERE Id IN: accnts];

        for (Account acc: AcountOwners) {
            mAcc.put(acc.Id, acc.OwnerId);
        }

        //# Code as Part Of CR 3310 - Start
        List < User > Owners = [Select id, Department, Division from User WHERE Id IN: ownerIds];
        for (User usr: Owners) {
            mOwnrsDep.put(usr.Id, usr.Department);
            mOwnrsDiv.put(usr.Id, usr.Division);
        }
        //# Code as Part Of CR 3310 - End

        for (iDepot__c LS: Trigger.new) {           
            if (LS.RecordTypeId == '01220000000AGff' && LS.WSX_Route_To_Market__c == 'BTLB') {
                //# Code as Part Of CR 4239 - Start
                //if(mOwnrsDiv.get(mAcc.get(LS.Account__c)) == 'BTLB')
                    system.debug('JMM mOwnrsDiv.get: ' + mOwnrsDiv.get(mAcc.get(LS.Account__c)));
                    system.debug('JMM mOwnrsDiv: ' + mOwnrsDiv);
                    system.debug('JMM mAcc: ' + mAcc);
                    LS.OwnerId = mAcc.get(LS.Account__c);
  
                //# Code as Part Of CR 4239 - End
            }
            LS.LEM_SS_Raised_By__c = UserInfo.getUserId();
            //# Code as Part Of CR 3310 - Start
            if (LS.RecordTypeId == '01220000000AJjv') {
                if (mOwnrsDep.containsKey(LS.OwnerId)) {
                    LS.Owner_Department__c = mOwnrsDep.get(LS.OwnerId);
                    LS.Owner_Division__c = mOwnrsDiv.get(LS.OwnerId);
                }
            }
            //# Code as Part Of CR 3310 - End
        }
    }
    //system.debug('AllLS' + LSIDs) ;
    if (Trigger.isUpdate) {

        //# Code as Part Of CR 3310 - Start   
        Set < String > ownerIds = new Set < String > ();
        Map < String, String > mOwnrsDep = new Map < String, String > ();
        Map < String, String > mOwnrsDiv = new Map < String, String > ();
        for (iDepot__c LS: Trigger.new) {
            string strOwnerId = LS.OwnerId;
            if (strOwnerId.substring(0, 3) == '005') {
                ownerIds.add(LS.OwnerId);
            }
        }
        List < User > Owners = [Select id, Department, Division from User WHERE Id IN: ownerIds];
        for (User usr: Owners) {
            mOwnrsDep.put(usr.Id, usr.Department);
            mOwnrsDiv.put(usr.Id, usr.Division);
        }
        for (iDepot__c LS: Trigger.new) {
            if (LS.RecordTypeId == '01220000000AJjv') {
                if (mOwnrsDep.containsKey(LS.OwnerId)) {
                    LS.Owner_Department__c = mOwnrsDep.get(LS.OwnerId);
                    LS.Owner_Division__c = mOwnrsDiv.get(LS.OwnerId);
                }
            }
        }
        //# Code as Part Of CR 3310 - End      
        
            

        // insert custom history object
        List < iDepot_History__c > HistNew = new List < iDepot_History__c > ();
        //system.debug('AllLS' + LSIDs) ;


        for (iDepot__c LS: Trigger.new) {
            iDepot__c beforeUpdate = System.Trigger.oldMap.get(LS.Id);
            
            
            // CR3403 - Base Enquiries - Count Status Changes
            statusCounter = 0;      
            if(beforeUpdate.ABC2_Status_Counter__c != null)
                statusCounter = (Integer) beforeUpdate.ABC2_Status_Counter__c;
            // CR3403 - END 

            //------------ABC2 Website Fields History Track-------------------

            //Enquiry Type Section :- ABC2 Status, ABC2 Raised By, ABC2 Sub Status, Assigned to, Request Type

            if (LS.ABC2_Status__c != beforeUpdate.ABC2_Status__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Status';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Status__c;
                HistRec.New_Value__c = LS.ABC2_Status__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Enquiry Type';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
                statusCounter++;// CR3403 - Base Enquiries - Count Status Changes
            }

            if (LS.ABC2_Raised_By__c != beforeUpdate.ABC2_Raised_By__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Raised By';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Raised_By__c;
                HistRec.New_Value__c = LS.ABC2_Raised_By__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Enquiry Type';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Sub_Status__c != beforeUpdate.ABC2_Sub_Status__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Sub Status';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Sub_Status__c;
                HistRec.New_Value__c = LS.ABC2_Sub_Status__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Enquiry Type';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
                statusCounter++;// CR3403 - Base Enquiries - Count Status Changes
            }

            if (LS.Assigned_to__c != beforeUpdate.Assigned_to__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Assigned To';
                HistRec.Old_Value__c = beforeUpdate.Assigned_to__c;
                HistRec.New_Value__c = LS.Assigned_to__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Enquiry Type';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Request_Type__c != beforeUpdate.Request_Type__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Request Type';
                HistRec.Old_Value__c = beforeUpdate.Request_Type__c;
                HistRec.New_Value__c = LS.Request_Type__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Enquiry Type';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            // Company Information Section :- Company Name/ Trading Name, ABC2 Company Owner Name,  Company Type, 
            // Company Reg No, Charity No, Main BT Telephone Number      

            if (LS.Company_Name_Trading_Name__c != beforeUpdate.Company_Name_Trading_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Company Name/ Trading Name';
                HistRec.Old_Value__c = beforeUpdate.Company_Name_Trading_Name__c;
                HistRec.New_Value__c = LS.Company_Name_Trading_Name__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Company Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Owner__c != beforeUpdate.ABC2_Owner__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Company Owner Name';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Owner__c;
                HistRec.New_Value__c = LS.ABC2_Owner__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Company Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Company_Type__c != beforeUpdate.Company_Type__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Company Type';
                HistRec.Old_Value__c = beforeUpdate.Company_Type__c;
                HistRec.New_Value__c = LS.Company_Type__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Company Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Company_Reg_No__c != beforeUpdate.Company_Reg_No__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Company Reg No';
                HistRec.Old_Value__c = beforeUpdate.Company_Reg_No__c;
                HistRec.New_Value__c = LS.Company_Reg_No__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Company Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Charity_No__c != beforeUpdate.Charity_No__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Charity No';
                HistRec.Old_Value__c = beforeUpdate.Charity_No__c;
                HistRec.New_Value__c = LS.Charity_No__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Company Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Main_BT_Telephone_Number__c != beforeUpdate.Main_BT_Telephone_Number__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Main BT Telephone Number';
                HistRec.Old_Value__c = beforeUpdate.Main_BT_Telephone_Number__c;
                HistRec.New_Value__c = LS.Main_BT_Telephone_Number__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Company Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            // Address Section :- Property Name / Number, Thoroughfare Name, Locality / Village, Town / City, PostCode

            if (LS.Property_Name_Number__c != beforeUpdate.Property_Name_Number__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Property Name / Number';
                HistRec.Old_Value__c = beforeUpdate.Property_Name_Number__c;
                HistRec.New_Value__c = LS.Property_Name_Number__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Address';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Thoroughfare_Name__c != beforeUpdate.Thoroughfare_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Thoroughfare Name';
                HistRec.Old_Value__c = beforeUpdate.Thoroughfare_Name__c;
                HistRec.New_Value__c = LS.Thoroughfare_Name__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Address';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Locality_Village__c != beforeUpdate.Locality_Village__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Locality / Village';
                HistRec.Old_Value__c = beforeUpdate.Locality_Village__c;
                HistRec.New_Value__c = LS.Locality_Village__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Address';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Town_City__c != beforeUpdate.Town_City__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Town / City';
                HistRec.Old_Value__c = beforeUpdate.Town_City__c;
                HistRec.New_Value__c = LS.Town_City__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Address';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.PostCode__c != beforeUpdate.PostCode__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Postcode';
                HistRec.Old_Value__c = beforeUpdate.PostCode__c;
                HistRec.New_Value__c = LS.PostCode__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Address';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            // Contact Details Section :- Salutation, Forename, Surname, Contact Number

            if (LS.Salutation__c != beforeUpdate.Salutation__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Salutation';
                HistRec.Old_Value__c = beforeUpdate.Salutation__c;
                HistRec.New_Value__c = LS.Salutation__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Contact Details';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Forename__c != beforeUpdate.Forename__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Forename';
                HistRec.Old_Value__c = beforeUpdate.Forename__c;
                HistRec.New_Value__c = LS.Forename__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Contact Details';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Surname__c != beforeUpdate.Surname__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Surname';
                HistRec.Old_Value__c = beforeUpdate.Surname__c;
                HistRec.New_Value__c = LS.Surname__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Contact Details';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Contact_Number__c != beforeUpdate.Contact_Number__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Contact Number';
                HistRec.Old_Value__c = beforeUpdate.Contact_Number__c;
                HistRec.New_Value__c = LS.Contact_Number__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Contact Details';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            // Comments/Business Reason Section :- Business Reason/Comments, OID Contract Reference, 
            // Proposed Sac Name, Sac LEs, Are LEs subsidiaries?

            if (LS.Business_Reason_Comments__c != beforeUpdate.Business_Reason_Comments__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Business Reason/Comments';
                HistRec.Old_Value__c = beforeUpdate.Business_Reason_Comments__c;
                HistRec.New_Value__c = LS.Business_Reason_Comments__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Comments/Business Reason';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.OID_Contract_Reference__c != beforeUpdate.OID_Contract_Reference__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'OID Contract Reference';
                HistRec.Old_Value__c = beforeUpdate.OID_Contract_Reference__c;
                HistRec.New_Value__c = LS.OID_Contract_Reference__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Comments/Business Reason';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Proposed_Sac_Name__c != beforeUpdate.Proposed_Sac_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Proposed Sac Name';
                HistRec.Old_Value__c = beforeUpdate.Proposed_Sac_Name__c;
                HistRec.New_Value__c = LS.Proposed_Sac_Name__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Comments/Business Reason';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Sac_LEs__c != beforeUpdate.Sac_LEs__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Sac LEs';
                HistRec.Old_Value__c = beforeUpdate.Sac_LEs__c;
                HistRec.New_Value__c = LS.Sac_LEs__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Comments/Business Reason';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.Are_LEs_subsidiaries__c != beforeUpdate.Are_LEs_subsidiaries__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Are LEs subsidiaries?';
                HistRec.Old_Value__c = beforeUpdate.Are_LEs_subsidiaries__c;
                HistRec.New_Value__c = LS.Are_LEs_subsidiaries__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Comments/Business Reason';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            // Product Information Section :- Product Category, Product

            if (LS.Product_Category__c != beforeUpdate.Product_Category__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Product Category';
                HistRec.Old_Value__c = beforeUpdate.Product_Category__c;
                HistRec.New_Value__c = LS.Product_Category__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Product Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Product__c != beforeUpdate.ABC2_Product__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Product';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Product__c;
                HistRec.New_Value__c = LS.ABC2_Product__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Product Information';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Internal_Notes__c != beforeUpdate.ABC2_Internal_Notes__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Internal Notes';
                HistRec.Old_Value__c = 'Not Available';
                HistRec.New_Value__c = 'Not Available';
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            // SCARS Revenues Section :- ABC2 LE Name, ABC2 SCARS LE CODE, ABC2 SAC Name, ABC2 Sac Code, ABC2 Scars Period,
            // ABC2 Spend Current Month, ABC2 Spend CYTD, ABC2 Spend LYTD, ABC2 Variance, ABC2 Percentage Variance, 
            // ABC2 Spend LYFY, ABC2 Proposed Gaining Sac Name

            if (LS.ABC2_LE_Name__c != beforeUpdate.ABC2_LE_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 LE Name';
                HistRec.Old_Value__c = beforeUpdate.ABC2_LE_Name__c;
                HistRec.New_Value__c = LS.ABC2_LE_Name__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_SCARS_LE_CODE__c != beforeUpdate.ABC2_SCARS_LE_CODE__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 SCARS LE CODE';
                HistRec.Old_Value__c = beforeUpdate.ABC2_SCARS_LE_CODE__c;
                HistRec.New_Value__c = LS.ABC2_SCARS_LE_CODE__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Sac_Name__c != beforeUpdate.ABC2_Sac_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Sac Name';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Sac_Name__c;
                HistRec.New_Value__c = LS.ABC2_Sac_Name__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Sac_Code__c != beforeUpdate.ABC2_Sac_Code__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Sac Code';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Sac_Code__c;
                HistRec.New_Value__c = LS.ABC2_Sac_Code__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Scars_Period__c != beforeUpdate.ABC2_Scars_Period__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Scars Period';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Scars_Period__c;
                HistRec.New_Value__c = LS.ABC2_Scars_Period__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Spend_Current_Month__c != beforeUpdate.ABC2_Spend_Current_Month__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Spend Current Month';
                HistRec.Old_Value__c = string.valueOf(beforeUpdate.ABC2_Spend_Current_Month__c);
                HistRec.New_Value__c = string.valueOf(LS.ABC2_Spend_Current_Month__c);
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Spend_CYTD__c != beforeUpdate.ABC2_Spend_CYTD__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Spend CYTD';
                HistRec.Old_Value__c = string.valueOf(beforeUpdate.ABC2_Spend_CYTD__c);
                HistRec.New_Value__c = string.valueOf(LS.ABC2_Spend_CYTD__c);
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Spend_LYTD__c != beforeUpdate.ABC2_Spend_LYTD__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Spend LYTD';
                HistRec.Old_Value__c = string.valueOf(beforeUpdate.ABC2_Spend_LYTD__c);
                HistRec.New_Value__c = string.valueOf(LS.ABC2_Spend_LYTD__c);
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            /*if(LS.ABC2_Variance__c != beforeUpdate.ABC2_Variance__c){
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Variance';
                HistRec.Old_Value__c = string.valueOf(beforeUpdate.ABC2_Variance__c);
                HistRec.New_Value__c = string.valueOf(LS.ABC2_Variance__c);   
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';                     
                HistNew.add(HistRec);
    }
    
    if(LS.ABC2_Percentage_Variance__c != beforeUpdate.ABC2_Percentage_Variance__c){
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Percentage Variance';
                HistRec.Old_Value__c = string.valueOf(beforeUpdate.ABC2_Percentage_Variance__c);
                HistRec.New_Value__c = string.valueOf(LS.ABC2_Percentage_Variance__c);   
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';                     
                HistNew.add(HistRec);
    }*/

            if (LS.ABC2_Spend_LYFY__c != beforeUpdate.ABC2_Spend_LYFY__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Spend LYFY';
                HistRec.Old_Value__c = string.valueOf(beforeUpdate.ABC2_Spend_LYFY__c);
                HistRec.New_Value__c = string.valueOf(LS.ABC2_Spend_LYFY__c);
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Proposed_Gaining_Sac_Name__c != beforeUpdate.ABC2_Proposed_Gaining_Sac_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Proposed Gaining Sac Name';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Proposed_Gaining_Sac_Name__c;
                HistRec.New_Value__c = LS.ABC2_Proposed_Gaining_Sac_Name__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'SCARS Revenues';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            // Base Team Updates Section :- ABC2 LE Code, ABC2 Education, BOBCAT REFERENCE NUMBER, 
            // ABC2 Opportunity Reference, PSM Involved

            if (LS.ABC2_LE_Code__c != beforeUpdate.ABC2_LE_Code__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 LE Code';
                HistRec.Old_Value__c = beforeUpdate.ABC2_LE_Code__c;
                HistRec.New_Value__c = LS.ABC2_LE_Code__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Base Team Updates';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Education__c != beforeUpdate.ABC2_Education__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Education';
                HistRec.Old_Value__c = string.valueOf(beforeUpdate.ABC2_Education__c);
                HistRec.New_Value__c = string.valueOf(LS.ABC2_Education__c);
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Base Team Updates';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.BOBCAT_REFERENCE_NUMBER__c != beforeUpdate.BOBCAT_REFERENCE_NUMBER__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'BOBCAT REFERENCE NUMBER';
                HistRec.Old_Value__c = beforeUpdate.BOBCAT_REFERENCE_NUMBER__c;
                HistRec.New_Value__c = LS.BOBCAT_REFERENCE_NUMBER__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Base Team Updates';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.ABC2_Opportunity_Reference__c != beforeUpdate.ABC2_Opportunity_Reference__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'ABC2 Opportunity Reference';
                HistRec.Old_Value__c = beforeUpdate.ABC2_Opportunity_Reference__c;
                HistRec.New_Value__c = LS.ABC2_Opportunity_Reference__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Base Team Updates';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            if (LS.PSM_Involved__c != beforeUpdate.PSM_Involved__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'PSM Involved';
                HistRec.Old_Value__c = beforeUpdate.PSM_Involved__c;
                HistRec.New_Value__c = LS.PSM_Involved__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'Base Team Updates';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            //----------------ABC2 Website Changes End------------------------

            If(LS.Account__c != beforeUpdate.Account__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Account';
                HistRec.Old_Value__c = beforeUpdate.zWSXAccount__c;
                HistRec.New_Value__c = LS.zWSXAccount__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.WSX_Customer_Contact_Name__c != beforeUpdate.WSX_Customer_Contact_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Customer Contact Name';
                HistRec.Old_Value__c = beforeUpdate.zWSXContact__c;
                HistRec.New_Value__c = LS.zWSXContact__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Opportunity__c != beforeUpdate.WSX_Opportunity__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Opportunity';
                HistRec.Old_Value__c = beforeUpdate.zWSXOpp__c;
                HistRec.New_Value__c = LS.zWSXOpp__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Assigned_to__c != beforeUpdate.Assigned_to__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Assigned to';
                HistRec.Old_Value__c = beforeUpdate.zAssignedto__c;
                HistRec.New_Value__c = LS.zAssignedto__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Assigned_to_B2B__c != beforeUpdate.WSX_Assigned_to_B2B__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Assigned to (B2B)';
                HistRec.Old_Value__c = beforeUpdate.zWSXAssignedB2B__c;
                HistRec.New_Value__c = LS.zWSXAssignedB2B__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.WSX_Assigned_to_Specialist__c != beforeUpdate.WSX_Assigned_to_Specialist__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Assigned to (Specialist)';
                HistRec.Old_Value__c = beforeUpdate.zWSXAssignedSpec__c;
                HistRec.New_Value__c = LS.zWSXAssignedSpec__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_BTLB_Contact_Name__c != beforeUpdate.WSX_BTLB_Contact_Name__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX BTLB Contact Name';
                HistRec.Old_Value__c = beforeUpdate.zWSXBTLBContact__c;
                HistRec.New_Value__c = LS.zWSXBTLBContact__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.WSX_Quantity__c != beforeUpdate.WSX_Quantity__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Quantity';
                If(LS.WSX_Quantity__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Quantity__c.format();
                }
                If(beforeUpdate.WSX_Quantity__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Quantity__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Verified_Quantity__c != beforeUpdate.WSX_Verified_Quantity__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Verified Quantity';
                If(LS.WSX_Verified_Quantity__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Verified_Quantity__c.format();
                }
                If(beforeUpdate.WSX_Verified_Quantity__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Verified_Quantity__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.Product_Area__c != beforeUpdate.Product_Area__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Product Area';
                HistRec.Old_Value__c = beforeUpdate.Product_Area__c;
                HistRec.New_Value__c = LS.Product_Area__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Status__c != beforeUpdate.Status__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Status';
                HistRec.Old_Value__c = beforeUpdate.Status__c;
                HistRec.New_Value__c = LS.Status__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Sub_Status__c != beforeUpdate.Sub_Status__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Sub Status';
                HistRec.Old_Value__c = beforeUpdate.Sub_Status__c;
                HistRec.New_Value__c = LS.Sub_Status__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Type_of_Payment__c != beforeUpdate.Type_of_Payment__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Type of Payment';
                HistRec.Old_Value__c = beforeUpdate.Type_of_Payment__c;
                HistRec.New_Value__c = LS.Type_of_Payment__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Year_Covered__c != beforeUpdate.Year_Covered__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Year Covered';
                HistRec.Old_Value__c = beforeUpdate.Year_Covered__c;
                HistRec.New_Value__c = LS.Year_Covered__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Contact_Details__c != beforeUpdate.WSX_Contact_Details__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Contact Details';
                HistRec.Old_Value__c = beforeUpdate.WSX_Contact_Details__c;
                HistRec.New_Value__c = LS.WSX_Contact_Details__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Product__c != beforeUpdate.WSX_Product__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Product';
                HistRec.Old_Value__c = beforeUpdate.WSX_Product__c;
                HistRec.New_Value__c = LS.WSX_Product__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Product_Category__c != beforeUpdate.WSX_Product_Category__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Product Category';
                HistRec.Old_Value__c = beforeUpdate.WSX_Product_Category__c;
                HistRec.New_Value__c = LS.WSX_Product_Category__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Status__c != beforeUpdate.WSX_Status__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Status';
                HistRec.Old_Value__c = beforeUpdate.WSX_Status__c;
                HistRec.New_Value__c = LS.WSX_Status__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Sub_Status__c != beforeUpdate.WSX_Sub_Status__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Sub Status';
                HistRec.Old_Value__c = beforeUpdate.WSX_Sub_Status__c;
                HistRec.New_Value__c = LS.WSX_Sub_Status__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Term_Years__c != beforeUpdate.WSX_Term_Years__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Term (Years)';
                HistRec.Old_Value__c = beforeUpdate.WSX_Term_Years__c;
                HistRec.New_Value__c = LS.WSX_Term_Years__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Route_To_Market__c != beforeUpdate.WSX_Route_To_Market__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Route To Market';
                HistRec.Old_Value__c = beforeUpdate.WSX_Route_To_Market__c;
                HistRec.New_Value__c = LS.WSX_Route_To_Market__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.WSX_Verified_on_Scars__c != beforeUpdate.WSX_Verified_on_Scars__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Verified on Scars';
                HistRec.Old_Value__c = beforeUpdate.WSX_Verified_on_Scars__c;
                HistRec.New_Value__c = LS.WSX_Verified_on_Scars__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Verified_Term__c != beforeUpdate.WSX_Verified_Term__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Verified Term';
                HistRec.Old_Value__c = beforeUpdate.WSX_Verified_Term__c;
                HistRec.New_Value__c = LS.WSX_Verified_Term__c;
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.Priority_Case__c != beforeUpdate.Priority_Case__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Priority Case';
                If(LS.Priority_Case__c == True) {
                    HistRec.New_Value__c = 'Ticked';
                } Else {
                    HistRec.New_Value__c = 'Unticked';
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Deleted__c != beforeUpdate.WSX_Deleted__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Deleted';
                If(LS.WSX_Deleted__c == True) {
                    HistRec.New_Value__c = 'Ticked';
                } Else {
                    HistRec.New_Value__c = 'Unticked';
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Finance__c != beforeUpdate.WSX_Finance__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Finance';
                If(LS.WSX_Finance__c == True) {
                    HistRec.New_Value__c = 'Ticked';
                } Else {
                    HistRec.New_Value__c = 'Unticked';
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.Commission_Due__c != beforeUpdate.Commission_Due__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Commission Due';
                If(LS.Commission_Due__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.Commission_Due__c.format();
                }
                If(beforeUpdate.Commission_Due__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.Commission_Due__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Outstanding_Commission_Due__c != beforeUpdate.Outstanding_Commission_Due__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Outstanding Commission Due';
                If(LS.Outstanding_Commission_Due__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.Outstanding_Commission_Due__c.format();
                }
                If(beforeUpdate.Outstanding_Commission_Due__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.Outstanding_Commission_Due__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Annual_Maintenance_Charge__c != beforeUpdate.WSX_Annual_Maintenance_Charge__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Annual Maintenance Charge';
                If(LS.WSX_Annual_Maintenance_Charge__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Annual_Maintenance_Charge__c.format();
                }
                If(beforeUpdate.WSX_Annual_Maintenance_Charge__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Annual_Maintenance_Charge__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }

            If(LS.WSX_Annual_Rental_Charge__c != beforeUpdate.WSX_Annual_Rental_Charge__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Annual Rental Charge';
                If(LS.WSX_Annual_Rental_Charge__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Annual_Rental_Charge__c.format();
                }
                If(beforeUpdate.WSX_Annual_Rental_Charge__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Annual_Rental_Charge__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Connection_Charge__c != beforeUpdate.WSX_Connection_Charge__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Connection Charge';
                If(LS.WSX_Connection_Charge__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Connection_Charge__c.format();
                }
                If(beforeUpdate.WSX_Connection_Charge__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Connection_Charge__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Verified_Annual_Maintenance_Charge__c != beforeUpdate.WSX_Verified_Annual_Maintenance_Charge__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Verified Annual Maintenance Charge';
                If(LS.WSX_Verified_Annual_Maintenance_Charge__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Verified_Annual_Maintenance_Charge__c.format();
                }
                If(beforeUpdate.WSX_Verified_Annual_Maintenance_Charge__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Verified_Annual_Maintenance_Charge__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Verified_Annual_Rental_Charge__c != beforeUpdate.WSX_Verified_Annual_Rental_Charge__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Verified Annual Rental Charge';
                If(LS.WSX_Verified_Annual_Rental_Charge__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Verified_Annual_Rental_Charge__c.format();
                }
                If(beforeUpdate.WSX_Verified_Annual_Rental_Charge__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Verified_Annual_Rental_Charge__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Verified_Connection_Charge__c != beforeUpdate.WSX_Verified_Connection_Charge__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Verified Connection Charge';
                If(LS.WSX_Verified_Connection_Charge__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Verified_Connection_Charge__c.format();
                }
                If(beforeUpdate.WSX_Verified_Connection_Charge__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Verified_Connection_Charge__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Cobra_Payment__c != beforeUpdate.Cobra_Payment__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Cobra Payment';
                If(LS.Cobra_Payment__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.Cobra_Payment__c.format();
                }
                If(beforeUpdate.Cobra_Payment__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.Cobra_Payment__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.Date_Scheduled__c != beforeUpdate.Date_Scheduled__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'Date Scheduled';
                If(LS.Date_Scheduled__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.Date_Scheduled__c.format();
                }
                If(beforeUpdate.Date_Scheduled__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.Date_Scheduled__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.From__c != beforeUpdate.From__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'From';
                If(LS.From__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.From__c.format();
                }
                If(beforeUpdate.From__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.From__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.To__c != beforeUpdate.To__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'To';
                If(LS.To__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.To__c.format();
                }
                If(beforeUpdate.To__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.To__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Commissions_Month__c != beforeUpdate.WSX_Commissions_Month__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Commissions Month';
                If(LS.WSX_Commissions_Month__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Commissions_Month__c.format();
                }
                If(beforeUpdate.WSX_Commissions_Month__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Commissions_Month__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Contract_signed__c != beforeUpdate.WSX_Contract_signed__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Contract signed';
                If(LS.WSX_Contract_signed__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Contract_signed__c.format();
                }
                If(beforeUpdate.WSX_Contract_signed__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Contract_signed__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Next_Update__c != beforeUpdate.WSX_Next_Update__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Next Update';
                If(LS.WSX_Next_Update__c == null) {
                    HistRec.New_Value__c = ' ';
                } else {
                    HistRec.New_Value__c = LS.WSX_Next_Update__c.format();
                }
                If(beforeUpdate.WSX_Next_Update__c == null) {
                    HistRec.Old_Value__c = ' ';
                } else {
                    HistRec.Old_Value__c = beforeUpdate.WSX_Next_Update__c.format();
                }
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            If(LS.WSX_Internal_Notes__c != beforeUpdate.WSX_Internal_Notes__c) {
                iDepot_History__c HistRec = new iDepot_History__c();
                HistRec.Question__c = 'WSX Internal Notes';
                HistRec.Old_Value__c = 'Not Available';
                HistRec.New_Value__c = 'Not Available';
                HistRec.iDepot__c = LS.Id;
                HistRec.Section__c = 'NA';
                HistRec.QuestionIdentifier__c = '';
                jmRun = '1';
                HistNew.add(HistRec);
            }
            
            LS.ABC2_Status_Counter__c = statusCounter;// CR3403 - Base Enquiries - Count Status Changes
            //system.debug('Status Counter:'+statusCounter);
    }

    If(jmRun != null) {     
        insert HistNew;
    }
    }
}