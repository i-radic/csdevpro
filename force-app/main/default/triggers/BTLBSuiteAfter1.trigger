trigger BTLBSuiteAfter1 on BTLB_Suite__c (after update){
	Set<String> updIds = New Set<String>();
    for(BTLB_Suite__c t:Trigger.New){ 
		updIds.add(t.Id);
	}
	List<BTLB_Suite_Order_Lines__c> updRecs = [SELECT Id,Order_Reference__c FROM BTLB_Suite_Order_Lines__c WHERE Order_Reference__c= :updIds AND RecordType.DeveloperName ='BTLB_Suite_BTLB_Entered'];

	for(BTLB_Suite__c t:Trigger.New){ 
		for(BTLB_Suite_Order_Lines__c u:updRecs){ 
			if(t.Id == u.Order_Reference__c){
				BTLB_Suite_Order_Lines__c updRec = New BTLB_Suite_Order_Lines__c();
				u.BTLB_Commission_Sacrifice__c = t.BTLB_Commission_Sacrifice__c;
			}
		}
	}
    update updRecs;
}