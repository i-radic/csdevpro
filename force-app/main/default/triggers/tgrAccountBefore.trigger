trigger tgrAccountBefore on Account(before insert, before update) {

    //
    //  Date            Author          Comment
    //  10 Aug 2010     Jon Brayshaw    Added merge functionality this calls out to @futures method to do the merge
    //  22 Aug 2010     Alan Jackson    Added code to run only if Apex_Trigger_Account__c is true on user object
    //  09 Sep 2010     Alan Jackson    Move & merge functionality enabled
    //                                  Initially merge functionality on BTLB  Account Only init
    //  23 Sep 2010     Alan Jackson    following CAT call requirement changed not to move tasks
    //                                  merge disabled until SCARs etc changes implemented
    //  28 Sep 2010     Alan Jackson    added static variable to prevent trigger running on Contract upload
    //  28 Sep 2010     Alan Jackson    added variable to prevent automatic account ownerships changes within 15 days of manual assignment
    //                                  change postcode allocation to assign ownership using postcode if sector is not BTLB or CORP if contact entered by a user
    //  20 Jan 2011     Alan Jackson    re enabled merge functionality
    //     Aug 2011     John McGovern   added NI functionality
    //  29 Aug 2011     Alan Jackson    Disabled merge functionality ( for Ediburgh name change)
    //  16 Nov 2011     Alan Jackson    recode section updated to include enterprise accounts 
    //  05 Apr 2012     Alan Jackson    re enabled merge functionality ( for luton split) 
    //  15 May 2012     Alan Jackson    dis enabled merge functionality ( for Chris F) 
    //  22 Nov 2012     Phanidra Mangipudi for populating user mangager hierarchy into 3 new fields for 'BTB Corporate' CR3601
    //   01/05/2014     Alan JAckson  //BTLBOppTaskUpdateOwners.add(a.id); // commented out to allow BTLB name change
    //   10/09/2016     Adam Soobroy    Added ROI sectors and default managers
    //    Feb 2018      Adam Soobroy    Added Global Services sector and default manager
    
    if(TriggerDeactivating__c.getInstance().Account__c) {
         System.debug('Bypassing trigger due to custom setting');
         return;
     }    

     system.debug('ADJ StaticVariables.getAccountDontRun value = ' + StaticVariables.getAccountDontRun());
    if (StaticVariables.getAccountBeforeDontRun()) return;
    if (StaticVariables.getAccountDontRun()) return;


    //  StaticVariables.setAccountBeforeDontRun(true);
    Date LockDate = (Date.today() - 15);

    /*  commented out as now using static variables


 List<User> ApexTriggerEnabled = [Select Apex_Trigger_Account__c  from user  Where id = :Userinfo.getUserId()];
    boolean triggerRunner = ApexTriggerEnabled[0].Apex_Trigger_Account__c;
 if (triggerRunner == true || Test_Factory.GetProperty('IsTest') == 'yes') { // only trigger if dataload user is updating records
*/

    ////////// BTLB Account Load Info Gathering /////////////////////////
    // Get BTLB CCAT details linked on LOB Name
    set < string > SubSectorList = new Set < string > ();

    // Set up the map to call the merge class
    Map < String, Id > Merges = New Map < String, Id > {};
    List < Id > BTLBOppTaskUpdateOwners = new List < id > ();

    for (Account a: Trigger.new) {

        SubSectorList.add(a.sub_sector__c);
        
        if ((a.sector__c == 'BT Local Business') || (a.sector__c == 'Enterprise')) { // updated to include enterprise accounts
            if (a.MergeLe_Code__c != null) {
                Merges.put(a.MergeLe_Code__c, a.Id);
                a.MergeLe_Code__c = null;
            }
        }
    }
    system.debug('srisri'+SubSectorList);
    if (Merges.size() > 0) AccountUpdateMerge.mergeAccounts(Merges); // merge commented out until SCARS & Lansccaping merging tested


    //create & populate maps to hold detail
    Map < String, String > BTLB_Name = new Map < String, String > ();
    Map < String, String > BTLB_AccountOwner = new Map < String, String > ();
    Map < String, String > BTLB_AccountOwnerDiv = new Map < String, String > ();
    Map < String, String > BTLB_AccountOwnerDept = new Map < String, String > ();
    Map < String, String > BTLB_SScode = new Map < String, String > ();
    Map < String, String > BTLB_LOBcode = new Map < String, String > ();
    Map < String, String > BTLB_LOBname = new Map < String, String > ();
    List <User> users = new List<User>();
    List <Id> accOwnerIds = new List<Id>();
    
    List < BTLB_CCAT__c > BTLBDetails = [Select BTLB_name__r.Name, LOB_name__c, Sub_Sector_Code__c, Sub_Sector__c, LOB_Code__C, BTLB_name__r.account_owner__c, BTLB_name__r.account_owner_Division__c, BTLB_name__r.account_owner_department__c from BTLB_CCAT__c where Sub_Sector__c in : SubSectorList];//];
   system.debug('divya'+BTLBDetails);
    for (BTLB_CCAT__c d: BTLBDetails) {
        BTLB_Name.put(d.Sub_Sector__c, d.BTLB_name__r.Name);
        BTLB_AccountOwner.put(d.Sub_Sector__c, d.BTLB_name__r.account_owner__c);
        system.debug('divya'+d.BTLB_name__r.account_owner__c);
        system.debug('divya2'+d.Sub_Sector__c);
        BTLB_AccountOwnerDiv.put(d.Sub_Sector__c, d.BTLB_name__r.account_owner_Division__c);
        BTLB_AccountOwnerDept.put(d.Sub_Sector__c, d.BTLB_name__r.account_owner_Department__c);
        BTLB_SScode.put(d.Sub_Sector__c, d.Sub_Sector_Code__c);
        BTLB_LOBcode.put(d.Sub_Sector__c, d.LOB_Code__C);
        BTLB_LOBname.put(d.Sub_Sector__c, d.LOB_name__C);
    }
    // Get Marketing SACs details
    set < string > SACList = new Set < string > (); //get all associated SACs in batch
    for (Account a: Trigger.new) {
        SACList.add(a.SAC_code__c);

    }
    //create map to hold Those SAC that are Marketing SACs
    set < string > BTLB_MarketSac = new Set < string > ();
    // read in values
    List < BTLB_Market_SAC__c > MarketDetails = [Select SAC_Code__C from BTLB_Market_SAC__c where SAC_Code__C in : SACList];
    for (BTLB_Market_SAC__c m: MarketDetails) {
        BTLB_MarketSac.add(m.SAC_Code__C);
    }

    ///////////Corporate Account Load Info Gathering ////////////////////
    // get list of people's eins if shown as managers on upsert records
    SET < String > AccMgrList = new Set < String > ();
    // add default managers
    AccMgrList.add('defBTLB');
    AccMgrList.add('defCORP');
    AccMgrList.add('defOTHER');
    AccMgrList.add('defNI');
    AccMgrList.add('defMPS');
    AccMgrList.add('RoIdefNI'); //ROI Default NI Business
    AccMgrList.add('RoIdefWS'); //ROI Default Wholesale User 
    AccMgrList.add('RoIdafBUS'); //ROI Default Business User
  AccMgrList.add('defGS'); //Global Services Default User

    //add corp user managers to lookup list
    for (Account a: Trigger.new) {
        system.debug('debuggingacount' + a);
        if (a.AM_ein__c != null) {
            AccMgrList.add(a.AM_ein__c);
            system.debug('ADJ AccMgrList' + a.AM_EIN__c);
        }
    }
    //create list of ids of managers
    Map < String, String > AccMgrId = new Map < String, String > ();
    Map < String, String > AccMgrDiv = new Map < String, String > ();
    Map < String, String > AccMgrDept = new Map < String, String > ();

    for (User ui: [Select Id, EIN__c, Division, Department from User where EIN__c in : AccMgrList and isActive = true]) {
    system.debug('Anusha');
        AccMgrId.put(ui.ein__c, ui.Id); //populate map
        AccMgrDiv.put(ui.ein__c, ui.Division);
        AccMgrDept.put(ui.ein__c, ui.Department);

        system.debug('ADJ AccMgrId' + ui.Id);
    }

    /*
   List <BTLB_Postcode__c> OutcodeList = [Select Outcode__c, BTLB__r.Name, BTLB__r.account_owner__c, BTLB__r.account_owner_Division__c, BTLB__r.account_owner_department__c from BTLB_Postcode__c WHERE Outcode__c in :OutCodes];

        system.debug('ADJ OutcodeList :' + OutcodeList);

        for (BTLB_Postcode__c oc :OutcodeList) {
        ocBTLB_Name.put(oc.Outcode__c, oc.BTLB__r.Name);
        ocBTLB_AccountOwner.put(oc.Outcode__c,oc.BTLB__r.account_owner__c);
        ocBTLB_AccountOwnerDiv.put(oc.Outcode__c,oc.BTLB__r.account_owner_Division__c);
        ocBTLB_AccountOwnerDept.put(oc.Outcode__c,oc.BTLB__r.account_owner_Department__c);
        }

  */

    ////loop throught accounts in Batch/////
        
    for (Account a: Trigger.new) {
        // BTLB Accounts (Customers)
        if ((a.sector__c == 'BT Local Business' || a.sector__c == 'BPS') && (a.Base_Team_Assign_Date__c < LockDate || a.Base_Team_Assign_Date__c == null)) {
            a.BTLB_Common_Name__c = BTLB_Name.get(a.sub_sector__c);
            a.Sector_Code__c = 'BTLB';
            if (BTLB_SScode.containsKey(a.sub_sector__c) <> false) {
                a.Sub_Sector_Code__c = BTLB_SScode.get(a.sub_sector__c);
            }
            if (BTLB_Lobname.containsKey(a.sub_sector__c) <> false) {
                a.LOB__c = BTLB_LOBname.get(a.sub_sector__c);
            }
            if (BTLB_LOBcode.containsKey(a.sub_sector__c) <> false) {
                a.LOB_Code__c = BTLB_LOBcode.get(a.sub_sector__c);
            }
            if (BTLB_AccountOwnerDiv.containsKey(a.sub_sector__c) <> false) {
                a.Account_Owner_Division__c = BTLB_AccountOwnerDiv.get(a.sub_sector__c);
            }
            if (BTLB_AccountOwnerDept.containsKey(a.sub_sector__c) <> false) {
                a.Account_Owner_Department__c = BTLB_AccountOwnerDept.get(a.sub_sector__c);
            }
            system.debug('****************'+BTLB_AccountOwner+'--------     '+BTLB_AccountOwner.containsKey(a.sub_sector__c));
            if (BTLB_AccountOwner.containsKey(a.sub_sector__c) <> false) {
            system.debug('harish1'+a.ownerid);
                           a.OwnerId = BTLB_AccountOwner.get(a.sub_sector__c);
                                       system.debug('harish2'+a.ownerid);
            } else {
                        system.debug('divya3'+a.ownerid);
                a.OwnerId = AccMgrId.get('defBTLB'); // replace with custome settings
                                        system.debug('divya4'+a.ownerid);
            }

            if (BTLB_MarketSac.contains(a.SAC_Code__c) == TRUE) { // only if Account is in BTLB Owning SACs
                a.Marketing_SAC__c = True;
                If(a.eSnap_LE_Name__c != null){
                a.Name = a.eSnap_LE_Name__c;
            }

            } else {
                a.Marketing_SAC__c = False;
                If(a.eSnap_SAC_Name__c != null){
                a.Name = a.eSnap_SAC_Name__c;
                }
                
            }

            if (trigger.isupdate) {
                if (a.BTLB_Common_Name__c != trigger.oldMap.get(a.id).BTLB_Common_Name__c) { // added trigger.isupdate so on fire on existing accounts
                    //BTLBOppTaskUpdateOwners.add(a.id); // commented out to allow BTLB name change
                }
            }
        }
        /////  Corporate Account Load  /////////////////////
//JMM ALBION START ########################################################################        
        else if ((a.sector__c == 'BTB Corporate'||a.sector__c == 'Corporate'||a.sector__c == 'Major and Public Sector') && (a.Base_Team_Assign_Date__c < LockDate || a.Base_Team_Assign_Date__c == null)) {
//JMM ALBION END ######################################################################## 
            if (AccMgrId.containsKey(a.am_ein__c) <> false) {
                system.debug('Am IN');
                a.OwnerId = AccMgrId.get(a.am_ein__c);
                system.debug('Phani Owner Id :' + a.OwnerId);
                a.account_owner_division__c = AccMgrDiv.get(a.am_ein__c);
                a.account_owner_department__c = AccMgrDept.get(a.am_ein__c);
                system.debug('ADJ set Corp OwnerId' + AccMgrId.get(a.am_ein__c));

                /* CR3601 - Created 3 test fields & inserting manager hierarchy */
                // SELECT Id, Name, Manager.Name, Manager.Manager.Name, Manager.Manager.Manager.Name FROM User WHERE Id = '005200000026ouA'
                accOwnerIds.add(a.OwnerId);
                
            } else if(a.sector__c == 'Major and Public Sector') {
                a.OwnerId = AccMgrId.get('defMPS');
                a.account_owner_division__c = AccMgrDiv.get('defMPS');
                a.account_owner_department__c = AccMgrDept.get('defMPS');
            } else {
                a.OwnerId = AccMgrId.get('defCORP');
                a.account_owner_division__c = AccMgrDiv.get('defCORP');
                a.account_owner_department__c = AccMgrDept.get('defCORP');
            }
        }

        /////  NI Account Load  /////////////////////
        else if (a.sector__c == 'NI Enterprise' || a.sector__c == 'NI DBAM' || a.sector__c == 'NI Strategic & Key' || a.sector__c == 'NI Microbusiness' ||
                a.sector__c == 'NI Major Business' || a.sector__c == 'Government North' || a.sector__c == 'Finance and Key') {
            if (AccMgrId.containsKey(a.am_ein__c) <> false) {
                a.OwnerId = AccMgrId.get(a.am_ein__c);
                a.account_owner_division__c = AccMgrDiv.get(a.am_ein__c);
                a.account_owner_department__c = AccMgrDept.get(a.am_ein__c);
                system.debug('ADJ set NI OwnerId' + AccMgrId.get(a.am_ein__c));
            } else {
                a.OwnerId = AccMgrId.get('defNI');
                a.account_owner_division__c = AccMgrDiv.get('defNI');
                a.account_owner_department__c = AccMgrDept.get('defNI');
            }
        }

        /////  ROI Account Load  /////////////////////
        else if (a.sector__c == 'ROI Business' || a.sector__c == 'ROI Wholesale' || a.sector__c == 'NI Business') {
            if (AccMgrId.containsKey(a.am_ein__c) <> false) {
                a.OwnerId = AccMgrId.get(a.am_ein__c);
                a.account_owner_division__c = AccMgrDiv.get(a.am_ein__c);
                a.account_owner_department__c = AccMgrDept.get(a.am_ein__c);
            } else if(a.sector__c == 'ROI Business') {
                a.OwnerId = AccMgrId.get('RoIdafBUS');
                a.account_owner_division__c = AccMgrDiv.get('RoIdafBUS');
                a.account_owner_department__c = AccMgrDept.get('RoIdafBUS');    
            } else if(a.sector__c == 'ROI Wholesale') {
                a.OwnerId = AccMgrId.get('RoIdefWS');
                a.account_owner_division__c = AccMgrDiv.get('RoIdefWS');
                a.account_owner_department__c = AccMgrDept.get('RoIdefWS');
            } else {
                a.OwnerId = AccMgrId.get('RoIdefNI');
                a.account_owner_division__c = AccMgrDiv.get('RoIdefNI');
                a.account_owner_department__c = AccMgrDept.get('RoIdefNI');
            }
        }

    /////  Global Services Account Load  /////////////////////
        else if (a.sector__c == 'Global Services') {
            if (AccMgrId.containsKey(a.am_ein__c) <> false) {
                a.OwnerId = AccMgrId.get(a.am_ein__c);
                a.account_owner_division__c = AccMgrDiv.get(a.am_ein__c);
                a.account_owner_department__c = AccMgrDept.get(a.am_ein__c);
            } else {
                a.OwnerId = AccMgrId.get('defGS');
                a.account_owner_division__c = AccMgrDiv.get('defGS');
                a.account_owner_department__c = AccMgrDept.get('defGS');
            }
    }

        /// IF not BTLB or Corporate Account allocate to Bulk owner/////
        else if (a.Base_Team_Assign_Date__c < LockDate || a.Base_Team_Assign_Date__c == null) {
            a.OwnerId = AccMgrId.get('defOTHER');
            a.account_owner_division__c = AccMgrDiv.get('defOTHER');
            a.account_owner_department__c = AccMgrDept.get('defOTHER');
        }
    }
    
    if(!accOwnerIds.isEmpty()){
        users.addAll([SELECT Id, Name, Manager.Name, Manager.Manager.Name, Manager.Manager.Manager.Name FROM User WHERE Id IN :accOwnerIds]);
    }
    
    if (BTLBOppTaskUpdateOwners.size() > 0) {
        //      AccountUpdateMerge.updateAccOpps(BTLBOppTaskUpdateOwners); // comment out is opportunities are not to be moved
        //      AccountUpdateMerge.updateAccTasks(BTLBOppTaskUpdateOwners);  /// dont move tasks 23 Sep
    }
    //}

    //Always run this bit of code to ATTEMPT to allocate Accounts by PostCode
    // get list of outcode associated with accounts being loaded
    set < string > OutCodes = new Set < string > ();
    for (Account a: Trigger.new) {
        system.debug('Am Owner: '+a.OwnerID);
        system.debug('ADJ PostCode2:' + a.postcode__c);

        if (a.postcode__c != null) {
            string postcode = a.postcode__c;
            postcode = postcode.toUpperCase();
            postcode = postcode.split(' ')[0];
            system.debug('ADJ split :' + postcode);
            OutCodes.add(postcode);

        }
        
        /**for(user u : users){
            if(u.Id == a.OwnerId){
                system.debug('Phani Test: '+u.Id);
                //a.Account_SM_Name_Trigger__c = u.Manager.Name;
                //system.debug('*-* SM Name: '+u.Manager.Name);
                //a.Account_DGM_Name_Trigger__c = u.Manager.Manager.Name;
                //system.debug('*-* DGM Name: '+u.Manager.Manager.Name);
                //a.Account_GM_Name_Trigger__c = u.Manager.Manager.Manager.Name;
                //system.debug('*-* GM Name: '+u.Manager.Manager.Manager.Name);
            }           
        } */       
    }

    // if there are outCodes present then get BTLB info for these OutCodes
    // build arrays to store info relating to postcode
    if (OutCodes.size() > 0) { //code set to not run it testing completed ( changed from zero!!)
        Map < String, String > ocBTLB_Name = new Map < String, String > ();
        Map < String, String > ocBTLB_AccountOwner = new Map < String, ID > ();
        Map < String, String > ocBTLB_AccountOwnerDiv = new Map < String, String > ();
        Map < String, String > ocBTLB_AccountOwnerDept = new Map < String, String > ();

        List < BTLB_Postcode__c > OutcodeList = [Select Outcode__c, BTLB__r.Name, BTLB__r.account_owner__c, BTLB__r.account_owner_Division__c, BTLB__r.account_owner_department__c from BTLB_Postcode__c WHERE Outcode__c in : OutCodes];

        system.debug('ADJ OutcodeList :' + OutcodeList);

        for (BTLB_Postcode__c oc: OutcodeList) {
            ocBTLB_Name.put(oc.Outcode__c, oc.BTLB__r.Name);
            ocBTLB_AccountOwner.put(oc.Outcode__c, oc.BTLB__r.account_owner__c);
            ocBTLB_AccountOwnerDiv.put(oc.Outcode__c, oc.BTLB__r.account_owner_Division__c);
            ocBTLB_AccountOwnerDept.put(oc.Outcode__c, oc.BTLB__r.account_owner_Department__c);
        }




        for (Account a: Trigger.new) {
        // ALBION START ########################################################################   
            if (a.sector__c != 'BT Local Business' && a.sector__c != 'BTB Corporate' && a.sector__c != 'Corporate' && a.sector__c != 'Major and Public Sector' && a.postcode__c != null) { //only allocat if not already allocated to  BTLB or CORP
        // ALBION END ########################################################################   
                string postcode2 = a.postcode__c;
                postcode2 = postcode2.toUpperCase();
                postcode2 = postcode2.split(' ')[0];
                system.debug('ADJ split2 :' + postcode2);
                a.BTLB_Common_Name__c = ocBTLB_Name.get(postcode2);
                system.debug('ADJ BTLB_Common_Name__c :' + ocBTLB_Name.get(postcode2));
                if (a.Ownership_assigned_by_Contact__c == true && (a.Base_Team_Assign_Date__c < LockDate || a.Base_Team_Assign_Date__c == null)) { // only assign ownership if base team have marked record or postcode updated from Contact being created

                    if (ocBTLB_AccountOwnerDiv.containsKey(postcode2) <> false) {
                        a.Account_Owner_Division__c = ocBTLB_AccountOwnerDiv.get(postcode2);
                    }
                    if (ocBTLB_AccountOwnerDept.containsKey(postcode2) <> false) {
                        a.Account_Owner_Department__c = ocBTLB_AccountOwnerDept.get(postcode2);
                    }
                    if (ocBTLB_AccountOwner.containsKey(postcode2) <> false) {
                        a.OwnerId = ocBTLB_AccountOwner.get(postcode2);
                    }
                }

            }
        }
    }
}