trigger tgrUpdateRevenueSummary on Product_Revenue__c (after update) {
 /**********************************************************************
     Name:  tgrUpdateRevenueSummary.trigger()
     Copyright © 2010  BT.
     ======================================================
    ======================================================
    Purpose :                                                            
    ----------       
    This trigger is used recalculate the revenue summary records
    when a product revenue record is updated (i.e Manual Override).
    It simple calls a function already used by the tgrCreateSalesJourneyandNIBR_NIER trigger to do the refresh.
    
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
    1.0 -    Greg Scarrtott    15/04/2010        INITIAL DEVELOPMENT 		            Initial Build: 
    ***********************************************************************/
	
	private NIBRandNIERCalculator NandN = new NIBRandNIERCalculator();
    
    for(Product_Revenue__c prodrev : trigger.new){
           	NandN.refreshrevenuesummary(prodrev.opportunity__c);
    }
}