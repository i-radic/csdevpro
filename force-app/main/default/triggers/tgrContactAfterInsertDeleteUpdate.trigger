trigger tgrContactAfterInsertDeleteUpdate on Contact (after delete, after insert, after undelete, after update) {
/**********************************************************************
    Purpose 1:                                                            
    ----------       
    
    To count all contacts against a customer
                                                                                                                          
    ======================================================
    ======================================================
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL
    1.0 -    John McGovern     09/11/2010        1st Draft
    1.1 temp removed after update ,after update)
    1.2      John McGovern     24/07/2012        Added lines for Campaign member deletion to bypass this trigger.
  */
  
    if(TriggerDeactivating__c.getInstance().Contact__c) {
         System.debug('Bypassing trigger due to custom setting');
         return;
     } 
    
     //this var is set by the campaign member deletion trigger to ensure script limit is not reached. Not anything to do with Tasks as such.
     if (StaticVariables.getTaskIsBeforeRun()){
    return ;
    }
    
    Contact[] cons;
    if (Trigger.isDelete)
        cons = Trigger.old;
    else
        cons = Trigger.new;

    // get list of accounts
    Set<ID> acctIds = new Set<ID>();
    for (Contact con : cons) {
            if(con.AccountID != Null){
                acctIds.add(con.AccountId);
            }
    }
   
    Map<ID, Contact> contactsForAccounts = new Map<ID, Contact>([select Id, AccountId FROM Contact WHERE AccountId in :acctIds AND Status__c = 'Active' AND FirstName <> 'BT DEFAULT']);

    Map<ID, Account> acctsToUpdate = new Map<ID, Account>([select Id, Count_of_Contacts__c FROM Account WHERE Id in :acctIds]);
                                                                
    for (Account acct : acctsToUpdate.values()) {
        Set<ID> conIds = new Set<ID>();
        for (Contact con : contactsForAccounts.values()) {
            if (con.AccountId == acct.Id)
                conIds.add(con.Id);
        }
        if (acct.Count_of_Contacts__c != conIds.size())
            // These lines should be added when bulk updating contacts to reduce Script limit errors.
            //StaticVariables.setAccountBeforeDontRun(true);
            //StaticVariables.setAccountAfterDontRun(true);        
            //StaticVariables.setcontactBeforeTriggerRun(true);
            acct.Count_of_Contacts__c = conIds.size();
    }

    update acctsToUpdate.values();

}