trigger ProjectTrigger on Project__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	final ProjectHandler handler = ProjectHandler.getHandler();
	
	if(Trigger.isAfter && Trigger.isUpdate) { handler.afterUpdate(); return; }
}