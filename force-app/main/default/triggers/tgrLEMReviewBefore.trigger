/*
Client      : BT
Author      : Krupakar J
Date        : 17/05/2012 (dd/mm/yyyy)
Description : This trigger will handle before trigger logic for LEM Reviews.
*/

trigger tgrLEMReviewBefore on LEM_Review__c (before insert, before update) {
	
	if(trigger.isBefore && trigger.isInsert) {
		LEMReviewBeforeHandler triggerHandler = new LEMReviewBeforeHandler();
		triggerHandler.handleLEMReviewBeforeInserts(trigger.new);
	}
	
	if(trigger.isBefore && trigger.isUpdate) {
		LEMReviewBeforeHandler triggerHandler = new LEMReviewBeforeHandler();
		triggerHandler.handleLEMReviewBeforeUpdates(trigger.new, trigger.old);
	}
}