trigger GenieTagBefore on Genie_Tag__c (before insert, before update) {
    for (Genie_Tag__c newTag : trigger.new) {
        newTag.Upload_Date__c = system.Now();
    }
}