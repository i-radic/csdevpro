//  Date            Author          Comment
//  06 May 2011     Dan Measures    CR3931 - For Tasks that have record type of Corporate Campaign Task or Deluxe Survey Review prevent deletion for end users
//  14 JUN 2011     Raj             Tasks on Book2Bill records shouldn't be deleted by Sales specialists, Managers & Win Back 
trigger TaskBeforeDelete on Task (before delete) {
    if(TriggerDeactivating__c.getInstance().Task__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
     for(Task t : trigger.old){
        if (t.OkToDelete__c != 'ok to delete') {
            t.addError('You do not have an appropriate profile to delete this record, please contact a System Administrator!');
        }
    }

//Below code added by Raj
 Map<String,String> MapAdminProfiles=new Map<String,String>();   
 List<Profile> Admin=[Select Id,Name From Profile where Name in ('System Administrator','BookToBill Team','BT: Dataloader2')];
    For(Profile P:Admin)
    MapAdminProfiles.put(P.Id,P.Name);
    
  String BooktoBill_prefix = Schema.SObjectType.BookToBill__c.getKeyPrefix();
     
         for(Task T : Trigger.old){
            String P=MapAdminProfiles.get(UserInfo.getProfileId());
            if((P==null)&&((String)T.WhatId).startsWith(BooktoBill_prefix))
               T.addError('You are not allowed to delete this task');

         }
     
}