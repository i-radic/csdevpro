trigger trgAfterContract on Contract (after insert, after update) {
//  Date            Author          Comment
//  21 Sept 2010    JBrayshaw       Initial development for merge functionality
//                                  Need to set on associated account if contract is active
//
// Test Class Test_trgAfterContract
    Map<id, Account> updateAccounts = new Map<Id, Account>{};
    List<id> contractIds = new List<Id>();
    
    for(Contract thisContract : Trigger.New){
        contractIds.add(thisContract.id);
        if(thisContract.AccountId != null){
            Account acc = new Account(Id=thisContract.AccountId);
            //if(thisContract.Adder_Status__c == 'Active')
                //acc.Has_Active_Contract__c = true;
            //else
                //acc.Has_Active_Contract__c = false;
                
            Account inListAcc = updateAccounts.get(thisContract.AccountId);
            
            if(inListAcc == null)
                updateAccounts.put(thisContract.AccountId, acc);
            else {
              //if(acc.Has_Active_Contract__c != inListAcc.Has_Active_Contract__c){
                //acc.Has_Active_Contract__c = true;
                updateAccounts.put(thisContract.AccountId, acc);
             // }
            }
        }
    }

    for(list<Contract> theseContracts : [Select id, Adder_Status__c, AccountId From Contract 
                                        where accountid in :updateAccounts.KeySet() 
                                        and id not in :contractIds]){
        for(Contract thisContract : theseContracts){
            
          if(thisContract.AccountId != null){
            Account acc = new Account(Id=thisContract.AccountId);
           // if(thisContract.Adder_Status__c == 'Active')
               // acc.Has_Active_Contract__c = true;
            //else
               // acc.Has_Active_Contract__c = false;
                
            Account inListAcc = updateAccounts.get(thisContract.AccountId);
            
            if(inListAcc == null)
                updateAccounts.put(thisContract.AccountId, acc);
           /** else {
              if(acc.Has_Active_Contract__c != inListAcc.Has_Active_Contract__c){
                acc.Has_Active_Contract__c = true;
                updateAccounts.put(thisContract.AccountId, acc);
              }
            }*/
          } 
        }
    }
    
    update updateAccounts.values();

}