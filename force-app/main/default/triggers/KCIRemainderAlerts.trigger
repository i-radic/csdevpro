trigger KCIRemainderAlerts on KCI__c (after insert,after update) {
    
    Datetime dt2,dt2M;
    
    KCI__c[] kci = Trigger.new;
    KCI__c[] kciOld = trigger.old; 
    
    Id loggedinUser = UserInfo.getUserId();
    
    datetime dt1 = kci[0].CreatedDate;
    Date d = Date.newInstance(dt1.year(),dt1.month(),dt1.day());
    dt1 = dt1.addHours(2);  
    //dt2 = dt1.addHours(4);  
            
    datetime dt1M = kci[0].LastModifiedDate;
    Date dM = Date.newInstance(dt1M.year(),dt1M.month(),dt1M.day());
    dt1M = dt1M.addHours(2);         
    //dt2M = dt1M.addHours(4);
            
    if(Trigger.isInsert){     
        List<KCI__c> kciList = [Select Name from KCI__c where Name!=null AND Name=:kci[0].Name ORDER BY CreatedDate Desc LIMIT 4];
        if(kciList.size()>1){//duplicate exists. The current record is also included in the list.
            if(kciList.size()==2){//1 duplicate
                //CAT
                //kci[0].addError(kciList.size()-1+' KCI with the same VOL Ref already exists:<a href=\'https://cs2.salesforce.com/' + kciList[1].Id + '\'>' + kciList[1].Name + '</a>');               
                
                //LIVE
                kci[0].addError(kciList.size()-1+' KCI with the same VOL Ref already exists:<a href=\'https://emea.salesforce.com/' + kciList[1].Id + '\'>' + kciList[1].Name + '</a>');               
            }
            else if(kciList.size()==3){//2 duplicates
                //CAT
                //kci[0].addError(kciList.size()-1+' KCI with the same VOL Ref already exist:<a href=\'https://cs2.salesforce.com/' + kciList[1].Id + '\'>' + kciList[1].Name + '</a>' + '<a href=\'https://cs2.salesforce.com/' + kciList[2].Id + '\'>' + kciList[2].Name + '</a>');               
                
                //LIVE
                kci[0].addError(kciList.size()-1+' KCI with the same VOL Ref already exist:<a href=\'https://emea.salesforce.com/' + kciList[1].Id + '\'>' + kciList[1].Name + '</a>' + '<a href=\'https://emea.salesforce.com/' + kciList[2].Id + '\'>' + kciList[2].Name + '</a>');               
            }
            else if(kciList.size()==4){//3 duplicates
                //CAT
                //kci[0].addError(kciList.size()-1+' KCI with the same VOL Ref already exist:<a href=\'https://cs2.salesforce.com/' + kciList[1].Id + '\'>' + kciList[1].Name + '</a>' + '<a href=\'https://cs2.salesforce.com/' + kciList[2].Id + '\'>' + kciList[2].Name + '</a>' + '<a href=\'https://cs2.salesforce.com/' + kciList[3].Id + '\'>' + kciList[3].Name + '</a>');               
                
                //LIVE
                kci[0].addError(kciList.size()-1+' KCI with the same VOL Ref already exist:<a href=\'https://emea.salesforce.com/' + kciList[1].Id + '\'>' + kciList[1].Name + '</a>' + '<a href=\'https://emea.salesforce.com/' + kciList[2].Id + '\'>' + kciList[2].Name + '</a>' + '<a href=\'https://emea.salesforce.com/' + kciList[3].Id + '\'>' + kciList[3].Name + '</a>');
            }
        }   
        if(kci[0].Validation_Call_KCI_1_Attempt_1__c=='Not Successful'){
            Task t = new Task();
            t.ownerId=loggedinUser;  
            t.WhatId=kci[0].Id;
            t.subject='KCI Validation Call Attempt 1 Remainder';
            t.ActivityDate=d;
            t.IsReminderSet=true;
            t.ReminderDateTime=dt1;
            insert t; 
        
        }
        if(kci[0].Validation_Call_KCI_1_Attempt_2__c=='Not Successful'){
            Task t2 = new Task();
            t2.ownerId=loggedinUser;  
            t2.WhatId=kci[0].Id;
            t2.subject='KCI Validation Call Attempt 2 Remainder';
            t2.ActivityDate=d;
            t2.IsReminderSet=true;
            t2.ReminderDateTime=dt1;
            insert t2; 
        
        }
        if(kci[0].Confirmation_Call_KCI_1_Attempt_1__c=='Not Successful'){
            Task t11 = new Task();
            t11.ownerId=loggedinUser;  
            t11.WhatId=kci[0].Id;
            t11.subject='KCI Confirmation Call Attempt 1 Remainder';
            t11.ActivityDate=d;
            t11.IsReminderSet=true;
            t11.ReminderDateTime=dt1;
            insert t11; 
        
        }
        if(kci[0].Confirmation_Call_KCI_1_Attempt_2__c=='Not Successful'){
            Task t21 = new Task();
            t21.ownerId=loggedinUser;  
            t21.WhatId=kci[0].Id;
            t21.subject='KCI Confirmation Call Attempt 2 Remainder';
            t21.ActivityDate=d;
            t21.IsReminderSet=true;
            t21.ReminderDateTime=dt1;
            insert t21; 
        
        }
        
        if(kci[0].Validation_Call_KCI_1_Attempt_1__c=='Call Back Promised'){
            if(kci[0].Validation_Call_Back_Date_1__c!=null){
                Task t3 = new Task();
                t3.ownerId=loggedinUser;  
                t3.WhatId=kci[0].Id;
                t3.subject='KCI Validation Call Attempt 1 Call Back Promised';
                t3.ActivityDate=d;
                t3.IsReminderSet=true;
                t3.ReminderDateTime=kci[0].Validation_Call_Back_Date_1__c;
                insert t3; 
            }
        }
        if(kci[0].Validation_Call_KCI_1_Attempt_2__c=='Call Back Promised'){
            if(kci[0].Validation_Call_Back_Date_2__c!=null){
                Task t4 = new Task();
                t4.ownerId=loggedinUser;  
                t4.WhatId=kci[0].Id;
                t4.subject='KCI Validation Call Attempt 2 Call Back Promised';
                t4.ActivityDate=d;
                t4.IsReminderSet=true;
                t4.ReminderDateTime=kci[0].Validation_Call_Back_Date_2__c;
                insert t4; 
            }
        }
        if(kci[0].Validation_Call_KCI_1_Attempt_3__c=='Call Back Promised'){
            if(kci[0].Validation_Call_Back_Date_3__c!=null){
                Task t5 = new Task();
                t5.ownerId=loggedinUser;  
                t5.WhatId=kci[0].Id;
                t5.subject='KCI Validation Call Attempt 3 Call Back Promised';
                t5.ActivityDate=d;
                t5.IsReminderSet=true;
                t5.ReminderDateTime=kci[0].Validation_Call_Back_Date_3__c;
                insert t5; 
            }
        }
        if(kci[0].Confirmation_Call_KCI_1_Attempt_1__c=='Call Back Promised'){
            if(kci[0].Confirmation_Call_Back_Date_1__c!=null){
                Task t31= new Task();
                t31.ownerId=loggedinUser;  
                t31.WhatId=kci[0].Id;
                t31.subject='KCI Confirmation Call Attempt 1 Call Back Promised';
                t31.ActivityDate=d;
                t31.IsReminderSet=true;
                t31.ReminderDateTime=kci[0].Confirmation_Call_Back_Date_1__c;
                insert t31; 
            }
        }
        if(kci[0].Confirmation_Call_KCI_1_Attempt_2__c=='Call Back Promised'){
            if(kci[0].Confirmation_Call_Back_Date_2__c!=null){
                Task t41= new Task();
                t41.ownerId=loggedinUser;  
                t41.WhatId=kci[0].Id;
                t41.subject='KCI Confirmation Call Attempt 2 Call Back Promised';
                t41.ActivityDate=d;
                t41.IsReminderSet=true;
                t41.ReminderDateTime=kci[0].Confirmation_Call_Back_Date_2__c;
                insert t41; 
            }
        }
        if(kci[0].Confirmation_Call_KCI_1_Attempt_3__c=='Call Back Promised'){
            if(kci[0].Confirmation_Call_Back_Date_3__c!=null){
                Task t51= new Task();
                t51.ownerId=loggedinUser;  
                t51.WhatId=kci[0].Id;
                t51.subject='KCI Confirmation Call Attempt 3 Call Back Promised';
                t51.ActivityDate=d;
                t51.IsReminderSet=true;
                t51.ReminderDateTime=kci[0].Confirmation_Call_Back_Date_3__c;
                insert t51; 
            }
        }
    }
    
    if(Trigger.isUpdate){
        if((kci[0].Validation_Call_KCI_1_Attempt_1__c != kciOld[0].Validation_Call_KCI_1_Attempt_1__c) && kci[0].Validation_Call_KCI_1_Attempt_1__c=='Not Successful'){
                Task t = new Task();
                t.ownerId=loggedinUser;  
                t.WhatId=kci[0].Id;
                t.subject='KCI Validation Call Attempt 1 Remainder';
                t.ActivityDate=dM;
                t.IsReminderSet=true;
                t.ReminderDateTime=dt1M;
                insert t;
        }
        if((kci[0].Validation_Call_KCI_1_Attempt_2__c != kciOld[0].Validation_Call_KCI_1_Attempt_2__c) && kci[0].Validation_Call_KCI_1_Attempt_2__c=='Not Successful'){
                Task t2 = new Task();
                t2.ownerId=loggedinUser;  
                t2.WhatId=kci[0].Id;
                t2.subject='KCI Validation Call Attempt 2 Remainder';
                t2.ActivityDate=dM;
                t2.IsReminderSet=true;
                t2.ReminderDateTime=dt1M;
                insert t2;
        }
        if((kci[0].Validation_Call_KCI_1_Attempt_1__c != kciOld[0].Validation_Call_KCI_1_Attempt_1__c) && kci[0].Validation_Call_KCI_1_Attempt_1__c=='Call Back Promised'){
            if(kci[0].Validation_Call_Back_Date_1__c!=null){
                Task t2v1 = new Task();
                t2v1.ownerId=loggedinUser;  
                t2v1.WhatId=kci[0].Id;
                t2v1.subject='KCI Validation Call Attempt 1 Call Back Promised';
                t2v1.ActivityDate=dM;
                t2v1.IsReminderSet=true;
                t2v1.ReminderDateTime=kci[0].Validation_Call_Back_Date_1__c;
                insert t2v1;
            }
        }
        if((kci[0].Validation_Call_KCI_1_Attempt_2__c != kciOld[0].Validation_Call_KCI_1_Attempt_2__c) && kci[0].Validation_Call_KCI_1_Attempt_2__c=='Call Back Promised'){
            if(kci[0].Validation_Call_Back_Date_2__c!=null){
                Task t2v2 = new Task();
                t2v2.ownerId=loggedinUser;  
                t2v2.WhatId=kci[0].Id;
                t2v2.subject='KCI Validation Call Attempt 2 Call Back Promised';
                t2v2.ActivityDate=dM;
                t2v2.IsReminderSet=true;
                t2v2.ReminderDateTime=kci[0].Validation_Call_Back_Date_2__c;
                insert t2v2;
            }
        }
        if((kci[0].Validation_Call_KCI_1_Attempt_3__c != kciOld[0].Validation_Call_KCI_1_Attempt_3__c) && kci[0].Validation_Call_KCI_1_Attempt_3__c=='Call Back Promised'){
            if(kci[0].Validation_Call_Back_Date_3__c!=null){
                Task t2v3 = new Task();
                t2v3.ownerId=loggedinUser;  
                t2v3.WhatId=kci[0].Id;
                t2v3.subject='KCI Validation Call Attempt 3 Call Back Promised';
                t2v3.ActivityDate=dM;
                t2v3.IsReminderSet=true;
                t2v3.ReminderDateTime=kci[0].Validation_Call_Back_Date_3__c;
                insert t2v3;
            }
        }
        
        
        if((kci[0].Confirmation_Call_KCI_1_Attempt_1__c != kciOld[0].Confirmation_Call_KCI_1_Attempt_1__c) && kci[0].Confirmation_Call_KCI_1_Attempt_1__c=='Not Successful'){
                Task ct = new Task();
                ct.ownerId=loggedinUser;  
                ct.WhatId=kci[0].Id;
                ct.subject='KCI Confirmation Call Attempt 1 Remainder';
                ct.ActivityDate=dM;
                ct.IsReminderSet=true;
                ct.ReminderDateTime=dt1M;
                insert ct;
        }
        if((kci[0].Confirmation_Call_KCI_1_Attempt_2__c != kciOld[0].Confirmation_Call_KCI_1_Attempt_2__c) && kci[0].Confirmation_Call_KCI_1_Attempt_2__c=='Not Successful'){
                Task ct2 = new Task();
                ct2.ownerId=loggedinUser;  
                ct2.WhatId=kci[0].Id;
                ct2.subject='KCI Confirmation Call Attempt 2 Remainder';
                ct2.ActivityDate=dM;
                ct2.IsReminderSet=true;
                ct2.ReminderDateTime=dt1M;
                insert ct2;
        }
        if((kci[0].Confirmation_Call_KCI_1_Attempt_1__c != kciOld[0].Confirmation_Call_KCI_1_Attempt_1__c) && kci[0].Confirmation_Call_KCI_1_Attempt_1__c=='Call Back Promised'){
            if(kci[0].Confirmation_Call_Back_Date_1__c!=null){
                Task ct3= new Task();
                ct3.ownerId=loggedinUser;  
                ct3.WhatId=kci[0].Id;
                ct3.subject='KCI Confirmation Call Attempt 1 Call Back Promised';
                ct3.ActivityDate=dM;
                ct3.IsReminderSet=true;
                ct3.ReminderDateTime=kci[0].Confirmation_Call_Back_Date_1__c;
                insert ct3;
            }
        }
        if((kci[0].Confirmation_Call_KCI_1_Attempt_2__c != kciOld[0].Confirmation_Call_KCI_1_Attempt_2__c) && kci[0].Confirmation_Call_KCI_1_Attempt_2__c=='Call Back Promised'){
            if(kci[0].Confirmation_Call_Back_Date_2__c!=null){
                Task ct4= new Task();
                ct4.ownerId=loggedinUser;  
                ct4.WhatId=kci[0].Id;
                ct4.subject='KCI Confirmation Call Attempt 2 Call Back Promised';
                ct4.ActivityDate=dM;
                ct4.IsReminderSet=true;
                ct4.ReminderDateTime=kci[0].Confirmation_Call_Back_Date_2__c;
                insert ct4;
            }
        }
        if((kci[0].Confirmation_Call_KCI_1_Attempt_3__c != kciOld[0].Confirmation_Call_KCI_1_Attempt_3__c) && kci[0].Confirmation_Call_KCI_1_Attempt_3__c=='Call Back Promised'){
            if(kci[0].Confirmation_Call_Back_Date_3__c!=null){
                Task ct5= new Task();
                ct5.ownerId=loggedinUser;  
                ct5.WhatId=kci[0].Id;
                ct5.subject='KCI Confirmation Call Attempt 3 Call Back Promised';
                ct5.ActivityDate=dM;
                ct5.IsReminderSet=true;
                ct5.ReminderDateTime=kci[0].Confirmation_Call_Back_Date_3__c;
                insert ct5;
            }
        }

    }
    
    
    
    
    
    
}