trigger IdeaBefore on Idea(before insert, before update, after insert) {
  
    //Set<id> newCR = new Set<id>();
    List < Change_Request__c > newCR = new List < Change_Request__c > ();
    List < string > toAddList;
    String htmlBody, Subject;    
    string BaseURL = ApexPages.currentPage().getHeaders().get('Host');   

    for (Idea i: trigger.new) {

        // assigning status to Idea post
        if(trigger.isBefore  && Trigger.isInsert){          
            i.Status = 'Not Yet Reviewed';
        }           

        if ((i.Promote_to_CR__c == True) && (i.Change_Request__c == Null)) {
            Change_Request__c addID = new Change_Request__c(Description__c = i.Body, Feedback_Title__c = i.Title, Status__c = 'Logged');
            newCR.add(addID);
        }
        
        Database.SaveResult[] MySaveResult = Database.Insert(newCR);
        
        for (Database.SaveResult sr: MySaveResult) {
            if (sr.isSuccess()) {
                i.Change_Request__c = sr.getId();
            }
        }
    }
    
    if (trigger.isAfter) {
        
        for (Idea i: trigger.new) {
            
            // Creates a new feed When new Idea raised
            
            FeedItem fItem = new FeedItem();
            fItem.Type = 'TextPost';
            fItem.Body = 'Just raised a new idea on: ' + i.Title + '\nTo view or to vote on the idea click below link\nhttps://'+BaseURL+'/' + i.Id ;
           
            Subject = 'New idea Submitted: ' + i.Title;
            htmlBody = 'A new idea <b>'+ i.Title +'</b> has been submitted by <b>' + Userinfo.getName() + '</b><br /><br />' +
                       '<b>Description: </b>'+i.Body+'<br /><br /> '+
                'click <a href="https://'+BaseURL+'/' + i.Id + '">here</a> to view the detail.';

            if (i.Categories == 'Business Sales' || i.Categories == 'BTLB' || i.Categories == 'Business Sales; BTLB' || i.Categories == 'BTLB;Business Sales') {
                toAddList = new List < string > ();
                toAddList.add('chris.freeney@bt.com');
                toAddList.add('Nikki.Birch@bt.com');
                toAddList.add(BTUtilities.getUser(i.CreatedById).getEmail);
                // Inserting task to LEM CHatter Central group
                fItem.ParentId = '0F920000000PE37'; // here ID is LEM CHatter Central Group 
                insert fItem;
            } else {
                toAddList = new List < string > ();
                toAddList.add(Userinfo.getUserEmail());
               /** if(i.Categories.Contains('Corporate')){//commented as it was failing in Production the below Parent Id is not Active.
                    fItem.ParentId = '0F920000000PE3q'; // here ID is Corp Desk - Salesforce Group                     
                    insert fItem; 
                }*/
            }
            
            BTUtilities.sendHTMLEmail(toAddList, Subject, htmlBody);
             
        }
    }

    if (trigger.isUpdate) {

        for (Idea i: trigger.new) {                     

            if (trigger.oldMap.get(i.Id).Status == 'Not Yet Reviewed' && i.Status == 'Under Consideration') {

                Subject = 'New idea Updated: ' + i.Title;
                htmlBody = 'A status change has occurred on the following Idea <b>' + i.Title + '</b><br /><br />' +
                    'click <a href="https://'+BaseURL+'/' + i.Id + '">here</a> to view the detail.';

                if (i.Categories == 'Business Sales' || i.Categories == 'BTLB' || i.Categories == 'Business Sales; BTLB' || i.Categories == 'BTLB;Business Sales') {
                    toAddList = new List < string > ();
                    toAddList.add('chris.freeney@bt.com');
                    toAddList.add('Nikki.Birch@bt.com');
                    toAddList.add(BTUtilities.getUser(i.CreatedById).getEmail);
                    
                } else {
                    toAddList = new List < string > ();
                    toAddList.add(Userinfo.getUserEmail());
                }

                BTUtilities.sendHTMLEmail(toAddList, Subject, htmlBody);

            } else if (i.Status == 'Not Feasible At This Time' || i.Status == 'Delivered') {

                Subject = 'New idea Closed: ' + i.Title;
                htmlBody = 'The Idea <b>' + i.Title + '</b> has now been closed. <br /><br />' +
                    'click <a href="https://'+BaseURL+'/' + i.Id + '">here</a> to view the detail.';

                if (i.Categories == 'Business Sales' || i.Categories == 'BTLB' || i.Categories == 'Business Sales; BTLB' || i.Categories == 'BTLB;Business Sales') {
                    toAddList = new List < string > ();
                    toAddList.add('chris.freeney@bt.com');
                    toAddList.add('Nikki.Birch@bt.com');
                    toAddList.add(BTUtilities.getUser(i.CreatedById).getEmail);
                } else {
                    toAddList = new List < string > ();
                    toAddList.add(Userinfo.getUserEmail());
                }

                BTUtilities.sendHTMLEmail(toAddList, Subject, htmlBody);
            }
        }
    }
}