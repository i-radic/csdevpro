trigger CS_AllUsageProfileTriggers on Usage_Profile__c (after delete, after insert, after undelete, 
	after update, before delete, before insert, before update) {
		
		CS_TriggerHandler.execute(new CS_UsageProfileTriggerDelegate());
		
}