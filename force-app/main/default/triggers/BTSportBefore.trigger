trigger BTSportBefore on BT_Sport__c (before insert, before update) {
    Date TodaysDate = Date.today();
    public String roomsonly = null;
    public String roomsPmfLookupKey = null;
    public String PriceType = null;
    public String Band = null;
    public String var_site_Type = null;
    set<string> PMFUnique = new Set<string>();
    set<string> HeaderId = new Set<string>();
    
    for (BT_Sport__c t:Trigger.new) {
        if(t.PMF_Effective_Date__c != null){
            TodaysDate = t.PMF_Effective_Date__c;
        }
        
        if (t.BT_Sport_Header__c != null) {//only run if non msa (or msa)  
            system.debug('ADJ SITE ID = '+ t.id);     
            
            // read pricing table to choice pricing type ie single, with contract , with contract & band
            
            //Check for Hero Discounts
            var_site_Type = t.Site_Type__c; // set default 
            If (t.Discounts__c <> null) { // required else .contains fails on null fields
                If (t.Discounts__c.contains('Hero')){
                    
                    // Band override for Hero Pubs 
                    if (t.Site_Type__c == 'BT Sport Pack Pub (GB)'){
                        var_site_Type = 'BT Sport Pack Pub (GB) - Hero';
                    }
                    else if (t.Site_Type__c == 'BT Sport Pack Pub (NI)'){
                        var_site_Type = 'BT Sport Pack Pub (NI) - Hero';
                    }
                    // change site type form GB to NI to give discount for Hero
                    else if (t.Site_Type__c == 'BT Sport Pack Clubs (GB)' ){ 
                        /* Heor offer change from flipping to NI to moving down price band
var_site_Type = 'BT Sport Pack Clubs (NI)';
t.Site_Type__c = 'BT Sport Pack Clubs (NI)';
*/
                        var_site_Type = 'BT Sport Pack Clubs (GB) - Hero';
                    }
                    else if (t.Site_Type__c == 'BT Sport Pack Clubs (NI)' ){ 
                        var_site_Type = 'BT Sport Pack Clubs (NI) - Hero';
                    }    
                    
                    // Band over ride for Golf Clubs for Hero
                    else if (t.Site_Type__c == 'BT Sport Pack Golf Club (GB)'){
                        var_site_Type = 'BT Sport Pack Golf Club (GB) - Hero';
                    }
                    else if (t.Site_Type__c == 'BT Sport Pack Golf Club (NI)'){
                        var_site_Type = 'BT Sport Pack Golf Club (NI) - Hero';
                    }
                }
            }
            
            // Override contract type for 3 months free
            If (t.Discounts__c <> null) {
                if (t.Discounts__c.contains('3 months free')){
                    t.Contract_Type__c = '12 month contract with Summer 2014 FREE';
                }}
            system.debug('ADJ var_site_Type = '+ var_site_Type);  
            system.debug('ADJ t.Site_Type__c = '+ t.Site_Type__c);
            
            //Get price type           
            List<BT_Sport_Pricing__c> PriceTypeSelect = [Select Site_Type__c, Price_Type__c  from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type Limit 1 ];
            system.debug('ADJ PRICE TYPE = '+ PriceTypeSelect[0].Price_Type__c);   
            
            // set pmflookup key          
            If( PriceTypeSelect[0].Price_Type__c == '1'){ // ie price based purly on site type
                PMFUnique.add(t.Site_Type__c);
                t.PmfLookupKey__c = var_site_Type;
            }  
            else if( PriceTypeSelect[0].Price_Type__c == '2'){  // price based on site & contract (not bands)
                PMFUnique.add(t.Site_Type__c + t.Contract_Type__c);
                t.PmfLookupKey__c = t.Site_Type__c + t.Contract_Type__c;
                system.debug('Price_Type__c = '+ var_site_Type + t.Contract_Type__c);
                system.debug('ADJ BTS Other t.PmfLookupKey__c = '+ t.PmfLookupKey__c);   
            }
            else { 
                if  (PriceTypeSelect[0].Price_Type__c == '3RM' && t.Number_of_Rooms_receiving_BT_Sport__c != Null){  //JMM added not null condition to prevent mis banding
                    List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and Price_Band_Start__c <= :t.Number_of_Rooms_receiving_BT_Sport__c and Price_Band_end__c >= :t.Number_of_Rooms_receiving_BT_Sport__c Limit 1 ];
                    t.Band__c = PriceBandSelect[0].Band__c;
                }                
                else if  (PriceTypeSelect[0].Price_Type__c == '3RV' && t.Rateable_Value__c != Null){ //JMM added not null condition to prevent mis banding
                    List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and  Price_Band_Start__c <= :t.Rateable_Value__c and Price_Band_end__c >= :t.Rateable_Value__c Limit 1 ];
                    t.Band__c = PriceBandSelect[0].Band__c;
                }                
                PMFUnique.add(t.Site_Type__c + t.Contract_Type__c + t.Band__c);
                t.PmfLookupKey__c = t.Site_Type__c + t.Contract_Type__c + t.Band__c;
                roomsPmfLookupKey = 'BT Sport Pack Hotel Rooms' + t.Contract_Type__c;
                
                system.debug('ADJ PMFUnique.add = '+ t.Site_Type__c + t.Contract_Type__c + t.Band__c);
                system.debug('ADJ t.PmfLookupKey__c = '+ t.PmfLookupKey__c);   
                system.debug('ADJ roomsPmfLookupKey = '+ 'BT Sport Pack Hotel Rooms' + t.Contract_Type__c);  
            }
            // lookup using  PmfLookupKey__c the correct price entry    
            set<string> PMFcodes = new Set<string>();
            Map<String, String> PMF_Value = new Map<String, String>();
            List<BT_Sport_Pricing__c> pmfCode = [Select Site_Type__c, PMF_Code__c, PmfLookupKey__c from BT_Sport_Pricing__c where Entry_Type__C = 'PricePoint' and (PmfLookupKey__c in :PMFUnique or PmfLookupKey__c = :roomsPmfLookupKey)];
            for (BT_Sport_Pricing__c pc :pmfCode) {
                PMF_Value.put(pc.PmfLookupKey__c, pc.PMF_Code__c);
                PMFcodes.add(pc.PMF_Code__c);
                if (pc.PmfLookupKey__c == roomsPmfLookupKey){
                    RoomsOnly = pc.PMF_Code__c;
                }
                system.debug('ADJ pc.PmfLookupKey__c '+ pc.PmfLookupKey__c );
                system.debug('ADJ pc.PMF_Code__c = '+ pc.PMF_Code__c);
            }
            Map<String, Decimal> PMF_Price = new Map<String, Decimal>();
            Map<String, Decimal> PMF_Version = new Map<String, Decimal>();
            
            List<BT_Sport_Pricing__c> pmfPrice = [Select PMF_Code__c, Price__c, PMF_Version__c from BT_Sport_Pricing__c where Entry_Type__C = 'PMF' and PMF_Code__c in :PMFcodes and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate  ] ;
            for (BT_Sport_Pricing__c pp :pmfPrice) {
                PMF_Price.put(pp.PMF_Code__c, pp.Price__c);
                PMF_Version.put(pp.PMF_Code__c, pp.PMF_Version__c);
                system.debug('ADJ pp.PMF_Code__c '+ pp.PMF_Code__c );
                system.debug('ADJ pp.Price__c = '+ pp.Price__c);
            }
            
            t.gross_price__c = PMF_Price.get(PMF_Value.get(t.PmfLookupKey__c));
            t.pmf_version__c = PMF_Version.get(PMF_Value.get(t.PmfLookupKey__c));
            t.Discount_Amount__c = 0;
            system.debug('ADJ t.gross_price__c = '+ t.gross_price__c);
            system.debug('ADJ t.price__c = '+ t.price__c);
            system.debug('ADJ t.pmf_version__c = '+ t.pmf_version__c);
            
            
            //Calculate Hotel Rooms only Price
            If(t.Site_Type__c == 'BT Sport Pack Hotel Rooms' && t.Number_of_Rooms_receiving_BT_Sport__c != null){
                //t.gross_price__c = PMF_Price.get(roomsonly) * t.Number_of_Rooms_receiving_BT_Sport__c;
                t.gross_price__c = PMF_Price.get(PMF_Value.get(t.PmfLookupKey__c)) * t.Number_of_Rooms_receiving_BT_Sport__c;
                // Minimum spend on rooms
                if(t.gross_price__c <60){
                    t.gross_price__c =60;
                }
            }
            
            //BT Sport Pack Hotel Bar = add the value of rooms ( if any) to gross price 
            If (t.Site_Type__c == 'BT Sport Pack Hotel Bar' && t.Number_of_Rooms_receiving_BT_Sport__c != null) {
                t.gross_price__c =  t.gross_price__c + (PMF_Price.get(roomsonly) * integer.valueof(t.Number_of_Rooms_receiving_BT_Sport__c)); 
            }
            
            //Discounts - Perishable discount  
            Map<String, Decimal> Discount_Amount = new Map<String, Decimal>();
            Map<String, Decimal> Discount_Period = new Map<String, Decimal>();
            Map<String, String> SiteType = new Map<String, String>();
            Map<String, String> ContractType = new Map<String, string>();
            List<BT_Sport_Pricing__c> Discounts = [Select Discount_Amount__c, Discount_Period__c, Discount_Name__C, UnavailableSiteTypes__c, UnavailableContractTypes__c from BT_Sport_Pricing__c where Entry_Type__C = 'Discount' and Discount_Name__C = :t.Discounts__c and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate  ] ;
            system.debug('ADJ Discounts.size() = '+ Discounts.size());
            
            for (BT_Sport_Pricing__c dc :Discounts) {
                Discount_Amount.put(dc.Discount_Name__C, dc.Discount_Amount__c);
                Discount_Period.put(dc.Discount_Name__C, dc.Discount_Period__c);
                SiteType.put(dc.Discount_Name__C, dc.UnavailableSiteTypes__c);
                ContractType.put(dc.Discount_Name__C, dc.UnavailableContractTypes__c);
                system.debug('ADJ dc.Discount_Amount__c = '+ dc.Discount_Amount__c);
                system.debug('ADJ dc.Discount_Period__c = '+ dc.Discount_Period__c);
                system.debug('ADJ dc.UnavailableSiteTypes__c = '+ dc.UnavailableSiteTypes__c);
                system.debug('ADJ dc.UnavailableContractTypes__c = '+ dc.UnavailableContractTypes__c);
            }
            
            if (t.Discounts__c != null){
                
                if (Discounts.size() < 1){
                    t.Discounts__c.addError('This Discount is not Currently available');
                }
                /*                    
else if (SiteType.get(t.Discounts__c) != null) {

if (SiteType.get(t.Discounts__c).contains(t.Site_Type__c +'::')){
t.Discounts__c.addError('This Discount is not available to this site type');
}
}
*/
                else if (ContractType.get(t.Discounts__c) != null){
                    
                    if (ContractType.get(t.Discounts__c).contains(t.Contract_Type__c +'::')){
                        t.Discounts__c.addError('This Discount is not available to this contract type');
                    }
                    else {
                        if(t.gross_price__c != null)
                            t.Discount_Amount__c = t.gross_price__c * Discount_Amount.get(t.Discounts__c)/ 100;
                    }
                } 
            }
            //Net Price - Calculate net price
            if(t.gross_price__c != null)
                t.Price_after_Discounts__c = t.gross_price__c - t.Discount_Amount__c ;
            
        }
    }  
}