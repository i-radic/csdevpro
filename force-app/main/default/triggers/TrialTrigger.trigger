trigger TrialTrigger on Trial__c (before insert, before update) {
    //Instantiate object
    TrialHelper aTrialHelper = new TrialHelper();
    
    //Validate signed trials have a signed agreement
    /* THIS HAS BEEN COMMENTED OUT TO TEMP PREVENT VALIDATION AS PER CLIENT REQUEST
     * WILL IMPLEMENT AGAIN WHEN THUNDERHEAD.COM HAVE COMPLETED THEIR ECHOSIGN 
     * INTEGRATION DUE FEB 2013
    aTrialHelper.ValidateSignedAgreement(Trigger.new);
    aTrialHelper.ValidateSentAgreement(Trigger.new);
    */
    
    //Get list of signed trials
    List<Trial__c> signedTrials = new List<Trial__c>();
    for(Trial__c aTrial : Trigger.new){
        //Check against old version
        if(aTrial.Status__c == 'Signed' && Trigger.oldMap != null && Trigger.oldMap.get(aTrial.Id).Status__c != 'Signed'){
            signedTrials.add(aTrial);
        }
    }
    //Not required now as Workflows are configured.
    //aTrialHelper.SendOrderCreatedEmails(signedTrials);
    aTrialHelper.UpdateEstimatedTrialStartDate(signedTrials);
    
    
    //Get list of trials where all stock items are dispatched
    List<Trial__c> trialsToDispatch = new List<Trial__c>();
    for(Trial__c aTrial : Trigger.new){
        if(aTrial.Status__c != 'Dispatched' && (aTrial.Total_Line_Item_Quantity__c > 0 && aTrial.Total_Dispatched__c > 0) && aTrial.Total_Line_Item_Quantity__c == aTrial.Total_Dispatched__c){
            trialsToDispatch.add(aTrial);
        }
    }
    aTrialHelper.SetDispatchedStatus(trialsToDispatch);
}