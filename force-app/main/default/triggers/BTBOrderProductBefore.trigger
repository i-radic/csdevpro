//
// History
//
// Version      Date            Author          Comments
// 1.0.0        05-04-2011      Krupakar        Initial version
//

//
// Comments
//
// 1. This Trigger will change the product family from Calls and Lines New provide 
//    or Calls and Lines SIM New provide to Calls and Lines New.

trigger BTBOrderProductBefore on BTB_Order_Product__c (before insert, before update) {
    //This Trigger will change the product family from Calls and Lines New provide or Calls and Lines SIM New provide to Calls and Lines New
    for (BTB_Order_Product__c bop : Trigger.new) {
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines New provide') || bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines SIM New provide') || bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Resign') || bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Winback')) { 
            bop.Product_Family__c = 'Calls and Lines'; 
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines New provide')) { 
            bop.Order_Type__c = 'New Provide'; 
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines SIM New provide')) {
            bop.Order_Type__c = 'SIM New Provide';
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Resign')) {
            bop.Order_Type__c = 'Resign';
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Winback')) {
            bop.Order_Type__c = 'Winback';
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Featureline provide') || bop.PROD_GROUP__c.equalsIgnoreCase('Featureline cease') || bop.PROD_GROUP__c.equalsIgnoreCase('Featureline modify')) {
            bop.Product_Family__c = 'Featureline';
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Featureline provide')) {
            bop.Order_Type__c = 'Provide';
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Featureline cease')) {
            bop.Order_Type__c = 'Cease';
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Featureline modify')) {
            bop.Order_Type__c = 'Modify';
        }
        if(bop.PROD_GROUP__c.equalsIgnoreCase('Mobility')) {
            bop.Product_Family__c = 'Mobility';
        }
        
    }
}