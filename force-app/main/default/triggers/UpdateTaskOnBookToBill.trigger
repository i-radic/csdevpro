trigger UpdateTaskOnBookToBill on Task (before insert, Before Update, after update) {
    if(TriggerDeactivating__c.getInstance().Task__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }
    Map<string,string> RCMap = new Map<string,string>();  
    Map<String,String> MapProfiles=new Map<String,String>();
    Map<String,String> MapSSProfiles=new Map<String,String>();
    
    List<Profile> Pro=[Select Id,Name From Profile];
    For(Profile P:Pro)
    MapProfiles.put(P.Id,P.Name);
    
    List<Profile> SS=[Select Id,Name From Profile where Name like  '%Standard User%'];
    For(Profile P:SS)
    MapSSProfiles.put(P.Id,P.Name);
    
    Task[] Tnew=Trigger.new;
    Id BBId=Tnew[0].WhatId; 
    List<BookToBill__c> BB = [select Id,SE_SM_Feedback__c,FeedbackNotes__c,Measurement_Status__c,Revenue_Assurance_Status__c,RecordTypeId from BookToBill__c where Id=:BBId];
  
    
    if(BB.size() > 0){ //should trigger for BookToBill recordtypes only
            for(RecordType R:[Select Id,DeveloperName From RecordType where SOBJECTTYPE='BookToBill__c']) {
                RCMap.put(R.Id,R.DeveloperName);
            }                                   
            String BBrecordTypeName = RCMap.get(BB[0].RecordTypeId);            
             System.debug('BBrecordTypeName========='+BBrecordTypeName);
            if(BBrecordTypeName != null){     
            System.debug('BBrecordTypeName========='+BBrecordTypeName);              
                if(Trigger.isInsert){
                         
                        if(BBrecordTypeName.contains('Revenue')){
                            if(BB[0].FeedbackNotes__c == null){
                                BB[0].FeedbackNotes__c='';
                            }
                            Tnew[0].Description = BB[0].FeedbackNotes__c;
                            Tnew[0].RecordTypeId=[select Id from RecordType where DeveloperName=:'Book2Bill_Task'].Id;
                            Tnew[0].Status=BB[0].Revenue_Assurance_Status__c;
                           
                          }                               
                        if(BBrecordTypeName.contains('Calls')){ 
                            System.debug('B2b::::::::::::::::'+Tnew[0].whatId);
                            Tnew[0].RecordTypeId=[select Id from RecordType where DeveloperName=:'Book2Bill_Measurement_Task'].Id;
                            Tnew[0].Status=BB[0].Measurement_Status__c;
                            System.debug('MeasurementStatus '+ Tnew[0].Status); 
                            if(Tnew[0].Description != null && Tnew[0].Description.length() > 255)
                                BB[0].Task_Comments_Measurement__c = Tnew[0].Description.substring(0,255);
                            else
                                BB[0].Task_Comments_Measurement__c = Tnew[0].Description;
                            System.debug('BB[0].Task_Comments_Measurement__c'+ BB[0].Task_Comments_Measurement__c); 
                        }
                     
                }
                if(Trigger.isBefore && Trigger.isUpdate){
                                                               
                          
                    Task[] Told=Trigger.old;    
                    
               //Validation to not to edit Related Field(What Id)
                  String BooktoBill_prefix = Schema.SObjectType.BookToBill__c.getKeyPrefix();
                     if(((String)Tnew[0].WhatId).startsWith(BooktoBill_prefix)){
                       if(Tnew[0].WhatId!= Told[0].WhatId)
                       Tnew[0].addError('You are not allowed to change Related To details');
                      }
                      
                    String P=MapSSProfiles.get(UserInfo.getProfileId());                        
                     if(P!=null){
                     Tnew[0].status=Trigger.oldMap.get(Told[0].Id).Status;
                     Tnew[0].subject=Trigger.oldMap.get(Told[0].Id).subject;
                     Tnew[0].priority=Trigger.oldMap.get(Told[0].Id).priority;
                     }   
                   
                //End of validation rule Related Field(What Id)
                
                String profileId = [select profileId from User where Id=: Tnew[0].OwnerId].profileId;
                String OldProfileId=[select profileId from User where Id=: Told[0].OwnerId].profileId;                   
                    
                        if(BBrecordTypeName.contains('Revenue')){
                       
                            if(Tnew[0].OwnerId != Told[0].OwnerId)
                            {                                   
                                if(MapProfiles.get(profileId).contains('Sales Manager') || MapProfiles.get(profileId) == 'BookToBill Team')// if its from SS to SM or BTBTeam
                                {    
                                    Tnew[0].Status='Audit Response';
                                    BB[0].Revenue_Assurance_Status__c=Tnew[0].Status;
                                    if(Tnew[0].Description.length() > 255)
                                        BB[0].FeedbackNotes__c = Tnew[0].Description.substring(0,255);
                                    else
                                        BB[0].FeedbackNotes__c = Tnew[0].Description;                                   
                                }                              
                              else if(MapProfiles.get(profileId).contains('Standard User'))//  SM to SS
                                {       
                                    Tnew[0].Status='Audit Query';       
                                    BB[0].Revenue_Assurance_Status__c=Tnew[0].Status;
                                    if(Tnew[0].Description.length() > 255)
                                        BB[0].FeedbackNotes__c = Tnew[0].Description.substring(0,255);
                                    else
                                        BB[0].FeedbackNotes__c = Tnew[0].Description;     
 
                                }
                            }                           
                        }   
                        
                    //code added by Raj
                   
                    if(BBrecordTypeName.contains('Calls')){
                    
                  if((Tnew[0].OwnerId!=Told[0].OwnerId)&& (MapProfiles.get(profileId).contains('Sales Manager') && (MapProfiles.get(OldprofileId).contains('Standard User')))){
                      Tnew[0].Status = 'Feedback Awaiting SM Verification';
                        
                      }

                      
                  If((Tnew[0].OwnerId!=Told[0].OwnerId)&&(MapProfiles.get(profileId) == 'BookToBill Team')&& (MapProfiles.get(OldprofileId).contains('Sales Manager'))){
                    
                       If(Tnew[0].Feedback_Verified_by_SM_DGM__c==FALSE || Tnew[0].Forecast_shortfall_confirmation__c==FALSE)
                        Tnew[0].addError('Please tick Feedback Verified by SM/DGM checkbox and Forecast shortfall confirmation.');
                       Else
                         Tnew[0].Status = 'Feedback Under Review';
                       }  
                     If((Tnew[0].OwnerId!=Told[0].OwnerId)&&(MapProfiles.get(profileId).contains('Standard User'))&& (MapProfiles.get(OldprofileId).contains('Sales Manager'))){
                    
                  
                    If(Tnew[0].Feedback_Verified_by_SM_DGM__c==TRUE)
                        Tnew[0].addError('Please uncheck Feedback Verified by SM/DGM checkbox');
                      Else
                          Tnew[0].Status = 'Feedback Required';

                       }  

                    }
                        
                        }
                      if(Trigger.isAfter && Trigger.isUpdate)
                      {   
                       Task[] Told=Trigger.old;       
                        if(BBrecordTypeName.contains('Calls')){
                            if( Tnew[0].Status!=Told[0].Status)
                                    BB[0].Measurement_Status__c=Tnew[0].Status;
                            
                            if(Tnew[0].Description != null && Tnew[0].Description.length() > 255)
                               BB[0].Task_Comments_Measurement__c = Tnew[0].Description.substring(0,255);
                            else
                                BB[0].Task_Comments_Measurement__c= Tnew[0].Description;
                            
                        }
                                
                }                       
                update BB;
                
                
            }
                
    }
}