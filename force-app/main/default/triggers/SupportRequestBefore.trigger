trigger SupportRequestBefore on Support_Request__c (before insert, before update, before delete) {
    final SupportRequestHandler handler = SupportRequestHandler.getHandler();
    
    //before insert
    if(Trigger.isBefore && Trigger.isInsert) {
        handler.beforeInsert(); return;
    }
}