trigger OrderProductAfter on Order_Product__c (after insert, after update, after delete) {
    List<Order_Product__c> addProducts = new List<Order_Product__c>();    

	Set<String> prodIds = New Set<String>();				//List of products in the trigger
    Set<Id> siteIds = New Set<Id>();						//List of site Ids
    Set<String> requiredIds = New Set<String>();			//List of required products
    //Set<String> existingIds = New Set<String>();			//List of existing products
	Map<Id, Order_Product__c> existingIds = New Map<Id, Order_Product__c>();			//List of existing products
    
	if(Trigger.isDelete){
		for(Order_Product__c c:Trigger.Old){
			prodIds.add(c.Id);
		}
		List<Order_Product__c > relatedIds= [Select Id, SFCSSProdId__c From Order_Product__c WHERE AutoAddedBy__c = :prodIds];
		delete relatedIds;
	}else if(Trigger.isInsert){
		for(Order_Product__c c:Trigger.New){           
			siteIds.add(c.Order_Site__c);        
			prodIds.add(c.SFCSSProdId__c);
		}    
 
		List<CSS_Products__c > requiredProducts = [Select Unit__c, Type__c, Required_Product__r.Grouping__c, Product_Name__c, Related_Product__c, Unit_Cost__c, A_Code__c, ShopLink__c,Connection_Charge__c,Sort_Order__c,Required_Product__c FROM CSS_Products__c WHERE Id = :prodIds AND (Required_Product__c <> null OR Related_Product__c <> null) AND Active__c = True];
		for(CSS_Products__c r:requiredProducts ){
			requiredIds.add(r.Required_Product__c);
			requiredIds.add(r.Related_Product__c);
		}

		List<Order_Product__c > existingProd= [Select Id, SFCSSProdId__c, AutoAddedBy__c, A_Code__c, Quantity__c From Order_Product__c WHERE Order_Site__c = :siteIds AND SFCSSProdId__c = :requiredIds AND Type__c <> 'Line' AND Grouping__c <> 'Connection'];
		for(Order_Product__c e:existingProd ){
			existingIds.put(e.SFCSSProdId__c, e);
		}  
		//add required products
		for(CSS_Products__c r:requiredProducts ){
			for(Order_Product__c p:Trigger.New ){
				Order_Product__c reqProd = New Order_Product__c();
                if(r.Required_Product__c != Null && existingProd.size() == 0 && r.Id == p.SFCSSProdId__c){
					reqProd.Order_Site__c = p.Order_Site__c ;
					reqProd.AutoAddedBy__c = p.Id;
					reqProd.SFCSSProdId__c = r.Required_Product__c;
					reqProd.Quantity__c = p.Quantity__c;
					if(p.Limit_Auto_Add_to_One__c){
						reqProd.Quantity__c = 1;
					}
					reqProd.Number_info__c = p.Number_info__c;
					reqProd.Information__c = p.Information__c;
					reqProd.Care__c = p.Care__c;
					reqProd.Locked_Edit__c = True;
					reqProd.Locked_Delete__c = True;
					if(p.Limit_Auto_Add_to_One__c && p.zQuoteType__c != 'Cloud Voice Modify'){
						addProducts.add(reqProd);
					}
				}
                if(r.Related_Product__c != Null && existingProd.size() == 0 && r.Id == p.SFCSSProdId__c){
					reqProd.Order_Site__c = p.Order_Site__c;
					//reqProd.AutoAddedBy__c = p.Id;
					reqProd.SFCSSProdId__c = r.Related_Product__c;
					reqProd.Quantity__c = 1;
					reqProd.Locked_Edit__c = True;
					reqProd.Locked_Delete__c = True;

					addProducts.add(reqProd);
				}
			}
		}
		insert addProducts;
    }  
}