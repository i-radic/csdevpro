trigger SetAgreementStatusesOnOpp on echosign_dev1__SIGN_Agreement__c (after insert, after update) {
    Map<Id,echosign_dev1__SIGN_Agreement__c> oppsMap = new Map<Id,echosign_dev1__SIGN_Agreement__c>();
    List<Opportunity> oppsToUpdate = new List<Opportunity>();
    
    //Build Opportunity Map for insert / update (only if status changes)
    for( echosign_dev1__SIGN_Agreement__c agreement : Trigger.new ) {
        if( Trigger.isInsert || (Trigger.isUpdate && 
            ( Trigger.oldMap.get(agreement.Id).echosign_dev1__Status__c != agreement.echosign_dev1__Status__c ))) {
        
            if( agreement.echosign_dev1__Opportunity__c != null ) {
                oppsMap.put(agreement.echosign_dev1__Opportunity__c, agreement);
            }
        }   
    }
    
    if( oppsMap.keySet().size() == 0 ) {
        return;
    }
    
    //This should run only for EEBA and Credit Checks
    for( ID oppid : oppsMap.keySet()) 
    {
        echosign_dev1__SIGN_Agreement__c agreement = oppsMap.get( oppid );
        Opportunity opp = new Opportunity(id=oppid);
        
        if (agreement.Name.contains('EE Business Agreement'))
        {
            if( agreement.echosign_dev1__Status__c == 'Draft' || agreement.echosign_dev1__Status__c == 'Pre-Send' ) {
                opp.EEBA_Status__c = 'No Agreement Generated';
            } else if( agreement.echosign_dev1__Status__c == 'Out for Signature' || 
                agreement.echosign_dev1__Status__c == 'Waiting for Counter-Signature' || 
                agreement.echosign_dev1__Status__c == 'Cancelled / Declined' ) {
                opp.EEBA_Status__c = agreement.echosign_dev1__Status__c;
            } else if( agreement.echosign_dev1__Status__c == 'Signed'){
                opp.EEBA_Status__c = 'Fully ' + agreement.echosign_dev1__Status__c;
            }
            else if( agreement.echosign_dev1__Status__c == 'Expired'){
                opp.EEBA_Status__c = agreement.echosign_dev1__Status__c;
            }
            oppsToUpdate.add(opp);
        }
        else if (agreement.Name.contains('Credit Check'))
        {
            if( agreement.echosign_dev1__Status__c == 'Draft' || agreement.echosign_dev1__Status__c == 'Pre-Send' ) {
                opp.CreditCheck_Status__c = 'No Credit Check Generated';
            } else if( agreement.echosign_dev1__Status__c == 'Out for Signature' || 
                agreement.echosign_dev1__Status__c == 'Waiting for Counter-Signature' || 
                agreement.echosign_dev1__Status__c == 'Cancelled / Declined' ) {
                opp.CreditCheck_Status__c = agreement.echosign_dev1__Status__c;
            } else if( agreement.echosign_dev1__Status__c == 'Signed' ){ 
                opp.CreditCheck_Status__c = 'Fully ' + agreement.echosign_dev1__Status__c;
            }
            oppsToUpdate.add(opp);      
        }
        
    }   
    
    if (oppsToUpdate.size() > 0)
        update oppsToUpdate;
}