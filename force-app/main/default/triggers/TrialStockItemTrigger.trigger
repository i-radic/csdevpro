trigger TrialStockItemTrigger on Trial_Stock_Item__c (before insert, before update) {
	//Populate stock item details
	TrialStockItemHelper aTrialStockItemHelper = new TrialStockItemHelper();
	aTrialStockItemHelper.GetStockItemDetails(Trigger.new);
}