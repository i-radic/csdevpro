/**************************************************************************************************************************************************
    Class Name : CustomerExpClientplanBefore
    Test Class Name : Test_CustomerExpClientplanBefore
    Description : Code to Insert and Update Customer Experience client plan records. 
    Version : V0.1
    Created By Author Name : BALAJI MS
    Date : 08/09/2017
 *************************************************************************************************************************************************/

trigger CustomerExpClientplanBefore on Service_Improvement_Plan__c (before insert, before update) {
    
    if(Trigger.IsBefore){
        
        String sBreak = '</br>';
        Set<Id> SIPIds = new Set<Id>();
        Set<Id> Acc_ids = new Set<Id>();
        if(Trigger.isInsert){
            for (Service_Improvement_Plan__c SIP: Trigger.new) {
                Acc_ids.add(SIP.Account__c);
            }
            Set<Id> SIPlist= new Set<Id>();
            for(Service_Improvement_Plan__c s:[Select Id,Account__c From Service_Improvement_Plan__c WHERE Account__c IN:Acc_ids])
            {
                SIPlist.add(s.Account__c);
            }         
            
            if(SIPlist.size()>0){
                for (Service_Improvement_Plan__c SIP2: Trigger.new) {
                    if(SIPlist.contains(SIP2.Account__c)){
                        SIP2.adderror(''+ sBreak +'You should only have one Customer Experience Plan per SAC',false);
                    }
                }
            }
            
            //Medallia Integration
            ServiceImprovementPlanTriggerHandler.BeforeInsertTriggerHandler(Trigger.New);
        }
        
        //Recurssion Check to avaoid Trigger to Fire again and again in Before Update
        
        if(Trigger.IsUpdate && ServiceImprovementPlanTriggerHandler.BeforeUpdateRecursionChecker){
            
            //Enabling Recurssion Flag
            
            ServiceImprovementPlanTriggerHandler.BeforeUpdateRecursionChecker = False;
            ServiceImprovementPlanTriggerHandler.BeforeUpdateTriggerHandler(Trigger.NewMap, Trigger.OldMap);
            
        }
    }
}