trigger BTSportAfter on BT_Sport__c (after insert, after update, after delete) {
    
    public String HeaderId = null;
    
    
    //Get header ID from current record or Deleted record
    if (Trigger.isDelete){
        for (BT_Sport__c ot:Trigger.old) {
            HeaderId = ot.BT_Sport_Header__c;
        }
    }
    else {
        for (BT_Sport__c ot:Trigger.new) {
            HeaderId = ot.BT_Sport_Header__c;
        }
    }
    
    // only run if MSA or nonMSA Record TypesT ( ie not header)
     if (HeaderId == null) return ;
    
    // Calculated the roll up summary ( cant use rollup summary fields as same object, but different RecordType )
    Map<String, Decimal> Gross_Price = new Map<String, Decimal>();    
    Map<String, Decimal> Discount_Amount = new Map<String, Decimal>();   
    Map<String, Decimal> Price_after_Discounts  = new Map<String, Decimal>(); 
    
    List <aggregateResult> groupedResults = [SELECT BT_Sport_Header__c, Sum(Gross_Price__c),Sum(Discount_Amount__c), Sum(Price_after_Discounts__c)  from BT_Sport__c where BT_Sport_Header__c = :HeaderId GROUP BY BT_Sport_Header__c];
    for (AggregateResult ar : groupedResults)  {
        Gross_Price.put(string.valueOf(ar.get('BT_Sport_Header__c')), double.valueof(ar.get('expr0')));
        Discount_Amount.put(string.valueOf(ar.get('BT_Sport_Header__c')), double.valueof(ar.get('expr1')));
        Price_after_Discounts.put(string.valueOf(ar.get('BT_Sport_Header__c')), double.valueof(ar.get('expr2')));
        system.debug('ADJ Opportunity__c   ' + ar.get('BT_Sport_Header__c' ));
        system.debug('ADJ expr0   '+ ar.get('expr0'));
    }       
    
    List <BT_Sport__c> SportHeader = [SELECT id, BT_Sport_Gross_Price__c, BT_Sport_Total_Discounts__c, BT_Sport_Price_after_Discounts__c  FROM  BT_Sport__c where ID = :HeaderId ];
    for (BT_Sport__c sp : SportHeader)  {      
        sp.BT_Sport_Gross_Price__c = Gross_Price.get(sp.id);
        sp.BT_Sport_Total_Discounts__c = Discount_Amount.get(sp.id);
        sp.BT_Sport_Price_after_Discounts__c = Price_after_Discounts.get(sp.id);
    }
    
    update SportHeader;
}