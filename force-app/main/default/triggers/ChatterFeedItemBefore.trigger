trigger ChatterFeedItemBefore on FeedItem (before insert, before update) {

string fParent = Null;
    for (FeedItem f : trigger.new){
        fParent = f.ParentID;
        if (fParent != null && fParent.startsWith('01Z') ){  //this prevents all posting to limit to snapshots use f.Type == 'DashboardComponentSnapshot'
            f.addError('You cannot post into the Dashboard feed. You can only post snapshots to a User or Group.');
        }
    }

/*  COMMENTED OUT UNTIL READONLY GROUPS ARE EMPLOYED
//check for public groups with publishing rights
List<Group> grpBTLB = [SELECT Id, Name FROM Group WHERE Id = '00G200000012W4m' LIMIT 1];
List<Group> grpDesk = [SELECT Id, Name FROM Group WHERE Id = '00G200000012W4r' LIMIT 1];
List<Group> grpField = [SELECT Id, Name FROM Group WHERE Id = '00G200000012W51' LIMIT 1];
List<Group> grpCS = [SELECT Id, Name FROM Group WHERE Id = '00G200000012W4w' LIMIT 1];
*/
/* Sandbox 
List<Group> grpLEMServ = [SELECT Id, Name FROM Group WHERE Id = '00GP0000000Ls1n' LIMIT 1];
*/
/* Production */
List<Group> grpLEMServ = [SELECT Id, Name FROM Group WHERE Id = '00G200000012XFH' LIMIT 1];


Set<Id> lUserBTLB = new Set<Id>();
Set<Id> lUserDesk = new Set<Id>();
Set<Id> lUserField = new Set<Id>();
Set<Id> lUserCS = new Set<Id>();
Set<Id> lUserLEMServ = new Set<Id>();

/*
for(Group g: grpBTLB){
    List<GroupMember> grpM = [SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId = :Userinfo.GetUserID() AND GroupId = :g.Id];  
    for (GroupMember gM: grpM){
        lUserBTLB.add(gM.Id);
    }
} 
for(Group g: grpDesk){
    List<GroupMember> grpM = [SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId = :Userinfo.GetUserID() AND GroupId = :g.Id];  
    for (GroupMember gM: grpM){
        lUserBTLB.add(gM.Id);
    }
} 
for(Group g: grpField){
    List<GroupMember> grpM = [SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId = :Userinfo.GetUserID() AND GroupId = :g.Id];  
    for (GroupMember gM: grpM){
        lUserBTLB.add(gM.Id);
    }
}   
for(Group g: grpCS){
    List<GroupMember> grpM = [SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId = :Userinfo.GetUserID() AND GroupId = :g.Id];  
    for (GroupMember gM: grpM){
        lUserCS.add(gM.Id);
    }
} 
*/
for(Group g: grpLEMServ ){
    List<GroupMember> grpM = [SELECT Id, UserOrGroupId FROM GroupMember WHERE UserOrGroupId = :Userinfo.GetUserID() AND GroupId = :g.Id];  
    for (GroupMember gM: grpM){
        lUserLEMServ.add(gM.Id);
    }
} 
//Restrict posting in a group to certain users
for (FeedItem f : trigger.new){
        /*
        if (lUserBTLB.isEmpty() && f.ParentID == ChatterHelper.getBTLBgroup()){
            f.addError('You can only comment on existing posts in this group.') ; 
        }  
        if (lUserDesk.isEmpty() && f.ParentID == ChatterHelper.getDeskgroup()){
            f.addError('You can only comment on existing posts in this group.') ; 
        }
        if (lUserField.isEmpty() && f.ParentID == ChatterHelper.getFieldgroup()){
            f.addError('You can only comment on existing posts in this group.') ; 
        }
        if (lUserCS.isEmpty() && f.ParentID == ChatterHelper.getCSgroup()){
            f.addError('You can only comment on existing posts in this group.') ; 
        }  
        */  
        if (lUserLEMServ.isEmpty() && f.ParentID == ChatterHelper.getLEMServicegroup()){
            f.addError('You can only comment on existing posts in this group.') ; 
        }           
    }  
 
}