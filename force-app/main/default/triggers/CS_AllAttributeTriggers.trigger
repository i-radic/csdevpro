trigger CS_AllAttributeTriggers on cscfga__Attribute__c (after delete, after insert, after undelete, 
    after update, before delete, before insert, before update) {
        
        CS_TriggerHandler.execute(new CS_AttributeTriggerDelegate());
        
}