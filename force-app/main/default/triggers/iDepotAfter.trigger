trigger iDepotAfter on iDepot__c (after update) {
    
    static boolean isExecuted = false;
    List<Note> lstNotes = new List<Note>();
    List<Note> lstNotes_Del = new List<Note>();
    String s = '#';
    if(trigger.isUpdate) {
        if(StaticVariables.getTaskIsRun() == false) {
            for(iDepot__c LS:Trigger.new){
                                
                if(LS.ABC2_Status__c != trigger.oldMap.get(LS.id).ABC2_Status__c) {
                    s += '<b> ABC2 Status </b> Changed From <b>'+ trigger.oldMap.get(LS.id).ABC2_Status__c +' </b> to <b> '+LS.ABC2_Status__c + '</b><br /><br />';                
                }
                
                if(LS.ABC2_Sub_Status__c != trigger.oldMap.get(LS.id).ABC2_Sub_Status__c){
                    if(LS.ABC2_Status__c != trigger.oldMap.get(LS.id).ABC2_Status__c){
                        s += '#<b> ABC2 Sub Status </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Sub_Status__c +' </b> to <b> '+LS.ABC2_Sub_Status__c + '</b><br /><br />' ;                        
                    } else {
                        s += '<b> ABC2 Sub Status </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Sub_Status__c +' </b> to <b> '+LS.ABC2_Sub_Status__c + '</b><br /><br />' ;
                    }                   
                }
                
                if(LS.ABC2_Acquisition__c != trigger.oldMap.get(LS.id).ABC2_Acquisition__c){
                    s += '<b> ABC2 Acquisition</b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Acquisition__c +' </b> to <b> '+LS.ABC2_Acquisition__c + '</b><br /><br />' ;
                    
                }                   
                
                if(LS.ABC2_Bobcat_Ref__c != trigger.oldMap.get(LS.id).ABC2_Bobcat_Ref__c){
                    s += '<b> ABC2 Bobcat Reference</b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Bobcat_Ref__c +' </b> to <b> '+LS.ABC2_Bobcat_Ref__c + '</b><br /><br />' ;
                }
                
                if(LS.ABC2_BOBCAT_Reference_Numbe__c != trigger.oldMap.get(LS.id).ABC2_BOBCAT_Reference_Numbe__c){
                    s += '<b> ABC2 BOBCAT Reference Number </b> Changed From <b> '+trigger.oldMap.get(LS.id).ABC2_BOBCAT_Reference_Numbe__c +'<b> to </b>'+LS.ABC2_BOBCAT_Reference_Numbe__c + '</b><br /><br />' ;
                }
                
                if(LS.ABC2_Education__c != trigger.oldMap.get(LS.id).ABC2_Education__c){
                    s += '<b> ABC2 Education </b> Changed From <b> '+trigger.oldMap.get(LS.id).ABC2_Education__c +' </b> to <b> '+LS.ABC2_Education__c + '</b><br /><br />' ;
                }
                
                /*
                if(LS.ABC2_Internal_Notes__c != trigger.oldMap.get(LS.id).ABC2_Internal_Notes__c){
                    s += '<b> ABC2 Internal Notes </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Internal_Notes__c +' </b> to <b> '+LS.ABC2_Internal_Notes__c + '</b><br /><br />' ;
                }
                */
                        
                if(LS.PostCode__c != trigger.oldMap.get(LS.id).PostCode__c){
                    s += '<b> Postcode </b> Changed From <b>'+trigger.oldMap.get(LS.id).PostCode__c +' </b> to <b> '+LS.PostCode__c + '</b><br /><br />' ;
                }
                
                if(LS.ABC2_LE_Code__c != trigger.oldMap.get(LS.id).ABC2_LE_Code__c){
                    s += '<b> ABC2 LE Code </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_LE_Code__c +' </b> to <b> '+LS.ABC2_LE_Code__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_LE_Name__c != trigger.oldMap.get(LS.id).ABC2_LE_Name__c){
                    s += '<b> ABC2 LE Name </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_LE_Name__c +' </b> to <b> '+LS.ABC2_LE_Name__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Opportunity_Reference__c != trigger.oldMap.get(LS.id).ABC2_Opportunity_Reference__c){
                    s += '<b> ABC2 Opportunity Reference </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Opportunity_Reference__c +' </b> to <b> '+LS.ABC2_Opportunity_Reference__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Owner__c != trigger.oldMap.get(LS.id).ABC2_Owner__c){
                    s += '<b> ABC2 Owner </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Owner__c +' </b> to <b> '+LS.ABC2_Owner__c + '</b><br /><br />' ;
                }                   
                        
                if(LS.ABC2_Percentage_Variance__c != trigger.oldMap.get(LS.id).ABC2_Percentage_Variance__c){
                    s += '<b> ABC2 Percentage Variance </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Percentage_Variance__c +' </b> to <b> '+LS.ABC2_Percentage_Variance__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Product__c != trigger.oldMap.get(LS.id).ABC2_Product__c){
                    s += '<b> ABC2 Product </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Product__c +' </b> to <b> '+LS.ABC2_Product__c + '</b><br /><br />' ;
                }
                        
                if(LS.Proposed_Sac_Name__c != trigger.oldMap.get(LS.id).Proposed_Sac_Name__c){
                    s += '<b> Proposed Sac Name </b> Changed From <b>'+trigger.oldMap.get(LS.id).Proposed_Sac_Name__c +' </b> to <b> '+LS.Proposed_Sac_Name__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Raised_By__c != trigger.oldMap.get(LS.id).ABC2_Raised_By__c){
                    s += '<b> ABC2 Raised By </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Raised_By__c +' </b> to <b> '+LS.ABC2_Raised_By__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Sac_Code__c != trigger.oldMap.get(LS.id).ABC2_Sac_Code__c){
                    s += '<b> ABC2 Sac Code </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Sac_Code__c +' </b> to <b> '+LS.ABC2_Sac_Code__c + '</b><br /><br />' ;
                }                  
                        
                if(LS.ABC2_SAC_Name__c != trigger.oldMap.get(LS.id).ABC2_SAC_Name__c){
                    s += '<b> ABC2 SAC Name </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_SAC_Name__c +' </b> to <b> '+LS.ABC2_SAC_Name__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_SCARS_LE_CODE__c != trigger.oldMap.get(LS.id).ABC2_SCARS_LE_CODE__c){
                    s += '<b> ABC2 SCARS LE CODE </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_SCARS_LE_CODE__c +' </b> to <b> '+LS.ABC2_SCARS_LE_CODE__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Scars_Period__c != trigger.oldMap.get(LS.id).ABC2_Scars_Period__c){
                    s += '<b> ABC2 Scars Period </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Scars_Period__c +' </b> to <b> '+LS.ABC2_Scars_Period__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Spend_Current_Month__c != trigger.oldMap.get(LS.id).ABC2_Spend_Current_Month__c){
                    s += '<b> ABC2 Spend Current Month </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Spend_Current_Month__c +' </b> to <b> '+LS.ABC2_Spend_Current_Month__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Spend_CYTD__c != trigger.oldMap.get(LS.id).ABC2_Spend_CYTD__c){
                    s += '<b> ABC2 Spend CYTD </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Spend_CYTD__c +' </b> to <b> '+LS.ABC2_Spend_CYTD__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Spend_LYFY__c != trigger.oldMap.get(LS.id).ABC2_Spend_LYFY__c){
                    s += '<b> ABC2 Spend LYFY </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Spend_LYFY__c +' </b> to <b> '+LS.ABC2_Spend_LYFY__c + '</b><br /><br />' ;
                }
                        
                if(LS.ABC2_Spend_LYTD__c != trigger.oldMap.get(LS.id).ABC2_Spend_LYTD__c){
                    s += '<b> ABC2 Spend LYTD </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Spend_LYTD__c +' </b> to <b> '+LS.ABC2_Spend_LYTD__c + '</b> </b><br /><br />' ;
                }
                        
                if(LS.ABC2_Variance__c != trigger.oldMap.get(LS.id).ABC2_Variance__c){
                    s += '<b> ABC2 Variance </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Variance__c +' </b> to <b> '+LS.ABC2_Variance__c + '</b><br /><br />' ;
                }
                
                /*
                if(LS.Assigned_to__c != trigger.oldMap.get(LS.id).Assigned_to__c){
                            
                     String oldUser, newUser;
                    
                    if(trigger.oldMap.get(LS.id).Assigned_to__c != null){
                        User oldUser1 = [ SELECT Id, Name FROM User WHERE Id = : trigger.oldMap.get(LS.id).Assigned_to__c ];
                        oldUser = oldUser1.Name;
                    } else {
                        oldUser = null;
                    }
                    
                    if(LS.Assigned_to__c != null){
                        User newUser1 = [ SELECT Id, Name FROM User WHERE Id = : LS.Assigned_to__c ];
                        newUser = newUser1.Name;
                    } else {
                        newUser = null;
                    }           
                                            
                    s += '<b> Assigned to </b> Changed From <b>\''+ oldUser +'\' <b> to </b> \''+ newUser + '\'</b><br /><br />' ;
                }*/
                
                if(LS.Request_Type__c != trigger.oldMap.get(LS.id).Request_Type__c){
                    s += '<b> Request Type </b> Changed From <b>'+trigger.oldMap.get(LS.id).Request_Type__c +' </b> to <b> '+LS.Request_Type__c + '</b><br /><br />' ;
                }
                
                if(LS.Company_Name_Trading_Name__c != trigger.oldMap.get(LS.id).Company_Name_Trading_Name__c){
                    s += '<b> Company Name Trading Name </b> Changed From <b>'+trigger.oldMap.get(LS.id).Company_Name_Trading_Name__c +' </b> to <b> '+LS.Company_Name_Trading_Name__c + '</b><br /><br />' ;
                }
                
                if(LS.Company_Type__c != trigger.oldMap.get(LS.id).Company_Type__c){
                    s += '<b> Company Type </b> Changed From <b>'+trigger.oldMap.get(LS.id).Company_Type__c +' </b> to <b> '+LS.Company_Type__c + '</b><br /><br />' ;
                }
                
                if(LS.Company_Reg_No__c != trigger.oldMap.get(LS.id).Company_Reg_No__c){
                    s += '<b> Company Reg No </b> Changed From <b>'+trigger.oldMap.get(LS.id).Company_Reg_No__c +' </b> to <b> '+LS.Company_Reg_No__c + '</b><br /><br />' ;
                }
                
                if(LS.Charity_No__c != trigger.oldMap.get(LS.id).Charity_No__c){
                    s += '<b> Charity No </b> Changed From <b>'+trigger.oldMap.get(LS.id).Charity_No__c +' </b> to <b> '+LS.Charity_No__c + '</b><br /><br />' ;
                }
                
                if(LS.Main_BT_Telephone_Number__c != trigger.oldMap.get(LS.id).Main_BT_Telephone_Number__c){
                    s += '<b> Main BT Telephone Number </b> Changed From <b>'+trigger.oldMap.get(LS.id).Main_BT_Telephone_Number__c +' </b> to <b> '+LS.Main_BT_Telephone_Number__c + '</b><br /><br />' ;
                }
                if(LS.Property_Name_Number__c != trigger.oldMap.get(LS.id).Property_Name_Number__c){
                    s += '<b> Property Name Number </b> Changed From <b>'+trigger.oldMap.get(LS.id).Property_Name_Number__c +' </b> to <b> '+LS.Property_Name_Number__c + '</b><br /><br />' ;
                }
                    
                if(LS.Thoroughfare_Name__c != trigger.oldMap.get(LS.id).Thoroughfare_Name__c){
                    s += '<b> Thoroughfare Name </b> Changed From <b>'+trigger.oldMap.get(LS.id).Thoroughfare_Name__c +' </b> to <b> '+LS.Thoroughfare_Name__c + '</b><br /><br />' ;
                }
                
                if(LS.Locality_Village__c != trigger.oldMap.get(LS.id).Locality_Village__c){
                    s += '<b> Locality Village </b> Changed From <b>'+trigger.oldMap.get(LS.id).Locality_Village__c +' </b> to <b> '+LS.Locality_Village__c + '</b><br /><br />' ;
                }
                if(LS.Town_City__c != trigger.oldMap.get(LS.id).Town_City__c){
                    s += '<b> Town City </b> Changed From <b>'+trigger.oldMap.get(LS.id).Town_City__c +' </b> to <b> '+LS.Town_City__c + '</b><br /><br />' ;
                }
                
                if(LS.Salutation__c != trigger.oldMap.get(LS.id).Salutation__c){
                    s += '<b> Salutation </b> Changed From <b>'+trigger.oldMap.get(LS.id).Salutation__c +' </b> to <b> '+LS.Salutation__c + '</b><br /><br />' ;
                }
                
                if(LS.Forename__c != trigger.oldMap.get(LS.id).Forename__c){
                    s += '<b> Forename </b> Changed From <b>'+trigger.oldMap.get(LS.id).Forename__c +' </b> to <b> '+LS.Forename__c + '</b><br /><br />' ;
                }
                
                if(LS.Surname__c != trigger.oldMap.get(LS.id).Surname__c){
                    s += '<b> Surname </b> Changed From <b>'+trigger.oldMap.get(LS.id).Surname__c +' </b> to <b> '+LS.Surname__c + '</b><br /><br />' ;
                }
                
                if(LS.Contact_Number__c != trigger.oldMap.get(LS.id).Contact_Number__c){
                    s += '<b> Contact Number </b> Changed From <b>'+trigger.oldMap.get(LS.id).Contact_Number__c +' </b> to <b> '+LS.Contact_Number__c + '</b><br /><br />' ;
                }
                
                if(LS.Business_Reason_Comments__c != trigger.oldMap.get(LS.id).Business_Reason_Comments__c){
                    s += '<b> Business Reason Comments </b> Changed From <b>'+trigger.oldMap.get(LS.id).Business_Reason_Comments__c +' </b> to <b> '+LS.Business_Reason_Comments__c + '</b><br /><br />' ;
                }
                                    
                if(LS.OID_Contract_Reference__c != trigger.oldMap.get(LS.id).OID_Contract_Reference__c){
                    s += '<b> OID Contract Reference </b> Changed From <b>'+trigger.oldMap.get(LS.id).OID_Contract_Reference__c +' </b> to <b> '+LS.OID_Contract_Reference__c + '</b><br /><br />' ;
                }
                
                if(LS.Proposed_Sac_Name__c != trigger.oldMap.get(LS.id).Proposed_Sac_Name__c){
                    s += '<b> Proposed Sac Name </b> Changed From <b>'+trigger.oldMap.get(LS.id).Proposed_Sac_Name__c +' </b> to <b> '+LS.Proposed_Sac_Name__c + '</b><br /><br />' ;
                }
                
                if(LS.Sac_LEs__c != trigger.oldMap.get(LS.id).Sac_LEs__c){
                    s += '<b> Sac LEs </b> Changed From <b>'+trigger.oldMap.get(LS.id).Sac_LEs__c +' </b> to <b> '+LS.Sac_LEs__c + '</b><br /><br />' ;
                }
                
                if(LS.Are_LEs_subsidiaries__c != trigger.oldMap.get(LS.id).Are_LEs_subsidiaries__c){
                    s += '<b> Are LEs subsidiaries </b> Changed From <b>'+trigger.oldMap.get(LS.id).Are_LEs_subsidiaries__c +' </b> to <b> '+LS.Are_LEs_subsidiaries__c + '</b><br /><br />' ;
                }
                
                if(LS.Product_Category__c != trigger.oldMap.get(LS.id).Product_Category__c){
                    s += '<b> Product Category </b> Changed From <b>'+trigger.oldMap.get(LS.id).Product_Category__c +' </b> to <b> '+LS.Product_Category__c + '</b><br /><br />' ;
                }
                
                if(LS.Product__c != trigger.oldMap.get(LS.id).Product__c){
                    s += '<b> Product </b> Changed From <b>'+trigger.oldMap.get(LS.id).Product__c +' </b> to <b> '+LS.Product__c + '</b><br /><br />' ;
                }
                
                if(LS.BOBCAT_REFERENCE_NUMBER__c != trigger.oldMap.get(LS.id).BOBCAT_REFERENCE_NUMBER__c){
                    s += '<b> BOBCAT REFERENCE NUMBER </b> Changed From <b>'+trigger.oldMap.get(LS.id).BOBCAT_REFERENCE_NUMBER__c +' </b> to <b> '+LS.BOBCAT_REFERENCE_NUMBER__c + '</b><br /><br />' ;
                }
                
                if(LS.PSM_Involved__c != trigger.oldMap.get(LS.id).PSM_Involved__c){
                    s += '<b> PSM Involved </b> Changed From <b>'+trigger.oldMap.get(LS.id).PSM_Involved__c +' </b> to <b> '+LS.PSM_Involved__c + '</b><br /><br />' ;
                }
                
                /* commenting as per CR3587 (making additional notes on Notes section)
                
                if(LS.LEM_SS_User1__c != trigger.oldMap.get(LS.id).LEM_SS_User1__c){
                    s += '<b> User1 </b> Changed From <b>'+trigger.oldMap.get(LS.id).LEM_SS_User1__c +' </b> to <b> '+LS.LEM_SS_User1__c + '</b><br /><br />' ;
                }
                
                if(LS.LEM_SS_User2__c != trigger.oldMap.get(LS.id).LEM_SS_User2__c){
                    s += '<b> User2 </b> Changed From <b>'+trigger.oldMap.get(LS.id).LEM_SS_User2__c +' </b> to <b> '+LS.LEM_SS_User2__c + '</b><br /><br />' ;
                }
                
                if(LS.LEM_SS_User3__c != trigger.oldMap.get(LS.id).LEM_SS_User3__c){
                    s += '<b> User3 </b> Changed From <b>'+trigger.oldMap.get(LS.id).LEM_SS_User3__c +' </b> to <b> '+LS.LEM_SS_User3__c + '</b><br /><br />' ;
                }
                
                if(LS.LEM_SS_User4__c != trigger.oldMap.get(LS.id).LEM_SS_User4__c){
                    s += '<b> User4 </b> Changed From <b>'+trigger.oldMap.get(LS.id).LEM_SS_User4__c +' </b> to <b> '+LS.LEM_SS_User4__c + '</b><br /><br />' ;
                }
                */
                
                if(LS.ABC2_Proposed_Gaining_Sac_Name__c != trigger.oldMap.get(LS.id).ABC2_Proposed_Gaining_Sac_Name__c){
                    s += '<b> ABC2 Proposed Gaining Sac Name </b> Changed From <b>'+trigger.oldMap.get(LS.id).ABC2_Proposed_Gaining_Sac_Name__c +' </b> to <b> '+LS.ABC2_Proposed_Gaining_Sac_Name__c + '</b><br /><br />' ;
                }       
                                
                if(LS.ABC2_Status__c != trigger.oldMap.get(LS.id).ABC2_Status__c) {
                    if((LS.ABC2_Status__c == 'Closed' || LS.ABC2_Status__c == 'Cancelled' || LS.ABC2_Status__c == 'Rejected')) {
                        string s2 = '***<b>Consolidated Notes</b>***<br />';
                        s2 += s + '<br />-----------';
                        for(Note n : [select Id, body, CreatedBy.Name, CreatedDate  from Note where parentId = :LS.Id ORDER BY CreatedDate DESC]){                              
                            if(n.Body != null && !n.Body.contains('***<b>Consolidated Notes</b>***')){
                                s2  += '<br />' + n.Body +' <b>'+ n.CreatedBy.Name + ' ' + FormattedDatetime.getTimeZoneValue(n.CreatedDate)+ '</b><br />-----------';                               
                                system.debug('Hi am in loop :'+n);
                                lstNotes_Del.add(n);
                            }                               
                        }
                        s=s2;
                    }
                }                
                                        
                Note n = new Note();
                n.OwnerId = UserInfo.getUserId();
                n.ParentId = LS.Id ;
                n.Title = 'Closing Notes';
                n.Body= s;
                if(s != '#'){
                    lstNotes.add(n);
                }
            }
            StaticVariables.setTaskIsRun(true);
        }
        if(!lstNotes_Del.isEmpty()) {
            try {
                delete lstNotes_Del;
            }                                 
            catch (DMLException e) {
            }
        }
        if(!lstNotes.isEmpty()) {
            system.debug('Notes Size : ' + lstNotes.size());
            system.debug('Notes Value : ' + lstNotes);
            try {
                insert lstNotes;
            }                                 
            catch (DMLException e) {
            }
        }
    }
}