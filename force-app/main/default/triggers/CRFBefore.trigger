//
// History
//
// Version      Date            Author          Comments
// 1.0.0        09-02-2011      Dan Measures    Initial version
//

//
// Comments
//
// 1. Populate the CRF address fields from associated contact if no previous values in the address fields are present
//
trigger CRFBefore on CRF__c (before insert, before update) {
 Set<Id> opp_ids = new Set<Id>();
    Set<Id> contact_ids = new Set<Id>();
    Map<Id, String> accIds_names = new Map<Id, String>();
    Map<Id, SObject> contactIds_contacts = new Map<Id, SObject>();
    for (CRF__c b : Trigger.new) {
        opp_ids.add(b.Opportunity__c);
        contact_ids.add(b.Contact__c);
    }
    
    for (Contact c : [select Id, Contact_Building__c, Contact_Sub_Building__c, Contact_Address_Number__c, Address_POBox__c, Contact_Address_Street__c, Contact_Locality__c, Contact_Post_Town__c, Contact_Post_Code__c, Address_County__c, Address_Country__c from Contact where Id in :contact_ids]) {
        contactIds_contacts.put(c.Id, c);
    }
    for (CRF__c b : Trigger.new) {
        //if(b.Company_Name__c != null)
        //b.Test_Account_Name__c = b.Company_Name__c;
        
        Contact c = (Contact)contactIds_contacts.get(b.Contact__c);
        
        if(c != null && b.Building_Name__c == null && b.Sub_Building__c == null && b.Number__c == null && b.POBox__c == null &&
                b.Street__c == null && b.Locality__c == null && b.Post_Town__c == null && b.Post_Code__c == null && b.County__c == null && b.Country__c == null){
            b.Building_Name__c = c.Contact_Building__c; 
            b.Sub_Building__c = c.Contact_Sub_Building__c;
            b.Number__c = c.Contact_Address_Number__c;
            b.POBox__c = c.Address_POBox__c;
            b.Street__c = c.Contact_Address_Street__c;
            b.Locality__c = c.Contact_Locality__c;
            b.Post_Town__c = c.Contact_Post_Town__c;
            b.Post_Code__c = c.Contact_Post_Code__c;
            b.County__c = c.Address_County__c;
            b.Country__c = c.Address_Country__c;
        }
        if(c != null && b.Billing_Building_Name__c == null && b.Billing_Sub_Building__c == null && b.Billing_Number__c == null && b.Billing_POBox__c == null &&
                b.Billing_Street__c == null && b.Billing_Locality__c == null && b.Billing_Post_Town__c == null && b.Billing_Post_Code__c == null && b.Billing_County__c == null && b.Billing_Country__c == null){
            b.Billing_Building_Name__c = c.Contact_Building__c; 
            b.Billing_Sub_Building__c = c.Contact_Sub_Building__c;
            b.Billing_Number__c = c.Contact_Address_Number__c;
            b.Billing_POBox__c = c.Address_POBox__c;
            b.Billing_Street__c = c.Contact_Address_Street__c;
            b.Billing_Locality__c = c.Contact_Locality__c;
            b.Billing_Post_Town__c = c.Contact_Post_Town__c;
            b.Billing_Post_Code__c = c.Contact_Post_Code__c;
            b.Billing_County__c = c.Address_County__c;
            b.Billing_Country__c = c.Address_Country__c;
        }
    }
}