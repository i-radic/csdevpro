trigger CloudVoiceBefore on Cloud_Voice__c (before insert, before update, before delete) {

if(Trigger.isInsert){
    PricebookEntry pb = [Select Id, Product2Id, Pricebook2Id FROM PricebookEntry WHERE Name = 'Cloud Voice Solution' AND IsActive = True LIMIT 1];
    
    //add in non-leased line
    List<OpportunityLineItem> oliList = New List<OpportunityLineItem>();   
    for (Cloud_Voice__c c:trigger.New){
        OpportunityLineItem oli = New OpportunityLineItem();
        oli.OpportunityId = c.Opportunity__c;
        oli.PricebookEntryId = pb.Id;
        oli.Qty__c= 1;
        oli.Sales_Type__c = 'New Sale';
        oli.Category__c = 'Main';
        oli.Billing_Cycle__c = 'Monthly';
        oliList.add(oli);
    }  
    insert oliList;
    for(OpportunityLineItem oliId:oliList) {
        for (Cloud_Voice__c c:trigger.New){
           c.zOpportunityLineItem__c = oliId.Id;
        }
    }
    //add in leasing product line
    List<OpportunityLineItem> oliList2 = New List<OpportunityLineItem>(); 
    for (Cloud_Voice__c c:trigger.New){
        OpportunityLineItem oli = New OpportunityLineItem();
        oli.OpportunityId = c.Opportunity__c;
        oli.PricebookEntryId = pb.Id;
        oli.Qty__c= 1;
        oli.Sales_Type__c = 'New Sale';
        oli.Category__c = 'Main';
        oli.Billing_Cycle__c = 'Leasing';
        oliList2.add(oli);
    }  
    insert oliList2;
    for(OpportunityLineItem oliId:oliList2 ) {
        for (Cloud_Voice__c c:trigger.New){
            c.zOpportunityLineItemLease__c = oliId.Id;
        }
    }    

}

//delete product line when header deleted
if(Trigger.isDelete){
    Set<Id> Ids= Trigger.oldMap.keyset();
    Set<Id> delIds= new Set<Id>();

    for (Cloud_Voice__c oP :[SELECT zOpportunityLineItemLease__c, zOpportunityLineItem__c FROM Cloud_Voice__c WHERE Id IN :Ids]){
    system.debug('JMM zOpportunityLineItemLease: '+ oP.zOpportunityLineItemLease__c);
    system.debug('JMM zOpportunityLineItem: '+ oP.zOpportunityLineItem__c);
        delIds.add(oP.zOpportunityLineItem__c);
        delIds.add(oP.zOpportunityLineItemLease__c);
    }
    system.debug('JMM del: '+ delIds);
    Delete [SELECT Id FROM OpportunityLineItem WHERE Id IN :delIds];

}
    
}