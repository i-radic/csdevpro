trigger tgrWarringtonCallLog on BTLB_Warrington_Sales_Log__c (after insert) {
    if(trigger.isInsert) {
        BTLB_Warrington_Sales_Log__c b;
        //BTLBWaringtonCallLog a = new BTLBWaringtonCallLog();
       
        for(BTLB_Warrington_Sales_Log__c sales : trigger.new) {
             BTLBWaringtonCallLog.getWarringtonCallLogData(sales.Id);
        }      
    }
}