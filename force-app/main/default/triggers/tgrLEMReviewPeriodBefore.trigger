/*
Client      : BT
Author      : Krupakar J
Date        : 17/05/2012 (dd/mm/yyyy)
Description : This trigger will handle before trigger logic for LEM Reviews.
*/
trigger tgrLEMReviewPeriodBefore on LEM_Review_Period__c (before insert) {

	if(trigger.isBefore && trigger.isInsert) {
		LEMReviewBeforeHandler triggerHandler = new LEMReviewBeforeHandler();
		triggerHandler.handleLEMReviewPeriodBeforeInserts(trigger.new);
	}

}