/*
    When an opportunity is created a default company implementation record 
    gets created automatically and later when opportunity EEBA Status = Fully Signed
     and there is BST Deal attached to opportunity then update the Credit status = In Progress.
*/
trigger OpportunityCompanyImplementation on Opportunity (after insert, after update) {
    if(TriggerDeactivating__c.getInstance().Opportunity__c ) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }
    
    Opportunity opp = trigger.new[0];
    Company_Implementation__c ci;
    List<Company_Implementation__c> lstCI;

// Execute if Trigger Enabler custom setting is enabled

if (EE_TriggerClass.isTriggerEnabled('OpportunityCompanyImplementation')) {

    //Check Opp RecordType
    List<RecordType> rtOpp = 
        new List<RecordType>([SELECT Id FROM RecordType 
        WHERE sObjectType =: 'Opportunity' AND Name =: 'Standard' LIMIT 1]);


    
    if( Trigger.isInsert )
    {
         // #PG if(!test.isRunningTest())
         // Do not run for test classes or Salesforce to Salesforce operations
         // PG Sep 2016
        System.debug('##opp.ConnectionReceivedId '+opp.ConnectionReceivedId);
        if( (opp.ConnectionReceivedId == null) && (opp.RecordTypeId == rtOpp[0].Id) )
        {
            //Get Account Name
            List<Account> lstacc = new List<Account>();
        
            if (opp.AccountId != null)
            {    
                lstacc = [Select Id, Name FROM Account WHERE Id =: opp.AccountId];
            
                try
                {
                    ci = new Company_Implementation__c();
                    lstCI = new List<Company_Implementation__c>();
            
                    if (lstacc[0] != null && lstacc[0].Name != null)
                    {
                        if (lstacc[0].Name.length() > 80)
                            ci.Name = String.valueOf(lstacc[0].Name).substring(0,80);
                        else
                            ci.Name = lstacc[0].Name;
                    }
                    ci.Company__c = opp.AccountId;
                    ci.Opportunity__c = opp.Id;
                    ci.Overview_Stage__c = 'New';
                    ci.Credit_Status__c = 'New';
                    lstCI.add(ci);             
                    
                    insert lstCI;
                }
                catch(Exception e)
                {
                    opp.adderror('Error creating default Company Implementation record - ' + e.getMessage()); 
                }
            
            }    
        }
    }
    else if (Trigger.isUpdate && opp.EEBA_Status__c == 'Fully Signed' && OpportunityTriggerUtilities.companyImplementationTrigger == false) 
    {
        
        OpportunityTriggerUtilities.companyImplementationTrigger = true;
        if (opp.Acquisition_Status_post_approval__c == 'Processed')
        {
            //Check Checklist Validation RecordType
            List<RecordType> rtCheckValidation = new List<RecordType>([Select Id from RecordType 
                                                where sObjectType =: 'Checklist__c' and Name =: 'Validation' Limit 1]);
            
            //Get Company Implementation and related Checklists of recordtype validation
            //and set thier Acquisition status = 'Processed' so that emails can be sent out
            
            lstCI = new List<Company_Implementation__c>([Select id, 
                                                    (Select Id FROM Checklists__r WHERE RecordtypeId =: rtCheckValidation[0].Id) 
                                                    FROM Company_Implementation__c WHERE Opportunity__c =: opp.Id]);
            if (lstCI.size() > 0)
            {
                List<Checklist__c> lstCheck = new List<Checklist__c>();
                List<Checklist__c> lstCheckToUpdate = new List<Checklist__c>();
                lstCheck = lstCI[0].Checklists__r;
                if (lstCheck != null && lstCheck.size() > 0)
                {   
                    for (Checklist__c chk : lstCheck)
                    {   
                        chk.Acquisition_Status__c = 'Processed';
                        lstCheckToUpdate.add(chk);
                    }
                    try
                    {
                        update lstCheckToUpdate;
                    }
                    catch(Exception e)
                    {
                        opp.adderror('Error updating related Company Implementation(s) Checklist record - ' + e.getMessage()); 
                    }
                }
            }
        }
        

    }     
    /*
    else if (Trigger.isUpdate && opp.EEBA_Status__c == 'Fully Signed' && OpportunityTriggerUtilities.companyImplementationTrigger == false) 
    {
        try 
        {
            OpportunityTriggerUtilities.companyImplementationTrigger = true;
    
            //Check if there is any BST Deal Credit
            List<BST_Deal_Credits__c> lstDealCredit = new List<BST_Deal_Credits__c>([Select id from BST_Deal_Credits__c
                                                    where Opportunity__c =: opp.Id limit 1]);
            
            if(!test.isRunningTest())
            {
                lstCI = new List<Company_Implementation__c>();
                lstCI = [Select id from Company_Implementation__c where Opportunity__c =: opp.Id limit 1]; 
                system.debug('CI Record After Query--->' + lstCI);
                if (lstDealCredit.size() > 0 && lstCI != null && lstCI.size() > 0)
                {
                    system.debug('CI Record found --->' + lstCI.size());
                    lstCI[0].Credit_Status__c = 'In Progress';
                }
                system.debug('CI Record Before Update--->' + lstCI);
                
                update lstCI;
            }
        }
        catch(Exception e)
        {
            opp.adderror('Error updating Company Implementation record - ' + e.getMessage()); 
        }
    }
    */

// Execute if Trigger Enabler custom setting is enabled    
}    
    
}