trigger updateAccountOppLastCall on Event (after insert,after update) {
   //Added by GS 08-09-10
    //Update the account last call date field for these accounts 
    //  28 Sep 2010     Alan Jackson    added static variable to prevent Account trigger running 
    //  28/03/2011      Anil Devunoori  Added code related to CR2180 - Last Activity on a campaign
	if(TriggerDeactivating__c.getInstance().Event__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }
     StaticVariables.setAccountDontRun(true); // stop account trigger from running     
     Set<id> accids = new set<id>();
   
     Set<id> eventids = new Set<id>();
     Set<id> whatIds = new set<id>();
     
     Set<id> contactids = new set<id>();
     Set<id> oppids = new set<id>();
     Set<id> accountids = new set<id>();
     List<opportunity> newOpps = new List<opportunity>();
     
     Set<Id> allCampaignIds = new Set<Id>(); //CR2180
     
     	ID SystemAdminProfileId = '00e20000001MX7zAAG';
    	ID DataLoaderprofileId = '00e200000015EhzAAE';
    	ID ReportingProfileId = '00e20000001QB4tAAG';
    	ID BTLBAdminProfileId = '00e200000015K0NAAU';
    	ID CorpAdminProfileID = '00e200000015IRvAAM';
     //Get all of the what id's
      for(Event e : trigger.new){
          system.debug('adding' + e.whatid);
         whatids.add(e.whatid);  
         eventids.add(e.id);   
      }
    
     List<Event> eventlist = [select what.type,whatid,who.type from event where id in :eventids];
      for(Event e1 : eventlist){
      String strWhatId = e1.WhatId;  //CR2180
      system.debug('gswhattype' + e1.who.type);
         if(e1.what.type=='Opportunity'){
             oppids.add(e1.whatid);
             // ROS - 24/01/2013
             //Opportunity newOpp = new Opportunity(id=e1.whatid,last_updated__c =date.today());
             Opportunity newOpp = new Opportunity(id=e1.whatid);
             newopps.add(newopp);
         }else if(e1.what.type=='Account'){
             accountids.add(e1.whatid);
         }else if(e1.who.type=='Contact'){
             contactids.add(e1.whoid);
         }
         
         //CR2180 - start                          
             //Get all what Ids of campaign tasks                      
             if(strWhatId != null && strWhatId.startsWith('701')) {                             
                 allCampaignIds.add(e1.WhatId);                             
                 System.debug('AAAAAAAAAAAAAAAAAAAAAA'+e1.WhatId);                     
             }                                     
         //CR2180 - end
      }
      
      //CR2180 - start                                    
          if(allCampaignIds.size()>0) {                                   
              list<Campaign> allcampaignlist = new list<Campaign>();                         
              updateCampaignLastContacted updateLastActivityDate = new updateCampaignLastContacted();              
              allcampaignlist = updateLastActivityDate.updateCampaign(allCampaignIds);                                    
              update allcampaignlist;                                   
          }                      
      //CR2180 - end
      if(newOpps.size()>0){
         try{
          update newOpps;
        } Catch(exception e){
            system.debug('error inserting list: '+ newOpps);
        } 
      }
     //Get a list of related contacts
        if(contactids.size()>0){
            List<contact> contactAccounts = [select accountid from contact where id in :contactids];
            for (contact thisContact:contactAccounts){
            system.debug('found contact' + thiscontact);
                accids.add(thisContact.accountid);
            }
        }
     
     //Get a list of related opps
      if(oppids.size()>0){
       
        List<opportunity> oppAccounts = [select accountid from opportunity where id in :oppids];
        for (opportunity thisOpp:oppAccounts ){
            accids.add(thisOpp.accountid);
        }
      }  
     //Get the related accounts
         if(accountids.size()>0){
             List<account> accountAccounts = [select id from account where id in :whatids];
             for (Account thisAccount:accountAccounts ){
                accids.add(thisaccount.id);
             }
         }
             
        if(accids.size()>0){
            if(UserInfo.getProfileId() != SystemAdminProfileId && UserInfo.getProfileId() != DataLoaderprofileId && 
               UserInfo.getProfileId() != ReportingProfileId && UserInfo.getProfileId() != BTLBAdminProfileId && 
               UserInfo.getProfileId() != CorpAdminProfileID ){
            system.debug('gothere2');
            list<account> updateCallDate = new list<account>();
            updateAccountLastContacted updateAccountCallDate = new updateAccountLastContacted();                     
            //updateCallDate = updateAccountCallDate.updateAccount(accids);
            String objName='Events';
            updateCallDate = updateAccountCallDate.updateAccount(accids, objName);  
            try{
                update updateCallDate;
            } Catch(exception e){
                system.debug('error inserting list: '+ updateCallDate);
            }          
                              
        	} 
        }
}