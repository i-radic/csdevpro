trigger tgrADPLandscapeBeforeInsertUpdate on ADP_Landscape__c (before insert, before update) {
    Set<Id> ids = new Set<Id>();
    Map<Id, String> names = new Map<Id, String>();
    for (ADP_Landscape__c l : Trigger.new) {
        ids.add(l.RecordTypeId);
    }
    System.debug('ids: ' + ids);
    for (RecordType rt : [Select Id, Name from RecordType Where Id in :ids]) {
        names.put(rt.Id, rt.Name);
    }
    System.debug('names: ' + names);
    for (ADP_Landscape__c l : Trigger.new) {    
        l.Record_Type_Name__c = names.get(l.RecordTypeId); 
    }
}