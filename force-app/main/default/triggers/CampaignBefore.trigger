trigger CampaignBefore on Campaign (before insert, before update, before delete) {
/*
###################################################################################################
Orignal creator and statement of purpose not present. JMM 18/11/10

18/11/10    John McGovern    Added lines to pick up the department and division of the owner.
17/01/11    Dan Measures     Added the updating of child campaigns (when type = Marketing) when parent campaign updated or child campaign inserted - CR2360
                             Also the deletion of child 'MARKETING' campaigns if parent deleted.  NB) business stipulated max of 55 child campaigns to a parent.
07/10/11    John McGovern    Added logic to stamp values to the campaign when Completed to allow for deletion of member records.
###################################################################################################
*/

    Set<Id> campaignIds = new Set<Id>();
    Map<Id, Campaign> mParentCampaigns = new Map<Id, Campaign>();
    Set<Id> parentCampaignIds = new Set<Id>();
    final String MARKETING_CAMPAIGN = 'Marketing';

    //process to stamp total contacts and total in hierarchy on status = complete : JMM 07/10/11
    if(trigger.isUpdate) {
        for(Campaign c: trigger.new){
            campaignIds.add(c.Id);  
        }
        List<Campaign> lCamp = [Select ParentId, NumberOfContacts, HierarchyNumberOfContacts, Call_Backs__c, Completed_Calls__c From Campaign WHERE ID = :campaignIDs];            
        for(Campaign c: trigger.new){
        Campaign cOld = System.Trigger.oldMap.get(c.Id);
            if((cOld.Status != c.Status) && (c.Status == 'Completed')) {
                c.Static_Total_Contacts__c = c.NumberOfContacts;
                c.Static_Total_Contacts_in_Hierarchy__c = c.HierarchyNumberOfContacts;
                c.Static_Call_Backs__c = c.Call_Backs__c;
                c.Static_Completed_Calls__c = c.Completed_Calls__c; 
            }
        }        
    } 
    //JMM 07/10/11 END
    
    if(trigger.isDelete){   //DM - CR2360
        for(Campaign c: trigger.old){
            if(c.Campaign_Type__c == MARKETING_CAMPAIGN){
                parentCampaignIds.add(c.Id);
            }
        }
        if(!parentCampaignIds.isEmpty()){
            List<Campaign> campaignsToDelete = new List<Campaign>();
            for(Campaign c : [select Id from Campaign where ParentId in :parentCampaignIds]){
                campaignsToDelete.add(c);
                if(campaignsToDelete.size() == 1000){
                    delete campaignsToDelete;
                    campaignsToDelete.clear();
                }                   
            }
            if(!campaignsToDelete.isEmpty())
                delete campaignsToDelete;
        }
    }else{
        for(Campaign c: trigger.new){
            if (trigger.isupdate){
                if(trigger.oldmap.get(c.id).ownerid !=c.ownerid){
                        campaignIds.add(c.OwnerId);      
                }
                //  DM - CR2360
                if(c.Campaign_Type__c == MARKETING_CAMPAIGN && (trigger.oldmap.get(c.id).Status !=c.Status 
                                                            || trigger.oldmap.get(c.id).Description !=c.Description
                                                            || trigger.oldmap.get(c.id).Chapter__c !=c.Chapter__c
                                                            || trigger.oldmap.get(c.id).StartDate !=c.StartDate
                                                            || trigger.oldmap.get(c.id).EndDate !=c.EndDate
                                                            || trigger.oldmap.get(c.id).EndDate !=c.EndDate))
                    mParentCampaigns.put(c.Id,c);                        
            }else{
                campaignIds.add(c.OwnerId);
                //  DM - CR2360
                if(c.ParentId != null)
                    parentCampaignIds.add(c.ParentId);
            } 
        }
    
        //create list of ids of Campaign Owners
        Map<Id, String> mUser = new Map<Id, String>();
        Map<Id, String> mUserDept = new Map<Id, String>();
        Map<Id, String> mUserDiv = new Map<Id, String>();
        //Added by GS - don't want to run this soql if user hasn't changed.
        If(campaignIds.size()>0){
        
            List<User> UserEins = [Select Id, EIN__c, Department, Division from User where  Id in:campaignIds and isActive = true];
            for (User ui :UserEins) {
                 mUser.put(ui.Id, ui.ein__c ); //populate map EIN
                 mUserDept.put(ui.Id, ui.Department ); //populate map Dept
                 mUserDiv.put(ui.Id, ui.Division  ); //populate map Div
                           }
             
            for (Campaign c:Trigger.new){
          
                if (mUser.containsKey(c.OwnerId) == True) { 
                     c.Campaign_Owner_EIN__c = mUser.get(c.OwnerId);
                     c.Campaign_Owner_Department__c = mUserDept.get(c.OwnerId);
                     c.Campaign_Owner_Division__c = mUserDiv.get(c.OwnerId);
                }     
            }
            
        }
        
        //  DM - CR2360 update child campaigns with value from parent campaign 
        if(trigger.isUpdate){
            system.debug(!mParentCampaigns.isEmpty());        
            if(!mParentCampaigns.isEmpty()){
                //  on updating parent campaigns, retrieve children and update
                List<Campaign> campaignsToUpdate = new List<Campaign>();
                for(Campaign c : [select Id, IsActive, Status, Description, Chapter__c, Campaign_Feedback__c, Data_Source__c, ParentId, StartDate, EndDate, Parent.Status, Parent.Description, Parent.Chapter__c, Parent.StartDate, Parent.EndDate, parent.Data_Source__c, Parent.Campaign_Feedback__c
                                        from Campaign where IsActive = true and ParentId in :mParentCampaigns.keySet()]){
                    Campaign parent = mParentCampaigns.get(c.ParentId);
                    system.debug('Coming Here..');
                    if(c.Status != parent.Status 
                            || c.Description != parent.Description 
                            || c.Chapter__c != parent.Chapter__c 
                            || c.StartDate != parent.StartDate 
                            || c.EndDate != parent.EndDate
                            || c.Campaign_Feedback__c != parent.Campaign_Feedback__c || c.Data_Source__c != parent.Data_Source__c){ //  CR3617 - populating camp Feedback for Child records
                        c.Status = parent.Status;
                        system.debug('Coming Inside Here..');
                        c.Description = parent.Description;
                        /*CR4594 Change to campaigns (Data Source) Start*/
                        c.Data_Source__c = parent.Data_Source__c;
                        /*CR4594 Change to campaigns (Data Source) End*/
                        c.Chapter__c = parent.Chapter__c;
                        c.StartDate = parent.StartDate;
                        c.EndDate = parent.EndDate;
                        //  CR3617 - populating camp Feedback for Child records
                        c.Campaign_Feedback__c = parent.Campaign_Feedback__c; 
                        if(!parent.IsActive)
                            c.IsActive = false;
                        // changes end  
                        campaignsToUpdate.add(c);
                    }
        
                    if(campaignsToUpdate.size() == 1000){
                        update campaignsToUpdate;
                        campaignsToUpdate.clear();
                    }
                }
                if(!campaignsToUpdate.isEmpty())
                    update campaignsToUpdate;
            }
        }else{
            if(!parentCampaignIds.isEmpty()){
                //  on inserting children campaigns copy relevant values from parent - MARKETING CAMPAIGN ONLY
                Map<ID, Campaign> m = new Map<ID, Campaign>([select Status, Description, Chapter__c, ParentId, Data_Source__c, StartDate, EndDate, Campaign_Type__c, Campaign_Feedback__c 
                                                                        from Campaign 
                                                                        where Id in :parentCampaignIds 
                                                                                and Campaign_Type__c = :MARKETING_CAMPAIGN]);
                for(Campaign c: trigger.new){
                    if(m.containsKey(c.ParentId)){
                        Campaign parent = m.get(c.ParentId);
                        c.Status = parent.Status;
                        c.Description = parent.Description;
                        c.Chapter__c = parent.Chapter__c;
                        c.StartDate = parent.StartDate;
                        c.EndDate = parent.EndDate;
                        /*CR4594 Change to campaigns (Data Source) Start*/
                        c.Data_Source__c = parent.Data_Source__c;
                        /*CR4594 Change to campaigns (Data Source) End*/
                        c.Campaign_Type__c = parent.Campaign_Type__c;
                        c.Campaign_Feedback__c = parent.Campaign_Feedback__c;// CR3617 - populating camp Feedback for Child records
                    }
                }
            }
        }// DM - CR2360 - END
    }
}