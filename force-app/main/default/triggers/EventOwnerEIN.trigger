trigger EventOwnerEIN on Event (before insert, before update) {
	if(TriggerDeactivating__c.getInstance().Event__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }   
    Set<Id> ownerIds = new Set<Id>();
    for (Event e:Trigger.new){
        if (trigger.isupdate){
            if(trigger.oldmap.get(e.id).ownerid !=e.ownerid){
                    ownerIds.add(e.ownerid);      
            }
        }else{
                    ownerIds.add(e.ownerId);
        } 
    }
   
  //create list of ids of Event Owners
    if(ownerIds.size() >0){
        Map<Id, User> mUser = new Map<Id, User>();
        List<User> UserEins = [Select Id, EIN__c, Division, Department from User where  Id in:ownerIds and isActive = true];
        System.debug('GOV:EventOwnerEIN1');
        
        for (User ui :UserEins) {
             mUser.put(ui.Id, ui );//populate map
                       }
         
        for (Event e:Trigger.new){
      
            if (mUser.containsKey(e.OwnerId) == True) { 
                 e.Event_Owner_EIN__c = mUser.get(e.OwnerId).ein__c;
                 e.Owner_Division__c = mUser.get(e.OwnerId).Division;
                 e.Owner_Department__c = mUser.get(e.OwnerId).Department;
            }     
        }
    }
}