trigger ProvideLineAutoNumber on Provide_Line_Details__c (before insert, after delete, before update) {

    if(Trigger.isInsert){
    Provide_Line_Details__c[] PL = Trigger.New;
    Id CRFId = PL[0].Related_to_CRF__c;

    
    List<Provide_Line_Details__c> PLList = [Select Id,Name,Related_to_CRF__c from Provide_Line_Details__c where Related_to_CRF__c=:CRFId];
    if(PLList.size()>=0){
    string a = string.valueOf(PLList.size()+1);
    PL[0].Name=a;
    PL[0].Serial_No__c = integer.valueof(PL[0].Name);
    }
    }
    
   /* if(Trigger.isUpdate){
    Provide_Line_Details__c[] PLNew = Trigger.New;
    Provide_Line_Details__c[] PLOld = Trigger.Old;
    PLNew[0].Name = PLOld[0].Name;
    }*/
     if(Trigger.isDelete){
    Provide_Line_Details__c[] PL = Trigger.Old;
    Id CRFId = PL[0].Related_to_CRF__c;
    List<Provide_Line_Details__c> aList=[SELECT Id,Name FROM Provide_Line_Details__c WHERE Related_to_CRF__c=:CRFId ORDER BY Createddate ASC];
    if(aList.size()>0){
    Integer k = 0;
    for(Integer i=aList.size();i!=0;i--){
    aList[k].Name = string.valueOf(aList.size()-i+1); 
    aList[k].Serial_No__c = integer.valueOf(aList[k].Name);
    k++;       
    }
    update aList;
    }
    }
    
    if(Trigger.isUpdate){
    
    Provide_Line_Details__c[] PLNew = Trigger.New;
    Provide_Line_Details__c[] PLOld = Trigger.Old;
    Id CRFId = PLOld[0].Related_to_CRF__c;

    
    
    
    
    PLNew[0].Serial_No__c = integer.valueof(PLOld[0].Name);
    
    
    }
}