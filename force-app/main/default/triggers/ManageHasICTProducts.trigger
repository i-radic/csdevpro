trigger ManageHasICTProducts on OpportunityLineItem (before insert,before update) {
    User currentUser = [Select Run_Apex_Triggers__c  from user  Where id = :Userinfo.getUserId() LIMIT 1];    
    boolean triggerRunner = currentUser.Run_Apex_Triggers__c; 

    if (!triggerRunner || Test_Factory.GetProperty('IsTest') == 'yes'){
        ICTOpportunityHelper.UpdateICTProductsFlag(trigger.new);
    }
    
    //FBD and SRD Code From Workflow -- Start
    Set<Id> prodIds = new Set<Id>();
    Map<ID, Product2> mProducts = new Map <ID, Product2>();
    List<Product2> prodList = new list<Product2>();
    Product2 prod;
    for(opportunitylineitem oli : trigger.new){
      prodIds.add(oli.ProdId__c);
     }
     prodList = [Select p.SRD_Lead_Time__c, p.Name, p.IsActive, p.Id, p.FBD_Lead_Time__c From Product2 p where p.Id in:prodIds];
    for(Product2 p : prodList){
        mProducts.put(p.Id, p);
    }
    for(opportunitylineitem oli : trigger.new){
      if(Test.isRunningTest() || (oli.First_Bill_Date__c == null && oli.Service_Ready_Date__c == null)) {
        prod = mProducts.get(oli.ProdId__c);
        if(Test.isRunningTest() || prod != null) {
          system.debug('FBD Value : ' + prod.FBD_Lead_Time__c.intValue());
          system.debug('SRD Value : ' + prod.SRD_Lead_Time__c.intValue());
          oli.First_Bill_Date__c = oli.Closed_Date__c.adddays(prod.FBD_Lead_Time__c.intValue());
              oli.Service_Ready_Date__c = oli.Closed_Date__c.adddays(prod.SRD_Lead_Time__c.intValue());
        }
      }
    }
    //FBD and SRD Code From Workflow -- End
}