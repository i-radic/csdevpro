trigger OrderApprovalBefore on Order_Approval__c (before insert, before update)  {
	if(Trigger.isInsert){
		String channel = 'CORP'; String LBName = '';
		String appLevelUsed = 'Not Found';
		String L0approver = ''; String L1approver = ''; String L2approver = '';
        Id managerId; 
		User u = [SELECT Id, Department, Division, ManagerId FROM User WHERE id = :UserInfo.getUserId()];
		L0approver = UserInfo.getUserId();
        managerId = u.ManagerId;
        channel = u.Division;
        LBName = u.Department;
        
		//testing only
		if(channel == 'BTLB'){			
			List<Account_Team_Contact_BTLB__c> app = [SELECT Name__c, BTLB__r.Name, BTLB__r.Account_Owner__c,Specialist_Role__c 
                                                      FROM Account_Team_Contact_BTLB__c WHERE BTLB__r.Name = :LBName and Specialist_Role__c =:'Switch'];
			for(Account_Team_Contact_BTLB__c a:app){				
				L1approver = a.Name__c;
				L2approver = a.BTLB__r.Account_Owner__c;				
			}
		}
        else if(channel == 'Corporate' || channel == 'Innovate & Design'){
			channel = 'CORP';
		}
        else if(channel == 'BPS'){

		}		

		for(Order_Approval__c a:Trigger.New){
            a.Channel__c = channel;
            a.User_Department__c = u.Department;
            a.User_Division__c = u.Division;
            
            if(a.Type__c == 'Switch'){
                List<Order_Approval_Levels__c> levels = [SELECT Name, Journey__c, Channel__c, Care_Level__c, Care_Term__c, Value_Start__c, 
                                                 Value_End__c, Max_Discount__c, Approval_Level__c 
                                                FROM Order_Approval_Levels__c
                                                WHERE Channel__c = :channel
                                                ORDER BY Approval_Level__c DESC];
				Decimal appLevel = 0;
                
                for(Order_Approval_Levels__c al:levels){
                    if(al.Journey__c == 'Switch' && al.Care_Level__c == a.Qualifying_Care_Level__c && 
                    al.Care_Term__c == a.Qualifying_Contract_Term__c && al.Value_Start__c <= a.Discountable_Initial__c && 
                    al.Value_End__c >= a.Discountable_Initial__c && al.Max_Discount__c >= a.Requested_Initial_PCT__c)
                    {
                        system.debug('jmm al.Max_Discount__c:'+al.Max_Discount__c+' GTE '+a.Requested_Initial_PCT__c);
                        appLevel = al.Approval_Level__c;
                        appLevelUsed = al.Name;
                    }
                }                
                a.Level_Required__c = appLevel;
                a.Criteria__c = appLevelUsed;                
                if(appLevel == 0){
                    a.Approver__c = L0approver;
                }else if(appLevel == 1 && L1approver!=''){
                    a.Approver__c = L1approver;
                }else if(appLevel == 2 && L2approver!=''){
                    a.Approver__c = L2approver;
                }                
                if(channel == 'BTLB' && L1approver!='' && L2approver!=''){
                    a.Approver_1__c = L1approver;
                    a.Approver_2__c = L2approver;
                }
            }
            else if(a.Type__c == 'BTnet'){
                if(channel == 'BTLB'){
                    a.Approver__c = managerId;
                    a.Approver_1__c = managerId;
                    //a.Approver_2__c = Ian Goldingay;// assigned directly from the Approval process
                }
                else if(channel == 'CORP'){
                    //a.Approver_1__c = Simon Baker;// assigned directly from the Approval process
                }
                else if(channel == 'BPS'){
                    //
                }
            }
		}
	}

	if(Trigger.isUpdate){
		for(Order_Approval__c a:Trigger.New){
			if(a.Status__c == 'Not Submitted' && a.Reason_for_Request__c != ''){
				a.Status__c = 'Pending';
			}
		}
	}
}