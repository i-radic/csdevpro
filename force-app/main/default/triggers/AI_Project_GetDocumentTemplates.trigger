trigger AI_Project_GetDocumentTemplates on Project__c (after insert) {
    if (TriggerUtils.hasSkipReason('AI_Project_GetDocumentTemplates')) {
        return;
    }

    List<Project__c> projects = trigger.new;
    ProjectTriggerUtilities.createProjectTemplates(projects);
}