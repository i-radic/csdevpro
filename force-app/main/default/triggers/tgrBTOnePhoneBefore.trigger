trigger tgrBTOnePhoneBefore on BT_One_Phone__c (before insert, before update) {      
    BusinessHours BOBusinessHours = [select id from BusinessHours where name = '86MF'];
    datetime myDateTime = datetime.now();
    for (BT_One_Phone__c t : Trigger.new) {
        t.Escalation_Order__c  = BusinessHours.addGmt (BOBusinessHours.id, myDateTime, 4 * 60 * 60 * 1000L);        
    }
    if(Trigger.isUpdate){
        if(BTOnePhoneTriggerHandler.BTOnePhoneTriggerBeforeHandleRecusion){
            BTOnePhoneTriggerHandler.beforeUpdate(Trigger.NewMap, Trigger.OldMap);
            BTOnePhoneTriggerHandler.BTOnePhoneTriggerBeforeHandleRecusion = False;
        }
    }
}