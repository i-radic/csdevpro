trigger CaseAfterUpdateInsert on Case (After update, After insert) {
id permSet = [SELECT Id FROM PermissionSet WHERE name = 'Salesforce_Mobile_App' ].id;     //Mobile permission set
id userID = userinfo.getUserId();
//id rectypeID = [SELECT id from recordtype where developername = 'Mobile_App'].id;         //Id of Salesforce Mobile record type
// Commented above code and hard coded the recordtype ID as test class is failing with Too many SOQL Queries
id rectypeID = '0120J0000003HUpQAM';
string assignID;    //ID of user to have perm assigned  
    
    if(Trigger.isUpdate){
    //Execute when update is updating app access
        for (case x : trigger.old){
            //Execute statements if case is for Salesforce Mobile
            if (x.recordtypeid == rectypeid){ 
                //Take id of record to change - not trigger creator
                assignId = x.created_by_id__c;
                //Handle changes if old and new mobile number are different but not null
                if(Case.Old_Mobile_Number__c != Case.Mobile_App_Number__c && Case.Mobile_App_Number__c != Null){
                    for (case i : trigger.new){
                    //Only change if case is approved
                    if(i.Mobile_App__c==True && i.Mobile_App_Number__c != Null){
                        //Update user record
                         List<user> userList = [SELECT id, SF_Mobile_Number__c from user WHERE id =: assignID LIMIT 1];
                         userList[0].SF_Mobile_Number__c = i.Mobile_App_Number__c;
                         try{
                             update userList;
                         }
                         catch(system.dmlexception e){
                             system.debug( e );}
                     //Check if Permission set needs applied
                     List<permissionSetAssignment> assignedPerm = [SELECT id from permissionSetAssignment where assigneeid =: assignID and permissionSetId =: permSet Limit 1]; //Permission set Assignment ID              
                     if(assignedPerm.isEmpty()){   
                         assignPermSet.insertPermRecord(permSet,assignID);
                     }
                    }
               }
            }
        } 
    }     
}
    
if(Trigger.isInsert){
        
        for (case x : trigger.new){
            if (x.recordtypeid == rectypeid){
                //Check for new mobile number as unique
                if (x.Mobile_App_Number__c != null){
                    List<user> caseUser = [Select SF_Mobile_Number__c from user where SF_Mobile_Number__c =: x.Mobile_App_Number__c];
                    if (caseUser.isEmpty()==false){
                        Trigger.new[0].addError('This number is already being used by another user. Please enter a different number.'); 
                    }
                }
                
                if (x.Mobile_App_Number__c != null ){       
                    //Populate previous detail fields
                    List<case> newCase = [Select id,Old_Mobile_Number__c from case where id =: x.id];
                    List<user> caseUser = [Select SF_Mobile_Number__c from user where id =: userId];
                    newCase[0].Old_Mobile_Number__c = caseUser[0].SF_Mobile_Number__c;
                    update newCase;
                    
                    //Check users next three managers for active user
                    //Gets current user details
                    List<user> appUser = [select id, name, Manager.id from user where id =: userID];
                    id mgrID = appUser[0].manager.id; 
                    if(mgrID != null){
                        //Gets managers details
                        List<user> manager =  [select id, isActive,manager.id from user where id =: mgrId];
                        //Check three managers up for active manager
                        for (integer i=0;i<=1;i++){
                            if (manager[0].IsActive == False){
                                mgrID = manager[0].manager.id;
                                manager =  [select id, isActive, manager.id from user where id =: mgrId];
                                if(i==1 && manager[0].IsActive == False){
                                    mgrID = [select id from user where name = 'Gareth James' AND EIN__c = '611700986'].id ;
                                }
                            }
                        }
                    }
                    //If user has null manager then send email to admin
                    else if(mgrID == null){
                        mgrID = [select id from user where name = 'Gareth James' AND EIN__c = '611700986'].id ;
                    }
                    
                    List<case> c = [select Mobile_Case_Approver__c from case where id =: x.id limit 1];
                    List<group> g = [select id from group where Name = 'MobileApp' Limit 1];
                    c[0].Mobile_Case_Approver__c = mgrID; 
                    c[0].ownerID = g[0].id;
                    update c; 
                    
                    //Send for approval to first active manager        
                    Approval.ProcessSubmitRequest a = new Approval.ProcessSubmitRequest(); 
                    a.setComments('Submitting request for approval.');
                    a.setObjectId(x.Id);
                    a.setNextApproverIds(new id[] {mgrId} );  
                    Approval.ProcessResult result = Approval.process(a);
                }
            }
        }
    }

    
    
}