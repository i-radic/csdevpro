trigger BTSportSiteAttachments on Attachment (after insert, after update, after delete) {

if (Trigger.isInsert || Trigger.isUpdate){
		for (Attachment at:Trigger.new){
        String thisID = at.ParentID;
      
        
	     if (thisID.startsWith('a6A')){  //count attachments on header
	        List <aggregateResult> groupedResults = [SELECT ParentId, count(id) FROM Attachment where ParentId = :thisID AND Name LIKE 'Order%' GROUP BY ParentId ];
	        
			if(groupedResults.size()>0){

	        BT_Sport_Site__c btsa = [Select id, Order_Confirmations__c From BT_Sport_Site__c where id = :at.ParentID ];
	             btsa.Order_Confirmations__c = double.valueof(groupedResults[0].get('expr0')) ;
	                update btsa;
	         }
	         }


	    
  		} 
	}else if (Trigger.isDelete){
		for (Attachment at:Trigger.old){
			String thisID = at.ParentID;

			if (thisID.startsWith('a6A')){  //count attachments on header
	        	List <aggregateResult> groupedResults = [SELECT ParentId, count(id) FROM Attachment where ParentId = :thisID AND Name LIKE 'Order%' GROUP BY ParentId ];
	        
	        	BT_Sport_Site__c btsa = [Select id, Order_Confirmations__c From BT_Sport_Site__c where id = :at.ParentID];
	        	
	        	if (groupedResults.size() > 0){
	        		btsa.Order_Confirmations__c = double.valueof(groupedResults[0].get('expr0'));
	        	}else{
	        		btsa.Order_Confirmations__c = 0;	
	        	}
	            
	            update btsa;
	         }

	     	
			
		}	
}
}