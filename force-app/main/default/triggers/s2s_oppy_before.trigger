trigger s2s_oppy_before on s2s_Opportunities__c (before insert, before update) {
if (StaticVariables.gets2sOpportunityDontRun())return;
//populate object with required fields
 if (userinfo.getname() == 'Connection User')return;
set<string> s2sList = new Set<string>();
for (s2s_Opportunities__c o:Trigger.new) {
system.debug('ADJ UserGetName' + userinfo.getname());
      s2sList.add(o.Opp_ID__c);
    }
    
//create a few maps to hold the values
    Map<String, Date> oppCloseDate = new Map<String, Date>();
    Map<String, Decimal> oppAmount = new Map<String, Decimal>();
    Map<String, String> oppDescription = new Map<String, String>();
    Map<String, String> oppOwnerName = new Map<String, String>();
    Map<String, String> oppOwnerEmail = new Map<String, String>();
    Map<String, String> oppOwnerPhone = new Map<String, String>();
    Map<String, String> oppStageName = new Map<String, String>();
    Map<String, String> oppName = new Map<String, String>();
    Map<String, String> oppOpportunityID = new Map<String, String>();
    Map<String, String> oppOppID = new Map<String, String>();
    Map<String, String> oppAccOwn = new Map<String, String>();
    Map<String, String> oppAcc = new Map<String, Id>();
    Map<String, String> oppCon = new Map<String, Id>();
    Map<String, String> oppLink = new Map<String, String>();
    Map<String, String> oppOvSum = new Map<String, String>();
  
// retrun the opp information  
List<Opportunity> oppys = [select Id, Opportunity_ID__c, CloseDate, Amount, Description, StageName, Name,  Owner.Phone, 
                        Owner.Email, Owner.Name, Account.Id, Account.Owner.Name, s2s_Link__c, Overall_Summary__c FROM Opportunity where id = :s2sList];                      
// return the primary contact for the opp info                        
List<OpportunityContactRole> oppCons = [SELECT ContactId, OpportunityId  FROM OpportunityContactRole WHERE OpportunityId = :s2sList AND IsPrimary = True];                        

for (OpportunityContactRole opCon :oppCons) {
    oppCon.put(opCon.OpportunityId,opCon.ContactId);
}   
 
for (Opportunity op :oppys) {
    oppCloseDate.put(op.ID,op.CloseDate); 
    oppAmount.put(op.ID,op.Amount); 
    oppDescription.put(op.ID,op.Description); 
    oppOwnerName.put(op.ID,(op.Owner.Name)); 
    oppOwnerEmail.put(op.ID,op.Owner.Email); 
    oppOwnerPhone.put(op.ID,op.Owner.Phone); 
    oppStageName.put(op.ID,op.StageName);
    oppName.put(op.ID,op.Name); 
    oppOpportunityID.put(op.ID,op.Opportunity_ID__c); 
    oppOppID.put(op.ID,op.ID); 
    oppAcc.put(op.ID,op.Account.Id); 
    oppAccOwn.put(op.ID,op.Account.Owner.Name);
    oppLink.put(op.ID,op.s2s_Link__c);
    oppOvSum.put(op.ID,op.Overall_Summary__c);
    }
       
//Populate / update values       
for (s2s_Opportunities__c o:Trigger.new) {  
  o.Amount__c =  oppAmount.get(o.Opp_ID__c);  
  o.title__c =  oppName.get(o.Opp_ID__c);  
  o.Status__c =  oppStageName.get(o.Opp_ID__c);
  o.Close_Date__c = oppCloseDate.get(o.Opp_ID__c);
  if (oppDescription.get(o.Opp_ID__c) != Null){
      o.Description__c = oppDescription.get(o.Opp_ID__c);
  }
  else {
      o.Description__c = oppOvSum.get(o.Opp_ID__c);
  }
  o.Owner_email__c =oppOwnerEmail.get(o.Opp_ID__c);
  o.Owner_name__c = oppOwnerName.get(o.Opp_ID__c);
  o.Owner_Phone__c = oppOwnerPhone.get(o.Opp_ID__c);
  o.Opportunity_ID__c = oppOpportunityID.get(o.Opp_ID__c);
  o.Opp_ID__c = oppOppID.get(o.Opp_ID__c);
  o.Opportunity__c = oppOppID.get(o.Opp_ID__c);
  o.linked_Org__c = oppLink.get(o.Opp_ID__c);
  o.Account__c = oppAcc.get(o.Opp_ID__c);
  o.Contact__c = oppCon.get(o.Opp_ID__c);
  o.AccountOwner__c = oppAccOwn.get(o.Opp_ID__c);
}


}