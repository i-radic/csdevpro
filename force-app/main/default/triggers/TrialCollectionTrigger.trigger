trigger TrialCollectionTrigger on Trial_Collection__c (after insert) {
	//Send collection request emails
	TrialCollectionHelper aTrialCollectionHelper = new TrialCollectionHelper();
	aTrialCollectionHelper.SendCollectionRequestEmail(Trigger.new);
}