trigger BTLBSuiteOrderLineAfter on BTLB_Suite_Order_Lines__c (after Insert, after Update) {
	Set<String> delIds = New Set<String>();
    List<BTLB_Suite_Commission__c> newRecs = new List<BTLB_Suite_Commission__c>();  

    for(BTLB_Suite_Order_Lines__c t:Trigger.New){ 
		delIds.add(t.Id);
	}
	List<BTLB_Suite_Commission__c> delRecs = [SELECT Id FROM BTLB_Suite_Commission__c WHERE BTLB_Suite_Order_Lines__c= :delIds];
	delete delRecs;

    for(BTLB_Suite_Order_Lines__c t:Trigger.New){ 
        if(t.Commission_Agent_1__c != null){
			BTLB_Suite_Commission__c newRec = New BTLB_Suite_Commission__c();
            newRec.Agent__c = t.Commission_Agent_1__c;
            newRec.BTLB_Suite_Order_Lines__c = t.Id;
            newRec.Indicative_Commission_Value__c = t.Indicative_Commission_Value_1__c;
            newRec.Indicative_Quantity_Value__c = t.Indicative_Quantity_Value_1__c;
            newRec.Indicative_SOV_Value__c = t.Indicative_SOV_Value_1__c;
			if(t.Referrer__c == t.Commission_Agent_1__c){
				newRec.Referral__c = 1;
			}
            newRecs.add(newRec);
        }
        if(t.Commission_Agent_2__c != null){
			BTLB_Suite_Commission__c newRec = New BTLB_Suite_Commission__c();
            newRec.Agent__c = t.Commission_Agent_2__c;
            newRec.BTLB_Suite_Order_Lines__c = t.Id;
            newRec.Indicative_Commission_Value__c = t.Indicative_Commission_Value_2__c;
            newRec.Indicative_Quantity_Value__c = t.Indicative_Quantity_Value_2__c;
            newRec.Indicative_SOV_Value__c = t.Indicative_SOV_Value_2__c;
			if(t.Referrer__c == t.Commission_Agent_2__c){
				newRec.Referral__c = 1;
			}
            newRecs.add(newRec);
        }
        if(t.Commission_Agent_3__c != null){
			BTLB_Suite_Commission__c newRec = New BTLB_Suite_Commission__c();
            newRec.Agent__c = t.Commission_Agent_3__c;
            newRec.BTLB_Suite_Order_Lines__c = t.Id;
            newRec.Indicative_Commission_Value__c = t.Indicative_Commission_Value_3__c;
            newRec.Indicative_Quantity_Value__c = t.Indicative_Quantity_Value_3__c;
            newRec.Indicative_SOV_Value__c = t.Indicative_SOV_Value_3__c;
			if(t.Referrer__c == t.Commission_Agent_3__c){
				newRec.Referral__c = 1;
			}
            newRecs.add(newRec);
        }
        if(t.Commission_Agent_4__c != null){
			BTLB_Suite_Commission__c newRec = New BTLB_Suite_Commission__c();
            newRec.Agent__c = t.Commission_Agent_4__c;
            newRec.BTLB_Suite_Order_Lines__c = t.Id;
            newRec.Indicative_Commission_Value__c = t.Indicative_Commission_Value_4__c;
            newRec.Indicative_Quantity_Value__c = t.Indicative_Quantity_Value_4__c;
            newRec.Indicative_SOV_Value__c = t.Indicative_SOV_Value_4__c;
			if(t.Referrer__c == t.Commission_Agent_4__c){
				newRec.Referral__c = 1;
			}
            newRecs.add(newRec);
        }
        if(t.Referrer__c != null && t.Referrer__c != t.Commission_Agent_1__c && t.Referrer__c != t.Commission_Agent_2__c && t.Referrer__c != t.Commission_Agent_3__c && t.Referrer__c != t.Commission_Agent_4__c){
			BTLB_Suite_Commission__c newRec = New BTLB_Suite_Commission__c();
            newRec.Agent__c = t.Referrer__c;
            newRec.Referral__c = 1;
            newRec.BTLB_Suite_Order_Lines__c = t.Id;
            newRecs.add(newRec);
        }
    }
    insert newRecs;
}