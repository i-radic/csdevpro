trigger tgrContactBefore on Contact (before insert, before update) {
    
/****************************************************************************************************************
    History                                                            
    -------  
 Date            Author          Comment
13 Aug 2010     Alan Jackson    renames from linkContactTo Account
                            updates to use CUG ID to link to Account 
28 Sept 2010    Jon Brayshaw    Added query to check to see if more than one contact has NOP_Survey_Contact set
28 Sept 2010    Alan Jackson    Added code to populate postCode on Account if present on Contact but missing on Account
06  Oct 2010    Alan JAckson    only run postcode allocation if contact created / updated by a user ( non Admin or DataLoad profiles)
23  Oct 2010    Alan JAckson    added dataloader2 profile to exception
07  Dec 2010    Alan JAckson    added code to populate phone2 & email2
13 Dec 2010     Jon Brayshaw    added test to ensure that it does not run for a record time 
27 Jun 2012     Alan Jackson    excluded admin & dataloader from NOP contact check 
17 Jun 2013     Alan Jackson    comment out - Ownership_assigned_by_Contact__c = true
18 Nov 2013     Dilip Singh     added - validation rule for Phone number avoiding alphabets
12 Aug 2014     Venkata Srinivas Maddirala   Change for CR5797-Deleted validation rule to ensure that only one NOP_Survey_Contact per account  
21 Aug 2014     Alan JAckson    Re inserted code overwritten to update Eloqua Address fields
02 Dec 2016     Adam Soobroy    CR9185 - Reparenting Contact Records

  */
  if(TriggerDeactivating__c.getInstance().Contact__c) {
         System.debug('Bypassing trigger due to custom setting');
         return;
     }    
    
    String AdministratorId = '00e20000001MX7z';
    String DataLoader1 = '00e20000001Q9wI'; //BT: Dataloader
    String DataLoader2 = '00e200000015Ehz';// BT: DataLoader2
    id Profile1 = userinfo.getProfileId();

     if (trigger.isDelete) {
        return;
    }
    
    if(StaticVariables.getcontactBeforeTriggerRun())return;
    
    StaticVariables.setcontactBeforeTriggerRun(true);
    
    
    for(Contact c:Trigger.new){  
        c.email2__c = c.email;
        c.phone2__c = c.phone;
        c.email2_2nd__c = c.email_2nd__c;
        c.email2_3rd__c = c.email_3rd__c;
        
        //Copy email to hidden field if consent is 'TPS' or 'No' and replace email with dummy value. This is to prevent BTLB's from sending campaign emails to customers who have elected to not receive such emails.            
        
         if (c.DL_Contact_Sector__c != 'Corporate' && c.DL_Contact_Sector__c != 'Major and Public Sector' && c.DL_Contact_Sector__c != 'ROI Business' &&
             c.DL_Contact_Sector__c != 'ROI Wholesale' && c.DL_Contact_Sector__c != 'NI Business' && c.DL_Contact_Sector__c != 'NI DBAM' ) {
            if (c.Email != 'DoNotContact@bt.co.uk' && c.Email_Consent__c == 'TPS' || c.Email_Consent__c == 'No') {
                c.emailHidden__c = c.Email;
                c.Email = 'DoNotContact@bt.co.uk';
            }if (c.Email_2nd__c != 'DoNotContact@bt.co.uk' && c.Email_Consent__c == 'TPS' || c.Email_Consent__c == 'No') {
                c.emailHidden_2nd__c = c.Email_2nd__c;
                c.Email_2nd__c = 'DoNotContact@bt.co.uk';
            }if (c.Email_3rd__c != 'DoNotContact@bt.co.uk' && c.Email_Consent__c == 'TPS' || c.Email_Consent__c == 'No') {
                c.emailHidden_3rd__c = c.Email_3rd__c;
                c.Email_3rd__c = 'DoNotContact@bt.co.uk';
            } 
        }
        
        if (Profile1 != AdministratorId && Profile1 != DataLoader1 && Profile1 != DataLoader2){ //CR9185 - Reparenting Contact Records
      /* Validation Rule for Phone Number */
      if(c.Eloqua_Prime_Flag__c != 'R' && c.Phone != null && c.Phone != '' && !Test.isRunningTest()){
        if(!((c.Phone.startsWith('0') || c.Phone.startsWith('+')) && c.Phone.length() <= 14)) {
          c.addError('Please enter a valid Phone Number');
        }             
        if (!Pattern.matches('[0-9]+', c.Phone.substring(1,c.Phone.length()))) {
          c.addError('Please enter a valid Phone Number');
        }
      }
    }
    }       
    
    if (Profile1 != AdministratorId && Profile1 != DataLoader1 && Profile1 != DataLoader2){
    
        Date myDate = Date.today();
        set<string> setPostCodeAccounts = new Set<string>(); 
        Map<String, String> mapPostCodeAccounts = new Map<String, String>();
            for(Contact ac:Trigger.new){   // get list of Contacts being updated
                if (ac.Contact_Post_Code__c != null){
                setPostCodeAccounts.add(ac.AccountId);
                mapPostCodeAccounts.put(ac.AccountId, ac.Contact_Post_Code__c);
                }
            }
        
        List<Account> findAccountsToUpdate = [Select Id, Postcode__c, Ownership_assigned_by_Contact__c from Account where PostCode__c = null and ID in :setPostCodeAccounts]; // get list of Account associated with contacts being update where current Postcode is null
        if(findAccountsToUpdate.size() > 0){
        
        List<Account> AccountsToUpdate = new List<Account>(); // create list to populate LE details if owning SAC
                
            for (Account a :findAccountsToUpdate) {
            a.PostCode__c = mapPostCodeAccounts.get(a.ID);
            //a.Ownership_assigned_by_Contact__c = true;// commented out as falsly assigning enterprises customers to BTLBs
            AccountsToUpdate.add(a); 
            }
    
            try{
                if(AccountsToUpdate.size() > 0) update AccountsToUpdate ;
            }
            catch (System.Dmlexception e) { 
                System.Debug('####################Error, populate LE details if owning SAC: ' + e);
            }       
       // }   
     }  
    }
        
        List<User> ApexTriggerEnabled = [Select Run_Apex_Triggers__c  from user  Where id = :Userinfo.getUserId()];
        boolean triggerRunner = ApexTriggerEnabled[0].Run_Apex_Triggers__c;
    
        if (triggerRunner == true || Test_Factory.GetProperty('IsTest') == 'yes') { // only trigger if dataload user is updating records
        
        if (triggerRunner == true){ 
        StaticVariables.setContactIsRunBefore(true); // stop contactAfter trigger running if dataloading
        }
            //populate set with CUG ids contained in batch ( or single)
            set<string> CUGList = new Set<string>();
            for(Contact c:Trigger.new){
                if (c.CUG__c != NULL){
                CUGList.add(c.CUG__c);
                }
            }
            // create MAP of CUG to account ID mapping and populate
            Map<String, String> CUGMapping = new Map<String, String>();
            if (CUGList.isEmpty() == False){
                List<Account> CUGIds = [Select a.Id, a.CUG__c from Account a where a.CUG__c in :CUGList];
                for (Account si :CUGIds){
                    CUGMapping.put(si.CUG__c,si.Id);//populate map
                }
             }
            
            for(Contact c:Trigger.new){  
                 if (CUGMapping.containsKey(c.CUG__c) == TRUE) {   
                    c.AccountId = CUGMapping.get(c.CUG__c);              
            } 
        }
    }  

///Elequa - populate specifice eloqua contact fields using rules below
        for(Contact c :Trigger.new){
            if(Trigger.isUpdate){
            if (c.Contact_Post_Code__c != null && (trigger.oldMap.get(c.id).Contact_Post_Code__c != c.Contact_Post_Code__c)){
          
                    c.ELQ_Address_Update_Source__c = '4';
                    c.ELQ_Contact_Building_Name__c  = c.Contact_Building__c;
                    c.ELQ_Contact_Sub_Building__c = c.Contact_Sub_Building__c;
                    c.ELQ_Contact_Number__c = c.Contact_Address_Number__c;
                    c.ELQ_Contact_Locality__c = c.Contact_Locality__c;
                    c.ELQ_Contact_PO_Box__c = c.Address_POBox__c;
                    c.ELQ_Contact_Street__c = c.Contact_Address_Street__c;
                    c.ELQ_Contact_Post_Town__c = c.Contact_Post_Town__c;
                    c.ELQ_Contact_County__c = c.Address_County__c;
                    c.ELQ_Contact_Post_Code__c =c.Contact_Post_Code__c;
                    c.ELQ_Contact_Country__c = c.Address_Country__c;

            // rules to counter data in incorrect fields

                            If (c.ELQ_Contact_Building_Name__c == c.Account_Name_Formula__c){
                                c.ELQ_Contact_Building_Name__c = null;}
                            If (c.ELQ_Contact_Building_Name__c == c.ELQ_Contact_Sub_Building__c){
                                c.ELQ_Contact_Sub_Building__c = null;}
                            If (c.ELQ_Contact_Building_Name__c == c.ELQ_Contact_Number__c){
                                c.ELQ_Contact_Number__c = null;}
                            If (c.ELQ_Contact_Building_Name__c == c.ELQ_Contact_Locality__c){
                                c.ELQ_Contact_Locality__c = null;
                            }
                            If (c.ELQ_Contact_Building_Name__c == c.ELQ_Contact_Street__c){
                                c.ELQ_Contact_Street__c = null;
                            }
                            If (c.ELQ_Contact_Building_Name__c == c.ELQ_Contact_Post_Town__c){
                                c.ELQ_Contact_Post_Town__c = null;
                            }
                            If (c.ELQ_Contact_Building_Name__c == c.ELQ_Contact_Country__c){
                                c.ELQ_Contact_Country__c = null;
                            }

                            If (c.ELQ_Contact_Sub_Building__c == c.ELQ_Contact_Number__c){
                                c.ELQ_Contact_Number__c = null;
                            }
                            If (c.ELQ_Contact_Sub_Building__c == c.ELQ_Contact_Locality__c){
                                c.ELQ_Contact_Locality__c = null;
                            }
                            If (c.ELQ_Contact_Sub_Building__c == c.ELQ_Contact_Street__c){
                                c.ELQ_Contact_Street__c = null;
                            }
                            If (c.ELQ_Contact_Sub_Building__c == c.ELQ_Contact_Post_Town__c){
                                c.ELQ_Contact_Post_Town__c = null;
                            }
                            If (c.ELQ_Contact_Sub_Building__c == c.ELQ_Contact_Country__c){
                                c.ELQ_Contact_Country__c = null;
                            }


                            If (c.ELQ_Contact_Number__c == c.ELQ_Contact_Locality__c){
                                c.ELQ_Contact_Locality__c = null;
                            }
                            If (c.ELQ_Contact_Number__c == c.ELQ_Contact_Street__c){
                                c.ELQ_Contact_Street__c = null;
                            }
                            If (c.ELQ_Contact_Number__c == c.ELQ_Contact_Post_Town__c){
                                c.ELQ_Contact_Post_Town__c = null;
                            }
                            If (c.ELQ_Contact_Number__c == c.ELQ_Contact_Country__c){
                                c.ELQ_Contact_Country__c = null;
                            }

                            If (c.ELQ_Contact_Locality__c == c.ELQ_Contact_Street__c){
                                c.ELQ_Contact_Street__c = null;
                            }
                            If (c.ELQ_Contact_Locality__c == c.ELQ_Contact_Post_Town__c){
                                c.ELQ_Contact_Post_Town__c = null;
                            }
                            If (c.ELQ_Contact_Locality__c == c.ELQ_Contact_Country__c){
                                c.ELQ_Contact_Country__c = null;
                            }


                            If (c.ELQ_Contact_Street__c == c.ELQ_Contact_Post_Town__c){
                                c.ELQ_Contact_Post_Town__c = null;
                            }
                            If (c.ELQ_Contact_Street__c == c.ELQ_Contact_Country__c){
                                c.ELQ_Contact_Country__c = null;
                            }


                            If (c.ELQ_Contact_Post_Town__c == c.ELQ_Contact_Country__c){
                                c.ELQ_Contact_Country__c = null;
                            }    
                       }
                   }
            }




}