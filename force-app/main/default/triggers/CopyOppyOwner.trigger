// For each opportunity copy the owner ID into a field that we can reference in formulas.

trigger CopyOppyOwner on Opportunity (before insert, before update) {
    Map<Id,String> oppToBasketMap = new Map<Id,String>();
    Map<String,String> basketIdToStatusMap = new Map<String,String>();
    Map<String,String> basketIdToBTApprovalStatusMap = new Map<String,String>();
    for (Opportunity iOppy : Trigger.new) {
        iOppy.Owner_Link__c = iOppy.OwnerID;
        if(Trigger.isUpdate){
           oppToBasketMap.put(iOppy.id,iOppy.Basket_ID__c);
        }
    }
    /*List<cscfga__Product_Basket__c> basketList = [Select Id, Basket_ID__c ,Approval_Status__c,BT_Basket_Approval_Status__c from cscfga__Product_Basket__c where Basket_ID__c in :oppToBasketMap.values()];
    for(cscfga__Product_Basket__c b :basketList){
        basketIdToStatusMap.put(b.Basket_ID__c,b.Approval_Status__c);
        basketIdToBTApprovalStatusMap.put(b.Basket_ID__c,b.BT_Basket_Approval_Status__c);
    } system.debug('**basketIdToBTApprovalStatusMap'+basketIdToBTApprovalStatusMap);
    for(Opportunity opp : Trigger.new){
        String basketId = oppToBasketMap.get(opp.id);
        if(basketIdToStatusMap.get(basketId)== 'Approved' || basketIdToBTApprovalStatusMap.get(basketId) == 'Approved'){
            opp.Deal_Approved_in_CloudSense__c = 'Yes';
        }else{
            opp.Deal_Approved_in_CloudSense__c = 'No';
        } 
    }*/
    
}