// History
//
//  Date            Author          Comments
//  14-07-2009      Alan Jackson    Initial version link line count to Account
//  15-10-2010      Alan Jackson    added static variable to prevent account trigger running

trigger AssetLinesCountLoad on Asset_Line_Count__c (before insert, before update){
public Integer aSize = 0;
public Account account = null;
public Account account2 = null;

StaticVariables.setAccountDontRun(true); 

// get ID of Dummy Account
 List<Account> tAccountId2 = [Select a.Id from Account a where sac_code__c = 'Dummy999'];
 account2 = tAccountId2[0];

//create list of accounts associated with lines data being updated
set<string> sacsList = new Set<string>();
for(Asset_Line_Count__c l:Trigger.new)
sacsList.add(l.sac_code__c);

//create map to hold SAC to ID mappings
Map<String, String> SacMapping = new Map<String, String>();
// read in values
List<Account> acountIds = [Select a.Id, a.sac_code__c from Account a where sac_code__c in :sacsList];
for (Account ai :acountIds)
SacMapping.put(ai.sac_code__c,ai.Id);//populate map

//create map to hold dates associated with updates
Map<String, Date> SacDates = new Map<String, Date>();
// read in values
for(Asset_Line_Count__c l:Trigger.new)
SacDates.put(l.sac_code__c,l.Month__c);//populate map

//create map to hold SAC to line Count mappings
Map<String, Double> AcctLineCount = new Map<String, Double>();
for(Asset_Line_Count__c l:Trigger.new)
AcctLineCount.put(l.sac_code__c,l.Total_Lines__c);//populate map

for(Asset_Line_Count__c l:Trigger.new)
    { 
    if(SacMapping.get(l.sac_code__c) != null)         // if SAC code is recognised allocate to that
        l.Account__c = SacMapping.get(l.sac_code__c); // that Account ID
        
    else                                             // link contract to Dummy Account ID   
        l.Account__c = account2.Id;
     }    
  

  //update Account object with total lines count
/**Account[] accts = [select  sac_code__c from Account where sac_code__c in :sacsList];
for (Account a : accts){
    a.Lines_Count_Previous__c = a.Lines_Count_Total__c;
    a.Lines_Count_Total__c = AcctLineCount.get(a.sac_code__c);
    a.Lines_Count_Date__c = SacDates.get(a.sac_code__c);  
}
update accts; */
}