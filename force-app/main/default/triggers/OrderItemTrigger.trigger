trigger OrderItemTrigger on OrderItem (after insert, after update, before insert, before update, before delete) {


	OrderItemTriggerHandler handler = new OrderItemTriggerHandler(
								Trigger.newMap, Trigger.oldMap, Trigger.new, Trigger.old,
								Trigger.Size,
								Trigger.IsInsert, Trigger.IsUpdate, Trigger.IsBefore, Trigger.IsAfter,
								Trigger.IsExecuting,
								Trigger.IsDelete	);

	// Execute if Trigger Enabler custom setting is enabled
    if (EE_TriggerClass.isTriggerEnabled('OrderItem')) {
    	
		handler.processTrigger();
		
    }


}