trigger tgrAttachmentAfter on Attachment (after insert, after update, after delete, after undelete) {
    // List of attachment record ids to update
    Set<Id> attIds = new Set<Id>();
    // List of parent record ids to update
    Set<Id> cssIds = new Set<Id>();
    ////SuppRequestIDs
    Set<Id> suppReqIds = new Set<Id>();
    // In-memory copy of parent records
    Map<Id,Customer_Sales_Support__c> cssRecords = new Map<Id, Customer_Sales_Support__c>();
    ////Copy of Support Request records
    Map<Id,Support_Request__c> suppReqRecords = new Map<Id, Support_Request__c>();
    // Gather the list of ID values to query on
    for(Attachment a : Trigger.isDelete?Trigger.old:Trigger.new) {
        system.debug('Attachment Record Id : ' + a.Id);
        attIds.add(a.Id);
    }
    // Avoid null ID values
    attIds.remove(null);
    // Get Only Customer Support Record as ParentId's
    for(Attachment a:[SELECT Id, ParentId, Parent.Type FROM Attachment where Id IN :attIds]) {
        system.debug('Parent Record Type : ' + a.Parent.Type);
        system.debug('Parent Record Id : ' + a.ParentId);
        if(a.Parent.Type == 'Customer_Sales_Support__c') {
            cssIds.add(a.ParentId);
        }
        //// Check for attachments on the Support Request Record
        if(a.Parent.Type == 'Support_Request__c') {
            suppReqIds.add(a.ParentId);
        }
    }
    // Avoid null ID values
    cssIds.remove(null);  
    ///// Avoid null ID values
    suppReqIds.remove(null);    
    // Create in-memory copy of parents     
    for(Id cssId:cssIds) {
        cssRecords.put(cssId,new Customer_Sales_Support__c(Id=cssId,Attachments_Count__c=0));
    }
    //// Create in-memory copy of parents     
    for(Id suppReqId:suppReqIds) {
        suppReqRecords.put(suppReqId,new Support_Request__c(Id=suppReqId,Attachments_Count__c=0));
    }
    // Query all children for all parents, update Rollup Field value
    for(Attachment a:[SELECT Id, ParentId, Parent.Type FROM Attachment where ParentId IN :cssIds]) {    
        cssRecords.get(a.ParentId).Attachments_Count__c += 1;
    }
    ////Query all children for all parents, update Rollup Field value
    for(Attachment a:[SELECT Id, ParentId, Parent.Type FROM Attachment where ParentId IN :suppReqIds]) {    
        suppReqRecords.get(a.ParentId).Attachments_Count__c += 1;
    }
    // Commit changes to the database
    Database.update(cssRecords.values());
    ///// Commit changes to the database
    Database.update(suppReqRecords.values());
}