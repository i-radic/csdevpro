trigger AU_Project_UpdateStageOwner on Project__c (after update) {
	ProjectTriggerUtilities.UpdateStageOwner(trigger.oldMap, trigger.newMap);
}