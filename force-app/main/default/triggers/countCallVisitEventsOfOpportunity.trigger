//CR SF2317
/* to count the total calls/visits logged against opportunities when an event is logged.
*/
trigger countCallVisitEventsOfOpportunity on Event (after insert, after update, before delete) {
    if(TriggerDeactivating__c.getInstance().Event__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
    Set<id> eventids = new Set<id>(); //Event Ids on which trigger is fired.
    Set<id> oppidsset = new Set<id>(); //Opportunity Ids of Events
    List<id> oppids=new List<id>(); //List of Opportunity Ids
    List<Event> eventlist; //List of Events on which trigger is fired
    Opportunity tempopp; //temporarily stores Opportunity
    List<Event> eventlist1; //List of Events of opportunities
    integer totalcalls=0; //to count total calls
    integer totalvisits=0;//to count total visits
    Opportunity opp; //to save updated opportunity         
    List<opportunity> newopps = new List<opportunity>(); //List of opportunities to be updated.
    Decimal temptotalcalls=0; //temporary total calls on opportunity
    Decimal temptotalvisits=0; //temporary total visits on opportunity
    integer delcalls=0; //number of deleted calls
    integer delvisits=0; //number of deleted visits    
    
    System.debug('---------------------Inside Trigger');
    
    if(trigger.isInsert || trigger.isUpdate) {
        
        System.debug('---------------------Inside isInsert Trigger');
        
        //Get Event Ids on which trigger fired    
        for(Event e : trigger.new){
            eventids.add(e.id);   
        }
        
        //Get what.type, whatid from Events 
        eventlist = [Select What.type, WhatId From Event where id in :eventids];
        
        //Get opportunity ids and store them in oppidsset
        for(Event e1 : eventlist) {
            if(e1.what.type=='Opportunity'){
                System.debug('---------------------is opportunity');
            oppidsset.add(e1.whatid);
            }
        }
        
        //Add all opportunity ids from oppidsset to oppids List
        //oppids consists opportunities on which Events are updated/ on which new Events are added
        oppids.addAll(oppidsset);
        
        //For each opportunity id, get events, count call events and visit events and update opportunity records.
        for(integer i=0;i<oppids.size();i++) {
            System.debug('---------------------oppids.......'+i);
            tempopp=[Select (Select Id, Activity_type__c, Activity_Area__c From Events) From Opportunity o where o.id=:oppids[i]];
            
            System.debug('---------------------opplist size==='+tempopp);
            
            eventlist1=tempopp.Events;
            totalcalls=0;
            totalvisits=0;
            for(Event e2 : eventlist1) {
                if( e2.Activity_type__c == 'Call Outbound' || 
                    e2.Activity_type__c == 'Call Outbound - DM NA' || 
                    e2.Activity_type__c == 'Customer Conference Call' ||
                    e2.Activity_type__c =='Healing Call' ||
                    e2.Activity_type__c =='Sales Genius Follow Up Call' ||
                    e2.Activity_type__c =='Call - Inbound' ||
                    e2.Activity_type__c =='Conference Call' ) {
                        totalcalls=totalcalls+1;
                } //check event is visit.
                else if(e2.Activity_type__c == 'Visit' ||
                    e2.Activity_type__c == 'Scheduled Visit' || 
                    e2.Activity_area__c == 'DBAM Visit - Half Day' ||
                    e2.Activity_area__c == 'DBAM Visit - Full Day') {
                        totalvisits=totalvisits+1;
                }
            }
            
            System.debug('---------------------'+'totalcalls'+totalcalls+'    '+'totalvisits'+'     '+totalvisits);
            
            // ROS - 24/01/2013
            //opp = new Opportunity(id=oppids[i],total_calls__c=totalcalls, total_visits__c=totalvisits, last_updated__c =date.today());
            //opp = new Opportunity(id=oppids[i], total_visits__c=totalvisits);
            //newopps.add(opp);
        }
        
        try {
            //update newopps;
        } Catch(exception e){
            system.debug('error inserting list');
        }  finally {
            eventids = null;
            oppids = null;
            eventlist=null;
            tempopp=null;
            eventlist1=null;
            totalcalls=null;
            totalvisits=null;
            opp=null;          
            newopps=null;
            
        }
    }//End of Insert and Update triggers
    
    if(trigger.isDelete) {
        
        System.debug('---------------------Inside isDelete Trigger');
        
        //Get the events on which delete trigger fired.
        if(trigger.isBefore) { 
            for(Event e : trigger.old){
                eventids.add(e.id);   
            }
            
            System.debug('---------------------eventids set size==='+eventids.size());
            
            //Get what.type, whatid from Events
            eventlist = [Select What.type, WhatId From Event where id in :eventids];
            System.debug('---------------------eventlist size==='+eventlist.size());
            
            //Get opportunity ids and store them in oppidsset
            for(Event e1 : eventlist) {
                if(e1.what.type=='Opportunity'){
                    System.debug('---------------------is opportunity');
                oppidsset.add(e1.whatid);
                }
            }
            
            //Add all opportunity ids from oppidsset to oppids List
            //oppids consists opportunities on which Events are updated/ on which new Events are added
            oppids.addAll(oppidsset);
            
            System.debug('---------------------oppidsset set size==='+oppidsset.size());
        
            //For each opportunity id, get events(eventlist1), compare each event with the events on which delete trigger fired(eventlist) and update total calls and total events accordingly and update the opportunity records.
            for(integer i=0;i<oppids.size();i++) {
                System.debug('---------------------oppids.......'+i);
                //opplist=[Select o.id, (Select Id, Activity_type__c, Activity_Area__c From Events) From Opportunity o where o.id=:oppids[i]];
                tempopp=[Select o.id, (Select Id, Activity_Area__c, Activity_type__c From Events) From Opportunity o where o.id=:oppids[i]];
                
                System.debug('---------------------opplist size==='+tempopp);
                
                eventlist1=tempopp.Events;
                
                System.debug('---------------------eventlist1 size==='+eventlist1.size());
                
               // temptotalcalls=tempopp.Total_Calls__c;
                //temptotalvisits=tempopp.Total_Visits__c;
                delcalls=0;
                delvisits=0;
                for(Event e2 : eventlist1) {
                    for(Event e1 : eventlist) {
                        if(e2.id==e1.id){
                            if( e2.Activity_type__c == 'Call Outbound' || 
                                e2.Activity_type__c == 'Call Outbound - DM NA' || 
                                e2.Activity_type__c == 'Customer Conference Call' ||
                                e2.Activity_type__c =='Healing Call' ||
                                e2.Activity_type__c =='Sales Genius Follow Up Call' ||
                                e2.Activity_type__c =='Call - Inbound' ||
                                e2.Activity_type__c =='Conference Call' ) {
                                    delcalls=delcalls+1;
                            } //check event is visit.
                            else if(e2.Activity_type__c == 'Visit' ||
                                e2.Activity_type__c == 'Scheduled Visit' || 
                                e2.Activity_area__c == 'DBAM Visit - Half Day' ||
                                e2.Activity_area__c == 'DBAM Visit - Full Day') {
                                    delvisits=delvisits+1;
                            }
                        }
                    }
                }
                
                System.debug('---------------------'+'totalcalls'+totalcalls+'    '+'totalvisits'+'     '+totalvisits);
                
                // ROS - 24/01/2013
                //opp = new Opportunity(id=oppids[i],total_calls__c=temptotalcalls-delcalls, total_visits__c=temptotalvisits-delvisits, last_updated__c =date.today());
                //opp = new Opportunity(id=oppids[i],total_calls__c=temptotalcalls-delcalls, total_visits__c=temptotalvisits-delvisits);
                //newopps.add(opp);
            }
            
            try {
                //update newopps;
            } Catch(exception e){
                system.debug('error inserting list');
            }  finally {
                eventids = null;
                oppids = null;
                eventlist=null;
                tempopp=null;
                eventlist1=null;
                totalcalls=null;
                totalvisits=null;
                opp=null;          
                newopps=null;
                temptotalcalls=null;
                temptotalvisits=null;
                delcalls=null;
                delvisits=null;
        
            }
        } //End of if Isbefore
    }//End of Delete Trigger              
} //End of Trigger