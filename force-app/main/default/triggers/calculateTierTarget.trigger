// History
//
// Version       Date            Author          Comments
// 1.01.0        10-07-2009      Alan Jackson    Initial version
// 1.02.0        30-07-2009      Alan Jackson    updated for global variables
// 1.10.0        09-07-2013      Alan Jackson    updated for cater for multiple text value of Tier_Value__c
trigger calculateTierTarget on Contract (before insert, before update){
    
// set record type for Adder contracts
RecordType rt_Adder = [select id from RecordType where SobjectType='Contract' and name ='Adder Contract' limit 1];
for(Contract c:Trigger.new){ 
         
        if  (c.adder_status__C != 'Lines LTC'){
            c.RecordTypeId  = rt_Adder.id;
            }
      }
    
    
    
    //get global variables
    /*Set<string> selectGV = new Set<string>();//Create a set of Global Variable to get from custom object
selectGV.add('contract_tier_tgt_red');
selectGV.add('contract_tier_tgt_amber');
Map<String, String> globVar = new Map<String, String>();//create map to hold values
List<Global_Variables__c> GVs = [select Name, Gvalue__c from Global_Variables__c where Name in :selectGV];// read in values
for (Global_Variables__c GV :GVs)
globVar.put(gv.Name,gv.gValue__c);//populate map
integer TargetRed = integer.valueOf(globVar.get('contract_tier_tgt_red'));//set variables
integer TargetAmber = integer.valueOf(globVar.get('contract_tier_tgt_amber'));
*/
    integer TargetRed = 90;
    integer TargetAmber = 100;
    
    
    public Date sDate = null;
    public Date mDate = null;
    public integer DaysIntoContract = 0;    
    public integer Tier = 0;   
    public string TierTemp = '';     
    public Double TargetSpendToDate = 0;    
    public Double spendPercent = 0; 
    public Double cSpend = 0;
    public Double mSpend = 0;
    
    
    for(Contract c:Trigger.new)
    { 
        //set tier values
        TierTemp = c.Tier_Value__c;
        
        if  (TierTemp != null){
            TierTemp = TierTemp.replace('£', ''); // remove pound signs
            TierTemp = TierTemp.replace(',', ''); // remove commas
        }
        
        if  (TierTemp == null)
            Tier = 0;
        
        else if (TierTemp.contains('K'))
            Tier =  integer.valueof(decimal.valueof(TierTemp.replace('K', '')) *1000); // remove K signs * multiply by 1000
        
        else if (TierTemp.contains('M'))
            Tier =  integer.valueof(decimal.valueof(TierTemp.replace('M', '')) *1000000); // remove M signs and multiply by 1000000 
        
        else 
            Tier = integer.valueof(decimal.valueof(TierTemp));
        
        /*
if (c.Tier_Value__c == '£0K')
Tier = 0;
else if (c.Tier_Value__c == '£200')
Tier = 200;
else if (c.Tier_Value__c == '£250')
Tier = 250;    
else if (c.Tier_Value__c == '£500')
Tier = 500;
else if (c.Tier_Value__c == '£750')
Tier = 750;
else if (c.Tier_Value__c == '£5K')
Tier = 5000;
else if (c.Tier_Value__c == '£10K')
Tier = 10000;
else if (c.Tier_Value__c == '£15K')
Tier = 15000;
else if (c.Tier_Value__c == '£30,000')
Tier = 30000;
else if (c.Tier_Value__c == '£30K')
Tier = 30000;
else if (c.Tier_Value__c == '£50K')
Tier = 50000;
else if (c.Tier_Value__c == '£100K')
Tier = 100000;
else if (c.Tier_Value__c == '£200K')
Tier = 200000;
else if (c.Tier_Value__c == '£250K')
Tier = 250000;
else if (c.Tier_Value__c == '£400K')
Tier = 400000;
else if (c.Tier_Value__c == '£700K')
Tier = 700000;
else if (c.Tier_Value__c == '£1M')
Tier = 1000000;
else if (c.Tier_Value__c == '£3M')
Tier = 3000000;
else if (c.Tier_Value__c == '£3.5M')
Tier = 3500000;
else if (c.Tier_Value__c == '£5M')
Tier = 5000000;
else if (c.Tier_Value__c == '£10M')
Tier = 10000000;
else if (c.Tier_Value__c == null)
Tier = 0;

*/
        
        if     (Tier < 0.01 ){//IF No commitment spend show Green RAG status
            c.Monitor_Spend_Tier_Image__c = '/img/samples/light_green.gif';
            c.Monitor_Spend_Tier_Percent__c = 99999999;
        }
        else if (c.Current_Monitor_Spend__c < 0.01 || c.Current_Monitor_Spend__c == null){//IF No monitor spend show red RAG status
            c.Monitor_Spend_Tier_Image__c = '/img/samples/light_red.gif';
            c.Monitor_Spend_Tier_Percent__c = 0;
        }       
        
        else{// calculate if on target to meet commitment 
            if (c.Last_Rollover_Date__c > c.StartDate)//set start date to be greater of start or rollover date
                sDate = c.Last_Rollover_Date__c;
            else
                sDate = c.StartDate;
            
            mDate = c.Current_Monitor_Spend_Date__c;
            
            DaysIntoContract = sDate.daysbetween(mDate);
            if (DaysIntoContract > 365)
                DaysIntoContract = (DaysIntoContract - 365);
            
            if (DaysIntoContract > 0)
                TargetSpendToDate = (Tier * DaysIntoContract / 365);
            else 
                TargetSpendToDate = 0;  
            
            if (TargetSpendToDate != 0)//calculate % spend to date - catch zero spend divide by error
                spendPercent = (c.Current_Monitor_Spend__c * 100 / TargetSpendToDate); 
            else 
                spendPercent = 0;
            spendPercent = spendPercent.intValue();   
            
            if  (spendPercent < TargetRed ){// Red TFL
                c.Monitor_Spend_Tier_Image__c = '/img/samples/light_red.gif';
                c.Monitor_Spend_Tier_Percent__c = spendPercent;
            }
            else if (spendPercent < TargetAmber ){// Amber TFL
                c.Monitor_Spend_Tier_Image__c = '/img/samples/light_yellow.gif';
                c.Monitor_Spend_Tier_Percent__c = spendPercent;
            }
            else { // Green TFC
                c.Monitor_Spend_Tier_Image__c = '/img/samples/light_green.gif';// Green TFL
                c.Monitor_Spend_Tier_Percent__c = spendPercent;
            }     
        } 
    } 
}