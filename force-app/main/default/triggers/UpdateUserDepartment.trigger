trigger UpdateUserDepartment on BTLB_Master__c (after insert, after update,before insert,before update) {
    /*
     for(BTLB_Master__c BM:Trigger.new){
    if(BM.Name!=null){
        List<User> uList = [Select Id,Department,Company_Name__c,Email_Signature__c,Company_Registration_No__c,Company_Registered_Address__c from User where isActive2__c=:'True' and Department!=null and Department=:BM.Name];                    
            if(uList.size()>0){
                for(User forUser:uList){
                    if(BM.Email_Signature__c!=null)
                        forUser.Email_Signature__c = BM.Email_Signature__c;
                    if(BM.Company_Name__c!=null)
                        forUser.Company_Name__c=BM.Company_Name__c;
                    if(BM.Company_Registration_No__c!=null)
                        forUser.Company_Registration_No__c=BM.Company_Registration_No__c;
                    if(BM.Company_Registered_Address__c!=null)
                        forUser.Company_Registered_Address__c=BM.Company_Registered_Address__c;       
                }  
                update uList;      
            }
        if(Trigger.isInsert){
            List<BTLB_Master__c> ExistingBTLBName = [Select Name from BTLB_Master__c where Name=:BM.Name limit 1];
            if(ExistingBTLBName.size()>0){
                BM.Name.addError(' A BTLB Master Object record already exists with the same name.');
            }
        }
    }
}*/
    
    
/*Fixed as part of CASE00400313 by 609701681*/    
    for(BTLB_Master__c bt: trigger.New) {
        if((trigger.isafter && trigger.isupdate) || (trigger.isafter && trigger.isinsert)) {           
            BTLB_Master_Helper.HandleUpdateUser(bt.Id, bt.Name, bt.Email_Signature__c);
        }
        else if(trigger.isbefore && trigger.isinsert) {
            integer btlb_count = BTLB_Master_Helper.HandleInsertBTLBRec(bt.Name);
            if(btlb_count>0) {
                bt.addError('A BTLB Master Object record already exists with the same name.');
            }
        }
        else if(trigger.isBefore && trigger.isUpdate && Trigger.OldMap.get(bt.Id).Name != bt.Name) {
            integer btlb_exist_count_check = BTLB_Master_Helper.HandleInsertBTLBRec(bt.Name);
            if(btlb_exist_count_check > 0) {
                bt.addError('A BTLB Master Object record already exists with the same name.');
            }
        }
    }
}