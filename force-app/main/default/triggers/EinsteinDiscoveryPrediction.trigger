/*
Client      : BT Business
Author      : Krupakar Reddy J
Date        : 13/07/2018 (dd/mm/yyyy)
Version     : 1.0
Description : This trigger calculates propensity and outcome for.
BB, Cloud, BTOP, Mobile, Switch and Data Netwok products
******************************************************************
Copyright (c) 2018, BT Business, Krupakar Reddy J
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
 
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of the BTUtilities nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written
      permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

******************************************************************
*/
trigger EinsteinDiscoveryPrediction on Einstein_Discovery__c (after insert, after update) {
    if(system.isFuture()) return;
    // Name of the custom setting
    private static final String CONFIG_NAME = 'BBDiscoveryOutcome';
	private static final String CLOUD_CONFIG_NAME = 'CloudDiscoveryOutcome';
	private static final String DN_CONFIG_NAME = 'DNDiscoveryOutcome';
	private static final String SW_CONFIG_NAME = 'SWDiscoveryOutcome';
	private static final String BTOP_CONFIG_NAME = 'BTOPDiscoveryOutcome';
	private static final String MOB_CONFIG_NAME = 'MOBDiscoveryOutcome';
	
    // get config
    analytics_edd__SDDPredictionConfig__c config = analytics_edd__SDDPredictionConfig__c.getInstance(CONFIG_NAME);
    List<Map<String, String>> fieldsList = new List<Map<String, String>>();
    for (sObject sObj: Trigger.new) {
        Map<String, Schema.SObjectField> objectFields =
        Schema.getGlobalDescribe().get(config.analytics_edd__object_api_name__c).getDescribe().fields.getMap();
        Map<String, String> fieldValues = new Map<String, String>();
        for (Schema.SObjectField field: objectFields.values()) {
            if (!field.getDescribe().getType().name().endsWithIgnoreCase('textarea')) {
                String fieldApiName = field.getDescribe().getName();
                fieldValues.put(fieldApiName, String.valueOf(sObj.get(fieldApiName)));
            }
        }
        // log field values
        System.debug(fieldValues);
        fieldsList.add(fieldValues);
    }
    // make setPrediction call
    if(!Test.isRunningTest()){  
        analytics_edd.PredictionIntegration.setPrediction(CONFIG_NAME,JSON.serialize(fieldsList));
		analytics_edd.PredictionIntegration.setPrediction(CLOUD_CONFIG_NAME,JSON.serialize(fieldsList));
		analytics_edd.PredictionIntegration.setPrediction(DN_CONFIG_NAME,JSON.serialize(fieldsList));
		analytics_edd.PredictionIntegration.setPrediction(SW_CONFIG_NAME,JSON.serialize(fieldsList));
		analytics_edd.PredictionIntegration.setPrediction(BTOP_CONFIG_NAME,JSON.serialize(fieldsList));
		analytics_edd.PredictionIntegration.setPrediction(MOB_CONFIG_NAME,JSON.serialize(fieldsList));
    }
}