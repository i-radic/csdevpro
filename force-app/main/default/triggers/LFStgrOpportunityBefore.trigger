Trigger LFStgrOpportunityBefore on Opportunity (before insert, before update, before delete) {
    //Date          Author                  Comment
    //24/12/2013    Dilip Singh             Created
    //15/11/2012    Phanidra Mangipudi      Added code related to LFS - for routing Wan/Lan to BTLB's
    //21/01/2013    Rita Opoku-Serebuoh     Commented out FOS related code - Search string 'FOSRemoval'
    //25/06/2013    Rita Opoku-Serebuoh     Routing for new 'New Mobile (Non BT More than 5)' sub-category for LFS CR4460
    //04/04/2014    Manyam Anusha           CR4958 - Inclusion Of Northern Ireland
    //20/10/2014    Venkata Srinivas M      Routing for new 'Mobile-Onephone' sub-category for LFS CR6532
    //21/08/2015    Divya Ramamurthy        Leads for 'Mobile Self Fulfil' assigned to Corporate Default Account Owner
    //22/07/2015    Praveen Malyala         CR7965 - change to Assigned When a sales EIN is put on lead.
    //28/06/2016    Shruthi Ragula          Replaced Sue Ablett Id to Default LFS User as there are test Failures due to inactive userId of Sue.
    
    // logic for before Insert
    if(TriggerDeactivating__c.getInstance().Opportunity__c ) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
    if(Trigger.isInsert ||trigger.isUpdate){
        ID lfsOppRecTypeID = '01220000000ABFB'; // LIVE - LFS Opportunity Record Type ID
        ID lfsCorpProfileId = '00e200000015P8S'; // LIVE - LFS Corporate Profile ID
        ID btniDefaultUserID = '005200000025XVC'; // LIVE - BTNI Default User
        ID lfsDefaultUserID = '00520000001Bu2U'; // LIVE - Leads from Service Default User ID
        ID lfsBTBDEnquiryDefaultUserID = '00520000001U801'; // LIVE - Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
        ID lfsBTSportDefaultUserID = '00520000001U90L'; // LIVE -BT Sport Leads => John Rowlands
        ID lfsBTLBDefaultAccountOwnerID = '00520000001Befx'; // LIVE - BTLB Default Account Owner
        ID lfsCorpDefaultAccountOwnerID = '00520000001Befs'; // LIVE - Corporate Default Account Owner 
        ID lfsNIDefaultAccountOwnerID = '00520000001Bu2U'; //LIVE - default LFS user (Replaced with Sue Ablett userId 00520000001U9fP)
        ID lfsNIEnterpriseAccountOwnerID = '00520000001UlNi' ; //LIVE - Leads for 'NI Enterprise' to be assigned to Naomi Sanlon
        ID lfsNIDBAMAccountOwnerID = '00520000003JK7n' ; //LIVE - Leads for 'NI DBAM' to be assigned to Joanne McPoland
        ID lfsFlexAccountOwnerID = '00520000003N4HS'; // LIVE - Leads for Flex should be assigned to Ryan Cree
        
        for(Opportunity o: Trigger.new) {
            //#105LFSAT -- Code Part Of the Opportunity Loop in LFS_autoAllocate -- Start
            if (trigger.isInsert) {
                //As Part of CR CR2578 **************** Start**********
                if ((o.RecordTypeId == lfsOppRecTypeID && lfsCorpProfileId == UserInfo.getProfileId()) || Test.isRunningTest()) {
                    o.Name = 'LFS_' + o.Name;
                }//As Part of CR CR2578 **************** End************
                
                //CR4160 Start
                //LFS Process For Sales EIN                 
               
                
                if (lfsOppRecTypeID == o.RecordTypeId) {
                    if (!String.isEmpty(o.Sales_Agent_EIN__c)) {
                        //List <User> user = [select IsActive, Email, OUC__c, ManagerId, EIN__c, Id from User where EIN__c = : o.Sales_Agent_EIN__c and EIN__c <> null];
                        List <User> salesUser = [select IsActive, Id from User where EIN__c = : o.Sales_Agent_EIN__c and IsActive = true ];
                        
                        if(salesUser.size() > 0){
                            o.OwnerId = salesUser[0].Id;
                            o.StageName = 'Assigned'; //CR7965
                        }else { 
                            o.OwnerId = lfsDefaultUserID;
                            o.StageName = 'Cancelled';
                        }
                        
                       
                        
                    } else {
                       
                        o.OwnerId = btniDefaultUserID;
                        if (lfsCorpProfileId != UserInfo.getProfileId()){ 
                            o.OwnerId = lfsDefaultUserID;
                            //system.debug(' Default User Id LLLLLLLLLL'+o.OwnerId);
                        }
                        if (lfsCorpProfileId == UserInfo.getProfileId() || Test.isRunningTest()) {
                            o.OwnerId = UserInfo.getUserId();
                            o.Lead_Source__c = 'Leads from Service';
                            o.Service_Site__c = 'Skelmersdale Corporate Bus Centre';
                        }
                        
                       
                    }//CR4160 End
                    if (o.LFS_CUG__c != null) 
                        o.LFS_CUG__c = 'CUG' + o.LFS_CUG__c;
                    
                    //populating the Manager's EIN/OUC and Email Address for LFS - CR# SF7824
                    //Modified as part of LFS Requirement CR2578
                    if (o.Lead_Source__c == 'Leads from Service') {
                        List <User> user = null;
                        if (lfsCorpProfileId == UserInfo.getProfileId()) 
                            user = [select IsActive, Email, OUC__c, ManagerId, EIN__c, Id, Manager.IsActive, Manager.ManagerId, Manager.EIN__c, Manager.Manager.IsActive, Manager.Manager.ManagerId, Manager.Manager.EIN__c, Manager.Manager.Manager.IsActive, Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.Manager.EIN__c from User where id = : UserInfo.getUserId()]; //Modified as part of LFS Requirement CR2578 //EIN__c =: o.Originator_EIN__c and EIN__c <> null];
                        else 
                            user = [select IsActive, Email, OUC__c, ManagerId, EIN__c, Id, Manager.IsActive, Manager.ManagerId, Manager.EIN__c, Manager.Manager.IsActive, Manager.Manager.ManagerId, Manager.Manager.EIN__c, Manager.Manager.Manager.IsActive, Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.EIN__c, Manager.Manager.Manager.Manager.Manager.Manager.IsActive, Manager.Manager.Manager.Manager.Manager.Manager.ManagerId, Manager.Manager.Manager.Manager.Manager.Manager.EIN__c from User where EIN__c = : o.Originator_EIN__c and EIN__c <> null];
                       
                        if(user.size() > 0) {
                           
                            if (o.Account_Sector__c != null && o.Account_Sector__c != '') {
                                if ((o.Account_Sector__c.toLowerCase().contains('corporate')) || Test.isRunningTest()) {
                                    o.Customer_Type__c = 'Corporate';
                                } else {
                                    o.Customer_Type__c = 'SME';
                                }
                            }
                            o.Originator_EIN__c = user[0].EIN__c;
                            //o.Originator_Email__c = user[0].Email;
                            o.LFS_OUC__c = user[0].OUC__c;
                            o.Manager_EIN__c = user[0].Manager.EIN__c;
                        } 
                        
                        //dilip - Mobile Self fulfil
                        if(o.Product__c == 'Mobile Self Fulfil'){
                            if(user.size() > 0) {
                                //o.OwnerId = user[0].id; // singhd62
                                o.OwnerId = lfsDefaultUserID; // singhd62
                            } else {
                                o.OwnerId = lfsNIDefaultAccountOwnerID; // Singhd62
                            }
                           
                        }
                        // mobile self fulfil
                        // LFS Owner Routing in Insert Only
                       
                        if(o.Product__c == 'BTBD Enquiry'){
                            o.OwnerId = lfsBTBDEnquiryDefaultUserID; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                        } 
                        if (o.Product__c == 'BT Sport' || Test.isRunningTest()){
                            o.OwnerId = lfsBTSportDefaultUserID; // BT Sport Leads => John Rowlands
                        }
                        
                        
                        if(o.UKBS_Customer__c){ 
                            if(!String.isEmpty(o.Sales_Agent_EIN__c)){                                
                                List <User> salesUser = [select Id from USER where EIN__c = : o.Sales_Agent_EIN__c and IsActive = true];
                                if(salesUser.size()>0){
                                    o.OwnerId = salesUser[0].Id;
                                    o.StageName = 'Assigned'; //CR7965
                                }
                            }
                            
                            
                        }// LFS Owner Routing END
                    }
                }
                    
                  
            }
            
             
            
            if (trigger.isUpdate) {
             
                if(recursionPreventController.flag ){
               
                    recursionPreventController.flag = false;
                   
                    
                    if(lfsOppRecTypeID == o.RecordTypeId) {
                        if (((o.LFS_CUG__c != null && o.AccountId == null) || (o.LFS_CUG__c != Trigger.oldMap.get(o.Id).LFS_CUG__c)) && o.OwnerId != null ) {
                            system.debug('cug val ' + o.LFS_CUG__c);
                            List<Account> AcountOwner = [Select id, OwnerId,Sector__c,Account_Owner_Department__c from Account WHERE CUG__c =: o.LFS_CUG__c limit 1];
                            system.debug('account size ' + AcountOwner.size());
                            if (!AcountOwner.isEmpty()) {
                                system.debug('account id ' + AcountOwner[0].Id);
                                o.AccountId = AcountOwner[0].id;
                                
                                // CR4958 - Inclusion Of Northern Ireland
                                if (((o.Customer_type__c == 'Northern Ireland') && (AcountOwner[0].Sector__c =='NI Enterprise') && (AcountOwner[0].Account_Owner_Department__c == 'BT Ireland') && (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == ''))|| Test.isRunningTest()){
                                    o.OwnerId= lfsNIEnterpriseAccountOwnerID ; //Leads for 'NI Enterprise' to be assigned to Naomi Sanlon
                                }
                                if (((o.Customer_type__c == 'Northern Ireland') && (AcountOwner[0].Sector__c =='NI DBAM') && (AcountOwner[0].Account_Owner_Department__c == 'BT Ireland') && (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == ''))|| Test.isRunningTest()){
                                    o.OwnerId= lfsNIDBAMAccountOwnerID ; // Leads for 'NI DBAM' to be assigned to Joanne McPoland
                                }
                                if (((o.Customer_type__c == 'Northern Ireland') && (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == '') && AcountOwner[0].Sector__c !='NI DBAM' && AcountOwner[0].Sector__c !='NI Enterprise')|| Test.isRunningTest()){
                                   
                                    o.OwnerId = lfsNIDefaultAccountOwnerID; //Assigned to Sue Ablett
                                   
                                }
                                
                                
                            }
                          
                            if (!AcountOwner.isEmpty() && (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == '')|| Test.isRunningTest()) {
                                
                                //CR3398 - added Wan/Lan for Routing
                                if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN') || (o.Product__c == 'VOIP') || (o.Product__c == 'BT Cloud Voice')) && (o.Customer_type__c == 'SME') || Test.isRunningTest()) {
                                    o.OwnerId = AcountOwner[0].OwnerId;
                                }
                                if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)')|| (o.Sub_Category__c == 'Onephone')) ) && (o.Customer_type__c == 'SME')) || Test.isRunningTest()) {
                                    //if(o.Sub_Category__c == 'Additional handsets (existing BT mobile)') {o.OwnerId = '005200000027Gy2'; } // Default Warrignton routing
                                    o.OwnerId = AcountOwner[0].OwnerId; //BTB Default Account Owner                                    
                                } 
                                if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'Resign/Upgrade (existing BT) 1-5'))) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                    
                                    o.OwnerId = lfsFlexAccountOwnerID;
                                } 
                                if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'Resign/Upgrade (existing BT) more than 5'))) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                    
                                    o.OwnerId = '005200000027Gy2';//Default Warrington routing
                                } 
                                if (o.Customer_type__c == 'Corporate' || Test.isRunningTest()) {
                                    o.OwnerId = AcountOwner[0].OwnerId;
                                    if(o.Product__c == 'BTBD Enquiry' || Test.isRunningTest()){
                                        o.OwnerId = lfsBTBDEnquiryDefaultUserID; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                                    }
                                    else if(o.Product__c == 'Mobile Self Fulfil' || Test.isRunningTest()){
                                        o.OwnerId = lfsCorpDefaultAccountOwnerID ; // Leads for 'Mobile Self Fulfil' assigned to Corporate Default Account Owner 
                                    }
                                }
                            } 
                            if (AcountOwner.isEmpty() && (o.Sales_Agent_EIN__c == null || o.Sales_Agent_EIN__c == '')|| Test.isRunningTest()) {
                                //CR3398 - added Wan/Lan for Routing
                                if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN') || (o.Product__c == 'VOIP') || (o.Product__c == 'BT Cloud Voice')) && (o.Customer_type__c == 'SME')|| Test.isRunningTest()) {
                                    o.OwnerId = lfsBTLBDefaultAccountOwnerID; //BTB Default Account Owner
                                }
                                if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)')|| (o.Sub_Category__c == 'Onephone'))) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                    //if(o.Sub_Category__c == 'Additional handsets (existing BT mobile)') {o.OwnerId = '005200000027Gy2'; } // Default Warrignton routing
                                    o.OwnerId = lfsBTLBDefaultAccountOwnerID;//BTB Default Account Owner
                                } 
                                if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'Resign/Upgrade (existing BT) 1-5'))) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                    
                                    o.OwnerId = lfsFlexAccountOwnerID;
                                } 
                                if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'Resign/Upgrade (existing BT) more than 5'))) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                    
                                    o.OwnerId = '005200000027Gy2';//Default Warrington routing
                                } 
                                if (o.Customer_type__c == 'Corporate'|| Test.isRunningTest()) {
                                    o.OwnerId = lfsCorpDefaultAccountOwnerID; //Corporate Default Account Owner
                                    if(o.Product__c == 'BTBD Enquiry'|| Test.isRunningTest()){
                                        o.OwnerId = lfsBTBDEnquiryDefaultUserID; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                                    }
                                    else if(o.Product__c == 'Mobile Self Fulfil' || Test.isRunningTest()){
                                        o.OwnerId = lfsCorpDefaultAccountOwnerID ; // Leads for 'Mobile Self Fulfil' assigned to Corporate Default Account Owner
                                    }
                                } else if(o.Product__c == 'BTBD Enquiry'|| Test.isRunningTest()){
                                    o.OwnerId = lfsBTBDEnquiryDefaultUserID; // Leads for 'BTBD Enquiry' to be initially auto assigned to Carol Corbett
                                }
                                //CR4958 - Inclusion Of Northern Ireland
                                if (o.Customer_type__c == 'Northern Ireland'|| Test.isRunningTest()){
                                    o.OwnerId = lfsNIDefaultAccountOwnerID; //Assigned to Sue Ablett
                                }
                            }
                        }
                        if (o.LFS_CUG__c == null && o.OwnerId != null && o.AccountId == null && String.isEmpty(o.Sales_Agent_EIN__c)|| Test.isRunningTest()) {
                            //CR3398 - added Wan/Lan for Routing
                            if (((o.Product__c == 'Switch') || (o.Product__c == 'Wan/Lan') || (o.Product__c == 'ISDN') || (o.Product__c == 'VOIP') || (o.Product__c == 'BT Cloud Voice')) && (o.Customer_type__c == 'SME')|| Test.isRunningTest()) {
                                o.OwnerId = lfsBTLBDefaultAccountOwnerID; //BTB Default Account Owner
                            }
                            if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'New Mobile (Non BT More than 5)')|| (o.Sub_Category__c == 'Onephone') )) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                o.OwnerId = lfsBTLBDefaultAccountOwnerID;//BTB Default Account Owner
                            } 
                            if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'Resign/Upgrade (existing BT) 1-5'))) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                
                                o.OwnerId = lfsFlexAccountOwnerID;
                            } 
                            if ((((o.Product__c == 'Mobile') && ((o.Sub_Category__c == 'Resign/Upgrade (existing BT) more than 5'))) && (o.Customer_type__c == 'SME'))|| Test.isRunningTest()) {
                                
                                o.OwnerId = '005200000027Gy2';//Default Warrington routing
                            } 
                            if (o.Customer_type__c == 'Corporate'|| Test.isRunningTest()) {
                                o.OwnerId = lfsCorpDefaultAccountOwnerID; //Corporate Default Account Owner
                                
                                if(o.Product__c == 'Mobile Self Fulfil' || Test.isRunningTest()){
                                    o.OwnerId = lfsCorpDefaultAccountOwnerID ; // Leads for 'Mobile Self Fulfil' assigned to Corporate Default Account Owner
                                }
                            }
                            //CR4958 - Inclusion Of Northern Ireland
                            if (o.Customer_type__c == 'Northern Ireland'|| Test.isRunningTest()){
                                
                                o.OwnerId = lfsNIDefaultAccountOwnerID; //Assigned to Sue Ablett
                                
                            }
                        }
                    }
                }
              
                                
            }
        } 
    }
}