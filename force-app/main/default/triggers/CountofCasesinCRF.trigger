trigger CountofCasesinCRF on Case (before insert, before delete) {

    
    
    if(Trigger.isInsert){  
    
    
         
    Case[] c = Trigger.new;
    
    Id CRFId = c[0].Related_to_CRF__c;
    if(CRFId!= null){
    Id CaseRTId = c[0].RecordTypeId;
    String CaseRTName = [Select DeveloperName from RecordType where Id=:CaseRTId].DeveloperName;
    
    
    
    List<CRF__c> crf = [Select Id, Count_Of_CVDs__c, Count_Of_ODFs__c from CRF__c where Id=: CRFId];
    if(crf.size()>0){
        if(CaseRTName == 'Order_Discrepency'){
            if(crf[0].Count_Of_ODFs__c==0 ||crf[0].Count_Of_ODFs__c==null ){
            crf[0].Count_Of_ODFs__c =0;}
            crf[0].Count_Of_ODFs__c++; 
               
        }           
        else if (CaseRTName == 'Credit_Vet'){
            if(crf[0].Count_Of_CVDs__c==0 ||crf[0].Count_Of_CVDs__c==null ){
            crf[0].Count_Of_CVDs__c =0;}
            crf[0].Count_Of_CVDs__c++; 
               
        }           
        update crf;    
    }
    }
    }
    
    if(Trigger.isDelete){ 
    
    Case[] c = Trigger.old;
    
    
    Id CRFId = c[0].Related_to_CRF__c;
    if(CRFId!= null){
    Id CaseRTId = c[0].RecordTypeId;
    String CaseRTName = [Select DeveloperName from RecordType where Id=:CaseRTId].DeveloperName;
    
    
    List<CRF__c> crf = [Select Id, Count_Of_CVDs__c, Count_Of_ODFs__c from CRF__c where Id=: CRFId];
    if(crf.size()>0){
        if(CaseRTName == 'Order_Discrepency'){
            if(crf[0].Count_Of_ODFs__c==0 ||crf[0].Count_Of_ODFs__c==null ){
            crf[0].Count_Of_ODFs__c =0;}
            crf[0].Count_Of_ODFs__c--; 
               
        }           
        else if (CaseRTName == 'Credit_Vet'){
            if(crf[0].Count_Of_CVDs__c==0 ||crf[0].Count_Of_CVDs__c==null ){
            crf[0].Count_Of_CVDs__c =0;}
            crf[0].Count_Of_CVDs__c--; 
               
        }           
        update crf;    
    }
    }
    
    
    
    
    }
}