trigger CS_AllAccountsTriggers on Account (after delete, after insert, after undelete, 
	after update, before delete, before insert, before update) {
		
		CS_TriggerHandler.execute(new CS_AccountsTriggerDelegate());
		
}