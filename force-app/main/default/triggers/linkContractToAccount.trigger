trigger linkContractToAccount on Contract (before insert, before update) {

//
//  Date            Author          Comment
//  28 Sep 2010     Alan Jackson    added static variable to prevent trigger on Account running on Contract update

StaticVariables.setAccountDontRun(true); 

public String sac_code_Local= null; 

List<User> ApexTriggerEnabled = [Select Run_Apex_Triggers__c  from user  Where id = :Userinfo.getUserId()];
    //boolean triggerRunner = ApexTriggerEnabled[0].Run_Apex_Triggers__c;
    //if (triggerRunner == true || Test_Factory.GetProperty('IsTest') == 'yes') { // only trigger if dataload user is updating records
        // get id of dummy account
        Account DummyAccount = [Select a.Id from Account a where sac_code__c = 'Dummy999' LIMIT 1];

        //create list of LE codes associated with Contract being upserted
        //set<string> LeList = new Set<string>();
        //for(Contract a:Trigger.new){
        //    LeList.add(a.le_code__c);
        //}
        
        //create map to hold LE to ID mappings
        Map<String, String> LeMapping = new Map<String, String>();
    
        // read in values
        //List<Account> LeIds = [Select a.Id, a.le_code__c from Account a where le_code__c in :LeList and le_code__c <> null];
        //for (Account le :LeIds){
        //    LeMapping.put(le.le_code__c,le.Id);//populate map
        //}
        
        //create list of Sales Account Codes codes associated with Accounts being upserted
        set<string> SacList = new Set<string>();
        for(Contract a:Trigger.new){
            if(a.sac_code__c != Null){
                SacList.add(a.sac_code__c);
            }    
        }
        
        //create map to hold SAC to ID mappings
        Map<String, String> SACMapping = new Map<String, String>();
        
        // read in values
        List<Account> SacIds = [Select a.Id, a.sac_code__c from Account a where a.sac_code__c in :SacList]; //and a.LE_Code__c = null];
        system.debug('JMM SAC '+ SACIds);
        for (Account si :SACIds){
            SacMapping.put(si.sac_code__c,si.Id);//populate map
        }

        // will need reviewing when LE level accounts start to be added
        // update all accounts associated with contract(s) with todays date
        Account[] accts = [select UVS_Plan_Updated_Date__c from Account where sac_code__c in :sacList];
        for (Account a : accts) {
            a.UVS_Plan_Updated_Date__c = Date.today();  
        }  
        update accts;

        for(Contract a:Trigger.new){
            //if (LeMapping.containsKey(a.le_code__c) <> false){ // IF SAC begins with LB then link to the LE level account
            //    a.AccountId = LeMapping.get(a.le_code__c);              
            //}
            //else {
                if (SacMapping.containsKey(a.sac_code__c) <> false){ // else link to SAC level account
                    a.AccountId = SacMapping.get(a.sac_code__c);
                }   
                else {
                    a.AccountId = DummyAccount.Id;
                }
            //}       
        }               
}