//  Date            Author                     Comment
//  05/12/11        Krupakar Jogannagari 
//  Copyright © 2010

trigger tgrCustomerSalesSupportAfter on Customer_Sales_Support__c (after delete, after insert) {
	
	Set<String> PM_Opps = new Set<String>();
	List<Opportunity> OppsToFlag = new List<Opportunity>();
	List<Customer_Sales_Support__c> cssFindAnyPMs = new List<Customer_Sales_Support__c>();
	RecordType rt_PM = [select id from RecordType where SobjectType='Customer_Sales_Support__c' and name ='Project Management' limit 1];
	Map<Id,Id> mOppCSS=new Map<Id,Id>(); 
	
	if(trigger.isInsert) {
		
		for(Customer_Sales_Support__c css:Trigger.new) {
			if(css.RecordTypeId == rt_PM.Id)
				PM_Opps.add(css.Opportunity_Name__c);
		} 
		
		OppsToFlag = [Select Id, CSS_Project_Management__c From Opportunity WHERE Id IN : PM_Opps];

		for(Opportunity o:OppsToFlag) {
			o.CSS_Project_Management__c = true;	
		}
		update OppsToFlag;
	}

	if(trigger.isDelete) {
		
		for(Customer_Sales_Support__c css:Trigger.old) {
			if(css.RecordTypeId == rt_PM.Id)
				PM_Opps.add(css.Opportunity_Name__c);
		}
		
		OppsToFlag = [Select Id, CSS_Project_Management__c From Opportunity WHERE Id IN : PM_Opps];
		
		cssFindAnyPMs = [Select RecordTypeId, Opportunity_Name__c, Id From Customer_Sales_Support__c WHERE Opportunity_Name__c IN : PM_Opps];
		
		for(Customer_Sales_Support__c css:cssFindAnyPMs) {
			if(css.RecordTypeId == rt_PM.Id)
				mOppCSS.put(css.Opportunity_Name__c, css.Id);
		}
		
		for(Opportunity o:OppsToFlag) {
			if(!mOppCSS.containsKey(o.Id))
				o.CSS_Project_Management__c = false;	
		}
		update OppsToFlag;
	}
}