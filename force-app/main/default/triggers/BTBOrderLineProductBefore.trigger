//
// History
//
// Version      Date            Author          Comments
// 1.0.0        05-04-2011      Krupakar        Initial version
//

//
// Comments
//
// 1. This Trigger will change the product family from Calls and Lines New provide 
//    or Calls and Lines SIM New provide to Calls and Lines New.
trigger BTBOrderLineProductBefore on BTB_Order_Line_Product__c (before insert, before update, after insert, after update) {
	
	Profile checkProfile;
	checkProfile = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileID()];

	Boolean bUpdateDataload = true;
	//This Trigger will change the product family from Calls and Lines New provide or Calls and Lines SIM New provide to Calls and Lines New
	for (BTB_Order_Line_Product__c bolp : Trigger.new) {
		
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines New provide')) { 
			bolp.Order_Type__c = 'New Provide'; 
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines SIM New provide')) {
			bolp.Order_Type__c = 'SIM New Provide';
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Resign')) {
			bolp.Order_Type__c = 'Resign';
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Winback')) {
			bolp.Order_Type__c = 'Winback';
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Featureline provide')) {
			bolp.Order_Type__c = 'Provide';
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Featureline cease')) {
			bolp.Order_Type__c = 'Cease';
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Featureline modify')) {
			bolp.Order_Type__c = 'Modify';
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines New provide') || bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines SIM New provide') || bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Resign') || bolp.PROD_GROUP__c.equalsIgnoreCase('Calls and Lines Winback')) { 
			bolp.PROD_GROUP__c = 'Calls and Lines'; 
		}
		if(bolp.PROD_GROUP__c.equalsIgnoreCase('Featureline provide') || bolp.PROD_GROUP__c.equalsIgnoreCase('Featureline cease') || bolp.PROD_GROUP__c.equalsIgnoreCase('Featureline modify')) {
			bolp.PROD_GROUP__c = 'Featureline';
		}
		
    }
    
    if(Test.isRunningTest() || checkProfile.Name == 'System Administrator') { 
	    if(Trigger.isAfter) {
	    	if(bUpdateDataload) {
			    Dataloads__c dl_InProgress = [Select Status__c, Next_Update__c, Name, Id, Data_Load__c, DataLoadOrg__c, DataLoadNotes__c, DataLoadDate__c From Dataloads__c d WHERE d.Data_Load__c =:'E2E Reporting'];
			    dl_InProgress.Status__c = 'Success';
			    update dl_InProgress;
			    bUpdateDataload = false;
	    	}
	    }
    }
    
}