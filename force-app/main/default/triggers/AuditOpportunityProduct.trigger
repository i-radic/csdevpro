trigger AuditOpportunityProduct on OpportunityLineItem (after update){
    
    List<Opportunity_Product_History__c> pHistory = new List<Opportunity_Product_History__c>();
    
        
    for(OpportunityLineItem oli : trigger.new){
        String strAction = '';
        Opportunity_Product_History__c oph = new Opportunity_Product_History__c();
        oph.Opportunity__c = oli.OpportunityId;
        oph.Opportunity_Product_ID__c = oli.PricebookEntryId;
        oph.Product__c = oli.ProductName__c; 
        oph.User__c = oli.LastModifiedById;
        
        // Build up Action field string    
        // Check for Sales Price change
        if ((oli.UnitPrice != trigger.oldMap.get(oli.Id).UnitPrice)){
            strAction += 'Changed Sales Price from £'  + trigger.oldMap.get(oli.Id).UnitPrice + ' to £' + oli.UnitPrice + '\n';
        }

        
        // Check for Qty__c change
        if ((oli.Qty__c != trigger.oldMap.get(oli.Id).Qty__c)){
            strAction += 'Changed Quantity from '  + trigger.oldMap.get(oli.Id).Qty__c + ' to ' + oli.Qty__c + '\n';
        }

        
        // Check for ServiceDate change
        if ((oli.ServiceDate != trigger.oldMap.get(oli.Id).ServiceDate)){
            strAction += 'Changed ServiceDate from '  + trigger.oldMap.get(oli.id).ServiceDate + ' to ' + oli.ServiceDate + '\n';
        }

        
        // Check for SystemModStamp change
        if ((oli.SystemModStamp != trigger.oldMap.get(oli.Id).SystemModStamp)){
            strAction += 'Changed SystemModStamp from '  + trigger.oldMap.get(oli.Id).SystemModStamp + ' to ' + oli.SystemModStamp + '\n';
        }

        
        // Check for First_Bill_Date__c change
        if ((oli.First_Bill_Date__c != trigger.oldMap.get(oli.Id).First_Bill_Date__c)){
            strAction += 'Changed First Bill Date from '  + trigger.oldMap.get(oli.Id).First_Bill_Date__c + ' to ' + oli.First_Bill_Date__c + '\n';
        }
        
        
        // Check for Initial_Cost__c change
        if ((oli.Initial_Cost__c != trigger.oldMap.get(oli.Id).Initial_Cost__c)){
            strAction += 'Changed Initial Cost from £'  + trigger.oldMap.get(oli.Id).Initial_Cost__c + ' to £' + oli.Initial_Cost__c + '\n';
        }

        
        
        // Check for Service_Ready_Date__c change
        if ((oli.Service_Ready_Date__c != trigger.oldMap.get(oli.Id).Service_Ready_Date__c)){
            strAction += 'Changed Service Ready Date from '  + trigger.oldMap.get(oli.Id).Service_Ready_Date__c + ' to ' + oli.Service_Ready_Date__c + '\n';
        }
        
        
        // Check for Billing_Cycle__c change
        if ((oli.Billing_Cycle__c != trigger.oldMap.get(oli.Id).Billing_Cycle__c)){
            strAction += 'Changed Billing Cycle from '  + trigger.oldMap.get(oli.Id).Billing_Cycle__c + ' to ' + oli.Billing_Cycle__c + '\n';
        }
        
        
        // Check for Sales_Type__c change
        if ((oli.Sales_Type__c != trigger.oldMap.get(oli.Id).Sales_Type__c)){
            strAction += 'Changed Sales Type from '  + trigger.oldMap.get(oli.Id).Sales_Type__c + ' to ' + oli.Sales_Type__c + '\n';
        }
        
        // Check for Category__c change
        if ((oli.Category__c != trigger.oldMap.get(oli.Id).Category__c)){
            strAction += 'Changed Category from '  + trigger.oldMap.get(oli.Id).Category__c + ' to ' + oli.Category__c + '\n';
        }
        
        
        // Check for Overlay_Specialist__c change
        if ((oli.Overlay_Specialist__c != trigger.oldMap.get(oli.Id).Overlay_Specialist__c)){
            strAction += 'Changed Overlay Specialist from '  + trigger.oldMap.get(oli.Id).Overlay_Specialist__c + ' to ' + oli.Overlay_Specialist__c + '\n';
        }
        
        
        // Check for Contract_Term__c change
        if ((oli.Contract_Term__c != trigger.oldMap.get(oli.Id).Contract_Term__c)){
            strAction += 'Changed Contract Term from '  + trigger.oldMap.get(oli.Id).Contract_Term__c + ' to ' + oli.Contract_Term__c + '\n';
        }
        
        
        // Check for LastModifiedDate change
        if ((oli.LastModifiedDate != trigger.oldMap.get(oli.Id).LastModifiedDate)){
            strAction += 'Changed Last Modified Date from '  + trigger.oldMap.get(oli.Id).LastModifiedDate + ' to ' + oli.LastModifiedDate + '\n';
        }
        
        // Check for LastModifiedById change
        if ((oli.LastModifiedById != trigger.oldMap.get(oli.Id).LastModifiedById)){
            strAction += 'Changed Last Modified By from '  + trigger.oldMap.get(oli.Id).LastModifiedById + ' to ' + oli.LastModifiedById + '\n';
        }


        // Check for ACV_Calc__c change
        if ((oli.ACV_Calc__c != trigger.oldMap.get(oli.Id).ACV_Calc__c)){
            strAction += 'Changed ACV calc from '  + trigger.oldMap.get(oli.Id).ACV_Calc__c + ' to ' + oli.ACV_Calc__c + '\n';
        }

        
        // Check for Contract_Term_Months__c change
        if ((oli.Contract_Term_Months__c != trigger.oldMap.get(oli.Id).Contract_Term_Months__c)){
            strAction += 'Changed Contract Term Months from '  + trigger.oldMap.get(oli.Id).Contract_Term_Months__c + ' to ' + oli.Contract_Term_Months__c + '\n';
        }

        
        // Check for opp_service_ready__c change
        /*if ((oli.opp_service_ready__c != trigger.oldMap.get(oli.Id).opp_service_ready__c)){
            strAction += 'Changed Opp Service Ready from '  + trigger.oldMap.get(oli.Id).opp_service_ready__c + ' to ' + oli.opp_service_ready__c + '\n';
        }*/
        
        
        // Check for opp_first_bill__c change
        /*if ((oli.opp_first_bill__c != trigger.oldMap.get(oli.Id).opp_first_bill__c)){
            strAction += 'Changed Opp First Bill from '  + trigger.oldMap.get(oli.Id).opp_first_bill__c + ' to ' + oli.opp_first_bill__c + '\n';
        }*/
 
        
        if (strAction <> null){
            oph.Action__c = strAction;
            pHistory.add(oph);
        }     
    }
                 
    insert pHistory;
}