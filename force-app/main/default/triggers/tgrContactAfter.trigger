trigger tgrContactAfter on Contact (after delete, after insert, after update) {
/**********************************************************************
    History                                                            
    -------                                                            
    VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
    1.0 -    Greg Scarrtott     02/09/2010        INITIAL DEVELOPMENT                    Initial Build: 
    1.1      Jon Brayshaw       28/09/2010        Added functionality to populate Survey_Contact__c field on account
    1.2      John McGovern      01/10/2010        Added lines to exit the trigger when delete/merge is used.
    1.21     Alan Jackson       16/11/2010        Added lines to check if account already added to updateCallDate
    1.22     John McGovern      18/11/2010        Change to the NOP functionality SF614
    1.23     Dan Measures       19/05/2011        Change for CR2364 - prevent last account call date update if sys admin/dataloader user
    1.24     Adam Soobroy       31/11/2012        Added functionality for Marketo - make call to SF.Services to update OneView if email consent changed.
    1.25    Venkata Srinivas    12/08/2014        Change for CR5797-Deleted the code related to NOP_Survey_Contact__c.
               Maddirala  
    1.26    Alan Jackson        21/08/2014        Re inserted code overwritten to:- update CMPS where contact details changed by Eloqua
                                                      
  */
  if(TriggerDeactivating__c.getInstance().Contact__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }  
    if (staticvariables.getContactIsRunBefore()){
        return;  
    }
    
    //JMM by pass on deletes
    if (trigger.isDelete) {
        return;
    }
    // JMM END
    staticvariables.setContactIsRunBefore(true);
    system.debug('passed initial test');
    Set<id> accIDs = new Set<id>();
    Set<Id> alreadyAddedToUpdateCallDate = new Set<Id>();
    List<contact> accountContacts = new list<contact>();
    List<account> accountcontactlist = new list<account>();
    List<account> updateCallDate = new list<account>();
    Profile profile_sysadmin = [select id from Profile where Name = 'System Administrator']; //CR2364
    Profile profile_dataloader = [select id from Profile where Name = 'BT: Dataloader2']; //CR2364
    
    ID ReportingProfileId = '00e20000001QB4tAAG';
    ID BTLBAdminProfileId = '00e200000015K0NAAU';
    ID CorpAdminProfileID = '00e200000015IRvAAM';
    
    Contact bestContactYet;
    Double bestPriorityYet = 0; 
    Datetime bestmodified;
    List<contact> refreshList = new List<contact>();    
                
     //Get the account id's      
      for(Contact c:Trigger.new){  
         accIDs.add(c.accountid);
            }
     
      //Update the account last call date field for these accounts
      
      if(trigger.isinsert){
        if(UserInfo.getProfileId() != profile_sysadmin.Id && UserInfo.getProfileId() != profile_dataloader.Id && 
           UserInfo.getProfileId() != ReportingProfileId && UserInfo.getProfileId() != BTLBAdminProfileId && 
           UserInfo.getProfileId() != CorpAdminProfileID ){         //CR2364 - do not want to update last contacted date when system admin or dataloader inserts a contacts (esnap uses a user with dataloader profile)
          updateAccountLastContacted updateAccountCallDate = new updateAccountLastContacted();
          //updateCallDate = updateAccountCallDate.updateAccount(accids);
          String objName='Contacts';
          updateCallDate = updateAccountCallDate.updateAccount(accids, objName);
        } 
       } 
     else {
      for(Contact c:Trigger.new){
        Account thisAccount = null;
        for(account a : updateCallDate) {
            if(a.id == c.AccountId) {
                thisAccount = a;
                break;
            }
        }

        if(thisAccount == null){
           thisAccount = new Account(id=c.AccountId);
        }
        system.debug('The Contact ID Is?: ' + c.id);
        updateCallDate.add(thisAccount);
      }
        system.debug('The Size is : ' + updateCallDate.size());
      }
  
     if(updateCallDate.size() > 0)update updateCallDate;
      
      //Get the accounts and contacts
      accountcontactlist = [select id,name,(select kdm_priority__c,LastModifiedDate from contacts where Status__c = 'Active') from Account where id in :accIDs]; 

    
      for(account thisaccount:accountcontactlist){
          accountContacts = thisaccount.contacts;
          
          bestContactYet = new contact();
          bestmodified = datetime.newInstance(1);
    
        for(contact thiscontact:accountContacts){
            system.debug('entering loop');
             if (thiscontact.kdm_priority__c > bestPriorityYet || bestmodified ==null ||(thiscontact.kdm_priority__c==bestPriorityYet && thiscontact.lastmodifieddate > bestmodified)||accountContacts.size()==1){   
                bestPriorityYet = thiscontact.kdm_priority__c;
                bestmodified = thiscontact.lastmodifieddate;
                bestContactYet = thiscontact;
            system.debug('setting best yet' + thiscontact);
             }
            thiscontact.target_contact__c = false;
            
        }
        bestContactYet.target_contact__c =true;    
        refreshlist.addall(accountContacts);
      } 
      
    update refreshlist;
    //++++
    //Code to break as part of MArketo testing
    /*   
    List<Contact> AllCons = [select id from Contact];
    for (Contact cBrk : AllCons){
        Contact AllCons2 = [select id from Contact];
    }
    */
    
    
    //Marketo / Eloqua - make call to SF.Services to update OneView if email consent changed. 
    for (Contact newContact : Trigger.new) {    
        
        try{
            Contact oldContact = Trigger.oldMap.get(newContact.Id);
            Id MarketoProfile = '00e200000015Rp0AAE';
            Id ElequaProfile = '00e200000015aJvAAI'; 
            system.debug('ADJ ContactAfter Userid:' + UserInfo.getProfileId() );

            
            if (UserInfo.getProfileId() == MarketoProfile && oldContact.Email_Consent__c != newContact.Email_Consent__c) {      
                updateMarketoContact.updateContact(newContact.Id);
            }
            
            else if (UserInfo.getProfileId() == ElequaProfile && (
                oldContact.Phone_Consent__c != newContact.Phone_Consent__c ||
                oldContact.Email_Consent__c != newContact.Email_Consent__c ||
                oldContact.Phone != newContact.Phone ||
                oldContact.Salutation != newContact.Salutation ||
                oldContact.FirstName != newContact.FirstName ||
                oldContact.LastName != newContact.LastName 
            )){
                updateMarketoContact.updateContact(newContact.Id);
            }      
            
        }
        catch(Exception e){
            system.debug('Marketo Trigger Exception -> ' + e);
        }
    } 
}