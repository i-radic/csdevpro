/*
    Based on checklist record type and current status this trigger updates and 
    sets the desired status on Company Implementation record.
    This is not written to support bulk operation
*/
trigger ChecklistManageStatuses on Checklist__c (after insert, after update) 
{
    Checklist__c chkRecord = Trigger.new[0];
    List<RecordType> lstRT = new List<RecordType>([Select id, name from Recordtype where 
                                 SObjectType = 'Checklist__c']);
    
    Company_Implementation__c ciRecord = new Company_Implementation__c(id=chkRecord.Company_Implementation__c);
    for (RecordType rt : lstRT)
    {
        system.debug('####RecordType'  +rt.Name + rt.Id);
        if (chkRecord.RecordTypeId == String.valueOf(rt.Id).Substring(0,15))
        {
            if (rt.Name == 'Validation')
            {
                ciRecord.Validation_Status__c = chkRecord.Status__c;
                //BST
                if (chkRecord.Credits__c == true)
                {
                    ciRecord.Credit_Status__c = 'In Progress';
                }
                //Artemis
                if (chkRecord.Credits__c == false && chkRecord.Status__c == 'Complete')
                {
                    ciRecord.Credit_Status__c = 'Not Required';             
                }
            }
            else if (rt.Name.Contains('Data Set-Up Solution') || (rt.Name.Contains('Data Setup'))){//EV:Data setup new record type
                system.debug('####ciRecord'  +ciRecord);
                ciRecord.Data_Status__c = chkRecord.Status__c;
             }   
            else if (rt.Name == 'Contract Setup')
                ciRecord.Contract_Setup_Status__c = chkRecord.Status__c;
            else if (rt.Name == 'Contract Setup Change Note')
                ciRecord.CSU_Change_Note_Status__c = chkRecord.Status__c;
        }   
    }
    update ciRecord;
}