trigger BulkTaskUploads on Task (before insert, before update) {
	if(TriggerDeactivating__c.getInstance().Task__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 

	//commented out as causing occational error , can be restored Tempporarly to asllow MArc / Neil to upload
	//test
	 
     System.debug('----> Value of Static variable TaskUpdatingCampaignTasks : ' + StaticVariables.getTaskUpdatingCampaignTasks());
    System.debug('GOV:BulkTaskUploads Called'); 
    if(StaticVariables.getTaskUpdatingCampaignTasks())
        return;
    
    System.debug('GOV:BulkTaskUploads1'); 
        
    List<User> ApexTriggerEnabled = [Select Apex_Trigger_BulkLoadTasks__c  from user  Where id = :Userinfo.getUserId()];
    boolean triggerRunner = ApexTriggerEnabled[0].Apex_Trigger_BulkLoadTasks__c;
    if (triggerRunner == true || Test_Factory.GetProperty('IsTest') == 'yes') { // only trigger if dataload user is updating records
         
        //create list of EINs associated with Tasks being inserted
                set<string> EinList = new Set<string>();
                for(Task t:Trigger.new){
                EinList.add(t.Owner_EIN__c);
                }
        //create map to hold list of Active users listed in bulk file
        List<User> EinIds = [Select Id, EIN__c from User where ISACTIVE = True and EIN__c in :EinList and EIN__c <> null];
        System.debug('GOV:BulkTaskUploads2'); 
        Map<String, String> EINMapping = new Map<String, String>();
        for (User ui :EinIds)
        EINMapping.put(ui.EIN__c, ui.Id);//populate map

     
      
   //create list of Sales Account Codes codes associated with Tasks being inserted
        set<string> SacList = new Set<string>();
        for(Task t:Trigger.new){
        SacList.add(t.sac_code__c);
        }
   
    //Get Account ID & Owner associated with SACs being loaded
        List<Account> SacDetails = [Select Id, OwnerId, sac_code__c from Account
        where sac_code__c in :SacList and LE_Code__c = null];
        System.debug('GOV:BulkTaskUploads3'); 
            
   //create map to hold SAC to AccountID mappings & read in AccountId values
        Map<String, String> SacIdMapping = new Map<String, String>();   
        for (Account si :SacDetails){
        SacIdMapping.put(si.sac_code__c,si.Id);//populate map
        }
   //create map to hold SAC to OwnerID mappings & read in AccountId values
    List<Account> SacDetails2 = [Select Id, OwnerId, sac_code__c from Account
        where sac_code__c in :SacList and LE_Code__c = null];
       Map<String, String> SacOwnerMapping = new Map<String, String>();
       for (Account so :SacDetails2){
        SacOwnerMapping.put(so.sac_code__c,so.OwnerId);//populate map
        }
      System.debug('GOV:BulkTaskUploads4'); 
  
  //create list of Sales Account IDs  associated with Tasks being inserted 
        set<string> SacIdList = new Set<string>();
        for (Account si2 :SacDetails){
        SacIdList.add(si2.Id);
        }
  
   //create list of Sales Account ROLES associated with Tasks being inserted
        set<string> SacRole = new Set<string>();
        for(Task t:Trigger.new){
        SacRole.add(t.activity_role__c);
        }

   
   List<AccountTeamMember> roles = [Select AccountId, TeamMemberRole, UserId from AccountTeamMember 
   where TeamMemberRole in :SacRole and AccountId in :SacIdList];
       System.debug('GOV:BulkTaskUploads5'); 
      
   //create map to hold AccountId to UserId mappings of Roles
       Map<String, String> UserRoleMapping = new Map<String, String>();
       for (AccountTeamMember ar :roles){
        UserRoleMapping.put(ar.AccountId, ar.UserId);//populate map
        }
    
        
   for(Task t:Trigger.new){
    if (t.Task_Category__c == 'Bulk Tasks'){
                            if (t.reminder_days__c != null){  // only set reminder if days not null
                                t.ReminderDateTime = datetime.newInstance((t.ActivityDate - t.Reminder_Days__c.intValue()).year(),(t.ActivityDate - t.Reminder_Days__c.intValue()).month(),(t.ActivityDate - t.Reminder_Days__c.intValue()).day(),8,0,0);
                                t.ISREMINDERSET = TRUE;
                                }
                           
                           if (EINMapping.containsKey(t.owner_ein__c) <> false) { //map to supplied EIN if available
                                t.OwnerId = EINMapping.get(t.owner_ein__c);
                            }
                            else if (UserRoleMapping.containsKey((SacIdMapping.get(t.sac_code__c))) <> false) { // else map to supplied role if available
                                t.OwnerId = UserRoleMapping.get((SacIdMapping.get(t.sac_code__c)));
                            }
                            else {
                                 t.OwnerId = SacOwnerMapping.get(t.sac_code__c); // else map to account owner
                                 }
                             t.WhatId = SacIdMapping.get(t.sac_code__c);
                            
                      }
        
        
    }
        
    }
    
    

   }