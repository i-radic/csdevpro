trigger BsSlProductBefore on BS_Classic_SL_Product__c (before insert, before update) {
//  Date            Author          Comment
//  10 Aug 2010     Jon Brayshaw    Added merge functionality this calls out to @futures method to do the merge

//create & populate maps to hold details of Email strings
  Map<String, boolean> OmitTable = new Map<String, boolean>();
  Map<String, String> emailMap = new Map<String, String>();
  Map<String, String> reCapMap = new Map<String, String>();
  Map<String, String> reCapPeriod = new Map<String, String>();
  Map<String, String> reCapFName = new Map<String, String>();
List<BSCLassicEmail__c> RecapDetails = [Select Omit_From_Recap_Email_Table__c, Product__c, period__c, Friendly_name__c, email_codes__c, recap_codes__c from BSCLassicEmail__c where type__c ='product'];
for (BSCLassicEmail__c d :RecapDetails) {
      OmitTable.put(d.Product__c, d.Omit_From_Recap_Email_Table__c);
      emailMap.put(d.Product__c, d.email_codes__c);
      reCapMap.put(d.Product__c, d.recap_codes__c);
      reCapPeriod.put(d.Product__c, d.period__c);
      reCapFName.put(d.Product__c, d.Friendly_name__c);
    }

for (BS_Classic_SL_Product__c a:Trigger.new) {
      // BTLB Accounts (Customers)
      if (OmitTable.get(a.Product__c)==FALSE){
      a.include_in_Recap_Email__c = 1;
      }
      else{
      a.include_in_Recap_Email__c = 0;
      }
      a.email_statements__c = emailMap.get(a.Product__c);
      a.recap_statements__c = reCapMap.get(a.Product__c);
      a.period__c = reCapPeriod.get(a.Product__c);
      a.Friendly_name__c = reCapFName.get(a.Product__c);
      
}

}