/*
Client      : BT Business
Author      : Krupakar Reddy J
Date        : 11/02/2019 (dd/mm/yyyy)
Version     : 1.0
Description : This trigger calculates propensity and outcome for.
BB, Cloud, BTOP, Mobile, Switch and Data Netwok products
******************************************************************
Copyright (c) 2018, BT Business, Krupakar Reddy J
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
 
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of the BTUtilities nor the names
      of its contributors may be used to endorse or promote products
      derived from this software without specific prior written
      permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.

******************************************************************
*/
trigger EinsteinAnalyticsPredictionSME on Einstein_Analytics__c (after insert, after update) {   
   
    if(TriggerDeactivating__c.getInstance().Einstein_Analytics__c) {
         System.debug('Bypassing trigger due to custom setting');
         return;
     } 
 
   if(System.isFuture()) return;
    if(ed_insights.CheckRecursive.runOnce()) {
        // Config Names For Predictions
        String BB_CONFIG_NAME = 'BBDiscoveryOutcome';
        String CLOUD_CONFIG_NAME = 'CloudDiscoveryOutcome';
        String DN_CONFIG_NAME = 'DNDiscoveryOutcome';
        String SW_CONFIG_NAME = 'SWDiscoveryOutcome';
        String BTOP_CONFIG_NAME = 'BTOPDiscoveryOutcome';
        String MOB_CONFIG_NAME = 'MOBDiscoveryOutcome';
        // Handler To WriteBackPredictions
        ed_insights.TriggerHandler.insertUpdateHandle(BB_CONFIG_NAME);
        ed_insights.TriggerHandler.insertUpdateHandle(CLOUD_CONFIG_NAME);
        ed_insights.TriggerHandler.insertUpdateHandle(DN_CONFIG_NAME);
        ed_insights.TriggerHandler.insertUpdateHandle(SW_CONFIG_NAME);
        ed_insights.TriggerHandler.insertUpdateHandle(BTOP_CONFIG_NAME);
        ed_insights.TriggerHandler.insertUpdateHandle(MOB_CONFIG_NAME);
    }
}