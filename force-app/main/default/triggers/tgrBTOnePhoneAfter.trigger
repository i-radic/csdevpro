/******************************************************************************************************
Name : tgrBTOnePhoneAfter
Description : After Trigger of BT_One_Phone__c object

Version                 Date                    Author                             Summary Of Change
--------------------------------------------------------------------------------------------------------- 
1.0                    01/02/2017                 Praveen		                       Trigger created         
*********************************************************************************************************/

trigger tgrBTOnePhoneAfter on BT_One_Phone__c (after update) {    
    // commented as part of CR9917 -start
    if(Trigger.isupdate){
       // if(BTOnePhoneTriggerHandler.BTOnePhoneTriggerAfterHandleRecusion){
            //BTOnePhoneTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
           // BTOnePhoneTriggerHandler.BTOnePhoneTriggerAfterHandleRecusion = false;
        //}
    } 
    // commented as part of CR9917 -end
}