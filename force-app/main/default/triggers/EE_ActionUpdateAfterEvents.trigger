trigger EE_ActionUpdateAfterEvents on Action_update__c (after delete, after insert, after update) {
    
     //EV : Changes to update Progress update on Action tracker
    if(Trigger.isAfter && Trigger.isInsert)
        EE_ActionUpdateTriggerHelper.updateProgresscommentsOnTracker(Trigger.newMap,Trigger.oldMap,1);
    else if (Trigger.isAfter && Trigger.isUpdate)
        EE_ActionUpdateTriggerHelper.updateProgresscommentsOnTracker(Trigger.newMap,Trigger.oldMap,2);

}