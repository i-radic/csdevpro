/**
 * Updated 29th June by Martin Eley to prevent error on saving leads that are assigned to a q not a user.
 */
trigger LeadOwnerShip on Lead (before insert, before update) {

    
    //Build map of all active users
    map<Id,User> usrMap = new map<Id,User>([SELECT
        id
        ,name
        ,Email
        ,ManagerId
        ,isActive
        FROM User
        WHERE IsActive = TRUE]);
   
    //List<Account> lstAcc=new List<Account>();
   // RecordType recType=[SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='Lead' and DeveloperName='Small_Sales' limit 1];
   // System.debug('reCORDTYPE->'+recType);
    
    //Set the sales manager email for leads owned by a user not a q
    for(Lead l : Trigger.new){
        if(usrMap.containsKey(l.OwnerId) && l.LeadSource=='Astute'){ //Test to see is user exists, if not then owned by queue
            l.Lead_Owner_Lookup__c = l.OwnerId;
            Id manager=usrMap.get(l.OwnerId).ManagerId;
            if(manager!=null){
                System.debug('TRIGGER MANAGER->'+usrMap.get(l.OwnerId).ManagerId+'<-');
                if(usrMap.containsKey(manager) && usrMap.get(manager).isActive==true){

//                    Id manager=usrMap.get(l.OwnerId).ManagerId;
                    if(null!=usrMap.get(manager))
                    {
                        System.debug('TRIGGER MANAGER EMAIL->'+usrMap.get(manager).Email);
                        l.Sales_Manager_Email__c=usrMap.get(manager).Email;
                    }
                }
                else{
                    l.Sales_Manager_Email__c=null;
                }
            }
       /* if(Trigger.isUpdate && l.RecordTypeId==recType.Id)
        {
            if(null!=l.Company_Custom__c)
              lstAcc.add(new Account(id=l.Company_Custom__c,OwnerId=l.OwnerId));
        }*/
        
        }
    }
   /* if(Trigger.isUpdate && lstAcc.size()>0 )
      update lstAcc;*/
}