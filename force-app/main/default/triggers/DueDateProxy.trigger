trigger DueDateProxy  on Task (before insert, before update, before delete) {
    if(TriggerDeactivating__c.getInstance().Task__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
    System.debug('----> Value of Static variable TaskUpdatingCampaignTasks : ' + StaticVariables.getTaskUpdatingCampaignTasks());
    system.debug('due date called with this list ' + trigger.new);
    if(StaticVariables.getTaskUpdatingCampaignTasks())
        return;
  
  if(StaticVariables.getTaskIsBeforeRun())
        return;
  
  StaticVariables.setTaskIsBeforeRun(true);
    
    Map<String, Task> oppsId = new Map<String, Task>{};
    Map<String, Opportunity> oppsForTasks = new Map<String, Opportunity>{};
    Opportunity [] updateOpps = new Opportunity [] {};     
    List<Task> lCampaignTasks = new List<Task>();
    Set<Id> sCampaignIds = new Set<Id>();
    public Date NextTaskDate = null;
    public ID TaskID = null;
        
    if(Trigger.isDelete){
        for (Task t : Trigger.old) {
            String thisID = t.WhatId;
            if (thisID != null && thisID.startsWith('701')) {
//              t.addError('You cannot delete a Task related to a Campaign!');
//              continue;
            }
        }   
    }else{
        
        for(Task t: Trigger.new)
        { 
            t.Due_Date_Proxy__c = t.ActivityDate;
            
            String thisID = t.WhatId;
            if(thisID != null && thisID.startsWith('006')){
                oppsId.put(t.WhatId, t);
            }else if(thisID != null && thisID.startsWith('701') && t.Status != 'Expired' && t.Type == 'Call'){
                //Campaign related Task - Exclude 'Expired' Tasks as nightly batch run sets Tasks to expired when their Campaign reaches its End Date.
                lCampaignTasks.add(t);
                sCampaignIds.add(t.WhatId);
            }                
        }    
        
        if(!sCampaignIds.isEmpty())
        {
            Map<Id, Campaign> mCampaign = new Map<Id, Campaign>([select Id, X_Day_Rule__c, EndDate from Campaign where Id IN :sCampaignIds]);
           System.debug('GOV:DDProxy1'); 
        
            for(Task t: lCampaignTasks)
            {
                t.X_Day_Rule_Copy__c = mCampaign.get(t.WhatId).X_Day_Rule__c;
                //GS 2-3-11 changed for corporate they want user to be able to change the due date themselves
                if(!((StaticVariables.corp_campaign_task_record_type == t.recordtypeid)&& (trigger.isupdate))){
                  t.ActivityDate = mCampaign.get(t.WhatId).EndDate;
                  t.Due_Date_Proxy__c = mCampaign.get(t.WhatId).EndDate;
              }
                //Automatically set task (campaign related) to completed on Save if 'Call Status/Call Outcome' defined. Reduces button clicks for agents.
                //Updates by GS 02-03-2011 CR2039 -included call back and unable to contact clauses to stop completing all corporate campaign tasks
                
                if(t.Call_Status__c != null && (t.call_status__c !='Call Back Scheduled')){
                    t.Status = 'Completed';
                }
            }
        }
         System.debug('GOV:DDProxy2'+oppsId.keyset()); 
         
            List<Opportunity> opps1 = new List<Opportunity>();
                           opps1 =  [select id, Next_Task_Due_Date__c from opportunity where (Id in :oppsId.keyset()) AND (StageName NOT IN('Won','Lost','Cancelled','Closed Won','Closed Lost','Straight Order','Closed - Expired'))];            
        system.debug('LLLLLLLLLLL'+opps1);
        if(!opps1.isEmpty())
        {system.debug('Entered in if ');
       //for(Opportunity [] opps : [select id, Next_Task_Due_Date__c from opportunity where (Id in :oppsId.keyset()) AND (StageName NOT IN('Won','Lost','Cancelled','Closed Won','Closed Lost','Straight Order','Closed - Expired'))])
          for(Opportunity opp : opps1)
          {
            Task thisTask = oppsId.get(opp.Id);
            
            if(!thisTask.isClosed && ( thisTask.ActivityDate < opp.Next_Task_Due_Date__c || opp.Next_Task_Due_Date__c == null || 
            trigger.isupdate && (Trigger.oldMap.get(thisTask.Id). ActivityDate) != thisTask.ActivityDate &&(Trigger.oldMap.get( thisTask.Id). ActivityDate) == opp.Next_Task_Due_Date__c))
            {
              system.debug('JB:: task:' + thisTask);
              opp.Next_Task_Due_Date__c = thisTask.ActivityDate;
              TaskID = thisTask.id;
              NextTaskDate = thisTask.ActivityDate;
              oppsForTasks.put(opp.id, opp);
              updateOpps.add(opp);
            }
            
            
            //else if(thisTask.isClosed)
            //JMM Change to ensure correct working 11/3/11
            else if(thisTask.Status.contains('Completed') || thisTask.Status == 'Cancelled' || thisTask.Status == 'Expired')
            {
              opp.Next_Task_Due_Date__c = null;
              oppsForTasks.put(opp.id, opp);
              updateOpps.add(opp);
            }
          }
        }  
            
        System.debug('GOV:DDProxy3');
        //JMM Change to ensure correct working 11/3/11
         List<Opportunity> opps2 = new List<Opportunity>();
                           opps2 = [select id, Next_Task_Due_Date__c from opportunity where (Id in :oppsId.keyset()) AND (StageName NOT IN('Won','Lost','Cancelled','Closed Won','Closed Lost','Straight Order','Closed - Expired'))];       
         if(!opps2.isEmpty())
        {    system.debug('Entered in if 2');     
         //for(Opportunity [] opps : [select id, Next_Task_Due_Date__c from opportunity where (Id in :oppsId.keyset()) AND (StageName NOT IN('Won','Lost','Cancelled','Closed Won','Closed Lost','Straight Order','Closed - Expired'))])        
          for(Opportunity opp : opps2)
          {
            Task thisTask = oppsId.get(opp.Id);
            TaskID = thisTask.id;
          }   
       //JMM END 
            
       for(Task [] tasks : [select ActivityDate, whatid from Task where id <> :TaskID and whatid in :oppsForTasks.keyset() AND IsClosed = false])
       
          for(Task tsk : tasks)
          {
            Opportunity thisOpp = oppsForTasks.get(tsk.whatid);
            if((tsk.ActivityDate < thisOpp.Next_Task_Due_Date__c && tsk.ActivityDate < NextTaskDate )|| thisOpp.Next_Task_Due_Date__c == null)
            {
              thisOpp.Next_Task_Due_Date__c = tsk.ActivityDate;
              integer removeOpp=-1;
              integer cnt=0;
              for(Opportunity opp : updateOpps)
              {
                  if(opp.id == thisopp.id)removeOpp=cnt;
                  cnt++;
              }
              if(removeOpp > -1)updateOpps.remove(removeOpp);
              updateOpps.add(thisOpp);
            }
          }
      
      system.debug('JB:: allopps:' + updateOpps);
      if(updateOpps.size() > 0)update updateOpps;
    }
       
   }   
}