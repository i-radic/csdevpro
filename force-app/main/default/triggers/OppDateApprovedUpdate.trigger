trigger OppDateApprovedUpdate on Task (after Update,after Insert) {

//Date            Author          Comment
//22 July 2015    Shruthi Ragula  Added this Trigger as per CR7946
        if(TriggerDeactivating__c.getInstance().Task__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }
         Set<Id> OppID = new Set<Id>();
   
        
        for(Task T:Trigger.New){ 
                  
            if(T.Subject=='Bespoke Pricing Request' && T.Status=='Completed' && T.WhatId != null){
                  
                OppID.add(T.WhatId);
            }
        }
           
         List<Opportunity> OppDateApproved = new List<Opportunity>([Select Id from Opportunity 
                                                                    Where Id IN : OppID]); // Removed Date_Approved__c from the Query
         
         List<Opportunity> UpdateDateApproved = new List<Opportunity>();
         
         for(Opportunity O :OppDateApproved){
             
             //O.Date_Approved__c = Date.Today();
             //UpdateDateApproved.add(O);
             
         }
                 
         Update UpdateDateApproved;
         
         

}