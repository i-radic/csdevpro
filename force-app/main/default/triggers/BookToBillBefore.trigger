trigger BookToBillBefore on BookToBill__c (before update) {

/* ---- Trigger to add history of into a custom object for tracking purposes.---- */

Set<id> LSIDs = new Set<id>();
string jmRun = null; 

// insert custom history object
List<BookToBill_History__c> HistNew= new List <BookToBill_History__c>() ; 
    //system.debug('AllLS' + LSIDs) ;

for(BookToBill__c LS:Trigger.new){
    BookToBill__c beforeUpdate = System.Trigger.oldMap.get(LS.Id);
    String RtName=[Select Name from RecordType where Id=:LS.RecordTypeId].Name;


        If (LS.UV1_Correct_Sales_Type_Selected__c != beforeUpdate.UV1_Correct_Sales_Type_Selected__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV1 - Correct Sales Type Selected';
            HistRec.Old_Value__c = beforeUpdate.UV1_Correct_Sales_Type_Selected__c;
            HistRec.New_Value__c = LS.UV1_Correct_Sales_Type_Selected__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV2_Mobius_Evolution_report_attached__c != beforeUpdate.UV2_Mobius_Evolution_report_attached__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV2 - Mobius/Evolution report attached';
            HistRec.Old_Value__c = beforeUpdate.UV2_Mobius_Evolution_report_attached__c;
            HistRec.New_Value__c = LS.UV2_Mobius_Evolution_report_attached__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV3_Contract_attached__c != beforeUpdate.UV3_Contract_attached__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV3 – Contract attached';
            HistRec.Old_Value__c = beforeUpdate.UV3_Contract_attached__c;
            HistRec.New_Value__c = LS.UV3_Contract_attached__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV4_CCC_Schedule_attached__c != beforeUpdate.UV4_CCC_Schedule_attached__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV4 – CCC Schedule attached';
            HistRec.Old_Value__c = beforeUpdate.UV4_CCC_Schedule_attached__c;
            HistRec.New_Value__c = LS.UV4_CCC_Schedule_attached__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV5_Customer_email_for_rollover__c != beforeUpdate.UV5_Customer_email_for_rollover__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV5 - Customer email for rollover';
            HistRec.Old_Value__c = beforeUpdate.UV5_Customer_email_for_rollover__c;
            HistRec.New_Value__c = LS.UV5_Customer_email_for_rollover__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV6_Contract_Signed_Dated__c != beforeUpdate.UV6_Contract_Signed_Dated__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV6 - Contract Signed & Dated';
            HistRec.Old_Value__c = beforeUpdate.UV6_Contract_Signed_Dated__c;
            HistRec.New_Value__c = LS.UV6_Contract_Signed_Dated__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }

        If (LS.UV7_Contract_Number_recorded_on_SFDC__c != beforeUpdate.UV7_Contract_Number_recorded_on_SFDC__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV7 - Contract Number recorded on SFDC';
            HistRec.Old_Value__c = beforeUpdate.UV7_Contract_Number_recorded_on_SFDC__c;
            HistRec.New_Value__c = LS.UV7_Contract_Number_recorded_on_SFDC__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV8_Network_Build_attached__c != beforeUpdate.UV8_Network_Build_attached__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV8 - Network Build attached';
            HistRec.Old_Value__c = beforeUpdate.UV8_Network_Build_attached__c;
            HistRec.New_Value__c = LS.UV8_Network_Build_attached__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV9_Product_Match__c != beforeUpdate.UV9_Product_Match__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV9 - Product Match';
            HistRec.Old_Value__c = beforeUpdate.UV9_Product_Match__c;
            HistRec.New_Value__c = LS.UV9_Product_Match__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.UV10_Accurate_Product_Lines__c != beforeUpdate.UV10_Accurate_Product_Lines__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'UV10 - Accurate Product Lines';
            HistRec.Old_Value__c = beforeUpdate.UV10_Accurate_Product_Lines__c;
            HistRec.New_Value__c = LS.UV10_Accurate_Product_Lines__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        
        If (LS.M1_Product_Match__c != beforeUpdate.M1_Product_Match__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'M1- Product Match';
            HistRec.Old_Value__c = beforeUpdate.M1_Product_Match__c;
            HistRec.New_Value__c = LS.M1_Product_Match__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.M2_Product_Term_Match__c != beforeUpdate.M2_Product_Term_Match__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'M2 - Product Term Match';
            HistRec.Old_Value__c = beforeUpdate.M2_Product_Term_Match__c;
            HistRec.New_Value__c = LS.M2_Product_Term_Match__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.M3_Volumes_Correct_Resign_or_Upsell_o__c != beforeUpdate.M3_Volumes_Correct_Resign_or_Upsell_o__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'M3 - Volumes Correct';
            HistRec.Old_Value__c = beforeUpdate.M3_Volumes_Correct_Resign_or_Upsell_o__c;
            HistRec.New_Value__c = LS.M3_Volumes_Correct_Resign_or_Upsell_o__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        
        
        If (LS.DC1__c != beforeUpdate.DC1__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC1 - Documentation Attached';
            HistRec.Old_Value__c = beforeUpdate.DC1__c;
            HistRec.New_Value__c = LS.DC1__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC2__c != beforeUpdate.DC2__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC2 - Document Type';
            HistRec.Old_Value__c = beforeUpdate.DC2__c;
            HistRec.New_Value__c = LS.DC2__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC3__c != beforeUpdate.DC3__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC3 - Company Name Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC3__c;
            HistRec.New_Value__c = LS.DC3__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC4__c != beforeUpdate.DC4__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC4 - LE Match';
            HistRec.Old_Value__c = beforeUpdate.DC4__c;
            HistRec.New_Value__c = LS.DC4__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC5__c != beforeUpdate.DC5__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC5 - Company Address Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC5__c;
            HistRec.New_Value__c = LS.DC5__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC6__c != beforeUpdate.DC6__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC6 - Contact Name Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC6__c;
            HistRec.New_Value__c = LS.DC6__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC7__c != beforeUpdate.DC7__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC7 - Product Description Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC7__c;
            HistRec.New_Value__c = LS.DC7__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC8__c != beforeUpdate.DC8__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC8 - Clear Values& SC/CC Codes Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC8__c;
            HistRec.New_Value__c = LS.DC8__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC9__c != beforeUpdate.DC9__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC9 - Total Contract Value Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC9__c;
            HistRec.New_Value__c = LS.DC9__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC10__c != beforeUpdate.DC10__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC10 - Customer Signature Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC10__c;
            HistRec.New_Value__c = LS.DC10__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC11__c != beforeUpdate.DC11__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC11 - Customer Signed Date Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC11__c;
            HistRec.New_Value__c = LS.DC11__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
          
        If (LS.DC12__c != beforeUpdate.DC12__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC12 - Signed Date';
             If (LS.DC12__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.DC12__c.format();
            }
            If (beforeUpdate.DC12__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.DC12__c.format();
            }  
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);
                     
        }
                   
        
        If (LS.SC4__c != beforeUpdate.SC4__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC13 - Annual Contract Value';
            If (beforeUpdate.SC4__c!= null)
            HistRec.Old_Value__c = beforeUpdate.SC4__c.format();
            HistRec.New_Value__c = LS.SC4__c.format();
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.DC14_Total_Contract_Value_Recorded_FF__c != beforeUpdate.DC14_Total_Contract_Value_Recorded_FF__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'DC14 - Total Contract Value Recorded';
            HistRec.Old_Value__c = beforeUpdate.DC14_Total_Contract_Value_Recorded_FF__c;
            HistRec.New_Value__c = LS.DC14_Total_Contract_Value_Recorded_FF__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        
        If (LS.SC1__c != beforeUpdate.SC1__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'SC1 - Product Match';
            HistRec.Old_Value__c = beforeUpdate.SC1__c;
            HistRec.New_Value__c = LS.SC1__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.SC2__c != beforeUpdate.SC2__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'SC2- Products claimed in current Payplan';
            HistRec.Old_Value__c = beforeUpdate.SC2__c;
            HistRec.New_Value__c = LS.SC2__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.SC3__c != beforeUpdate.SC3__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'SC3 - Product Term Match';
            HistRec.Old_Value__c = beforeUpdate.SC3__c;
            HistRec.New_Value__c = LS.SC3__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.SC5__c != beforeUpdate.SC5__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'SC4- Sale Date is within current Payplan';
            HistRec.Old_Value__c = beforeUpdate.SC5__c;
            HistRec.New_Value__c = LS.SC5__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.SC5_Net_ACV_Calculation_Correct__c != beforeUpdate.SC5_Net_ACV_Calculation_Correct__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'SC5 - Net ACV Calculation Correct';
            HistRec.Old_Value__c = beforeUpdate.SC5_Net_ACV_Calculation_Correct__c;
            HistRec.New_Value__c = LS.SC5_Net_ACV_Calculation_Correct__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        
        //Rest of the fields
        If (LS.FeedbackNotes__c != beforeUpdate.FeedbackNotes__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'Feedback Notes';
            If (beforeUpdate.FeedbackNotes__c!= null)
            HistRec.Old_Value__c = beforeUpdate.FeedbackNotes__c;
            HistRec.New_Value__c = LS.FeedbackNotes__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.Task_Comments_Measurement__c != beforeUpdate.Task_Comments_Measurement__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'Measurement Comments Task';
            If (beforeUpdate.Task_Comments_Measurement__c!= null)
            HistRec.Old_Value__c = beforeUpdate.Task_Comments_Measurement__c;
            HistRec.New_Value__c = LS.Task_Comments_Measurement__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.Project_Status__c != beforeUpdate.Project_Status__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'Project Status';
            HistRec.Old_Value__c = beforeUpdate.Project_Status__c;
            HistRec.New_Value__c = LS.Project_Status__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.Revenue_Assurance_Status__c != beforeUpdate.Revenue_Assurance_Status__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'Revenue Assurance Status';
            HistRec.Old_Value__c = beforeUpdate.Revenue_Assurance_Status__c;
            HistRec.New_Value__c = LS.Revenue_Assurance_Status__c;
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);         
        }
        
        If (LS.Status_Date__c != beforeUpdate.Status_Date__c) {
            BookToBill_History__c HistRec = new BookToBill_History__c();
            HistRec.Field__c = 'Status Date';
             If (LS.Status_Date__c == null) {
                HistRec.New_Value__c = ' ' ;
            }
            else {
                HistRec.New_Value__c = LS.Status_Date__c.format();
            }
            If (beforeUpdate.Status_Date__c == null) {
                HistRec.Old_Value__c = ' ' ;
            }
            else {
                HistRec.Old_Value__c = beforeUpdate.Status_Date__c.format();
            }  
            HistRec.BookToBill__c = LS.Id;
            HistRec.Record_Type__c = RtName;          
            jmRun = '1';                     
            HistNew.add(HistRec);
                     
        }
}
    If (jmRun != null) {
    
        insert HistNew;
        system.debug('hhhhhhhhhhhhhhhhhhhhhhh'+HistNew);
    }
}