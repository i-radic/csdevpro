trigger TaskOwnerEIN on Task (before insert, before update) {
    if(TriggerDeactivating__c.getInstance().Task__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }
    Set<Id> ownerIds = new Set<Id>();
    for (Task t:Trigger.new){
        if (trigger.isupdate){
            if(trigger.oldmap.get(t.id).ownerid !=t.ownerid){
                    ownerIds.add(t.ownerid);      
            }
        }else{
                    ownerIds.add(t.ownerId);
        } 
    }
   
//create list of ids of Task Owners
    if(ownerIds.size() >0){
        Map<Id, User> mUser = new Map<Id, User>();
        List<User> UserEins = [Select Id, EIN__c, Department, Division from User where  Id in:ownerIds and isActive = true];
        for (User ui :UserEins) {
             mUser.put(ui.Id, ui );//populate map
                       }
         
        for (Task t:Trigger.new){
      
            if (mUser.containsKey(t.OwnerId) == True) { 
                 //t.Task_Owner_EIN__c = mUser.get(t.OwnerId).ein__c;
                 t.Owner_Division__c = mUser.get(t.OwnerId).Division;
                 t.Owner_Department__c = mUser.get(t.OwnerId).Department; 
            }     
        }
    }
}