trigger Risk_Register_advise_Manager  on Risk_Register__c (after insert) {

for(Risk_Register__c r:Trigger.new){
//Create task to advise of risk creation 
    Task task1 = new Task();
        task1.OwnerId = r.UVS_SM__c;
        task1.ActivityDate = (Date.today()+7);
        task1.ReminderDateTime = datetime.newInstance(Date.today().year(),Date.today().month(),Date.today().day(),8,0,0);
        task1.ISREMINDERSET = TRUE;
        task1.Target_Completion_Date__c = (Date.today()+ 7);
        task1.Subject = 'Risk Register Escalation Initial';
        task1.WhatId = r.Id;
        task1.Task_Category__c = 'Risk Register Escalation';
        insert task1;
}
}