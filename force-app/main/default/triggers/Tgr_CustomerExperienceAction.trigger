/**************************************************************************************************************************************************
    Class Name : Tgr_CustomerExperienceAction
    Test Class Name : CustomerExperienceActionHelper_Test
    Description : Code to Insert and Update Customer Experience Action, And to Calculate RollUp Summary, to Update in Survey Response Object Record
    Version : V0.1
    Created By Author Name : Bikash Kumar Gupta
    Date : 08/09/2017
 *************************************************************************************************************************************************/

trigger Tgr_CustomerExperienceAction on Customer_Experience_Actions__c (before insert, before update, after insert, after update, after delete, after undelete) {
    
    if(Trigger.IsAfter){
        
        if(Trigger.IsInsert)
            CustomerExperienceActionHandler.AfterInsertTrigger(Trigger.NewMap, Trigger.New);
        
        //Recurssion check, To prevent Trigger to fire again and again by Before Update Process
        
        if(Trigger.IsUpdate && CustomerExperienceActionHandler.AfterUpdateRecursionCheck){
            
            //Enabling Recursion for Before Update Process
            
            CustomerExperienceActionHandler.AfterUpdateRecursionCheck = False;
            CustomerExperienceActionHandler.AfterUpdateTrigger(Trigger.NewMap, Trigger.OldMap, Trigger.New, Trigger.Old);
        }
        
        if(Trigger.IsDelete)
            CustomerExperienceActionHandler.AfterDeleteTrigger(Trigger.Old);
        
        if(Trigger.IsUnDelete)
            CustomerExperienceActionHandler.AfterUnDeleteTrigger(Trigger.Old);
    } 
    
    if(Trigger.IsBefore){
        
        if(Trigger.IsInsert) 
            CustomerExperienceActionHandler.BeforeInsertTrigger(Trigger.New);
        
        //Recurssion check, To prevent Trigger to fire again and again by After Update Process
        
        if(Trigger.IsUpdate && CustomerExperienceActionHandler.BeforeUpdateRecursionCheck){
            
            //Enabling Recursion for After Update Process
            
            CustomerExperienceActionHandler.BeforeUpdateRecursionCheck = False;
            CustomerExperienceActionHandler.BeforeUpdateTrigger(Trigger.NewMap, Trigger.OldMap);
        }
    }
}