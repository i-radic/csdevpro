trigger CloudVoiceProductAfter on Cloud_Voice_Site_Product__c (after insert, after update, after delete) {
    Set<String> cvPId = New Set<String>();
    Set<Id> cvsId = New Set<Id>();
    Set<String> cvRPId = New Set<String>();
    List<Cloud_Voice_Site_Product__c> cvsProdId = new List<Cloud_Voice_Site_Product__c>();
    Public Boolean runDelete = false;

    if(Trigger.isDelete){
        for(Cloud_Voice_Site_Product__c c:Trigger.Old){           
            cvsId.add(c.Cloud_Voice_Site__c);
            runDelete = true;
        }       
    }else{
        for(Cloud_Voice_Site_Product__c c:Trigger.New){           
            cvsId.add(c.Cloud_Voice_Site__c);          
        }         
    }
    if(!Trigger.isDelete && !HelperRollUpSummary.getCVSiteStop()){
        for(Cloud_Voice_Site_Product__c c:Trigger.New){           
            cvsId.add(c.Cloud_Voice_Site__c);        
            cvPId.add(c.SFCSSProdId__c);
        }    
       
        List<CSS_Products__c > sRelProd = [Select Unit__c, Type__c, Product_Name__c, Unit_Cost__c, A_Code__c, ShopLink__c,Connection_Charge__c,Sort_Order__c,Required_Product__c FROM CSS_Products__c WHERE Id = :cvPId AND Active__c = True];
        for(CSS_Products__c sP:sRelProd ){
            cvRPId.add(sP.Required_Product__c);
        }
        List<Cloud_Voice_Site_Product__c > existingRProd= [Select Id, SFCSSProdId__c From Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :cvsId AND SFCSSProdId__c = :cvRPId AND Type__c <> 'Line' AND Grouping__c <> 'Connection'];
        Cloud_Voice_Site_Product__c relProd = New Cloud_Voice_Site_Product__c();
        if(trigger.isInsert){   
            for(CSS_Products__c sP:sRelProd ){
                //add required products
                for(Cloud_Voice_Site_Product__c cvProd:Trigger.New ){
                    if(sP.Required_Product__c != Null && existingRProd.size() == 0){
                        relProd.Cloud_Voice_Site__c = cvProd.Cloud_Voice_Site__c ;
                        relProd.AutoAddedBy__c = cvProd.Id;
                        relProd.SFCSSProdId__c = sP.Required_Product__c;
                        relProd.Quantity__c = cvProd.Quantity__c;
                        relProd.Number_info__c = cvProd.Number_info__c;
                        relProd.Locked_Edit__c = True;   
                        relProd.Locked_Delete__c = True;        
                        
                        insert relProd;
                    }
                }
            }
        }
    }
    if(RecursiveHandler.isNotRecursive)
    {
        RecursiveHandler.isNotRecursive = false;

//#############################################################################
//ROLL UP SUMMARY
//#############################################################################
        Decimal runTotal = 0;
        List<Cloud_Voice_Site__c> site = [SELECT Total_CPE__c FROM Cloud_Voice_Site__c WHERE Id IN :cvsId];
        List<Cloud_Voice_Site_Product__c> prod = new List<Cloud_Voice_Site_Product__c>();
        cvsProdId = [SELECT Id, Type__c, Grouping__c, A_Code__c, Unit__c, Ports__c, Purchasing_Option__c, Finance_Option__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c IN :cvsId]; 
        Set<Id> totalCPEId = New Set<Id>();
        Set<Id> totalLicencesId = New Set<Id>();
        Set<Id> totalSoftPhonesId = New Set<Id>();
        Set<Id> totalBasicId = New Set<Id>();
        Set<Id> totalConnectId = New Set<Id>();
        Set<Id> totalCollaborateId = New Set<Id>();
        Set<Id> totalLinesId = New Set<Id>();
        Set<Id> totalNewLinesId = New Set<Id>();
        Set<Id> totalCallRecId = New Set<Id>();
        Set<Id> totalUCBusId = New Set<Id>();
        Set<Id> totalPortsId = New Set<Id>();
        Set<Id> totalFinanceSelId = New Set<Id>();
        Set<Id> totalFinanceTotId = New Set<Id>();

        for(Cloud_Voice_Site_Product__c cvsProd:cvsProdId){
            if (cvsProd.Unit__c == null){
                cvsProd.Unit__c = '';
            }
            //Total CPE
            if((cvsProd.Type__c == 'CPE' && cvsProd.Grouping__c == 'Phone')){
                totalCPEId.Add(cvsProd.Id);
            }
            //Total softphones
            if(cvsProd.A_Code__c == 'CV0000' || cvsProd.A_Code__c == 'CP0000'){
                totalSoftPhonesId.Add(cvsProd.Id);
                totalLicencesId.Add(cvsProd.Id);
            }
            //Total basic
            if(cvsProd.Grouping__c == 'Basic' || cvsProd.Unit__c == 'Basic'){
                totalBasicId.Add(cvsProd.Id);
                totalLicencesId.Add(cvsProd.Id);
            }
            //Total Connect
            if(cvsProd.Grouping__c == 'Connect' || cvsProd.Unit__c.contains('Connect')){
                totalConnectId.Add(cvsProd.Id);
                totalLicencesId.Add(cvsProd.Id);
            }
            //Total Collaborate
            if(cvsProd.Grouping__c == 'Collaborate' || cvsProd.Unit__c.contains('Collaborate')){
                totalCollaborateId.Add(cvsProd.Id);
                totalLicencesId.Add(cvsProd.Id);
            }
            //Total Lines
            if((cvsProd.Type__c == 'Line' && cvsProd.Grouping__c == 'Connection')){
                totalLinesId.Add(cvsProd.Id);
            }
            //Total New Lines
            if((cvsProd.Type__c == 'Line' && cvsProd.Grouping__c != 'Connection')){
                totalNewLinesId.Add(cvsProd.Id);
            }
            //Total CallRecording
            if(cvsProd.A_Code__c == 'B44108'){
                totalCallRecId.Add(cvsProd.Id);
            }
            //Total UCBus
            if(cvsProd.A_Code__c == 'B44106'){
                totalUCBusId.Add(cvsProd.Id);
            }
            //Total Ports
            if(cvsProd.Ports__c > 0){
                totalPortsId.Add(cvsProd.Id);
            }
            //Total FinanceSelected
            if(cvsProd.Purchasing_Option__c == 'Finance' || cvsProd.Purchasing_Option__c == 'Outright Sale'){
                totalFinanceSelId.Add(cvsProd.Id);
            }
            //Total FinanceTotal
            if(cvsProd.Finance_Option__c == True){
                totalFinanceTotId.Add(cvsProd.Id);
            }
        }    
        //Total CPE
        if((totalCPEId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalCPEId AND Type__c = 'CPE' AND Grouping__c = 'Phone'];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;
                    }
                }
                s.Total_CPE__c = runTotal;
            }
        }  
        //Total softphones
        if((totalSoftPhonesId.Size() > 0 || totalLicencesId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalLicencesId AND (A_Code__c = 'CV0000' OR A_Code__c = 'CP0000')];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;
                    }
                }
                s.zSoftPhone__c = runTotal;
            }
        }
        //Total basic
        if((totalBasicId.Size() > 0 || totalLicencesId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalLicencesId AND Type__c = 'License' AND (Grouping__c = 'Basic' OR Unit__c = 'Basic')];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;
                    }
                }
                s.Total_CV_Basic_Licenses__c = runTotal;
            }
        }
        //Total Connect
        if((totalConnectId.Size() > 0 || totalLicencesId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalLicencesId AND Type__c = 'License' AND (Grouping__c = 'Connect' OR Unit__c Like 'Connect%')];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;}
                    }
                s.Total_CV_Connect_Licenses__c = runTotal;
            }
        }
        //Total Collaborate
        if((totalCollaborateId.Size() > 0 || totalLicencesId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalLicencesId AND Type__c = 'License' AND (Grouping__c = 'Collaborate' OR Unit__c Like 'Collaborate%')];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;}
                    }
                s.Total_CV_Collaborate_Licenses__c = runTotal;
            }
        }
        //Total Lines
        if((totalLinesId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalLinesId AND Type__c = 'Line' AND Grouping__c = 'Connection'];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;}
                    }
                s.Total_Lines__c = runTotal;
            }
        }
        //Total New Lines
        if((totalNewLinesId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalNewLinesId AND Type__c = 'Line' AND Number_Info__c Like '%New%' AND Grouping__c <> 'Connection'];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;}
                    }
                s.Total_New_Lines__c = runTotal;
            }
        }
        //Total CallRecording
        if((totalCallRecId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalCallRecId AND A_Code__c = 'B44108'];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;}
                    }
                s.zCallRecording__c = runTotal;
            }
        }
        //Total UCBus
        if((totalUCBusId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Quantity__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalUCBusId AND A_Code__c = 'B44106'];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Quantity__c;}
                    }
                s.zUCBusiness__c = runTotal;
            }
        }
        //Total Ports
        if((totalPortsId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Ports__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalPortsId AND Ports__c >0];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Ports__c;}
                    }
                s.LAN_Max_Ports__c = runTotal;
            }
        }
        //Total FinanceSelected
        if((totalFinanceSelId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Total_One_Off_Cost__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalFinanceSelId AND Purchasing_Option__c = 'Finance'];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Total_One_Off_Cost__c;}
                    }
                s.Total_Selected_Finance__c = runTotal;
            }
        }
        //Total FinanceTotal
        if((totalFinanceTotId.Size() > 0) || runDelete){
            prod = [SELECT Cloud_Voice_Site__c, Total_One_Off_Cost__c FROM Cloud_Voice_Site_Product__c WHERE Id IN :totalFinanceTotId AND Finance_Option__c = True];
            for(Cloud_Voice_Site__c s:site){runTotal = 0;
                for(Cloud_Voice_Site_Product__c p:prod){
                    if(s.Id == p.Cloud_Voice_Site__c){
                        runTotal = runTotal+p.Total_One_Off_Cost__c;}
                    }
                s.Total_Financeable__c = runTotal;
            }
        }

        //GROUPED AS USING THE SAME INITIAL CRITERIA     #############################################################################
        //Total Power
        prod = [SELECT Cloud_Voice_Site__c, Power__c, Total_Care_pm__c, Total_Discount_Initial__c, Total_Discount_Recurring__c FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c IN :cvsId];
        for(Cloud_Voice_Site__c s:site){runTotal = 0;
            for(Cloud_Voice_Site_Product__c p:prod){
                if(s.Id == p.Cloud_Voice_Site__c){
                    runTotal = runTotal+p.Power__c;}
                }
            s.Power__c = runTotal;
        }
        //Total Care
        for(Cloud_Voice_Site__c s:site){runTotal = 0;
            for(Cloud_Voice_Site_Product__c p:prod){
                if(s.Id == p.Cloud_Voice_Site__c){
                    runTotal = runTotal+p.Total_Care_pm__c;}
                }
            s.Site_Recurring_Charges_Total_Care__c = runTotal;
        }
        //Total Initial Discount
        for(Cloud_Voice_Site__c s:site){runTotal = 0;
            for(Cloud_Voice_Site_Product__c p:prod){
                if(s.Id == p.Cloud_Voice_Site__c){
                    runTotal = runTotal+p.Total_Discount_Initial__c;}
                }
            s.Site_Discounts_Initial__c = runTotal;
        }
        //Total Recurring Discount
        for(Cloud_Voice_Site__c s:site){runTotal = 0;
            for(Cloud_Voice_Site_Product__c p:prod){
                if(s.Id == p.Cloud_Voice_Site__c){
                    runTotal = runTotal+p.Total_Discount_Recurring__c;}
                }
            s.Site_Discounts_Recurring__c = runTotal;
        }
        update site;
    }
HelperRollUpSummary.setCVSiteStop(true);

}