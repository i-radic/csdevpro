trigger GenieLinkAfter on Genie_Link__c (after insert, after update) {
    List<Genie_Link__c> records = new List<Genie_Link__c>();
    if(Trigger.isInsert) {
        for(Genie_Link__c record : Trigger.new) {
            if(record.Visibility__c != null) records.add(record);
       }  
   } else if(Trigger.isUpdate) {
       for(Genie_Link__c record : Trigger.new) {
           if(record.Visibility__c != Trigger.oldMap.get(record.id).Visibility__c) records.add(record);
       }
   }
   if(records.size() > 0) GenieController.recalculateSharing(records);
}