/**************************************************************************************************************************************************
        Trigger Name : CSAT_Feedback_SRM_BM
        Handler Class Name : CSATContactFeedbackTriggerHandler
        Helper Class Name : CSATContactFeedbackTriggerHelper
        Test Class Name : Test_CSAT_Feedback_SRM_BM
        Description : Trigger to Insert and Update Survey Response Records, And to Calculate RollUp Summary, to Update in CSAT Record
        Version : V0.2
        Last Modified Author Name : BALAJI MS
        Last Modified Date : 08/09/2017
 *************************************************************************************************************************************************/

trigger CSAT_Feedback_SRM_BM on CSAT_Contact_Feedback__c (before Insert,before update, after Insert, after Update, after delete, after undelete) {
    
    If(Trigger.IsBefore){
        
        set<id> Accounts = new set<id>(); 
        Map<string,Id> UserId = new Map<string,Id>();
        Map<Id,Id> AccountOwner = new Map<Id,Id>();
        
        for(CSAT_Contact_Feedback__c CSAT_Feedback:Trigger.New){
            Accounts.add(CSAT_Feedback.Account__c);                 
        }
        
        for(Lead_Locator__c L : [select id,Person_Role__c,User__c,Account__c,Account__r.OwnerId from Lead_Locator__c where Account__c IN : Accounts]){
            
            if(L.Person_Role__c == 'Service Manager'){
                UserId.put('Service Manager-'+L.Account__c,L.User__c);    
            }if(L.Person_Role__c == 'Business Manager'){
                UserId.put('Business Manager-'+L.Account__c,L.User__c);    
            }
            AccountOwner.put(L.Account__c,L.Account__r.OwnerId);
        }
        
        for(CSAT_Contact_Feedback__c CSAT_Feedback:Trigger.New){
            if(UserId.containsKey('Service Manager-'+CSAT_Feedback.Account__c)){              
                CSAT_Feedback.SRM__c = UserId.get('Service Manager-'+CSAT_Feedback.Account__c);
            }else{
                CSAT_Feedback.SRM__c = null;
            }
            if(UserId.containsKey('Business Manager-'+CSAT_Feedback.Account__c)){ 
                CSAT_Feedback.BM__c = UserId.get('Business Manager-'+CSAT_Feedback.Account__c);                  
            }else{
                CSAT_Feedback.BM__c = null;
            }             
            if(AccountOwner.containsKey(CSAT_Feedback.Account__c))
                CSAT_Feedback.Accounts_Owner__c = AccountOwner.get(CSAT_Feedback.Account__c);      
        } 
        
        //  Medallia Integration         
        
        If(Trigger.IsInsert)
            CSATContactFeedbackTriggerHandler.BeforeInsertHandler(Trigger.new);
        
        //Recurssion check to void Trigger to fire again and again in Before Update Process
        
        If(Trigger.IsUpdate && CSATContactFeedbackTriggerHandler.BeforeUpdateRecurssionFlag){
            
            //Enabling Recurssion Flag
            
            CSATContactFeedbackTriggerHandler.BeforeUpdateRecurssionFlag = False;
            CSATContactFeedbackTriggerHandler.BeforeUpdateHandler(Trigger.NewMap, Trigger.OldMap);
            
        }
        
    }
    
    If(Trigger.IsAfter){
        
        If(Trigger.IsInsert)
            CSATContactFeedbackTriggerHandler.AfterInsertHandler(Trigger.newMap, Trigger.New);
        
        //Recurssion check to void Trigger to fire again and again in After Update Process
        
        If(Trigger.IsUpdate && CSATContactFeedbackTriggerHandler.AfterUpdateRecurssionflag){
            
            //Enabling Recurssion Flag
            
            CSATContactFeedbackTriggerHandler.AfterUpdateRecurssionflag = False;
            CSATContactFeedbackTriggerHandler.AfterUpdateHandler(Trigger.NewMap, Trigger.OldMap, Trigger.New);  
            
        }
        
    }
    
    If(Trigger.IsDelete){
        
        If(Trigger.IsDelete)
            CSATContactFeedbackTriggerHandler.AfterDeleteHandler(Trigger.Old);
        
        If(Trigger.IsUnDelete)
            CSATContactFeedbackTriggerHandler.AfterUnDeleteHandler(Trigger.Old); 
        
    }
    
}