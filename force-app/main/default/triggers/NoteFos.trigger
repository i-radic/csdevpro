trigger NoteFos on Note (after insert, after update) {

    for (Note nb:Trigger.new){
        String thisID = nb.ParentID;
    
        If ( thisID.startsWith('006')){
            User Usr = [Select extension from user where id = :nb.OwnerId];
            Opportunity Opps = [Select id, StageName, Name, Opportunity_Id__c, CreatedDate, Owner__c, Owner_Dept__c, Service_Site__c, Description, Lead_Source__c, Product__c From Opportunity where id = :nb.ParentID];
            if((opps.Lead_Source__c == 'Cosine') && (nb.IsPrivate == false)){
                opps.Description = nb.LastModifiedDate +' | Title: '+ nb.Title +' | Body: ' +nb.Body + ' | Extn:' + Usr.Extension + '\n'+ opps.Description ;
                update Opps;
            }
            
            //Updating Notes for LFS opportunity
            if((opps.Lead_Source__c == 'Leads from Service') && (nb.IsPrivate == false)){
                opps.Description = nb.LastModifiedDate +' | Title: '+ nb.Title +' | Body: ' +nb.Body + ' | Extn:' + Usr.Extension + '\n'+ opps.Description ;
                update Opps;
            }
            
            // send mail to LFS users
            if(opps.Service_Site__c == 'BT Business Direct' && opps.Lead_Source__c == 'Leads from Service' && opps.Product__c == 'Leased Lined /Private Circuits ( BTnet)'){

                User user = [SELECT Id,Name , email FROM User WHERE Id = :Opps.Owner__c];
                String emailBody = '<br />The status of your Leads 2 Service Opportunity has changed to : <b>'+Opps.StageName+'<br />' +
                                   '<br />Opportunity Name : </b>'+Opps.Name +
                                   '<b><br />Opportunity ID : </b>'+Opps.Opportunity_Id__c +
                                   '<b><br />Created Date : </b>'+Opps.CreatedDate +
                                   '<b><br />Opportunity Owner : </b>' +user.Name +
                                   '<b><br />Owner Department : </b>' +Opps.Owner_Dept__c +
                                   '<b><br />Notes:<br /></b>' +nb.Body +
                                   '<br /><br />' + 
                                   'Thank you<br /> <br />' +
                                   '<font color=#ff0000 size=1>Note: This email was sent from a notification-only email address that cannot accept incoming email. Please do not reply to this message</font>';
                List<string> senderList = new List<string>();                
                senderList.add(user.email);
                senderList.add(Userinfo.getUserEmail());                
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();                
                mail.setToAddresses(senderList);
                //mail.setSenderDisplayName('BT Business No Reply');                              
                mail.setSubject(Opps.Opportunity_Id__c + ' - Notes Amended to L2S Opportunity');                
                mail.setHtmlBody(emailBody);  
                mail.setOrgWideEmailAddressId('0D220000000CbZl');                        
                mail.setSaveAsActivity(false);                
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });           
            }
        }
    }
}