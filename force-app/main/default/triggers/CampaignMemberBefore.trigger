trigger CampaignMemberBefore on CampaignMember (before insert, before update, before delete) {

//  28/03/2011      Anil Devunoori              Added code related to CR2180 - Last Activity on a campaign
//  25/06/2012      John McGovern               Changes to capture tasks on member deletions

if(trigger.isUpdate || trigger.isInsert){
Set<Id> AccIds = new Set<Id>();
Set<Id> allCampaignIds = new Set<Id>(); //CR2180
Map<Id,String> AccIdSubSect=new Map<Id,String>();
 for (CampaignMember cm : trigger.new) {

        if(cm.CampaignId != null)          
          allCampaignIds.add(cm.CampaignId);//CR2180
           
        if(cm.ContactId != null && cm.CampaignId != null)
            cm.Unique_Key__c = cm.ContactId + '_' + cm.CampaignId;
         
         if((cm.Status=='Business has Ceased')&&(cm.Contact_Account_Sector__c=='BT Local Business' ||cm.Contact_Account_Sector__c=='Enterprise'))
            AccIds.add(cm.Contact_Account_Id__c);
            AccIdSubSect.put(cm.Contact_Account_Id__c,cm.Contact_Account_Sub_Sector__c);
        }
  system.debug('MMMMMMMMMMMMMMMMMMM'+AccIdSubSect);
  List<Account> CeasedAccs=[Select Account.Id,Ceased_Account__c From Account where Id IN:AccIds]; 
  for(Account A:CeasedAccs){
  if(!A.Ceased_Account__c){
     A.Ceased_Account__c=True;
     //A.Ceased_Date__c=System.Today();
     //A.Ceased_Sub_Sector__c=AccIdSubSect.get(A.Id);
     //A.Account_Ceased_By__c=UserInfo.getName();
    } 
  }     
  Update CeasedAccs;
           
  //CR2180 - start
   if(allCampaignIds.size()>0){                      
          list<Campaign> allcampaignlist = new list<Campaign>();            
          updateCampaignLastContacted updateLastActivityDate = new updateCampaignLastContacted();                                 
          allcampaignlist = updateLastActivityDate.updateCampaign(allCampaignIds);                       
          update allcampaignlist;                          
      } 
   
    //CR2180 - end
}

// ArchDel code for task capture
if(trigger.isDelete){    
    //create Id maps for campaign and contacts
    Set<Id> delCampIds = new Set<Id>();
    Set<Id> delConIds = new Set<Id>();
    //set static variables to prevent script limits
    StaticVariables.setTaskIsRun(true);
    StaticVariables.setcontactBeforeTriggerRun(true);
    StaticVariables.setContactIsRunBefore(true);  
    StaticVariables.setTaskIsBeforeRun(true);     
        
    for (CampaignMember cm : trigger.old) {
        if(cm.CampaignId != null && cm.zzArchDel__c == 'True') 
        delCampIds.add(cm.CampaignId);//set list of campaigns where auto member deletions is taking place
        delConIds.add(cm.ContactId);//set list of contacts where auto member deletions is taking place
    }
    
    //select tasks related to user and campaigns and delete
    Map<id,Task> tMap = new Map<id,Task>([SELECT Id FROM Task WHERE WhatId IN:delCampIds AND WhoId IN:delConIds AND IsRecurrence = False AND IsDeleted = False LIMIT 50000 ALL ROWS]);
    List<Task> campTasks= tMap.values() ;
    //List<Task> campTasks=[SELECT Id FROM Task WHERE WhatId IN:delCampIds AND WhoId IN:delConIds ALL ROWS]; 
    delete campTasks;
    
    // write the campaign outcome to the contact object storing any exist data
    Map<id,Contact> cMap = new Map<id,Contact>([SELECT Id, zCampaignHistory__c FROM Contact WHERE Id IN:delConIds LIMIT 50000]);
    List<Contact> campContacts= cMap.values() ;
    
    //create and add text of campaign outcomes and write to the contact.
    String jmText = null;
    for (CampaignMember cm : trigger.old) {
        for (Contact cc : campContacts) {
            if(cm.ContactId == cc.Id){
            jmText = cm.Campaign_Name__c + ' on ' + cm.LastModifiedDate.day() + '/' + cm.LastModifiedDate.month() + '/' + cm.LastModifiedDate.year() + '. Outcome: ' + cm.Status;
                if(cc.zCampaignHistory__c == Null){
                    cc.zCampaignHistory__c = jmText;
                }
                else {
                    cc.zCampaignHistory__c = jmText + '\n' + cc.zCampaignHistory__c ;
                }
            }
        }
    }
    update campContacts;
}          
}