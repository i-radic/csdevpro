/**
 * All basket totals triggers
 */
trigger CS_AllBasketTotalsTriggers on Basket_Totals__c (before update) {
    CS_TriggerHandler.execute(new CS_BasketTotalsTriggerDelegate());
}