trigger LeadBefore on Lead (before insert, before update) {
    
    Set<String> ownerids = new Set<String>();
    List<User> lowners = new List<User>();
    Map<string, string> lownerIdNames = new Map<string, string>();
    for(Lead l: Trigger.new){
        ownerids.add(l.OwnerId);
    }
    for(User u: [select u.Id, u.FirstName, u.LastName from User u where u.Id in : ownerids]){
        lownerIdNames.put(u.Id, u.FirstName + ' ' + u.LastName);
    }
    for(Lead l: Trigger.new){
        if(lownerIdNames.containsKey(l.OwnerId)) {
            l.Owner_Name__c = lownerIdNames.get(l.OwnerId);
        }
        // fix for BTSport error while daily loads - line 20|FATAL_ERROR|System.LimitException: Apex CPU time limit exceeded
        if(l.LeadSource != NULL && l.LeadSource !='BT Sport'){
            
            //Adobe Campaign Management Changes
            if(l.LeadSource!=NULL && l.RecordTypeId!=Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Adobe Campaign').getRecordTypeId() && l.Description!=NULL){
                //Adobe Campaign Management Changes

                if(l.LeadSource == 'Desk Landscape' || l.LeadSource == 'COT Landscape' || l.LeadSource == 'BS Classic Landscape' || l.LeadSource == 'LEM Landscape'){
                    //Code as per the CR2739 ---- Start
                    if(l.Description.contains('Broadband')) { l.Broadband_Count__c = 1; } else { l.Broadband_Count__c = 0; } 
                    if(l.Description.contains('Calls')) {   l.Calls_Count__c = 1; } else { l.Calls_Count__c = 0; }
                    if(l.Description.contains('Lines')) { l.Lines_Count__c = 1; } else { l.Lines_Count__c = 0; }
                    if(l.Description.contains('Mobile')) { l.Mobile_Count__c = 1; } else { l.Mobile_Count__c = 0; }
                    if(l.Description.contains('Switch')) { l.Switch_Count__c = 1; } else { l.Switch_Count__c = 0; }
                    if(l.Description.contains('LAN')) { l.LAN_Count__c = 1; } else { l.LAN_Count__c = 0; }
                    if(l.Description.contains('WAN')) { l.WAN_Count__c = 1; } else { l.WAN_Count__c = 0; }
                    //Code as per the CR2739 ---- Start
                }
            }
        }
    }
}