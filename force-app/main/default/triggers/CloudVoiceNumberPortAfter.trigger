trigger CloudVoiceNumberPortAfter on Number_Port_Line__c (after insert) {
Integer count = 0;

if(!HelperRollUpSummary.getCVSiteStop() || system.Test.isRunningTest()){   
    Set<Id> cvIds = new Set<Id>();   
    Set<Id> oppIds = new Set<Id>(); 

    for (Number_Port_Line__c n:trigger.New){
        cvIds.add(n.Cloud_Voice__c);
    }

    List<Cloud_Voice__c> cvList= [SELECT ID, Opportunity__r.Id, zTotalPortLines__c FROM Cloud_Voice__c WHERE Id IN :cvIds];
    for (Cloud_Voice__c c:cvList){ 
        if(c.zTotalPortLines__c > 0){
            oppIds.add(c.Opportunity__r.Id);
        }
    }    
    List<Opportunity> oppList= [SELECT ID, zCVNumberPort__c FROM Opportunity WHERE Id IN :oppIds];   

    for(Opportunity o:oppList){
        o.zCVNumberPort__c = 0;
    }
    update oppList;
}
}