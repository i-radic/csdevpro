/*
###########################################################################
# Project Name..........: EveryThing Everywhere
# File..................: EE_setAttachmentIdOnArtemisDeal 
# Version...............: 1
# Created by............: Kanishk Prasad
# Created Date..........: 25-September-2014
# Last Modified by......: Adam Soobroy   
# Last Modified Date....: 02-April-2018  
# Description...........: This trigger works on after insert of Attachment and populates the corresponding Deal with the Attachment id
*/
trigger EE_setAttachmentIdOnArtemisDeal on Attachment (after insert) {
    Map<String,Artemis_Deal__c> mapArtemisDeal=new Map<String,Artemis_Deal__c>();
    for(Attachment attachment :Trigger.New)
    {
        String sObjName = attachment.parentid.getSObjectType().getDescribe().getName(); 
        //if(((String)attachment.parentid).startsWith('a1N'))
         if(sObjName == 'Artemis_Deal__c')
            mapArtemisDeal.put(attachment.parentId,new Artemis_Deal__c(id=attachment.parentid,Attachment_Id__c=attachment.id));
    }
    if(mapArtemisDeal.size()>0)
        update mapArtemisDeal.values();
}