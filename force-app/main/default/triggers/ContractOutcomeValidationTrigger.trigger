/**
 * Apex Trigger to display Reason for failure multi-picklist values in different lines with Bullet points (Used to display in Email Templates.
 * Test Class: TestContractOutcomeValidationTrigger 
 */

trigger ContractOutcomeValidationTrigger on Contract_Validation_Outcome__c (before insert,before update) {

    for(Contract_Validation_Outcome__c  outcome : Trigger.New)
    {
        if(outcome.Reason_for_Failure__c != null)
        {
            String richTextString = '<ul>';
            
            if(outcome.Reason_for_Failure__c.contains(';'))
            {
                String[] outcomes = outcome.Reason_for_Failure__c.split(';');
                
                
                for(String outcomeStr : outcomes)
                {
                    richTextString += '<li>' + outcomeStr + '</li>';
                }
                
                
                richTextString += '</ul>';
            }
            else
            {
                richTextString += '<li>' + outcome.Reason_for_Failure__c+ '</li></ul>';
            }
            
            outcome.Reasons_for_failure_Rich_Text__c = richTextString ;
        }
        else
            outcome.Reasons_for_failure_Rich_Text__c = '';
            
    }

}