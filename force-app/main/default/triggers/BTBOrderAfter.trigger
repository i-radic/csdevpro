trigger BTBOrderAfter on BTB_Order__c (after insert, after update) {
	RecordType rt = [select id from RecordType where SobjectType='Case' and name ='Order Discrepancy' limit 1];
	RecordType rt_creditvet = [select id from RecordType where SobjectType='Case' and name ='Credit Vet' limit 1];
	List<Case> casesToUpdate = new List<Case>();
	Map<String,Id> mOrderVolsToOrderIds = new Map<String, Id>();
	Map<String,String> mOrderVolsToOrderStatus = new Map<String, String>();
	Map<Id,Id> mCasesIds = new Map<Id, Id>();
	
	for (BTB_Order__c o : Trigger.new){
		if(o.OV_ORDER_NUM__c != null) {
    		mOrderVolsToOrderIds.put(o.OV_ORDER_NUM__c.toLowerCase(), o.Id);
    		mOrderVolsToOrderStatus.put(o.OV_ORDER_NUM__c.toLowerCase(), o.SFDC_STATUS__c);
		}
	}
	if(trigger.isInsert) {
		for(Case c : [select id, Vol_Reference__c, RecordTypeId from Case where BTB_Order__c  = null and Vol_Reference__c != null and Vol_Reference__c in :mOrderVolsToOrderIds.keySet() and (RecordTypeId = :rt_creditvet.Id or RecordTypeId = :rt.Id)]){
			if(mOrderVolsToOrderIds.containsKey(c.Vol_Reference__c.toLowerCase())){
				c.BTB_Order__c = mOrderVolsToOrderIds.get(c.Vol_Reference__c.toLowerCase());
				mCasesIds.put(c.Id, c.Id);
				casesToUpdate.add(c);
			}
	    }
	}
	if(trigger.isUpdate) {
		for(Case c : [select id, Vol_Reference__c, RecordTypeId, Status, Response__c, Case_Age_In_Business_Hours__c from Case where BTB_Order__c  != null and Vol_Reference__c != null and Vol_Reference__c in :mOrderVolsToOrderIds.keySet() and (RecordTypeId = :rt_creditvet.Id or RecordTypeId = :rt.Id)]){
			if(mOrderVolsToOrderStatus.containsKey(c.Vol_Reference__c.toLowerCase())){
				system.debug('Case Status : ' + c.Status);
				if(c.RecordTypeId == rt.Id && (c.Status != 'Closed' && c.Status != 'Auto-Closed') && (mOrderVolsToOrderStatus.get(c.Vol_Reference__c.toLowerCase()) == 'Cancellation in OV' || mOrderVolsToOrderStatus.get(c.Vol_Reference__c.toLowerCase()) == 'Complete')) {
					c.Response__c = c.Response__c + '\n\n ***Automatically closed due to oneview order being closed/cancelled.***';
					c.Status = 'Auto-Closed';
					c.Case_Age_In_Business_Hours__c = 0;
					if(!mCasesIds.containsKey(c.Id)){
						casesToUpdate.add(c);
					}
				}
			}
	    }
	}
	system.debug('Case Size' + casesToUpdate.size()); 
    if(!casesToUpdate.isEmpty()) {
    	system.debug('Update Statement Loop'); 
    	update casesToUpdate;
    }
}