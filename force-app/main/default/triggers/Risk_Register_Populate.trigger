trigger Risk_Register_Populate on Risk_Register__c (before insert) {
	
	public Account account = null;
	
	public Contract contract = null;
	public Asset_Line_Count__c lines = null;
	
	for (Risk_Register__c r:Trigger.new) {
		if (r.account__c != null) {
			//get account info and populate
			List<Account> accounts = [select Id, SAC_Code__C, LOB_code__C,OwnerId from Account where Id = :r.account__c];
			account = accounts[0];
			
			//Get SM
			User UvsSm = [Select u.id, u.managerId from user u where u.ID =:Userinfo.getUserId()];
			//Get DGM			
			User UvsDgm = [Select u.id, u.managerId from user u where u.Id =:UvsSm.managerId];			
		   	//Get GM
			User UvsGm = [Select u.id, u.managerId from user u where u.Id =:UvsDgm.managerId];			  
			
			//update object
			r.AM__c = account.OwnerId;
			
			r.UVS_SM__c = UvsSm.managerId;
			r.UVS_DGM__c = UvsDGM.managerId;
			r.UVS_GM__c = UvsGm.managerId;
			r.SAC_Code__c = account.SAC_Code__C;
			r.Treatment__C = account.LOB_code__C;
			
			if (r.UVS_SM__c != null) {
				r.Risk_Escalated_To__c = r.UVS_SM__c;
			} else {
				r.Risk_Escalated_To__c = r.UVS_DGM__c;
			}			
		}
	}
	
	//get contract info if contract number entered
	for (Risk_Register__c r:Trigger.new) {
		if (r.contract__c != null) {
			
			List<Contract> contracts = [select AccountId, Id, Adder_Status__c, Adder_Contract_ID__c, Current_Monitor_Spend__c, EndDate, Committed_Spend__c, Current_Monitor_Spend_Date__c, Base_Product__c  from Contract where Id = :r.contract__c];
			contract = contracts[0];
			List<Account> accounts2 = [select Id, SAC_Code__C, LOB_code__C,OwnerId from Account where Id = :contract.Accountid];
			account = accounts2[0];
			
			//Get SM
			User UvsSm = [Select u.id, u.managerId from user u where u.ID =:Userinfo.getUserId()];	
			//Get DGM
			User UvsDgm = [Select u.id, u.managerId from user u where u.Id =:UvsSm.managerId];
			//Get GM		
			User UvsGm = [Select u.id, u.managerId from user u where u.Id =:UvsDgm.managerId];	
			
			//update Account related objects
			r.Account__c = account.ID;
			r.AM__c = account.OwnerId;
			
			r.UVS_SM__c = UvsSm.managerId;
			r.UVS_DGM__c = UvsDGM.managerId;
			r.UVS_GM__c = UvsGm.managerId;
			r.SAC_Code__c = account.SAC_Code__C;
			r.Treatment__C = account.LOB_code__C;
			if (r.UVS_SM__c != null) {
				r.Risk_Escalated_To__c = r.UVS_SM__c;
			} else {
				r.Risk_Escalated_To__c = r.UVS_DGM__c;
			}		
			//update Contract related objects
			r.Expiry_Date__c = contract.EndDate;
			r.Committed_Spend__C = contract.Committed_Spend__c;
			r.Current_Monitor_Spend__c = contract.Current_Monitor_Spend__c;
			r.Adder_Contract_ID__c = contract.Adder_Contract_ID__c;
			r.Current_Monitor_Spend_Date__c = contract.Current_Monitor_Spend_Date__c;
			r.Base_Product__c = contract.Base_Product__c;
		}
	}
}