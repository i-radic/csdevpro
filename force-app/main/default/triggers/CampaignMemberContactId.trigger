trigger CampaignMemberContactId on CampaignMember (before insert, before update,after insert) {
    
    if(Trigger.isBefore)
    {
        Set<Id> cmIds = new Set<Id>();
        for (CampaignMember cm : trigger.new) {
            cmIds.add(cm.ContactId);
        }
        
        Map<Id, String> cMember = new Map<Id, String>();
        List<Contact> mContacts = [Select Id, IntegrationId__c From Contact Where Id IN :cmIds];
        
        for (Contact ci :mContacts){
            cMember.put(ci.Id, ci.IntegrationId__c);
        }
        
        for (CampaignMember cm:Trigger.new){
            if (cMember.containsKey(cm.ContactId) == True){
                cm.Contact_Id__c = cMember.get(cm.ContactId);
            }
        }
    }
    //Related to Eloqua Change- Update the Parent Campaign field "Campaign Split Status" to 'To be Done'
    if(Trigger.isAfter && Trigger.isInsert)
    {
        set<Id> campIds= new Set<Id>();
        List<Campaign> updateCampaigns= new List<Campaign>();
        for(CampaignMember campMem:[Select Id,CampaignId from CampaignMember where Id IN: Trigger.New ])
        {
            campIds.add(campMem.CampaignId);
        }
        
        for(Campaign c:[Select Id,Campaign_Split_Status__c from Campaign where Id IN: campIds AND Campaign_Type__c = 'BTLB Split'])
        {
            if(c.Campaign_Split_Status__c=='Done Successfully')
            {
                c.Campaign_Split_Status__c='To be Done';
                updateCampaigns.add(c);
            }
        }
        update updateCampaigns;
    }
 }