trigger OpportunityTeamMemberAfter on OpportunityTeamMember (after insert, after update, after delete, after undelete) {
  OpportunityTeamMemberHandler handler  = OpportunityTeamMemberHandler.getHandler();
  if (Trigger.isInsert) {
    handler.afterInsert();
  } else if (Trigger.isUpdate) {
    handler.afterUpdate();
  } else if (Trigger.isDelete) {
    handler.afterDelete();
  } else if (Trigger.isUnDelete) {
    handler.afterUnDelete();
  }
}