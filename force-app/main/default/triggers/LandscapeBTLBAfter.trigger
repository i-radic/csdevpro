trigger LandscapeBTLBAfter on Landscape_BTLB__c (after update, after insert) {
//START write to Account
Set<id> LSIDs = new Set<id>();

//Get the id's      
for(Landscape_BTLB__c LS:Trigger.new){  
     LSIDs.add(LS.Customer__c);
}
    
    Map<ID, Account> acctsToUpdate = new Map<ID, Account>([select Id, Count_of_Contacts__c FROM Account WHERE Id in :LSIDs]);    
    
    for (Account acct : acctsToUpdate.values()) {
        for(Landscape_BTLB__c LS:Trigger.new){  
            acct.LS_Mobile_Last_Updated_By__c = LS.Mobile_Last_Updated_By__c;
            acct.LS_ISP_Last_Updated_By__c = LS.ISP_Last_Updated_By__c;
            acct.LS_Calls_Last_Updated_By__c = LS.Calls_Last_Updated_By__c;
            acct.LS_Lines_Last_Updated_By__c = LS.Lines_Last_Updated_By__c;
            acct.LS_Mobile_Last_Updated__c = LS.Mobile_Last_Updated__c;
            acct.LS_ISP_Last_Updated__c = LS.ISP_Last_Updated__c;
            acct.LS_Calls_Last_Updated__c = LS.Calls_Last_Updated__c ;
            acct.LS_Lines_Last_Updated__c = LS.Lines_Last_Updated__c ;
        }   
   }
   update acctsToUpdate.values();
//ENDwrite to Account  

// START insert only History object    
// insert custom history object
List<Landscape_BTLB_History__c> HistNew= new List <Landscape_BTLB_History__c>() ; 
string jmRun = null; 
for(Landscape_BTLB__c LS:Trigger.new){
if (Trigger.isInsert) {
system.debug('LS Id: '+ LS.Id);


If (LS.Lines_contract_expiry_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Lines contract expiry date';
            HistRec.New_Value__c = LS.Lines_contract_expiry_date__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q25';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Approx_when_did_your_biz_start_trading__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Approx when did your biz start trading';
            HistRec.New_Value__c = LS.Approx_when_did_your_biz_start_trading__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q30';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.IT_support_contract_renewal_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'IT support contract renewal date';
            HistRec.New_Value__c = LS.IT_support_contract_renewal_date__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11.3';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.LAN_contract_renewal_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'LAN contract renewal date';
            HistRec.New_Value__c = LS.LAN_contract_renewal_date__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10.3';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Finance_contract_expiry_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Finance contract expiry date';
            HistRec.New_Value__c = LS.Finance_contract_expiry_date__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.5';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Phone_system_maint_contract_expiry_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Phone system maint contract expiry date';
            HistRec.New_Value__c = LS.Phone_system_maint_contract_expiry_date__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.2';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Contract_expiry_for_calls_over_internet__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Contract expiry for calls over internet';
            HistRec.New_Value__c = LS.Contract_expiry_for_calls_over_internet__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'VOIP';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q29';
            jmRun = '1';                         
            HistNew.add(HistRec);          
        }
If (LS.Confirm_no_of_lines_your_company_has__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Confirm no of lines your company has';
            HistRec.New_Value__c = LS.Confirm_no_of_lines_your_company_has__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q22';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Do_you_use_BT_for_all_your_calls__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you use BT for all your calls';
            HistRec.New_Value__c = LS.Do_you_use_BT_for_all_your_calls__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q18';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Do_you_use_BT_for_all_your_lines__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you use BT for all your lines';
            HistRec.New_Value__c = LS.Do_you_use_BT_for_all_your_lines__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q23';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_a_call_contract_expiry_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a call contract expiry date';
            HistRec.New_Value__c = LS.Has_a_call_contract_expiry_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q19.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_line_contract_expiry_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has line contract expiry date';
            HistRec.New_Value__c = LS.Has_line_contract_expiry_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q24.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.How_much_spent_annually_on_calls__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How much spent annually on calls';
            HistRec.New_Value__c = LS.How_much_spent_annually_on_calls__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q21';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Are_you_planning_to_move_premises__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Are you planning to move premises';
            HistRec.New_Value__c = LS.Are_you_planning_to_move_premises__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q5';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.How_many_employees_are_not_office_based__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many employees are not office based';
            HistRec.New_Value__c = LS.How_many_employees_are_not_office_based__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.How_many_employees_in_your_company__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many employees in your company';
            HistRec.New_Value__c = LS.How_many_employees_in_your_company__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.How_many_sites_abroad__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many sites abroad';
            HistRec.New_Value__c = LS.How_many_sites_abroad__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.How_many_sites_in_the_UK__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many sites in the UK';
            HistRec.New_Value__c = LS.How_many_sites_in_the_UK__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Customer Details';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q3';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Do_you_have_a_website__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you have a website';
            HistRec.New_Value__c = LS.Do_you_have_a_website__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Do_you_sell_online__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you sell online';
            HistRec.New_Value__c = LS.Do_you_sell_online__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Does_your_company_have_Broadband__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Does your company have Broadband';
            HistRec.New_Value__c = LS.Does_your_company_have_Broadband__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q6';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_an_IT_Support_contract_renewal_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has an expiry date for ISP contract';
            HistRec.New_Value__c = LS.Has_an_IT_Support_contract_renewal_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q7.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Who_is_your_internet_service_provider__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who is your internet service provider';
            HistRec.New_Value__c = LS.Who_is_your_internet_service_provider__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q7';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Data_internet_security_provider__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Data & internet security provider';
            HistRec.New_Value__c = LS.Data_internet_security_provider__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8.3';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Who_is_your_leased_line_provider__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who is your leased line provider';
            HistRec.New_Value__c = LS.Who_is_your_leased_line_provider__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q31';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Leased_line_contract_end_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Leased line contract end date';
            HistRec.New_Value__c = LS.Leased_line_contract_end_date__c.format();   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q31.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Number_of_leased_line_connections__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Number of leased line connections';
            HistRec.New_Value__c = LS.Number_of_leased_line_connections__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q31.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Are_the_PC_s_networked_or_stand_alone__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Are the PC\'s networked or stand alone';
            HistRec.New_Value__c = LS.Are_the_PC_s_networked_or_stand_alone__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Are_the_sites_networked_together__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Are the sites networked together';
            HistRec.New_Value__c = LS.Are_the_sites_networked_together__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Do_you_have_a_server__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Do you have a server';
            HistRec.New_Value__c = LS.Do_you_have_a_server__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_a_network_contract_renewal_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a network contract renewal date';
            HistRec.New_Value__c = LS.Has_a_network_contract_renewal_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4.4';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_an_expiry_date_for_ISP_contract__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has an IT Support contract renewal date';
            HistRec.New_Value__c = LS.Has_an_expiry_date_for_ISP_contract__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.How_many_are_Laptops_Notebooks__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many are Laptops/Notebooks';
            HistRec.New_Value__c = LS.How_many_are_Laptops_Notebooks__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.No_of_company_PC_s_including_Apple_Mac_s__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'No of company PC\'s including Apple Mac\'s';
            HistRec.New_Value__c = LS.No_of_company_PC_s_including_Apple_Mac_s__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q9';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Who_manages_your_network__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who manages your network';
            HistRec.New_Value__c = LS.Who_manages_your_network__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q4.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Who_provides_your_IT_support__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who provides your IT support';
            HistRec.New_Value__c = LS.Who_provides_your_IT_support__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q11.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Who_supplies_your_network_LAN__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who supplies your network (LAN)';
            HistRec.New_Value__c = LS.Who_supplies_your_network_LAN__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'IT';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q10.2';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_a_mobile_contract_renewal_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a mobile contract renewal date';
            HistRec.New_Value__c = LS.Has_a_mobile_contract_renewal_date__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q16.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Use_company_mob_to_send_receive_emails__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Use company mob to send & receive emails';
            HistRec.New_Value__c = LS.Use_company_mob_to_send_receive_emails__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q17.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_a_contract_expiry_date_for_maint__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has a contract expiry date for maint';
            HistRec.New_Value__c = LS.Has_a_contract_expiry_date_for_maint__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_expiry_date_for_finance_contract__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has expiry date for finance contract';
            HistRec.New_Value__c = LS.Has_expiry_date_for_finance_contract__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.4';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_finance_on_existing_system__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has finance on existing system';
            HistRec.New_Value__c = LS.Has_finance_on_existing_system__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14.3';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.How_many_years_old_is_your_phone_system__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'How many years old is your phone system';
            HistRec.New_Value__c = LS.How_many_years_old_is_your_phone_system__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q13';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Who_manufactured_your_phone_switch_sys__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Who manufactured your phone switch sys';
            HistRec.New_Value__c = LS.Who_manufactured_your_phone_switch_sys__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q12';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.When_purchased_your_phone_switch_system__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'When purchased your phone switch system';
            HistRec.New_Value__c = LS.When_purchased_your_phone_switch_system__c.format();   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Telephone System';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q13.1';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Has_internet_calls_supp_contract_expiry__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Has internet calls supp contract expiry';
            HistRec.New_Value__c = LS.Has_internet_calls_supp_contract_expiry__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'VOIP';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q28';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Which_supplier_for_calls_over_internet__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Which supplier for calls over internet';
            HistRec.New_Value__c = LS.Which_supplier_for_calls_over_internet__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'VOIP';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q27';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.what_is_your_Domain_Name__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'what is your Domain Name';
            HistRec.New_Value__c = LS.what_is_your_Domain_Name__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'VOIP';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q32';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Internet_Solution__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Internet Solution';
            HistRec.New_Value__c = LS.Internet_Solution__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q34';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Internet_Supplier__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Internet Supplier';
            HistRec.New_Value__c = LS.Internet_Supplier__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q35';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.ISP_contract_expiry_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Internet Contract End Date';
            HistRec.New_Value__c = LS.ISP_contract_expiry_date__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet / Broadband';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q8';
            jmRun = '1';                         
            HistNew.add(HistRec);          
}
If (LS.Internet_Network__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Internet Network';
            HistRec.New_Value__c = LS.Internet_Network__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Internet';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q33';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Mobile_Handsets__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Mobile Handsets';
            HistRec.New_Value__c = LS.Mobile_Handsets__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q15';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Mobile_Supplier_Purchased__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Mobile Supplier';
            HistRec.New_Value__c = LS.Mobile_Supplier_Purchased__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q48';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Mobile_Contract_expiry_Date__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Mobile Contract End Date';
            HistRec.New_Value__c = string.valueof(LS.Mobile_Contract_expiry_Date__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q46';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Who_is_your_business_mobile_supplier__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Mobile Network';
            HistRec.New_Value__c = LS.Who_is_your_business_mobile_supplier__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q49';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Tablets__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Tablets';
            HistRec.New_Value__c = string.valueof(LS.Tablets__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q50';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Mobile_Broadband__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Mobile Broadband';
            HistRec.New_Value__c = string.valueof(LS.Mobile_Broadband__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Mobile';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q45';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Network_Solution__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Network Solution';
            HistRec.New_Value__c = string.valueof(LS.Network_Solution__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Network';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q53';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Network_Contract_End_Date__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Network Contract End Date';
            HistRec.New_Value__c = string.valueof(LS.Network_Contract_End_Date__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Network';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q52';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Who_supplies_your_network_LAN__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Network Supplier';
            HistRec.New_Value__c = string.valueof(LS.Who_supplies_your_network_LAN__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Network';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q54';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Network__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Network';
            HistRec.New_Value__c = LS.Network__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Network';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q51';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Voice__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Voice Solution';
            HistRec.New_Value__c = LS.Voice__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Voice';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q42';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Calls_contract_expiry_date__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Voice Contract End Date';
            HistRec.New_Value__c = LS.Calls_contract_expiry_date__c.format();
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Calls / Lines';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q20';
            jmRun = '1';                         
            HistNew.add(HistRec);          
}
If (LS.If_not_BT_who_do_you_use_for_your_lines__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Voice Network';
            HistRec.New_Value__c = LS.If_not_BT_who_do_you_use_for_your_lines__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Voice';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q24';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.If_not_BT_who_do_you_use_for_calls__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Voice Supplier';
            HistRec.New_Value__c = LS.If_not_BT_who_do_you_use_for_calls__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Voice';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q19';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Voice_Handsets__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Voice Handsets';
            HistRec.New_Value__c = string.valueof(LS.Voice_Handsets__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Voice';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q40';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Who_maintains_your_phone_switch_system__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Manufacturer';
            HistRec.New_Value__c = LS.Who_maintains_your_phone_switch_system__c;   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Voice';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q14';
            jmRun = '1';                     
            HistNew.add(HistRec);         
}
If (LS.Call_spend__c != Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Call Spend';
            HistRec.New_Value__c = string.valueof(LS.Call_spend__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Voice';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q38';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
If (LS.Lines__c!= Null) {
            Landscape_BTLB_History__c HistRec = new Landscape_BTLB_History__c();
            HistRec.Question__c = 'Lines';
            HistRec.New_Value__c = string.valueof(LS.Lines__c);   
            HistRec.Landscape_BTLB__c = LS.Id;
            HistRec.Section__c = 'Voice';
            HistRec.QuestionIdentifier__c = 'MKTG_BTLB:Q37';
            jmRun = '1';                     
            HistNew.add(HistRec);      
}
system.debug('HistNew: '+ HistNew);
}
}
    If (jmRun != null) {
        insert HistNew;
    }
// END insert only History object        
}