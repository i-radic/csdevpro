/**
 * All Product Basket Triggers
 */
trigger CS_AllProductBasketTriggers on cscfga__Product_Basket__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    CS_TriggerHandler.execute(new CS_ProductBasketTriggerDelegate());
}