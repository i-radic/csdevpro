trigger StartProxy  on Event (before insert, before update) {
	if(TriggerDeactivating__c.getInstance().Event__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
    for(Event t: Trigger.new)
    { 
    t.Start_Proxy__c = t.StartDateTime;                
    }      
}