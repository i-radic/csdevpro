trigger tgrOLICompetitor on OpportunityLineItem (Before insert,After update) {


    Set<String> oppIds = new Set<String>();

    for(OpportunityLineItem oli: trigger.new){
    oppIds.add(String.ValueOf(oli.OpportunityId));
    }
    List<Opportunity> oplist=[select Id,Main_Competitor__c,ACV_Calc__c from Opportunity where Id in :oppIds];


    if (trigger.isInsert) {
        for(OpportunityLineItem oli : trigger.new){
        Id opid=oli.OpportunityId;
        String ctgry=oli.Category__c;
        Double acv=oli.ACV_Calc__c;
        
       // List<Opportunity> oplist=[select Id,Main_Competitor__c,ACV_Calc__c from Opportunity where Id=:opid];
            Double acvNew=oplist[0].ACV_Calc__c + acv;
            if(acvNew>30000 && ctgry=='Main'){
            oplist[0].Main_Competitor__c=oli.Competitor__c; 
            try{
            update oplist;         
            }catch(Exception ex){system.debug(ex);}
            }       
        }
    }
     
     if (trigger.isUpdate) {
        for(OpportunityLineItem oli : trigger.new){
        Id opid=oli.OpportunityId;
        String ctgry=oli.Category__c;
        Double acv=oli.ACV_Calc__c;
        
        //List<Opportunity> oplist=[select Id,Main_Competitor__c,ACV_Calc__c from Opportunity where Id=:opid];

            Double updatedAcv =oli.ACV_Calc__c + oplist[0].ACV_Calc__c;
            if ((oli.ACV_Calc__c != trigger.oldMap.get(oli.Id).ACV_Calc__c)&& updatedAcv >30000 && ctgry=='Main'){
            oplist[0].Main_Competitor__c=oli.Competitor__c;     
            update oplist;
            }
            
            if ((oli.Competitor__c != trigger.oldMap.get(oli.Id).Competitor__c) && updatedAcv >30000 && ctgry=='Main'){
            oplist[0].Main_Competitor__c=oli.Competitor__c;     
            try{
            update oplist;
            }catch(Exception e){system.debug(e);}
            }
                 
        }
    }
}