// This Trigger is made inactive. The functionality of this trigger is being done by workflow rules.
//  Date               Modified By
//  09-09-2014         Sridevi Bayyapuneedi 

trigger IPSRecruitmentEmails on IPS_Recruitment__c (before update) {
//  This trigger checks if there has been a change in the status of an IPS Recruitment
//  object and send the appropriate email to the recipients.
//
//  Date                Author                      Comment
//  16/11/2011          Rita Opoku-Serebuoh         Initial development
//  Copyright © 2010  BT


    // Reserve email capacity to ensure current transaction is committed
    Messaging.reserveSingleEmailCapacity(2);
    
    for(IPS_Recruitment__c ipsr:trigger.new){
        if (((Trigger.oldMap.get(ipsr.Id).Status__c)!= 'Referred to BT Wholesale/Openreach') && (ipsr.Status__c == 'Referred to BT Wholesale/Openreach')){           
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[]{ipsr.Email_Address__c});
            mail.setSubject('Working with BT Group');
            
            String message = 'Dear ' + ipsr.Contact_Person__c + ',' +
                            '<BR><BR>' + 'Thank you for your enquiry. BT Business Partner Sales work with Partners and Resellers who promote our Retail ' +
                            '<BR>' + 'products and services on a referral basis.' +
                            '<BR>' +  'As this model is not appropriate to your business needs, you may want to contact BT Wholesale or BT Openreach.' +
                            '<BR><BR>' + 'BT Wholesale offers fixed and mobile communications providers, internet service providers, resellers and broadcast ' +
                            '<BR>' + 'organisations a large and growing portfolio of communications products and services, as well as harnessing our ' +
                            '<BR>' + 'resources, skills, experience and commitment to innovation to create and support bespoke solutions for our ' +
                            '<BR>' + 'customers. For more information, please contact 0800 671 045 or visit <A HREF="https://www.btwholesale.com/">https://www.btwholesale.com/</A>' +
                            '<BR><BR>' + 'BT Openreach supply Communications Providers with products and services that are linked to the nationwide local ' +
                            '<BR>' + 'access network. They also work on their behalf to ensure that the tens of millions of people across the UK have ' +
                            '<BR>' + 'reliable local access to the telephony and internet services they offer. For more information please visit <A HREF="http://www.openreach.co.uk/orpg/home/ourcustomers/becomeacustomer.do">http://www.openreach.co.uk/orpg/home/ourcustomers/becomeacustomer.do</A>' +
                            '<BR><BR>' + 'Regards ' +
                            //'<BR>' + 'Kay Leech ' +
                            '<BR>' + 'BT Business Partner Sales recruitment team ' +
                            '<BR>' + '<a href="mailto:btb.partner.sales.recruitment@bt.com">btb.partner.sales.recruitment@bt.com</a>';
                            
            mail.setHtmlBody(message);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }else if ((((Trigger.oldMap.get(ipsr.Id).Status__c)!= 'Referred as Reseller') && (ipsr.Status__c == 'Referred as Reseller'))){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[]{ipsr.Email_Address__c});
            mail.setSubject('Working with BT Indirect Partner Sales - Reseller option');
            
          
            String message = 'Dear ' + ipsr.Contact_Person__c + ','+
                            '<BR><BR>' + 'Thank you for your enquiry. BT Business Partner Sales work with Partners and Resellers who promote our Retail products ' +
                            '<BR>' + 'and services on a referral basis. We only work with a small number of Partners and I feel that at this moment in time, your company is best placed to work with us, as a Reseller.' +
                            '<BR>' + 'For more information on how to progress this, please view/download the following document: <A HREF="http://btbusiness.force.com/workingwithBTBPS/servlet/servlet.FileDownload?file=01520000001oJyx">BT Business Partner Sales Reseller.pdf</A>' +
                            '<BR><BR>' + 'Regards ' +
                            //'<BR>' + 'Kay Leech ' +
                            '<BR>' + 'BT Business Partner Sales recruitment team ' +
                            '<BR>' + '<a href="mailto:btb.partner.sales.recruitment@bt.com">btb.partner.sales.recruitment@bt.com</a>';
                            
            mail.setHtmlBody(message);   
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });     
        }else if ((((Trigger.oldMap.get(ipsr.Id).Status__c)!= 'Information provided on Partner/Reseller programme') && (ipsr.Status__c == 'Information provided on Partner/Reseller programme'))){
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[]{ipsr.Email_Address__c});
            mail.setSubject('Working with BT Indirect Partner Sales - Proposition detail');
            
          
            String message = 'Dear ' + ipsr.Contact_Person__c + ','+
                            '<BR><BR>' + 'Thank you for your enquiry. BT Business Partner Sales work with Partners and Resellers who promote our Retail products and services on a referral basis. ' +
                            '<BR>' +  'For more information on both programmes, please review the following documents - <A HREF="http://btbusiness.force.com/workingwithBTBPS/servlet/servlet.FileDownload?file=01520000001oJyx">BT Business Partner Sales Reseller.pdf</A> and <A HREF="http://btbusiness.force.com/workingwithBTBPS/servlet/servlet.FileDownload?file=01520000001oLRq">BT Business Partner Sales brochure</A>' +
                            '<BR><BR>' + 'Regards ' +
                            //'<BR>' + 'Kay Leech ' +
                            '<BR>' + 'BT Business Partner Sales recruitment team ' +
                            '<BR>' + '<a href="mailto:btb.partner.sales.recruitment@bt.com">btb.partner.sales.recruitment@bt.com</a>';
                                                        
            mail.setHtmlBody(message);   
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });        
        }else if ((((Trigger.oldMap.get(ipsr.Id).Status__c)!= 'Information provided on Partner programme') && (ipsr.Status__c == 'Information provided on Partner programme'))){
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(new String[]{ipsr.Email_Address__c});
            mail.setSubject('Working with BT Indirect Partner Sales - Partner option');
            
          
            String message = 'Dear ' + ipsr.Contact_Person__c + ','+
                            '<BR><BR>' + 'Thank you for your enquiry. BT Business Partner Sales works with Partners and Resellers who promote our Retail products and services on a referral basis. ' +
                            '<BR>' + 'In this instance, I feel that a Partnership would be appropriate. For more information, please review the following document - <A HREF="http://btbusiness.force.com/workingwithBTBPS/servlet/servlet.FileDownload?file=01520000001oLRq">BT Business Partner Sales brochure</A>' +
                            '<BR><BR>' + 'Please do not hesitate to contact me if you have any questions.' + 
                            '<BR><BR>' + 'Regards ' +
                            //'<BR>' + 'Kay Leech ' +
                            '<BR>' + 'BT Business Partner Sales recruitment team ' +
                            '<BR>' + '<a href="mailto:btb.partner.sales.recruitment@bt.com">btb.partner.sales.recruitment@bt.com</a>';
                            
            mail.setHtmlBody(message);   
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });              }
    }
}