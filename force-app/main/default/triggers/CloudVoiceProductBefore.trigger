trigger CloudVoiceProductBefore on Cloud_Voice_Site_Product__c (before insert,before update, before delete) {

 
if(!Trigger.isDelete){
    Set<String> cvId = New Set<String>();
    
    for(Cloud_Voice_Site_Product__c c:Trigger.New){
        cvId.add(c.SFCSSProdId__c);
    }
    
    List<CSS_Products__c > sProd = [Select Id, Name, Unit__c, Type__c, Product_Name__c, Unit_Cost__c, Discountable__c, A_Code__c, ShopLink__c,Connection_Charge__c,Sort_Order__c,
		Finance_Option__c,DiscountStringI__c,DiscountStringR__c,Term__c,Ports__c,Required_Product__c,Grouping__c,Total_Care_pm__c, Power__c,Line_Required__c,
		Term_Alternative__c, Term_Alternative_12__c, Term_Alternative_24__c, Term_Alternative_36__c, Term_Alternative_60__c
		FROM CSS_Products__c WHERE Id = :cvId AND Active__c = True];

    for(CSS_Products__c sP:sProd ){
        for(Cloud_Voice_Site_Product__c cvProd:Trigger.New ){
            if(cvProd.SFCSSPRodId__c == sP.ID){
                cvProd.Type__c = sP.Type__c;
                if(sP.ID != 'a1e200000064h8IAAQ' && cvProd.Unit__c != 'DeliveryX'){ //do not add prices for structure cabling quote
                    cvProd.Unit_Cost__c = sP.Unit_Cost__c;
                    cvProd.Connection_Charge__c = sP.Connection_Charge__c;
                }
                cvProd.A_Code__c = sP.A_Code__c;
                cvProd.Unit__c = sP.Unit__c;
                cvProd.Product_Name__c = sP.Name;
                cvProd.Image__c = sP.ShopLink__c;
                cvProd.Sort_Order__c = sP.Sort_Order__c;
                cvProd.Grouping__c = sP.Grouping__c;
                cvProd.Discountable__c = sP.Discountable__c;
                cvProd.DiscountStringI__c = sP.DiscountStringI__c;
                cvProd.DiscountStringR__c = sP.DiscountStringR__c;                
                cvProd.Term__c = sP.Term__c;
                if(sP.Ports__c > 0){
                    cvProd.Ports__c = sP.Ports__c * cvProd.Quantity__c;
                }
                cvProd.Finance_Option__c = sP.Finance_Option__c;
                cvProd.RequiredProdID__c = sP.Required_Product__c;
                cvProd.Term_Alternative__c = sP.Term_Alternative__c;
                cvProd.Term_Alternative_12__c = sP.Term_Alternative_12__c;
                cvProd.Term_Alternative_24__c = sP.Term_Alternative_24__c;
                cvProd.Term_Alternative_36__c = sP.Term_Alternative_36__c;
                cvProd.Term_Alternative_60__c = sP.Term_Alternative_60__c;
                cvProd.Line_Required__c = sP.Line_Required__c;
                if(sP.Total_Care_pm__c > 0){
                    cvProd.Total_Care_pm__c = sP.Total_Care_pm__c*cvProd.Quantity__c;
                }else 
                    cvProd.Total_Care_pm__c = 0; 
                if(sP.Power__c != Null && sP.Power__c != 0){
                    cvProd.Power__c = sP.Power__c*cvProd.Quantity__c;
                }else 
                    cvProd.Power__c = 0;   
            }
        }
    }  
}else if (!HelperRollUpSummary.getCVSiteStop() ){
    Set<Id> Ids= Trigger.oldMap.keyset();
    Set<Id> prodIds= new Set<Id>();
    Set<Id> delIds= new Set<Id>();
    //delete lines related to this product 
    Delete [SELECT Id FROM Number_Port_Line__c WHERE LinkedProductLine__c IN :Ids];

    //delete any items added to the basket automatically for this product
    //Delete [SELECT Id FROM Cloud_Voice_Site_Product__c WHERE AutoAddedBy__c IN :Ids];
    
    //delete any items that require this product
   
    for (Cloud_Voice_Site_Product__c oP :[SELECT Id FROM Cloud_Voice_Site_Product__c WHERE AutoAddedBy__c IN :Ids]){
        delIds.add(oP.Id);
    }
    for( Cloud_Voice_Site_Product__c oP : Trigger.old ) {
        prodIds.add(oP.SFCSSProdId__c);
    }
    for (Cloud_Voice_Site_Product__c oP :[SELECT Id FROM Cloud_Voice_Site_Product__c WHERE RequiredProdID__c IN :prodIds AND Cloud_Voice_Site__c IN :Ids]){
        delIds.add(oP.Id);
    }
    system.debug('JMM delIds2: ' + delIds);
    HelperRollUpSummary.setCVSiteStop(true);
    Delete [SELECT Id FROM Cloud_Voice_Site_Product__c WHERE Id IN :delIds];
    
    
    //delete license options if the core license is deleted
    for( Cloud_Voice_Site_Product__c oP : Trigger.old ) {
        if(oP.Type__c == 'License'){
            Delete [SELECT Id FROM Cloud_Voice_Site_Product__c WHERE Cloud_Voice_Site__c = :oP.Cloud_Voice_Site__c AND Number_Info__c = :oP.Grouping__c];
        }
    }
    
}    
}