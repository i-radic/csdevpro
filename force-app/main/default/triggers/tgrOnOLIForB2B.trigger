trigger tgrOnOLIForB2B on OpportunityLineItem (Before Update) {


    Set<String> oppIds = new Set<String>();
    Map<Id,BookToBill__c> B2BMap=new Map<Id,BookToBill__c>();
    List<BookToBill__c> b2bUpdateList = new List<BookToBill__c>();
    
    for(OpportunityLineItem oli : trigger.new)
    {
    if(oli.Category__c=='Main') 
    oppIds.add(oli.OpportunityId);
    } 
    
    List<RecordType> RTNames = [Select Id from RecordType where DeveloperName like 'ICT%'];
    Set<id> RTset=new Set<id>();
    for(RecordType R:RTNames)
    RTset.add(R.Id);
    
    List<BookToBill__c> b2bList=[select Id,RecordTypeId,Opportunity__c,Main_Product__c,Initial_Cost__c,First_Bill_Date__c from BookToBill__c where Opportunity__c in : oppIds and RecordTypeId IN:RTset];
    
    if(Test.isRunningTest() || b2bList.SIZE()>0){ 
    for(BookToBill__c b2b:b2bList) 
        B2BMap.put(b2b.Opportunity__c,b2b);
      }
      

    
    system.debug('SSSSSSSSSSSSSSSSSSSSS'+RTset);
    
    for(OpportunityLineItem oli : trigger.new){
    if(oli.Category__c=='Main'){
    system.debug('AAAAAAAAAAAAAAAAAAA'+oli);
      if(B2BMap.get(oli.OpportunityId)!=NULL){
      
        BookToBill__c BB=B2BMap.get(oli.OpportunityId);
        system.debug('CCCCCCCCCCCCCCCCCCCC'+BB);
        if(RTset.contains(BB.RecordTypeId)){   
        system.debug('RRRRRRRRRRRRRRRRRRRRRR');
        BB.Initial_Cost__c=oli.Initial_Cost__c;
        BB.First_Bill_Date__c=oli.First_Bill_Date__c;
        b2bUpdateList.add(BB);
      }
      }  
      }
    }
    
    if(b2bUpdateList.size() > 0) 
     update b2bUpdateList;

}