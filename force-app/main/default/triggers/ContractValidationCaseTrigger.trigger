trigger ContractValidationCaseTrigger on Case (before Insert, before Update, after insert,after update) {
          
    if(Trigger.isBefore && Trigger.isInsert)
            CaseHelper.beforeInsert(Trigger.New);  
    
    if(Trigger.isBefore && Trigger.isUpdate)
            CaseHelper.beforeUpdate(Trigger.New,Trigger.oldMap);   
    
    if(Trigger.isAfter && Trigger.isInsert)
       CaseHelper.afterInsert(Trigger.New);
       
    if(Trigger.isAfter && Trigger.isUpdate)
            CaseHelper.afterUpdate(Trigger.New,Trigger.oldMap);
}