trigger GenieContentVersionAfter on ContentVersion (after insert, after update) {
    //Recalculate Library Membership
    List<ContentVersion> records = new List<ContentVersion>();
    if(Trigger.isInsert) {
        for(ContentVersion record : Trigger.new) {
            if(record.Visibility__c != null) records.add(record);
        }
    } else if(Trigger.isUpdate) {
       for(ContentVersion record : Trigger.new){
           if(record.Visibility__c != Trigger.oldMap.get(record.id).Visibility__c) records.add(record);
       }
    }
    if(records.size() > 0) {
      GenieController.recalculateSharing(records);
    }
}