trigger CRFAge on CRF__c (before update) {
    BusinessHours BOBusinessHours = [select id from BusinessHours where name = '86MF'];
    for (CRF__c c:trigger.new) {
        
        if ((c.Back_Office_Status__c != trigger.oldMap.get(c.id).Back_Office_Status__c)){
            // 20110510 - calc cycle time (age in business hours)
        if((c.Back_Office_Status__c=='Order Issued') || (c.Back_Office_Status__c=='Cancelled')){
            //On the off-chance that the business hours on the case are null, use the default ones instead
            //Id hoursToUse = c.BusinessHoursId!=null?c.BusinessHoursId:BOBusinessHours.Id;
            //The diff method comes back in milliseconds, so we divide by 3600000 to get hours.
            Double timebetweenOpenAndCloseinBusinessHours = BusinessHours.diff(BOBusinessHours.id, c.CreatedDate, System.now())/3600000.0;
            c.Age_of_CRF__c = timebetweenOpenAndCloseinBusinessHours;
        }
        else{
            c.Age_of_CRF__c = 0;
        }
        }
    }
    
}