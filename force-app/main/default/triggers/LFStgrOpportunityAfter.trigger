/*
    Client      : BT Business
    Author      : Dilip Singh
    Date        : 24th December 2013
    Description : Logic only for LFS Opportunity for After Events
*/

/* Query on OpportunityLineItem
Select p.UseStandardPrice, p.UnitPrice, p.ProductCode, p.Product2Id, p.Pricebook2Id, p.Name, 
p.IsActive, p.Id From PricebookEntry p where p.PriceBook2Id = '01s20000000AbOb' and p.isActive = true and p.product2Id = '01t110000015cyw'
*/

trigger LFStgrOpportunityAfter on Opportunity (after insert, after update) {
    if(TriggerDeactivating__c.getInstance().Opportunity__c ) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
    if(trigger.isInsert || trigger.isUpdate) {
        ID lfsOppRecTypeID = '01220000000ABFB'; // LIVE - LFS Opportunity Record Type ID
        List<OpportunityLineItem> oppLineItemList = new List<OpportunityLineItem>();
        Set<Id> OppId = new Set<Id>();
        OpportunityLineItem oppLineItem;
        
        if( trigger.isInsert ) {
            for (Opportunity o: Trigger.new) {
                if (o.RecordTypeId == lfsOppRecTypeID) {
                    LfsCallToCMPS.getCUGId(o.Customer_Contact_Mobile__c, o.Id,o.LFS_CUG__c);
                }
                if(o.Product__c == 'Mobile Self Fulfil'){
                    oppLineItem = new OpportunityLineItem();
                    oppLineItem.OpportunityId = o.Id;
                    oppLineItem.Billing_Cycle__c = 'Outright Sale'; 
                    oppLineItem.Contract_Term__c = o.Contract_Term__c;
                    //oppLineItem.Contract_Term__c = o.Customer_Type__c == 'BT Business' ? 1 : o.Contract_Term__c;
                    //oppLineItem.Qty__c = o.Customer_Type__c == 'BT Business' ? 1 : (o.Customer_Type__c == 'Corporate' ? Integer.valueof(o.Contract_Term__c) : null);
                    oppLineItem.Category__c = 'Main';
                    oppLineItem.UnitPrice = o.Revenue__c;
                    oppLineItem.Sales_Type__c = 'Mobile Self Fulfil';
                    oppLineItem.Initial_Cost__c = 0.0;
                    oppLineItem.PricebookEntryId = '01u2000000XHrLW'; // ID for Mobile Self Fulfill product 
                    oppLineItemList.add(oppLineItem); 
                }
                 if(o.Product__c == 'Service Self-fulfil'){
                    oppLineItem = new OpportunityLineItem();
                    oppLineItem.OpportunityId = o.Id;
                    if(o.Outright_Sale__c!=true)
                    oppLineItem.Billing_Cycle__c = 'Quarterly';
                    else
                    oppLineItem.Billing_Cycle__c = 'Outright Sale';
                    oppLineItem.Contract_Term__c = o.Contract_Term__c;
                    oppLineItem.Qty__c = o.Qty__c;
                    //oppLineItem.Category__c = 'Main';
                    oppLineItem.UnitPrice = o.Revenue__c;
                    oppLineItem.Sales_Type__c = 'Service Self-fulfil';
                    //oppLineItem.Initial_Cost__c = 0.0;
                    oppLineItem.PricebookEntryId = '01u2000000XgPX5'; 
                    //'01ug0000009RUJo'; ID for Service Self-fulfil product 
                    oppLineItemList.add(oppLineItem); 
                }
                
                              
            }
            insert oppLineItemList;
            
        } //CR3682 - End */
        
        
    }
}