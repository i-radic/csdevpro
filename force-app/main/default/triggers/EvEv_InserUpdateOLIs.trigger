/*
###########################################################################
# Project Name..........: EveryThing Everywhere
# File..................: EvEv_InserUpdateOLIs.trigger
# Version...............: 1
# Created by............: Kanishk Prasad
# Created Date..........: 15-September-2011
# Last Modified by......:    
# Last Modified Date....:   
# Description...........: This trigger is works on after insert and after update of Artemis_Deal__c records. Calls the createUpdateOLIs() of Helper Class
#                           to take care of the following tasks
#                           1. Works only when the inserted/updated record is a Primary Deal
#                           2. Deletes all opportunity line items of the given opportunity which have Primary_Deal__c attribute as "true"
#                           3. Get the Product Ids for products {'Voice','Blackberry Data Only','Tablet Data Only','Mobile Broadband Data Only','Fleetlink','M2M'}
#                           4. Create OLIs for only those products which have Quantity>0 Revenue generated>0  
#
# 
########################################################################### 
*/
trigger EvEv_InserUpdateOLIs on Artemis_Deal__c (after insert, after update) {
    
  System.debug('here->');
  EvEV_Helper.createUpdateOLIs(Trigger.new);   
    
}