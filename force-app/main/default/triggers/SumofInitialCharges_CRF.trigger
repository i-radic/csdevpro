trigger SumofInitialCharges_CRF on Provide_Line_Details__c (before insert, before update, before delete) {
    Double Diff;
    if(Trigger.isInsert){
    
    Provide_Line_Details__c[] PL = Trigger.new;
    //Double InitialCharges = PL[0].Initial_charges__c;
    //Double CurrentIC;
    //Double SumIC;
    
    Id CRFId = PL[0].Related_to_CRF__c;
    
    List<CRF__c> crf = [Select Id,Total_initial_charges__c from CRF__c where Id=: CRFId];
    if(crf.size()>0){

        if(PL[0].Initial_charges__c > 0){
            if(crf[0].Total_initial_charges__c == null || crf[0].Total_initial_charges__c == 0){
                crf[0].Total_initial_charges__c= PL[0].Initial_charges__c; 
            }
            else if(crf[0].Total_initial_charges__c >= 0){
                crf[0].Total_initial_charges__c = crf[0].Total_initial_charges__c + PL[0].Initial_charges__c;
            }
            update crf;    
        }  
    }
    }


    if(Trigger.isUpdate){
    
    Provide_Line_Details__c[] PLNew = Trigger.new;
    Provide_Line_Details__c[] PLOld = Trigger.old;
            
    if(PLNew[0].Initial_charges__c != PLOld[0].Initial_charges__c)
    {

         Id CRFId = PLOld[0].Related_to_CRF__c;
         List<CRF__c> crf = [Select Id,Total_initial_charges__c from CRF__c where Id=: CRFId];
         if(crf.size()>0){
         if(crf[0].Total_initial_charges__c == null)
          crf[0].Total_initial_charges__c = 0;
         if(PLNew[0].Initial_charges__c == null)
             PLNew[0].Initial_charges__c = 0;
         if(PLOld[0].Initial_charges__c == null)
             crf[0].Total_initial_charges__c = crf[0].Total_initial_charges__c+PLNew[0].Initial_charges__c;
         if(PLNew[0].Initial_charges__c >= 0 && PLOld[0].Initial_charges__c != null){

                Diff = PLNew[0].Initial_charges__c - PLOld[0].Initial_charges__c;       
                crf[0].Total_initial_charges__c = crf[0].Total_initial_charges__c + Diff;

            }
            
            update crf;    

         }  
        
    }
    
    }
  
    if(Trigger.isDelete){
    
    Provide_Line_Details__c[] PLOld = Trigger.old;
    Id CRFId = PLOld[0].Related_to_CRF__c;
         List<CRF__c> crf = [Select Id,Total_initial_charges__c from CRF__c where Id=: CRFId];
         if(crf.size()>0){
         System.debug('VAluea'+PLOld[0].Initial_charges__c); 
         if(PLOld[0].Initial_charges__c != null){
         System.debug('VAlues'+crf[0].Total_initial_charges__c); 
          
         crf[0].Total_initial_charges__c = crf[0].Total_initial_charges__c - PLOld[0].Initial_charges__c;
         System.debug('VAlued'+crf[0].Total_initial_charges__c);}
         else {
         System.debug('VAluef'+PLOld[0].Initial_charges__c);
         System.debug('VAlueg'+crf[0].Total_initial_charges__c);
         crf[0].Total_initial_charges__c = crf[0].Total_initial_charges__c;
         System.debug('VAlueh'+crf[0].Total_initial_charges__c);}
         
         update crf;
         }
    }
    
}