trigger tgrAddressAutoNumber on Address_Details__c (after delete, before insert) {

	if(Trigger.isInsert){
	    Address_Details__c[] AD = Trigger.New;
	    Id CRFId = AD[0].Related_to_CRF__c;

	    if(CRFId == Null){
	        return;
	    }

	    List<Address_Details__c> ADList = [Select Id,Name,Related_to_CRF__c from Address_Details__c where Related_to_CRF__c=:CRFId];
	    if(ADList.size()>=0){
		    string a = string.valueOf(ADList.size()+1);
		    AD[0].Name=a;
	    }
    }

    if(Trigger.isDelete){
	    Address_Details__c[] AD = Trigger.Old;
	    Id CRFId = AD[0].Related_to_CRF__c;
	    List<Address_Details__c> aList=[SELECT Id,Name FROM Address_Details__c WHERE Related_to_CRF__c=:CRFId ORDER BY Createddate ASC];
	    if(aList.size()>0){
	    	Integer k = 0;
		    for(Integer i=aList.size();i!=0;i--){
			    aList[k].Name = string.valueOf(aList.size()-i+1);
			    k++;
		    }
	    	update aList;
	    }
    }

}