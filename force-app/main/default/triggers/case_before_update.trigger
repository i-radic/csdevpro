trigger case_before_update on Case (before update) 
{
	
	if (Trigger.isBefore && Trigger.isUpdate)
	{
		CaseTriggerHandler triggerHandler = new CaseTriggerHandler();
		triggerHandler.handleCaseBeforeUpdates(Trigger.newMap, Trigger.oldMap);
	}

}