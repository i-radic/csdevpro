trigger TrialLineItemTrigger on Trial_Line_Item__c (before insert, before update) {
	TrialLineItemHelper aTrialLineItemHelper = new TrialLineItemHelper();
	//Set tablet device flag
	aTrialLineItemHelper.SetTabletDevice(Trigger.new);
	//Calculate sub totals
	aTrialLineItemHelper.CalculateSubTotals(Trigger.new);
	//Set type on line item
	aTrialLineItemHelper.SetLineItemType(Trigger.new);
}