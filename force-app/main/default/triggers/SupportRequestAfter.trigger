trigger SupportRequestAfter on Support_Request__c (after insert, after update, after delete, after undelete) {

    SupportRequestHandler handler = SupportRequestHandler.getHandler();
    if (Trigger.isInsert) {
        handler.afterInsert();
    } else if (Trigger.isUpdate) {
        handler.afterUpdate();
    } else if (Trigger.isUnDelete) {
        handler.afterUnDelete();
    } else if (Trigger.isDelete) {
        handler.afterDelete();
    }
}