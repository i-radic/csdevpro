//
// History
//
// Version      Date            Author          Comments
// 1.0.0        25-01-2011      Dan Measures    Initial version
// 2.0.0        04-18-2011      Krupakar Reddy  Second version

//
// Comments
//
// 1. Work out the sub-status (i.e. the step in the order process) that the orderline is in.
// 
//  NB. may need to autopopulate relationship with Product but at time of writing this relationship validaty was yet to be defined.  
//  This will derive what  BT parameter information to use for the product in the orderline, at time of writing it is just 1 x product family in this table and is derived in formula field.
trigger BTBOrderLineBefore on BTB_Order_Line__c (before insert, before update) {
    for (BTB_Order_Line__c ol : Trigger.new){       
        ol.Name = ol.BTB_CSS_ORDER_NO__c;
        /* RAG RATING ALGORITHM */
        E2EReporting.generateOrderlineSubStatus(ol);
    }
}