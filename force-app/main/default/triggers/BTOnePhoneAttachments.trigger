trigger BTOnePhoneAttachments on Attachment (after insert, after update, after delete) {
	if (Trigger.isInsert || Trigger.isUpdate){
		for (Attachment at:Trigger.new){
        String thisID = at.ParentID;
        system.debug('ROS - Update/Insert Attachment - Parent opportunity id: ' + thisID);
        
	     if (thisID.startsWith('a1q')){  //count attachments on header
	        List <aggregateResult> groupedResults = [SELECT ParentId, count(id) FROM Attachment where ParentId = :thisID GROUP BY ParentId ];
	        
	        BT_One_Phone__c btop = [Select id, Number_of_Attachments__c From BT_One_Phone__c where id = :at.ParentID];
	             btop.Number_of_Attachments__c = double.valueof(groupedResults[0].get('expr0')) ;
	                update btop;
	         }

	     else if (thisID.startsWith('a1p')){  //count attachments on site
	     List <aggregateResult> groupedResults2 = [SELECT ParentId, count(id) FROM Attachment where ParentId = :thisID GROUP BY ParentId ];
	        
	        BT_One_Phone_Site__c btops = [Select id, Number_of_Attachments__c From BT_One_Phone_Site__c where id = :at.ParentID];
	             btops.Number_of_Attachments__c = double.valueof(groupedResults2[0].get('expr0')) ;
	                update btops;
	         }
  		} 
	}else if (Trigger.isDelete){
		for (Attachment at:Trigger.old){
			String thisID = at.ParentID;

			if (thisID.startsWith('a1q')){  //count attachments on header
	        	List <aggregateResult> groupedResults = [SELECT ParentId, count(id) FROM Attachment where ParentId = :thisID GROUP BY ParentId ];
	        
	        	BT_One_Phone__c btop = [Select id, Number_of_Attachments__c From BT_One_Phone__c where id = :at.ParentID];
	        	
	        	if (groupedResults.size() > 0){
	        		btop.Number_of_Attachments__c = double.valueof(groupedResults[0].get('expr0'));
	        	}else{
	        		btop.Number_of_Attachments__c = 0;	
	        	}
	            
	            update btop;
	         }

	     	else if (thisID.startsWith('a1p')){  //count attachments on site
	     		List <aggregateResult> groupedResults2 = [SELECT ParentId, count(id) FROM Attachment where ParentId = :thisID GROUP BY ParentId ];
	        
	        	BT_One_Phone_Site__c btops = [Select id, Number_of_Attachments__c From BT_One_Phone_Site__c where id = :at.ParentID];
	        	
	        	if (groupedResults2.size() > 0){
	        		btops.Number_of_Attachments__c = double.valueof(groupedResults2[0].get('expr0')) ;
	        	}else{
	        		btops.Number_of_Attachments__c = 0;	
	        	}
	            
	            update btops;
	         }
			
		}	
	}

}