trigger tgrOnBook2Bill on BookToBill__c (Before Insert) {

    Set<String> oppIds = new Set<String>();
    Map<Id,OpportunityLineItem> OLIMap=new Map<Id,OpportunityLineItem>();
  
    Set<String> b2bIds = new Set<String>();
    Map<Id,String> mRecTypes=new Map<Id,String>();
    Map<Id,Integer> OLIMap2=new Map<Id,Integer>();
    Integer Initialcost = 0;
    
    for(BookToBill__c b2b: trigger.new)
        oppIds.add(b2b.Opportunity__c);
    
    List<OpportunityLineItem> oplist=[select Id,ProdName__c,Opportunity.Id,Initial_Cost__c,First_Bill_Date__c from OpportunityLineItem where OpportunityId in : oppIds and Category__c='Main'];
    List<OpportunityLineItem> oplistall=[select Id,ProdName__c,Opportunity.Id,Initial_Cost__c,First_Bill_Date__c from OpportunityLineItem where OpportunityId in : oppIds];
     
    for(OpportunityLineItem OLI:oplist)  
        OLIMap.put(OLI.Opportunity.Id,OLI); 
        
    for(OpportunityLineItem OLI:oplistall)  {
    
        if(OLIMap2.containskey(OLI.Opportunity.Id)){
        system.debug('Initialcost1' +OLI.Initial_Cost__c);
        Initialcost= OLIMap2.get(OLI.Opportunity.Id)+ integer.valueOf(OLI.Initial_Cost__c);
        system.debug('Initialcost2' +Initialcost);
        OLIMap2.put(OLI.Opportunity.Id,Initialcost);
        }
        else
        {
        OLIMap2.put(OLI.Opportunity.Id,integer.valueOf(OLI.Initial_Cost__c));
        system.debug('Initialcost3' +Initialcost);
        }
    }
    
    List<RecordType> RTNames = [Select Id from RecordType where DeveloperName like 'ICT%'];
    Set<id> RTset=new Set<id>();
    for(RecordType R:RTNames)
    RTset.add(R.Id);
    
    for(BookToBill__c b2b: trigger.new){
     
    if(RTset.contains(b2b.RecordTypeId)){
    
      if(OLIMap.get(b2b.Opportunity__c)!=NULL){
        OpportunityLineItem OL=OLIMap.get(b2b.Opportunity__c);
        //b2b.Initial_Cost__c=OL.Initial_Cost__c;
        b2b.Main_Product__c=OL.ProdName__c;
        b2b.First_Bill_Date__c=OL.First_Bill_Date__c;
      } 
      if(OLIMap2.get(b2b.Opportunity__c)!=NULL){
        b2b.Initial_Cost__c=OLIMap2.get(b2b.Opportunity__c);
      } 
     } 
    }
   
  }