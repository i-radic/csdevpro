trigger validateAcquiredLines on BookToBill__c(Before Insert, Before Update) {

//    History

//      Date                Author                                                      Details
//    05/09/2014        Venkata Srinivas M                                Edited this Trigger for Auto Update of "Revenue Status" based on
//                                                                        Project Status,ForecastAnnualCallsGrowth and MeasuredAnnualCallsGrowth
//                                                                        as requested in CR6039 
//
//    17/09/2014        Sridevi Bayyapuneedi                              5(MTHMG,MHHF,MHMG,IPHF,AIRWT)new product codes were added and 3(TAB01,TAB02,TAB03) removed to update the pull through criteria
//                                                                        on the Mobile Workforce Solutions Record Type of Book2Bill record as a part of CR6240
//
//
//    10/10/2014        Venkata Srinivas M                               As requested in CR6508,3 Product codes(MVNEE,MBNEE,MBINE) have added in to the pull through criteria for MVNO B2B records.
//              
//    13/01/2015         Venkata Srinivas M                              As a part of CR6845, edited to pull Audit Manager's Email using Record Owner.                        
    set < ID > ICTRTS = new set < ID > ();
    set < ID > ICTRTSCalls = new set < ID > ();
    Set < String > ProfileSet = new set < String > ();
    
    List < ID > OpptyIds = new List < ID > ();
    List<ID> b2bIds= new List<ID>();
    List < OpportunityLineItem > OLI = new List < OpportunityLineItem > ();
    List < Opportunity > Opptys = new List < Opportunity > ();
    List < User > oOwners = new List < User > ();
    List < RecordType > ICTRTL = [Select Id From RecordType where(Name = 'ICT Book to Bill' or Name = 'ICT Book to Bill Read Only') and SOBJECTTYPE = 'BookToBill__c'];
    //List < Profile > PList = [select Id From Profile where Name like '%Standard User%' or Name Like '%Sales Manager%'];
    List < Profile > PList = [select Id From Profile where Name like '%Standard User%' or Name Like '%Sales Manager%' or Name Like '%System Administrator%'];

    Map < String, Decimal > ProdQuantity = new Map < String, Decimal > ();
    Map < String, String > OpptyName = new Map < String, String > ();
    Map < String, String > OpptyOwner = new Map < String, String > ();
    Map < String, Decimal > OpptyRevenueSOV = new Map < String, Decimal > ();
    Map < String, Decimal > opptyAmount = new Map < String, Decimal > ();
    Map < Id, Decimal > oppGrossAnnualList = new Map < Id, Decimal > ();       

    // CR3943
        // THESE variable are used for autopopulation of MVNO/Workforce Record Tyeps in BookToBill RecordTypes
        //system.debug('12345');
        Integer volVoiceNew = 0;
        Integer volBlackberryNew = 0;
        Integer volDataNew = 0;
        Integer voliPhoneNew = 0;
        Integer volMobileNew = 0;      
        
    // END 
        
    List < RecordType > ICTRTCalls = [Select Id From RecordType where(Name = 'Calls And Lines Acquisition' or Name = 'Calls And Lines Acquisition Read Only'
                                      or Name = 'Calls And Lines Acquisition BTBTeam' or Name = 'Calls And Lines Defence'
                                      or Name = 'Calls And Lines Defence BTBTeam' or Name = 'Calls And Lines Defence Read Only') and SOBJECTTYPE = 'BookToBill__c'];
    
   RecordType R1 = [Select Id From RecordType where Name = 'Calls And Lines Acquisition' and SOBJECTTYPE = 'BookToBill__c'];   
    
    
    for (RecordType RT: ICTRTL)
        ICTRTS.add(RT.Id);                                        
    
    for (RecordType RT: ICTRTCalls)
        ICTRTSCalls.add(RT.Id);    

    for (Profile P: PList)
        ProfileSet.add(P.Id);
        
    for (BookToBill__c BC: Trigger.New)        
        OpptyIds.add(BC.Opportunity__c);

    OLI = [Select Opportunity.Id, Qty__c, ProdName__c, Category__c, TotalPrice From OpportunityLineItem where OpportunityId IN: OpptyIds and Sales_Type__c = 'Acquisition' and ProdName__c in ('PSTN - Analogue Exchange Line', 'PSTN Bespoke Line', 'PSTN Bespoke Line - with Hosted Voice', 'Featureline CORPORATE', 'Featureline COMPACT', 'Featureline (Not Compact or Corporate)', 'OnePlan Lines - Not Long Term Contracted', 'ISDN2', 'ISDN30', 'International Direct Connect')];
    Opptys = [Select Id, Name, OwnerId, Owner__c, sum_of_REVENUE_SOV__c,Amount,ACV_Calc__c From Opportunity where Id IN: OpptyIds];
    List < OpportunityLineItem > OLI1 = [Select Qty__c,Product_Family__c, ProdCode__c, ProdName__c,FFA_Supplier__c, Category__c,First_Bill_Date__c, TotalPrice, OpportunityId, Description,ACV_Calc__c,Sales_Type__c From OpportunityLineItem where OpportunityId IN: OpptyIds];
    
    // THESE variable are used for autopopulation of MVNO/Workforce Record Tyeps in BookToBill RecordTypes
    
    double GrossValue = 0.00;
    double foreCastValue = 0.00;    
    String fDate,FFASupplier;
    Date dat;
    integer productQty =  0;  
    string b2bLineDescription;  
    boolean NotisTablet = false;
    boolean NotTablet = false;
    
    //CR6845
    for(BookToBill__c B: Trigger.New){
      if (B.Record_Owner__c == NULL){  
        B.Record_Owner__c = UserInfo.getUserId();
        }
        b2bIds.add(B.Record_Owner__c);
        system.debug('aaaaaaaa'+B.Record_Owner__c);
    }
    Map<Id,User> users= new Map<Id,User>([Select Id,Manager.Email from User where Id in :b2bIds]);
    system.debug('bbbbbb'+users);
     for(BookToBill__c B: Trigger.New){
     B.Audit_Manager__c=users.get(B.Record_Owner__c).Manager.Email;
     system.debug('aeaeaeaea'+B.Audit_Manager__c+' '+users.get(B.Record_Owner__c).Manager.Email);
     }
    //END   
    system.debug(' LLLLL :::'+OLI1);
    
    for (OpportunityLineItem lineItem: OLI1) {
        
        system.debug('LinIteem Code :::::::'+lineItem.ProdCode__c);
        
        if (lineItem.ProdCode__c == 'MVNRV' || lineItem.ProdCode__c == 'MVDBO' || lineItem.ProdCode__c == 'TTMON' || lineItem.ProdCode__c == 'TTOFN' || lineItem.ProdCode__c == 'MVNEE') {
            //Added Product code MVNEE as requested in CR6508
            volVoiceNew += (Integer) lineItem.Qty__c;
        } else if (lineItem.ProdCode__c == 'BBNEW' || lineItem.ProdCode__c == 'TTBBR') {
            volBlackberryNew += (Integer) lineItem.Qty__c;
        } else if (lineItem.ProdCode__c == 'BTBMB' || lineItem.ProdCode__c == 'MDEXN' || lineItem.ProdCode__c == 'TAB04' || lineItem.ProdCode__c == 'MBNEE' || lineItem.ProdCode__c == 'MBINE') {
            //Added Product codes MBNEE and MBINE as requested in CR6508
            volDataNew += (Integer) lineItem.Qty__c;
        } else if (lineItem.ProdCode__c == 'IPHON' || lineItem.ProdCode__c == 'TTIPN') {
            voliPhoneNew += (Integer) lineItem.Qty__c;
        } else if (lineItem.ProdCode__c == 'MWS03' || lineItem.ProdCode__c == 'MWS04' || lineItem.ProdCode__c == 'IPHO' || lineItem.ProdCode__c == 'IPHF' || lineItem.ProdCode__c == 'AIRWT' ){
           //Added Product Codes IPHF and AIRWT as part of CR6204
            NotisTablet = true;
        }else if (lineItem.ProdCode__c == 'MTHMG' || lineItem.ProdCode__c == 'MHHF' || lineItem.ProdCode__c == 'MHMG' || lineItem.ProdCode__c == 'MWS02' || lineItem.ProdCode__c == 'MBHDW' ){
           //Added Product Codes MTHMG,MHHF,MHMG and removed TAB01,TAB02,TAB03 as part of CR6204
            NotTablet = true;
        }              
            
        productQty += (Integer) lineItem.Qty__c;
        
        if (lineItem.Category__c == 'Main') {
            If( lineItem.ProdCode__c != 'toDLAWL' && ( lineItem.Product_Family__c == 'UVM Calls, OnePlan' || lineItem.Product_Family__c == 'UVM Calls, Business Plan' || lineItem.Product_Family__c == 'UVM Calls, Customer Commitment' )){
                GrossValue += lineItem.ACV_Calc__c;
            }                   
            oppGrossAnnualList.put(lineItem.OpportunityId, GrossValue);
            fDate = String.valueOf(lineItem.First_Bill_Date__c);     
            dat = lineItem.First_Bill_Date__c;       
            FFASupplier = lineItem.FFA_Supplier__c;
            b2bLineDescription = lineItem.Description;
        }
    }
    
    volMobileNew = volBlackberryNew + volDataNew + voliPhoneNew + volVoiceNew;
    
    if(fDate != null){
        List<string> slist = fDate.split('-',4);  
        fDate = slist[2]+'/'+slist[1]+'/'+slist[0];
    }     
    //system.debug('5678');
    for (Opportunity OP: Opptys) {
        OpptyName.put(OP.Id, OP.Name);
        OpptyOwner.put(OP.Id, OP.OwnerId);
        opptyAmount.put(OP.Id, OP.Amount);
        for (OpportunityLineItem lineItem: OLI1) {  
        //if (lineItem.Category__c == 'Main') {
        If( lineItem.ProdCode__c != 'toDLAWL' && ( lineItem.Product_Family__c == 'UVM Calls, OnePlan' || lineItem.Product_Family__c == 'UVM Calls, Business Plan' || lineItem.Product_Family__c == 'UVM Calls, Customer Commitment' )&& lineItem.Sales_Type__c == 'Acquisition'){  
            foreCastValue+=lineItem.ACV_Calc__c;
        }
        //}
        }//system.debug('aaaaa11111'+foreCastValue);
        OpptyRevenueSOV.put(OP.Id,foreCastValue);                        
    }

    if (ProfileSet.contains(UserInfo.getProfileId())) {
        Decimal pstnCount = 0;
        String opId;
        for (OpportunityLineItem O: OLI) {
            opId = O.Opportunity.Id;
            if (!ProdQuantity.containsKey(opId + '_PSTN')) ProdQuantity.put(opId + '_PSTN', 0);
            if (!ProdQuantity.containsKey(opId + '_International Direct Connect')) ProdQuantity.put(opId + '_International Direct Connect', 0);
            if (!ProdQuantity.containsKey(opId + '_ISDN2')) ProdQuantity.put(opId + '_ISDN2', 0);
            if (!ProdQuantity.containsKey(opId + '_ISDN30')) ProdQuantity.put(opId + '_ISDN30', 0);


            if (O.ProdName__c == 'PSTN - Analogue Exchange Line' || O.ProdName__c == 'PSTN Bespoke Line' || O.ProdName__c == 'PSTN Bespoke Line - with Hosted Voice' || O.ProdName__c == 'Featureline CORPORATE' || O.ProdName__c == 'Featureline COMPACT' || O.ProdName__c == 'Featureline (Not Compact or Corporate)' || O.ProdName__c == 'OnePlan Lines - Not Long Term Contracted') {
                if (O.Qty__c != NULL) ProdQuantity.put(opId + '_PSTN', O.Qty__c);
                Else
                ProdQuantity.put(opId + '_PSTN', 0);
            } else {
                if (O.Qty__c == NULL) ProdQuantity.put(opId + '_' + O.ProdName__c, 0);
                Else
                ProdQuantity.put(opId + '_' + O.ProdName__c, O.Qty__c);
            }
        }    
                
        //system.debug('entering'+R1.Id);    
        //for (Integer i = 0; i < Trigger.new.size(); i++) {
        for(BookToBill__c bb : Trigger.new){
        
            if (Trigger.isInsert) {
        
                String currOppId = bb.Opportunity__c;
                String rtId = bb.RecordTypeId;  
                system.debug('RtId:'+rtId +'IIIIII ::::::'+bb.RecordTypeId+'jhvhvjhv'+R1.Id);         
                
                //if(R1.size()>0){
                //system.debug('aaaaa');
                if (bb.RecordTypeId == R1.Id) {//system.debug('bbbb');
                    if ((bb.IDC_Acquired_Lines__c != 0) && (bb.IDC_Acquired_Lines__c != ProdQuantity.get(currOppId + '_International Direct Connect'))) bb.IDC_Acquired_Lines__c.addError('IDC acquired lines for opportunity: ' + OpptyName.get(currOppId) + ' is ' + ProdQuantity.get(currOppId + '_International Direct Connect'));
                    if ((bb.PSTN_Acquired_Lines__c != 0) && (bb.PSTN_Acquired_Lines__c != ProdQuantity.get(currOppId + '_PSTN'))) bb.PSTN_Acquired_Lines__c.addError('PSTN acquired lines for opportunity: ' + OpptyName.get(currOppId) + ' is ' + ProdQuantity.get(currOppId + '_PSTN'));
                    if ((bb.ISDN2_Acquired_Lines__c != 0) && (bb.ISDN2_Acquired_Lines__c != ProdQuantity.get(currOppId + '_ISDN2'))) bb.ISDN2_Acquired_Lines__c.addError('ISDN2 acquired lines for opportunity: ' + OpptyName.get(currOppId) + ' is ' + ProdQuantity.get(currOppId + '_ISDN2'));
                    if ((bb.ISDN30_Acquired_Lines__c != 0) && (bb.ISDN30_Acquired_Lines__c != ProdQuantity.get(currOppId + '_ISDN30'))) bb.ISDN30_Acquired_Lines__c.addError('ISDN30 acquired lines for opportunity: ' + OpptyName.get(currOppId) + ' is ' + ProdQuantity.get(currOppId + '_ISDN30'));
                    system.debug('Forecast Annual Growth' + bb.Forecast_Annual_Growth__c +OpptyRevenueSOV.get(currOppId) );
                    if (bb.Forecast_Annual_Growth__c !=OpptyRevenueSOV.get(currOppId)) bb.Forecast_Annual_Growth__c.addError('Forecast Annual Calls Growth for opportunity: ' + OpptyName.get(currOppId) + ' is ' +OpptyRevenueSOV.get(currOppId) );
                    //system.debug('Gross from BB' + bb.Gross_Annual_Growth__c+' ::from Oppty:'+ +oppGrossAnnualList.get(currOppId) );
                    //if (bb.Gross_Annual_Growth__c !=oppGrossAnnualList.get(currOppId) ) bb.Gross_Annual_Growth__c.addError('Forecast Acquired Calls Growth for opportunity: ' + OpptyName.get(currOppId) + ' is ' +oppGrossAnnualList.get(currOppId) );
        
              //  }
                }else if (rtId.substring(0, 15) == '01220000000AMUl') {    
                    // InTo Mobile MVNO Record Type
                    bb.Status_Date__c = System.today();                             
                    
                    //if (bb.Opportunity__c != Trigger.oldMap.get(currOppId).Opportunity__c) bb.Opportunity__c.addError('To Create New Record, Opportunity name must be: ' + OpptyName.get(currOppId));
                    if (bb.Volume_Voice_New__c != String.valueOf(volVoiceNew)) bb.Volume_Voice_New__c.addError('Volume Voice New for opportunity: ' + OpptyName.get(currOppId) + ' is ' + volVoiceNew);
                    if (bb.Volume_Blackberry_New__c != String.valueOf(volBlackberryNew)) bb.Volume_Blackberry_New__c.addError('Volume BlackBerry New for opportunity: ' + OpptyName.get(currOppId) + ' is ' + volBlackberryNew);
                    if (bb.Volume_Data_New__c != String.valueOf(volDataNew)) bb.Volume_Data_New__c.addError('Volume Data New for opportunity: ' + OpptyName.get(currOppId) + ' is ' + volDataNew);
                    if (bb.Volume_iPhone_New__c != String.valueOf(voliPhoneNew)) bb.Volume_iPhone_New__c.addError('Volume iPhone New for opportunity: ' + OpptyName.get(currOppId) + ' is ' + voliPhoneNew);
                    //if (bb.Total_Mobile_current__c != String.valueOf(volMobileNew)) bb.Total_Mobile_current__c.addError('Total Mobile (Current) for opportunity: ' + OpptyName.get(currOppId) + ' is ' + volMobileNew);
                    if (bb.Total_Mobile_New__c != volMobileNew) bb.Total_Mobile_New__c.addError('Total Mobile New for opportunity: ' + OpptyName.get(currOppId) + ' is ' + volMobileNew);
                    
                } else if (rtId.substring(0, 14) == '01220000000AMV' ) {                
                    
                    // InTo Mobile Workforce Solutions Record Type     
                    
                    system.debug('Entered into Workforce in Trigger identified by RT id');
                    bb.Line_description__c = b2bLineDescription; 
                    bb.Status_Date__c = System.today();
                    if(NotisTablet){
                        bb.Project_Status__c = 'Project In Progress - BT Managed';
                    } else if(NotTablet){
                        bb.Project_Status__c = 'Project in Progress - Tablet / Other';
                    }
                    system.debug('1-1# '+bb.Opportunity__c);
                    system.debug('1-1# '+currOppId);
                    system.debug('anuuuuu'+opptyAmount.get(currOppId));
                    
                    //if (bb.Opportunity__c != Trigger.oldMap.get(currOppId).Opportunity__c) bb.Opportunity__c.addError('To Create New Record, Opportunity name must be: ' + OpptyName.get(currOppId));
                    if (bb.Mobile_Workforce_Volume__c != String.valueOf(productQty)) bb.Mobile_Workforce_Volume__c.addError('Mobile Workforce Volume for opportunity: ' + OpptyName.get(currOppId) + ' is ' + productQty);
                    if (bb.Revenue_SOV__c != opptyAmount.get(currOppId)) bb.Revenue_SOV__c.addError('Revenue/SOV for opportunity: ' + OpptyName.get(currOppId) + ' is ' + opptyAmount.get(currOppId));
                    if (bb.Expected_1st_Bill_Date_original__c != Dat) bb.Expected_1st_Bill_Date_original__c.addError('Expected 1st Bill Date (Orginal) for opportunity: ' + OpptyName.get(currOppId) + ' is ' + fDate);
                    //if (bb.Revenue_SOV_Dynamic__c != opptyAmount.get(currOppId)) bb.Revenue_SOV_Dynamic__c.addError('Revenue/SOV for opportunity: ' + OpptyName.get(currOppId) + ' is ' + opptyAmount.get(currOppId));
                    //if (bb.Expected_1st_Bill_Date_current__c != Dat) bb.Expected_1st_Bill_Date_current__c.addError('Expected 1st Bill Date (Orginal) for opportunity: ' + OpptyName.get(currOppId) + ' is ' + fDate);
                }
            }
        }
    }
            
    //Code to update Progress comments history field
    for (BookToBill__c BC: Trigger.New) {
        
        String rtId = BC.RecordTypeId;  
              

        if (Trigger.IsInsert){                  
                            
            if (BC.Record_Owner__C == NULL){
                 BC.Record_Owner__c = UserInfo.getUserId();
            }
            if (BC.Progress_Comments__c != NULL){
                 BC.Progress_Comments_History__c = System.now() + ', added by: ' + UserInfo.getUserName() + ' \n ' + BC.Progress_Comments__c;
            }
            //Code to update status date for ICT B2B
            if (ICTRTS.contains(BC.RecordTYPEID)){
                 BC.Status_Date__c = System.today();
            }
          
        }

        if (Trigger.IsUpdate) {
            //Code to update status date for ICT B2B
            if (ICTRTS.contains(BC.RecordTYPEID)) {
                if (Trigger.oldMap.get(BC.Id).Status__c != BC.Status__c) {
                    BC.Status_Date__c = System.today();
                }
            }
            
            if (Trigger.oldMap.get(BC.Id).Progress_Comments__c != BC.Progress_Comments__c) {
                BC.Progress_Comments_History__c = System.now() + ', added by: ' + UserInfo.getUserName() + ' \n ' + BC.Progress_Comments__c + '\n' + BC.Progress_Comments_History__c;
            }
                
            if (BC.New_Measurement__c == 'Yes' && BC.Measurement_Status__c == null){ 
                BC.Measurement_Status__c = 'Feedback Required';
            }

            //Code to update Project status & status date fields for Measurements bulk upload
            system.debug('PPPPP :::::'+ICTRTSCalls.contains(BC.RecordTYPEID)+'OOOOOOO ::::'+BC.RecordTYPEID);
            if (ICTRTSCalls.contains(BC.RecordTYPEID)) {               
                If(Trigger.oldMap.get(BC.Id).Project_Status__c != BC.Project_Status__c)
                BC.Status_Date__c = System.today();
            }
            
            if (rtId.substring(0, 14) == '01220000000AMV') {
              if(Trigger.oldMap.get(BC.Id).Project_Status__c != BC.Project_Status__c )
                    BC.Status_Date__c = System.today();
                                                 
                if((Trigger.oldMap.get(BC.Id).Project_Status__c != BC.Project_Status__c) && (
                    Trigger.oldMap.get(BC.Id).Project_Status__c == 'Signed Off (SMS)' ||
                    Trigger.oldMap.get(BC.Id).Project_Status__c == 'Signed Off (Invoiced)' ||
                    Trigger.oldMap.get(BC.Id).Project_Status__c == 'Signed Off (Annuity)' ||
                    Trigger.oldMap.get(BC.Id).Project_Status__c == 'Cancelled' ))
                {
                    // 00e200000015Lsh -> B2B Team Profile ID
                    if(ProfileSet.contains(UserInfo.getProfileId()) || !(UserInfo.getProfileId().contains('00e200000015Lsh')) ){
                        BC.Project_Status__c.addError('You are not allowed to Change Status to '+BC.Project_Status__c);
                    }                       
                }
                if( BC.Project_Status__c == 'Signed Off (SMS)' || BC.Project_Status__c == 'Signed Off (Invoiced)' || BC.Project_Status__c == 'Signed Off (Annuity)' || BC.Project_Status__c == 'Cancelled' ){
                    System.debug('***pID:'+Userinfo.getProfileId());
                    if(ProfileSet.contains(UserInfo.getProfileId())){
                        BC.Project_Status__c.addError('You are not allowed to Change Status to '+BC.Project_Status__c);
                    }                       
                }               
            
            } else if (rtId.substring(0, 14) == '01220000000AMU') {
                If(Trigger.oldMap.get(BC.Id).Project_Status__c != BC.Project_Status__c )
                    BC.Status_Date__c = System.today();
                
                if((Trigger.oldMap.get(BC.Id).Project_Status__c != BC.Project_Status__c) && (
                    Trigger.oldMap.get(BC.Id).Project_Status__c == 'Signed Off' || 
                    Trigger.oldMap.get(BC.Id).Project_Status__c == 'Cancelled' ||
                    Trigger.oldMap.get(BC.Id).Project_Status__c == 'Completed'))
                {
                    // 00e200000015Lsh -> B2B Team Profile ID
                    if(ProfileSet.contains(UserInfo.getProfileId()) || !(UserInfo.getProfileId().contains('00e200000015Lsh')) ){
                        BC.Project_Status__c.addError('You are not allowed to Change Status to '+BC.Project_Status__c);
                    }                       
                }
                
                if(BC.Project_Status__c == 'Signed Off' || BC.Project_Status__c == 'Cancelled' || BC.Project_Status__c == 'Completed' ){
                   if(ProfileSet.contains(UserInfo.getProfileId())){
                        BC.Project_Status__c.addError('You are not allowed to Change Status to '+BC.Project_Status__c);
                    }                       
                }
            }//                                    
        }
    }

    //Auto Updating of Revenue Status based on Project Status,ForecastAnnualcallsGrowth and MeasuredAnnualcallsGrowth
    if(trigger.isInsert || trigger.isUpdate)
    {
    system.debug('Auto Update Trigger Started');
    Boolean flag=false;                                        
    List<RecordType> RecordTypes= new List<RecordType>();
    RecordTypes=[Select Id from RecordType where (Name = 'Calls And Lines Acquisition' or Name = 'Calls And Lines Acquisition BTBTeam'
                   or Name = 'Calls And Lines Acquisition Read Only') and SOBJECTTYPE = 'BookToBill__c'];
    Map<Id,Id> recordMap= new Map<Id,Id>();
    
    for(RecordType r:RecordTypes)
    {
        recordMap.put(r.Id,r.Id);
    }
    system.debug('recordMap'+recordMap);
    for(BookToBill__c b2B:Trigger.New)
    {   
         system.debug('b2B Id'+b2B.RecordTypeId);              
         if(recordMap.containsKey(b2B.RecordTypeId))
          {
             system.debug('status'+b2B.Project_Status__c);
             if(b2B.Project_Status__c=='Cancelled' || b2B.Project_Status__c=='Deleted')
            {
                b2B.Revenue_Status__c='White';
            }
            if(b2B.Project_Status__c=='Service Desk to validate' || b2B.Project_Status__c=='Project In Progress - Customer Delay' || b2B.Project_Status__c=='Project In Progress - Customer Managed' || b2B.Project_Status__c=='Project In Progress - BT Managed' || b2B.Project_Status__c=='Awaiting more data from SE' || b2B.Project_Status__c=='Returned To SE - More Data' || (b2B.Project_Status__c=='Completed' && b2B.Measured_Annual_Growth__c==NULL))
            {
                b2B.Revenue_Status__c='Yellow';
            }
            if((b2B.Project_Status__c=='Completed' && b2B.Measured_Annual_Growth__c==0) || (b2B.Project_Status__c=='Clawback'))
            {
                b2B.Revenue_Status__c='Red';
            }
            if((b2B.Project_Status__c=='Completed' && b2B.Forecast_Annual_Growth__c>0 && b2B.Measured_Annual_Growth__c>b2B.Forecast_Annual_Growth__c && b2B.Measured_Annual_Growth__c!=0) || (b2B.Project_Status__c=='Signed Off' && b2B.Forecast_Annual_Growth__c>0 && b2B.Measured_Annual_Growth__c>b2B.Forecast_Annual_Growth__c) || (b2B.Project_Status__c=='Signed Off - No Measurement'))
            {
                b2B.Revenue_Status__c='Green';
            }
            if((b2B.Project_Status__c=='Completed' && b2B.Forecast_Annual_Growth__c>0 && b2B.Measured_Annual_Growth__c<b2B.Forecast_Annual_Growth__c && b2B.Measured_Annual_Growth__c!=0) || (b2B.Project_Status__c=='Signed Off' && b2B.Forecast_Annual_Growth__c>0 && b2B.Measured_Annual_Growth__c<b2B.Forecast_Annual_Growth__c))
            {
                b2B.Revenue_Status__c='Amber';
            }
            if((b2B.Project_Status__c=='Project in Progress - Reopened')|| ((b2B.Project_Status__c=='Project In Progress - BT Managed' || b2B.Project_Status__c=='Project In Progress - Customer Managed') && b2B.Forecast_Annual_Growth__c>0 && b2B.Measured_Annual_Growth__c==0) || ((b2B.Project_Status__c=='Project In Progress - BT Managed' || b2B.Project_Status__c=='Project In Progress - Customer Managed') && b2B.Forecast_Annual_Growth__c>0 && b2B.Measured_Annual_Growth__c<b2B.Forecast_Annual_Growth__c) || ((b2B.Project_Status__c=='Project In Progress - BT Managed' || b2B.Project_Status__c=='Project In Progress - Customer Managed') && b2B.Forecast_Annual_Growth__c>0 && b2B.Measured_Annual_Growth__c>b2B.Forecast_Annual_Growth__c))
            {
                b2B.Revenue_Status__c='Yellow 2';
            }
       }
    }
    }
    
}