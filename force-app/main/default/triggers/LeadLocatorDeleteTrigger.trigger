trigger LeadLocatorDeleteTrigger on Lead_Locator__c (Before Delete,after Insert,after Update) {
    
    if(Trigger.IsAfter){
        
        Map<id,Lead_Locator__c> UpdateAccount = new Map<id,Lead_Locator__c>();
        set<Id> AccountIds = new set<Id>();                             
        for(Lead_Locator__c LeadLocator : Trigger.New){          
            if(Trigger.isInsert){
                if(LeadLocator.Person_Role__c == 'Service Manager' || LeadLocator.Person_Role__c == 'Business Manager' || 
                   LeadLocator.Person_Role__c == 'Credit Support' || LeadLocator.Person_Role__c == 'Sales Manager (Primary)' || 
                   LeadLocator.Person_Role__c == 'General Manager' || LeadLocator.Person_Role__c == 'Desk Based Account Manager' ||
                   LeadLocator.Person_Role__c == 'Desk Based Sales Manager' || LeadLocator.Person_Role__c == 'Service Director' ||
                   LeadLocator.Person_Role__c == 'Service Management Centre' || LeadLocator.Person_Role__c == 'Service Management Centre Manager'
                   || LeadLocator.Person_Role__c == 'Business Direct Specialist' || LeadLocator.Person_Role__c == 'Service Manager Escalation'
                   || LeadLocator.Person_Role__c == 'Service Management Centre Escalation' ){                  
                    UpdateAccount.put(LeadLocator.id,LeadLocator);
                    AccountIds.add(LeadLocator.account__c);               
                }
            } if(Trigger.IsUpdate){             
                if((LeadLocator.Person_Role__c != 'Service Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'Service Manager') || 
                   (LeadLocator.Person_Role__c != 'Sales Manager (Primary)' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'Sales Manager (Primary)') ||    
                   (LeadLocator.Person_Role__c != 'Business Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'Business Manager') ||
                   (LeadLocator.Person_Role__c != 'Credit Support' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'Credit Support') || 
                   (LeadLocator.Person_Role__c != 'General Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'General Manager') ||
                   (LeadLocator.Person_Role__c != 'Desk Based Account Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'Desk Based Account Manager') ||
                   (LeadLocator.Person_Role__c != 'Desk Based Sales Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'Desk Based Sales Manager') ||
                   (LeadLocator.Person_Role__c == 'Service Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Service Manager') || 
                   (LeadLocator.Person_Role__c == 'Business Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Business Manager') ||
                   (LeadLocator.Person_Role__c == 'Sales Manager (Primary)' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Sales Manager (Primary)') ||    
                   (LeadLocator.Person_Role__c == 'Credit Support' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Credit Support') ||
                   (LeadLocator.Person_Role__c == 'General Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'General Manager') ||
                   (LeadLocator.Person_Role__c == 'Desk Based Account Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Desk Based Account Manager') ||
                   (LeadLocator.Person_Role__c == 'Desk Based Sales Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Desk Based Sales Manager') ||
                   (LeadLocator.Person_Role__c == 'Service Director' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Service Director') ||
                   (LeadLocator.Person_Role__c == 'Service Management Centre' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Service Management Centre') ||
                   (LeadLocator.Person_Role__c == 'Service Management Centre Manager' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Service Management Centre Manager') ||                  
                   (LeadLocator.Person_Role__c == 'Service Manager Escalation' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Service Manager Escalation') ||
                   (LeadLocator.Person_Role__c == 'Service Management Centre Escalation' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Service Management Centre Escalation') ||  
                   LeadLocator.Person_Role__c == 'Service Manager' || LeadLocator.Person_Role__c == 'Business Manager' || LeadLocator.Person_Role__c == 'Credit Support'||
                   LeadLocator.Person_Role__c == 'Sales Manager (Primary)' || LeadLocator.Person_Role__c == 'General Manager' || LeadLocator.Person_Role__c == 'Desk Based Account Manager' 
                   || LeadLocator.Person_Role__c == 'Desk Based Sales Manager' || LeadLocator.Person_Role__c == 'Service Management Centre' || LeadLocator.Person_Role__c == 'Service Director'
                   || LeadLocator.Person_Role__c == 'Service Management Centre Manager' || LeadLocator.Person_Role__c == 'Service Manager Escalation'|| LeadLocator.Person_Role__c == 'Service Management Centre Escalation' 
                   || LeadLocator.User__c != Trigger.oldMap.get(LeadLocator.Id).User__c
                   || (LeadLocator.Person_Role__c == 'Business Direct Specialist' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c != 'Business Direct Specialist') 
                   || (LeadLocator.Person_Role__c != 'Business Direct Specialist' && Trigger.oldMap.get(LeadLocator.Id).Person_Role__c == 'Business Direct Specialist')){             
                       UpdateAccount.put(LeadLocator.id,LeadLocator);
                       AccountIds.add(LeadLocator.account__c);                 
                   }                 
            }                      
        }            
        Map<ID, Account> A = new Map<ID, Account>([select Id,SRM_Manager__c,SRM_Managers__c, SRM_Managers__r.Email,
                                                   DBAM_User__c, DBAM_User__r.Email, DBAM_SM_User__c, DBAM_SM_User__r.Email, 
                                                   Business_Manager__c,Business_Managers__c,Sales_Manager_Primary__c, Sales_Manager_Primary__r.Email,
                                                   Business_Managers__r.Email, Credit_Support__c, General_Manager__c, General_Manager__r.Email,
                                                   Service_Management_Centre__c,Service_Management_Centre_Manager__c,Service_Director__c,
                                                   Service_Manager_Escalation__c,Service_Management_Centre_Escalation__c,Business_Direct_Specialist__c
                                                   from Account Where ID IN : AccountIds]);                    
        for(Lead_Locator__c LeadLocatorValues : Trigger.New){              
            if(UpdateAccount.ContainsKey(LeadLocatorValues.id)){                  
                if(A.ContainsKey(LeadLocatorValues.Account__c)){       
                    if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Sales Manager (Primary)'){
                        A.get(LeadLocatorValues.Account__c).Sales_Manager_Primary__c = UpdateAccount.get(LeadLocatorValues.id).User__c;                     
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Business Manager'){
                        A.get(LeadLocatorValues.Account__c).Business_Manager__c = UpdateAccount.get(LeadLocatorValues.id).User_Name__c;                     
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Service Manager'){
                        A.get(LeadLocatorValues.Account__c).SRM_Manager__c = UpdateAccount.get(LeadLocatorValues.id).User_Name__c;                                                                                                                    
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Credit Support'){
                        A.get(LeadLocatorValues.Account__c).Credit_Support__c = UpdateAccount.get(LeadLocatorValues.id).User_Name__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Business Manager'){
                        A.get(LeadLocatorValues.Account__c).Business_Managers__c = UpdateAccount.get(LeadLocatorValues.id).User__c;
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Service Manager'){
                        A.get(LeadLocatorValues.Account__c).SRM_Managers__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Account Manager'){
                        A.get(LeadLocatorValues.Account__c).DBAM_User__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Sales Manager'){
                        A.get(LeadLocatorValues.Account__c).DBAM_SM_User__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Account Manager'){
                        A.get(LeadLocatorValues.Account__c).DBAM__c = UpdateAccount.get(LeadLocatorValues.id).User_Name__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Sales Manager'){
                        A.get(LeadLocatorValues.Account__c).DBAM_SM__c = UpdateAccount.get(LeadLocatorValues.id).User_Name__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'General Manager'){
                        A.get(LeadLocatorValues.Account__c).General_Manager__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Service Director'){
                        A.get(LeadLocatorValues.Account__c).Service_Director__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Service Management Centre'){
                        A.get(LeadLocatorValues.Account__c).Service_Management_Centre__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Service Management Centre Manager'){
                        A.get(LeadLocatorValues.Account__c).Service_Management_Centre_Manager__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Service Manager Escalation'){
                        A.get(LeadLocatorValues.Account__c).Service_Manager_Escalation__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Service Management Centre Escalation'){
                        A.get(LeadLocatorValues.Account__c).Service_Management_Centre_Escalation__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c == 'Business Direct Specialist'){
                        A.get(LeadLocatorValues.Account__c).Business_Direct_Specialist__c = UpdateAccount.get(LeadLocatorValues.id).User__c;  
                    }
                    if(Trigger.isUpdate){
                        if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Sales Manager (Primary)' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Sales Manager (Primary)'){
                            A.get(LeadLocatorValues.Account__c).Sales_Manager_Primary__c = NULL;                                              
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Service Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Service Manager'){
                            A.get(LeadLocatorValues.Account__c).SRM_Manager__c = '';                                              
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Business Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Business Manager'){
                            A.get(LeadLocatorValues.Account__c).Business_Manager__c = '';
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Credit Support' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Credit Support'){
                            A.get(LeadLocatorValues.Account__c).Credit_Support__c = '';                                              
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Business Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Business Manager'){
                            A.get(LeadLocatorValues.Account__c).Business_Managers__c = NULL;
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Desk Based Account Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Account Manager'){
                            A.get(LeadLocatorValues.Account__c).DBAM_User__c = NULL;
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Desk Based Sales Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Sales Manager'){
                            A.get(LeadLocatorValues.Account__c).DBAM_SM_User__c = NULL;
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Desk Based Account Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Account Manager'){
                            A.get(LeadLocatorValues.Account__c).DBAM__c = '';
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Desk Based Sales Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Desk Based Sales Manager'){
                            A.get(LeadLocatorValues.Account__c).DBAM_SM__c = '';
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Service Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Service Manager '){
                            A.get(LeadLocatorValues.Account__c).SRM_Managers__c = NULL;
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'General Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'General Manager'){
                            A.get(LeadLocatorValues.Account__c).General_Manager__c = NULL;
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Service Director'  && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Service Director'){
                            A.get(LeadLocatorValues.Account__c).Service_Director__c = NULL;  
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Service Management Centre' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Service Management Centre'){
                            A.get(LeadLocatorValues.Account__c).Service_Management_Centre__c = NULL;  
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Service Management Centre Manager' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Service Management Centre Manager'){
                            A.get(LeadLocatorValues.Account__c).Service_Management_Centre_Manager__c = NULL;  
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Service Manager Escalation' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Service Management Escalation'){
                            A.get(LeadLocatorValues.Account__c).Service_Manager_Escalation__c = NULL;  
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Service Management Centre Escalation' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Service Management Centre Escalation'){
                            A.get(LeadLocatorValues.Account__c).Service_Management_Centre_Escalation__c = NULL;  
                        }if(UpdateAccount.get(LeadLocatorValues.id).Person_role__c != 'Business Direct Specialist' && Trigger.OldMap.get(LeadLocatorValues.id).Person_role__c == 'Business Direct Specialist'){
                            A.get(LeadLocatorValues.Account__c).Business_Direct_Specialist__c = NULL;  
                        }
                    }
                }                                             
            }
        }
        Update A.values();
        system.debug('Service Director'+A.Values()); 
    }          
    
    if(Trigger.isDelete){
        
        Map<String,String> MapCorpProfiles=new Map<String,String>();
        Map<String,String> SRMBusinessManagers = new Map<String,String>();
        set<id> updateSRMABusinessccounts = new  set<id>();    
        List<Account> SRM_BusinessManager = new List<Account>();        
        
        List<Profile> corp=[Select Id,Name From Profile Where Name In ('Corporate: Admin user','Custom:Standard User')];
        
        for(Profile P:corp)
            MapCorpProfiles.put(P.Id,P.Name);
        
        for(Lead_Locator__c LLD : Trigger.old){  
            String P=MapCorpProfiles.get(UserInfo.getProfileId());
            If((P !=null )&&(LLD.Person_Role__c!='Service Relationship Manager'))
                LLD.addError('You are not allowed to delete this record');
            
            if(LLD.Person_Role__c == 'Service Manager' || LLD.Person_Role__c == 'Business Manager'  || LLD.Person_Role__c == 'Credit Support' ||
               LLD.Person_Role__c == 'Sales Manager (Primary)' || LLD.Person_Role__c == 'General Manager' || LLD.Person_Role__c == 'Desk Based Account Manager' || 
               LLD.Person_Role__c == 'Desk Based Sales Manager' || LLD.Person_Role__c == 'Service Director' || LLD.Person_Role__c == 'Service Management Centre' ||
               LLD.Person_Role__c == 'Service Management Centre Manager' || LLD.Person_Role__c == 'Service Manager Escalation' ||
               LLD.Person_Role__c == 'Service Management Centre Escalation' || LLD.Person_Role__c == 'Business Direct Specialist'){          
                updateSRMABusinessccounts.add(LLD.Account__c);
                if(LLD.Person_Role__c=='Sales Manager (Primary)' || LLD.Person_Role__c=='Service Manager' || LLD.Person_Role__c=='Desk Based Sales Manager' 
                   || LLD.Person_Role__c=='General Manager' || LLD.Person_Role__c=='Business Manager' || LLD.Person_Role__c=='Desk Based Account Manager'
                   || LLD.Person_Role__c=='Service Director' || LLD.Person_Role__c=='Service Management Centre' || 
                   LLD.Person_Role__c=='Service Management Centre Manager' || LLD.Person_Role__c == 'Business Direct Specialist' || LLD.Person_Role__c == 'Service Manager Escalation' ||
                   LLD.Person_Role__c == 'Service Management Centre Escalation')
                {
                    SRMBusinessManagers.put(LLD.User__c+'-'+LLD.Account__c+'-'+LLD.Person_Role__c,LLD.Person_Role__c);
                }
                SRMBusinessManagers.put(LLD.User_Name__c+'-'+LLD.Account__c+'-'+LLD.Person_Role__c,LLD.Person_Role__c);                               
            }    
        }   
        List<Account> UpdateSRMBusinessFields = [select id,SRM_Manager__c,SRM_Managers__c,DBAM_User__c,
        DBAM_SM__c,DBAM__c,DBAM_SM_User__c,Business_Manager__c,Business_Managers__c,Credit_Support__c,
        Sales_Manager_Primary__c,General_Manager__c,Service_Director__c,Service_Management_Centre__c,
        Service_Management_Centre_Manager__c,Service_Manager_Escalation__c,Service_Management_Centre_Escalation__c,
        Business_Direct_Specialist__c from Account Where ID IN : updateSRMABusinessccounts ];
        
        for(Account SRMBusiness:UpdateSRMBusinessFields ){
            
            if(SRMBusinessManagers.containsKey(SRMBusiness.Sales_Manager_Primary__c+'-'+SRMBusiness.id+'-Sales Manager (Primary)')){
                if(SRMBusinessManagers.get(SRMBusiness.Sales_Manager_Primary__c+'-'+SRMBusiness.id+'-Sales Manager (Primary)') == 'Sales Manager (Primary)'){
                    SRMBusiness.Sales_Manager_Primary__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.SRM_Managers__c+'-'+SRMBusiness.id+'-Service Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.SRM_Managers__c+'-'+SRMBusiness.id+'-Service Manager') == 'Service Manager'){
                    SRMBusiness.SRM_Managers__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.DBAM_User__c+'-'+SRMBusiness.id+'-Desk Based Account Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.DBAM_User__c+'-'+SRMBusiness.id+'-Desk Based Account Manager') == 'Desk Based Account Manager'){
                    SRMBusiness.DBAM_User__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.DBAM_SM_User__c+'-'+SRMBusiness.id+'-Desk Based Sales ManagerSM')){
                if(SRMBusinessManagers.get(SRMBusiness.DBAM_SM_User__c+'-'+SRMBusiness.id+'-Desk Based Sales Manager') == 'Desk Based Sales Manager'){
                    SRMBusiness.DBAM_SM_User__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Service_Director__c+'-'+SRMBusiness.id+'-Service Director')){
                if(SRMBusinessManagers.get(SRMBusiness.Service_Director__c+'-'+SRMBusiness.id+'-Service Director') == 'Service Director'){
                    SRMBusiness.Service_Director__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Service_Manager_Escalation__c+'-'+SRMBusiness.id+'-Service Manager Escalation')){
                if(SRMBusinessManagers.get(SRMBusiness.Service_Manager_Escalation__c+'-'+SRMBusiness.id+'-Service Manager Escalation') == 'Service Manager Escalation'){
                    SRMBusiness.Service_Manager_Escalation__c= NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Service_Management_Centre_Escalation__c +'-'+SRMBusiness.id+'-Service Management Centre Escalation')){
                if(SRMBusinessManagers.get(SRMBusiness.Service_Management_Centre_Escalation__c +'-'+SRMBusiness.id+'-Service Management Centre Escalation') == 'Service Management Centre Escalation'){
                    SRMBusiness.Service_Management_Centre_Escalation__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Service_Management_Centre__c+'-'+SRMBusiness.id+'-Service Management Centre')){
                if(SRMBusinessManagers.get(SRMBusiness.Service_Management_Centre__c+'-'+SRMBusiness.id+'-Service Management Centre') == 'Service Management Centre'){
                    SRMBusiness.Service_Management_Centre__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Service_Management_Centre_Manager__c+'-'+SRMBusiness.id+'-Service Management Centre Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.Service_Management_Centre_Manager__c+'-'+SRMBusiness.id+'-Service Management Centre Manager') == 'Service Management Centre Manager'){
                    SRMBusiness.Service_Management_Centre_Manager__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.SRM_Manager__c+'-'+SRMBusiness.id+'-Service Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.SRM_Manager__c+'-'+SRMBusiness.id+'-Service Manager') == 'Service Manager'){
                    SRMBusiness.SRM_Manager__c = '';
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Business_Manager__c+'-'+SRMBusiness.id+'-Business Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.Business_Manager__c+'-'+SRMBusiness.id+'-Business Manager') == 'Business Manager'){
                    SRMBusiness.Business_Manager__c = '';
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Credit_Support__c+'-'+SRMBusiness.id+'-Credit Support')){
                if(SRMBusinessManagers.get(SRMBusiness.Credit_Support__c+'-'+SRMBusiness.id+'-Credit Support') == 'Credit Support'){
                    SRMBusiness.Credit_Support__c = '';
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.DBAM__c+'-'+SRMBusiness.id+'-Desk Based Account Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.DBAM__c+'-'+SRMBusiness.id+'-Desk Based Account Manager') == 'Desk Based Account Manager'){
                    SRMBusiness.DBAM__c = '';
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.DBAM_SM__c+'-'+SRMBusiness.id+'-Desk Based Sales Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.DBAM_SM__c+'-'+SRMBusiness.id+'-Desk Based Sales Manager') == 'Desk Based Sales Manager'){
                    SRMBusiness.DBAM_SM__c = '';
                }
            }/**if(SRMBusinessManagers.containsKey(SRMBusiness.Service_Director__c+'-'+SRMBusiness.id+'-Service Director')){
                if(SRMBusinessManagers.get(SRMBusiness.Service_Director__c+'-'+SRMBusiness.id+'-Service Director') == 'Service Director'){
                    SRMBusiness.Service_Director__c = '';
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Service_Management_Centre_Manager__c+'-'+SRMBusiness.id+'-Service Management Centre Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.Service_Management_Centre_Manager__c+'-'+SRMBusiness.id+'-Service Management Centre Manager') == 'Service Management Centre Manager'){
                    SRMBusiness.Service_Management_Centre_Manager__c = '';
                }
            }*/if(SRMBusinessManagers.containsKey(SRMBusiness.Business_Managers__c+'-'+SRMBusiness.id+'-Business Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.Business_Managers__c+'-'+SRMBusiness.id+'-Business Manager') == 'Business Manager'){
                    SRMBusiness.Business_Managers__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.General_Manager__c+'-'+SRMBusiness.id+'-General Manager')){
                if(SRMBusinessManagers.get(SRMBusiness.General_Manager__c+'-'+SRMBusiness.id+'-General Manager') == 'General Manager'){
                    SRMBusiness.General_Manager__c = NULL;
                }
            }if(SRMBusinessManagers.containsKey(SRMBusiness.Business_Direct_Specialist__c+'-'+SRMBusiness.id+'-Business Direct Specialist')){
                if(SRMBusinessManagers.get(SRMBusiness.Business_Direct_Specialist__c+'-'+SRMBusiness.id+'-Business Direct Specialist') == 'Business Direct Specialist'){
                    SRMBusiness.Business_Direct_Specialist__c = NULL;
                }
            }
            
            SRM_BusinessManager.add(SRMBusiness);       
        } 
        update SRM_BusinessManager; 
        
    }
    
}