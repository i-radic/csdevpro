trigger s2s_oppy_after on s2s_Opportunities__c (after insert, after update) {
/*
Date            Author                       Comment
12/03/2013      John McGovern                Additional code to support Busiess Direct SF to SF CR3809
*/
if (trigger.isinsert){
    List<PartnerNetworkRecordConnection> ShareRecords = new List<PartnerNetworkRecordConnection>();
      
    for (s2s_Opportunities__c o:Trigger.new){ 
        //object declaration for PartnerNetwork
        PartnerNetworkRecordConnection newrecord = new PartnerNetworkRecordConnection();
        //set default connection to Engage
        newrecord.ConnectionId = '04P2000000019ha';
        
        //choose connection based on link type Engage or BusDirect
        if(StaticVariables.s2sType == 'Engage'){
            newrecord.ConnectionId = '04P2000000019ha';
        }
        //added in CR3809
        else if(StaticVariables.s2sType == 'BusDir'){
            newrecord.ConnectionId = '04P2000000019k0'; 
        }
        
        system.debug('JMM LinkedOrg:'+o.linked_Org__c);
        system.debug('JMM Link:'+o.Opportunity__r.s2s_Link__c);
        system.debug('JMM Id:'+o.Opportunity__r.Id);
        newrecord.LocalRecordId = o.id;  
        newrecord.SendClosedTasks = false;
        newrecord.SendOpenTasks = false;
        newrecord.SendEmails = false;
        //All the Records are added to the PartnerNetwork
        ShareRecords.add(newrecord);
    }    
    //These Records are inserted into Partnernetwork
    insert ShareRecords;
}
                    
              
if (trigger.isupdate){
StaticVariables.sets2sOpportunityDontRun(true);
if (Test.isRunningTest() == False && userinfo.getname() != 'Connection User')return;

set<string> OppsToUpdate = new Set<string>();//get all associated SACs in batch
for (s2s_Opportunities__c a:Trigger.new) {
      OppsToUpdate.add(a.opp_ID__c);
    }

system.debug('ADJ OppsToUpdate' + OppsToUpdate);    

 
//create a few maps to hold the values
    Map<String, String> opName = new Map<String, String>();
    Map<String, String> s2sOwnerName = new Map<String, String>();
    Map<String, String> s2sStatus = new Map<String, String>();
    Map<String, String> s2sDescription = new Map<String, String>();
    Map<String, String> s2sOwnerPhone = new Map<String, String>();
    Map<String, String> s2sOwnerEmail = new Map<String, String>();
    Map<String, Date> s2sDate = new Map<String, Date>();
    Map<String, Decimal> s2sAmount = new Map<String, Decimal>();
    Map<String, String> s2sStage = new Map<String, String>();
    
List<s2s_Opportunities__c> oppys = [select  id, opp_ID__c, s2s_status__c, s2s_title__c, s2s_description__c, s2s_Owner_Name__c, s2s_Owner_Phone__c, s2s_Owner_Email__c, linked_Org__c, s2s_Amount__c, s2s_Close_Date__c, s2s_Stage__c from s2s_Opportunities__c where opp_ID__c = :OppsToUpdate];


for (s2s_Opportunities__c op :oppys) {
    opName.put(op.opp_ID__c,op.s2s_title__c); 
    s2sOwnerName.put(op.opp_ID__c,op.s2s_Owner_Name__c); 
    s2sStatus.put(op.opp_ID__c,op.s2s_status__c); 
    s2sDescription.put(op.opp_ID__c,op.s2s_description__c); 
    s2sOwnerPhone.put(op.opp_ID__c,op.s2s_Owner_Phone__c); 
    s2sOwnerEmail.put(op.opp_ID__c,op.s2s_Owner_Email__c); 
    s2sDate.put(op.opp_ID__c,op.s2s_Close_Date__c); 
    s2sAmount.put(op.opp_ID__c,op.s2s_Amount__c); 
    s2sStage.put(op.opp_ID__c,op.s2s_Stage__c); 
    }
    
system.debug('ADJ s2sStatus' + s2sStatus);  
    

                
   List<Opportunity> updateOpps = new List<Opportunity>();
        //for(List<Opportunity> opps : [select id, Opportunity_ID__c, s2s_status__c, s2s_Owner_Name__c, s2s_Owner_Email__c, name, s2s_Owner_Phone__c, s2s_description__c from Opportunity where id = :OppsToUpdate]){
        for(List<Opportunity> opps : [select id, Opportunity_ID__c, s2s_status__c, name, s2s_description__c,s2s_Link__c, s2s_Close_Date__c, s2s_Amount__c from Opportunity where id = :OppsToUpdate]){
            for(Opportunity opp : Opps){
                updateOpps.add(new Opportunity(
                id=opp.id,
                s2s_status__c = s2sStatus.get(opp.ID),
                s2s_Close_Date__c = s2sDate.get(opp.ID),
                s2s_Amount__c = s2sAmount.get(opp.ID),
                s2s_Stage__c = s2sStage.get(opp.ID),
                //s2s_Owner_Name__c = s2sOwnerName.get(opp.ID),
                //s2s_Owner_Phone__c = s2sOwnerPhone.get(opp.ID),
                //s2s_Owner_Email__c = s2sOwnerEmail.get(opp.ID),
                s2s_description__c = 'Owner: ' + s2sOwnerName.get(opp.ID) + '\n Phone: ' + s2sOwnerPhone.get(opp.ID) + '\n Email: ' + s2sOwnerEmail.get(opp.ID) + '\n Description: ' + s2sDescription.get(opp.ID)
                )); 
                    update updateOpps;  
                    }
                }    
            }   
  
}