trigger tgrADPAfter on ADP__c (after insert, after update) {
   Set<ID> adpIDs = new Set<ID>();
   for (ADP__c adps : trigger.new) {   
      //build a set of unique account ids associated to the case(s) being created
      // system.debug('caseAccountID'+c.AccountID);
      if (adps.Customer__c != null) {
         adpIDs.add(adps.Customer__c);
      }
   }
   
   system.debug('ADPIDS: ' + adpIDs);
   Map<ID, Account> acctsToUpdate = new Map<ID, Account>([select Id FROM Account WHERE Id in :adpIDs]);
   system.debug('ADPIDS: ' + acctsToUpdate );                
   for (ADP__c adps : trigger.new) {
        for (Account acct : acctsToUpdate.values()) {
        //system.debug('acct.zMobileAcq__c: ' + acct.zMobileAcq__c);
        system.debug('adps.Number_of_Customers__c: ' + adps.Number_of_Customers__c);
        
            //acct.zMobileAcq__c = adps.zMobileAcq__c;
            //acct.zMobileDef__c = adps.zMobileDef__c ;
            //acct.zNetworkingAcq__c = adps.zNetworkingAcq__c ;
            //acct.zNetworkingDef__c = adps.zNetworkingDef__c ;
            //acct.zSwitchLANAcq__c = adps.zSwitchLANAcq__c ;
            //acct.zSwitchLANDef__c = adps.zSwitchLANDef__c ;
            //acct.zUVSAcq__c = adps.zUVSAcq__c ;
            //acct.zUVSDef__c = adps.zUVSDef__c ;
         
        }
    }
    update acctsToUpdate.values();
     
}