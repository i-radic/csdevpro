trigger UpdateBBBackOfficeStatus on CRF__c (before update) {
  
if(Trigger.isupdate){ 
CRF__c[] CRFNew=Trigger.New;
CRF__c[] CRFOld=Trigger.Old;
 Boolean Flag=True;
 Id RTId =CRFNew[0].RecordTypeId;
 String RTName = [Select DeveloperName from RecordType where Id=:RTId].DeveloperName;
 if(RTName=='NSO_BS'){
        If (CRFOld[0].Back_Office_Status__c != CRFNew[0].Back_Office_Status__c)  {
            List<CRF__c> ListBBCRF = [Select Id,Install_Activation_Date__c,Back_Office_Status__c,Related_to_NSO_CRF__c,Related_NSO_CRF__c from CRF__c where (Related_to_NSO_CRF__c=:CRFNew[0].Id OR Related_NSO_CRF__c=:CRFNew[0].Id)];
            if(ListBBCRF.size()>0){
                for(CRF__c forcrf:ListBBCRF){
                    forcrf.Back_Office_Status__c=CRFNew[0].Back_Office_Status__c;                
                    if(forcrf.Install_Activation_Date__c!=null && forcrf.Install_Activation_Date__c<System.today()+5){
                    CRFNew[0].addError(' The Install/Activation Date for the related \'One plan and BB CRF(s)\' must be at least 5 days from today.');
                    Flag=False;
                    }
                }
            If(Flag==True)    
            update ListBBCRF;
            
            }
        
        }
    
    }
}
 
}