trigger TAS_CompetitorSync on Opportunity (after insert, after update) {
   if(TriggerDeactivating__c.getInstance().Opportunity__c ) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     }  
    for (Opportunity o : Trigger.new) { 
        if (Trigger.isUpdate) {
            If (Test.isRunningTest() != true){
                
                if (o.Main_Competitor__c != Trigger.OldMap.Get(o.id).Main_Competitor__c ){  //only fire if Competitor changes
                    /*List < OpportunityCompetitor > existingCompetitor = [Select Id from OpportunityCompetitor where OpportunityId = :o.Id];
                    if (existingCompetitor.size() > 0){
                        delete existingCompetitor;
                    }*/
                    if (o.Main_Competitor__c != Null )  {
                        string MainCompName= [SELECT Main_Competitor__r.name FROM Opportunity where Id = :o.id Limit 1].Main_Competitor__r.name;
                        OpportunityCompetitor newCompetitor = new OpportunityCompetitor();
                        If (MainCompName.length() > 40) { 
                            newCompetitor.CompetitorName = MainCompName.substring(1, 39); 
                        } 
                        else { 
                            newCompetitor.CompetitorName = MainCompName; 
                        } 
                        newCompetitor.OpportunityId = o.id; 
                        //Update newCompetitor; 
                        insert newCompetitor; 
                    } 
                }
            }
        } // end of is update
        
        if (Trigger.isInsert) {
            If (Test.isRunningTest() != true){
                if (o.Main_Competitor__c != Null )  {
                    string MainCompName= [SELECT Main_Competitor__r.name FROM Opportunity where Id = :o.id Limit 1].Main_Competitor__r.name;
                    OpportunityCompetitor newCompetitor = new OpportunityCompetitor();
                    If (MainCompName.length() > 40) { 
                        newCompetitor.CompetitorName = MainCompName.substring(1, 39); 
                    } 
                    else { 
                        newCompetitor.CompetitorName = MainCompName; 
                    } 
                    newCompetitor.OpportunityId = o.id; 
                    insert newCompetitor; 
                } 
            }
        } // end is insert
    } // end if 
} // end for