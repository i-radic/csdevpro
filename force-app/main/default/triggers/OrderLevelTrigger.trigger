trigger OrderLevelTrigger on Order_Level__c (after insert, after update, before insert, before update, before delete, after delete) {

	OrderLevelTriggerHandler handler = new OrderLevelTriggerHandler(
								Trigger.newMap, Trigger.oldMap, Trigger.new, Trigger.old,
								Trigger.Size,
								Trigger.IsInsert, Trigger.IsUpdate, Trigger.IsBefore, Trigger.IsAfter,
								Trigger.IsExecuting,
								Trigger.IsDelete	);

	// Execute if Trigger Enabler custom setting is enabled
    if (EE_TriggerClass.isTriggerEnabled('Order_Level__c')) {
    	
		handler.processTrigger();
		
    }

}