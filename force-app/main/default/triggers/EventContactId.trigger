trigger EventContactId on Event (before insert, before update) {
	if(TriggerDeactivating__c.getInstance().Event__c) {
         System.debug('Bypassing trigger due to custom setting' );
         return;
     } 
     Set<Id> ContactIds = new Set<Id>();
     for (Event e:Trigger.new){
        if (e.WhoId != null){
          ContactIds.add(e.whoId);
        }
     }
     
    if(ContactIds.size()>0){
        Map<Id, String> mContacts = new Map<Id, String>();
        List<Contact> ContactIIds = [Select Id, IntegrationId__c from Contact where  Id in:ContactIds];
        System.debug('GOV:EventContactID1'); 
       

        for (Contact ci :ContactIIds) {
           System.debug('***************************************************************************');
             mContacts.put(ci.Id, ci.IntegrationId__c);//populate map
        }
            
        for (Event e:Trigger.new){
           if (mContacts.containsKey(e.whoId) == True) { 
               e.Event_Contact_Id__c = mContacts.get(e.whoId);
           }    
        }   
    } 
}