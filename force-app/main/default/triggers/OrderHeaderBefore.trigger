trigger OrderHeaderBefore on Order_Header__c (before delete)  { 
	//delete opp line item when header deleted
	if(Trigger.isDelete){
		Set<Id> Ids= Trigger.oldMap.keyset();
		Set<Id> delIds= new Set<Id>();

		for (Order_Header__c oh :[SELECT zXMLInfo__c FROM Order_Header__c WHERE Id IN :Ids]){
			String oliOS = qbOrderCTRL.readApexXML(oh.zXMLInfo__c, 'oliOutrightSale');
			String oliL = qbOrderCTRL.readApexXML(oh.zXMLInfo__c, 'oliLease');
			if(oliOS != '' && oliL != '')
				delIds.add(oliOS);
				delIds.add(oliL);
		}
		if(delIds.size()>0 && !Test.isRunningTest()){
			Delete [SELECT Id FROM OpportunityLineItem WHERE Id IN :delIds];
		}
	}

}