trigger case_before_insert on Case (before insert) 
{

	if (Trigger.isBefore && Trigger.isInsert)
	{
		CaseTriggerHandler triggerHandler = new CaseTriggerHandler();
		system.debug('First Time Instance Created');
		triggerHandler.handleCaseBeforeInserts(Trigger.new);
	}
}