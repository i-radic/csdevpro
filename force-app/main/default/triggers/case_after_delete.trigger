trigger case_after_delete on Case (after delete) {
    
    if (Trigger.isAfter && Trigger.isDelete)
    {
        CaseTriggerHandler triggerHandler = new CaseTriggerHandler();
        
        triggerHandler.handleCaseAfterDelete(Trigger.old);
    }

}