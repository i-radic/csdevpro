trigger BTSportSiteBefore on BT_Sport_Site__c (before insert, before update) {
    
    
    Date TodaysDate = Date.today();
    Date vStartDate = Date.today();
    Date vEndDate = Date.valueOf('2016-08-31');
    public String roomsonly = null;
    public String roomsPmfLookupKey = null;
    public Decimal roomsPrice =null;
    public String PriceType = null;
    public String Band = null;
    public String var_site_Type = null;
    public String var_contract_Type = null;
    set<string> PMFUnique = new Set<string>();
    set<string> HeaderId = new Set<string>();
    //Variable To Get OppIds vvv
    map<string, string> mSiteHIds = new map<string,string>();
    map<string, string> mSiteHIdOppIdss = new map<string,string>();
    map<string, string> mOppIdContRoles = new map<string,string>();
    map<string, string> mConIdEmails = new map<string,string>();
    map<string, string> mConIdContactBuilding = new map<string,string>();
    map<string, string> mConIdAddressNumber = new map<string,string>();
    map<string, string> mConIdAddressStreet = new map<string,string>();
    map<string, string> mConIdPostCode = new map<string,string>();
    Id BTDataloader2Id = '00e200000015EhzAAE';
    RecordType RecType = [Select Id From RecordType  Where SobjectType = 'BT_Sport_Site__c' and DeveloperName = 'Season3' limit 1];
    
    /* Code To Get OppIds Start*/
    for (BT_Sport_Site__c t:Trigger.new) {
        If(t.RecordTypeID == RecType.ID && userinfo.getProfileId() != BTDataloader2Id) { 
            system.debug('Entered Trigger!!!!!!  ' + t.BT_Sport_CL__c);
            if(!String.isBlank(t.BT_Sport_CL__c)) {
                mSiteHIds.put(t.Id, t.BT_Sport_CL__c);
                system.debug('Opportunity Map : ' + mSiteHIds);
            }        
        }
        for(BT_Sport_CL__c btscl : [SELECT Id,Opportunity__c FROM BT_Sport_CL__c WHERE Id IN : mSiteHIds.values()]) {
            mSiteHIdOppIdss.put(btscl.Id, btscl.Opportunity__c);
        }
        for(OpportunityContactRole ocr : [SELECT ContactId,Id,OpportunityId FROM OpportunityContactRole WHERE OpportunityId IN : mSiteHIdOppIdss.values()]) {
            mOppIdContRoles.put(ocr.OpportunityId, ocr.ContactId);
            system.debug('Opportunity Contact Role Map : ' + mOppIdContRoles);
        }
        for(Contact c : [SELECT Id,Email,Contact_Building__c, Contact_Address_Number__c, Contact_Address_Street__c, Contact_Post_Code__c FROM Contact WHERE Id IN : mOppIdContRoles.values()]) {
            mConIdEmails.put(c.Id, c.Email);
            mConIdContactBuilding.put(c.Id, c.Contact_Building__c);
            mConIdAddressNumber.put(c.Id, c.Contact_Address_Number__c);
            mConIdAddressStreet.put(c.Id, c.Contact_Address_Street__c);
            mConIdPostCode.put(c.Id, c.Contact_Post_Code__c);
            system.debug('Contact Map : ' + mConIdEmails);
            
            /*Code To Get OppIds End*/
            
            
            /* Code To Get Contact Details Start*/
            t.zPrimaryContactEmail__c = mConIdEmails.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Building__c = mConIdContactBuilding.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Address_Number__c = mConIdAddressNumber.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Address_Street__c = mConIdAddressStreet.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            t.zContact_Post_Code__c = mConIdPostCode.get(mOppIdContRoles.get(mSiteHIdOppIdss.get(t.BT_Sport_CL__c)));
            
            /* Code To Get Contact Details  End*/
            
            if(t.PMF_Effective_Date__c != null){
                TodaysDate = t.PMF_Effective_Date__c;
            }
            
            // select either PAYG price points or default to 12 months
            if(t.Contract_Type__c == 'Pay as you go monthly'){
                var_contract_Type = 'Pay as you go monthly';
            }
            
            else {
                var_contract_Type = '12 month contract';
            }   
            
            
            
            //Check for Hero Discounts, if present update site type
            var_site_Type = t.Site_Type__c; // set default 
            If (t.Hero__c == 'Yes'){
                var_site_Type = t.Site_Type__c +' - Hero';
            }
            
            
            
            
            //Get price type           
            List<BT_Sport_Pricing__c> PriceTypeSelect = [Select Site_Type__c, Price_Type__c  from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type Limit 1 ];
            system.debug('ADJ PRICE TYPE = '+ PriceTypeSelect[0].Price_Type__c);   
            
            // set pmflookup key          
            If( PriceTypeSelect[0].Price_Type__c == '1'){ // ie price based purly on site type
                PMFUnique.add(var_site_Type);
                t.PmfLookupKey__c = var_site_Type;
            }  
            else if( PriceTypeSelect[0].Price_Type__c == '2'){  // price based on site & contract (not bands)
                PMFUnique.add(var_site_Type + var_contract_Type);
                t.PmfLookupKey__c = var_site_Type + var_contract_Type;
                system.debug('Price_Type__c = '+ var_site_Type + var_contract_Type);
                system.debug('ADJ BTS Other t.PmfLookupKey__c = '+ t.PmfLookupKey__c);   
            }
            else { 
                if  (PriceTypeSelect[0].Price_Type__c == '3RM' && t.Number_of_Rooms_receiving_BT_Sport__c != Null){  //JMM added not null condition to prevent mis banding
                    List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and Price_Band_Start__c <= :t.Number_of_Rooms_receiving_BT_Sport__c and Price_Band_end__c >= :t.Number_of_Rooms_receiving_BT_Sport__c Limit 1 ];
                    t.Band__c = PriceBandSelect[0].Band__c;
                }
                
                else if  (PriceTypeSelect[0].Price_Type__c == '3RV' && t.Rateable_Value__c != Null){ //JMM added not null condition to prevent mis banding
                    List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and  Price_Band_Start__c <= :t.Rateable_Value__c and Price_Band_end__c >= :t.Rateable_Value__c Limit 1 ];
                    t.Band__c = PriceBandSelect[0].Band__c;
                }
                
                PMFUnique.add(t.Site_Type__c + var_contract_Type + t.Band__c);
                t.PmfLookupKey__c = t.Site_Type__c + var_contract_Type + t.Band__c;
                
                If (t.Product__C == 'BT Sport Total'){
                    roomsPmfLookupKey = 'BT Sport Total Hotel Rooms' + var_contract_Type;
                }
                If (t.Product__C == 'BT Sport 1'){
                    roomsPmfLookupKey = 'BT Sport 1 Hotel Rooms' + var_contract_Type;
                }
                
                system.debug('ADJ PMFUnique.add = '+ t.Site_Type__c + var_contract_Type + t.Band__c);
                system.debug('ADJ t.PmfLookupKey__c = '+ t.PmfLookupKey__c);   
                system.debug('ADJ roomsPmfLookupKey = '+ 'BT Sport Pack Hotel Rooms' + var_contract_Type);  
            }
            
            
            
            // lookup using  PmfLookupKey__c the correct price entry    
            set<string> PMFcodes = new Set<string>();
            Map<String, String> PMF_Value = new Map<String, String>();
            List<BT_Sport_Pricing__c> pmfCode = [Select Site_Type__c, PMF_Code__c, PmfLookupKey__c from BT_Sport_Pricing__c where Entry_Type__C = 'PricePoint' and (PmfLookupKey__c in :PMFUnique or PmfLookupKey__c = :roomsPmfLookupKey)];
            for (BT_Sport_Pricing__c pc :pmfCode) {
                PMF_Value.put(pc.PmfLookupKey__c, pc.PMF_Code__c);
                PMFcodes.add(pc.PMF_Code__c);
                if (pc.PmfLookupKey__c == roomsPmfLookupKey){
                    RoomsOnly = pc.PMF_Code__c;
                }
                system.debug('ADJ pc.PmfLookupKey__c '+ pc.PmfLookupKey__c );
                system.debug('ADJ pc.PMF_Code__c = '+ pc.PMF_Code__c);
            }
            Map<String, Decimal> PMF_Price = new Map<String, Decimal>();
            Map<String, Decimal> PMF_Version = new Map<String, Decimal>();
            
            List<BT_Sport_Pricing__c> pmfPrice = [Select PMF_Code__c, Price__c, PMF_Version__c from BT_Sport_Pricing__c where Entry_Type__C = 'PMF' and PMF_Code__c in :PMFcodes and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate  ] ;
            for (BT_Sport_Pricing__c pp :pmfPrice) {
                PMF_Price.put(pp.PMF_Code__c, pp.Price__c);
                PMF_Version.put(pp.PMF_Code__c, pp.PMF_Version__c);
                system.debug('ADJ pp.PMF_Code__c '+ pp.PMF_Code__c );
                system.debug('ADJ pp.Price__c = '+ pp.Price__c);
            }
            
            t.gross_price__c = PMF_Price.get(PMF_Value.get(t.PmfLookupKey__c));
            t.pmf_version__c = PMF_Version.get(PMF_Value.get(t.PmfLookupKey__c));
            t.Discount_Amount__c = 0;
            system.debug('ADJ t.gross_price__c = '+ t.gross_price__c);
            system.debug('ADJ t.pmf_version__c = '+ t.pmf_version__c);
            
            
            //Calculate Hotel Rooms only Price
            If(t.Site_Type__c.contains('Hotel Rooms') && t.Number_of_Rooms_receiving_BT_Sport__c != null){
                t.gross_price__c = PMF_Price.get(PMF_Value.get(t.PmfLookupKey__c)) * t.Number_of_Rooms_receiving_BT_Sport__c;
                // Minimum spend on rooms
                if(t.gross_price__c <60){
                    t.gross_price__c =60;
                }
            }
            
            // Hotel Bar = add the value of rooms ( if any) to gross price 
            If (t.Site_Type__c.contains('Hotel Bar')  && t.Number_of_Rooms_receiving_BT_Sport__c != null  && !(t.Site_Type__c.contains('Hotel Bar Only')))  {
                roomsPrice = PMF_Price.get(roomsonly) * integer.valueof(t.Number_of_Rooms_receiving_BT_Sport__c);
                if(roomsPrice <60){
                    roomsPrice =60;
                }
                t.gross_price__c =  t.gross_price__c + roomsPrice; 
            }
            
            // Discounts - Perishable discount  ( now called offer on screen)
            Map<String, Decimal> Discount_Amount = new Map<String, Decimal>();
            Map<String, Decimal> Discount_Period = new Map<String, Decimal>();
            Map<String, String> UnAvailableSiteType = new Map<String, String>();
            Map<String, String> UnAvailableContractType = new Map<String, string>();
            // Map<String, integer> DiscountPeriodMonths = new Map<String, string>();
            // Map<String, Date> DiscountPeriodEndDate = new Map<String, Date>();
            
            List<BT_Sport_Pricing__c> Discounts = [Select Discount_Period_End_Date__c, Discount_Period_Months__c, Discount_Amount__c, Discount_Period__c, Discount_Name__C, UnavailableSiteTypes__c, UnavailableContractTypes__c from BT_Sport_Pricing__c where Entry_Type__C = 'Discount' and Discount_Name__C = :t.Discounts__c and Start_Date__C <= :TodaysDate and End_Date__C >= :vEndDate  ] ;
            system.debug('ADJ Discounts.size() = '+ Discounts.size());
            
            for (BT_Sport_Pricing__c dc :Discounts) {
                Discount_Amount.put(dc.Discount_Name__C, dc.Discount_Amount__c);
                Discount_Period.put(dc.Discount_Name__C, dc.Discount_Period__c);
                UnAvailableSiteType.put(dc.Discount_Name__C, dc.UnavailableSiteTypes__c);
                UnAvailableContractType.put(dc.Discount_Name__C, dc.UnavailableContractTypes__c);
                t.Discount_Period_End_Date__c = null;
                t.Discount_Period_Months__c = null;
                
                t.Discount_Period_End_Date__c = dc.Discount_Period_End_Date__c;
                t.Discount_Period_Months__c = dc.Discount_Period_Months__c;
                
                system.debug('ADJ dc.Discount_Amount__c = '+ dc.Discount_Amount__c);
                system.debug('ADJ dc.Discount_Period__c = '+ dc.Discount_Period__c);
                system.debug('ADJ dc.UnavailableSiteTypes__c = '+ dc.UnavailableSiteTypes__c);
                system.debug('ADJ dc.UnavailableContractTypes__c = '+ dc.UnavailableContractTypes__c);
                system.debug('ADJ dc.Discount_Name__C = '+ dc.Discount_Name__C);
                system.debug('ADJ dc.Discount Amount = '+ Discount_Amount.get(t.Discounts__c));
            }
            
            if (t.Discounts__c != null){                
                if (Discounts.size() < 1){
                    t.Discounts__c.addError('This Discount is not Currently available');
                }                
                else if (UnAvailableContractType.get(t.Discounts__c) == null){
                    t.Discount_Amount__c = t.gross_price__c * Discount_Amount.get(t.Discounts__c)/ 100;
                    system.debug('ADJ t.Discount_Amount__c = '+ t.Discount_Amount__c);
                }
                else if (UnAvailableContractType.get(t.Discounts__c) != null){
                    
                    if (UnAvailableContractType.get(t.Discounts__c).contains(t.Contract_Type__c +'::')){
                        t.Discounts__c.addError('This Discount is not available to this contract type');
                    }   
                    else {
                        t.Discount_Amount__c = t.gross_price__c * Discount_Amount.get(t.Discounts__c)/ 100;
                        system.debug('ADJ t.Discount_Amount__c = '+ t.Discount_Amount__c);
                    }
                }
            }
            //Net Price - Calculate net price
            t.Price_after_Discounts__c = t.gross_price__c - t.Discount_Amount__c ;
            
            //START Calc existing season 2 prices ////////////////////////////////////////////////////////////////////////
            
            if (t.Site_Type__c.MID(0 , 14) == 'BT Sport Total') { 
                t.Site_Type_S2__c = 'BT Sport Pack'+ t.Site_Type__c.MID(14 , (t.Site_Type__c.Length() -14) ) ;
            }
            if (t.Site_Type__c.MID(0 , 10) == 'BT Sport 1') { 
                t.Site_Type_S2__c = 'BT Sport Pack'+ t.Site_Type__c.MID(10 , (t.Site_Type__c.Length() -10) ) ;
            }
            if (t.Site_Type__c.MID(0 , 15) == 'BT Sport Select') { 
                t.Site_Type_S2__c = 'BT Sport Pack'+ t.Site_Type__c.MID(15 , (t.Site_Type__c.Length() -15) ) ;
            }
            // set pmflookup key          
            If( PriceTypeSelect[0].Price_Type__c == '1'){ // ie price based purly on site type
                PMFUnique.add(t.Site_Type_S2__c);
                t.PmfLookupKey__c = var_site_Type;
            }  
            else if( PriceTypeSelect[0].Price_Type__c == '2'){  // price based on site & contract (not bands)
                PMFUnique.add(t.Site_Type_S2__c + var_contract_Type);
                t.PmfLookupKey__c = t.Site_Type_S2__c + var_contract_Type;
                system.debug('Price_Type__c = '+ var_site_Type + var_contract_Type);
                system.debug('ADJ BTS Other t.PmfLookupKey__c = '+ var_contract_Type);   
            }
            else { 
                if  (PriceTypeSelect[0].Price_Type__c == '3RM' && t.Number_of_Rooms_receiving_BT_Sport__c != Null){  //JMM added not null condition to prevent mis banding
                    List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and Price_Band_Start__c <= :t.Number_of_Rooms_receiving_BT_Sport__c and Price_Band_end__c >= :t.Number_of_Rooms_receiving_BT_Sport__c Limit 1 ];
                    t.Band__c = PriceBandSelect[0].Band__c;
                }                
                else if  (PriceTypeSelect[0].Price_Type__c == '3RV' && t.Rateable_Value__c != Null){ //JMM added not null condition to prevent mis banding
                    List<BT_Sport_Pricing__c> PriceBandSelect = [Select Site_Type__c, Band__c from BT_Sport_Pricing__c where Site_Type__c = :var_site_Type and  Price_Band_Start__c <= :t.Rateable_Value__c and Price_Band_end__c >= :t.Rateable_Value__c Limit 1 ];
                    t.Band__c = PriceBandSelect[0].Band__c;
                }                
                PMFUnique.add(t.Site_Type_S2__c + var_contract_Type + t.Band__c);
                t.PmfLookupKey__c = t.Site_Type_S2__c + var_contract_Type + t.Band__c;
                roomsPmfLookupKey = 'BT Sport Pack Hotel Rooms' + var_contract_Type;                
                system.debug('ADJ PMFUnique.add2 = '+ t.Site_Type__c + var_contract_Type + t.Band__c);
                system.debug('ADJ t.PmfLookupKey__c2 = '+ t.PmfLookupKey__c);   
                system.debug('ADJ roomsPmfLookupKey2 = '+ roomsPmfLookupKey);  
            }
            // lookup using  PmfLookupKey__c the correct price entry    
            set<string> PMFcodes2 = new Set<string>();
            Map<String, String> PMF_Value_S2 = new Map<String, String>();
            List<BT_Sport_Pricing__c> pmfCode_S2 = [Select Site_Type__c, PMF_Code__c, PmfLookupKey__c from BT_Sport_Pricing__c where Entry_Type__C = 'PricePoint' and (PmfLookupKey__c in :PMFUnique or PmfLookupKey__c = :roomsPmfLookupKey)];
            for (BT_Sport_Pricing__c pc2 :pmfCode_S2) {
                PMF_Value_S2.put(pc2.PmfLookupKey__c, pc2.PMF_Code__c);
                PMFcodes2.add(pc2.PMF_Code__c);
                if (pc2.PmfLookupKey__c == roomsPmfLookupKey){
                    RoomsOnly = pc2.PMF_Code__c;
                }                
            }
            Map<String, Decimal> PMF_Price_S2 = new Map<String, Decimal>();
            Map<String, Decimal> PMF_Version_S2 = new Map<String, Decimal>();
            
            List<BT_Sport_Pricing__c> pmfPrice_S2 = [Select PMF_Code__c, Price__c, PMF_Version__c from BT_Sport_Pricing__c where Entry_Type__C = 'PMF' and PMF_Code__c in :PMFcodes2 and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate  ] ;
            for (BT_Sport_Pricing__c pp2 :pmfPrice_S2) {
                PMF_Price_S2.put(pp2.PMF_Code__c, pp2.Price__c);
                PMF_Version_S2.put(pp2.PMF_Code__c, pp2.PMF_Version__c);                
            }
            
            t.gross_price_s2__c = PMF_Price_S2.get(PMF_Value_S2.get(t.PmfLookupKey__c));
            t.pmf_version_S2__c = PMF_Version.get(PMF_Value_S2.get(t.PmfLookupKey__c));
            t.Discount_Amount_s2__c = 0;
            t.Discount_Amount_RO__c = 0;
            system.debug('ADJ t.gross_price_S2__c = '+ t.gross_price_s2__c);
            system.debug('ADJ t.pmf_version_S2__c = '+ t.pmf_version_S2__c);
            system.debug('ADJ S2 rooms price = '+ PMF_Price_S2.get(roomsonly));
            //Calculate Hotel Rooms only Price
            If(t.Site_Type_S2__c.contains('Hotel Rooms') && t.Number_of_Rooms_receiving_BT_Sport__c != null){
                t.gross_price_s2__c = PMF_Price_S2.get(PMF_Value_S2.get(t.PmfLookupKey__c)) * t.Number_of_Rooms_receiving_BT_Sport__c;
                if(t.gross_price_s2__c <60){
                    t.gross_price_s2__c =60;
                }
            }
            //BT Sport Pack Hotel Bar = add the value of rooms ( if any) to gross price 
            If (t.Site_Type_S2__c.contains('Hotel Bar') && t.Number_of_Rooms_receiving_BT_Sport__c != null&& !(t.Site_Type_S2__c.contains('Hotel Bar Only')))  {
                // t.gross_price_s2__c =  t.gross_price_s2__c + (PMF_Price_S2.get(roomsonly) * integer.valueof(t.Number_of_Rooms_receiving_BT_Sport__c)); 
                roomsPrice = PMF_Price_S2.get(roomsonly) * integer.valueof(t.Number_of_Rooms_receiving_BT_Sport__c);
                if(roomsPrice <60){
                    roomsPrice =60;
                }
                t.gross_price_S2__c =  t.gross_price_s2__c + roomsPrice;                
            }
            //Loyalty Discounts - Perishable discount  
            Map<String, Decimal> Discount_Amount2 = new Map<String, Decimal>();
            Map<String, Decimal> Discount_Period2 = new Map<String, Decimal>();
            Map<String, String> UnAvailableSiteType2 = new Map<String, String>();
            Map<String, String> UnAvailableContractType2 = new Map<String, string>();
            List<BT_Sport_Pricing__c> Discounts2 = [Select Discount_Period_End_Date__c, Discount_Period_Months__c,Discount_Amount__c, Discount_Period__c, Discount_Name__C, UnavailableSiteTypes__c, UnavailableContractTypes__c from BT_Sport_Pricing__c where Entry_Type__C = 'Discount' and Discount_Name__C = :t.Discounts_S2__c and Start_Date__C <= :TodaysDate and End_Date__C >= :TodaysDate  ] ;
            system.debug('ADJ Discounts2.size() = '+ Discounts2.size());
            
            for (BT_Sport_Pricing__c dc2 :Discounts2) {
                Discount_Amount2.put(dc2.Discount_Name__C, dc2.Discount_Amount__c);
                Discount_Period2.put(dc2.Discount_Name__C, dc2.Discount_Period__c);
                UnAvailableSiteType2.put(dc2.Discount_Name__C, dc2.UnavailableSiteTypes__c);
                UnAvailableContractType2.put(dc2.Discount_Name__C, dc2.UnavailableContractTypes__c);
                t.Discount_Period_End_Date_S2__c = null;
                t.Discount_Period_Months_S2__c = null;
                
                t.Discount_Period_End_Date_S2__c = dc2.Discount_Period_End_Date__c;
                t.Discount_Period_Months_S2__c = dc2.Discount_Period_Months__c;
                system.debug('ADJ dc.Discount_Amount__c2 = '+ dc2.Discount_Amount__c);
                system.debug('ADJ dc.Discount_Period__c2 = '+ dc2.Discount_Period__c);
                system.debug('ADJ dc.UnavailableSiteTypes__c2 = '+ dc2.UnavailableSiteTypes__c);
                system.debug('ADJ dc.UnavailableContractTypes__c2 = '+ dc2.UnavailableContractTypes__c);
                system.debug('ADJ dc.Discount_Name__C2 = '+ dc2.Discount_Name__C);
                system.debug('ADJ dc.Discount Amount2 = '+ Discount_Amount2.get(t.Discounts_S2__c));
            }
            
            if (t.Discounts_S2__c != null){                
                if (Discounts2.size() < 1){
                    t.Discounts_S2__c.addError('This Discount is not Currently available');
                }                
                else if (UnAvailableContractType2.get(t.Discounts__c) == null){
                    if(t.gross_price_S2__c != null && t.Discount_Amount_S2__c!= null)
                    	t.Discount_Amount_S2__c = t.gross_price_S2__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;
                    t.Discount_Amount_RO__c = t.gross_price__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;
                    system.debug('ADJ t.Discount_Amount__c2 = '+ t.Discount_Amount_S2__c);
                }
                else if (UnAvailableContractType2.get(t.Discounts_S2__c) != null){                    
                    if (UnAvailableContractType2.get(t.Discounts_S2__c).contains(t.Contract_Type__c +'::')){
                        t.Discounts_S2__c.addError('This Discount is not available to this contract type');
                    }   
                    else {
                        t.Discount_Amount_S2__c = t.gross_price_s2__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;
                        t.Discount_Amount_RO__c = t.gross_price__c * Discount_Amount2.get(t.Discounts_S2__c)/ 100;
                        system.debug('ADJ t.Discount_Amount_s2__c = '+ t.Discount_Amount_s2__c);
                    }
                }
            }
            //Net Price - Calculate net price
            if(t.gross_price_S2__c != null && t.Discount_Amount_S2__c!= null)
                t.Price_after_Discounts_S2__c = t.gross_price_S2__c - t.Discount_Amount_S2__c ;
            
            t.Price_after_Discounts_RO__c = t.gross_price__c - t.Discount_Amount_RO__c ;
            
            //END Interim Season 2 additions
            
            
        }
    }
}