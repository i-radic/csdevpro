trigger BTLBSuiteOrderLineBefore on BTLB_Suite_Order_Lines__c (before Insert, before Update, before Delete) {
	Set<String> prods = New Set<String>();
	if(trigger.IsInsert || trigger.isUpdate){
		for(BTLB_Suite_Order_Lines__c t:Trigger.New){
			prods.add(t.Product_Description__c);
		}

		List<BTLB_Suite_Rate_Card__c > rateCards = [SELECT Cobra_Report__c,Commission__c,Commission_Type__c,Product_Description__c ,Product_Area__c,Product_Group_1__c,Product_Group_2__c,
			Schedule_5_Non_Schedule_5__c, Uplift__c
			FROM BTLB_Suite_Rate_Card__c WHERE Product_Description__c = :prods];
		Decimal multiplier = 1;
		for(BTLB_Suite_Order_Lines__c t:Trigger.New){
			//reset to null
			t.Cobra_Report__c = null;
			t.Uplift_Used__c = 'None';
			t.Indicative_Commission_Rate__c = null;
			t.Commission_Type__c = null;
			t.Product_Area__c = null;
			t.Product_Group_1__c = null;
			t.Product_Group_2__c = null;
			t.Schedule_5_Non_Schedule_5__c = null;
			for(BTLB_Suite_Rate_Card__c r:rateCards ){
				if(t.Product_Description__c == r.Product_Description__c){
					t.Cobra_Report__c = r.Cobra_Report__c;
					if(t.zBusCat__c == '1819 Acquisition' && r.Uplift__c > 0){
						multiplier += (r.Uplift__c/100);
						t.Uplift_Used__c = r.Uplift__c+'% '+t.zBusCat__c;
					}
					t.Indicative_Commission_Rate__c = r.Commission__c*multiplier;
					t.Commission_Type__c = r.Commission_Type__c;
					t.Product_Area__c = r.Product_Area__c;
					t.Product_Group_1__c = r.Product_Group_1__c;
					t.Product_Group_2__c = r.Product_Group_2__c;
					t.Schedule_5_Non_Schedule_5__c = r.Schedule_5_Non_Schedule_5__c;
				}
			}
		}
	}
	if(trigger.IsDelete){
		String profileCheck = userinfo.getProfileId();
		if((profileCheck != '00e20000001MX7zAAG' && profileCheck != '00e200000015EhzAAE') || Test.isRunningTest()){
			for (BTLB_Suite_Order_Lines__c delT : Trigger.old) {
				if(delT.Order_Line_Type__c == 'BT Entered' || delT.Order_Line_Type__c == 'Cobra Entered'){
					if(!Test.isRunningTest()){
						delT.addError('You cannot delete a BT or Cobra entered record');
					}
				}
			}
		}
	}
}