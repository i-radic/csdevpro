trigger CRF_AutoNumber on CRF__c (before insert, before update, before delete) {
    
    if(Trigger.isInsert){
    CRF__c[] CRF=Trigger.New;
    Integer a=10000;
    Integer b=0;
    Id recType=CRF[0].RecordTypeId;
    Id oppId=CRF[0].Opportunity__c;
        
    List<Recordtype> recList=[Select Name,DeveloperName from RecordType where Id =:recType];
    if(recList.size()>0){    
    if(recList[0].DeveloperName =='Movers' || recList[0].DeveloperName =='Mover_with_existing_BB' || recList[0].DeveloperName =='Business_Mover'){        
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Movers' limit 1]; 
        if(CRFList.size()>0){
            String MaxNumber = CRFList[0].CRF_Number__c;
            Integer SplitCRFNumber = Integer.valueOf(Maxnumber.substring(1,6));
            SplitCRFNumber++; 
            a=SplitCRFNumber;
            if(a <= 85000){
                CRF[0].Name='B'+a;      
                //CRF[0].Record_Type__c=recList[0].DeveloperName; 
                CRFList[0].CRF_Number__c=CRF[0].Name;
                update CRFList[0];     
                }
            if(a > 85000){//Reseting a to 10000 after it croses 85000
                a = 10000;            
                a = a + (SplitCRFNumber+1) - 85000;
                CRF[0].Name='B'+a;
                //CRF[0].Record_Type__c=recList[0].DeveloperName;
                CRFList[0].CRF_Number__c=CRF[0].Name;
                update CRFList[0];
                }
        }       
        if(CRFList.size()==0)
        CRF[0].Name='B10001';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;    
    }
    
    if(recList[0].DeveloperName =='One_Plan_and_Broadband' || recList[0].DeveloperName =='Broadband' || recList[0].DeveloperName =='One_Plan'){      
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Broadband' limit 1]; 
        if(CRFList.size()>0){              
        String CRFName = CRFList[0].CRF_Number__c;                    
        List<String> parts = CRFName.split(' ');
        Integer CRFNameSplit = Integer.valueOf(parts[2]);
        CRFNameSplit++;                        
        CRF[0].Name='CRF - '+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;   
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];                     
        }
        if(CRFList.size()==0)
        CRF[0].Name='CRF - 1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;

    }
    if(recList[0].DeveloperName =='NSO_BS'){
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'NSO_BS' limit 1];        
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split(' ');
        Integer CRFNameSplit = Integer.valueOf(parts[2]);
        CRFNameSplit++;
        //b=b+CRFList.size()+1;
        CRF[0].Name='NSO - '+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='NSO - 1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }
    
    if(recList[0].DeveloperName =='Direct_Debit_CRF'){
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Direct_Debit_CRF' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split(' ');
        Integer CRFNameSplit = Integer.valueOf(parts[2]);
        CRFNameSplit++;
        CRF[0].Name='DD - '+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='DD - 1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }
    if(recList[0].DeveloperName =='AX_Form'){       
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'AX_Form' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='AX-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='AX-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }

    if(recList[0].DeveloperName =='Number_Port'){       
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Number_Port' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='NP-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='NP-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }
    if(recList[0].DeveloperName =='WLR3_Authorisation'){       
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'WLR3_Authorisation' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='WLRauth-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='WLRauth-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }
    if(recList[0].DeveloperName =='Resign'){        
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Resign' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='Resign-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='Resign-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }
        if(recList[0].DeveloperName =='Name_Change_CRF'){        
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Name_Change_CRF' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='Name Change-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='Name Change-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }
    
    if(recList[0].DeveloperName =='ISDN30'){        
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'ISDN30' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='ISDN30-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='ISDN-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        
    }
    
    if(recList[0].DeveloperName =='NCP'){        
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'NCP' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='NCP-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='NCP-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;  
    }
    
    if(recList[0].DeveloperName =='Business_Complete'){        
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Business_Complete' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='BC-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='BC-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;  
    }
        
    if(recList[0].DeveloperName =='Offer_Exception'){        
        List<CRFAutoNumber__c> CRFList= [Select CRF_Number__c from CRFAutoNumber__c where Record_Type__c=:'Offer_Exception' limit 1];
        if(CRFList.size()>0){
        String CRFName = CRFList[0].CRF_Number__c;
        List<String> parts = CRFName.split('-');
        Integer CRFNameSplit = Integer.valueOf(parts[1]);
        CRFNameSplit++;
        CRF[0].Name='OE-'+CRFNameSplit;
        //CRF[0].Record_Type__c=recList[0].DeveloperName;
        CRFList[0].CRF_Number__c=CRF[0].Name;
        update CRFList[0];
        }
        if(CRFList.size()==0)
        CRF[0].Name='OE-1';
        //CRF[0].Record_Type__c=recList[0].DeveloperName;  
    }
        
        
    }

    }//End of Insert
    
    if(Trigger.isUpdate){
    CRF__c[] CRFOld=Trigger.Old; 
    CRF__c[] CRFNew=Trigger.New;
    CRFNew[0].Name = CRFOld[0].Name;   
    
    //Backoffice team of Noida(identified by OUCs) shouldn't have permission to edit records when in Res to Bus status 
    Id RTId = CRFOld[0].RecordTypeId;
    String RTName=[Select DeveloperName from RecordType where Id =:RTId limit 1].DeveloperName;
    
    if(RTName=='NSO_BS'){    
        Id UserId = UserInfo.getUserId();
        String sBreak = '</br>';
        List<User> U = [Select Id,OUC__c from User where Id=:UserId limit 1];
        if(U.size()>0){
            if(U[0].OUC__c!=null){                     
                if(CRFOld[0].Back_Office_Status__c=='Pending Res to Bus' && (U[0].OUC__c=='M7AF21' || U[0].OUC__c=='M7AF22' || U[0].OUC__c=='M7AF23' || U[0].OUC__c=='M7AF24' || U[0].OUC__c=='M7AF25' || U[0].OUC__c=='M7AF26' || U[0].OUC__c=='M7AF27' || U[0].OUC__c=='M7AF14' || U[0].OUC__c=='M7AF31' || U[0].OUC__c=='M7AF34' || U[0].OUC__c=='M7AF3A' || U[0].OUC__c=='M7AF3B' || U[0].OUC__c=='M7AF11' || U[0].OUC__c=='M7AF28' || U[0].OUC__c=='M7AF41' || U[0].OUC__c=='M7AF45' || U[0].OUC__c=='M7AF47')){
                CRFNew[0].addError('****DO NOT ISSUE THIS ORDER UNTIL ORDER SHOWS A STATUS OF CREATED****'+sBreak+'***The order is currently sat with the Res 2 Bus team for validation. Before you issue the order in the back office wait for the order status to show CREATED***');
                }
            }
        }
    }
    

       
    }
    
    // Canterbury team should not have the permission to delete NSO CRF
    if(Trigger.isDelete){
    
    CRF__c[] CRFOld=Trigger.Old;
    Id RTId = CRFOld[0].RecordTypeId;
    String RTName=[Select DeveloperName from RecordType where Id =:RTId limit 1].DeveloperName;
    
    if(RTName=='NSO_BS'){    
        Id UserId = UserInfo.getUserId();
        List<User> U = [Select EIN__c,Manager_EIN__c from User where Id=:UserId limit 1];
        if(U.size()>0){
            String ManagerEIN = U[0].Manager_EIN__c;
            String UserEIN = U[0].EIN__c;
            if(ManagerEIN=='803181357'||UserEIN=='803181357'){
                CRFOld[0].addError('You do not have permission to delete this CRF');
            }
        }
    }
       
    }
}