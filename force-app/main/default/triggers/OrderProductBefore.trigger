trigger OrderProductBefore on Order_Product__C (before insert,before update) {
    Set<String> prodId = New Set<String>();
    Set<String> siteId = New Set<String>();
    Boolean finOption = false;
    for(Order_Product__c c:Trigger.New){
        prodId.add(c.SFCSSProdId__c);
        siteId.add(c.Order_Site__c);
    }
    
    List<CSS_Products__c > sProd = [Select Id, Name, Unit__c, Type__c, Product_Name__c, Unit_Cost__c, Discountable__c, A_Code__c, ShopLink__c,Connection_Charge__c,
										Sort_Order__c,Finance_Option__c,DiscountStringI__c,DiscountStringR__c,Term__c,Ports__c, Grouping__c,Sub_Grouping__c, 
										Total_Care_pm__c,Critical_Care_pm__c,Prompt_Care_pm__c,Standard_Care_pm__c,Power__c,Term_Alternative__c,Line_Required__c,
										Term_Alternative_12__c, Term_Alternative_24__c, Term_Alternative_36__c, Term_Alternative_48__c, Term_Alternative_60__c,
										Required_Product__c, Limit_Auto_Add_to_One__c, Related_Bracket_Single__c, Related_Bracket_Multi__c, Related_PSU_Single__c, Related_PSU_Multi__c, Related_Care__c
										FROM CSS_Products__c WHERE Id = :prodId AND Active__c = True];
    List<Order_Site__c > site = [Select Id, Service_Site__c, Order_Header__r.Finance__c FROM Order_Site__c WHERE Id = :siteId];

    for(CSS_Products__c sP:sProd ){
        for(Order_Product__c sfoProd:Trigger.New ){
			for(Order_Site__c s:site ){
				finOption = false;
				if(s.Id == sfoProd.Order_Site__c){
					sfoProd.zServiceOnly__c = s.Service_Site__c;
					finOption = s.Order_Header__r.Finance__c;
				}
				if(sfoProd.SFCSSPRodId__c == sP.ID){
					sfoProd.Type__c = sP.Type__c;					
					if(sP.Unit_Cost__c > 0){
						sfoProd.Unit_Cost__c = sP.Unit_Cost__c*qbSiteCTRL.priceTerm(sP.Unit__c);
					}	
					//if default value is zero but new val is not add it
					if(sfoProd.Connection_Charge__c > 0 && (sP.Connection_Charge__c==0 || sP.Connection_Charge__c==null)){
					}else{
						sfoProd.Connection_Charge__c = sP.Connection_Charge__c;
					}
					sfoProd.A_Code__c = sP.A_Code__c;
					sfoProd.Product_Name__c = sP.Name;
					sfoProd.Image__c = sP.ShopLink__c;
					sfoProd.Sort_Order__c = sP.Sort_Order__c;
					sfoProd.Grouping__c = sP.Grouping__c;
					sfoProd.Sub_Grouping__c = sP.Sub_Grouping__c;
					sfoProd.Term__c = sP.Term__c;
					if(sP.Ports__c > 0){
						sfoProd.Ports__c = sP.Ports__c * sfoProd.Quantity__c;
					}
					sfoProd.Finance_Option__c = sP.Finance_Option__c;
					if(trigger.isInsert){
						if(finOption && sP.Finance_Option__c && sfoProd.Purchasing_Option__c <> 'Not Financed'){
							sfoProd.Purchasing_Option__c = 'Finance';
						}else{
							sfoProd.Purchasing_Option__c = 'Not Financed';
						}
					}
					sfoProd.RequiredProdID__c = sP.Required_Product__c;
					sfoProd.Limit_Auto_Add_to_One__c = sP.Limit_Auto_Add_to_One__c;
					sfoProd.Term_Alternative__c = sP.Term_Alternative__c;
					sfoProd.Line_Required__c = sP.Line_Required__c;

					sfoProd.Related_Bracket_Single__c = sP.Related_Bracket_Single__c;
					sfoProd.Related_Bracket_Multi__c = sP.Related_Bracket_Multi__c;
					sfoProd.Related_PSU_Single__c = sP.Related_PSU_Single__c;
					sfoProd.Related_PSU_Multi__c = sP.Related_PSU_Multi__c;
					sfoProd.Related_Care__c = sP.Related_Care__c;
					sfoProd.Term_Alternative_12__c = sP.Term_Alternative_12__c;
					sfoProd.Term_Alternative_24__c = sP.Term_Alternative_24__c;
					sfoProd.Term_Alternative_36__c = sP.Term_Alternative_36__c;
					sfoProd.Term_Alternative_48__c = sP.Term_Alternative_48__c;
					sfoProd.Term_Alternative_60__c = sP.Term_Alternative_60__c;

					sfoProd.Care_Cost__c = 0;
					if(sfoProd.Care__c == null){ 
						sfoProd.Care__c = 'no care';
						sfoProd.Care_Cost__c = 0;
					}
					else if(sfoProd.Care__c.toLowerCase() == 'no care'){
						sfoProd.Care_Cost__c = 0;
					}else if(sfoProd.Care__c.toLowerCase() == 'standard'){					
						if(sP.Standard_Care_pm__c != null && sP.Unit__c!= null)
							sfoProd.Care_Cost__c = sP.Standard_Care_pm__c*qbSiteCTRL.priceTerm(sP.Unit__c); 
						
					}else if(sfoProd.Care__c.toLowerCase() == 'prompt'){
						if(sP.Prompt_Care_pm__c != null && sP.Unit__c!= null)
							sfoProd.Care_Cost__c = sP.Prompt_Care_pm__c*qbSiteCTRL.priceTerm(sP.Unit__c); 						
						
					}else if(sfoProd.Care__c.toLowerCase() == 'total'){
						if(sP.Total_Care_pm__c != null && sP.Unit__c!= null)
							sfoProd.Care_Cost__c = sP.Total_Care_pm__c*qbSiteCTRL.priceTerm(sP.Unit__c);						
						
					}else if(sfoProd.Care__c.toLowerCase() == 'critical'){
						if(sP.Critical_Care_pm__c != null && sP.Unit__c!= null)
							sfoProd.Care_Cost__c = sP.Critical_Care_pm__c*qbSiteCTRL.priceTerm(sP.Unit__c);
					}
					if(sfoProd.Care__c == 'btnet1'){
						sfoProd.Care__c = 'No Care';
						sfoProd.Information__c = 'Primary'; 
					} 
					if(sfoProd.Care__c == 'btnet2'){
						sfoProd.Care__c = 'No Care';
						sfoProd.Information__c = 'Secondary'; 
					} 
				}
			}
        }
    }  
}