trigger SetAgreementStatusesOnTrial on echosign_dev1__SIGN_Agreement__c (after update) 
{
  Map<Id,echosign_dev1__SIGN_Agreement__c> trialsMap = new Map<Id,echosign_dev1__SIGN_Agreement__c>();
  List<Trial__c> trialsToUpdate = new List<Trial__c>();
  
  //Build Opportunity Map for insert / update (only if status changes)
  for( echosign_dev1__SIGN_Agreement__c agreement : Trigger.new ) {
    if( Trigger.isInsert || (Trigger.isUpdate && 
      ( Trigger.oldMap.get(agreement.Id).echosign_dev1__Status__c != agreement.echosign_dev1__Status__c ))) {
    
      if(agreement.Trial__c != null ) {
        trialsMap.put(agreement.Trial__c, agreement);
      }
    }  
  }
  
  if(trialsMap.keySet().size() == 0 ) {
    return;
  }
  
  for(ID trialid : trialsMap.keySet()) 
  {
    echosign_dev1__SIGN_Agreement__c agreement = trialsMap.get( trialid );
    Trial__c trial = new Trial__c(id=trialid);
    
    if(agreement.echosign_dev1__Status__c == 'Out for Signature') 
      trial.Status__c = agreement.echosign_dev1__Status__c;
    else if(agreement.echosign_dev1__Status__c == 'Signed')
      trial.Status__c = agreement.echosign_dev1__Status__c;
    trialsToUpdate.add(trial);    
  }  
  
  if (trialsToUpdate.size() > 0)
    update trialsToUpdate;
}