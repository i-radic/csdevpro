trigger tgrAccountAfter on Account(after insert, after update){
  //
  //  Date            Author          Comment
  //  22 Aug 2010     Alan Jackson    Added code to run only if Apex_Trigger_Account__c is true on user object
  //  29 Aug 2010     Alan Jackson    Added check for BTLB ie a.Sector__c == 'BT Local Business'
  //  09 Sep 2010     Alan Jackson    Move & merge functionality enabled
  //  16 Sep 2010     Jon Brayshaw    Added check to see if this has run before - this will now only run once per thread
  //  28 Sep 2010     Alan Jackson    added static variable to prevent trigger running on Contact upload
  //  11/11/10        John McGovern   Commented out Stat Var ref as this is covered in Contact tgr
  //  21 Jul 2010     Alan Jackson    update LesToDelete list to limit to 1000 rows
  //  09 Apr 2012     Alan Jackson    updated ***Merge Corp Accounts*** to only invoke on Corp accounts
  //  26 Oct 2012     Alan Jackson    only incluse BT Local Business in trigger around line 53/54
    //  26 Oct 2012     Alan Jackson  disabled account merge on duplicate Corp Sacs - line 100

    if(TriggerDeactivating__c.getInstance().Account__c) {
         System.debug('Bypassing trigger due to custom setting');
         return;
     } 

   if (StaticVariables.getAccountAfterDontRun()){
    return ;
  }
  StaticVariables.setAccountAfterDontRun(true);
  if (StaticVariables.getContactIsRunBefore()){
    return ;
  }

  // Get Marketing SACs details 
  set < string > SACList = new Set < string > (); //get all associated SACs
  for (Account a: Trigger.new){
    SACList.add(a.SAC_code__c);
  }
  //create map to hold Those SAC that are Marketing SACs
  set < string > BTLB_MarketSac = new Set < string > ();
  // read in values
  List < BTLB_Market_SAC__c > MarketDetails = [Select SAC_Code__C from BTLB_Market_SAC__c where SAC_Code__C in :SACList];
  for (BTLB_Market_SAC__c m: MarketDetails){
    BTLB_MarketSac.add(m.SAC_Code__C);
  }

  // delete the associated LE records so record can point to new master object
  set < string > LeList = new Set < string > (); //get all associated LEs
  for (Account a: Trigger.new){
      if(a.Sector__c == 'BT Local Business') {  
        LeList.add(a.le_code__c);
      }
  }
  system.debug('666666666' + LeList);
 List < Associated_LEs__c > LesToDelete = [select id, le_code__c from Associated_LEs__c where le_code__c in :LeList and le_code__c != null limit 1000];
 if (LesToDelete.size() > 0){
   //Code Commented to Fix SME LE Delete Issue  
   //delete LesToDelete;
 }


  List < Associated_LEs__c > newLE = new List < Associated_LEs__c > (); // create list to populate LE details if owning SAC
  for (Account a: Trigger.new){

    // IF BTLB Owning SACs
    if (BTLB_MarketSac.contains(a.SAC_Code__c) != TRUE && a.le_Code__c != null && a.le_Code__c != ''&& a.Sector__c == 'BT Local Business') {
    //if (BTLB_MarketSac.contains(a.SAC_Code__c) != TRUE && a.le_Code__c != null && a.le_Code__c != ''){
      Associated_LEs__c le = new Associated_LEs__c();
      le.name = a.le_name__c;
      le.le_code__c = a.LE_Code__c;
      le.account__c = a.ID;
      le.Traiding_As__c = a.Trading_As__c;
      le.Turnover__c = a.turnover__c;
      le.Employees__c = a.NumberOfEmployees;
      le.aic__c = a.aic__c;
      newLE.add(le);
      system.debug('88888888888' + newLE);
    }
  }

  //upsert le associated records in in batch ( rather than one by one which breaks Govenor Limits) 
  system.debug('999999999999' + newLE.size());
  if (newLE.size() > 0){
    //Code Commented to Fix Duplicate LE Creation Issue
    //insert newLE ;
  }

//***Merge Corp Accounts***

// Set up the map to call the @ future merge class - primarly to merge duplicate Corp SACS
Map<ID, Id> MergesById = New Map<Id, Id>{};
  
// create maps to hold corp sac details    
Map<String, String> Corp_CUG = new Map<String, String>();
Map<String, String> Corp_Account_Referenced = new Map<String, String>();
// read in values
List<BTB_Corporate_Account__c> CorpAccDetails = [Select id, CUG__c, Sac_Code__c, Account_Referenced__c from BTB_Corporate_Account__c where SAC_Code__c in :SACList];
if(CorpAccDetails.size() > 0) {
for (BTB_Corporate_Account__c sl :CorpAccDetails) {
      Corp_CUG.put(sl.Sac_Code__c, sl.CUG__c);
      Corp_Account_Referenced.put(sl.Sac_Code__c, sl.Account_Referenced__c);   
}      
}
Map<Id, Account> MedalliaAccountValueMap = new Map<Id, Account>();
for (Account a:Trigger.new) {
    MedalliaAccountValueMap.put(a.Id, a);
  if (CorpAccDetails.size() > 0 && a.Sector__c == 'Corporate' && a.CUG__c != null && Corp_Account_Referenced.containsKey(a.Sac_Code__c) && a.CUG__c != Corp_CUG.get(a.Sac_Code__c)){ // only attempt to deDup Corp accounts & if its in Corp Opject- added 01Apr2012
    //system.debug('a.CUG__c 1 ' + a.CUG__c);
    //system.debug('Corp_CUG  1 ' + Corp_CUG.get(a.Sac_Code__c));
    //system.debug('Corp_Account_Referenced 1 ' + Corp_Account_Referenced.get(a.Sac_Code__c));
    MergesById.put( a.Id, Corp_Account_Referenced.get(a.Sac_Code__c));
    //system.debug('MergesById  2' + MergesById.get(a.Id));
    }
  }

//if(MergesById.size() > 0)AccountUpdateMerge.mergeAccountsByIds(MergesById); //disabled due to causing error in campaign load

//Medallia Update
  
    if(Trigger.isUpdate){
        
        Set<Id> SurveyResponseRecordIdSet = new Set<Id>();
        Set<Id> CustomerPlanRecordIdSet = new Set<Id>();
        List<Service_Improvement_Plan__c> ClientPlanList = new List<Service_Improvement_Plan__c>();
        List<CSAT_Contact_Feedback__c> SurveyResponseList = new List<CSAT_Contact_Feedback__c>();
        List<Customer_Experience_Actions__c> ExperienceActionList = new List<Customer_Experience_Actions__c>();
        List<Account> AccountList = new List<Account>([Select Id, General_Manager__c, General_Manager__r.Email,Owner.Email,Owner.Manager.Email,OwnerId,
                                                       SRM_Managers__c,SRM_Managers__r.Email,Business_Managers__c,Business_Managers__r.Email,Owner.ManagerId,
                                                       Sales_Manager_Primary__c,Sales_Manager_Primary__r.Email,DBAM_User__c,DBAM_User__r.Email,
                                                       (Select Id from CSAT_Contact_Feedback__r order by CreatedDate desc limit 1),
                                                       (Select Id from Service_Improvement_Plans__r order by CreatedDate desc limit 1) 
                                                       from Account where Id IN : Trigger.New and (sector__c='Corporate' or sector__c='Major and Public Sector')]);

        for(Account Acc : AccountList){
            if((Trigger.NewMap.get(Acc.Id).General_Manager__c != Trigger.OldMap.get(Acc.Id).General_Manager__c) || 
                 (Trigger.NewMap.get(Acc.Id).OwnerId != Trigger.OldMap.get(Acc.Id).OwnerId) ||
                 (Trigger.NewMap.get(Acc.Id).Owner.Managerid != Trigger.OldMap.get(Acc.Id).Owner.Managerid) ||
                 (Trigger.NewMap.get(Acc.Id).SRM_Managers__c != Trigger.OldMap.get(Acc.Id).SRM_Managers__c) ||
                 (Trigger.NewMap.get(Acc.Id).Business_Managers__c != Trigger.OldMap.get(Acc.Id).Business_Managers__c) ||
                 (Trigger.NewMap.get(Acc.Id).Sales_Manager_Primary__c != Trigger.OldMap.get(Acc.Id).Sales_Manager_Primary__c) ||
                 (Trigger.NewMap.get(Acc.Id).DBAM_User__c != Trigger.OldMap.get(Acc.Id).DBAM_User__c)){
                 
                 system.debug('************ Was not working'+Acc.Owner.id);
            
                if(Acc.Service_Improvement_Plans__r.size()>0)   
                    CustomerPlanRecordIdSet.add(Acc.Service_Improvement_Plans__r[0].Id);
                if(Acc.CSAT_Contact_Feedback__r.size()>0)   
                    SurveyResponseRecordIdSet.add(Acc.CSAT_Contact_Feedback__r[0].Id);
           }
        }
        
        /****************************** Survey Response Updation Initialisation Starts ********************************************************/
        If(SurveyResponseRecordIdSet.size() > 0){

            for(CSAT_Contact_Feedback__c CSATContactFeedBack : [Select Id, Account__c, Contact_Account_General_Manager_Email__c, Contact_Account_Owner_Email__c, Contact_Account_SRM_Manager_Email__c,Contact_Account_Business_Manager_Email__c, Contact_Account_Sales_Manager_Email__c, DBAM_Email__c from CSAT_Contact_Feedback__c where Id IN : SurveyResponseRecordIdSet]){
                if(MedalliaAccountValueMap.containsKey(CSATContactFeedBack.Account__c)){
                    CSATContactFeedBack.Contact_Account_General_Manager_Email__c = MedalliaAccountValueMap.get(CSATContactFeedBack.Account__c).General_Manager__r.Email;
                    SurveyResponseList.add(CSATContactFeedBack);
                }
            }
            
        }
        
        /****************************** Survey Response Updation Initialisation Ends  ********************************************************/
        
         /****************************** Client Plan Updation Initialisation Starts ********************************************************/
        system.debug('*****CustomerPlanRecordIdSet'+CustomerPlanRecordIdSet);
        If(CustomerPlanRecordIdSet.size() > 0){
          
            Set<Id> ClientPlanId = new Set<Id>();
            for(Service_Improvement_Plan__c ClientPlanRecord : [Select Id, Account__c, General_Manager_Email__c,SRM_Manager_Email__c,
                                                                Business_Manager_Email__c,Account_Owner_Manager_Email__c,Sales_Manager_Email__c,
                                                                DBAM_Email__c
                                                                from Service_Improvement_Plan__c where Id IN : CustomerPlanRecordIdSet]){
                if(MedalliaAccountValueMap.containsKey(ClientPlanRecord.Account__c)){
                    ClientPlanRecord.General_Manager_Email__c = MedalliaAccountValueMap.get(ClientPlanRecord.Account__c).General_Manager__r.Email;
                    ClientPlanRecord.SRM_Manager_Email__c = MedalliaAccountValueMap.get(ClientPlanRecord.Account__c).SRM_Managers__r.Email;
                    ClientPlanRecord.Business_Manager_Email__c = MedalliaAccountValueMap.get(ClientPlanRecord.Account__c).Business_Managers__r.Email;
                    ClientPlanRecord.Account_Owner_Manager_Email__c = MedalliaAccountValueMap.get(ClientPlanRecord.Account__c).Owner.Manager.Email;
                    ClientPlanRecord.DBAM_Email__c = MedalliaAccountValueMap.get(ClientPlanRecord.Account__c).DBAM_User__r.Email;
                    ClientPlanRecord.Sales_Manager_Email__c = MedalliaAccountValueMap.get(ClientPlanRecord.Account__c).Sales_Manager_Primary__r.Email;
                    ClientPlanRecord.Account_Owner_Email__c = MedalliaAccountValueMap.get(ClientPlanRecord.Account__c).Owner.Email;
                    ClientPlanList.add(ClientPlanRecord);
                    ClientPlanId.add(ClientPlanRecord.Id);
                }                
            }
            /****************************** Client Plan Updation Initialisation Ends  ********************************************************/
            
            /****************************** Experience Action Updation Initialisation Starts ********************************************************/
            
            
            for(Customer_Experience_Actions__c ExperienceActionRec : [Select Customer_Experience_Client_Plan__r.Account__c, General_Manager_Email__c from Customer_Experience_Actions__c where Customer_Experience_Client_Plan__c IN : ClientPlanId order by CreatedDate desc]){
                if(MedalliaAccountValueMap.containsKey(ExperienceActionRec.Customer_Experience_Client_Plan__r.Account__c)){
                    ExperienceActionRec.General_Manager_Email__c = MedalliaAccountValueMap.get(ExperienceActionRec.Customer_Experience_Client_Plan__r.Account__c).General_Manager__r.Email;
                    ExperienceActionList.add(ExperienceActionRec);
                }
            }
            
            /****************************** Experience Action Updation Initialisation Ends  ********************************************************/
        }
        system.debug('*****ClientPlanList'+ClientPlanList); 
        //Survey Response Update
        if(SurveyResponseList.size() > 0)
            Update SurveyResponseList;
        
        //Client Plan update
        if(ClientPlanList.size() > 0)
            Update ClientPlanList;
        
        //Experience Action Update
        if(ExperienceActionList.size() > 0)
            Update ExperienceActionList;
    }
}