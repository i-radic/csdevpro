trigger tgrRecalcICTDueDates on ICT_Sales_Journey__c (before update) {
/**********************************************************************
	 Name:  tgrRecalcICTDueDates.trigger()
	 Copyright © 2010  BT.
	 ======================================================
	======================================================
	Purpose:                                                            
	-------       
	
	This Trigger is used to recalculate the due date on a Sales Journey record.
	It uses a Custom Setting which defines the Standard Sales Timeline
	It uses this Custom Setting to calculate the due dates for each of the milestones
	in the Sales process. 
	It also includes addition features such as only changing due dates > the changed due date.
	It also uses the dateholidayHandler class to avoid booking a due date on a weeken or any of the hoidays defined
	under the company holidays.
	
	======================================================
	======================================================
	History                                                            
	-------                                                            
	VERSION  AUTHOR            DATE              DETAIL                                 FEATURES
	1.0 -    Greg Scarrott    22/02/2010        INITIAL DEVELOPMENT       				Initial Build: 
	
	***********************************************************************/

	//Trigger variables
	list<ict_sales_journey__c> listictsj = new list<ict_sales_journey__c>();
	Integer daysbetween = 0;
	Boolean bdatesback = false;
	
	for(ict_sales_journey__c ictsj : trigger.new){
		
		if(ictsj.Lead_Due_Date__c != trigger.oldmap.get(ictsj.id).Lead_Due_Date__c){
			//lead date has changed. move all other dates on by the same difference.
			if(trigger.oldmap.get(ictsj.id).Lead_Due_Date__c !=null){
				//daysbetween =trigger.oldmap.get(ictsj.id).Lead_Due_Date__c.daysbetween(ictsj.Lead_Due_Date__c);
				
				If(trigger.oldmap.get(ictsj.id).Lead_Due_Date__c > ictsj.Lead_Due_Date__c){
					daysbetween= dateHolidayHandler.getbusinessdays(ictsj.Lead_Due_Date__c,trigger.oldmap.get(ictsj.id).Lead_Due_Date__c);	
					bdatesback = true;
				}else{
					daysbetween= dateHolidayHandler.getbusinessdays(trigger.oldmap.get(ictsj.id).Lead_Due_Date__c,ictsj.Lead_Due_Date__c);
				}
				
			}
			if (daysbetween>0){
				//This call checks that the user has not changed a date to a holiday or weekend
				ictsj.lead_Due_Date__c = dateHolidayHandler.isdaybusinessday(ictsj.lead_Due_Date__c,bdatesback);
				
				//Add the difference to all the other dates
				ictsj.qualify_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.qualify_Due_Date__c,daysbetween,bdatesback);
				ictsj.sell_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.sell_Due_Date__c,daysbetween,bdatesback);
				ictsj.propose_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.propose_Due_Date__c,daysbetween,bdatesback);
				ictsj.closure_Due_Date__c = dateHolidayHandler.adddayswithholidays(ictsj.closure_Due_Date__c,daysbetween,bdatesback);
			}
		}else
		if(ictsj.qualify_Due_Date__c != trigger.oldmap.get(ictsj.id).qualify_Due_Date__c){
			//Qualify date has changed. move all other dates on by the same difference.
			If(trigger.oldmap.get(ictsj.id).qualify_Due_Date__c > ictsj.qualify_Due_Date__c){
					daysbetween= dateHolidayHandler.getbusinessdays(ictsj.qualify_Due_Date__c,trigger.oldmap.get(ictsj.id).qualify_Due_Date__c);	
					bdatesback = true;
			}else{
					daysbetween= dateHolidayHandler.getbusinessdays(trigger.oldmap.get(ictsj.id).qualify_Due_Date__c,ictsj.qualify_Due_Date__c);
			}
				
			if (daysbetween>0){
				//This call checks that the user has not changed a date to a holiday or weekend
				ictsj.qualify_Due_Date__c = dateHolidayHandler.isdaybusinessday(ictsj.qualify_Due_Date__c,bdatesback);
				
				//Add the difference to all the other dates
				ictsj.sell_Due_Date__c =dateHolidayHandler.adddayswithholidays(ictsj.sell_Due_Date__c,daysbetween,bdatesback);
				ictsj.propose_Due_Date__c =dateHolidayHandler.adddayswithholidays(ictsj.propose_Due_Date__c,daysbetween,bdatesback);
				ictsj.closure_Due_Date__c =dateHolidayHandler.adddayswithholidays(ictsj.closure_Due_Date__c,daysbetween,bdatesback);
			}
		}else
		if(ictsj.sell_Due_Date__c != trigger.oldmap.get(ictsj.id).sell_Due_Date__c){
			//Sell date has changed. move all other dates on by the same difference.
			If(trigger.oldmap.get(ictsj.id).sell_Due_Date__c > ictsj.sell_Due_Date__c){
					daysbetween= dateHolidayHandler.getbusinessdays(ictsj.sell_Due_Date__c,trigger.oldmap.get(ictsj.id).sell_Due_Date__c);	
					bdatesback = true;
			}else{
					daysbetween= dateHolidayHandler.getbusinessdays(trigger.oldmap.get(ictsj.id).sell_Due_Date__c,ictsj.sell_Due_Date__c);
			}
			
			if (daysbetween>0){
				//This call checks that the user has not changed a date to a holiday or weekend
				ictsj.sell_Due_Date__c = dateHolidayHandler.isdaybusinessday(ictsj.sell_Due_Date__c,bdatesback);
				
				//Add the difference to all the other dates
				ictsj.propose_Due_Date__c =dateHolidayHandler.adddayswithholidays(ictsj.propose_Due_Date__c,daysbetween,bdatesback);
				ictsj.closure_Due_Date__c =dateHolidayHandler.adddayswithholidays(ictsj.closure_Due_Date__c,daysbetween,bdatesback);
				
			}
		}else
		if(ictsj.propose_Due_Date__c != trigger.oldmap.get(ictsj.id).propose_Due_Date__c){
			//Propose date has changed. move all other dates on by the same difference.
			If(trigger.oldmap.get(ictsj.id).propose_Due_Date__c > ictsj.propose_Due_Date__c){
					daysbetween= dateHolidayHandler.getbusinessdays(ictsj.propose_Due_Date__c,trigger.oldmap.get(ictsj.id).propose_Due_Date__c);	
					bdatesback = true;
			}else{
					daysbetween= dateHolidayHandler.getbusinessdays(trigger.oldmap.get(ictsj.id).propose_Due_Date__c,ictsj.propose_Due_Date__c);
			}
			
			if (daysbetween>0){
				//This call checks that the user has not changed a date to a holiday or weekend
				ictsj.propose_Due_Date__c = dateHolidayHandler.isdaybusinessday(ictsj.propose_Due_Date__c,bdatesback);
				
				//Add the difference to all the other dates
				ictsj.closure_Due_Date__c =dateHolidayHandler.adddayswithholidays(ictsj.closure_Due_Date__c,daysbetween,bdatesback);
			}
		}
	}
	}