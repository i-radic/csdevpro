trigger tgrAttachmentBefore on Attachment (before delete, before insert, before update) {
    if(Trigger.isDelete) {
        // List of attachment record ids to update
        Set<Id> attIds = new Set<Id>();
        // List of parent record ids to update
        Set<Id> cssIds = new Set<Id>();
        //// List of parent record ids to update
        Set<Id> suppReqIds = new Set<Id>();
        // In-memory copy of parent records
        Map<Id,Customer_Sales_Support__c> cssRecords = new Map<Id, Customer_Sales_Support__c>();
        //// In-memory copy of parent records
        Map<Id,Support_Request__c> suppReqRecords = new Map<Id, Support_Request__c>();
        // Validate AttachmentId which is deleted for the Parent
        Map<Id,Id> cssRecordIds = new Map<Id, Id>();
        ///// Validate AttachmentId which is deleted for the Parent
        Map<Id,Id> suppReqRecordIds = new Map<Id, Id>();
        
        // Gather the list of ID values to query on
        for(Attachment a : Trigger.isDelete?Trigger.old:Trigger.new) {
            system.debug('Attachment Record Id : ' + a.Id);
            attIds.add(a.Id);
            cssRecordIds.put(a.Id, a.Id);
            ////
            suppReqRecordIds.put(a.Id, a.Id);
        }
        // Avoid null ID values
        attIds.remove(null);
        // Get Only Customer Support Record as ParentId's
        for(Attachment a:[SELECT Id, ParentId, Parent.Type FROM Attachment where Id IN :attIds]) {
            system.debug('Parent Record Type : ' + a.Parent.Type);
            system.debug('Parent Record Id : ' + a.ParentId);
            if(a.Parent.Type == 'Customer_Sales_Support__c') {
                cssIds.add(a.ParentId);
                system.debug('Attachment Is CSS : Yes ' + a.ParentId);
            }
            ////
            if(a.Parent.Type == 'Support_Request__c') {
                suppReqIds.add(a.ParentId);
                system.debug('Attachment Is Supp Request: Yes ' + a.ParentId);
            }
        }
        // Avoid null ID values
        cssIds.remove(null);        
        ///// Avoid null ID values
        suppReqIds.remove(null);    
        // Create in-memory copy of parents     
        for(Customer_Sales_Support__c css : [SELECT Id, Attachments_Count__c FROM Customer_Sales_Support__c where Id IN : cssIds]) {
            cssRecords.put(css.Id,css);
        }
        //// Create in-memory copy of parents     
        for(Support_Request__c suppreq : [SELECT Id, Attachments_Count__c FROM Support_Request__c where Id IN : suppreqIds]) {
            suppreqRecords.put(suppreq.Id,suppreq );
        }
        // Query all children for all parents, update Rollup Field value
        for(Attachment a:[SELECT Id, ParentId, Parent.Type FROM Attachment where ParentId IN :cssIds]) {
            system.debug('Entered Into This Loop : ' + a.Id);
            if(cssRecordIds.containsKey(a.Id)) {    
                cssRecords.get(a.ParentId).Attachments_Count__c -= 1;
            }
        }
        //// Query all children for all parents, update Rollup Field value
        for(Attachment a:[SELECT Id, ParentId, Parent.Type FROM Attachment where ParentId IN :suppreqIds]) {
            system.debug('Entered Into This Loop : ' + a.Id);
            if(suppreqRecordIds.containsKey(a.Id)) {    
                suppreqRecords.get(a.ParentId).Attachments_Count__c -= 1;
            }
        }
        // Commit changes to the database
        Database.update(cssRecords.values());
        //// Commit changes to the database
        Database.update(suppreqRecords.values());
    }
}