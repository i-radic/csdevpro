trigger CloudVoiceSiteBefore on Cloud_Voice_Site__c (before update, before insert) {

//RecordType siteRT = [SELECT Id,Name FROM RecordType WHERE DeveloperName='Cloud_Voice_Modify_Service_Only'];
String siteRT = '01220000000Avlx';
if(UserInfo.getOrganizationId() =='00Dg0000006RI2x'){
	siteRT = '012g0000000D6TU';
}

for(Cloud_Voice_Site__c cvs:Trigger.New){
    
    cvs.zStatus__c = cvs.zAccess__c +'|'+ cvs.zLicenses__c +'|'+ cvs.zCPE__c +'|'+ cvs.zLAN__c +'|'+ cvs.zNumbers__c +'|'+ cvs.zOther__c;
    
    if(cvs.RecordTypeId == siteRT){
        if(cvs.LocationCodeCheck__c == Null){
            cvs.LocationCodeCheck__c = 'Cloud_Voice_Modify_Service_Only_'+ system.today();
        }
    }else if(cvs.Existing_Site__c != Null){
        if(cvs.LocationCodeCheck__c == Null){
            cvs.LocationCodeCheck__c = 'MODIFY_'+cvs.Existing_Site__c+'_'+ system.now();
        }
    }else if(cvs.Location_Code__c == Null){
        cvs.LocationCodeCheck__c = cvs.AccountId__c + '_' + cvs.Id;
    }else{
        cvs.LocationCodeCheck__c = cvs.AccountId__c + '_' + cvs.Location_Code__c;
    }

    if(Trigger.isUpdate){
        Cloud_Voice_Site__c cvsOld = Trigger.oldMap.get(cvs.Id); 
    }
        
    if(cvs.Use_Contact_Address_for_Site__c == True){
         cvs.Site_address__c = cvs.Site_contact_address__c.replaceAll('_BR_ENCODED_','\n');
    }
    if(cvs.Use_Site_Address_for_Billing__c == True){
        cvs.Billing_address__c = cvs.Site_address__c;
    }    
    if(cvs.Use_Site_Address_for_Delivery__c == True){
        cvs.Delivery_address__c = cvs.Site_address__c;
    }
}

}