<apex:page standardController="cscfga__Product_Basket__c" extensions="CS_BasketViewerCtrl" action="{!init}">
<apex:includeScript value="{!$Resource.cspduia__jQuery}" />
<style type="text/css">
.oRight { position: relative; }

h3.section {
    padding: 0.75em 0;
    font-size: 1.25em;
    display: block;
}

.bold { font-weight: bold; }

.numeric { text-align: right; }

.basketTable {
    width: 100%;
    border-collapse: collapse;
}

.basketTable tr td {
    border: 1px solid #bbb;
    vertical-align: middle;
}

.basketTable tr th { text-align: center; }
.basketTable tr th.left { text-align: left; }
.basketTable tr th.center { text-align: center; }
.basketTable tr th.right { text-align:right; }
.basketTable tr td.left { text-align: left; }
.basketTable tr td.center { text-align: center; }
.basketTable tr td.right { text-align: right; }

.basketTable tbody tr td div {
    position: relative;
    padding: 2px 4px;
}

.basketTable tbody.valid tr td { background:#DEF2E0; }
.basketTable tbody.valid:hover tr td { background: #CAF0CE; }
.basketTable tbody.requiresUpdate tr td { background: #F5F4E0; }
.basketTable tbody.requiresUpdate:hover tr td { background: #ECF0CA; }
.basketTable tbody.incomplete tr td { background: #FAD2D5; }
.basketTable tbody.incomplete:hover tr td { background: #F0CACA; }
.basketTable tbody.other tr td { background: #E7E6F0; }
.basketTable tbody.other:hover tr td { background: #D9D6ED; }
.basketTable tbody.processing tr td { background: #eef; }
.basketTable tbody.processing:hover tr td { background: #ddf; }
.basketTable tbody.processing tr td div { opacity: 0.5; }

.basketTable tbody.processing tr td div > div {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    background: transparent url('{!$Resource.TransparentPNG}') repeat 0 0;
}

.basketTable tbody tr td.message {
    background: #ffe;
    color: #a40;
    font-weight: bold;
    padding: 0.5rem;
}

.basketTable tbody:hover tr td.message { background: #ffd; }

.basketTable tbody tr td.message img {
    float: left;
    margin-right: 0.5rem;
}

.basketTable tr td.actions { width: 75px; }
/*
.modal .basketTable { table-layout: fixed; }
.modal .basketTable tr td { width: 15%; }
*/

.basketTable tr td input {
    border: none;
    height: 20px;
    width: 20px;
    background-image: none;
    background-color: transparent;
    outline: none;
    cursor: pointer;
    background-position: 0 0;
}

.basketTable tr td input:hover {
    background-position: 0 0;
    box-shadow: 1px 1px 3px gray;
}

.basketTable tr td input.delete { background-image: url('/img/func_icons/util/trash20.gif'); }
.basketTable tr td input.edit { background-image: url('/img/func_icons/util/customize20.gif'); }

.basketTable tr td input.copy {
    background-image: url('/img/func_icons/util/export20.gif');
    /*display: none;*/
}

.basketTable tr td input.refresh {
    background-image: url('/img/alohaSkin/sync.png');
    display: none;
}

.basketTable tr td input.details { background-image: url('/img/func_icons/util/lookup20.gif'); }

.basketTable tr td input.processing {
    background-image: url('/img/icon/custom51_100/gears16.png');
    background-repeat: no-repeat;
    background-position: center;
}

.overlay {
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    background: rgba(200, 200, 200, 0.5);
    z-index: 100;
}

.modal {
    position: fixed;
    top: 10%;
    left: 10%;
    bottom: 10%;
    right: 10%;
    background: #fff;
    z-index: 110;
    border-radius: 1rem;
    padding: 1rem;
}

.modal-header {
    border-bottom: 1px solid #ddd;
    padding: 0 0 0.5rem 0.5rem;
    overflow: hidden;
    position: absolute;
    top: 1rem;
    left: 1rem;
    right: 1rem;
}

.modal-header h3 {
    float: left;
    padding-top: 0.5rem;
}

.modal-header .modal-cancel {
    float: right;
    background-image: none;
    border-radius: 1rem;
    height: 1.5rem;
    width: 1.5rem;
    border: none;
    outline: none;
    cursor: pointer;
    background-color: #ddd;
}

.modal-header .modal-cancel:hover { background-color: #ccc; }

.modal-content {
    overflow: auto;
    position: absolute;
    left: 1rem;
    bottom: 1rem;
    right: 1rem;
    top: 3rem;
}

.modal-inside-wrapper { padding: 1rem 0.2rem 0 0.2rem; }
.productBasket { position: relative; }

.basketOverlay {
    position: absolute;
    top: 50px;
    left: 0;
    bottom: 0;
    right: 0;
    background: rgba(200, 200, 200, 0.5);
    z-index: 100;
    text-align: center;
    font-weight: bold;
    font-size: 1.25rem;
}

.basketOverlay img {
    display: block;
    margin: 1rem auto;
}

.fullBasketOverlay {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    z-index: 100;
    background: rgba(200,200,200,0.5) url('{!$Resource.TransparentPNG}') repeat 0 0;
}

.copyBasketDrop, copyBasketOverlay {
    position: fixed;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
}

.copyBasketDrop {
    overflow: auto;
    z-index: 100;
    background: rgba(200,200,200,0.5);
    padding: 160px;
}

.copyBasketDialog {
    position: relative;
    margin: 0 auto;
    width: 450px;
    max-width: 100%;
    overflow: hidden;
    padding: 1rem;
    border-radius: 0.5rem;
    -moz-border-radius: 0.5rem;
    -webkit-border-radius: 0.5rem;
    background: #fff;
}

.copyBasketDialog header, .copyBasketDialog label {
    display: block;
    margin-bottom: 1rem;
}

.copyBasketDialog header { font-size: 1rem; }

.copyBasketDialog input[type="button"] {
    float: right;
    cursor: pointer;
    margin: 0 0.25rem;
}

.copyBasketDialog header input {
    background: #ddd;
    height: 1.25rem;
    width: 1.25rem;
    border-radius: 1rem;
    -moz-border-radius: 1rem;
    -webkit-border-radius: 1rem;
    border: none;
}
</style>
<script type="text/javascript">
function deleteConfig(pcId) {
    if ({!currOpportunityClosed}) {
        alert('It is not possible to delete configurations if opportunity stage is Closed!')
        return;
    }

    if(confirm('Are you sure?'))
        delProduct(pcId);
}

function newConfig() {
    var urlString = '/apex/cscfga__SelectProductCategoryForBasket?retURL=%2F{!basketId}&linkedId={!basketId}';
    window.location.href = urlString;
}

function openConfig(basketId, pcId) {
    if ({!currOpportunityClosed && !($Profile.Name == 'System Administrator')}) {
        alert('It is not possible to edit configuration if opportunity stage is Closed!');
        return;
    }
    var urlString = '/apex/cscfga__ConfigureProduct?configId=' + pcId + '&retURL=%2F' + basketId;
    window.location.replace(urlString);
}
</script>
<apex:outputPanel id="fullBasket" layout="block">
<apex:pageMessages />
<apex:detail id="basketLayoutDetail" inlineEdit="true" subject="{!basketId}" rerender="productBasket,basketLayoutDetail" />
<apex:form >
<apex:outputPanel id="productBasket" layout="block" styleClass="productBasket">
    <apex:pageMessages />
    <apex:actionStatus id="basketStatus">
        <apex:facet name="start">
            <apex:outputPanel layout="block" styleClass="basketOverlay">
                <apex:image value="/img/loading32.gif" />
                <apex:outputText >Processing</apex:outputText>
            </apex:outputPanel>
        </apex:facet>
    </apex:actionStatus>
    <!--
    Basket summary block
    -->
    <apex:pageBlock title="Basket Items">
        <apex:outputPanel style="color:red">If any amendments to this deal are needed, please create a new Product Basket and re-create the deal.</apex:outputPanel>
        <apex:actionFunction id="loadSubProducts" action="{!loadSubProducts}" name="loadSubProducts" rerender="productBasket,basketLayoutDetail">
            <apex:param id="currProduct" name="currProduct" assignTo="{!currProduct}" value="" />
        </apex:actionFunction>
        <apex:actionFunction id="delProduct" name="delProduct" action="{!delProduct}" reRender="productBasket,basketLayoutDetail" status="basketStatus" oncomplete="location.reload(true);">
            <apex:param id="currProd" name="currProd" assignTo="{!currProduct}" value="" />
        </apex:actionFunction>
        <apex:actionFunction id="cancelSubProducts" name="cancelSubProducts" action="{!cancelSubProducts}" rerender="productBasket,basketLayoutDetail" />
        <apex:outputPanel id="dummmyPanel" />
        <apex:outputPanel layout="block">
            <apex:repeat value="{!tables}" var="table">
                <h3 class="section"><apex:outputText value="{!table.name}" /></h3>
                <table class="basketTable">
                    <tr>
                        <th>Action</th>
                        <th class="left">Name</th>
                        <apex:repeat value="{!table.columns}" var="column">
                            <th class="{!column.headerStyle}"><apex:outputText value="{!column.name}" /></th>
                        </apex:repeat>
                    </tr>
                    <apex:repeat value="{!table.rows}" var="row">
                        <tbody class="{!IF(row.status == 'Valid','valid',IF((row.status == 'Requires Update'),'requiresUpdate',IF((row.status == 'Incomplete'),'incomplete','other')))}">
                            <tr>
                                <td class="actions" rowspan="2">
                                    <div>
                                        <apex:outputPanel rendered="{!(!basketApprovalInProgress || $Profile.Name == 'System Administrator' || $Profile.Name == 'System Administrator(Custom)' || $Profile.Name == 'Business Academy' || $Profile.Name == 'Finance' || $Profile.Name == 'Finance - Deal Review')}">
                                            <input type="button" class="edit" onclick="openConfig('{!basketId}', '{!row.recordId}')" />
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!(basketApprovalInProgress && !($Profile.Name == 'System Administrator' || $Profile.Name == 'System Administrator(Custom)' || $Profile.Name == 'Business Academy' || $Profile.Name == 'Finance' || $Profile.Name == 'Finance - Deal Review'))}">
                                            <input type="button" class="edit" onclick="" />
                                        </apex:outputPanel>
                                        <input type="button" class="delete" onclick="deleteConfig('{!row.recordId}')" />
                                        <input type="button" class="details" onclick="loadSubProducts('{!row.recordId}')" />
                                        <input type="button" class="refresh" />
                                        <div></div>
                                    </div>
                                </td>
                                <td rowspan="2">
                                    <div>
                                        <apex:outputpanel rendered="{!$Profile.Name == 'System Administrator'}">
                                        <a href="../{!row.recordId}" target="_blank">{!row.name}</a>
                                        </apex:outputpanel>
                                        <apex:outputpanel rendered="{!$Profile.Name != 'System Administrator'}">
                                            <apex:outputText value="{!row.name}" />
                                        </apex:outputpanel>
                                        <div></div>
                                    </div>
                                </td>
                                <apex:repeat value="{!table.columns}" var="column">
                                    <td class="{!column.style}">
                                        <apex:outputPanel rendered="{!column.type == 'Decimal'}">
                                            <apex:outputText value="{!column.format}">
                                                <apex:param value="{!row.fields[column.name].data}" />
                                            </apex:outputText>
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!column.type == 'Boolean'}">
                                            <c:outputcheckbox value="{!row.fields[column.name].data}" />
                                        </apex:outputPanel>
                                        <apex:outputPanel rendered="{!column.type == 'String'}">
                                            <apex:outputText value="{!row.fields[column.name].data}" />
                                        </apex:outputPanel>
                                    </td>
                                </apex:repeat>
                            </tr>
                        </tbody>
                    </apex:repeat>
                </table>
            </apex:repeat>
        </apex:outputPanel>
        <apex:outputPanel rendered="{!subProducts != null}" layout="block" styleClass="overlay" onclick="cancelSubProducts()" />
        <apex:outputPanel rendered="{!subProducts != null}" layout="block" styleClass="modal">
            <apex:outputPanel layout="block" styleClass="modal-header">
                <h3>Services for {!currProductName}</h3>
                <input type="button" value="X" class="modal-cancel" onclick="cancelSubProducts()" />
            </apex:outputPanel>
            <apex:outputPanel layout="block" styleClass="modal-content">
                <apex:outputPanel layout="block" styleClass="modal-inside-wrapper">
                    <table class="basketTable">
                        <tr>
                            <th class="left">Name</th>
                            <th>Total expected revenue</th>
                            <th>Gross margin</th>
                            <th>Tenure</th>
                        </tr>
                        <tbody class="valid">
                            <tr>
                                <td><a href="../{!currProduct}" target="_blank"><b>{!currProductName}</b></a></td>
                                <td class="numeric">
                                    <div>
                                        <apex:outputText value="{0, number, £#,###,##0.00}">
                                            <apex:param value="{!currProductTotalRevenue}" />
                                        </apex:outputText>
                                        <div></div>
                                    </div>
                                </td>
                                <td class="numeric">
                                    <apex:outputText value="{0, number, 0.00 %}">
                                        <apex:param value="{!currProductGrossMargin / 100}" />
                                    </apex:outputText>
                                </td>
                                <td class="center"><apex:outputText value="{!currProductTenure}" /></td>
                            </tr>
                        </tbody>
                    </table>
                    <!-- Subsection - popup-->
                    <apex:repeat value="{!orderedSubDefinitions}" var="def">
                        <h3 class="section">{!subDefinitionMap[def]}</h3>
                        <table class="basketTable">
                            <tr>
                                <th class="left">Name</th>
                                <th>Total expected revenue</th>
                                <th>Gross margin</th>
                                <th>Tenure</th>
                                <th style="{!IF(subDefinitionMap[def] != 'Mobile Broadband User Group', 'display:none', '')}">{!IF(subDefinitionMap[def] == 'Mobile Broadband User Group', 'FCC', '')}</th>
                                <th style="{!IF(subDefinitionMap[def] != 'User Group', 'display:none', '')}">{!IF(subDefinitionMap[def] == 'User Group', 'FCC', '')}</th>
                            </tr>
                            <apex:repeat value="{!subProducts[def]}" var="product">
                                <tbody class="{!IF(product.status == 'Valid', 'valid', IF((product.status == 'Requires Update'), 'requiresUpdate', IF((product.status == 'Incomplete'), 'incomplete', 'other')))}">
                                    <tr>
                                        <td>
                                            <div>
                                                <a href="../{!product.recordId}" target="_blank"><b>{!product.Name}</b></a>
                                                <div></div>
                                            </div>
                                        </td>
                                        <td class="numeric">
                                            <div>
                                                <apex:outputText value="{0, number, £#,###,##0.00}">
                                                    <apex:param value="{!product.totalExpectedRevenue}" />
                                                </apex:outputText><div></div>
                                            </div>
                                        </td>
                                        <td class="numeric">
                                            <div>
                                                <apex:outputText value="{0, number, 0.00 %}">
                                                    <apex:param value="{!product.grossMargin / 100}" />
                                                </apex:outputText>
                                                <div></div>
                                            </div>
                                        </td>
                                        <td class="center">
                                            <div>{!product.tenure}<div></div></div>
                                        </td>
                                        <td class="center" style="{!IF(subDefinitionMap[def] != 'Mobile Broadband User Group', 'display: none', '')}">{!IF(subDefinitionMap[def] == 'Mobile Broadband User Group', product.fcc, '')}</td>
                                        <td class="center" style="{!IF(subDefinitionMap[def] != 'User Group', 'display: none', '')}">{!IF(subDefinitionMap[def] == 'User Group', product.fcc, '')}</td>
                                    </tr>
                                    <apex:outputPanel layout="none" rendered="{!product.subProducts.size>0}">
                                        <apex:repeat value="{!product.subProducts}" var="sub">
                                            <tbody class="{!IF(sub.status == 'Valid', 'valid', IF((sub.status == 'Requires Update'), 'requiresUpdate', IF((sub.status == 'Incomplete'), 'incomplete', 'other')))}">
                                                <tr>
                                                    <td>
                                                        <div><img src="/img/func_icons/ispan12.gif" /> <a href="../{!sub.recordId}" target="_blank">{!sub.Name}</a><div>
                                                        </div></div>
                                                    </td>
                                                    <td class="numeric">
                                                        <div>
                                                            <apex:outputText value="{0, number, £#,###,##0.00}">
                                                                <apex:param value="{!sub.totalExpectedRevenue}" />
                                                            </apex:outputText>
                                                            <div></div>
                                                        </div>
                                                    </td>
                                                    <td class="numeric">
                                                        <div>
                                                            <apex:outputText value="{0, number, 0.00 %}">
                                                                <apex:param value="{!sub.grossMargin / 100}" />
                                                            </apex:outputText>
                                                            <div></div>
                                                        </div>
                                                    </td>
                                                    <td class="center">
                                                        <div>{!sub.tenure}<div>
                                                        </div></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </apex:repeat>
                                    </apex:outputPanel>
                                </tbody>
                            </apex:repeat>
                        </table>
                    </apex:repeat>
                </apex:outputPanel>
            </apex:outputPanel>
            <!--
            <hr style="color: red;" />
            <c:CS_ProductConfigurationTreeView
                jsonResourceName="CS_SubProductTreeJSON"
                productConfigurationTree="{!filteredProdConfigTree}" />
            <hr style="color: red;" />
            -->
        </apex:outputPanel>
    </apex:pageBlock>
</apex:outputPanel>
</apex:form>
</apex:outputPanel>
</apex:page>