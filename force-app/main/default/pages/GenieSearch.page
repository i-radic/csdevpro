<apex:page controller="GenieController" standardStylesheets="false" tabstyle="The_Genie__tab" title="The Genie - Search results">
<apex:stylesheet value="{!URLFOR($Resource.GenieResources, '/v2/css/genie.css')}"/>
<apex:includeScript value="/EXT/ext-3.2.2/ext.js"/>
<!-- JQuery Files -->
<apex:includeScript value="{!URLFOR($Resource.GenieResources, '/v2/js/libs/jquery-1.11.3.min.js')}"/>
<apex:includeScript value="{!URLFOR($Resource.GenieResources, '/v2/js/libs/jquery-ui-1.9.2.custom.min.js')}"/>
<apex:includeScript value="{!URLFOR($Resource.GenieResources, '/v2/js/libs/modernizr.min.js')}"/>
<!-- Javascript Files -->
<script type="text/javascript" src="{!URLFOR($Resource.GenieResources, '/v2/js/btb.thegenie.js')}"></script>
<script type="text/javascript" src="{!URLFOR($Resource.GenieResources, '/v2/js/btb.thegenie.salesforce.js')}"></script>
<script type="text/javascript" src="{!URLFOR($Resource.GenieResources, '/v2/js/btb.thegenie.omniture.js')}"></script>
<style>.banners{overflow:hidden}</style>
<script type="text/javascript">
    // Search suggest
    function render(template, data) {
        return template.replace(/%(\w*)%/g,function(m,key){return data.hasOwnProperty(key)?data[key]:"";});
    };
    
    var searchSuggest = {
        settings : {
            template :              '<li><a href="%url%" title="%title%" target="%target%"><i></i>%title% <span class="right">%tagtype%</span></a></li>',
            count :                 0,
            items :                 []
        },
        data : {
            suggestItems :          [],
            results :               ''
        },
        
        handleTags : function(tags) {
            /* Manually entered */
            searchSuggest.data.suggestItems.push( {title: 'Case Studies', tagtype: 'The Genie library', url: 'https://btbusiness.my.salesforce.com/apex/GenieLibrary?section=Case Studies&display=Case Studies', target: '_self'} );
            searchSuggest.data.suggestItems.push( {title: 'Brochure generators', tagtype: 'Link', url: ' https://channeldocuments.com/landing/', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'BTNet brochure generator', tagtype: 'Link', url: ' https://channeldocuments.com/net', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'Tags On The Line', tagtype: 'Link', url: 'http://www.dslchecker.bt.com/adsl/totl.login?identifier=O%2FM57089700&password=btr1208&login.x=34&login.y=11', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'Mobile Coverage checker', tagtype: 'Link', url: 'https://coverage.ee.co.uk/bt/coveragechecker.html', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'Call pricing lookup', tagtype: 'Link', url: 'https://myprofile.bt.com/sites/genie/SitePages/Call%20Pricing%20Lookup.aspx', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'AX Form', tagtype: 'Link', url: 'https://office.bt.com/sites/CBSProcessandProcedures/Calls%20and%20Lines/AX%20Order%20Form.doc', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'Mobile Proposal Tool (Corporate only)', tagtype: 'Link', url: 'https://corporate.channeldocuments.com/mobile/', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'BTnet Bespoke Pricing Tool', tagtype: 'Link', url: 'https://btbusiness.my.salesforce.com/069200000027K43', target: '_self'} );
            searchSuggest.data.suggestItems.push( {title: 'Current offers', tagtype: 'Link', url: 'https://btbusiness.my.salesforce.com/apex/GenieTagDetail?name=Current offers', target: '_self'} );
            searchSuggest.data.suggestItems.push( {title: 'Call charges', tagtype: 'Link', url: 'https://myprofile.bt.com/sites/genie/SitePages/Call%20Pricing%20Lookup.aspx', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'International calls', tagtype: 'Link', url: 'https://myprofile.bt.com/sites/genie/SitePages/Call%20Pricing%20Lookup.aspx', target: '_blank'} );
            searchSuggest.data.suggestItems.push( {title: 'AM Summary TCO', tagtype: 'Link', url: 'https://btbusiness.my.salesforce.com/06920000002TELK', target: '_self'} );
            searchSuggest.data.suggestItems.push( {title: 'UVM TCO', tagtype: 'Link', url: 'https://btbusiness.my.salesforce.com/069200000028Zey', target: '_self'} );

            /* Tags */
            for (var i = 0; i < searchSuggest.settings.count; i++) {
                searchSuggest.data.suggestItems.push( {title: tags[i].Name, tagtype: tags[i].Tag_Type__c, url: 'https://btbusiness.my.salesforce.com/apex/GenieTagDetail?name=' + tags[i].Name, target: '_self'} );
                if (typeof tags[i].Synonyms__c != 'undefined') {
                    var synonyms = tags[i].Synonyms__c.split('; ');
                    for (var j = 0; j < synonyms.length; j++) {
                        searchSuggest.data.suggestItems.push( {title: synonyms[j].replace(/;+$/, ''), tagtype: tags[i].Name, url: 'https://btbusiness.my.salesforce.com/apex/GenieTagDetail?name=' + tags[i].Name, target: '_self'} );
                    }
                }
            }
        },
        
        getData : function() {
            Visualforce.remoting.Manager.invokeAction('{!$RemoteAction.GenieController.getTags}',
                function(result, event){           
                    if (event.status && event.result) {
                        searchSuggest.settings.count = result.length;
                        searchSuggest.handleTags(event.result);
                    } else {
                        alert('An error occured getting list!');
                    }
                }, {escape:true}
            );
        },
        
        getSuggest : function(term) {
            var matches = 0;
            searchSuggest.data.results = '';
            j$.each(searchSuggest.data.suggestItems, function(key, item) {
                if (item.title.toLowerCase().indexOf(term.toLowerCase()) >= 0 && matches <= 10) {
                    searchSuggest.data.results += render(searchSuggest.settings.template, {title: item.title, tagtype: item.tagtype, url: item.url});
                    matches++;
                }
                else {
                    j$('#suggest-results').html(searchSuggest.data.results);
                }
            });
            if (matches !== 0) {
                var pos = j$('#search-keyword').offset();
                if(j$('.bPageHeader').css('display') != 'none') {
                    pos.top = pos.top - j$('.bPageHeader').height();
                }
                j$('#suggest').css({
                    display: 'block',
                    position: 'absolute',
                    top: pos.top + 35,
                    left: pos.left - 10
                });
            } else {
                j$('#suggest-results').html('');
                j$(this).css({
                    display: 'none',
                    top: -1000
                });
            }
        },
        
        init : function() {
            searchSuggest.getData();
            j$('#search-keyword').on('focus', function(){
                if (j$(this).val() == 'Search the Genie') {
                    j$(this).val('');
                }
            });
            j$('#search-keyword').on('keyup', function() {
                searchSuggest.getSuggest(j$(this).val());
            });
            j$('#genie').on('keyup', function(e) {
                var code = e.keyCode || e.which;
                if (code == 13) {
                    e.preventDefault();
                    if (j$('#suggest-results').children().length > 0) {
                        window.location = j$('#suggest-results li:first-child a').attr('href');
                    } else {
                        window.location = '/apex/GenieSearch?search=' + j$('#search-keyword').val();
                    }
                }
            });
            j$('#genie').on('click', '#search-submit', function(e) {
                if (j$('#suggest-results').children().length > 0) {
                    window.location = j$('#suggest-results li:first-child a').attr('href');
                } else {
                    window.location = '/apex/GenieSearch?search=' + j$('#search-keyword').val();
                }
            });
            j$('#suggest').mouseleave(function(e){ 
                j$(this).css({
                    display: 'none',
                    top: -1000
                });
            }); 
        }
    };
    
    var remoting = false;
    
    function toggleFullscreen() {
        if (j$('#AppBodyHeader').css('display') == 'block') {
            j$('#AppBodyHeader').css('display', 'none');
        } else {
            j$('#AppBodyHeader').css('display', 'block');
        }
        if (j$('.sidebarCollapsible').hasClass('sidebarCollapsed')) {
            j$('.sidebarCollapsible').removeClass('sidebarCollapsed');
        } else {
            j$('.sidebarCollapsible').addClass('sidebarCollapsed');
        }
    };
    
    function fixSalesforceLinks() {
        j$('a[href^="https://btbusiness.my.salesforce.com/"]').removeAttr('target');
        setTimeout(fixSalesforceLinks, 500);
    };
    
    var footerMenu = {
        init : function() {
            j$('#genie').on('click', '#footerQuickLinks', function() {
                if (!j$(this).parent().hasClass('selected')) {
                    j$('.footerMenuQuickLink').removeClass('selected');
                    j$('.menuModule').css('display','none');
                    j$(this).parent().addClass('selected');
                    j$(this).next().css('display','block');
                } else {
                    j$(this).parent().removeClass('selected');
                    j$(this).next().css('display','none');
                }
            });
            j$('#genie').on('click', '.close', function() {
                j$(this).parent().parent().removeClass('selected');
                j$(this).parent().css('display','none');
            });
        }
    };
    
    j$(document).ready(function() {        
        //BTB.theGenie.init();
        //BTB.theGenie.salesforce.init();
        searchSuggest.init();
        BTB.theGenie.omniture.init('', 'Search results', '{!$User.OUC__c}');
        
        footerMenu.init();
        fixSalesforceLinks();

        j$('#genie').on('click', '.tab-title', function(e){
            j$('.tab-title').removeClass('active');
            j$(this).addClass('active');
            e.preventDefault();
        });
        j$('#genie').on('click', '.toggleFullScreen', toggleFullscreen);
    });
</script>

<div id="genie">
    <div class=" ">
        <div class="content-wrap masthead">
            <div class="row">
                <ul id="topNav" class="sectionNav" role="navigation">
                    <li class="first">
                        <a href="https://btbusiness.my.salesforce.com/apex/GenieHome">The Genie home</a>
                    </li>
                    <li>
                       <a href="https://btbusiness.my.salesforce.com/apex/GenieProducts">Products &amp; services</a>
                    </li>
                    <li>
                        <a href="https://btbusiness.my.salesforce.com/apex/GenieCampaign">Connected business</a>
                    </li>
                    <li>
                        <a href="https://btbusiness.my.salesforce.com/apex/GenieLibrary">The Genie library</a>
                    </li>
                    <li class="last toggleFullScreen">Toggle Fullscreen</li>
                </ul>
                <!-- Search Bar -->
                <apex:form id="search">
                <div class="row">
                    <div class="search-container">
                        <div class="columns five"><input id="search-keyword" placeholder="Search the Genie" class="search-keyword" type="text" autocomplete="off"/></div>
                        <div class="columns one"><a href="javascript:void(0)" id="search-submit" class="button round">Go</a></div>
                    </div>
                </div>
                </apex:form>
            </div>
        </div>
    </div>
    <div class="banner-main banner-home banner-image" style="margin-bottom:20px" >
        <div class="content-wrap" style="background-image:url({!URLFOR($Resource.GenieResources, '/v2/images/banners/banner-fractals.png')})">
            <div class="row">
                <div class="columns overlay">
                    <div class="centerer">
                        <h1>Search Results</h1>
                        <p>You searched for <strong>"{!search}"</strong>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <apex:outputPanel layout="block" rendered="{!AND(searchTags != null, searchTags.size > 0)}">
        <div class="content-wrap">
            <div class="row">
                <apex:repeat value="{!searchTags}" var="tagResult"> 
                    <div class="columns four pages-search-results">
                        <h3><a href="/apex/GenieTagDetail?name={!tagResult.Name}">{!tagResult.Name}</a></h3>
                        <p>{!tagResult.Description__c}</p>
                    </div>
                </apex:repeat>
            </div>
        </div>
    </apex:outputPanel>
    <apex:outputPanel layout="block" styleClass="blue-10" style="padding:20px 0 30px" rendered="{!AND(searchDocuments != null, searchDocuments.size > 0)}">
        <div class="content-wrap">
            <div class="row">
                <div class="columns twelve">
                    <h2>{!searchDocuments.size} documents found</h2>
                </div>
            </div>
        </div>
    </apex:outputPanel>
    <!-- Document results -->
    <apex:outputPanel layout="block" styleClass="search-results-downloads" rendered="{!AND(searchDocuments != null, searchDocuments.size > 0)}">
        <div class="content-wrap">
            <div class="row">
            <apex:repeat value="{!searchDocuments}" var="document" id="documentsListRepeat">
                <div class="columns twelve results">
                    <h3><a href="/{!document.Id}" target="_blank">{!document.Title}</a></h3>
                    <p>{!document.Description}</p>
                    <p></p>
                    <table class="metaData fancyBorders" width="100%">
                        <tbody>
                            <tr>
                                <th style="width:20%">Document type</th>
                                <td style="width:20%">{!document.Document_Type__c}</td>
                                <th style="width:15%">Audience</th>
                                <td style="width:15%">{!document.Audience__c}</td>
                                <th style="width:15%">Last updated</th>
                                <td style="width:15%">
                                    <apex:outputText value="{0,date,dd'/'MM'/'yyyy}">
                                        <apex:param value="{!document.LastModifiedDate}"/>
                                    </apex:outputText>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <hr class="" />
                </div>
            </apex:repeat>
            </div>
        </div>
    </apex:outputPanel>
    
    <apex:outputPanel layout="block" rendered="{!AND(searchTags.size <= 0, searchDocuments.size <= 0)}">
        <div class="content-wrap">
            <div class="row">
                <div class="columns twelve">
                    <h2>No results found</h2>
                    <p>Sorry we were unable to find any matches for <strong>"{!search}"</strong>, if you are having any problems finding content please use the <strong>Contact us</strong> form at the bottom of the page.</p>
                </div>
            </div>
        </div>
    </apex:outputPanel>
    <!-- Footer Menu -->
    <div class="row footerMenu">
        <div class="content-wrap">
            <div class="footerMenuInner">
                <c:GenieFooterMenu />
            </div>
        </div>
    </div>
</div>
<div id="suggest">
    <div class="">
        <ul id="suggest-results"></ul>
    </div>
</div>
<!-- SiteCatalyst code version: H.26.
Copyright 1996-2013 Adobe, Inc. All Rights Reserved
More info available at http://www.omniture.com -->
<script type="text/javascript" src="{!URLFOR($Resource.GenieResources, '/js/s_code.js')}"></script>
<script type="text/javascript">
/* You may give each page an identifying name, server, and channel on
the next lines. */
s.pageName="BTB:Salesforce:Search results"
s.heir1="BT.com,BTB,Salesforce,Search results"
s.channel="BTB"
s.server="https://www.salesforce.com"
s.prop1="{!$User.OUC__c}"
s.prop3="{!search}"
s.prop5="{!searchDocuments.size + searchTags.size}"
s.prop9="Not Logged in"
s.prop36="BTB:Salesforce:Search results"
s.prop37="BTB:Salesforce"
s.eVar36="Not Logged in"
s.eVar37="BTB:Salesforce"
/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
var s_code=s.t();if(s_code)document.write(s_code)</script>
<script type="text/javascript">
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
</script><noscript><img src="http://britishtelecom.112.2o7.net/b/ss/btcomdev/1/H.26--NS/0"
height="1" width="1" border="0" alt="" /></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.26. -->
</apex:page>